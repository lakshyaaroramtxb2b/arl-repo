var camera;
var projector;
//var $trackingOverlay;
//var m;
var width;
var height;
var sphere;
var controls
var clouds;
var renderer;
var scene;
var stats;

var uniforms = null;

var start_time = Date.now();

var kCheckAlertQueueSeconds = 30000; //30 seconds

var markersOverlay = [];
var markersOverlayTarget = [];
var markers = [];
var lines = [];

var kSecondsInDay = 86400;
var kSecondsPerDegree = 240;
var kGlobeOffset = 70;
var kRadius = 0.6371;

var rootDir = "/resource/1404276351000/globe_images/";

(function () {

	var webglEl = document.getElementById('webgl');

	/*
	stats = new Stats();
	stats.domElement.style.position = 'absolute';
	stats.domElement.style.top = '0px';
	webgl.appendChild( stats.domElement );
	*/

	projector = new THREE.Projector();

	deltaFrame = 0;
	lastFrame = Date.now();; 

	if (!Detector.webgl) {
		Detector.addGetWebGLMessage(webglEl);
		return;
	}

	width  = window.innerWidth,
	height = window.innerHeight;

	setTimeout(function(){CheckAlertQueue()},3000); //first call
	setInterval(function(){CheckAlertQueue()},kCheckAlertQueueSeconds);

	// Earth params
	var radius   = kRadius,
		segments = 32,
		rotation = 0;  

	var now = new Date(); 
	var now_utc = new Date(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate(),  now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds());

	var seconds = now_utc.getSeconds();
	var minutes = now_utc.getMinutes();
	var hour = now_utc.getHours();

	

	var secondCount = (hour * 60 * 60) + (minutes * 60) + seconds;

	rotation = ((secondCount / kSecondsPerDegree) + kGlobeOffset) % 360;

	console.log("rotation = " + rotation);

	scene = new THREE.Scene();

	camera = new THREE.PerspectiveCamera(45, width / height, 0.01, 1000);
	camera.position.z = 2.0;

	//var renderer = new THREE.WebGLRenderer();
	renderer = new THREE.WebGLRenderer( { antialias: true, alpha: true } );
	renderer.setSize(width, height);

	scene.add(new THREE.AmbientLight(0x333333));

	var light = new THREE.DirectionalLight(0xffffff, 1);
	light.position.set(0,1.5,5);
	scene.add(light);


	var flareColor = new THREE.Color( 0xffffff );
	flareColor.setHSL( 0.55, 0.9, 0.5 + 0.5 );
	light.color.setHSL( 0.55, 0.9, 0.5 + 0.5);
	
	var textureFlare0 = THREE.ImageUtils.loadTexture( rootDir + "images/lensflare/lensflare0.png" );
	var textureFlare2 = THREE.ImageUtils.loadTexture( rootDir + "images/lensflare/lensflare2.png" );
	var textureFlare3 = THREE.ImageUtils.loadTexture( rootDir + "images/lensflare/lensflare3.png" );

	var lensFlare = new THREE.LensFlare( textureFlare0, 700, 0.0, THREE.AdditiveBlending, flareColor );
	lensFlare.add( textureFlare2, 512, 0.0, THREE.AdditiveBlending );
	lensFlare.add( textureFlare2, 512, 0.0, THREE.AdditiveBlending );
	lensFlare.add( textureFlare2, 512, 0.0, THREE.AdditiveBlending );

	lensFlare.add( textureFlare3, 60, 0.6, THREE.AdditiveBlending );
	lensFlare.add( textureFlare3, 70, 0.7, THREE.AdditiveBlending );
	lensFlare.add( textureFlare3, 120, 0.9, THREE.AdditiveBlending );
	lensFlare.add( textureFlare3, 70, 1.0, THREE.AdditiveBlending );

	lensFlare.customUpdateCallback = lensFlareUpdateCallback;
	lensFlare.position = light.position;
	
	scene.add( lensFlare );


    sphere = createEarth(radius, segments);
	sphere.rotation.y = rotation * Math.PI / 180; 
	scene.add(sphere)

    clouds = createClouds(radius, segments);
	clouds.rotation.y = rotation;
	scene.add(clouds)

	var stars = createStars(90, 64);
	scene.add(stars);

	//var lat = 90 - (Math.acos(y / RADIUS_SPHERE)) * 180 / Math.PI;
	//var lon = ((270 + (Math.atan2(x , z)) * 180 / Math.PI) % 360) -180;


	controls = new THREE.OrbitControls(camera);

	document.addEventListener( 'mousemove', onMouseMove, false );

	controls.autoRotateSpeed = 0.5;
	controls.autoRotate = true; 

	webglEl.appendChild(renderer.domElement);

	render();
}());

function render() {

		deltaFrame = (Date.now() - lastFrame) / 1000; 
		
		if(uniforms != null)
			uniforms.time.value = ( Date.now() - start_time ) / 1000; //seconds

		controls.update();
		positionTrackingOverlay();
		
		//update pos of sun
		sphere.rotation.y += (deltaFrame / kSecondsPerDegree) * Math.PI / 180.0;;

		//console.log(deltaFrame)

		clouds.rotation.y += 0.000015;	

		requestAnimationFrame(render);
		renderer.render(scene, camera);

		lastFrame = Date.now();

		//stats.update();
	}

	function lensFlareUpdateCallback( object ) {

		var f, fl = object.lensFlares.length;
		var flare;
		var vecX = -object.positionScreen.x * 2;
		var vecY = -object.positionScreen.y * 2;


		for( f = 0; f < fl; f++ ) {

			   flare = object.lensFlares[ f ];

			   flare.x = object.positionScreen.x + vecX * flare.distance;
			   flare.y = object.positionScreen.y + vecY * flare.distance;

			   flare.rotation = 0;

		}

		object.lensFlares[ 2 ].y += 0.025;
		object.lensFlares[ 3 ].rotation = object.positionScreen.x * 0.5 + THREE.Math.degToRad( 45 );

	}

	
	function RemoveAlert(index)
	{

	}

	function createMarker(radius, segments) {
		var sphereMaterial =  new THREE.MeshLambertMaterial(
							    {
							      color: 0xCC0000,
							      emissive: 0xff0000
							    });

		return new THREE.Mesh(
		  new THREE.SphereGeometry(
		    radius,
		    segments,
		    segments),

		  sphereMaterial);
	}

	function createDebugMarker(radius, segments) {
		var sphereMaterial =  new THREE.MeshLambertMaterial(
							    {
							      color: 0x0000FF,
							      emissive: 0xff0000
							    });

		return new THREE.Mesh(
		  new THREE.SphereGeometry(
		    radius,
		    segments,
		    segments),

		  sphereMaterial);
	}

	function createEarth(radius, segments) {
		return new THREE.Mesh(
			new THREE.SphereGeometry(radius, segments, segments),
			new THREE.MeshPhongMaterial({
				map:         THREE.ImageUtils.loadTexture(rootDir + 'images/2_no_clouds_4k.jpg'),
				bumpMap:     THREE.ImageUtils.loadTexture(rootDir + 'images/elev_bump_4k.jpg'),
				bumpScale:   0.005,
				specularMap: THREE.ImageUtils.loadTexture(rootDir + 'images/water_4k.png'), 
				shininess:   5,
				specular:    new THREE.Color('grey')								
			})
		);
	}

	function createClouds(radius, segments) {
		return new THREE.Mesh(
			new THREE.SphereGeometry(radius + 0.003, segments, segments),			
			new THREE.MeshPhongMaterial({
				map:         THREE.ImageUtils.loadTexture('/resource/1404278858000/globe_clouds'), /*over single static resource size*/
				transparent: true
			})
		);		
	}

	function createStars(radius, segments) {
		return new THREE.Mesh(
			new THREE.SphereGeometry(radius, segments, segments), 
			new THREE.MeshBasicMaterial({
				map:  THREE.ImageUtils.loadTexture(rootDir + 'images/galaxy_starfield.png'), 
				side: THREE.BackSide
			})
		);
	}

	function LatLngToXYZ(lat, lon, eradius, heigth) {
      var phi = (lat)*Math.PI/180;
      var theta = (lon-180)*Math.PI/180;

      var x = -(eradius+heigth) * Math.cos(phi) * Math.cos(theta);
      var y = (eradius+heigth) * Math.sin(phi);
      var z = (eradius+heigth) * Math.cos(phi) * Math.sin(theta);

      return new THREE.Vector3(x,y,z);
    }

    function createSphereArc(P,Q)
	{
		var sphereArc = new THREE.Curve();
		sphereArc.getPoint = greatCircleFunction(P,Q);
		return sphereArc;
	}

	function greatCircleFunction(P, Q)
	{
		var angle = P.angleTo(Q);

		return function(t)
		{
		    var X = new THREE.Vector3().addVectors(
				P.clone().multiplyScalar(Math.sin( (1 - t) * angle )), 
				Q.clone().multiplyScalar(Math.sin(      t  * angle )))
				.divideScalar( Math.sin(angle) );
		    return X;
		};
	}

	function drawCurve(curve, color, sphere)
	{
		var lineGeometry = new THREE.Geometry();
		lineGeometry.vertices = curve.getPoints(100);
		lineGeometry.computeLineDistances();
		var lineMaterial = new THREE.LineBasicMaterial();
		lineMaterial.color = color;
		var line = new THREE.Line( lineGeometry, lineMaterial );
		//scene.add(line);
		sphere.add(line);
	}

	function createCurvePath(start, end, elevation, sphere) {
        //var start3 = sphere.translateCordsToPoint(start.latitude,start.longitude);
        //var end3 = sphere.translateCordsToPoint(end.latitude, end.longitude);
        //var mid = (new LatLon(start.latitude,start.longitude)).midpointTo(new LatLon(end.latitude, end.longitude));
        //var middle3 = sphere.translateCordsToPoint(mid.lat(), mid.lon(), elevation);

        var mid = new THREE.Vector3((start.x + end.x ) / 2, (start.y + end.y) / 2, (start.z + end.z) / 2);
        //var middle = new THREE.Vector3(mid.x - elevation, mid.y + elevation, mid.z);
        var mp = new THREE.Vector3(0,0,0);
        var mpVec = new THREE.Vector3((mp.x - mid.x), (mp.y - mid.y), (mp.z - mid.z));
        
        //console.log(mpVec);

        var middle = new THREE.Vector3(mid.x - (mpVec.x * elevation), mid.y - (mpVec.y * elevation), mid.z - (mpVec.z * elevation));

        var curveQuad = new THREE.QuadraticBezierCurve3(start, middle, end);
        //var curveQuad = new THREE.QuadraticBezierCurve3(start, middle, end);

   //   var curveCubic = new THREE.CubicBezierCurve3(start3, start3_control, end3_control, end3);

        var cp = new THREE.CurvePath();
        cp.add(curveQuad);
   //   cp.add(curveCubic);
        return cp;
    }

    function lineDistance( point1, point2 )
	{
		  var xs = 0;
		  var ys = 0;
		 
		  xs = point2.x - point1.x;
		  xs = xs * xs;
		 
		  ys = point2.y - point1.y;
		  ys = ys * ys;
		 
		  return Math.sqrt( xs + ys );
	}

	function getRandomArbitrary(min, max) {
    	return Math.random() * (max - min) + min;
	}

	function onMouseMove( event )
	{
		controls.autoRotate = false; 

		setTimeout( function(){ 
		   controls.autoRotate = true; 
		  }
		 , 5000 );
	}

	function attachMarkerBillboard(min, min2, cline, o1, ot1, o2, ot2, o3, ot3)
	{
		//$trackingOverlay = $("#tracking-overlay");
		//m = inm;

		markersOverlay.push(o1);
		markersOverlayTarget.push(ot1);

		markersOverlay.push(o2);
		markersOverlayTarget.push(ot2);

		markersOverlay.push(o3);
		markersOverlayTarget.push(ot3);

		markers.push(min);
		markers.push(min2);
		lines.push(cline);

	}

	var runLine = false;

	

	/*
	function positionTrackingOverlay()
	{
		for(var i = 0; i < markers.length/2; i++)
		{
			var visibleWidth, visibleHeight, p, v, percX, percY, left, top;

			scene.updateMatrixWorld();

			var p2 = new THREE.Vector3();
			p2.setFromMatrixPosition( markers[i].matrixWorld );

			p = p2;

			v = projector.projectVector(p, camera);

			// translate our vector so that percX=0 represents
			// the left edge, percX=1 is the right edge,
			// percY=0 is the top edge, and percY=1 is the bottom edge.
			percX = (v.x + 1) / 2;
			percY = (-v.y + 1) / 2;

			// scale these values to our viewport size
			left = percX * width;
			top = percY * height;

			// position the overlay so that it's center is on top of
			// the sphere we're tracking
			markersOverlay[i]
			.css('left', (left - markersOverlay[i].width() / 2) + 'px')
			.css('top', (top - markersOverlay[i].height() / 2) + 'px');

			var camWorldP = new THREE.Vector3();
			camWorldP.setFromMatrixPosition( camera.matrixWorld );

			//need to reset p
			p = new THREE.Vector3();
			p.setFromMatrixPosition( markers[i].matrixWorld );
					
			var v1 = new THREE.Vector3(p.x, p.y, p.z);
			var v2 = new THREE.Vector3(camWorldP.x, camWorldP.y, camWorldP.z);

			if(v1.normalize().dot(v2.normalize()) > 0.25)
			{
				markersOverlay[i].show();
			}
			else
			{
				markersOverlay[i].hide();
			}
		}
	}
	*/

	function positionTrackingOverlay()
	{
		for(var i = 0; i < markersOverlay.length; i++)
		{
			var visibleWidth, visibleHeight, p, v, percX, percY, left, top;

			scene.updateMatrixWorld();

			var p2 = new THREE.Vector3();
			p2.setFromMatrixPosition( markersOverlayTarget[i].matrixWorld );

			p = p2;

			v = projector.projectVector(p, camera);

			// translate our vector so that percX=0 represents
			// the left edge, percX=1 is the right edge,
			// percY=0 is the top edge, and percY=1 is the bottom edge.
			percX = (v.x + 1) / 2;
			percY = (-v.y + 1) / 2;

			// scale these values to our viewport size
			left = percX * width;
			top = percY * height;

			// position the overlay so that it's center is on top of
			// the sphere we're tracking
			markersOverlay[i]
			.css('left', (left - markersOverlay[i].width() / 2) + 'px')
			.css('top', (top - markersOverlay[i].height() / 2) + 'px');

			var camWorldP = new THREE.Vector3();
			camWorldP.setFromMatrixPosition( camera.matrixWorld );

			//need to reset p
			p = new THREE.Vector3();
			p.setFromMatrixPosition( markersOverlayTarget[i].matrixWorld );
					
			var v1 = new THREE.Vector3(p.x, p.y, p.z);
			var v2 = new THREE.Vector3(camWorldP.x, camWorldP.y, camWorldP.z);

			if(v1.normalize().dot(v2.normalize()) > 0.25)
			{
				markersOverlay[i].show();
			}
			else
			{
				markersOverlay[i].hide();
			}
		}
	}

	function CreateNewAlert(alertTitle, alertTime, srcIP, dstIP, srcLat, srcLng, dstLat, dstLng, srcCity, dstCity, srcDetails, dstDetails)
	{
		/*$.get(
		    "https://amcneilly-wsl/IPData/GetRecord.php",
		    {paramOne : '1.1.1.1', paramX : 'ip'},
		    function(data) {
		       alert('page content: ' + data);
		    }
		);*/

		

		//add marker
		var pos = LatLngToXYZ(srcLat, srcLng, kRadius, 0.00001);
		//console.log(pos);
		var marker = createMarker(0.01, 12); 
		//marker.position.set(0.5,0,0);
		marker.position.set(pos.x, pos.y, pos.z);
		sphere.add(marker);

		var pos2 = LatLngToXYZ(dstLat, dstLng, kRadius, 0.00001);
		var marker2 = createMarker(0.01, 12); 
		//marker.position.set(0.5,0,0);
		marker2.position.set(pos2.x, pos2.y, pos2.z);
		sphere.add(marker2);
		
		//drawCurve( createSphereArc(pos, pos2), new THREE.Color(0x0000ff), sphere );

		var cp = createCurvePath(pos, pos2, 2.8, sphere);
		//var curvedLineMaterial =  new THREE.LineBasicMaterial({color: 0xFFFFAA, linewidth: 3});

		var d = lineDistance(pos, pos2);

		console.log(d);

		uniforms = {
		    time: { type: "f", value: 0 },
		    distance: {type: "f", value: d},
		    startpoint: {type: "v3", value: pos },
		    endpoint: {type: "v3", value: pos2 },
		    resolution: { type: "v2", value: new THREE.Vector2 },
		    texture: { type: "t", value: THREE.ImageUtils.loadTexture(rootDir + 'images/blue_stripes.jpg') }
		};

		var curvedLineMaterial = new THREE.ShaderMaterial({
	    	uniforms: uniforms,
	    	linewidth: 5,
	    	vertexShader: document.getElementById('vertexshader_networkline').innerHTML,
	    	fragmentShader: document.getElementById('fragmentshader_networkline').innerHTML
		});

		curvedLineMaterial.transparent = true

		var curvedLine = new THREE.Line(cp.createPointsGeometry(20), curvedLineMaterial);
		sphere.add(curvedLine);

		var container = $('#tracking-overlays');	
		var template = $('#tracking-overlay-template');

		var o1 = template.clone();
		o1.find("#title").text(srcIP + ", " + srcCity + ", " + srcDetails);
		container.append( o1 );
		o1.css( "color", "white" );

		var o2 = template.clone();
		o2.find("#title").text(dstIP + ", " + dstCity + ", " + dstDetails);
		container.append( o2 );
		o2.css( "color", "white" );

		var o3 = template.clone();
		o3.find("#title").text(alertTitle);
		container.append( o3 );
		o3.css( "color", "white" );

		console.log(cp.createPointsGeometry(20).vertices[0]);
		console.log(cp.createPointsGeometry(20).vertices[10]);

		attachMarkerBillboard(marker, marker2, curvedLine, o1, marker, o2, marker2, o3, curvedLine);
	}

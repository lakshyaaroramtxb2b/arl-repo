var clocks = [];

function DigitalClock() {
  this.utcTimezone;
  this.digits;
  this.weekdays;
  this.twentyFourHourSite;
  this.clockOnline;
  this.obj;
  this.sTime;
  this.eTime;
  this.alarm;
  this.ampm;
  this.tgtSpan;
  this.tgtDots;
}

// Map digits to their names (this will be an array)
var digit_to_name = 'zero one two three four five six seven eight nine'.split(' ');

// Positions for the hours, minutes, and seconds
var clock_positions = [
'h1', 'h2', ':', 'm1', 'm2', ':', 's1', 's2'
];

var now_utc_tick;

//start tick
ClockTick();
setInterval(function(){ClockTick()},1000);


function init_clock(obj, utcTimezone, twentyFourHourSite, sTime, eTime)
{
 
// Cache some selectors
 
//var clock = $('clock'),
var clock = obj,
alarm = clock.find('.alarm'),
ampm = clock.find('.ampm');

var clockOnline = true;

//console.log(obj);

// Map digits to their names (this will be an array)
//var digit_to_name = 'zero one two three four five six seven eight nine'.split(' ');
 
// This object will hold the digit elements
var digits = {};
 
// Positions for the hours, minutes, and seconds
//var positions = [
//'h1', 'h2', ':', 'm1', 'm2', ':', 's1', 's2'
//];
 
// Generate the digits with the needed markup,
// and add them to the clock
 
var digit_holder = clock.find('.digits');

//console.log(digit_holder);
 
$.each(clock_positions, function(){
 
if(this == ':'){
digit_holder.append('<div class="dots">');
}
else{
 
var pos = $('<div>');
 
for(var i=1; i<8; i++){
pos.append('<span class="d' + i + '">');
}
 
// Set the digits as key:value pairs in the digits object
digits[this] = pos;
 
// Add the digit elements to the page
digit_holder.append(pos);
}
 
});
 
// Add the weekday names
 
var weekday_names = 'MON TUE WED THU FRI SAT SUN'.split(' '),
weekday_holder = clock.find('.weekdays');
 
$.each(weekday_names, function(){
weekday_holder.append('<span>' + this + '</span>');
});
 
var weekdays = clock.find('.weekdays span');

//if(twentyFourHourSite)
//{
//	SetClockTextOnline(obj);
//}

//default state

var newClock = new DigitalClock();
newClock.utcTimezone = utcTimezone;
newClock.digits = digits;
newClock.weekdays = weekdays;
newClock.twentyFourHourSite = twentyFourHourSite;
newClock.clockOnline = clockOnline;
newClock.obj = obj;
newClock.sTime = sTime;
newClock.eTime = eTime;
newClock.alarm = alarm;
newClock.ampm = ampm;
newClock.tgtSpan = obj.find( "span" );
newClock.tgtDots = obj.find( ".dots" );

clocks.push(newClock);

SetClockTextOnline(obj, newClock.tgtSpan ,newClock.tgtDots);
clockOnline = true;
 
}

function ClockTick()
{
	now_utc_tick =  moment().utc();

	update_time();
}

function update_time(){
	 
	// Use moment.js to output the current time as a string
	// hh is for the hours in 12-hour format,
	// mm - minutes, ss-seconds (all with leading zeroes),
	// d is for day of week and A is for AM/PM

	for(var i = 0; i < clocks.length; i++)
	{
		//moved form indivudal tick to global
		//var now_utc =  moment().utc();
		var now = now_utc_tick.clone().tz(clocks[i].utcTimezone).format("HHmmss");

		//console.log("now: " + now);

		clocks[i].digits.h1.attr('class', digit_to_name[now[0]]);
		clocks[i].digits.h2.attr('class', digit_to_name[now[1]]);
		clocks[i].digits.m1.attr('class', digit_to_name[now[2]]);
		clocks[i].digits.m2.attr('class', digit_to_name[now[3]]);
		clocks[i].digits.s1.attr('class', digit_to_name[now[4]]);
		clocks[i].digits.s2.attr('class', digit_to_name[now[5]]);
		 
		//console.log("sTime " + sTime);

		//Toggle colour of clocks to indicate if site is online / offline
		 if(!clocks[i].twentyFourHourSite)
		 {
		 	//follow ssun
		 	var cUTC =  moment().utc().tz(clocks[i].utcTimezone);

			var a = moment(cUTC.format("YYYY-MM-DD") + " " + clocks[i].sTime + " " + cUTC.format("ZZ"), "YYYY-MM-DD HH:mm ZZ").tz(clocks[i].utcTimezone);
			var b = moment(cUTC.format("YYYY-MM-DD") + " " + clocks[i].eTime + " " + cUTC.format("ZZ"), "YYYY-MM-DD HH:mm ZZ").tz(clocks[i].utcTimezone);

			//console.log(obj);

			if(cUTC >= a && cUTC <= b)
			{
				//obj.removeClass("offlineRed");
				//obj.removeClass(".clock .digits div");
				if(!clocks[i].clockOnline)
				{
					SetClockTextOnline(clocks[i].obj, clocks[i].tgtSpan, clocks[i].tgtDots);
					clocks[i].clockOnline = true;
				}
				//console.log("online " + utcTimezone);
			}
			else
			{
				if(clocks[i].clockOnline)
				{
					SetClockTextOffline(clocks[i].obj, clocks[i].tgtSpan, clocks[i].tgtDots);
					clocks[i].clockOnline = false;
				}
				//console.log("offline " + utcTimezone);
			}
		 }

		// The library returns Sunday as the first day of the week.
		// Stupid, I know. Lets shift all the days one position down,
		// and make Sunday last
		 
		var dow = now[6];
		dow--;
		 
		// Sunday!
		if(dow < 0){
		// Make it last
		dow = 6;
		}
		 
		// Mark the active day of the week
		clocks[i].weekdays.removeClass('active').eq(dow).addClass('active');
		 
		// Set the am/pm text:
		clocks[i].ampm.text(now[7]+now[8]);
	}
}

function SetClockTextOffline(obj, tgtSpan, tgtDots)
{
	//var tgt = obj.find( "span" );
	tgtSpan.css( "background-color", "red" );
	tgtSpan.css( "border-color", "red" );
	tgtSpan.css( "box-shadow", "0px 0px 8px #F0BDBD" );

	//tgt = obj.find( ".dots" );
	tgtDots.removeClass("greenDots");
	tgtDots.addClass("redDots");
}

function SetClockTextOnline(obj, tgtSpan, tgtDots)
{
	//var tgt = obj.find( "span" );
	tgtSpan.css( "background-color", "#66FF33" );
	tgtSpan.css( "border-color", "#66FF33" );
	tgtSpan.css( "box-shadow", "0px 0px 8px #CCFF66" );

	//tgt = obj.find( ".dots" );
	tgtDots.removeClass("redDots");
	tgtDots.addClass("greenDots");
}
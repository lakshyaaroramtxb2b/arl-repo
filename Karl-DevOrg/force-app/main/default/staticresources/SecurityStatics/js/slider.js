var slideIntervalId;
var SLIDER_INTERVAL = 14000;
var SLIDER_WIDTH = 938;
var isSliding = false;
var EASELONG = 1000;
var slidesNumber;
var leftval;
var startx;

function doSlide(slideNumber,duration,continueSliding,doTextSlide,backease,textease){
	if(typeof doTextSlide == 'undefined')
		doTextSlide = true
	if(!slider.is(':animated')){
		//
		var loopEnd = false;
		//
		var leftPos = 0;
		//
		var acutalSlideNumber = jQuery.inArray($("#EasingIndex ul li.active")[0],$("#EasingIndex ul li"));
		// set active next slide index
		$("#EasingIndex ul li").removeClass('active');
		$($("#EasingIndex ul li")[slideNumber]).addClass('active');
		
		if(typeof backease == 'undefined')
			backease = "easeInExpo";
			
		if(typeof textease == 'undefined')
			textease = "linear";
		
		if( slideNumber== 0 && acutalSlideNumber == slidesNumber-1 )
			loopEnd = true;
		leftPos = (loopEnd)?-(slidesNumber*SLIDER_WIDTH):-(slideNumber*SLIDER_WIDTH);
		
		if(doTextSlide){
			$(sliderTextContents[acutalSlideNumber]).animate(
				{"left": -SLIDER_WIDTH}, 
				{
					"duration": duration,
					"easing": textease,
					"complete":function(){
						//
					}
				}
			);
		}
		
		slider.animate(
			{"left": leftPos}, 
			{
				"duration": duration,
				"easing": backease,
				"complete":function(){
					$(sliderTextContents[acutalSlideNumber]).css('left' ,20);
					isSliding = false;
					if(loopEnd)
						slider.css('left' ,0);
						
					if(continueSliding){
						if(slideNumber < slidesNumber-1){
							slideIntervalId = setTimeout("doSlide("+(slideNumber+1)+",EASELONG,true);", SLIDER_INTERVAL);
						}else{
							slideIntervalId = setTimeout("doSlide(0,EASELONG,true);", SLIDER_INTERVAL);
						}
					}

				}
			}
		);
	}
}

$(document).ready( function(){	

	$('#EasingSlider .slideItem .text a').bind(clickEvent,function(){
        window.location(this.href);
	})
	// Add Draggable Helper
	$('#EasingSlider .slideItem').each(function(){
		$(this).append('<div class="helper">&nbsp;</div>');
		//Init Touch events if is in Ipad
		if(isIpad){
		   initTouchEvents($(this).children('.helper'),false);
		}
	});
	// Background Part of Slider
	slider = $('#EasingSlider');
	// Text Part pf Slider
	sliderTextContents = $('#EasingSlider .slideItem .text');
	// Start Sliding
	slideIntervalId = setTimeout("doSlide(1,EASELONG,true)", SLIDER_INTERVAL);
	// Number of Slides
	slidesNumber = $('.slideItem').size()-1;
	
	//touchstart, touchmove, touchend
	$('#EasingSlider .slideItem .helper').bind("drag dragstart dragend touchstart touchmove touchendpor",function(o,e){
		var slide = jQuery.inArray($("#EasingIndex ul li.active")[0],$("#EasingIndex ul li"));
		var offsetTrigger = $('.slideItem').width() /4;
		var slideTo;
				
		switch(o.type){
			//case 'touchstart':
			case 'dragstart':
				leftval = slider.position().left;
				//startx = e.startX;
			//case 'touchmove':
			case 'drag':
				if(slideIntervalId != null)
					clearInterval(slideIntervalId); 
				if( !(slide == 0 &&  e.deltaX > 0) )
					slider.css('left', leftval + e.deltaX);
				break;
			//case 'touchend':	
			case 'dragend':
				if( !(slide == 0 &&  e.deltaX > 0) ){
					if(Math.abs(e.deltaX) > offsetTrigger){
						
						if(e.deltaX>0){
							slideTo = slide-1;
						}else{
							if(slide < slidesNumber-1){
								slideTo = slide+1;
							}else{
								slideTo = 0;
							 }
						}
						doSlide(slideTo,EASELONG/2,false,false,'linear','linear');
					}else{
						doSlide(slide,EASELONG/2,false,false,'linear','linear');
					}
				}
				break;
		}
	});
	
	
	$("#EasingIndex ul li:not(.active)").live(clickEvent,function(){
		if(!isSliding){
			isSliding = true;
			clearInterval(slideIntervalId);
			var slideNumber = jQuery.inArray(this,$("#EasingIndex ul li"));
			doSlide(slideNumber,EASELONG);
		}
	});

});

draw2d.XMLSerializer_01=function(){};
draw2d.XMLSerializer_01.prototype.type="XMLSerializer_01";
draw2d.XMLSerializer_01.prototype.toXML=function(_573e){
  var serializeTags = {'Question':true,'Variable':true,'Url':true,'Decision':true,'Start':true};
  var xml="<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n";
  xml=xml+"<form>\n";
  var _5740=_573e.getFigures();
  for(var i=0;i<_5740.getSize();i++){
    var _5742=_5740.get(i);
    if ((serializeTags[_5742.type] == false) || (serializeTags[_5742.type] == undefined)) {
      continue;
    }
    // We seem to have ghost nodes for some reason
    if ((_5742.type != 'Start') && ((_5742.getProperty('name') == undefined) || 
        (_5742.getProperty('name') == null) || 
        (_5742.getProperty('name') == ''))) {
        continue;
    }
    xml=xml+"<"+_5742.type+" x=\""+_5742.getX()+"\" y=\""+_5742.getY()+"\" id=\""+_5742.getId()+"\">\n";
    xml=xml+this.getPropertyXML(_5742,"\n   ");
    xml=xml+"</"+_5742.type+">\n";
  }
  xml=xml+"</form>\n";
  return xml;
};
count = 0
draw2d.XMLSerializer_01.prototype.getPropertyXML=function(obj,spacing){
  var xml="";
  var value='';
  var sf = obj.serializeFields;
  if ((sf == null) || (sf == undefined)) {
    // not a supported object
    return '';
  }

  for(var i=0;i<sf.length;i++){
    var key = sf[i];
    value=obj.getProperty(key);
    
    if ((value == '""') || (value==null) || (value == undefined)) {
      value = ''; // stray empty string
    }
    
    xml=xml+spacing+"<"+key+" value="+Ext.encode(Ext.util.Format.htmlEncode(value))+" />\n";
  }
  var mycs = obj.myconnections;
  for(var i=0;i<mycs.length;i++){
    var mcon = mycs[i];
    var port = obj.getPort(mcon);
    if (port == undefined) {continue;}
    var conns = port.getConnections(); //strange draw2d.ArrayList object
    var targId = '';
    if (conns.getSize() == 0){
      //it'll be a blank target
    } else {
      var targId = conns.get(0).getTarget().getParent().getId(); // only one egress connection supported
    }
    xml=xml+spacing+"<"+mcon+" value=\""+targId+"\" />\n";
  }
  return xml;
};
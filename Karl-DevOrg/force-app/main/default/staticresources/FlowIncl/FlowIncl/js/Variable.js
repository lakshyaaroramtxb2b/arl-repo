draw2d.Variable=function(){
  draw2d.ImageFigure.call(this,IMAGE_BASE+this.type+".png");
  this.inputPort=null;
  this.setDimension(50,50);
  this.label=new draw2d.Label(' '); // the below setProperty('name'.. will set it
  this.label.setBackgroundColor(new draw2d.Color(230,230,250));
  this.label.setBorder(new draw2d.LineBorder(1));
  this.updateLabel();
  
  this.setProperty('setTrue','');
  this.setProperty('setFalse','');
  this.setProperty('appendTo','');
  this.setProperty('appendVal','');
  this.setProperty('name','');
};

draw2d.Variable.prototype=new draw2d.ImageFigure;
draw2d.Variable.prototype.type="Variable";
draw2d.Variable.prototype.serializeFields=['name','setTrue','setFalse','appendTo','appendVal'];
draw2d.Variable.prototype.myconnections=['next'];
draw2d.Variable.prototype.setWorkflow=function(_4db7){
  draw2d.ImageFigure.prototype.setWorkflow.call(this,_4db7);
  if(_4db7!=null&&this.inputPort==null){
    _4db7.addFigure(this.label,this.x-20,this.y-10);
    
    this.inputPort=new draw2d.FlowInputPort();
    this.inputPort.setWorkflow(_4db7);
    this.inputPort.setBackgroundColor(new draw2d.Color(115,115,245));
    this.inputPort.setColor(null);
    this.inputPort.setName('input');
    this.addPort(this.inputPort,0,this.height/2);
    
    this.outputPort=new draw2d.ArrowOutputPort();
    this.outputPort.setMaxFanOut(5);
    this.outputPort.setWorkflow(_4db7);
    this.outputPort.setBackgroundColor(new draw2d.Color(245,115,115));
    this.outputPort.setName("next");
    this.addPort(this.outputPort,this.width,this.height/2);
  }
};

draw2d.Variable.prototype.updateLabel=function(){
  var myname = this.getProperty('name');
  this.label.setText(this.getProperty('name'));
  var xpos=this.getX()+(this.getWidth()/2)-(this.label.getWidth()/2);
  this.label.setPosition(xpos,this.y-this.label.getHeight()-3);
};
draw2d.Variable.prototype.onDrag=function(){
  draw2d.ImageFigure.prototype.onDrag.call(this);
  this.updateLabel();
};
draw2d.Variable.prototype.setProperty=function(key,val){
  draw2d.ImageFigure.prototype.setProperty.call(this,key,val);
  if (key=='name') { this.updateLabel();}
}

// dispose() isn't being called so we're assuming that asking
// if we're deleteable shows intent to delete
draw2d.Variable.prototype.isDeleteable=function(){
  this.workflow.removeFigure(this.label);
  return draw2d.ImageFigure.prototype.isDeleteable(this);
};
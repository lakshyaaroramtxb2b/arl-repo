draw2d.FormShower=function(obj){
  this.workflow=obj;
};

draw2d.FormShower.prototype.type="FormShower";
draw2d.FormShower.prototype.onSelectionChanged=function(obj){
  if (obj == null) {
    return;
  }
  var form = Ext.get('edit-form');
  form.dom.innerHTML = '';
  if (obj.type == 'Question') {
      bindQuestionForm(obj,form);
  } else if (obj.type == 'Variable') {
      bindVariableForm(obj,form);
  } else if (obj.type == 'Url') {
      bindUrlForm(obj,form);
  } else if (obj.type == 'Decision') {
      bindDecisionForm(obj,form);
  } else {
      // we don't care about this object
  }
};
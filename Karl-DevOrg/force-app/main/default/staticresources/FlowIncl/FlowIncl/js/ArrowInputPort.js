draw2d.ArrowInputPort=function(_4d69){
  draw2d.InputPort.call(this,_4d69);
};

draw2d.ArrowInputPort.prototype=new draw2d.InputPort;
draw2d.ArrowInputPort.prototype.type="ArrowInputPort";
draw2d.ArrowInputPort.prototype.onDrop=function(port){
  if(port.getMaxFanOut&&port.getMaxFanOut()<=port.getFanOut()){
    return;
  }
  if(this.parentNode.id==port.parentNode.id){
  }
  else{
    var _4d6b=new draw2d.CommandConnect(this.parentNode.workflow,port,this);
    _4d6b.setConnection(new draw2d.ArrowDecoratedConnection());
    this.parentNode.workflow.getCommandStack().execute(_4d6b);
  }
};
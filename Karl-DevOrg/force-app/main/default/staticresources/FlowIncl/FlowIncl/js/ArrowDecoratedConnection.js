draw2d.ArrowDecoratedConnection=function(){
  draw2d.Connection.call(this);
  this.sourcePort=null;
  this.targetPort=null;
  this.lineSegments=new Array();
  this.setColor(new draw2d.Color(0,0,115));
  this.setLineWidth(2);
  this.setTargetDecorator(new draw2d.ArrowConnectionDecorator());
  //this.setRouter(new draw2d.FanConnectionRouter());
};

draw2d.ArrowDecoratedConnection.prototype=new draw2d.Connection();
draw2d.ArrowDecoratedConnection.prototype.type="ArrowDecoratedConnection";
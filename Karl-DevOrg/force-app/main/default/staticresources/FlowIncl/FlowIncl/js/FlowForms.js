function bindDecisionForm(sourceObj,renderTo) {
    f = new Ext.form.FormPanel({
          id:'edit_form',
          border:false,
          renderTo:renderTo,
          labelWidth:50,
          items:[{
            xtype:'textfield',
            id:'f_name',
            fieldLabel:'Name',
            name:'name',
            width:235,
            value:sourceObj.getProperty('name')
           },{
            xtype:'combo',
            id:'f_andOr',
            fieldLabel:'Rule AND/OR',
            triggerAction:'all',
            mode:'local',
            editable:false,
            store:new Ext.data.SimpleStore({
              fields:['name','value'],
              data:[['AND','AND'],['OR','OR']],
             }),
            displayField:'name',
            valueField:'value',
            hiddenName:'andOr',
            value:sourceObj.getProperty('andOr')
          },{
            xtype:'textarea',
            id:'f_rules',
            name:'rules',
            fieldLabel:'Rules',
            width:235,
            value:sourceObj.getProperty('rules')
          }],
         buttons:[{
            text:'Save',
            scope:this,
            handler:function () {
              var form = Ext.getCmp('edit_form').getForm();
              sourceObj.setProperty('name',form.findField('f_name').getValue());
              sourceObj.setProperty('andOr',form.findField('f_andOr').getValue());
              sourceObj.setProperty('rules',form.findField('f_rules').getValue());
            }
         }]
       });
}

function bindQuestionForm(sourceObj,renderTo) {
    f = new Ext.form.FormPanel({
          id:'edit_form',
          border:false,
          renderTo:renderTo,
          labelWidth:50,
          items:[{
            xtype:'textfield',
            id:'f_name',
            fieldLabel:'Name',
            name:'name',
            width:235,
            value:sourceObj.getProperty('name')
          },{
            xtype:'checkbox',
            id:'f_isYesNo',
            name:'isYesNo',
            fieldLabel:'Yes/No Question',
            value:sourceObj.getProperty('isYesNo'),
            checked:sourceObj.getProperty('isYesNo'),
          },{
            xtype:'textarea',
            id:'f_text',
            name:'text',
            width:235,
            fieldLabel:'Question',
            value:sourceObj.getProperty('text')
          }],
         buttons:[{
            text:'Save',
            scope:this,
            handler:function () {
              var form = Ext.getCmp('edit_form').getForm();
              sourceObj.setProperty('name',form.findField('f_name').getValue());
              sourceObj.setProperty('isYesNo',form.findField('f_isYesNo').getValue());
              sourceObj.setProperty('text',form.findField('f_text').getValue());
            }
         }]
       });
}

function bindVariableForm(sourceObj,renderTo) {
    f = new Ext.form.FormPanel({
          id:'edit_form',
          border:false,
          renderTo:renderTo,
          labelWidth:50,
          items:[{
            xtype:'textfield',
            id:'f_name',
            fieldLabel:'Name',
            name:'name',
            width:235,
            value:sourceObj.getProperty('name')
          },{
            xtype:'textarea',
            id:'f_setTrue',
            name:'setTrue',
            fieldLabel:'Set True',
            width:235,
            value:sourceObj.getProperty('setTrue')
          },{
            xtype:'textarea',
            id:'f_setFalse',
            name:'setFalse',
            fieldLabel:'Set False',
            width:235,
            value:sourceObj.getProperty('setFalse')
          },{
            xtype:'textarea',
            id:'f_appendTo',
            name:'appendTo',
            fieldLabel:'Append To',
            width:235,
            value:sourceObj.getProperty('appendTo')
          },{
            xtype:'textfield',
            id:'f_appendVal',
            fieldLabel:'Append Value',
            name:'appendVal',
            width:235,
            value:sourceObj.getProperty('appendVal')
         }],
         buttons:[{
            text:'Save',
            scope:this,
            handler:function () {
              var form = Ext.getCmp('edit_form').getForm();
              sourceObj.setProperty('name',form.findField('f_name').getValue());
              sourceObj.setProperty('setTrue',form.findField('f_setTrue').getValue());
              sourceObj.setProperty('setFalse',form.findField('f_setFalse').getValue());
              sourceObj.setProperty('appendTo',form.findField('f_appendTo').getValue());
              sourceObj.setProperty('appendVal',form.findField('f_appendVal').getValue());
            }
         }]
       });
}

function bindUrlForm(sourceObj,renderTo) {
    f = new Ext.form.FormPanel({
          id:'edit_form',
          border:false,
          renderTo:renderTo,
          labelWidth:50,
          items:[{
            xtype:'textfield',
            id:'f_name',
            fieldLabel:'Name',
            name:'name',
            width:235,
            value:sourceObj.getProperty('name')
          },{
            xtype:'textarea',
            id:'f_urlval',
            name:'urlval',
            fieldLabel:'URL',
            width:235,
            value:sourceObj.getProperty('urlval')
          }],
         buttons:[{
            text:'Save',
            scope:this,
            handler:function () {
              var form = Ext.getCmp('edit_form').getForm();
              sourceObj.setProperty('name',form.findField('f_name').getValue());
              sourceObj.setProperty('urlval',form.findField('f_urlval').getValue());
            }
         }]
       });
}          
draw2d.Start=function(){
  draw2d.ImageFigure.call(this,IMAGE_BASE+this.type+".png");
  this.inputPort=null;
  this.setDimension(50,50);
};


draw2d.Start.prototype=new draw2d.ImageFigure;
draw2d.Start.prototype.type="Start";
draw2d.Start.prototype.myconnections=['node'];
draw2d.Start.prototype.serializeFields=[];

draw2d.Start.prototype.setWorkflow=function(_4db7){
  draw2d.ImageFigure.prototype.setWorkflow.call(this,_4db7);
  if(_4db7!=null&&this.inputPort==null){
    this.outputPort=new draw2d.ArrowOutputPort();
    this.outputPort.setMaxFanOut(5);
    this.outputPort.setWorkflow(_4db7);
    this.outputPort.setBackgroundColor(new draw2d.Color(245,115,115));
    this.outputPort.setName("node");
    this.addPort(this.outputPort,this.width,this.height/2);
  }
};
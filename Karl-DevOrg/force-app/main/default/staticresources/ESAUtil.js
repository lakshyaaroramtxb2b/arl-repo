window.$ESAUtil = window.$ESAUtil || {};

window.$ESAUtil.copyToClipBoard = function(text) {
    // Create temp textarea element and remove after copy
    var htmlElem = document.createElement('textarea');
    htmlElem.setAttribute('readonly', '');
    htmlElem.innerHTML = text;
    htmlElem.innerHTML = htmlElem.innerText;
    htmlElem.style = {left: '-9999px',position: 'absolute'};
    document.body.appendChild(htmlElem);
    htmlElem.select();
    document.execCommand('copy');
    document.body.removeChild(htmlElem);
 };

 window.$ESAUtil.stopEvent = function (evt) {
    if (evt && evt.preventDefault) {
      evt.preventDefault();
    }
    if (evt && evt.stopPropagation) {
      evt.stopPropagation();
    }
}
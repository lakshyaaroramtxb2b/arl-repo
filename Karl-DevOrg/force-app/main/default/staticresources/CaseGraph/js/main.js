
var stigmaGraph = null;
var ipCache = {};
var selNode = -1;
var selNodeAndChildKeys = [];
var selNodeRelatedEdges = [];

var orgBaseUri = 'https://security.my.salesforce.com/';

$( document ).ready(function() {
  $('#panelHideBtn').click(function()
    {
      removeNodeHighlights();
      $('#graph-panel').hide();
    });

});

//Start code for custom images
sigma.utils.pkg('sigma.canvas.nodes');

//custom node renderer
sigma.canvas.nodes.image = (function() {
  var _cache = {},
      _loading = {},
      _callbacks = {};

  // Return the renderer itself:
  var renderer = function(node, context, settings) {
    var args = arguments,
        prefix = settings('prefix') || '',
        size = node[prefix + 'size'],
        color = node.color || settings('defaultNodeColor'),
        url = node.url;

    if (_cache[url]) {
      context.save();

      // Draw the clipping disc:
      context.beginPath();
      context.arc(
        node[prefix + 'x'],
        node[prefix + 'y'],
        node[prefix + 'size'] + 50,
        0,
        Math.PI * 2,
        true
      );
      context.closePath();
      context.clip();

      // Draw the image
      context.drawImage(
        _cache[url],
        node[prefix + 'x'] - size,
        node[prefix + 'y'] - size,
        2 * size,
        2 * size
      );

      //console.log('about to render a node');
      //console.dir(node);


      // Quit the "clipping mode":
      context.restore();

      //highlight if selected
      if(selNodeAndChildKeys.indexOf(node.key) >= 0)
      {
        // Draw the border:
        context.beginPath();
        context.arc(
          node[prefix + 'x'],
          node[prefix + 'y'],
          node[prefix + 'size'] * 1.3,
          0,
          Math.PI * 2,
          true
        );

        context.lineWidth = size/1.6;

        if(selNode == node.key)
        {
          context.strokeStyle = '#00CC00';
          context.size *= 1.2;
        }
        else  
          context.strokeStyle = '#99FF66';
        
        context.stroke();
      }

    } else {
      sigma.canvas.nodes.image.cache(url);
      sigma.canvas.nodes.def.apply(
        sigma.canvas.nodes,
        args
      );
    }
  };

  //custom edge renderer
  sigma.utils.pkg('sigma.canvas.edges');
  sigma.canvas.edges.t = function(edge, source, target, context, settings) {
    var color = edge.color,
        prefix = settings('prefix') || '',
        edgeColor = settings('edgeColor'),
        defaultNodeColor = settings('defaultNodeColor'),
        defaultEdgeColor = settings('defaultEdgeColor');

    if (!color)
      switch (edgeColor) {
        case 'source':
          color = source.color || defaultNodeColor;
          break;
        case 'target':
          color = target.color || defaultNodeColor;
          break;
        default:
          color = defaultEdgeColor;
          break;
      }

    

    if(selNodeRelatedEdges.indexOf(edge.id) >= 0)
    {
      context.strokeStyle = '#99FF66';
      context.lineWidth = edge[prefix + 'size'] + 2;
    }
    else
    {
      context.strokeStyle = color;
      context.lineWidth = edge[prefix + 'size'] || 1;
    }

    context.beginPath();
    context.moveTo(
      source[prefix + 'x'],
      source[prefix + 'y']
    );
    context.lineTo(
      target[prefix + 'x'],
      target[prefix + 'y']
    );
    context.stroke();
  };

  // Let's add a public method to cache images, to make it possible to
  // preload images before the initial rendering:
  renderer.cache = function(url, callback) {
    if (callback)
      _callbacks[url] = callback;

    if (_loading[url])
      return;

    var img = new Image();

    img.onload = function() {
      _loading[url] = false;
      _cache[url] = img;

      if (_callbacks[url]) {
        _callbacks[url].call(this, img);
        delete _callbacks[url];
      }
    };

    _loading[url] = true;
    img.src = url;
  };

  return renderer;
})();

//end code for custom images

function loadJSGraph(json, caseNumber)
{
  json = JSON.parse(json.replace(/&quot;/g,'"'));

  console.log('loadJSGraph()');
  console.dir(json);

  if(stigmaGraph != null)
  {
    //this gets rid of all the ndoes and edges
    stigmaGraph.graph.clear();
    //this gets rid of any methods you've attached to s.
    stigmaGraph.graph.kill();

    //remove vurren thihglights
    selNode = -1;
    selNodeAndChildKeys = [];
    selNodeRelatedEdges = [];

    $('#graph-container').html('');
    $('#graph-panel').hide();
  }

  $('#caseIdTxt').text('CSIRT: ' + caseNumber);

  //sfdc.json
  var s = new sigma({
    graph: json,
    container: 'graph-container',
    renderer: {
      container: document.getElementById('graph-container'),
      type: 'canvas'
    },
    settings: {
      drawEdges: true,
      sideMargin: 0.1,
      edgeLabelSize: 'proportional',
      enableEdgeHovering: true,
      labelThreshold: 1,
      minNodeSize: 12,
      maxNodeSize: 16
    }
  });

      $('#loadingSpinner').hide();
      console.log("loading graph");

      stigmaGraph = s;

      stigmaGraph.graph.nodes()[1].label = '[SEED EVENT] - ' + stigmaGraph.graph.nodes()[1].label;

      s.startForceAtlas2(
        {
          worker: true, 
          barnesHutOptimize: false, 
          strongGravityMode: true,
          startingIterations: 10000,
          adjustSizes: true
        });

      //force layoutt can get stuck so lets give it 30 seconds then stop it.
      //it also needs to stop to suppoort dragg events
      setInterval(
        function ()
        {
          s.stopForceAtlas2();
        }, 
        1*1000
      );

      //click events
      s.bind('clickNode', function(e) {
        highlightNode(e.data.node.key, e.data.node.relatedNodeKeys);
        loadPointDetails(e.data.node);
      });
      s.bind('clickEdge', function(e) {
        //console.dir(e);
        loadPointDetails(e.data.node);
      });

      // // Initialize the dragNodes plugin:
      // var dragListener = sigma.plugins.dragNodes(s, s.renderers[0]);

      // dragListener.bind('startdrag', function(event) {
      //   console.log(event);
      // });
      // dragListener.bind('drag', function(event) {
      //   console.log(event);
      // });
      // dragListener.bind('drop', function(event) {
      //   console.log(event);
      // });
      // dragListener.bind('dragend', function(event) {
      //   console.log(event);
      // });
      //});
}

function highlightNode(nodeKey, relatedNodeKeys)
{
  removeNodeHighlights();

  selNode = nodeKey;
  selNodeAndChildKeys.push(nodeKey);
  selNodeAndChildKeys = selNodeAndChildKeys.concat(relatedNodeKeys);

  selNodeRelatedEdges = buildEdgeList(selNode);

  stigmaGraph.refresh();
}

function buildEdgeList(nKey)
{
  var ret = [];

  var n = getNode(nKey);

  for(var i = 0; i < n.relatedEdgeKeys.length; i++)
  {
    if(ret.indexOf(n.relatedEdgeKeys[i]) < 0)
      ret.push(n.relatedEdgeKeys[i]);
  }

  console.dir(ret);

  return ret;
}

function getNode(key)
{
  var nodes = stigmaGraph.graph.nodes();

  for(var i = 0; i < nodes.length; i++)
  {
    if( key == nodes[i].key)
      return nodes[i];
  }

  return null;
}

function removeNodeHighlights()
{
  selNodeAndChildKeys = [];
  selNodeRelatedEdges = [];
  selNode = -1;

  stigmaGraph.refresh();
}

function loadPointDetails(c)
{
  //get body
  var htmlBodyContent = '';
  var cYype = 'Unknown';

  if(c.nodeType == 'EVENT')
  {
    cType = 'Event';

    htmlBodyContent += '<div>';

    htmlBodyContent += '<p><b>Alert Name: </b><span>' + c.alertName + '</span></p>';
    htmlBodyContent += '<p><b>Alert Subject: </b><span>' + c.alertSubject + '</span></p>';
    htmlBodyContent += '</div>';

    var htmRelatedContent = '<hr/>';
    htmRelatedContent += '<p><b>Event Instances: </b><span>' + c.relatedEvents + '</span></p>';
    htmRelatedContent += '<ul>';

    for(var i = 0; i < c.caseRefs.length; i++)
    {
      htmRelatedContent += '<li><a href="' + orgBaseUri + c.caseRefs[i].CaseId + '" target="_blank">' + c.caseRefs[i].CaseNumber + '</a><br/> ' + c.caseRefs[i].Status + '</li>';
    }
    htmRelatedContent += '</ul>';

    $('#detailsTitle').html('<b style="font-size: 1.2em;">' + cType + '</b><br/><br/>');
    $('#detailsbody').html(htmlBodyContent);
    $('#detailsbodyRelatedEvents').html(htmRelatedContent);
  }
  else
  {
    cType = 'IP';

    $('#detailsTitle').html('<b style="font-size: 1.2em;">' + cType + '</b><br/><br/>');
    
    var htmRelatedContent = '<hr/>';
    htmRelatedContent += '<p><b>Instances: </b><span>' + c.caseRefs.length + '</span></p>';

    htmRelatedContent += '<ul>';
    for(var i = 0; i < c.caseRefs.length; i++)
    {
      htmRelatedContent += '<li><a href="' + orgBaseUri + c.caseRefs[i].CaseId + '" target="_blank">' + c.caseRefs[i].CaseNumber + '</a><br/> ' + c.caseRefs[i].Status + '</li>';
    }
    htmRelatedContent += '</ul>';

    $('#detailsbodyRelatedEvents').html(htmRelatedContent);


    if(c.hasNullKey == true)
    {
      //some cases have null value for IP address which we use for a key
      $('#detailsbody').html('<p><b></b><span>IP is Null in case</span></p>');
    }
    else
    {
      

      if(isPrivateIP(c.key))
      {
        //private ip address
        htmlBodyContent = '<div>';
        htmlBodyContent += '<p><b>Internal IP: </b><span>' + c.key+ '</span></p>';
        htmlBodyContent += '<a href=https://hunter-sfm.internal.salesforce.com/cgi-bin/host_lookup.py?target=' + c.key + ' target="_blank">lookup sfdc workstation</a>'
        htmlBodyContent += '</div>';

        $('#detailsTitle').html('<b style="font-size: 1.2em;">' + cType + '</b><br/><br/>');
        $('#detailsbody').html(htmlBodyContent);
      }
      else
      {
        //public ip address
        $('#detailsbody').html('<p><b></b><span>loading ... </span></p>');

        //get ip geo stuff
        if(ipCache[c.key] == null)
          getIPDAtaFromAPI(c, cType);
        else
          LoadIPDetails(ipCache[c.key], cType);
      }
    }
  }

  $('#graph-panel').show();
}

function isPrivateIP(ip) {
   var parts = ip.split('.');
   return parts[0] === '10' || 
      (parts[0] === '172' && (parseInt(parts[1], 10) >= 16 && parseInt(parts[1], 10) <= 31)) || 
      (parts[0] === '192' && parts[1] === '168');
}

function getIPDAtaFromAPI(c, cType)
{
  var ipGeoIPAPI = "https://www.telize.com/geoip/" + c.key + "?callback=?";
    $.getJSON( ipGeoIPAPI, {
      format: "json"
    })
      .done(function(d) {
        console.log(d);  

        d.nodeKey = c.key;

        ipCache[c.key] = d;

        LoadIPDetails(d, cType);
      }
    );
}

function LoadIPDetails(d, cType)
{
    var htmlBodyContent = '';

    htmlBodyContent += '<div>';
    htmlBodyContent += '<p><b>IP: </b><span>' + d.ip + '</span></p>';
    htmlBodyContent += '<p><b>Country: </b><span>' + d.country + '</span></p>';
    htmlBodyContent += '<p><b>City: </b><span>' + d.city + '</span></p>';
    htmlBodyContent += '<p><b>Region: </b><span>' + d.region + '</span></p>';
    htmlBodyContent += '<p><b>ISP: </b><span>' + d.isp + '</span></p>';
    htmlBodyContent += '</div>';

    $('#detailsTitle').html('<b style="font-size: 1.2em;">' + cType + '</b><br/><br/>');
    $('#detailsbody').html(htmlBodyContent);
}
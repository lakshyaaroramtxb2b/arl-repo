/*!
 */
(function(root) {
    /*global define,module */
    'use strict';
    var execReport = {};
    execReport.constructor = function execReport(){};


    execReport.postures = function(posture) {
        var keys = Object.keys(posture);
        for (var i=0; i<keys.length;i++) {
          var key = keys[i]
          var elem = document.getElementById(key)
          if (elem) {
            elem.innerHTML=posture[key];
          }
        }
    }

    // AMD / RequireJS
    if (typeof define !== 'undefined' && define.amd) {
      define([], function () {
        return execReport;
      });
    }
    // CommonJS / Node.js
    else if (typeof module !== 'undefined' && module.exports) {
      module.exports = execReport;
    }
    // included directly via <script> tag
    else {
      root.execReport = execReport;
    }
    
}(this));
/*!
 */
(function(root) {
    /*global define,module */
    'use strict';
    var metricsCharts = {};
    metricsCharts.constructor = function metricsCharts(){};


    metricsCharts.georgeTable = function(div_id, data, acquisitions, categories, maxes) {
        var heatmapTable = document.getElementById(div_id);
        for (var j=0; j<acquisitions.length; j++) {
          var rowDiv = document.createElement("div");
          rowDiv.setAttribute("class","heatmapTableRow");
          heatmapTable.appendChild(rowDiv);

          var labelSectionDiv = document.createElement("div");
          labelSectionDiv.setAttribute("class","heatmapLabelContainer");
          rowDiv.appendChild(labelSectionDiv);
          var acquisition_span = document.createElement("span");
          acquisition_span.setAttribute("class","column_one");
          acquisition_span.innerHTML = acquisitions[j];
          labelSectionDiv.appendChild(acquisition_span);

          var indicatorsSectionDiv = document.createElement("div");
          indicatorsSectionDiv.setAttribute("class","heatmapIndicatorContainer");
          rowDiv.appendChild(indicatorsSectionDiv);
          for(var i=0; i < categories.length; i++) {
            var id = "a_row_" + acquisitions[j].replace(" ","_").replace(".","-") + "_col_" + categories[i].replace(" ","_").replace(".","-")

            var indicator_span = document.createElement("span");
            indicator_span.setAttribute("class","column_indicator");
            var indicator_div = document.createElement("div");
            indicator_div.setAttribute("id",id)
            indicator_span.appendChild(indicator_div)
            indicatorsSectionDiv.appendChild(indicator_span)

            var value = data[acquisitions[j]][categories[i]]
            var max = maxes[i]
            metricsCharts.indicator_square("#" + id,35,value,max,data[acquisitions[j]][categories[i]])
          }
          var summarySectionDiv = document.createElement("div");
          rowDiv.appendChild(summarySectionDiv);

          summarySectionDiv.setAttribute("class","heatmapSummaryContainer");

          var summary_span = document.createElement("span");
          summarySectionDiv.appendChild(summary_span);

          summary_span.setAttribute("class","column_summary");
          summary_span.innerHTML = data[acquisitions[j]]["Summary"]

          /*
          var cell1 = row.insertCell();
          cell1.innerHTML = data[acquisitions[j]]["Summary"]
          cell1.setAttribute("style","border-bottom:1pt dashed #ccc;")
          */
        }
    }


    metricsCharts.indicator_square = function(div_id, size, value, max, label) {
      var margin = { top: 0, right: 0, bottom: 0, left: 0 }

      var width = size
          ,height = size
          ,buckets = 6
          ,colors = ["#c0392b","#e67e22","#f1c40f","#f1c40f","#27ae60"]

      var svg = dimple.newSvg(div_id, size, size);

      var colorScale = d3.scale.linear()
          .domain([0, 20, 40, 80, 95])
          .range(colors);

      // 3, -7  = 45  4/7
      // 0, -7  = 100 7/7
      var value_pct = 0; 
      if (max > 0) {
        value_pct = (value / max);
      } else if (max < 0) {
        value_pct = (parseInt(max) + parseInt(value)) / max
      }

      value_pct = Math.min(value_pct, 1.0);
      value_pct = Math.max(value_pct, 0.01);

      var actual_radius = size - (value_pct * ((size/2)-5))

      var heatMap = svg
          .append("rect")
          .attr("x", (size-actual_radius)/2)
          .attr("y", (size-actual_radius)/2)
          .attr("rx", 30)
          .attr("ry", 30)
          .attr("width", actual_radius)
          .attr("height", actual_radius)
          .style("fill", colorScale(value_pct*100))

      if (max == 100) {
        var text = svg
          .append("text")
            .attr("x", function(d, i) { return size/2 })
            .attr("y", function(d, i) { return size/2 + 3; })
            .attr("text-anchor", "middle")
            .attr("font-size","8px")
            .attr("fill","#FFF")
          .text(function(d) { 
              return label + "%"
          });

      } else {
        var text = svg
          .append("text")
            .attr("x", function(d, i) { return size/2 })
            .attr("y", function(d, i) { return size/2 + 4; })
            .attr("text-anchor", "middle")
            .attr("font-size","11px")
            .attr("fill","#FFF")
          .text(function(d) { 
            if (max > 0) {
              return label + "/" + max  
            }
            return label;
          });        
      }
    }

    metricsCharts.spider_maturity_comparison = function(canvas_id, acq_data, acquisition) {
      var options = {
          //Boolean - Whether to show lines for each scale point
          scaleShowLine : true,
          scaleSteps: 7,
          scaleStepWidth: 1,
          //Boolean - Whether we show the angle lines out of the radar
          angleShowLineOut : true,
          //Boolean - Whether to show labels on the scale
          scaleShowLabels : true,
          // Boolean - Whether the scale should begin at zero
          scaleBeginAtZero : true,
          //String - Colour of the angle line
          angleLineColor : "rgba(0,0,0,.1)",
          //Number - Pixel width of the angle line
          angleLineWidth : 1,
          //String - Point label font declaration
          pointLabelFontFamily : "'Helvetica'",
          //String - Point label font weight
          pointLabelFontStyle : "normal",
          //Number - Point label font size in pixels
          pointLabelFontSize : 12,
          //String - Point label font colour
          pointLabelFontColor : "#000",
          //Boolean - Whether to show a dot for each point
          pointDot : true,
          //Number - Radius of each point dot in pixels
          pointDotRadius : 3,
          //Number - Pixel width of point dot stroke
          pointDotStrokeWidth : 1,
          //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
          pointHitDetectionRadius : 20,
          //Boolean - Whether to show a stroke for datasets
          datasetStroke : true,
          //Number - Pixel width of dataset stroke
          datasetStrokeWidth : 2,
          //Boolean - Whether to fill the dataset with a colour
          datasetFill : true
      }
      var acq_levels = [1,
       acq_data["Product Security Level"],
       acq_data["Production Network Security Level"],
       1,
       acq_data["Internal Network Security Level"],
       acq_data["Incident Response Security Level"],
       3,
       acq_data["Policy & Programs Level"]
      ]
      var data = {
          labels: ["Secure Development Lifecycle",
                    "Product Security",
                    "Production Network",
                    "Network Security Lifecycle",
                    "Internal Network",
                    "Incident Response",
                    "Physical Security",
                    "Policy and Programs"],
          datasets: [
              {
                  label: "Salesforce",
                  fillColor: "rgba(220,220,220,0.2)",
                  strokeColor: "rgba(220,220,220,1)",
                  pointColor: "rgba(220,220,220,1)",
                  pointStrokeColor: "#fff",
                  pointHighlightFill: "#fff",
                  pointHighlightStroke: "rgba(220,220,220,1)",
                  data: [3, 5, 5, 3, 5, 5, 5, 5]
              },
              {
                  label: acquisition,
                  fillColor: "#2ecc71",
                  strokeColor: "#2ecc71",
                  pointColor: "#2ecc71",
                  pointStrokeColor: "#fff",
                  pointHighlightFill: "#fff",
                  pointHighlightStroke: "#2ecc71",
                  data: acq_levels
              }
              /*,
              {
                  label: "My Second dataset",
                  fillColor: "rgba(151,187,205,0.2)",
                  strokeColor: "rgba(151,187,205,1)",
                  pointColor: "rgba(151,187,205,1)",
                  pointStrokeColor: "#fff",
                  pointHighlightFill: "#fff",
                  pointHighlightStroke: "rgba(151,187,205,1)",
                  data: [28, 48, 40, 19, 96, 27, 100]
              }*/
          ]
      };
      var ctx = $(canvas_id).get(0).getContext("2d");
      var myRadarChart = new Chart(ctx).Radar(data, options);
      legend(document.getElementById("lineLegend"), data);
    }


    metricsCharts.next_level = function(acq_data, table_id, level_field_name) {
      if (level_field_name == undefined) {
        level_field_name = "Maturity Level"
      }
      var level_num = acq_data[level_field_name]
      var next_level_idx = "Level " + (parseInt(level_num)+1)
      var next_level = acq_data[next_level_idx]
      var area_names = Object.keys(next_level);
      var requirementsTable = document.getElementById(table_id);

      for (var i=0; i < area_names.length; i++) {
        var area_name = area_names[i]
        var area_requirements =  Object.keys(next_level[area_name])
        for (var j=0; j < area_requirements.length; j++) {
          var area_requirement = area_requirements[j]
          var area_status = next_level[area_name][area_requirement]
          if (area_status == "Y") {
            continue;
          }
          var row = requirementsTable.insertRow();
          var cell1 = row.insertCell();
          var cell2 = row.insertCell();
          var cell3 = row.insertCell();
          cell1.innerHTML = area_name;
          cell2.innerHTML = area_requirement;
          if (area_status == "In-Progress") {
            cell3.innerHTML = "In-Progress"
          } else {
            cell3.innerHTML = "Not Planned"
          }
        }
      }
    }


    /******************************************************/
    /* Vulnerabilities */
    /******************************************************/
    metricsCharts.vulnerabilities_chart = function(div_id, data, vulnerabilities_table_id) {
      function addVulnerabilityToTable(vulnerability) {
        var table = document.getElementById(vulnerabilities_table_id);
        row = table.insertRow();
        cell = row.insertCell();
        cell.innerHTML = vulnerability["Speed"]
        cell = row.insertCell();
        cell.innerHTML = vulnerability["Acquisition"] + " - " + vulnerability["Issue"]
        cell = row.insertCell();
        cell.innerHTML = vulnerability["Worst Case"] + "<br>" + vulnerability["Months to Fix"] + " Months, " + vulnerability["Exploit Complexity"] + " Complexity, " + vulnerability["Company Risk"] + " Risk"
      }

      var statuses = ["In-Progress","Slow","Stalled","Not Planned"]
      var chartData = {}
      for (var i=0; i < statuses.length; i++) {
        chartData[statuses[i]] = {"Complexity":[statuses[i]],
                                  "Months":[statuses[i] + " Months"],
                                  "Vulnerabilities":["Vulnerabilities"],
                                 }
      }
      if (vulnerabilities_table_id) {
        var table = document.getElementById(vulnerabilities_table_id);
        table.innerHTML=""
        var header = table.createTHead();
        var row = header.insertRow();
        var cell;
        cell = row.insertCell();
        cell.innerHTML = "<b>Status</b>";
        cell = row.insertCell();
        cell.innerHTML = "<b>Issue</b>";
        cell = row.insertCell();
        cell.innerHTML = "<b>Worst Case</b>";
      }
      for (var i=0; i < data.length; i++) {
        var vulnerability = data[i];
        var speed = vulnerability["Speed"]
        var x = + Math.random()
        if (statuses.indexOf(speed) !== -1) {
          console.log("months " + JSON.stringify(chartData[speed]))
          chartData[speed]["Complexity"].push(parseInt(vulnerability["Exploit Complexity"]) + x);
          chartData[speed]["Months"].push(parseInt(vulnerability["Months to Fix"]));
          chartData[speed]["Vulnerabilities"].push(vulnerability);
          if (vulnerabilities_table_id) {
            addVulnerabilityToTable(vulnerability)
          }
        }
      }
      var chart = c3.generate({
          size: {
            height: 300,
          },
          bindto: div_id,
          data: {
            xs: {
              "In-Progress": "In-Progress Months",
              "Slow":"Slow Months",
              "Stalled": "Stalled Months",
              "Not Planned": "Not Planned Months",
            },
            columns: [
                  chartData["In-Progress"]["Complexity"],
                  chartData["In-Progress"]["Months"],
                  chartData["Slow"]["Complexity"],
                  chartData["Slow"]["Months"],
                  chartData["Stalled"]["Complexity"],
                  chartData["Stalled"]["Months"],
                  chartData["Not Planned"]["Complexity"],
                  chartData["Not Planned"]["Months"],
            ],
            colors: {"In-Progress":"#2ecc71",
                     "Slow":"#f1c40f",
                     "Stalled": '#e74c3c',
                     "Not Planned": '#8e8e8e',
            },
            type: 'scatter'

          },
          point: {
            r: function(d) {
              var radius = parseInt(chartData[d.id]["Vulnerabilities"][d.index+1]["Company Risk"]);
              radius = (radius / 100) * 30;
              return radius + 1;
            }
          },
          axis: {
              x: {
                  label: 'Months to Fix',
                  min:1,
                  max:18,
                  tick: {
                    values: [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18]
                  },
              },
              y: {
                  label: 'Complexity to Exploit',
                  min:1,
                  max:100,

              }
          },
           tooltip: {
                  format: {
                      title: function (d) { return d + ' Months'; },
                      value: function (value, ratio, id, idx) {
                          var vuln = "<div align='left' class='wordwrap' style='width:250px;'><b>" + chartData[id]["Vulnerabilities"][idx+1]["Acquisition"] + "</b>"
                          vuln += "<br><i>" + chartData[id]["Vulnerabilities"][idx+1]["Issue"] + "</i>"
                          vuln += "<br>" + chartData[id]["Vulnerabilities"][idx+1]["Worst Case"]
                          vuln += "</div>"
                          return vuln
                      }
                  }
              }
      });
    }



    /******************************************************/    
    /* Bugs */
    /******************************************************/
    metricsCharts.bugs_sla_bubble = function(div_id, data, sla_details_table_id) {
        // bugs_sla_bubble("#div_to_put_graph_in", [{data},], "id_of_table_to_put_details_in")
        var acquisition_names = dimple.getUniqueValues(data, ["acquisition"])
        var priorities = dimple.getUniqueValues(data, ["priority"])


        var svg = dimple.newSvg(div_id, "100%", 125 + 20*priorities.length*acquisition_names.length);

        var myChart = new dimple.chart(svg, data);
        var x = myChart.addMeasureAxis("x", "days_past_sla");
        x.overrideMin = -40;
        x.overrideMax = 180;
        x.showGridlines = true
        var y = myChart.addCategoryAxis("y", ["acquisition","priority"]);
        y.showGridlines = true
        y.title=''
        y.addGroupOrderRule(["P2", "P1", "P0"]);
        var z = myChart.addMeasureAxis("z","count");
        z.useLog = true
        z.logBase = 1.75; 

        var mySeries = myChart.addSeries(["b","cloud","days_past_sla","priority"], dimple.plot.bubble);
        mySeries.addOrderRule(function(a,b) {
          return -1;
        });

        function displayAreaBugs(obj) {
          if (sla_details_table_id == null) {
            return
          }
          var table = document.getElementById(sla_details_table_id);
          table.innerHTML=""
          var header = table.createTHead();
          var row = header.insertRow(0);
          var cell;
          cell = row.insertCell();
          cell.innerHTML = "<b>ID</b>";
          cell = row.insertCell();
          cell.innerHTML = "<b>Title</b>";

          var bugs = obj["seriesValue"][0]
          for (var i=0; i < bugs.length; i++) {
            row = table.insertRow();
            cell = row.insertCell();
            cell.innerHTML = "<A HREF=" + bugs[i].u + " target=\"_blank\">" + bugs[i].i + "</A>";
            cell = row.insertCell();
            cell.innerHTML = bugs[i].t;
          }
        }
        mySeries.addEventHandler("click", displayAreaBugs);

        myChart.setMargins("75px", "25px", "5%", "25px");
        // (left, top, right, bottom)
        myChart.addLegend(20, 10, 300, 20, "left");
        myChart.assignColor("P0", "#c0392b", "#900903", .75);
        myChart.assignColor("P1", "#2980b9", "#096099", .75);
        myChart.assignColor("P2", "#f1c40f", "#b1a40f", .75);

        myChart.draw();
        $( window ).resize(function() {
          myChart.draw(0, false);
        });
    }

   metricsCharts.georgeSLAHistory = function (div_id, data) {
        var acquisition_names = dimple.getUniqueValues(data, ["acquisition"])
        var data_map = {};
        var d = data.map(function(entry) {
            var week = entry["week"]
            if (data_map[week] == undefined) {
              data_map[week] = entry
            }
            data_map[week][entry["acquisition"]] = entry["violation_pct"]
            return entry
        });
        var dataArray = new Array;
        for(var o in data_map) {
            dataArray.push(data_map[o]);
        }
        //d = dimple.filterData(d, "priority", ["P0"]);
          var chart = c3.generate({
              size: {
                height: 200,
             },
              bindto: div_id,
              data: {
                json: dataArray,
                keys: {
                  x:"week",
                  value: acquisition_names
                },
                type:"spline",
              },
            grid: {
              y: {
                  lines: [{value: 80, color:"#F00", text: 'V2MOM'}]
              }
            },
 point: {
        show: false
    },
    padding: {
        top: 2,
        right: 40,
        bottom: 2,
        left: 30,
    },
              axis: {
                  y: {
                    min:1,
                    max:100,
                  },
                  x: {
                      type: 'timeseries',
                      height: 55,
                      tick: {
                        rotate: 50,
                        format: '%Y-%m-%d'
                      }
                  }
              }
          });
   }

   metricsCharts.bugs_sla_history = function(div_id, data) {
        var data_map = {};
        var d = data.map(function(entry) {
            var week = entry["week"]
            if (data_map[week] == undefined) {
              data_map[week] = entry
            }
            data_map[week][entry["priority"]] = entry["violation_pct"]
            return entry
        });
        var dataArray = new Array;
        for(var o in data_map) {
            dataArray.push(data_map[o]);
        }
        //d = dimple.filterData(d, "priority", ["P0"]);
          var chart = c3.generate({
              size: {
                  height: 225
              },
              bindto: div_id,
              data: {
                json: dataArray,
                keys: {
                  x:"week",
                  value: ["P0","P1"]
                },
                groups: [["acquisition"]],
                type:"line",
                colors: {
                  "P1": '#2980b9',
                  "P0": '#e74c3c'
                }
              },
            grid: {
              y: {
                  lines: [{value: 80, color:"#F00", text: 'V2MOM'}]
              }
            },
              axis: {
                  y: {
                    min:1,
                    max:100,
                  },
                  x: {
                      type: 'timeseries',
                      tick: {
                          format: '%Y-%m-%d'
                      }
                  }
              }
          });
      }


    metricsCharts.bugs_open_closed_timeseries = function(div_id, priority, open_or_closed, data, max_num_of_bugs, min_num_of_bugs) {
        var chart = c3.generate({
            size: {
                height: 225
            },
            bindto: div_id,
            data: {
              json: data,
              keys: {
                x:"date",
                value: ["opened", "closed", "open_at_end"]
              },
              names: {
                closed: priority + " Bugs Closed",
                opened: priority + " Bugs Opened",
                open_at_end: priority + " Bugs Leftover"         
              },
              types: {
                closed: 'bar',
                opened: 'bar',
                open_at_end: 'spline'
              },
              groups: [
                ["opened", "closed"]
              ],
              colors: {
                opened: '#e74c3c',
                closed: '#a1a1a1',
                open_at_end: '#34495e'
              }
            },
            grid: {
              y: {
                  lines: [{value: 0, text: ''}]
              }
            },
            padding: {right:20,left:20,bottom:20},

            axis: {
                y : {
                  max: max_num_of_bugs,
                  min: min_num_of_bugs,
                  tick: {
                    format: d3.format("d")
                  }
                },
                x: {
                    type: 'timeseries',
                    tick: {
                        rotate: 45,
                        height: 55,
                        format: '%Y-%m'
                    }
                }
            }
        });
    }





    /******************************************************/
    /* Stuff generally from Google Docs */
    /******************************************************/
    metricsCharts.graph_timeline = function(div_id, data, months_plus_or_minus) {
        function pad(num, size) {
            var s = num+"";
            while (s.length < size) s = "0" + s;
            return s;
        }
        var date = new Date();
        date = new Date(date.getFullYear(), date.getMonth()+1+months_plus_or_minus, date.getDate()+20);
        var firstDay = new Date(date.getFullYear(), date.getMonth(), -1);
        var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, +1);

        var months = ["","January","February","March","April","May","June","July","August","September","October","November","December"]
        var firstDay_str = firstDay.getFullYear() + "-" + pad(firstDay.getMonth(),2) + "-" + pad(firstDay.getDate(),2)
        var lastDay_str = lastDay.getFullYear() + "-" + pad(lastDay.getMonth(),2) + "-" + pad(lastDay.getDate(),2)
        var filter_month = date.getFullYear() + "-" + pad(date.getMonth(),2)
        data = dimple.filterData(data, "month", [filter_month]);

        var chart = c3.generate({
            size: {
                width: 300,
                height: 400,
            },
             bindto: div_id,
            data: {
              json: data,
              keys: {
                x:"date",
                value: ["y"]
              },
              colors: {
                y: '#fffff',
              },
              type: "line"
            },
              show:false,
            grid: {
              x: {
                  show: false,
                  lines: data,
              }
            },
            legend: {
              show: false,
            },
            axis: {
              rotated: true,
                y : {
                  min:0,
                  max:10,
                  show:false,
                  tick: {
                      fit: false,
                      format: d3.format("d")
                  }
                },
                x: {
                  label:months[date.getMonth()],
                  min: firstDay_str,
                  max: lastDay_str,
                  type: 'timeseries'

                }
            }
        });
    }

    function reduce(data, keep_keys, sum_keys) {
        var data_map = {}
        for (var i=0; i< data.length; i++) {
          var entry = data[i]
          var z = {}
          for(var j=0; j < keep_keys.length; j++) {
            var keep_key = keep_keys[j]
            z[keep_key] = entry[keep_key]
          }
          var key = JSON.stringify(z)
          if(data_map[key] == undefined) {
            data_map[key] = entry
          } else {
            for(var j=0; j < sum_keys.length; j++) {
              var sum_key = sum_keys[j]
              data_map[key][sum_key] += entry[sum_key]
            }
          }
        }
        var dataArray = new Array;
        for(var o in data_map) {
            dataArray.push(data_map[o]);
        }
        return dataArray      
    }

    metricsCharts.cloudpassage_unique_servers_chart = function(div_id, data) {
        var d = reduce(data,["cloud","group"],["count","unique_count"])
        var chart = c3.generate({
            bindto: div_id,
            size: {
              height: 600
            },
            data: {
              json: d,
              keys: {
                x: "group",
                value: ["count","unique_count"]
              },
              type : 'bar',
              colors: {
                "count": '#2980b9',
                "unique_count": '#e74c3c'
              },
            },
            axis: {
                rotated: true,
                x: {
                    type: 'category',
                  },
            },
            legend: {
                show: false
            }
        });
    }

    metricsCharts.cloudpassage_vulnerable_packages_chart = function(div_id, dataArray) {
        var d = reduce(data,["acquisition","cloud","vulnerable_package"],["count"])
        var d = d.map(function(entry) {
            entry[entry["vulnerable_package"]] = entry["count"];
            return entry
        });
        var values = dimple.getUniqueValues(d, "vulnerable_package"); 

        var chart = c3.generate({
              bindto: div_id,
              size: {
                height: 300
              },
              data: {
                json: d,
                keys: {
                  value: values
                },
                type : 'pie',

              },
              legend: {
                  show: false
              },
              pie:{
                label:{
                    format:function(x, id){
                     return id;
                  }
               }
           },
         tooltip:{
             format:{
              value:function(x){
              return x;
               }
           }
         },
        });
    },

    metricsCharts.maturity_donut_chart = function(div_id, data, level) {
        var chart = c3.generate({
            bindto: div_id,
            size: {
              height: 125
            },
            data: {
              json: data,
              keys: {
                value: ["Complete", "In-Progress", "No Plans"]
              },
              type : 'donut',
              colors: {
                Complete: '#2ecc71',
                "In-Progress": '#f1c40f',
                "No Plans": '#eeeeee'
              }
            },
            legend: {
                show: false
            },
            donut: {
                title: "Level " + level,
                width: 10,
                sort: false,
                label: {
                  show: false // to turn off the min/max labels.
                },
            },
         tooltip:{
             format:{
              value:function(x){
              return x;
               }
           }
         },
        });
    }

 metricsCharts.aggregate_maturity_history = function(div_id, data) {
    var aggregate_data = {}
    var total = 0;
    for (var i=0; i < data.length; i++) {
      if (aggregate_data[data[i]["date"]] == undefined) {
        aggregate_data[data[i]["date"]] = {"date":data[i]["date"], "In-Progress":0, "Total":0, "Yes":0};
      }
      for (var j=1; j<= 5; j++) {
        aggregate_data[data[i]["date"]]["In-Progress"] += parseInt(data[i]["Level " + j + " In-Progress"])
        aggregate_data[data[i]["date"]]["Total"] += parseInt(data[i]["Level " + j + " Total"])
        aggregate_data[data[i]["date"]]["Yes"] += parseInt(data[i]["Level " + j + " Yes"])
        total += parseInt(data[i]["Level " + j + " Total"])
      }
    }
    var new_data = []
    for(var key in aggregate_data) {
      new_data.push(aggregate_data[key]);
      total = aggregate_data[key]["Total"]
    }
    var colors = {}
    colors["Yes"] = "#2ecc71"
    colors["In-Progress"] = "#f1c40"
    console.log(JSON.stringify(new_data));
    console.log(total)
    var chart = c3.generate({
          size: {
            height: 175,
          },
          bindto: div_id,
          data: {
            json: new_data,
            keys: {
              x:"date",
              value: ["Yes","In-Progress"], 
            },
            names: {"Yes":"Complete","In-Progress":"In-Progress"},
            colors: {"In-Progress":"#f1c40f","Yes":"#2ecc71"},
            groups: [
                ["Yes","In-Progress"]
            ],
            order: null,
            type: 'area',
          },
          legend: {
          },
          grid: {
             y: {
              show: true
            },
          },
          padding: {right:30,left:20},

          axis: {
              y : {
                max: total-.5,
                min: 0,
                show: false,
              },
              x: {
                  type: 'timeseries',
                  height: 55,
                  tick: {
                      rotate: 55,
                      format: '%Y-%m-%d'
                  }
              }
          }
      });
 }

 metricsCharts.maturity_history_metrics = function(data, up_to_maturity_level) {
    var aggregate_data = {}
    var total = 0;
    var inprogress = 0;
    var yes = 0;
    for (var i=0; i < data.length; i++) {
      if (aggregate_data[data[i]["date"]] == undefined) {
        aggregate_data[data[i]["date"]] = {"date":data[i]["date"], "In-Progress":0, "Total":0, "Yes":0};
      }
      for (var j=1; j<= up_to_maturity_level; j++) {
        aggregate_data[data[i]["date"]]["In-Progress"] += parseInt(data[i]["Level " + j + " In-Progress"])
        aggregate_data[data[i]["date"]]["Total"] += parseInt(data[i]["Level " + j + " Total"])
        aggregate_data[data[i]["date"]]["Yes"] += parseInt(data[i]["Level " + j + " Yes"])
        total += parseInt(data[i]["Level " + j + " Total"])
      }
    }
    var new_data = []
    var now = null;
    for(var key in aggregate_data) {
      new_data.push(aggregate_data[key]);
      console.log(key)
      now = new Date(key)
      total = aggregate_data[key]["Total"]
      yes = aggregate_data[key]["Yes"]
      inprogress = aggregate_data[key]["In-Progress"]
    }
    var now_utc = new Date(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate(),  now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds());
    var before_utc = new Date(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate()-35,  now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds());
    var now_str = now_utc.getUTCFullYear() + '-'
                + ('0' + (now_utc.getMonth()+1)).slice(-2) + '-'
                + ('0' + now_utc.getUTCDate()).slice(-2)
    var before_str = before_utc.getUTCFullYear() + '-'
                + ('0' + (before_utc.getMonth()+1)).slice(-2) + '-'
                + ('0' + before_utc.getUTCDate()).slice(-2)
    //var now_str = now_utc.getUTCFullYear().toString()  + "-" + now_utc.getUTCMonth().toString() + "-" + now_utc.getUTCDate().toString()
    //var before_str = before_utc.getUTCFullYear().toString()  + "-" + before_utc.getUTCMonth().toString() + "-" + before_utc.getUTCDate().toString()


    var history_metrics = {
      current: {
        date: now_str,
        inprogress: aggregate_data[now_str]["In-Progress"],
        complete: aggregate_data[now_str]["Yes"],
      },
      fourweeksago: {
        date: now_str,
        inprogress: aggregate_data[before_str]["In-Progress"],
        complete: aggregate_data[before_str]["Yes"],
      }
    }
    return history_metrics
 }


 metricsCharts.level3_maturity_history = function(div_id, data) {
    var aggregate_data = {}
    var total = 0;
    for (var i=0; i < data.length; i++) {
      if (aggregate_data[data[i]["date"]] == undefined) {
        aggregate_data[data[i]["date"]] = {"date":data[i]["date"], "In-Progress":0, "Total":0, "Yes":0};
      }
      for (var j=1; j<= 3; j++) {
        aggregate_data[data[i]["date"]]["In-Progress"] += parseInt(data[i]["Level " + j + " In-Progress"])
        aggregate_data[data[i]["date"]]["Total"] += parseInt(data[i]["Level " + j + " Total"])
        aggregate_data[data[i]["date"]]["Yes"] += parseInt(data[i]["Level " + j + " Yes"])
        total += parseInt(data[i]["Level " + j + " Total"])
      }
    }
    var new_data = []
    for(var key in aggregate_data) {
      new_data.push(aggregate_data[key]);
      total = aggregate_data[key]["Total"]
    }
    var colors = {}
    colors["Yes"] = "#2ecc71"
    colors["In-Progress"] = "#f1c40"
    console.log(JSON.stringify(new_data));
    console.log(total)
    var chart = c3.generate({
          size: {
            height: 175,
          },
          bindto: div_id,
          data: {
            json: new_data,
            keys: {
              x:"date",
              value: ["Yes","In-Progress"], 
            },
            names: {"Yes":"Complete","In-Progress":"In-Progress"},
            colors: {"In-Progress":"#f1c40f","Yes":"#2ecc71"},
            groups: [
                ["Yes","In-Progress"]
            ],
            order: null,
            type: 'area',
          },
          legend: {
          },
          grid: {
             y: {
              show: true
            },
          },
          padding: {right:30,left:20},

          axis: {
              y : {
                max: total-.5,
                min: 0,
                show: false,
              },
              x: {
                  type: 'timeseries',
                  height: 55,
                  tick: {
                      rotate: 55,
                      format: '%Y-%m-%d'
                  }
              }
          }
      });
 }

 metricsCharts.maturity_level_progress_chart_all = function(div_id, data) {
        var total = parseInt(data[0]["Level "+trust_level+" Total"]);
        var trust_level_complete = "Level " + trust_level + " Yes"
        var colors = {}
        colors["Level " + trust_level + " Yes"] = "#2ecc71"
        colors["Level " + trust_level + " In-Progress"] = "#f1c40f"
        var names = {}
        names["Level " + trust_level + " Yes"] = "Complete"
        names["Level " + trust_level + " In-Progress"] = "In-Progress"

        var trust_level_inprogress = "Level " + trust_level + " In-Progress"
        var chart = c3.generate({
            size: {
                height: 125
            },
            bindto: div_id,
            data: {
              json: data,
              keys: {
                x:"Date",
                value: [trust_level_inprogress, 
                        trust_level_complete]
              },
              names: names,
              colors: colors,
              groups: [
                  [trust_level_complete, trust_level_inprogress]
              ],
              type: 'area',
            },
            legend: {
              show: false
            },
            grid: {
               y: {
                ticks: total,
                show: true
              },
            },
            axis: {
                y : {
                  max: total-.5,
                  min: .1,
                  show: false,
                },
                x: {
                    type: 'timeseries',
                    tick: {
                        format: '%Y-%m',
                    }
                }
            }
        });
    }


    metricsCharts.maturity_level_progress_chart = function(div_id, data, trust_level) {
        var total = parseInt(data[0]["Level "+trust_level+" Total"]);
        var trust_level_complete = "Level " + trust_level + " Yes"
        var colors = {}
        colors["Level " + trust_level + " Yes"] = "#2ecc71"
        colors["Level " + trust_level + " In-Progress"] = "#f1c40f"
        var names = {}
        names["Level " + trust_level + " Yes"] = "Complete"
        names["Level " + trust_level + " In-Progress"] = "In-Progress"

        var trust_level_inprogress = "Level " + trust_level + " In-Progress"
        var chart = c3.generate({
            size: {
                height: 150
            },
            bindto: div_id,
            data: {
              json: data,
              keys: {
                x:"date",
                value: [trust_level_complete,trust_level_inprogress],
              },
              names: names,
              colors: colors,
              groups: [
                  [trust_level_complete, trust_level_inprogress]
              ],
              order: null,
              type: 'area',
            },
            legend: {
              show: false
            },
            grid: {
               y: {
                ticks: total,
                show: true
              },
            },
            axis: {
                y : {
                  max: total-.5,
                  min: .1,
                  show: false,
                },
                x: {
                    type: 'timeseries',
                    height: 100,
                    tick: {
                        rotate: 45,

                        format: '%Y-%m'
                    }
                }
            }
        });
    }

    // AMD / RequireJS
    if (typeof define !== 'undefined' && define.amd) {
      define([], function () {
        return metricsCharts;
      });
    }
    // CommonJS / Node.js
    else if (typeof module !== 'undefined' && module.exports) {
      module.exports = metricsCharts;
    }
    // included directly via <script> tag
    else {
      root.metricsCharts = metricsCharts;
    }

}(this));
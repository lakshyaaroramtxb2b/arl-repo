trigger ESA_Service_Item_Question_BeforeInsert on ESA_Service_Item_Question__c (before insert) {
     EsaOHAppValidationsHelper.Esa_Service_Item_Question_Dulicate_Validation(Trigger.new);
     EsaOHAppValidationsHelper.createFieldTrack(Trigger.new, Trigger.oldMap);
}
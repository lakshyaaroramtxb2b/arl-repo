trigger TM_Environment_BeforeDelete on Environment__c (before delete) {

    TM_preventEnvDeleteIfObjectivesExist.preventEnvDeleteIfObjectivesExist(Trigger.oldMap.keySet());
}
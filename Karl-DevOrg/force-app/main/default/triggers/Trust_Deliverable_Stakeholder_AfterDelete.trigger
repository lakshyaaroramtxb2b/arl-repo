trigger Trust_Deliverable_Stakeholder_AfterDelete on Trust_Deliverable_Stakeholder__c (after delete) {
    
    // remove stakeholder from chatterfeed
    TD_StakeholderChatterHelper.removeStakeholderFromChatterFeed(trigger.old);   
    
    // remove stakeholder from denormalize data
    TD_StakeholderChatterHelper.updateDenormilizeData(trigger.old, true);   

}
trigger PRT_V2MOMChangeRequestTrigger on V2MOM_Change_Request__c (after update) {
    List<V2MOM_Change_Request__c> newList = new List<V2MOM_Change_Request__c>();
    List<V2MOM_Change_Request__c> oldList = new List<V2MOM_Change_Request__c>();
    Map<Id, V2MOM_Change_Request__c> newMap = new Map<Id, V2MOM_Change_Request__c>();
    Map<Id, V2MOM_Change_Request__c> oldMap = new Map<Id, V2MOM_Change_Request__c>();

    for(V2MOM_Change_Request__c crRecord : Trigger.new) {
        if(crRecord.Status__c == 'Approved - Changed Manually') {
            newList.add(crRecord);
            if(Trigger.newMap!=null && !Trigger.newMap.isEmpty()){
                newMap.put(crRecord.Id,Trigger.newMap.get(crRecord.Id)); 
            }
            if(Trigger.oldMap!=null && !Trigger.oldMap.isEmpty()){
                oldMap.put(crRecord.Id,Trigger.oldMap.get(crRecord.Id));   
                oldList.add(Trigger.oldMap.get(crRecord.Id));             
            }
        } 
    }  
    
    if(newList != null  && !(newList.isEmpty())){
        if(Trigger.isAfter){
            // After Update Functionality
            if(Trigger.isUpdate){
                PRT_V2MOMChangeReqTriggerHandler.afterUpdate(newList, oldMap);
            }
        }
    }
}
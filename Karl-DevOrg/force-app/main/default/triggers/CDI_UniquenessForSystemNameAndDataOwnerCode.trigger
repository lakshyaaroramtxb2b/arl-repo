trigger CDI_UniquenessForSystemNameAndDataOwnerCode on CDI_Data_Center_Owner__c (before insert,before update) {
for(CDI_Data_Center_Owner__c cdco:Trigger.new)
{
  
If(cdco.Source_System_Name__c !=null && cdco.Source_Data_Center_Owner_Code__c!=null)
    { 
    cdco.Unique_key__c=cdco.Source_System_Name__c + ' - ' + cdco.Source_Data_Center_Owner_Code__c;
    }
    If(cdco.Source_System_Name__c !=null && cdco.Source_Data_Center_Owner_Code__c==null)
    {
        cdco.addError('Either Source System Name or Source Data Center Owner Code is missing');
    }
    Else if(cdco.Source_System_Name__c ==null && cdco.Source_Data_Center_Owner_Code__c!=null)
    {
        cdco.addError('Either Source System Name or Source Data Center Owner Code is missing');
    }
   }
}
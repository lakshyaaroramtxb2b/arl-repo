trigger ValidateCodeScan on CodeScan__c (before insert, before update) {

    if (Trigger.isInsert) {
        Map<String, CodeScan__c> orgs = new Map<String, CodeScan__c>();
        Map<String, CodeScan__c> users = new Map<String, CodeScan__c>();
        Datetime now = Datetime.now();
        for (CodeScan__c cs : Trigger.new) {
            //add token
            cs.token__c = generateToken();
            
            //add missing created date
            if (cs.Created__c == null) cs.Created__c = now;
            if (cs.Job_Type__c == 'PartnerPortal') {
              if (cs.OrgID__c == null) {
                      cs.WorkState__c='Never';
                      cs.comments__c='No Org Id';
                  } else if (orgs.containsKey(cs.OrgID__c)) {
                      cs.WorkState__c='Never';
                      cs.comments__c='Duplicate OrgID in partner portal job';
                  } else {
                        orgs.put(cs.OrgID__c, cs);
                  }
              if (cs.Username__c == null) {
                      cs.WorkState__c='Never';
                      cs.comments__c='No Username';
                  } else if (users.containsKey(cs.Username__c)) {
                      cs.WorkState__c='Never';
                      cs.comments__c='Duplicate Username in partner portal job';
                  } else {
                    orgs.put(cs.Username__c, cs);
                  }        
              }
           }     
 
         List<CodeScan__c> dupes = [SELECT Id, OrgID__c, Username__c, CreatedDate FROM CodeScan__c WHERE Job_Type__c='PartnerPortal' AND ( 
                    (WorkState__c IN ('New','Download Started','Passed To Worker') AND OrgID__c in :orgs.keySet()) OR
                    (Username__c in :users.keySet() AND CreatedDate > :Datetime.now().addMinutes(-30))
                    )];
                    
        //never out codescans that are duplicates   
        for (CodeScan__c dupe : dupes) {
            if (orgs.containsKey(dupe.OrgID__c)) {
              CodeScan__c dupeCS = orgs.get(dupe.OrgID__c);
              dupeCS.WorkState__c = 'Never';
              dupeCS.comments__c = 'Duplicate OrgID or Username in partner portal job';   
            }  
        }
    }
            
   
    if (Trigger.isUpdate) {
        for (CodeScan__c obj : Trigger.New) {
            // don't allow modification of this field
            // because if it's deleted or changed, we 
            // can have lower entropy tokens
            obj.token__c = Trigger.oldMap.get(obj.id).token__c;
        } 
    }
    
    private string generateToken() {
        // Generates a random 64 character token that can be used to look up scan status
        Integer len = 64;
        Blob blobKey = crypto.generateAesKey(256);
        String key = EncodingUtil.convertToHex(blobKey);
        return key.substring(0,len);
    }
}
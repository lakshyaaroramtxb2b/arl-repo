trigger basicDataCopyTrigger on Scan_Queue__c (after update) {
    List<Scan_Info__c> scanInfoToUpdate = new List<Scan_Info__c>();
    for (Scan_Queue__c sq : Trigger.new) {
        Id si_id = sq.Scan_Info__c;
        if (si_id != null) {
            datetime startTime = sq.Scan_Start__c;
            Integer lines = (Integer) sq.Line_Count__c;
            Integer queueLen = (Integer) sq.Queue_Size__c;
            Scan_Info__c si = [SELECT Id, Scan_Start__c, Queue_Size__c, Line_Count__c FROM Scan_Info__c WHERE Id =: si_id];
            // these checks are to ensure old sq objects don't overwrite values in si objects
            if (startTime != null) {
                si.Scan_Start__c = startTime;
            }
            if (lines != null) {
                si.Line_Count__c = lines;
            }
            if (queueLen != null) {
                si.Queue_Size__c = queueLen;
            }
            scanInfoToUpdate.add(si);
        }
    }
    update scanInfoToUpdate;
}
trigger DAC_DSEC_Linker on Detection_Comparison__c (after insert, after update, before delete) {
    
    if(Trigger.IsDelete) {
        
        for( Detection_Comparison__c dcDeleted : Trigger.old )
        {
			Id DACId = null;
            
            List<Detection_Stage__c> stages = [SELECT DetectionAlertCriteria__c FROM Detection_Stage__c WHERE Id =: dcDeleted.DetectionStage__c];

            if(stages.size() == 1)
            {
                if(stages[0].DetectionAlertCriteria__c != null)
                {
                    DACId = stages[0].DetectionAlertCriteria__c;
                }
            }
            
           delete [SELECT Id, DAC__c , DSEC__c
                 								FROM DAC_linked_DSEC__c
                 								WHERE DAC__c =: DACId
                                                  AND DSEC__c =: dcDeleted.ValueToCompare__c];
    	}
        
        return;
    }
    
    for( Detection_Comparison__c dc : Trigger.new ) {
        
        List<Detection_Stage__c> stages = [SELECT DetectionAlertCriteria__c FROM Detection_Stage__c WHERE Id =: dc.DetectionStage__c];
        
        Id DACId = null;
        
        if(stages.size() == 1)
        {
            if(stages[0].DetectionAlertCriteria__c != null)
            {
                DACId = stages[0].DetectionAlertCriteria__c;
            }
        }
        
        //DAC_linked_DSEC__c x = new DAC_linked_DSEC__c();
        //             x.DAC__c = DACId;
        //             x.DSEC__c = dc.ValueToCompare__c;
                    
        //            insert x; dc.FieldToCompare__r.name == 'DetectionSecurityEventCriteria' && 

        if(DACId != null)
        {
            if (dc.ValueToCompare__c instanceOf Id)
            {
                if (Trigger.isUpdate) {
                    
                    Detection_Comparison__c dc_old = trigger.oldMap.get(dc.Id);
                    
                    List<DAC_linked_DSEC__c> dld = [SELECT Id, DAC__c , DSEC__c
                                                        FROM DAC_linked_DSEC__c
                                                        WHERE DAC__c =: DACId
                                                          AND DSEC__c =: dc_old.ValueToCompare__c ]; 
                        
                        if(dld.size() == 1)
                        {
                            dld[0].DSEC__c = dc.ValueToCompare__c;
                            
                            update dld[0];
                        }
                        else
                        {
                            DAC_linked_DSEC__c new_dld = new DAC_linked_DSEC__c();
                            new_dld.DAC__c = DACId;
                            new_dld.DSEC__c = dc.ValueToCompare__c;
                            
                            insert new_dld;
                        }
            	}
                else
                {
                     DAC_linked_DSEC__c new_dld = new DAC_linked_DSEC__c();
                     new_dld.DAC__c = DACId;
                     new_dld.DSEC__c = dc.ValueToCompare__c;
                    
                    insert new_dld;
                }
            }
        }
    }
}
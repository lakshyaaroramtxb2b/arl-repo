trigger ESA_Service_Item_Rule_BeforeInsert on ESA_Service_Item_Rule__c (before insert) {
   ESASIRuleHandler.setSIRuleOrderSequence(Trigger.new);
   ESASIRuleHandler.validateFormula(Trigger.new);
}
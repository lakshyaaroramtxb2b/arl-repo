trigger KARL_CycleAuditReportsTrigger on KARL_Cycle_Audit_Report_Items__c (before update,after update) {
    if(Trigger.isBefore){
        if(Trigger.isUpdate){
            KARL_CycleAuditReportsTriggerHandler.beforeUpdateOperations(Trigger.new,Trigger.oldMap);
        }  
    }
    if(Trigger.isAfter){
        if(Trigger.isUpdate){
            KARL_CycleAuditReportsTriggerHandler.afterUpdateOperations(Trigger.new,Trigger.newMap,Trigger.oldMap);
        }
    }
}
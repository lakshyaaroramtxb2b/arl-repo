trigger CDI_NoSourceandTragetDatacentersameandUniqueness on CDI_Data_Transfer__c (before insert,before update) {
    set<Id> datastoredeploymentids=new set<Id>();
    List<CDI_Data_Store_Deployment__c> dst =new List<CDI_Data_Store_Deployment__c>();
    Map<Id,Id> datacentermap=new Map<Id,Id>();
  
    for(CDI_Data_Transfer__c dt:Trigger.new)
    {
        datastoredeploymentids.add(dt.Data_Store_Deployment__c);
         dt.Unique_Key__c=dt.Data_Store_Deployment__c + ' - ' + dt.Data_Center__c;
    
  If(dt.Source_System_Name__c !=null && dt.Source_Data_Transfer_Code__c!=null)
    { 
    dt.Unique_Key_Source_System_and_Code__c=dt.Source_System_Name__c + ' - ' + dt.Source_Data_Transfer_Code__c;
    }
    If(dt.Source_System_Name__c !=null && dt.Source_Data_Transfer_Code__c==null)
    {
        dt.addError('Either Source System Name or Source Data Transfer Code is missing');
    }
    Else if(dt.Source_System_Name__c ==null && dt.Source_Data_Transfer_Code__c!=null)
    {
        dt.addError('Either Source System Name or Source Data Transfer Code is missing');
    }
    }
    dst=[Select Id,Name,Data_Center__c,Data_Store__c,Data_Store__r.Name,Data_Center__r.Name from CDI_Data_Store_Deployment__c where Id IN:datastoredeploymentids];
    for(CDI_Data_Store_Deployment__c dsd:dst)
    {
        datacentermap.put(dsd.Data_Center__c, dsd.Data_Center__c);
        
    }
    for(CDI_Data_Transfer__c dt:Trigger.new)
    {
        If(dt.Data_Center__c == datacentermap.get(dt.Data_Center__c))
        {
            dt.addError('Source and Target Data Center Should not be Same');
        }
    }
}
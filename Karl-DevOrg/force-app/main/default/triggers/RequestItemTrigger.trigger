trigger RequestItemTrigger on Request_Item__c (After update, After insert){
    ARL_RequestItemTriggerHandler.run();
}
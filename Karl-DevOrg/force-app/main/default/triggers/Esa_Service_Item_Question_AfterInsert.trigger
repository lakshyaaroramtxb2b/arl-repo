trigger Esa_Service_Item_Question_AfterInsert on ESA_Service_Item_Question__c (after insert) {
  EsaOHAppValidationsHelper.Esa_SecurityQuestion_EntityValidation(Trigger.newMap);
}
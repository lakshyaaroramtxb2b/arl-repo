trigger ESA_Security_Question_BeforeInsert on ESA_Security_Question__c (before insert) {   
    // Check for uniqueness
    ESASIRuleHandler.validateUniqueName(Trigger.new);
}
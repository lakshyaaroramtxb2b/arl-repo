/*
Developer: Jorge Caceres <jcaceres@salesforce.com>
Description:
    Place all OBJECT OPERATION trigger actions in this file to provide
    to allow for easy deduction of execution order
*/
trigger TrainingCourse_BeforeInsert on Training_Course__c (before insert) {
   SC_TrainingCourseValidations.validatePopulateOrgWideEmail(Trigger.new, null);
}
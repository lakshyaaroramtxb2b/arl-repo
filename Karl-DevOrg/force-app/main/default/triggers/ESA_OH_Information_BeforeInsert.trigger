trigger ESA_OH_Information_BeforeInsert on ESA_Office_Hours_Information__c (before insert) {
    EsaOHAppValidationsHelper.Esa_OH_Validations(trigger.New);
}
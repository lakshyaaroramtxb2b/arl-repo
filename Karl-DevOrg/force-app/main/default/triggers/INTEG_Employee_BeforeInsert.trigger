trigger INTEG_Employee_BeforeInsert on INTEG_Employee__c (before insert) {

    INTEG_EmployeeHelper.populateManagerName(trigger.new, null);
    INTEG_EmployeeHelper.populateActiveEmployeeFlag(trigger.New);

}
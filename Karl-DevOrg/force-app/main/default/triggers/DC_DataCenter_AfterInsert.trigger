/*

    Developer Name: Jorge L Caceres - jcaceres@salesforce.com | July 2018
    
    Description:
        Place all object trigger actions in this class to provide
        easy deduction of execution order
*/

trigger DC_DataCenter_AfterInsert on DC_DataCenter__c (after insert) {    
    DC_RollUp.rollUpClouds(trigger.new, null);    
}
trigger CDI_UniquenessForProductSourceSystemNameAndCode on CDI_Product__c (before insert,before update) {
Map<Id,CDI_Product__c> productrecords= new Map<Id,CDI_Product__c>([Select Id,Name,Parent_Product__c,Parent_Product__r.Name,Product_Full_Name__c from CDI_Product__c]);

for(CDI_Product__c cp:Trigger.new)
{
    If(Trigger.isBefore)
    {
   if(cp.Parent_Product__c == null)
   {
       cp.Product_Full_Name__c=cp.Name;
   }
   else if(cp.Parent_Product__c != null )
   {
       cp.Product_Full_Name__c=productrecords.get(cp.Parent_Product__c).Product_Full_Name__c + '>' + cp.Name;
    }
    else
    {
        System.debug('Product Full Name Genearted');
    }
    
    if(cp.Source_System_Name__c !=null && cp.Source_Product_Code__c!=null)
    { 
    cp.Unique_key__c=cp.Source_System_Name__c + ' - ' + cp.Source_Product_Code__c;
    }
    if(cp.Source_System_Name__c !=null && cp.Source_Product_Code__c==null)
    {
        cp.addError('Either Source System Name or Source Product Code is missing');
    }
    else if(cp.Source_System_Name__c ==null && cp.Source_Product_Code__c!=null)
    {
        cp.addError('Either Source System Name or Source Product Code is missing');
    }
	
    }
    
}
      
}
trigger ESA_Case_AfterDelete on Case (after delete) {
    ESA_TriggerHelper.deleteSForceCaseChatterSubscription(Trigger.old);
}
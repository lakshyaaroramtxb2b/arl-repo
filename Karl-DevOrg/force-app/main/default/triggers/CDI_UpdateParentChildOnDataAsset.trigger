trigger CDI_UpdateParentChildOnDataAsset on CDI_Data_Asset__c (after update,after insert) {
    if(CDI_RecursiveHandler.IsNotRecursive){
        CDI_RecursiveHandler.IsNotRecursive = false;
        List<CDI_Data_Asset__c> dataassets=new List<CDI_Data_Asset__c>();
        Set<Id> idset=trigger.newMap.keySet();
        Set<Id> parentIds=new Set<Id>();
        //Map Parent DataAsset Id and Data Classification Level Value
        Map<Id,Set<Decimal>> mapParentIdAndDataClassificationLevelValue=new Map<Id,Set<Decimal>>();
        //Map Data Classification Level And Id 
        Map<Decimal,Id> mapLevelAndId=new Map<Decimal,Id>();
        //Map Data Classification Id And Level
        Map<Id,Decimal> mapIdAndLevel=new Map<Id,Decimal>();
        //Map Parent DataAsset Id and DataStore Name
        Map<Id,String> parentDataAssetIdAndDataStoreNameMap=new Map<Id,String>();
        // SOQL Query to Fetch Parent Data Asset Id From Child for Data Classification Update of Parent based on Child
        Map<Id,CDI_Data_Asset__c> dataassetsmap=new Map<Id,CDI_Data_Asset__c>([Select Id,Data_Store__r.Name,Parent_Data_Asset__r.Id,Parent_Data_Asset__r.Data_Classification__c, Data_Classification__r.Id,Data_Classification__r.Level__c from CDI_Data_Asset__c where Id IN:idset ]);
        //Select Child Data Assets For The Current Parent Data Asset to Update Children Data Based on Parent
        Map<Id,CDI_Data_Asset__c> childdataassetsmap=new Map<Id,CDI_Data_Asset__c>([ SELECT Id,Name,Data_Store__c,Data_Store__r.Name,Parent_Data_Asset__c,Parent_Data_Asset__r.Name,Parent_Data_Asset__r.Data_Store__c,External_Id__c,Data_Asset_Full_Name__c,Unique_Key__c FROM CDI_Data_Asset__c WHERE Parent_Data_Asset__c IN : idset ]);
        List<CDI_Data_Classification__c> dataclassifications=[Select Id,Level__c from CDI_Data_Classification__c];
        for(CDI_Data_Classification__c classification:dataclassifications)
        {
            if(classification.Level__c !=null){
                mapLevelAndId.put(classification.Level__c,classification.Id);
                mapIdAndLevel.put(classification.Id,classification.Level__c);    
            }        
        }
        
        for(Id key:dataassetsmap.keySet())
        {
            CDI_Data_Asset__c dataasset=dataassetsmap.get(key);
            parentIds.add(dataasset.Parent_Data_Asset__r.Id); 
            parentDataAssetIdAndDataStoreNameMap.put(dataasset.Id,dataasset.Data_Store__r.Name);
        }
        //Get Child Data Assets to fetch their Data Classification Valeues and Match them to the parent
        List<CDI_Data_Asset__c> childdataassets=[Select Id,Data_Store__r.Name,Parent_Data_Asset__r.Id,Parent_Data_Asset__r.Data_Classification__c, Data_Classification__r.Id,Data_Classification__r.Level__c from CDI_Data_Asset__c where Parent_Data_Asset__c IN:parentIds ];
        for(CDI_Data_Asset__c dataasset:childdataassets)
        {
            Set<Decimal> childLevels=null;
            if(mapParentIdAndDataClassificationLevelValue.get(dataasset.Parent_Data_Asset__r.Id)!=null)
            {
                childLevels= mapParentIdAndDataClassificationLevelValue.get(dataasset.Parent_Data_Asset__r.Id);
            }
            else
            {
                childLevels= new Set<Decimal>();
            }
            if(dataasset.Data_Classification__r.Level__c != null)
            {
                childLevels.add(dataasset.Data_Classification__r.Level__c);
            }
            if(dataasset.Parent_Data_Asset__r.Id!=null)
            {
                mapParentIdAndDataClassificationLevelValue.put(dataasset.Parent_Data_Asset__r.Id,childLevels);  
            }
        }
/*
* Code to Update Parent Data Classification based on Children
*/ 
        
        if(mapParentIdAndDataClassificationLevelValue.size()>0){
            for(Id key:mapParentIdAndDataClassificationLevelValue.keySet())
            {
                CDI_Data_Asset__c dataasset=new CDI_Data_Asset__c();
                if(key!=null && mapParentIdAndDataClassificationLevelValue.get(key)!=null && mapParentIdAndDataClassificationLevelValue.get(key).size()>0 ){
                    dataasset.Id=key;
                    List<Decimal> maxValue=new List<Decimal>(mapParentIdAndDataClassificationLevelValue.get(key));
                    maxvalue.sort();
                    dataasset.Data_Classification__c=mapLevelAndId.get(maxvalue[maxvalue.size()-1]);
                    //dataasset.Data_Category__c='a082x000001YIuNAAW';
                    dataassets.add(dataasset);
                }
                else
                {
                    dataasset.Id=key;
                    dataasset.Data_Classification__c=null;
                    //dataasset.Data_Category__c=null;
                    dataassets.add(dataasset);
                }
            }
        }
    
/*
* Code to Update Children Data based on Parent
*/
        
        if(childdataassetsmap.size()>0){
            for ( Id key : childdataassetsmap.keySet() ) {
                CDI_Data_Asset__c asset=childdataassetsmap.get(key);
                asset.Id=key;
                asset.External_Id__c=parentDataAssetIdAndDataStoreNameMap.get(asset.Parent_Data_Asset__c)+'/'+asset.Parent_Data_Asset__r.Name+'/'+asset.Name;
                asset.Data_Asset_Full_Name__c=asset.Parent_Data_Asset__r.Name+' > '+asset.Name;
                asset.Unique_Key__c=parentDataAssetIdAndDataStoreNameMap.get(asset.Parent_Data_Asset__c)+'/'+asset.Parent_Data_Asset__r.Name+'/'+asset.Name;
                asset.Data_Store__c=asset.Parent_Data_Asset__r.Data_Store__c;
                dataassets.add(asset);
            } 
        }
        update dataassets;   
    }
}
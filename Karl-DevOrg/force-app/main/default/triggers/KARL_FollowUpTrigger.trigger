trigger KARL_FollowUpTrigger on KARL_Cycle_Follow_up__c (After update, After insert) {
    KARL_FollowUpTriggerHelper.run();
}
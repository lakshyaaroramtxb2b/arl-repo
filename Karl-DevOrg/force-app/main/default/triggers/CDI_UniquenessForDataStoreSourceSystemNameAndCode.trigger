trigger CDI_UniquenessForDataStoreSourceSystemNameAndCode on CDI_Data_Store__c (before insert,before update) {
for(CDI_Data_Store__c cds:Trigger.new)
{

If(cds.Source_System_Name__c !=null && cds.Source_Data_Store_Code__c!=null)
    { 
    cds.Unique_key__c=cds.Source_System_Name__c + ' - ' + cds.Source_Data_Store_Code__c;
    }
    If(cds.Source_System_Name__c !=null && cds.Source_Data_Store_Code__c==null)
    {
        cds.addError('Either Source System Name or Source Data Store Code is missing');
    }
    Else if(cds.Source_System_Name__c ==null && cds.Source_Data_Store_Code__c!=null)
    {
        cds.addError('Either Source System Name or Source Data Store Code is missing');
    }
   }
}
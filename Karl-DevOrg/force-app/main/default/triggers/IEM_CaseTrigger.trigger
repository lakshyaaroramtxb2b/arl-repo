/*
* Created by - Prashant Gupta (MTX Group Inc.)
* IEM CASE TRIGGER, Specifically created for IEM Case related functionalities
*/
trigger IEM_CaseTrigger on Case (before insert, before Update,after insert, after update) {
    //Filtering data from the trigger 
    // Ensuring the triggers only for IEM Case Type
    List<Case> newList = new List<Case>();
    List<Case> oldList = new List<Case>();
    Map<id,Case> newMap = new Map<id,Case>();
    Map<id,Case> oldMap = new Map<id,Case>();
    for(Case caseRec : Trigger.new ){
        if(caseRec.RecordTypeId == IEM_Constants.IEM_CASERECORDTYPEID){
            newList.add(caseRec);
            if(Trigger.newMap!=null && !Trigger.newMap.isEmpty()){
            	newMap.put(caseRec.id,Trigger.newMap.get(caseRec.id)); 
            }
            if(Trigger.oldMap!=null && !Trigger.oldMap.isEmpty()){
            	oldMap.put(caseRec.id,Trigger.oldMap.get(caseRec.id));   
            	oldList.add(Trigger.oldMap.get(caseRec.id));             
            }
        }        
    }
    system.debug('newList'+ newList);
    if(newList==null  || newList.isEmpty()){
        return;
    }
    //Trigger Run
    if(Trigger.isAfter){
        if(trigger.isInsert){
            IEM_CaseTriggerHandler.afterInsert(newList);
        }else if(trigger.isUpdate){
            IEM_CaseTriggerHandler.afterUpdate(newList, oldMap);
            SBE_CaseTriggerHandler.afterUpdate(newList, oldMap);
        }
    }else if(Trigger.isBefore){
        if(trigger.isInsert){
            IEM_CaseTriggerHandler.beforeInsert(newList);
        }else if(trigger.isUpdate){
            IEM_CaseTriggerHandler.beforeUpdate(newList, oldMap,newMap);
            SBE_CaseTriggerHandler.beforeUpdate(newList, oldMap,newMap);
        }
    }
    
}
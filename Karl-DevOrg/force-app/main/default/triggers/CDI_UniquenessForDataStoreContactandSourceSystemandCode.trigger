trigger CDI_UniquenessForDataStoreContactandSourceSystemandCode on CDI_Data_Store_Contact__c (before insert,before update) {

for(CDI_Data_Store_Contact__c cdsc:Trigger.new)

{
    If(cdsc.Contact__c !=null)
    {
     cdsc.Unique_Key__c=cdsc.Data_Store__c + ' - ' + cdsc.Contact__c;
    }
    else
    {
        cdsc.Unique_Key__c=cdsc.Data_Store__c + ' - ' + cdsc.Contact_EMail__c;
    }
    If(cdsc.Source_System_Name__c !=null && cdsc.Source_Data_Store_Contact_Code__c!=null)
    { 
    cdsc.Unique_key_Source_System_and_Code__c=cdsc.Source_System_Name__c + ' - ' + cdsc.Source_Data_Store_Contact_Code__c;
    }
    If(cdsc.Source_System_Name__c !=null && cdsc.Source_Data_Store_Contact_Code__c==null)
    {
        cdsc.addError('Either Source System Name or Source Data Store Contact Code is missing');
    }
    Else if(cdsc.Source_System_Name__c ==null && cdsc.Source_Data_Store_Contact_Code__c!=null)
    {
        cdsc.addError('Either Source System Name or Source Data Store Contact Code is missing');
    }
}

}
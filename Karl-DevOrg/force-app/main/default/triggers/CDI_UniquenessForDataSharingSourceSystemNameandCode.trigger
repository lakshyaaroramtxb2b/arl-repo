trigger CDI_UniquenessForDataSharingSourceSystemNameandCode on CDI_Data_Sharing__c (before insert,before update) {

for(CDI_Data_Sharing__c ds:Trigger.new)
{
    ds.Unique_Key__c=ds.Data_Asset__c + ' - ' + ds.Data_Sharing_Party_Name__c;
    
    If(ds.Source_System_Name__c !=null && ds.Source_Data_Sharing_Code__c!=null)
    { 
    ds.Unique_Key_Source_System_and_Code__c=ds.Source_System_Name__c + ' - ' + ds.Source_Data_Sharing_Code__c;
    }
    If(ds.Source_System_Name__c !=null && ds.Source_Data_Sharing_Code__c==null)
    {
        ds.addError('Either Source System Name or Source Data Sharing Code is missing');
    }
    Else if(ds.Source_System_Name__c ==null && ds.Source_Data_Sharing_Code__c!=null)
    {
        ds.addError('Either Source System Name or Source Data Sharing Code is missing');
    }
}

}
trigger Audit_Cycle_Coverage on Audit_Cycle_Coverage__c (before insert,before update) {
     if(Trigger.isBefore && Trigger.isInsert){
         KARL_AuditCycleCoverageTriggerHandler.beforInsert(Trigger.new);
    }
    if(Trigger.isBefore && Trigger.isUpdate){ 
        KARL_AuditCycleCoverageTriggerHandler.beforeUpdate(Trigger.new, Trigger.oldMap);
    }

}
/*
Developer: Ralph Callaway <ralph@callaway.cloud>
Description:
    Place all OBJECT OPERATION trigger actions in this file to provide
    to allow for easy deduction of execution order
*/
trigger TrainingCourse_AfterInsert on Training_Course__c (after insert) {

    SC_AlternateCourseLinker.process(Trigger.new, Trigger.oldMap);

}
trigger UpdateDACEnv on DAC_linked_DSEC__c (after update) {
   Set<String> setPickList;
   Set<String> setNewList = new Set<String>();
   String newList;
    for (DAC_linked_DSEC__c co : Trigger.new){
      DAC_linked_DSEC__c old = Trigger.oldMap.get(co.Id);
      if (co.Env__c != old.Env__c) {
        Detection_Alert_Criteria__c po = [SELECT Id, Name, Environments__c FROM Detection_Alert_Criteria__c WHERE Id = :co.DAC__c];
    System.debug(po);
        List<DAC_linked_DSEC__c> l_co = [SELECT Id, Env__c FROM DAC_linked_DSEC__c WHERE DAC__c = :po.Id];
        System.debug(l_co);
        for(DAC_linked_DSEC__c all_co : l_co) {
            if (all_co.Env__c != null) { 
                setPickList = new Set<String> (all_co.Env__c.split(';') );
                setNewList.addAll(setPickList);
              System.debug(setNewList);
            }        
        } 
        for (String nl : setNewList) {
            if (newList == null) {
              newlist = nl;
            } else {
                newlist += ';' + nl;
            }
            System.debug(newList);
        }  
        po.Environments__c = newlist; 
        update po;
        newlist = null;
    } 
  }
}
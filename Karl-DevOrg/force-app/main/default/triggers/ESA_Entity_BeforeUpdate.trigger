trigger ESA_Entity_BeforeUpdate on ESA_Entity__c (Before Update) {

    // Validation for Create/Update Privileges for loggedin user.
    String profileName = [SELECT Name FROM Profile WHERE Id=:UserInfo.getProfileId()].Name;
    if(profileName != 'System Administrator') {
        EsaOHAppValidationsHelper.Esa_Entity_Handler(Trigger.new);
    }
}
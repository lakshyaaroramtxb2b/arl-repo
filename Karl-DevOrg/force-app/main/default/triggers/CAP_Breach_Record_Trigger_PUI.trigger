trigger CAP_Breach_Record_Trigger_PUI on CAP_Breach_Record__c (after update) 
{
    Boolean debug = false;
    
    for(CAP_Breach_Record__c capBreachRec : Trigger.new)
    {
        if(!capBreachRec.Breach_Status__c.toLowerCase().contains('false_positive'))
        {
            if(capBreachRec.Malware_Name__c.toLowerCase() == 'dyre' && capBreachRec.Breach_Status__c.toLowerCase().contains('available') && capBreachRec.User_Type__c.toLowerCase() == 'standard')
            {
                if(capBreachRec.Breach_Status__c.toLowerCase().contains('primary_contact_available') || capBreachRec.Breach_Status__c.toLowerCase().contains('security_contact_available'))
                {
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

                    
                    String[] toAddresses = new String[] {'amcneilly@salesforce.com', 'svlassis@salesforce.com'};
                            
                    mail.setToAddresses(toAddresses);
                    mail.setSubject('AutoCAP Trigger - Lock Account ' + capBreachRec.Name );

                    if(debug)
                    {
                        //for testing 
                        mail.setPlainTextBody('Testing. wanted to AutoCAP breach record ' + capBreachRec.Name + ' https://security.my.salesforce.com/' + capBreachRec.Id);
                        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
                    }
                    else
                    {
                        mail.setPlainTextBody('Live. did AutoCAP breach record ' + capBreachRec.Name + ' https://security.my.salesforce.com/' + capBreachRec.Id);
                        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
                        
                        CAPTriggerBridge.AsyncNotifyCustomer(capBreachRec.Id);
                    }
                }
            }
        }
    }
}
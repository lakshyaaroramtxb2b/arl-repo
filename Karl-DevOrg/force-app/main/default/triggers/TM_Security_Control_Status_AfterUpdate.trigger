/*

    Developer Name: Jorge L Caceres - jcaceres@salesforce.com | October 2015
        
    Description:
        Place all object trigger actions in this class to provide
        easy deduction of execution order
*/

trigger TM_Security_Control_Status_AfterUpdate on Security_Control_Status__c (after update) {
    
    // to avoid causing any issues with existing MTG system enclose everything with try/catch    
    /*
    try {
        TM_GenerateConcretes.updateConcreteFromSCS(trigger.new, trigger.oldMap);
    }
    catch (exception e) {
        system.debug(e);
    }
    */
}
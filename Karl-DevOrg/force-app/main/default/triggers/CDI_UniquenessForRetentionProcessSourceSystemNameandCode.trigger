trigger CDI_UniquenessForRetentionProcessSourceSystemNameandCode on CDI_Retention_Process__c (before insert,before update) {

for(CDI_Retention_Process__c rp:Trigger.new)
{
    If(rp.Source_System_Name__c !=null && rp.Source_Retention_Process_Code__c!=null)
    { 
    rp.Unique_Key_Source_System_and_Code__c=rp.Source_System_Name__c + ' - ' + rp.Source_Retention_Process_Code__c;
    }
    If(rp.Source_System_Name__c !=null && rp.Source_Retention_Process_Code__c==null)
    {
        rp.addError('Either Source System Name or Source Retention Process Code is missing');
    }
    Else if(rp.Source_System_Name__c ==null && rp.Source_Retention_Process_Code__c!=null)
    {
        rp.addError('Either Source System Name or Source Retention Process Code is missing');
    }
}


}
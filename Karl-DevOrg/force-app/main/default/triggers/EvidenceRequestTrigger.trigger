trigger EvidenceRequestTrigger on KARL_Evidence_Request__c (after insert, after update) {
    KARL_EvidenceRequestTriggerHelper.run();
}
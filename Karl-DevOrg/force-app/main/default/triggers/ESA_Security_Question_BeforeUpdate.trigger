trigger ESA_Security_Question_BeforeUpdate on ESA_Security_Question__c (before update) {
   
   // Check for uniqueness
   ESASIRuleHandler.validateUniqueName(Trigger.new);
    
   ESASIRuleHandler.validateNameChange(Trigger.newMap, Trigger.oldMap);
}
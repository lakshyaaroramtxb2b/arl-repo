trigger Esa_SecurityRequest_AfterUpdate on ESA_Security_Request__c (after update) {

    ESA_IntakeCancelAppointment.cancelAppoOnReq(trigger.new);

    if(trigger.new[0].Entity_Code__c == 'BUS' ) {
       EsaOHAppValidationsHelper.submitforApproval(trigger.new[0]);
    }

    //update contacts info
    EsaOHAppValidationsHelper.updateContacts(trigger.new);
    
    if (system.isBatch() == false && system.isFuture() == false){
       //Update GUS Team Info for SA Automation
       EsaOHAppValidationsHelper.updateGusTeamRecord(trigger.new[0], trigger.Old[0]);
       
       //Close  case/Work records depends on case approval.
       EsaIntakeRequestTriggerHelper.closeCaseWorkRecord(trigger.new[0], trigger.Old[0]);
    }
}
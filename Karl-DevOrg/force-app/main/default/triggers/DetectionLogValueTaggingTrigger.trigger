trigger DetectionLogValueTaggingTrigger on Detection_Log_Tagging_Value__c (before update) {
    
   List<Detection_Log_Tagging__c> logRecs = new List<Detection_Log_Tagging__c>();
    
    //cache needed records
    /*Map<Id, Detection_Log_Tagging__c> logTagRecords = 
        new Map<Id, Detection_Log_Tagging__c>([SELECT Id 
                                               FROM Detection_Log_Tagging__c]);*/

    //Goal: Force update of parent
    for( Detection_Log_Tagging_Value__c detectionLogValue : Trigger.new ) {
        
        //Detection_Log_Tagging__c logRec = detectionLogValue.Detection_Log_Tagging__c;
        Detection_Log_Tagging__c logRec = [SELECT Id 
                                           FROM Detection_Log_Tagging__c logRec 
                                           WHERE Id =: detectionLogValue.Detection_Log_Tagging__c];
        
        logRec.lastTagUpdated__c = detectionLogValue.Id;
        
        logRecs.add(logRec);
        
        /*Detection_Log_Tagging__c logRec = logTagRecords.get(detectionLogValue.Detection_Log_Tagging__c);
        logRec.lastTagUpdated__c = detectionLogValue.Id;
        
        logRecs.add(logRec);*/
    }
    
    //batch update
    update logRecs;
}
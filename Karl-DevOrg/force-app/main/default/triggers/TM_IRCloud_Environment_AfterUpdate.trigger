/*
    Developer Name: Jorge L Caceres - jcaceres@salesforce.com | November 2016    

    Description:
        Place all object trigger actions in this calss to provide
        easy deduction of execution order
*/
trigger TM_IRCloud_Environment_AfterUpdate on IRCloud__Environment__c (after update) {
    TM_GenerateConcretes.generateConcretesForEnvironment(trigger.new, trigger.oldMap);    
}
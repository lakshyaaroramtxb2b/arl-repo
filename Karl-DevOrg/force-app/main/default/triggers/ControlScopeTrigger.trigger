trigger ControlScopeTrigger on Control_Scope__c (Before update, Before insert, After update, After insert) {
    ARL_ControlScopeTriggerHandler.run();
}
trigger CSIRT_IncidentCloseCorrectiveActionCheck on Case (before update) {
Id rt1 = [SELECT Id FROM RecordType WHERE DeveloperName='CSIRT_Security_Incident' AND SobjectType='Case'].id;

for (Case C : Trigger.new) {
    IF (C.RecordTypeId == rt1 && C.Status == 'Closed'){
    Integer CICount = [SELECT COUNT() FROM Work_Item__c WHERE Related_Case__r.CaseNumber =:C.CaseNumber AND (Status__c <> 'Complete' AND Status__c <> 'Cancelled')];
    IF (CICount > 0){
    Trigger.new[0].addError('All corrective actions must be marked "Complete" or "Cancelled" before an incident can be closed.');
    }
    }
       }
}
trigger KARL_GRCControlsTrigger on KARL_GRC_Controls__c (Before insert, Before update, After update) {
    KARL_GRCControlsTriggerHelper.run();
}
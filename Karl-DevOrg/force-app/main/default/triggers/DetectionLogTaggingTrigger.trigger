trigger DetectionLogTaggingTrigger on Detection_Log_Tagging__c (after update) {
    
     Set<ID> ids = Trigger.newMap.keySet();
   
     List<Detection_Comparison__c> compObjsToUpdate = new List<Detection_Comparison__c>();
    
     Map<Id, Detection_Log_Tagging__c> tagCache 
                = new Map<Id, Detection_Log_Tagging__c>([SELECT Id, FieldToCompare__c, ValueToCompare__c, OperatorToApply__c, system_connector__c
                                                         FROM Detection_Log_Tagging__c 
                                                         WHERE Id in: ids]);
    
	 for( Detection_Log_Tagging__c dl : Trigger.new ) {
        
        if(Trigger.IsUpdate) {
            
            /*Detection_Log_Tagging__c dlFull = [SELECT Id, FieldToCompare__c, ValueToCompare__c, OperatorToApply__c
                      FROM Detection_Log_Tagging__c
                      WHERE Id =: dl.Id LIMIT 1];*/
            
            Detection_Log_Tagging__c dlFull = tagCache.get(dl.Id);
            
            if(dlFull.system_connector__c != 'maxmind') {
            
                Detection_Security_Event_Criteria__c [] parentEvent = [SELECT Id, Status__c, Implementation_System__c, Event_Type_Technical__c, Security_Event_Type__c, KillChain__c, Event_Rating__c 
                         FROM Detection_Security_Event_Criteria__c 
                         WHERE Id =: dl.Detection_Security_Event_Criteria__c LIMIT 1];
                
                if(parentEvent.size() > 0) {
                
                    if(parentEvent[0] != null) {
                    
                        //Adding a Text post
                        FeedItem post = new FeedItem();
                        post.ParentId = parentEvent[0].Id;
                        post.Body = parentEvent[0].Id;
                        //insert post;
    
                        Detection_Comparison__c [] compObjs = [SELECT Id
                                                       FROM Detection_Comparison__c
                                                       WHERE 
                                                        (FieldToCompare__c !=: dl.FieldToCompare__c OR
                                                         ValueToCompare__c !=: dl.ValueToCompare__c OR
                                                         OperatorToApply__c !=: dl.OperatorToApply__c) AND
                                                        Detection_Security_Event_Criteria__c =: parentEvent[0].Id 
                                                       LIMIT 1];
                    
                        if(compObjs.size() > 0) {
                            compObjs[0].FieldToCompare__c = dlFull.FieldToCompare__c;
                            compObjs[0].ValueToCompare__c = dlFull.ValueToCompare__c;
                            compObjs[0].OperatorToApply__c = dlFull.OperatorToApply__c;
                            
                            compObjsToUpdate.add(compObjs[0]); 
                        }
                    } 
                }
            }
        }
    }
    
    update compObjsToUpdate;
}
trigger CDI_UniquenessForDataAssetUsepurposeSourceSystemNameandCode on CDI_Data_Asset_Use_Purpose__c (before insert,before update) {
for(CDI_Data_Asset_Use_Purpose__c daup:Trigger.new)
{
    daup.Unique_Key__c=daup.Data_Asset__c + ' - ' + daup.Data_Use_Purpose__c;
    If(daup.Source_System_Name__c !=null && daup.Source_Data_Asset_Use_Purpose_Code__c!=null)
    { 
    daup.Unique_Key_Source_System_and_Code__c=daup.Source_System_Name__c + ' - ' + daup.Source_Data_Asset_Use_Purpose_Code__c;
    }
    If(daup.Source_System_Name__c !=null && daup.Source_Data_Asset_Use_Purpose_Code__c==null)
    {
        daup.addError('Either Source System Name or Source Data Asset Use Purpose Code is missing');
    }
    Else if(daup.Source_System_Name__c ==null && daup.Source_Data_Asset_Use_Purpose_Code__c!=null)
    {
        daup.addError('Either Source System Name or Source Data Asset Use Purpose Code is missing');
    }
}

}
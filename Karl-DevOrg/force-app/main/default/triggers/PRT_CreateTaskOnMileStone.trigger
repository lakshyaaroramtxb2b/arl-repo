/**
 * Created by hari ch on 23-08-2019.
 */
trigger PRT_CreateTaskOnMileStone on Milestone__c (after insert, after update) {

     if(Trigger.isAfter && (trigger.isInsert || trigger.isUpdate)) {
        //PRT_CreateTaskOnMileStoneHandler.afterInsert(Trigger.new);
     }
}
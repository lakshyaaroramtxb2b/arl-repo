/*
    Developer Name: Jorge L Caceres - jcaceres@salesforce.com | November 2016    

    Description:
        Place all object trigger actions in this calss to provide
        easy deduction of execution order
*/ 
trigger TM_IRCloud_Environment_AfterInsert on IRCloud__Environment__c (after insert) {
    TM_GenerateConcretes.generateConcretesForEnvironment(trigger.new, null);    
}
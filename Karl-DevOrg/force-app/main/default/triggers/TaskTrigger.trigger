trigger TaskTrigger on Task (after update) {
    if(Trigger.isAfter){
        if(Trigger.isUpdate){
           KARL_TaskTriggerHandler.afterUpdateOperations(Trigger.new,Trigger.oldMap);
        }
    }
}
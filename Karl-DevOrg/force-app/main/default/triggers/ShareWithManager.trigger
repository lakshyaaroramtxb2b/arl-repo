trigger ShareWithManager on ResponseForce__CSIRT_Training_Request__c (after insert) {
    Map<Id, Id> managerMap = new Map<Id, Id>();
    List<Id> ownerIdList = new List<Id>();
    List<ResponseForce__CSIRT_Training_Request__share> newShares = new List<ResponseForce__CSIRT_Training_Request__share>();
    
    
    // Get List of OwnerId's from training request
    for(ResponseForce__CSIRT_Training_Request__c request : Trigger.New){
        if(!ownerIdList.contains(request.OwnerId)){
            ownerIdList.add(request.OwnerId);
        }
    }
    
    List<User> userList = [SELECT Id, ManagerId FROM User WHERE Id IN :ownerIdList];
    
    // Map User to Manager
    for(User u : userList){
        if(!managerMap.containsKey(u.Id)){
            managerMap.put(u.Id, u.ManagerId);
        }
    }
    
    
    // Share request with manager
    for(ResponseForce__CSIRT_Training_Request__c request : Trigger.New){
        ResponseForce__CSIRT_Training_Request__share requestShare = new ResponseForce__CSIRT_Training_Request__share();
        requestShare.ParentId  = request.Id;
        requestShare.UserOrGroupId = managerMap.get(request.OwnerId); // Get Manager from the Map
        requestShare.AccessLevel = 'edit';
        requestShare.RowCause = Schema.ResponseForce__CSIRT_Training_Request__share.RowCause.Manager__c;
        newShares.add(requestShare);    
    }
    
    insert newShares;
}
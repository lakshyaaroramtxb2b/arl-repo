trigger CDI_UniquenessForDataIdentifiationCategorySourceSystemNameandCode on CDI_Data_Identification_Category__c (before insert,before update) {

for(CDI_Data_Identification_Category__c dic:Trigger.new)
{

      If(dic.Source_System_Name__c !=null && dic.Source_Data_Identification_Category_Code__c!=null)
    { 
    dic.Unique_Key_Source_System_and_Code__c=dic.Source_System_Name__c + ' - ' + dic.Source_Data_Identification_Category_Code__c;
    }
    If(dic.Source_System_Name__c !=null && dic.Source_Data_Identification_Category_Code__c==null)
    {
        dic.addError('Either Source System Name or Source Data Identification Category Code is missing');
    }
    Else if(dic.Source_System_Name__c ==null && dic.Source_Data_Identification_Category_Code__c!=null)
    {
        dic.addError('Either Source System Name or Source Data Identification Category Code is missing');
        }
}

}
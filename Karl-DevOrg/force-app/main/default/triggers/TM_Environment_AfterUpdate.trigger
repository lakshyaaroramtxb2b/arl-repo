/*
    Developer Name: Jorge L Caceres - jcaceres@salesforce.com | September 2015    

    Description:
        Place all object trigger actions in this calss to provide
        easy deduction of execution order
*/

trigger TM_Environment_AfterUpdate on Environment__c (after update) {

    //TM_GenerateConcretes.generateConcretesForEnvironment(trigger.new, trigger.oldMap);

}
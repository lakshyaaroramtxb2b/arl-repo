trigger ESA_Category_AfterInsert on ESA_Service_Item_Category__c (after Insert) {
    EsaOHAppValidationsHelper.Esa_Category_ExternalAssignment_Validation(Trigger.newMap);
}
trigger HeadCount_BeforeUpdate on Headcount__c (before update) {
    for(Headcount__c headCount : [SELECT Business_Unit_LU__c,Business_Unit_LU__r.Name__c, Business_Unit__c,
                                         Cost_Center_LU__c,Cost_Center_LU__r.Name__c,Cost_Center__c,
                                         Parent_Cloud__c,Parent_Cloud__r.Name__c,Parent_Cloud_Text__c,
                                         Employee_Name_Text__c,Headcount_Name__c,Headcount_Name__r.Name__c
                                  FROM Headcount__c
                                  WHERE Id IN :trigger.new]){
    
             if(headCount.Business_Unit_LU__c != null){
                headCount.Business_Unit__c = headCount.Business_Unit_LU__r.Name__c;
             }
            
             if(headCount.Cost_Center_LU__c != null){
                headCount.Cost_Center__c = headCount.Cost_Center_LU__r.Name__c ;
             }
            
             if(headCount.Parent_Cloud__c != null){
                headCount.Parent_Cloud_Text__c = headCount.Parent_Cloud__r.Name__c ;
             }
             
             if(headCount.Headcount_Name__c != null){
                headCount.Employee_Name_Text__c = headCount.Headcount_Name__r.Name__c;
             }else{
                headCount.Employee_Name_Text__c = null;
             }
    }

}
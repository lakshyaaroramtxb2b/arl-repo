trigger ScriptBreakeruper on QuizQuestion__c (before insert, before update) {
    pattern p = pattern.compile('Script|SCRIPT|script');
    for (QuizQuestion__c question: Trigger.new) {
        if (question.vf_code__c != null) {
            Matcher m = p.matcher(question.vf_code__c);
            if (m.find() == True) {
                // The damn UI would have HTML encoded everything.  We need to put it back
                question.vf_code__c = question.vf_code__c.replaceAll('&gt;','>');
                question.vf_code__c = question.vf_code__c.replaceAll('&lt;','<');
                question.vf_code__c = question.vf_code__c.replaceAll('&quot;','"');
                question.vf_code__c = question.vf_code__c.replaceAll('&#39;','\'');
                question.vf_code__c = question.vf_code__c.replaceAll('&amp;','&'); 
            }
        }
        if (question.apex_code__c != null) {
            Matcher m = p.matcher(question.apex_code__c);
            if (m.find() == True) {
                // The damn UI would have HTML encoded everything.  We need to put it back
                question.apex_code__c = question.apex_code__c.replaceAll('&gt;','>');
                question.apex_code__c = question.apex_code__c.replaceAll('&lt;','<');
                question.apex_code__c = question.apex_code__c.replaceAll('&quot;','"');
                question.apex_code__c = question.apex_code__c.replaceAll('&#39;','\'');
                question.apex_code__c = question.apex_code__c.replaceAll('&amp;','&'); 
            }
        }
        if (question.correct_line_opt1__c != null) {
            Matcher m = p.matcher(question.correct_line_opt1__c);
            if (m.find() == True) {
                // The damn UI would have HTML encoded everything.  We need to put it back
                question.correct_line_opt1__c = question.correct_line_opt1__c.replaceAll('&gt;','>');
                question.correct_line_opt1__c = question.correct_line_opt1__c.replaceAll('&lt;','<');
                question.correct_line_opt1__c = question.correct_line_opt1__c.replaceAll('&quot;','"');
                question.correct_line_opt1__c = question.correct_line_opt1__c.replaceAll('&#39;','\'');
                question.correct_line_opt1__c = question.correct_line_opt1__c.replaceAll('&amp;','&'); 
            }
        }
        if (question.correct_line_opt2__c != null) {
            Matcher m = p.matcher(question.correct_line_opt2__c);
            if (m.find() == True) {
                // The damn UI would have HTML encoded everything.  We need to put it back
                question.correct_line_opt2__c = question.correct_line_opt2__c.replaceAll('&gt;','>');
                question.correct_line_opt2__c = question.correct_line_opt2__c.replaceAll('&lt;','<');
                question.correct_line_opt2__c = question.correct_line_opt2__c.replaceAll('&quot;','"');
                question.correct_line_opt2__c = question.correct_line_opt2__c.replaceAll('&#39;','\'');
                question.correct_line_opt2__c = question.correct_line_opt2__c.replaceAll('&amp;','&'); 
            }
        }
        if (question.correct_line_opt3__c != null) {
            Matcher m = p.matcher(question.correct_line_opt3__c);
            if (m.find() == True) {
                // The damn UI would have HTML encoded everything.  We need to put it back
                question.correct_line_opt3__c = question.correct_line_opt3__c.replaceAll('&gt;','>');
                question.correct_line_opt3__c = question.correct_line_opt3__c.replaceAll('&lt;','<');
                question.correct_line_opt3__c = question.correct_line_opt3__c.replaceAll('&quot;','"');
                question.correct_line_opt3__c = question.correct_line_opt3__c.replaceAll('&#39;','\'');
                question.correct_line_opt3__c = question.correct_line_opt3__c.replaceAll('&amp;','&'); 
            }
        }
        if (question.correct_version__c != null) {
            Matcher m = p.matcher(question.correct_version__c);
            if (m.find() == True) {
                // The damn UI would have HTML encoded everything.  We need to put it back
                question.correct_version__c = question.correct_version__c.replaceAll('&gt;','>');
                question.correct_version__c = question.correct_version__c.replaceAll('&lt;','<');
                question.correct_version__c = question.correct_version__c.replaceAll('&quot;','"');
                question.correct_version__c = question.correct_version__c.replaceAll('&#39;','\'');
                question.correct_version__c = question.correct_version__c.replaceAll('&amp;','&'); 
            }
        }
    }
}
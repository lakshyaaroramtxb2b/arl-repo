/***********************************************************
* Trigger for Portfolio
* Created by 	- Prashant Gupta
* Date - 01-12-2019
************************************************************/

trigger PRT_PortfolioTrigger on Portfolio__c (after Insert, after Update, before Insert, before update) {
	
    if(!PRT_Constants.INTEGRATION_SETTING.Disable_Portfolio_Trigger__c  ){
        if(Trigger.isbefore && trigger.isUpdate) {
            PRT_PortfolioTriggerHandler.beforeUpdate(Trigger.new, trigger.oldMap);
        }
        
        else if(Trigger.isafter && trigger.isUpdate) {
            IF(PRT_Constants.RECURSIVE_TRIGGER_CHECK){
                PRT_Constants.RECURSIVE_TRIGGER_CHECK =FALSE;
                PRT_PortfolioTriggerHandler.afterUpdate(Trigger.new, trigger.oldMap);
            }
        }    
        else if(Trigger.isafter && trigger.isInsert) {
            PRT_PortfolioTriggerHandler.afterInsert(Trigger.new);
        }
        else if(Trigger.isbefore && trigger.isInsert) {
            PRT_PortfolioTriggerHandler.beforeInsert(Trigger.new);
        }
    }
}
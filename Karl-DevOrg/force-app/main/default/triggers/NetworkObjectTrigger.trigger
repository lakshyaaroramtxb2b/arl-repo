trigger NetworkObjectTrigger on Networks__c (before insert, before update) {

    String regex='^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$';
    Pattern p = Pattern.compile(regex);
        
    for(Networks__c network : Trigger.new)
    {
        if(p.matcher(network.Start_IP__c).matches())
        {
            network.IP_Start_Num__c = IPConverter.parseIp(network.Start_IP__c);
            
            System.debug('IP: '+ network.Start_IP__c + ', Converted to : ' + network.IP_Start_Num__c);
        }
        else
        {
            System.debug('Could not convert IP start to number. IP address is not valid. ' + network.Start_IP__c );
        }
        
        if(p.matcher(network.End_IP__c).matches())
        {
            network.IP_End_Num__c = IPConverter.parseIp(network.End_IP__c);
            
            System.debug('IP: '+ network.End_IP__c + ', Converted to : ' + network.IP_End_Num__c);
        }
        else
        {
            System.debug('Could not convert IP end to number. IP address is not valid. ' + network.End_IP__c );
        }
    }
}
trigger CSIRT_AlertToIRCloud on Case (before insert) {
    Map<String,Id> caseTypes = Utils.getRecordTypeMapForObjectGeneric(Case.SObjectType);
    for (Case c : Trigger.new){
        if (c.RecordTypeId != '01230000001GdDR'){
            continue;
        }
        //c.RecordTypeId = '0123A000001dxqg';
        
        if(string.IsBlank(c.Destination_IP_Address__c) == False && c.Destination_IP_Address__c.Length() > 15){
            c.IRCloud__IPDestination__c = c.Destination_IP_Address__c.substring(0,14);
        } else {
            c.IRCloud__IPDestination__c = c.Destination_IP_Address__c;
        }
            
        if(string.IsBlank(c.Alert_Destination_MAC_Address__c) == False && c.Alert_Destination_MAC_Address__c.Length() > 17){
            c.IRCloud__MacAddrDestination__c = c.Alert_Destination_MAC_Address__c.substring(0,16);
        } else {
            c.IRCloud__MacAddrDestination__c = c.Alert_Destination_MAC_Address__c;
        }
        
        if(string.IsBlank(c.Alert_Destination_Port__c)  == False && c.Alert_Destination_Port__c.Length() > 5){
            c.IRCloud__PortDestination__c = c.Alert_Destination_Port__c.substring(0,4);
        } else {
            c.IRCloud__PortDestination__c = c.Alert_Destination_Port__c;
        }
            
        c.IRCloud__Signature__c = c.Alert_Signature_Name__c;

        if(string.IsBlank(c.Alert_Source_IP__c) == False && c.Alert_Source_IP__c.Length() > 15){
            c.IRCloud__IPSource__c = c.Alert_Source_IP__c.substring(0,14);
        } else {
            c.IRCloud__IPSource__c = c.Alert_Source_IP__c;
        }
        
        if(string.IsBlank(c.Alert_Source_MAC_Address__c)  == False && c.Alert_Source_MAC_Address__c.Length() > 17){
            c.IRCloud__MacAddrSource__c = c.Alert_Source_MAC_Address__c.substring(0,16);
        } else {
            c.IRCloud__MacAddrSource__c = c.Alert_Source_MAC_Address__c;
        }
        
        if(string.IsBlank(c.Alert_Source_Port__c)  == False && c.Alert_Source_Port__c.Length() > 5){
            c.IRCloud__PortSource__c = c.Alert_Source_Port__c.substring(0,4);
        } else {
            c.IRCloud__PortSource__c = c.Alert_Source_Port__c;
        }
        
        c.IRMigrated__c = True;
        
    }
    
}
trigger CDI_UniquniessForDataStoreTypeSourceSystemNameAndCode on CDI_Datastore_Type__c (before insert,before update) {
for(CDI_Datastore_Type__c cdst:Trigger.new)
{

If(cdst.Source_System_Name__c !=null && cdst.Source_DataStore_Type_Code__c!=null)
    { 
    cdst.Unique_key__c=cdst.Source_System_Name__c + ' - ' + cdst.Source_DataStore_Type_Code__c;
    }
    If(cdst.Source_System_Name__c !=null && cdst.Source_DataStore_Type_Code__c==null)
    {
        cdst.addError('Either Source System Name or Source DataStore Type Code is missing');
    }
    Else if(cdst.Source_System_Name__c ==null && cdst.Source_DataStore_Type_Code__c!=null)
    {
        cdst.addError('Either Source System Name or Source DataStore Type Code is missing');
    }
   }
}
trigger ESA_Case_AfterUpdate on Case (after update) {
    ESA_TriggerHelper.manageSForceCaseChatterSubscription(Trigger.new);
    
    ESA_RequestCancelAppointment.cancelAppoOnReq(Trigger.new);
    
}
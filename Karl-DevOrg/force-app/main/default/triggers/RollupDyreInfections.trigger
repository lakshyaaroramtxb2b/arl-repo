trigger RollupDyreInfections on Case_to_User_Relationship__c (after insert, after delete) {
    
    if(Trigger.isInsert){
        
        InfectedUsersUtility.RollUpDyreInfectedUsers(Trigger.New, TRUE);
        
    }else if (Trigger.isDelete){
        
        InfectedUsersUtility.RollUpDyreInfectedUsers(Trigger.Old, FALSE);
        
    }
    
    
    

}
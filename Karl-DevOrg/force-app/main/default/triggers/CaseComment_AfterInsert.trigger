trigger CaseComment_AfterInsert on CaseComment (after insert) {
	EsaCaseCommentTriggerHandler.updateCaseStatus(trigger.new);
	EsaCaseCommentTriggerHandler.emailCaseContact(trigger.new);
}
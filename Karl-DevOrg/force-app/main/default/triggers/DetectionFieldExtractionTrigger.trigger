trigger DetectionFieldExtractionTrigger on Detection_Field_Extraction__c (before insert, before update) {

    List<Detection_Field_Extraction__c> allDFSs = [SELECT Id, Name, Log_Identifier_Regex__c 
                                                   FROM Detection_Field_Extraction__c
                                                   ];
    
    //System.debug('DetectionFieldExtractionTrigger');
  
    
    for( Detection_Field_Extraction__c in_dfe : Trigger.new )
	{        
        if(in_dfe.Log_Identifier_Regex__c != null) 
        {
            String in_dfe_mStr = ((in_dfe.Log_Identifier_Regex__c.replace('+', '')).replace('-', '')).replace('\n', '');
            Set<String> in_dfe_mStr_Vals =  new Set<String>(in_dfe_mStr.split('~~'));
            
            for(Detection_Field_Extraction__c dfe : allDFSs) {
            	
                if(dfe.Id != in_dfe.Id && (dfe.Log_Identifier_Regex__c != null && in_dfe.Log_Identifier_Regex__c != null))
                {
                  	String mStr = ((dfe.Log_Identifier_Regex__c.replace('+', '')).replace('-', '')).replace('\n', '');
                  	Set<String> mStr_Vals =  new Set<String>(mStr.split('~~'));
                        
                    /*if( (mStr_Vals.size() == in_dfe_mStr_Vals.size()) 
                        && mStr_Vals.containsAll(in_dfe_mStr_Vals) )*/
                    if(mStr_Vals.size() == in_dfe_mStr_Vals.size())
                    {
                        //mStr_Vals.containsAll(in_dfe_mStr_Vals)
                        
                        boolean containsAll = true;
                        
                        for(String val : mStr_Vals) 
                        {
                            boolean found = false; 
                            
                            for(String val2 : in_dfe_mStr_Vals)
                            {
                                if(val.equals(val2))
                                {
                                    found = true;
                                    break;
                                }
                            }
                            
                            if(found == false)
                            {
                                containsAll = false;
                                break;
                            }
                        }
                        
                        if(containsAll) {
                        
                            System.debug('================');
                                System.debug(dfe.Name);
                                System.debug(dfe.Log_Identifier_Regex__c);
                                System.debug(mStr);
                                System.debug(mStr_Vals);
                                System.debug('Size: ' + mStr_Vals.size());
                                System.debug('*******************');
                                System.debug(in_dfe.Name);
                                System.debug(in_dfe.Log_Identifier_Regex__c);
                                System.debug(in_dfe_mStr);
                                System.debug(in_dfe_mStr_Vals);
                                System.debug('Size: ' + in_dfe_mStr_Vals.size());
                            System.debug('================');
                            
                            in_dfe.addError('A field extraction already exists with the same log source string \'' + in_dfe.Log_Identifier_Regex__c + '\' (See \'' + dfe.Name  + '\'). Duplicates values are not supported as one log entry is supported by one field extraction');
                            
                            return;
                            
                        }
                    }
                }
            }
        }
    }
    
}
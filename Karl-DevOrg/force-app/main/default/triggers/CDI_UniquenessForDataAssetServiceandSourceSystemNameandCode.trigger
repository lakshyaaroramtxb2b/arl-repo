trigger CDI_UniquenessForDataAssetServiceandSourceSystemNameandCode on CDI_Data_Asset_Service__c (before insert,before update) {
for(CDI_Data_Asset_Service__c das:Trigger.new)
{
    das.Unique_key__c=das.Data_Asset__c + ' - ' + das.Service__c;
    
     If(das.Source_System_Name__c !=null && das.Source_Data_Asset_Service_Code__c!=null)
    { 
    das.Unique_Key_Source_System_and_Code__c=das.Source_System_Name__c + ' - ' + das.Source_Data_Asset_Service_Code__c;
    }
    If(das.Source_System_Name__c !=null && das.Source_Data_Asset_Service_Code__c==null)
    {
        das.addError('Either Source System Name or Source Data Asset Service Code is missing');
    }
    Else if(das.Source_System_Name__c ==null && das.Source_Data_Asset_Service_Code__c!=null)
    {
        das.addError('Either Source System Name or Source Data Asset Service Code is missing');
    }
}

}
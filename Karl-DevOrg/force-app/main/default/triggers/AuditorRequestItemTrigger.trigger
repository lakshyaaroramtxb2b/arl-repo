trigger AuditorRequestItemTrigger on KARL_Auditor_Request_Item__c ( Before Update, After Update , After Insert, Before Insert) {
    KARL_AuditorRequestItemTriggerHelper.run();
}
trigger CDI_DatastoreServiceUnique on CDI_Data_Store_Service__c (before insert,before update) {

for(CDI_Data_Store_Service__c dss:trigger.new)
{
        dss.Unique_Key__c=dss.Data_Store__c + ' - ' + dss.Service__c;
        If(dss.Source_System_Name__c !=null && dss.Source_Data_Store_Service_Code__c!=null)
    { 
    dss.Unique_Key_Source_System_and_Code__c=dss.Source_System_Name__c + ' - ' + dss.Source_Data_Store_Service_Code__c;
    }
    If(dss.Source_System_Name__c !=null && dss.Source_Data_Store_Service_Code__c==null)
    {
        dss.addError('Either Source System Name or Source Data Store Service Code is missing');
    }
    Else if(dss.Source_System_Name__c ==null && dss.Source_Data_Store_Service_Code__c!=null)
    {
        dss.addError('Either Source System Name or Source Data Store Service Code is missing');
    } 
}

}
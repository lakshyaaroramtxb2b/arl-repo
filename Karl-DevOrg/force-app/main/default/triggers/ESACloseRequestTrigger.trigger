trigger ESACloseRequestTrigger on ESA_Close_Request__e (after insert) {
   for (ESA_Close_Request__e eventObj : Trigger.new) {
      ESA_TriggerHelper.closeRequest(eventObj.Id__c, 'Closed', 'Auto Approved \n\n');
   }
}
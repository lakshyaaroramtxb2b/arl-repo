trigger CSIRT_chatterFirstPostSetsInProgress on FeedItem (after insert) {
    String caseKeyPrefix = Case.sObjectType.getDescribe().getKeyPrefix();
    List<RecordType> rts = [SELECT Id FROM RecordType WHERE SObjectType = 'Case' 
                                                     AND DeveloperName = 'CSIRT_Security_Incident'];
    RecordType rt;
    if (rts.size()  == 1) {
        rt = rts.get(0);
    } else {
        System.debug('Missing CSIRT_Security_Incident record type or there are more than 1');
        return;
    }
    Case[] toUpdate = new Case[]{};
    for (FeedItem f: trigger.new) {
        String parentId = f.parentId;
        if (parentId.startsWith(caseKeyPrefix) && (f.Type != 'TrackedChange')) {    
            
            Case c = [SELECT Id,Status,RecordTypeId FROM Case WHERE Id=:f.ParentId];
            if (c.RecordTypeId != rt.Id) {
                continue;
                
                
            }
            if (c.Status == 'New') {
                c.Status = 'In-Progress';
                toUpdate.add(c);
                

            }
        }
    }
    update toUpdate;
}
trigger CSIRT_assignWorkItem on Work_Item__c (before insert) {

    for (Work_Item__c wi: trigger.new) {
        
       String relatedComponent = wi.Component__c;
       String relatedDL = wi.Detection_Logic__c;
       String relatedLS = wi.Log_Source__c;
       String relatedSC = wi.SecurityControl__c;
       String componentOwnerId;
       
       if (relatedComponent != NULL) {
           componentOwnerId = [SELECT OwnerId FROM Component__c WHERE Id = :relatedComponent].OwnerId;
         }  
       else if (relatedDL != NULL) {
           componentOwnerId = [SELECT OwnerId FROM Detection_Logic__c WHERE Id = :relatedDL].OwnerId;
          }  
       else if (relatedLS != NULL) {
           componentOwnerId = [SELECT OwnerId FROM Log_Source__c WHERE Id = :relatedLS].OwnerId;
          }
    
       else if (relatedSC != NULL) {
           componentOwnerId = [SELECT OwnerId FROM Security_Control_Status__c WHERE Id = :relatedSC].OwnerId;
          }
            
       if (componentOwnerId != NULL) {
               wi.OwnerId = componentOwnerId;
            }
        }}
/**
 * @File Name          : IEM_CaseVisibilityTrigger.trigger
 * @Description        : 
 * @Author             : Banshi
 * @Group              : 
 * @Last Modified By   : Banshi
 * @Last Modified On   : 11/14/2019, 12:43:25 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    11/14/2019   Banshi     Initial Version
**/
trigger IEM_CaseVisibilityTrigger on Case_Visibility__c (after insert, after delete) {
    if(Trigger.isAfter){
        if(Trigger.isInsert){
            IEM_CaseVisibilityTriggerHandler.afterInsert(Trigger.new);
        }
        if(Trigger.isDelete){
            IEM_CaseVisibilityTriggerHandler.afterDelete(Trigger.old);
        }
    }
}
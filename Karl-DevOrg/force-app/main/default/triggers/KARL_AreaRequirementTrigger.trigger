trigger KARL_AreaRequirementTrigger on Area_Requirement__c (After update, After insert) {
    KARL_AreaRequirementTriggerHelper.run();
}
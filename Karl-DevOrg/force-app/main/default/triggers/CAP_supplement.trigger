trigger CAP_supplement on CAP_Breach_Record__c (after insert, after update) {
/*   
    for (CAP_Breach_Record__c workrd : Trigger.new)
    {
        if(workrd.Breach_Status__c == 'notprocessed-dyre-app-log-new') // 'notprocessed-dyre-app-log-new'
        {
            Map<String,String> btlookup;    
            CAP_Breach_Record__c workwrite = [select Username__c,UserID__c,id,ID__c from CAP_Breach_Record__c where Id = :workrd.Id];
            CAP_User__c userwrite = [select UserID__c,Username__c,OrgID__c from CAP_User__c where Id =:workrd.ID__c];
            String userName;
            if(workrd.Username__c.containsAny('@'))
            {
                btlookup = (BlackTab.getCustomerInfo(workrd.Username__c));        
                userName = btlookup.get('username');
            }
            else if (String.isBlank(userwrite.OrgID__c))
            {  
                userName = 'bogusdata';
            }
            else
            {
                btlookup = (BlackTab.getCustomerInfo(userwrite.OrgID__c,workrd.UserID__c));
                userName = btlookup.get('username');
            }
           
            if(userName.containsAny('@'))
            {
                String phone = btlookup.get('phone');
                String orgId = btlookup.get('orgId'); 
                String lastName = btlookup.get('lastName');                
                String email = btlookup.get('email');
                String userId = btlookup.get('userId'); 
                String user_status = btlookup.get('active'); 
                String org_status = btlookup.get('orgStatus'); //mark whether it is a demo or not
                String firstName = btlookup.get('firstName');
                String orgName = btlookup.get('orgName'); //use this for parent of parent ? (or just use later)
                String userType = btlookup.get('userType'); //check to see what type of use it is or just set field.
            
                userwrite.Username__c = userName;
                userwrite.OrgID__c = orgId;  
                userwrite.UserId__c = userId;
                userwrite.Account_Status__c = 'ready4nextstage';
                userwrite.User_Type__c = userType;
                userwrite.Org_Status__c = org_status;
                userwrite.Name = userName;
                
                workwrite.Username__c = userName;
                workwrite.UserId__c = userId;
                workwrite.Breach_Status__c = 'ready4nextstage';
                workwrite.Name = userName + ' compromised by DYRE';
                workwrite.User_Type__c = userType;
                
             //   Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
             //   String[] toAddresses = new String[]{'svlassis@salesforce.com'};
             //   mail.setToAddresses(toAddresses);
             //   mail.setSubject('lol here after sv-test');
             //   String body =  btlookup.get('phone') + ' '+ btlookup.get('orgId') + ' ' + btlookup.get('lastName') + ' ' +btlookup.get('username') +' ' + btlookup.get('email') + ' '+btlookup.get('userId')+' '+btlookup.get('active') +' '+ btlookup.get('orgStatus') + ' '+ btlookup.get('firstName') + ' '+btlookup.get('orgName') + ' '+btlookup.get('userType');
             //   mail.setPlainTextBody(body);
             //   Messaging.sendEmail(new Messaging.SingleEmailMessage[]{mail});
                
            update workwrite;
            update userwrite;
            }
            else
            {
             //mark as bogus data set breach status to what it is in the perl script.
                userwrite.Account_Status__c = 'no-valid-username';
                workwrite.Breach_Status__c = 'no-valid-username';
                update workwrite;
                update userwrite;
            }
    }
    }
*/
}
/*

    Developer Name: Jorge L Caceres - jcaceres@salesforce.com | Septermber 2015
    
    Description:
        Place all object trigger actions in this calss to provide
        easy deduction of execution order
*/

trigger TM_Account_AfterUpdate on Account (after update) {
    
    TM_BUsharingUtil.createAccessControlObjects(trigger.new, trigger.oldMap);
    TM_GenerateConcretes.generateConcretesForAccount(trigger.new, trigger.oldMap);

}
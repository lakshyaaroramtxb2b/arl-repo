trigger CSIRT_setToAddress on EmailMessage (after insert) {
/*sending an auto reply when a handler takes ownership
 * of a case requires the "To address" field to be populated
 * so that the response email is sent from the same 
 * org wide address it was sent to
 * This trigger also creates a contact and sets it on the case
 * as the contact details of the inbound sender email
 */
    //only fire if email is inbound
    //only set email_to__c if it has not been set
    
    //Debug test
    /*Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
    String[] toAddresses = new String[] {'bmalcolmson@salesforce.com'}; 
    mail.setToAddresses(toAddresses); 
    mail.setSubject('EmailMessage Trigger Fired'); 
    String body = 'SANDBOX parent id: ' + trigger.new[0].parentId + ' email is ' + 
        trigger.new[0].fromaddress + ' name ' + trigger.new[0].fromName; 
    mail.setPlainTextBody(body); 
    Messaging.sendEmail(new Messaging.SingleEMailMessage[]{mail}); 
    */
    
    //Choose the account record type for newly created contacts
    RecordType acctRecordType;
    AcctRecordType = [SELECT id FROM RecordType WHERE developername = 'Salesforce_Business_Unit' LIMIT 1]; 
    
    //select the email record type so we only create contact on email-to-case cases
    List<RecordType> emailCases = new List<RecordType>(CSIRT_caseCache.EmailRtId());
    Id emailCase = emailCases[0].id;
    system.debug('EmailCase is : ' +emailCase);
    
    //Map for parent case to the inbound email
    Map<Id, EmailMessage> inboundEmails = new Map<Id, EmailMessage>();  
    
    for(emailMessage e : trigger.new){
        system.debug('****** e.incoming value ' + e.incoming);
        if (e.incoming == true){
            inboundEmails.put(e.parentId, e);
                
            //debug the inbound email stuff
            system.debug('****** value of email fromemail is ' + e.fromaddress);
            system.debug('****** value of email to addressis' + e.toaddress);
            system.debug('****** value of email from name is' + e.fromName);
        }
    }
    
    //Check if the email operated on is inbound
    if(inboundEmails.size() >0){ 
    
    //Get details to be updated of parent case
    List<Case> parentCases = new List<Case>([select Id, email_to__c, contactId, suppliedName, SuppliedEmail, Impacted_Enviroment__c 
                                           FROM CASE
                                           WHERE id IN :inboundEmails.keyset() AND 
                                             recordTypeId = :emailCase]); 

    //This needs to be bulkified in future
    List<contact> contacts = new List<contact>();
        
    //Select the account type (govt cases are separated by this value)
    Account salesforceAccount = new Account();
    salesforceAccount = [Select Id FROM account where account.Name = 'salesforce.com'];
    
    Account governmentAccount = new Account();
    governmentAccount = [Select id FROM account where account.Name = 'salesforce.com - Government'];
    
    //Setup Contacts Map
    Map<String, Id> contactMap = new Map<String, Id>();
    for (Contact cm : [Select Id, Email FROM Contact where accountid = :salesforceAccount.id]){
        contactMap.put(cm.Email, cm.Id);
    }    
   
    for (case c : parentCases){
        
        system.debug('****** value of c.email_to__c is' +c.Email_To__c);
        //Retrieve the email from the map created earlier
        emailMessage email = inboundEmails.get(c.id);
        
        //Get Impacted Environment Id
        Environment__c[] env = [SELECT Id from Environment__c WHERE Security_Email__c = :email.toAddress];
        
        if (string.isblank(c.email_to__c)){
            
            system.debug('****** value of c.contactid is if the contact is set ' +c.contactId);
            system.debug('****** parent.id value is: ' +email.parentId);
            
            if (email.toAddress.contains('security_gov@salesforce.com')){
                c.email_to__c = 'security_gov@salesforce.com';
                
                //set the account to gov so that permissions are handled correctly.
                c.AccountId = governmentAccount.id;
            }
            else{
                c.email_to__c = 'security@salesforce.com';
            }
        }
        
        //Check Impacted Env
        if (string.isblank(c.Impacted_Enviroment__c)){
            if (env.size() > 0){
                    //set impacted environment
                    c.Impacted_Enviroment__c = env[0].Id;
                }
        }
        
        List<String> nameParts = new List<String>();
        if(c.contactId == null){
            system.debug('****** entered loop if c.contactId == null ' +c.contactId);
            system.debug('****** suppiedname =  ' + c.suppliedname);
            system.debug('****** value of email fromemail is ' + email.fromaddress);
            system.debug('****** value of email to addressis' + email.toaddress);
            system.debug('****** value of email from name is' + email.fromName);
            
            // if there is a supplied name check certain types of name formats (case field)
            // such as "firstname space lastname"
            // such as "email@email.com"
            
                if(c.suppliedName != null){
                    if (c.suppliedName.contains(' ')){
                            nameParts = c.SuppliedName.split(' ',2); 
                }
                    //Last name must be populated for a contact
                    //We will set the only name suppolied as the last name
                    else{
                        nameParts.add(c.SuppliedName);
                        nameParts.add(c.SuppliedName);
                    }
                } 
                //eElse the supplied name is null
                //check if the from name is provided from inbound email first
                else{
                    if(email.fromname == null){
                        nameparts.add('Name Not Provided');//If there is no supplied name
                        nameParts.add('Name Not Provided');
                    }
                    //if from name is not null set name parts if split by ' '
                    //or just add the supplied name as first name and last name
                    else{
                        if (email.fromname.contains(' ')){
                        nameParts = email.FromName.split(' ',2); 
                    }
                        //Else we set first name and last name as the same value
                        else{
                            nameParts.add(email.fromname);
                            nameParts.add(email.fromname); 
                        }
                    }
                }

                if (contactMap.get(email.fromaddress) == null){
                    if (nameParts.size() == 2){
                        Contact cont = new Contact(FirstName=nameParts[0],
                                                    LastName=nameParts[1],
                                                    Email=email.fromaddress,
                                                    AccountId=salesforceAccount.id,
                                                    RecordType = AcctRecordType);

                        insert cont;
                        c.contactId = cont.id;
                        system.debug('****** create new contact loop, case contact is now set to ' + c.contactId);
                    }   
                    //Name parts should always be of size 2.
                    //If not, we set the first and last name as 'Name not Provided'
                    else{
                        Contact cont = new Contact(FirstName='Name not Provided',
                                                   LastName='Name not Provided',
                                                   Email=email.fromaddress,
                                                   AccountId=salesforceAccount.id,
                                                   RecordType = AcctRecordType);

                        insert cont;
                        c.contactId = cont.id;
                        system.debug('****** create new contact loop, case contact is now set to ' + c.contactId);
                    }
                } else {
                    c.contactId = contactMap.get(email.fromaddress);
                }
            }
            
            system.debug('****** before updateing the case the case contact is now ' + c.contactId);
            update c;
    }
  }    
}
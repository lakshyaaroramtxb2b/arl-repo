/*
Developer: Ralph Callaway <ralph@callaway.cloud>
Description:
    Place all OBJECT OPERATION trigger actions in this file to provide
    to allow for easy deduction of execution order
*/
trigger TrainingCourseTaken_BeforeDelete on Training_Course_Taken__c (before delete) {
    SC_TrainingCourseTakenUtil.deleteRelationships(Trigger.old);
}
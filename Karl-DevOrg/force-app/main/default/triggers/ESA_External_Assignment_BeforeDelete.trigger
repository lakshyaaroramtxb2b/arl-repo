trigger ESA_External_Assignment_BeforeDelete on ESA_External_Assignment__c (before delete) {
   EsaOHAppValidationsHelper.Esa_ExternalAssignment_Delete_Validations(Trigger.oldMap);
}
/**
 * BPO App v1
 * Feb 2017
 * Written By: Joanna Chen
 * 
 * Prevent the deletion of a BPO Assessment if child BPO Tasks exist.
 * 
**/


trigger BPO_preventDeleteIfTasksExist on BPO_Assessment__c (before delete) {

    Set <ID> triggerIds = Trigger.oldMap.keySet();
    List<BPO_Assessment__c> assessments = new List<BPO_Assessment__c>();
    
    assessments = [SELECT Id,
            (SELECT Id FROM BPO_Tasks__r)
            FROM BPO_Assessment__c 
            WHERE Id IN :triggerIds];

    for (BPO_Assessment__c a: assessments) {
        if (a.BPO_Tasks__r.size() > 0) {
            Trigger.oldMap.get(a.Id).addError('Cannot delete a BPO Assessment if it has BPO Tasks associated with it.');
        }
    }
}
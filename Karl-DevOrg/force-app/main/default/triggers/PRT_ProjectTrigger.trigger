/***********************************************************
* Created by 	- Prashant Gupta
* Date 		- 16th July 2019
************************************************************/
trigger PRT_ProjectTrigger on Project__c (after insert,before update, after update, before insert) {
    
    if(!PRT_Constants.INTEGRATION_SETTING.Disable_Project_Trigger__c ){
        if(Trigger.isbefore && trigger.isUpdate) {
            PRT_ProjectTriggerHandler.beforeUpdate(Trigger.new, trigger.oldMap);
        }
        
        else if(Trigger.isafter && trigger.isUpdate) {
            IF(PRT_Constants.RECURSIVE_TRIGGER_CHECK){
                PRT_Constants.RECURSIVE_TRIGGER_CHECK =FALSE;
                PRT_ProjectTriggerHandler.afterUpdate(Trigger.new, trigger.oldMap);
            }
        }    
        else if(Trigger.isafter && trigger.isInsert) {
            PRT_ProjectTriggerHandler.afterInsert(Trigger.new);
        }
        else if(Trigger.isbefore && trigger.isInsert) {
            PRT_ProjectTriggerHandler.beforeInsert(Trigger.new);
        }
    }
}
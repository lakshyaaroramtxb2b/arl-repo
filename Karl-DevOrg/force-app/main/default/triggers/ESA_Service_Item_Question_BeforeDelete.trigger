trigger ESA_Service_Item_Question_BeforeDelete on ESA_Service_Item_Question__c (before delete) {
    ESASIRuleHandler.validateSIQuestionUsageInRules(Trigger.old);
}
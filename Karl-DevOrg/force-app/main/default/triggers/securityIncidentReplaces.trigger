trigger securityIncidentReplaces on Security_Incident__c (before update) {
	
    Security_Incident_Config__c cfg = [select Documents_Folder_ID__c, Trust_Recent_Phishing_URL__c, Instance_Name__c from Security_Incident_Config__c where Active__c = True limit 1];
	
    for (Security_Incident__c newSI : Trigger.new) {
        if (newSI.Security_Approval_Status__c == 'Closed' && newSI.Replaces__c != null) {
            Security_Incident__c oldSI = [select Replaced_By__c from Security_Incident__c where id =:newSI.Replaces__c limit 1];
            if (oldSI.Replaced_By__c == null) {
                oldSI.Replaced_By__c = newSI.id;
                oldSI.Replaced_ByURL__c = newSI.Name + ' (https://' + cfg.Instance_Name__c + '.salesforce.com/' + newSI.id + ')';
                update oldSI;
            }
        }
    }
}
trigger CycleRequestItemTrigger on Cycle_Request_Item__c ( Before Update, After Update , After Insert, Before Insert) {
    ARL_CycleRequestItemTriggerHelper.run();
}
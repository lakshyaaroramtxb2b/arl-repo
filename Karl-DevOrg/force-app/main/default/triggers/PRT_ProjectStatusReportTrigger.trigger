/*
* Created by - Prashant Gupta
* Date - 19-09-2019
* Project_Status_Report__c Trigger
*/
trigger PRT_ProjectStatusReportTrigger on Project_Status_Report__c (before insert, before update, after Insert, after update) {      
    
    if(!PRT_Constants.INTEGRATION_SETTING.Disable_Project_Status_Report_Trigger__c  ){
    if(Trigger.isbefore && trigger.isInsert) {
        PRT_ProjectStatusReportTriggerHandler.beforeInsert(Trigger.new);
    }
    if(Trigger.isbefore && trigger.isupdate) {
        PRT_ProjectStatusReportTriggerHandler.beforeUpdate(Trigger.new, Trigger.oldMap);
    }
    if(Trigger.isafter && trigger.isInsert) {
        PRT_ProjectStatusReportTriggerHandler.afterInsert(Trigger.new);
    }
    if(Trigger.isafter && trigger.isupdate) {
        PRT_ProjectStatusReportTriggerHandler.afterUpdate(Trigger.new, Trigger.oldMap);
    }
    }
}
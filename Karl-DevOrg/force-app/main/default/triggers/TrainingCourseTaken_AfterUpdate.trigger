/*
Developer: Ralph Callaway <ralph@callaway.cloud>
Description:
    Place all OBJECT OPERATION trigger actions in this file to provide
    to allow for easy deduction of execution order
*/
trigger TrainingCourseTaken_AfterUpdate on Training_Course_Taken__c (after update) {

    SC_TrainingCourseTakenModuleManager.rollupModuleProgress(Trigger.new);
    if (SC_AlternateEnrollmentLinker.hasAlternateMismatch(Trigger.new)) { SC_AlternateEnrollmentLinker.run(); }

}
trigger CDI_DataGovernancehasOnlyoneRecord on CDI_Governance_Limit__c (before insert) {
List<CDI_Governance_Limit__c> limitrecord=[select id from CDI_Governance_Limit__c];
for(CDI_Governance_Limit__c cgt:Trigger.new)
{
    if(limitrecord.size()+1>1)
    {
        cgt.addError('Only One record has Permission For This Record');
    }
}

}
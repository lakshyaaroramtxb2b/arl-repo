trigger ESA_ApprovalInfo_BeforeInsert on ESA_Approval_Information__c (before insert) {
    
    ESA_ApprovalInfoValidation.infoValidation(trigger.new);

}
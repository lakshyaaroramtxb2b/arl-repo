trigger FieldExtractionTrigger on Detection_Field_Extraction__c (before update, after insert)
{    
    //Check that regex vars have DD field
    for( Detection_Field_Extraction__c dfeId : Trigger.new )
    {
        boolean hasChanged = false;
        
        //check that Log Identifier String is unique
        //
        String logIdString = [SELECT Log_Identifier_Regex__c FROM Detection_Field_Extraction__c WHERE Id =: dfeId.ID LIMIT 1][0].Log_Identifier_Regex__c;
        List<Detection_Field_Extraction__c> recLen = [SELECT Id FROM Detection_Field_Extraction__c WHERE Log_Identifier_Regex__c =: logIdString];
        
        //CHECK NOW DONE IN DetectionFieldExtractionTrigger
        /*if(recLen.size() > 1) {
            trigger.new[0].Log_Identifier_Regex__c.addError('Duplicate Log Identifier String');
        }*/
        
        if(Trigger.IsUpdate) {
            Detection_Field_Extraction__c oldDfeId = Trigger.oldMap.get(dfeId.Id);
            
            if(dfeId.Field_Extraction_Regex__c != oldDfeId.Field_Extraction_Regex__c)
            {
         		hasChanged = true;
            }
        }

        //Check that regex has changed
        if((hasChanged || Trigger.IsInsert) && !String.isBlank(dfeId.Field_Extraction_Regex__c))
        {
            System.debug('FieldExtractionTrigger End');
            
            //benchmark regex
            RegexChecker.benchmarkRegex(dfeId.Id);
            
            if(!Trigger.IsInsert) {
                dfeId.Regex_Benchmark_Match__c = 'updating...';
                dfeId.Regex_Benchmark_Non_Match__c = 'updating...';
            }
            
            Pattern regexPattern = Pattern.compile('\\?<(.*?)>');
            Matcher regexMatcher = regexPattern.matcher(dfeId.Field_Extraction_Regex__c);
            
            Set<String> regexVars = new Set<String>();
            List<Detection_Data_Dictionary_Mappings__c> ddJun = new  List<Detection_Data_Dictionary_Mappings__c>();
            
            while (regexMatcher.find()) 
            {
                String regexVar = regexMatcher.group().replace('?<', '').replace('>', '');
                
                //dont allow duplicate regex vars
                //CHECK NOW DONE IN DetectionFieldExtractionTrigger
                /*if(regexVars.contains(regexVar))
                {
                    trigger.new[0].Field_Extraction_Regex__c.addError('Duplicate Data Dictionary Field \"' + regexVar + '\".');
                    return;
                }*/
                
                //check for DD fields
                Detection_Data_Dictionary_Items__c [] ddMatch = [SELECT Id, Name FROM Detection_Data_Dictionary_Items__c WHERE Name =: regexVar];

                if(ddMatch.size() == 0)
                {
                    trigger.new[0].Field_Extraction_Regex__c.addError('No matching Data Dictionary Field \"' + regexVar + '\". Please check the spelling or add a new field.');
                    return;
                }
                
                //case senstive check
                if(!ddMatch[0].Name.Equals(regexVar))
                {
                    trigger.new[0].Field_Extraction_Regex__c.addError('Case appears incorrect. Please check that the case matchs an exsiting data dictionary item.');
                    return;
                }
                
                //create many-to-many relationships between field and data dictionary
                Detection_Data_Dictionary_Mappings__c newJun = new Detection_Data_Dictionary_Mappings__c();
                newJun.Data_Dictionary_Item__c = ddMatch[0].Id;
                newJun.Detection_Field_Extraction__c = dfeId.Id;
                ddJun.add(newJun);
                
                regexVars.add(regexVar);
            }
            
            if(Trigger.IsUpdate)
            {
                String varString = String.join(new List<String>(regexVars), ', ');
                dfeId.Extracted_Fields__c = varString.substring(0, varString.length() < 255 ? varString.length() : 255);
                
                //setup many-to-many relationships
                DELETE [SELECT Id FROM Detection_Data_Dictionary_Mappings__c WHERE Detection_Field_Extraction__c =: dfeId.Id];
            }
            
            if(ddJun.size() > 0)
                INSERT ddJun;
        }
    }
}
trigger ESA_ServiceItem_AfterInsert on ESA_Service_Item__c (After Insert) {
    EsaOHAppValidationsHelper.Esa_ServiceItem_ExternalAssignment_Validation(Trigger.newMap);
}
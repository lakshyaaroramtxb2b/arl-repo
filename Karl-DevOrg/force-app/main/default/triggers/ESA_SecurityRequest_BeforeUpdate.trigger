/*

    Developer Name: Jorge L Caceres - jcaceres@salesforce.com | August 2017 
    
    Description:
        Place all object trigger actions in this class to provide
        easy deduction of execution order
*/

trigger ESA_SecurityRequest_BeforeUpdate on ESA_Security_Request__c (before update) {

    ESA_TriggerHelper.populateEntityCode(trigger.new);
    EsaHandlerIntakeCalendar.CronstructEventURL(trigger.new);
    if(trigger.new[0].Entity_Code__c == 'BUS' ) {
        EsaOHAppValidationsHelper.setApproversForBUS(trigger.new[0]);
    }
    
}
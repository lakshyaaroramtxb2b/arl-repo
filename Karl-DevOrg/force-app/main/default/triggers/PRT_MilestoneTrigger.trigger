/***********************************************************
* Created by 	- Prashant Gupta
* Trigger for Milestone
************************************************************/
trigger PRT_MilestoneTrigger on Milestone_PRT__c (before insert, before update, after update, after insert) {
	
    if(!PRT_Constants.INTEGRATION_SETTING.Disable_Milestone_Trigger__c ){
        if(Trigger.isBefore){
            if(Trigger.isUpdate){
                PRT_MilestoneTriggerHandler.beforeUpdate(Trigger.new, Trigger.oldMap);
            } else if(Trigger.isInsert){
                PRT_MilestoneTriggerHandler.beforeInsert(Trigger.new);            
            }
        }
        if(Trigger.isAfter) {
            if(Trigger.isUpdate){
                System.debug('>>>>>>>>>>IS AFTER');
                PRT_MilestoneTriggerHandler.afterUpdate(Trigger.New, Trigger.oldMap);
            }
            if(Trigger.isInsert){
                System.debug('>>>>>>>>>>IS AFTER INSERT');
                PRT_MilestoneTriggerHandler.afterInsert(Trigger.New, Trigger.oldMap);
            }
        }
    }
}
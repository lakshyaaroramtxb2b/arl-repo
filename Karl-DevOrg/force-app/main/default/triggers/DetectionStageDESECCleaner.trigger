trigger DetectionStageDESECCleaner on Detection_Stage__c (before delete) {
    
    List<DAC_linked_DSEC__c> linksToDelete = new List<DAC_linked_DSEC__c>();
    
    List<Detection_Stage__c> stages = [SELECT Id, DetectionAlertCriteria__c
                                              FROM Detection_Stage__c 
                                              WHERE Id IN: Trigger.old];

    for(Detection_Stage__c stage : stages)
    {
        List<Detection_Comparison__c> comps = [SELECT Id, ValueToCompare__c
                                              FROM Detection_Comparison__c 
                                              WHERE DetectionStage__c =: stage.id];
        
		for(Detection_Comparison__c comp : comps)
        {
            List<DAC_linked_DSEC__c> links = [SELECT Id
                                              FROM DAC_linked_DSEC__c 
                                              WHERE DSEC__c =: comp.ValueToCompare__c 
                                              		AND DAC__c =: stage.DetectionAlertCriteria__c];
            
            for(DAC_linked_DSEC__c link : links)
            {
                linksToDelete.add(link);
            }
        }
    }
    
    DELETE linksToDelete;
}
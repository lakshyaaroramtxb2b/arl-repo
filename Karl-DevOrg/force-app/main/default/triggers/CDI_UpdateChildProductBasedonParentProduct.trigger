trigger CDI_UpdateChildProductBasedonParentProduct on CDI_Product__c (after update) {
    //List of Child Products
    List<CDI_Product__c> listproducts = new List<CDI_Product__c>();
    Map<Id,Set<Boolean>> parentIdChildBooleanMap=new Map<Id,Set<Boolean>>();
    Map<Id,Set<Id>> parentIdChildIdMap=new Map<Id,Set<Id>>();
    Set<Id> parentproductids= trigger.newMap.keySet();
    Map<Id,CDI_Product__c> productrecords= new Map<Id,CDI_Product__c>([Select Id,IsDeleted__c,Name,Parent_Product__c,Parent_Product__r.Name,Parent_Product__r.Product_Full_Name__c ,Product_Full_Name__c from CDI_Product__c Where Parent_Product__c IN : parentproductids]);
    
        for(Id key:productrecords.keySet())
        {  
            CDI_Product__c product=productrecords.get(key);
            System.debug('name '+ product.Name);
            System.debug(product.Parent_Product__r.Product_Full_Name__c + '>' + product.Name);
            product.Product_Full_Name__c=product.Parent_Product__r.Product_Full_Name__c + '>' + product.Name;
            product.CDI_DB_External_Id__c=product.Name;
            listproducts.add(product);
            Set<Boolean> childBooleanValues=parentIdChildBooleanMap.get(product.Parent_Product__c);
            system.debug('bool'+parentIdChildBooleanMap.get(product.Parent_Product__c));
            if(childBooleanValues==null)
            {
                childBooleanValues=new Set<Boolean>();
            }
            childBooleanValues.add(product.IsDeleted__c);
            parentIdChildBooleanMap.put(product.Parent_Product__c,childBooleanValues);
            Set<Id> childIds=parentIdChildIdMap.get(product.Parent_Product__c);
            if(childIds==null)
            {
                childIds=new Set<Id>();
            }
            childIds.add(product.Id);
            parentIdChildIdMap.put(product.Parent_Product__c,childIds);
        }
    
    
    for ( CDI_Product__c product : trigger.new ) {
        if(parentIdChildBooleanMap.get(product.Id) !=null && product.IsDeleted__c == true && parentIdChildBooleanMap.get(product.Id).contains(false))
        {
            product.addError('There are active Child Records, mark IsDeleted field on the child records and then mark the parent record as IsDeleted');
        }
        if(product.Parent_Product__c !=null &&parentIdChildIdMap.get(product.Parent_Product__c)!=null && parentIdChildIdMap.get(product.Parent_Product__c).contains((product.Parent_Product__c)))
        {
            product.addError('The selected Parent Product record already exists as a child. You cannot add it as Parent.');
        }
    }
    if(listproducts.size()>0)
    {
        try
        {
            update listproducts;
        }
        catch(Exception e)
        {
            e.getStackTraceString();
        }
    }
}
trigger INTEG_Employee_BeforeUpdate on INTEG_Employee__c (before update) {

    INTEG_EmployeeHelper.hackLastWorkDay(trigger.new, trigger.oldMap);
    INTEG_EmployeeHelper.populateManagerName(trigger.new, trigger.oldMap);

}
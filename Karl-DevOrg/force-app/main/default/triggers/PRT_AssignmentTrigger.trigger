/***********************************************************
* Created by 	- Harinath Ch
* Date 		- 06th August 2019
* Trigger for Assignment
************************************************************/
trigger PRT_AssignmentTrigger on Assignment__c (before insert, before update, after insert, after update) {
    
    
    if(!PRT_Constants.INTEGRATION_SETTING.Disable_Assignment_Trigger__c  ){
    if(Trigger.isBefore && trigger.isInsert) {
        IF(PRT_Constants.RECURSIVE_TRIGGER_CHECK){
            PRT_Constants.RECURSIVE_TRIGGER_CHECK =FALSE;
            PRT_AssignmentTriggerHandler.beforeInsert(Trigger.new);
             //PRT_AssignmentTriggerHandler.beforeUpdate(Trigger.new, Trigger.oldMap);
        }
    }
    if(Trigger.isAfter && trigger.isInsert){
        PRT_AssignmentTriggerHandler.afterInsert(Trigger.new);
    }
    
    if(Trigger.isBefore && trigger.isUpdate){
        PRT_AssignmentTriggerHandler.beforeUpdate(Trigger.new, Trigger.oldMap);
    }
    if(Trigger.isAfter && trigger.isUpdate){
        PRT_AssignmentTriggerHandler.afterUpdate(Trigger.new, Trigger.oldMap);
    }
    }
}
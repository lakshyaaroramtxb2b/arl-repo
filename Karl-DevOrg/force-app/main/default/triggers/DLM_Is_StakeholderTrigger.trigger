trigger DLM_Is_StakeholderTrigger on IS_Stakeholder__c (before insert , before update, after insert, after update) {
    
    if(Trigger.isBefore && Trigger.isInsert){
         DLM_Is_StakeholderTriggerHandler.beforInsert(Trigger.new);
    }
    if(Trigger.isBefore && Trigger.isUpdate){ 
        DLM_Is_StakeholderTriggerHandler.beforeUpdate(Trigger.new, Trigger.oldMap);
    }
     if(Trigger.isAfter && Trigger.isInsert){ 
        DLM_Is_StakeholderTriggerHandler.afterUpdate(Trigger.new, Trigger.oldMap);
    }
    if(Trigger.isAfter && Trigger.isUpdate){ 
       DLM_Is_StakeholderTriggerHandler.afterUpdate(Trigger.new, Trigger.oldMap);
    } 

}
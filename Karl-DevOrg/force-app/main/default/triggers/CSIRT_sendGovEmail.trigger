trigger CSIRT_sendGovEmail on Case (before update) {
/************************************************
*** This trigger handles the sending of an
*** email to security@ when an email arrives 
*** in security_gov@ during non-Govt CSIRT hours.
*************************************************/
    
Integer startUTC = 01;  //Start of shift (in UTC time) of non-Govt CSIRT
Integer endUTC = 12;    //End of shift (in UTC time) of non-Govt CSIRT

        String ToAddress = 'security@salesforce.com'; 
        String ContactId = '0033000001iy8zh';   // id for 'Salesforce.com Security' contact
    
        List<Case> cases = [ 
            SELECT RecordType.DeveloperName, Status, Email_To__c
            FROM Case
            WHERE Email_To__c = 'security_gov@salesforce.com' AND id IN :trigger.new]; //BM -added  AND id IN :trigger.new because this query was returning ALL cases
    
        //Email Template to security@
        List<Messaging.SingleEmailMessage> govmessages = new List<Messaging.SingleEmailMessage>();
        
       //BM - added a new list because we don't want to run against all cases, only emails before the loop.
       List<case> emailCases = new List<case>();
        for (case c : cases){
            if (c.Status=='New' && c.RecordType.DeveloperName == 'CSIRT_Inbound_Email' ){
                emailCases.add(c);
            }
        }
        for (Case e : emailCases ) {
             {

           
            //Send from the security@email address
            OrgWideEmailAddress emailAddress = [
                                        SELECT Id
                                        FROM OrgWideEmailAddress
                                        WHERE Address = :ToAddress
                                    ];
            //select the correct template to send
        
                EmailTemplate govTemplate = [
                SELECT Id
                FROM EmailTemplate
                WHERE Name = 'CSIRT - Email sent to security_gov' 
                ];    
        
                System.debug('templatename : ' + govTemplate);
        
                 //Add emails to the list of emails to be sent
                 Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
                 message.setOrgWideEmailAddressId(emailAddress.Id);
                 message.setTargetObjectId(ContactId);
                 // message.setToAddresses(govToAddress);
                 message.setTemplateId(govTemplate.Id);
                 govmessages.add(message);
                 }
           
           
            }
            
        if (govmessages.size()>0){ //have to check this to avoid the gov limit: Too many Email Invocations: 1
        Messaging.sendEmail(govmessages); 
        }    
    
}
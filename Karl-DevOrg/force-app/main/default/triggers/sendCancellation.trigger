trigger sendCancellation on Partner_Appointment__c (before delete) {
    OfficeHoursController ohc = new OfficeHoursController();
    for (Partner_Appointment__c pa : trigger.old) {
        ohc.sendCancellation(pa);
    }
}
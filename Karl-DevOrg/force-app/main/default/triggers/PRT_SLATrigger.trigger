trigger PRT_SLATrigger on SLA__c (before insert, before update, after insert, after update) {
    List<SLA__c> newList = new List<SLA__c>();
    List<SLA__c> oldList = new List<SLA__c>();
    Map<Id, SLA__c> newMap = new Map<Id, SLA__c>();
    Map<Id, SLA__c> oldMap = new Map<Id, SLA__c>();

    for(SLA__c slaRecord : Trigger.new) {
        newList.add(slaRecord);
        if(Trigger.newMap!=null && !Trigger.newMap.isEmpty()){
            newMap.put(slaRecord.Id,Trigger.newMap.get(slaRecord.Id)); 
        }
        if(Trigger.oldMap!=null && !Trigger.oldMap.isEmpty()){
            oldMap.put(slaRecord.Id,Trigger.oldMap.get(slaRecord.Id));   
            oldList.add(Trigger.oldMap.get(slaRecord.Id));             
        }
    }  
    
    if(newList != null  && !(newList.isEmpty())){
        if(Trigger.isBefore){
            if(Trigger.isInsert) {
                PRT_SLATriggerHandler.beforeInsert(newList, oldMap);
            }
            if(Trigger.isUpdate) {
                PRT_SLATriggerHandler.beforeUpdate(newList, oldMap);
            }
        }
        if(Trigger.isAfter) {
            if(Trigger.isInsert) {
                PRT_SLATriggerHandler.afterInsert(newList, oldMap);
            }
            if(Trigger.isUpdate) {
                PRT_SLATriggerHandler.afterUpdate(newList, oldMap);
            }
        }
    }
}
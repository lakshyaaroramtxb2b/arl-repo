trigger CSIRT_emailToDetection on Case (before insert) {
    for (Case C : Trigger.new) {
        if (C.SuppliedEmail != NULL) {
            system.debug('SuppliedEmail = ' + C.SuppliedEmail);
            try {
                Detection_Logic__c[] detectionLogic = [SELECT Id from Detection_Logic__c WHERE Email_to_Case_Address__c = :C.SuppliedEmail];
                if (detectionLogic.size() > 0){
                    C.Detection_Logic__c = detectionLogic[0].Id;
                }
            } catch (system.DmlException e) {
                system.debug (e);
            }
        }
    }
}
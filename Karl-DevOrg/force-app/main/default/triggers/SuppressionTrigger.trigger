trigger SuppressionTrigger on Suppression_Rules__c (before update, after insert) {

    List<Suppression_Rules__c> rules = [SELECT Id, Filter__c
                                        FROM Suppression_Rules__c 
                                        WHERE Id IN : Trigger.new];
    
    for( Suppression_Rules__c rule : Trigger.new )
    {
        //check not blank
        if(String.isBlank(rule.Filter__c))
        {
            rule.Filter__c.addError('Filter cannot be bank');
            return;
        }
        
        //check in correct format and matches DD
        for(String logicRule : rule.Filter__c.split('\n'))
        {
            List<String> parts = logicRule.split('=');
            
            if(parts.size() != 2)
            {
                rule.Filter__c.addError('Filter \"' + logicRule + '\" not in correct format missing \"Key=Value\"');
            	return;
            }
            
            //Check if DD item
            Detection_Data_Dictionary_Items__c [] ddMatch = [SELECT Id, Name FROM Detection_Data_Dictionary_Items__c WHERE Name =: parts[0]];

            if(ddMatch.size() == 0)
            {
                rule.Filter__c.addError('Value \"' + parts[0] + '\" is not a valid DataDictionaryItem record');
            	return;
            }
            
            //check length value
            if(String.isBlank(parts[1]))
            {
                rule.Filter__c.addError('Value \"' + parts[1] + '\" cannot be blank');
            	return;
            }
        }
    }
    
    /*
    Detection_Data_Dictionary_Items__c [] ddMatch = [SELECT Id, Name FROM Detection_Data_Dictionary_Items__c WHERE Name =: regexVar];

                if(ddMatch.size() == 0)
                {
                    trigger.new[0].Field_Extraction_Regex__c.addError('No matching Data Dictionary Field \"' + regexVar + '\". Please check the spelling or add a new field.');
                    return;
                }
                
                //case senstive check
                if(!ddMatch[0].Name.Equals(regexVar))
                {
                    trigger.new[0].Field_Extraction_Regex__c.addError('Case appears incorrect. Please check that the case matchs an exsiting data dictionary item.');
                    return;
                }
   */ 
    
}
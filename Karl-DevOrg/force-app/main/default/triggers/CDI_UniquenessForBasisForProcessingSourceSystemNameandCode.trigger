trigger CDI_UniquenessForBasisForProcessingSourceSystemNameandCode on CDI_Basis_For_Processing__c (before insert,before update) {

for(CDI_Basis_For_Processing__c bfp:Trigger.new)
{
If(bfp.Source_System_Name__c !=null && bfp.Source_Basis_For_Processing_Code__c!=null)
    { 
    bfp.Unique_Key_Source_System_and_Code__c=bfp.Source_System_Name__c + ' - ' + bfp.Source_Basis_For_Processing_Code__c;
    }
    If(bfp.Source_System_Name__c !=null && bfp.Source_Basis_For_Processing_Code__c==null)
    {
        bfp.addError('Either Source System Name or Source Basis For Processing Code is missing');
    }
    Else if(bfp.Source_System_Name__c ==null && bfp.Source_Basis_For_Processing_Code__c!=null)
    {
        bfp.addError('Either Source System Name or Source Basis For Processing Code is missing');
    }

}

}
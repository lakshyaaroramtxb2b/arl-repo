trigger IEM_CaseWorkTrigger on Case_Work__c (after insert, after update) {
    if(Trigger.isAfter){
        if(Trigger.isInsert){
            IEM_CaseWorkTriggerHandler.afterInsert(Trigger.New);
        }
        if(Trigger.isUpdate){
            IEM_CaseWorkTriggerHandler.afterUpdate(Trigger.New, Trigger.oldMap);
        }
    }
}
/*
* Created by - Himanshu Shrimali (MTX Group Inc.)
* Created Date - 03/01/2020
*/
trigger SPT_CaseCommentTrigger on CaseComment (after insert) {

    Set<Id> caseIdSet = new Set<Id>();
    Set<Id> sfCaseIdSet = new Set<Id>();
    List<CaseComment> newSFList = new List<CaseComment>();
    List<CaseComment> oldSFList = new List<CaseComment>();
    Map<Id,CaseComment> newSFMap = new Map<Id,CaseComment>();
    Map<Id,CaseComment> oldSFMap = new Map<Id,CaseComment>();
    Map<Id, String> caseIdToRecordTypeMap = new Map<Id, String>();

    for(CaseComment caseComment : Trigger.new) {
        caseIdSet.add(caseComment.ParentId);
    }

    for(Case caseRecord : [SELECT Id, RecordTypeId 
                     FROM Case
                     WHERE Id IN :caseIdSet
                     AND RecordTypeId =: SPT_Constants.SEC_RECORDTYPE_ID]) {
        sfCaseIdSet.add(caseRecord.Id);
    }

    for(CaseComment caseComment : Trigger.new) {
        if(sfCaseIdSet.contains(CaseComment.ParentId)) {
            newSFList.add(caseComment);
            if(Trigger.newMap != null && !Trigger.newMap.isEmpty()) {
                newSFMap.put(caseComment.Id, Trigger.newMap.get(caseComment.Id));
            }
        }
    }

    if(Trigger.isAfter) {
        if(Trigger.isInsert) {
            SPT_CaseCommentTriggerHandler.afterInsert(newSFList);
        }
    }
}
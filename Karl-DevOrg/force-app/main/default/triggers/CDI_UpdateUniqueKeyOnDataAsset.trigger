trigger CDI_UpdateUniqueKeyOnDataAsset on CDI_Data_Asset__c (before insert,before update) {
   
    /*
* Check Child and Parent Data Stores are different
*/
   Map<Id,Id> childIdAndParentDataStoreMap=new Map<Id,Id>();
    //Set of Child Data Asset Ids
    Set<Id> assets=new Set<Id>();
    Map<Decimal,Id> mapLevelAndId=new Map<Decimal,Id>();
    List<CDI_Data_Classification__c> dataclassifications=new List<CDI_Data_Classification__c>([Select Id,Level__c from CDI_Data_Classification__c]);
    for(CDI_Data_Classification__c classification:dataclassifications){
        if(classification.Level__c !=null){
            mapLevelAndId.put(classification.Level__c,classification.Id);
           
        }        
    }  

    
    if(trigger.isInsert){
    for(CDI_Data_Asset__c asset:Trigger.new){
        
        assets.add(asset.Id);
        //System.debug('Trigger.new');
           //System.debug(asset.Data_Classification__c);
       
    }
    }
     if(trigger.isUpdate){
    for(CDI_Data_Asset__c asset:Trigger.new)
    {
        
        assets.add(asset.Id);
        //System.debug('Trigger.new');
           //System.debug(asset.Data_Classification__c);
       
    }}
  	System.debug(Trigger.new.Size());
    System.debug(Trigger.new[0].Data_Classification__c);
    //Select Child Data Assets
  // List<CDI_Data_Asset__c> listdataassets = new List<CDI_Data_Asset__c>([ SELECT Id,Name,Parent_Data_Asset__r.Data_Store__c FROM CDI_Data_Asset__c Where Id IN : assets ]);
 // System.debug('List Assets:'+listdataassets.size());
  
    for(CDI_Data_Asset__c asset:[ SELECT Id,Name,Parent_Data_Asset__r.Data_Store__c FROM CDI_Data_Asset__c Where Id IN : assets ]){
        childIdAndParentDataStoreMap.put(asset.Id,asset.Parent_Data_Asset__r.Data_Store__c);
    }
    //Code to Update Parent Data Classification Based on Child
    Map<Id,Set<Decimal>> mapper= new Map<Id,Set<Decimal>>();
    //Map<Id,CDI_Data_Asset__c> childdataassets=new Map<Id,CDI_Data_Asset__c>([SELECT Data_Classification__r.Level__c,Parent_Data_Asset__c FROM CDI_Data_Asset__c WHERE Parent_Data_Asset__c IN : assets AND Is_Parent__c=false]);
   	//List<CDI_Data_Asset__c> childdataassets = [ SELECT Data_Classification__r.Level__c,Parent_Data_Asset__c FROM CDI_Data_Asset__c WHERE Parent_Data_Asset__c IN : assets AND Is_Parent__c=false ];
   //  List<CDI_Data_Asset__c> childdataassets = [ SELECT Data_Classification__r.Level__c,Parent_Data_Asset__c  FROM CDI_Data_Asset__c WHERE Parent_Data_Asset__c IN : assets AND Is_Parent__c=false ];
    
    //List<CDI_Data_Classification__c> dataclassifications=[Select Id,Level__c from CDI_Data_Classification__c];
   //Map<id,CDI_Data_Classification__c> dataclassifications=new Map<id,CDI_Data_Classification__c>([Select Id,Level__c from CDI_Data_Classification__c]);
    //for(CDI_Data_Classification__c classification:dataclassifications)

   
    //Map Parent Id and Child Classification Values
    //for(CDI_Data_Asset__c childdataasset:childdataassets){
    for(CDI_Data_Asset__c childdataasset:[ SELECT Data_Classification__r.Level__c,Parent_Data_Asset__c FROM CDI_Data_Asset__c WHERE Parent_Data_Asset__c IN : assets AND Is_Parent__c=false]){
        Set<Decimal> childLevels=null;
        if(mapper.get(childdataasset.Parent_Data_Asset__c)!=null)
        {
            childLevels= mapper.get(childdataasset.Parent_Data_Asset__c);
        }
        else
        {
            childLevels= new Set<Decimal>();
        }
        if(childdataasset.Data_Classification__r.Level__c != null)
        {
            childLevels.add(childdataasset.Data_Classification__r.Level__c);
        }
        if(childdataasset.Parent_Data_Asset__c !=null)
        {
            mapper.put(childdataasset.Parent_Data_Asset__c,childLevels);
        }
    }
    for(CDI_Data_Asset__c asset:trigger.new)
    {
        if(mapper.containsKey(asset.Id) && (asset.Data_Asset_Type__c =='Table' ||asset.Data_Asset_Type__c =='File')){
            if(mapper.get(asset.Id) !=null && mapper.get(asset.Id).size()>0)
            {
                List<Decimal> maxValue=new List<Decimal>(mapper.get(asset.Id));
                maxvalue.sort();
                if(asset.Data_Classification__c!=mapLevelAndId.get(maxvalue[maxvalue.size()-1])){
                   asset.addError('The Data Classification of Parent Data Asset should be same as highest Data Classification set on Child Data Asset.') ;
                }
                else{
                    asset.Data_Classification__c=mapLevelAndId.get(maxvalue[maxvalue.size()-1]);}
            }
            else
            {
                asset.Data_Classification__c=null;
            }    
        }

        if(asset.Data_Store__c != childIdAndParentDataStoreMap.get(asset.Id) && childIdAndParentDataStoreMap.get(asset.Id) !=null)
        {
            asset.addError('Child Data Store should be the same as Parent Data Store');
        }
    }
 
    
}
trigger DetectionComparisonTrigger on Detection_Comparison__c (after insert, after update, after delete) {
    
    if(Trigger.IsDelete) {
        
        for( Detection_Comparison__c dcDeleted : Trigger.old )
        {
        Detection_Log_Tagging__c tagObj = new Detection_Log_Tagging__c();
        
        Detection_Security_Event_Criteria__c [] parentEvent = [SELECT Id, Status__c, Implementation_System__c, Event_Type_Technical__c, Security_Event_Type__c, KillChain__c, Event_Rating__c 
                 FROM Detection_Security_Event_Criteria__c 
                 WHERE Id =: dcDeleted.Detection_Security_Event_Criteria__c LIMIT 1];
            
            if(parentEvent.size() > 0) {
    delete [SELECT Id 
                FROM Detection_Log_Tagging__c
                WHERE Detection_Security_Event_Criteria__c =: parentEvent[0].Id];
            }
    }
        
        return;
    }
                
for( Detection_Comparison__c dc : Trigger.new ) {
        
        //Adding a Text post
//FeedItem post = new FeedItem();
//post.ParentId = dc.Id;
//post.Body = 'Trigger processing record';
//insert post;
//

        Detection_Log_Tagging__c tagObj = new Detection_Log_Tagging__c();
        
        Detection_Security_Event_Criteria__c [] parentEvent = [SELECT Id, Status__c, Implementation_System__c, Event_Type_Technical__c, Security_Event_Type__c, KillChain__c, Event_Rating__c 
                 FROM Detection_Security_Event_Criteria__c 
                 WHERE Id =: dc.Detection_Security_Event_Criteria__c LIMIT 1];
        
        //if no match then linking via Detection_Stage__c 
        if(parentEvent.size() <= 0)
return;
        
        //we only create Detection_Comparison__c if 
        //Detection Security Event Criteria is Regex and Spark
        if(parentEvent[0].Event_Type_Technical__c == 'Regex'
            && parentEvent[0].Implementation_System__c == 'Spark'
          ) //&& parentEvent[0].Status__c == 'Production')
        {
            if(Trigger.IsUpdate) {
                
                //Adding a Text post
                FeedItem post = new FeedItem();
                post.ParentId = dc.Id;
                post.Body = dc.ValueToCompare__c;
                insert post;
                //return;
                    
                Detection_Log_Tagging__c [] resultArray = [SELECT Id 
                          FROM Detection_Log_Tagging__c
                          WHERE 
                          (FieldToCompare__c !=: dc.FieldToCompare__c OR
                          ValueToCompare__c !=: dc.ValueToCompare__c OR
                          OperatorToApply__c !=: dc.OperatorToApply__c) AND
                          Detection_Security_Event_Criteria__c =: parentEvent[0].Id LIMIT 1];
                
                if(resultArray.size() > 0)
tagObj = resultArray[0];
                else
                    tagObj = null;
                
                if(tagObj != null) {
                
            tagObj.FieldToCompare__c = dc.FieldToCompare__c;
                tagObj.ValueToCompare__c = dc.ValueToCompare__c;	
                tagObj.OperatorToApply__c = dc.OperatorToApply__c;
                    
                update tagObj;
                }
                
            } else if(Trigger.IsInsert) {
                // new Detection_Comparison__c object
                // create new Detection_Log_Tagging  
                tagObj.Enabled__c = true;
                
                tagObj.Detection_Security_Event_Criteria__c = parentEvent[0].Id;
                
                tagObj.FieldToCompare__c = dc.FieldToCompare__c;
                tagObj.ValueToCompare__c = dc.ValueToCompare__c;
                tagObj.OperatorToApply__c = dc.OperatorToApply__c;
                insert tagObj;
                
                Detection_Log_Tagging_Value__c secEventType = new Detection_Log_Tagging_Value__c();
                secEventType.TagName__c = 'a373A000000PZ8b';
                secEventType.Detection_Log_Tagging__c = tagObj.Id;
                secEventType.tagValue__c = parentEvent[0].Security_Event_Type__c;
                insert secEventType;
                    
                Detection_Log_Tagging_Value__c killChain = new Detection_Log_Tagging_Value__c();
                killChain.TagName__c = 'a373A000000PZ8g';
                killChain.Detection_Log_Tagging__c = tagObj.Id;
                killChain.tagValue__c = parentEvent[0].KillChain__c;
                insert killChain;
                    
                Detection_Log_Tagging_Value__c eventRating = new Detection_Log_Tagging_Value__c();
                eventRating.TagName__c = 'a373A000000PZ8l';
                eventRating.Detection_Log_Tagging__c = tagObj.Id;
                eventRating.tagValue__c = parentEvent[0].Event_Rating__c;
                insert eventRating;
                    
                Detection_Log_Tagging_Value__c eventObjectId = new Detection_Log_Tagging_Value__c();
                eventObjectId.TagName__c = 'a373A000000PZ8q';
                eventObjectId.Detection_Log_Tagging__c = tagObj.Id;
                eventObjectId.tagValue__c = parentEvent[0].Id;
                insert eventObjectId;
            }
        }
    }
}
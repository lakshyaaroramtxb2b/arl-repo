trigger CSAP_Trigger on CSAP_QUEUE__c (before insert) {
    for (CSAP_QUEUE__c workrd : Trigger.new)
    {
        if(workrd.Detection_Logic_Name__c == 'DL-0275' || workrd.Detection_Logic_Name__c == 'DL-0252' || workrd.Detection_Logic_Name__c == 'DL-0244' || workrd.Detection_Logic_Name__c == 'DL-0204' || workrd.Detection_Logic_Name__c == 'DL-0168' || workrd.Detection_Logic_Name__c == 'DL-203' || workrd.Detection_Logic_Name__c == 'DL-0251')         
        {
             String Subject;
             String Record_Type = '01230000001GgU1AAK'; // Customer Security Alert
             String Impacted_Env = 'a1p3000000016MdAAI'; //Salesforce Service
             String DL_ID; 
             String AccountID = '0013000001EtVd9AAF'; //Salesforce.com
             String OwnerID = '00G300000030Spp'; // CSIRT Queue
             String link_to_csap_case = ''; // get the ID of the record to link it together.
             String alert_name ='';
             if(workrd.Detection_Logic_Name__c == 'DL-0275')
             {
                 DL_ID = 'a1330000006rycD';
                 Subject = 'Salesforce Service | Malicious Cookie and Browser combination identified';
                 alert_name = 'Malicious Cookie and Browser combination identified';

             }
             if (workrd.Detection_Logic_Name__c == 'DL-0204')
             {
                 DL_ID = 'a13300000068162';
                 Subject = 'Salesforce Service | Credential Compromise Rule followed by IC activation';
                 alert_name = 'Credential Compromise Rule followed by IC activation';
             }
             if(workrd.Detection_Logic_Name__c == 'DL-0252')
             {
                 DL_ID = 'a133000000684fV';
                 Subject = 'Salesforce Service | Login to eyepaste org';
                 alert_name = 'Login to eyepaste org';
             }
             if (workrd.Detection_Logic_Name__c == 'DL-0251')
             {
                 DL_ID = 'a133000000684It';
                 Subject = 'Salesforce Service | Spoofing of security@salesforce.com from force.com';
                 alert_name = 'Spoofing of security@salesforce.com from force.com';
             }
             if (workrd.Detection_Logic_Name__c == 'DL-0203')
             {
                 DL_ID = 'a1330000006815x';
                 Subject = 'Salesforce Service | Credential Compromise Rule followed by Data Export - Different IP Address';
                 alert_name = 'Credential Compromise Rule followed by Data Export - Different IP Address';
             }  
             if (workrd.Detection_Logic_Name__c == 'DL-0168')
             {
                 DL_ID = 'a13300000067x72';
                 Subject = 'Salesforce Service | Credential Compromise Rule followed by Data Export - Same IP Address';
                 alert_name = 'Credential Compromise Rule followed by Data Export - Same IP Address';
             }
             if (workrd.Detection_Logic_Name__c == 'DL-0244')
             {
                 DL_ID = 'a133000000683TM';
                 Subject = 'Salesforce Service | Login attempt from Global blacklisted IP Address';
                 alert_name = 'Login attempt from Global blacklisted IP Address';
             }
             
            //unique key based on userid,orgid,ip address,.
            List <Case> records = [SELECT Id from case where Detection_Logic__c =:workrd.Detection_Logic__c and OrgID__c =:workrd.OrgID__c and UserName__c =:workrd.UserID__c and Alert_Source_IP__c =:workrd.IP_Address__c];
            Integer i = records.size();
            if(i > 0)
            {
                //do update to existing record, update case status and set to 'Updated'
                Case update_case = [SELECT Id,Status from case where Detection_Logic__c =:workrd.Detection_Logic__c and OrgID__c =:workrd.OrgID__c and UserName__c =:workrd.UserID__c and Alert_Source_IP__c =:workrd.IP_Address__c ];
                if(update_case.Status.contains('Not'))
                {
                // do nothing.
                }
                else if (update_case.Status.contains('False'))
                {
                 // do nothing.
                }
                else
                {
                    update_case.Status = 'Updated';
                    update update_case;
                } 
                //add in chatter post to say activity happened again.
            }
            else
            {
        
            Case c1 = new Case();
                c1.Status = 'New';
                c1.AccountId  = AccountID;
                c1.RecordTypeId = Record_Type;
                c1.Impacted_Environment__c = Impacted_Env;
                c1.OwnerId = OwnerID;
                c1.Subject = Subject;
                c1.Alert_Source_IP__c = workrd.IP_Address__c;
                c1.Description = workrd.Information__c;
                c1.Alert_Name__c = alert_name;
                c1.OrgID__c = workrd.OrgID__c;
                c1.Username__c = workrd.UserID__c;
                c1.Alert_Signature_Name__c = alert_name;
                c1.Detection_Logic__c = DL_ID;
                c1.Console_URL__c = 'https://splunk-web.crz.salesforce.com/en-US/app/search/search?q=search%20index%3D'+workrd.Instance__c+'%20userId%3D'+workrd.UserID__c+'%20organizationId%3D'+workrd.OrgID__c+'&earliest=-24h%40h&latest=now';
            
            insert c1;
           }
        }
    }
}
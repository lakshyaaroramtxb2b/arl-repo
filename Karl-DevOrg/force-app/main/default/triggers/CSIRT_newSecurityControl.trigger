trigger CSIRT_newSecurityControl on Security_Control__c (after insert, after update) {

RecordType rt1 = [SELECT Id FROM RecordType Where SobjectType = 'Environment__c' and Name = 'Parent - Infrastructure (IT)'];
RecordType rt2 = [SELECT Id FROM RecordType Where SobjectType = 'Environment__c' and Name = 'Parent - Infrastructure (Production)'];
RecordType rt3 = [SELECT Id FROM RecordType Where SobjectType = 'Environment__c' and Name = 'Parent - Service'];

for (Security_Control__c sc: trigger.new) {
    // select all existing environment mappings for this security control.
    List<Environment__c> env = new List<Environment__c>(); 
    IF (sc.Environment_Types__c != NULL)
    {
    // select all environments matching the environment type of this security control.
    IF (sc.Environment_Types__c.contains('Parent - Infrastructure (IT)')) {
        env.addAll([SELECT Id, OwnerId, Name FROM Environment__c WHERE RecordTypeId = :rt1.id]);
        }
        
    IF (sc.Environment_Types__c.contains('Parent - Infrastructure (Production)'))  {
        env.addAll([SELECT Id, OwnerId, Name FROM Environment__c WHERE RecordTypeId = :rt2.id]);
         }
        
    IF (sc.Environment_Types__c.contains('Parent - Service')){
        env.addAll([SELECT Id, OwnerId, Name FROM Environment__c WHERE RecordTypeId = :rt3.id]); 
         }
    }
       System.Debug('Number of existing environments: ' + env.size()); 
 
   for(Environment__c e : env){
   // does this environment have a mapping for this security control? If not, create a new SCS record.
       System.Debug('Processing Environment ' + e.Name);
        Security_Control_Status__c temp_scs = new Security_Control_Status__c();
        temp_scs.Environment__c = e.id;
        temp_scs.Security_Control__c = sc.id;
        temp_scs.OwnerId = e.OwnerId;
        System.Debug('temp_scs: ' + temp_scs);

       List<Security_Control_Status__c> scs = [SELECT Environment__c FROM Security_Control_Status__c where Security_Control__c = :sc.id and Environment__c = :e.id];
       if(scs.size()==0) {
           System.Debug('Creating SCS for ' + e.Name);
           insert temp_scs;
   
   }

    }
    
   } 
   
   
   
}
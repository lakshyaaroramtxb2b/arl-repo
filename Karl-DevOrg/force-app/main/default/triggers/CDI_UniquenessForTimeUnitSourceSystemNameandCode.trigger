trigger CDI_UniquenessForTimeUnitSourceSystemNameandCode on CDI_Time_Unit__c (before insert,before update) {

for(CDI_Time_Unit__c tu:Trigger.new)
{
    If(tu.Source_System_Name__c !=null && tu.Source_Time_Unit_Code__c!=null)
    { 
    tu.Unique_Key_Source_System_and_Code__c=tu.Source_System_Name__c + ' - ' + tu.Source_Time_Unit_Code__c;
    }
    If(tu.Source_System_Name__c !=null && tu.Source_Time_Unit_Code__c==null)
    {
        tu.addError('Either Source System Name or Source Time Unit Code is missing');
    }
    Else if(tu.Source_System_Name__c ==null && tu.Source_Time_Unit_Code__c!=null)
    {
        tu.addError('Either Source System Name or Source Time Unit Code is missing');
    }
}

}
trigger KARL_AuditScopeReportTrigger on KARL_Audit_Scope_Reports__c (After update, After insert) {
    KARL_AuditScopeReportTriggerHelper.run();
}
/*
    Developer Name: Jorge L Caceres - jcaceres@salesforce.com | December 2015    

    Description:
        Place all object trigger actions in this class to provide
        easy deduction of execution order
*/

trigger TM_Objective_AfterUpdate on TM_Objective__c (after update) {
	
    TM_ObjectiveValidationRules.validateStatusChange (trigger.new, trigger.oldMap);
	
}
/***********************************************************
* Created by -Swarnima Singh Mandhata
* Date 		 - 02-July-2020
************************************************************/
trigger DLM_IsDocumentTrigger on IS_Document__c (before insert,after insert,after update,before update) {
    
    if(Trigger.isBefore && Trigger.isInsert){
        DLM_IsDocumentTriggerHandler.beforeInsert(Trigger.new);    
    }
    if(Trigger.isAfter && Trigger.isInsert){ 
        DLM_IsDocumentTriggerHandler.afterInsert(Trigger.new);
    }
    if(Trigger.isBefore && Trigger.isUpdate){ 
        DLM_IsDocumentTriggerHandler.beforeUpdate(Trigger.new,Trigger.OldMap);
    }
    if(Trigger.isAfter && Trigger.isUpdate){ 
        DLM_IsDocumentTriggerHandler.afterUpdate(Trigger.new,Trigger.OldMap);
    }    
}
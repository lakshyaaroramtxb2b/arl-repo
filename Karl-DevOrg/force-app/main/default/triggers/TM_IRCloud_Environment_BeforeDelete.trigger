/*
    Developer Name: Jorge L Caceres - jcaceres@salesforce.com | November 2016    

    Description:
        Place all object trigger actions in this calss to provide
        easy deduction of execution order
*/ 
trigger TM_IRCloud_Environment_BeforeDelete on IRCloud__Environment__c (before delete) {    
    TM_preventEnvDeleteIfObjectivesExist.preventEnvDeleteIfObjectivesExist(Trigger.oldMap.keySet());    
}
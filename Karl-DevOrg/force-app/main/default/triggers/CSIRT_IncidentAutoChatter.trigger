//Purpose of this trigger is to create chatter message on incident creation so that Squawker can detect the creation of incident
//and alert users of new incident

trigger CSIRT_IncidentAutoChatter on Case (after insert) {
    
    RecordType incidentType = [SELECT Id FROM RecordType WHERE Name = 'CSIRT Security Incident' LIMIT 1];
    
    List<FeedItem> feedItems = new List<FeedItem>();
    
    if(incidentType != null)
    {
        for(Case c : Trigger.new)
        {
            //incidents only
            if(c.recordTypeId == incidentType.Id)
            {
                FeedItem item = new feedItem();
                item.Title = 'New CSIRT Incident';
                item.ParentId = c.Id;
                item.Type = 'TextPost';
                item.Body = 'New CSIRT Incident : ' + c.Subject + ', ' + c.Description;
                
                feedItems.add(item);
            }
        }
        
        insert feedItems;
    }
}
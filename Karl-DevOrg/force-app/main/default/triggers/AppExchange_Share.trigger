// This is broken.  You can't use Apex managed sharing to give full access to a record.
// http://www.salesforce.com/us/developer/docs/apexcode/Content/apex_bulk_sharing_understanding.htm#sharing_access_levels
trigger AppExchange_Share on Partner_Appointment__c (after insert) {
    Group g = [SELECT Id,Name FROM Group WHERE Name = 'AppExchange' LIMIT 1];
    List<Partner_Appointment__Share> appointmentShares = new List<Partner_Appointment__Share>();
    for (Partner_Appointment__c pa : trigger.new){
        Partner_Appointment__Share paShare = new Partner_Appointment__Share();
        paShare.ParentId = pa.Id;
        paShare.UserOrGroupId = g.id;
        paShare.AccessLevel = 'all';
        paShare.RowCause = Schema.Partner_Appointment__Share.RowCause.AppExchange_Team_Member__c;
        appointmentShares.add(paShare);
    }
    
    Database.SaveResult[] appointmentShareInsertResult = Database.insert(appointmentShares,true);
}
trigger DetectionSecurityEventCriteriaTrigger on Detection_Security_Event_Criteria__c (after update) {
    
    try {
    
        Set<ID> ids = Trigger.newMap.keySet();
        List<FeedItem> feedItems = new List<FeedItem>();
        List<Detection_Log_Tagging_Value__c> events = new List<Detection_Log_Tagging_Value__c>();
     
        Map<Id, Detection_Security_Event_Criteria__c> secEvents 
            = new Map<Id, Detection_Security_Event_Criteria__c>([SELECT Id, 
                                                                                Implementation_System__c, 
                                                                                Event_Type_Technical__c, 
                                                                                Security_Event_Type__c, 
                                                                                KillChain__c, 
                                                                                Event_Rating__c 
                                                                         FROM Detection_Security_Event_Criteria__c 
                                                                         WHERE Id in: ids]);
		
        Detection_Security_Event_Criteria__c [] triggerVals = [SELECT Id, KillChain__c FROM Detection_Security_Event_Criteria__c WHERE Id IN: ids];
                
        for( Detection_Security_Event_Criteria__c dc : triggerVals ) {
                    
    
                    Detection_Log_Tagging__c tagObj = new Detection_Log_Tagging__c();    
                    Detection_Security_Event_Criteria__c secEvent = secEvents.get(dc.Id);
            
                    //we only create Detection_Comparison__c if 
                    //Detection Security Event Criteria is Regex and Spark
                    if(secEvent.Event_Type_Technical__c == 'Regex'
                        && secEvent.Implementation_System__c == 'Spark'
                            ) //&& parentEvent.Status__c == 'Production')
                    {
                        if(Trigger.IsUpdate) {
                            // Update to Detection_Comparison__c object
                            // update Detection_Log_Tagging object
                             
                            tagObj = [SELECT Id 
                                      FROM Detection_Log_Tagging__c
                                      WHERE Detection_Security_Event_Criteria__c =: secEvent.Id];

                            DetectionCloudFutureMethods.cacheRecordUpdate('a373A000000PZ8b', tagObj.Id, secEvent.Security_Event_Type__c);
                            DetectionCloudFutureMethods.cacheRecordUpdate('a373A000000PZ8g', tagObj.Id, secEvent.KillChain__c);
                            DetectionCloudFutureMethods.cacheRecordUpdate('a373A000000PZ8l', tagObj.Id, secEvent.Event_Rating__c);
                            DetectionCloudFutureMethods.cacheRecordUpdate('a373A000000PZ8q', tagObj.Id, secEvent.Id);
                            
                            /*    
                            FeedItem post = new FeedItem();
                			post.ParentId = 'a353A000000HDNv';
                			post.Body = 'FUTURE ' + tagObj.Id;
                			insert post;
							*/
                        }
                    }
                }
    		}
            catch(Exception e) {
            
                FeedItem post = new FeedItem();
                post.ParentId = 'a353A000000HDNv';
                post.Body = 'FUTURE ' + e.getMessage();
                insert post;
                
        	} 
}
trigger KARL_ServiceDeskSLATrigger on KARL_SLA__c (before insert,before update) {
    KARL_ServiceDeskSLATriggerHelper.run();
}
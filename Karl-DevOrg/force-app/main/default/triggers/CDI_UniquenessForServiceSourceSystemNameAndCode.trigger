trigger CDI_UniquenessForServiceSourceSystemNameAndCode on CDI_Service__c (before insert,before update) {
for(CDI_Service__c cs:Trigger.new)
{
If(cs.Source_System_Name__c !=null && cs.Source_Service_Code__c!=null)
    { 
    cs.Unique_key__c=cs.Source_System_Name__c + ' - ' + cs.Source_Service_Code__c;
    }
    If(cs.Source_System_Name__c !=null && cs.Source_Service_Code__c==null)
    {
        cs.addError('Either Source System Name or Source Service Code is missing');
    }
    Else if(cs.Source_System_Name__c ==null && cs.Source_Service_Code__c!=null)
    {
        cs.addError('Either Source System Name or Source Service Code is missing');
    }
    }
}
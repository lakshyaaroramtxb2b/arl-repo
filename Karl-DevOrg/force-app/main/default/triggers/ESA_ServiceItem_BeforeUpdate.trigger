trigger ESA_ServiceItem_BeforeUpdate on ESA_Service_Item__c (Before Update) {
  
    EsaOHAppValidationsHelper.Esa_ServiceItem_ExternalAssignment_Required_Validation(Trigger.newMap,Trigger.new,false);
}
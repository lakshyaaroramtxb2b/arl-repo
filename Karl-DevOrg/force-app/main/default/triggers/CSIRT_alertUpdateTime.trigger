trigger CSIRT_alertUpdateTime on Case (before insert, before update) {
    RecordType rt = [SELECT Id FROM RecordType WHERE DeveloperName='CSIRT_Alert' and SobjectType='Case'];
    for (Case c : trigger.new ) {
    
        // Check that this is the CSIRT_Security_Incident type
        if (c.RecordTypeId == rt.Id) {
            // Get the old version of this Case so we can see what field changed
            Case oldCase = null;
            if (Trigger.oldMap != null) {
                // apparently Trigger.oldMap doesn't exist for inserts
                oldCase = Trigger.oldMap.get(c.Id);
            }
            if ((c.Status == 'In-Progress') && ((Trigger.isInsert == true) || (oldCase.Status != 'In-Progress'))) {
                // In Progress change. This only gets set once in the life of the Case
                if (c.Alert_In_Progress_Time__c == null) {
                    // Only update Investment_Time__c if it doesn't have a value
                    c.Alert_In_Progress_Time__c = Datetime.now();
                }
            } 
        }
    }
}
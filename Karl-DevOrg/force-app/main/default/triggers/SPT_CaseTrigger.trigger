/*
* Created By - Himanshu Shrimali (MTX Group Inc.)
* Created Date - 07-01-2020
* SPT Case Trigger, Handles the insert and update of supportforce case records
*/
trigger SPT_CaseTrigger on Case (before insert, before update, after insert, after update) {
    
    List<Case> newSFList = new List<Case>();
    List<Case> oldSFList = new List<Case>();
    Map<id,Case> newSFMap = new Map<id,Case>();
    Map<id,Case> oldSFMap = new Map<id,Case>();

    // Iterates over case records
    for(Case caseRec : Trigger.new){
        // Below condition helps in taking only Security Case records in consideration  
        if(caseRec.RecordTypeId == SPT_Constants.SEC_RECORDTYPE_ID){
            newSFList.add(caseRec);
            if(Trigger.newMap!=null && !Trigger.newMap.isEmpty()){
                newSFMap.put(caseRec.id,Trigger.newMap.get(caseRec.id)); 
            }
            if(Trigger.oldMap!=null && !Trigger.oldMap.isEmpty()){
                oldSFMap.put(caseRec.id,Trigger.oldMap.get(caseRec.id));   
                oldSFList.add(Trigger.oldMap.get(caseRec.id));             
            }         
        }
    }

    if(newSFList != null  && !(newSFList.isEmpty())){
        if(Trigger.isAfter){
            // AfterInsert Functionality
            if(Trigger.isInsert){
                SPT_CaseTriggerHandler.afterInsert(newSFList);
            }
            // After Update Functionality
            else if(Trigger.isUpdate){
                SPT_CaseTriggerHandler.afterUpdate(newSFList, oldSFMap);
            }
        }else if(Trigger.isBefore){
            // Before Insert Functionality
            if(Trigger.isInsert){
                SPT_CaseTriggerHandler.beforeInsert(newSFList);
            }
            // Before Update Functionality
            else if(Trigger.isUpdate){
                SPT_CaseTriggerHandler.beforeUpdate(newSFList, oldSFMap);
            }
        }
    }
}
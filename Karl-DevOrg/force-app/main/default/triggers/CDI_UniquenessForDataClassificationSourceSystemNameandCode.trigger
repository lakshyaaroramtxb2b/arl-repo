trigger CDI_UniquenessForDataClassificationSourceSystemNameandCode on CDI_Data_Classification__c (before insert,before update) {

for(CDI_Data_Classification__c ds:Trigger.new)
{
If(ds.Source_System_Name__c !=null && ds.Source_Data_Classification_Code__c!=null)
    { 
    ds.Unique_Key_Source_System_and_Code__c=ds.Source_System_Name__c + ' - ' + ds.Source_Data_Classification_Code__c;
    }
    If(ds.Source_System_Name__c !=null && ds.Source_Data_Classification_Code__c==null)
    {
        ds.addError('Either Source System Name or Source Data Classification Code is missing');
    }
    Else if(ds.Source_System_Name__c ==null && ds.Source_Data_Classification_Code__c!=null)
    {
        ds.addError('Either Source System Name or Source Data Classification Code is missing');
    }
}

}
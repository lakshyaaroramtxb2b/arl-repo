trigger CDI_UniquenessForDataUsePurposeSourceSystemNameandCode on CDI_Data_Use_Purpose__c (before insert,before update) {

for(CDI_Data_Use_Purpose__c dup:Trigger.new)
{
    If(dup.Source_System_Name__c !=null && dup.Source_Data_Use_Purpose_Code__c!=null)
    { 
    dup.Unique_Key_Source_System_and_Code__c=dup.Source_System_Name__c + ' - ' + dup.Source_Data_Use_Purpose_Code__c;
    }
    If(dup.Source_System_Name__c !=null && dup.Source_Data_Use_Purpose_Code__c==null)
    {
        dup.addError('Either Source System Name or Source Data Asset Use Purpose Code is missing');
    }
    Else if(dup.Source_System_Name__c ==null && dup.Source_Data_Use_Purpose_Code__c!=null)
    {
        dup.addError('Either Source System Name or Source Data Asset Use Purpose Code is missing');
    }
}

}
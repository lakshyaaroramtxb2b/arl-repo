//Trigger to populate employee and Allorgs lookup fields in guests_sfdc_admins__c
trigger SetGuestsLookup on guests_sfdc_admins__c(before insert, before update) {
    
    Set<String> emails = new set<String>();
    Set<String> orgIDs = new Set<String>();
    //SFDC orgIds are case sensitive so are map keys
    for (guests_sfdc_admins__c usr : Trigger.new) {
      emails.add(usr.email__c);
      orgIDs.add(usr.organization_id__c);
    }
    
    List<INTEG_Employee__c> emplist = [SELECT Id,INTEG_Email__c FROM INTEG_Employee__c WHERE INTEG_Email__c=:emails];
    Map<String,Id> mapEmployees = new Map<String,Id>();
    
    for(INTEG_Employee__c emp : emplist) {
      mapEmployees.put(emp.INTEG_Email__c,emp.Id);
    }
    
    /*for (guests_sfdc_admins__c usr : Trigger.new) {
      usr.Employee__c = mapEmployees.get(usr.email__c);
    }*/
    
    //populate orgID lookup
    Map<String,ID> orgsMap = new Map<String,ID>();
    for(guests_all_orgs__c org: [select ID, organization_id__c from guests_all_orgs__c where organization_id__c in :orgIDs]){
        orgsMap.put(org.organization_id__c, org.ID);
    }
    
    for(guests_sfdc_admins__c  org: Trigger.new){
        org.guests_all_orgs__c = orgsMap.get(org.organization_id__c);
        org.Employee__c = mapEmployees.get(org.email__c);
    }
}
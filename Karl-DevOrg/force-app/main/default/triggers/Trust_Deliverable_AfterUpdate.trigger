trigger Trust_Deliverable_AfterUpdate on Trust_Deliverable__c (after update) {
	    
    /* 
	Feed Tracked changes do not trigger a FeedItem trigger (another weirdness)
	This should've been in a FeedItem trigger.    
	Retrieve the related FeedItem records and email stake holders
    */
    if (TD_NotifyStakeholderOfChatterFeeds.firstRun) { 
        TD_NotifyStakeholderOfChatterFeeds.emailStakeHolders(trigger.newMap.keySet()); 
        TD_NotifyStakeholderOfChatterFeeds.firstRun = false;
    }                             

}
trigger CDI_UniquenessForSourceSystemAndCode on CDI_Geo_Area__c (before insert,before update) {
for(CDI_Geo_Area__c cga:Trigger.new)
{
 
 If(cga.Source_System_Name__c !=null && cga.Source_Geo_Area_Code__c!=null)
    { 
    cga.Unique_key__c=cga.Source_System_Name__c + ' - ' + cga.Source_Geo_Area_Code__c;
    }
    If(cga.Source_System_Name__c !=null && cga.Source_Geo_Area_Code__c==null)
    {
        cga.addError('Either Source System Name or Source Geo Area Code is missing');
    }
    Else if(cga.Source_System_Name__c ==null && cga.Source_Geo_Area_Code__c!=null)
    {
        cga.addError('Either Source System Name or Source Geo Area Code is missing');
    }
}
}
trigger CSIRT_envToCase on Case (after insert) {
    for (Case C : Trigger.new) {
        if (C.Email_To__c != NULL) {
            system.debug('Email_To__c = ' + C.Email_To__c);
            try {
                Environment__c[] env = [SELECT Id from Environment__c WHERE Security_Email__c = :C.Email_To__c];
                if (env.size() > 0){
                    C.Impacted_Enviroment__c = env[0].Id;
                }
            } catch (system.DmlException e) {
                system.debug (e);
            }
        update c;
        }
    }
}
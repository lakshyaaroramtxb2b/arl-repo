trigger DetectionAlertCriteriaTriggerBU on Detection_Security_Event_Criteria__c (before update) {

    Integer index = 0;
    
    for( Detection_Security_Event_Criteria__c dsec : Trigger.New )
    {
        //feedpost
        
        
        //ignore the SDC account sdc_service@salesforce.com used for bulk upload
        if(dsec.Status__c == 'Production' && UserInfo.getUserName() != 'sdc_service@salesforce.com.security')
        {
            if(String.isBlank(dsec.Rule_Name__c)) {
                trigger.new[index].Rule_Name__c.addError('Cannot be blank');
            }
            
            if(String.isBlank(dsec.Log_Source__c)) {
                trigger.new[index].Log_Source__c.addError('Cannot be blank');
            }
            
            if(String.isBlank(dsec.Description__c)) {
                trigger.new[index].Description__c.addError('Cannot be blank');
            }
            
            if(String.isBlank(dsec.Analysis_Notes__c)) {
                trigger.new[index].Analysis_Notes__c.addError('Cannot be blank');
            }
            
            if(String.isBlank(dsec.Assumptions__c)) {
                trigger.new[index].Assumptions__c.addError('Cannot be blank');
            }

            if(String.isBlank(dsec.False_Positives__c)) {
                trigger.new[index].False_Positives__c.addError('Cannot be blank');
            }
            
            if(String.isBlank(dsec.Additional_Resources__c)) {
                trigger.new[index].Additional_Resources__c.addError('Cannot be blank');
            }

            if(String.isBlank(dsec.Source_of_Logic__c)) {
                trigger.new[index].Source_of_Logic__c.addError('Cannot be blank');
            }
            
             
        }
                                                              
       index++;
    }  
}
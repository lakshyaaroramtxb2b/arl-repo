/*

    Developer Name: Jorge L Caceres - jcaceres@salesforce.com | September 2015
        
    Description:
        Place all object trigger actions in this class to provide
        easy deduction of execution order
*/

trigger TM_Placement_AfterInsert on TM_Placement__c (after insert) {

    TM_GenerateConcretes.generateConcretesForPlacement(trigger.new);

}
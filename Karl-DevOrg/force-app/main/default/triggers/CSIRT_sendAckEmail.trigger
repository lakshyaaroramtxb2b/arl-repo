trigger CSIRT_sendAckEmail on Case (after update) {
/********************************************
*** This trigger handles the sending of an
*** acknowledgement email to an employee
*** who sends an email to security@ when a
*** handler takes ownership of a case and 
*** it moves to 'In-Progress'
*********************************************/
public CSIRT_emailHelper clsEmailHelper = new CSIRT_emailHelper();
clsEmailHelper.trgCommentEmail(trigger.new, trigger.old);
}
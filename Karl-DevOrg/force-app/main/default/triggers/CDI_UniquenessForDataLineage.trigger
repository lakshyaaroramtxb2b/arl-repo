trigger CDI_UniquenessForDataLineage on CDI_Data_Lineage__c (before insert,before update) {
for(CDI_Data_Lineage__c dl:Trigger.new)
{
    If(dl.Data_Asset__c == dl.Source_Data_Asset__c)
    {
        dl.addError('Data Asset and Source Data Asset Should not be Same Value');
    }
    dl.Unique_Key__c=dl.Data_Asset__c + ' - ' + dl.Source_Data_Asset__c;
    
    If(dl.Source_System_Name__c !=null && dl.Source_Data_Lineage_Code__c!=null)
    { 
    dl.Unique_Key_Source_System_and_Code__c=dl.Source_System_Name__c + ' - ' + dl.Source_Data_Lineage_Code__c;
    }
    If(dl.Source_System_Name__c !=null && dl.Source_Data_Lineage_Code__c==null)
    {
        dl.addError('Either Source System Name or Source Data Lineage Code is missing');
    }
    Else if(dl.Source_System_Name__c ==null && dl.Source_Data_Lineage_Code__c!=null)
    {
        dl.addError('Either Source System Name or Source Data Lineage Code is missing');
    }
}

}
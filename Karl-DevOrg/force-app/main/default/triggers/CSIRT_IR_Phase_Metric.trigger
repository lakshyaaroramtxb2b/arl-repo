trigger CSIRT_IR_Phase_Metric on Case (before insert, before update) {
   
    for (Case c : trigger.new ) {
        
            // Get the old version of this Case so we can see what field changed
            Case oldCase = null;
            if (Trigger.oldMap != null) {
                // apparently Trigger.oldMap doesn't exist for inserts
                oldCase = Trigger.oldMap.get(c.Id);
            }

           If (c.Status == 'In-Progress' || c.Status == 'Updated' || c.Status == 'New'){
           
               If (c.Incident_Phase__c == 'Detection') {
                    c.Incident_Triage_Start_Time__c = Datetime.now();
            
                }else if (c.Incident_Phase__c == 'Response' && c.Response_Phase_Start_Time__c == null) {
                    // Only update Response_Phase_Start_Time_c if it doesn't have a value
                    c.Response_Phase_Start_Time__c = Datetime.now();
          
                }else if (c.Incident_Phase__c == 'Investigation' && c.Investigation_Phase_Start_Time__c == null) {
                    //Only update Investigation_Phase_Start_Time_c if it doesn't have a value 
                     c.Investigation_Phase_Start_Time__c = Datetime.now();
               
                }else if (c.Incident_Phase__c == 'Containment' && c.Containment_Phase_Start_Time__c == null) {
                    //Only update Containment_Phase_Start_Time_c if it doesn't have a value 
                     c.Containment_Phase_Start_Time__c = Datetime.now();
                     
             
            }
            } else c.Incident_Phase__c = 'Close';
                }
            }
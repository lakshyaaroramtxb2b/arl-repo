trigger CSIRT_newEnvAddSecurityControls on Environment__c (after insert) {

RecordType rt1 = [SELECT Id FROM RecordType Where SobjectType = 'Environment__c' and Name = 'Parent - Infrastructure (IT)'];
RecordType rt2 = [SELECT Id FROM RecordType Where SobjectType = 'Environment__c' and Name = 'Parent - Infrastructure (Production)'];
RecordType rt3 = [SELECT Id FROM RecordType Where SobjectType = 'Environment__c' and Name = 'Parent - Service'];
RecordType rt4 = [SELECT Id FROM RecordType Where SobjectType = 'Environment__c' and Name = 'Parent - Infrastructure (Dev/QE)'];

for (Environment__c e: trigger.new) {
    // select all existing environment mappings for this security control.
    List<Security_Control__c> scs = new List<Security_Control__c>(); 
    
    // select all environments matching the environment type of this security control.
    
    IF (e.RecordTypeId == rt1.id) {
        scs.addAll([SELECT Id, Name FROM Security_Control__c WHERE Environment_Types__c INCLUDES ('Parent - Infrastructure (IT)')]);
        }
        
    IF (e.RecordTypeId == rt2.id) {
        scs.addAll([SELECT Id, Name FROM Security_Control__c WHERE Environment_Types__c INCLUDES ('Parent - Infrastructure (Production)')]);
        }

    IF (e.RecordTypeId == rt3.id) {
        scs.addAll([SELECT Id, Name FROM Security_Control__c WHERE Environment_Types__c INCLUDES ('Parent - Service')]);
        }

    IF (e.RecordTypeId == rt4.id) {
        scs.addAll([SELECT Id, Name FROM Security_Control__c WHERE Environment_Types__c INCLUDES ('Parent - Infrastructure (Dev/QE)')]);
        }
    
       System.Debug('Number of security controls: ' + scs.size()); 
      
   for(Security_Control__c sc : scs){
   // does this environment have a mapping for this security control? If not, create a new SCS record.
       System.Debug('Processing Security Control ' + sc.Name);
        Security_Control_Status__c temp_scs = new Security_Control_Status__c();
        temp_scs.Environment__c = e.id;
        temp_scs.Security_Control__c = sc.id;
        temp_scs.OwnerId = e.OwnerId;

           System.Debug('Creating SCS for ' + e.Name + ' and Security Control ' + sc.Name);
           insert temp_scs;
   

    }
    
   } 
   

}
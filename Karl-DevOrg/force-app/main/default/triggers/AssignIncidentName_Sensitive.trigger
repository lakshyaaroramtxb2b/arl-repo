trigger AssignIncidentName_Sensitive on Case (after insert) {
    
    Map<String,Id> caseTypes = IRCloud.IRCloud_Utils.getRecordTypeMapForObjectGeneric(Case.SObjectType);
    System.debug(caseTypes);
    Set<Id> incidentTypes = new Set<Id>();
    incidentTypes.add(caseTypes.get('CSIRT_Sensitive_Incident'));
    
    List<Case> cases = [SELECT Id, RecordTypeId, CaseNumber, IRCloud__Incident_Severity__c, IRCloud__Vulnerability_Priority__c  FROM Case WHERE Id IN :Trigger.newMap.keySet()];
    List<Case> casesToUpdate = new List<Case>();
       
    for (Case c : cases){
        
        if (incidentTypes.contains(c.RecordTypeId)){
            String iName = CreateIncidentName.getName(c);
            if (iname == Null){
                return; // Blank Incident Name; rest of this trigger is pointless.
            }
            c.ResponseForce__Incident_Name_New__c = iName;
        }
        casesToUpdate.add(c);
    }
    update casesToUpdate;
}
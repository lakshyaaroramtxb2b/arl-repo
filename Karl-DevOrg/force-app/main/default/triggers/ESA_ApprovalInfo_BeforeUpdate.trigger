trigger ESA_ApprovalInfo_BeforeUpdate on ESA_Approval_Information__c (before update) {
    
    ESA_ApprovalInfoValidation.infoValidation(trigger.new);

}
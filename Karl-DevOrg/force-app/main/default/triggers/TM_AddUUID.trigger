trigger TM_AddUUID on TM_AbstractObjective__c (before insert) {
  for (TM_AbstractObjective__c ao: Trigger.new) {
    if (ao.UUID__c != NULL) {
      continue;
    }
    
    ao.UUID__c = SecureUUID.NewV4();
  }
}
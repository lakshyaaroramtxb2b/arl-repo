/***********************************************************
* Trigger for Program
* Created by 	- Prashant Gupta
* Date - 09-12-2019
************************************************************/

trigger PRT_ProgramTrigger on Program__c (after Insert, after Update, before Insert, Before Update) {
	
    if(!PRT_Constants.INTEGRATION_SETTING.Disable_Program_Trigger__c  ){
        if(Trigger.isbefore && trigger.isUpdate) {
            PRT_ProgramTriggerHandler.beforeUpdate(Trigger.new, trigger.oldMap);
        }
        
        else if(Trigger.isafter && trigger.isUpdate) {
            PRT_ProgramTriggerHandler.afterUpdate(Trigger.new, trigger.oldMap);
            
        }    
        else if(Trigger.isafter && trigger.isInsert) {
            PRT_ProgramTriggerHandler.afterInsert(Trigger.new);
        }
        else if(Trigger.isbefore && trigger.isInsert) {
            PRT_ProgramTriggerHandler.beforeInsert(Trigger.new);
        }
    }
}
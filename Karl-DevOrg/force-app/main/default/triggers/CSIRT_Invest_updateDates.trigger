trigger CSIRT_Invest_updateDates on Case (before insert, before update) {
RecordType rt = [SELECT Id FROM RecordType WHERE DeveloperName='D_R_Investigation' and SobjectType='Case'];
for (Case c : trigger.new ) {
    
        // Check that this is the CSIRT_Security_Incident type
        if (c.RecordTypeId == rt.Id) {
        
            // Get the old version of this Case so we can see what field changed
            Case oldCase = null;
            if (Trigger.oldMap != null) {
                // apparently Trigger.oldMap doesn't exist for inserts
                oldCase = Trigger.oldMap.get(c.Id);
            }

            if ((c.Status == 'In-Progress') && ((Trigger.isInsert == true) || (oldCase.Status != 'In-Progress'))) {
                // In Progress change. This only gets set once in the life of the Case
                if (c.Investigation_Date__c == null) {
                    // Only update Investment_Date__c if it doesn't have a value
                    c.Investigation_Date__c = Datetime.now();
                }
            } else if ((c.Status == 'Resolved') && ((Trigger.isInsert == true) || (oldCase.Status != 'Resolved'))) {
                // Resolved change. This gets updated every time it moves into this state
                // from another state
            
                // Update Containment_Date__c every time it pops into the Resolved state
                c.Containment_Date__c = Datetime.now();
            }
            
            
        }
    }

}
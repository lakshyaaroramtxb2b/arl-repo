trigger ESA_Security_Question_BeforeDelete on ESA_Security_Question__c (before delete) {
   ESASIRuleHandler.validateQuestionUsageInRules(Trigger.old);
}
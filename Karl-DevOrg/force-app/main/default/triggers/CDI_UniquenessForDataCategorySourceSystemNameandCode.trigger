trigger CDI_UniquenessForDataCategorySourceSystemNameandCode on CDI_Data_Category__c (before insert,before update) {

for(CDI_Data_Category__c dc:Trigger.new)
{

        If(dc.Source_System_Name__c !=null && dc.Source_Data_Category_Code__c!=null)
    { 
    dc.Unique_Key_Source_System_and_Code__c=dc.Source_System_Name__c + ' - ' + dc.Source_Data_Category_Code__c;
    }
    If(dc.Source_System_Name__c !=null && dc.Source_Data_Category_Code__c==null)
    {
        dc.addError('Either Source System Name or Source Data Category Code is missing');
    }
    Else if(dc.Source_System_Name__c ==null && dc.Source_Data_Category_Code__c!=null)
    {
        dc.addError('Either Source System Name or Source Data Category Code is missing');
    }
}


}
trigger CDI_NoSameValueSourceandTargetDataStores on CDI_Data_Store_Data_Flow__c (before insert,before update ) {
for(CDI_Data_Store_Data_Flow__c dsdf:Trigger.new)
{
    if(dsdf.Data_Store__c == dsdf.Target_Data_Store__c)
    {
        dsdf.addError('Source and Target Data Stores Should not be Same name');
    }
    If(dsdf.Source_System_Name__c !=null && dsdf.Source_Data_Store_Data_Flow_Code__c!=null)
    { 
    dsdf.Unique_Key_Source_System_and_Code__c=dsdf.Source_System_Name__c + ' - ' + dsdf.Source_Data_Store_Data_Flow_Code__c;
    }
    If(dsdf.Source_System_Name__c !=null && dsdf.Source_Data_Store_Data_Flow_Code__c==null)
    {
        dsdf.addError('Either Source System Name or Source Data Store Data Flow Code is missing');
    }
    Else if(dsdf.Source_System_Name__c ==null && dsdf.Source_Data_Store_Data_Flow_Code__c!=null)
    {
        dsdf.addError('Either Source System Name or Source Data Store Data Flow Code is missing');
    }
}

}
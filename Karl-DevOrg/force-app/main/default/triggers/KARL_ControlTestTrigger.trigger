trigger KARL_ControlTestTrigger on Control_Test__c (Before update, Before insert, After update, After insert) {
    KARL_ControlTestTriggerHelper.run();
}
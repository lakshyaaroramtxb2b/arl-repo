trigger IDStringPrevent on Detection_Identifier_String__c (before delete) {
	
    for(Detection_Identifier_String__c idStr : trigger.old)
    {
        if(idStr.Field_Extraction__c != null)
        {        
            String status = [SELECT Status__c 
                             FROM Detection_Field_Extraction_Mark_two__c  
                             WHERE Id =: idStr.Field_Extraction__c][0].Status__c;
            
            if(status == 'Production')
            {
                idStr.adderror('Identifier String cannot be deleted while the parent Field Extraction has a status of Production');
            }
        }
    }
}
trigger ESA_Service_Item_Rule_Action_BeforeInsert on ESA_Service_Item_Rule_Action__c (before insert) {
   ESASIRuleHandler.setSIRuleActionOrderSequence(Trigger.new);
}
/*
Copyright (c) 2010 salesforce.com, inc.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. The name of the author may not be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, 
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

By: Chris Kemp <ckemp@salesforce.com> and Sandy Jones <sajones@salesforce.com>
    with contributions from John Kucera <jkucera@salesforce.com>
*/

trigger caseSwarmPlusNotification on Case(after insert, after update) {
   List<Id> caseIds = new List<Id>();

   for (Case c: trigger.new){
     caseIds.add(c.Id);
   }//for
   
    if (caseIds.size() > 1) {
        if (!System.isBatch() && !System.isFuture()) {
           swarmHelper.evaluateCaseRulesFuture(caseIds);
        }
    } else {
       List<EntitySubscription> subs = swarmHelper.evaluateCaseRules(caseIds);
       
       if (trigger.isInsert) {
           
       
           // soby - 9/13/2013. The above isn't giving us good values and it seems like it's seeing a lot of pre-existing subscribers
           // We're just going to get all of the subscribers from the DB
           subs = [select SubscriberId, ParentId from EntitySubscription where ParentId in :caseIds];
           
           // bsoby, requested from wwebb on 8/30/2013. Only send notifications to the people following the 
           // case. Look, I don't want to put this here. You don't want me to put this here. Unfortunately,
           // triggers aren't ordered and I need to make sure that the swarm rules have already created the followers
           if ((subs != null) && (subs.size() > 0) ){
               List<Messaging.SingleEmailMessage> mails;
               try {
                   System.debug('Trying to send email for caseId: '+caseIds+' to followers');
                   System.debug(subs);
                   mails = CSIRT_FeedItemEmailer.newCaseFollowersToEmail(caseIds,subs);   
               } catch (Exception e) {
                   System.debug(Logginglevel.ERROR,'Could\'t generate emails after swarming');
                   System.debug(Logginglevel.ERROR,e);
               }
               if ((mails != null) && (mails.size() > 0) ) {
                   try {
                       Messaging.sendEmail(mails,true);
                   } catch (Exception e) {
                       System.debug(Logginglevel.ERROR,'Could\'t send emails after swarming');
                       System.debug(Logginglevel.ERROR,e);
                   }
               }
           }
       }
    }
}
/*

    Developer Name: Jorge L Caceres - jcaceres@salesforce.com | Septermber 2015
    
    Description:
        Place all object trigger actions in this calss to provide
        easy deduction of execution order
*/

trigger TM_Account_AfterInsert on Account (after insert) {
    
    TM_BUsharingUtil.createAccessControlObjects(trigger.new, null);
    TM_GenerateConcretes.generateConcretesForAccount(trigger.new, null);
    
}
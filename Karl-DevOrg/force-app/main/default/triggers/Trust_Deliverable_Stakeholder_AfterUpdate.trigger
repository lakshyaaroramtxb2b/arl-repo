trigger Trust_Deliverable_Stakeholder_AfterUpdate on Trust_Deliverable_Stakeholder__c (after update) {
        
    // add stakeholder to chatterfeed
    TD_StakeholderChatterHelper.addStakeholderFromChatterFeed(trigger.newMap);   

    // update denormilize data 
    TD_StakeholderChatterHelper.updateDenormilizeData(trigger.new);   

}
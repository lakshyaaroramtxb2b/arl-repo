//Part of User conformation POC
//Checks that email template string is valid i.e. referneced feilds are valid etc
trigger DetectionAlertCriteriaEmailTemplateChecker on Detection_Alert_Criteria__c (before update) {
    
    if(Trigger.isUpdate)
    {
        Set<String> ddis = new Set<String>();
        
        for(Detection_Data_Dictionary_Items__c ddi : [SELECT Name FROM Detection_Data_Dictionary_Items__c])
        {
            ddis.add(ddi.Name);
        }
        
        for(Detection_Alert_Criteria__c dac : trigger.new)
        {
            if(!String.isEmpty(dac.User_Verification_Email_Message__c))
            {
                List<String> missingDDIs = new List<String>();
                
                Pattern p = Pattern.compile('##([^#]+)##');
                Matcher m = p.matcher(dac.User_Verification_Email_Message__c);
               
                while (m.find()) {
                    String s = m.group();
                    s = s.replaceAll('##', '');
    
                    if(ddis.contains(s) == false)
                    {
                        missingDDIs.add(s);
                    }
                 }
                
                 if(missingDDIs.size() > 0)
                 {
                     dac.adderror('Email template is invalid. Could not find Detection Data Items matching ' + string.join(missingDDIs,','));
                 }
            }
        }
    }
}
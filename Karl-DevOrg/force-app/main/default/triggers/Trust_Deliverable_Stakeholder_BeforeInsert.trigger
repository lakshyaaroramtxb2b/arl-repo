trigger Trust_Deliverable_Stakeholder_BeforeInsert on Trust_Deliverable_Stakeholder__c (before insert) {
	
	// populate name field
    TD_PopulateStakeholderName.populateName(trigger.new);	

}
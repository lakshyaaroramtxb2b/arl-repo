trigger DetectionSecurityEventPOCTaggingTrigger on Detection_Security_Event__c (after insert, after update) {
    // FYI: THIS IS A POC TRIGGER.
    try {
        List<Detection_Security_Event__c> theseAlerts = trigger.new;
        Boolean flagged = false;
        
        //List<Detection_Security_Event__c> alertsToUpdate = new List<Detection_Security_Event__c>();
        
        for (Detection_Security_Event__c thisAlert : theseAlerts) {
            try {
                if (thisAlert.DetectionSecurityEventCriteria__c == 'a353A000000jH2AQAU' ) {
                    Map<String, Object> unraw = (Map<String, Object>) JSON.deserializeUntyped(thisAlert.fieldmapraw__c);
                    String orgId = (String) unraw.get('ForceOrganizationId');
                    String userId = (String) unraw.get('ForceUserId');
                    String securityAnalyticsScore = (String) unraw.get('SecurityAnalyticsScore');
                    // This is a future call, so this doesn't hold up the connection
                    if (orgId == '00D0O000002Flvd' && userId == '0050O000007DhRI' && flagged == false) {
                        SessionTaggingUtils.tagByMatching(UserInfo.getSessionId(), orgId, userId, 'eu9', '#sessionRoamingScore != null and #sessionRoamingScore > 0 and #sessionRoamingScore <= ' + securityAnalyticsScore);
                        flagged = true;
                    }
                }
                /*else if (thisAlert.DetectionSecurityEventCriteria__c == 'a353A000000jGmg' ) {
                    Map<String, Object> unraw = (Map<String, Object>) JSON.deserializeUntyped(thisAlert.fieldmapraw__c);
                    String IPDestination = (String) unraw.get('IPDestination');
                    String IPSource = (String) unraw.get('IPSource');
                    
                    if(String.isNotBlank(IPDestination) && String.isNotBlank(IPSource))
                    {
                        thisAlert.IP_Destination__c = IPDestination;
                        thisAlert.IP_Source__c = IPSource;
                        
                        alertsToUpdate.add(thisAlert);
                    }
                    else //remove me
                    {
                        thisAlert.IP_Destination__c = 'test';
                        thisAlert.IP_Source__c = 'test';
                        
                        alertsToUpdate.add(thisAlert);
                    } // ===============
                }*/
            }
            catch (Exception ex) {
                // POC for now, ignore any per record failures
            }
        }
        
        //if(alertsToUpdate.size() > 0)
        //	update(alertsToUpdate);
    }
    catch (Exception ex) {
        // POC for now ignore anything here. 
        // either it succeeded or if it failed right now not exactly concerned.
    }
}
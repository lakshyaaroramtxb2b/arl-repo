/**
 * Trust Maturity Model v2.0
 * Oct 2015
 * jcaceres, jchen, jstashewsky
 * 
 * Place all object trigger actions in this class to provide easy deduction of execution order.
*/

trigger TM_Account_BeforeUpdate on Account (before update) {

    TM_enforceAccountRTEnvRTIntegrity.enforceAccountRTEnvRTIntegrity(Trigger.newMap, Trigger.oldMap);

}
//code that pre-processes new alerts created from Orion (historicaly Brask 1/2). The pre-process steps are required to provide various
//functionlity enhancments 1) Alert to DSEC mapping 2) manual and automated supression 3) email verfication 4) AIQ checks and 5) most importantly send the alert to ResponseOrg via the CSIRT_HANDLEDETECTIONALERT.ProcessAlert API call
//See DSE trigger DetectionSecurityEventTrigger for tagging for AIQ, event hash and manual suppression mappings
trigger DetectionAlertTrigger on Detection_Alert__c (after insert) {
    
    try
    {    
        Map<Id,Boolean> aipMap = new Map<Id,Boolean>();
        List<Detection_Alert__c> alertsPendingAIQ = new List<Detection_Alert__c>(); 
        
        Set<Id> AIQDACIds = new Set<Id>();
        List<Detection_Alert_Criteria__c> alertsPendingAIQDAC = new List<Detection_Alert_Criteria__c>();
        
        List<Detection_Alert__c> alertsPendingSupress = new List<Detection_Alert__c>(); 
        List<CSIRT_HANDLEDETECTIONALERT.IntakeAlert> newAlerts = new List<CSIRT_HANDLEDETECTIONALERT.IntakeAlert>(); 
        List<AlertsPendingJudication__c> apjs = new List<AlertsPendingJudication__c>();
        
        List<Detection_Events_Suppressed__c> dess = new List<Detection_Events_Suppressed__c>();
        
        Map<Id, List<Id>> alertEvents = new Map<Id, List<Id>>();
        List<Id> allEvents = new List<Id>();
        
        //VERY IMPORTANT
        //All custom feilds that are processed by this trigger need to be first selected in the SOQL below. Otherwise the trigger will error
        //IF you create a new feild or reference an exisiting feild you MUST check it is referenced in the SOQL below as triggers lazy load custom feilds
        List<Detection_Alert__c> alerts = [SELECT Id, OwnerId, orion_alert_score__c, DetectionAlertCriteria__r.OwnerId, CSIRT_Adjudication__c, AlertVersion__c, json_data__c, DetectionAlertCriteria__r.Requires_User_Verification__c, DetectionAlertCriteria__r.Last_Validated__c, DetectionAlertCriteria__r.Automated_Suppression__c, DetectionAlertCriteria__r.Suppression_Threshold__c, DetectionAlertCriteria__r.Require_FP_Adjudication__c, HiddenDataMap__c, HiddenEventMap__c, AlertInformation__c, Environment__c, Subject__c, Severity__c, Description__c, RecipientTeam__c, CreatedDate, AttackIQ_Alert__c, Suppressed__c, RecipientTeamFormat__c
                                           FROM Detection_Alert__c 
                                           WHERE Id IN : Trigger.new];
        
        //Get simular alerts for automated suppression
        //NOTE!!!! if suppression is over more than 2 days then need to edit the LAST_N_DAYS value
        List<Detection_Alert__c> simularAlert = [SELECT Id, EventHash__c, DetectionAlertCriteria__c, CreatedDate, CSIRT_Adjudication__c
                                                 FROM Detection_Alert__c  
                                                 WHERE //RecipientTeam__c = 'CSIRT' AND
                                                    DetectionAlertCriteria__r.Automated_Suppression__c = 'If duplicates within threshold period'
                                                    AND CreatedDate = LAST_N_DAYS:2];
        
        //used to hold linked DSEC to DA. these events are not linked to the DA object before pre-processing
        List<Detection_Alert_Event_Map__c> evtList = new List<Detection_Alert_Event_Map__c>();
        //Used to populated the realted object supressed alerts so that from the DA the engineer can see supressed alerts on the DA page view
        List<Supressed_Alerts__c> newSupressedAlerts = new List<Supressed_Alerts__c>();
        
        for( Detection_Alert__c detectionAlert : alerts ) {

            list<Id> events = new List<Id>();
            
            //set owner otherwise set to API user by default. DC use owner to makesure team members own the health of the alert
            detectionAlert.OwnerId = detectionAlert.DetectionAlertCriteria__r.OwnerId;
            
            //If the alert is in the V1 alert format. See bottm else for V2 alert format processing
            if(detectionAlert.AlertVersion__c == 1)
            {
                //split out the HiddenDataMap__c that will allow for the below event map processing
                for(String detAlertDataString : detectionAlert.HiddenDataMap__c.split('\\n')) {
                    
                    List<String> dAlert = detAlertDataString.split('\\|\\|');
                    
                    Detection_Alert_Data__c detAlertData = new Detection_Alert_Data__c();
                    
                    detAlertData.Detection_Alert__c = detectionAlert.Id;
                    detAlertData.FieldName__c = dAlert[0];
                    detAlertData.FieldValue__c  = dAlert[1]; 

                    insert detAlertData;
                }
                
                //Build Alert Event Map for V1 (Link events to alert)
                for(String detAlertEventString : detectionAlert.HiddenEventMap__c.split('\\n')) {
                    
                    Detection_Alert_Event_Map__c detAlertEvent = new Detection_Alert_Event_Map__c();
                    
                    detAlertEvent.Detection_Alert__c = detectionAlert.Id;
                    detAlertEvent.Detection_Security_Event__c = detAlertEventString;
                    
                    allEvents.add(detAlertEventString);
                    
                    if(alertEvents.containsKey(detectionAlert.Id)) {
                        List<Id> ids = alertEvents.get(detectionAlert.Id);
                        ids.add(detAlertEventString);
                        alertEvents.put(detectionAlert.Id, ids);
                    }
                    else
                    {
                        alertEvents.put(detectionAlert.Id, new List<Id> { detAlertEventString });
                    }

                    //for AIQ checks. get a list of DSE objects for this DA and then we look for if the AIQ filter flag was set
                    events.add(detAlertEventString); 
                    //bulkified processing i.e. added at end of trigger etc
                    evtList.add(detAlertEvent);
                }   
            } //V2 alert
            else if(detectionAlert.AlertVersion__c == 2)
            {
                if(String.isBlank(detectionAlert.Description__c))
                {
                    detectionAlert.Description__c = detectionAlert.Subject__c;
                }
                
                //Build Alert Event Map for V2 (Link events to alert)
                
                if(String.isNotBlank(detectionAlert.json_data__c))
                {                
                    //map DSE's 
                    Map<String, Object> unraw = (Map<String, Object>) JSON.deserializeUntyped(detectionAlert.json_data__c);
                    Map<String, Object> eventData = (Map<String, Object>) unraw.get('9.summary_relationship');
    
                    for(String k : eventData.keySet())
                    {
                        String dseId = (String)((Map<String, Object>)eventData.get(k)).get('dse');
                        
                        Detection_Alert_Event_Map__c detAlertEvent = new Detection_Alert_Event_Map__c();
                        
                        detAlertEvent.Detection_Alert__c = detectionAlert.Id;
                        detAlertEvent.Detection_Security_Event__c = dseId;
                        
                        allEvents.add(dseId);
                        
                        if(alertEvents.containsKey(detectionAlert.Id)) {
                            List<Id> ids = alertEvents.get(detectionAlert.Id);
                            ids.add(dseId);
                            alertEvents.put(detectionAlert.Id, ids);
                        }
                        else
                        {
                            alertEvents.put(detectionAlert.Id, new List<Id> { dseId });
                        }
                        
                        //for AIQ checks. get a list of DSE objects for this DA and then we look for if the AIQ filter flag was set
                        events.add(dseId);
                        //bulkified processing
                        evtList.add(detAlertEvent);
                    }
                }
            }

            //AIQ Check
            //How it works... The 'AIQ filter' object contains logic that if a DSE object, when created, matches then the DSE is marked as 
            //an AttackIQ_Event__c. When the DA is created we look to see if the DA contains any of these events and if so the DA is marked as an AIQ alert.
            //This is done at the DSE level becouse 1) AIQ logic is defined at the DSE level, not DA and 2) to allow the SecOrg code to stay within governer lmits
            integer count = [SELECT Count() 
                             FROM Detection_Security_Event__c
                             WHERE id IN: events 
                                   AND AttackIQ_Event__c = true];
            
            Boolean suppress = false;
            
            if(count > 0)
            {
                //IS AttackIQ event 
                aipMap.put(detectionAlert.id, true);
                
                //Set to prevent from going to SSRC and to allow the DC team to setup reports e.g. if RecipientTeam__c is AIQ
                detectionAlert.RecipientTeam__c = 'AIQ';
                //allows engineer to see checkbox in page layout for the DA
                detectionAlert.AttackIQ_Alert__c = true;
                //when DA flagged as AIQ we set a timestamp for healthchecks / UI to see the last time AIQ triggered this test
                //we can then see how long it has been since AIQ sucessfuly tested this DA
                detectionAlert.DetectionAlertCriteria__r.Last_Validated__c = DateTime.now();

                //update parent. Bulkified code
                if(!AIQDACIds.contains(detectionAlert.DetectionAlertCriteria__r.Id))
                {
                    AIQDACIds.add(detectionAlert.DetectionAlertCriteria__r.Id);
                    alertsPendingAIQDAC.add(detectionAlert.DetectionAlertCriteria__r);
                }
                            
                //update Checkbox
                alertsPendingAIQ.add(detectionAlert);
                //supress the AIQ alert i.e. no need to send to ResponseOrg as was created by AIQ
                alertsPendingSupress.add(detectionAlert);
            } 
            //end AIQ
     
            //Supress check
            //
            //Events_Matching_Suppression_Rule__c is set in the DSE trigger. DSE events are checekd to see if they match supression logic if so are added to the Events_Matching_Suppression_Rule__c object
            //this is done to maintain record of matching events and to allow functionality to stay within governer limits. DC also wants the ability in the future to
            //remove supressed events from the DA object and this architecture will easily allow for that
            List<Events_Matching_Suppression_Rule__c> emsrs = [SELECT Suppression_Rules__c, Detection_Security_Event__c, Suppression_Rules__r.Id, Suppression_Rules__r.Alert_Status__c 
                                                                 FROM Events_Matching_Suppression_Rule__c
                                                                 WHERE Detection_Security_Event__c  IN: events 
                                                                       AND (Suppression_Rules__r.Detection_Alert_Criteria__c =: detectionAlert.DetectionAlertCriteria__c
                                                                       OR Suppression_Rules__r.Detection_Alert_Criteria__c = null)];
            
            //do we have events within the alert that need to be suppressed
            //================================================================
            if(emsrs.size() > 0)
            {
                //remove from event map
                integer eventCount = 0;
                
                for(Events_Matching_Suppression_Rule__c emsr : emsrs)
                {
                    integer i = 0;
                    while(i < evtList.size())
                    {
                        if(evtList[i].Detection_Alert__c == detectionAlert.Id)
                        {
                            if(evtList[i].Detection_Security_Event__c == emsr.Detection_Security_Event__c)
                            {
                                evtList.remove(i);
                                
                                Detection_Events_Suppressed__c des = new Detection_Events_Suppressed__c();
                                des.Detection_Alert__c = detectionAlert.Id;
                                des.Detection_Security_Event__c  = emsr.Detection_Security_Event__c;
                                des.Suppression_Rule__c = emsr.Suppression_Rules__c;
                                
                                dess.add(des);
                                
                                eventCount++;
                                
                                break;
                            }
                        }
                    }
                }
                
                if(emsrs.size() == eventCount)
                {
                    //alert has no events as all were suppressed so lets suppress the whole alert
                    Supressed_Alerts__c sa = new Supressed_Alerts__c();
                    sa.Detection_Alert__c = detectionAlert.Id;
                    sa.Suppression_Rule__c = emsrs[0].Suppression_Rules__c;
                    sa.Detection_Alert_Criteria__c = detectionAlert.DetectionAlertCriteria__c;
                        
                    detectionAlert.Suppressed__c = true; 
                    
                    if(String.isEmpty(emsrs[0].Suppression_Rules__r.Alert_Status__c))
                        detectionAlert.CSIRT_Adjudication__c = emsrs[0].Suppression_Rules__r.Alert_Status__c;
                    
                    newSupressedAlerts.add(sa);
                
                    suppress = true; 
                }
            }
            //================================= 
 
            //The Meat of the trigger. Create IntakeAlert Objected based on the DA. Sent at end of trigger as code is bulkified
            boolean isGroundhogzAlert = detectionAlert.Description__c == 'Groundhogz DAC : An Alert was generated as expected' || detectionAlert.Description__c == 'Groundhogz DAC for streaming searches: An Alert was generated as expected';

            if(!aipMap.containsKey(detectionAlert.id) || isGroundhogzAlert)
            {
                //Below IF statement .. TODO remove after testing complete for Requires_User_Verification__c
                //part of User verfication POC
                //Do not send alerts to CSIRT if needs user to confirm i.e. suppress the alert
                //The running Scheduled job 'DetectionContactUsersWorker' will pick up these alerts and send
                //emails to the detected user to confirm the activity etc
                //Job started i.e. DetectionContactUsersWorker p = new DetectionContactUsersWorker(); ... system.schedule('Detection Contact Users Worker', CRON_EXP, p);
                if(detectionAlert.DetectionAlertCriteria__r.Requires_User_Verification__c == false)
                {
                    //Make sure alert not suppressed from AIQ checks, Automated supression, email verfication etc
                    if(suppress == false)
                    {
                        //RecipientTeam__c controls which team recivies the alert. Currently STAR and CSIRT are only customers
                        //In future we might add TI and then the filter the RecipientTeam__c == 'TI' here and send using various API calls
                        //Checks if RecipientTeam__c matches and then create IntakeAlert alert for sending to ResponseOrg
                        if((isGroundhogzAlert) || ((detectionAlert.RecipientTeam__c == 'CSIRT' || detectionAlert.RecipientTeam__c == 'CSIRT - Test Queue' || detectionAlert.RecipientTeam__c == 'CSIRT - GIA')
                           && detectionAlert.RecipientTeamFormat__c == 'CSIRTAlert')) { //&& detectionAlert.AttackIQ_Alert__c == false) {
                            
                            CSIRT_HANDLEDETECTIONALERT.IntakeAlert newAlert = new  CSIRT_HANDLEDETECTIONALERT.IntakeAlert();
                            
                            //Note if SSRC change IntakeAlert API then we would need to add new mappings here
                            newAlert.DetectionAlert = detectionAlert.Id;
                            newAlert.Environment = detectionAlert.Environment__c;
                            newAlert.Subject = detectionAlert.Subject__c;
                            newAlert.Description = detectionAlert.AlertInformation__c;
                            newAlert.EventTimestamp = detectionAlert.CreatedDate;
                            newAlert.RecipientTeam = detectionAlert.RecipientTeam__c; 
                            if(isGroundhogzAlert) {
                                newAlert.RecipientTeam = 'Health Monitoring';
                            }
                            newAlert.Severity = detectionAlert.Severity__c; 
                            newAlert.OrionAlertScore = detectionAlert.orion_alert_score__c;  
                            newAlerts.add(newAlert);
                            System.debug('adding a new alert: ' + newAlert.Subject);
                        }
                    }
                    else
                    {
                        //Alert was supressed. For testing atm notify admins for awareness
                        //TODO code should be bulkified
                        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
                        message.toAddresses = new String[] { 'detectioncloud@salesforce.com' };
                        message.subject = 'Alert Suppressed';
                        message.plainTextBody = detectionAlert.Id;
                        Messaging.SingleEmailMessage[] messages =   new List<Messaging.SingleEmailMessage> {message};
                        Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
                    }
                }
                else if(!aipMap.containsKey(detectionAlert.id))
                {
                    //alert requires user verfication
                    //Create new AlertsPendingJudication__c job for processing by the running Scheduled job 'DetectionContactUsersWorker'
                    
                    AlertsPendingJudication__c apj = new AlertsPendingJudication__c();
                    apj.Attempts__c = 0;
                    apj.DA__c = detectionAlert.Id;
                    apj.DAC__c = detectionAlert.DetectionAlertCriteria__c;
                    
                    apjs.add(apj);
                }
            }
        }
        
        //lookup object for DA/DSE used for automated supression checks
        Map<Id, Detection_Security_Event__c> eventDataMap = 
            new Map<Id, Detection_Security_Event__c>([SELECT Id, UniqueHash_Time_Independent__c
                                                      FROM Detection_Security_Event__c
                                                      WHERE Id in: allEvents]);
    
        //AUTOMATED suppression checks
        //calc EventHash__c. Roll up DSE hashes to the DA
        for( Detection_Alert__c detectionAlert : alerts )
        {
            if(detectionAlert != null && alertEvents != null && alertEvents.containsKey(detectionAlert.Id))
            {
                String h = ''; 
                
                for(Id evtId : alertEvents.get(detectionAlert.Id))
                {
                    Detection_Security_Event__c evt = eventDataMap.get(evtId);
                    
                    List<Integer> finalVal = new List<Integer>();
                    
                    if(evt != null)
                    {
                       h += evt.UniqueHash_Time_Independent__c + ',';
                    }
                }
                
                detectionAlert.EventHash__c = h.substring(0, h.length() - 1);
            }
        }
        
        List<FeedItem> debugSuppression = new List<FeedItem>();
        
        //DO FINAL CHECK for suppression here as we have the current alert event hash list
        //if match then we do a removal from the newAlerts list
        for(Detection_Alert__c detectionAlert : alerts)
        {
            //do not suppress attackiq alerts
            if(detectionAlert.AttackIQ_Alert__c == false)
            {
                FeedItem post = new FeedItem();
                post.ParentId = detectionAlert.id;
                
                if(detectionAlert.DetectionAlertCriteria__r.Automated_Suppression__c == 'If duplicates within threshold period')
                {
                    if(detectionAlert.DetectionAlertCriteria__r.Suppression_Threshold__c != null
                        && detectionAlert.EventHash__c != null)
                    {
                        post.Body = 'Checking for Automated suppression';
                        
                        Decimal threshold = detectionAlert.DetectionAlertCriteria__r.Suppression_Threshold__c;
    
                        //if we have single simular past old alert (i.e. hash values match) then suppress the alert
                        for( Detection_Alert__c oldDA : simularAlert )
                        {
                            //post.Body += '\nLooking at past alert ' + oldDA.Id;
                            
                            //we get all past alerts for performance reaons so here we only look at same class e.g. DAC
                            if((oldDA.DetectionAlertCriteria__c == detectionAlert.DetectionAlertCriteria__c)
                               & (oldDA.CSIRT_Adjudication__c == 'False Positive' | !detectionAlert.DetectionAlertCriteria__r.Require_FP_Adjudication__c)) 
                            {
                                //check timeframe
                                Decimal timeDiffHours = (detectionAlert.CreatedDate.getTime() - oldDA.CreatedDate.getTime()) / 1000 / 60 / 60;
                                
                                if(timeDiffHours <= threshold)
                                {
                                    if(oldDA.EventHash__c != null)
                                    {
                                        //The length will be the same but not the order
                                        //if(oldDA.EventHash__c.length() == detectionAlert.EventHash__c.length())
                                        //Want to look at subsets so disabled the exact match check above
                                        {
                                            integer matches = 0;
              
                                            //check for same items
                                            for(String eventId : detectionAlert.EventHash__c.split(','))
                                            {
                                                 for(String oldEventId : oldDA.EventHash__c.split(','))
                                                 {
                                                     if(oldEventId == eventId) {
                                                         matches++;
                                                     }
                                                 }
                                            }
                                            
                                            post.Body += '\nMatches ' + matches + ' of ' + detectionAlert.EventHash__c.split(',').size() + ' all required events in alert ' + oldDA.Id;
                                            
                                            Boolean alertSuppressed = false;
                                            
                                            //have all events been seen in past alert. could have more events but the main question 
                                            //is have we seen a subset ? if so then matches will equal the number of hashvalues i.e. detectionAlert.EventHash__c.length()
                                            //Note if matches is higher that is becouse of duplicate events i.e. hash matches more than once as event occured multiple times
                                            if(matches >= detectionAlert.EventHash__c.split(',').size())
                                            {
                                                detectionAlert.Suppressed__c = true;
                                                alertSuppressed = true;
                                                post.Body += 'Alert suppressed 1-1 match ' + detectionAlert.Id + ' detectionAlert.Id';

                                                //TODO add to suppressed list
                                                //TODO annotate as automated suppression etc
                                                Supressed_Alerts__c sa = new Supressed_Alerts__c();
                                                sa.Detection_Alert__c = detectionAlert.Id;
                                                sa.Suppression_Rule__c = null;
                                                sa.From_Automated_Suppression__c = true;
                                                sa.Automated_Match__c = oldDA.Id;
                                                sa.Detection_Alert_Criteria__c = detectionAlert.DetectionAlertCriteria__c;

                                                newSupressedAlerts.add(sa);

                                                //Suppress alert from CSIRT / ResponseOrg as 1-1 match
                                                //will remove from list sent to CSIRT_HANDLEDETECTIONALERT
                                                for(CSIRT_HANDLEDETECTIONALERT.IntakeAlert ia : newAlerts)
                                                {
                                                    post.Body += '\n ' + ia.detectionAlert + ' ' + detectionAlert.Id;
                                                    
                                                    if(ia.detectionAlert == detectionAlert.Id)
                                                    {
                                                        newAlerts.remove(newAlerts.indexOf(ia));
                                                        break;
                                                    }
                                                }
                                                
                                                //exit parent loop otherwise checks all alerts. we only care about first
                                                if(alertSuppressed)
                                                {
                                                    break;
                                                }
                                            }
                                            else
                                            {
                                                //post.Body += '\nNot suppressing hash values do not match for any previous alerts';
                                            }
                                        }
                                    }
                                } 
                            }
                        }
                    }
                    
                    debugSuppression.add(post);
                }
            }
        }
        //END
        //
        
        //Bulkified object updates
        //=============================
        
        //Any changes to the DA
        UPDATE alerts;
        
        //Add in linked DSE's
        INSERT evtList; 
        
        //Send Alerts to ResponseOrg
        CSIRT_HANDLEDETECTIONALERT.ProcessAlert(newAlerts);
        
        //Alerts pending User confirmation. apjs picked up by the scheduled user confirmation service worker
        INSERT apjs;
        
        //Linked list of supressed alerts. Shown on the DA pageview so that enigneers can see what was supressed (manual supression rule see manaual supression logic rule)
        INSERT dess;
        
        //update AIQ filter. Linked list that shows what DA's were assosaited with AIQ. shown on pagelayout for engineer etc
        UPDATE alertsPendingAIQ;
        UPDATE alertsPendingAIQDAC;
        
        //Update list of alerts supressed for enigneer (automated)
        UPDATE alertsPendingSupress;
        
        //New supressed alerts
        INSERT newSupressedAlerts;
        
        //debug object used for testing
        INSERT debugSuppression;
    }
    catch (Exception ex)
    {
        //notify support team that trigger has failed. will prevent batched jobs from failing and provide visibility
        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
        message.toAddresses = new String[] { 'andyma@salesforce.com', 'brahbarinia@salesforce.com' };
        message.subject = 'Trigger Alert from Security Org';
        message.plainTextBody = ex.getMessage() + ',' + ex.getStackTraceString();
        Messaging.SingleEmailMessage[] messages =   new List<Messaging.SingleEmailMessage> {message};
        Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
    }
}
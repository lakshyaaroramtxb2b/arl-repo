/***********************************************************
* Created by 	- Prashant Gupta
* Trigger for Risk
************************************************************/
trigger PRT_RiskTrigger on Risk__c (before insert, before update) {
	
    if(!PRT_Constants.INTEGRATION_SETTING.Disable_Risk_Trigger__c  ){
        if(Trigger.isBefore){
            if(Trigger.isUpdate){
                PRT_RiskTriggerHandler.beforeUpdate(Trigger.new, Trigger.oldMap);
            }else if(Trigger.isInsert){
                PRT_RiskTriggerHandler.beforeInsert(Trigger.new);            
            }
        }
    }
}
trigger SiteOrgLookup on guests_sfdc_sites__c (before insert, before update) {

    Set<String> orgIDs = new Set<String>();
    //SFDC orgIds are case sensitive so are map keys
    for(guests_sfdc_sites__c  org: Trigger.new){
        orgIDs.add(org.organization_id__c);
    }
    Map<String,ID> orgsMap = new Map<String,ID>();
    for(guests_all_orgs__c org: [select ID, organization_id__c from guests_all_orgs__c where organization_id__c in :orgIDs]){
        orgsMap.put(org.organization_id__c, org.ID);
    }
    for(guests_sfdc_sites__c  org: Trigger.new){
        org.guests_all_orgs__c = orgsMap.get(org.organization_id__c);
    }
}
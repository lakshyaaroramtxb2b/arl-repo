trigger Trust_Deliverable_BeforeDelete on Trust_Deliverable__c (before delete) {
        
    /* 
    Delete calendar events
    */
    system.debug('trigger.oldMap.keySet()' + trigger.oldMap.keySet());
    TD_Deliverable_Form.deleteGoogleCalendarEvents(trigger.oldMap.keySet());
                       
}
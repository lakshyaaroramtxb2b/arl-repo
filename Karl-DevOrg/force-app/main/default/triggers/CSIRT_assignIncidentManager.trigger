trigger CSIRT_assignIncidentManager on Case (before insert) {
    List<RecordType> rts = new List<RecordType>(CSIRT_caseCache.Incident());
    Id incid = rts[0].id;
    for (Case c: trigger.new) {
       
       if (c.RecordTypeId ==  incid) {
       String caseOwnerId = c.OwnerId;
       
       if (caseOwnerId != NULL && caseOwnerId != '00G300000032MFUEA2') {
           c.IncidentManager__c = c.OwnerId;
         }  
       
        }
    }
}
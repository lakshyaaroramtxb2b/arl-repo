trigger DetectionAlertCriteriaTrigger on Detection_Alert_Criteria__c (before update) {

    /*List<Detection_Alert_Criteria__c> dacs = [SELECT Id, AlertStatus__c, Expected_Volume__c , TimePeriod__c, Analysis_Notes__c, Assumptions__c, False_Positives__c, Additional_Resources__c  
                                              FROM Detection_Alert_Criteria__c
                                              WHERE Id IN : Trigger.New];*/
    Integer index = 0;
    
    for( Detection_Alert_Criteria__c dac : Trigger.New )
    {
        if(dac.AlertStatus__c == 'Live')
        {
            if(dac.Expected_Volume__c == null) {
                trigger.new[index].Expected_Volume__c.addError('Cannot be blank');
            }
            
            if(String.isBlank(dac.TimePeriod__c)) {
                trigger.new[index].TimePeriod__c.addError('Cannot be blank');
            }
            
            if(String.isBlank(dac.Assumptions__c)) {
                trigger.new[index].Assumptions__c.addError('Cannot be blank');
            }
            
            if(String.isBlank(dac.False_Positives__c)) {
                trigger.new[index].False_Positives__c.addError('Cannot be blank');
            }
            
            if(String.isBlank(dac.Expected_Volume__c)) {
                trigger.new[index].Expected_Volume__c.addError('Cannot be blank');
            }
            
            if(String.isBlank(dac.Analysis_Notes__c)) {
                trigger.new[index].Analysis_Notes__c.addError('Cannot be blank');
            }

            if(String.isBlank(dac.Additional_Resources__c)) {
                trigger.new[index].Additional_Resources__c.addError('Cannot be blank');
            }
        }
                                                              
       index++;
    }  
}
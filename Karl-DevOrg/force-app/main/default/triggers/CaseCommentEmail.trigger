trigger CaseCommentEmail on CaseComment (after insert) {

    //This is not neccesary see check on line 34 
    //RecordType rt = [SELECT Id FROM RecordType WHERE DeveloperName='CSIRT_Inbound_Email' and SobjectType='Case'];
     
    //only notify when comment is public
    Set<Id> caseIds = new Set<Id>();
    for (CaseComment caseComment : Trigger.new) {
        if (caseComment.IsPublished) 
            caseIds.add(caseComment.ParentId);
    }
    
    //Get record type ID, Contact ID and make sure the contact has an email
    List<Case> cases = [ 
        SELECT ContactId, RecordTypeId, Contact.Email, OwnerId, Email_To__c
        FROM Case
        WHERE Id IN :caseIds AND Contact.Email != ''
    ];
    if (!cases.isEmpty()) {  //Check that there is an actual case selected to avoid a null exception
        Case c = cases.get(0);  //Get info about case
        String record = c.RecordTypeId;  //assign record to the case record type 
        String email = c.Contact.Email;
        String uid = userinfo.getUserId();
        String pid = userinfo.getProfileId();
        String owner = c.OwnerId;
        String ToAddress = c.Email_To__c;
        
        //Select the profile ID for the service account profile
        Profile p = [
        SELECT id
        FROM profile
        WHERE name = 'CSIRT_Services'
        ];
   
        //Select the record type for CSIRT Inbound Email
        List<RecordType> typeList = [
            select Id 
            from RecordType 
            where SobjectType = 'Case' and Name = 'CSIRT Inbound Email'
        ];
    
        if (!typeList.isEmpty()) { //make sure the list has some entry to prevent System.ListException: List index out of bounds: 0
        RecordType r = typeList.get(0); //get the record type id
        String tid = r.Id;  //record type id for CSIRT inbound email
        
        if (record.equals(tid) && (!tid.equals(pId))) {  //Check that the comment will only get made if its on the CSIRT Inbound email record type.
                                                        //Check that the user profile isn't the service account
            //Send from the security@email address
            OrgWideEmailAddress emailAddress = [
                SELECT Id
                FROM OrgWideEmailAddress
                WHERE Address = :ToAddress
            ];
        
                //select the correct templates to send
                EmailTemplate inttemplate = [
                    SELECT Id
                    FROM EmailTemplate
                    WHERE Name = 'CSIRT Case Comment - Internal'
                ];
          
                EmailTemplate exttemplate = [
                    SELECT Id
                    FROM EmailTemplate
                    WHERE Name = 'CSIRT Case Comment - External'
                ];  
                
                if (email.contains('@salesforce.com') || email.contains('@exacttarget.com')) {
               
                    //Create list of emails to be sent
                    List<Messaging.SingleEmailMessage> messages = new List<Messaging.SingleEmailMessage>();
                    for (Case basketCase : cases) {
                        updateCaseOwner(basketCase, userinfo.getUserId());
                        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
                        message.setOrgWideEmailAddressId(emailAddress.Id);
                        message.setTargetObjectId(basketCase.ContactId);
                        message.setWhatId(basketCase.Id);
                        message.setTemplateId(inttemplate.Id);
                        messages.add(message);
                    }
                    Messaging.sendEmail(messages);  //Send email 
                }
                else {
                    List<Messaging.SingleEmailMessage> messages = new List<Messaging.SingleEmailMessage>();
                    for (Case basketCase : cases) {
                        updateCaseOwner(basketCase, userinfo.getUserId());
                        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
                        message.setOrgWideEmailAddressId(emailAddress.Id);
                        message.setTargetObjectId(basketCase.ContactId);
                        message.setWhatId(basketCase.Id);
                        message.setTemplateId(exttemplate.Id);
                        messages.add(message);
                    }
                    Messaging.sendEmail(messages);  //Send email
                }
            }
        }
    
   }
    private void updateCaseOwner(Case c, String newOwnerId){
        //Update the owner Id of the case so that the email sends with the case owner's name.
        c.OwnerId = newOwnerId;
        Update c;     
    }
}
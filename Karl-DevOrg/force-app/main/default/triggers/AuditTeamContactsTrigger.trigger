trigger AuditTeamContactsTrigger on KARL_Audit_Team_Contacts__c (after insert,before delete) {
    KARL_AuditTeamContactTriggerHelper.run();
}
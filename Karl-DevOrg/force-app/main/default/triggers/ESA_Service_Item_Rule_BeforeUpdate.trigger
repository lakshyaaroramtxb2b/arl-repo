trigger ESA_Service_Item_Rule_BeforeUpdate on ESA_Service_Item_Rule__c (before update) {
    ESASIRuleHandler.validateFormula(Trigger.new);
}
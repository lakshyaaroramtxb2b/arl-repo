trigger genCode on Scan_Queue__c (before insert, before update) {

        private String gen_code() {
        Blob b = Crypto.GenerateAESKey(128);
        String h = EncodingUtil.ConvertTohex(b);
        String access_code = h.SubString(0,31); //128 bits of entropy
        return access_code; 
    }
    
    if (Trigger.isInsert) {
        for (Scan_Queue__c obj : Trigger.new) {
            obj.access_code__c = gen_code(); 
        }
    }
    if (Trigger.isUpdate) {
        for (Scan_Queue__c obj : Trigger.new) {
            //don't update code. This is important for security:
            //
            //If a prodsec user changes the code to something low entropy
            //an unauth user would be able to pull data out of the portal
            //even though we only show status info, we still want to 
            //make sure an attacker can't guess the code
            //Therefore the code is either missing (handled in portal) or 
            //high entropy.
            //
            obj.access_code__c = Trigger.oldMap.get(obj.id).access_code__c;
        }
    }
}
trigger ESA_Case_AfterInsert on Case (after insert) {
    ESA_TriggerHelper.manageSForceCaseChatterSubscription(Trigger.new);
}
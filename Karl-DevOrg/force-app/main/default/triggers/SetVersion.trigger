trigger SetVersion on Scan_Info__c (before insert) {
    if (Trigger.isInsert) {
        Id versionId = getVersion().Id;
        
        for (Scan_Info__c obj : Trigger.new) {
            obj.POPCRAB_Version__c = versionId; 
        }
    }
    
    public POPCRAB_Version__c getVersion() {
        return [SELECT Id FROM POPCRAB_Version__c WHERE 
                	Released__c = true AND 
                	Released_On__c <= TODAY
                	ORDER BY Released_On__c DESC LIMIT 1][0];  			
    }
    
}
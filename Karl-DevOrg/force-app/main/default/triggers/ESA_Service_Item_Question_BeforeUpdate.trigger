trigger ESA_Service_Item_Question_BeforeUpdate on ESA_Service_Item_Question__c (before update) {
     EsaOHAppValidationsHelper.createFieldTrackFlag(Trigger.new, Trigger.oldMap);
     EsaOHAppValidationsHelper.createFieldTrack(Trigger.new, Trigger.oldMap);
}
/***********************************************************
* Created by 	- Prashant Gupta
* Trigger for Change Request
************************************************************/
trigger PRT_ChangeRequestTrigger on Change_Request__c (before insert, before update, after insert, after update) {
	
    if(!PRT_Constants.INTEGRATION_SETTING.Disable_Change_Request_Trigger__c  ){
    if(Trigger.isBefore){
        if(Trigger.isUpdate){
            PRT_ChangeRequestTriggerHandler.beforeUpdate(Trigger.new, Trigger.oldMap);
        }else if(Trigger.isInsert){
            PRT_ChangeRequestTriggerHandler.beforeInsert(Trigger.new);            
        }
    }else if(Trigger.isAfter){
        if(Trigger.isUpdate){
            PRT_ChangeRequestTriggerHandler.afterUpdate(Trigger.new, Trigger.oldMap);
        }else if(Trigger.isInsert){
            PRT_ChangeRequestTriggerHandler.afterInsert(Trigger.new);            
        }
    }
    }
}
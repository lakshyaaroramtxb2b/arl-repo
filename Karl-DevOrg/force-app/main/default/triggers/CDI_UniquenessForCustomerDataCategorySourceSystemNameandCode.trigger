trigger CDI_UniquenessForCustomerDataCategorySourceSystemNameandCode on CDI_Customer_Data_Category__c (before insert,before update) {

for(CDI_Customer_Data_Category__c cdc:Trigger.new)
{
    
  If(cdc.Source_System_Name__c !=null && cdc.Source_Customer_Data_Category_Code__c!=null)
    { 
    cdc.Unique_Key_Source_System_and_Code__c=cdc.Source_System_Name__c + ' - ' + cdc.Source_Customer_Data_Category_Code__c;
    }
    If(cdc.Source_System_Name__c !=null && cdc.Source_Customer_Data_Category_Code__c==null)
    {
        cdc.addError('Either Source System Name or Source Customer Data Category Code is missing');
    }
    Else if(cdc.Source_System_Name__c ==null && cdc.Source_Customer_Data_Category_Code__c!=null)
    {
        cdc.addError('Either Source System Name or Source Customer Data Category Code is missing');
    }
}


}
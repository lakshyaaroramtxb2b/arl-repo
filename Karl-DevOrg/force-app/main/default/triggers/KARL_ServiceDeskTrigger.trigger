/**
 * @author Lakshya Arora
 * @email lakshya.arora@mtxb2b.com
 * @description Trigger on KARL_Service_Desk__c
*/
trigger KARL_ServiceDeskTrigger on KARL_Service_Desk__c (before insert,after insert,before update,after update) {
    if(Trigger.isBefore){
        if(Trigger.isInsert){
            KARL_ServiceDeskTriggerHandler.beforeInsertOperations(Trigger.new);
        }
        if(Trigger.isUpdate){
            KARL_ServiceDeskTriggerHandler.beforeUpdateOperations(Trigger.new,Trigger.oldMap);
        }
    }
    if(Trigger.isAfter){
        if(Trigger.isInsert){
            KARL_ServiceDeskTriggerHandler.afterInsertOperations(Trigger.new);
        }
        if(Trigger.isUpdate){
            KARL_ServiceDeskTriggerHandler.afterUpdateOperations(Trigger.new,Trigger.oldMap);
        }
    }
}
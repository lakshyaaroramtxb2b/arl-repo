trigger FeedItem_AfterInsert on FeedItem (after insert) {
    
    // continue only if FeedItem is for Trust Deliverables  
    If (trigger.new[0].ParentId.getSObjectType().getDescribe().getName() ==
        Trust_Deliverable__c.sObjectType.getDescribe().getName())
    {   
        TD_NotifyStakeholderOfChatterFeeds.emailStakeHolders(trigger.new);  
    }
          

}
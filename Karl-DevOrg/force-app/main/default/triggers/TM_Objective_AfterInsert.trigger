/*
    Developer Name: Jorge L Caceres - jcaceres@salesforce.com | December 2015    

    Description:
        Place all object trigger actions in this class to provide
        easy deduction of execution order
*/

trigger TM_Objective_AfterInsert on TM_Objective__c (after insert) {
    
    TM_BUsharingUtil.createObjectiveSharingRules (trigger.new);
    
}
trigger CDI_UniquenessForGusTeamSourceSystemNameandCode on CDI_GUS_Team__c (before insert,before update) {
for(CDI_GUS_Team__c cgt:Trigger.new)
{

If(cgt.Source_System_Name__c !=null && cgt.Source_GUS_Team_Code__c!=null)
    { 
    cgt.Unique_key__c=cgt.Source_System_Name__c + ' - ' + cgt.Source_GUS_Team_Code__c;
    }
    If(cgt.Source_System_Name__c !=null && cgt.Source_GUS_Team_Code__c==null)
    {
        cgt.addError('Either Source System Name or Source Gus Team Code is missing');
    }
    Else if(cgt.Source_System_Name__c ==null && cgt.Source_GUS_Team_Code__c!=null)
    {
        cgt.addError('Either Source System Name or Source Gus Team Code is missing');
    }
  }
}
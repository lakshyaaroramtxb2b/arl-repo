trigger CSIRT_updateDates on Case (before insert, before update) {
   
    for (Case c : trigger.new ) {
        
            // Get the old version of this Case so we can see what field changed
            Case oldCase = null;
            if (Trigger.oldMap != null) {
                // apparently Trigger.oldMap doesn't exist for inserts
                oldCase = Trigger.oldMap.get(c.Id);
            }

            if ((c.Status == 'In-Progress') && ((Trigger.isInsert == true) || (oldCase.Status != 'In-Progress'))) {
                // In Progress change. This only gets set once in the life of the Case
                if (c.Investigation_Date__c == null) {
                    // Only update Investment_Date__c if it doesn't have a value
                    c.Investigation_Date__c = Datetime.now();
                }
             }else if ((c.Status == 'Resolved') && ((Trigger.isInsert == true) || (oldCase.Status != 'Resolved'))) {
                // Resolved change. This gets updated every time it moves into this state
                // from another state 
                // Update Containment_Date__c if it is an incident RT and every time it pops into the Resolved state
                c.Containment_Date__c = Datetime.now();
                }
            }
        }
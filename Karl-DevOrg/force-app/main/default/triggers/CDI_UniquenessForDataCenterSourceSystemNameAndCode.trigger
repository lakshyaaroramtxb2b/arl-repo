trigger CDI_UniquenessForDataCenterSourceSystemNameAndCode on CDI_Data_Center__c (before insert,before update) {
for(CDI_Data_Center__c cdc:Trigger.new)
{
If(cdc.Source_System_Name__c !=null && cdc.Source_Data_Center_Code__c!=null)
    { 
    cdc.Unique_key__c=cdc.Source_System_Name__c + ' - ' + cdc.Source_Data_Center_Code__c;
    }
    If(cdc.Source_System_Name__c !=null && cdc.Source_Data_Center_Code__c==null)
    {
        cdc.addError('Either Source System Name or Source Data Center Code is missing');
    }
    Else if(cdc.Source_System_Name__c ==null && cdc.Source_Data_Center_Code__c!=null)
    {
        cdc.addError('Either Source System Name or Source Data Center Code is missing');
    }
    }
}
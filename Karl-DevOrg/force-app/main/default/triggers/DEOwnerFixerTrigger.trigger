trigger DEOwnerFixerTrigger on Detection_Errors__c (before insert) 
{
	try 
    {
        List<Detection_Errors__c> errors = [SELECT Id, Detection_Alert_Criteria__r.OwnerId, OwnerId
                                            FROM Detection_Errors__c 
                                           	WHERE Id IN : Trigger.new];
        
         for (Detection_Errors__c error : errors) {
             
             error.OwnerId = error.Detection_Alert_Criteria__r.OwnerId;
             
         }
        
        UPDATE errors;
        
    }
    catch (Exception ex) {
        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
        message.toAddresses = new String[] { 'amcneilly@salesforce.com' };
        message.subject = 'DEOwnerFixerTrigger Alert';
        message.plainTextBody = ex.getMessage();
        Messaging.SingleEmailMessage[] messages =   new List<Messaging.SingleEmailMessage> {message};
        Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
    }
}
trigger ControlTrigger on Control__c (Before insert, Before update, After update, After insert) {
    ARL_ControlTriggerHandler.run();
}
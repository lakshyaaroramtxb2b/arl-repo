//Trigger to populate employee lookup field in Heroku_Warden_users__c
trigger EmployeeLookup on Heroku_Warden_users__c (before insert, before update) {
    
    Set<String> emails = new set<String>();
    for (Heroku_Warden_users__c user : Trigger.new) {
      emails.add(user.email__c);
    }
    
    List<INTEG_Employee__c> emplist = [SELECT Id,INTEG_Email__c FROM INTEG_Employee__c WHERE INTEG_Email__c=:emails];
    Map<String,Id> mapEmployees = new Map<String,Id>();
    
    for(INTEG_Employee__c emp : emplist) {
      mapEmployees.put(emp.INTEG_Email__c,emp.Id);
    }
    
    for (Heroku_Warden_users__c user : Trigger.new) {
      user.Employee__c = mapEmployees.get(user.email__c);
    }
}
trigger CDI_UniquenessForCryptoAlgorithmSourceSystemNameandCode on CDI_Crypto_Algorithm__c (before insert,before update) {

for(CDI_Crypto_Algorithm__c ca:Trigger.new)
{

    If(ca.Source_System_Name__c !=null && ca.Source_Crypto_Algorithm_Code__c!=null)
    { 
    ca.Unique_Key_Source_System_and_Code__c=ca.Source_System_Name__c + ' - ' + ca.Source_Crypto_Algorithm_Code__c;
    }
    If(ca.Source_System_Name__c !=null && ca.Source_Crypto_Algorithm_Code__c==null)
    {
        ca.addError('Either Source System Name or Source Crypto Algorithm Code is missing');
    }
    Else if(ca.Source_System_Name__c ==null && ca.Source_Crypto_Algorithm_Code__c!=null)
    {
        ca.addError('Either Source System Name or Source Crypto Algorithm Code is missing');
    }
}

}
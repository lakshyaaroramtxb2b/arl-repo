trigger IEM_MilestoneTrigger on Milestone__c (before Insert, after update) {
    if(Trigger.isInsert && Trigger.isBefore){
        IEM_MilestoneTriggerHandler.beforeInsert(Trigger.New);
    }
    /*if(Trigger.isUpdate && Trigger.isAfter){
        IEM_MilestoneTriggerHandler.afterUpdate(Trigger.New, Trigger.oldMap);
    }*/
}
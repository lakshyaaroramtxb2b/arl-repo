/***********************************************************
* Created by 	- Prashant Gupta
* Trigger for Purchase Order
************************************************************/
trigger PRT_PurchaseOrderTrigger on Purchase_Order__c (before insert, before Update, after insert, after update) {
	
    if(!PRT_Constants.INTEGRATION_SETTING.Disable_Purchase_Order_Trigger__c  ){
    if(Trigger.isBefore){
        if(Trigger.isUpdate){
            PRT_PurchaseOrderTriggerHandler.beforeUpdate(Trigger.new, Trigger.oldMap);
        }else if(Trigger.isInsert){
            PRT_PurchaseOrderTriggerHandler.beforeInsert(Trigger.new);            
        }
    }else if(Trigger.isAfter){
        if(Trigger.isUpdate){
            PRT_PurchaseOrderTriggerHandler.afterUpdate(Trigger.new, Trigger.oldMap);
        }else if(Trigger.isInsert){
            PRT_PurchaseOrderTriggerHandler.afterInsert(Trigger.new);            
        }
    }
    }
}
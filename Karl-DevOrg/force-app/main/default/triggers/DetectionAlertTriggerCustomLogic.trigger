trigger DetectionAlertTriggerCustomLogic on Detection_Alert__c (before insert) {

    //If DetectionAlertCriteria__r.Custom_Logic_Path__c see DetectionAlertTrigger
    List<Detection_Alert__c> alerts = [SELECT Id, OwnerId, DetectionAlertCriteria__r.OwnerId, CSIRT_Adjudication__c, AlertVersion__c, json_data__c, DetectionAlertCriteria__r.Requires_User_Verification__c, DetectionAlertCriteria__r.Last_Validated__c, DetectionAlertCriteria__r.Automated_Suppression__c, DetectionAlertCriteria__r.Suppression_Threshold__c, HiddenDataMap__c, HiddenEventMap__c, AlertInformation__c, Environment__c, Subject__c, Severity__c, Description__c, RecipientTeam__c, CreatedDate, AttackIQ_Alert__c, Suppressed__c, RecipientTeamFormat__c
                                       FROM Detection_Alert__c 
                                       WHERE Id IN : Trigger.new
                                       	AND DetectionAlertCriteria__r.Custom_Logic_Path__c = True];
    
    for( Detection_Alert__c detectionAlert : alerts ) {
        
        
        
    }
}
trigger ESA_Category_BeforeUpdate on ESA_Service_Item_Category__c (Before Update) {

    EsaOHAppValidationsHelper.Esa_Category_ExternalAssignment_Required_Validation(Trigger.newMap, Trigger.oldMap);
    EsaOHAppValidationsHelper.Esa_Category_ExternalAssignment_Validation(Trigger.newMap);

}
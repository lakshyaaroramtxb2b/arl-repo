trigger ESA_CategoryAfterUpdate on ESA_Service_Item_Category__c (after update) {
   EsaOHAppValidationsHelper.Esa_Category_ExternalAssignment_Validation(Trigger.newMap);
}
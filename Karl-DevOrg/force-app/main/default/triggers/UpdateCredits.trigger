trigger UpdateCredits on Scan_Info__c (after insert, after update) {

list<id> NeedToUpdateList=new list<id>();
for( Id si : Trigger.newMap.keySet() )
{
  if( Trigger.newMap.get( si ).Status__c!=null && Trigger.newMap.get( si ).Status__c=='Done')
  {
  
      if( (Trigger.isUpdate && Trigger.oldMap.get( si ).Status__c != Trigger.newMap.get( si ).Status__c) || Trigger.isInsert )
      {
         Scan_Info__c ChangedSi = Trigger.newMap.get( si );
         if(ChangedSi.OrgID__c!=null)
         {
             Id TempId=ChangedSi.OrgID__c;
             NeedToUpdateList.add(TempId);
         }
      }
  }
}
list<Contributing_Org__c> finalUpdateList=new list<Contributing_Org__c>();
if(NeedToUpdateList.size()>0)
{
//do the update
list<Contributing_Org__c> Co_Creditslist=new list<Contributing_Org__c>();

    for(Contributing_Org__c co:[select id,Credits__c from Contributing_Org__c where Org_Id__c IN :NeedToUpdateList])
    {
        Co_CreditsList.add(co);
    }
    if(Co_CreditsList.size()>0)
    {
        for(Contributing_Org__c co:Co_CreditsList)
        {
            if(co.Credits__c>0)
            {
                co.Credits__c=co.Credits__c-1;
                finalUpdateList.add(co);
            }
        }
    }
    
}

if(finalUpdateList.size()>0)
    update finalUpdateList;

}
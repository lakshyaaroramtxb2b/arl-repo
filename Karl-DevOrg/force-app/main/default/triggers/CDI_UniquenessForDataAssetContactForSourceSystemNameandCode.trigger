trigger CDI_UniquenessForDataAssetContactForSourceSystemNameandCode on CDI_Data_Asset_Contact__c (before insert,before update) {
for(CDI_Data_Asset_Contact__c dsc:Trigger.new)
{
   
     If(dsc.Gus_User__c !=null)
    {
     dsc.Unique_Key__c=dsc.Data_Asset__c + ' - ' + dsc.Gus_User__c;
    }
    else
    {
       dsc.Unique_Key__c=dsc.Data_Asset__c + ' - ' + dsc.Contact_Email_Id__c;
    }
    
      If(dsc.Source_System_Name__c !=null && dsc.Source_Data_Asset_Contact_Code__c!=null)
    { 
    dsc.Unique_Key_Source_System_and_Code__c=dsc.Source_System_Name__c + ' - ' + dsc.Source_Data_Asset_Contact_Code__c;
    }
    If(dsc.Source_System_Name__c !=null && dsc.Source_Data_Asset_Contact_Code__c==null)
    {
        dsc.addError('Either Source System Name or Source Data Asset Contact Code is missing');
    }
    Else if(dsc.Source_System_Name__c ==null && dsc.Source_Data_Asset_Contact_Code__c!=null)
    {
        dsc.addError('Either Source System Name or Source Data Asset Contact Code is missing');
    }
}

}
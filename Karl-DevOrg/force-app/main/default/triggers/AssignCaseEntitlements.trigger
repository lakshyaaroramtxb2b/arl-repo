trigger AssignCaseEntitlements on Case (before insert, before update) {
  
    // This code block needs to be moved to utility class //
    Map<String, String> irCaseMap = new Map<String, String>();
    for(IRCloud__IRCloud_Case_RecordType__mdt rtMD : [SELECT DeveloperName, IRCloud__RecordType__c FROM IRCloud__IRCloud_Case_RecordType__mdt]){
        irCaseMap.put(rtMD.DeveloperName, rtMD.IRCloud__RecordType__c);
    }
    
    Map<String,Id> caseTypes = IRCloud.IRCloud_Utils.getRecordTypeMapForObjectGeneric(Case.SObjectType);
    
    Map<Id, String> irRecordTypeIds = new Map<Id, String>();
    for(String devName : irCaseMap.keySet()){
        irRecordTypeIds.put(caseTypes.get(devName), devName);
    }
    // ---END BLOCK-- //
    
    
    // Get Entitlements from salesforce.com Account
    Map<String, Id> entitlementMap = new Map<String, Id>();
    for(Entitlement e : [SELECT Id, Name FROM Entitlement WHERE AccountId = '0013000001EtVd9']){
        entitlementMap.put(e.Name, e.Id);
    }
    
    
    for(Case c : Trigger.new){
        if(!irRecordTypeIds.containsKey(c.RecordTypeId)){
            continue; // Don't care about this Case; goto next in loop.
        }
        

        String rtName = irRecordTypeIds.get(c.RecordTypeId);
            
        if(trigger.IsInsert){
            c.BusinessHoursId = '01m3A000000JOWZ';
            if(rtName == 'CSIRT_Alert'){
                c.EntitlementId = entitlementMap.get('CSIRT Alerts & Emails');
            } else if(rtName == 'CSIRT_Email'){
                c.EntitlementId = entitlementMap.get('CSIRT Alerts & Emails');
            } else if(rtName == 'CSIRT_Customer_Security_Incident'){
                if(c.IRCloud__Incident_Severity__c == 'Severity 0'){
                    c.EntitlementId = entitlementMap.get('CSIRT Severity 0 Incidents');
                } else if(c.IRCloud__Incident_Severity__c == 'Severity 1'){
                    c.EntitlementId = entitlementMap.get('CSIRT Severity 1 Incidents');
                } else if(c.IRCloud__Incident_Severity__c == 'Severity 2'){
                    c.EntitlementId = entitlementMap.get('CSIRT Severity 2 Incidents');
                } else if(c.IRCloud__Incident_Severity__c == 'Severity 3'){
                    c.EntitlementId = entitlementMap.get('CSIRT Severity 3 Incidents');
                }  
            } else if(rtName == 'CSIRT_Customer_Support'){
                return;
            } else if(rtName == 'CSIRT_Escalation'){
                c.EntitlementId = entitlementMap.get('CSIRT Escalation');
            } else if(rtName == 'CSIRT_Internal_Security_Incident'){
                if(c.IRCloud__Incident_Severity__c == 'Severity 0'){
                    c.EntitlementId = entitlementMap.get('CSIRT Severity 0 Incidents');
                } else if(c.IRCloud__Incident_Severity__c == 'Severity 1'){
                    c.EntitlementId = entitlementMap.get('CSIRT Severity 1 Incidents');
                } else if(c.IRCloud__Incident_Severity__c == 'Severity 2'){
                    c.EntitlementId = entitlementMap.get('CSIRT Severity 2 Incidents');
                } else if(c.IRCloud__Incident_Severity__c == 'Severity 3'){
                    c.EntitlementId = entitlementMap.get('CSIRT Severity 3 Incidents');
                }  
            } else if(rtName == 'CSIRT_Investigation'){
                return;
            } else if(rtName == 'CSIRT_Vulnerability'){
                c.EntitlementId = entitlementMap.get('CSIRT Vulnerability');
            } else if(rtName == 'STAR_Alert'){
                c.EntitlementId = entitlementMap.get('STAR Security Reports');
            } else if(rtName == 'STAR_Email'){
                c.EntitlementId = entitlementMap.get('STAR Security Reports');
            } else if(rtName == 'STAR_Incident'){
                c.EntitlementId = entitlementMap.get('STAR Incidents');
            } else if(rtName == 'eDiscovery'){
                return;
            }
        }
        
        if(trigger.isUpdate){
            Case oldCase = Trigger.OldMap.get(c.Id);
            if(oldCase.IRCloud__Incident_Severity__c == c.IRCloud__Incident_Severity__c){
                    return; // Don't need to do anything if Severity stays the same.
            }
            if(rtName == 'CSIRT_Customer_Security_Incident' || rtName == 'CSIRT_Internal_Security_Incident'){
                if(c.IRCloud__Incident_Severity__c == 'Severity 0'){
                    c.EntitlementId = entitlementMap.get('CSIRT Severity 0 Incidents');
                } else if(c.IRCloud__Incident_Severity__c == 'Severity 1'){
                    c.EntitlementId = entitlementMap.get('CSIRT Severity 1 Incidents');
                } else if(c.IRCloud__Incident_Severity__c == 'Severity 2'){
                    c.EntitlementId = entitlementMap.get('CSIRT Severity 2 Incidents');
                } else if(c.IRCloud__Incident_Severity__c == 'Severity 3'){
                    c.EntitlementId = entitlementMap.get('CSIRT Severity 3 Incidents');
                }
            }
        }
        
    }

}
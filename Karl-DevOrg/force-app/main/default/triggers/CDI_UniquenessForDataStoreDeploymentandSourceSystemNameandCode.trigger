trigger CDI_UniquenessForDataStoreDeploymentandSourceSystemNameandCode on CDI_Data_Store_Deployment__c (before insert,before update) {
for(CDI_Data_Store_Deployment__c dsd:Trigger.new)
{
    dsd.Unique_Key__c=dsd.Data_Center__c + ' - ' + dsd.Data_Store__c;
    
    If(dsd.Source_System_Name__c !=null && dsd.Source_Data_Store_Deployment_Code__c!=null)
    { 
    dsd.Unique_key_Source_System_and_Code__c=dsd.Source_System_Name__c + ' - ' + dsd.Source_Data_Store_Deployment_Code__c;
    }
    If(dsd.Source_System_Name__c !=null && dsd.Source_Data_Store_Deployment_Code__c==null)
    {
        dsd.addError('Either Source System Name or Source Data Store Deployment Code is missing');
    }
    Else if(dsd.Source_System_Name__c ==null && dsd.Source_Data_Store_Deployment_Code__c!=null)
    {
        dsd.addError('Either Source System Name or Source Data Store Deployment Code is missing');
    }
}

}
trigger DataDictionaryTrigger on Detection_Data_Dictionary_Items__c (before insert, before update) 
{
    //make sure that coma seperated
    for( Detection_Data_Dictionary_Items__c in_ddI : Trigger.new )
	{
        if(in_ddI.Captures_values__c != null) 
        {
            if(in_ddI.Captures_values__c.contains(' ') && !in_ddI.Captures_values__c.contains(','))
            {
                in_ddI.addError(' \'Captured data types\' values must be comma separated. spaces are not supported e.g. a,b,c');
                    
                return;
            }
            
            if(in_ddI.Name.contains('_'))
            {
                //Becouse Regex groups do not support underscores
                in_ddI.addError('Names cannot contain underscores \'_\'.');
                    
                return;
            }
            
            in_ddI.Captures_values__c = in_ddI.Captures_values__c.replace(' ', '');
        }
    }
    
    // ==================    ======================    ===================    ==================
  
    //look for exisitng values
    List<Detection_Data_Dictionary_Items__c> allItems = [SELECT Id, Name, Captures_values__c 
                                                         FROM Detection_Data_Dictionary_Items__c
                                                         WHERE Captures_values__c != null];
    
    for( Detection_Data_Dictionary_Items__c in_ddI : Trigger.new )
	{       
        if(in_ddI.Captures_values__c != null )
        {
            String ddIHealthWarnings = '';
            
            for(Detection_Data_Dictionary_Items__c ddI : allItems)
            {
                for(String capturedDataTypeA : ddI.Captures_values__c.split(',')) 
                {
                    for(String capturedDataTypeB : in_ddI.Captures_values__c.split(','))
                    {
                        if(in_ddI.Id != ddI.Id) 
                        {
                            //system.debug(capturedDataTypeA);
                            //system.debug(capturedDataTypeB);
                            
                            if(capturedDataTypeA.toLowerCase() == capturedDataTypeB.toLowerCase()) 
                            {
                                ddIHealthWarnings += ' Possible redundant object. The data dictionary item \'' + ddI.Name + '\' already appears to capture the same data type as this data dicitonary item. They both listed \'' + capturedDataTypeA + '\' as a captured \'Captured data types\'.' + '\n'; 
                            }
                        }
                    }
                }
            }
            
            in_ddI.Health_Check_Warnings__c = ddIHealthWarnings;
        }
    }
    
    
    /*return; 
    
    Map<Id, List<String>> changedFnames = new Map<Id, List<String>>();

	//Check that regex vars have DD field
	for( Detection_Data_Dictionary_Items__c ddI : Trigger.new )
	{
        //Check that name has not changed
		if(ddI.name != Trigger.oldMap.get(ddI.Id).name)
		{
            //prevent concurent name changes
            if(!changedFnames.containsKey(ddI.Id))
            {
                List<String> nameChange = new List<String>();
                //New Name
                nameChange.add(ddI.name);
                //Old Name
                nameChange.add(Trigger.oldMap.get(ddI.Id).name);
                
            	changedFnames.put(ddI.Id, nameChange);
                
            }
        }
    }
    
    //rename regex fields
  
    //get junction object as guide
    List<Detection_Field_Extraction__c> fxToUpdate = [SELECT Id, Name FROM Detection_Field_Extraction__c WHERE Id IN: changedFnames.keySet()];
    
    for(Detection_Field_Extraction__c fx : fxToUpdate)
    {
        String str = 'Rewrite Field_Extraction_Regex__c "' + fx.Name + '"';

        str += 'Old Regex = ' + fx.Field_Extraction_Regex__c;
        
        fx.Field_Extraction_Regex__c.replace('?<' + changedFnames.get(fx.Id)[1] + '>', '?<' + changedFnames.get(fx.Id)[0] + '>');
        
        str += 'New Regex = ' + fx.Field_Extraction_Regex__c;
        str += '====================================================================================';

        //=====================================

    	// First, reserve email capacity for the current Apex transaction to ensure
        // that we won't exceed our daily email limits when sending email after
        // the current transaction is committed.
        Messaging.reserveSingleEmailCapacity(2);
        
        // Processes and actions involved in the Apex transaction occur next,
        // which conclude with sending a single email.
        
        // Now create a new single email message object
        // that will send out a single email to the addresses in the To, CC & BCC list.
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        
        // Strings to hold the email addresses to which you are sending the email.
        String[] toAddresses = new String[] {'amcneilly@salesforce.com'}; 

        // Assign the addresses for the To and CC lists to the mail object.
        mail.setToAddresses(toAddresses);
        
        // Specify the name used as the display name.
        mail.setSenderDisplayName('Debug');
        
        // Specify the subject line for your email address.
        mail.setSubject('Debug');
        
        // Set to True if you want to BCC yourself on the email.
        mail.setBccSender(false);
        
        // Optionally append the salesforce.com email signature to the email.
        // The email address of the user executing the Apex Code will be used.
        mail.setUseSignature(false);
        
        // Specify the text content of the email.
        mail.setPlainTextBody(str);
        
        mail.setHtmlBody(str);
        
        // Send the email you have created.
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
    
    //UPDATE fxToUpdate;*/

}
trigger TriggerWhitelistRequest on Whitelist_Request__c (before delete, before insert, before update, after delete, after insert, after update) {
	
    /*This if block will only run after succesful insert of records into salesforce database*/
         
    if(Trigger.isInsert && Trigger.isAfter){
    
        for (Whitelist_Request__c newRec : Trigger.new) {
            
            Whitelist_Request__c newRecFull = [SELECT Id, 
                                               		  Detection_Alert_Criteria__r.Name, Reason_for_request__c,
                                               		  Reference_CSIRT_Case__c
                                                 FROM Whitelist_Request__c
                                                 WHERE Id =: newRec.Id];
            
            //DCGusConnector con = new DCGusConnector();
            
            //string wlName, string wlId, string wlReason
            DCGusConnector.CreateGUSWhiteLsitWorkItem(newRecFull.Detection_Alert_Criteria__r.Name,
                                                       (String)newRecFull.id,
                                                       newRecFull.Reason_for_request__c, 
                                                       newRecFull.Reference_CSIRT_Case__c);
            
            //DCGusConnector.TestCreate();
        }
    }
}
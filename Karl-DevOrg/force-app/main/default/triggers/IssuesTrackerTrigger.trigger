trigger IssuesTrackerTrigger on KARL_Issue_Tracker__c (After update, After insert) {
    KARL_IssuesTrackerTriggerHelper.run();
}
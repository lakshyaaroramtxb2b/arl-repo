/**
 * Trust Maturity Model v2.0
 * September 2015
 * jcaceres, jchen, jstashewsky
 * 
 * Place all object trigger actions in this class to provide easy deduction of execution order.
*/

trigger TM_Placement_BeforeInsert on TM_Placement__c (before insert) {

    TM_enforcePlacementEnvTypeIntegrity.enforcePlacementEnvTypeIntegrity(Trigger.new);
    
}
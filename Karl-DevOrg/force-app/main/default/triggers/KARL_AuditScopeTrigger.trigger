/**
 * @author Lakshya Arora
 * @email lakshya.arora@mtxb2b.com
 * @description Trigger on KARL_Audit_Scope_Master_Data__c
*/
trigger KARL_AuditScopeTrigger on KARL_Audit_Scope_Master_Data__c(before insert,before update) {
    if(Trigger.isBefore){
        if(Trigger.isInsert){
            KARL_AuditScopeTriggerHandler.beforeInsertOperations(Trigger.new);
        }
        if(Trigger.isUpdate){
            KARL_AuditScopeTriggerHandler.beforeUpdateOperations(Trigger.new,Trigger.oldMap);
        }
    }
}
trigger ESA_ServiceItem_BeforeInsert on ESA_Service_Item__c (before insert) {
   EsaOHAppValidationsHelper.Esa_ServiceItem_ExternalAssignment_Required_Validation(Trigger.newMap,Trigger.new,true);
}
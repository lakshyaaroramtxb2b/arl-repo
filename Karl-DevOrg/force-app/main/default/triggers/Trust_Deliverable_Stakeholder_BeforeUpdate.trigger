trigger Trust_Deliverable_Stakeholder_BeforeUpdate on Trust_Deliverable_Stakeholder__c (before update) {
    
    // populate name field
    TD_PopulateStakeholderName.populateName(trigger.new);   

}
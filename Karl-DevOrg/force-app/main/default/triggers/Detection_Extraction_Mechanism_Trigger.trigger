trigger Detection_Extraction_Mechanism_Trigger on Detection_Extraction_Mechanism_mk2__c (before update, after insert) {           
    
    //Check that regex vars have DD field
    for( Detection_Extraction_Mechanism_mk2__c dfmId : Trigger.new )
    {
        boolean hasChanged = false;
        
        List<Detection_Data_Dictionary_Map_mk2__c> ddJun = new  List<Detection_Data_Dictionary_Map_mk2__c>();

        if(Trigger.IsUpdate) {
            Detection_Extraction_Mechanism_mk2__c oldDfmId = Trigger.oldMap.get(dfmId.Id);

            if(dfmId.Regular_Expression__c  != oldDfmId.Regular_Expression__c)
            {
                hasChanged = true;
            }
        }
        
                if((hasChanged || Trigger.IsInsert) && !String.isBlank(dfmId.Regular_Expression__c))
                {

                    Pattern regexPattern = Pattern.compile('\\?<(.*?)>');
                    Matcher regexMatcher = regexPattern.matcher(dfmId.Regular_Expression__c);
                    
                    Set<String> regexVars = new Set<String>();
                    
                    while (regexMatcher.find()) 
                    {
                        String regexVar = regexMatcher.group().replace('?<', '').replace('>', '');
                        
                        //check for DD fields
                        Detection_Data_Dictionary_Items__c [] ddMatch = [SELECT Id, Name FROM Detection_Data_Dictionary_Items__c WHERE Name =: regexVar];
        
                        if(ddMatch.size() == 0)
                        {
                            trigger.new[0].Regular_Expression__c.addError('No matching Data Dictionary Field \"' + regexVar + '\". Please check the spelling or add a new field.');
                            return;
                        }
                        
                        //case senstive check
                        if(!ddMatch[0].Name.Equals(regexVar))
                        {
                            trigger.new[0].Regular_Expression__c.addError('Case appears incorrect. Please check that the case matchs an exsiting data dictionary item.');
                            return;
                        }
                        
                        //create many-to-many relationships between field and data dictionary
                        Detection_Data_Dictionary_Map_mk2__c  newJun = new Detection_Data_Dictionary_Map_mk2__c ();
                        newJun.Data_Dictionary_Item__c = ddMatch[0].Id;
                        newJun.Detection_Extraction_Mechanism__c = dfmId.Id;
                        ddJun.add(newJun);
                        
                        regexVars.add(regexVar);
                    }
                    
                    if(Trigger.IsUpdate)
                    {
                        String varString = String.join(new List<String>(regexVars), ', ');
                        dfmId.Extracted_Fields__c = varString.substring(0, varString.length() < 255 ? varString.length() : 255);
                        
                        //setup many-to-many relationships
                        DELETE [SELECT Id FROM Detection_Data_Dictionary_Map_mk2__c WHERE Detection_Extraction_Mechanism__c =: dfmId.Id];
                    }
                    
                        //==============
                        //FeedItem post = new FeedItem();
                        //post.ParentId = dfmId.Id;
                        //post.Body = 'aaaaaa';
                        //insert post;
                        //==============
                    
                    if(ddJun.size() > 0)
                        insert ddJun;
                }
    }
}
/***********************************************************
* Created by 	- Harinath Ch
* Date 		- 06th August 2019
* Trigger for Assignment
************************************************************/
trigger PRT_AssignmentWeekTrigger on Assignment_Week__c (before insert, before update, after insert, after update) {
    
    if(!PRT_Constants.INTEGRATION_SETTING.Disable_Assignment_Week_Trigger__c  ){
    if(Trigger.isBefore && trigger.isInsert) {
        IF(PRT_Constants.RECURSIVE_TRIGGER_CHECK){
            PRT_Constants.RECURSIVE_TRIGGER_CHECK =FALSE;
            PRT_AssignmentWeekTriggerHandler.beforeInsert(Trigger.new);
        }
    }
    if(Trigger.isAfter && trigger.isInsert){
            PRT_AssignmentWeekTriggerHandler.AfterInsert(Trigger.new);
    }
    
    if(Trigger.isBefore && trigger.isUpdate){
            PRT_AssignmentWeekTriggerHandler.beforeUpdate(Trigger.new,Trigger.oldMap);
    }
    if(Trigger.isAfter && trigger.isUpdate){
            PRT_AssignmentWeekTriggerHandler.afterUpdate(Trigger.new,Trigger.oldMap);
    }
    }
}
trigger GusSLATrackingTrigger on KARL_GUS_SLA_Tracking__c (before insert,before update) {
    GusSLATrackingTriggerHelper.run();
}
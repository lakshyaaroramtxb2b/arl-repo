trigger DetectionAlertCriteriaTriggerFeedUpdate on Detection_Security_Event_Criteria__c (after update) {

    for (Detection_Security_Event_Criteria__c dsec : Trigger.new) {
        // Access the "old" record by its ID in Trigger.oldMap
        Detection_Security_Event_Criteria__c oldDSEC = Trigger.oldMap.get(dsec.Id);
    
        // Trigger.new records are conveniently the "new" versions!
        if( oldDSEC.V2SplunkSearch__c != dsec.V2SplunkSearch__c)
        {
            if(oldDSEC.V2SplunkSearch__c != null)
            {
                string v2SplunkSearch = oldDSEC.V2SplunkSearch__c;
                
                if(v2SplunkSearch.length() > 5000)
                {
                    v2SplunkSearch = v2SplunkSearch.substring(0, 4997) + '...';
                }
                
                FeedItem post = new FeedItem();
                post.ParentId = dsec.id; 
                post.Body = 'V2SplunkSearch updated. Previous string: ' + v2SplunkSearch;
                insert post;
            }
        }
	}
    
}
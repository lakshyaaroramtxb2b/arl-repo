trigger detectionLogicWICreation on Detection_Logic__c (after insert) {

RecordType rt = [SELECT Id FROM RecordType WHERE DeveloperName='Work_Item' and SobjectType='Work_Item__c'];
List <Work_Item__c> WItoInsert = new List <Work_Item__c> ();

for (Detection_Logic__c D : Trigger.new) {
    Work_Item__c WI = new Work_Item__c();
    
    //WI.Owner = D.Owner;
    WI.RecordTypeId = rt.Id;
    WI.Detection_Logic__c = D.Id;
    WI.Work_Item_Name__c = 'Create Detection Logic "' + D.Name + '"';
    
    WItoInsert.add(WI);
    }
    
    try {

        insert WItoInsert;

    } catch (system.Dmlexception e) {

        system.debug (e);

    }

    
}
/*

    Developer Name: Jorge L Caceres - jcaceres@salesforce.com | August 2017 
    
    Description:
        Place all object trigger actions in this class to provide
        easy deduction of execution order
*/

trigger ESA_SecurityRequest_BeforeInsert on ESA_Security_Request__c (before insert) {

    ESA_TriggerHelper.populateOwner(trigger.new);
    ESA_TriggerHelper.populateEntityCode(trigger.new);
    ESA_TriggerHelper.populateChatterJunctionRecordId(trigger.new);
    if(trigger.new[0].Entity_Code__c == 'BUS' ) {
        EsaOHAppValidationsHelper.setApproversForBUS(trigger.new[0]);
    }
}
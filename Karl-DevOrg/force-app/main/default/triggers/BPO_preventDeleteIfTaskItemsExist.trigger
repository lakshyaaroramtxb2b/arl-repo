/**
 * BPO App v1
 * Feb 2017
 * Written By: Joanna Chen
 * 
 * Prevent the deletion of a BPO Task if child BPO Task Items exist.
 * 
**/


trigger BPO_preventDeleteIfTaskItemsExist on BPO_Task__c (before delete) {

    Set <ID> triggerIds = Trigger.oldMap.keySet();
    List<BPO_Task__c> tasks = new List<BPO_Task__c>();
    
    tasks = [SELECT Id,
            (SELECT Id FROM BPO_Task_Items__r)
            FROM BPO_Task__c 
            WHERE Id IN :triggerIds];
    
    for (BPO_Task__c t: tasks) {
        if (t.BPO_Task_Items__r.size() > 0) {
            Trigger.oldMap.get(t.Id).addError('Cannot delete a BPO Task if it has BPO Task Items associated with it.');
        }
    }
}
trigger CSIRT_PreventEmailDelete on EmailMessage (after delete) {
	String profileName=[Select Id,Name from Profile where Id=:System.UserInfo.getProfileId()].Name;
    for(EmailMessage email : Trigger.Old){
        if (profileName == 'CSIRT'){
            email.addError('Do you want to get fired? Because this is how you get fired. DON\'T DELETE EMAILS.');
        }
    }
}
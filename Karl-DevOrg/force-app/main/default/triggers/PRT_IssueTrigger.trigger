trigger PRT_IssueTrigger on Issues__c (before insert, before update) {
    if(Trigger.isBefore){
        if(Trigger.isUpdate){
            PRT_IssueTriggerHandler.beforeUpdate(Trigger.new, Trigger.oldMap);
        }else if(Trigger.isInsert){
            PRT_IssueTriggerHandler.beforeInsert(Trigger.new);            
        }
    }
}
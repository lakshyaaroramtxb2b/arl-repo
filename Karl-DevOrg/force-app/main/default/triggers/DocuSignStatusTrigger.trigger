trigger DocuSignStatusTrigger on dfsle__EnvelopeStatus__c (Before Update, After Update , After Insert, Before Insert) {
     KARL_DocuSignStatusTriggerHandler.run();

}
({
	toggleExpand : function(cmp, event, helper) {
		var isExpanded = $A.util.getBooleanValue(cmp.get("v.isExpanded"));
        // Toggle
		cmp.set("v.isExpanded", !isExpanded);
	}
})
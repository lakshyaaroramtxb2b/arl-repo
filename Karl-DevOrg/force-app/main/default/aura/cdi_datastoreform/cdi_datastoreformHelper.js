({
    
    addDataStoreContactRecord: function(component, event) {
        var gususerList = component.get("v.gususerList");
        gususerList.push({
            'sobjectType': 'CDI_GUS_User__c',
            'Name': '',
        });
        component.set("v.gususerList", gususerList);
    },     
    saveDataStoreContactList: function(component, event, helper) {
        var action = component.get("c.saveDataStoreContacts");
        action.setParams({
            "gususerList": component.get("v.gususerList"),
            "parentRecordId":event.getParams().response.id
        });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.dscList", []);   
            }
        }); 
        $A.enqueueAction(action);
    },
    saveDatastoreType: function(component, event) {
        var datastoretype=null;
        if(component.get("v.datastoretypename")!==null && typeof component.get("v.datastoretypename") !=='undefined')
        {
            datastoretype=component.get("v.datastoretypename");
        }
        else
        {
            datastoretype=component.get("v.datastoretype");
        }
        var action = component.get("c.saveDatastoreType");
        action.setParams({
            "datastoretypeId": datastoretype,
            "datastoreId":event.getParams().response.id
        });  
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log(state);
        })
        $A.enqueueAction(action);
    },
    addDataStoreServiceRecord: function(component, event) {
        
        var dssList = component.get("v.dssList");
        dssList.push({
            'sobjectType': 'CDI_Data_Store_Service__c',
            'Is_Data_Stored__c': '',
            'Is_Data_Processed__c': '',
            'Is_Data_Transmitted__c': '',
            'Service__c':''
        });
        component.set("v.dssList", dssList);
    },          
    saveDataStoreServiceList: function(component, event, helper) {
        var action = component.get("c.saveDataStoreServices");
        action.setParams({
            "dssList": component.get("v.dssList"),
            "parentRecordId":event.getParams().response.id
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.dssList", []);  
            }
            
        }); 
        $A.enqueueAction(action);
    },
})
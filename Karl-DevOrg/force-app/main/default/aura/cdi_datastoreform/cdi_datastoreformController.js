({
    
    addRowDSC: function(component, event, helper) {
        var selectRecName = event.getParam('selectName');
        var selectRecId = event.getParam('currentRecId');
        if(selectRecName != undefined) {
            component.set("v.selectRecordName", selectRecName);
            component.set("v.selectRecordId", selectRecId);         
        }
        
        helper.addDataStoreContactRecord(component, event);
    },     
    removeRowDSC: function(component, event, helper) {
        var gususerList = component.get("v.gususerList");
        var selectedItem = event.currentTarget;
        var index = selectedItem.dataset.record;
        gususerList.splice(index, 1);
        component.set("v.gususerList", gususerList);
    },
    addRowDSS: function(component, event, helper) {
        
        helper.addDataStoreServiceRecord(component, event);
    },     
    removeRowDSS: function(component, event, helper) {
        
        var dssList = component.get("v.dssList");
        var selectedItem = event.currentTarget;
        var index = selectedItem.dataset.record;
        dssList.splice(index, 1);
        component.set("v.dssList", dssList);
    },
    save: function(component, event, helper) {
        var payload = event.getParams().response;
        var navService = component.find("navService");
        helper.saveDataStoreContactList(component, event);
        helper.saveDataStoreServiceList(component, event);
        helper.saveDatastoreType(component,event);
        var pageReference = {
            type: 'standard__recordPage',
            attributes: {
                "recordId": payload.id,
                "objectApiName": "CDI_Data_Store__c",
                "actionName": "view"
            }
        }
        event.preventDefault();
        navService.navigate(pageReference);
        
    },
    selectedServiceRecords : function(component, event, helper) {
        var selectRecName = event.getParam('selectName');
        var selectRecId = event.getParam('currentRecId');
        var indexValue=event.getParam('indexValue');
        var arr=component.find("customservicelookup");
        try
        {
            arr.set("v.value", selectRecId); 
        }
        catch(err)
        {
            arr[indexValue].set("v.value", selectRecId);  
        }
    },
    selectedDatastoreTypeRecords : function(component, event, helper) {
        var selectRecId = event.getParam('currentRecId');
        var arr=component.find("customdatastoretypelookup");
        component.set("v.checkDropDown",event.getParam('checkDropDown'))
        arr.set("v.value", selectRecId);
        component.set("v.datastoretypename",null);
    },
    selectedGusUserRecords : function(component, event, helper) {
        var selectRecName = event.getParam('selectName');
        var selectsecRecName = event.getParam('secrecNames');
        var selectRecId = event.getParam('currentRecId');
        var indexValue=event.getParam('indexValue');
        var arr=component.find("customgususerlookup");
        try
        {
            arr.set("v.value", selectRecName+','+selectsecRecName); 
        }
        catch(err)
        {
            arr[indexValue].set("v.value", selectRecName+','+selectsecRecName);  
        }
    },
    storeIsDataStoredValue : function(component, event, helper) {
        var checkCmp = component.find("isdatastored");
        var resultCmp = component.find("isdatastoredValue");
        var indexValue=event.getSource().get("v.name"); 
        console.log(event.getSource().get("v.name"));
        try
        {
            resultCmp.set("v.value",checkCmp.get("v.value"));
        }
        catch(err)
        {
            resultCmp[indexValue].set("v.value", checkCmp[indexValue].get("v.value"));  
        }
        
    },
    storeIsDataProcessedValue : function(component, event, helper) {
        var checkCmp = component.find("isdataprocessed");
        var resultCmp = component.find("isdataprocessedValue");
        var indexValue=event.getSource().get("v.name");
        console.log(event.getSource().get("v.name"));
        try
        {
            resultCmp.set("v.value",checkCmp.get("v.value"));
        }
        catch(err)
        {
            resultCmp[indexValue].set("v.value", checkCmp[indexValue].get("v.value"));  
        }
        
    },
    storeIsDataTransmittedValue : function(component, event, helper) {
        var checkCmp = component.find("isdatatransmitted");
        var resultCmp = component.find("isdatatransmittedValue");
        var indexValue=event.getSource().get("v.name"); 
        console.log(event.getSource().get("v.name"));
        try
        {
            resultCmp.set("v.value",checkCmp.get("v.value"));
        }
        catch(err)
        {
            resultCmp[indexValue].set("v.value", checkCmp[indexValue].get("v.value"));  
        }
        
    },
     openModel: function(component, event, helper) {
      // for Display Model,set the "isOpen" attribute to "true"
      component.set("v.isOpen", true);
   },
    checkDropDown:function(component, event, helper) {
      var checkDropDown=event.getParam('checkDropDown');
      component.set("v.checkDropDown",checkDropDown);
      if(event.getParam('currentText').trim()==''||event.getParam('currentText').trim()!==component.get("v.datastoretypename")||event.getParam('currentText')==null)
      {
        component.set("v.datastoretypename",null);
      }
   },
    checkIfCleared:function(component, event, helper) {
      var checkDropDown=event.getParam('checkDropDown');
        component.set("v.checkDropDown",checkDropDown);
        component.set("v.datastoretypename",null);
        component.set("v.datastoretype",null);
   },
   closeModel: function(component, event, helper) {
      component.set("v.isOpen", false);
   },
 
   submitDataStoreType: function(component, event, helper) {
      event.preventDefault();
      component.set("v.isOpen", false);
      component.set("v.checkDropDown",false);
      var fields = event.getParam('fields');
      component.set("v.datastoretypename",fields.Name);
      component.find("datastoretypeeditForm").submit();
       
   },
})
({
	doInit : function(cmp, event, helper) {
        helper.parseQuestion(cmp);
        // Hide when controlling or parent questions are present
        if (helper.hasParentQ(cmp) || helper.hasControllingQ(cmp)) { 
            helper.handleDisplay(cmp);
        }
	},
    
    handleInitDisplay : function (cmp, event, helper) {        
        var ctrlId =  event.getParam("controllingId");     
        // Respond with toggle when dependent question is redering/init.
        var id = cmp.get("v.id");
        var value = cmp.get("v.value");
        var isShow = $A.util.getBooleanValue(cmp.get("v.isShow"));
        if (cmp.isValid() && id.indexOf(ctrlId) != -1) {                
               event.setParam("id", id);
               event.setParam("value", value);
               event.setParam("isHidden", !isShow);
               //console.log(JSON.stringify(cmp.get("v.question")));
        }
    },

    handleCheckDisplay : function(cmp, event, helper) { 
        var id = event.getParam("id");
        if (id == cmp.get("v.id")) {
            event.setParam("isVisible", $A.util.getBooleanValue(cmp.get("v.isShow")));
        }
    },
    
    handleDisplay : function(cmp, event, helper) { 
        var ctrlQId = cmp.get("v.controllingId");
        var parentQId = cmp.get("v.controllingParentId");
        var id = event.getParam("id");
        var value = event.getParam("value");

        if (id == cmp.get("v.id")) {
            cmp.set("v.value", value);
        } else if ((helper.hasParentQ(cmp) && id.indexOf(parentQId) != -1)
                || (helper.hasControllingQ(cmp) && id.indexOf(ctrlQId) != -1)) {
            var isToggled = helper.handleDisplay(cmp);  
            if (isToggled) {
                // Notifies the multi level nested/children
                $A.get("e.c:toggleDisplayEvt").setParams({
                    "id" : cmp.get("v.id"),
                    "value" : cmp.get("v.value"),
                    "isHidden" : !cmp.get("v.isShow")
                }).fire();
            }
        }
	}
})
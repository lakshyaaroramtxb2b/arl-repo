({
	parseQuestion : function(cmp) {
        var q = cmp.get("v.question");
        if (q) {
            // If Controlled by Parent Question
            cmp.set("v.controllingParentId", q.serviceItemQuestion.Controlling_Group_Parent__c);
            // Controlling Question
            cmp.set("v.controllingId", q.serviceItemQuestion.Controlling_Question_Id__c);
            cmp.set("v.controllingComparison", q.serviceItemQuestion.Controlling_Question_Comparison__c);
            cmp.set("v.controllingValue", q.serviceItemQuestion.Controlling_Question_Answer__c);
            // Current Question
            cmp.set("v.id", q.serviceItemQuestion.ESA_Security_Question__c);
            cmp.set("v.value", q.answer);
        }		
	},
    
    // Returns true if display is toggled
    handleDisplay : function(cmp) {
        var isShow = false;
        var isParentQHidden = this.isQuestionHidden(cmp.get("v.controllingParentId"));
        var isCtrlQHidden = this.isQuestionHidden(cmp.get("v.controllingId"));

        if (!isParentQHidden && !this.hasControllingQ(cmp)) {
            isShow = true;
        } 

        if (this.hasControllingQ(cmp) && !isCtrlQHidden) {
            var cUtil = cmp.find('commonUtil');
            var ctrlComparison = cmp.get("v.controllingComparison");
            var ctrlValue = cmp.get("v.controllingValue");
            var value = this.getQuestionValue(cmp.get("v.controllingId"));

            switch(ctrlComparison) {
                case "EQ":
                    isShow = (value == ctrlValue);
                    break;
                case "NE":
                    isShow = (value != ctrlValue);
                    break;
                case "LT":
                    isShow = (value < ctrlValue);
                    break;
                case "LE":
                    isShow = (value <= ctrlValue);
                    break; 
                case "GT":
                    isShow = (value > ctrlValue);
                    break;
                case "GE":
                    isShow = (value >= ctrlValue);
                    break;
                case "RegExp":
                    isShow = !$A.util.isUndefinedOrNull(value) && !$A.util.isUndefinedOrNull(value.match(ctrlValue));
                    break;
                case "Contains":
                    isShow = (!$A.util.isEmpty(value) && value.indexOf(ctrlValue) != -1);
                    if (!isShow && !$A.util.isEmpty(ctrlValue)) { // Also supports multiple comma separated values
                        cUtil.forEach(ctrlValue.split(","), function(ctrlValueItem) {
                            // forEach iterates through all values 
                            // so set to true only when match is found
                            if (!$A.util.isEmpty(value) && value.indexOf(cUtil.trim(ctrlValueItem)) != -1) {
                                isShow = true;
                            }
                        });
                    }
                    break;
                default:
                    isShow = false;
            }
        }	

        // If display is toggled 
        if ($A.util.getBooleanValue(cmp.get("v.isShow")) != isShow) {
            cmp.set("v.isShow", isShow);
            if (!isShow) {
                this.cleanUp(cmp); 
            }
            return true;
        }
        return false;
    },
    
    cleanUp : function (cmp) {
        var esaUtil = cmp.find("esaUtil");
        var cUtil = cmp.find("commonUtil");
        // Don't clean the value for hidden fields
        if (!$A.util.getBooleanValue(cmp.get("v.question.isHidden"))) {
            var inputCmp = esaUtil.findCmpByType(cmp, "ui:inputText") 
                            || esaUtil.findCmpByType(cmp, "ui:inputTextArea") 
                            || esaUtil.findCmpByType(cmp, "ui:inputSelect") 
                            || esaUtil.findCmpByType(cmp, "ui:inputDate") 
                            || esaUtil.findCmpByType(cmp, "ui:inputNumber");
            
            if (inputCmp) {
                var defaultValue = cmp.get("v.question.defaultValue");
                inputCmp.set("v.value", defaultValue);
                cmp.set("v.value", defaultValue);
                if ($A.util.getBooleanValue(cmp.get("v.question.isPicklist"))
                        || $A.util.getBooleanValue(cmp.get("v.question.isMultiPicklist"))) {
                    var optionCmps = esaUtil.findCmpByType(cmp, "c:ESA_Select_Option", true);
                    cUtil.forEach(optionCmps, function (optionCmp) {
                        optionCmp.set("v.selectedValue", defaultValue);
                    });
                }
            } else {
                // Attachment Pill, Lookup Field selection pill etc
                var customInputCmp = esaUtil.findCmpByType(cmp, "c:ESA_Pill") || esaUtil.findCmpByType(cmp, "c:ESA_Lookup");
                if (customInputCmp) {
                    var clearEvt = customInputCmp.getEvent("onremove");
                    if (clearEvt) {
                        clearEvt.fire();
                    }
                } 
            }
        }
    },

    hasParentQ: function(cmp) {
        return !$A.util.isEmpty(cmp.get("v.controllingParentId"));
    },

    hasControllingQ: function(cmp) {
        return !$A.util.isEmpty(cmp.get("v.controllingId"));
    },

    isQuestionHidden: function (questionId) {
        if ($A.util.isEmpty(questionId)) {
            // No question exists, so not hidden
            return false;
        }

        try {
            // Fire init event so that parent can respond
            var initDisplayEvt = $A.get("e.c:esaInitDisplayEvt");
            initDisplayEvt.setParams({
                "controllingId" : questionId
            }).fire();
            // Event synchronously populates the value
            return $A.util.getBooleanValue(initDisplayEvt.getParam("isHidden"));
        } catch (e) {
            // On exception consider as hidden
            return true;
        }
    },

    getQuestionValue: function (questionId) {
        if ($A.util.isEmpty(questionId)) {
            return null;
        }
        try {
            // Fire init event so that parent can respond
            var initDisplayEvt = $A.get("e.c:esaInitDisplayEvt");
            initDisplayEvt.setParams({
                "controllingId" : questionId
            }).fire();
            // Event synchronously populates the value
            return initDisplayEvt.getParam("value");
        } catch (e) {
            return null;
        }
    }
})
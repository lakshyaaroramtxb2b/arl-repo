({
    init : function(component, event, helper) {
        /*  component.set('v.columns', [
            {label: 'EntSec Case Number', fieldName: 'linkName', type: 'url', 
            typeAttributes: {label: { fieldName: 'Enterprise_Security_Case_Number__c' }, target: '_self'}},
            {label: 'Subject', fieldName: 'Subject', type: 'text',sortable: true },
            {label: 'Status', fieldName: 'Status', type: 'text'},
            {label: 'Priority', fieldName: 'Priority'},
            {label: 'Owner', fieldName: 'OwnerId'},
        ]); */
        var action = component.get("c.getAllStatus");
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log(""+state);
            if(state == 'SUCCESS') {
                var statusOptions = response.getReturnValue();
                component.set('v.options', statusOptions);
                var selectedItems = [];
                component.get("v.options").forEach(function(item) {
                    selectedItems.push(item.value);
                });
                component.set("v.mySelectedItems", selectedItems);
                helper.fetchData(component, event, helper);
                console.log('Status Options: '+component.get('v.options'));   
            }
        });
        $A.enqueueAction(action);
    },
    
    handleStatusChange : function(component, event, helper) {
        var items = event.getParam("values");
        var selectedStatus = [];
        items.forEach(function(item) {
            selectedStatus.push(item);
        });
        console.log("Selected items are: "+items);
        component.set("v.mySelectedItems", selectedStatus);
        helper.fetchData(component, event, helper);
    },
    
    //    handleSave: function(component, event, helper) {
    //        var updatedRecords = component.find("caseTable").get("v.draftValues");
    //        var action = component.get("c.updateCases");  
    //        action.setParams({  
    //            'updatedCaseList' : updatedRecords  
    //        });  
    //        action.setCallback(this, function(response) {
    //            var state = response.getState();   
    //            if (state === "SUCCESS") {  
    //                if (response.getReturnValue() === true) {
    //                    helper.toastMsg('success', 'Records Saved Successfully.');  
    //                    component.find("acctTable").set("v.draftValues", null);  
    //                } else {   
    //                    helper.toastMsg( 'error', 'Something went wrong. Contact your system administrator.' );  
    //                }  
    //            } else {  
    //                helper.toastMsg( 'error', 'Something went wrong. Contact your system administrator.' );
    //            }  
    //        });  
    //        $A.enqueueAction( action );  
    //    },
    
    navigateToCaseCommentComponent : function(component, event, helper) {
        console.log(component.get("v.caseIds"));
        if(component.get("v.caseIds").length <= 0) {
            var errorEvent = $A.get("e.force:showToast");
            errorEvent.setParams({
                "title": "Error!",
                "message": "Please select some case first.",
                "type": "error"
            });
            errorEvent.fire(); 
        }
        else {
            component.set("v.buttonValue", "Create Comment");
            component.set("v.showModal", true);
            console.log('buttonValue');
        }
    },
    
    openStatusUpdateWindow : function(component, event, helper) {
        console.log(component.get("v.caseIds"));
        if(component.get("v.caseIds").length <= 0) {
            var errorEvent = $A.get("e.force:showToast");
            errorEvent.setParams({
                "title": "Error!",
                "message": "Please select some case first.",
                "type": "error"
            });
            errorEvent.fire();    
        }
        else {
            component.set("v.buttonValue", "Update Status");
            component.set("v.setValue", true);
            //component.set("v.showModal", true);
            console.log("buttonvalue status");
        }
    },
    
    handleClose : function(component, event, helper) {
        component.set("v.showModal", false);
        console.log('close --> ',"v.showModal");
    },
    
    updateSelectedText : function(component, event, helper) {
        var selectedRows = event.getParam('selectedRows');
        var idsList = [];
        console.log("SELECTED ROWS: "+selectedRows);
        selectedRows.forEach(row => idsList.push(row.Id));
        var titleString = "Add Case Comment (Case Count: "+idsList.length+")";
        component.set("v.caseCount", titleString);
        component.set("v.caseIds", idsList);
        console.log("Ids: "+component.get("v.caseIds"));
    },
    
    handleRecentlyContactedChange : function(component, event, helper) {
        var noOfDays = component.get("v.FILTER_recentlyContacted");
        var action = component.get("c.checkNumber");
        action.setParams({
            "noOfDays": noOfDays
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var returnVal = response.getReturnValue();
                if(returnVal) {
                    helper.fetchData(component, event, helper);
                }
                else {
                    var errorEvent = $A.get("e.force:showToast");
                    errorEvent.setParams({
                        "title": "Error!",
                        "message": "Enter only positive integers.",
                        "type": "error"
                    });
                    errorEvent.fire(); 
                }
            }
        });
        $A.enqueueAction(action);
    },
    
    handleNewCasesChange : function(component, event, helper) {
        helper.fetchData(component, event, helper);
    },
    updateColumnSorting:function(cmp, event, helper) {
        var fieldName = event.getParam('fieldName');
        var sortDirection = event.getParam('sortDirection');
        cmp.set('v.sortedBy', fieldName);
        cmp.set('v.sortedDirection', sortDirection);
        helper.sortData(cmp, fieldName, sortDirection);
    },
    listViewFilter : function(component, event, helper) {
        helper.filterData(component, event, helper);
    },
})
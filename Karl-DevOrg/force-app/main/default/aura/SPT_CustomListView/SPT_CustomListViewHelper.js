({
    /*fetchData : function(component, event, helper) {
        console.log("Fetch Selected Values: "+component.get('v.mySelectedItems'));
        var action = component.get("c.getAllCases");
        action.setParams({
            "statusList": component.get('v.mySelectedItems'),
            "noOfDays": component.get("v.FILTER_recentlyContacted"),
            "newCases": component.get("v.FILTER_newCases")
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var records = response.getReturnValue();
                records.forEach(function(record){
                    record.linkName = '/'+record.Id;
                    if(record.OwnerId) 
                        record.OwnerId = record.Owner.Name;
                });
                component.set('v.data',records);
            }
            // error handling when state is "INCOMPLETE" or "ERROR"
        });
        $A.enqueueAction(action);
    }, */
    fetchData : function(component, event, helper) {
        console.log("Fetch Selected Values: "+component.get('v.mySelectedItems'));
        
        //Selected Items
        var selectedItems = [];
        var  mySelectedItems=  component.get("v.mySelectedItems");
        if(mySelectedItems.includes("All")){
            component.get("v.options").forEach(function(item) {
                if(item.type != 'None'){
                    selectedItems.push(item.value);
                }
            });
        }else if(mySelectedItems.includes("All Open")){
            component.get("v.options").forEach(function(item) {
                if(item.type == 'Open'){
                    selectedItems.push(item.value);
                }
            });            
        }else if(mySelectedItems.includes("All Closed")){
            component.get("v.options").forEach(function(item) {
                if(item.type == 'Closed'){
                    selectedItems.push(item.value);
                }
            });
        }else if(mySelectedItems.includes("All Others")){
            component.get("v.options").forEach(function(item) {
                if(item.type == 'Other'){
                    selectedItems.push(item.value);
                }
            });
        }else{
            selectedItems = mySelectedItems;
        }
        var action = component.get("c.getCaseRecords");
        console.log('selectedItems --> ', selectedItems);
        action.setParams({
            statusList: selectedItems,
            noOfDays: component.get("v.FILTER_recentlyContacted"),
            newCases: component.get("v.FILTER_newCases"),
            strObjectName : 'Case',
            strFieldSetName : 'SPT_CaseListView',
            ownerId : component.get("v.selectOwnerRecordId") ? component.get("v.selectOwnerRecordId") : ''
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var rtnValue = response.getReturnValue();
                rtnValue.lstDataTableColumns.forEach(function (column) {
                    switch (column.fieldName) {
                        case 'Enterprise_Security_Case_Number__c':
                            column.type = 'url';
                            column.fieldName = 'linkName';
                            column['typeAttributes'] = { label: { fieldName: 'Enterprise_Security_Case_Number__c' } };
                            break;
                        default:
                            break;
                    }
                });
                component.set("v.columns", rtnValue.lstDataTableColumns);
                var records =rtnValue.lstDataTableData;
                console.log('recordsdata--> ', records);
                records.forEach(function(record){
                    record.linkName = '/'+record.Id;
                });
                component.set('v.data',records);
                component.set("v.filteredData",records);
               
                
            }
            // error handling when state is "INCOMPLETE" or "ERROR"
        });
        $A.enqueueAction(action);
    },
    filterData : function(component, event, helper){
        var records = component.get("v.data");
        console.log('records', records);
        var searchString = component.get("v.searchInput");
        console.log('searchString --> ', searchString);
        var filteredData =  records.filter(o =>
                                           Object.keys(o).some(k => o[k].toLowerCase().includes(searchString.toLowerCase())));
        component.set("v.filteredData",filteredData);
        console.log('filteredData --> ', filteredData);
    },
    sortData:function(cmp, fieldName, sortDirection) {
        var data = cmp.get('v.filteredData');
        var reverse = sortDirection !== 'asc';
        data.sort(this.sortBy(fieldName, reverse))
        cmp.set('v.filteredData', data);
    },
    sortBy:function(field, reverse, primer) {
        var key = primer ?
            function (x) { return primer(x[field]) } :
        function (x) { return x[field] };
        reverse = !reverse ? 1 : -1;
        return function (a, b) {
            return a = key(a), b = key(b), reverse * ((a > b) - (b > a));
        }
    },
    toastMsg : function(strType, strMessage) {
        var showToast = $A.get("e.force:showToast");   
        showToast.setParams({   
            message : strMessage,  
            type : strType
        });   
        showToast.fire();     
    }
})
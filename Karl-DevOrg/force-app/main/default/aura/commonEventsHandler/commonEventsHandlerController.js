({
	handleSessionTimeout: function(cmp, event, helper) {
        var commonUtil = cmp.find("commonUtil");
        commonUtil.clearMessages(true);
        $A.get("e.c:commonConfirmEvt").setParams({
            "headerText" : $A.get("$Label.c.Common_SessionTimeout"),
            "bodyText" : $A.get("$Label.c.Common_ConfirmLoginMsg"),
            "cancelLabel" : $A.get("$Label.c.Common_ConfirmLoginCancel"),
            "oKLabel" : $A.get("$Label.c.Common_ConfirmLoginOk"),
            "onOk": $A.getCallback(function(returnVal) {
                window.location.reload();
            })
        }).fire(); 
    }
})
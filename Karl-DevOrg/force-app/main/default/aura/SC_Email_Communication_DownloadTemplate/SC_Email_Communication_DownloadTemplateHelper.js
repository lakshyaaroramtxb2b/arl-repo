({
    // Generic method to do apex callout
    doCallout : function(cmp, methodName, params, callBackFunc){
        var action = cmp.get(methodName);
        action.setParams(params);
        action.setCallback(this, callBackFunc);
        $A.enqueueAction(action);
    },
    
    // Method to fetch data and create <a> tag and click it to download the data in csv format
    fetchDataAndDownload : function(component, event, helper) {
        try{
            helper.doCallout(component,"c.getCampaignMemberDataTemplate",{}, function(response){
                var state = response.getState();
                if(state === 'SUCCESS') {
                    var _resp = response.getReturnValue();
                    
                    if(_resp && _resp != null ) {
                        var hiddenElement = document.createElement('a'); // Created an <a> tag
                        hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(_resp);
                        hiddenElement.target = '_self'; 
                        hiddenElement.download = component.get('v.csvFileName') + '.csv'; 
                        document.body.appendChild(hiddenElement); 
                        hiddenElement.click(); // Click the <a> tag
                    }
                }
                
                helper.navigateBackToListViewPage(component, event, helper);
            });    
        }
        catch(Ex) {
            helper.navigateBackToListViewPage(component, event, helper);
            component.find('notifLib').showToast({
                "title": "Failed!",
                "variant": "error",
                "message": "Unable to download the data!"
            });
        }
    },
    
    // method to navigate back to the [SC_CISO_CampaignMember__c] List page
    navigateBackToListViewPage : function(component, event, helper) {
      
        var action = component.get("c.getAllListViewDetails");
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                var listViews = response.getReturnValue();
                
                if(listViews) {
                    
                    // Navigate to List view page
                    var navEvent = $A.get("e.force:navigateToList");
                    navEvent.setParams({
                        "listViewId": listViews[0].Id,
                        "listViewName": listViews[0].Name,
                        "scope": component.get('v.apiName')
                    });
                    navEvent.fire();
                }
                
            }
        });
        $A.enqueueAction(action);
        
    }
    
    /*
    
    fetchDocumentId : function(component, event, helper) {
        debugger;
        
        helper.doCallout(component,"c.getDocumentIdByName",{}, function(response){
            var state = response.getState();
            if(state === 'SUCCESS') {
                var _resp = response.getReturnValue();
                
                if(_resp) {
                   helper.downloadFile(component, _resp);
                }
                
            } else {}
        });    
    },
    
    // Download the content version of the file
    downloadFile : function(component, documentId) {
        var url = window.location.href;
        console.log('URL: - ' + url.split('/lightning/')[0]);        
        window.open(url.split('/lightning/')[0] + '/sfc/servlet.shepherd/version/download/' + documentId +'?');
        
    },
    
    
    // Method to convert Record Data to csv formated string
    convertArrayOfObjectsToCSV : function(component,objectRecords){
        var csvStringResult, counter, keys, columnDivider, lineDivider;
        
        if (objectRecords == null || !objectRecords.length) {
            return null;
        }
        
        // store ,[comma] in columnDivider variabel for sparate CSV values and 
        // for start next line use '\n' [new line] in lineDivider varaible  
        columnDivider = ',';
        lineDivider =  '\n';
        
        // In the fieldsToDownload variable store fields API Names as a key 
        // Custom label to get the fields columns to download
        let fieldsToDownload = $A.get("$Label.c.SC_CISO_CampaignMembers_Dataload_Template_Fields"); 
        keys = fieldsToDownload.split(',');
        
        csvStringResult = '';
        csvStringResult += keys.join(columnDivider);
        csvStringResult += lineDivider;
        
        for(var i=0; i < objectRecords.length; i++){   
            counter = 0;
            
            for(var sTempkey in keys) {
                var skey = keys[sTempkey] ;  
                
                // add [comma] after every String value [except first]
                if(counter > 0){ 
                    csvStringResult += columnDivider; 
                }   
                
                csvStringResult += '"'+ objectRecords[i][skey]+'"'; 
                counter++;
                
            } 
            
            csvStringResult += lineDivider;
        } 
        
        // return the CSV formate String 
        return csvStringResult;        
    },
    */   
})
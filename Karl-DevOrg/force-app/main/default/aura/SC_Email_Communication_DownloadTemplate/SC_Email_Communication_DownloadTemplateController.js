({
    // Init handler to fetch data and download the data in csv format
    doInit : function(component, event, helper) {
        helper.fetchDataAndDownload(component, event, helper);
    },
})
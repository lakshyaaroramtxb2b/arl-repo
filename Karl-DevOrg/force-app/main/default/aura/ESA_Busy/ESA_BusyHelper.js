({
	incrementCounter : function(cmp) {
		var reqCount = cmp.get("v.requestCount");
        //console.log("In Increment Count is " + reqCount);
        cmp.set("v.requestCount", ++reqCount);
	},
    
    decrementCounter : function(cmp) {
		var reqCount = cmp.get("v.requestCount");
        //console.log("In Decrement Count is " + reqCount);
        cmp.set("v.requestCount", --reqCount);
	},
    
    canHide : function(cmp) {
        var reqCount = cmp.get("v.requestCount");            
        return reqCount == 0;
    },
    
    showBusy : function(cmp) {
        this.incrementCounter(cmp);
        if (!$A.util.getBooleanValue(cmp.get("v.isBusy"))) {
           cmp.set("v.isBusy", true);
        }
	}, 
    
    hideBusy : function(cmp) {
        this.decrementCounter(cmp);
        if ($A.util.getBooleanValue(cmp.get("v.isBusy")) && this.canHide(cmp)) {
           cmp.set("v.isBusy", false);
        }
	}
})
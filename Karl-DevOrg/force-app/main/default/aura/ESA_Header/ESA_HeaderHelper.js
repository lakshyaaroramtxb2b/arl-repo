({
	handleHeaderDisplay : function(cmp) {
        if (!$A.util.isUndefinedOrNull(cmp.get("v.securityRequest"))) {
            if ($A.util.isEmpty(cmp.get("v.securityRequest.Entity_Code__c"))) {
                cmp.set("v.headerLogoText", null); // Triggers default logo display. Storing default space to hide by default
            }
        }
	}
})
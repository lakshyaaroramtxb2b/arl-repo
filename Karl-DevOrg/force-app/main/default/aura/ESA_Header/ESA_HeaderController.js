({
	doInit : function(cmp, event, helper) {
       // console.log(JSON.stringify(cmp.get("v.securityRequest")));
       helper.handleHeaderDisplay(cmp);
	}, 
	
	handleGroupSelection: function(cmp, event, helper) {
        cmp.set("v.headerLogoText", event.getParam("groupName"));  
        cmp.set("v.isSingleTab", event.getParam("isSingleTab"));     
    },

    navToGroup: function(cmp, event, helper) {
        // Only when multiple entities are present we can allow nav to group to display all entity tabs
        // Also when in singleTab type entity we don't want to load other entities in the same group
        if (!$A.util.getBooleanValue(cmp.get("v.isSingleTab"))) {
            var groupURL = "?groupName=" + encodeURIComponent(cmp.get("v.headerLogoText"));
            window.location.href = groupURL;
        }
    }
})
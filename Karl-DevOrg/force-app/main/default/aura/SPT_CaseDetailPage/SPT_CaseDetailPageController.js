({
    doInitDetailPage : function(component,event,helper) {
        console.log("Inside Detail Page Controller doInit");
        var act = component.get("c.getLayoutFields");
        act.setParams({});
        act.setCallback(this, function(response) {
            var state = response.getState();
            if(state == "SUCCESS") {
                console.log(response.getReturnValue());
                component.set("v.fieldList", response.getReturnValue());
            }
        });

        var action = component.get("c.getEntSecRecordTypeId");
        action.setParams({});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state == "SUCCESS") {
                component.set("v.recordType", response.getReturnValue());
            }
        });

        $A.enqueueAction(act);
        $A.enqueueAction(action);

        helper.refreshDescriptionField(component, event, helper);
    },
    handleLoad: function(cmp, event, helper) {
        console.log("Inside load function");
        cmp.set('v.showSpinner', false);
    },
    
    handleSubmit: function(cmp, event, helper) {
        console.log("Inside Handle Submit");        
        cmp.set('v.showSpinner', true);
        helper.validateUpdate(cmp,event,helper);
        
    },
    
    handleError: function(cmp, event, helper) {
        // errors are handled by lightning:inputField and lightning:messages
        // so this just hides the spinner
        cmp.set('v.showSpinner', false);
    },
    
    handleSuccess: function(cmp, event, helper) {
        cmp.set('v.showSpinner', false);
    },
    
    openModel: function(component, event, helper) {
        // for Display Model,set the "isOpen" attribute to "true"
        component.set("v.isModalOpen", true);
    },
    
    closeModel: function(component, event, helper) {
        // for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
        console.log("Inside Close Button OnClick");
        component.set("v.isModalOpen", false);
        component.set("v.descriptionWindow", false);
    },
    
    refreshCaseDetailPage: function(component, event, helper) {
        // set set the "isOpen" attribute to "False for close the model Box.
        component.set("v.isModalOpen", false);
        helper.refreshRecordHelper2(component, event, helper);
    },

    changeShowType: function(component, event, helper) {
        component.set("v.showType", "Show More");
        helper.refreshDescriptionField(component, event, helper);
    }
});
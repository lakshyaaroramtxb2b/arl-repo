({
    // Below function checks the change in field values and take action accordingly
    validateUpdate : function(component,event,helper) {
        var fieldMap = [];
        // stops the form from submitting
        event.preventDefault();       
        var fields = event.getParam('fields');
        console.log(JSON.stringify(fields),' <---------- fields');
        
        // Below line is getting the controller method validateCaseUpdate() to take action
        var action = component.get("c.validateCaseUpdate");
        // Setting parameters for the method
        action.setParams({
            "caseID": component.get("v.recordId"),
            "fieldsData": fields
        });
        // Handling the post-processing of the method
        action.setCallback(this, function (response) {
            var state = response.getState();            
            //component.set('v.showSpinner', false);
            console.log(JSON.stringify(response), ' <------- Response');
            console.log(state,'state');
            console.log(state === "SUCCESS",'is it');
            if (state === "SUCCESS") {
                // If there is no change in field values between two orgs, submit the form.
                if(response.getReturnValue()){
                    component.find('recordEditForm').submit(fields);
                    component.set('v.showSpinner', false);
                    window.location.reload();  
                }
                // Else, take refresh first from supportForce org.
                else{
                    helper.openErrorModel(component, event, helper);  
                    component.set('v.showSpinner', false);                
                }
            }else if(state == 'ERROR'){
                helper.openErrorModel(component, event, helper);  
                    component.set('v.showSpinner', false);   
            }
        });
        $A.enqueueAction(action);
        
    },
    // Below function helps in taking refresh of all case field values from supportForce org.
    refreshRecordHelper2 : function(component,event,helper) {   
        // Shows the spinner while sync in progress     
        component.set('v.showSpinner', true);
        // Getting the controller method refreshRecordCont()
        var action = component.get("c.refreshRecordCont");
        // Setting the method parameters
        action.setParams({
            "caseID": component.get("v.recordId")
        });
        // Handling the post-processing of method
        action.setCallback(this, function (response) {
            var state = response.getState();           
            console.log(JSON.stringify(response));
            if (state === "SUCCESS") { 
                location.reload();
                component.set('v.showSpinner', false);
            }else if(state == 'ERROR'){
                helper.openErrorModel(component, event, helper);  
                    component.set('v.showSpinner', false);   
            }
        });
        $A.enqueueAction(action);
        
    },
    // Below functions opens an error model window when field values doesn't match 
    openErrorModel: function(component, event, helper) {
        component.set("v.isModalOpen", true);
    },

    refreshDescriptionField: function(component, event, helper) {
        var descriptionAction = component.get("c.getDescriptionValue");
        descriptionAction.setParams({
            "recordId": component.get("v.recordId"),
            "showType": component.get("v.showType")
        });
        descriptionAction.setCallback(this, function(response) {
            var state = response.getState();
            if(state == "SUCCESS") {
                var descriptionData = response.getReturnValue();
                console.log("ContentLess: "+descriptionData.contentLess);
                console.log("DescriptionVal: " +descriptionData.descriptionVal);
                if(component.get("v.showType") == "Show Less") {
                    component.set("v.descriptionVal", descriptionData.descriptionVal);
                }
                else {
                    component.set("v.descriptionWindow", true);
                    component.set("v.completeDescription", descriptionData.descriptionVal);
                    helper.openErrorModel(component, event, helper); 
                }
                if(descriptionData.contentLess == false) {
                    component.set("v.showButton", true);
                }
                else {
                    component.set("v.showButton", false);
                }
            }
        });
        $A.enqueueAction(descriptionAction);       
    }
})
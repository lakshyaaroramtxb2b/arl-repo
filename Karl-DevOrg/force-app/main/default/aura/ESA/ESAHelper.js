({
	resetURL : function(cmp) {
        var resetURLJS = cmp.get("v.data.onResetURL");
        if (resetURLJS) {    
           resetURLJS(cmp.get("v.entityCode"), "", cmp.get("v.serviceItemId"), cmp.get("v.requestTemplate.Template_Name__c")); 
        }        
	},
    
    updateURL : function(cmp) {
        var resetURLJS = cmp.get("v.data.onResetURL");
        if (resetURLJS) {
            resetURLJS(cmp.get("v.entityCode"), 
                       cmp.get("v.hashKey"), 
                       // Ignore when service item id when hashkey is present
                       ($A.util.isEmpty(cmp.get("v.hashKey")) ? cmp.get("v.serviceItemId") : ""),
                       ($A.util.isEmpty(cmp.get("v.hashKey")) ? cmp.get("v.requestTemplate.Template_Name__c") : null)); 
        }        
	},
    
    resetNavDisplay : function(cmp) {
        var isNavOpen = true;
        var isServiceItemsHidden = cmp.get("v.config.settings.Service_Item_Categories_Display__c") == "Hidden";
        var isDocsEmpty = $A.util.isEmpty(cmp.get("v.documents"));    
        var isArticlesEmpty = $A.util.isEmpty(cmp.get("v.articles"));
        if (isServiceItemsHidden && isDocsEmpty && isArticlesEmpty) {
            isNavOpen = false;
        }
        cmp.set("v.isNavOpen", isNavOpen);
    },
    
    fetchVanityURLEntityCode : function (cmp, callback) {
        this.toggleBusy(cmp, true);
        var action = cmp.get("c.getVanityURLEntityCode");
        var entityCode = cmp.get("v.data.queryParams.entityCode");
        action.setParams({"entityCode": entityCode});
        
        action.setCallback(this, function(response) {
            this.toggleBusy(cmp, false);
            var THIS = this;
            $A.get("e.c:esaHandleResponse").setParams({
                "response": response,
                "messageId": "page",
                "onSuccess": $A.getCallback(function(returnVal) {
                    //console.log("vanityURLEntityCode");
                    //console.log(returnVal);                  
                    if (callback) {
                        // If vanity url returned either valid entitycode from vanityurl or just returns the passed in entitycode
                        // returns null when its invalid, pass some 'invalid' code to throw error in UI
                        callback(returnVal || (entityCode ? 'invalid' : null));
                    }
                })
            }).fire();            
        });
        $A.enqueueAction(action);
    },
    
    fetchDetails : function (cmp, callback, includePending) {        
        var hashKey = cmp.get("v.hashKey");
        var caseWorkNo = cmp.get("v.caseWorkNo");
        var entityCode = cmp.get("v.entityCode");
        this.toggleBusy(cmp, true);
        var action = cmp.get("c.getRequestDetails");
        action.setParams({"id" : hashKey || null, 
                          "entityCode" : entityCode,
                          "caseWorkNo" : caseWorkNo,
                          "includePending" : includePending ? 'true' : 'false'});// Parsing string to boolean on server-side
      
        action.setCallback(this, function(response) {
            this.toggleBusy(cmp, false);
            var THIS = this;
            $A.get("e.c:esaHandleResponse").setParams({
                "response": response,
                "messageId": "page",
                "onSuccess": $A.getCallback(function(returnVal) {
                    // console.log("config");
                    // console.log(returnVal.config);
                    // console.log("secReq");
                    // console.log(JSON.stringify(returnVal.securityRequest));
                    cmp.set("v.config", returnVal.config);
                    if (!THIS.isInActiveUserAndBlocked(returnVal.config)) {
                        THIS.setSecurityRequest(cmp, returnVal.securityRequest);                    
                        if (callback) {
                            callback(cmp);
                        }
                        THIS.resetNavDisplay(cmp);
                    }
                }),
                "onFinally" : $A.getCallback(function(returnVal) {
                    // Clear the caseNo, we only need it the first time
                    cmp.set("v.caseWorkNo", null);
                })
            }).fire();            
        });
        $A.enqueueAction(action);
    },
    
    setSecurityRequest : function (cmp, secReq) {
        // Set the selected entity code
        if ($A.util.isEmpty(secReq.Entity_Code__c)) {
            secReq.Entity_Code__c = cmp.get("v.entityCode");            
        }
        
        // Set the service item id
        if ($A.util.isEmpty(secReq.ESA_Service_Item__c)) {
            secReq.ESA_Service_Item__c = cmp.get("v.serviceItemId");            
        }
        
        //console.log(JSON.stringify(secReq));
        // Always reset the entitycode and hashKey to current Req
        cmp.set("v.entityCode", secReq.Entity_Code__c);
        cmp.set("v.hashKey", secReq.Hash_Key__c);
        cmp.set("v.serviceItemId", secReq.ESA_Service_Item__c);
        cmp.set("v.securityRequest", secReq);
        this.updateURL(cmp);
    },

    isInActiveUserAndBlocked: function (config) {
        // In .cmp file there is a hidden img tag which logsout user in the background
        // This login here again tries to logout and redirect to login page
        if ($A.util.getBooleanValue(config.isDeActivatedUser)) {
            $A.get("e.c:esaShowAlertEvt").setParams({
                "type": "error",
                "text": $A.get("$Label.c.ESA_DeActivatedUserMessage"),
                "onOk": function() {
                    $A.get("e.c:commonLogoutEvt").fire();
                }
            }).fire();
            return true;
        }
        return false;
    },

    setLogoutURL: function(cmp) {
        var cUtil = cmp.find("commonUtil");
        var logoutURL = cUtil.trimSuffix(cmp.get("v.data.site.prefix"), "/s") + "/secur/logout.jsp";
        cmp.set("v.logoutURL", logoutURL);
    },
    
    toggleBusy: function (cmp, isBusy) {
        if (isBusy) {
           cmp.find('busy').get('e.show').fire(); 
        } else {
           cmp.find('busy').get('e.hide').fire();
        }
    },
    
    clearMessages: function () {
        $A.get("e.c:esaClearMessage").setParams({
            "doNotClearSticky" : true
        }).fire();
    },
    
    clearAllMessages: function () {
        $A.get("e.c:esaClearMessage").setParams({
            "doNotClearSticky" : false
        }).fire();
    }
})
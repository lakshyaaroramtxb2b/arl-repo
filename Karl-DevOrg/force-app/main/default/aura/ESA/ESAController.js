({
	doInit : function(cmp, event, helper) {
        helper.setLogoutURL(cmp);
        var hashKey = cmp.get("v.data.queryParams.id");
        var serviceItemId = cmp.get("v.data.queryParams.si");
        var caseWorkNo = cmp.get("v.data.queryParams.caseno");
        var groupName = cmp.get("v.data.queryParams.groupName");
        var requestTemplate = cmp.get("v.data.requestTemplate");

        // VanityURL returns back same entityCode if no vanity url is set for that entity
        // Returns null when entityCode is not valid
        helper.fetchVanityURLEntityCode(cmp, function(entityCode) {
            // Set hashKeyId
            if (!$A.util.isEmpty(hashKey)) {
                cmp.set("v.hashKey", hashKey);
            } else if (!$A.util.isEmpty(serviceItemId)) {
                // Ignore service item id if hashkey is present
                cmp.set("v.serviceItemId", serviceItemId);
            } else if (!$A.util.isUndefinedOrNull(requestTemplate)) {
                cmp.set("v.requestTemplate", requestTemplate);
                cmp.set("v.serviceItemId", requestTemplate['ESA_Service_Item__c']);
            }
            
            // Set case work number
            if (!$A.util.isEmpty(caseWorkNo)) {
                cmp.set("v.caseWorkNo", caseWorkNo);
            }
            
            // Set entityCode
            cmp.set("v.entityCode", entityCode);
            if ($A.util.isEmpty(groupName) || !$A.util.isEmpty(entityCode) || !$A.util.isEmpty(hashKey)) {
               helper.updateURL(cmp);
            }

            if (!$A.util.isEmpty(hashKey) || !$A.util.isEmpty(entityCode) 
                 || (!$A.util.isEmpty(caseWorkNo) && !$A.util.isEmpty(entityCode))) {
                // Fetch pending only when serviceItem id is empty and not a template flow
                var includePending = $A.util.isUndefinedOrNull(cmp.get("v.requestTemplate")) && $A.util.isEmpty(cmp.get("v.serviceItemId"));
                helper.fetchDetails(cmp, function(cmp) { 
                    cmp.set("v.isReady", true);
                }, includePending); 
            } else {
                cmp.set("v.isReady", true);
            }
         });
	},
    
    toggleNav : function(cmp, event, helper) {
       var isNavOpen = $A.util.getBooleanValue(cmp.get("v.isNavOpen"));
       cmp.set("v.isNavOpen", !isNavOpen);
	},
    
    resetNav : function(cmp, event, helper) {
       helper.resetNavDisplay(cmp);  
    },

    handleLogout : function(cmp, event, helper) {
        window.location.href = cmp.get("v.logoutURL");
    },
    
    handleResponse: function (cmp, event, helper) {
        var cUtil = cmp.find("commonUtil");
        var response = event.getParam("response");
        var onSuccess = event.getParam("onSuccess");
        var onFailure = event.getParam("onFailure");
        var onFinally = event.getParam("onFinally");
        var msgId = event.getParam("messageId");
        var isSticky = $A.util.getBooleanValue(event.getParam("isStickyMsg"));
        var isIgnoreMsg = $A.util.getBooleanValue(event.getParam("isIgnoreServerMsg"));
        var state = response.getState();
        var esaResponse = response.getReturnValue() || {isError : false, data : response.getReturnValue()}; // Retains empty or null for data 
        var esaResponseHasError = esaResponse.isError || false;
        var esaResponseData = (esaResponse.hasOwnProperty("data") ? esaResponse.data : esaResponse);
        
        var process = function() {
            // Fire events, should before calling onSuccess, onFailure etc
            // as we are handling display of validatin errors etc in onFailure
            if (esaResponse.events) {
                for (var event in esaResponse.events) {
                    var eventParamsList = esaResponse.events[event];
                    // Multiple instances of same event but with different params
                    for (var i=0;i<eventParamsList.length;i++) {
                       $A.get(event).setParams(eventParamsList[i]).fire();
                    }
                } 
            }
            
            if (state == "SUCCESS" && !esaResponseHasError) {
                if (onSuccess) {
                    onSuccess(esaResponseData); 
                }  
            } else if (state == "INCOMPLETE") {
                $A.get("e.c:esaShowMessage").setParams({"id": msgId, "text": $A.get("$Label.c.ESA_TryAgain"), "variant": "warning"}).fire();
            } else if (state == "ERROR") {
                var errors = response.getError();
                if (errors) {
                    for (var i=0;i<errors.length;i++) {
                        $A.get("e.c:esaShowMessage").setParams({
                            "id": msgId,
                            "text": errors[i].message, 
                            "variant": "error", 
                            "isSticky" : false
                        }).fire();
                    }                  
                }  
                cUtil.checkSessionTimeout();
            }
            
            if (state != "SUCCESS" || esaResponseHasError) {
                if (onFailure) {
                    onFailure(esaResponseData); 
                }  
            }
    
            if (onFinally) {
                onFinally(esaResponseData); 
            }
        }
        
        var confirmMessages = [];
        if (esaResponse.messages) { // For non esaresponses its null
            for (var i=0;i<esaResponse.messages.length;i++) {
                var message = esaResponse.messages[i];
                if (message.type == 'message') {
                    if (!isIgnoreMsg && !$A.util.isEmpty(message.text)) {
                        $A.get("e.c:esaShowMessage").setParams({
                            "id": msgId,
                            "text": message.text, 
                            "variant": message.variant, 
                            "isSticky" : isSticky
                        }).fire();
                    }     
                } else if (message.type == 'alert') {
                    $A.get("e.c:esaShowAlertEvt").setParams({
                            "title": message.title,
                            "text": message.text
                    }).fire();
                } else if (message.type == 'confirm') {
                    confirmMessages.push(message);
                }
            }
        }
        
        var msgCount = confirmMessages.length;
        if (msgCount == 0) {
            process();
        } else {
            for (var i=0;i<msgCount;i++) {
                var isLast = (i == (msgCount-1));
                var message = confirmMessages[i];
                $A.get("e.c:commonConfirmEvt").setParams({
                    "headerText" : message.title,
                    "bodyText" : message.text,
                    "oKLabel" : message.okLabel,
                    "cancelLabel" : message.cancelLabel,
                    "onCancel" : $A.getCallback(function() {
                        process = function() {
                            $A.get("e.c:esaShowAlertEvt").setParams({
                                "type" : "error",
                                "title": $A.get("$Label.c.ESA_Error"),
                                "text": $A.get("$Label.c.ESA_Confirm_Action_OK_Fail")
                            }).fire();
                        };
                    }),
                    "onOk": isLast ? $A.getCallback(function() {process();}) : null // Only process after last confirm is handled
                }).fire();
            }
        }
    },
    
    handleEntitySelection : function(cmp, event, helper) {
        var selectedEntity = event.getParam("entity");
        var selectedEntityCode = selectedEntity.EntityCode__c;       
        var currentEntityCode = cmp.get("v.entityCode");
        //console.log("currentEntityCode=" + currentEntityCode);
        //console.log("selectedEntityCode=" + selectedEntityCode);
        if (selectedEntityCode != currentEntityCode) {
            cmp.set("v.entityCode", selectedEntityCode);
            cmp.set("v.hashKey", null);
            cmp.set("v.serviceItemId", null);
            helper.resetURL(cmp);
            helper.fetchDetails(cmp, function(cmp) {}, true);
        }
	},
	
	handleReset : function(cmp, event, helper) {
        var onAfterReset = event.getParam("onAfterReset");
        cmp.set("v.serviceItemId", event.getParam("serviceItemId"));
        cmp.set("v.caseWorkNo", event.getParam("caseWorkNo"));
        cmp.set("v.hashKey", null);
        var templateName = event.getParam("template");
        if ($A.util.isEmpty(templateName) 
                || (templateName != cmp.get("v.requestTemplate.Template_Name__c"))) {
            cmp.set("v.requestTemplate", null);
        }
        
        // Do not clear initial error messages such as invalid id / access denied
        if ($A.util.getBooleanValue(cmp.get("v.isReady"))) { 
           helper.clearAllMessages();
        }
        helper.resetURL(cmp);
        helper.fetchDetails(cmp, function(cmp) {
            cmp.set("v.isReady", true);
        }, $A.util.getBooleanValue(cmp.get("v.config.settings.Single_Open_Appointment__c"))); // Always load pending req if single open is true
        
        if (onAfterReset) {
            onAfterReset();
        }
	},
    
    handleSessionTimeout: function(cmp, event, helper) {
        helper.clearAllMessages();
    },
    
    handleRefresh: function(cmp, event, helper) {
        helper.clearMessages();
        var hashKey = event.getParam("hashKey");
        var secReq = event.getParam("securityRequest");
        //console.log(JSON.stringify(secReq));
        if (!$A.util.isUndefinedOrNull(secReq) && !$A.util.isEmpty(secReq.Hash_Key__c)) {
            hashKey = secReq.Hash_Key__c;
        } 
        
        if (!$A.util.isEmpty(hashKey)) {
            cmp.set("v.hashKey", hashKey);
            cmp.set("v.entityCode", null);
            helper.fetchDetails(cmp);
        }
    }
})
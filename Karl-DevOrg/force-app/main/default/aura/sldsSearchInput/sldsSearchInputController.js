({
	onInit : function(cmp, event, helper) {
		
	},
    
    updateCurrentText : function(cmp, event, helper) {
        var text = cmp.get("v.searchText") || "";
        if (text != cmp.get("v.currentSearchText")) {
            cmp.set("v.currentSearchText", text);
            if (helper.isSearchOnInputChange(cmp)) {
                helper.onChange(cmp, text);
            }            
        }
	},
    
    search : function(cmp, event, helper) {
        helper.fireOnChange(cmp);
    },
    
    searchOnEnter : function(cmp, event, helper) {
        if (!helper.isSearchOnInputChange(cmp)
           && event.getParams().keyCode == 13) {
           helper.fireOnChange(cmp);
        }
    },
    
    clearSearch : function(cmp, event, helper) {
        var searchText = cmp.get("v.searchText");
        if (!$A.util.isEmpty(searchText)) {
            cmp.set("v.searchText", "");
            // When search is not triggered with input changes
            // then on clear triger the search to refresh results
            if (!helper.isSearchOnInputChange(cmp)) {
               helper.fireOnChange(cmp);
            }
        }
    },
    
    getCurrentSearchText : function (cmp, event, helper) {
        return cmp.get("v.currentSearchText");
    },

    onBlur : function(cmp, event, helper) {
        cmp.getEvent("onBlur").fire();
    }
})
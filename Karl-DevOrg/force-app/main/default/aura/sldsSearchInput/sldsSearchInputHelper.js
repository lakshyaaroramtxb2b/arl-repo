({
	onChange : function(cmp, searchText) {
        var THIS = this;
        setTimeout($A.getCallback(function() {
            // Process only if the searchText is valid as user might have typed more chars during allowed delay
            var currentSearchText = cmp.get("v.currentSearchText");
            if (searchText == currentSearchText) {
                THIS.fireOnChange(cmp);
            }
        }), cmp.get("v.delayInMillis"));	
	},
    
    fireOnChange : function (cmp) {
        var onChangeEvt = cmp.getEvent("onChange");
        onChangeEvt.setParams({"data" : {"searchText" : cmp.get("v.currentSearchText")}});
        onChangeEvt.fire();
    },
    
    isSearchOnInputChange : function (cmp) {
        var isSearchButtonVisible = $A.util.getBooleanValue(cmp.get("v.showSearchButton")); 
        if (isSearchButtonVisible) {
            // If search button is visible don't trigger search on input changes
            return false;
        }
        return true;
    }
})
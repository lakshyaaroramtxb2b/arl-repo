({
	doInitCaseHistory : function(component,event,helper) {
        console.log("Inside 2nd comp doInit");
        var action = component.get('c.getAllFilterOptions');
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log(""+state);
            if(state == 'SUCCESS') {
                var filterOptions = response.getReturnValue();
                component.set('v.options', filterOptions);
                console.log('Filter Options: '+component.get('v.options'));   
            }
        });
        $A.enqueueAction(action);
        helper.getCaseHistoryAndCommentRecords(component,event,helper);
    },

    handleSelectChangeEvent : function(component, event, helper) {
        var items = event.getParam("values");
        console.log("Selected values are "+items);
        component.set("v.mySelectedItems", items);
        component.set("v.showPublicInboundComments", false);
        component.set("v.showPublicOutboundComments", false);
        component.set("v.showStatus", false);
        component.set("v.showOwnerChange", false);
        component.set("v.showPrivateInboundComments", false);
        component.set("v.showPrivateOutboundComments", false);
        helper.getCaseHistoryAndCommentRecords(component,event,helper);
        items.forEach(function(item) {
            if(item == "Select All") {
                component.set("v.showPublicInboundComments", true);
                component.set("v.showPublicOutboundComments", true);
                component.set("v.showStatus", true);
                component.set("v.showOwnerChange", true);
                component.set("v.showPrivateInboundComments", true);
                component.set("v.showPrivateOutboundComments", true);
            }
            else if(item == "Public Inbound Comments") {
                component.set("v.showPublicInboundComments", true);
            } 
            else if(item == "Public Outbound Comments") {
                component.set("v.showPublicOutboundComments", true);
            }
            else if(item == "Owner") {
                component.set("v.showOwnerChange", true);
            }
            else if(item == "Status") {
                component.set("v.showStatus", true);
            }
            else if(item == "Private Inbound Comments") {
            	component.set("v.showPrivateInboundComments", true);      
            }
            else if(item == "Private Outbound Comments") {
            	component.set("v.showPrivateOutboundComments", true);      
            }
        });
    },

    handleChangeOrder : function(component, event, helper) {
        var toggleValue = component.get('v.isChecked');
        console.log("Inside Change Order");
        helper.getCaseHistoryAndCommentRecords(component, event, helper);
    }
})
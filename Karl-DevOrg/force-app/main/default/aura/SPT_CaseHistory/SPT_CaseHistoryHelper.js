({
	getCaseHistoryAndCommentRecords : function(component, event, helper){
        console.log('Record Id: '+component.get("v.recordId"));
        var action = component.get("c.getCaseData");
        // set param to method  
        action.setParams({
            'caseID' : component.get("v.recordId"),
            'selectedPicklistValues' : component.get("v.mySelectedItems"),
            'isToggle': component.get('v.isChecked')
        });
        action.setCallback(this,function(response){
            console.log('Record Id First: '+component.get("v.recordId"));
            console.log('Response State: '+response.getState());
            var state= response.getState();
            $A.log(response);
            if(state == "SUCCESS"){
                var returnedValue = response.getReturnValue();
                console.log('Record Id Second: '+component.get("v.recordId"));
                console.log(returnedValue);
                component.set("v.items", returnedValue.caseWrapperList);
                component.set("v.caseStatus", returnedValue.currentCaseStatus);
                if(returnedValue.caseCreated != null) {
                 	component.set("v.caseOpen",returnedValue.caseCreated);
                }
                if(returnedValue.caseClosed != null) {
                 	component.set("v.caseClosed",returnedValue.caseClosed);
                }
            }
        });
        $A.enqueueAction(action);
    },
})
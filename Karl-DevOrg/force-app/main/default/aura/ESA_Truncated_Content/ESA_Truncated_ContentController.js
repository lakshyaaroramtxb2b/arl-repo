({
	doInit : function(cmp, event, helper) {

	},
    
    checkContent : function(cmp, event, helper) {
        setTimeout($A.getCallback(function() {
            helper.checkContent(cmp);
        }), 100);
	},
    
    toggleTruncation : function(cmp, event, helper) {
        helper.toggleTruncation(cmp);
	}
})
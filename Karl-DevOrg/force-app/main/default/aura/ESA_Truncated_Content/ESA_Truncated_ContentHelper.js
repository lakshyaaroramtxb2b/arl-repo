({
	setStyle : function(cmp, isExpand) {
		var style = "" ;
        if (!isExpand) {
            var maxHeight = cmp.get("v.maxHeightPx");
            style = "height:" + maxHeight + "px;" ;
        }
        cmp.set("v.style", style);
	},
    
    checkContent : function(cmp) {
        if (!$A.util.getBooleanValue(cmp.get("v.truncate"))) {
            return;
        }
		var maxHeight = cmp.get("v.maxHeightPx");
        var contentNode = cmp.find("content").getElement();
        var contentHeight = contentNode.clientHeight;
        var isLong = (contentHeight > maxHeight);
        cmp.set("v.isLong", isLong);
        this.setStyle(cmp, !isLong); // When long we truncate so isExpand is false 
        cmp.set("v.isTruncated", isLong); // When long text is truncated
	},
    
    toggleTruncation : function (cmp) {
        var isTruncated = $A.util.getBooleanValue(cmp.get("v.isTruncated"));
        this.setStyle(cmp, isTruncated ? true : false); // When in trucated state then we need to expand
        cmp.set("v.isTruncated", !isTruncated);
    }
})
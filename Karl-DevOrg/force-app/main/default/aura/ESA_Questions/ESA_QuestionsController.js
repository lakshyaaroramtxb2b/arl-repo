({
	doInit : function(cmp, event, helper) {
        helper.setCheckboxOptions(cmp);
        helper.fetchPriorityOptions(cmp); 
    },

    handleFieldError : function(cmp, event, helper) {
        var fieldName = event.getParam("errorField");
        var errorMsg = event.getParam("errorMessage");
        helper.addFieldError(cmp, fieldName, errorMsg);
    },
    
    processFiles : function(cmp, event, helper) {      
        // Files will be uploaded to chatter junc obj first as a workaround for master/detail issues
        // So, now after upload is successfull move them to sec req object
        helper.moveFilesFromChatterObjToSecReq(cmp, function() {
            helper.appendFilesForDisplay(cmp, event.getParam("files"));
        }); 
    },
    
    handleRemoveAttachment : function(cmp, event, helper) {
        var questionIndex = event.getSource().get("v.name"); 
        // Just cleanup on the client side, when saved server will clear attachment
        helper.clearAttachmentDetails(cmp, questionIndex);
    },
    
    handleUpload : function(cmp, event, helper) {
        helper.parseUploadChunks(cmp, event.target);
    },

    reset : function(cmp, event, helper) {
        // Do not allow manual edit
        // eg: date should always be selected from popup
        event.getSource().set("v.value", ""); 
        event.stopPropagation();
        event.preventDefault();
    },
    
    handleServiceItemSelection : function(cmp, event, helper) {
        // console.log(JSON.stringify(event.getParam("config")));
        // console.log(JSON.stringify(event.getParam("securityRequest")));
        //console.log(JSON.stringify(event.getParam("serviceItem")));
        var isClearTab = helper.canClearTab(cmp.get("v.securityRequest.Id"), event.getParam("securityRequest") ? event.getParam("securityRequest").Id : null);
        helper.clearQuestions(cmp, isClearTab);
        cmp.set("v.config", event.getParam("config"));
        cmp.set("v.securityRequest", event.getParam("securityRequest"));
        cmp.set("v.requestTemplate", event.getParam("requestTemplate"));
        cmp.set("v.serviceItem", event.getParam("serviceItem"));
        cmp.set("v.isInstructions",  cmp.get("v.serviceItem.Type__c") == 'Instructions');      
        helper.setIsTemplateCreationFlow(cmp); // Must be called after setting sec req
        helper.fetchQuestions(cmp);
    },
    
    handleEntitySelection : function(cmp, event, helper) {
        // On Entity change clear all questions data
        helper.clearQuestions(cmp);
    },
    
    toggleDependentDisplay : function(cmp, event, helper) { 
        var params;
        var inputCmp = event.getSource();
        if (inputCmp.isInstanceOf("lightning:radioGroup")) {
           var value = event.getParam("value");
            if ($A.util.isArray(value)) {
                // Seems a bug and fixed now but to be safe handle array case
                value = value[0];
            }
           params = {"id": inputCmp.get("v.name"), // Storing question id in the name
                     "value": value};
        } else {        
           params = {"id": inputCmp.get("v.label"), // Storing question id in the label
                     "value": inputCmp.get("v.value")};
        }
        //console.log(params);
        $A.get("e.c:toggleDisplayEvt").setParams(params).fire();
    },
    
    populateNeedByDate : function(cmp, event, helper) {
        var needByDate = event.getSource().get("v.value");
        helper.setSecurityRequestProp(cmp, "Need_By_Date__c", needByDate);
    },
    
    selectTab : function(cmp, event, helper) {
        var tabIndex = event.target.getAttribute("tabindex");
        cmp.set("v.selectedTab", cmp.get("v.tabs")[tabIndex].Id); 
    },
    
    handleSubmit : function(cmp, event, helper) { 
        helper.clearMessage(false);
        helper.populateAnswers(cmp);
        
        // Validate case number
        var caseNumber = helper.getCaseNumber(cmp);
        var entityCode = cmp.get("v.securityRequest.Entity_Code__c");
        cmp.set("v.isValidCaseNum", true); // Reset
        if (!$A.util.isEmpty(caseNumber)) { 
            helper.toggleBusy(cmp, true);
            var action = cmp.get("c.validateCaseWorkNumber");
            action.setParams({"caseNumber": caseNumber,
                              "entityCode": entityCode});
            action.setCallback(this, function(response) {
                $A.get("e.c:esaHandleResponse").setParams({
                    "response": response,
                    "onSuccess": $A.getCallback(function(returnVal) {
                        // If all other validations are successful then save
                        if (helper.isValid(cmp)) {
                           helper.confirmApptDateAndRunRules(cmp);
                        }
                    }),
                    "onFailure": $A.getCallback(function(returnVal) {
                        // Adds field error
                        cmp.set("v.isValidCaseNum", false);
                        // Run all other validations
                        helper.isValid(cmp);
                    })
                }).fire();
                helper.toggleBusy(cmp, false);
            });
            $A.enqueueAction(action);
        } else if (helper.isValid(cmp)) {
            helper.confirmApptDateAndRunRules(cmp);
        }
    },
    
    handleDiscard : function(cmp, event, helper) {        
        $A.get("e.c:commonConfirmEvt").setParams({
                "bodyText" : $A.get("$Label.c.ESA_ConfirmDiscard"),
                "onOk": $A.getCallback(function(returnVal) {
                    helper.clearMessage(false);
                    helper.toggleBusy(cmp, true);
                    var action = cmp.get("c.discardChanges");
                    action.setParams({"securityRequest" : cmp.get("v.securityRequest")});
                    action.setCallback(this, function(response) {            
                        $A.get("e.c:esaHandleResponse").setParams({
                            "response": response,
                            "onSuccess": $A.getCallback(function(returnVal) {
                                helper.reset(cmp, $A.getCallback(function() {
                                    helper.showMessage($A.get("$Label.c.ESA_DiscardSuccessMsg"), null, true);
                                }));                
                                                
                            })
                        }).fire();            
                        helper.toggleBusy(cmp, false);
                    });
                    $A.enqueueAction(action);
                })
         }).fire();         
    },
    
    handleExit : function(cmp, event, helper) {
        $A.get("e.c:commonConfirmEvt").setParams({
                "bodyText" : $A.get("$Label.c.ESA_ConfirmExit"),
                "onOk": $A.getCallback(function() {
                    helper.reset(cmp);
                })
        }).fire();
    },
    
    handleClone : function(cmp, event, helper) {
        $A.get("e.c:commonConfirmEvt").setParams({
                "bodyText" : $A.get("$Label.c.ESA_CloneHelp"),
                "onOk": $A.getCallback(function() {
                    helper.clearMessage(false);
                    helper.prepareForClone(cmp);
                    helper.populateAnswers(cmp);
                    // Doesn't validate case number for now
                    cmp.set("v.isValidCaseNum", true); // Reset
                    if (helper.isValid(cmp, true)) {
                       // Allows empty values for required fields
                       helper.saveForLater(cmp, true);
                    }
                 })
         }).fire();    
    },
    
    handleSaveForLater : function(cmp, event, helper) {
        helper.clearMessage(false);
        helper.populateAnswers(cmp);
        // Doesn't validate case number for now
        cmp.set("v.isValidCaseNum", true); // Reset
        if (helper.isValid(cmp, true)) {
           // Allows empty values for required fields
           helper.saveForLater(cmp);
        }
    },
    
    handleApptUpdate : function (cmp, event, helper) {
        helper.clearMessage(false);
        helper.processAppt(cmp, cmp.get("v.securityRequest"));
        helper.stopEvent(event);
    },

    handleTemplate : function(cmp, event, helper) {
        cmp.set("v.isTemplateCreationFlow", true);
        cmp.set("v.securityRequest.Template_Parent_Entity__c", cmp.get("v.config.settings.Id"));
        helper.isValidTemplateName(cmp, $A.getCallback(function() {
            cmp.getEvent("onSaveTemplate").fire();
        }));
    },

    handleReadonly: function(cmp, event, helper) {
        var isReadonly = $A.util.getBooleanValue(event.getSource().get("v.value"));
        if (isReadonly) {
            // Populate answers before displaying it as readonly
           helper.populateAnswers(cmp);
        }
    }
})
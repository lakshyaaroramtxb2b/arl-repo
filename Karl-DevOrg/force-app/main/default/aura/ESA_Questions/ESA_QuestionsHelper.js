({
	fetchQuestions : function(cmp) {     
        this.toggleBusy(cmp, true);
        var secReq = cmp.get("v.securityRequest");
        // Load from template if exist
        var useTemplate = $A.util.isEmpty(secReq.Id) && !$A.util.isEmpty(cmp.get("v.requestTemplate.Id"));
        if (useTemplate) {
            secReq = cmp.get("v.requestTemplate");
        }
		var action = cmp.get("c.getQuestions");
        action.setParams({"securityRequest": secReq});
        action.setCallback(this, function(response) {
            var THIS = this;
            $A.get("e.c:esaHandleResponse").setParams({
                "response": response,
                "onSuccess": $A.getCallback(function(returnVal) {
                    THIS.processQuestions(cmp, returnVal);
                    if (useTemplate) {
                        THIS.clearAllAttachments(cmp);
                    } else {
                        THIS.setAttachments(cmp);
                    }
                })
            }).fire();
            this.toggleBusy(cmp, false);
        });
        $A.enqueueAction(action);
    },
    
    setAttachments : function(cmp) {
        var cUtil = cmp.find('commonUtil');
        var filteredAttachments = [];
        var attachments = cmp.get("v.securityRequest.CombinedAttachments") || [];
        if (attachments.length > 0) {
            var questions = cmp.get("v.questions") || [];
            var answerAttachmentIds = [];
            cUtil.forEach(questions, function(question) {
                if (question.isAttachment && !$A.util.isEmpty(question.answerId)) {
                    answerAttachmentIds.push(question.answerId);
                }
            });
  
            cUtil.forEach(attachments, function(attachment) {
                if (answerAttachmentIds.indexOf(attachment.Id) == -1) {
                    filteredAttachments.push(attachment);
                }
            });
        }
        cmp.set("v.attachments", filteredAttachments);
    },

    appendFilesForDisplay : function(cmp, files) {
        var cUtil = cmp.find('commonUtil');
        var attachments = cmp.get("v.attachments");
        cUtil.forEach(files, function(file) {
            attachments.push({"Id": file.documentId, "Title": file.name});
        });
        cmp.set("v.attachments", attachments);
    },

    moveFilesFromChatterObjToSecReq : function(cmp, callback) {
        var THIS = this;
        THIS.toggleBusy(cmp, true);
        var cUtil = cmp.find('commonUtil');
        var action = cmp.get("c.moveFilesFromChatterJunc");
        action.setParams({"securityRequest": cmp.get("v.securityRequest")});
        cUtil.processAction(this, action, {
            "onSuccess": $A.getCallback(function(returnVal) {
                callback();
            }),
            "onFailure" : $A.getCallback(function(returnVal) {
                THIS.showMessage($A.get("$Label.c.ESA_FileUploadProcessingError"), 'error');
            }),
            "onFinally": $A.getCallback(function(returnVal) {
                THIS.toggleBusy(cmp, false);
            })
        });
    },

    setCheckboxOptions : function(cmp) {
        var checkboxOptions = cmp.get("v.checkboxOptions");
        checkboxOptions.push({
            "label" : $A.get("$Label.c.ESA_Yes"),
            "value" : "true"
        });
        checkboxOptions.push({
            "label" : $A.get("$Label.c.ESA_No"),
            "value" : "false"
        });
    },
    
    fetchPriorityOptions : function(cmp) {     
		var action = cmp.get("c.getPriorityOptions");
        action.setCallback(this, function(response) {
            var THIS = this;
            $A.get("e.c:esaHandleResponse").setParams({
                "response": response,
                "onSuccess": $A.getCallback(function(returnVal) {
                    cmp.set("v.priorityOptions", returnVal);
                })
            }).fire();
        });
        $A.enqueueAction(action);
	},
    
    getCaseNumber : function(cmp) {
        var isCaseNumAvailable = ($A.util.getBooleanValue(cmp.get("v.config.settings.Case_Work_No_Visible__c"))
                                  && $A.util.getBooleanValue(cmp.get("v.config.settings.Allow_user_to_enter_work_number__c"))
                                  && !$A.util.getBooleanValue(cmp.get("v.config.isCollaborator")));
        if (!isCaseNumAvailable) {
            return null;
        }
        var caseNum = cmp.get("v.securityRequest.Supportforce_Case_Number__c");
        return caseNum;
    },
    
    clearQuestions : function(cmp, isClearTab) { 
        try {
            cmp.set("v.questions", null);
            cmp.set("v.tabs", null);
            if (isClearTab) {
               cmp.set("v.selectedTab", null);
            }
            cmp.set("v.uploadChunksMap", null);
        } catch (e) {}
    },
    
    canClearTab : function(secReqId, updatedSecReqId) {
        if (!$A.util.isEmpty(secReqId) && !$A.util.isEmpty(updatedSecReqId) && secReqId != updatedSecReqId) {
            // Clear when id's not match
            return true;
        } else if ($A.util.isEmpty(updatedSecReqId)) {
            // Clean for new request
            return true;
        } 
        return false;
    },
    
    processQuestions : function (cmp, questions) { 
        if (!questions) {
            return;
        }
        var tabs = [{Id: '-', Name: 'General', hasError: false}];
        // This second array helps to check uniqueness 
        var uniqueTabs = [];
       
        for (var i=0;i<questions.length;i++) {
            var question = questions[i];
            // Handle tabs
            var tab = question.serviceItemQuestion.ESA_Service_Item_Tab__r;            
            if (tab && uniqueTabs.indexOf(tab.Id) == -1) {                
                uniqueTabs.push(tab.Id);
                var clonedTab = JSON.parse(JSON.stringify(tab));
                clonedTab.hasError = false;
                tabs.push(clonedTab);
            }
        }                         
        cmp.set("v.tabs", tabs);   
        if (uniqueTabs.indexOf(cmp.get("v.selectedTab")) == -1) {
            // Select the first tab by default
            cmp.set("v.selectedTab", tabs[0].Id);
        }
        // Set the parsed data 
        cmp.set("v.questions", questions);
        //console.log(questions);
        
    },
    
    confirmApptDateAndRunRules: function (cmp) { 
        if (this.isCalVisible(cmp) && this.isApptDateSelected(cmp)) {
            this.runRulesAndSubmitSecurityRequest(cmp, true);
        } else {
            this.runRulesAndSubmitSecurityRequest(cmp, false);
        } 
    },
    
    runRulesAndSubmitSecurityRequest: function (cmp, isApptDateSelected) { 
        var THIS = this;        
        THIS.toggleBusy(cmp, true);
        var questionsJSON =  JSON.stringify(cmp.get("v.questions"));
        var displayedQuestionsJSON = JSON.stringify(cmp.get("v.displayedQuestionsOnly"));
        var action = cmp.get("c.runPreRulesAndActions");
        var securityReq = cmp.get("v.securityRequest");
        if (isApptDateSelected) {
            // Clone security request so that we can add ApptDate for validaton purposes in formula without disturbing the original sec req
            // As actual selected appointment date will be set on the security request later in a separate ajax call
            securityReq = JSON.parse(JSON.stringify(securityReq));
            securityReq.Office_Hours_Reservation__c = cmp.find("apptCalendar").get("v.selectedDateTime");
        }
        action.setParams({"securityRequest" : securityReq,
                          "questionsJson" : questionsJSON,
                          "displayedQuestionsJson" : displayedQuestionsJSON});
        action.setCallback(this, function(response) {
            $A.get("e.c:esaHandleResponse").setParams({
                "response": response,
                "onSuccess": $A.getCallback(function(returnVal) {
                    THIS.submitSecurityRequest(cmp, isApptDateSelected, questionsJSON, displayedQuestionsJSON);
                }),
                "onFailure": $A.getCallback(function(returnVal) {
                    // If has field errors will display
                    THIS.displayFieldsHasErrors(cmp);
                })
            }).fire();
            THIS.toggleBusy(cmp, false);
        });
        $A.enqueueAction(action);
    },

    runPostSubmitRules: function (cmp, secReq, questionsJSON, displayedQuestionsJSON) { 
        var THIS = this;        
        THIS.toggleBusy(cmp, true);
        var action = cmp.get("c.runPostSaveRulesAndActions");
        
        action.setParams({"securityRequest" : secReq,
                          "questionsJson" : questionsJSON,
                          "displayedQuestionsJson" : displayedQuestionsJSON});
        action.setCallback(this, function(response) {
            $A.get("e.c:esaHandleResponse").setParams({
                "response": response,
                "onSuccess": $A.getCallback(function(returnVal) {
                    secReq = returnVal;
                }),
                "onFinally": $A.getCallback(function(returnVal) {
                    THIS.refresh(secReq);
                })
            }).fire();
            THIS.toggleBusy(cmp, false);
        });
        $A.enqueueAction(action);
    },
    
    submitSecurityRequest : function (cmp, isApptDateSelected, questionsJSON, displayedQuestionsJSON) { 
        var THIS = this;
        THIS.toggleBusy(cmp, true);
        var action = cmp.get("c.submitSecurityRequest");
        //console.log(JSON.stringify(cmp.get("v.questions")));
        //console.log(cmp.get("v.questions"));
        action.setParams({"securityRequest" : cmp.get("v.securityRequest"),
                          "questionsJson" : questionsJSON,
                          "displayedQuestionsJson" : displayedQuestionsJSON});
        action.setCallback(this, function(response) {  
            THIS.toggleBusy(cmp, false);
            $A.get("e.c:esaHandleResponse").setParams({
                "response": response,
                "isStickyMsg" : true,
                "onSuccess": $A.getCallback(function(returnVal) {  
                    THIS.toggleBusy(cmp, true);
                    THIS.completeUploadChunks(cmp, returnVal, $A.getCallback(function() {
                        THIS.toggleBusy(cmp, false);
                        if (isApptDateSelected) {
                            THIS.processAppt(cmp, returnVal, questionsJSON, displayedQuestionsJSON);
                        } else {
                            THIS.runPostSubmitRules(cmp, returnVal, questionsJSON, displayedQuestionsJSON);
                        }
                        THIS.showMessage($A.get("$Label.c.ESA_SuccessSubmitMessage"), null, true);
                    }));
                }),
                "onFailure": $A.getCallback(function(returnVal) { 
                    // Might come here when create ext work record is failed and server calls saveforlater and throw error
                    if (returnVal && returnVal.Id) {
                        THIS.showMessage($A.get("$Label.c.ESA_SuccessSaveMessage"), null, true);
                        if (isApptDateSelected) {
                           THIS.showMessage($A.get("$Label.c.ESA_ErrorApptSaveMessage"), "error", true);
                        }
                        THIS.toggleBusy(cmp, true);
                        THIS.completeUploadChunks(cmp, returnVal, $A.getCallback(function() {
                            THIS.toggleBusy(cmp, false);
                            THIS.refresh(returnVal);
                        }));
                    }
                })   
            }).fire();   
        });
        $A.enqueueAction(action);
    },
    
    saveForLater : function (cmp, isClone) {
        var THIS = this;
        var isTemplate = !$A.util.isEmpty(cmp.get("v.securityRequest.Template_Parent_Entity__c"));
        var isApptDateSelected = (this.isCalVisible(cmp) && this.isApptDateSelected(cmp));
        this.toggleBusy(cmp, true);
        var action = cmp.get("c.saveForLater");
        //console.log(JSON.stringify(cmp.get("v.questions")));
        //console.log(cmp.get("v.questions"));
        //console.log( JSON.stringify(cmp.get("v.securityRequest")));
        action.setParams({"securityRequest" : cmp.get("v.securityRequest"),
                          "questionsJson" : JSON.stringify(cmp.get("v.questions")),
                          "displayedQuestionsJson" : JSON.stringify(cmp.get("v.displayedQuestionsOnly"))});
        action.setCallback(this, function(response) {
            $A.get("e.c:esaHandleResponse").setParams({
                "response": response,
                "onSuccess" : $A.getCallback(function(returnVal) {
                    THIS.toggleBusy(cmp, true);
                    THIS.completeUploadChunks(cmp, returnVal, $A.getCallback(function() {
                        THIS.toggleBusy(cmp, false);
                        THIS.refresh(returnVal);
                        if (isClone) {
                            THIS.showMessage($A.get("$Label.c.ESA_SuccessCloneMessage"), null, true);  
                        } else if (isTemplate) {
                            THIS.showMessage($A.get("$Label.c.ESA_SuccessSaveTemplateMessage"), null, true);
                        } else {
                            THIS.showMessage($A.get("$Label.c.ESA_SuccessSaveMessage"), null, true);
                        }
                        
                        if (isApptDateSelected) {
                            THIS.showMessage($A.get("$Label.c.ESA_WarningApptDateIgnored"), "warning");
                        }
                    }));
                }),
                "onFailure" : $A.getCallback(function(returnVal) {
                    if (isClone) {
                        $A.get("e.c:esaShowAlertEvt").setParams({
                            "type": "error",
                            "text": $A.get("$Label.c.ESA_ErrorCloneMessage"),
                            "onOk": function() {
                                location.reload();
                            }
                        }).fire();
                    }
                })
            }).fire();            
            THIS.toggleBusy(cmp, false);
        });
        $A.enqueueAction(action);
    },
    
    processAppt : function (cmp, secReq, questionsJSON, displayedQuestionsJSON) {
        secReq.Time_Zone__c = cmp.get("v.timezone");
        var calCmp = cmp.find("apptCalendar");
        var uglyApptDateTime = calCmp.get("v.uglySelectedDateTime");
        // console.log(apptDateTime);
        // console.log(uglyApptDateTime);
        // console.log(JSON.stringify(secReq));
        this.toggleBusy(cmp, true);
        var action = cmp.get("c.reserveAppointment");
        action.setParams({"securityRequest" : secReq,
                          "apptDateTime" : uglyApptDateTime});
        action.setCallback(this, function(response) {
            var THIS = this;
            $A.get("e.c:esaHandleResponse").setParams({
                "response": response,
                "isStickyMsg": true,
                "onSuccess" : $A.getCallback(function(returnVal) {
                    //console.log(returnVal);
                    secReq = returnVal;
                    //THIS.showMessage($A.get("$Label.c.ESA_SuccessApptSaveMessage"), "success", true);
                }),
                "onFailure" : $A.getCallback(function(returnVal) {
                    //THIS.showMessage($A.get("$Label.c.ESA_ErrorApptSaveMessage"), "error", true);
                    THIS.addErrorAndRefreshCalendar(cmp);
                }),
                "onFinally" : $A.getCallback(function(returnVal) {
                    if (questionsJSON && displayedQuestionsJSON) {
                        THIS.runPostSubmitRules(cmp, secReq, questionsJSON, displayedQuestionsJSON);
                    } else {
                        THIS.refresh(secReq);
                    }
                })
            }).fire();            
            this.toggleBusy(cmp, false);
        });
        $A.enqueueAction(action);
    },
    
    prepareForClone: function (cmp) {
        var secReq = cmp.get("v.securityRequest");
        // Reset security request with selective fields to copy
        cmp.set("v.securityRequest", {
            "Entity_Code__c" : secReq.Entity_Code__c,
            "ESA_Service_Item__c" : secReq.ESA_Service_Item__c,
            "Priority__c" : secReq.Priority__c,
            "Subject__c" : secReq.Subject__c,
            "Description__c" : secReq.Description__c
        });
        
        // Reset Appt Dates
        var calCmp = cmp.find("apptCalendar");
        calCmp.getEvent("reset").fire();
        
        // Clear attachments and answers object
        this.clearAllAttachments(cmp);
    },

    clearAllAttachments: function(cmp) {
        cmp.set("v.uploadChunksMap", null);
        var questions = cmp.get("v.questions") || [];
        for (var i=0;i<questions.length;i++) {
            var q = questions[i];
            if (q.isAttachment) {
                this.clearAttachmentDetails(cmp, i);
            } 
        }
    },
    
    populateAnswers : function (cmp) {
        var esaUtil = cmp.find("esaUtil");
        var questions = cmp.get("v.questions");     
        var questionsAndAnswers = [];   
        var inputCmps = this.toArray(cmp.find("questionInputWrapper"));
        for (var i=0;i<inputCmps.length;i++) {
            var inputWrapperCmp = inputCmps[i];
            var q = questions[inputWrapperCmp.get("v.dataIndex")];

            if (!q || !inputWrapperCmp.isValid()) {
               continue; 
            }

            var inputCmp = esaUtil.findCmpByType(inputWrapperCmp, "c:ESA_Lookup")
                        || esaUtil.findCmpByType(inputWrapperCmp, "ui:inputText")
                        || esaUtil.findCmpByType(inputWrapperCmp, "ui:inputNumber")
                        || esaUtil.findCmpByType(inputWrapperCmp, "ui:inputTextArea")
                        || esaUtil.findCmpByType(inputWrapperCmp, "ui:inputSelect")
                        || esaUtil.findCmpByType(inputWrapperCmp, "ui:inputDate");

            if (inputCmp) {
                q.answer = this.parseEmpty(inputCmp.get("v.value")); 
                if (q.isLookup) {
                    q.answerId = this.parseEmpty(inputCmp.get("v.id"));   
                } else if (q.isMultiPicklist && q.answer) {
                    q.answer = q.answer.replace(/;/g, ",");            
                }  
            } 

            if (esaUtil.isQuestionVisible(q)) {
                questionsAndAnswers.push(q); 
            }
        }
        
        var inputRadioCmps = this.toArray(cmp.find("inputRadio"));
        
        // Aura two way bining is storing array so pick the selected radio value which is first
        for (var i=0;i<questions.length;i++) {
            var q = questions[i];
            if (q.isCheckbox) {
                for (var j=0;j<inputRadioCmps.length;j++) {
                    var inputRadioCmp = inputRadioCmps[j];
                    if (inputRadioCmp.isValid() &&
                        inputRadioCmp.get("v.name") == q.serviceItemQuestion.ESA_Security_Question__c) {
                        var value = inputRadioCmp.get("v.value");
                        if ($A.util.isArray(value)) {                            
                            value = value[0];                            
                        }
                        q.answer = $A.util.getBooleanValue(value);
                        break;
                    }
                } 
                if (esaUtil.isQuestionVisible(q)) {
                    questionsAndAnswers.push(q);
                }                             
            }
        }

        cmp.set("v.displayedQuestionsOnly", questionsAndAnswers);
    },
                
    setSecurityRequestProp: function(cmp, fieldName, fieldValue, allowEmpty) {
        if (fieldValue || allowEmpty) {
           cmp.set("v.securityRequest." + fieldName, fieldValue);
        }
    },
    
    isValid: function (cmp, allowEmpty) {
        var isValid = true;
        var questions = cmp.get("v.questions");

        // Clear errors on radio which are not wrapped by sldsFormElement
        var radioCmps = this.toArray(cmp.find("questions").find({"instancesOf" : "lightning:radioGroup"}));
        for (var i=0;i<radioCmps.length;i++) {
            var radioCmp = radioCmps[i];
            radioCmp.setCustomValidity(null);
            radioCmp.reportValidity();
        }

        var inputCmps = this.toArray(cmp.find("questions").find({"instancesOf" : "c:sldsFormElement"}));
        for (var i=0;i<inputCmps.length;i++) {
            var fieldIsValid = true;
            var inputWrapperCmp = inputCmps[i];
            // Reset errors
            inputWrapperCmp.set("v.hasError", false); 
            inputWrapperCmp.set("v.errorMessage", null);
            
            if (inputWrapperCmp.getLocalId() == "commonInputWrapper") {
                var inputCmp = inputWrapperCmp.get("v.body[0]");                 
                fieldIsValid = allowEmpty || this.isRequiredValuePresent(inputCmp)
            } else {
                var q = questions[inputWrapperCmp.get("v.dataIndex")];
                var lookupCmps = this.toArray(inputWrapperCmp.find({"instancesOf" : "c:ESA_Lookup"}));
                if (!$A.util.isEmpty(lookupCmps)) {
                    fieldIsValid = allowEmpty || this.isRequiredValuePresent(lookupCmps[0]);
                } else {
                    var inputTextCmps = this.toArray(inputWrapperCmp.find({"instancesOf" : "ui:inputText"}));
                    if (!$A.util.isEmpty(inputTextCmps)) {
                        var inputTextCmp = inputTextCmps[0];
                        fieldIsValid = allowEmpty || this.isRequiredValuePresent(inputTextCmp);
                        if (fieldIsValid) {
                            // Check length email                     
                            if (q && q.isText) {
                            fieldIsValid = this.isValidLength(inputTextCmp, 255);
                            } else if (q && q.isEmail) {
                            fieldIsValid = this.isValidEmail(inputTextCmp);
                            }
                        }
                    } else {                    
                        var inputNumberCmps = this.toArray(inputWrapperCmp.find({"instancesOf" : "ui:inputNumber"}));
                        if (!$A.util.isEmpty(inputNumberCmps)) {
                            var inputNumberCmp = inputNumberCmps[0];
                            fieldIsValid = allowEmpty || this.isRequiredValuePresent(inputNumberCmp);
                        } else {
                            var inputTextAreaCmps = this.toArray(inputWrapperCmp.find({"instancesOf" : "ui:inputTextArea"}));
                            if (!$A.util.isEmpty(inputTextAreaCmps)) {
                                var inputTextAreaCmp = inputTextAreaCmps[0];
                                fieldIsValid = allowEmpty || this.isRequiredValuePresent(inputTextAreaCmp);
                                if (fieldIsValid) {
                                    // Check length, number, email                     
                                    if (q && q.isLongText) {
                                    fieldIsValid = this.isValidLength(inputTextAreaCmp, 32000);
                                    } 
                                }
                            } else {
                                var inputSelectCmps = this.toArray(inputWrapperCmp.find({"instancesOf" : "ui:inputSelect"}));
                                if (!$A.util.isEmpty(inputSelectCmps)) {
                                    fieldIsValid = allowEmpty || this.isRequiredValuePresent(inputSelectCmps[0]);
                                } else {
                                    var inputDateCmps = this.toArray(inputWrapperCmp.find({"instancesOf" : "ui:inputDate"}));
                                    if (!$A.util.isEmpty(inputDateCmps)) {
                                        fieldIsValid = allowEmpty || this.isRequiredValuePresent(inputDateCmps[0]);
                                    } else {
                                        if (q.isAttachment) {
                                            fieldIsValid = allowEmpty || this.isRequiredFilePresent(inputWrapperCmp, q);
                                            if (fieldIsValid) {
                                                fieldIsValid = this.isValidFile(cmp, q);
                                                if (!fieldIsValid) {
                                                    inputWrapperCmp.set("v.errorMessage", $A.get("$Label.c.ESA_AttachmentMaxSizeLimit").replace("{0}", cmp.get("v.maxAttachSizeMB")));
                                                } 
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
                        
            if (!fieldIsValid) {
               isValid = false;
               inputWrapperCmp.set("v.hasError", true); 
            }
        }       
        
        /*var inputRadioCmps = this.toArray(cmp.find("inputRadio"));
        for (var i=0;i<inputRadioCmps.length;i++) {
            var inputRadioCmp = inputRadioCmps[i];
            if(!inputRadioCmp.get("v.required") || (inputRadioCmp.get("v.required") && inputRadioCmp.checkValidity())) {                  
                inputRadioCmp.set("v.class", '');
            } else {     
                //isValid = false; 
                inputRadioCmp.set("v.class", 'slds-has-error');
            }                               
        }*/
        
        var inputCommonCmps = this.toArray(cmp.find("commonInput"));        
        for (var i=0;i<inputCommonCmps.length;i++) { 
            var inputCommonCmp = inputCommonCmps[i];
            var cssClasses = inputCommonCmp.get("v.class") || ""; 
            if(!allowEmpty && inputCommonCmp.checkValidity && !inputCommonCmp.checkValidity()) {       
                inputCommonCmp.reportValidity();           
                isValid = false;
            } 
        }
        
        // Appt Date validation
        if (!this.isValidApptDate(cmp)) {
            isValid = false;
        }
        // Displays form level message and also highlights tabs with errors
        this.displayFieldsHasErrors(cmp, !isValid);
        
        return isValid;
    },
    
    displayFieldsHasErrors : function(cmp, hasErrors) {

        var tabs = cmp.get("v.tabs") || [];
        // Clear all error Tab
        for (var i=0;i<tabs.length;i++) {
           cmp.set("v.tabs[" + i + "].hasError", false);  
        }

        // Set error on first tab if any common input has errors
        var inputCommonCmps = this.toArray(cmp.find("commonInput"));        
        for (var i=0;i<inputCommonCmps.length;i++) { 
            var inputCommonCmp = inputCommonCmps[i];
            if (inputCommonCmp.checkValidity && !inputCommonCmp.checkValidity()) {
                cmp.set("v.tabs[" + 0 + "].hasError", true); 
                break;
            }
        }

        // Set error on tabs if any radio button has errors
        var radioCmps = this.toArray(cmp.find("questions").find({"instancesOf" : "lightning:radioGroup"}));
        for (var i=0;i<radioCmps.length;i++) {
            var radioCmp = radioCmps[i];
            if (!radioCmp.checkValidity()) {
                hasErrors = true;
                var classValue = radioCmp.get("v.class") || '';
                var tabIndex = classValue.split(",")[0];
                
                if (tabIndex < tabs.length) { // safe check
                   cmp.set("v.tabs[" + tabIndex + "].hasError", true); 
                }
            }
        }

        // Set error on tabs if any question inputs has errors
        var inputCmps = this.toArray(cmp.find("questions").find({"instancesOf" : "c:sldsFormElement"}));
        for (var i=0;i<inputCmps.length;i++) {
            var inputWrapperCmp = inputCmps[i];
            var tabIndex = inputWrapperCmp.get("v.tabIndex");
            if ($A.util.getBooleanValue(inputWrapperCmp.get("v.hasError"))) {
                hasErrors = true;
                if (tabIndex < tabs.length) { // safe check
                   cmp.set("v.tabs[" + tabIndex + "].hasError", true); 
                }
            }
        }

        if (hasErrors) {
            this.showMessage($A.get("$Label.c.ESA_FieldValidationError"), "error");
        } 
    },

    addFieldError: function(cmp, fieldName, errorMsg) {
        var cUtil = cmp.find('commonUtil');
        // Handle Radio Buttons
        var radioCmps = this.toArray(cmp.find("questions").find({"instancesOf" : "lightning:radioGroup"}));
        for (var i=0;i<radioCmps.length;i++) {
            var radioCmp = radioCmps[i];
            var classNames = radioCmp.get("v.class") || '';
            if (classNames.indexOf(fieldName) != -1) {
                radioCmp.setCustomValidity(cUtil.toText(errorMsg)); // Does not support rich text, hence to text
                radioCmp.reportValidity();
            }
        }

        // Handle Input Wrapper - sldsFormElement
        var inputCmps = this.toArray(cmp.find("questions").find({"instancesOf" : "c:sldsFormElement"}));
        for (var i=0;i<inputCmps.length;i++) {
            var inputCmp = inputCmps[i];
            if (fieldName == inputCmp.get("v.name")) {
                inputCmp.set("v.hasError", true);
                inputCmp.set("v.errorMessage", errorMsg);
            }
        }
    },
    
    isRequiredFilePresent: function(inputCmp, question) {
        if (!inputCmp.get("v.required") || !$A.util.isEmpty(question.answer)) { 
            return true;
        }
        return false;
    },
    
    isValidFile: function(cmp, question) {
        // Check whether valid size, type etc
        // console.log(question.theAttachmentWrapper);
        if (question.theAttachmentWrapper) { 
            return this.isValidFileSizeMB(cmp, question.theAttachmentWrapper.size);
        }
        return true;
    },
    
    isValidFileSizeMB : function(cmp, size) {
        var maxFileSizeMB = parseFloat(cmp.get("v.maxAttachSizeMB"));
        if (size > (maxFileSizeMB*1000*1000)) {
           return false;
        }
        return true;
    },

    isValidTemplateName: function(cmp, callback) {
        var templateNameInput = cmp.find("templateName")
        if(templateNameInput) { 
            var templateName = templateNameInput.get("v.value");
            if ($A.util.isEmpty(templateName)) { 
                this.showMessage($A.get("$Label.c.ESA_TemplateNameError"), "error");
                templateNameInput.focus();
            } else {
                var action = cmp.get("c.isUniqueTemplateName");
                action.setParams({"templateName" : templateName,
                                "secReqId" : cmp.get("v.securityRequest.Id"),
                                "entityCode" : cmp.get("v.securityRequest.Entity_Code__c")});
                action.setCallback(this, function(response) {
                    this.toggleBusy(cmp, false);
                    var THIS = this;
                    $A.get("e.c:esaHandleResponse").setParams({
                        "response": response,
                        "isStickyMsg": false,
                        "onSuccess" : $A.getCallback(function(returnVal) {
                            var isUniqueName = $A.util.getBooleanValue(returnVal);
                            if (!isUniqueName) {
                                THIS.showMessage($A.get("$Label.c.ESA_TemplateNameError"), "error");
                                templateNameInput.focus();
                            } else {
                                callback();
                            }
                        })
                    }).fire();            
                });
                this.toggleBusy(cmp, true);
                $A.enqueueAction(action);
            }
        } 
    },
    
    isRequiredValuePresent: function(inputCmp) {
        var isRequiredValuePresent = true;
        if (inputCmp.get("v.required") && $A.util.isEmpty(inputCmp.get("v.value"))) {  
            isRequiredValuePresent = false;
        } 
        // Error will be added to wrapper cmp - red border
        return isRequiredValuePresent;
    },
    
    isValidLength: function(inputCmp, length) {
        var isValidLength = true;
        var value = inputCmp.get("v.value");
        //console.log(value.length);
        if (!$A.util.isEmpty(value) && value.length > length) { 
            // Add error
            inputCmp.set("v.errors", [{"message": "Maximum allowed length is " + length + " characters"}]);
            isValidLength = false;            
        } else {
            // Clear errors
            inputCmp.set("v.errors", null);
        }
        return isValidLength;
    },
    
    isValidValue: function(inputCmp) { // Generic check to if field is valid by storing a value in messageWhenBadInput attribute
        var isValid = !$A.util.getBooleanValue(inputCmp.get("v.messageWhenBadInput"));
        var value = inputCmp.get("v.value");
        // console.log(inputCmp.get("v.name") + '=' + value + '-' + isValid);
        if (!$A.util.isEmpty(value) && !isValid) { 
            // Add error
            return false;         
        } else {
            return true;
        }
    },
    
    isValidEmail: function(inputCmp) {
        var isValidEmail = true;
        var value = inputCmp.get("v.value") || '';
        value = value.replace(/^\s+|\s+$/gm,''); // Trim 
        // characters followed by an @ sign, followed by more characters, and then a "."
        //  After the "." sign, you can only write 2 to 3 letters from a to z:
        var emailPattern = /[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,3}$/;
        if (!$A.util.isEmpty(value) && $A.util.isUndefinedOrNull(value.match(emailPattern))) { 
            // Add error
            inputCmp.set("v.errors", [{"message": "Please enter valid email"}]);
            isValidEmail = false;            
        } else {
            // Clear errors
            inputCmp.set("v.errors", null);
        }
        return isValidEmail;
    }, 
    
    isValidApptDate: function(cmp, isClearErrorOnly) {
        var calCmp = cmp.find("apptCalendar");
        calCmp.set("v.hasError", false);        
        if (!isClearErrorOnly 
                && (this.isCalVisible(cmp) && this.isApptDateNeeded(cmp) && !this.isApptDateSelected(cmp))) {
           calCmp.set("v.hasError", true);
           return false;
        }
        return true;
    },

    isCalVisible: function(cmp) {
        var calCmp = cmp.find("apptCalendar");
        return $A.util.getBooleanValue(calCmp.get("v.isVisible"));
    },
    
    isApptDateNeeded: function(cmp) {
        var calCmp = cmp.find("apptCalendar");
        return $A.util.getBooleanValue(calCmp.get("v.isAppointmentNeeded"));
    },
    
    isApptDateSelected: function(cmp) {
        var calCmp = cmp.find("apptCalendar");
        var apptDateTime = calCmp.get("v.selectedDateTime");
        return !$A.util.isEmpty(apptDateTime);     
    },
    
    toArray: function (elements) {
        var elemArray = [];
        if (!$A.util.isEmpty(elements)) {            
            if ($A.util.isArray(elements)) {
              return elements;
            } else {
              elemArray.push(elements);
            }            
        }
        return elemArray;
    },
    
    toggleBusy: function (cmp, isShow) {
        if (isShow) {
           cmp.find('busy').get('e.show').fire(); 
        } else {
           cmp.find('busy').get('e.hide').fire();
        }
    },
    
    showMessage: function (message, variant, isSticky) {
        $A.get("e.c:esaShowMessage").setParams({
            "text": message, 
            "variant": (variant || "success"), 
            "isSticky" : isSticky || false
        }).fire();
    },
    
    clearMessage: function (doNotClearSticky) {
        $A.get("e.c:esaClearMessage").setParams({
            "doNotClearSticky" : doNotClearSticky
        }).fire();
    },
    
    reset: function (cmp, onAfterReset) {
        var resetEvt = $A.get("e.c:esaReset");
        resetEvt.setParams({
            "serviceItemId" : cmp.get("v.securityRequest.ESA_Service_Item__c"), // Resets to current service item selection
            "onAfterReset" : onAfterReset
        }); 
        resetEvt.fire();
    },
    
    refresh: function (secReq, hashKey) {
        var refreshEvt = $A.get("e.c:esaRefreshEvt");
        if (!$A.util.isUndefinedOrNull(secReq)) {
            refreshEvt.setParams({
                "securityRequest" : secReq
            });
        } else if (!$A.util.isUndefinedOrNull(hashKey)) {
            refreshEvt.setParams({
                "hashKey" : hashKey
            });
        }
        refreshEvt.fire();
    },
    
    addErrorAndRefreshCalendar : function (cmp) {
        var calCmp = cmp.find("apptCalendar");
        calCmp.set("v.hasError", true);
        calCmp.getEvent("refresh").fire();
    },
    
    parseEmpty : function (value) {
        if ($A.util.isEmpty(value)) {
            return null;
        }
        return value;
    },
    
    stopEvent: function (event) {
        if (!$A.util.isUndefinedOrNull(event)) {
            event.stopPropagation();
            event.preventDefault();            
        }
    },
    
    clearAttachmentDetails: function (cmp, questionIndex) {
        cmp.set("v.questions[" + questionIndex + "].answer", null);
        cmp.set("v.questions[" + questionIndex + "].answerId", null);
        cmp.set("v.questions[" + questionIndex + "].theAttachmentWrapper", null);
    },
    
    hasAttachmentChunks : function(cmp) {        
        var chunksMap = cmp.get("v.uploadChunksMap");
        if (chunksMap != null) {
            for(var key in chunksMap) {
                var chunks = chunksMap[key].chunks;
                if (!$A.util.isEmpty(chunks)) {
                   return true;
                }
            } 
        }
        return false;
    }, 
    
    completeUploadChunks : function(cmp, secReq, callback) {
        var hasChunks = this.hasAttachmentChunks(cmp);
        if (!hasChunks) {
            callback();
            return;
        }
        
        var THIS = this;
        var action = cmp.get("c.getAnswersWithAttachments");
        action.setParams({"securityRequest" : secReq});
        action.setCallback(this, function(response) {
            $A.get("e.c:esaHandleResponse").setParams({
                "response" : response,
                "isIgnoreMsg" : true,
                "onSuccess" : $A.getCallback(function(quesWithAttachment) {
                    if ($A.util.isArray(quesWithAttachment) && quesWithAttachment.length > 0) {
                        var uploadAttachment = function() {
                            var q = quesWithAttachment.shift();
                            var isLastItem = (quesWithAttachment.length == 0);
                            var chunkCounter = 0;
                            chunkCounter += THIS.uploadChunks(cmp, secReq, q, $A.getCallback(function() {
                                // Execute async as counter need to get incremented before getting here
                                setTimeout($A.getCallback(function() {
                                    chunkCounter--;
                                    // console.log("chunkCounter="+chunkCounter);
                                    if (chunkCounter == 0) {
                                        if (isLastItem) {
                                           // console.log("IS LAST");
                                           callback(); 
                                        } else {
                                           uploadAttachment(); 
                                        }
                                    } 
                                }), 0);
                            })); 
                        }
                        uploadAttachment();
                    } else {
                       callback(); 
                    }
                }),
                "onFailure" : $A.getCallback(function(returnVal) {
                    callback();
                    THIS.showMessage($A.get("$Label.c.ESA_ErrorUploadMessage"), "error", true);
                })
            }).fire();
        });
        $A.enqueueAction(action);
    },
    
    uploadChunks : function(cmp, secReq, quesWithAttach, callback) { 
        var THIS = this;
        var hasErrors = false;
        var uploadChunksMap = cmp.get("v.uploadChunksMap") || {};
        var chunkVal = uploadChunksMap[quesWithAttach["ESA_Security_Question__c"]];
        var attachmentId = quesWithAttach["AttachmentId__c"];
        //console.log(JSON.stringify(uploadChunksMap));
        //console.log(JSON.stringify(quesWithAttach));
        var enqueueUploadAction = function(chunks) {
            var data = chunks.shift();
            var isEOF = (chunks.length == 0); // After shift if nothing left then this is EOF
            
            var processNext = function() {
                callback();
                if (!isEOF) {
                    enqueueUploadAction(chunks); // Call recurrsively
                }
            };
            
            if (hasErrors) {
                processNext();
            } else {
                var action = cmp.get("c.appendToAttachment");
                action.setParams({"securityRequest" : secReq,
                                  "attachmentId" : attachmentId,
                                  "base64Data" : data,
                                  "isEOF" : isEOF});
                action.setCallback(THIS, function(response) {  
                    $A.get("e.c:esaHandleResponse").setParams({
                        "response" : response,
                        "isIgnoreMsg" : true,
                        "onFailure" : $A.getCallback(function() {
                            hasErrors = true;
                            THIS.removeAttachment(cmp, secReq, attachmentId, chunkVal.questionIndex);
                            THIS.showMessage($A.get("$Label.c.ESA_ErrorUploadMessage") + " - " + quesWithAttach["Answer__c"], "error", true);
                        }),
                        "onFinally" : $A.getCallback(function() {
                            processNext();
                        })
                    }).fire();            
                });
                $A.enqueueAction(action);
            }
        };
        if (chunkVal && $A.util.isArray(chunkVal.chunks) && chunkVal.chunks.length > 0) {
            var chunksCount = chunkVal.chunks.length;
            enqueueUploadAction(chunkVal.chunks);
            return chunksCount;
        }
        callback();
        return 1;
    }, 
    
    parseUploadChunks : function(cmp, htmlFileElem) {
        var inputFiles = htmlFileElem.files;
        var questionIndex = htmlFileElem.getAttribute("data-index");
        //console.log("questionIndex=" + questionIndex);
        if (inputFiles && inputFiles.length > 0) {
            var inputFile = inputFiles[0];
            var question = cmp.get("v.questions")[questionIndex]; 
            // console.log(inputFile);
            // console.log(JSON.stringify(question));
            if (inputFile && question.isAttachment) {             
                var reader = new FileReader();
                reader.onload = $A.getCallback(function() {
                    var fileContents = reader.result;
                    var startPosition = 0;
                    var endPosition = 0;
                    var eofPosition = fileContents.length;
                    var chunkSize = cmp.get("v.attachChunkSizeKB")*1000;

                    var getNextChunk = function(size) {
                        endPosition = Math.min(eofPosition, startPosition + size);
                        return window.btoa(fileContents.substring(startPosition, endPosition));  
                    };

                    // Populate the question with first chunk
                    cmp.set("v.questions[" + questionIndex + "].answer", inputFile.name);
                    cmp.set("v.questions[" + questionIndex + "].theAttachmentWrapper", {
                                  "name": inputFile.name,
                                  "contentType": inputFile.type,
                                  "size": inputFile.size,
                                  "body": ""
                    });
                    // Push rest into Map for upload as chunks 
                    var uploadChunksMap = cmp.get("v.uploadChunksMap") || {};
                    var questionId = question.serviceItemQuestion.ESA_Security_Question__c;
                    //console.log("questonId=" + questionId);
                    var pushNextChunk = function() {
                        if (endPosition < eofPosition) {    
                            startPosition = endPosition;
                            var chunks = uploadChunksMap[questionId] ? uploadChunksMap[questionId].chunks : [];
                            chunks.push(getNextChunk(chunkSize)); 
                            uploadChunksMap[questionId] = {
                                "chunks" : chunks,
                                "questionIndex" : questionIndex
                            };
                            // Call reccursively until EOF
                            pushNextChunk();
                        }
                    };
                    pushNextChunk();
                    cmp.set("v.uploadChunksMap", uploadChunksMap);
                });
                reader.readAsBinaryString(inputFile);   
            } 
        } 
    },

    setIsTemplateCreationFlow : function(cmp) {
        var isTemplateCreationFlow =  !$A.util.isEmpty(cmp.get("v.securityRequest.Template_Parent_Entity__c"));
        cmp.set("v.isTemplateCreationFlow", isTemplateCreationFlow);
    }
})
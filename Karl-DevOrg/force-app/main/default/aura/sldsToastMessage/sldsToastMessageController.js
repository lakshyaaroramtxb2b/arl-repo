({
    doInit : function(cmp, event) {
        
	},
    
    close : function(cmp, event) {
        var msgIdx = event.getSource().get("v.name");
        var messages = cmp.get("v.messages");
        messages.splice(msgIdx, 1);
        cmp.set("v.messages", messages);
	},
    
    showMessage : function (cmp, event, helper) {
        var targetId = event.getParam("id");
        if (!helper.isTarget(cmp, targetId)) {
            return;
        }
        var messages = cmp.get("v.messages");
        messages.push(event.getParams());
        cmp.set("v.messages", messages);
        if (window.scrollTo) {
           window.scrollTo(0, 0); 
        }
    },
    
    clearMessage : function (cmp, event, helper) {
        var targetId = event.getParam("id");
        if (!helper.isTarget(cmp, targetId)) {
            return;
        }
        var messages = cmp.get("v.messages");
        var clearSticky = !$A.util.getBooleanValue(event.getParam("doNotClearSticky"));
        for (var i=0;!$A.util.isUndefinedOrNull(messages[i]);) {
            var message = messages[i]; 
            if (!message.isSticky || (message.isSticky && clearSticky)) {
                messages.splice(i, 1);
                continue;
            }
            // Increments only when not spliced, ie item is removed
            i++;
        }
        cmp.set("v.messages", messages);
    }
})
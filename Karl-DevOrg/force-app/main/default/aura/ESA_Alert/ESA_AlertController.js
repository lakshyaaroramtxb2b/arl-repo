({
    showAlert : function(cmp, event, helper) {  
        var alertEvts = cmp.get("v.alertEvts");
        alertEvts.push(event);
        helper.showAlert(cmp);
	},
    
    hideAlert : function(cmp, event, helper) {
        helper.hideAlert(cmp);
    }
})
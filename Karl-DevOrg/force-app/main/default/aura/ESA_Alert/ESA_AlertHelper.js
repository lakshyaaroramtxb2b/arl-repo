({
	showAlert : function(cmp) {
        var alertEvts = cmp.get("v.alertEvts");
        if (!$A.util.getBooleanValue(cmp.get("v.showDialog")) && !$A.util.isEmpty(alertEvts)) {
            var evt = alertEvts.shift();
            cmp.set("v.headerText", evt.getParam("title") || $A.get("$Label.c.ESA_Alert"));
            cmp.set("v.bodyText", evt.getParam("text"));
            cmp.set("v.onOk", evt.getParam("onOk"));
            cmp.set("v.showDialog", true);
            cmp.set("v.type", evt.getParam("type") || "default");
        }
	},
    
    hideAlert : function(cmp) {
        cmp.set("v.showDialog", false);
        var onOkCallback =  cmp.get("v.onOk");
        if (onOkCallback) {
            try {
               onOkCallback();
            } catch(e) {}
        }
        this.showAlert(cmp);
    }
})
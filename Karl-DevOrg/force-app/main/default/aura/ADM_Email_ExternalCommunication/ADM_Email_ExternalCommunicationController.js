({
    doInit : function(component, event, helper) {
        helper.fetchData(component, event, helper);
    },
    
    handleSendEmails : function(component, event, helper){    
        var allValid = component.find('requiredField').reduce(function (validSoFar, inputCmp) {
            inputCmp.showHelpMessageIfInvalid();
            return validSoFar && !inputCmp.get('v.validity').valueMissing;
        }, true);
        
        let _accList = [];
        let _listOfCampaignMembers = component.get('v.wrapperData.listOfCampaignMembers');
        
        _listOfCampaignMembers.forEach(function(item){
            let _accEmail = {};
            _accEmail.aeEmail = item.AE_Email__c;
            _accEmail.id = item.Id;
            
            if(_accEmail)
                _accList.push(_accEmail);
        });
        
        //console.log('_accList: ' , _accList);
        //allValid = false;
        if(allValid) {
            //console.log('Send Params: ', JSON.stringify(component.get('v.wrapperData.staticRecordsList')));
            helper.doCallout(component,"c.sendEmails", {
                statusRecords : JSON.stringify(component.get('v.wrapperData.staticRecordsList')),
                aeEmailRecords : _accList 
            },
            function(response){
            	var state = response.getState();
                if(state === 'SUCCESS') {
                	var _resp = response.getReturnValue();
                    //console.log('Resp from Send Emailds: ', _resp);
                    if(_resp){
                    	component.set('v.wrapperData.listOfCampaignMembers', _resp);
                    }
                }
        	});
        }
    },
    
    handleTableRefresh : function(component, event, helper) {
        helper.handleTableRefresh(component, event, helper);       
    },
    
    handleDeleteEmails : function(component, event, helper) {
        component.set('v.showSpinner', true); 
        
        try{ 
            helper.doCallout(component,"c.deleteData",{}, function(response){
                var state = response.getState();
                if(state === 'SUCCESS') {
                    var _resp = response.getReturnValue();
                    
                    component.set('v.wrapperData', _resp);
                    component.set('v.showSpinner', false); 
                } 
                else {
                    component.set('v.showSpinner', false); 
                }
            });   
        } 
        catch(ex) {
            component.set('v.showSpinner', false);  
        }
    },
    
    handleEditRec : function(component, event, helper) {     
        component.set('v.recordId', event.currentTarget.name);
        
        var recId = event.currentTarget.name;
        var editRecordEvent = $A.get("e.force:editRecord");
        editRecordEvent.setParams({
            "recordId" : recId,
            "navigationLocation" : "LOOKUP",
            "panelOnDestroyCallback" : function(event){},
            "successAction" : function(event){}
        });
        editRecordEvent.fire();
    },
    
    showErrorsInTable : function(component, event, helper) {
        var _listOfCampaignMembers = component.get('v.wrapperData').listOfCampaignMembers;
        
        try {            
            component.set('v.showSpinner', true); 
            
            var _resp = [];
            _listOfCampaignMembers.find((cam) => {
                var _cam = (cam.SuccessOrError__c) ? cam.SuccessOrError__c : '' ;
                
                if(_cam && _cam != 'Success') {
                	_resp.push(cam);
            	}
            });
            
            component.set('v.wrapperData.listOfCampaignMembers', _resp);
            component.set('v.showSpinner', false); 
        } 
        catch (ex) {
            component.set('v.showSpinner', false); 
        }        
    },   
    
    recordUpdated :  function(component, event, helper) {
        if(component.get('v.recordId')) {
            helper.fetchData(component, event, helper);
        }
    },
})
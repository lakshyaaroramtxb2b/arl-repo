({
    doCallout : function(cmp, methodName, params, callBackFunc){
        var action = cmp.get(methodName);
        action.setParams(params);
        action.setCallback(this, callBackFunc);
        $A.enqueueAction(action);
    },
    
    fetchData : function(component, event, helper) {
        component.set('v.showSpinner', true); 
        try{
            helper.doCallout(component,"c.fetchData",{}, function(response){
                var state = response.getState();
                if(state === 'SUCCESS') {
                    var _resp = response.getReturnValue();
                    //console.log('Resp from INIT: ', _resp);
                    component.set('v.wrapperData', _resp);
                    component.set('v.showSpinner', false); 
                } 
                else {
                    component.set('v.showSpinner', false); 
                }
            });    
        } 
        catch(Ex) {
            component.set('v.showSpinner', false); 
        }
    },
    
    handleTableRefresh : function(component, event, helper){
        component.set('v.showSpinner', true); 
        try{
            helper.doCallout(component,"c.getCampaignMemberData",{}, function(response){
                var state = response.getState();
                if(state === 'SUCCESS') {
                    var _resp = response.getReturnValue();
                    component.set('v.wrapperData.listOfCampaignMembers', _resp);
                    component.set('v.showSpinner', false); 
                } 
                else {
                    component.set('v.showSpinner', false); 
                }
            });  
        } 
        catch(Ex) {
            component.set('v.showSpinner', false); 
        }
    },
})
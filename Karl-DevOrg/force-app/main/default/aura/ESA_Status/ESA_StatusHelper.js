({
	parseStatus : function(cmp) {
		var req = cmp.get("v.securityRequest");
        //console.log(req.Status__c);
        if (!$A.util.isEmpty(req.Status__c) && 
            req.Status__c != "New" 
            && req.Status__c != "Incomplete") {
            
            cmp.set("v.status", "COMPLETE");
            cmp.set("v.percent", 100);
        } else {
            cmp.set("v.status", "IN_PROGRESS");
            cmp.set("v.percent", 20);
        }
	}
})
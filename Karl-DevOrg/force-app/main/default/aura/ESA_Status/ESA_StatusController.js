({
	doInit : function(cmp, event, helper) {
		helper.parseStatus(cmp);
	},
    
    update : function(cmp, event, helper) {
		helper.parseStatus(cmp);
	}
})
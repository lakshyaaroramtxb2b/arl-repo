({
	handleTimezoneChange : function(cmp, event, helper) {
		var timezone = event.getParam("timezone");
        cmp.set("v.timezone", timezone);
	}
})
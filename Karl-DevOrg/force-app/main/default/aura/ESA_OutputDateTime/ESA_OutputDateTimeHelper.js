({
	formatDateTime : function(cmp) {
		var value = cmp.get("v.value");
        var timezone = cmp.get("v.timezone");
        
        if (!$A.util.isEmpty(value)) {
            var action = cmp.get("c.getFormattedDateTime");
            action.setParams({"dateTimeValue" : value,
                              "timeZoneValue" : timezone});
            action.setCallback(this, function(response) {            
                $A.get("e.c:esaHandleResponse").setParams({
                    "response": response,
                    "onSuccess": $A.getCallback(function(returnVal) {
                        cmp.set("v.formattedDateTime", returnVal);
                    })
                }).fire();            
            });
            $A.enqueueAction(action);
        }
	}
})
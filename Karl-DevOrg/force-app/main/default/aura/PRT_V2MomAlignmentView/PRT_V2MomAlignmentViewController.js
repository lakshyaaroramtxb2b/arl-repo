({
    doInit : function(component, event, helper) {
        document.title = "V2MOM Alignment";
        var action = component.get("c.getV2MomInformation");
        var spinner = component.get("v.showSpinner");
        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log('response', response);
            console.log('state',state);
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                
            	console.log('storeResponse',storeResponse);
                component.set("v.userInfo", storeResponse.userRec);
                component.set("v.v2MomData", storeResponse.v2MomMethods);
                var methods = component.get("v.v2MomData");
        		var newMap = component.get("v.v2momMapping");
                if(typeof newMap == 'undefined' || newMap === null){
                    newMap = new Map();
                }
                methods.forEach(function(element){
                    console.log(element.externalID);
                    console.log(element.relatedMeasures);
                	newMap[element.externalID] = element.relatedMeasures;
                });
                component.set("v.v2momMapping",newMap);
                    console.log(JSON.stringify(component.get("v.v2momMapping")));
            }
        });
        $A.enqueueAction(action);
  

    },
    
    handleSave: function(component, event, helper){
        var target = event.target;
        var newMap = component.get("v.v2momMapping");
        console.log('v2mom map', newMap);
        var action = component.get("c.createV2Mapping");
        action.setParams({
        "dependentIdVsControllerIdMap": newMap
    });
        action.setCallback(this, function(a) {
           var state = a.getState();
            if (state === "SUCCESS") {
                console.log("SUCCESS");
                debugger;
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title : 'Success',
                    message: 'Your record has been successfully saved',
                    duration:' 3000',
                    key: 'info_alt',
                    type: 'success',
                    mode: 'dismissible'
                });
                toastEvent.fire();
            }
        });
    $A.enqueueAction(action);
    },
    
   /* handlePublish:function(component, event, helper){
        var newMap = component.get("v.v2momMapping");
        var action = component.get("c.publishV2Mom");
        action.setCallback(this, function(a) {
            var state = a.getState();
            if (state === "SUCCESS") {
                console.log("SUCCESS");
            }
        });
        $A.enqueueAction(action);
        
    },*/
 
    handleAccordian : function(component, event, helper){
        var target = event.target;
        var methodIndex = target.getAttribute("data-row-index");
        var measureIndex = target.getAttribute("data-measure-index");
        console.log('methodIndex', methodIndex, 'measureIndex', measureIndex);
        var methodsList = component.get("v.v2MomData");
        if(measureIndex) {
            debugger;
            if(methodsList[methodIndex].v2MomMeasures[measureIndex].isOpen) {
                methodsList[methodIndex].v2MomMeasures[measureIndex].isOpen = false;
            } else {
                methodsList[methodIndex].v2MomMeasures[measureIndex].isOpen = true;
            }
        } else {
            debugger;
            if(methodsList[methodIndex].isOpen) {
                methodsList[methodIndex].isOpen = false;
            } else {
                methodsList[methodIndex].isOpen = true;
            }
        }
        component.set("v.v2MomData", methodsList);
    }
})
({
    displaySearchResults : function(cmp, event, helper) {
        var searchText = event.getParam("data")["searchText"];
        if ($A.util.isEmpty(searchText)) {
            helper.clearSearch(cmp);
        } else {
            helper.search(cmp, searchText);
        }
    },

    selectResult : function (cmp, event, helper) {
        var dataIndex = parseInt(event.target.getAttribute("data-index"));
        var selectedResult = helper.getResultByIndex(cmp, dataIndex);
        helper.clearSearch(cmp);
        cmp.set("v.value", selectedResult.value);
        cmp.set("v.id", selectedResult.id);
    },

    fireRemove : function (cmp, event, helper) {
        cmp.getEvent("onremove").fire();
    },

    removeSelection : function (cmp, event, helper) {
        cmp.set("v.id", null);
        cmp.set("v.value", null);
    },

    handleOnBlur : function (cmp, event, helper) {
        if (!helper.isFocusWithin(cmp)) {
            // SafeCheck: Delay to allow selection and then clear
            setTimeout($A.getCallback(function() {
                helper.clearSearch(cmp);
            }), 200); 
        }
    }
})
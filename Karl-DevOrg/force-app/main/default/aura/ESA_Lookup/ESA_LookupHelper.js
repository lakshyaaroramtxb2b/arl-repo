({
    search : function(cmp, searchText) {
        var THIS = this;
        var cUtil = cmp.find("commonUtil");
        var action = cmp.get("c.searchLookup");
        action.setStorable();
        action.setParams({ 
                          "questionId" : cmp.get("v.questionId"),
                          "searchText" : searchText
                        });
        // Clear previous results
        cmp.set("v.searchResults", null);
        THIS.toggleBusy(cmp, true);
        cUtil.processAction(this, action, {
            "onFinally": $A.getCallback(function(returnVal) {
                THIS.toggleBusy(cmp, false);
                // Success or Failure we want to set results 
                var results = THIS.parseObjs(cmp, returnVal);
                cmp.set("v.searchResults", results);
            })
        });
    },

    parseObjs : function (cmp, returnVal) {
        var config = returnVal["config"] || {};
        var results = returnVal["results"] || [];
        var items = [];
        var cUtil = cmp.find("commonUtil");

        cUtil.forEach(results, function(result) {
            items.push({
                "iconName" : config.iconName,
                "iconURL" : result[config.iconURLField],
                "value" : cUtil.replaceParams(config.displayTemplate, result),
                "id" : result[config.idField]
            });
        });
   
        if ($A.util.isEmpty(items)) {
            items.push({
                "value" : $A.get("$Label.c.ESA_EmptySearchResults")
            });
        }

        return items;
    },

    getResultByIndex : function (cmp, dataIndex) {
        // Concat to create a copy as onBlur might clear the reference
        var results = [].concat(cmp.get("v.searchResults") || []);
        if (!$A.util.isEmpty(results) && dataIndex < results.length) {
            return results[dataIndex];
        }
        return null;
    },

    clearSearch : function (cmp) {
        var searchInputCmp = cmp.find("lookupSearch");
        if (searchInputCmp) {
            searchInputCmp.clear();
        }
        cmp.set("v.searchResults", null);
    },

    toggleBusy : function(cmp, isShow) {
        if (isShow) {
            cmp.find("busy").getEvent("show").fire();
        } else {
            cmp.find("busy").getEvent("hide").fire();
        }
    },
    
    isFocusWithin : function(cmp) {
        var resultsContainerCmp = cmp.find("resultsContainer");
        if (resultsContainerCmp && resultsContainerCmp.isValid()) {
            var htmlElem = resultsContainerCmp.getElement();
            var focusedElems = htmlElem.querySelectorAll("a:hover");
            if (focusedElems && focusedElems.length > 0) {
                return true;
            }
        }
        return false;
    }
})
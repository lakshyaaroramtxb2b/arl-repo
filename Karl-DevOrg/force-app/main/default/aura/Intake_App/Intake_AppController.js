({
	doInit : function(cmp, event, helper) {
        var cUtil = cmp.find('commonUtil');
        var action = cmp.get("c.getInitData");
        action.setParams({"entityCode" : cmp.get("v.entityCode"), 
                            "templateName" : cmp.get("v.template")});
        cUtil.processAction(this, action, {
            "onSuccess": $A.getCallback(function(returnVal) {
                helper.initESA(cmp, returnVal);
            })
        });
    }
})
({
	initESA : function(cmp, dataMap) {
        var anglrUtil = cmp.find('commonAnglrUtil');
		var data = {
            "queryParams" : {
                "groupName": cmp.get("v.groupName") || "",
                "entityCode": cmp.get("v.entityCode") || "",
                "id": cmp.get("v.id") || "",
                "caseno" : cmp.get("v.caseno") || "",
                "template" : cmp.get("v.template") || "",
                "si" : cmp.get("v.si") || ""
            },
            "requestTemplate" : dataMap.requestTemplate,
            "user" : {
                "firstName": dataMap.user.FirstName,
                "lastName": dataMap.user.LastName,
                "email": dataMap.user.Email,
                "phone": dataMap.user.Phone // FYI, Empty for now
            },
            "site" : {
                "prefix": $A.get("$Site.siteUrlPrefix")
            },
            "onResetURL" : function(entityCode, id, si, template) {
                anglrUtil.clearURLParams();
                var params = {"entityCode" : entityCode};
                if (!$A.util.isEmpty(template)) {
                    params["template"] = template;
                } else if (!$A.util.isEmpty(id)) {
                    params["Id"] = id;
                } else if (!$A.util.isEmpty(si)) {
                    params["si"] = si;
                }
                anglrUtil.updateURLParams(params);
            },
            "onFetchArticles" : function(entityCode, searchText, callback) {
                var cUtil = cmp.find("commonUtil");
                var url = cUtil.trimSuffix($A.get("$Site.siteUrlPrefix"), "/s") + "/ESA_KnowledgeArticlesJSON?entityCode=" + entityCode + "&keyword=" + (searchText || "");
                anglrUtil.callAjax(url, $A.getCallback(function(response) {
                    var articleList = [];
                    if (response && $A.util.isArray(response.data)) {
                        // remove the last dummy getting added during VF JSON generation
                        articleList = response.data.slice(0,-1);
                    }
                    callback(articleList);
                }));
            }
        };
		
        $A.createComponent(
            "c:ESA", 
            {
                "data" : data
            },
            function(esaCmp, status, errorMessage){
                if (status === "SUCCESS") {
                    cmp.set("v.esaCmp", esaCmp);
                }
                else if (status === "INCOMPLETE") {
                    // Show offline error
                }
                else if (status === "ERROR") {
                    // Show error message
                }
            }
        );
	}
})
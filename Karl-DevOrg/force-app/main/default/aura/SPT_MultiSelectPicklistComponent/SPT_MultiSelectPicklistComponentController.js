({

  reInit: function(component, event, helper) {
    component.set("v.initialized",false);
    helper.init(component);
  },

  init: function(component, event, helper) {
    helper.init(component);
  },

  handleClick: function(component, event, helper) {
    var mainDiv = component.find('main-div');
    $A.util.addClass(mainDiv, 'slds-is-open');
  },

  handleSelection: function(component, event, helper) {
    var item = event.currentTarget;
    if (item && item.dataset) {
      var value = item.dataset.value;
      var selected = item.dataset.selected;
      console.log("Value: "+value+" Selected: "+selected);

      var options = component.get("v.options_");
       //
        if(value == "All" || value == "All Open" || value == "All Closed" || value == "All Others") {
            options.forEach(function(element) {
                if(element.value == value) {
                    element.selected = selected == "true" ? false : true;
                }else{
                    element.selected = false;
                }
            });
        }else{
            options.forEach(function(element) {
                if(element.value == "All" || element.value == "All Open" || element.value == "All Closed" || element.value == "All Others") {
                    element.selected = false;
                }else if(element.value == value){
                     element.selected = selected == "true" ? false : true;
                }
            });
        }
      // Himanshu Update Starts
     /* options.forEach(function(element) {
          if(value == "Select All" && element.value == value) {
              element.selected = selected == "true" ? false : true;
              options.forEach(function(val) {
                  val.selected = element.selected;
              });
          }
          else if(element.value == value){
              element.selected = selected == "true" ? false : true;
              if(!element.selected) {
                  options.forEach(function(val) {
                      if(val.value == "Select All") {
                          val.selected = false;
                      } 
                  });   
              }
          }
      }); */
      // Himanshu Update Ends
      component.set("v.options_", options);
      var values = helper.getSelectedValues(component);
      var labels = helper.getSelectedLabels(component);

      helper.setInfoText(component, labels);
      //helper.despatchSelectChangeEvent(component, values);
    }
  },

  handleMouseLeave: function(component, event, helper) {
    var values = helper.getSelectedValues(component);
    helper.despatchSelectChangeEvent(component, values);
    component.set("v.dropdownOver", false);
    var mainDiv = component.find('main-div');
    $A.util.removeClass(mainDiv, 'slds-is-open');
  },

  handleMouseEnter: function(component, event, helper) {
    component.set("v.dropdownOver", true);
  },

  handleMouseOutButton: function(component, event, helper) {
    window.setTimeout(
      $A.getCallback(function() {
        if (component.isValid()) {
          //if dropdown over, user has hovered over the dropdown, so don't close.
          if (component.get("v.dropdownOver")) {
            return;
          }
          var mainDiv = component.find('main-div');
          $A.util.removeClass(mainDiv, 'slds-is-open');
        }
      }), 200
    );
  }

})
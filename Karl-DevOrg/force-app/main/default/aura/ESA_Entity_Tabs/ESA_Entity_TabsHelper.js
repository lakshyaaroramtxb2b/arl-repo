({
    setEntitiesToGroupMap : function(cmp) {
        var entitiesMap = {};  
        var records = cmp.get("v.allEntityRecords");
              
        for (var i=0;i<records.length;i++) {
            var record = records[i];
            
            var groupName = record["Header_Logo_Text__c"];
            if ($A.util.isEmpty(groupName)) {
                continue; // Group name is required so ignore empty ones
            } else if ($A.util.isEmpty(entitiesMap[groupName])) {
               entitiesMap[groupName] = []; 
            }
            entitiesMap[groupName].push(record);   
        }
        cmp.set("v.entitiesToGroupMap", entitiesMap);
    },
    
    // Set entities based on selected group
	setEntities : function(cmp) {
        var entitiesMap = cmp.get("v.entitiesToGroupMap");        
        var selectedEntityCode = cmp.get("v.securityRequest.Entity_Code__c"); 
        var selectedGroup = cmp.get("v.selectedGroup") || "Security";
        if (selectedEntityCode) {
           var entity = this.getEntityByCode(cmp, selectedEntityCode, true);
            if ($A.util.isUndefinedOrNull(entity)) {
               this.clearMessage(true, "page");            
               this.showMessage($A.get("$Label.c.ESA_Invalid_EntityCode"), "error", true, "page"); 
            } else {
                selectedGroup = entity["Header_Logo_Text__c"];
            }           
        } 
        
        //console.log("selectedGroup="+selectedGroup);
        //console.log(JSON.stringify(entitiesMap));
        var entities = [];
        var records = entitiesMap[selectedGroup];  
        if (!$A.util.isEmpty(selectedGroup) && $A.util.isUndefinedOrNull(records)) {  
           this.clearMessage(true, "page");            
           this.showMessage($A.get("$Label.c.ESA_Invalid_Group"), "error", false, "page");
        } else {
           // Select entity
           for (var i=0;i<records.length;i++) {
                var record = records[i];
                var entityCode = record["EntityCode__c"];
                var isSingleTab = $A.util.getBooleanValue(record["Single_Tab__c"]);
                var isSelectedTab = (entityCode === selectedEntityCode);
                      
                // Add it if its not a single tab or when landed with single tab entity code in url
                if (!isSingleTab || isSelectedTab) {
                    // When single tab is selected when clear other tabs
                    if (isSingleTab) {
                       entities = [];
                    }
                    entities.push(record);
                }  
                
                // If selected tab is a single tab we can skip other tabs
                if (isSelectedTab && isSingleTab) {              
                    break;
                }               
            } 
        }
        //console.log(records);
        //console.log(entities);
        $A.get("e.c:esaGroupSelected").setParams({
            "groupName": selectedGroup,
            "isSingleTab" : entities.length <= 1
        }).fire();
        cmp.set("v.entities", entities);
	},
    
    setSelectedEntity : function(cmp, entityCode) {
    	var entity = this.getEntityByCode(cmp, entityCode);
        if ($A.util.isUndefinedOrNull(entity)) {
            return;
        }
        
        cmp.set("v.selectedEntityCode", entityCode);
        cmp.set("v.showSelectTeamMsg", false);
        this.updateDocumentTitle(entity);
        $A.get("e.c:ESA_Entity_Selected").setParams({"entity": entity}).fire();
        this.showNotification(entity);
    },
    
    setShowSelectTeamMsg : function(cmp) {
        this.setEntityFromReq(cmp);
        if ($A.util.isEmpty(cmp.get("v.selectedEntityCode"))) {
        	    var entities = cmp.get("v.entities");
	        if (entities.length > 1) {
	            cmp.set("v.showSelectTeamMsg", true);
	        } else if (entities.length == 1) {
	        	// When there is only one entity select it by default
	        	this.setSelectedEntity(cmp, entities[0].EntityCode__c);
	        }
        }
    },
    
    setEntityFromReq : function (cmp) {
        var entityCode = cmp.get("v.securityRequest.Entity_Code__c");
        var entity = this.getEntityByCode(cmp, entityCode);
        // If current set of tabs doesn't include selected entity then
        if ($A.util.isUndefinedOrNull(entity)) {
           this.setEntities(cmp);        
        }
        this.setSelectedEntity(cmp, entityCode);
    },
    
    updateDocumentTitle : function(entity) {
        if (entity) {
          document.title = entity.Name;
        }
    },
    
    getEntityByCode : function(cmp, entityCode, includeAll) {
        if ($A.util.isEmpty(entityCode)) {
           return null; 
        }
        var entities = includeAll ? cmp.get("v.allEntityRecords") : cmp.get("v.entities");
        for (var i=0;i<entities.length;i++) {
            var entity = entities[i];
            if (entity.EntityCode__c.toLowerCase() == entityCode.toLowerCase()) {
                return entity;
            }
        }
        return null;
    },

    showNotification: function(entity) {
        if (!$A.util.isEmpty(entity.Notification__c)) {
            this.showMessage(entity.Notification__c, "info", true, null, function() {
                // Clear the notification on clear button
                entity.Notification__c = null;
            });
        }
    },

    isSelectedEntity: function(cmp, entityCode) {
        return (entityCode && entityCode == cmp.get("v.selectedEntityCode"));
    }, 
    
    showMessage: function (message, variant, isSticky, id, onClearCallback) {
        $A.get("e.c:esaShowMessage").setParams({
            "text": message, 
            "variant": (variant || "success"), 
            "isSticky" : isSticky || false,
            "id" : id,
            "onClear" : onClearCallback
        }).fire();
    },
    
    clearMessage: function (doNotClearSticky, id) {
        $A.get("e.c:esaClearMessage").setParams({
            "doNotClearSticky" : doNotClearSticky,
            "id" : id
        }).fire();
    }
})
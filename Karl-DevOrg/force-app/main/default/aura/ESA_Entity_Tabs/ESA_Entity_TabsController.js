({
	doInit : function(cmp, event, helper) {
        cmp.find('busy').get('e.show').fire();
		var action = cmp.get("c.getEntities");
        action.setStorable();
        action.setCallback(this, function(response) {
            cmp.find('busy').get('e.hide').fire();
            $A.get("e.c:esaHandleResponse").setParams({
                "response": response,
                "onSuccess" : $A.getCallback(function(returnVal) {
                	    //console.log(returnVal);
                	 cmp.set("v.allEntityRecords", returnVal);
                     helper.setEntitiesToGroupMap(cmp);
                     helper.setEntities(cmp);
                })     
            }).fire();
            helper.setShowSelectTeamMsg(cmp);
        });
        $A.enqueueAction(action);
	},
    
    selectEntity : function(cmp, event, helper) {
        var entityCode = event.target.getAttribute('data-entity-code');
        if (!helper.isSelectedEntity(cmp, entityCode)) { 
              helper.clearMessage(false);
              helper.setSelectedEntity(cmp, entityCode);
        }
    },
    
    handleEntitySelection: function(cmp, event, helper) {
        helper.setEntityFromReq(cmp);
    },
    
    hideSelectTeamMsg : function(cmp, event, helper) {
        cmp.set("v.showSelectTeamMsg", false);
    }
    
})
/*({
	init : function(component) {
        
        var flow = component.find("screenFlow");
        var userId = $A.get("$SObjectType.CurrentUser.Id");
        
       var inputVariables = [
            {
                name : "recordId",
                type : "String",
                value: component.get("v.recordId")
                
            },
            {
                name : "userId",
                type : "SObject",
                value: userId
                
            }
        ];
        
        flow.startFlow("Assign_Case_to_me_screenFlow", inputVariables);
		
	}
    
})*/



({
    init : function(component, event, helper) {
        
        var myPageRef = component.get('v.pageReference');
        var recordId = myPageRef.state.c__recordId;
        var flow = component.find("screenFlow");
        var userId = $A.get("$SObjectType.CurrentUser.Id");
        
        
      
        var inputVariables = [
            {
                name : "recordId",
                type : "String",
                value: recordId
                
            },
            {
                name : "userId",
                type : "SObject",
                value: userId
                
            }
        ];
        flow.startFlow("Assign_Case_to_me_screenFlow", inputVariables);
    }
})
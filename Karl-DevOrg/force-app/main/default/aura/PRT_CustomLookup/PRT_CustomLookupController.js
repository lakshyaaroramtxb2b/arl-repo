({
    doInit : function( component, event, helper ) {
        $A.util.toggleClass(component.find('resultsDiv'),'slds-is-open');
        console.log('value --> ' + component.get('v.value'));
        console.log('userRec value --> ' + component.get('v.userRec'));
        if( !$A.util.isEmpty(component.get('v.value')) ) {
            helper.searchRecordsHelper( component, event, helper, component.get('v.value') );
        }else{
            component.set("v.selectedRecord","");
        }
        
        console.log(JSON.stringify(component.get('v.methodIdSetMap')));
    },
    
    // When a keyword is entered in search box
    searchRecords : function( component, event, helper ) {
        console.log('value --> ' + JSON.stringify(component.get('v.userRec')));
        console.log('value --> ' + component.get('v.value'));
        if( !$A.util.isEmpty(component.get('v.searchString')) ) {
            helper.searchRecordsHelper( component, event, helper, '');
            
        } else {
            $A.util.removeClass(component.find('resultsDiv'),'slds-is-open');
        }
    },
    
    // When an item is selected
    selectItem : function( component, event, helper ) {
        
        if(!$A.util.isEmpty(event.currentTarget.id)) {
            var recordsList = component.get('v.recordsList');
            var index = recordsList.findIndex(x => x.value === event.currentTarget.id)
            if(index != -1) {
                var selectedRecord = recordsList[index];
            }
            $A.util.removeClass(component.find('resultsDiv'),'slds-is-open');
            component.set('v.selectedRecord',selectedRecord);
            console.log('selected record value here', selectedRecord);
            component.set('v.value',selectedRecord.value);
            if(component.get("v.objectName")=='User'){                
                component.set('v.userId',selectedRecord.value);
            }
            component.set('v.value',selectedRecord.value);
            //var action = component.get('c.createV2Mapping');
            var mapToSend = component.get('v.methodIdSetMap');            
            mapToSend[component.get('v.dependentMethodId')] = component.get('v.value');
            console.log(JSON.stringify(component.get("v.methodIdSetMap")));
        }
    },
    
    showRecords : function( component, event, helper ) {
        if(!$A.util.isEmpty(component.get('v.recordsList')) && !$A.util.isEmpty(component.get('v.searchString'))) {
            $A.util.addClass(component.find('resultsDiv'),'slds-is-open');
        }
    },
    
    // To remove the selected item.
    removeItem : function( component, event, helper ){
         //var action = component.get('c.deleteV2Mapping');
        var newMap = component.get('v.methodIdSetMap');
        newMap[component.get('v.dependentMethodId')] = null;
        component.set("v.methodIdSetMap",newMap);
            console.log(JSON.stringify(component.get("v.methodIdSetMap")));
        component.set('v.selectedRecord','');
        component.set('v.value','');
        component.set('v.searchString','');
        if(component.get("v.objectName")=='User'){                
            component.set('v.userId','');
        }
        setTimeout( function() {
            component.find( 'inputLookup' ).focus();
        }, 250);
    },
    
    // To close the dropdown if clicked outside the dropdown.
    blurEvent : function( component, event, helper ){
        $A.util.removeClass(component.find('resultsDiv'),'slds-is-open');
    }
})
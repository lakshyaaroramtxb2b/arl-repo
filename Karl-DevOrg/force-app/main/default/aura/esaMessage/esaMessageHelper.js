({
	isTarget : function(cmp, targetId) {
		var id = cmp.get("v.id");
        if (($A.util.isEmpty(id) && $A.util.isEmpty(targetId))
             || id == targetId) {
            return true;
        } 
        return false;
	}
})
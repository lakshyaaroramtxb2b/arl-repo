({
    init : function(component, event, helper) {
        var userId = $A.get("$SObjectType.CurrentUser.Id");
        var action = component.get("c.isEnterpriseSecurityGroupMember");
            action.setParams({
                "currentUserId" : userId
            });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state == 'SUCCESS') {
                var trustAppSecurityAssuranceId = response.getReturnValue();
                if(trustAppSecurityAssuranceId != ''){
                    component.set('v.selectRecordId',trustAppSecurityAssuranceId); 
              	    component.set('v.selectRecordName','Trust - Application Security Assurance');
                }
            }
            else if(state == "ERROR"){
                var errors = response.getError();
            }
        });
        $A.enqueueAction(action);
    },
    searchField : function(component, event, helper) {
        var currentText = event.getSource().get("v.value");
        var resultBox = component.find('resultBox');
        component.set("v.LoadingText", true);
        if(currentText.length > 1) {
            $A.util.addClass(resultBox, 'slds-is-open');
            var action = component.get("c.getResults");
            action.setParams({
                "value" : currentText
            });
            
            action.setCallback(this, function(response){
                var STATE = response.getState();
                if(STATE === "SUCCESS") {
                    component.set("v.searchRecords", response.getReturnValue());
                    if(component.get("v.searchRecords").length == 0) {
                        console.log('000000');
                    }
                }
                else if (STATE === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + 
                                        errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
                component.set("v.LoadingText", false);
            });
            
            $A.enqueueAction(action);
        }
        else {
            $A.util.removeClass(resultBox, 'slds-is-open');
        }
        
    },
    
    setSelectedRecord : function(component, event, helper) {
        var currentText = event.currentTarget.id;
        var resultBox = component.find('resultBox');
        $A.util.removeClass(resultBox, 'slds-is-open');
        //component.set("v.selectRecordName", currentText);
        component.set("v.selectRecordName", event.currentTarget.dataset.name);
        component.set("v.selectRecordId", currentText);
        component.find('userinput').set("v.readonly", true);
    }, 
    
    resetData : function(component, event, helper) {
        component.set("v.selectRecordName", "");
        component.set("v.selectRecordId", "");
        component.find('userinput').set("v.readonly", false);
    }
})
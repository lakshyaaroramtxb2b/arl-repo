({
    doInit: function (component, event, helper) {
        var action = component.get("c.createCycleRequestItems");
        var recId = component.get("v.recordId");

        action.setParams({
            auditCycleId: recId
        });

        action.setCallback(this, function (response) {
            var state = response.getState();

            if (state === "SUCCESS") {
                let toastParams = {
                    title: "Success Message",
                    message: "Successfully created Cycle Request Items",
                    type: "success"
                };
                // Fire error toast
                let toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams(toastParams);
                toastEvent.fire();

                $A.get('e.force:refreshView').fire();
                $A.get("e.force:closeQuickAction").fire();

            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        let toastParams = {
                            title: "Error",
                            message: errors[0].message, // Default error message
                            type: "error"
                        };
                        // Fire error toast
                        let toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams(toastParams);
                        toastEvent.fire();
                    }
                    console.log('error>>' + JSON.stringify(errors));
                } else {
                    console.log("Unknown error");
                }

                $A.get('e.force:refreshView').fire();
                $A.get("e.force:closeQuickAction").fire();
            }
        });
        $A.enqueueAction(action);
    }
})
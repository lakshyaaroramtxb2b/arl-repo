({
	getDependencies : function(component, event, helper){
        
        //Calling base component's helper method to call AuraEnabled Method
        helper.doCallout(component,'c.getDependencies', {})
        .then(
            $A.getCallback(function(response){
                component.set("v.dependenciesMap",response);
            }),
            $A.getCallback(function(error) {
                helper.showMessage('Error', error[0].message, 'error');
            })
        );
    },
    getHelpURL : function(component, event, helper){
        var action= component.get("c.getHelpURL");
        // set param to method  
        action.setParams({
            'urlToGet':"Intake Form"
        });
        action.setCallback(this,function(response){
            var state= response.getState();
              $A.log(response);
            if(state == "SUCCESS"){
                component.set("v.helpLinkURL",response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    checkDependencies : function(component, event, helper){
        var dependencyMap = component.get("v.dependenciesMap");
        var controllingField = event.getSource().getLocalId();
        if(typeof component.find(controllingField)!== "undefined"){
            var controllingValue = event.getSource().get("v.value");
            var tag = helper.getAuraIDTag(component, event, helper, controllingField);
            var dependencyTag = controllingField.replace(tag,'');
            if(typeof dependencyMap[dependencyTag] !== "undefined"){
                var valueFieldMap =  dependencyMap[dependencyTag] ;
                for(var key in valueFieldMap){
                    if(controllingField=="Existing_Work_Story__c"
                       || controllingField=="AppointmentBookingQ"){
                        var controlledfield = component.find(valueFieldMap[key]+'_div');                            
                    }else if(component.get("v.caseType") == component.get("v.VOSLA")){
                        var controlledfield = component.find('BUG_'+valueFieldMap[key]+'_div');
                    }else if(component.get("v.caseType") == component.get("v.ASSESSMENT")){
                        var controlledfield = component.find('AIT_'+valueFieldMap[key]+'_div');
                    }else if(component.get("v.caseType") == component.get("v.SELFREPORTED")
                             || component.get("v.caseType") == component.get("v.SECURITYASSESSMENT")){
                        var controlledfield = component.find('SRS_'+valueFieldMap[key]+'_div');
                    }
                    if(key == controllingValue+"" || (controllingValue+"").includes(key)==true){                        
                        $A.util.removeClass(controlledfield, 'slds-hide');
                        $A.util.addClass(controlledfield, 'slds-show');
                        $A.util.addClass(controlledfield, 'subQuestion');
                    }else{
                        $A.util.addClass(controlledfield, 'slds-hide');
                        $A.util.removeClass(controlledfield, 'slds-show');
                        $A.util.removeClass(controlledfield, 'subQuestion');
                    }    
                }
            }
        }
    },
    getRecordTypeID : function(component,event,helper){
        var action= component.get("c.getRecordId");
        action.setCallback(this,function(response){
            var state= response.getState();
              $A.log(response);
            if(state == "SUCCESS"){
                component.set("v.recordTypeID",response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    createWorkCaseRecords: function(component,event,helper,updatedRecord){
        var selectedrecords = component.get("v.selectedRecordsList");
        var action= component.get("c.createCaseWorkRecords");
        var params = event.getParams('fields');
        
        // set param to method  
        action.setParams({
            'selectedRecords':component.get("v.selectedRecordsList"),
            'caseRecordID':updatedRecord.response.id
        });
        action.setCallback(this,function(response){
            var state= response.getState();
              $A.log(response);
            if(state == "SUCCESS"){
            }
        });
        $A.enqueueAction(action);
    },
    createUserSharingAccess: function(component,event,helper,updatedRecord){
        var action= component.get("c.createUserSharingAccess");        
        // set param to method  
        action.setParams({
            'caseRecordID':updatedRecord.response.id
        });
        action.setCallback(this,function(response){
            var state= response.getState();
              $A.log(response);
            if(state == "SUCCESS"){
            }
        });
        $A.enqueueAction(action);
    },
    getOwnerDetail : function(component, event, helper){
        
        //Calling base component's helper method to call AuraEnabled Method
        helper.doCallout(component,'c.getOwnerAvailableSlots', {})
        .then(
            $A.getCallback(function(response){
                component.set("v.caseOwner",response.ownerId);
                component.set("v.caseRequestor",response.caseRequestor);
                component.set("v.ownerAvailability",response.slots);
            }),
            $A.getCallback(function(error) {
                helper.showMessage('Error', error[0].message, 'error');
            })
        );
    },
    getIssueOwner: function(component,event,helper,fields){
      	var action= component.get("c.getIssueOwner");
		// set param to method  
        action.setParams({
            'teamResponsibleId':fields.Team_Responsible__c
        });
        action.setCallback(this,function(response){
            var state= response.getState();
            $A.log(response);
            if(state == "SUCCESS"){
                if(response.getReturnValue()!=''){
           			fields.Issue_Owner__c = response.getReturnValue();                
                }
            }
			component.find('recordEditForm').submit(fields);            
        });
        $A.enqueueAction(action);      
    },
    /*getParentcloud: function(component,event,helper,fields){
      	var action= component.get("c.getParentCloudName");
		// set param to method  
        action.setParams({
            'cloudExternalId': fields.Cloud__c
        });
        action.setCallback(this,function(response){
            var state= response.getState();
            $A.log(response);
            if(state == "SUCCESS"){
                if(response.getReturnValue()!=''){
           			fields.Parent_Cloud__c = response.getReturnValue();                
                }                
            }
            if(component.get("v.caseType") == component.get("v.VOSLA")){
                helper.getIssueOwner(component,event,helper,fields);
            } else{   
                component.find('recordEditForm').submit(fields);
            }
        });
        $A.enqueueAction(action);      
    },*/
    bookAppointment : function(component, event, helper) {
        var updatedRecord = event.getParams(); 
        var selectedSlot = component.get("v.selectedSlot");
        var isBooking = component.find("AppointmentBookingQ");
            if(selectedSlot && isBooking.get("v.value") == "Yes"){
                var _param = {'dt' :  selectedSlot, 'ownerId' : component.get("v.caseOwner"),
                             'CaseID' : updatedRecord.response.id};
                helper.doCallout(component,"c.createEvent",_param).then(function(response){
                     if(response){ 
                    }else{
                        //helper.showMessage('Error', 'Error while boooking appointment', 'error');
                    }
                }).catch(function(error){
                    //helper.showMessage('Error', error[0].message, 'error');
                });
            }    
           
    },
    getRequiredFields : function(component, event, helper){        
        //Calling base component's helper method to call AuraEnabled Method
        helper.doCallout(component,"c.getrequiredFieldsSet",{}).then(function(response){
            if(response){
               component.set("v.requiredFieldsList",response);
            }else{
                helper.showMessage('Error', 'Error while boooking appointment', 'error');
            }
        }).catch(function(error){
            helper.showMessage('Error', error[0].message, 'error');
        });
    },
    ensureRequiredFields :function(component, event, helper){ 
        event.preventDefault();          
       	component.set('v.disabled', true);
       	component.set('v.showSpinner', true);
        var errorFieldsString = '';
        var dependencyMap = component.get("v.dependenciesMap");
        var subQ = {};
        var subVal = {};
        var errorField = [];
        var fields = event.getParam('fields'); 
        var requiredfields = component.get("v.requiredFieldsList");
        var isError = false;
        fields.IEM_Case_Type__c = 'Issue';
        for(var key in dependencyMap){
            for(var key2 in dependencyMap[key]){
                var k = dependencyMap[key][key2]+'';
            	subQ[k]=key; 
                subVal[k] = key2;
            }
        }
        if(component.get("v.caseOwner")!='' && component.get("v.caseOwner")!=null){
           // fields.OwnerId =  component.get("v.caseOwner");
             fields.Reported_Case_Owner__c = component.get("v.caseOwner");
        }
        if(fields.Residual_Severity__c!=null && fields.Residual_Severity__c!=''
           && typeof fields.Residual_Severity__c != 'undefined'){
            fields.Severity_Assessment_Status__c = 'Pre-assigned';
        }
        if(component.get("v.caseRequestor")!='' && component.get("v.caseRequestor")!=null){
            fields.Requestor__c =  component.get("v.caseRequestor");
        }
        requiredfields.forEach(function(element, i) {
            if(typeof fields[element]!='undefined' ){
                if(fields[element]=='' || fields[element]==null ){
                    if(typeof subQ[element]!= 'undefined'){
                        var compoenentID = helper.makeAuraID(component, event, helper, subQ[element]);
                        if(typeof fields[subQ[element]] == 'undefined' 
                           && typeof component.find(compoenentID)!='undefined'
                           && component.find(compoenentID)!=null){
								if(component.find(compoenentID).get("v.value")+""==subVal[element]+""
                                || (component.find(compoenentID).get("v.value")+"").includes(subVal[element]+"")){
                                isError = true;
                                errorField.push(parseInt(document.getElementsByClassName(subQ[element]+'-serial')[0].getAttribute('data-qid')));
                              }
                        }else{ 
                            if(fields[subQ[element]]+""==subVal[element]+"" || (fields[subQ[element]]+"").includes(subVal[element]+"")){
                                isError = true;
                                errorField.push(parseInt(document.getElementsByClassName(subQ[element]+'-serial')[0].getAttribute('data-qid')));
                              }
                        }
                    }else{
                        isError = true;
                        errorField.push(parseInt(document.getElementsByClassName(element+'-serial')[0].getAttribute('data-qid')));
                    }
                }
            }
        });
        if(!isError){
            //helper.getParentcloud(component, event, helper, fields);
            if(component.get("v.caseType") == component.get("v.VOSLA")){
                helper.getIssueOwner(component,event,helper,fields);
            } else{   
                component.find('recordEditForm').submit(fields);
            }
        }else{
            errorField.sort(function(a, b){return a-b});
            errorField.forEach(function(element) {
                if(errorFieldsString!=''){
                    errorFieldsString += ', '
                }    
                errorFieldsString += 'Q.' + element;                
            });        
           component.set('v.disabled', false);
           component.set('v.showSpinner', false);
           helper.showMessage('Error', 'Please answer following required questions : '+ errorFieldsString, 'error');
        }
        
        
    },
    openModal: function(component, event, helper) {
        // Set isModalOpen attribute to true
        component.set('v.disabled', true);
        component.set('v.showSpinner', false);
        component.set("v.isModalOpen", true);
    },
    makeAuraID: function(component, event, helper, controllingField){    
        if(controllingField=="Existing_Work_Story__c"
            || controllingField=="AppointmentBookingQ"){
            return controllingField;                           
        }else if(component.get("v.caseType") == component.get("v.VOSLA")){
            return 'BUG_' + controllingField;  
        }else if(component.get("v.caseType") == component.get("v.ASSESSMENT")){
            return 'AIT_' + controllingField; 
        }else if(component.get("v.caseType") == component.get("v.SELFREPORTED")
            || component.get("v.caseType") == component.get("v.SECURITYASSESSMENT")){
            return 'SRS_' + controllingField; 
        }
	},
    getAuraIDTag: function(component, event, helper, controllingField){    
        if(controllingField=="Existing_Work_Story__c"
            || controllingField=="AppointmentBookingQ"){
            return '';                           
        }else if(component.get("v.caseType") == component.get("v.VOSLA")){
            return 'BUG_' 
        }else if(component.get("v.caseType") == component.get("v.ASSESSMENT")){
            return 'AIT_' 
        }else if(component.get("v.caseType") == component.get("v.SELFREPORTED")
            || component.get("v.caseType") == component.get("v.SECURITYASSESSMENT")){
            return 'SRS_' 
        }
	}
})
({
    doInit : function(component,event,helper) {
        helper.getDependencies(component,event,helper);
        helper.getRecordTypeID(component,event,helper);
        helper.getHelpURL(component,event,helper);
        helper.getOwnerDetail(component,event,helper);
        helper.getRequiredFields(component,event,helper);
    },
    onerror: function(component, event) {
            console.log(event);
            console.log(event.getParam("output").fieldErrors);
            console.log(JSON.stringify(event.getParam("output")));
            console.log(JSON.stringify(event));
        	component.set('v.disabled', false);
           	component.set('v.showSpinner', false);
        },
    caseTypeUpdate : function(component, event, helper) {
        var caseType = component.find("caseTypeInput");
        component.set("v.caseType",caseType.get("v.value"));
    },
    checkDependencies : function(component, event, helper){
        helper.checkDependencies(component,event,helper);        
    },
    formSuccess: function(component, event, helper) {
        component.set('v.showSpinner', false);
        // Set isModalOpen attribute to true
        var updatedRecord = event.getParams();      
        //helper.createWorkCaseRecords(component, event, helper,updatedRecord);
        helper.createUserSharingAccess(component, event, helper,updatedRecord);
        helper.bookAppointment(component,event,helper);
        component.set("v.recordID", updatedRecord.response.id);
        helper.openModal(component, event, helper);
        $A.get('e.force:refreshView').fire();
    },
    handleHelpClick: function(component, event, helper) {
        var navEvt = $A.get("e.force:navigateToURL");
        navEvt.setParams({
            "url": component.get("v.helpLinkURL")
        });
        navEvt.fire();
    },
    onSubmit: function(component, event, helper) {
        component.set('v.showSpinner', true);
        helper.ensureRequiredFields(component, event, helper);
    },
    onRadioChange : function(component, event, helper) {
        var selected = event.target.getAttribute('value');
        component.set('v.selectedSlot', selected);
    },    
    closeModal: function(component, event, helper) {
        // Set isModalOpen attribute to false  
        component.set("v.isModalOpen", false);
        if(component.get("v.isCommunity") == false){
            var navEvt = $A.get("e.force:navigateToSObject");
            navEvt.setParams({
                "recordId": component.get("v.recordID"),
                "slideDevName": "Detail"
            });  
            navEvt.fire();
        }else{
            var url = window.location.href;    
            if (url.indexOf('?') > -1){
                url += '&tabset-e0a4e=2'
            }else{
                url += '?tabset-e0a4e=2'
            }
            window.location.href = url;
           // $A.get('e.force:refreshView').fire();
        }
        
    },
    
    
    
})
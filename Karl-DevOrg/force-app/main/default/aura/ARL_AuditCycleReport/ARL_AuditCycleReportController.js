({
	doInit : function(component, event, helper) {
        var pageRef = component.get("v.pageReference");
        
        //For internal Salesforce
        if( pageRef ){
            component.set("v.recordId", pageRef.state.c__recordid);   
            component.set("v.communityRecordDetailPrefix", "");   
        }
        //For community
        else{
            component.set("v.recordId", helper.getURLParameterValue().c__recordid );   
            component.set("v.communityRecordDetailPrefix", "/KARLCommunity/s/detail");   
        }
        
        helper.retrieveData(component, event, helper);
	},
    
    goBack : function(component, event, helper) {
        var navService = component.find("navService");
        var pageReference = {
            "type": "standard__recordPage",
            "attributes": {
                "recordId": component.get("v.recordId"),
                "objectApiName": "Audit_Cycle__c",
                "actionName": "view"
            }
        }
        navService.navigate(pageReference);
    },
    
    handleSelectChangeEvent: function(component, event, helper) {
        let selectedValues = event.getParam("values");
        let items = [];
                
        if( selectedValues && selectedValues.size > 0 ){            
            Array.from( component.get("v.originalItems") ).forEach(function(node){
                if( selectedValues.has( node.fullControlScope ) ){
                    items.push(node);
                }
            }); 
            
            component.set("v.items", items);
        }
        else{
            component.set("v.items", component.get("v.originalItems") );
        }
    },
    
    openRecordInNewTab: function(component, event, helper) {
        //Using window.open as Target = _Blank dont works in the community.
        window.open(component.get("v.communityRecordDetailPrefix")+'/'+event.target.dataset.recid, '_blank');
    }
})
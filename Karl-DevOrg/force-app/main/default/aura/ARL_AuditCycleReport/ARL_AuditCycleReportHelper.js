({
	retrieveData : function(component, event, helper) {
        component.set("v.items",[]); 
        
		var action = component.get("c.getReport");
        action.setParams({auditCycleId : component.get("v.recordId") });
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if(state==="SUCCESS"){                
                let items = response.getReturnValue();
                component.set("v.items", items);   
                component.set("v.originalItems", items);   
                
                let opts = [];
                let isOptExists = new Set();

                for( let i = 0 ; i < items.length ; i++ ){
                    if( !isOptExists.has(items[i].fullControlScope) ){
                    	opts.push( {Id: items[i].fullControlScope ,Name : items[i].fullControlScope } );
                        isOptExists.add( items[i].fullControlScope );
                    }
                }
                              
                component.set("v.fullControlScopes", opts);
                
                var childComp = component.find('my-multi-select');
        		childComp.refresh();
            }
            else if(state === "ERROR"){
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        let toastParams = {
                            title: "Error",
                            message: errors[0].message, // Default error message
                            type: "error"
                        };
                        // Fire error toast
                        let toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams(toastParams);
                        toastEvent.fire();
                    }
                }
                else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);  
	},
    
    getURLParameterValue: function() {
 
        var querystring = location.search.substr(1);
        var paramValue = {};
        querystring.split("&").forEach(function(part) {
            var param = part.split("=");
            paramValue[param[0]] = decodeURIComponent(param[1]);
        });
 
        console.log('paramValue-' + paramValue);
        return paramValue;
    }
})
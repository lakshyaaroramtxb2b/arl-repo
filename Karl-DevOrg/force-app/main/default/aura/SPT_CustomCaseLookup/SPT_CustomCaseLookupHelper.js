({
    updateParentCaseId : function(component, event) {
        component.set("v.showSpinner", true);
        var action = component.get("c.updateParentCase");
        var currentText = component.get("v.selectRecordId");
        action.setParams({
            "recordId" : component.get("v.recordId"),
            "parentCaseId" : currentText
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            var updateDone = response.getReturnValue();
            if(state == "SUCCESS") {
                console.log("Record Updated");
                component.set("v.showSpinner", false);
                // window.setTimeout(
                //     function(){
                //         component.set("v.showSpinner", false);
                //     }, 5000
                // );
            }
            else {
                console.log("Error");
            }
        });
        $A.enqueueAction(action);  
    },
    removeCase : function(component, event, helper){
        component.set("v.showSpinner", true);
        var action = component.get("c.deleteCase");
        action.setParams({
            "recordId" : component.get("v.recordId")
        });
        action.setCallback(this, function(response){
            component.set("v.showSpinner", false);
            var state = response.getState();
            var updateDone = response.getReturnValue();
            if(state == "SUCCESS") {
                helper.showToast('success', 'Success','Case Removed successfully.');
                let urlString = window.location.href;
                let baseURL = urlString.substring(0, urlString.indexOf("/lightning"));
                window.location.reload();
                //window.location.href = baseURL+'/lightning/n/Case_List_View';
            }
            else if (state === "ERROR") {
                var errors = response.getError(); 
                helper.showToast('error', 'Error',errors[0].message);
            }
        });
        $A.enqueueAction(action);  
        
    },
    showToast : function(type, title, message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : title,
            message:message,
            type: type,
        });
        toastEvent.fire();
    },
})
({
    /*doInitCustomLookup : function(component, event, helper) {
        var caseRecordId = component.get("v.recordId");
        var action = component.get("c.getCaseNumber");
        action.setParams({
            "recordId" : caseRecordId
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state == "SUCCESS") {
                var parentCase = response.getReturnValue();
                console.log("Response: "+parentCase);
                console.log("Response1: "+parentCase[0]);
                if(parentCase[0] != null)
                    component.set("v.selectRecordName", parentCase[0].caseNumber);
                else 
                    component.set("v.selectRecordName", '');
                if(parentCase[0] != null)
                    component.set("v.selectRecordId", parentCase[0].parentId);
                else 
                    component.set("v.selectRecordId", '');
                if(parentCase!=null && parentCase.length>0) {
                    component.set("v.parentCasePresent", true);
                }
                else {
                    component.set("v.parentCasePresent", false);
                }
            }
        });
        $A.enqueueAction(action);
    }, */
    doInitCustomLookup : function(component, event, helper) {
        var caseRecordId = component.get("v.recordId");
        var action = component.get("c.getCaseDetail");
        action.setParams({
            "recordId" : caseRecordId
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state == "SUCCESS") {
                var caseDetail = response.getReturnValue();
                component.set("v.selectRecordName", caseDetail.caseNumber);
                component.set("v.selectRecordId", caseDetail.parentId);
                component.set("v.isValidRecordType", caseDetail.isValidRecordType);
                console.log('valid rec-> ', caseDetail.isValidRecordType);
                component.set("v.isOwnerNotQueueOrGroup", caseDetail.isOwnerNotQueueOrGroup);
                if(caseDetail.parentId != null){
                    component.set("v.parentCasePresent", true);
                }else{
                    component.set("v.parentCasePresent", false);
                }
            }
        });
        $A.enqueueAction(action);
    },

    searchField : function(component, event, helper) {
        var currentText = event.getSource().get("v.value");
        var resultBox = component.find('resultBox');
        component.set("v.LoadingText", true);
        if(currentText.length > 0) {
            $A.util.addClass(resultBox, 'slds-is-open');
        }
        else {
            $A.util.removeClass(resultBox, 'slds-is-open');
        }
        var action = component.get("c.getResults");
        action.setParams({
            "ObjectName" : component.get("v.objectName"),
            "fieldName" : component.get("v.fieldName"),
            "value" : currentText
        });
        
        action.setCallback(this, function(response){
            var STATE = response.getState();
            if(STATE === "SUCCESS") {
                component.set("v.searchRecords", response.getReturnValue());
                if(component.get("v.searchRecords").length == 0) {
                    console.log('000000');
                }
            }
            else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                    errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
            component.set("v.LoadingText", false);
        });
        
        $A.enqueueAction(action);
    },
    
    setSelectedRecord : function(component, event, helper) {
        var currentText = event.currentTarget.id;
        var resultBox = component.find('resultBox');
        $A.util.removeClass(resultBox, 'slds-is-open');
        //component.set("v.selectRecordName", currentText);
        component.set("v.selectRecordName", event.currentTarget.dataset.name);
        component.set("v.selectRecordId", currentText);
        console.log("REcord Name: "+component.get("v.selectRecordName"));
        component.find('userinput').set("v.readonly", true);
        component.set("v.parentCasePresent", true);
        helper.updateParentCaseId(component, event);
    }, 
    
    resetData : function(component, event, helper) {
        component.set("v.selectRecordName", "");
        component.set("v.selectRecordId", "");
        component.set("v.parentCasePresent", false);
        console.log("Id value: "+component.get("v.selectRecordId"));
        component.find('userinput').set("v.readonly", false);
        helper.updateParentCaseId(component, event);
    },

    navigateToCloseCaseComponent : function(component, event, helper) {
        component.set("v.showModalWindow", true);
    },
    removeCase : function (component, event, helper){
        var isValidRecordType       =      component.get("v.isValidRecordType");
        console.log('isValidRecordType',isValidRecordType);
        var isOwnerNotQueueOrGroup  =      component.get("v.isOwnerNotQueueOrGroup");
        console.log('isOwnerNotQueueOrGroup', isOwnerNotQueueOrGroup);
        if(isValidRecordType && isOwnerNotQueueOrGroup){
             helper.removeCase(component, event, helper);
        }else{
            component.set("v.showWarningModal", true);
        }
        
    },
    handleClose: function(component, event, helper) {
        component.set("v.showWarningModal", false);
    }
})
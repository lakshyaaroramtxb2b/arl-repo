({
    isQuestionVisible: function (cmp, event, helper) {
        var question = event.getParam('arguments').question;
        var checkDisplayEvt = $A.get("e.c:esaCheckDisplayEvt").setParams({
                                    "id" : question.serviceItemQuestion.ESA_Security_Question__c
                               });
        checkDisplayEvt.fire();
        return $A.util.getBooleanValue(checkDisplayEvt.getParam("isVisible"));
    },

    findCmpByType: function (cmp, event, helper) {  
        var parentCmp = event.getParam('arguments').parentCmp;  
        var type = event.getParam('arguments').type;   
        var includeAll = $A.util.getBooleanValue(event.getParam('arguments').includeAll);    
        var elems = parentCmp.find({"instancesOf" : type});
        if (!includeAll && $A.util.isArray(elems)) {
           // Returns null when array is empty
           return elems[0];           
        }
        return elems;
    }
})
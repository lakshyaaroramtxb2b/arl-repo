({
	doInit : function(cmp, event, helper) {
		if (cmp.get("v.config.settings.Page_Template__c") == 'Embedded') {
			cmp.set("v.isVisible", false);
		}
	}
})
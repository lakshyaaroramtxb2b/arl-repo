({
	doInit : function(component,event,helper) {
        console.log('IN getRecord');
        var action= component.get("c.getRecord");
        // set param to method  
        action.setParams({
            'recordId':component.get("v.recordId")
        });
                console.log(component.get("v.recordId"));
        action.setCallback(this,function(response){
            var state= response.getState();
              $A.log(response);
            if(state == "SUCCESS"){
                console.log(response.getReturnValue());
                component.set("v.caseRecord",response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    }
})
({
	doInit : function(cmp, event, helper) {
        // Handle onShow/onHide events
        helper.handleShowHideEvt(cmp);
    },
    
    toggleExpand : function(cmp, event, helper) {
		var isExpanded = $A.util.getBooleanValue(cmp.get("v.isExpanded"));
        // Toggle
		cmp.set("v.isExpanded", !isExpanded);
        
        // Handle onShow/onHide events
        helper.handleShowHideEvt(cmp);
	}
})
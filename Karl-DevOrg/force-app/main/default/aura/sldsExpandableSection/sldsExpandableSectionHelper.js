({
    handleShowHideEvt : function(cmp) {
        var isExpanded = $A.util.getBooleanValue(cmp.get("v.isExpanded"));
        if (isExpanded) {
            cmp.getEvent("onShow").fire();
        } else {
            cmp.getEvent("onHide").fire();
        }
    }
})
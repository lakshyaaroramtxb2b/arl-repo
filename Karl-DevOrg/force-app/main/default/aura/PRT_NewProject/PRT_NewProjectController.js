({
    doInit : function(component, event, helper) {
        var createRecordEvent = $A.get("e.force:createRecord");
        var recordTypeid = $A.get("{!$Label.c.PRT_Project_Record_Type_ID}");
        console.log(recordTypeid);
        createRecordEvent.setParams({
            "entityApiName": "Project__c",
            "defaultFieldValues": {
                'RecordTypeId' : recordTypeid
            }
        });
        createRecordEvent.fire();
    }
})
({
	doInit : function(component, event, helper) {
        
        //Calling base component's helper method to call AuraEnabled Method
        helper.doCallout(component,'c.getMileStones', {
            'recordId':component.get("v.recordId")
        })
        .then(
            $A.getCallback(function(response){
                console.log('Response Data--> ', JSON.stringify(response));
                if(response){
                    if(response.isSecurityReviewer){
                        console.log('inside security reviewer');
                         component.set('v.columns', [
                            {label: 'Name', fieldName: 'linkName', type: 'url',
                             typeAttributes: {label: { fieldName: 'Name' }, target: '_blank'}},
                            {label: 'Status', fieldName: 'Status__c', type: 'text'},
                            {label: 'Description', fieldName: 'Description__c', type: 'text'},
                            {label: 'Due Date', fieldName: 'Due_Date__c', type: 'Date'}
                        ]);
                    }else if(response.isStandardUser){
                        console.log('inside standard User');
                        component.set('v.columns', [
                            {label: 'Name', fieldName: 'linkName', type: 'url', 
                             typeAttributes: {label: { fieldName: 'Name' }, target: '_blank'}},
                            {label: 'Due Date', fieldName: 'Due_Date__c', type: 'Date'}
                        ]);
                        if(response && response.milestoneList != undefined){
                            response.milestoneList.forEach(function(record){
                                record.linkName = '/'+record.Id;
                            });
                        }
                    }else{
                        component.set('v.columns', [
                            {label: 'Name', fieldName: 'Name', type: 'text'},
                            {label: 'Due Date', fieldName: 'Due_Date__c', type: 'Date'}
                        ]);
                        
                    }
                    
                    component.set("v.records",response.milestoneList);
                }
                
            }),
            $A.getCallback(function(error) {
                helper.showMessage('Error', error[0].message, 'error');
            })
        );
        
        /*
		console.log('IN getRecord');
        var action= component.get("c.getMileStones");
        // set param to method  
        action.setParams({
            'recordId':component.get("v.recordId")
        });
                console.log(component.get("v.recordId"));
        action.setCallback(this,function(response){
            var state= response.getState();
              $A.log(response);
            if(state == "SUCCESS"){
                console.log(response.getReturnValue());
                component.set('v.columns', [
                    {label: 'Name', fieldName: 'linkName', type: 'url', 
            			typeAttributes: {label: { fieldName: 'Name' }, target: '_blank'}},
                    {label: 'Due Date', fieldName: 'Due_Date__c', type: 'Date'}
                ]);
				var records =response.getReturnValue();
                records.forEach(function(record){
                    record.linkName = '/'+record.Id;
                });
                
                component.set("v.records",records);
            }
        });
        $A.enqueueAction(action); */
	}
})
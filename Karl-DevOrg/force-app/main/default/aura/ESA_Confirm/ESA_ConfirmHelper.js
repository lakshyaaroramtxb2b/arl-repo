({
	confirm : function(cmp) {
        var isShowing = $A.util.getBooleanValue(cmp.get("v.showDialog"));
        var confirmations = cmp.get("v.confirmationEvts");
        //console.log("isShowing=" + isShowing);
        //console.log("confirmations=" + confirmations);
        if (!isShowing && !$A.util.isEmpty(confirmations)) {
            var confirmEvt = confirmations.shift();
            cmp.set("v.confirmConfig", confirmEvt.getParams());
            cmp.set("v.showDialog", true);  
        }
	},
    
    closeAndHandleNext : function(cmp) {
        cmp.set("v.showDialog", false);
        this.confirm(cmp);
    }
})
({
	handleConfirm : function(cmp, event, helper) {        
		var confirmations = cmp.get("v.confirmationEvts");
        confirmations.push(event);
        helper.confirm(cmp);
	},
    
    handleOk : function(cmp, event, helper) {
        var confirmConfig = cmp.get("v.confirmConfig");
        if (!$A.util.isUndefinedOrNull(confirmConfig.onOk)) {
            confirmConfig.onOk();
        } 
        helper.closeAndHandleNext(cmp);
    },
    
    handleCancel : function(cmp, event, helper) {
        var confirmConfig = cmp.get("v.confirmConfig");
        if (!$A.util.isUndefinedOrNull(confirmConfig.onCancel)) {
            confirmConfig.onCancel();
        } 
        helper.closeAndHandleNext(cmp);
    }
})
({
    sortBy: function(component, field) {
        var sortAsc = component.get("v.sortAsc"),
            sortField = component.get("v.sortField"),
            records = component.get("v.data");
        sortAsc = sortField != field || !sortAsc;
        records.sort(function(a,b){
            var t1 = a[field] == b[field],
                t2 = (!a[field] && b[field]) || (a[field] < b[field]);
            return t1? 0: (sortAsc?-1:1)*(t2?1:-1);
        });
        component.set("v.sortAsc", sortAsc);
        component.set("v.sortField", field);
        component.set("v.data", records);
        this.renderPage(component);
    },
	renderPage: function(component,event, helper) {
		var records = component.get("v.data"),
            pageNumber = component.get("v.pageNumber"),
            pageSize = component.get("v.pageSize"),
            
            pageRecords = records.slice((pageNumber-1)*pageSize, pageNumber*pageSize);
        component.set("v.currentList", pageRecords);
        this.showHide(component);
	},
    showHide : function(component) {
		var currentPageNumber =  component.get("v.pageNumber");
        var maxPageNumber =  component.get("v.maxPage");
        if(currentPageNumber == 1){
            component.set("v.firstPage", true);
        }else{
            component.set("v.firstPage", false);
        }
        if(currentPageNumber == maxPageNumber){
            component.set("v.lastPage", true);
        }else{
            component.set("v.lastPage", false);
        }
	},
    getV2moms : function(component, helper) {
        
        component.set("v.showSpinner", true);
        var action = component.get("c.getV2Mom");
        var fromDate = component.get("v.fromDate");
        var toDate = component.get("v.toDate");
        if(fromDate != null && toDate !=null){
            fromDate = fromDate.toString();
            toDate = toDate.toString();
            action.setParams({
                'fromDate' : fromDate,
                'toDate' : toDate,
            });
            action.setCallback(this,function(response) {
                component.set("v.showSpinner", false);
                var state = response.getState();
                if (state === "SUCCESS") {
                    var records = response.getReturnValue();
                    component.set("v.data", records);
                    component.set("v.pageNumber", 1);
                    var pageSize = component.get("v.pageSize")
                    component.set("v.maxPage", Math.floor((records.length+(pageSize - 1))/pageSize));
                    helper.sortBy(component, "Name__c");
                }
            });
            $A.enqueueAction(action);
        }else{
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "type": "error",
                "title": "Error!",
                "message": "Must select From and To Date."
            });
            toastEvent.fire();
            
        }
        
    }
})
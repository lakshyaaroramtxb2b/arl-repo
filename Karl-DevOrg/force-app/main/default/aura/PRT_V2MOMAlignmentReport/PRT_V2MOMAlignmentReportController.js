({
	doInit: function(component, event, helper) {
        var today = new Date();
        component.set('v.toDate', $A.localizationService.formatDate(today, "YYYY-MM-DD"));
        today.setMonth(today.getMonth() - 12);
        component.set('v.fromDate', $A.localizationService.formatDate(today, "YYYY-MM-DD"));
        helper.getV2moms(component, helper);
        
	},
    filterData : function (component, event, helper){
        helper.getV2moms(component, helper);
    },
    sortByName: function(component, event, helper) {
        helper.sortBy(component, "Name__c");
    },
    sortByV2momUser: function(component, event, helper) {
        helper.sortBy(component, "V2MOM_User_c__c");
    },
    sortByPublishedOn: function(component, event, helper) {
        helper.sortBy(component, "Published_on_c__c");
    },
    renderPage: function(component, event, helper) {
        helper.renderPage(component, event, helper);
    }
})
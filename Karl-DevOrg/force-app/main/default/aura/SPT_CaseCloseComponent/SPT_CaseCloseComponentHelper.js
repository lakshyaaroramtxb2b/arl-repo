({
    fetchPickListVal: function(component, event, helper) {
        console.log("Inside Feth PicklistVal");
        var action = component.get("c.getCloseStatusValues");
        action.setParams({
            "allValues": component.get("v.allStatus")
        });
        var opts = [];
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                console.log("Values: "+allValues);
                
                if (allValues != undefined && allValues.length > 0) {
                    opts.push({
                        label: "--- None ---",
                        value: ""
                    });
                }
                for (var i = 0; i < allValues.length; i++) {
                    opts.push({
                        label: allValues[i],
                        value: allValues[i]
                    });
                }
                component.set("v.options", opts);
            }
        });
        $A.enqueueAction(action);
    },
})
({
	doInit: function(component, event, helper) {
        helper.fetchPickListVal(component, event, helper);
        console.log("Options: "+component.get("v.options"));
    },
    
    
    updateStatus: function(component, event, helper) {
        // get the value of select option
        var selectedValue = component.find("statusPicklist").get("v.value");
        
        
        if(selectedValue == "" || selectedValue == '--- None ---') {
            var infoEvent = $A.get("e.force:showToast");
    		infoEvent.setParams({
                "title": "Information!",
                "message": "Please select proper value.",
                "type": "info"
            });
            infoEvent.fire();
        }
        else {
            var idArray = [];
        	idArray.push(component.get("v.recordId"));
        	if(!component.get("v.bulkMode")) {
            	component.set("v.caseIdsList", idArray);
        	}
        	var caseIdList = component.get("v.caseIdsList");
            var action = component.get("c.saveCaseStatus");
            var checkB = component.find("checkbox").get("v.value");
            
            action.setParams({
                "caseIdList": caseIdList,
                "statusVal": selectedValue,
                "emailFlag": checkB
            });
            action.setCallback(this, function(response) {
                if(response.getState() == "SUCCESS") {
                    var notUpdatedCase = response.getReturnValue();
                    var notUpdatedCaseArr = [];
                    if(notUpdatedCase) {
                        notUpdatedCaseArr = notUpdatedCase.split(',');
                    }
                    var messageString = "Following cases already had this status value: \n"+notUpdatedCase;
                    if(notUpdatedCase) {
                        /*if(component.get("v.bulkMode")) {
                         	var infoEvent = $A.get("e.force:showToast");
                            infoEvent.setParams({
                                "title": "Information!",
                                "message": messageString,
                                "type": "info",
                                "mode": "sticky"
                            });
                            infoEvent.fire();
                            component.set("v.showModal", false);   
                        }*/
                        if(!component.get("v.bulkMode")) {
                            var infoEvent = $A.get("e.force:showToast");
                            infoEvent.setParams({
                                "title": "Information!",
                                "message": "Case already has this status.",
                                "type": "info"
                            });
                            infoEvent.fire();
                        }
                    }
                    if(notUpdatedCaseArr.length != caseIdList.length) {
                    	var successEvent = $A.get("e.force:showToast");
                        successEvent.setParams({
                            "title": "Success!",
                            "message": "Case status updated successfully.",
                            "type": "success"
                        });
                        successEvent.fire();
                        component.set("v.showModal", false);
                        window.location.reload();
                    }
                }
            });
            $A.enqueueAction(action);
        }
    },

    handleSave : function(component, event, helper) {
        if(component.get("v.bulkMode")) {
            component.set("v.showConfirmationWindow", true);
        }
        else {
            var action = component.get("c.updateStatus");
            $A.enqueueAction(action);
        }
    },

    handleClose: function(component, event, helper) {
        component.set("v.showModal", false);
    }
})
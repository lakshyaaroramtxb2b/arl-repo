/**
 * @File Name          : SC_Email_ExternalCommunicationHelper.js
 * @Description        : 
 * @Author             : Hardik
 * @Group              : 
 * @Last Modified By   : Banshi
 * @Last Modified On   : 10/14/2019, 3:41:12 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    10/14/2019   Banshi     Initial Version
**/
({
    doCallout : function(cmp, methodName, params, callBackFunc){
        var action = cmp.get(methodName);
        action.setParams(params);
        action.setCallback(this, callBackFunc);
        $A.enqueueAction(action);
    },
    
    fetchData : function(component, event, helper) {
        component.set('v.showSpinner', true); 
        try{
            helper.doCallout(component,"c.fetchData",{}, function(response){
                var state = response.getState();
                if(state === 'SUCCESS') {
                    var _resp = response.getReturnValue();
                    console.log('Resp from INIT: ', _resp);
                    component.set('v.wrapperData', _resp);
                    component.set('v.showSpinner', false); 
                } else {
                    component.set('v.showSpinner', false); 
                }
            });    
        } 
        catch(Ex) {
            component.set('v.showSpinner', false); 
        }
    },
    
    /**
     * @author Vishnu Kumar
     * @email vishnu.kumar@mtxb2b.com
     * @desc This method get called in recursion untill all the records are not get updated to the Pending status.
    */
    updateCampaignMemberStatusAsPendingFun : function(component, event, helper){    
        var records = component.get("v.wrapperData.staticRecordsList");
        if(records!=null && typeof records != 'undefined' && records.length > 0){
            var allValid = component.find('requiredField').reduce(function (validSoFar, inputCmp) {
                inputCmp.showHelpMessageIfInvalid();
                return validSoFar && !inputCmp.get('v.validity').valueMissing;
            }, true);
        }
                
        //Calling method in recursion to update status of records to pending if all the required fields are filled.
        if( allValid ){
            component.set('v.showSpinner', true); 
            component.set('v.inProgressMessage', 'Queuing the list of emails. Please don\'t refresh or leave this page.');
            
            try{
                helper.doCallout(component,"c.updateCampaignMemberStatusAsPending",{}, function(response){
                    var state = response.getState();
                    if(state === 'SUCCESS') {
    
                        //If it returns true that means there are not records to update for Pending
                        if( response.getReturnValue() ){  
                            component.set('v.inProgressMessage', 'Placing the email sending job. Please don\'t refresh or leave this page.');
                            helper.doSendEmails(component, event, helper);
                        }
                        else{
                            helper.updateCampaignMemberStatusAsPendingFun(component, event, helper);
                        }
                    }
                    else {
                        component.set('v.showSpinner', false); 
                    }
                });    
            } 
            catch(Ex) {
                component.set('v.showSpinner', false); 
            }
        }
    },
    
    /**
     * @author Vishnu Kumar
     * @email vishnu.kumar@mtxb2b.com
     * @desc This method will schedule a batch to send emails to all the Pending records.
    */
    doSendEmails : function(component, event, helper){    
        //Generating list of emails
        let _accList = [];
        let _listOfCampaignMembers = component.get('v.wrapperData.listOfCampaignMembers');
        _listOfCampaignMembers.forEach(function(item){
            let _accEmail = {};
            _accEmail.accEmail = item.Account_Emails__c;
            _accEmail.id = item.Id;
            
            if(_accEmail){
                _accList.push(_accEmail);
            }
        });

        //component.set('v.wrapperData.listOfCampaignMembers', _listOfCampaignMembers);
       
        //Place the job to send the emails.
        helper.doCallout(component,"c.sendEmails", {
            statusRecords : JSON.stringify(component.get('v.wrapperData.staticRecordsList')),
            accEmailRecords : _accList 
        },function(response){
            var state = response.getState();
            
            if( state === 'SUCCESS') {
                helper.showToast('success', 'Success', 'A job has been placed successfully to send emails.');
                
                //Refresh the data
                helper.fetchData(component, event, helper);
            }
            
            component.set('v.showSpinner', false);
            component.set('v.inProgressMessage', '');
        });
    },
    
    /**
     * @author Vishnu Kumar
     * @email vishnu.kumar@mtxb2b.com
     * @desc This is generic method to show the toast message.
    */
    showToast : function(type , title, message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "message": message,
            "type" : type
        });
        toastEvent.fire();
    },
    
    fetchErrorData : function(component, event, helper) {
        component.set('v.showSpinner', true); 
        try{
            helper.doCallout(component,"c.fetchErrorRecords",{}, function(response){
                var state = response.getState();
                if(state === 'SUCCESS') {
                    var _resp = response.getReturnValue();
                    component.set('v.wrapperData.listOfCampaignMembers', _resp.listOfCampaignMembers);
                    component.set('v.wrapperData.noOfRecordsDisplayed', _resp.noOfRecordsDisplayed);
                    component.set('v.showSpinner', false); 
                } else {
                    component.set('v.showSpinner', false); 
                }
            });    
        } 
        catch(Ex) {
            component.set('v.showSpinner', false); 
        }
    },
    
})
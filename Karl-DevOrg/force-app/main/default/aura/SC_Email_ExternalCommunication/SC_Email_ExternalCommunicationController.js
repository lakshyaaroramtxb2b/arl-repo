({
    doInit : function(component, event, helper) {        
        helper.fetchData(component, event, helper);
    },
    
    processForSendEmails : function(component, event, helper){    
        helper.updateCampaignMemberStatusAsPendingFun(component, event, helper);
    },
    
    handleDeleteEmails : function(component, event, helper) {
        component.set('v.showSpinner', true); 
        
        try{ 
            helper.doCallout(component,"c.deleteData",{}, function(response){
                var state = response.getState();
                if(state === 'SUCCESS') {
                    var _resp = response.getReturnValue();
                    
                    component.set('v.wrapperData', _resp);
                    component.set('v.showSpinner', false); 
                } 
                else {
                    component.set('v.showSpinner', false); 
                }
            });   
        } 
        catch(ex) {
            component.set('v.showSpinner', false);  
        }
    },
    
    handleEditRec : function(component, event, helper) {     
        component.set('v.recordId', event.currentTarget.name);
        
        var recId = event.currentTarget.name;
        var editRecordEvent = $A.get("e.force:editRecord");
        editRecordEvent.setParams({
            "recordId" : recId,
            "navigationLocation" : "LOOKUP",
            "panelOnDestroyCallback" : function(event){},
            "successAction" : function(event){}
        });
        editRecordEvent.fire();
    },
    
    showErrorsInTableFromFrontend : function(component, event, helper) {
		
        var _listOfCampaignMembers = component.get('v.wrapperData').listOfCampaignMembers;
        
        try {            
            component.set('v.showSpinner', true); 
            
            var _resp = [];
            _listOfCampaignMembers.find((cam) => {
                var _cam = (cam.SuccessOrError__c) ? cam.SuccessOrError__c : '' ;
                
                if(_cam && _cam != 'Success') {
                	_resp.push(cam);
            	}
            });
            
            component.set('v.wrapperData.listOfCampaignMembers', _resp);
            component.set('v.showSpinner', false); 
        } 
        catch (ex) {
            component.set('v.showSpinner', false); 
        }        
    }, 
    
    showErrorsInTableFromBackend : function(component, event, helper) {
      	helper.fetchErrorData(component, event, helper);
    }, 
    
    recordUpdated :  function(component, event, helper) {
        if(component.get('v.recordId')) {
            helper.fetchData(component, event, helper);
        }
    },
})
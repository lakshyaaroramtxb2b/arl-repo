({    
    toggleShowDialog : function(cmp, isShow) { 
        if (isShow) {
           cmp.find("dialog").getEvent("show").fire();
           this.fetchCollaborators(cmp);
        } else {
           cmp.find("dialog").getEvent("hide").fire();
           this.cleanUp(cmp);
        }
	},
    
    spliceUserByEmail : function (cmp, email) {
        var searchResults = cmp.get("v.searchResults");
        for (var i=0;i<searchResults.length;i++) {
            var user = searchResults[i];
            if (user.email == email) {
                searchResults.splice(i, 1);
                cmp.set("v.searchResults", searchResults);
                return user;
            }
        }
    },
    
    fetchCollaborators : function(cmp) {
        this.toggleBusy(cmp, true);
        var action = cmp.get("c.getCollaborators");
        action.setParams({"securityRequest": cmp.get("v.securityRequest")});
        action.setCallback(this, function(response) {
            this.toggleBusy(cmp, false);
            var THIS = this;
            $A.get("e.c:esaHandleResponse").setParams({
                "response": response,
                "messageId" : "shareDialog",
                "onSuccess" : $A.getCallback(function(returnVal) {
                    cmp.set("v.selectedUsers", THIS.processCollaborators(returnVal));
                })
            }).fire();
        });
        $A.enqueueAction(action);
    },
    
    searchUsers : function(cmp, searchText) {
        if ($A.util.isEmpty(searchText)) {
            cmp.set("v.searchResults", []);
            return;
        }
        var action = cmp.get("c.searchUsers");
        action.setParams({"searchText": searchText,
                          "entityCode": cmp.get("v.securityRequest.Entity_Code__c")});
        action.setCallback(this, function(response) {
            var THIS = this;
            $A.get("e.c:esaHandleResponse").setParams({
                "response": response,
                "messageId" : "shareDialog",
                "onSuccess" : $A.getCallback(function(returnVal) {
                      cmp.set("v.searchResults", THIS.processEmployees(returnVal)); 
                })
            }).fire();
        });
        $A.enqueueAction(action);
    },
    
    updateUsers : function(cmp) {
        var collaborators = [];
        var selectedUsers = cmp.get("v.selectedUsers");
        for (var i=0;i<selectedUsers.length;i++) {
            collaborators.push({
                "Collaborator_Email__c" : selectedUsers[i].email
            });
        }
        this.toggleBusy(cmp, true);
        var action = cmp.get("c.updateCollaborators");
        action.setParams({"collaboratorsJson": JSON.stringify(collaborators),
                          "securityRequest" : cmp.get("v.securityRequest")
                         });
        action.setCallback(this, function(response) {
            var THIS = this;
            this.toggleBusy(cmp, false);
            $A.get("e.c:esaHandleResponse").setParams({
                "response": response,
                "messageId" : "shareDialog",
                "onSuccess" : $A.getCallback(function(returnVal) {
                    THIS.toggleShowDialog(cmp, false);
                    $A.get("e.c:esaShowMessage").setParams({
                        "text": $A.get("$Label.c.ESA_SuccessCollaborationChanges"), 
                        "variant": "success"
                    }).fire();
                })
            }).fire();            
        });
        $A.enqueueAction(action);
    },
        
    processEmployees : function(empList) {
        var users = []
        if ($A.util.isArray(empList)) {
            for(var i=0;i<empList.length;i++) {
               var emp = empList[i];
               users.push(this.createUser(
                   emp.Id,
                   emp.INTEG_First_Name__c,
                   emp.INTEG_Last_Name__c ,
                   emp.INTEG_Email__c 
               ));
            }
        }
        return users;
    },
        
    processCollaborators : function(collabList) {
        var users = [];
        if ($A.util.isArray(collabList)) {
            for(var i=0;i<collabList.length;i++) {
               var collab = collabList[i];
               //console.log(collab);
               users.push(this.createUser(
                   collab.Employee__r.Id,
                   collab.Employee__r.INTEG_First_Name__c,
                   collab.Employee__r.INTEG_Last_Name__c ,
                   collab.Collaborator_Email__c,
                   collab.isOwner__c
               ));
            }
        }
        return users;
    },
    
    createUser : function(id, firstName, lastName, email, isOwner) {
        return {
            "id" : id,
            "name" : (firstName || "") + " " + (lastName || ""),
            "email" : email || "",
            "isOwner" : isOwner || false
        }
    },
    
    clearSearch : function(cmp) {
        if(!$A.util.isEmpty(cmp.get("v.currentSearchText"))) {
            cmp.set("v.searchText", "");
            cmp.set("v.currentSearchText", "");
            cmp.set("v.searchResults", []);
        }  
    },
    
    cleanUp : function(cmp) {
        this.clearSearch(cmp);
        cmp.set("v.selectedUsers", []);
        $A.get("e.c:esaClearMessage").setParams({
            "id" : "shareDialog",
            "doNotClearSticky" : false
        }).fire();
    },
    
    toggleBusy : function(cmp, isBusy) {
        if (isBusy) {
           cmp.find('busy').get('e.show').fire(); 
        } else {
           cmp.find('busy').get('e.hide').fire();
        }
    }
})
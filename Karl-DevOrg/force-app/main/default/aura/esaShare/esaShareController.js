({
    handleServiceItemSelection : function(cmp, event, helper) {
        var config = event.getParam("config");        
        var securityRequest = event.getParam("securityRequest");
        //console.log(config);
        //console.log(securityRequest);
        cmp.set("v.config", config);
        cmp.set("v.securityRequest", securityRequest);  
    },
    
	showShareDialog : function(cmp, event, helper) {
        $A.get("e.c:esaClearMessage").setParams({"doNotClearSticky" : false}).fire();
        if ($A.util.isEmpty(cmp.get("v.securityRequest.Id"))) {
           $A.get("e.c:esaShowMessage").setParams({
               "text": $A.get("$Label.c.ESA_WarningSaveToShare"), 
               "variant": "info"
           }).fire();
        } else {
		   helper.toggleShowDialog(cmp, true);
        }
	},
    
    removeUser : function(cmp, event, helper) {
		var email = event.getSource().get("v.name");
		var selectedUsers = cmp.get("v.selectedUsers");
        for (var i=0;i<selectedUsers.length;i++) {
            var user = selectedUsers[i];
            if (user.email == email) {
                selectedUsers.splice(i, 1);
                break;
            }
        }
        cmp.set("v.selectedUsers", selectedUsers);
	},
    
    addUser : function(cmp, event, helper) {
        var email = event.getSource().get("v.name");
        var selectedUser = helper.spliceUserByEmail(cmp, email);
		var selectedUsers = cmp.get("v.selectedUsers");
        selectedUsers.unshift(selectedUser);
        cmp.set("v.selectedUsers", selectedUsers);
        helper.clearSearch(cmp);
	},
    
    updateUsers : function(cmp, event, helper) {
		helper.updateUsers(cmp);
	},
    
    updateUserSearch : function(cmp, event, helper) {
        var searchText = event.getParam("data")["searchText"];
        helper.searchUsers(cmp, searchText);
	},
    
    clearSearch : function(cmp, event, helper) {
        helper.clearSearch(cmp);        
    }
})
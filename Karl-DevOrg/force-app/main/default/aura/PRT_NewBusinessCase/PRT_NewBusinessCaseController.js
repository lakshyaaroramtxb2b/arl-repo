({
    doInit : function(component, event, helper) {
        component.set("v.recordTypeId",$A.get("{!$Label.c.PRT_Business_Record_Type_ID}"));
    },
    handleSuccess : function(component, event, helper){
        var params = event.getParams(); //get event params
		var recordId = params.response.id; //get record id
        
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": recordId
        });
        navEvt.fire();
    },
    handleLoad: function(cmp, event, helper) {
        cmp.set('v.showSpinner', false);
    },

    handleSubmit: function(cmp, event, helper) {
        console.log('Submit');
        cmp.set('v.showSpinner', true);
    },

    handleError: function(cmp, event, helper) {
        console.log('handleError');
        // errors are handled by lightning:inputField and lightning:nessages
        // so this just hides the spinnet
        cmp.set('v.showSpinner', false);
    },
    handleClose: function(cmp, event, helper) {
        console.log('handlClose');
        // errors are handled by lightning:inputField and lightning:nessages
        // so this just hides the spinnet
        cmp.set('v.showSpinner', false);
        cmp.set('v.showModal', false);
    },
})
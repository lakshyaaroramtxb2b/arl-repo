({
	toggleShowDialog : function(cmp, isShow) { 
        if (isShow) {
            cmp.getEvent("onShow").fire();
        } else {
            cmp.getEvent("onHide").fire();
        }
        cmp.set("v.showDialog", isShow);
	}
})
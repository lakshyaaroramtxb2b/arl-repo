({
    showDialog : function(cmp, event, helper) {
		helper.toggleShowDialog(cmp, true);
	},
    
    hideDialog : function(cmp, event, helper) {
		helper.toggleShowDialog(cmp, false);
	}
})
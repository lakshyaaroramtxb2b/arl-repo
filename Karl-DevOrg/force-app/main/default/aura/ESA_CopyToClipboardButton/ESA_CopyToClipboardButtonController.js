({
    copyToClipBoard : function(cmp, event, helper) {
        $ESAUtil.stopEvent(event);
        $ESAUtil.copyToClipBoard(cmp.get("v.value"));
    }
})
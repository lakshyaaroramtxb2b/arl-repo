/**
 * Created by Banshi on 2019-08-01.
 */
({
    /*
     * This method will call the server side action and will execute callback method
     * it will also show error if generated any
     * @param component (required) - Calling component
     * @param method (required) - Server side(@AuraEnabled) method name
     * @param callback (required) - Callback function to be executed on server response
     * @param params (optional) - parameter values to pass to server
     * @param extraParams(optional) - extra utility params
     * */
    callServer : function(component, method, callback, params, extraParams) {
        var action = component.get(method);
        var extraParams = Object.assign({setStorable: false}, extraParams);
        //Set params if any
        if (params) {
            action.setParams(params);
        }

        if(extraParams.setStorable){
            action.setStorable();
        }

        action.setCallback(this,function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                // pass returned value to callback function
                callback.call(this,response.getReturnValue());
            } else if (state === "ERROR") {
                // generic error handler
                var errors = response.getError();
                if (errors) {
                    console.log("Errors", errors);
                    this.showToast({
                        "title": "ERROR IN SERVER CALL",
                        "type": "error",
                        "message": errors
                    });
                    if (errors[0] && errors[0].message) {
                        throw new Error("Error" + errors[0].message);
                    }
                } else {
                    throw new Error("Unknown Error");
                }
            }
        });

        $A.enqueueAction(action);
    },

    /*
     * This function displays toast based on the parameter values passed to it
     * */
    showToast : function(params) {
        var toastEvent = $A.get("e.force:showToast");
        if(toastEvent){
            if(!params){
                toastEvent.setParams({
                    "title": "TOAST ERROR!",
                    "type": "error",
                    "message": "Toast Param not defined"
                });
                toastEvent.fire();
            } else{
                toastEvent.setParams(params);
                toastEvent.fire();
            }
        } else{
            alert(params.message);
        }
    },
    
    /*
    * @description :Use this method to call any apex class method
    * @params :  component : instance of component
    *         : method : name of apex class method (for ex. 'c.getAccount')
    *         : params : params needs to be passed to apex class. should be in json format. for example {'nameString' : 'testName'}
    * @return : promise with either results or error
    */
    doCallout: function (component, method, params) {
        return new Promise($A.getCallback(function (resolve, reject) {
            // Set action and param
            var action = component.get(method);
            if (params != null) {
                action.setParams(params);
            }
            // Callback
            //
            action.setCallback(component, function (response) {
                console.log('in action');
                var state = response.getState();
                if (component.isValid() && state === "SUCCESS") {
                    resolve(response.getReturnValue());
                } else {
                    var errors = response.getError();
                    reject(errors);
                }
            });
            $A.enqueueAction(action);
        }));
    },
    /*
    * @description : Use this method to show any toast message
    * @params : title : title for toast
    *         : message : message to show in toast
    *         : type : type can be info , success,warning,error
    * @return : promise with either results or error
    */
    showMessage: function(title, message,type) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title" : title,
            "message" : message,
            "type" : type
        });
        toastEvent.fire();
    },
    /*
    * @description : Use this method to open any custom component in modal
    * @params : component : instance of component
    *         : componentName : name of custom component to open in modal
    *         : attributes : attributes of custom component in json format
    *         : customClasses : any custom classes to apply on modal
    * @return : promise with either results or error
    */
    openModal : function(component,componentName,attributes,customClasses){
        return new Promise(function(resolve,reject){
            var overlayLib = component.find('overlayLib');
            //if component has overlay library, then only go ahead otherwise show error
            if(componentName  && componentName.trim() !== ''){
                if(overlayLib){
                    $A.createComponent(
                        componentName,
                        attributes || {},
                        function(content, status, errorMessage){
                            if (status === "SUCCESS") {
                                component.find('overlayLib').showCustomModal({
                                    body : content || '',
                                    cssClass : customClasses || ''
                                }).then(function(modal){
                                    resolve(modal);
                                },function(error){
                                    reject();
                                    console.log('ERROR::MTXBASE::Error while creating overlay '+ error);
                                })
                            }else{ //error while creating new component
                                reject();
                                console.log('ERROR::MTXBASE::Error while creating new component '+ errorMessage);
                            }
                        }
                    );
                }else{
                    reject();
                    console.log('ERROR::MTXBASE::Please add "<lightning:overlayLibrary aura:id="overlayLib" />" in child component ');
                }
            }else{
                reject();
                console.log('ERROR::MTXBASE::Custom component name not provided ');
            }
        });

    },
    /*
    * @description : Use this method to check if required fields are valid or not
                   : use only for fields which doesnt have standard checkValidity for ex. lightning:inputField
                   : it will assign them a class that is passed as parameter if its not valid
    * @params : component : instance of component
    *         : errorClass : css class to assign if field is not valid
    *         : listOfFields : list of aura ids of fields to validate
    *
    * @return : returns true if even a single field is not valid
    */
    checkRequired : function(component,errorClass,listOfFields){
        let validSoFar = true;
        var _this = this;

        for(var x=0;x<listOfFields.length;x++){
            if(!_this.checkRequiredFieldById(component,errorClass,listOfFields[x]))
                validSoFar = false;
        }
        return validSoFar;
    },


    //check required on individual fields
    checkRequiredFieldById : function(component,errorClass,auraId){
        let _this = this;
        let field = component.find(auraId);
        if(field){
            if(field.constructor === Array){
                let validSoFar = true;
                for(var x=0;x<field.length;x++){
                    if(!_this.checkRequiredIndividualField(errorClass,field[x]))
                        validSoFar = false;
                }
                return validSoFar;
            }else{
                return _this.checkRequiredIndividualField(errorClass,field);
            }
        }else{
            return true;
        }
    },

    //check required on individual fields
      checkRequiredIndividualField : function(errorClass,field){
        let value = field.get("v.value");
        if(!value || value.trim().length == 0){
            $A.util.addClass(field, errorClass);
            return false;
        }else{
            $A.util.removeClass(field, errorClass);
            return true;
        }
      },
    goToDetailPage : function(recordId) {
        var sObectEvent = $A.get("e.force:navigateToSObject").setParams({
            "recordId": recordId,
            "slideDevName": "detail"
        }).fire();
        
    },
    navigateToURL : function(url) {
        //Find the text value of the component with aura:id set to "address"
        var address = '/'+url;
        let fullURL = window.location.href.split('?');
        if(!$A.util.isEmpty(fullURL) && fullURL.length > 1){
            address += "?"+fullURL[1];
        }
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": address,
            "isredirect" :false
        });
        urlEvent.fire();
        
    },
    getLanguage : function(){
        var selectedLanguage ='en_US';
        /*let fullURL = window.location.href.split('?');
                if(!$A.util.isEmpty(fullURL) && fullURL.length > 1){
                    fullURL.forEach(function(element, index){
                        var languageOption = fullURL[index].split('=');
                        if(typeof (languageOption[0]) != 'undefined' &&  languageOption[0] == 'language'){
                             selectedLanguage = typeof (languageOption[1]) === 'undefined' ? 'en_US' : languageOption[1];
                        }
                     })
                } */
        selectedLanguage = this.getLanguageNew('language');
                return selectedLanguage;
    },
    getLanguageNew : function qs(key) {
        var vars = [], hash;
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for(var i = 0; i < hashes.length; i++)
        {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        return vars[key];
    }
})
({
	doInit : function(cmp, event, helper) {
		var cUtil = cmp.find('commonUtil');
		// For Lightning community strip out /s prefix for logout
		var logoutURL = cUtil.trimSuffix(cmp.get("v.site.prefix"), "/s") + "/secur/logout.jsp";
		cmp.set("v.logoutURL", logoutURL);
	}
})
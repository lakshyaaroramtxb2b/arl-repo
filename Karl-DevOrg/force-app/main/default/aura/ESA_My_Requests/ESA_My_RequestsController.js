({	
    doInit : function(cmp, event, helper) {
		
	},
    
    updateSearchResults : function(cmp, event, helper) {
        helper.cleanUp(cmp);
        helper.fetchRequests(cmp);
	},
    
    showDialog : function(cmp, event, helper) {
        helper.cleanUp(cmp);
        helper.fetchRequests(cmp);
		helper.toggleDialogDisplay(cmp, true);
	},
    
    onHideDialog : function(cmp, event, helper) {
        helper.cleanUp(cmp);
        helper.reset(cmp);
	},
    
    showRequests : function(cmp, event, helper) {
        helper.clear(cmp);
        var selectedType = event.getSource().get("v.name");
        cmp.set("v.selectedType", selectedType);
        helper.fetchRequests(cmp);
    },
    
    getDetails : function(cmp, event, helper) {
        var index = event.getSource().get("v.name");     
        var secReq = helper.getRequest(cmp, index);
        
        helper.selectRequest(cmp, secReq);
    },
    
    viewRequest : function(cmp, event, helper) {
        event.preventDefault();
        event.stopPropagation();
        helper.clearMessage(false);
        var secReq = cmp.get("v.selectedRequest");
        //console.log(secReq);
        var refreshEvt = $A.get("e.c:esaRefreshEvt");   
        refreshEvt.setParams({
            "hashKey" : secReq.Hash_Key__c
        });
        refreshEvt.fire();
        helper.toggleDialogDisplay(cmp, false);
    }
})
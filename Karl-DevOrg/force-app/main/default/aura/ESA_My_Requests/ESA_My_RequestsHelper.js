({
	toggleDialogDisplay : function(cmp, isShow) {
        if (isShow) {
            cmp.find("dialog").getEvent("show").fire();
        } else {
            cmp.find("dialog").getEvent("hide").fire();
        }
	},
    
    toggleBusy : function(cmp, isShow, id) {
        var dialogId = id || "busy";
        if (isShow) {
            cmp.find(dialogId).getEvent("show").fire();
        } else {
            cmp.find(dialogId).getEvent("hide").fire();
        }
	},
    
    cleanUp : function (cmp) {
        this.clear(cmp);
        cmp.set("v.myRequests", []);
        cmp.set("v.sharedRequests", []);
    },
    
    clear : function (cmp) {
        cmp.set("v.selectedRequest", null);
        cmp.set("v.recordDetails", null);
        this.clearMessage(false, "myReqs");
    },
    
    reset : function (cmp) {
        cmp.set("v.selectedType", "createdByMe"); 
        cmp.find("search").clear();
    },
    
    showMessage: function (message, variant, isSticky) {
        $A.get("e.c:esaShowMessage").setParams({
            "id" : "myReqs",
            "text": message, 
            "variant": (variant || "success"), 
            "isSticky" : isSticky || false
        }).fire();
    },
    
    clearMessage: function (doNotClearSticky, id) {
        $A.get("e.c:esaClearMessage").setParams({
            "id" : id,
            "doNotClearSticky" : doNotClearSticky
        }).fire();
    },
    
    selectFirst : function (cmp) {
        var req = this.getRequest(cmp, 0);        
        if (req) {
           this.selectRequest(cmp, req);
        } else {
           this.showMessage($A.get("$Label.c.ESA_EmptyMessage"), "info");
        }
    }, 
    
    selectRequest : function (cmp, secReq) {
        if (secReq && secReq.Id != cmp.get("v.selectedRequest.Id")) {
            cmp.set("v.selectedRequest", secReq);
            this.fetchDetails(cmp);
        }
    },
    
    getRequest : function(cmp, index) {
        var type = cmp.get("v.selectedType");   
        var requests = (type == "createdByMe" 
                      ? cmp.get("v.myRequests")
                      : cmp.get("v.sharedRequests"));
        
        if (!$A.util.isEmpty(requests) && requests.length > index) {
            return requests[index];
        } 
        return null;
    },
    
    fetchRequests : function(cmp) {
        var type = cmp.get("v.selectedType");
        //console.log(type);
        var action;
        if (type == "createdByMe" && $A.util.isEmpty(cmp.get("v.myRequests"))) {
            action = cmp.get("c.getMyRequests");
        } else if (type == "sharedWithMe" && $A.util.isEmpty(cmp.get("v.sharedRequests"))) {
            action = cmp.get("c.getSharedRequests");
        } 
 
        if ($A.util.isUndefinedOrNull(action)) {
            this.selectFirst(cmp);
            //console.log("Return");
            return;
        } 
        
        this.toggleBusy(cmp, true);
        action.setParams({"searchText" : cmp.find("search").getValue()});
        action.setCallback(this, function(response) {
            this.toggleBusy(cmp, false);
            var THIS = this;
            $A.get("e.c:esaHandleResponse").setParams({
                "response": response,
                "messageId" : "myReqs",
                "onSuccess" : $A.getCallback(function(returnVal) {
                    //console.log(returnVal);
                    if (type == "createdByMe") {
                        cmp.set("v.myRequests", returnVal);
                    } else if (type == "sharedWithMe") {
                        cmp.set("v.sharedRequests", returnVal);
                    }
                    THIS.selectFirst(cmp);
                })
            }).fire();
        });
        $A.enqueueAction(action);
    },
    
    fetchDetails : function (cmp) {
        var THIS = this;
        cmp.set("v.recordDetails", null);
        var req = cmp.get("v.selectedRequest");
        // console.log(JSON.stringify(req));
        
        var action = cmp.get("c.getRequestDescription");
        action.setParams({"securityRequest": req});
        action.setStorable();
        this.toggleBusy(cmp, true, "rhsBusy");
        action.setCallback(this, function(response) {
            //console.log(response.getState());
            this.toggleBusy(cmp, false, "rhsBusy");
            $A.get("e.c:esaHandleResponse").setParams({
                "response": response,
                "messageId" : "myReqs",
                "onSuccess" : $A.getCallback(function(returnVal) {
                    //console.log(returnVal);
                    THIS.setRecordDetails(cmp, returnVal);
                })
            }).fire();
        });
        $A.enqueueAction(action);
    },
    
    setRecordDetails : function (cmp, text) {
        var searchText = cmp.find("search").getValue();
        if (!$A.util.isEmpty(searchText)) {
            // Highlight
            text = text.replace(new RegExp(searchText, "gi"),  "<span class='highlight'>" + searchText + "</span>");
        }
        cmp.set("v.recordDetails", text);
    }
})
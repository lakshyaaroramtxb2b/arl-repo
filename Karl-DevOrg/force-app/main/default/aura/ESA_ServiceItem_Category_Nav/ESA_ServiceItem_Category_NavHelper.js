({
	setServiceItemCategories : function(cmp, serviceItemCategories) {
        var catToSIsArrayMap = [];
        for (var category in serviceItemCategories) {
            catToSIsArrayMap.push({
                "category" : category,
                "serviceItems" : serviceItemCategories[category]
            });
        } 
        cmp.set("v.catToSIsArrayMap", catToSIsArrayMap);
	},
    
    setSelectedServiceItem : function(cmp, isInitialLoad) {
        var secReq = cmp.get("v.securityRequest");
        var isNewReq = $A.util.isEmpty(secReq.Id);
        var hasSelectedServiceItem = !$A.util.isEmpty(secReq.ESA_Service_Item__c);
        var serviceItem = this.getServiceItem(cmp, secReq.ESA_Service_Item__c);
        var isCategoriesHidden = (cmp.get("v.config.settings.Service_Item_Categories_Display__c") == "Hidden");
        var useTemplate = isNewReq && !$A.util.isUndefinedOrNull(cmp.get("v.requestTemplate"));
        
        if (isCategoriesHidden && !hasSelectedServiceItem) {
            this.showMessage($A.get("$Label.c.ESA_Invalid_URL"), "error", false);
            return;
        } else if (isNewReq && !serviceItem) { // serviceItem will be null when coming with invalid SI in URL
            var firstServiceItem = cmp.get("v.catToSIsArrayMap[0].serviceItems[0].Id");
            if (!$A.util.isEmpty(firstServiceItem)) {
                $A.get("e.c:esaReset").setParams({
                    "serviceItemId" : firstServiceItem,
                    "caseWorkNo" : secReq.Supportforce_Case_Number__c,
                    "template" : useTemplate ? cmp.get("v.requestTemplate.Template_Name__c") : null
                }).fire();
            }
            return;
        }  
        
        if (isInitialLoad && isNewReq && hasSelectedServiceItem) {
            // Collapse all other service items if coming here with preselected service item in URL
            this.handleCategoriesDisplay(cmp, true, serviceItem);
        } else {
            // Expands category with selected service item, if collapsed by default
            this.handleCategoriesDisplay(cmp, false, serviceItem);
        }

        var requestTemplate;
        if (useTemplate) {
            requestTemplate = cmp.get("v.requestTemplate");
            if ($A.util.isEmpty(cmp.get("v.requestTemplate.Id"))) {
                // Template Creation Flow
                secReq.Template_Parent_Entity__c = cmp.get("v.config.settings.Id");
            } else {
                // Clone from tempate flow
                // Populate common fields on sec request
                secReq.Subject__c = cmp.get("v.requestTemplate.Subject__c");
                secReq.Description__c = cmp.get("v.requestTemplate.Description__c");
            }
        }
        $A.get("e.c:ESA_ServiceItem_Selected").setParams({
            "securityRequest" : secReq,
            "requestTemplate" : requestTemplate, 
            "config" : cmp.get("v.config"),
            "serviceItem" : serviceItem
        }).fire();
    },
    
    handleCategoriesDisplay : function (cmp, isCollapseAll, serviceItem) {
        var categoryCmps = cmp.find("category");
        for (var i=0;i<categoryCmps.length;i++) {
            var categoryCmp = categoryCmps[i];
            if (isCollapseAll) {
                categoryCmp.set("v.isExpanded", false);
            } 
            // Expand selected Service Item's category, service item is null when loading inactie SI related requests
            if (serviceItem && categoryCmp.get("v.id") == serviceItem.Category__c) {
                categoryCmp.set("v.isExpanded", true);
            }
        }
    },
    
    getServiceItem : function(cmp, serviceItemId) {
        var catToSIsArrayMap = cmp.get("v.catToSIsArrayMap");
        if ($A.util.isEmpty(serviceItemId) || $A.util.isEmpty(catToSIsArrayMap)) {
            return null;
        }
        for (var i=0;i<catToSIsArrayMap.length;i++) {
            var svcItems = catToSIsArrayMap[i].serviceItems;
            if (svcItems) {
                for (var j=0;j<svcItems.length;j++) {
                    var svcItem = svcItems[j];
                    if (svcItem.Id == serviceItemId) {
                        return svcItem;
                    }
                }
            }
        }
        return null;
    }, 
    
    fetchServiceItemCategories : function(cmp) {
        var THIS = this;
        var secReq = cmp.get("v.securityRequest");
        if (!secReq || $A.util.isEmpty(secReq.Entity_Code__c)) {
            return;
        } 
        cmp.find('busy').get('e.show').fire();
        var action = cmp.get("c.getServiceItemCategories");
        action.setStorable();
        action.setParams({"entityCode" : secReq.Entity_Code__c});
        action.setCallback(this, function(response) {
            cmp.find('busy').get('e.hide').fire();
            $A.get("e.c:esaHandleResponse").setParams({
                "response": response,
                "onSuccess": $A.getCallback(function(returnVal) {
                    // Identify whether this is initial loading 
                    // If yes we want to collapse navigation when si is present in url
                    var isInitialLoad = $A.util.isEmpty(cmp.get("v.entityCode"));
                    if (secReq.Entity_Code__c != cmp.get("v.entityCode")) {
                        cmp.set("v.entityCode", secReq.Entity_Code__c);
                        THIS.setServiceItemCategories(cmp, returnVal); 
                    } 
                    THIS.handleSecurityRequest(cmp, isInitialLoad); 
                })
            }).fire();
        });
        $A.enqueueAction(action);
    },
      
    handleSecurityRequest : function (cmp, isInitialLoad) {
        this.setSelectedServiceItem(cmp, isInitialLoad); 
        var timezone = cmp.get("v.securityRequest.Time_Zone__c");
        if ($A.util.isEmpty(timezone)) {
            timezone = cmp.get("v.config.timezone");
        } 
        $A.get("e.c:esaTimezoneChangeEvt").setParams({"timezone" : timezone}).fire();
    }, 
    
    cleanUp : function (cmp) {
        
    },
    
    showMessage: function (message, variant, isSticky, id) {
        $A.get("e.c:esaShowMessage").setParams({
            "text": message, 
            "variant": (variant || "success"), 
            "isSticky" : isSticky || false,
            "id" : id
        }).fire();
    },
    
    clearMessages: function (doNotClearSticky) {
        $A.get("e.c:esaClearMessage").setParams({
            "doNotClearSticky" : doNotClearSticky
        }).fire();
    }
})
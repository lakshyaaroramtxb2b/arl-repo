({
	doInit : function(cmp, event, helper) {
		helper.fetchServiceItemCategories(cmp);
	},
    
    handleSecReq: function(cmp, event, helper) {
        helper.cleanUp(cmp);
        helper.fetchServiceItemCategories(cmp);        
    },
    
    selectServiceItem : function(cmp, event, helper) {
        helper.clearMessages(false);
        var serviceItemId = event.target.getAttribute('data-service-item-id');
        if (!$A.util.isEmpty(cmp.get("v.securityRequest.Id"))) {
            $A.get("e.c:commonConfirmEvt").setParams({
                "bodyText" : $A.get("$Label.c.ESA_ConfirmExit"),
                "onOk": $A.getCallback(function() {
                    $A.get("e.c:esaReset").setParams({
                        "serviceItemId" : serviceItemId
                    }).fire();
                })
            }).fire();        
        } else {
            $A.get("e.c:esaReset").setParams({
                "serviceItemId" : serviceItemId,
                // Pass Template when Create New Template flow, Not when populating from template
                "template" : $A.util.isEmpty(cmp.get("v.requestTemplate.Id")) ? cmp.get("v.requestTemplate.Template_Name__c") : null
            }).fire();
        }  
    }
})
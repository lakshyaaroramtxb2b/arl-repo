({
	setColumnNames : function(cmp) {
		cmp.set('v.columnNames', 
                [ 
                  { label: 'Copy API Name', fieldName: 'fieldApiName', type: 'button-icon', 
                    typeAttributes: {'iconName': 'utility:copy_to_clipboard', 
                                     'iconClass': '',
                                     'title': 'Copy to clipboard',
                                     'variant': 'bare'
                                    }
                  },
                  { label: 'Field Name', fieldName: 'fieldLabel', type: 'text'},
                  { label: 'Type (Length)', fieldName: 'fieldType', type: 'text'}
                ]);
	},
    
    copyToClipBoard : function(text) {
       // Create temp textarea element and remove after copy
       var htmlElem = document.createElement('textarea');
       htmlElem.setAttribute('readonly', '');
       htmlElem.value = text;
       htmlElem.style = {left: '-9999px',position: 'absolute'};
       document.body.appendChild(htmlElem);
       htmlElem.select();
       document.execCommand('copy');
       document.body.removeChild(htmlElem);
    }
})
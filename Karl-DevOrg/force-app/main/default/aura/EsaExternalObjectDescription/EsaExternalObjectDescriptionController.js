({
	doInit : function(cmp, event, helper) {
        helper.setColumnNames(cmp);
        
		var action = cmp.get('c.getObjectFields');
        action.setParams({'serviceItemquestionId':cmp.get("v.recordId")});
        action.setCallback(this, function(response) {
            cmp.set('v.valuesMap',response.getReturnValue());
        	
        });
        $A.enqueueAction(action); 
	},
    
    copyToClipBoard : function(cmp, event, helper) {
        var row = event.getParam('row');
        if (row.isUpdateable) {
            helper.copyToClipBoard(row.fieldApiName);
            $A.get("e.c:esaShowAlertEvt").setParams({
                "type":"success",
                "title": "",
                "text": $A.get("$Label.c.Esa_API_Name_copied_to_clipboard").replace('{0}',row.fieldApiName)
            }).fire();
        } else {
            $A.get("e.c:esaShowAlertEvt").setParams({
                "type":"error",
                "title": "Warning",
                "text": $A.get("$Label.c.Esa_Update_Permission_Denied").replace('{0}',row.fieldApiName)
            }).fire();
        }
    }
})
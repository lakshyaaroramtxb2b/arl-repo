({
    initAngular : function (cmp) {

        if (this.isAnglrInitialized(cmp)) {
            // Already initialized
            return true;
        }

        var cUtil = cmp.find('commonUtil');
        // Angular logic to handle URL changes without page refresh
        var app = angular.module('anglr',[]).config(function($locationProvider) {
            // use the HTML5 History API
            $locationProvider.html5Mode({
                enabled: true,
                requireBase: false
            });
        });

        app.controller('anglrCtrl', ['$scope', '$location', '$http', $A.getCallback(function($scope, $location, $http) {   
            
            $scope.clearURLParams = function() {
                $location.search({});
                $scope.$apply(); 
            };
            $scope.updateURLParams = function(urlParams) {
                cUtil.forEach(urlParams, function(name, value) {
                    $location.search(name, value).replace(); 
                });
                $scope.$apply();
            };
            $scope.callAjax = function(url, callback) {
                try {
                    $http.get(url).then(function(response) { // Success
                        callback(response);
                    }, function(response) { // Error
                        callback(null);
                    });
                } catch(e) {
                    callback(null);
                }
                
            };
            
            $scope.init = $A.getCallback(function() {
                cmp.set("v.anglrScope", $scope);
            });
            
        })]); 

        var domElem = cmp.find('anglr').getElement();
        angular.bootstrap(domElem, ['anglr']);
        
        return false;
    },

    isAnglrInitialized : function(cmp) {
        try {
            return !$A.util.isUndefinedOrNull(angular.module('anglr'));
        } catch (e) {
            return false;
        }
    },

    isReady : function(cmp) {
        return !$A.util.isUndefinedOrNull(cmp.get("v.anglrScope"));
    }
})
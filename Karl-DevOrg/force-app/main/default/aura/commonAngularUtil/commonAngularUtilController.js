({
    doInit : function(cmp, event, helper) {
        helper.initAngular(cmp);
    }, 

    clearURLParams : function(cmp, event, helper) {
        var cUtil = cmp.find('commonUtil');
        cUtil.waitFor(
            $A.getCallback(function() { return helper.isReady(cmp); }),
            function () { cmp.get("v.anglrScope").clearURLParams(); },  
        500);
    },

    updateURLParams : function(cmp, event, helper) {
        var cUtil = cmp.find('commonUtil');
        var urlParams = event.getParam('arguments').urlParams;
        cUtil.waitFor(
            $A.getCallback(function() { return helper.isReady(cmp); }),
            function () { cmp.get("v.anglrScope").updateURLParams(urlParams); },
        1000);
     },

     callAjax : function(cmp, event, helper) {
        var cUtil = cmp.find('commonUtil');
        var url = event.getParam('arguments').url;
        var callback = event.getParam('arguments').callback;
        cUtil.waitFor(
            $A.getCallback(function() { return helper.isReady(cmp); }),
            function () { cmp.get("v.anglrScope").callAjax(url, callback); },
        1000);
     }
})
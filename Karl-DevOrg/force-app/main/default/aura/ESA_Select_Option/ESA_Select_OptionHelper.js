({
    setSelection : function(cmp) {
        var value = cmp.get("v.value");
        var selectedValue = cmp.get("v.selectedValue");
        var isSelected = false;
        if (!$A.util.isEmpty(selectedValue)) {
            var selectedValues = selectedValue.split(","); // For Multi-picklists
            var isSelected = (selectedValue == value || selectedValues.indexOf(value) != -1);
        }
        cmp.set("v.isSelected", isSelected);
    }
})
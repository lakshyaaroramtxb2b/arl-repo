({
    doInit : function(cmp, event, helper) {
       helper.setSelection(cmp);
    },

    resetSelection : function(cmp, event, helper){
       helper.setSelection(cmp); 
    }
});
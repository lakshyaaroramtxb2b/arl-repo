({
	fetchArticles : function(cmp, searchText) {        
        var fetchArticlesJS = cmp.get("v.onFetchArticlesJS");               
        if (typeof fetchArticlesJS == "function") { 
           var THIS = this; 
           THIS.toggleBusy(cmp, true);
           var entityCode = cmp.get("v.securityRequest.Entity_Code__c");
           // Min 2 chars required for search
           searchText = ($A.util.isEmpty(searchText) || searchText.length < 2) ? "" : searchText;
           fetchArticlesJS(entityCode, searchText, $A.getCallback(function(returnVal) {
                 //console.log("Articles List");
                 //console.log(returnVal);
                 THIS.toggleBusy(cmp, false);
                 cmp.set("v.articles", returnVal); 
            }));
        } 
	},
    
    cleanUp : function(cmp) {
        cmp.set("v.articles", null);
    },
    
    toggleBusy: function (cmp, isBusy) {
        if (isBusy) {
           cmp.find('busy').get('e.show').fire(); 
        } else {
           cmp.find('busy').get('e.hide').fire();
        }
    },
    
    showArticleDialog: function (cmp, articleId) {
        var articles = cmp.get("v.articles");
        for (var i=0;i<articles.length;i++) {
            var article = articles[i];
            if (articleId == article.id) {
                this.toggleBusy(cmp, true);
                var THIS = this;
                var action = cmp.get("c.getArticleDetails");
                action.setParams({"urlName" : article.urlName,
                                  "entityCode" : cmp.get("v.securityRequest.Entity_Code__c")});
                action.setCallback(this, function(response) {
                    THIS.toggleBusy(cmp, false);
                    $A.get("e.c:esaHandleResponse").setParams({
                        "response": response,
                        "onSuccess": $A.getCallback(function(returnVal) {
                            cmp.set("v.selectedArticle", returnVal);
                            cmp.find("articleDialog").getEvent("show").fire();
                        })
                    }).fire();            
                });
                $A.enqueueAction(action);
            }
        }
        
    }
})
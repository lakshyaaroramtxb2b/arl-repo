({
	doInit : function(cmp, event, helper) {
	    helper.fetchArticles(cmp);
	},
    
    updateArticles : function(cmp, event, helper) {
        helper.cleanUp(cmp);
	    helper.fetchArticles(cmp);
	},
    
    showArticle : function (cmp, event, helper) {
        var articleId = event.target.getAttribute('data-article-id');
        helper.showArticleDialog(cmp, articleId);  
    },
    
    updateSearch : function(cmp, event, helper) {
        var text = event.getParam("data")["searchText"];
        helper.fetchArticles(cmp, text);
	},
    
    clearSearch : function(cmp, event, helper) {
        helper.clearSearch(cmp);      
    }
})
({
	doInitCaseComment : function(component, event, helper) {
        var action = component.get("c.getAllPublishedTemplateTypes");
        action.setCallback(this, function(result){
            var templateTypesLabel = result.getReturnValue();
            console.log("==== All Template Types ==== "+templateTypesLabel);
            component.set("v.templateTypes", templateTypesLabel);
        });
        $A.enqueueAction(action);
        component.set("v.selectedType", "Blank Template");
	},
    
    changeCommentBody : function(component, event, helper) {
        var templateType = component.get("v.selectedType");
        console.log("==== Selected Template Type ==== "+templateType);
        console.log("======= Case Ids list ======= "+component.get("v.caseIdsList"));
        var caseIdList = component.get("v.caseIdsList");
        // var caseRecordId = component.get("v.recordId"); 
        var bulkMode = component.get("v.bulkMode");
        var action;

        if(bulkMode) {
            action = component.get("c.getTemplateBody");
            action.setParams({
                "templateLabel" : templateType
            });
        }
        else {
            action = component.get("c.getCaseCommentBody");
            action.setParams({
                "caseId" : component.get("v.recordId"),
                "templateLabel" : templateType
            });
        }
        
        action.setCallback(this, function(response) {
            console.log('State: '+response.getState());
            if(response.getState()=="SUCCESS") {
             	var caseCommentBody = response.getReturnValue();
                console.log("=== Comment Body === "+caseCommentBody);
                if(caseCommentBody != null) {
                    var splittedBody = caseCommentBody.split("\n");
                    component.set("v.maxRows", (splittedBody+1)*50);
                    console.log(splittedBody);
                    component.set("v.commentBody", caseCommentBody);
                    component.set("v.showCaseComment", true);
                    console.log("== Show Comment Body === "+component.get("v.showCaseComment"));
                    if(caseCommentBody != '')
                        component.set("v.isButtonActive", false);
                    else
                        component.set("v.isButtonActive", true);
                }
                else {
                    component.set("v.isButtonActive", true);
                    component.set("v.showCaseComment", false);
                }   
            }
        })
   		$A.enqueueAction(action);
    },

    checkCommentType : function(component, event, helper) {
        var publicOrPrivate = component.get("v.commentType");
    },

    insertCaseComments : function(component) {
        var caseId = component.get("v.recordId");
        var caseCommentBody = component.get("v.commentBody");
        var type = component.get("v.commentType");
        var idArray = [];
        idArray.push(component.get("v.recordId"));
        if(!component.get("v.bulkMode")) {
            component.set("v.caseIdsList", idArray);
        }
        var caseIdList = component.get("v.caseIdsList");
        console.log("Bulk Mode: "+component.get("v.bulkMode"));
        if(caseCommentBody) {
            var action = component.get("c.createCaseCommentRecord");
            action.setParams({
                "caseIdList" : caseIdList,
                "templateBody" : caseCommentBody,
                "commentType" : type,
                "bulkMode" : component.get("v.bulkMode")
            });
            
            action.setCallback(this, function(response) {     			
                if(response.getState() == "SUCCESS") {
                    var notUpdatedCases = response.getReturnValue();
                    if(notUpdatedCases.length > 0) {
                        component.set("v.notUpdatedCaseList", notUpdatedCases);
                    }
                    else {
                        var successEvent = $A.get("e.force:showToast");
                        successEvent.setParams({
                            "title": "Success!",
                            "message": "Case comment record created successfully.",
                            "type": "success"
                        });
                        successEvent.fire();
                        window.location.reload();
                    }
                    //component.set("v.showCaseComment", false);
                    //component.set("v.selectedType", "None");
                }
            });
            
            $A.enqueueAction(action);
        }
        else {
            var errorEvent = $A.get("e.force:showToast");
            errorEvent.setParams({
                "title": "Error!",
                "message": "Please enter correct template body to save.",
                "type": "error"
            });
            errorEvent.fire(); 
        }
    },
    
    handleClick : function(component, event, helper) {
        if(component.get("v.bulkMode")) {
            component.set("v.showConfirmationWindow", true);
        }
        else {
            var action = component.get("c.insertCaseComments");
            $A.enqueueAction(action);
        }
    },

    activeButton : function(component, event, helper) {
        var templateBody = component.find("comment").get("v.value");
        console.log("Template: "+templateBody);
        if(templateBody != null && templateBody != '') {
            component.set("v.isButtonActive", false);
        }
        else {
            component.set("v.isButtonActive", true);
        }
    }
})
({
	redirect : function(component, event, helper) {
		var action = component.get("c.isCommunity");
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if(state==="SUCCESS"){  
                var navService = component.find("navService");
                var pageReference;
                //For Community
                if( response.getReturnValue() ){                    
                    pageReference = {
                        "type": "standard__namedPage",
                        "attributes": {
                            "pageName": "arl-audit-cycle-report"
                        },
                        "state": {
                            "c__recordid": component.get("v.recordId")
                        }
                    }
                }
                //For Internal
                else{
                    pageReference = {
                        "type": "standard__component",
                        "attributes": {
                            "componentName": "c__ARL_AuditCycleReport"
                        },
                        "state": {
                            "c__recordid": component.get("v.recordId")
                        }
                    }
                }
                
                navService.navigate(pageReference);
                $A.get("e.force:closeQuickAction").fire() 
            }
            else if(state === "ERROR"){
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        let toastParams = {
                            title: "Error",
                            message: errors[0].message, // Default error message
                            type: "error"
                        };
                        // Fire error toast
                        let toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams(toastParams);
                        toastEvent.fire();
                    }
                }
                else {
                    console.log("Unknown error");
                }
                $A.get("e.force:closeQuickAction").fire() 
            }
        });
        $A.enqueueAction(action); 
	}
})
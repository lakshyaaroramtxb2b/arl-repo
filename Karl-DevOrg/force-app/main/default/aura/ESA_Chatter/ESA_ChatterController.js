({
	doInit : function(cmp, event, helper) {
        helper.handleFeed(cmp);
    },
    
    resetFeed : function(cmp, event, helper) {
        helper.handleFeed(cmp);
    }
})
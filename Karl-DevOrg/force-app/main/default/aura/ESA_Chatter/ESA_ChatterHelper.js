({
	handleFeed : function(cmp) {
        var isVisible = this.isFeedVisible(cmp);
        cmp.set("v.isFeedVisible", isVisible);
        if (isVisible) {
            if (this.isOldCommunity()) {
                this.createFullFeed(cmp);
            } else {
                this.createPublisherAndFeed(cmp);
            }
        }  
	},
    
    isFeedVisible : function(cmp) {
        var isChatterEnabled = $A.util.getBooleanValue(cmp.get("v.config.settings.Enable_Chatter__c"));
        var isCollaboratorsEnabled = $A.util.getBooleanValue(cmp.get("v.config.settings.Enable_Collaborators__c"));
        var chatterJunctionObj = cmp.get("v.securityRequest.Chatter_Junction__c");
        return isChatterEnabled && isCollaboratorsEnabled && !$A.util.isEmpty(chatterJunctionObj);
    },
    
    isOldCommunity : function() {
        return $A.util.isEmpty($A.get("$Site"));
    },
    
    createPublisherAndFeed : function(cmp) {
        var recordId = cmp.get("v.securityRequest.Chatter_Junction__c");
        
        $A.createComponent("forceChatter:publisher", {
            "context": "RECORD",
            "recordId": recordId
        }, function (publisherCmp) {
            $A.createComponent("forceChatter:feed", {
                "feedDesign": "DEFAULT",
                "type": "Record",
                "subjectId": recordId
            }, function (feedCmp) {
                cmp.set("v.feedCmps", [publisherCmp, feedCmp]);
            });
        });
    },
    
    createFullFeed : function(cmp) {
        var recordId = cmp.get("v.securityRequest.Chatter_Junction__c");
        $A.createComponent("forceChatter:fullFeed", {
            "type": "Record",
            "subjectId": recordId,
            "": true
        }, function (feedCmp) {
            cmp.set("v.feedCmps", [feedCmp]);
        });
    }    
})
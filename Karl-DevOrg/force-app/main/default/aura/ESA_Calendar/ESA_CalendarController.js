({
	doInit : function(cmp, event, helper) {
        //console.log(JSON.stringify(cmp.get("v.securityRequest")));
        helper.setWeekDays(cmp);
        helper.setMonths(cmp);
        helper.setTimezones(cmp);
        helper.initCalendar(cmp);
	},
    
    handleServiceItemSelection : function(cmp, event, helper) {
        cmp.set("v.serviceItem", event.getParam("serviceItem"));
        cmp.set("v.config", event.getParam("config"));
        cmp.set("v.securityRequest", event.getParam("securityRequest"));           
        helper.initCalendar(cmp);
    },
    
    showTimeslots : function(cmp, event, helper) {
        helper.cleanUp(cmp);
        var key = event.target.getAttribute("data-key"); 
        //console.log(key);
        cmp.set("v.clickedDateKey", key);
	},
    
    resetAppt : function(cmp, event, helper) {
        helper.clearSelection(cmp);
        helper.stopEvent(event);
    },
    
    changeTimezone : function(cmp, event, helper) {
        // Event is fired from either button or buttonMenu hence event.getParam and event.getSource().get...
        var selectedTzIndex = event.getParam("value") || event.getSource().get("v.value");
        //console.log(selectedOption);
        helper.setSelectedTimezone(cmp, selectedTzIndex);
        // Update the timeslot display
        helper.updateAvailableDates(cmp);
         
    },
    
    cancelAppt : function(cmp, event, helper) {
        $A.get("e.c:commonConfirmEvt").setParams({
                "bodyText" : $A.get("$Label.c.ESA_ConfirmApptCancel"),
                "onOk": $A.getCallback(function(returnVal) {
                    helper.cancelAppt(cmp);
                })
         }).fire(); 
        helper.stopEvent(event);
    },
    
    setApptDateTime : function(cmp, event, helper) {
        var apptDateTime = event.target.getAttribute("data-appt-date-time");
        var uglyApptDateTime = event.target.getAttribute("data-ugly-date-time");
        
        if (helper.isReschedule(cmp)) {
            $A.get("e.c:commonConfirmEvt").setParams({
                "bodyText" : $A.get("$Label.c.ESA_ConfirmReschedule"),
                "onOk": $A.getCallback(function(returnVal) {
                    helper.setApptDateTime(cmp, apptDateTime, uglyApptDateTime);
                })
         }).fire(); 
        } else {
            helper.setApptDateTime(cmp, apptDateTime, uglyApptDateTime);
        }
        
    },
    
    fetchApptDates : function(cmp, event, helper) { 
        // Only fetch the first time, subsequently user can hit refresh button
        if (helper.isCalVisible(cmp) && !helper.isReadOnly(cmp) && $A.util.isEmpty(cmp.get("v.apptDates"))) {
		   helper.updateAvailableDates(cmp);
        }
	},
    
    refetchApptDates : function(cmp, event, helper) { 
        helper.updateAvailableDates(cmp);
	},
    
    handleNextMonth : function(cmp, event, helper) {
		helper.displayNextMonth(cmp);
	},
    
    handlePrevMonth : function(cmp, event, helper) {
		helper.displayPrevMonth(cmp);
	}    
})
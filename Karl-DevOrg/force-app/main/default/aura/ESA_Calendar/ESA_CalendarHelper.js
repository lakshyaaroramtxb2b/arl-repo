({
	initCalendar : function(cmp) {
        this.clearSelection(cmp);
        this.cleanUp(cmp, true);
        this.setIsReadOnly(cmp);
        this.setIsApptNeeded(cmp);
        if (this.isCalVisible(cmp) && !this.isReadOnly(cmp)) {
            var initialDate = new Date();
            cmp.set("v.calendarConfig", {
                "month" : initialDate.getMonth(),
                "year" : initialDate.getFullYear(),
                "monthName" : this.getMonthName(cmp, initialDate.getMonth())
            }); 
            
            // Expand calendar when no appt date is selected yet
            var showCal = this.isApptNeeded(cmp) && $A.util.isEmpty(cmp.get("v.securityRequest.Office_Hours_Reservation__c"));
            cmp.find("calContainer").set("v.isExpanded", showCal);
            if (showCal) {
              this.updateAvailableDates(cmp);
            }
        }
	},
    
    setIsApptNeeded : function(cmp) {
        var config = cmp.get("v.config");
        var serviceItem = cmp.get("v.serviceItem");
        var isApptNeeded = false;
        var isCalVisible = false;
        if (!(this.isTemplate(cmp) || $A.util.isEmpty(config.settings.Google_Calendar_Name__c)) && !$A.util.isUndefinedOrNull(serviceItem)) {        
            //console.log(JSON.stringify(config));
            //console.log(JSON.stringify(serviceItem));        
            isCalVisible = (serviceItem.Office_Hours_Appointment__c != 'Not Required'
                             && !$A.util.getBooleanValue(serviceItem.ItemCategory__r.Appointment_Not_Needed__c));
            if (isCalVisible) {
                isApptNeeded = serviceItem.Office_Hours_Appointment__c == 'Required';
            }
        }
        cmp.set("v.isVisible", isCalVisible);
        cmp.set("v.isAppointmentNeeded", isApptNeeded);
    },
    
    setIsReadOnly : function (cmp) {
        var isReadOnly = $A.util.getBooleanValue(cmp.get("v.config.isCollaborator")) || (cmp.get("v.securityRequest.Status__c") == "Closed");
        cmp.set("v.isReadOnly", isReadOnly);
    },
    
    isReadOnly : function (cmp) {
        return $A.util.getBooleanValue(cmp.get("v.isReadOnly"));
    },
    
    isCalVisible : function(cmp) {
        return $A.util.getBooleanValue(cmp.get("v.isVisible"));
    },

    isApptNeeded : function(cmp) {
        return $A.util.getBooleanValue(cmp.get("v.isAppointmentNeeded"));
    },

    isTemplate : function(cmp) {
        return !$A.util.isEmpty(cmp.get("v.securityRequest.Template_Parent_Entity__c"));
    },
    
    updateAvailableDates : function(cmp) {
        this.cleanUp(cmp, true);
        this.toggleBusy(cmp, true);
        this.refreshTimezones(cmp);
        var action = cmp.get("c.getAppointmentDates");
        // console.log(JSON.stringify(cmp.get("v.securityRequest")));
        action.setParams({"securityRequest": cmp.get("v.securityRequest"),
                          "timezone" : cmp.get("v.selectedTimezone")});
        action.setCallback(this, function(response) {
            var THIS = this;
            $A.get("e.c:esaHandleResponse").setParams({
                "response": response,
                "onSuccess": $A.getCallback(function(returnVal) {
                    // console.log(returnVal);
                    cmp.set("v.apptDates", returnVal);
                    var hasAvailableDates =  THIS.setHasAvailableApptDates(cmp);
                    var hasAvailableSlots = THIS.setDaysOfMonthByWeek(cmp);
                    // Display first available dates/slots by default by looking upto 6 months
                    for (var i=0; i < 6 && hasAvailableDates && !hasAvailableSlots; i++) {
                        // Display next month
                        hasAvailableSlots = THIS.displayNextMonth(cmp);
                    }
                })
            }).fire();
            this.toggleBusy(cmp, false);
        });
        $A.enqueueAction(action);
    },

    getWeekdayNames : function() {
        return [$A.get("$Label.c.ESA_Sunday"),
                            $A.get("$Label.c.ESA_Monday"),
                            $A.get("$Label.c.ESA_Tuesday"),
                            $A.get("$Label.c.ESA_Wednesday"),
                            $A.get("$Label.c.ESA_Thursday"),
                            $A.get("$Label.c.ESA_Friday"),
                            $A.get("$Label.c.ESA_Saturday")];
    },
    
    setWeekDays : function(cmp) {
        var weekdayNames = this.getWeekdayNames();
        var weekdays = [];
        for(var i=0;i<weekdayNames.length;i++) {
            var weekday = weekdayNames[i];
            weekdays.push({
                "name": weekday,
                "abbrName": weekday.slice(0,3)
            });
        }
        cmp.set("v.weekdays", weekdays);
    },
    
    setTimezones: function (cmp) {
        var timezonesString = $A.get("$Label.c.ESA_TimeZones");
        //console.log(timezonesString.split(";"));
        if ($A.util.isEmpty(timezonesString)) {
            cmp.set("v.timezones", null);
        } else {
            cmp.set("v.timezones", timezonesString.split(";"));
            if ($A.util.isEmpty(cmp.get("v.selectedTimezone"))) {
                this.setSelectedTimezone(cmp, 0);
            }  
        }
    },

    getMonthNames : function() {
        return [$A.get("$Label.c.ESA_January"), 
                          $A.get("$Label.c.ESA_February"), 
                          $A.get("$Label.c.ESA_March"), 
                          $A.get("$Label.c.ESA_April"), 
                          $A.get("$Label.c.ESA_May"), 
                          $A.get("$Label.c.ESA_June"), 
                          $A.get("$Label.c.ESA_July"), 
                          $A.get("$Label.c.ESA_August"), 
                          $A.get("$Label.c.ESA_September"), 
                          $A.get("$Label.c.ESA_October"), 
                          $A.get("$Label.c.ESA_November"), 
                          $A.get("$Label.c.ESA_December")];
    },
    
    setMonths : function(cmp) {   
        var monthNames = this.getMonthNames();
        var months = [];
        for(var i=0;i<monthNames.length;i++) {
            var month = monthNames[i];
            months.push({
                "name": month,
                "abbrName": month.slice(0,3)
            });
        }
        cmp.set("v.months", months);
    },
    
    setHasAvailableApptDates : function(cmp) {
        var found = false;
        var apptDatesMap = cmp.get("v.apptDates");
        for (var dayKey in apptDatesMap) {
            var timeslots = apptDatesMap[dayKey];
            if (!$A.util.isEmpty(timeslots)) {
                found = true;
                break;  
            }
        }
        cmp.set("v.hasAvailableApptDates", found);
        return found;
    },
    
    setDaysOfMonthByWeek : function(cmp) {
        var cUtil = cmp.find("commonUtil");
        var monthNames = this.getMonthNames();
        var weekdayNames = this.getWeekdayNames();
        var hasAvailableSlots = false;
        var apptDatesMap = cmp.get("v.apptDates");
        var month = cmp.get("v.calendarConfig.month");
        var year = cmp.get("v.calendarConfig.year");
        var firstDayDt = new Date(year, month, 1);
        var lastDayDt = new Date(year, month+1, 0); // returns last day of month eg: 30 or 31 etc
        var today = new Date();
        
        var daysOfMonth = [];
        // Prefill empty weekdays before the start of first day of month
        for (var i=0;i<firstDayDt.getDay();i++) {
            daysOfMonth.push({
                "isAvailable" : false,
                "date" : "",                
                "weekday": i                
            });
        }
        
        for (var i=1;i<=lastDayDt.getDate();i++) {
            var day = new Date(year, month, i);
            var dayKey = this.getDateString(day);
            var timeslots = apptDatesMap[dayKey];
            if (!$A.util.isEmpty(timeslots)) {
                hasAvailableSlots = true;
            }
            // console.log(dayKey);
            // console.log(timeslots);
            var isAvailable = !$A.util.isUndefinedOrNull(timeslots); // Working date
            daysOfMonth.push({
                "key" : dayKey,
                "label" : cUtil.replaceParams("{weekDay}, {month} {date}", {
                    "weekDay" : weekdayNames[day.getDay()].slice(0,3),
                    "month" : monthNames[day.getMonth()].slice(0,3),
                    "date" : day.getDate()
                }),
                "isAvailable" : isAvailable,
                "isApptDay" : false, // Pass apptdate if you want to highlight in calendar
                "date" : day.getDate(),
                "weekday": day.getDay(),
                "timeslots" : timeslots
            });
        }
        
        // Postfill empty weekdays after the last day of month
        for (var i=lastDayDt.getDay()+1;i<6;i++) {
            daysOfMonth.push({
                "isAvailable" : false,
                "date" : "",
                "weekday": i                
            });
        }

        // Split into weekly chunks
        var daysOfMonthByWeek = [];
        for (var i=0;i<daysOfMonth.length;i+=7) {
            daysOfMonthByWeek.push(daysOfMonth.slice(i, i+7));
        } 
        
        cmp.set("v.daysOfMonthByWeek", daysOfMonthByWeek);
        return hasAvailableSlots;
    },
    
    getMonthName : function(cmp, month) {
        return cmp.get("v.months[" + month + "].name");
    },
    
    toggleBusy: function (cmp, isShow) {
        try {
            var busyCmp = cmp.find('busy');
            if (busyCmp && busyCmp.isValid()) {
                if (isShow) {
                   busyCmp.get('e.show').fire(); 
                } else {
                   busyCmp.get('e.hide').fire();
                }
            }
        } catch(e) {}
    },
    
    getDateString: function (dateObj) {
        var month = parseInt(dateObj.getMonth()) + 1;
        var date = dateObj.getDate();
        // Date key format YYYY-mm-dd
        return dateObj.getFullYear() + "-" + month + "-" + date;
    },
    
    cancelAppt: function (cmp) {
        var secReq = cmp.get("v.securityRequest");
        secReq.Time_Zone__c = null;
        $A.get("e.c:esaClearMessage").setParams({
            "doNotClearSticky" : false
        }).fire();
        this.toggleBusy(cmp, true);
        var action = cmp.get("c.cancelAppointment");        
        action.setParams({"securityRequest": secReq});
        action.setCallback(this, function(response) {
            $A.get("e.c:esaHandleResponse").setParams({
                "isStickyMsg" : true,
                "response": response,
                "onSuccess": $A.getCallback(function(returnVal) {
                    $A.get("e.c:esaRefreshEvt").setParams({
                        "securityRequest": returnVal
                    }).fire();
                })
            }).fire();
            this.toggleBusy(cmp, false);
        });
        //console.log(JSON.stringify(cmp.get("v.securityRequest")));
        $A.enqueueAction(action);
    },
    
    displayNextMonth : function(cmp) {
        this.cleanUp(cmp);
		var month = cmp.get("v.calendarConfig.month");
        var year = cmp.get("v.calendarConfig.year");
        if (month < 11) {
            month = month+1;
        } else {
            month = 0;
            year = year+1;
        }
        cmp.set("v.calendarConfig.month", month);
        cmp.set("v.calendarConfig.monthName", this.getMonthName(cmp, month));
        cmp.set("v.calendarConfig.year", year);
        return this.setDaysOfMonthByWeek(cmp);
	},
    
    displayPrevMonth : function(cmp) {
        this.cleanUp(cmp);
		var month = cmp.get("v.calendarConfig.month");
        var year = cmp.get("v.calendarConfig.year");
        if (month > 0) {
            month = month-1;
        } else {
            month = 11;
            year = year-1;
        }
        cmp.set("v.calendarConfig.month", month);
        cmp.set("v.calendarConfig.monthName", this.getMonthName(cmp, month));
        cmp.set("v.calendarConfig.year", year);
        return this.setDaysOfMonthByWeek(cmp);
	},
    
    stopEvent: function (event) {
        if (!$A.util.isUndefinedOrNull(event)) {
            event.stopPropagation();
            event.preventDefault();            
        }
    },
    
    setApptDateTime : function (cmp, apptDateTime, uglyApptDateTime) {        
        //console.log(apptDateTime);
        //console.log(uglyApptDateTime);
        cmp.set("v.selectedDateTime", apptDateTime);
        cmp.set("v.uglySelectedDateTime", uglyApptDateTime);
        
        this.toggleBusy(cmp, true);
        var THIS = this;
        // We don't want to close the calender immediate for better user experience
        setTimeout($A.getCallback(function() {
            THIS.toggleBusy(cmp, false);
            // Closes the timeslots popup
            cmp.set("v.clickedDateKey", null);
            // Close the expanded calendar
            cmp.find("calContainer").set("v.isExpanded", false);
        }), 500);
    },
    
    isReschedule : function (cmp) {
        var selectedDateTime = cmp.get("v.selectedDateTime");
        var confirmedDateTime = cmp.get("v.securityRequest.Office_Hours_Reservation__c");
        // If has confirmed appt and first time selecting other time then its a reschedule change
        if ($A.util.isEmpty(selectedDateTime) && !$A.util.isEmpty(confirmedDateTime)) {
            return true;
        }
        return false;
    },

    refreshTimezones : function(cmp) {
        var cUtil = cmp.find("commonUtil");
        var timezones = cmp.get("v.timezones");
        var selectedTz = cmp.get("v.selectedTimezone"); 

        if (!$A.util.isEmpty(selectedTz)) {
            var selectedTzIndex = cUtil.indexOf(timezones, function(tz) {
                return tz == selectedTz;
            });
            
            // Reoder list if needed to display the selected timezone in UI
            var breakPointIndex = parseInt(cmp.get("v.maxTimezoneOptions"));
            if (selectedTzIndex > breakPointIndex) {
                var itemToMove = timezones.splice(selectedTzIndex, 1);
                timezones.splice((breakPointIndex-1), 0, String(itemToMove));
                cmp.set("v.timezones", timezones);
            }
        }
    },
    
    setSelectedTimezone : function(cmp, selectedTzIndex) {        
        var index = parseInt(selectedTzIndex);
        //console.log(index);
        var timezones = cmp.get("v.timezones");        
        var selectedTimezone = timezones[index];
        cmp.set("v.selectedTimezone", selectedTimezone);
        this.refreshTimezones(cmp);
        $A.get("e.c:esaTimezoneChangeEvt").setParams({"timezone" : selectedTimezone}).fire();
    },
    
    cleanUp : function (cmp, clearApptDates) {
        cmp.set("v.clickedDateKey", null);
        if (clearApptDates) {
           cmp.set("v.apptDates", null); 
        }
    },
    
    clearSelection : function(cmp) {
        cmp.set("v.selectedDateTime", null);
        cmp.set("v.prettySelectedDateTime", null);
    }
})
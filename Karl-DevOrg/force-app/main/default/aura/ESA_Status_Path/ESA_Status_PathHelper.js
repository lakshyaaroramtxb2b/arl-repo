({
    fetchStatusOptions : function(cmp) { 
        // If present get it from config 
        // 'My Requests' flow will not have config as its reqs from various entities
        var optionsMap = cmp.get("v.config.statusOptions");
        if (optionsMap) {
            this.updateStatus(cmp, optionsMap);
        } else {
            var entityCode = cmp.get("v.securityRequest.Entity_Code__c");
            if (!$A.util.isEmpty(entityCode)) {
                var action = cmp.get("c.getStatusOptions");
                action.setParams({"entityCode" : entityCode});
                action.setCallback(this, function(response) {
                    var THIS = this;
                    $A.get("e.c:esaHandleResponse").setParams({
                        "response": response,
                        "onSuccess": $A.getCallback(function(returnVal) {
                            THIS.updateStatus(cmp, returnVal);
                        })
                    }).fire();
                });
                $A.enqueueAction(action);
            }
        }
	},
    
    updateStatus : function(cmp, optionsMap) {
        var options = optionsMap["ALL"];
        var currentIndex = this.getCurrentStatusIndex(cmp, options);
        for (var i=0;i<options.length;i++) {
            var option = options[i];
            var state = "incomplete";
            if (i < currentIndex) {
                state = "complete";
            } else if (i == currentIndex) {
                option.internalStatus = cmp.get("v.securityRequest.Internal_Status__c") || "";
                // Show blue highlighted states only for below as for other it doesn't fit well, show them as complete
                if (["New", "Pending", 'In Progress'].indexOf(options[i].value) != -1 ) {
                   state = "active";
                } else {
                   state = "complete";
                } 
            } else if (i-1 == currentIndex && options[i-1].state == "complete") {
                state = "current";
            }
            option.state = state;
        } 
        cmp.set("v.statusOptions", options);
    },
    
    // This method is deprecated, always set the internal status on current active state
    getInternalStatus : function(cmp, option, optionsMap) {
        var internalStatus = cmp.get("v.securityRequest.Internal_Status__c");
        if ($A.util.isEmpty(internalStatus)) {
            return "";
        }
        var inProgressIntStatusList = optionsMap["IN_PROGRESS"] || [];
        var closedIntStatusList = optionsMap["CLOSED"] || [];
        var isInternalInProgressStatus = inProgressIntStatusList.indexOf(internalStatus) != -1;
        var isInternalClosedStatus = closedIntStatusList.indexOf(internalStatus) != -1;

        if ((isInternalInProgressStatus && option.value == "In Progress")
            || (isInternalClosedStatus && option.value == "Closed")) {
           return internalStatus; 
        }
        return "";
    },
    
    getCurrentStatusIndex : function(cmp, options) {
        var currentStatus = cmp.get("v.securityRequest.Status__c");
        for (var i=0;i<options.length;i++) {
            if (options[i].value == currentStatus) {
                return i;
            }
        } 
        return 0;
    },
    
    cleanUp : function(cmp) {
        cmp.set("v.statusOptions", null);
    },
     
    setIsHidden : function(cmp) {
        var isHidden = ((cmp.get("v.serviceItem.Type__c")  == 'Instructions') 
                            || (!$A.util.isEmpty(cmp.get("v.securityRequest.Template_Parent_Entity__c"))));
        cmp.set("v.isHidden",  isHidden);
    }
    
})
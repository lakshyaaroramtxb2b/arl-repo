({
	doInit : function(cmp, event, helper) {
        helper.fetchStatusOptions(cmp);
        helper.setIsHidden(cmp);
	},
    
    updateStatus : function(cmp, event, helper) {
        helper.cleanUp(cmp);
        helper.fetchStatusOptions(cmp);
        helper.setIsHidden(cmp);
    },

    handleServiceItemSelection : function(cmp, event, helper) {
        cmp.set("v.serviceItem" , event.getParam("serviceItem"));
        helper.setIsHidden(cmp);        
    }
})
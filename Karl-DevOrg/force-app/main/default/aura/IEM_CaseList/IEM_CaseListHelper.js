({
    getCaseList: function (component, event, helper) {
        component.set('v.columns', [
            { label: 'Case Number', fieldName: 'CaseNumber', type: 'text' },
            { label: 'Subject', fieldName: 'Subject', type: 'text' },
            { label: 'Status', fieldName: 'Status', type: 'text' },
            {
                label: 'Action', initialWidth: 120, type: 'button', typeAttributes:
                {
                    label: 'View', name: 'View', disabled: false, value: 'View'
                }
            }
        ]);
        //Calling base component's helper method to call AuraEnabled Method
        helper.doCallout(component, 'c.getCases', {})
            .then(
                $A.getCallback(function (response) {
                    component.set("v.caseList", response);
                }),
                $A.getCallback(function (error) {
                    helper.showMessage('Error', error[0].message, 'error');
                })
            );

    },
    getCaseDetail: function (component, event, helper) {
        component.set("v.caseData", null);
        //Calling base component's helper method to call AuraEnabled Method
        helper.doCallout(component, 'c.getCaseData', {'caseId': component.get("v.recordId"), 'isInitialLoad':true })
            .then(
                $A.getCallback(function (response) {
                    // component.set("v.milestoneList",response);
                    component.set("v.caseData", response);

                }),
                $A.getCallback(function (error) {
                    helper.showMessage('Error', error[0].message, 'error');
                })
            );

    },

    createMilestoneRec: function (component, event, helper) {
        var _this = this;
        if (_this.validateFields(component)) {
            component.set("v.showSpinner", true);
            var caseData = component.get("v.caseData");
            var cloudName = caseData.cloudName;
            var teamResponsibleName = caseData.teamResponsibleName;
            var securityAssessmentRecordName = caseData.securityAssessmentRecordName;
            
            var milestoneDetail = component.get("v.milestoneDetail");
            milestoneDetail.Case__c = component.get("v.recordId");
            var msg = 'The record has been created successfully.';
            if(milestoneDetail.Id != undefined){
                msg = 'The record has been modified successfully.';
            }
            //Calling base component's helper method to call AuraEnabled Method
            helper.doCallout(component, 'c.createMilestone', { 'milestone': milestoneDetail })
                .then(
                    $A.getCallback(function (response) {
                        response.cloudName = cloudName;
                        response.teamResponsibleName = teamResponsibleName;
                        response.securityAssessmentRecordName = securityAssessmentRecordName;
                        component.set("v.caseData", response);
                        helper.showMessage('Success!', msg, 'success');

                        component.set("v.showSpinner", false);
                        component.set("v.isOpen", false);
                    }),
                    $A.getCallback(function (error) {
                        helper.showMessage('Error', error[0].message, 'error');
                        component.set("v.showSpinner", true);
                    })
                );
        }

    },
    deleteMilestone: function (component, event, helper) {
        var _this = this;
        component.set("v.showSpinner", true);
        var caseData = component.get("v.caseData");
        var cloudName = caseData.cloudName;
        var teamResponsibleName = caseData.teamResponsibleName;
        var securityAssessmentRecordName = caseData.securityAssessmentRecordName;
        
        var selectedRecordId = event.getSource().get("v.name");
        //Calling base component's helper method to call AuraEnabled Method
        helper.doCallout(component, 'c.deleteMilestoneRecord', { 'milestoneId': selectedRecordId, 'caseId': component.get("v.recordId") })
            .then(
                $A.getCallback(function (response) {
                    response.cloudName = cloudName;
                    response.teamResponsibleName = teamResponsibleName;
                    response.securityAssessmentRecordName = securityAssessmentRecordName;
                    component.set("v.caseData", response);
                    helper.showMessage('Success!', 'The record has been deleted successfully.', 'success');

                    component.set("v.showSpinner", false);
                    component.set("v.isOpen", false);
                }),
                $A.getCallback(function (error) {
                    helper.showMessage('Error', error[0].message, 'error');
                    component.set("v.showSpinner", true);
                })
            );


    },
    getRegionPicklist: function (component, event, helper) {
        var action = component.get("c.getPicklistValues");
        action.setParams({
            "ObjectApi_name": 'Case',
            "Field_name": 'Action_Plan_Status__c'
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.ActionPlanStatusPicklist", response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    getIsAwarePicklist: function (component, event, helper) {
        var action = component.get("c.getPicklistValues");
        action.setParams({
            "ObjectApi_name": 'Case',
            "Field_name": 'Action_Plan_Owner_is_Aware__c'
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.ActionPlanOwnerIsAware", response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    getMilestoneStatusPicklist: function (component, event, helper) {
        var action = component.get("c.getPicklistValues");
        action.setParams({
            "ObjectApi_name": 'Milestone__c',
            "Field_name": 'Status__c'
        });
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.MilestoneStatusPicklist", response.getReturnValue());
            }
        });
        $A.enqueueAction(action);
    },
    saveCaseData: function (component, event, helper) {
        component.set("v.showSpinner", true);
        var caseData = component.get("v.caseData");
        var cloudName = caseData.cloudName;
        var teamResponsibleName = caseData.teamResponsibleName;
        var securityAssessmentRecordName = caseData.securityAssessmentRecordName;
        //Calling base component's helper method to call AuraEnabled Method
        helper.doCallout(component, 'c.saveCase', { 'caseRec': component.get("v.caseData.caseRecord") })
            .then(
                $A.getCallback(function (response) {
                    response.cloudName = cloudName;
                    response.teamResponsibleName = teamResponsibleName;
                    response.securityAssessmentRecordName = securityAssessmentRecordName;
                    component.set("v.caseData", response);
                    helper.showMessage('Success!', 'The record has been updated successfully.', 'success');
                    component.set("v.showSpinner", false);
                }),
                $A.getCallback(function (error) {
                    helper.showMessage('Error', error[0].message, 'error');
                    component.set("v.showSpinner", false);
                })
            );

    },
    showToast: function (component, event, helper) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Success!",
            "message": "The record has been updated successfully."
        });
        toastEvent.fire();
    },
    validateFields: function (component) {
        var nameField = component.find("name");
        var name = nameField.get("v.value");

        var dueDateField = component.find("dueDate");
        var dueDate = dueDateField.get("v.value");

        var completedDateField = component.find("completedDate");
        var completedDate = completedDateField.get("v.value");

        var statusField = component.find("status");
        var status = statusField.get("v.value");

        var descriptionField = component.find("description");
        var description = descriptionField.get("v.value");

        var definitionOfDoneField = component.find("definitionOfDone");
        var definitionOfDone = definitionOfDoneField.get("v.value");

        var validSoFar = true;
        component.set("v.descriptionError", false);
        component.set("v.definationError", false);
        if ($A.util.isEmpty(name) || $A.util.isUndefined(name)) {
            nameField.showHelpMessageIfInvalid();
            validSoFar = false;
        }
        if ($A.util.isEmpty(dueDate) || $A.util.isUndefined(dueDate)) {
            dueDateField.showHelpMessageIfInvalid();
            validSoFar = false;
        }
        /* if ($A.util.isEmpty(completedDate) || $A.util.isUndefined(completedDate)) {
             completedDateField.showHelpMessageIfInvalid();
             validSoFar = false;
         } */

        if ($A.util.isEmpty(status) || $A.util.isUndefined(status)) {
            statusField.showHelpMessageIfInvalid();
            validSoFar = false;
        }

        if (description == '' || description == null) {
            component.set("v.descriptionError", true);
            validSoFar = false;
        }

        if (definitionOfDone == '' || definitionOfDone == null) {
            component.set("v.definationError", true);
            validSoFar = false;
        }

        return validSoFar;
    },
    getApprovalCondition: function (component, event, helper, selectedRecordId) {
        //Calling base component's helper method to call AuraEnabled Method
        console.log('HI');
        helper.doCallout(component, 'c.getApprovalProcessAccess', { 'targetObjectId': selectedRecordId })
            .then(
                $A.getCallback(function (response) {
                    console.log(response);
                    console.log(JSON.stringify(console.log(response)));
                    component.set("v.approvalButton", response);
                    component.set("v.rejectButton", response);
                }),
                $A.getCallback(function (error) {
                    helper.showMessage('Error', error[0].message, 'error');
                    component.set("v.showSpinner", false);
                })
            );
    },
    processApprovalRecordsHelper: function (component, event, helper) {

        component.set("v.showSpinner", true);
        var selectedRecordId = component.get("v.recordId");
        var action = component.get("v.actionLabel");
        console.log('processApprovalRecords Helper');
        //Calling base component's helper method to call AuraEnabled Method
        helper.doCallout(component, 'c.approveRejectRecord', {
            'targetObjectId': selectedRecordId,
            'Action': action,
            'commentMessage': component.get("v.approvalProcessComment")
        })
            .then(
                $A.getCallback(function (response) {
                    console.log(response);
                    console.log(JSON.stringify(console.log(response)));
                    //component.set("v.approvalButton", response);
                    component.set("v.rejectButton", response);
                    helper.getApprovalCondition(component, event, helper, selectedRecordId);
                    component.set("v.showSpinner", false);
                    component.set("v.actionLabel", '');
                    component.set("v.isApprovalModalOpen", false);
                    var messageStr = 'Case was approved.';
                    if(action == 'Reject'){
                        messageStr = 'Case was rejected.';
                    }
                    helper.showMessage('Success!', messageStr, 'success');
                    
                }),
                $A.getCallback(function (error) {
                    helper.showMessage('Error', error[0].message, 'error');
                    component.set("v.showSpinner", false);
                })
            );
    }
})
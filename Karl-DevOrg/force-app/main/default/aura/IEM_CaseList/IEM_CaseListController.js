({
    doInit : function(component,event,helper) {
        helper.getCaseList(component,event,helper);
        helper.getRegionPicklist(component, event);
        helper.getIsAwarePicklist(component, event);
    },
    handleRowAction: function (cmp, event, helper) {
        var action = event.getParam('action');
        var row = event.getParam('row');
        console.log('handleRowAction');
        switch (action.name) {
            case 'View':
                cmp.set("v.showList", false);
                cmp.set("v.recordId", row.Id);
                cmp.set("v.approvalButton", false);
                cmp.set("v.rejectButton", false);
        		helper.getApprovalCondition(cmp, event, helper,row.Id);
                helper.getCaseDetail(cmp, event, helper);
                helper.getMilestoneStatusPicklist(cmp, event, helper);
                break;
            case 'delete':
                helper.removeBook(cmp, row);
                break;
        }
    },
    handleClick: function(cmp, event, helper){
        cmp.set("v.showList", true);
    },
    saveCaseButton: function(cmp, event, helper){
        helper.saveCaseData(cmp, event, helper);
    },
    openModel: function(component, event, helper) {
        // for Display Model,set the "isOpen" attribute to "true"
        component.set("v.mode", 'edit');
        component.set("v.isOpen", true);
        var defaultData = { 'sobjectType': 'Milestone__c',
            			'Name': '','Status__c' : 'Pending',
                        'Due_Date__c' : '',
                        'Description__c': '', 
                        'Definition_of_Done__c':'',
                        'Completed_Date__c':''
                   }
        component.set("v.milestoneDetail", defaultData);
    },
    closeModel: function(component, event, helper) {
        // for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
        component.set("v.isOpen", false);
    },
    closeApprovalProcessModel: function(component, event, helper) {
        component.set("v.isApprovalModalOpen", false);
    }, 
    saveAndClose: function(component, event, helper) {
        helper.createMilestoneRec(component, event, helper);
    },
    deleteMilestone: function(component, event, helper) {
        helper.deleteMilestone(component, event, helper);
    },
    viewMilestone : function(component, event, helper){
        var selectedRecordId = event.getSource().get("v.name");
        var caseData = component.get("v.caseData");
        caseData.milestoneList.forEach(function(element) {
            if(element.Id == selectedRecordId){
                component.set("v.milestone", element);
                return;
            }
        });
        
        component.set("v.mode", 'read');
        component.set("v.isOpen", true);
    },
    editMilestone : function(component, event, helper){
        var selectedRecordId = event.getSource().get("v.name");
        var caseData = component.get("v.caseData");
        
        caseData.milestoneList.forEach(function(element) {
            if(element.Id == selectedRecordId){
                component.set("v.milestoneDetail", element);
                return;
            }
        });
        
        component.set("v.mode", 'edit');
        component.set("v.isOpen", true);
    },
    processApprovalRecords : function(component, event, helper){
        var actionLabel = event.getSource().get("v.label");
        component.set("v.actionLabel", actionLabel);
        component.set("v.isApprovalModalOpen", true);
        //helper.processApprovalRecordsHelper(component, event, helper);
    },
    approveAndClose : function(component, event, helper){
        helper.processApprovalRecordsHelper(component, event, helper);
    }
})
({
	doInit : function(cmp, event, helper) {
		helper.setFileDownloadURL(cmp);
	    helper.fetchDocuments(cmp);
	},
    
    handleSecReq : function(cmp, event, helper) {
        helper.cleanUp(cmp);
	    helper.fetchDocuments(cmp);
	}
})
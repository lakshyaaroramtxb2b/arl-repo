({
	fetchDocuments : function(cmp) {
		// cmp.find('busy').get('e.show').fire(); Errors on my requests between single vs tabs
        var action = cmp.get("c.getAllDocuments");
        action.setStorable();
        action.setParams({"entityCode": cmp.get("v.securityRequest.Entity_Code__c")});
        action.setCallback(this, function(response) {
            cmp.find('busy').get('e.hide').fire();
            $A.get("e.c:esaHandleResponse").setParams({
                "response": response,
                "onSuccess": $A.getCallback(function(returnVal) {
                    //console.log("Documents");
                    //console.log(returnVal);
                    cmp.set("v.documents", returnVal);
                })
            }).fire();
        });
        $A.enqueueAction(action);
    },
    
    setFileDownloadURL : function(cmp) {
        var cUtil = cmp.find("commonUtil");
		var downloadURL = cUtil.trimSuffix(cmp.get("v.site.prefix"), "/s") + "/servlet/servlet.FileDownload?file=";
		cmp.set("v.fileDownloadURL", downloadURL);
    },
    
    cleanUp : function(cmp) {
        cmp.set("v.documents", null);
    }
})
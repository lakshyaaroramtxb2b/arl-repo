({
    isFunction : function(value) {
        return (typeof value === "function");
    },

    isString : function(value) {
        return (typeof value === "string");
    },

    trim : function(value) {
        if ($A.util.isEmpty(value) || !this.isString(value)) {
            return '';
        }

        if (value.trim) {
            return value.trim();
        } else if (value.replace) {
            return value.replace(/^\s+|\s+$/gm,'');
        }
        return value;
    },

    toggleBusy : function(id, isBusy) {
        if ($A.util.isEmpty(id)) {
            return;
        }
        /* TODO: Replace ESA_Busy with sldsSpinner and then uncomment
        $A.get("e.c:sldsSpinnerEvt").setParams({
            "id" : id,
            "isShow" : isBusy
        }).fire();*/
    },

    forEach : function(value, callback, delim) {
        if ($A.util.isUndefinedOrNull(value) || $A.util.isUndefinedOrNull(callback)) {
            return;
        }
        if ($A.util.isArray(value)) {
            for (var i=0;i<value.length;i++) {
                var item = value[i];
                if (this.isString(item)) {
                    item = this.trim(item);
                }
                callback(item); 
            }
        } if (this.isString(value) && !$A.util.isEmpty(delim)) {
            this.forEach(value.split(delim), callback);
        } else if ($A.util.isObject(value)) {
            for (var prop in value) {
                callback(prop, value[prop]);
            }       
        }
    },
    
    checkSessionTimeout : function(cmp) {
        var THIS = this;
        var action = cmp.get("c.isSessionActive");
        action.setCallback(this, function(response) {
            var state = response.getState();
            var isSessionActive = $A.util.getBooleanValue(response.getReturnValue());        
            if (state == "ERROR" || !isSessionActive) {            
               $A.get("e.c:commonSessionTimeoutEvt").fire(); 
            }              
        });
        $A.enqueueAction(action);
    },
    
    processResponse : function(cmp, config, response) {
        var onSuccess = config.onSuccess;
        var onFailure = config.onFailure;
        var onFinally = config.onFinally;
        var msgId = config.messageId;
        var isSticky = config.isStickyMsg;
        var isIgnoreMsg = config.isIgnoreServerMsg;
        var state = response.getState();
        var responseVal = response.getReturnValue() || {};
        
        if (state == "SUCCESS" && !responseVal.isError) {
            if (onSuccess) {
                onSuccess(responseVal.data || responseVal); 
            }  
        } else if (state == "INCOMPLETE") {
            $A.get("e.c:commonShowMessageEvt").setParams({"id": msgId, "text": $A.get("$Label.c.Common_TryAgain"), "variant": "warning"}).fire();
        } else if (state == "ERROR") {
            this.checkSessionTimeout(cmp);
            var errors = response.getError();
            if (errors) {
                for (var i=0;i<errors.length;i++) {
                    $A.get("e.c:commonShowMessageEvt").setParams({
                        "id": msgId,
                        "text": errors[i].message, 
                        "variant": "error", 
                        "isSticky" : false
                    }).fire();
                }                  
            }                 
        }
        
        if (state != "SUCCESS" || responseVal.isError) {
            if (onFailure) {
                onFailure(responseVal.data || responseVal); 
            }  
        }
        
        if (onFinally) {
            onFinally(responseVal.data || responseVal); 
        } 
	} 
})
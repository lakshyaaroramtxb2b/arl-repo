({
	toArray : function(cmp, event, helper) {
        var arrayObj = [];
        var value = event.getParam('arguments').value;
        if (!$A.util.isUndefinedOrNull(value)) {
            if ($A.util.isArray(value)) {
                arrayObj = value;
            } else {
                arrayObj.push(value);
            }
        }
        return arrayObj;
    },

    toText : function(cmp, event, helper) {
        var htmlValue = event.getParam('arguments').value;
        if ($A.util.isEmpty(htmlValue)) {
            return "";
        }

        var htmlElem = document.createElement('div');
        htmlElem.innerHTML = htmlValue;

        return htmlElem.innerText;
        
    },
    
    trim : function(cmp, event, helper) {
        var value = event.getParam('arguments').value || "";
        return helper.trim(value);
    },

    trimSuffix : function(cmp, event, helper) {
        var value = event.getParam('arguments').value || "";
        var suffix = event.getParam('arguments').suffix;
        
        if ($A.util.isEmpty(suffix)) {
            return value;
        }
        // add some random string to end to identify suffix at the end of string 
        // and ignore any other occurrences in the middle of string
        var suffixExt = " $#"; 
        value = value + suffixExt;

        value = value.replace(suffix+suffixExt, ""); // Correctly replaces the suffix at the end of string if found
        value = value.replace(suffixExt, ""); // suffixExt won't get cleared when no matching suffix is found so clear it now
        return value;
    },

    replaceParams : function(cmp, event, helper) {
        var text = event.getParam('arguments').text || "";
        var params = event.getParam('arguments').params || {};

        if ($A.util.isArray(params)) {
            var count = 0;
            helper.forEach(params, function(value) {
                text = text.replace('{'+count+'}', value);
                count++;
            });
        } else if ($A.util.isObject(params)) {
            helper.forEach(params, function(name, value) {
                text = text.replace('{'+name+'}', value);
            });
        }   
        return text;
    },
    
    
    forEach : function(cmp, event, helper) {
        var value = event.getParam('arguments').value;
        var callback = event.getParam('arguments').callback;
        var delim = event.getParam('arguments').delimiter;
        helper.forEach(value, callback, delim);
    },

    indexOf : function(cmp, event, helper) {
        var value = event.getParam('arguments').value;
        var condition = event.getParam('arguments').condition;
        if ($A.util.isArray(value)) {
            for(var i=0;i<value.length;i++) {
                if (condition(value[i])) {
                    return i;
                }
            }
        } 
        return -1;
    },

    waitFor : function(cmp, event, helper) {
        var condition = event.getParam('arguments').condition;
        var callback = event.getParam('arguments').callback;
        var interval = event.getParam('arguments').interval;
        if (!helper.isFunction(condition) || !helper.isFunction(callback)) {
            return;
        }
        var counter = 0;
        var checkCondition = function() {
            // Max 10 attempts
            if (counter++ > 10) {
                return; 
            }
            if (condition()) {
                callback();
            } else {
                // Wait and check after the interval
                setTimeout($A.getCallback(function() {
                    checkCondition();
                }), interval);
            }
        };
        checkCondition();
    },
    
    clearMessages : function(cmp, event, helper) {
        // TODO
    },
    
    processAction : function(cmp, event, helper) {
        var args = event.getParam('arguments');
        var THIS = args.scope || this;
        var action = args.action;
        var config = args.config;
        helper.toggleBusy(config.busyId, true);
        action.setCallback(THIS, function(response) {
            helper.processResponse(cmp, config, response);
            helper.toggleBusy(config.busyId, false);
        });
        $A.enqueueAction(action); 
    },

    checkSessionTimeout  : function(cmp, event, helper) {
        helper.checkSessionTimeout(cmp);
    }
    
})
({
    executeShareBatch: function (component, event, helper) {
        var valid;
        component.find('minutesLength').showHelpMessageIfInvalid();
        valid = component.find('minutesLength').get("v.validity").valid;

        if (!valid) {
            return;
        }

        var action = component.get("c.executeFollowUpShareBatch");
        action.setParams({
            minutesLength: component.get("v.minutesLength")
        });
        console.log('calling apexxx' + component.get("v.minutesLength"));
        action.setCallback(this, function (response) {
            console.log('response.value()>>>' + response.getReturnValue());
            var state = response.getState();
            console.log('response.state()>>>' + response.getState());
            if (state === "SUCCESS") {
                var dismissActionPanel = $A.get("e.force:closeQuickAction");
                dismissActionPanel.fire();
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "type": "error",
                        "message": errors[0].message
                    });
                    toastEvent.fire();
                    var dismissActionPanel = $A.get("e.force:closeQuickAction");
                    dismissActionPanel.fire();

                    console.log("Error message: " + JSON.stringify(errors));
                } else {
                    console.log("Unknown error");
                }

            }
        })
        $A.enqueueAction(action);
        setTimeout(() => {
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "Success",
                "type": "success",
                "message": 'Batch executing in background'
            });
            toastEvent.fire();
            var dismissActionPanel = $A.get("e.force:closeQuickAction");
            dismissActionPanel.fire();
        }, 5000)
    },
    closeModal: function (component) {
        $A.get("e.force:closeQuickAction").fire();
    }

})
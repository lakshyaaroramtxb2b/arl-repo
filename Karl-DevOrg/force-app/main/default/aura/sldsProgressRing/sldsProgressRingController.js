({
	doInit : function(cmp, event, helper) {
		helper.handleStatusDisplay(cmp);
	},
    
    refresh : function(cmp, event, helper) {
		helper.handleStatusDisplay(cmp);
	}
})
({
	handleStatusDisplay : function(cmp) {
		var status = cmp.get("v.status");
        var icon = "";
        var theme = ""
        if (status == "NOT_APPLICABLE") {
            icon = "dash";
            theme = "success";
        } else if (status == "COMPLETED") {
            icon = "check";
            theme = "success";
        } else if (status == "FAILED") {
            icon = "warning";
            theme = "error";
        } else if (status == "IN_PROGRESS") {
            icon = "info";
            theme = "warning";
        }
        cmp.set("v.icon", icon);
        cmp.set("v.theme", theme);
	}
})
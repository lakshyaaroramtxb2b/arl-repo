import { LightningElement, api, track } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';

/**
 * @author Ben Harvie
 * @email ben.harvie@salesforce.com
 * @desc This LWC Component Builds the tiles as a part of the ARI Component on Cycle Request Item Object
 */
export default class karl_areq_tile extends NavigationMixin(LightningElement) {
    @api recordId;
    @api areq;
    @track recordPageUrl;

    /** 
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @desc Standard Connected Call Back for Child
     * @modifier recordId to be modified based on the object in question
     */
    connectedCallback() {
        // Generate a URL to the relevant record
        this[NavigationMixin.GenerateUrl]({
            type: 'standard__recordPage',
            attributes: {
                recordId: this.areq.Id,
                actionName: 'view',
            },
        }).then(url => {
            this.recordPageUrl = url;
        });
    }

    /** 
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @desc Create On Click Event
     */
    handleOpenRecordClick() {
        const selectEvent = new CustomEvent('karlrecordview', {
            detail: this.areq.Id
        });
        this.dispatchEvent(selectEvent);
    }

    /** 
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @desc Styling for Icons (Full Name)
     */
    get iconFullStatus(){
        if( this.areq.KARL_Status__c == "Follow-ups" ){
            return "utility:help";
        }else if( this.areq.KARL_Status__c == "Testing Completed" ){
            return "utility:success";
        }else if( this.areq.KARL_Status__c == "Testing in Progress" ){ 
            return "utility:success";
        }else if( this.areq.KARL_Status__c == "Not Applicable" ){
            return "utility:record";
        }else{
            return "utility:rotate";
        }
    }

    /** 
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @desc Styling for Icons (Variant Styling)
     */
    get iconShortStatus(){
        if( this.areq.KARL_Status__c == "Follow-ups" || this.areq.KARL_Status__c == "Not Applicable" ){
            return "warning";
        }else if( this.areq.KARL_Status__c == "Testing in Progress" || this.areq.KARL_Status__c == "Testing Completed" ){
            return "success";
        }else{
            return "";
        }
    }
    
}
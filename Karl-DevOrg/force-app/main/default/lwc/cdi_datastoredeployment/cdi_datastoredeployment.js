import { LightningElement,track } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import DATASTOREDEPLOYMENT_OBJECT from '@salesforce/schema/CDI_Data_Store_Deployment__c';
import DATACENTER_FIELD_DATASTOREDEPLOYMENT from '@salesforce/schema/CDI_Data_Store_Deployment__c.Data_Center__c';
import DATASTOREVALUE_FIELD from '@salesforce/schema/CDI_Data_Store_Deployment__c.Data_Store__c';
export default class Datatransferform extends NavigationMixin(LightningElement) {
     datastoredeploymentobject=DATASTOREDEPLOYMENT_OBJECT;
   datacentervalueField=DATACENTER_FIELD_DATASTOREDEPLOYMENT;
   datastorevalueField=DATASTOREVALUE_FIELD;
   
   


   @track bShowModal = false;

   /* javaScipt functions start */
   openModal(event) {    
       // to open modal window set 'bShowModal' tarck value as true
       this.bShowModal = true;
       const inputFields = this.template.querySelectorAll(
           'lightning-input-field'
       );
       if (inputFields) {
           inputFields.forEach(field => {
               field.reset();
           });
       }
       this[NavigationMixin.Navigate]({
           type: 'standard__recordPage',
           attributes: {
               recordId: event.detail.id,
               objectApiName: 'Data_Store_Deployment__c',
               actionName: 'view',
           },
       });
   }

   closeModal() {    
       // to close modal window set 'bShowModal' tarck value as false
       this.bShowModal = false;
       
   }
   /* javaScipt functions end */
}
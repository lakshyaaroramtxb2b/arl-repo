import { LightningElement, track, wire } from 'lwc';
import {CurrentPageReference, NavigationMixin} from 'lightning/navigation';
import { fireEvent } from 'c/pubsub';
import { registerListener, unregisterAllListeners } from 'c/pubsub';
import getAllApprovalRequests from '@salesforce/apex/IntakeBulkApprovalController.getAllApprovalRequests';

// Import custom labels
import emptyApprovals from '@salesforce/label/c.ESA_EmptyApprovals';
import approve from '@salesforce/label/c.ESA_Approve';
import reject from '@salesforce/label/c.ESA_Reject';
import requestor from '@salesforce/label/c.ESA_Requestor';
import type from '@salesforce/label/c.ESA_Type';
import amount from '@salesforce/label/c.ESA_Amount';
import status from '@salesforce/label/c.ESA_Status';
import actions from '@salesforce/label/c.ESA_Actions';
import onBehalfOf from '@salesforce/label/c.ESA_OnBehalfOf';
import details from '@salesforce/label/c.ESA_Details';
import recordPage from '@salesforce/label/c.ESA_RecordPage';

const columns = [
    { label: requestor, fieldName: 'processWorkItem.Actor.Name' },
    { label: type, fieldName: 'securityRequest.Service_Item_Name__c'},
    { label: amount, fieldName: 'securityRequest.Tracked_Number_01__c', type: 'number' },
    { label: status, fieldName: 'securityRequest.Approval_Status__c'},
    {
        label: actions,
        type: 'action',
    },
];

export default class IntakeBulkApprovals extends NavigationMixin(LightningElement) {
    label = {
        emptyApprovals, approve, reject, onBehalfOf, details, recordPage
    }

    @track status = 'Pending';
    @track columns = columns;
    @track approvalRequests = [];
    @track isEmptyApprovalRequests = false;
    @track detailsURL;

    @wire(CurrentPageReference) pageRef;

    connectedCallback() { 
        registerListener('showBusy', this.showBusy, this);
        registerListener('hideBusy', this.hideBusy, this);
        registerListener('updateApprovalRequests', this.updateApprovalRequests, this);
	}
	disconnectedCallback() {
		// unsubscribe
		unregisterAllListeners(this);
    }

    showBusy() {
        this.template.querySelector('c-busy-spinner').showBusy();
    }

    hideBusy() {
        this.template.querySelector('c-busy-spinner').hideBusy();
    }

    @wire(getAllApprovalRequests, {status: '$status'})
    setApprovalRequests(result) {
        if (result.data && result.data.length > 0) {
            this.approvalRequests = result.data;  
        } 
        this.checkForEmpty();
    }

    checkForEmpty() {
        this.isEmptyApprovalRequests = !this.approvalRequests || this.approvalRequests.length < 1;
    }

    setDetailsURL(event) {
        const id = event.target.getAttribute('data-id');
        this[NavigationMixin.GenerateUrl]({
            type: 'standard__recordPage',
            attributes: {
                recordId: id,
                actionName: 'view',
            }
        }).then(url => {this.detailsURL = url;});
    }

    showDetailsDialog(event) {
        const id = event.target.value;
        fireEvent(this.pageRef, 'openRequestDetails', id);
    }

    handleApprove(event) {
        const id = event.target.value;
        fireEvent(this.pageRef, 'approveRequest', this.getApprovalRequest(id));
    }

    handleReject(event) {
        const id = event.target.value;
        fireEvent(this.pageRef, 'rejectRequest', this.getApprovalRequest(id));
    }

    updateApprovalRequests(id) {
        var i=0;
        var updatedApprovalRequests = [];
        this.showBusy();
        for (;i<this.approvalRequests.length;i++) {
            if (this.approvalRequests[i].processWorkItem.Id === id) {
               continue; 
            }
            updatedApprovalRequests.push(this.approvalRequests[i]);
        }
        this.approvalRequests = updatedApprovalRequests;
        this.checkForEmpty();
        this.hideBusy();
    }

    getApprovalRequest(id) {
        var approvalRequest;
        var i=0;
        for (;i<this.approvalRequests.length;i++) {
            if (this.approvalRequests[i].processWorkItem.Id === id) {
               approvalRequest = this.approvalRequests[i];
               break;
            }
        }
        return approvalRequest; 
    }
	
}
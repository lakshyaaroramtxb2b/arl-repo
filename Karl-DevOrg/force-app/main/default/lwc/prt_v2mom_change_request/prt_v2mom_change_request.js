import { LightningElement, track, wire } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import createChangeRequest from '@salesforce/apex/PRT_V2MOMChangeRequestCtrl.createChangeRequest';
import getSecurityPortfolio from '@salesforce/apex/PRT_V2MOMChangeRequestCtrl.securityGRCRecord';
import getMethodAndMeasureRecords from '@salesforce/apex/PRT_V2MOMChangeRequestCtrl.getMethodAndMeasureRecords';
import updateMethodAndMeasureChange from '@salesforce/apex/PRT_V2MOMChangeRequestCtrl.updateMethodAndMeasureChange';
import getMethodAndTheirMeasures from '@salesforce/apex/PRT_V2MOMChangeRequestCtrl.getMethodAndTheirMeasures';
import deleteMethodOrMeasureChange from '@salesforce/apex/PRT_V2MOMChangeRequestCtrl.deleteMethodOrMeasureChange';
import getRelatedMethod from '@salesforce/apex/PRT_V2MOMChangeRequestCtrl.getRelatedMethod';
import { loadStyle } from 'lightning/platformResourceLoader';

import { getPicklistValues } from 'lightning/uiObjectInfoApi';
import { getObjectInfo } from 'lightning/uiObjectInfoApi';
import {NavigationMixin} from 'lightning/navigation'; 
import CHANGE_REQUEST from '@salesforce/schema/V2MOM_Change_Request__c';
import STATUS_FIELD from '@salesforce/schema/V2MOM_Change_Request__c.Status__c';
import CUSTOM_CSS from '@salesforce/resourceUrl/v2mom_entry_textarea_css';

export default class Prt_v2mom_change_request extends NavigationMixin(LightningElement) {
    @track req = true;
    @track showRadioGroup = true;
    @track showDeleteButton = false;
    @track statusValues = [];

    @track v2momIconFlag = true;
    @track portfolioIconFlag = true;
    @track methodIconFlag = true;
    @track measureIconFlag = true;
    @track portfolioClear;
    @track portfolioRead;

    @track v2momId = '';
    @track v2momName = '';
    @track selectedPortfolioId;
    @track selectedPortfolioName;
    @track methodAndMeasureData = [];
    @track noDataPresent = 'There are no method or measure change data for this change request.';
    @track disabled;
    @track currentStatus;
    @track changerequestId;
    @track methodOrMeasureChange = [];

    @track methodAndTheirMeasures = [];

    @wire(getObjectInfo, { objectApiName: CHANGE_REQUEST })
    objectInfo;

    @wire(getPicklistValues, { recordTypeId: '$objectInfo.data.defaultRecordTypeId', fieldApiName: STATUS_FIELD })  
    StatusPicklistValues;

    connectedCallback() {
        loadStyle(this, CUSTOM_CSS)
        .then(() => {
            console.log('STYLE LOADED!!!!!!!!!!');
        });
        let data = [];
        let hashValue = window.location.hash.substr(1);
        this.changerequestId = hashValue.split('=')[1];
        console.log("New URL: "+this.changerequestId);
        if(this.changerequestId != undefined) {
            getMethodAndMeasureRecords({
                changeRequestId: this.changerequestId
            })
            .then(result => {
                console.log("RRRRRR: "+result);
                if(result != null) {
                    if(result[0].methodOrMeasureName != '') {
                        this.methodAndMeasureData = result;
                    }
                    this.v2momId = result[0].v2MOMId;
                    this.v2momName = result[0].v2MOMName;
                    this.selectedPortfolioId = result[0].portfolioId;
                    this.selectedPortfolioName = result[0].portfolioName;
                    this.currentStatus = result[0].changeRequestStatus;
                    console.log("Current Status Value: "+this.currentStatus);
                    data.push({
                        "index": 0,
                        "isInsert": true,
                        "isMethod": true,
                        "value1": "New",
                        "value2": "Method",
                        "v2momId": result[0].v2MOMId,
                        "portfolioId": result[0].portfolioId,
                        "methodOrMeasureId": "",
                        "methodOrMeasureName": "",
                        "relatedMethodId": "",
                        "relatedMethodName": "",
                        "relatedMethodIcon": true,
                        "relatedMethodClear": false,
                        "relatedMethodRead": false,
                        "currentDescOrComm": "",
                        "updatedDescOrComm": "",
                        "detailedJustification": "",
                        "targetDateOfChange": "",
                        "radioGroup1": "rg10",
                        "radioGroup2": "rg20",
                        "measureTargetValue": "",
                    });
                    this.methodOrMeasureChange = data;
                    console.log("Value: "+JSON.stringify(this.methodOrMeasureChange));
                    console.log("V2MOM: "+this.v2momId+"  "+this.v2momName+"  "+this.selectedPortfolioId+"  "+this.selectedPortfolioName);
                    console.log('Incoming Data: '+JSON.stringify(this.methodAndMeasureData));
                }
                else {
                    this.noDataPresent = 'There are no method or measure change data for this change request.';
                }
            })
            this.v2momIconFlag = false;
            this.disabled = true;
            this.portfolioIconFlag = false;
            this.portfolioClear = false;
            this.portfolioRead = true;
        }
        else {
            getSecurityPortfolio({})
            .then(result => {
                if(result) {
                    this.selectedPortfolioId = result.id;
                    this.selectedPortfolioName = result.name;
                    data.push({
                        "index": 0,
                        "isInsert": true,
                        "isMethod": true,
                        "value1": "New",
                        "value2": "Method",
                        "v2momId": "",
                        "portfolioId": result.id,
                        "methodOrMeasureId": "",
                        "methodOrMeasureName": "",
                        "relatedMethodId": "",
                        "relatedMethodName": "",
                        "relatedMethodIcon": true,
                        "relatedMethodClear": false,
                        "relatedMethodRead": false,
                        "currentDescOrComm": "",
                        "updatedDescOrComm": "",
                        "detailedJustification": "",
                        "targetDateOfChange": "",
                        "radioGroup1": "rg10",
                        "radioGroup2": "rg20",
                        "measureTargetValue": "",
                    });
                    this.methodOrMeasureChange = data;
                    console.log("Value: "+JSON.stringify(this.methodOrMeasureChange));
                    console.log("Selected Portfolio: "+this.selectedPortfolioId+"  "+this.selectedPortfolioName);
                }
            })
            this.portfolioIconFlag = false;
            this.portfolioClear = true;
            this.portfolioRead = true;
        }  
    }

    get options1() {
        return [
            { label: 'Create a change in existing V2MOM', value: 'Change' },
            { label: 'Create a new V2MOM method or measure', value: 'New' }
        ];
    }

    get options2() {
        return [
            { label: 'Method', value: 'Method' },
            { label: 'Measure', value: 'Measure' }
        ];
    }

    handleChange1(event) {
        var targetId = event.target.dataset.id;
        console.log("Target Radio Group 1 Id: "+targetId);
        const insertOrChange = event.target.value;
        var toInsert;
        if(insertOrChange == 'New') {
            toInsert = true;
        }
        else {
            toInsert = false;
        }        
        console.log('Insert or Change '+insertOrChange);
        if(this.v2momId == '' && insertOrChange == 'Change') {
            this.showRadioGroup = false;
            window.clearTimeout(this.delayTimeout);
            this.delayTimeout = setTimeout(() => {
                this.showRadioGroup = true;
            }, 0);
            const toastEvent = new ShowToastEvent({
                title: 'INFO!!',
                message: 'Select a V2MOM first!!',
                variant: 'info'
            });
            this.dispatchEvent(toastEvent);
        }
        else {    
            this.methodOrMeasureChange.forEach(function(data) {
                console.log("ToInsert: "+toInsert);
                console.log('Data.Index: '+data.index);
                if(data.index == targetId) {
                    console.log("ToInsert: "+toInsert);
                    data.isInsert = toInsert;
                    data.value1 = insertOrChange;
                    data.methodOrMeasureId = "";
                    data.methodOrMeasureName = "";
                    data.currentDescOrComm = "";
                    data.updatedDescOrComm = "";
                    data.detailedJustification = "";
                    data.targetDateOfChange = "";
                    data.measureTargetValue = "";
                }
            })
        }
    }

    handleChange2(event) {
        const targetId = event.target.dataset.id;
        const methodOrMeasure = event.target.value;
        var toMethod;
        console.log('Method Or Measure '+methodOrMeasure);
        if(methodOrMeasure == 'Method') {
            toMethod = true;
        }
        else {
            toMethod = false;
        }
        this.methodOrMeasureChange.forEach(function(data) {
            if(data.index == targetId) {
                data.isMethod = toMethod;
                data.value2 = methodOrMeasure;
                data.methodOrMeasureId = "";
                data.methodOrMeasureName = "";
                data.currentDescOrComm = "";
                data.updatedDescOrComm = "";
                data.detailedJustification = "";
                data.targetDateOfChange = "";
                data.measureTargetValue = "";
            }
        })
    }

    methodChange(event) {
        let targetId = event.target.dataset.id;
        this.methodOrMeasureChange.forEach(function(data) {
            if(data.index == targetId) {
                data.methodOrMeasureName = event.target.value;
                console.log("value: "+data.methodOrMeasureName);
            }
        })
    }

    relatedMethodChange(event) {
        let targetId = event.target.dataset.id;
        this.methodOrMeasureChange.forEach(function(data) {
            if(data.index == targetId) {
                data.relatedMethodName = event.target.value;
                console.log("value: "+data.methodOrMeasureName);
            }
        })
    }

    methodDateChange(event) { 
        var today = new Date();        
        var dd = today.getDate();
        var mm = today.getMonth() + 1;
        var yyyy = today.getFullYear(); 
        if(dd < 10){
            dd = '0' + dd;
        }     
        if(mm < 10){
            mm = '0' + mm;
        }
        
        let targetId = event.target.dataset.id;
        var todayFormattedDate = yyyy+'-'+mm+'-'+dd;
        var selectedDate = event.target.value;
        if(selectedDate != '' && selectedDate < todayFormattedDate) {
            event.target.value = '';
            const toastEvent = new ShowToastEvent({
                title: 'ERROR!!',
                message: 'Target Date of Change can be in present or future.',
                variant: 'error'
            });
            this.dispatchEvent(toastEvent);
        } 
        else {
            this.methodOrMeasureChange.forEach(function(data) {
                if(data.index == targetId) {
                    data.targetDateOfChange = event.target.value;
                    console.log("value: "+data.targetDateOfChange);
                }
            })
        }    
    }
    methodDescChange(event) {
        let targetId = event.target.dataset.id;
        this.methodOrMeasureChange.forEach(function(data) {
            if(data.index == targetId) {
                data.updatedDescOrComm = event.target.value;
                console.log("value: "+data.updatedDescOrComm);
            }
        })
    }
    methodJustifyChange(event) {
        let targetId = event.target.dataset.id;
        this.methodOrMeasureChange.forEach(function(data) {
            if(data.index == targetId) {
                data.detailedJustification = event.target.value;
                console.log("value: "+data.detailedJustification);
            }
        })
    }
    measureChange(event) {
        let targetId = event.target.dataset.id;
        this.methodOrMeasureChange.forEach(function(data) {
            if(data.index == targetId) {
                data.methodOrMeasureName = event.target.value;
                console.log("value: "+data.methodOrMeasureName);
            }
        })
    }
    targetValueChange(event) {
        let targetId = event.target.dataset.id;
        this.methodOrMeasureChange.forEach(function(data) {
            if(data.index == targetId) {
                data.measureTargetValue = event.target.value;
                console.log("value: "+data.measureTargetValue);
            }
        })
    }
    measureDateChange(event) {
        var today = new Date();        
        var dd = today.getDate();
        var mm = today.getMonth() + 1;
        var yyyy = today.getFullYear(); 
        if(dd < 10){
            dd = '0' + dd;
        }     
        if(mm < 10){
            mm = '0' + mm;
        }
        
        let targetId = event.target.dataset.id;
        var todayFormattedDate = yyyy+'-'+mm+'-'+dd;
        var selectedDate = event.target.value;
        if(selectedDate != '' && selectedDate < todayFormattedDate) {
            event.target.value = '';
            const toastEvent = new ShowToastEvent({
                title: 'ERROR!!',
                message: 'Target Date of Change can be in present or future.',
                variant: 'error'
            });
            this.dispatchEvent(toastEvent);
        } 
        else {
            this.methodOrMeasureChange.forEach(function(data) {
                if(data.index == targetId) {
                    data.targetDateOfChange = event.target.value;
                    console.log("value: "+data.targetDateOfChange);
                }
            })
        }
    }
    measureCommChange(event) {
        let targetId = event.target.dataset.id;
        this.methodOrMeasureChange.forEach(function(data) {
            if(data.index == targetId) {
                data.updatedDescOrComm = event.target.value;
                console.log("value: "+data.updatedDescOrComm);
            }
        })
    }
    measureJustifyChange(event) {
        let targetId = event.target.dataset.id;
        this.methodOrMeasureChange.forEach(function(data) {
            if(data.index == targetId) {
                data.detailedJustification = event.target.value;
                console.log("value: "+data.detailedJustification);
            }
        })
    }

    selectedV2MOM(event) {
        let recordData = event.detail;
        this.v2momId = recordData.currentRecId;
        this.v2momName = recordData.selectName;
        let len = this.methodOrMeasureChange.length;
        let totalLookups = len*3;

        for(var i=2; i<=totalLookups+1; i++) {
            // if(this.template.querySelectorAll('c-prt_v2momreq_custom_lookup')[2*(i+1)] != null) {
            //     this.template.querySelectorAll('c-prt_v2momreq_custom_lookup')[2*(i+1)].resetData();
            // }
            if(this.template.querySelectorAll('c-prt_v2momreq_custom_lookup')[i] != null) {
                this.template.querySelectorAll('c-prt_v2momreq_custom_lookup')[i].resetData();
            }
        }

        getMethodAndTheirMeasures({
            v2momId: recordData.currentRecId
        })
        .then(result => {
            if(result) {
                console.log("Resullllllllllttttttttttt: "+JSON.stringify(result));
                this.methodAndTheirMeasures = result;
            }
        })
        
        this.methodOrMeasureChange.forEach(function(data) {
            data.v2momId = recordData.currentRecId;
            data.methodOrMeasureName = '';
        })
        console.log("V2MOM Id: "+this.v2momId);
        console.log("V2MOM Name: "+this.v2momName);
    }

    selectedMethod(event) {
        let recordData = event.detail;
        let selectedMethodId = recordData.currentRecId;
        let selectedMethodName = recordData.selectName;
        let methodDesc = recordData.descOrComm;
        let targetId = event.target.dataset.id;
        this.methodOrMeasureChange.forEach(function(data) {
            if(data.index == targetId) {
                data.methodOrMeasureName = selectedMethodName;
                data.methodOrMeasureId = selectedMethodId;
                data.currentDescOrComm = methodDesc;
            }
        })
        console.log("Method Id: "+selectedMethodId);
        console.log("Method Name: "+selectedMethodName);
    }
    
    selectedMeasure(event) {
        let recordData = event.detail;
        let selectedMeasureId = recordData.currentRecId;
        let selectedMeasureName = recordData.selectName;
        let measureComm = recordData.descOrComm;
        let targetId = event.target.dataset.id;
        this.methodOrMeasureChange.forEach(function(data) {
            if(data.index == targetId) {
                data.methodOrMeasureName = selectedMeasureName;
                data.methodOrMeasureId = selectedMeasureId;
                data.currentDescOrComm = measureComm;
            }
        })
        getRelatedMethod({
            measureId: recordData.currentRecId
        })
        .then(result => {
            if(result != null) {
                let relatedMethodData = JSON.stringify(result);
                console.log('Result: '+JSON.stringify(result));
                
                this.methodOrMeasureChange.forEach(function(data) {
                    if(data.index == targetId) {
                        console.log("Related Method Id: "+result.relatedMethodId);
                        console.log("Related Method Name: "+result.relatedMethodName);
                        
                        data.relatedMethodId = result.relatedMethodId;
                        data.relatedMethodName = result.relatedMethodName;
                        data.relatedMethodIcon = false;
                        data.relatedMethodClear = true;
                        data.relatedMethodRead = true;
                    }
                })
            }
        });
        console.log("Measure Id: "+selectedMeasureId);
        console.log("Measure Name: "+selectedMeasureName);
    }

    selectedPortfolio(event) {
        let recordData = event.detail;
        this.selectedPortfolioId = recordData.currentRecId;
        this.selectedPortfolioName = recordData.selectName;
        this.methodOrMeasureChange.forEach(function(data) {
            data.portfolioId = recordData.currentRecId;
        })
        console.log("Portfolio Id: "+this.selectedPortfolioId);
        console.log("Portfolio Name: "+this.selectedPortfolioName);
    }

    selectedRelatedMethod(event) {
        let targetId = event.target.dataset.id;
        let recordData = event.detail;
        let relMethodId = recordData.currentRecId;
        let relMethodName = recordData.selectName;
        let lookupNumber = 1;
        this.methodOrMeasureChange.forEach(function(data) {
            if(data.isInsert == false) {
                if(data.isMethod == true) 
                    lookupNumber = lookupNumber+1;
                else
                    lookupNumber = lookupNumber+2;
            }
            if(data.index == targetId) {
                data.relatedMethodName = relMethodName;
                data.relatedMethodId = relMethodId;
                return false;
            }
        })
        console.log("Lookup Number: "+lookupNumber);
        if(this.template.querySelectorAll('c-prt_v2momreq_custom_lookup')[lookupNumber] != null) {
            this.template.querySelectorAll('c-prt_v2momreq_custom_lookup')[lookupNumber].resetData();
        }
    }

    resetRelatedMethod(event) {
        let recordData = event.detail;
        let relMethodId = recordData.currentRecId;
        let relMethodName = recordData.selectName;
        let targetId = event.target.dataset.id;
        this.methodOrMeasureChange.forEach(function(data) {
            if(data.index == targetId) {
                data.relatedMethodId = relMethodId;
                data.relatedMethodName = relMethodName;
                data.relatedMethodClear = false;
                data.relatedMethodIcon = true;
                data.relatedMethodRead = false;
            }
        })
    }

    resetV2MOM(event) {
        let recordData = event.detail;
        this.v2momId = recordData.currentRecId;
        this.v2momName = recordData.selectName;
        this.methodOrMeasureChange.forEach(function(data) {
            data.v2momId = recordData.currentRecId;
        })
        this.methodAndTheirMeasures = [];
        console.log("V2MOM Id: "+this.v2momId);
        console.log("V2MOM Name: "+this.v2momName);
    }

    resetMethod(event) {
        let recordData = event.detail;
        let selectedMethodId = recordData.currentRecId;
        let selectedMethodName = recordData.selectName;
        let methodDesc = recordData.descOrComm;
        let targetId = event.target.dataset.id;
        this.methodOrMeasureChange.forEach(function(data) {
            if(data.index == targetId) {
                data.methodOrMeasureName = selectedMethodName;
                data.methodOrMeasureId = selectedMethodId;
                data.currentDescOrComm = methodDesc;
            }
        })
        console.log("Method Id: "+selectedMethodId);
        console.log("Method Name: "+selectedMethodName);
    }
    
    resetMeasure(event) {
        let recordData = event.detail;
        let selectedMeasureId = recordData.currentRecId;
        let selectedMeasureName = recordData.selectName;
        let measureComm = recordData.descOrComm;
        let targetId = event.target.dataset.id;
        this.methodOrMeasureChange.forEach(function(data) {
            if(data.index == targetId) {
                data.methodOrMeasureName = selectedMeasureName;
                data.methodOrMeasureId = selectedMeasureId;
                data.currentDescOrComm = measureComm;
            }
        })
        console.log("Measure Id: "+selectedMeasureId);
        console.log("Measure Name: "+selectedMeasureName);
    }

    resetPortfolio(event) {
        let recordData = event.detail;
        this.selectedPortfolioId = recordData.currentRecId;
        this.selectedPortfolioName = recordData.selectName;
        this.methodOrMeasureChange.forEach(function(data) {
            data.portfolioId = recordData.currentRecId;
        })
        console.log("Portfolio Id: "+this.selectedPortfolioId);
        console.log("Portfolio Name: "+this.selectedPortfolioName);
    }

    handleRemoveChanges() {
        this.methodOrMeasureChange.pop();
        var len = this.methodOrMeasureChange.length;
        if(len == 1) {
            this.showDeleteButton = false;
        }
    }

    handleAdditionalChanges() {
        var len = this.methodOrMeasureChange.length;
        let radioG1 = "rg1"+len;
        let radioG2 = "rg2"+len;
        let data;
        data = {
            "index": len,
            "isInsert": true,
            "isMethod": true,
            "value1": "New",
            "value2": "Method",
            "v2momId": this.v2momId,
            "portfolioId": this.selectedPortfolioId,
            "methodOrMeasureId": "",
            "methodOrMeasureName": "",
            "currentDescOrComm": "",
            "updatedDescOrComm": "",
            "detailedJustification": "",
            "targetDateOfChange": "",
            "measureTargetValue": "",
            "radioGroup1": radioG1,
            "radioGroup2": radioG2,
        };
        this.methodOrMeasureChange.push(data);
        if(len > 0) {
            this.showDeleteButton = true;
        }
    }

    handleSave() {
        let requiredFieldsFilled = true;
        console.log("Complete Data: "+JSON.stringify(this.methodOrMeasureChange));
        this.methodOrMeasureChange.forEach(function(data) {
            if(data.v2momId == "" || data.portfolioId == ""){
                requiredFieldsFilled = false;
                return false;
            }
            else if((data.isInsert == true && data.isMethod == true) || (data.isInsert == false && data.isMethod == true)) {
                if(data.methodOrMeasureName == "" || data.targetDateOfChange == "" || data.updatedDescOrComm == "" || data.detailedJustification == "") {
                    requiredFieldsFilled = false;
                    return false;
                }
            }
            else if(data.isInsert == true && data.isMethod == false) {
                if(data.relatedMethodName == "" || data.methodOrMeasureName == "" || data.targetDateOfChange == "" || 
                    data.detailedJustification == "" || data.updatedDescOrComm == "") {
                    requiredFieldsFilled = false;
                    return false;
                }
            }
            else if(data.isInsert == false && data.isMethod == false) {
                if(data.methodOrMeasureName == "" || data.targetDateOfChange == "" || 
                    data.detailedJustification == "" || data.updatedDescOrComm == "" || data.relatedMethodName =="" || data.relatedMethodId == "") {
                    requiredFieldsFilled = false;
                    return false;
                }
            }
        })
        console.log('Required Fields Value: '+requiredFieldsFilled);
        if(requiredFieldsFilled) {
            createChangeRequest({
                methodOrMeasureData: this.methodOrMeasureChange
            })
            .then(result => {
                if(result != '') {
                    const toastEvent = new ShowToastEvent({
                        title: 'SUCCESS!!',
                        message: 'Change Request Submitted Successfully',
                        variant: 'success'
                    });
                    this.dispatchEvent(toastEvent);

                    console.log('Record Id ==> '+ result);
                    this[NavigationMixin.Navigate]({
                        type:'standard__recordPage',
                        attributes:{
                            "recordId":result,
                            "objectApiName":"V2MOM_Change_Request__c",
                            "actionName": "view"
                        }
                    });
                }
            })
            
        }   
        else {
            const toastEvent = new ShowToastEvent({
                title: 'ERROR!!',
                message: 'Fill all the required fields',
                variant: 'error'
            });
            this.dispatchEvent(toastEvent);
        }     
    }

    methodDateUpdated(event) {
        var today = new Date();        
        var dd = today.getDate();
        var mm = today.getMonth() + 1;
        var yyyy = today.getFullYear(); 
        if(dd < 10){
            dd = '0' + dd;
        }     
        if(mm < 10){
            mm = '0' + mm;
        }
        
        let targetId = event.target.dataset.id;
        var todayFormattedDate = yyyy+'-'+mm+'-'+dd;
        var selectedDate = event.target.value;
        if(selectedDate != '' && selectedDate < todayFormattedDate) {
            event.target.value = '';
            const toastEvent = new ShowToastEvent({
                title: 'ERROR!!',
                message: 'Target Date of Change can be in present or future.',
                variant: 'error'
            });
            this.dispatchEvent(toastEvent);
        } 
        else {
            this.methodAndMeasureData.forEach(function(data) {
                if(data.recordId == targetId) {
                    data.targetDateOfChange = event.target.value;
                }
            })
        }
    }

    methodDescUpdated(event) {
        let targetId = event.target.dataset.id;
        this.methodAndMeasureData.forEach(function(data) {
            if(data.recordId == targetId) {
                data.descOrComment = event.target.value;
            }
        })
    }

    methodJustifyUpdated(event) {
        let targetId = event.target.dataset.id;
        this.methodAndMeasureData.forEach(function(data) {
            if(data.recordId == targetId) {
                data.detailedJustification = event.target.value;
            }
        })
    }

    measureDateUpdated(event) {
        var today = new Date();        
        var dd = today.getDate();
        var mm = today.getMonth() + 1;
        var yyyy = today.getFullYear(); 
        if(dd < 10){
            dd = '0' + dd;
        }     
        if(mm < 10){
            mm = '0' + mm;
        }
        
        let targetId = event.target.dataset.id;
        var todayFormattedDate = yyyy+'-'+mm+'-'+dd;
        var selectedDate = event.target.value;
        if(selectedDate != '' && selectedDate < todayFormattedDate) {
            event.target.value = '';
            const toastEvent = new ShowToastEvent({
                title: 'ERROR!!',
                message: 'Target Date of Change can be in present or future.',
                variant: 'error'
            });
            this.dispatchEvent(toastEvent);
        } 
        else {
            this.methodAndMeasureData.forEach(function(data) {
                if(data.recordId == targetId) {
                    data.targetDateOfChange = event.target.value;
                }
            })
        }
    }

    measureValueUpdated(event) {
        let targetId = event.target.dataset.id;
        this.methodAndMeasureData.forEach(function(data) {
            if(data.recordId == targetId) {
                data.measureTargetValue = event.target.value;
            }
        })
    }

    measureCommUpdated(event) {
        let targetId = event.target.dataset.id;
        this.methodAndMeasureData.forEach(function(data) {
            if(data.recordId == targetId) {
                data.descOrComment = event.target.value;
            }
        })
    }

    measureJustifyUpdated(event) {
        let targetId = event.target.dataset.id;
        this.methodAndMeasureData.forEach(function(data) {
            if(data.recordId == targetId) {
                data.detailedJustification = event.target.value;
            }
        })
    }

    handleUpdate() {
        let requiredFieldsFilled = true;
        console.log('Updated Data: '+JSON.stringify(this.methodAndMeasureData));
        if(this.methodAndMeasureData.length > 0) {
            this.methodAndMeasureData.forEach(function(data) {
                if(data.v2MOMId == '' || data.portfolioId == ''|| data.methodOrMeasureName == '' || data.descOrComment == ''
                    || data.detailedJustification == '' || data.targetDateOfChange == ''){
                    console.log('Hi 1');
                    requiredFieldsFilled = false;
                    return false;
                }
                else if(data.isMethod == 'false' && data.measureTargetValue == '') {
                    console.log('Hi 2');
                    requiredFieldsFilled = false;
                    return false;
                }
            })
            if(requiredFieldsFilled) {
                updateMethodAndMeasureChange({
                    methodAndMeasureData: this.methodAndMeasureData,
                    changeRequestId: this.changerequestId
                })
                .then(result => {
                    if(result != '') {
                        let msg = result+' Method And Measure Records Updated.';;
    
                        const toastEvent = new ShowToastEvent({
                            title: 'SUCCESS!!',
                            message: msg,
                            variant: 'success'
                        });
                        this.dispatchEvent(toastEvent);
                    }
                })
                
            }   
            else {
                const toastEvent = new ShowToastEvent({
                    title: 'ERROR!!',
                    message: 'Fill all the required fields',
                    variant: 'error'
                });
                this.dispatchEvent(toastEvent);
            }
        } 
    }

    handleDelete(event) {
        let targetId = event.target.dataset.id;
        this.methodAndMeasureData.forEach(function(data, index, object) {
            if(data.recordId == targetId) {
                object.splice(data, 1);
                deleteMethodOrMeasureChange({
                    recordId: data.recordId
                })
                .then(result => {
                    if(result) {
                        const toastEvent = new ShowToastEvent({
                            title: 'SUCCESS!!',
                            message: 'Record deleted',
                            variant: 'success'
                        });
                        this.dispatchEvent(toastEvent);
                    }
                })
                console.log('Deleted values: ');
            }
        })
    }
}
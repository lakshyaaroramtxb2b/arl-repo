import { LightningElement, track, api } from 'lwc';
// Import custom labels
import close from '@salesforce/label/c.ESA_Close';

export default class SldsModal extends LightningElement {
    htmlContentId = "modal-content-id-" + Math.random();
    htmlHeaderId = "modal-heading-" + Math.random();
    spinnerCmp;
    label = {
        close
    }

    @track
    isHidden = true;
    @track
    message
    @track
    messageType

    @api
    show(error, type) {
        this.isHidden = false;
        this.message = error;
        this.messageType = type;
    }

    @api
    hide() {
        this.template.querySelector("c-slds-message-toast").hide();
        this.isHidden = true;
    }

    @api
    showBusy() {
        this.spinnerCmp.showBusy();
    }

    @api
    hideBusy() {
        this.spinnerCmp.hideBusy();
    }

    renderedCallback() {
        this.spinnerCmp = this.template.querySelector("c-busy-spinner");
    }

}
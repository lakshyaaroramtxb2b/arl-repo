import { LightningElement, api } from 'lwc';

/**
 * @author Ben Harvie
 * @email ben.harvie@salesforce.com
 * @desc This LWC Component Builds a Modal for Create and Update requests on RIA
 */

export default class Karl_mapping_reqs_modal extends LightningElement {
    @api closeLabel = 'Cancel';
    @api submitLabel = 'Save';
    @api targetrecord;
    @api parentrecord;

    closeModal() {
        this.dispatchEvent(new CustomEvent('close'));
    }

    handleSubmit() {
        this.dispatchEvent(new CustomEvent('submit'));
    }
}
import { LightningElement, api } from 'lwc';

export default class Karl_car_datatable_picklist_item extends LightningElement {
    @api label;
    @api placeholder;
    @api options;
    @api value;
    @api context;
    @api field;

    containerClass = 'combobox-container picklist-container';

    handleChange(event) {
        this.containerClass = 'combobox-container combobox-container-edited picklist-container'; // set a border on changed values
        this.value = event.detail.value; // set the new value

        // Dispact an event back to the parent
        this.dispatchEvent(new CustomEvent('picklistchanged', {
            composed: true, // doesn't work if this isn't set
            bubbles: true, // doesn't work if this isn't set
            cancelable: true,
            detail: {
                data: { context: this.context, value: this.value, field: this.field }
            }
        }));
    }

}
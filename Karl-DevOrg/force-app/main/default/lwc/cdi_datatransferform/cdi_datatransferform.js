import { LightningElement,track } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import DATATRANSFER_OBJECT from '@salesforce/schema/CDI_Data_Transfer__c';
import DATACENTER_FIELD from '@salesforce/schema/CDI_Data_Transfer__c.Data_Center__c';
import DATASTOREDEPLOYEMENT_FIELD from '@salesforce/schema/CDI_Data_Transfer__c.Data_Store_Deployment__c';
import CRYPTO_FIELD from '@salesforce/schema/CDI_Data_Transfer__c.Crypto_Algorithm__c';





/**
 * Creates Account records.
 */
export default class Datatransferform extends NavigationMixin(LightningElement) {
    datatransferObject = DATATRANSFER_OBJECT;
    
    datacenterField = DATACENTER_FIELD;
    datastoredeploymentField=DATASTOREDEPLOYEMENT_FIELD; 
    cryptoField=CRYPTO_FIELD;
  
    


    @track bShowModal = false;
 
    /* javaScipt functions start */ 
    openModal(event) {    
        // to open modal window set 'bShowModal' tarck value as true
        this.bShowModal = true;
        const inputFields = this.template.querySelectorAll(
            'lightning-input-field'
        );
        if (inputFields) {
            inputFields.forEach(field => {
                field.reset();
            });
        }
        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                recordId: event.detail.id,
                objectApiName: 'CDI_Data_Transfer__c',
                actionName: 'view',
            },
        });
    }
 
    closeModal() {    
        // to close modal window set 'bShowModal' tarck value as false
        this.bShowModal = false;
        
    }
    /* javaScipt functions end */ 
}
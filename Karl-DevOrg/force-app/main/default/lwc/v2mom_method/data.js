export default {
    methods: [{
        methodName: "Methods (Some text over here)",
        description: "",
        label1: "value1",
        label2: "value2",
        label3: "value3",
        label4: "value4",
        projectList: [{
                name: "Project 1",
                code: "#123",
                startDate: "01/12/2019",
                endDate: "01/12/2019",
                status: "Cancelled"
            },
            {
                name: "Project 2",
                code: "#455",
                startDate: "11/09/2019",
                endDate: "21/12/2019",
                status: "Completed"
            },
            {
                name: "Project 3",
                code: "#76543",
                startDate: "31/12/2019",
                endDate: "31/12/2019",
                status: "In progress"
            },
        ],
        v2momContacts: [{
            name: "Virendra Yadav",
            v2mom: "Lorem ipsum doler sit"
        }],
        measures: [
            // looping new measures
            {
                measureName: "Measures 1",
                description: "",
                label1: "value1",
                label2: "value2",
                label3: "value3",
                label4: "value4",
                projectList: [{
                        name: "Project 1",
                        code: "#123",
                        startDate: "01/12/2019",
                        endDate: "01/12/2019",
                        status: "Cancelled"
                    },
                    {
                        name: "Project 2",
                        code: "#455",
                        startDate: "11/09/2019",
                        endDate: "21/12/2019",
                        status: "Completed"
                    },
                    {
                        name: "Project 3",
                        code: "#76543",
                        startDate: "31/12/2019",
                        endDate: "31/12/2019",
                        status: "In progress"
                    },
                ],
                v2momContacts: [{
                    name: "Virendra Yadav",
                    v2mom: "Lorem ipsum doler sit"
                }]
            },
            {
                measureName: "Measures 1",
                description: "",
                label1: "value1",
                label2: "value2",
                label3: "value3",
                label4: "value4",
                projectList: [{
                        name: "Project 1",
                        code: "#123",
                        startDate: "01/12/2019",
                        endDate: "01/12/2019",
                        status: "Cancelled"
                    },
                    {
                        name: "Project 2",
                        code: "#455",
                        startDate: "11/09/2019",
                        endDate: "21/12/2019",
                        status: "Completed"
                    },
                    {
                        name: "Project 3",
                        code: "#76543",
                        startDate: "31/12/2019",
                        endDate: "31/12/2019",
                        status: "In progress"
                    },
                ],
                v2momContacts: [{
                    name: "Virendra Yadav",
                    v2mom: "Lorem ipsum doler sit"
                }]
            },
            {
                measureName: "Measures 1",
                description: "",
                label1: "value1",
                label2: "value2",
                label3: "value3",
                label4: "value4",
                projectList: [{
                        name: "Project 1",
                        code: "#123",
                        startDate: "01/12/2019",
                        endDate: "01/12/2019",
                        status: "Cancelled"
                    },
                    {
                        name: "Project 2",
                        code: "#455",
                        startDate: "11/09/2019",
                        endDate: "21/12/2019",
                        status: "Completed"
                    },
                    {
                        name: "Project 3",
                        code: "#76543",
                        startDate: "31/12/2019",
                        endDate: "31/12/2019",
                        status: "In progress"
                    },
                ],
                v2momContacts: [{
                    name: "Virendra Yadav",
                    v2mom: "Lorem ipsum doler sit"
                }]
            }
        ]
    }, ]
};
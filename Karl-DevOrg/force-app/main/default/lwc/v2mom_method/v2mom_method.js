/**
 * @File Name          : v2mom_method.js
 * @Description        : 
 * @Author             : virendra.yadav
 * @Group              : 
 * @Last Modified By   : virendra.yadav
 * @Last Modified On   : 1/3/2020, 1:33:36 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    1/3/2020   virendra.yadav      Initial Version
 **/
import { LightningElement, track, api, wire } from 'lwc';
import fetchV2MOMMethods from '@salesforce/apex/PRT_V2MOMHealthViewController.fetchV2MOMMethods';

export default class v2mom_method extends LightningElement {
    @track methods = [];
    @api userId = "";
    @track apiIsCalling = true;

    @track methodContactHeaders = [{
            label: "Contact Name",
            isSort: false,
            isAscending: false,
            isDescending: false,
            key: "name",
        },
        {
            label: "V2MOM",
            isSort: false,
            isAscending: false,
            isDescending: false,
            key: "v2mom",
        }
    ];
    @track methodProjectHeaders = [{
            label: "Name",
            isSort: false,
            isAscending: false,
            isDescending: false,
            key: "name",
        },
        {
            label: "Start Date",
            isSort: false,
            isAscending: false,
            isDescending: false,
            key: "startDate",
        },
        {
            label: "End Date",
            isSort: false,
            isAscending: false,
            isDescending: false,
            key: "endDate",
        },
        {
            label: "Status",
            isSort: false,
            isAscending: false,
            isDescending: false,
            key: "status",
        },
        {
            label: "Overall Project Health",
            isSort: false,
            isAscending: false,
            isDescending: false,
            key: "overallStatus",
        }
    ];
    @wire(fetchV2MOMMethods, { userId: '$userId' })
    wirefetchV2MOMMethods({ error, data }) {
        if (data) {
            this.apiIsCalling = false;
            this.methods = JSON.parse(JSON.stringify(data));
            for (let i = 0; i < this.methods.length; i++) {
                this.methods[i].projectHeaders = this.methodProjectHeaders;
                // set no project check
                if (this.methods[i].projectList && this.methods[i].projectList.length !== 0) {
                    this.methods[i].notProject = false;
                    this.methods[i] = this.setClassNameForMethodAndMeasure(this.methods[i]);
                } else {
                    this.methods[i].notProject = true;
                }
                // set no project check
                this.methods[i].noContact = true;

                for (let j = 0; j < this.methods[i].measures.length; j++) {
                    this.methods[i].measures[j].projectHeaders = JSON.parse(JSON.stringify(this.methodProjectHeaders));
                    this.methods[i].measures[j].contactHeaders = JSON.parse(JSON.stringify(this.methodProjectHeaders));
                    // set no project check
                    if (this.methods[i].measures[j].projectList && this.methods[i].measures[j].projectList.length !== 0) {
                        this.methods[i].measures[j].notProject = false;
                        this.setClassNameForMethodAndMeasure(this.methods[i].measures[j]);
                    } else {
                        this.methods[i].measures[j].notProject = true;
                    }
                    // set no contact check
                    if (this.methods[i].measures[j].v2momContacts && this.methods[i].measures[j].v2momContacts.length !== 0) {
                        this.methods[i].measures[j].noContact = false;
                        this.methods[i].v2momContacts = this.methods[i].v2momContacts.concat(this.methods[i].measures[j].v2momContacts);
                        this.methods[i].noContact = false;
                    } else {
                        this.methods[i].measures[j].noContact = true;
                    }
                }
            }
        }
    }

    /**
     * @param event from the onclick
     * @name openMethod
     * @description Open the respective method accordion
     * @author Virendra Yadav
     */
    openMethod(event) {
        if (this.methods[event.target.dataset.index].isOpen) {
            this.methods[event.target.dataset.index].isOpen = false;
        } else {
            this.methods[event.target.dataset.index].isOpen = true;
        }
    }

    /**
     * @param event from the onclick
     * @name openMeasure
     * @description Open the respective measure accordion
     * @author Virendra Yadav
     */
    openMeasure(event) {
        var indexObj = event.target.dataset;
        if (this.methods[indexObj.methodIndex].measures[indexObj.measureIndex].isOpen) {
            this.methods[indexObj.methodIndex].measures[indexObj.measureIndex].isOpen = false;
        } else {
            this.methods[indexObj.methodIndex].measures[indexObj.measureIndex].isOpen = true;
        }
    }

    /**
     * @param event from the onclick
     * @name openContactFields
     * @description Open the respective contact accordion
     * @author Virendra Yadav
     */
    openContactFields(event) {
        var indexObj = event.target.dataset;
        if (indexObj.measureIndex) {
            if (this.methods[indexObj.methodIndex].measures[indexObj.measureIndex].v2momContacts[indexObj.contactIndex].isOpen) {
                this.methods[indexObj.methodIndex].measures[indexObj.measureIndex].v2momContacts[indexObj.contactIndex].isOpen = false;
            } else {
                this.methods[indexObj.methodIndex].measures[indexObj.measureIndex].v2momContacts[indexObj.contactIndex].isOpen = true;
            }
        } else {
            if (this.methods[indexObj.methodIndex].v2momContacts[indexObj.contactIndex].isOpen) {
                this.methods[indexObj.methodIndex].v2momContacts[indexObj.contactIndex].isOpen = false;
            } else {
                this.methods[indexObj.methodIndex].v2momContacts[indexObj.contactIndex].isOpen = true;
            }
        }
    }

    /**
     * @param event from the onclick
     * @name openMeasure
     * @description Open the respective measure accordion
     * @author Virendra Yadav
     */
    sortMethodProjects(event) {
        var indexObj = event.target.dataset;
        var projectHeaders = this.methods[indexObj.methodIndex].projectHeaders;
        var sortType = "";
        sortType = this.getSortTypeAndHeaders(projectHeaders, indexObj, sortType);
        this.methods[indexObj.methodIndex].projectHeaders = projectHeaders;
        this.methods[indexObj.methodIndex].projectList = this.sortArrays(sortType, indexObj.key, this.methods[indexObj.methodIndex].projectList);
    }

    getSortTypeAndHeaders(projectHeaders, indexObj, sortType) {
        for (let i = 0; i < projectHeaders.length; i++) {
            let header = projectHeaders[i];
            if (header.key === indexObj.key) {
                header.isSort = true;
                if (!header.isAscending && !header.isDescending) {
                    sortType = 'ascending';
                    header.isAscending = true;
                } else {
                    header.isAscending = !header.isAscending;
                    header.isDescending = !header.isDescending;
                    sortType = header.isAscending ? 'ascending' : 'decsending';
                }
            } else {
                header.isSort = false;
                header.isAscending = false;
                header.isDescending = false;
            }
            projectHeaders[i] = header;
        }
        return sortType;
    }

    /**
     * @param event from the onclick
     * @name openMeasure
     * @description Open the respective measure accordion
     * @author Virendra Yadav
     */
    sortMeasureProjects(event) {
        var indexObj = event.target.dataset;
        var projectHeaders = this.methods[indexObj.methodIndex].measures[indexObj.measureIndex].projectHeaders;
        var sortType = "";
        sortType = this.getSortTypeAndHeaders(projectHeaders, indexObj, sortType);
        this.methods[indexObj.methodIndex].measures[indexObj.measureIndex].projectHeaders = projectHeaders;
        this.methods[indexObj.methodIndex].measures[indexObj.measureIndex].projectList = this.sortArrays(sortType, indexObj.key, this.methods[indexObj.methodIndex].measures[indexObj.measureIndex].projectList);
    }

    /**
     * @param event from the onclick
     * @name openMeasure
     * @description Open the respective measure accordion
     * @author Virendra Yadav
     */
    sortArrays(type, key, array) {
        var returnValue = type === 'ascending' ? 1 : -1;
        return array.sort((a, b) => {
            if (a[key] > b[key]) {
                return returnValue;
            }
            if (b[key] > a[key]) {
                return -returnValue;
            }
            return 0;
        });
    }


    openV2mom(event) {
        window.open('/lightning/r/V2MOM_c__x/' + event.target.dataset.v2mom + '/view');
    }

    setClassNameForMethodAndMeasure(method) {
        let countObj = {
            Blocked: 0,
            'On-Track': 0,
            'At-Risk': 0
        }
        for (let pI = 0; pI < method.projectList.length; pI++) {
            const element = method.projectList[pI];
            countObj[element.className]++;
        }

        console.log("++++++++++++++++++++++++++++++++++");
        console.log(JSON.stringify(method));
        console.log(JSON.stringify(countObj));

        console.log("++++++++++++++++++++++++++++++++++");

        switch (true) {
            case countObj.Blocked != 0:
                method.className = 'Blocked';
                break;
            case countObj['At-Risk'] != 0:
                method.className = 'At-Risk';
                break;
            case countObj['On-Track'] != 0:
                method.className = 'On-Track';
                break;
            default:
                break;
        }
        return method;
    }

    get isShowMethods() {
        return this.methods.length !== 0;
    }
}
import { LightningElement, api } from 'lwc';

export default class karl_datatable_picklist_item extends LightningElement {
    @api label;
    @api placeholder;
    @api options;
    @api value;
    @api context;
    @api field;
    @api type;
    @api selected;

    containerClass = 'combobox-container picklist-container';

    get picklistName(){
        return "karlpicklist_" + this.context;
    }

    connectedCallback(){
        // On load, check the selected values to determine if any picklists should be highlighted
        // ! TODO: Performance of this is likely questionable 
        for(let item of this.selected){
            if(item == ('karlpicklist_' + this.context)){
                this.containerClass = 'combobox-container combobox-container-edited picklist-container';
            }
        }
    }

    handleChange(event) {
        this.containerClass = 'combobox-container combobox-container-edited picklist-container'; // set a border on changed values
        this.value = event.detail.value; // set the new value

        // Dispact an event back to the parent
        this.dispatchEvent(new CustomEvent('picklistchanged', {
            composed: true, // doesn't work if this isn't set
            bubbles: true, // doesn't work if this isn't set
            cancelable: true,
            detail: {
                data: { context: this.context, value: this.value, field: this.field }
            }
        }));
    }

}
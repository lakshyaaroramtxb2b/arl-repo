import {
    LightningElement, 
    api, 
    wire, 
    track
} from 'lwc';
import {
    refreshApex
} from '@salesforce/apex';
import {
    ShowToastEvent
} from 'lightning/platformShowToastEvent';
import {
    CurrentPageReference,
    NavigationMixin
} from 'lightning/navigation';
import Id from '@salesforce/user/Id';


// Import Apex Classes
import getAuditCycles from '@salesforce/apex/KARL_Utility.getAuditCycles';
import getAuditCycleScopes from '@salesforce/apex/KARL_Utility.getAuditCycleScopes';
import getCycleAuditReport from '@salesforce/apex/KARL_CycleAuditReportCtrl.getCycleAuditReport';

export default class Karl_cycle_audit_report_datatable extends NavigationMixin(LightningElement) {
    @api recordId;
    userId = Id;

    // Metadata variables
    @api auditorsView = false;
    @api defaultCycle;
    @api defaultReportStatusFilter = 'all';
    @api defaultReportFilter = 'allReports';
    @api defaultScope;
    @api defaultSortBy;
    @api defaultSortDirection;

    // Wire Data Output
    @track auditCycles = [];
    @track auditScopes = [];
    @track filteredData = [];
    @track wireData; // Full result (for refreshApex)

    // Wire Data Input / Management
    @track auditcycle;
    @track auditscope;
    @track totalRows;
	@track error;
    @track records;

    // Sorting variables
    @track sortDirection;
    @track sortBy;

    // Miscellaneous
    @track isLoading;
    @track togglecontrols = false;
    @track paramsSet = false;
    @track sfdcEdits = false;
    @track auditorEdits = false;
    @track saveButton = false;
    @track draftValues = [];

    // Button Filters
    @track reportStatusFilter = [];
    @track reportFilter = [];
    currentPageReference;

    statusButtonValues = ['all','pending','readyToSign','inProgress','docusignComplete','auditorsFinalizing','finalReview','published'];
    reportButtonValues = ['allReports','myReports'];

    buttonState = {
                    'all': false,
                    'pending':false,
                    'readyToSign': false,
                    'inProgress': false,
                    'docusignComplete': false,
                    'auditorsFinalizing': false,
                    'finalReview': false,
                    'published':false
                   };

    reportButtonsState = {
                    'allReports': false,
                    'myReports':false
                   };

    @wire(getAuditCycles, {})  wiredAuditCycles(auditCyclesResult) {
        this.auditCycles = auditCyclesResult.data;
    }

    // Supporting metadata: Audit Scopes (picklist)
    @wire(getAuditCycleScopes, {})  wiredAuditCycleScopes(auditScopesResult) {
        this.auditScopes = auditScopesResult.data;        
    }


    @wire(CurrentPageReference)
    setCurrentPageReference(currentPageReference) {
        this.currentPageReference = currentPageReference;
        // Loading URL params (if not defined; return defaults from metadata)
        if(this.connected){
            this.setFilters();
        }else{
            this.connectedQueue = true;
        }
    }


    @wire(getCycleAuditReport, { auditCycleId: '$auditcycle',reportStatusFilter: '$reportStatusFilter', reportAssigneeFilter: '$reportFilter'})  wiredARL(result) {
        this.wireData = result; // Save the full result, for diffing refreshApex later
        console.log(JSON.stringify(this.reportStatusFilter));
        if ( result.data && result.data.length ){
            // ! Check if the wire returns any data
            try {
                this.paramsSet = true;
                this.records = true; // Specify boolean for if:true
                this.filteredData = result.data; // Initially unfiltered is filteredData.
                this.isLoading = false;
                this.error = false;
                this.sortData(this.sortBy, this.sortDirection);
                this.totalRows = result.data.length;
            } catch(e) {
                console.log('KARL >> LWC Error catch', e.message || e.body.message);
                this.isLoading = false;
            }
        } else if ( result.data && !result.data.length ){
            // ! Check if the wire is null
            //console.log('KARL >> LWC Empty', result.data);
            this.paramsSet = true;
            this.records = false; // specify boolean for if:false
            this.isLoading = false;
            this.totalRows = result.data.length;
        } else if (result.error){
            // ! Check if the wire returns an error, and return the error
            if(!this.auditcycle){
                // False error --> just waiting on parameters
                //console.log('KARL >> LWC Parameters not set');
                this.paramsSet = false;
                this.records = false; // specify boolean for if:false
                this.isLoading = false;
            }
            else{
                // Legitimate error
                console.log('KARL >> LWC Error legitimate', result.error);
                this.error = result.error;
                this.paramsSet = true;
                this.isLoading = false;
                this.records = false;
            }
        }
    }

    connectedCallback() {
        //this.reportStatusFilter.push('all');
        // Load the columns of the datatable
        this.loadColumns();
        this.connected = true;

        if(this.connectedQueue){
           this.setFilters();
        }
    }

    renderedCallback(){
        this.setButtonSelection(this.statusButtonValues, this.reportStatusFilter);
        this.setButtonSelection(this.reportButtonValues, this.reportFilter);
    }

    setButtonSelection(buttonSet, selectedValues){
        for(var i=0;i<buttonSet.length;i++){
            if(selectedValues.includes(buttonSet[i])){
                this.makeSelectedButton(buttonSet[i]);
            }
        }
    }

    /** Default Filters */
    setFilters(){
        this.auditcycle         = this.getDefaultURLValue('c__cycleId', this.defaultCycle);
        this.auditscope         = this.getDefaultURLValue('c__scopeId', this.defaultScope);
        this.sortBy             = this.getDefaultURLValue('c__sortBy', this.defaultSortBy);
        this.sortDirection      = this.getDefaultURLValue('c__sortDirection', this.defaultSortDirection);
        this.reportStatusFilter = this.getDefaultURLValue('c__reportStatusFilter', this.defaultReportStatusFilter);
        this.reportFilter       = this.getDefaultURLValue('c__reportFilter', this.defaultReportFilter);
    }

    getDefaultURLValue(getParam, defaultValue){
        // Use the currentPageReference to get the requested
        const urlParam = this.currentPageReference.state[getParam];

        if((urlParam != null || urlParam != undefined)){
            if (urlParam.indexOf(',') > -1) { 
                return urlParam.split(',');
            }else{
                return urlParam;
            }
        }else{
            // Nothing passed via the URL, so we'll set default value from metadata (if exists)
            return defaultValue;
        }

    }

    loadColumns() {
        this.columns = [];

        this.columns.push({
            label: 'Report Name', 
            fieldName: 'reportRecordLink', 
            type: 'url',
            editable: false,
            sortable: true, 
            hideDefaultActions: true,
            wrapText: true,
            initialWidth: 400,
            typeAttributes: {
                label: {fieldName: 'reportName'},
                target: '_blank'
            }
        });
       
        /*this.columns.push({
            label: 'Scope', 
            fieldName: 'scope',
            sortable: true,
            wrapText: true
        });*/

        /*this.columns.push({
            label: 'Area of Compliance', 
            fieldName: 'area',
            sortable: true,
            wrapText: true
        });*/

        this.columns.push({
            label: 'GRC LT', 
            fieldName: 'contactGrcLt',
            sortable: true,
            wrapText: true
        });

        this.columns.push({
            label: 'BU Lead', 
            fieldName: 'contactBuLead',
            sortable: true,
            wrapText: true
        });

        this.columns.push({
            label: 'Qualified?', 
            fieldName: 'qualifiedReport',
            sortable: true,
            wrapText: true,
            type: 'label',
            initialWidth: 110,
            typeAttributes: {
                labelclass: { fieldName:'qualifiedLabel' },
                value: { fieldName: 'qualifiedReport' },
            },
            cellAttributes: { alignment: 'center' }
        });

        this.columns.push({
            label: 'Status', 
            fieldName: 'reportStatus',
            sortable: true,
            wrapText: true,
            type: 'label',
            initialWidth: 180,
            typeAttributes: {
                labelclass: { fieldName:'reportLabel' },
                value: { fieldName: 'reportStatus' },
            }
        });

        /*this.columns.push({
            label: 'Audit Cycle', 
            fieldName: 'auditCycleLink', 
            type: 'url',
            editable: false,
            wrapText: true,
            sortable: true, 
            hideDefaultActions: true,
            typeAttributes: {
                label: {fieldName: 'auditCycleName'},
                target: '_blank'
            }
        });*/

        this.columns.push({
            label: 'Target Date', 
            fieldName: 'targetDate', 
            type: 'date-local',
            initialWidth: 120,
            editable: false,
            sortable: true,
            wrapText: true
        });

        this.columns.push({
            label: 'Management Responses Approved', 
            fieldName: 'responseApprovedText', 
            editable: false,
            wrapText: true,
            sortable: true,
            cellAttributes: {
                iconName: { fieldName: 'responseApprovedIcon' },
                iconAlternativeText: 'Management Responses Status'
            }
        });

        this.columns.push({
            label: 'Auditor Readiness Confirmation', 
            fieldName: 'readinessConfirmationText', 
            editable: false,
            sortable: true,
            cellAttributes: {
                iconName: { fieldName: 'readinessConfirmationIcon' },
                iconAlternativeText: 'Auditor Readiness Status'
            }
        });

        this.columns.push({
            label: 'Audit Report Final Draft Confirmed', 
            fieldName: 'finalDraftText', 
            editable: false,
            sortable: true,
            cellAttributes: {
                iconName: { fieldName: 'finalDraftIcon' },
                iconAlternativeText: 'Report Draft Status'
            }
        });

        /*this.columns.push({
            label: 'Approved by Legal', 
            fieldName: 'approvedByLegal', 
            type: 'checkbox',
            editable: false,
            sortable: true
        });*/
    }

    setLoadingSpinner(){
        if(this.reportStatusFilter != undefined && this.auditcycle != undefined){
            this.isLoading = true;
        }else{
            this.isLoading = false;
        }
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Handle searches from the inputs on the main component page
     * @param event - js event handle
     */
    handleSearch(event){
        const rows = JSON.parse(JSON.stringify(this.filteredData)); // raw unfiltered data to start with
        const searchString = event.target.value;
        const searchField = event.target.name;
        var regex = new RegExp(searchString.replace('/[|\\{}()[\]^$+*?.]/g', '\\$&'),'i'); // previously tried gi - i much more reliable
        let filteredRows = rows;

        if(searchString !== ''){
            // TODO: More tech debt here than I would like with all the if/else switches. Couldn't get it working.
            if( searchField === 'searchReport' ){
                filteredRows = rows.filter(
                    row => regex.test(
                        row.reportName
                    )
                );
            }  else {
                // No search configured.. nothing to do.
            }
        }else{
            // Reset the fields back to original filter
            filteredRows = JSON.parse(JSON.stringify(this.wireData.data));
        }
        // Update the filteredData to respect the filteredRows.
        this.filteredData = filteredRows;
        this.refreshDraftValues();
    }


    refreshDraftValues(){
        this.draftValues.forEach(item => {
            this.updateDataValues(item);
        });
    }

    updateDataValues(updateItem) {
        let changeData = [... JSON.parse(JSON.stringify(this.filteredData))];
        changeData.forEach(item => {
            if (item.Id === updateItem.Id) {
                for (let field in updateItem) {
                  item[field] = updateItem[field];
                }
            }
        });

        this.filteredData = [...changeData];
    }

    handleRefresh(event){
            const searchFields = ['searchReport'];
            const onchangeEvent = new CustomEvent('change',{bubbles: true});
            if(JSON.stringify(this.wireData
                ) != '{}'){
                refreshApex(this.wireData).then(() => {
                    // Re-apply client-side search filters
                    for(let i = 0; i < searchFields.length; i++){
                        this.template.querySelector('lightning-input[data-field="' + searchFields[i] + '"]').value != '' ? this.template.querySelector('lightning-input[data-field="' + searchFields[i] + '"]').dispatchEvent(onchangeEvent) : null; 
                    }
                });
            }
    }

    handleAuditCycleChange(event) {
        // TODO: Need to also reset the Header Actions selection
        this.setLoadingSpinner();    
        this.auditcycle = event.detail.value;
        this[NavigationMixin.Navigate](this.updatedPageReference, true);
    }

    /**
     * @author Ben Harvie
     * @description get method to retrieve list of audit scopes into the combobox
     */
     get auditScopeOptions(){
        var auditScopeOptions = [];

        if(this.auditScopes){   
            for(var scope in this.auditScopes){
                auditScopeOptions.push({label:scope, value:this.auditScopes[scope]});
            }
        }
        return auditScopeOptions;
    }

    /**
     * @author Ben Harvie
     * @description event handler for handling changes to Audit Scope combobox
     * @param event - js event handle
     */
    handleAuditScopeChange(event) {
        this.setLoadingSpinner();
        this.auditscope = event.detail.value;
        this[NavigationMixin.Navigate](this.updatedPageReference, true);
    }

    handleRowAction(event){
        if(event.detail.action.name==='openRequest') {
            this[NavigationMixin.GenerateUrl]({
                type: 'standard__recordPage',
                attributes: {
                    recordId: event.detail.row.Id,
                    actionName: 'view'
                }
            }).then(url => {
                //console.log('Target URL >> ' + url); // Log the target URL in the console
                window.open(url, '_blank'); // open in a new tab/window
            });
        }
    }

    handleHeaderAction (event) {
        // Retrieves the name of the selected filter
        const actionName = event.detail.action.name;
        // Retrieves the current column definition
        const colName = event.detail.columnDefinition.fieldName;
        const filterColumns = this.columns;
        const activeFilter = this.activeFilter;

        // Exclude events that are for wrapText and clipText (default Salesforce header actions)
        if (actionName !== activeFilter && actionName != 'wrapText' && actionName != 'clipText') {
            filterColumns.map((col) => {
                if(col.fieldName === colName){
                    var actions = col.actions;

                    actions.forEach((action) => {
                        action.checked = action.name === actionName;
                    });
                }
            });
            this.activeFilter = actionName;
            this.updateDataTable(colName);
            this.columns = filterColumns;

            // Update the actions list to show which action is currently selected
            filterColumns.find(col => col.fieldName === colName).actions.forEach(action => action.checked = action.name === actionName);
            this.columns = [...filterColumns];
        }
    }

    doSorting(event){
        this.sortBy = event.detail.fieldName;
        this.sortDirection = event.detail.sortDirection;
        this.sortData(this.sortBy, this.sortDirection);
        this[NavigationMixin.Navigate](this.updatedPageReference, true);
    }

    sortData(fieldname, direction){
        let parseData = JSON.parse(JSON.stringify(this.filteredData));

        if(fieldname == 'reportRecordLink'){
            fieldname = 'reportName'; // workaround to filter on field instead of URL
        }

        let keyValue = (a) => {
            return a[fieldname];
        }

        let isReverse = direction ==='asc'?1:-1;
        
        parseData.sort((x, y) => {
            x = keyValue(x) ? keyValue(x) : ''; // handling null values
            y = keyValue(y) ? keyValue(y) : '';
            // sorting values based on direction
            return isReverse * ((x > y) - (y > x));
        });
        this.filteredData = parseData;
        //this.refreshDraftValues();
    }

    handleStatusChange(event){
        this.setLoadingSpinner();
        this.makeButtonChange(event, this.buttonState, this.statusButtonValues, this.defaultReportStatusFilter, this.reportStatusFilter);
        this.reportStatusFilterValue();
        this[NavigationMixin.Navigate](this.updatedPageReference, true);
    }

    handleMyreports(event){
        this.setLoadingSpinner();
        this.makeButtonChange(event, this.reportButtonsState, this.reportButtonValues, this.defaultReportFilter, this.reportFilter);
        this.reportMyFilterValue();
        this[NavigationMixin.Navigate](this.updatedPageReference, true);
    }

    get auditCycleOptions(){
        var auditCycleOptions = [];

        if(this.auditCycles){
            this.auditCycles.forEach(field => {
                auditCycleOptions.push({label:field.Name, value:field.Id});
            });
        }
        return auditCycleOptions;
    }

    /** Filter update for status toggles */
    reportStatusFilterValue(){
        this.reportStatusFilter = [];
        for(var i=0;i<this.statusButtonValues.length;i++){
            if(this.buttonState[this.statusButtonValues[i]]){
               this.reportStatusFilter.push(this.statusButtonValues[i]);
            }
        }
        if(this.reportStatusFilter.length == 0){
            this.reportStatusFilter.push('all');
        }
    }

    /** Filter update for report toggles */
    reportMyFilterValue(){
        this.reportFilter = [];
        for(var i=0;i<this.reportButtonValues.length;i++){
            if(this.reportButtonsState[this.reportButtonValues[i]]){
               this.reportFilter.push(this.reportButtonValues[i]);
            }
        }
        if(this.reportFilter.length == 0){
            this.reportFilter.push('allReports');
        }
    }

    makeButtonChange(event, buttonSet, buttonSetValues, defaultButtonValue, filterSet){
        if(event.target.value == defaultButtonValue){
            if(!buttonSet[event.target.value]){
                this.makeSelectedButton(event.target.value);
                for(var i=1;i<buttonSetValues.length;i++){
                    if(buttonSet[buttonSetValues[i]]){
                        this.makeUnselectedButton(buttonSetValues[i]);
                    }
                }
            }
            else{
                for(var i=0;i<buttonSetValues.length;i++){
                    if(buttonSet[buttonSetValues[i]]){
                        this.makeUnselectedButton(buttonSetValues[i]);
                    }
                }
            }
        }
        else if(buttonSet[event.target.value]){
            this.makeUnselectedButton(event.target.value);
        }
        else{
            this.makeSelectedButton(event.target.value);

            if(filterSet.includes(defaultButtonValue) || filterSet == defaultButtonValue){
                this.makeUnselectedButton(defaultButtonValue);
            }
        }
    }

    makeSelectedButton(targetValue){
        if(this.statusButtonValues.includes(targetValue)){
            this.buttonState[targetValue] = true;
        }else if(this.reportButtonValues.includes(targetValue)){
            this.reportButtonsState[targetValue] = true;
        }
        if(this.template.querySelector('button[value="' + targetValue + '"]').classList.contains('slds-button_neutral'))
        this.template.querySelector('button[value="' + targetValue + '"]').classList.remove('slds-button_neutral'); 
        if(!this.template.querySelector('button[value="' + targetValue + '"]').classList.contains('slds-button_brand'))
        this.template.querySelector('button[value="' + targetValue + '"]').classList.add('slds-button_brand');
    }

    makeUnselectedButton(targetValue){
        if(this.statusButtonValues.includes(targetValue)){
            this.buttonState[targetValue] = false;
        }else if(this.reportButtonValues.includes(targetValue)){
            this.reportButtonsState[targetValue] = false;
        }
        console.log('Remove ' + targetValue);
        if(this.template.querySelector('button[value="' + targetValue + '"]').classList.contains('slds-button_brand'))
        this.template.querySelector('button[value="' + targetValue + '"]').classList.remove('slds-button_brand'); 
        if(!this.template.querySelector('button[value="' + targetValue + '"]').classList.contains('slds-button_neutral'))
        this.template.querySelector('button[value="' + targetValue + '"]').classList.add('slds-button_neutral');
    }


    get updatedPageReference() {
        return this.getUpdatedPageReference({
            c__cycleId: this.auditcycle,
            c__scopeId: this.auditscope,
            c__reportStatusFilter: this.reportStatusFilter,
            c__reportFilter: this.reportFilter,
            c__sortBy: this.sortBy,
            c__sortDirection: this.sortDirection
        });
    }

    getUpdatedPageReference(stateChanges) {
        return Object.assign({}, this.currentPageReference, {
            state: Object.assign({}, this.currentPageReference.state, stateChanges)
        });
    }
}
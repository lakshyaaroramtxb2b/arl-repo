import { LightningElement, api} from 'lwc';

export default class SldsInputTextarea extends LightningElement {
    @api
    label;
    @api
    value;
    @api
    placeholder;
    @api
    maxLength = 255;

    @api
    updateValue(event) {
        this.value = event.target.value;
        // Creates the event with value.
        const changeEvent = new CustomEvent('change', {target : this});
        this.dispatchEvent(changeEvent);
    }

}
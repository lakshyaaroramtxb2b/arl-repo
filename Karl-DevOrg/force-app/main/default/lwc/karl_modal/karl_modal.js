import {
    LightningElement,
    api
} from 'lwc';

export default class Karl_modal extends LightningElement {
    @api confirmationMessage = '';
    @api isDisabled = false;
    @api noLabel = 'No';
    @api yesLabel = 'Yes'

    closeModal() {
        this.dispatchEvent(new CustomEvent('close'));
    }

    handleYes() {
        this.dispatchEvent(new CustomEvent('yes'));
    }
}
/**
 * @author Ben Harvie <ben.harvie@salesforce.com>
 * @description This LWC Component Builds a in-line editable datatable. Note that this is a unique
 *              LWC for the KARL app and is not reusable for other purposes.
 */

// Import Salesforce LWC Components
import {
    refreshApex
} from '@salesforce/apex';
// Import Apex Classes
import getReport from '@salesforce/apex/KARL_RcmReportController.getReport';
// Import Utility Components
import {
    arrayRemove, getDefaultURLValue, scopeOptions, controlFamilyOptions, setLoadingSpinner
} from 'c/karlUtil';
import {
    CurrentPageReference, NavigationMixin
} from 'lightning/navigation';
import {
    ShowToastEvent
} from 'lightning/platformShowToastEvent';
import {
    api, LightningElement, track, wire
} from 'lwc';

// Start Class Definition
export default class Karl_rcm_datatable extends NavigationMixin(LightningElement) {
    // Standard Variables
    @api recordId;
    @track isLoading;
    currentPageReference;

    // Metadata variables
    @api hideScopeCol;
    @api hideControlsCol;
    @api hideActionsCol;
    @api defaultScope;
    @api defaultSortBy;
    @api defaultSortDirection;

    // Wire Data Output
    @track filteredData = [];
    @track wireData; // Full result (for refreshApex)

    // Wire Data Input / Management
    @track auditScope;
    @track totalRows;
	@track error;
    @track records;

    // Sorting variables
    @track sortDirection;
    @track sortBy;

    // Value Sets
    @track auditScopeOptions    = [];
    @track controlFamilyOptions = [];
    // Pagination
    @track page             = 1;
    @track pageDefault      = 1;
    @track startingRecord   = 1;
    @track rowOffset        = 0;
    @track endingRecord     = 0;
    @track pageSize         = 50;
    @track pageSizeDefault  = 50;
    @track pageSizeOptions  = [
                                    { label: '25', value: '25' },
                                    { label: '50', value: '50' },
                                    { label: '100', value: '100' },
                                    { label: '200', value: '200' },
                                    { label: '200+', value: '999' }
                                ];
    @track totalRows        = 0;
    @track totalPages       = 0;
    @track pageLinks        = [];

    // Timer vars used for search waiting times
    typingWait      = 300;
    typingTimer;
    
    // Miscellaneous
    @track paramsSet = false;
    @track searching = false;
    @track searchRows;
    @track searchStartingRow;
    // Define the activated search fields
    searchFields            = ['searchname','searchcontrol','searchdesc' ];

    // Define search objects
    searchObj = {
        searchControl: 'controlId',
        searchName: 'controlName',
        searchDesc: 'ctrlDescription',
    }

    // Control Family Options
    @track toggleFamily           = '';
    toggleFamilyDefault           = 'allFamilies';

    // Active Button Toggles
    @track toggleActive         = [];
    toggleActiveDefault         = 'activeReq';
    toggleActiveValues          = ['activeReq','inactiveReq'];
    toggleActiveState           = {
        'activeReq': false,
        'inactiveReq': false
    }

    @track modalShowDetails;
    @track modalEditDetails;
    @track modalEditMapping;
    @track recordTarget;

    /* ------------------------- START Callbacks ------------------------- */
    /**
     * @author Ben Harvie <ben.harvie@salesforce.com>
     * @description utilize connectedCallback to update elements upon successful loading of components
     *              on the page - note that they may not have been rendered yet (see renderedCallback) 
     */
     connectedCallback() {
        // Set connected to for setFilters to operate after setCurrentPageReference
        this.connected = true;

        // Once connected, apply filters (captured from URL parameters)
        if(this.connectedQueue){
            this.setFilters();
        }

        // If the page value is less than 1 or not defined, set to default. This is held
        // separately from getDefaultURLValue due to the possibility of 0 or negatives.
        if(this.page < 1){
            this.page = this.pageDefault;
        }
    
        // Utility Class Initialization
        scopeOptions().then((result => this.auditScopeOptions = result ));
        controlFamilyOptions().then((result => this.controlFamilyOptions = result ));

        // Load the columns of the datatable
        this.loadColumns();
    }

    /**
     * @author Ben Harvie <ben.harvie@salesforce.com>
     * @description utilize renderedCallBack to update toggle elements after they've been
     *              rendered on the page in the browser (established DOM)
     */
    renderedCallback(){
        // Set default buttons on render
        if(!this.rendered){
            this.rendered = true;
            // Set default buttons on render
            this.setButtonSelection(this.toggleActiveValues, this.toggleActive);
            /*
            this.setButtonSelection(this.toggleQaValues, this.toggleQa);
            this.setButtonSelection(this.toggleAreaValues, this.toggleArea);
            this.setButtonSelection(this.toggleAreaMenuValues, this.toggleArea, 'buttonMenuItem');*/
        }
    }
    /* ------------------------- END Callbacks ------------------------- */

    /* ------------------------- START URL Parameters ------------------------- */
    /**
     * @author Ben Harvie <ben.harvie@salesforce.com>
     * @description Using the CurrentPageReference component of NavigiationMixin, retrieve 
     * metadata about this page.
     * @version 1.0 - common function for KARL datatables. Runs in a local context.
     */
    @wire(CurrentPageReference)
    setCurrentPageReference(currentPageReference) {
        this.currentPageReference = currentPageReference;
        // Set filters if the page is connected, or enable filters within connectedCallback
        this.connected ? this.setFilters() : this.connectedQueue = true;
    }

    /**
     * @author Ben Harvie <ben.harvie@salesforce.com>
     * @description Update the currentPageReference state with changes on the URL parameters
     * @param stateChanges - the state (URL component) which is changing
     */
    getUpdatedPageReference(stateChanges) {
        return Object.assign({}, this.currentPageReference, {
            state: Object.assign({}, this.currentPageReference.state, stateChanges)
        });
    }

    /**
     * @author Ben Harvie <ben.harvie@salesforce.com>
     * @description Update the page reference parameters based on this.* values
     */
    get updatedPageReference() {
        return this.getUpdatedPageReference({
            c__sortBy: this.sortBy,
            c__sortDirection: this.sortDirection,
            c__scopeId: this.auditScope,
            c__family: this.toggleFamily,
            c__active: this.toggleActive,
            c__page: this.page.toString(),
            c__rows: this.pageSize.toString()
        });
    }
    /* ------------------------- END URL Parameters ------------------------- */

    /**
     * @author Ben Harvie <ben.harvie@salesforce.com>
     * @description set the page filters based on URL parameters.
     */
    setFilters(){
        // Sorting Parameters
        this.sortBy             = getDefaultURLValue('c__sortBy', this.defaultSortBy, this.currentPageReference);
        this.sortDirection      = getDefaultURLValue('c__sortDirection', this.defaultSortDirection, this.currentPageReference);
        // Filter Parameters
        this.auditScope         = getDefaultURLValue('c__scopeId', this.defaultScope, this.currentPageReference);
        this.toggleFamily       = getDefaultURLValue('c__family', this.toggleFamilyDefault, this.currentPageReference);
        this.toggleActive       = getDefaultURLValue('c__active', this.toggleActiveDefault, this.currentPageReference);
        this.page               = getDefaultURLValue('c__page', this.pageDefault, this.currentPageReference);
        this.pageSize           = getDefaultURLValue('c__rows', this.pageSizeDefault, this.currentPageReference);
    }

    /**
     * @author Ben Harvie <ben.harvie@salesforce.com>
     * @description Manage the columns via this method, which pushes to the datatable. Doing it this way
     * instead of via basic array allows us to use metadata/other flags to determine which columns
     * to display to the end-user.
     */
    loadColumns() {
        this.columns = [];

        this.columns.push({
            label: 'Scope', 
            fieldName: 'scope', 
            type: 'text',
            editable: false,
            sortable: true, 
            hideDefaultActions: true,
            initialWidth: 110,
        });
        this.columns.push({
            label: 'Control ID', 
            fieldName: 'controlId', 
            type: 'text',
            editable: false,
            sortable: true, 
            hideDefaultActions: true,
            initialWidth: 120,
        });
        this.columns.push({
            label: 'Control Name', 
            fieldName: 'controlName', 
            type: 'text',
            wrapText: true,
            editable: false,
            sortable: true, 
            hideDefaultActions: true,
            initialWidth: 150,
        });
        this.columns.push({
            label: 'Description', 
            fieldName: 'ctrlDescription', 
            type: 'text',
            wrapText: true,
            editable: false,
            sortable: true, 
            hideDefaultActions: true,
            initialWidth: 350
        });
        this.columns.push({
            label: 'CCF Control', 
            fieldName: 'ccfControlName', 
            type: 'text',
            wrapText: true,
            editable: false,
            sortable: true, 
            hideDefaultActions: true,
            initialWidth: 110,
        });
        this.columns.push({
            label: 'Control Performer', 
            fieldName: 'controlPerformer', 
            type: 'text',
            wrapText: true,
            editable: false,
            sortable: true, 
            hideDefaultActions: true,
            initialWidth: 140,
        });
        this.columns.push({
            label: 'Control Owner', 
            fieldName: 'controlOwner', 
            type: 'text',
            wrapText: true,
            editable: false,
            sortable: true, 
            hideDefaultActions: true,
            initialWidth: 140,
        });
        this.columns.push({
            label: 'Control Executive', 
            fieldName: 'controlExecutive', 
            type: 'text',
            wrapText: true,
            editable: false,
            sortable: true, 
            hideDefaultActions: true,
            initialWidth: 140,
        });
        this.columns.push({
            label: 'Control Sub Cerifier', 
            fieldName: 'controlSubcertifier', 
            type: 'text',
            wrapText: true,
            editable: false,
            sortable: true, 
            hideDefaultActions: true,
            initialWidth: 140,
        });
        this.columns.push({
            label: 'Control Family', 
            fieldName: 'controlFamily', 
            type: 'text',
            wrapText: true,
            editable: false,
            sortable: true, 
            hideDefaultActions: true,
            initialWidth: 125,
        });
    }

    // Retrieve the primary data table information
    @wire(getReport, { auditScopeId: '$auditScope',
                        controlFamily: '$toggleFamily',
                        activeStatus: '$toggleActive'})  wiredData(result) {
        this.wireData = result; // Save the full result, for diffing refreshApex later

        console.log("KARL >> Parameters are set - data feed updating!");

		if ( result.data && result.data.length ){
			// ! Check if the wire returns any data
			try {
                this.paramsSet  = true;
                this.records    = true; // Specify boolean for if:true
                this.error      = false;
                this.totalRows  = result.data.length;
                // Pagination
                this.totalPages = Math.ceil(this.totalRows / this.pageSize);
                if(this.page > this.totalPages){
                    this.page = this.totalPages; // set to last page if page is higher
                }
                this.startingRecord = (this.page - 1) * parseInt(this.pageSize);
                this.endingRecord   = (this.pageSize * this.page); // assumes each page is full
                // Determine true end
                this.endingRecord = (this.endingRecord > this.totalRows) ? this.totalRows : this.endingRecord; 
                for (let i = 1; i <= this.totalPages; i++) {
                    this.pageLinks.push(i);
                }
                // Filter the data
                this.wireData.data  = this.sortData(this.wireData.data); // sort full dataset
                this.filteredData   = this.sortData(result.data).slice(this.startingRecord, this.endingRecord);
                // Adjust starting record & offset for display purposes
                this.rowOffset      = this.startingRecord; // should be -1 from actual
                this.startingRecord = this.startingRecord + 1;
                //this.sortData(this.sortBy, this.sortDirection);
                this.isLoading  = false; // do this last
                this.resetPaginationList();
			} catch(e) {
				console.log('KARL >> LWC Error Unknown: ', result.error);
                this.isLoading = false;
			}
		} else if ( result.data && !result.data.length ){
			// ! Check if the wire is null
            //console.log('KARL >> LWC Empty', result.data);
            this.paramsSet = true;
			this.records = false; // specify boolean for if:false
            this.isLoading = false;
            this.totalRows = result.data.length;
		} else if (result.error){
            // ! Check if the wire returns an error, and return the error
            if(!this.auditcycle){
                // False error --> just waiting on parameters
                //console.log('KARL >> LWC Parameters not set');
                this.paramsSet = false;
                this.records = false; // specify boolean for if:false
                this.isLoading = false;
            }else{
                // Legitimate error
                console.log('KARL >> LWC Error Legitimate: ', result.error);
                this.error = result.error;
                this.paramsSet = true;
                this.isLoading = false;
                this.records = false;
            }
		}
    }

     /**------------------------- START Local Pagination -------------------------
     * @author Ben Harvie <ben.harvie@salesforce.com>
     * @description the following functions manage pagination for the KARL datatables. 
     *              Pagination is used for performance optimization - preventing too
     *              many components from rendering in the DOM causing issues with user
     *              memory consumption. These components generally need to run in the 
     *              local context of the component which generated the datatable.
     */
      handlePagination(page){
        this.isLoading = true; // add the spinner
        this.startingRecord = (this.page - 1) * parseInt(this.pageSize);
        this.endingRecord   = (this.pageSize * page); // assumes each page is full
        // Determine true end
        this.endingRecord   = (this.endingRecord > this.totalRows) ? this.totalRows : this.endingRecord; 

        // Slice the data to show the required records 
        // Update the filteredData to respect the filteredRows.
        new Promise(
            (resolve,reject) => {
                setTimeout(()=> {
                    this.filteredData = this.wireData.data.slice(this.startingRecord, this.endingRecord);
                    resolve();
               }, 0);
            }).then(
                () => {
                    this.isLoading = false;
                    // Adjust starting record & offset for display purposes
                    this.rowOffset      = this.startingRecord; // should be -1 from actual
                    this.startingRecord = this.startingRecord + 1;
                    this.resetPaginationList();
                }
            );

        this[NavigationMixin.Navigate](this.updatedPageReference, true);  
    }

    // Pagination Handler: Go to the selected page
    handlePage(event) {
        this.page = event.detail;
        this.handlePagination(this.page);
    }

    // Pagination Handler: Go to the previous page
    previousHandler() {
        if (this.page > 1) {
            this.page = parseFloat(this.page) - 1; // decrease page by 1
            this.handlePagination(this.page);   
        }
    }

    // Pagination Handler: Go to the next page
    nextHandler() {
        if((this.page<this.totalPages) && this.page !== this.totalPages){
            this.page = parseFloat(this.page) + 1; // increase page by 1
            this.handlePagination(this.page);          
        }             
    }

    // Pagination Handler: Go to the first page
    firstHandler() {
        this.page = this.pageDefault; 
        this.handlePagination(this.page);
    }

    // Pagination Handler: Go to the last page
    lastHandler() {
        this.page = this.totalPages; 
        this.handlePagination(this.page);
    }
    
    // Pagination Handler: Manage changes in page size (e.g. 25/50/100/200/200+)
    handlePageSizeChange(event) {
        this.pageSize   = event.detail.value;
        this.totalPages = Math.ceil(this.totalRows / this.pageSize);
        this.pageLinks  = []; // reset and re-establish based on change in total pages
        for (let i = 1; i <= this.totalPages; i++) {
            this.pageLinks.push(i);
        }
        if(this.page > this.totalPages){
            this.page = this.totalPages; // set to last page if page is higher
        }
        
        // Delay reset pagination list to allow re-render
        new Promise(
            (resolve,reject) => {
                setTimeout(()=> {
                    this.handlePagination(this.page);
                    resolve();
                }, 0);
            }).then(
                () => {
                    this.resetPaginationList();
                }
            );

    }

    // Pagination Handler: Reset variables for re-establishment
    resetPagination(){
        this.page           = this.pageDefault; // reset to page 1
        this.startingRecord = 1;
        this.rowOffset      = 0;
        this.endingRecord   = 0;
        this.totalRows      = 0;
        this.totalPages     = 0;
        this.pageLinks      = [];
        this.draftValues    = [];
        this.resetHeaderAction();
    }

    // Pagination Handler: Reset the pagination list (e.g. 1,2,3...) for re-establishment
    resetPaginationList(){
        if(this.records){
            /** If records are already published, we need to reset the pagination
            * Note that this queries the child component c-karl_paginator which
            * appears multiple times, so we must iterate through each instance */
            var paginationLists = this.template.querySelectorAll('c-karl_paginator');
            for(var i = 0; i < paginationLists.length; i++){
                paginationLists[i].pageMaxList(this.pageLinks);
            }
        }
    }
    /** ------------------------- START Sorting Functions -------------------------
     * @author Ben Harvie <ben.harvie@salesforce.com>
     * @description Update the sorting based on header action selections
     */
    doSorting(event){
        this.isLoading = true;

        this.sortBy = event.detail.fieldName;
        this.sortDirection = event.detail.sortDirection;
        this.page           = this.pageDefault; // reset to page 1
        this.startingRecord = 0;
        this.endingRecord   = this.pageSize;

        new Promise(
        (resolve,reject) => {
            setTimeout(()=> {
                this.wireData.data  = this.sortData(this.wireData.data); // sort the full data set for page changes
                this.filteredData   = this.wireData.data.slice(this.startingRecord, this.endingRecord);
                resolve();
            }, 0);
        }).then(
            () => this.isLoading = false
        );
        
        this.startingRecord         = this.startingRecord + 1;
        
        this[NavigationMixin.Navigate](this.updatedPageReference, true);
    }

    // Perform the sorting based on the event received at doSorting
    sortData(currentData){
        let parseData = JSON.parse(JSON.stringify(currentData));

        let keyValue = (a) => {
            return a[this.sortBy];
        }

        let isReverse = this.sortDirection ==='asc'?1:-1;
        
        parseData.sort((x, y) => {
            x = keyValue(x) ? keyValue(x) : ''; // handling null values
            y = keyValue(y) ? keyValue(y) : '';
            // sorting values based on direction
            return isReverse * ((x > y) - (y > x));
        });

        return parseData;
    }
    /* ------------------------- END Sorting Functions ------------------------- */
    /* ------------------------- START Client-side Filtering Functions ------------------------- */
    /**
     * @author Ben Harvie <ben.harvie@salesforce.com>
     * @description event handler for handling onheaderaction events from the data table, for filtering.
     * @reusable 
     * @param event - js event handle
     */
     handleHeaderAction (event) {
        const actionName    = event.detail.action.name; // Retrieves the name of the selected filter
        const colName       = event.detail.columnDefinition.fieldName; // Retrieves the current column definition
        const filterColumns = this.columns;
        const activeFilter  = this.activeFilter;

        // Exclude events that are for wrapText and clipText (default Salesforce header actions)
        if (actionName !== activeFilter && actionName != 'wrapText' && actionName != 'clipText') {
            filterColumns.find(col => col.fieldName === colName).actions.forEach(action => action.checked = action.name === actionName);
            this.activeFilter = actionName;
            this.updateDataTable(colName);

            // Update the actions list to show which action is currently selected            
            this.columns = [...filterColumns];
        }
    }

    // Reset header actions back to 'All' when applicable
    resetHeaderAction(){
        let filterColumns = this.columns;
        filterColumns.map(col => {
            var actions = col.actions;
            if(actions){
                actions.forEach((action) => {
                    action.checked = action.name === 'all';
                });
            }
        });
        this.columns = [...filterColumns]; // use spread operator to force columns render
    }

    /**
     * @author Ben Harvie <ben.harvie@salesforce.com>
     * @description update the Data Table based on the selected filters (in header actions)
     * @param columnName - the column from the header action
     */
     updateDataTable(columnName) {
        const activeFilter = this.activeFilter;

        let rows = '';
        // Determine if we're searching raw data, or already filtered data
        if(this.searching){
            /*if(activeFilter != this.tempFilter){
                // ! TODO:  Because we want to also filter on searched data (search strings), toggling the header action
                // !        is also treated as a cumulative effect by using pre-existing filteredData. The issue
                // !        being is that we don't have a searched data state that we can revert back to.
                //console.log('Header action filter changed');
            }*/
            rows = this.filteredData; // search on already filtered data
        }else{
            rows = this.wireData.data; // raw unfiltered data to start with (no need to JSONify here)
        }
        let filteredRows = rows;

        // Apply the filter
        if (activeFilter !== 'all') {
            this.tempFilter = activeFilter;
            this.isLoading  = setLoadingSpinner([this.auditScope]);
            // Set values for pagination management
            this.searching  = true;
            this.searchStartingRow = this.rowOffset; // save for reset
            this.rowOffset  = 0;

            // Generate filteredRows
            filteredRows = rows.filter(function (row) {
                if(row[columnName] !== '' && row[columnName] !== undefined){
                    return row[columnName].toUpperCase() === activeFilter.toUpperCase();
                }     
            });
            this.searchRows = filteredRows.length;
        }else{
            // Reset the fields back to original filter
            this.searching = false;
            this.rowOffset = this.searchStartingRow; // Reset to before
            filteredRows = JSON.parse(JSON.stringify(this.wireData.data.slice((this.startingRecord - 1), this.endingRecord)));
        }

        // Update the filteredData to respect the filteredRows.
        new Promise(
            (resolve,reject) => {
                setTimeout(()=> {
                    this.filteredData = filteredRows;
                    resolve();
                }, 0);
            }).then(
                () => this.isLoading = false
            );
    }
    /* ------------------------- END Client-side Filtering Functions ------------------------- */
    /* ------------------------- START Client-side Searching Functions ------------------------- */
    /**
     * @author Ben Harvie <ben.harvie@salesforce.com>
     * @description Handle searches from the inputs on the main component page
     * @param event - js event handle
     */
    handleSearch(event){
        // Define variables (wouldn't accept event vars in timeout)
        let searchVal = event.target.value;
        let fieldName = event.target.name;

        // Delay to account for individual to stop typing before starting the search (performance)
        clearTimeout(this.typingTimer);
        this.typingTimer = setTimeout(() => {
            this.conductSearch(searchVal, fieldName);
        }, this.typingWait);
    }

    /**
     * @author Ben Harvie <ben.harvie@salesforce.com>
     * @description conduct the search
     * @param searchValue - the value being searched
     * @param searchValue - the field to search on
     */
    conductSearch(searchValue, searchField){
        let rows = '';
        // Determine if we're searching raw data, or already filtered data
        if(this.searching){
            rows = this.filteredData; // search on already filtered data
        }else{
            rows = JSON.parse(JSON.stringify(this.wireData.data)); // raw unfiltered data to start with
        }
        let filteredRows = rows;

        // Regular expression for how we're searching cleartext entries
        var regex = new RegExp(searchValue.replace('/[|\\{}()[\]^$+*?.]/g', '\\$&'),'i'); // previously tried gi - i much more reliable

        // Search data
        this.isLoading = setLoadingSpinner([this.auditScope]);
        if(searchValue !== ''){
            // Set values for pagination management
            this.searching = true;
            this.searchStartingRow = this.rowOffset; // save for reset
            this.rowOffset = 0;

            // Search through each field / filter results
            for(const searchString in this.searchObj){
                if (this.searchObj.hasOwnProperty(searchString)) {
                    if(searchField === searchString){
                        filteredRows = rows.filter(
                            row => regex.test(row[this.searchObj[searchString]])
                        );
                        this.searchRows = filteredRows.length;
                    }
                }
            }
        }else{
            // Reset the fields back to original filter
            this.searching = false;
            this.rowOffset = this.searchStartingRow; // Reset to before
            this.resetHeaderAction();
            filteredRows = JSON.parse(JSON.stringify(this.wireData.data.slice((this.startingRecord - 1), this.endingRecord)));
        }

        // Update the filteredData to respect the filteredRows.
        new Promise(
            (resolve,reject) => {
                setTimeout(()=> {
                    this.filteredData = filteredRows;
                    this.resetPaginationList();
                    resolve();
                }, 0);
            }).then( 
                () => {
                    this.isLoading = false;
                }
            );
    }
    /* ------------------------- END Client-side Searching Functions ------------------------- */
    /* ------------------------- START Server-side Filter Functions ------------------------- */
    /**
     * @author Ben Harvie <ben.harvie@salesforce.com>
     * @description event handler for handling changes to Audit Scope combobox
     * @param event - js event handle
     */
    handleAuditScopeChange(event) {
        this.isLoading = setLoadingSpinner([this.auditScope]);
        this.resetPagination();
        
        // Delay reset pagination list to allow @wire to run
        new Promise(
            (resolve,reject) => {
                setTimeout(()=> {
                    this.auditScope = event.detail.value; // triggers @wire
                    resolve();
                }, 0);
            }).then(
                () => {
                    // Update page vars
                    this[NavigationMixin.Navigate](this.updatedPageReference, true);
                }
            );
    }

    /**
     * @author Ben Harvie <ben.harvie@salesforce.com>
     * @description trigger wire data feed for control family toggles
     * @param event - js event handle
     */
     handleFamilyChange(event) {
        this.isLoading = setLoadingSpinner([this.auditScope]);
        this.resetPagination();
        
        // Delay reset pagination list to allow @wire to run
        new Promise(
            (resolve,reject) => {
                setTimeout(()=> {
                    this.toggleFamily = event.detail.value;// triggers @wire
                    resolve();
                }, 0);
            }).then(
                () => {
                    // Update page vars
                    this[NavigationMixin.Navigate](this.updatedPageReference, true);
                }
            );
    }

    /**
     * @author Ben Harvie <ben.harvie@salesforce.com>
     * @description trigger wire data feed for active/inactive toggles
     * @param event - js event handler
     */
    handleActive(event){
        this.isLoading = setLoadingSpinner([this.auditScope]);
        this.makeButtonChange(event, this.toggleActiveState, this.toggleActiveValues, this.toggleActiveDefault, this.toggleActive);
        this.resetPagination();

        // Update primary toggle array
        let temporaryActive = [];
        for(var i=0;i<this.toggleActiveValues.length;i++){
            if(this.toggleActiveState[this.toggleActiveValues[i]]){
                temporaryActive.push(this.toggleActiveValues[i]);
            }
        }
        if(this.toggleActive.length == 0){
            temporaryActive.push('activeReq');
        }
        
        // Use a promise for effective isLoading
        new Promise( (resolve,reject) => {
            setTimeout(()=> {
                this.toggleActive = temporaryActive;// triggers @wire
                resolve();
            }, 0);
        }).then(() => {
                // Update page vars
                this[NavigationMixin.Navigate](this.updatedPageReference, true);
            }
        );
    }

    /**
     * @author Ben Harvie <ben.harvie@salesforce.com>
     * @description Handle refreshing the datatable based on button click
     * @param event - js event handle (not used - simply a trigger)
     */
    handleRefresh(){
        const searchFields = this.searchFields;
        const onchangeEvent = new CustomEvent('change',{bubbles: true});
        refreshApex(this.wireData).then(() => {
            // Re-apply client-side search filters
            console.log('Wire Refreshed');
            this.searching  = false; // reset to full data set to perform fresh search
            for(let i = 0; i < searchFields.length; i++){
                this.template.querySelector('lightning-input[data-field="' + searchFields[i] + '"]').value != '' ? 
                    this.template.querySelector('lightning-input[data-field="' + searchFields[i] + '"]').dispatchEvent(onchangeEvent) : null; 
            }
        });
    }
    /* ------------------------- END Server-side Filter Functions ------------------------- */
    /* ------------------------- START Button Management Functions ------------------------- */
    /**
     * @author Ben Harvie <ben.harvie@salesforce.com>
     * @description set the button selection (page load handler)
     * @param {Array} buttonSet - state array of buttons
     * @param {Array} selectedValues - values that should be selected
     * @param {String} buttonType - type of the button for processing
     */
     setButtonSelection(buttonSet, selectedValues, buttonType='button'){
        let buttonMenuState = false;
        for(var i=0;i<buttonSet.length;i++){
            if(selectedValues.includes(buttonSet[i])){
                this.makeSelectedButton(buttonSet[i], true, buttonType);
                if(buttonType === 'buttonMenuItem') buttonMenuState = true;
            }
        }
        // Toggle button menu (for showMore)
        if(buttonMenuState){
            this.makeSelectedButton('showMore', true, 'buttonMenu')
        }
    }

    /**
     * @author Ben Harvie <ben.harvie@salesforce.com>
     * @description reset all buttons in a set (defaults to false)
     * @param {Array} buttonSet - state array of buttons
     * @param {Array} buttonSetValues - value array of buttons
     * @param {Boolean} buttonBoolean - whether to set or unset
     */
    resetButtons(buttonSet, buttonSetValues, buttonBoolean=false, buttonType='button'){
        for(var i=1;i<buttonSetValues.length;i++){
            if(buttonSet[buttonSetValues[i]]){
                this.makeSelectedButton(buttonSetValues[i], buttonBoolean, buttonType);
            }
        }
    }

    /**
     * @author Ben Harvie <ben.harvie@salesforce.com>
     * @description helper for making changes to the formatting of standard buttons
     */
    makeButtonChange(event, buttonSet, buttonSetValues, defaultButtonValue, filterSet, singleOnly=false){
        if(event.target.value == defaultButtonValue){
            if(!buttonSet[event.target.value]){
                this.makeSelectedButton(event.target.value, true);
                this.resetButtons(buttonSet, buttonSetValues);
            }
            else{
                this.resetButtons(buttonSet, buttonSetValues);
            }
        }
        else if(buttonSet[event.target.value]){
            this.makeSelectedButton(event.target.value, false);
        }
        else{
            this.makeSelectedButton(event.target.value, true);

            // Default Handler
            if((filterSet.includes(defaultButtonValue) || filterSet == defaultButtonValue) && defaultButtonValue != ''){
                this.makeSelectedButton(defaultButtonValue, false);
            }

            // Handler for single selection only button groups
            if(singleOnly){
                for(var i=0;i<buttonSetValues.length;i++){
                    if(buttonSetValues[i] != event.target.value){
                        this.makeSelectedButton(buttonSetValues[i], false);
                    }
                }
            }
        }
    }

    /** 
     * @author Ben Harvie <ben.harvie@salesforce.com>
     * @description helper for making changes to the formatting of lightning-button-menu & items 
     */
    makeButtonMenuChange(event, buttonSet){
        if(buttonSet[event.target.value]){
            // If already set, unset it (e.g. clicking on one that is set)
            this.makeSelectedButton(event.target.value, false, 'buttonMenuItem');
        }else{
            this.makeSelectedButton(event.target.value, true, 'buttonMenuItem');
        }
    }

    /** 
     * @author Ben Harvie <ben.harvie@salesforce.com>\
     * @description toggles the state values of each button
     * @todo Fix technical debt - moving away from hardcoded if statements - should iterate
     */
    toggleButtonState(targetValue, targetBoolean){
        if(this.toggleActiveValues.includes(targetValue)){
            this.toggleActiveState[targetValue] = targetBoolean;
        }
    }

    /**
     * @author Ben Harvie <ben.harvie@salesforce.com>
     * @description set the button formatting using querySelector
     */
     makeSelectedButton(targetValue, stateBoolean, buttonType='button'){
        this.toggleButtonState(targetValue, stateBoolean);

        let buttonClassSelected     = '';
        let buttonClassUnselected   = '';
        let unselectedProcessor     = false;
        let buttonVariantSelected   = '';
        let buttonVariantUnSelected = '';
        let variantProcessor        = false;
        let button;

        // Specify CSS class based on button type
        if(buttonType === 'buttonMenu'){
            buttonClassSelected     = 'karl-buttonmenu_selected';
            buttonVariantSelected   = 'border-inverse';
            buttonVariantUnSelected = 'border';
            variantProcessor        = true;

            button = this.template.querySelector('[data-id="' + targetValue + '"]');
        }else if (buttonType === 'buttonMenuItem'){
            buttonClassSelected     = 'karl-buttonmenuitem_selected';

            button = this.template.querySelector('[data-id="' + targetValue + '"]');
        }else{
            buttonClassSelected     = 'slds-button_brand';
            buttonClassUnselected   = 'slds-button_neutral';
            unselectedProcessor     = true;

            button = this.template.querySelector('button[value="' + targetValue + '"]');
        }

        if(button && stateBoolean){
            !button.classList.contains(buttonClassSelected) && button.classList.add(buttonClassSelected); 
            (unselectedProcessor && button.classList.contains(buttonClassUnselected)) && button.classList.remove(buttonClassUnselected); 
            if(variantProcessor) button.variant=buttonVariantSelected; 
        }
        if(button && !stateBoolean){
            (unselectedProcessor && !button.classList.contains(buttonClassUnselected)) && button.classList.add(buttonClassUnselected); 
            button.classList.contains(buttonClassSelected) && button.classList.remove(buttonClassSelected); 
            if(variantProcessor) button.variant=buttonVariantUnSelected; 
        }
    }

    /** 
     * @author Ben Harvie <ben.harvie@salesforce.com>
     * @description need to update lightning-menu-items as they aren't generated in the DOM until clicked
     */
    handleAreaDropdown(){
        this.setButtonSelection(this.toggleAreaMenuValues, this.toggleArea, 'buttonMenuItem');
    }

    /* ------------------------- END Button Management Functions ------------------------- */
    /**
     * @author Ben Harvie <ben.harvie@salesforce.com>
     * @description Handle rowActions in the data table, e.g. to take the user to the resource page in a new window.
     * @param event - js event handle     */
    handleRowAction(event){
        const action = event.detail.action.name;
        this.recordTarget = event.detail.row.Id;
        //console.log(JSON.stringify(event.detail.action) + ' on row ' + JSON.stringify(event.detail.row));

        if(action === 'show_details') {
            this.modalShowDetails = true;
        }else if(action === 'edit_details') {
            this.modalEditDetails = true;
        }else if(action === 'edit_mapping') {
            this.modalEditMapping = true;
        }
    }
    closeModals() {
        this.modalShowDetails = false;
        this.modalEditDetails = false;
        this.modalEditMapping = false;
    }

}
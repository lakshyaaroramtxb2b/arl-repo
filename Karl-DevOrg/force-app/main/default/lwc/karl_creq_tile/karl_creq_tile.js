import { LightningElement, api, track } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';

/**
 * @author Ben Harvie
 * @email ben.harvie@salesforce.com
 * @desc This LWC Component Builds the tiles as a part of the CREQ Component on Requests Master Data
 */
export default class karl_creq_tile extends NavigationMixin(LightningElement) {
    @api recordId;
    @api creq;
    @track recordPageUrl;

    /** 
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @desc Standard Connected Call Back for Child
     * @modifier recordId to be modified based on the object in question
     */
    connectedCallback() {
        // Generate a URL to the relevant record
        this[NavigationMixin.GenerateUrl]({
            type: 'standard__recordPage',
            attributes: {
                recordId: this.creq.Id,
                actionName: 'view',
            },
        }).then(url => {
            this.recordPageUrl = url;
        });
    }

    /** 
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @desc Create On Click Event
     */
    handleOpenRecordClick() {
        const selectEvent = new CustomEvent('karlrecordview', {
            detail: this.creq.Id
        });
        this.dispatchEvent(selectEvent);
    }

    /** 
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @desc Styling for Icons (Full Name)
     */
    get iconFullStatus(){
        if( this.creq.Cycle_Request_Status__c == "Follow-ups" ){
            return "utility:help";
        }else if( this.creq.Cycle_Request_Status__c == "Provided to Auditor" || this.creq.Cycle_Request_Status__c == "Closed" ){
            return "utility:success";
        }else if( this.creq.Cycle_Request_Status__c == "Not Applicable" ){
            return "utility:record";
        }else{
            return "utility:rotate";
        }
    }

    /** 
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @desc Styling for Icons (Variant Styling)
     */
    get iconShortStatus(){
        if( this.creq.Cycle_Request_Status__c == "Follow-ups" || this.creq.Cycle_Request_Status__c == "Not Applicable" ){
            return "warning";
        }else if( this.creq.Cycle_Request_Status__c == "Provided to Auditor" || this.creq.Cycle_Request_Status__c == "Closed" ){
            return "success";
        }else{
            return "";
        }
    }
    
}
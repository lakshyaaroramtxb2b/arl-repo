import { LightningElement, api, track } from 'lwc';

export default class Karl_paginator extends LightningElement {
    @api pagelinks;
    @api page;
    @api lastpage;
    @track buttonIds    = [];
    @track pageList     = [];
    @track firstButton  = 1;
    @track lastButton   = 15;
    @track maxButtons   = 15;

    connectedCallback(){
        this.connected = true;
        this.pageMaxList(this.pagelinks);
    }

    @api
    pageMaxList(pageLinkslist){
        this.pageList = pageLinkslist;
        if(this.pageList.length > this.maxButtons){
            if(this.page > (this.lastButton / 2)){
                this.firstButton    = Math.ceil(parseInt(this.page) - (this.maxButtons / 2));
                this.lastButton     = Math.floor(parseInt(this.page) + (this.maxButtons / 2));
            } else if(this.page < (this.maxButtons / 2)) {
                this.firstButton    = 1;
                this.lastButton     = this.maxButtons;
            } else if((this.lastButton - this.page) > (this.maxButtons / 2)){
                this.firstButton    = Math.ceil(parseInt(this.page) - (this.maxButtons / 2));
                this.lastButton     = Math.floor(parseInt(this.page) + (this.maxButtons / 2));
            }
            this.pageList = this.pageList.filter(n => n >= this.firstButton && n <= this.lastButton);
        }
    }

    renderedCallback(){
        // Page Button Rendering for Selection
        this.buttonIds = this.pageList;
        if(this.template.querySelector('button[data-id="' + this.page + '"]')){
            this.template.querySelector('button[data-id="' + this.page + '"]').classList.add('slds-button_brand'); 
            this.buttonIds = this.buttonIds.filter(button => button != this.page);
        }
        for(let i = 0; i < this.buttonIds.length; i++){
            this.template.querySelector('button[data-id="' + this.buttonIds[i] + '"]').classList.add('slds-button_neutral'); 
            this.template.querySelector('button[data-id="' + this.buttonIds[i] + '"]').classList.remove('slds-button_brand');
        }
    }

    previousHandler() {
        this.page = (parseInt(this.page) - parseInt(1));
        this.pageMaxList(this.pagelinks);
        this.dispatchEvent(new CustomEvent('previous'));
    }

    nextHandler() {
        this.page = (parseInt(this.page) + parseInt(1));
        this.pageMaxList(this.pagelinks);
        this.dispatchEvent(new CustomEvent('next'));
    }

    @api
    firstHandler() {
        this.page = 1;
        this.pageMaxList(this.pagelinks);
        this.dispatchEvent(new CustomEvent('first'));
    }

    lastHandler() {
        this.page = this.lastpage;
        this.pageMaxList(this.pagelinks);
        this.dispatchEvent(new CustomEvent('last'));
    }

    pageHandler(event) {
        this.page = event.target.value;
        this.pageMaxList(this.pagelinks);
        const selectEvent = new CustomEvent('page', {
            detail: this.page
        });
        this.dispatchEvent(selectEvent);
    }

    get notFirst(){
        return (this.page != "1") ? true : false;
    }

    get notLast(){
        return (this.page != this.lastpage) ? true : false;
    }
}
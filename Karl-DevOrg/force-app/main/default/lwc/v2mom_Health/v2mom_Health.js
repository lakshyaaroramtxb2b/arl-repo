import { LightningElement, wire, track } from 'lwc';
import { getRecord } from 'lightning/uiRecordApi';
import USER_ID from '@salesforce/user/Id';
import NAME_FIELD from '@salesforce/schema/User.Name';
import MED_IMAGE from '@salesforce/schema/User.MediumPhotoUrl';

export default class v2mom_Health extends LightningElement {
    @track userData;
    @track showChildComponent = false;
    @track methods = [];
    @track externalUser = {};
    @track userId = USER_ID;
    @wire(getRecord, { recordId: '$userId', fields: [NAME_FIELD, MED_IMAGE] })
    wireuser({ error, data }) {
        document.title = "V2MOM Health";
        if (error) {
            this.error = error;
        } else if (data) {
            this.userData = data.fields;
        }
    }

    selectedUser(event) {
        this.userId = event.detail.userId;
    }

    showComponents() {
        this.showChildComponent = true;
    }

}
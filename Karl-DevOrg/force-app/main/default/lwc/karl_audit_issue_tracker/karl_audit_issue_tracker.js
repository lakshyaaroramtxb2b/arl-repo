import { LightningElement,api,track,wire } from 'lwc';
import getAuditIssueTrackingInfo from '@salesforce/apex/KARL_AuditIssueTrackerController.getAuditIssueTrackingInfo';

export default class Karl_audit_issue_tracker extends LightningElement {
    @api recordId;

    @track wireData;
    @track filteredData = [];
    @track isLoading = false;

    @track sortDirection = 'asc';
    @track sortBy = 'issueLink';

    @wire(getAuditIssueTrackingInfo, { cycleAuditReportId: '$recordId'})  wiredARL(result) {
        this.wireData = result; // Save the full result, for diffing refreshApex later
        this.isLoading = true;
        if (result.data && result.data.length ){
            // ! Check if the wire returns any data
            try {
                // this.paramsSet = true;
                // this.records = true; // Specify boolean for if:true
                this.filteredData = result.data; // Initially unfiltered is filteredData.
                this.isLoading = false;
                this.error = false;
                this.sortData(this.sortBy, this.sortDirection);
            } catch(e) {
                console.log('KARL >> LWC Error catch', e.message || e.body.message);
                this.isLoading = false;
            }
        } else if ( result.data && !result.data.length ){
            // this.records = false; // specify boolean for if:false
            this.isLoading = false;
        } else if (result.error){
            console.log('KARL >> LWC Error legitimate', result.error);
            this.error = result.error;
            this.isLoading = false;
            // this.records = false;
        }
    }

    connectedCallback() {
        this.loadColumns();
    }

    doSorting(event){
        this.sortBy = event.detail.fieldName;
        this.sortDirection = event.detail.sortDirection;
        this.sortData(this.sortBy, this.sortDirection);
    }

    sortData(fieldname, direction){
        let parseData = JSON.parse(JSON.stringify(this.filteredData));

        let keyValue = (a) => {
            return a[fieldname];
        }

        let isReverse = direction ==='asc'?1:-1;
        
        parseData.sort((x, y) => {
            x = keyValue(x) ? keyValue(x) : ''; // handling null values
            y = keyValue(y) ? keyValue(y) : '';
            // sorting values based on direction
            return isReverse * ((x > y) - (y > x));
        });
        this.filteredData = parseData;
        //this.refreshDraftValues();
    }

    loadColumns() {
        this.columns = [];

        this.columns.push({
            label: 'Audit Issue Tracker Name', 
            fieldName: 'issueLink', 
            type: 'url',
            editable: false,
            sortable: true, 
            hideDefaultActions: true,
            typeAttributes: {
                label: {fieldName: 'issueName'},
                target: '_blank'
            }
        });

        // this.columns.push({
        //     label: 'Impaced Scope', 
        //     fieldName: 'impactedScope',
        //     sortable: true,
        //     wrapText: true
        // });

        this.columns.push({
            label: 'Issue Type', 
            fieldName: 'issueType',
            sortable: true,
            wrapText: true
        });

        this.columns.push({
            label: 'Issue Response Status', 
            fieldName: 'issueResponseStatus',
            sortable: true,
            wrapText: true
        });

        this.columns.push({
            label: 'GRC POC', 
            fieldName: 'grcPocLink', 
            type: 'url',
            editable: false,
            sortable: true, 
            hideDefaultActions: true,
            typeAttributes: {
                label: {fieldName: 'grcPocName'},
                target: '_blank'
            }
        });
        
    }

}
import { LightningElement, api, track } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';

/**
 * @author Ben Harvie
 * @email ben.harvie@salesforce.com
 * @description This LWC Component Builds the tiles as a part of the Evidence Component on the CREQ layout
 */
export default class Karl_evw_tile extends NavigationMixin(LightningElement) {
    @api recordId;
    @api evwitem;
    @track recordPageUrl;

    /** 
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Standard Connected Call Back for Child
     * @modifier recordId to be modified based on the object in question
     */
    connectedCallback() {
        // Generate a URL to the relevant record
        this[NavigationMixin.GenerateUrl]({
            type: 'standard__recordPage',
            attributes: {
                recordId: this.evwitem.Id,
                actionName: 'view',
            },
        }).then(url => {
            this.recordPageUrl = url;
        });
    }

    /** 
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Create On Click Event
     */
    handleOpenRecordClick() {
        const selectEvent = new CustomEvent('karlrecordview', {
            detail: this.evwitem.Id
        });
        this.dispatchEvent(selectEvent);
    }

    /** 
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Edit junction object record on Button Click.
     */
    handleUploadClick() {
        // Send REQCT ID via the Event system
        const selectEvent = new CustomEvent('karlcommunityview', {
            detail: this.evwitem.KARL_Direct_Link__c
        });
        this.dispatchEvent(selectEvent);
    }

    /** 
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Styling for Icons (Full Name)
     */
    get iconFullStatus(){
        if( this.evwitem.Evidence_Status__c == "Returned to Engineer" ){
            return "utility:help";
        }else if( this.evwitem.Evidence_Status__c == "Provided to Auditor" ){
            return "utility:success";
        }else if( this.evwitem.Evidence_Status__c == "Ready for Review" ){
            return "utility:topic";
        }else{
            return "utility:rotate";
        }
    }

    /** 
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Styling for Icons (Variant Styling)
     */
    get iconShortStatus(){
        if( this.evwitem.Evidence_Status__c == "Provided to Auditor" ){
            return "success";
        }else{
            return "warning";
        }
    }
    
}
import { LightningElement,track } from 'lwc';
//import saveADMScrumTeamInDataAsset from '@salesforce/apex/CDI_MultipleObjectsController.saveADMScrumTeamInDataAsset';
import { NavigationMixin } from 'lightning/navigation';
import DATAASSET_OBJ from '@salesforce/schema/CDI_Data_Asset__c';
import DATAASSET_OBJ_NAME from '@salesforce/schema/CDI_Data_Asset__c.Name';
import DATAASSET_OBJ_Description from '@salesforce/schema/CDI_Data_Asset__c.Data_Asset_Description__c';
import PARENT_DATAASSET from '@salesforce/schema/CDI_Data_Asset__c.Parent_Data_Asset__c';
import DATAASSETType from '@salesforce/schema/CDI_Data_Asset__c.Data_Asset_Type__c';
import DATA_RETENTION_PERIOD from '@salesforce/schema/CDI_Data_Asset__c.Data_Retention_Period__c';
import LINKTOBALANCING from '@salesforce/schema/CDI_Data_Asset__c.Link_To_Balancing_Argument_Document__c';
import LINKTOEXCEPTION from '@salesforce/schema/CDI_Data_Asset__c.Link_To_Excepn_For_Basis_For_Processing__c';
import ISENCRYPTED from '@salesforce/schema/CDI_Data_Asset__c.IsEncrypted__c';
import DATALIMITEDTOPURPOSE from '@salesforce/schema/CDI_Data_Asset__c.Is_Personal_Data_Limited_To_Purpose__c';
import HASDPIAPERFORMED from '@salesforce/schema/CDI_Data_Asset__c.Has_DPIA_Been_Performed__c';
import CANCONSUMERSACCESSDATA from '@salesforce/schema/CDI_Data_Asset__c.Can_Consumers_Access_Data__c';
import CRYPTO_ALGO from '@salesforce/schema/CDI_Data_Asset__c.Crypto_Algorithm__c';
import DATASTORE from '@salesforce/schema/CDI_Data_Asset__c.Data_Store__c';
import GUSTEAM from '@salesforce/schema/CDI_Data_Asset__c.GUS_Team__c';
import DATACLASSIFICATION from '@salesforce/schema/CDI_Data_Asset__c.Data_Classification__c';
import DATACATEGORY from '@salesforce/schema/CDI_Data_Asset__c.Data_Category__c';
import DATAIDENTIFICATIONCATEGORY from '@salesforce/schema/CDI_Data_Asset__c.Data_Identification_Category__c';
import BASIS_FOR_PROCESSING from '@salesforce/schema/CDI_Data_Asset__c.Basis_For_Processing__c';
import TIME_UNIT from '@salesforce/schema/CDI_Data_Asset__c.Time_Unit__c';
import RETENTION_PROCESS from '@salesforce/schema/CDI_Data_Asset__c.Retention_Process__c';
import CUSTOMER_DATA_CATEGORY from '@salesforce/schema/CDI_Data_Asset__c.Customer_Data_Category__c';
import STATUS_FIELD from '@salesforce/schema/CDI_Data_Asset__c.Status__c';
import COMMENTS_FIELD from '@salesforce/schema/CDI_Data_Asset__c.Comments__c';
import ISPARENT from '@salesforce/schema/CDI_Data_Asset__c.Is_Parent__c';



export default class Dataassetform extends NavigationMixin(LightningElement){
    dataassetobj=DATAASSET_OBJ;
    dataassetobjname=DATAASSET_OBJ_NAME;
    dataassetobjdescription=DATAASSET_OBJ_Description;
    parentDataAsset=PARENT_DATAASSET;
    dataassettype=DATAASSETType;
    gusteam=GUSTEAM;
    dataRetentionPeriod=DATA_RETENTION_PERIOD;
    linktobalancing=LINKTOBALANCING;
    linktoexception=LINKTOEXCEPTION;
    isencrypted=ISENCRYPTED;
    datalimitedtopurpose=DATALIMITEDTOPURPOSE;
    hasdpiaperformed=HASDPIAPERFORMED;
    canconsumersaccessdata=CANCONSUMERSACCESSDATA;
    cryptoAlgo=CRYPTO_ALGO;
    datastore=DATASTORE;
    dataclassification=DATACLASSIFICATION;
    datacategory=DATACATEGORY;
    dataidentificationcategory=DATAIDENTIFICATIONCATEGORY;
    basisForProcessing=BASIS_FOR_PROCESSING;
    timeUnit=TIME_UNIT;
    retentionProcess=RETENTION_PROCESS;
    customerdatacategory=CUSTOMER_DATA_CATEGORY;
    comments=COMMENTS_FIELD;
    statusField=STATUS_FIELD;
    isParent=ISPARENT;
    @track bShowModal = false;
    @track admscrumteamId;
    saveDataAssetRecordId(event){
    this.admscrumteamId=event.detail.currentRecId;
    }
    /* javaScipt functions start */ 
    openModal(event) { 
        //saveADMScrumTeamInDataAsset({admscrumteamid:this.admscrumteamId,dataassetid:event.detail.id}).then(result=>console.log(result)).catch(error=>console.log(error));   
        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                recordId: event.detail.id,
                objectApiName: 'CDI_Data_Asset__c',
                actionName: 'view',
            },
        });
       
    }
 
    closeModal() {    
        // to close modal window set 'bShowModal' tarck value as false
        this.bShowModal = false;
        location.reload();
       
    }
    makerequired() {
        if (this.dataassetobjname != null) {
            return true;
        } else {
            return false;
        }
    }
}
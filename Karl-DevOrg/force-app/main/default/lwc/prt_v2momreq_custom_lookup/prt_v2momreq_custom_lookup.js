import { LightningElement, api, track } from 'lwc';
import findRecords from '@salesforce/apex/PRT_V2MOMChangeRequestCtrl.findRecords';

export default class Prt_v2momreq_custom_lookup extends LightningElement {
    @api objectName = 'Account';
    @api fieldName = 'Name';
    @api selectRecordId = '';
    @api selectRecordName;
    @api v2momId = '';
    @api methodId = '';
    @api Label;
    @api searchRecords = [];
    @api required = false;
    @api iconName = 'action:new_account'
    @api LoadingText = false;
    @track txtclassname = 'slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click';
    @track messageFlag = false;
    @api iconFlag;
    @api clearIconFlag = false;
    @api inputReadOnly = false;
    @api disable;
   

    searchField(event) {
        var currentText = event.target.value;
        this.LoadingText = true;
        
        findRecords({ ObjectName: this.objectName, fieldName: this.fieldName, value: currentText, v2momId: this.v2momId, methodId: this.methodId})
        .then(result => {
            this.searchRecords= result;
            this.LoadingText = false;
            
            this.txtclassname =  result.length > 0 ? 'slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click slds-is-open' : 'slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click';
            if(currentText.length > 0 && result.length == 0) {
                this.messageFlag = true;
            }
            else {
                this.messageFlag = false;
            }

            if(this.selectRecordId != null && this.selectRecordId.length > 0) {
                this.iconFlag = false;
                this.clearIconFlag = true;
            }
            else {
                this.iconFlag = true;
                this.clearIconFlag = false;
            }
        })
        .catch(error => {
            console.log('-------error-------------'+error);
            console.log(error);
        });
        
    }
    
    setSelectedRecord(event) {
        var currentRecId = event.currentTarget.dataset.id;
        var selectName = event.currentTarget.dataset.name;
        var descOrComm = event.currentTarget.dataset.desc;
        this.txtclassname =  'slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click';
        this.selectRecordName = selectName;
        this.selectRecordId = currentRecId;
        this.iconFlag = false;
        this.clearIconFlag = true;
        this.inputReadOnly = true;
        const selectedEvent = new CustomEvent('selected', { detail: {selectName, currentRecId, descOrComm}, });
        // Dispatches the event.
        this.dispatchEvent(selectedEvent);
    }
    
    @api resetData() {
        this.selectRecordName = "";
        this.selectRecordId = "";
        this.inputReadOnly = false;
        this.iconFlag = true;
        this.clearIconFlag = false;
        var selectName = "";
        var currentRecId = "";
        var descOrComm = "";
        const resetEvent = new CustomEvent('reset', {detail: {selectName, currentRecId, descOrComm},});
        this.dispatchEvent(resetEvent);
    }
}
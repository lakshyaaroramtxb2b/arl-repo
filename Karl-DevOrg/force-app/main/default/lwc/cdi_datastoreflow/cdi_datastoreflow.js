import { LightningElement,track } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import DATASTOREDATAFLOW_OBJECT from '@salesforce/schema/CDI_Data_Store_Data_Flow__c';
import SERVICEVALUE from '@salesforce/schema/CDI_Data_Store_Data_Flow__c.Service__c';
import DATASTORE_FIELD from '@salesforce/schema/CDI_Data_Store_Data_Flow__c.Data_Store__c';
import TARGETDATASTORE_FIELD from '@salesforce/schema/CDI_Data_Store_Data_Flow__c.Target_Data_Store__c';



/**
 * Creates Account records.
 */
export default class DataStoreFlow extends NavigationMixin(LightningElement) {
    datastoredataflowflowObject = DATASTOREDATAFLOW_OBJECT;
    servicevalue = SERVICEVALUE;
    datastoreField = DATASTORE_FIELD;
    targetdatastoreField=TARGETDATASTORE_FIELD; 
  
    
   
    @track bShowModal = false;
 
    /* javaScipt functions start */ 
    openModal(event) {    
        // to open modal window set 'bShowModal' tarck value as true
         
        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                recordId: event.detail.id,
                objectApiName: 'CDI_Data_Store_Data_Flow__c',
                actionName: 'view',
            },
        });
        
    }
 
    closeModal() {    
        // to close modal window set 'bShowModal' tarck value as false
        this.bShowModal = false;
        location.reload();
    }
    /* javaScipt functions end */ 
}
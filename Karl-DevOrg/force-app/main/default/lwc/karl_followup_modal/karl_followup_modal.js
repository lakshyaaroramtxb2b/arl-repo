import { NavigationMixin } from 'lightning/navigation';
import { api, LightningElement, track } from 'lwc';

/**
 * @author Ben Harvie
 * @email ben.harvie@salesforce.com
 * @description This LWC Component Builds a Modal for Create and Update Followups
 */

export default class Karl_followup_modal extends NavigationMixin(LightningElement) {
    @api closeLabel = 'Cancel';
    @api submitLabel = 'Save';
    @api targetrecord;
    @api currentuser;
    @api auditoruser;
    @api auditteam;
    @api auditcycle;
    @api auditscope;
    @track isRendered = false;
    recordLink;
    closeModal() {
        this.dispatchEvent(new CustomEvent('close'));
    }

    handleSubmit() {
        this.dispatchEvent(new CustomEvent('submit'));
    }

    connectedCallback(){
        this[NavigationMixin.GenerateUrl]({
            type: 'standard__recordPage',
            attributes: {
                recordId: this.targetrecord,
                actionName: 'view'
            }
        }).then(url => {
            this.recordLink = url;
        });
    }
    get auditScope(){
        if(this.targetrecord == undefined){
            return (this.auditscope != 'allScopes' && this.auditscope != 'noScopes') ? this.auditscope : null;
        }else{
            return;
        }
    }

    get grcUser(){
        if(this.targetrecord == undefined){
            return this.auditoruser ? null : this.currentuser;
        }else{
            return;
        }
    }

    get auditUser(){
        if(this.targetrecord == undefined){
            return this.auditoruser ? this.currentuser : null;
        }else{
            return;
        }
    }
}
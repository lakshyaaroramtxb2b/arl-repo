import LightningDatatable from 'lightning/datatable';
import KarlPicklistTemplate from './karlpicklisttemplate.html';
import KarlRichTextTemplate from './karlrichtexttemplate.html';
import KarlCheckboxTemplate from './karlcheckboxtemplate.html';
import KarlLabelTemplate from './karllabeltemplate.html';
import { loadStyle } from 'lightning/platformResourceLoader';
import CustomCSS from '@salesforce/resourceUrl/karlPicklist';

export default class Karl_car_datatable_picklist extends LightningDatatable {
    hasLoadedStyle = false;

    static customTypes = {   //it show that we are creating custom type
        picklist: {  // type of custom element
            template: KarlPicklistTemplate,
            typeAttributes: ['label', 'placeholder', 'options', 'value', 'context', 'field'],
            standardCellLayout: false,
        },
        richtext: {
            template: KarlRichTextTemplate,
            typeAttributes: [ 'value', 'backgroundclass', 'wrap'],
            standardCellLayout: false,
        },
        checkbox:{
            template: KarlCheckboxTemplate,
            standardCellLayout: true
        },
        label:{
            template: KarlLabelTemplate,
            typeAttributes: [ 'value', 'labelclass'],
            standardCellLayout: true
        }
    };

    renderedCallback() {
        if (LightningDatatable.prototype.renderedCallback) {
            // Source: https://salesforce.stackexchange.com/questions/318454/how-to-increase-row-height-in-custom-lightning-data-table-in-lwc
            // Note that this fixes the headeraction error
            LightningDatatable.prototype.renderedCallback.call(this);
        }
        if (!this.hasLoadedStyle) {
            this.hasLoadedStyle = true;
            Promise.all([
                loadStyle(this, CustomCSS),
            ]).then(() => { })
        }
    }
}
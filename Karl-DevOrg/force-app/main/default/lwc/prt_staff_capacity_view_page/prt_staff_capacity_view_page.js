import { LightningElement, track } from 'lwc';
import getColumnHeadings from '@salesforce/apex/PRT_StaffCapacityController.getColumnHeadings';
import getCapacityData from '@salesforce/apex/PRT_StaffCapacityController.getCapacityData';
import getCapacityRange from '@salesforce/apex/PRT_StaffCapacityController.getCapacityRange';
import getAssignmentData from '@salesforce/apex/PRT_StaffCapacityController.getAssignmentData';
import getFilterPicklistValues from '@salesforce/apex/PRT_StaffCapacityController.getFilterPicklistValues';
import getProjectData from '@salesforce/apex/PRT_StaffCapacityController.getProjectData';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class Prt_staff_capacity_view_page extends LightningElement {
    @track staffCapacity = [];
    @track projectData = [];
    @track columnHeadings = []; 
    @track capacityRange = [];
    @track whereClause;
    @track sumOfAllocation;
    @track searchValue;
    @track period;
    @track category;
    @track staffPart;
    @track projectPart;
    @track advancedSearch = true;
    @track currentDate = new Date();
    @track assignmentData;
    @track staffName;
    @track isLoading = false;
    @track filterValues = [];
    @track portfolioValue = '';
    @track programValue = '';
    @track projectValue = '';
    @track projectManagerValue = '';
    @track operationValue = '';
    @track allocationValue = '';
    @track assignmentDataColumns = [
        {label: 'Assignment Name', fieldName: 'assignmentURL', type: 'url', typeAttributes: {label: {fieldName: 'Name'}, target: '_blank'}},
        {label: 'Program', fieldName: 'Program_Name__c', type: 'text'},
        {label: 'Project', fieldName: 'projectURL', type: 'url', typeAttributes: {label: {fieldName: 'Project_Name__c'}, target: '_blank'}},
        {label: 'Operations', fieldName: 'OperationName', type: 'text'},
        {label: 'Start Date', fieldName: 'Start_Date__c', type: 'date-local'},
        {label: 'End Date', fieldName: 'End_Date__c', type: 'date-local'},
        {label: '% of allocation', fieldName: 'Percentage_of_Capacity__c', type: 'text'},
    ];

    formatDate(date) {
        var d = new Date(date);
        var month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();
    
        if (month.length < 2) 
            month = '0' + month;
        if (day.length < 2) 
            day = '0' + day;
    
        return [year, month, day].join('-');
    }

    async getStaffCapacityTable(myDate, type, wherePart, allocation, searchEmp) {
        if(this.category === 'Employee') {
            await getCapacityData({
                currDate: myDate,
                type: type,
                wherePart: wherePart,
                sumOfAllocation: allocation,
                searchEmployee: searchEmp
            })
            .then(result => {
                console.log('staffCapacity: '+JSON.stringify(result));
                if(result != null) {
                    this.staffCapacity = result;
                }
                else {
                    this.staffCapacity = [];
                }
            })
        }
        else if(this.category === 'Project') {
            await getProjectData({
                currDate: myDate,
                type: type,
                wherePart: wherePart,
                sumOfHours: allocation,
                searchProject: searchEmp
            })
            .then(result => {
                console.log('projectData: '+JSON.stringify(result));
                if(result != null) {
                    this.projectData = result;
                }
                else {
                    this.projectData = [];
                }
            })
        }

        await getColumnHeadings({
            currDate: myDate,
            type: type
        })
        .then(result => {
            console.log('columnHeadings: '+JSON.stringify(result));
            let dataCol = result[0].dataColHeading;
            if(dataCol != null || dataCol != undefined) {
                this.columnHeadings = result;
            }
        })

        const headerColumns = this.template.querySelector('.header-columns');
        const dataColumns = this.template.querySelector('.data-columns');
        const headerElements = this.template.querySelectorAll('.flex-column__data');
        const rowElements = this.template.querySelectorAll('.flex-row__data');

        if(headerColumns!=null && headerColumns.classList.contains('slds-grid_align-space')) {
            headerColumns.classList.remove('slds-grid_align-space');
        }
        if(dataColumns!=null && dataColumns.classList.contains('slds-grid_align-space')) {
            dataColumns.classList.remove('slds-grid_align-space');
        }
        if(headerElements!=null) {
            headerElements.forEach(item => {
                if(!item.classList.contains('flex-1')) {
                    item.classList.add('flex-1');
                }
            })
        }
        if(rowElements!=null) {
            rowElements.forEach(item => {
                if(!item.classList.contains('flex-1')) {
                    item.classList.add('flex-1');
                }
            })
        }
    }

    async connectedCallback() {
        this.isLoading=true;
        this.period = 'Week';
        this.category = 'Employee';
        this.staffPart = true;
        this.projectPart = false;
        this.whereClause = '';
        this.sumOfAllocation = 0;
        this.searchValue = '';
        let currDate = new Date();
        console.log("currentDate: "+currDate);
        this.currentDate = currDate;
        
        let formattedDate = this.formatDate(currDate);
        console.log("formattedDate: "+formattedDate);

        getCapacityRange({}).then(result => {
            if(result != null || result != undefined) {
                this.capacityRange = result;
            }
        })

        await this.getStaffCapacityTable(formattedDate, "Week", this.whereClause, this.sumOfAllocation, this.searchValue);  
        
        getFilterPicklistValues({
            category: this.category
        }).then(result => {
            if(result!=null || result != undefined) {
                this.filterValues = result;
            }
        });

        const periodButton = this.template.querySelectorAll('.button__period');
        periodButton.forEach(function(data) {
            if(data.label === 'Week') {
                data.variant = 'Brand';
            }
            else {
                data.variant = 'Neutral';
            }
        })

        const typeButton = this.template.querySelectorAll('.button__type');
        typeButton.forEach(function(data) {
            if(data.label === 'Employee') {
                data.variant = 'Brand';
            }
            else {
                data.variant = 'Neutral';
            }
        })

        this.isLoading=false;
    }

    async handlePeriod(event) {
        this.isLoading = true;
        let currDate = new Date();
        console.log("currentDate: "+currDate);
        this.currentDate = currDate;
        let formattedDate = this.formatDate(currDate);
        console.log("formattedDate: "+formattedDate);
        const periodLabel = event.target.label;
        console.log("period: "+periodLabel);
        this.period = periodLabel;

        const periodButton = this.template.querySelectorAll('.button__period');
        periodButton.forEach(function(data) {
            if(data.label === periodLabel) {
                data.variant = 'Brand';
            }
            else {
                data.variant = 'Neutral';
            }
        })

        let projectDataVal = this.projectData;
        const accordianBtn = this.template.querySelectorAll('lightning-button-icon');
        await accordianBtn.forEach(function(data) {
            data.iconName = 'utility:chevronright';
            projectDataVal[data.value].expanded = false;
        }); 
        
        this.projectData = projectDataVal;

        await this.getStaffCapacityTable(formattedDate, periodLabel, this.whereClause, this.sumOfAllocation, this.searchValue); 

        this.isLoading = false;
    }

    async handlePrevious(event) {
        this.isLoading = true;
        console.log('currentDate ======== '+this.currentDate);
        if(this.period == 'Week') {
            this.currentDate.setDate(this.currentDate.getDate() - 7);
        }
        else if(this.period == 'Month') {
            this.currentDate.setMonth(this.currentDate.getMonth() - 1);
        }
        else if(this.period == 'Fiscal Quarter') {
            this.currentDate.setMonth(this.currentDate.getMonth() - 3);
        }
        else if(this.period == 'Fiscal Year') {
            this.currentDate.setFullYear(this.currentDate.getFullYear() - 1);
        }

        console.log('currentDateOnPrevious: '+this.currentDate);
        let formattedDate = this.formatDate(this.currentDate);
        console.log("formattedDate: "+formattedDate);
        await this.getStaffCapacityTable(formattedDate, this.period, this.whereClause, this.sumOfAllocation, this.searchValue);
        this.isLoading = false;
    }

    async handleNext(event) {
        this.isLoading = true;
        if(this.period == 'Week') {
            this.currentDate.setDate(this.currentDate.getDate() + 7);
        }
        else if(this.period == 'Month') {
            this.currentDate.setMonth(this.currentDate.getMonth() + 1);
        }
        else if(this.period == 'Fiscal Quarter') {
            this.currentDate.setMonth(this.currentDate.getMonth() + 3);
        }
        else if(this.period == 'Fiscal Year') {
            this.currentDate.setFullYear(this.currentDate.getFullYear() + 1);
        }

        console.log('currentDateOnNext: '+this.currentDate);
        let formattedDate = this.formatDate(this.currentDate);
        console.log("formattedDate: "+formattedDate);
        await this.getStaffCapacityTable(formattedDate, this.period, this.whereClause, this.sumOfAllocation, this.searchValue);
        this.isLoading = false;
    }

    handlePercentClick(event) {
        let selectedStaff = event.currentTarget.dataset.staff;
        let assignmentIds = event.currentTarget.dataset.assignments;
        console.log('Staff Name: '+selectedStaff);
        console.log('Assignment Ids: '+assignmentIds);

        getAssignmentData({
            assignmentIds: assignmentIds,
            staff: selectedStaff
        })
        .then(result => {
            if(result != null || result != undefined) {
                this.staffName = selectedStaff;
                result.forEach(function(record) {
                    record.assignmentURL = '/'+record.Id;
                    record.projectURL = '/lightning/r/'+record.Project__c+'/related/Assignments__r/view';
                    if(record.Operations__c === undefined)
                        record.OperationName = '';
                    else 
                        record.OperationName = record.Operations__r.Name;
                });
                this.assignmentData = result;
            }
            else {
                const toastEvent = new ShowToastEvent({
                    title: 'INFO!!',
                    message: 'No Assignments in this period.',
                    variant: 'info'
                });
                this.dispatchEvent(toastEvent);
            }
        });
    }

    async handleCategory(event) {
        this.isLoading = true;
        this.whereClause = '';
        this.sumOfAllocation = 0;
        this.searchValue = '';
        this.assignmentData = null;
        this.template.querySelector('lightning-input').value = '';
        let currDate = new Date();
        console.log("currentDate: "+currDate);
        this.currentDate = currDate;   
        let formattedDate = this.formatDate(currDate);
        console.log("formattedDate: "+formattedDate);

        const type = event.target.label;
        this.category = type;

        const typeButton = this.template.querySelectorAll('.button__type');
        typeButton.forEach(function(data) {
            if(data.label === type) {
                data.variant = 'Brand';
            }
            else {
                data.variant = 'Neutral';
            }
        })

        getFilterPicklistValues({
            category: type
        }).then(result => {
            if(result!=null || result != undefined) {
                this.filterValues = result;
            }
        });

        getCapacityRange({}).then(result => {
            if(result != null || result != undefined) {
                this.capacityRange = result;
            }
        })

        if(type === 'Employee') {
            this.template.querySelector('lightning-input').placeholder = 'Search employee...';
            this.staffPart = true;
            this.projectPart = false;
        }  
        else {
            this.template.querySelector('lightning-input').placeholder = 'Search project...';
            this.staffPart = false;
            this.projectPart = true;
        }

        await this.getStaffCapacityTable(formattedDate, this.period, this.whereClause, this.sumOfAllocation, this.searchValue);  

        this.isLoading = false;
    }

    async handleFilters(event) {
        this.isLoading = true;
        let name = event.currentTarget.name;
        let value = event.detail.value;
        let wherePart = '';
        let allocationSum = 0;

        console.log('name: '+name);
        console.log('value: '+value);
        console.log('wherePart: '+wherePart);
        console.log('filterValues: '+this.filterValues);

        this.filterValues.forEach(function(data) {
            console.log('data.name: '+data.name);
            if(data.name === name) {
                data.currentValue = value;
            }
        });

        console.log('whereClause: '+this.whereClause);
        console.log('sumOfAllocation: '+this.sumOfAllocation);
        this.filterValues.forEach(function(data) {
            if(data.name === 'Program' && data.currentValue != '') {
                wherePart = wherePart + 'AND Assignment__r.Program_Name__c = '+'\''+data.currentValue+'\'';
            }
            else if(data.name === 'Project' && data.currentValue != '') {
                wherePart = wherePart + 'AND Assignment__r.Project_Name__c = '+'\''+data.currentValue+'\'';
            }
            else if(data.name === 'Project Manager' && data.currentValue != '') {
                wherePart = wherePart + 'AND Assignment__r.Project__r.Project_Manager__r.Name = '+'\''+data.currentValue+'\'';
            }
            else if(data.name === 'Operations' && data.currentValue != '') {
                wherePart = wherePart + 'AND Assignment__r.Operations__r.Name = '+'\''+data.currentValue+'\'';
            }
            else if((data.name === 'Sum of Allocations' || data.name === 'Sum of Hours') && data.currentValue != '') {
                allocationSum = parseInt(data.currentValue);
            }
        });

        this.whereClause = wherePart;
        this.sumOfAllocation = allocationSum;
        console.log('whereClause: '+this.whereClause);
        console.log('sumOfAllocation: '+this.sumOfAllocation);
        let formattedDate = this.formatDate(this.currentDate);
        await this.getStaffCapacityTable(formattedDate, this.period, this.whereClause, this.sumOfAllocation, this.searchValue);
        this.isLoading = false;
    }

    async handleSearch(event) {
        this.isLoading = true;
        let empVal = event.currentTarget.value;
        console.log('Employee/Project Value: '+empVal);

        if(empVal != '') {
            this.searchValue = empVal;
        }
        else {
            this.searchValue = '';
        }
        let formattedDate = this.formatDate(this.currentDate);
        await this.getStaffCapacityTable(formattedDate, this.period, this.whereClause, this.sumOfAllocation, this.searchValue);
        this.isLoading = false;
    }

    async toggleAccordian(event) {
        const val = event.currentTarget.value;
        let projectDataVal = this.projectData;
        const accordianBtn = this.template.querySelectorAll('lightning-button-icon');
        await accordianBtn.forEach(function(data) {
            if(data.value === val) {
                if(data.iconName === 'utility:chevrondown') {
                    data.iconName = 'utility:chevronright';
                    projectDataVal[data.value].expanded = false;
                }
                else if(data.iconName === 'utility:chevronright') {
                    data.iconName = 'utility:chevrondown';
                    projectDataVal[data.value].expanded = true;
                }
            }
        }); 
        
        this.projectData = projectDataVal;

        const dataColumns = this.template.querySelector('.data-columns');
        const rowElements = this.template.querySelectorAll('.flex-row__data');
        if(dataColumns!=null && dataColumns.classList.contains('slds-grid_align-space')) {
            dataColumns.classList.remove('slds-grid_align-space');
        }
        if(rowElements!=null) {
            rowElements.forEach(item => {
                if(!item.classList.contains('flex-1')) {
                    item.classList.add('flex-1');
                }
            })
        }
    }
}
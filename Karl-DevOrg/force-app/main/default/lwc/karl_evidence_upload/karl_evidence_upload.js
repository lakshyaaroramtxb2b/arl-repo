import {
    LightningElement,
    track,
    api
} from 'lwc';
import {
    ShowToastEvent
} from 'lightning/platformShowToastEvent';
import getCurrentEvidenceInfo from '@salesforce/apex/KARL_EvidenceUploadController.getCurrentEvidenceInfo';
import getEvidenceContentVersionRecords from '@salesforce/apex/KARL_EvidenceUploadController.getEvidenceContentVersionRecords';
import saveContentVersionRecords from '@salesforce/apex/KARL_EvidenceUploadController.saveContentVersionRecords';
import submitContentVersionRecords from '@salesforce/apex/KARL_EvidenceUploadController.submitContentVersionRecords';
import updateDocument from '@salesforce/apex/KARL_EvidenceUploadController.updateDocument';
import getContentDistributionForFile from '@salesforce/apex/KARL_EvidenceUploadController.getContentDistributionForFile';
import deleteFile from '@salesforce/apex/KARL_EvidenceUploadController.deleteFile';
import fetchPicklist from '@salesforce/apex/KARL_Utility.fetchPicklist';
import ARL_Evidence_File_Submit_Checkbox_Text from "@salesforce/label/c.ARL_Evidence_File_Submit_Checkbox_Text";
import Id from '@salesforce/user/Id';
import {
    showAjaxErrorMessage
}
from 'c/karlUtil';
export default class Karl_evidence_upload extends LightningElement {
    @api recordId;
    @track data = [];
    @track isTableEmpty = false;
    @track submitted = false;
    @track statusOptions = [];
    @track showSpinner = false;
    @track showSubmitModal = false;
    @track evidenceDetails = {};
    @track agreedTermsAndConditions = false;
    @track deleteFileId;
    @track showDeleteModal = false;
    @track showPageOnLoad = false;
    @track errorLandingPage = false;
    @track showRequestAccessModal = false;
    @track submitRequestAccessMsg = 'Request Access';
    @track evidenceRequestInfo = {};
    userId = Id;
    ARL_Evidence_File_Submit_Checkbox_Text = ARL_Evidence_File_Submit_Checkbox_Text;
    submitConfirmationMsg = 'Are you sure you want to submit to the GRC team ?';
    deleteFileConfirmationMsg = 'Are you sure you want to delete the evidence attachment ? ';
    connectedCallback() {
        let fullUrl = window.location.href;
        let newURL = new URL(fullUrl).searchParams;
        if (!this.recordId) {
            this.recordId = newURL.get('id');
        }
        getCurrentEvidenceInfo({
            evidenceRecordId : this.recordId
        }).then(response=>{
            this.evidenceRequestInfo['label'] = response.Name;
            this.evidenceRequestInfo['value'] = response.Id;
            this.evidenceRequestInfo['secondaryField'] = response.KARL_Request_Name__c;
            this.evidenceRequestInfo['cyclearlitemid'] = response.KARL_Cycle_ARL_Item__c;
        }).catch(error=>{
            showAjaxErrorMessage(this, error);
        });
        this.showSpinner = true;
        fetchPicklist({
                objectName: 'ContentVersion',
                fieldName: 'Status__c'
            })
            .then(result => {
                this.statusOptions = result;
            })
            .catch(error => {
                console.log('error picklist >> ' + JSON.stringify(error));
                showAjaxErrorMessage(this, error);
            }).finally(() => {
                this.showSpinner = false;
            });
        this.loadColumns();
        this.refreshTableData();
    }
    /**
     * @author Mahima Aggarwal
     * @email mahima.aggarwal@mtxb2b.com
     * @desc This method loads columns
     */
    loadColumns() {
        this.columnConfiguration = [];
        this.columnConfiguration.push({
            heading: 'ATTACHMENT',
            fieldApiName: 'Attachment"',
            style: 'width:20%;'
        });
        this.columnConfiguration.push({
            heading: 'EVIDENCE NOTES',
            fieldApiName: 'Evidence_Notes',
            style: 'width:20%;'
        });
        this.columnConfiguration.push({
            heading: 'GRC NOTES',
            fieldApiName: 'Assignee_Notes',
            style: 'width:20%;'
        });
        this.columnConfiguration.push({
            heading: 'STATUS',
            fieldApiName: 'Status',
            style: 'width:20%;'
        });
        this.columnConfiguration.push({
            heading: 'UPLOADED BY',
            fieldApiName: 'Uploaded_By',
            style: 'width:20%;'
        });
    }
    /**
     * @author Mahima Aggarwal
     * @email mahima.aggarwal@mtxb2b.com
     * @desc This method refresh data
     */
    refreshTableData() {
        this.showSpinner = true;
        console.log('refreshTableDataCalled>>1' + this.recordId);
        this.data = [];
        this.evidenceDetails = {};
        getEvidenceContentVersionRecords({
                evidenceId: this.recordId
            })
            .then(result => {
                if (result) {
                    console.log('In side-> ', JSON.stringify(result));
                    if(result.fileWrapperList){
                        this.data = result.fileWrapperList;
                    }
                    
                    //console.log('data' + JSON.stringify(this.data));
                    if(result.fileWrapperList){
                        this.isTableEmpty = result.fileWrapperList.length === 0 ? true : false;
                    }
                    
                    if(result.submitted){
                        this.submitted = result.submitted;
                    }
                    
                    this.evidenceDetails = result.evidenceDetails;
                    let i = 0;
                    for (i = 0; i < this.data.length; i++) {
                        this.data[i].showDeleteIcon = (this.data[i].status != 'Approved') ? true : false;
                        this.data[i].evidenceNotesDisabled = (this.data[i].status == 'Approved' || this.data[i].status == 'Rejected') ? true : false;
                    }
                    this.showPageOnLoad = true;
                    this.errorLandingPage = false;
                } else {
                    this.errorLandingPage = true;
                    //showAjaxErrorMessage(this, 'You have Insufficient Access on this Record');
                }
                // if (this.submitted) {
                //     this.agreedTermsAndConditions = true;
                // }
            })
            .catch(error => {
                console.log("error occured >> 1" + JSON.stringify(error));
                showAjaxErrorMessage(this, error);
            }).finally(() => {
                this.showSpinner = false;
            });
    }
    /**
     * @author Mahima Aggarwal
     * @email mahima.aggarwal@mtxb2b.com
     * @desc This method handles save of File to database
     */
    handleSaveClick(event) {
        console.log('data>' + JSON.stringify(this.data));
        saveContentVersionRecords({
                jsonMap: JSON.stringify(this.data),
                evidenceId: this.recordId
            })
            .then(result => {
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Success',
                        message: 'Files Saved',
                        variant: 'success'
                    })
                );
                this.refreshTableData();
            })
            .catch(error => {
                console.log("error occurred >> " + JSON.stringify(error));
                showAjaxErrorMessage(this, error);
            });
    }
    /**
     * @author Mahima Aggarwal
     * @email mahima.aggarwal@mtxb2b.com
     * @desc This method handles save of File to database
     */
    handleSubmitClick(event) {
        //console.log('data>' + JSON.stringify(this.data));
        if (this.isTableEmpty) {
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Error!',
                    message: 'Please upload atleast one evidence attachment before submitting ',
                    variant: 'error'
                })
            );
            return;
        }
        submitContentVersionRecords({
                jsonMap: JSON.stringify(this.data),
                evidenceId: this.recordId
            })
            .then(result => {
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Success',
                        message: 'Evidence Submitted',
                        variant: 'success'
                    })
                );
                this.showSubmitModal = false;
                this.refreshTableData();
            })
            .catch(error => {
                this.showSubmitModal = false;
                console.log("error occured>> " + JSON.stringify(error));
                showAjaxErrorMessage(this, error);
            });
    }
    /**
     * @author Mahima Aggarwal
     * @email mahima.aggarwal@mtxb2b.com
     * @desc This method handles File fields change
     */
    handleChange(event) {
        var indexClicked = event.currentTarget.getAttribute("data-index");
        if (event.target.name == 'status') {
            this.data[indexClicked].status = event.detail.value;
        } else if (event.target.name == 'evidenceNotes') {
            this.data[indexClicked].evidenceNotes = event.target.value;
        } else if (event.target.name == 'assigneeNotes') {
            this.data[indexClicked].assigneeNotes = event.target.value;
        } else if (event.target.name === 'agreedTermsAndConditions') {
            this.agreedTermsAndConditions = event.target.checked;
        }
    }
    /**
     * @author Mahima Aggarwal
     * @email mahima.aggarwal@mtxb2b.com
     * @desc This method opens submit modal
     */
    openSubmitModal(event) {
        if (this.isValid()) {
            this.showSubmitModal = true;
        }
    }
    /**
     * @author Mahima Aggarwal
     * @email mahima.aggarwal@mtxb2b.com
     * @desc This method closes submit modal
     */
    closeModal() {
        this.showSubmitModal = false;
    }
    /**
     * @author Mahima Aggarwal
     * @email mahima.aggarwal@mtxb2b.com
     * @desc This method handles File upload(It updates sharing of File to "Set by Record")
     */
    handleUploadFinished(event) {
        const uploadedFiles = event.detail.files;
        let uploadedFileIds = [];
        let i;
        for (i = 0; i < uploadedFiles.length; i++) {
            uploadedFileIds.push(uploadedFiles[i].documentId);
        }
        console.log('uploadedFileIds--> ',uploadedFileIds);
        updateDocument({
            docIds: uploadedFileIds,
            evidenceId: this.recordId
        }).then(result => {
            this.refreshTableData();
            console.log('test2');
        }).catch(error => {
            console.log('error in file update>>2' + JSON.stringify(error));
            showAjaxErrorMessage(this, error);
        }).finally(() => {
            console.log('test sw2');
        })
    }
    /**
     * @author Mahima Aggarwal
     * @email mahima.aggarwal@mtxb2b.com
     * @desc This method is used to validate input
     */
    isValid() {
        let valid = false;
        valid = [...this.template.querySelectorAll("lightning-input")].reduce(
            (validSoFar, input) => {
                input.reportValidity();
                return validSoFar && input.checkValidity();
            },
            true
        );
        return valid;
    }
    /**
     * @author Mahima Aggarwal
     * @email mahima.aggarwal@mtxb2b.com
     * @desc This method shows preview of file that is uploaded
     */
    handleDownloadFile(event) {
        getContentDistributionForFile({
                contentDocumentId: event.currentTarget.dataset.id,
                evidenceId: this.recordId
            })
            .then(response => {
                if(response){
                    window.open(response.DistributionPublicUrl);
                }else{
                    showAjaxErrorMessage(this, 'Insufficient Access');
                }
               
            })
            .catch(error => {
                console.log('error in file open>>' + JSON.stringify(error));
                showAjaxErrorMessage(this, error);
            })
    }
    handleRequestAccess(){
        this.showRequestAccessModal = true;
    }
    closeRequestAccessModal(){
        this.showRequestAccessModal = false;
    }
    handleRequestAccessYesClick(event){
        this.showRequestAccessModal = false;
    }
    //Delete Functioanlity
    openDeleteModal(event) {
        this.deleteFileId = event.currentTarget.dataset.id
        this.showDeleteModal = true;
    }
    closeDeleteModal() {
        this.deleteFileId = '';
        this.showDeleteModal = false;
    }
    deleteFile(event) {
        deleteFile({
            recordId: this.deleteFileId,
            evidenceId: this.recordId
        }).then(result => {
            this.showDeleteModal = false;
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Success',
                    message: 'File successfully deleted',
                    variant: 'success'
                })
            );
            this.refreshTableData();
        }).catch(error => {
            this.showDeleteModal = false;
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Error',
                    message: 'Error occurred while deleting record.',
                    variant: 'error'
                })
            );
        })
    }
}
import {
    LightningElement, 
    api, 
    wire, 
    track
} from 'lwc';
import {
    refreshApex
} from '@salesforce/apex';
import {
    ShowToastEvent
} from 'lightning/platformShowToastEvent';
import {
    getRecordNotifyChange
} from 'lightning/uiRecordApi';
import {
    CurrentPageReference,
    NavigationMixin
} from 'lightning/navigation';
import {
    arrayRemove, getDefaultURLValue, scopeOptions, cycleOptions, teamOptions, setLoadingSpinner
} from 'c/karlUtil';

/**
 * @author Ben Harvie
 * @email ben.harvie@salesforce.com
 * @description This LWC Component Builds a in-line editable datatable. Note that this is a unique
 * LWC for the KARL app and is not reusable for other purposes. The LWC is utilized on both
 * the community as well as standard salesforce environment.
 */

// Import Apex Classes
import getEvidenceReport from '@salesforce/apex/KARL_ARLReportController.getReport';
import updateStatus from '@salesforce/apex/KARL_ARLReportController.updateStatus';
export default class KARL_ARL_Datatable extends NavigationMixin(LightningElement) {
    @api recordId;

    // Metadata variables
    @api hideScopeCol;
    @api hideControlsCol;
    @api hideActionsCol;
    @api auditorsView = false;
    @api defaultCycle;
    @api defaultScope;
    @api defaultTeam;
    @api defaultSortBy;
    @api defaultSortDirection;

    // Wire Data Output
    @track auditCycleOptions = [];
    @track auditScopeOptions = [];
    @track auditTeamOptions = [];
    @track filteredEvidenceData = [];
    @track evidenceWireData; // Full result (for refreshApex)

    // Wire Data Input / Management
    @track auditcycle;
    @track auditscope;
    @track auditteam;
    @track totalRows;
	@track error;
    @track records;
    fieldApis               = {
        Cycle_Request_Status__c: 'sfdcStatus',
        KARL_Status__c: 'auditorStatus',
        SFDC_Comments__c: 'sfdcComments',
        KARL_Auditor_Status_Comments__c: 'auditorComments',
        Request_Link__c: 'requestLink',
        }

    // Button Management
    @track areaofcompliance;
    @track openclosed;
    @track assignedtome;
    @track typefilter;
    defaultOpenclosed           = 'All';
    defaultAssignedToMe         = 'All_Assigned';
    defaultAreaofcompliance     = 'All_Aoc';
    defaultTypeFilter           = 'typeAll';
    buttonsOpenclosedAuditors   = ['All_A','Pending_A','Open_A','NotStarted_A','InProgress_A','Closed_A','FollowUp_A','NA_A'];
    buttonsOpenclosedInternal   = ['All','Open','Provided','FollowUp', 'Closed','NA'];
    buttonsAreas                = ['All_Aoc', 'SOC','FedRAMP','PCI DSS','iRAP'];
    buttonsAreasMenu            = ['ICT', 'ICTExclude', 'ICTUnique','METI','SOX','ISO_All','ISO 27001','ISO 27017','ISO 27018','NEN7510','ASIP Sante HDS','ENS','SPSCBv3_2'];
    buttonsAssigned             = ['All_Assigned','AssignedToMe'];
    toggleControlAreas          = ['SOC','All_Aoc','ICT', 'ICTExclude','ICTUnique',undefined];

    // Sorting variables
    @track sortDirection;
    @track sortBy;

    // Miscellaneous
    @track isLoading;
    @track togglecontrols = false;
    @track paramsSet = false;
    @track sfdcEdits = false;
    @track auditorEdits = false;
    @track saveButton = false;
    @track draftValues = [];
    @track picklistChanges = [];
    searchFields    = ['searchreq','searchcontrol','searchwt','searchdesc'];
    currentPageReference;

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description utilize connectedCallback to generate on load
     */
    connectedCallback() {
        if(!this.auditorsView){
            // Set the editable fields for Salesforce instead of Auditors
            this.sfdcEdits = true;
            this.defaultTeam = this.defaultTeam == undefined ? '' : this.defaultTeam;
            this.auditteam = ''; // required to process the wire (can't be undefined)
        }else{
            this.auditorEdits = true;
            this.defaultOpenclosed = 'All_A'; // For toggle rendering on default load
        }

        // Set connected to for setFilters to operate after setCurrentPageReference
        this.connected = true;

        if(this.connectedQueue){
            this.setFilters();
        }

        // Toggle controls after setFilters has executed
        if(!this.toggleControlAreas.includes(this.areaofcompliance)){
            this.togglecontrols = true;
        }

        // Utility Class Initialization
        scopeOptions().then((result => this.auditScopeOptions = result ));
        cycleOptions().then((result => this.auditCycleOptions = result ));
        teamOptions().then((result => this.auditTeamOptions = result ));

        // Load the columns of the datatable
        this.loadColumns();
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description utilize renderedCallBack to update toggle elements after they're loaded
     */
    renderedCallback(){
        if(!this.rendered){
            this.rendered = true;
            // Don't display some toggles to auditors
            if(!this.auditorsView){
                this.setButtonSelection(this.buttonsAssigned, this.assignedtome, this.defaultAssignedToMe);
                this.setButtonSelection(this.buttonsOpenclosedInternal, this.openclosed, this.defaultOpenclosed);
            }else{
                this.setButtonSelection(this.buttonsOpenclosedAuditors, this.openclosed, this.defaultOpenclosed);
            }

            this.setButtonSelection(this.buttonsAreas, this.areaofcompliance, this.defaultAreaofcompliance)
        }
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Using the CurrentPageReference component of NavigiationMixin, retrieve 
     * metadata about this page. 
     */
    @wire(CurrentPageReference)
    setCurrentPageReference(currentPageReference) {
        this.currentPageReference = currentPageReference;

        // Loading URL params (if not defined; return defaults from metadata)
        if(this.connected){
            this.setFilters();
        }else{
            this.connectedQueue = true;
        }
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description set the page filters
     */
    setFilters(){
        // Loading URL params (if not defined; return defaults from metadata)
        this.auditcycle         = this.getDefaultURLValue('c__cycleId', this.defaultCycle);
        this.auditscope         = this.getDefaultURLValue('c__scopeId', this.defaultScope);
        this.auditteam          = this.getDefaultURLValue('c__teamId', this.defaultTeam);
        this.sortBy             = this.getDefaultURLValue('c__sortBy', this.defaultSortBy);
        this.sortDirection      = this.getDefaultURLValue('c__sortDirection', this.defaultSortDirection);
        this.areaofcompliance   = this.getDefaultURLValue('c__area', this.defaultAreaofcompliance);
        this.assignedtome       = this.getDefaultURLValue('c__assigned', this.defaultAssignedToMe);
        this.typefilter         = this.getDefaultURLValue('c__type', this.defaultTypeFilter);
        this.openclosed         = this.getDefaultURLValue('c__state', this.defaultOpenclosed);
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description set the button shading using querySelector
     */
    setButtonSelection(buttonValues, setValue, defaultValue){
        if(buttonValues.indexOf(setValue) != -1){
            this.template.querySelector('button[value="' + setValue + '"]').classList.add('slds-button_brand');
        }else if(this.buttonsAreasMenu.indexOf(setValue) != -1){
            // Special handling for drop-down menu (lightning-button-menu)
            this.template.querySelector('[data-id="showMore"]').classList.add('karl-buttonmenu_selected');
        }else{
            this.template.querySelector('button[value="' + defaultValue + '"]').classList.add('slds-button_brand');
        }
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description formats the drop-down menu for selection on click render
     */
    handleAreaButtonClick(event){
        let buttonIds = this.buttonsAreasMenu;
        if(this.buttonsAreasMenu.indexOf(this.areaofcompliance) != -1){
            // Special handling for drop-down menu (lightning-button-menu)
            this.template.querySelector('[data-id="' + this.areaofcompliance + '"]').classList.add('karl-buttonmenu_selected');
            
            // Remove the selected area of compliance from the buttonIds array
            buttonIds = buttonIds.filter(button => button !== this.areaofcompliance);
        }
        // Reset formatting if not selected
        for(let i = 0; i < buttonIds.length; i++){
            this.template.querySelector('[data-id="' + buttonIds[i] + '"]').classList.remove('karl-buttonmenu_selected');
        }
    }
    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Update the currentPageReference state with changes on the URL parameters
     * @param stateChanges - the state (URL component) which is changing
     */
    getUpdatedPageReference(stateChanges) {
        return Object.assign({}, this.currentPageReference, {
            state: Object.assign({}, this.currentPageReference.state, stateChanges)
        });
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Update the page reference parameters based on this.* values
     */
    get updatedPageReference() {
        if(!this.auditorsView){
            return this.getUpdatedPageReference({
                c__scopeId: this.auditscope,
                c__cycleId: this.auditcycle,
                c__area: this.areaofcompliance,
                c__sortBy: this.sortBy,
                c__sortDirection: this.sortDirection,
                c__state: this.openclosed,
                c__type: this.typefilter,
                c__assigned: this.assignedtome
            });
        }else{
            return this.getUpdatedPageReference({
                c__scopeId: this.auditscope,
                c__cycleId: this.auditcycle,
                c__area: this.areaofcompliance,
                c__teamId: this.auditteam,
                c__sortBy: this.sortBy,
                c__sortDirection: this.sortDirection,
                c__type: this.typefilter,
                c__state: this.openclosed
            });
        }
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Retrieve the URL parameters from the currentPageReference. Note we use this
     * instead of searchParams in accordance with Salesforce guidance.
     * @param getParam - the value from the URL
     * @param defaultvalue - default value (set in metadata or code)
     */
     getDefaultURLValue(getParam, defaultValue){
        // Use the currentPageReference to get the requested
        const urlParam = this.currentPageReference.state[getParam];

        if(urlParam != null || urlParam != undefined){
            return urlParam;
        }else{
            // Nothing passed via the URL, so we'll set default value from metadata (if exists)
            return defaultValue;
        }

    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Set the spinner (visual UI) when something is loading
     */
    setLoadingSpinner(){
        if(this.auditscope != undefined && this.auditcycle != undefined){
            this.isLoading = true;
        }else{
            this.isLoading = false;
        }
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Manage the columns via this method, which pushes to the datatable. Doing it this way
     * instead of via basic array allows us to use metadata/other flags to determine which columns
     * to display to the end-user.
     */
    loadColumns() {
        this.columns = [];

        this.columns.push({
            label: 'Request', 
            fieldName: 'requestREQLink', 
            type: 'url',
            editable: false,
            sortable: true, 
            hideDefaultActions: true,
            initialWidth: 110,
            typeAttributes: {
                label: {fieldName: 'requestNumber'},
                target: '_blank'
            }
        });
        if(!this.hideScopeCol){
            this.columns.push({
                label: 'Scope(s)', 
                fieldName: 'scopeNames', 
                wrapText: true,
                editable: false,
                sortable: true, 
                initialWidth: 150 
            });
        }
        if(!this.hideControlsCol){
            if(!this.togglecontrols){
                this.columns.push({
                    label: 'Control(s)', 
                    fieldName: 'controlNames', 
                    wrapText: true,
                    editable: false,
                    sortable: true, 
                    initialWidth: 100
                });
            }else{
                this.columns.push({
                    label: 'Requirement(s)', 
                    fieldName: 'testRequirements', 
                    wrapText: true,
                    editable: false,
                    sortable: true, 
                    initialWidth: 100
                });
            }
        }
        this.columns.push({
            label: 'Walkthroughs (Playbooks)', 
            fieldName: 'testGroups', 
            wrapText: true,
            sortable: true,
            initialWidth: 125   
        });
        this.columns.push({
            label: 'Request Name', 
            fieldName: 'requestName',
            wrapText: true,
            sortable: true, 
            initialWidth: 125 
        });
        this.columns.push({
            label: 'Request Description', 
            fieldName: 'requestDescription',
            type: 'text', 
            wrapText: true,
            initialWidth: 320,
            typeAttributes: {
                linkify: true
            },
            cellAttributes: {
                class: 'slds-scrollable'
            }
        });
        this.columns.push({
            label: 'Type', 
            fieldName: 'requestType',
            initialWidth: 110, 
            wrapText: true,
            sortable: true,
            actions: [
                { label: 'All', checked: true, name: 'all' },
                { label: 'Document (Policy Standard Procedures)', checked: false, name: 'Document (Policy Standard Procedures)' },
                { label: 'Samples', checked: false, name: 'Samples' },
                { label: 'Inquiry', checked: false, name: 'Inquiry' },
                { label: 'Observation', checked: false, name: 'Observation' },
                { label: 'Configuration', checked: false, name: 'Configuration' },
                { label: 'Configuration Samples', checked: false, name: 'Configuration Samples' },
                { label: 'Configuration Observation', checked: false, name: 'Configuration Observation' },
                { label: 'Population', checked: false, name: 'Population' },
                { label: 'Others - General Evidence', checked: false, name: 'Others - General Evidence' },
                { label: 'Self Collection', checked: false, name: 'Self Collection' }
            ]
        });
        if(!this.auditorsView){
        // NOTE: for this custom data type we need to set wrapText: true so that the input values display
            this.columns.push({
                label: 'SFDC Status', 
                fieldName: 'sfdcStatus',
                type: 'picklist',
                editable: this.sfdcEdits,
                sortable: true,
                wrapText: true,
                initialWidth: 140, 
                typeAttributes: {
                    placeholder: 'Select Status...', 
                    options: [
                        { label: 'New', value: 'New' },
                        { label: 'Not Started', value: 'Not Started' },
                        { label: 'In Progress', value: 'In Progress' },
                        { label: 'Ready for Review', value: 'Ready for Review' },
                        { label: 'Provided to Auditor', value: 'Provided to Auditor' },
                        { label: 'Follow-ups', value: 'Follow-ups' },
                        { label: 'Not Applicable', value: 'Not Applicable' },
                        { label: 'Closed', value: 'Closed' }
                    ],
                    field: 'Cycle_Request_Status__c',
                    value: { fieldName: 'sfdcStatus' },
                    context: { fieldName: 'Id' },
                    selected: this.picklistChanges
                }, 
                actions: [
                    { label: 'All', checked: true, name: 'all' },
                    { label: 'New', checked: false, name: 'New' },
                    { label: 'Not Started', checked: false, name: 'Not Started' },
                    { label: 'In Progress', checked: false, name: 'In Progress' },
                    { label: 'Ready for Review', checked: false, name: 'Ready for Review' },
                    { label: 'Provided to Auditor', checked: false, name: 'Provided to Auditor' },
                    { label: 'Follow-ups', checked: false, name: 'Follow-ups' },
                    { label: 'Not Applicable', checked: false, name: 'Not Applicable' },
                    { label: 'Closed', checked: false, name: 'Closed' }
                ]
            });
        }else{
            this.columns.push({
                label: 'SFDC Status', 
                fieldName: 'sfdcStatus', 
                wrapText: true,
                initialWidth: 120,
                actions: [
                    { label: 'All', checked: true, name: 'all' },
                    { label: 'New', checked: false, name: 'New' },
                    { label: 'Not Started', checked: false, name: 'Not Started' },
                    { label: 'In Progress', checked: false, name: 'In Progress' },
                    { label: 'Ready for Review', checked: false, name: 'Ready for Review' },
                    { label: 'Provided to Auditor', checked: false, name: 'Provided to Auditor' },
                    { label: 'Follow-ups', checked: false, name: 'Follow-ups' },
                    { label: 'Not Applicable', checked: false, name: 'Not Applicable' },
                    { label: 'Closed', checked: false, name: 'Closed' }
                ]
            });
            this.columns.push({
                label: 'Auditor Status', 
                fieldName: 'auditorStatus',
                type: 'picklist',
                editable: this.auditorEdits,
                sortable: true,
                wrapText: true,
                initialWidth: 125, 
                typeAttributes: {
                    placeholder: 'Select Status...', 
                    options: [
                        { label: 'Pending', value: 'Pending' },
                        { label: 'Not Started', value: 'Not Started' },
                        { label: 'Testing in Progress', value: 'Testing in Progress' },
                        { label: 'Testing Completed', value: 'Testing Completed' },
                        { label: 'Follow-ups', value: 'Follow-ups' },
                        { label: 'Not Applicable', value: 'Not Applicable' }
                    ],
                    field: 'KARL_Status__c',
                    value: { fieldName: 'auditorStatus' },
                    context: { fieldName: 'Id' },
                    selected: this.picklistChanges
                }, 
                actions: [
                    { label: 'All', checked: true, name: 'all' },
                    { label: 'Pending', checked: false, name: 'Pending' },
                    { label: 'Not Started', checked: false, name: 'Not Started' },
                    { label: 'Testing in Progress', checked: false, name: 'Testing in Progress' },
                    { label: 'Testing Completed', checked: false, name: 'Testing Completed' },
                    { label: 'Follow-ups', checked: false, name: 'Follow-ups' },
                    { label: 'Not Applicable', checked: false, name: 'Not Applicable' }
                ]
            });
        }
        this.columns.push({
            label: 'Request Link', 
            fieldName: 'requestLink',
            type: 'url',
            initialWidth: 150, 
            editable: this.sfdcEdits,
            sortable: true,
            hideDefaultActions: true,
            cellAttributes: {
                iconName: { fieldName: 'requestWorkflow' },
                iconAlternativeText: 'Open the request to download the files'
            }
        });
        this.columns.push({
            label: 'SFDC Comments', 
            fieldName: 'sfdcComments',
            editable: this.sfdcEdits,
            sortable: true,
            wrapText: true
        });
        if(this.auditorsView){
            this.columns.push({
                label: 'Auditor Comments', 
                fieldName: 'auditorComments',
                editable: this.auditorEdits,
                sortable: true,
                wrapText: true
            });
        }
        if(!this.hideActionsCol){
            this.columns.push({
                type:  'button-icon',
                fixedWidth: 60,
                hideDefaultActions: true,
                typeAttributes: {
                    iconName: 'utility:new_window',
                    name: 'openRequest', 
                    title: 'Open Record', 
                    disabled: false
                }
            });
        }

    }

    // Retrieve the primary data table information
    @wire(getEvidenceReport, { auditCycleId: '$auditcycle', auditScopeId: '$auditscope', auditTeamId: '$auditteam', 
    auditorView: '$auditorsView', areaOfCompliance: '$areaofcompliance', filterState: '$openclosed', 
    filterAssigned: '$assignedtome', filterType: '$typefilter'})  wiredARL(result) {
        this.evidenceWireData = result; // Save the full result, for diffing refreshApex later

        //console.log("KARL >> Parameters are set - data feed updating!");

		if ( result.data && result.data.length ){
			// ! Check if the wire returns any data
			try {
                this.paramsSet = true;
                this.records = true; // Specify boolean for if:true
                this.filteredEvidenceData = result.data; // Initially unfiltered is filteredEvidenceData.
                this.isLoading = false;
                this.error = false;
                this.sortData(this.sortBy, this.sortDirection);
                this.totalRows = result.data.length;
			} catch(e) {
				console.log('KARL >> LWC Error', result.error);
                this.isLoading = false;
			}
		} else if ( result.data && !result.data.length ){
			// ! Check if the wire is null
            //console.log('KARL >> LWC Empty', result.data);
            this.paramsSet = true;
			this.records = false; // specify boolean for if:false
            this.isLoading = false;
            this.totalRows = result.data.length;
		} else if (result.error){
            // ! Check if the wire returns an error, and return the error
            if(!this.auditcycle){
                // False error --> just waiting on parameters
                //console.log('KARL >> LWC Parameters not set');
                this.paramsSet = false;
                this.records = false; // specify boolean for if:false
                this.isLoading = false;
            }else{
                // Legitimate error
                console.log('KARL >> LWC Error', result.error);
                this.error = result.error;
                this.paramsSet = true;
                this.isLoading = false;
                this.records = false;
            }
		}
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description event handler for handling changes to Audit Cycle combobox
     * @param event - js event handle
     */
    handleAuditCycleChange(event) {
        // TODO: Need to also reset the Header Actions selection
        this.setLoadingSpinner();
        this.auditcycle = event.detail.value;
        this[NavigationMixin.Navigate](this.updatedPageReference, true);
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description event handler for handling changes to Audit Scope combobox
     * @param event - js event handle
     */
    handleAuditScopeChange(event) {
        this.setLoadingSpinner();
        this.auditscope = event.detail.value;
        this[NavigationMixin.Navigate](this.updatedPageReference, true);
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description event handler for handling changes to Audit Team combobox
     * @param event - js event handle
     */
    handleAuditTeamChange(event) {
        this.setLoadingSpinner();
        this.auditteam = event.detail.value;
        this[NavigationMixin.Navigate](this.updatedPageReference, true);
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description get method to retrieve list of request type options to the combobox
     * @return type options for picklist
     */
    get typeOptions(){
        return [
            { label: 'All', value: 'typeAll' },
            { label: 'Documents', value: 'typeDoc' },
            { label: 'Populations', value: 'typePop' },
            { label: 'Samples', value: 'typeSample' },
            { label: 'Configurations', value: 'typeConfig' },
            { label: 'Observations', value: 'typeObs' },
            { label: 'Self-Collect', value: 'typeSelfcol' },
            { label: 'Others', value: 'typeOthers' }
        ];
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description event handler for handling changes to request type combobox
     * @param event - js event handle
     */
    handleTypeChange(event) {
        this.setLoadingSpinner();
        this.typefilter = event.detail.value;
        this[NavigationMixin.Navigate](this.updatedPageReference, true);
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description event handler for handling onheaderaction events from the data table, for filtering.
     * @param event - js event handle
     */
    handleHeaderAction (event) {
        // Retrieves the name of the selected filter
        const actionName = event.detail.action.name;
        // Retrieves the current column definition
        const colName = event.detail.columnDefinition.fieldName;
        const filterColumns = this.columns;
        const activeFilter = this.activeFilter;

        // Exclude events that are for wrapText and clipText (default Salesforce header actions)
        if (actionName !== activeFilter && actionName != 'wrapText' && actionName != 'clipText') {
            filterColumns.map((col) => {
                if(col.fieldName === colName){
                    var actions = col.actions;

                    actions.forEach((action) => {
                        action.checked = action.name === actionName;
                    });
                }
            });
            this.activeFilter = actionName;
            this.updateDataTable(colName);
            this.columns = filterColumns;

            // Update the actions list to show which action is currently selected
            filterColumns.find(col => col.fieldName === colName).actions.forEach(action => action.checked = action.name === actionName);
            this.columns = [...filterColumns];
        }
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description update the Data Table based on the selected filters (in header actions)
     * @param columnName - the column from the header action
     */
    updateDataTable(columnName) {
        this.filteredEvidenceData = this.evidenceWireData.data; // Reset to unfiltered data
        this.refreshDraftValues(); // Apply any changes we have stored in draftValues
        const rows = this.filteredEvidenceData;
        const activeFilter = this.activeFilter;
        let filteredRows = rows;

        if (activeFilter !== 'all') {
            filteredRows = rows.filter(function (row) {
                if(row[columnName] !== '' && row[columnName] !== undefined){
                    return row[columnName].toUpperCase() === activeFilter.toUpperCase();
                }     
            });
        }
        this.filteredEvidenceData = filteredRows;
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Update the sorting based on header action selections
     */
    doSorting(event){
        this.sortBy = event.detail.fieldName;
        this.sortDirection = event.detail.sortDirection;
        this.sortData(this.sortBy, this.sortDirection);
        this[NavigationMixin.Navigate](this.updatedPageReference, true);
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Sort the data based on selection; supporting method
     */
    sortData(fieldname, direction){
        let parseData = JSON.parse(JSON.stringify(this.filteredEvidenceData));

        let keyValue = (a) => {
            return a[fieldname];
        }

        let isReverse = direction ==='asc'?1:-1;
        
        parseData.sort((x, y) => {
            x = keyValue(x) ? keyValue(x) : ''; // handling null values
            y = keyValue(y) ? keyValue(y) : '';
            // sorting values based on direction
            return isReverse * ((x > y) - (y > x));
        });
        this.filteredEvidenceData = parseData;
        //this.refreshDraftValues();
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Handle searches from the inputs on the main component page
     * @param event - js event handle
     */
    handleSearch(event){
        const rows = JSON.parse(JSON.stringify(this.filteredEvidenceData)); // raw unfiltered data to start with
        const searchString = event.target.value;
        const searchField = event.target.name;
        var regex = new RegExp(searchString.replace('/[|\\{}()[\]^$+*?.]/g', '\\$&'),'i'); // previously tried gi - i much more reliable
        let filteredRows = rows;

        if(searchString !== ''){
            // TODO: More tech debt here than I would like with all the if/else switches. Couldn't get it working.
            if( searchField === 'searchDesc' ){
                filteredRows = rows.filter(
                    row => regex.test(
                        row.requestDescription
                    )
                );
            } else if ( searchField === 'searchRequest' ){
                filteredRows = rows.filter(
                    row => regex.test(
                        row.requestNumber
                    )
                );
            } else if ( searchField === 'searchControl' ){
                if(!this.togglecontrols){
                    // Search Controls
                    filteredRows = rows.filter(
                        row => regex.test(
                            row.controlNames
                        )
                    );
                }else{
                    // Search Requirements
                    filteredRows = rows.filter(
                        row => regex.test(
                            row.testRequirements
                        )
                    );
                }
            } else if ( searchField === 'searchWalkthrough' ){
                filteredRows = rows.filter(
                    row => regex.test(
                        row.testGroups
                    )
                );
            } else {
                // No search configured.. nothing to do.
            }
        }else{
            // Reset the fields back to original filter
            filteredRows = JSON.parse(JSON.stringify(this.evidenceWireData.data));
        }
        // Update the filteredEvidenceData to respect the filteredRows.
        this.filteredEvidenceData = filteredRows;
        this.refreshDraftValues();
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Handle picklist changes / adding them in to the new draftValues
     * @param event - js event handle
     */
    handlePicklistChange(event) {
        event.stopPropagation();
        this.saveButton = true;
        let dataReceived = event.detail.data;
        let updatedItem = { Id: dataReceived.context, [dataReceived.field]: dataReceived.value };

        this.updateDraftValues(updatedItem);
        this.updateDataValues(updatedItem);
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Update draftValues on normal cell edits, since we're not using the OOB draftValues
     * @param event - js event handle
     */
    handleCellChange(event){
        this.saveButton = true;

        for(var fieldApi in this.fieldApis){
            if(this.fieldApis[fieldApi] in event.detail.draftValues[0]){
                event.detail.draftValues[0][fieldApi] = event.detail.draftValues[0][this.fieldApis[fieldApi]];
            }
        }

        this.updateDraftValues(event.detail.draftValues[0]);
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description refresh this.draftValues (which is a Salesforce detault) to reflect changes from picklist/cells. 
     * This is necessary for being able to reset the data values (e.g. when clicking cancel on edit)
     */
    refreshDraftValues(){
        this.draftValues.forEach(item => {
            this.updateDataValues(item);
        });
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description update the data values in the datatable
     * @param updateItem - the value which is changing that needs to be updated
     */
    updateDataValues(updateItem) {
        let changeData = [... JSON.parse(JSON.stringify(this.filteredEvidenceData))];
        
        changeData.forEach(item => {
            if (item.Id === updateItem.Id) {
                for (let field in updateItem) {
                    if(this.fieldApis.hasOwnProperty(field) ){
                        item[this.fieldApis[field]] = updateItem[field];
                        this.picklistChanges.push("karlpicklist_" + item.Id);
                    }
                }
            }
        });

        this.filteredEvidenceData = [...changeData];
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Update the draftValues array with the draft edits from inline editing (from picklist change)
     */
    updateDraftValues(updateItem) {
        let draftValueChanged = false;
        let copyDraftValues = [...this.draftValues];

        // Determine if there is already a draft change, so we don't duplicate
        copyDraftValues.forEach(item => {
            if (item.Id === updateItem.Id) {
                for (let field in updateItem) {
                    item[field] = updateItem[field];
                }
                draftValueChanged = true;
            }
        });

        // Update draftValues using spread operator
        if (draftValueChanged) {
            this.draftValues = [...copyDraftValues];
        } else {
            this.draftValues = [...copyDraftValues, updateItem];
        }
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Handle save of the inline edits via Apex controller
     * @event - js event handle (not used - simply a triggering function)
     */
    async handleSave(event) {
        const updatedFields = this.draftValues;
        const searchFields = this.searchFields;
        const onchangeEvent = new CustomEvent('change',{bubbles: true});
        // Prepare the record IDs for getRecordNotifyChange
        const notifyChangeIds = updatedFields.map(row => { 
            return { "recordId": row.Id }
        });

        await updateStatus({data: updatedFields, auditorView: this.auditorsView})
            .then(result => { 
                //console.log(JSON.stringify("Apex update result >> "+ result));
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Success',
                        message: 'KARL records updated successfully.',
                        variant: 'success'
                    })
                );
        
                // Refresh LDS cache and wires
                getRecordNotifyChange(notifyChangeIds);
                
                // Display fresh data in the datatable
                refreshApex(this.evidenceWireData).then(() => {
                    // Clear all draft values in the datatable
                    this.draftValues = [];
                    this.saveButton = false;
                    // Re-apply client-side search filters
                    for(let i = 0; i < searchFields.length; i++){
                        this.template.querySelector('lightning-input[data-field="' + searchFields[i] + '"]').value != '' ? this.template.querySelector('lightning-input[data-field="' + searchFields[i] + '"]').dispatchEvent(onchangeEvent) : null; 
                    }
                });

        }).catch(error => {
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Error updating or refreshing records',
                    message: error.body.message,
                    variant: 'error'
                })
            );
        });
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Handle refreshing the datatable based on button click
     * @param event - js event handle (not used - simply a trigger)
     */
    handleRefresh(){
        const searchFields = this.searchFields;
        const onchangeEvent = new CustomEvent('change',{bubbles: true});
        refreshApex(this.evidenceWireData).then(() => {
            // Re-apply client-side search filters
            console.log('Wire Refreshed');
            for(let i = 0; i < searchFields.length; i++){
                this.template.querySelector('lightning-input[data-field="' + searchFields[i] + '"]').value != '' ? this.template.querySelector('lightning-input[data-field="' + searchFields[i] + '"]').dispatchEvent(onchangeEvent) : null; 
            }
        });
        if(!this.auditorView){
            this.template.querySelector('c-karl_arl_datatable_extension').handleRefresh();
        }
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Handle click of the Cancel button, resetting data back in the datatable and draftValues
     * @param event - js event handle (not used - simply a trigger)
     */
    handleCancel(event) {
        this.saveButton = false;
        this.draftValues = [];
        this.filteredEvidenceData = this.evidenceWireData.data;
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Handle rowActions in the data table, e.g. to take the user to the resource page in a new window.
     * @param event - js event handle
     */
    handleRowAction(event){
        //console.log(JSON.stringify(event.detail.action) + ' on row ' + JSON.stringify(event.detail.row));
        if(event.detail.action.name==='openRequest') {
            this[NavigationMixin.GenerateUrl]({
                type: 'standard__recordPage',
                attributes: {
                    recordId: event.detail.row.Id,
                    actionName: 'view'
                }
            }).then(url => {
                //console.log('Target URL >> ' + url); // Log the target URL in the console
                window.open(url, '_blank'); // open in a new tab/window
            });
        }
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Handle click on the button group for Area of Compliance
     * @param event - onClick event
     */
    handleAreaClick(event){
        let buttonIds = this.buttonsAreas;
        if(this.areaofcompliance != event.target.value){
            this.setLoadingSpinner();  
            this.areaofcompliance = event.target.value;

            // For SOC we want to toggle the Controls column
            if(!this.toggleControlAreas.includes(this.areaofcompliance)){
                this.togglecontrols = true;
                this.loadColumns();
            }else{
                this.togglecontrols = false;
                this.loadColumns();
            }

            // Set the formatting of the selected buttonId (only if the value exists)
            if(buttonIds.includes(this.areaofcompliance)){
                this.template.querySelector('button[value="' + this.areaofcompliance + '"]').classList.add('slds-button_brand'); 
                this.template.querySelector('button[value="' + this.areaofcompliance + '"]').classList.remove('slds-button_neutral'); 
                if (!this.auditorsView){
                    this.template.querySelector('[data-id="showMore"]').classList.remove('karl-buttonmenu_selected');
                }
            }else if(this.buttonsAreasMenu.includes(this.areaofcompliance)){
                this.template.querySelector('[data-id="showMore"]').classList.add('karl-buttonmenu_selected');
            }

            // Remove the selected area of compliance from the buttonIds array
            buttonIds = buttonIds.filter(button => button !== this.areaofcompliance);

            // Reset formatting of other options
            for(let i = 0; i < buttonIds.length; i++){
                this.template.querySelector('button[value="' + buttonIds[i] + '"]').classList.add('slds-button_neutral'); 
                this.template.querySelector('button[value="' + buttonIds[i] + '"]').classList.remove('slds-button_brand');
            }

            this[NavigationMixin.Navigate](this.updatedPageReference, true);
        }
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Handle click on the button group for Open/Provided/Closed
     * @param event - onClick event js handle
     */
    handleOpenClosed(event){
        let buttonIds = [];
        if(!this.auditorsView){
            buttonIds = this.buttonsOpenclosedInternal;
        }else{
            buttonIds = this.buttonsOpenclosedAuditors;
        }

        if(this.openclosed != event.target.value){
            this.setLoadingSpinner();  
            this.openclosed = event.target.value;
            
            // Remove the selected area of compliance from the buttonIds array
            buttonIds = buttonIds.filter(button => button !== this.openclosed);

            // Set the formatting of the selected buttonId
            this.template.querySelector('button[value="' + this.openclosed + '"]').classList.add('slds-button_brand'); 
            this.template.querySelector('button[value="' + this.openclosed + '"]').classList.remove('slds-button_neutral'); 

            // Reset formatting of other options
            for(let i = 0; i < buttonIds.length; i++){
                this.template.querySelector('button[value="' + buttonIds[i] + '"]').classList.add('slds-button_neutral'); 
                this.template.querySelector('button[value="' + buttonIds[i] + '"]').classList.remove('slds-button_brand');
            }

            this[NavigationMixin.Navigate](this.updatedPageReference, true);
        }
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Handle click on the button group for Assigned To Me
     * @param event - onClick event
     */
    handleAssigned(event){
        let buttonIds = this.buttonsAssigned;

        if(this.assignedtome != event.target.value){
            this.setLoadingSpinner();  
            this.assignedtome = event.target.value;
            
            // Remove the selected area of compliance from the buttonIds array
            buttonIds = buttonIds.filter(button => button !== this.assignedtome);

            // Set the formatting of the selected buttonId
            this.template.querySelector('button[value="' + this.assignedtome + '"]').classList.add('slds-button_brand'); 
            this.template.querySelector('button[value="' + this.assignedtome + '"]').classList.remove('slds-button_neutral'); 

            // Reset formatting of other options
            for(let i = 0; i < buttonIds.length; i++){
                this.template.querySelector('button[value="' + buttonIds[i] + '"]').classList.add('slds-button_neutral'); 
                this.template.querySelector('button[value="' + buttonIds[i] + '"]').classList.remove('slds-button_brand');
            }

            this[NavigationMixin.Navigate](this.updatedPageReference, true);
        }
    }
}
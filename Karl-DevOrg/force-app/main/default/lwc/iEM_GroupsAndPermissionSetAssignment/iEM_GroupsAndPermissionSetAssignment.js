/**
 * @File Name          : iEM_GroupsAndPermissionSetAssignment.js
 * @Description        : 
 * @Author             : Banshi
 * @Group              : 
 * @Last Modified By   : Banshi
 * @Last Modified On   : 11/18/2019, 12:12:39 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    11/11/2019   Banshi     Initial Version
 **/
import { LightningElement, wire, track,api } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

// row actions
const actions = [
    { label: 'Details', name: 'record_details' }
];
const columnsPubGroups = [
    { label: 'Label', fieldName: 'Name' },
    { label: 'API Name', fieldName: 'DeveloperName' },
    {
        label: 'Created Date',
        fieldName: 'CreatedDate',
        type: 'date',
        typeAttributes: {
            day: 'numeric',
            month: 'short',
            year: 'numeric',
            hour: '2-digit',
            minute: '2-digit',
            second: '2-digit',
            hour12: true
        }
    },
    {
        type: 'action',
        typeAttributes: {
            rowActions: actions,
            menuAlignment: 'right'
        }
    }
];
const columnsPermSets = [
    { label: 'Label', fieldName: 'Label' },
    { label: 'API Name', fieldName: 'Name' },
    { label: 'Description', fieldName: 'Description', wrapText: true },
    {
        label: 'Created Date',
        fieldName: 'CreatedDate',
        type: 'date',
        typeAttributes: {
            day: 'numeric',
            month: 'short',
            year: 'numeric',
            hour: '2-digit',
            minute: '2-digit',
            second: '2-digit',
            hour12: true
        }
    },
    {
        type: 'action',
        typeAttributes: {
            rowActions: actions,
            menuAlignment: 'right'
        }
    }
];
const columnsPermSetGroups = [
    { label: 'Label', fieldName: 'MasterLabel' },
    { label: 'API Name', fieldName: 'DeveloperName' },
    { label: 'Description', fieldName: 'Description', wrapText: true },
    {
        label: 'Created Date',
        fieldName: 'CreatedDate',
        type: 'date',
        typeAttributes: {
            day: 'numeric',
            month: 'short',
            year: 'numeric',
            hour: '2-digit',
            minute: '2-digit',
            second: '2-digit',
            hour12: true
        }
    },
    {
        type: 'action',
        typeAttributes: {
            rowActions: actions,
            menuAlignment: 'right'
        }
    }
];
const GROUP_TYPE = 'Group';
const PERMISSION_SET_TYPE = 'PermissionSet';
const PERMISSION_SET_GROUP_TYPE = 'PermissionSetGroup';

import getPublicGroups from '@salesforce/apex/IEM_GroupsAndPermissionSetController.getPublicGroups';
import getPermissionSets from '@salesforce/apex/IEM_GroupsAndPermissionSetController.getPermissionSets';
import getPermissionSetGroups from '@salesforce/apex/IEM_GroupsAndPermissionSetController.getPermissionSetGroups';
import getUsers from '@salesforce/apex/IEM_GroupsAndPermissionSetController.getUsers';
import getMembers from '@salesforce/apex/IEM_GroupsAndPermissionSetController.getMembers';
import updateMembers from '@salesforce/apex/IEM_GroupsAndPermissionSetController.updateMembers';
import searchUser from '@salesforce/apex/IEM_GroupsAndPermissionSetController.retreiveUser';
import searchExistingUser from '@salesforce/apex/IEM_GroupsAndPermissionSetController.retreiveExistingUser';
export default class iEM_GroupsAndPermissionSetAssignment extends LightningElement {
    // reactive variable

    @api recordId;
    @track columnsPubGroups = columnsPubGroups;
    @track columnsPermSets = columnsPermSets;
    @track columnsPermSetGroups = columnsPermSetGroups;
    @track showModel = false;
    @track publicGroups = [];
    @track premissionSets = [];
    @track premissionSetGroups = [];
    @track record = [];
    @track showLoadingSpinner = false;
    @track options = [];
    @track existingOptions = [];
    @track filteredOptions = [];
    @track allOptions = [];
    @track values = [];
    @track onSearchValues = false;
    @track displayValues = true;
    @track existingMembers = [];
    @track users = [];
    @track modelTitle = 'Add/Remove Group Member';
    @track modelSubTitle = 'Group Name: ';
    @track actionType = GROUP_TYPE;
    @track searchData;
    @track errorMsg = '';
    strSearchUserName = '';
    @track previousSelectedValue = [];
    @track currentRowId;
    @track valueSelected = [];
    // non-reactive variables
    refreshTable;
    error;

    // retrieving the data using wire service
    @wire(getPublicGroups)
    getPublicGroupsWired(result) {
            this.refreshTable = result;
            if (result.data) {
                this.publicGroups = result.data;
                this.error = undefined;

            } else if (result.error) {
                this.error = result.error;
                this.publicGroups = undefined;
            }
        }
    
    // retrieving the data using wire service
    @wire(getPermissionSets)
    getPermissionSetsWired(result) {
        if (result.data) {
            this.premissionSets = result.data;
            this.error = undefined;

        } else if (result.error) {
            this.error = result.error;
            this.premissionSets = undefined;
        }
    }

    // retrieving the data using wire service
    @wire(getPermissionSetGroups)
    getPermissionSetGroupsWired(result) {
        if (result.data) {
            this.premissionSetGroups = result.data;
            this.error = undefined;

        } else if (result.error) {
            this.error = result.error;
            this.premissionSetGroups = undefined;
        }
    }

    //fetch all the users
    connectedCallback() {
        //this.fetchAllRecords();
    }

    fetchAllRecords(){
        this.showLoadingSpinner = true;
        let getUserSuccess = result => {
            this.users = result;
            //this.users= [];
            this.showLoadingSpinner = false;
            const items = [];
            for (let i = 0; i < this.users.length; i++) {
                items.push({
                    label: this.users[i].Username,
                    value: this.users[i].Id,
                });
            }
            this.options.push(...items);
            let arrLength = 0;
        }

        let getUserError = error => {
            this.showLoadingSpinner = false;
            this.error = error;
        }

        getUsers().then(getUserSuccess).catch(getUserError);

    }

    // Handle row detail Action of group which pop-up the modal window
    handleRowActions(event) {
        //this.fetchAllRecords();
        let actionName = event.detail.action.name;
        console.log('actionName', actionName);
        let row = event.detail.row;
        //this.previousSelectedValue = [];
        console.log('row -> group ',JSON.stringify(row));
        this.actionType = GROUP_TYPE;
        
        // eslint-disable-next-line default-case
        switch (actionName) {
            case 'record_details':
                this.viewCurrentRecord(row);
                console.log('this');
                break;
        }
    }
    // Handle row detail Action of group which pop-up the modal window
    handlePermissionSetRowActions(event) {
        //this.fetchAllRecords();
        let actionName = event.detail.action.name;
        let row = event.detail.row;
        this.actionType = PERMISSION_SET_TYPE;
        // eslint-disable-next-line default-case
        switch (actionName) {
            case 'record_details':
                this.viewCurrentRecord(row);
                break;
        }
    }

    // Handle row detail Action of group which pop-up the modal window
    handlePermissionSetGroupRowActions(event) {
        //this.fetchAllRecords();
        let actionName = event.detail.action.name;
        let row = event.detail.row;
        this.actionType = PERMISSION_SET_GROUP_TYPE;
        // eslint-disable-next-line default-case
        switch (actionName) {
            case 'record_details':
                this.viewCurrentRecord(row);
                break;
        }
    }

    // view the current record details/users of that particular group/permission Set
    viewCurrentRecord(currentRow) {
        this.valueSelected = [];
        this.options = [];
        this.existingOptions = [];
        //this.fetchAllRecords();
        this.onSearchValues = false;
        this.showLoadingSpinner = true;
        this.showModel = true;
        this.currentRowId = currentRow.Id;

        if (this.actionType === PERMISSION_SET_TYPE) {
            this.modelTitle = 'Manage Permission Set';
            this.modelSubTitle = 'Permission Set Name: ';
            this.modalRecordName = currentRow.Label;
        }else if (this.actionType === PERMISSION_SET_GROUP_TYPE) {
            this.modelTitle = 'Manage Permission Set Group';
            this.modelSubTitle = 'Permission Set Group Name: ';
            this.modalRecordName = currentRow.MasterLabel;
        }else{
            this.modalRecordName = currentRow.Name;
        }
        this.record = currentRow;
        this.values = [];
        this.existingMembers = [];
        let getMembersSuccess = result => {
            console.log('result ->',result );
            if(result){
                for (let i = 0; i < result.length; i++) {
                    this.values.push(result[i]);
                    console.log('values',JSON.stringify(this.values));
                    this.existingMembers.push(result[i].MemberId);
                    console.log('existingMembers-> ', JSON.stringify(this.existingMembers));
                }

            }
            this.showLoadingSpinner = false;
        }
        //console.log('this.values ',JSON.stringify(this.values));
        let getMembersError = error => {
            console.log('error', JSON.stringify(error));
            console.log(error);
            this.showLoadingSpinner = false;
        }

        getMembers({
            type: this.actionType,
            recordId: currentRow.Id,
        }).then(getMembersSuccess).catch(getMembersError);

    }

    // closing modal box
    closeModal() {
            this.showModel = false;
            //location.reload();
            this.options = [];
            this.values = [];
            this.previousSelectedValue = [];
        }

    // Handling the change event (when value shift from available to selected option in dual-list box)
    handleChange(event) {
        this.values= (JSON.parse(JSON.stringify(event.detail.value)));
        console.log('value --> HC ', this.values);
        console.log('value --> HC ', this.values.length);
    }

    // update new members
    updateMembers() {
        console.log('this.values update -> ',JSON.stringify(this.values));
        console.log('this existingMembers-> ', JSON.stringify(this.existingMembers));
        console.log('type of -> ',typeof(this.values) );
        if(this.values.length!=0){
            for(let i = 0; i<this.values.length; i++){
                this.previousSelectedValue.push(this.values[i]);
            }
        }
        
        this.showLoadingSpinner = true;
        let updateMembersSuccess = result => {
            this.showLoadingSpinner = false;
            this.showModel = false;
            this.showNotification('Success', 'Saved succesfully!', 'success');
            //location.reload();
        }
        let updateMembersError = error => {
            this.showLoadingSpinner = false;
            this.showNotification('Error', error.body.message, 'error');
        }

        updateMembers({
            type: this.actionType,
            recordId: this.record.Id,
            newMemberData: JSON.stringify(this.values),
            existingMembers: JSON.stringify(this.existingMembers)
        }).then(updateMembersSuccess).catch(updateMembersError);


    }

    showNotification(title, message, variant) {
        const evt = new ShowToastEvent({
            title: title,
            message: message,
            variant: variant,
        });
        this.dispatchEvent(evt);
    }

    // take username input - in input text box
    handleUserName(event){
            this.strSearchUserName = event.detail.value;
        }

    // search for input and get 100 users.
    handleSearch() {
        if(!this.strSearchUserName) {
            this.errorMsg = 'Please enter User name to search.';
            this.searchData = undefined;
            return;
        }
        console.log('value --> HC ', this.values)
        console.log('value --> HC ', this.values.length);
        
        this.onSearchValues = true;
        //this.previousSelectedValue = this.existingMembers;
        if(this.values.length != 0){
            for(let i=0 ; i<this.values.length ; i++){
                this.previousSelectedValue.push(this.values[i]);
            }    
        }
        console.log('this.previousSelectedValue after search--> ',JSON.stringify(this.previousSelectedValue));
        console.log(this.recordId);
        console.log(this.currentRowId);
        
        this.options = [];
        //this.values = [];
        let getUserSuccess = result => {
            console.log('result after search ', result);
            for (let i of result) {
                this.options.push({
                    label: i.Username,
                    value: i.Id,
                });
            }
            console.log('options after search ',JSON.stringify(this.options));
        }

        this.existingOptions = [];
        //this.values = [];
        let getExistingUserSuccess = result => {
            console.log('existing result after search ', result);
            for (let i of result) {
                this.existingOptions.push({
                    label: i.Username,
                    value: i.Id,
                });
            }
            console.log('existing options after search ',JSON.stringify(this.existingOptions));
        }

        let getUserError = error => {
            this.showLoadingSpinner = false;
            this.error = error;
        }

        searchUser({
            strAccName: this.strSearchUserName,
            newMemberIds: JSON.stringify(this.values),
        }).then(getUserSuccess).catch(getUserError);

        searchExistingUser({
            strAccName: this.strSearchUserName,
            newMemberIds: JSON.stringify(this.values),
        }).then(getExistingUserSuccess).catch(getUserError);
        
    }

    //push values right
    handleLeft(event){
        console.log('this valueselected',JSON.stringify(this.valueSelected));
        console.log('this valueselected', this.valueSelected.length);
        if(!this.valueSelected.length > 0) {
            this.errorMsg = 'Please Select a value.';
            this.searchData = undefined;
            return;
        }else{
            if(this.valueSelected.length > 0){
    
                this.displayValues = false;
                this.onSearchValues = false;
                console.log('inside handleLeft');
                let indexFound;
                this.values.forEach((item,index) => {
                    //console.log('item-> ',JSON.parse(JSON.stringify(item)));
                    if(item.MemberId === this.valueSelected){
                        console.log('value found');
                        this.options.push({
                            label: item.Name,
                            value: item.MemberId
                        });
                        indexFound = index;
                        console.log('indexFound',indexFound);
                    }
                });
                this.values.splice(indexFound,1);
                this.onSearchValues = true;
                this.displayValues = true;
                this.valueSelected = [];
            }
        }
        
    }

    //push values left
    handleRight(){
        if(!this.valueSelected.length > 0) {
            this.errorMsg = 'Please Select a value.';
            //this.searchData = undefined;
            return;
        }else{
            if(this.valueSelected.length > 0){
                this.displayValues = false;
            this.onSearchValues = false;
            console.log('inside handleRight');
            let indexFound;
            this.options.forEach((item,index) => {
                //console.log('item-> ',JSON.parse(JSON.stringify(item)));
                if(item.value === this.valueSelected){
                    console.log('value found');
                    this.values.push({
                        MemberId: item.value,
                        Name: item.label
                    });
                    indexFound = index;
                    console.log('indexFound',indexFound);
                }
            });
            this.options.splice(indexFound,1);
            this.onSearchValues = true;
            this.displayValues = true;
            this.valueSelected = [];
    
            }
        }
          
    }
    selectUser(event){
        var substring = event.currentTarget.id;
        console.log(substring.length);
        console.log('target id value',event.currentTarget.id);
        this.valueSelected = substring.slice(0, 18);
        //var valueToBeRemove = this.valueSelected;
        //valueToBeRemove.shift();
        console.log('this valueselected after trim', JSON.stringify(this.valueSelected));
    }
}
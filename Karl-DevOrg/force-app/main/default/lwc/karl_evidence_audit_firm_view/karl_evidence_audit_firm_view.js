import {
    LightningElement,
    track,
    api
} from 'lwc';
import {
    ShowToastEvent
} from 'lightning/platformShowToastEvent';
import getCycleReqItemContentVersionRecords from '@salesforce/apex/KARL_EvidenceUploadController.getCycleReqItemContentVersionRecords';
import getContentDistributionForFileOrg from '@salesforce/apex/KARL_EvidenceUploadController.getContentDistributionForFileOrg';
import {
    showAjaxErrorMessage
}
from 'c/karlUtil';

export default class Karl_evidence_audit_firm_view extends LightningElement {
    @api recordId;

    @track data = [];
    @track isTableEmpty = false;
    @track showSpinner = false;
    @track isCommunity = false;

    connectedCallback() {
        let fullUrl = window.location.href;
        let newURL = new URL(fullUrl).searchParams;
        if (!this.recordId) {
            this.recordId = newURL.get('id');
        }
        this.loadColumns();
        this.refreshTableData();
    }

    /**
     * @author Mahima Aggarwal
     * @email mahima.aggarwal@mtxb2b.com
     * @desc This method loads columns
     */
    loadColumns() {
        this.columnConfiguration = [];

        this.columnConfiguration.push({
            heading: 'ATTACHMENT',
            fieldApiName: 'Attachment"',
            style: 'width:50%;'
        });
        this.columnConfiguration.push({
            heading: 'EVIDENCE NOTES',
            fieldApiName: 'Evidence_Notes',
            style: 'width:50%;'
        });
    }

    /**
     * @author Mahima Aggarwal
     * @email mahima.aggarwal@mtxb2b.com
     * @desc This method refresh data
     */
    refreshTableData() {
        this.showSpinner = true;
        console.log('refreshTableDataCalled>>' + this.recordId);
        this.data = [];
        getCycleReqItemContentVersionRecords({
                idField: this.recordId
            })
            .then(result => {
                this.data = result.fileWrapperList;
                console.log('data' + JSON.stringify(this.data));
                this.isCommunity = result.isCommunity;
                this.isTableEmpty = result.fileWrapperList.length === 0 ? true : false;
            })
            .catch(error => {
                console.log("error occured>> " + JSON.stringify(error));
                showAjaxErrorMessage(this, error);
            }).finally(() => {
                this.showSpinner = false;
            });
    }


    /**
     * @author Mahima Aggarwal
     * @email mahima.aggarwal@mtxb2b.com
     * @desc This method shows preview of file that is uploaded
     */
    handleDownloadFile(event) {
        getContentDistributionForFileOrg({
                contentDocumentId: event.currentTarget.dataset.id
            })
            .then(response => {
                window.open(response.DistributionPublicUrl);
            })
            .catch(error => {
                console.log('error in file open>>' + JSON.stringify(error));
                showAjaxErrorMessage(this, error);
            })
    }

    /**
     * @author Mahima Aggarwal
     * @email mahima.aggarwal@mtxb2b.com
     * @desc This method shows preview of file that is uploaded
     */
    handleDownloadAll(event) {
        console.log('data>' + JSON.stringify(this.data));
        if (this.isTableEmpty) {
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Error!',
                    message: 'No files available for download',
                    variant: 'error'
                })
            );
            return;
        }
        let finalUrl;
        console.log('this.isCommunity>>' + this.isCommunity);
        if (this.isCommunity) {
            finalUrl = '/KARLCommunity/sfc/servlet.shepherd/document/download/';
        } else {
            finalUrl = '/sfc/servlet.shepherd/document/download/';
        }
        let i = 0;
        for (i = 0; i < this.data.length; i++) {
            finalUrl += this.data[i].contentDocId;

            if (i == (this.data.length - 1)) {
                finalUrl += '?'
            } else {
                finalUrl += '/'
            }
        }
        console.log('finalUrl>>' + finalUrl);
        window.open(finalUrl);
    }
}
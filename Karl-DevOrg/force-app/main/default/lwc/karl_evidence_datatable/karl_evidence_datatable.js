import {
    LightningElement, 
    api, 
    wire, 
    track
} from 'lwc';
import {
    refreshApex
} from '@salesforce/apex';
import {
    ShowToastEvent
} from 'lightning/platformShowToastEvent';
import {
    CurrentPageReference,
    NavigationMixin
} from 'lightning/navigation';
import Id from '@salesforce/user/Id';
import ARL_Is_External_Auditor from '@salesforce/customPermission/ARL_Is_External_Auditor';


// Import Apex Classes
import getAuditCycles from '@salesforce/apex/KARL_Utility.getAuditCycles';
import getAuditCycleScopes from '@salesforce/apex/KARL_Utility.getAuditCycleScopes';
import getEvidenceReport from '@salesforce/apex/KARL_EvidenceReportController.getEvidenceReport';


export default class Karl_evidence_datatable extends NavigationMixin(LightningElement) {
    @api recordId;
    userId = Id;

    // Metadata variables
    @api auditorsView = false;
    @api defaultCycle;
    @api defaultScope;
    @api defaultTeam;
    @api defaultSortBy;
    @api defaultSortDirection;

    // Wire Data Output
    @track auditCycles = [];
    @track auditScopes = [];
    @track auditTeams = [];
    @track filteredData = [];
    @track wireData; // Full result (for refreshApex)

    // Wire Data Input / Management
    @track auditcycle;
    @track auditscope;
    @track auditteam;
    @track totalRows;
	@track error;
    @track records;

    // Button Management
    @track openclosed;
    @track assignedtome;
    @track assignment;
    defaultOpenclosed   = 'all';
    defaultAssignedToMe = 'allAssigned';
    defaultAssignment   = 'currentAll';
    buttonsOpenclosed   = ['all','pendingEngineer','readyForReview','providedToAuditor','returnedToEngineer'];
    buttonsAssigned     = ['allAssigned','assignedToMe'];

    // Sorting variables
    @track sortDirection;
    @track sortBy;

    // Miscellaneous
    @track isLoading;
    @track togglecontrols = false;
    @track paramsSet = false;
    @track sfdcEdits = false;
    @track auditorEdits = false;
    @track saveButton = false;
    @track draftValues = [];
    currentPageReference;

    @wire(getAuditCycles, {})  wiredAuditCycles(auditCyclesResult) {
        this.auditCycles = auditCyclesResult.data;
    }

    // Supporting metadata: Audit Scopes (picklist)
    @wire(getAuditCycleScopes, {})  wiredAuditCycleScopes(auditScopesResult) {
        this.auditScopes = auditScopesResult.data;        
    }

    @wire(CurrentPageReference)
    setCurrentPageReference(currentPageReference) {
        this.currentPageReference = currentPageReference;
        // Loading URL params (if not defined; return defaults from metadata)
        if(this.connected){
            this.setFilters();
        }else{
            this.connectedQueue = true;
        }
    }


    @wire(getEvidenceReport, { auditCycleId: '$auditcycle', auditScopeId: '$auditscope',
    auditorView: '$auditorsView', filterState: '$openclosed', filterAssigned: '$assignedtome', filterAssignment: '$assignment'})  wiredARL(result) {
        this.wireData = result; // Save the full result, for diffing refreshApex later
        if ( result.data && result.data.length ){
            // ! Check if the wire returns any data
            try {
                this.paramsSet = true;
                this.records = true; // Specify boolean for if:true
                this.filteredData = result.data; // Initially unfiltered is filteredData.
                this.isLoading = false;
                this.error = false;
                this.sortData(this.sortBy, this.sortDirection);
                this.totalRows = result.data.length;
            } catch(e) {
                console.log('KARL >> LWC Error catch', e.message || e.body.message);
                this.isLoading = false;
            }
        } else if ( result.data && !result.data.length ){
            // ! Check if the wire is null
            //console.log('KARL >> LWC Empty', result.data);
            this.paramsSet = true;
            this.records = false; // specify boolean for if:false
            this.isLoading = false;
            this.totalRows = result.data.length;
        } else if (result.error){
            // ! Check if the wire returns an error, and return the error
            if(!this.auditcycle){
                // False error --> just waiting on parameters
                //console.log('KARL >> LWC Parameters not set');
                this.paramsSet = false;
                this.records = false; // specify boolean for if:false
                this.isLoading = false;
            }else{
                // Legitimate error
                console.log('KARL >> LWC Error legitimate', result.error);
                this.error = result.error;
                this.paramsSet = true;
                this.isLoading = false;
                this.records = false;
            }
        }
    }

    connectedCallback() {

        this.sfdcEdits = ARL_Is_External_Auditor ? false : true; 

        // Load the columns of the datatable
        this.loadColumns();
        this.connected = true;

        if(this.connectedQueue){
           this.setFilters();
        }
    }

    renderedCallback(){
        // Utilize setButtonSelection so we also check for default values        
        this.setButtonSelection(this.buttonsAssigned, this.assignedtome, this.defaultAssignedToMe)
        this.setButtonSelection(this.buttonsOpenclosed, this.openclosed, this.defaultOpenclosed)
    }
    setButtonSelection(buttonValues, setValue, defaultValue){
        if(buttonValues.indexOf(setValue) != -1){
            this.template.querySelector('button[value="' + setValue + '"]').classList.add('slds-button_brand');
        }else{
            this.template.querySelector('button[value="' + defaultValue + '"]').classList.add('slds-button_brand');
        }
    }


    setFilters(){
        this.auditcycle     = this.getDefaultURLValue('c__cycleId', this.defaultCycle);
        this.auditscope     = this.getDefaultURLValue('c__scopeId', this.defaultScope);
        this.sortBy         = this.getDefaultURLValue('c__sortBy', this.defaultSortBy);
        this.sortDirection  = this.getDefaultURLValue('c__sortDirection', this.defaultSortDirection);
        this.assignedtome   = this.getDefaultURLValue('c__assignedTo', this.defaultAssignedToMe);
        this.assignment     = this.getDefaultURLValue('c__assignment', this.defaultAssignment);
        this.openclosed     = this.getDefaultURLValue('c__state', this.defaultOpenclosed);
    }

    getDefaultURLValue(getParam, defaultValue){
        // Use the currentPageReference to get the requested
        const urlParam = this.currentPageReference.state[getParam];

        if((urlParam != null || urlParam != undefined)){
            return urlParam;
        }else{
            // Nothing passed via the URL, so we'll set default value from metadata (if exists)
            return defaultValue;
        }

    }

    loadColumns() {
        this.columns = [];

        this.columns.push({
            label: 'Evidence Request', 
            fieldName: 'evidenceRequestItemLink', 
            type: 'url',
            editable: false,
            sortable: true, 
            hideDefaultActions: true,
            typeAttributes: {
                label: {fieldName: 'evidenceRequestItemName'},
                target: '_blank'
            }
        });

        this.columns.push({
            label: 'Request Name', 
            fieldName: 'requestName',
            sortable: true,
            wrapText: true
        });


        this.columns.push({
            label: 'GRC Assignee', 
            fieldName: 'requestAssigneeLink', 
            type: 'url',
            editable: false,
            sortable: true, 
            hideDefaultActions: true,
            typeAttributes: {
                label: {fieldName: 'requestAssigneeName'},
                target: '_blank'
            }
        });

        this.columns.push({
            label: 'Status', 
            fieldName: 'evidenceStatus',
            sortable: true,
            wrapText: true
        });
     
        // this.columns.push({
        //     label: 'Status', 
        //     fieldName: 'evidenceStatus',
        //     type: 'picklist',
        //     editable: true,
        //     sortable: true,
        //     wrapText: true,
        //     initialWidth: 140, 
        //     typeAttributes: {
        //         placeholder: 'Select Status...', 
        //         options: [
        //             { label: 'All', name: 'all' },
        //             { label: 'pendingEngineer', name: 'Pending Engineer' },
        //             { label: 'readyForReview', name: 'Ready for Review' },
        //             { label: 'providedToAuditor', name: 'Provided to Auditor' },
        //             { label: 'returnedToEngineer', name: 'Returned to Engineer' }
        //         ],
        //         field: 'Evidence_Status__c',
        //         value: { fieldName: 'evidenceStatus' },
        //         context: { fieldName: 'Id' }
        //     }, 
        //     actions: [
        //         { label: 'All', checked: true, name: 'all' },
        //         { label: 'pendingEngineer', checked: false, name: 'Pending Engineer' },
        //         { label: 'readyForReview', checked: false, name: 'Ready for Review' },
        //         { label: 'providedToAuditor', checked: false, name: 'Provided to Auditor' },
        //         { label: 'returnedToEngineer', checked: false, name: 'Returned to Engineer' }
        //     ] 
        // });
        this.columns.push({
            label: 'Evidence Upload Date', 
            fieldName: 'overdue', 
            initialWidth: 153,
            sortable: true,
            wrapText: true
        });
        this.columns.push({
            label: 'Evidence File', 
            fieldName: 'fileinfolist',
            sortable: true,
            wrapText: true,
            initialWidth: 295,
            type: 'fileurl',
            typeAttributes: {
                wrap: true, 
                value: {fieldName:'fileinfolist'}
            }
        });
        
    }

    handleAssigned(event){
        let buttonIds = this.buttonsAssigned;
        if(this.assignedtome != event.target.value){
            this.setLoadingSpinner();  
            this.assignedtome = event.target.value;
            
            // Remove the selected area of compliance from the buttonIds array
            buttonIds = buttonIds.filter(button => button !== this.assignedtome);

            // Set the formatting of the selected buttonId
            this.template.querySelector('button[value="' + this.assignedtome + '"]').classList.add('slds-button_brand'); 
            this.template.querySelector('button[value="' + this.assignedtome + '"]').classList.remove('slds-button_neutral'); 

            // Reset formatting of other options
            for(let i = 0; i < buttonIds.length; i++){
                this.template.querySelector('button[value="' + buttonIds[i] + '"]').classList.add('slds-button_neutral'); 
                this.template.querySelector('button[value="' + buttonIds[i] + '"]').classList.remove('slds-button_brand');
            }

            this[NavigationMixin.Navigate](this.updatedPageReference, true);
        }
        
    }

    setLoadingSpinner(){
        if(this.auditscope != undefined && this.auditcycle != undefined){
            this.isLoading = true;
        }else{
            this.isLoading = false;
        }
    }


    handleOpenClosed(event){
        let buttonIds = this.buttonsOpenclosed;

        if(this.openclosed != event.target.value){
            this.setLoadingSpinner();  
            this.openclosed = event.target.value;
            
            // Remove the selected area of compliance from the buttonIds array
            buttonIds = buttonIds.filter(button => button !== this.openclosed);

            // Set the formatting of the selected buttonId
            this.template.querySelector('button[value="' + this.openclosed + '"]').classList.add('slds-button_brand'); 
            this.template.querySelector('button[value="' + this.openclosed + '"]').classList.remove('slds-button_neutral'); 

            // Reset formatting of other options
            for(let i = 0; i < buttonIds.length; i++){
                this.template.querySelector('button[value="' + buttonIds[i] + '"]').classList.add('slds-button_neutral'); 
                this.template.querySelector('button[value="' + buttonIds[i] + '"]').classList.remove('slds-button_brand');
            }

            this[NavigationMixin.Navigate](this.updatedPageReference, true);
        }
    }

    handleSearch(event){
        const rows = JSON.parse(JSON.stringify(this.filteredData)); // raw unfiltered data to start with
        const searchString = event.target.value;
        const searchField = event.target.name;
        var regex = new RegExp(searchString.replace('/[|\\{}()[\]^$+*?.]/g', '\\$&'),'i'); // previously tried gi - i much more reliable
        let filteredRows = rows;

        if(searchString !== ''){
            // TODO: More tech debt here than I would like with all the if/else switches. Couldn't get it working.
            if( searchField === 'searchRequest' ){
                filteredRows = rows.filter(
                    row => regex.test(
                        row.requestName
                    )
                );
            }  else {
                // No search configured.. nothing to do.
            }
        }else{
            // Reset the fields back to original filter
            filteredRows = JSON.parse(JSON.stringify(this.wireData.data));
        }
        // Update the filteredData to respect the filteredRows.
        this.filteredData = filteredRows;
        this.refreshDraftValues();
    }


    refreshDraftValues(){
        this.draftValues.forEach(item => {
            this.updateDataValues(item);
        });
    }

    updateDataValues(updateItem) {
        let changeData = [... JSON.parse(JSON.stringify(this.filteredData))];
        changeData.forEach(item => {
            if (item.Id === updateItem.Id) {
                for (let field in updateItem) {
                  item[field] = updateItem[field];
                }
            }
        });

        this.filteredData = [...changeData];
    }

    handleRefresh(event){
            const searchFields = ['searchRequest'];
            const onchangeEvent = new CustomEvent('change',{bubbles: true});
            if(JSON.stringify(this.wireData
                ) != '{}'){
                refreshApex(this.wireData).then(() => {
                    // Re-apply client-side search filters
                    for(let i = 0; i < searchFields.length; i++){
                        this.template.querySelector('lightning-input[data-field="' + searchFields[i] + '"]').value != '' ? this.template.querySelector('lightning-input[data-field="' + searchFields[i] + '"]').dispatchEvent(onchangeEvent) : null; 
                    }
                });
            }
    }

    handleAuditCycleChange(event) {
        // TODO: Need to also reset the Header Actions selection
            this.setLoadingSpinner();    
            this.auditcycle = event.detail.value;
            this[NavigationMixin.Navigate](this.updatedPageReference, true);
    }

    handleAuditScopeChange(event) {
        this.setLoadingSpinner();
        this.auditscope = event.detail.value;
        this[NavigationMixin.Navigate](this.updatedPageReference, true);
    }

    handleRowAction(event){
        if(event.detail.action.name==='openRequest') {
            this[NavigationMixin.GenerateUrl]({
                type: 'standard__recordPage',
                attributes: {
                    recordId: event.detail.row.Id,
                    actionName: 'view'
                }
            }).then(url => {
                //console.log('Target URL >> ' + url); // Log the target URL in the console
                window.open(url, '_blank'); // open in a new tab/window
            });
        }
    }

    handleHeaderAction (event) {
        // Retrieves the name of the selected filter
        const actionName = event.detail.action.name;
        // Retrieves the current column definition
        const colName = event.detail.columnDefinition.fieldName;
        const filterColumns = this.columns;
        const activeFilter = this.activeFilter;

        // Exclude events that are for wrapText and clipText (default Salesforce header actions)
        if (actionName !== activeFilter && actionName != 'wrapText' && actionName != 'clipText') {
            filterColumns.map((col) => {
                if(col.fieldName === colName){
                    var actions = col.actions;

                    actions.forEach((action) => {
                        action.checked = action.name === actionName;
                    });
                }
            });
            this.activeFilter = actionName;
            this.updateDataTable(colName);
            this.columns = filterColumns;

            // Update the actions list to show which action is currently selected
            filterColumns.find(col => col.fieldName === colName).actions.forEach(action => action.checked = action.name === actionName);
            this.columns = [...filterColumns];
        }
    }

    doSorting(event){
        this.sortBy = event.detail.fieldName;
        this.sortDirection = event.detail.sortDirection;
        this.sortData(this.sortBy, this.sortDirection);
        this[NavigationMixin.Navigate](this.updatedPageReference, true);
    }

    sortData(fieldname, direction){
        let parseData = JSON.parse(JSON.stringify(this.filteredData));

        let keyValue = (a) => {
            return a[fieldname];
        }

        let isReverse = direction ==='asc'?1:-1;
        
        parseData.sort((x, y) => {
            x = keyValue(x) ? keyValue(x) : ''; // handling null values
            y = keyValue(y) ? keyValue(y) : '';
            // sorting values based on direction
            return isReverse * ((x > y) - (y > x));
        });
        this.filteredData = parseData;
        //this.refreshDraftValues();
    }

    get auditCycleOptions(){
        var auditCycleOptions = [];

        if(this.auditCycles){
            this.auditCycles.forEach(field => {
                auditCycleOptions.push({label:field.Name, value:field.Id});
            });
        }
        return auditCycleOptions;
    }

    get auditScopeOptions(){
        var auditScopeOptions = [];

        if(this.auditScopes){   
            for(var scope in this.auditScopes){
                auditScopeOptions.push({label:scope, value:this.auditScopes[scope]});
            }
        }
        return auditScopeOptions;
    }


    get updatedPageReference() {
        return this.getUpdatedPageReference({
            c__scopeId: this.auditscope,
            c__cycleId: this.auditcycle,
            c__sortBy: this.sortBy,
            c__sortDirection: this.sortDirection,
            c__state: this.openclosed,
            c__assignedTo: this.assignedtome,
            c__assignment: this.assignment
        });
    }

    getUpdatedPageReference(stateChanges) {
        return Object.assign({}, this.currentPageReference, {
            state: Object.assign({}, this.currentPageReference.state, stateChanges)
        });
    }
}
import { LightningElement, track, api } from 'lwc';
// Import custom labels
import close from '@salesforce/label/c.ESA_Close';

export default class SldsPopover extends LightningElement {
    label = {
        close
    }
    
    @api
    content;
    @api
    isSticky = false;
    
    @track
    isHidden = true;


    htmlId = 'dialog-body-id-' + Math.random();

    show() {
        this.isHidden = false;
    }

    hide() {
        this.isHidden = true;
    }
}
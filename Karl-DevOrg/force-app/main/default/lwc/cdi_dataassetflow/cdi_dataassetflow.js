import { LightningElement,track} from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import DATALINEAGE_OBJECT from '@salesforce/schema/CDI_Data_Lineage__c';
import DATAASSET_FIELD from '@salesforce/schema/CDI_Data_Lineage__c.Data_Asset__c';
import SOURCEDATAASSET_FIELD from '@salesforce/schema/CDI_Data_Lineage__c.Source_Data_Asset__c';

import SERVICE_FIELD from '@salesforce/schema/CDI_Data_Lineage__c.Service__c';
import CRYPTO_FIELD from '@salesforce/schema/CDI_Data_Lineage__c.Crypto_Algorithm__c';

import INTEGRATIONTYPE_FIELD from '@salesforce/schema/CDI_Data_Lineage__c.Integration_Type__c';
import INGESTIONMODE_FIELD from '@salesforce/schema/CDI_Data_Lineage__c.Ingestion_Mode__c';
import TIMEUNIT_FIELD from '@salesforce/schema/CDI_Data_Lineage__c.Time_Unit__c';
import REFRESHCADENCE_FIELD from '@salesforce/schema/CDI_Data_Lineage__c.Refresh_Cadence__c';
import COMMENTS_FIELD from '@salesforce/schema/CDI_Data_Lineage__c.Comments__c';
/**
 * Creates Account records.
 */
export default class DataAssetFlow extends NavigationMixin(LightningElement) {
    datalineageObject = DATALINEAGE_OBJECT;
    dataassetField = DATAASSET_FIELD;
    sourceField = SOURCEDATAASSET_FIELD;
    serviceField=SERVICE_FIELD; 
    cryptoField=CRYPTO_FIELD;
   
   
    integrationtypeField=INTEGRATIONTYPE_FIELD;

    ingestionmodeField=INGESTIONMODE_FIELD;
    timeunitField=TIMEUNIT_FIELD;
    refreshcadenceField=REFRESHCADENCE_FIELD;
    commentsField=COMMENTS_FIELD;
    @track bShowModal = false;
    /* javaScipt functions start */ 
    openModal(event) {    
        // to open modal window set 'bShowModal' tarck value as true
       
       
        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                recordId: event.detail.id,
                objectApiName: 'CDI_Data_Lineage__c',
                actionName: 'view',
            },
        });
        
    }
   
    closeModal() {    
        // to close modal window set 'bShowModal' tarck value as false
        this.bShowModal = false;
        location.reload();
       
    }
}
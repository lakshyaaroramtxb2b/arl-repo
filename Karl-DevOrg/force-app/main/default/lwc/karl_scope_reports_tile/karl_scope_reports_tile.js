import { LightningElement, api, track } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import KARL_Superuser from '@salesforce/customPermission/KARL_Superuser';

/**
 * @author Ben Harvie
 * @email ben.harvie@salesforce.com
 * @desc This LWC Component Builds Tiles for KARL_Scope_Reports LWC
 */

export default class Karl_scope_reports_tile extends NavigationMixin(LightningElement) {
    @api recordId;
    @api parentobject;
    @api reportitem;
    @api associationtype;
    @track recordPageId;
    @track recordPageUrl;
    @track targetrecord;
    @track error;
    @track inclusionText = false;

    /** 
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Standard Connected Callback
     */
    connectedCallback() {
        // Show Inclusion Text
        this.inclusionText = this.parentobject == 'KARL_Audit_Scope_Master_Data__c' ? true : false;

        // Set the target URL based on association type
        if(this.associationtype === 'Parent'){
            this.recordPageId = this.reportitem.KARL_Audit_Scope_Report__c;
        }else if(this.associationtype === 'Child'){
            this.recordPageId = this.reportitem.KARL_Parent_Audit_Scope__c;
        }else{
            this.recordPageId = this.reportitem.Id;
        }

        // Targeting the Test__c in this instance (instead of the junction object)
        this[NavigationMixin.GenerateUrl]({
            type: 'standard__recordPage',
            attributes: {
                recordId: this.recordPageId,
                actionName: 'view',
            },
        }).then(url => {
            this.recordPageUrl = url;
        });
    }

    /** 
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Create On Click Event
     */
    handleOpenRecordClick() {
        // Targeting the Test__c in this instance (instead of the junction object)
        const selectEvent = new CustomEvent('karlrecordview', {
            detail: this.recordPageId
        });
        this.dispatchEvent(selectEvent);
    }

    /** 
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Edit junction object record on Button Click.
     */
    handleEditClick() {
        // Send REQCT ID via the Event system
        const selectEvent = new CustomEvent('karledit', {
            detail: this.reportitem.Id
        });
        this.dispatchEvent(selectEvent);
    }

    /** 
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Edit junction object record on Button Click.
     */
    handleDeleteClick() {
        // Send REQCT ID via the Event system
        const selectEvent = new CustomEvent('karldelete', {
            detail: this.reportitem.Id
        });
        this.dispatchEvent(selectEvent);
    }

    /** 
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @desc Return true if associationType is Primary
     */
    get isPrimary(){
        if(this.associationtype == 'Primary'){
            return true;
        }else{
            return false;  
        }
    }

    /** 
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @desc Return true if associationType is Parent
     */
    get isParent(){
        if(this.associationtype == 'Parent'){
            return true;
        }else{
            return false;  
        }
    }

    /** 
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @desc Return true if associationType is Child
     */
    get isChild(){
        if(this.associationtype == 'Child'){
            return true;
        }else{
            return false;  
        }
    }

    /** 
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @desc Return true if associationType is Child
     */
    get isChildMaster(){
        if(this.associationtype == 'Child' && this.parentobject === 'KARL_Audit_Scope_Master_Data__c'){
            return true;
        }else{
            return false;  
        }
    }

    /** 
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @desc Return true if the user has the Custom Permission for KARL_Superuser
     * Returns undefined if not set.
     */
    get isKarlAdmin(){
        //console.log(' Is KARL Superuser? >> ' + KARL_Superuser);
        return KARL_Superuser;
    }

    /** 
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @desc Return true if the user has the Custom Permission for KARL_Superuser
     * Returns undefined if not set.
     */
    get notKarlAdmin(){
        //console.log(' Is NOT KARL Superuser? >> ' + !KARL_Superuser);
        return !KARL_Superuser;
    }
}
import { NavigationMixin } from 'lightning/navigation';
import { LightningElement, api, wire, track } from 'lwc';
import { refreshApex } from '@salesforce/apex';
import userId from '@salesforce/user/Id';
import getSubmittedServiceDeskInfo from '@salesforce/apex/KARL_ServiceDeskItemsController.getSubmittedServiceDeskInfo';

export default class Karl_service_desk_items extends NavigationMixin(LightningElement) {    
	@track records;
	@track error;
	@track wireData;
	@track serviceDeskItemsList;

    /** 
     * @author Lakshya Arora
     * @email lakshya.arora@mtxb2b.com
     * @desc Wire the relevant Service Desk Records
     * 
     */
	@wire(getSubmittedServiceDeskInfo) wiredResult(result) {
		this.wireData = result;
		if ( result.data && result.data.length ){
			// ! Check if the wire returns any data
			// Error handle the response just in case there is an issue returning the data
			try {
				this.records = true; // specify boolean for if:true
				this.serviceDeskItemsList = result.data; // establish the data as cReqs
				this.error = false;
			} catch(e) {
				console.log('KARL LWC Error', result.error)
			}
		} else if ( result.data && !result.data.length ){
			// ! Check if the wire is null
			this.records = false; // specify boolean for if:false
            console.log('No Data');
		} else if (result.error){
			// ! Check if the wire returns an error, and return the error
			console.log('KARL LWC Error', result.error)
			this.error = result.error;
			this.records = false;
		}
	}

    /** 
     * @author Lakshya Arora
     * @email lakshya.arora@mtxb2b.com
     * @desc Refresh apex to capture changes made upstream
	 * @param event - js event handler
     */
	handleRefresh(event){
		refreshApex(this.wireData);
	}

    /** 
     * @author Lakshya Arora
     * @email lakshya.arora@mtxb2b.com
     * @desc Standard handleRecordView component for KARL LWC's to navigate to Record Pages
	 * @param event - js event handler
     */
	handleRecordView(event) {
		// Get the Target Record ID from the event (typically onClick event)
		const targetRecordId = event.detail.Id;
		// Navigate to the record id
		this[NavigationMixin.Navigate]({
			type: 'standard__recordPage',
			attributes: {
				recordId: targetRecordId,
				actionName: 'view',
			},
		});
	}


}
import { LightningElement, track, wire } from 'lwc';
import { CurrentPageReference, NavigationMixin} from 'lightning/navigation';
import { registerListener, unregisterAllListeners } from 'c/pubsub';

import details from '@salesforce/label/c.ESA_Details';
import recordPage from '@salesforce/label/c.ESA_RecordPage';

export default class IntakeRequestDetailsDialog extends NavigationMixin(LightningElement) {
    @track
    id

    label = {
        details, recordPage
    }

    @wire(CurrentPageReference) pageRef;

    connectedCallback() {
        registerListener('openRequestDetails', this.showDetails, this);
    }
    disconnectedCallback() {
		// unsubscribe
		unregisterAllListeners(this);
    }

    show() {
        this.template.querySelector("c-slds-modal").show();
    }

    hide() {
        this.template.querySelector("c-slds-modal").hide();
    }
    
    showDetails(id) {
        this.id = id;
        this.show();
    }

    navigateToRecordPage() {
        // Generate a URL to a User record page
        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                recordId: this.id,
                actionName: 'view',
            }
        });
       
    }
}
import { LightningElement, api, track } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';


export default class Karl_audit_firm_stakeholdertile extends NavigationMixin(LightningElement) {
    @api recordId;
    @api record;
    @track recordPageUrl;

    /** 
     * @author Lakshya Arora
     * @email lakshya.arora@mtxb2b.com
     * @description Standard Connected Call Back for Child
     * @modifier recordId to be modified based on the object in question
     */
     connectedCallback() {
        // Generate a URL to the relevant record
        this[NavigationMixin.GenerateUrl]({
            type: 'standard__recordPage',
            attributes: {
                recordId: this.record.Id,
                actionName: 'view',
            },
        }).then(url => {
            this.recordPageUrl = url;
        });
    }

    /** 
     * @author Lakshya Arora
     * @email lakshya.arora@mtxb2b.com
     * @description Create On Click Event
     * @modifier recordId to be modified based on the object in question
     */
    handleOpenRecordClick() {
        const selectEvent = new CustomEvent('karlrecordview', {
            detail: this.record.Id
        });
        this.dispatchEvent(selectEvent);
    }

    /** 
     * @author Lakshya Arora
     * @email lakshya.arora@mtxb2b.com
     * @description Styling for Icons (Full Name)
     */
    get iconFullStatus(){
        if( this.record.Issue_Status__c == "Approved" || this.record.Issue_Status__c == "Closed" ){
            return "utility:success";
        }else if( this.record.Issue_Status__c == "Not Applicable" ){
            return "utility:lower_flag";
        }else{
            return "utility:warning";
        }
    }

    /** 
     * @author Lakshya Arora
     * @email lakshya.arora@mtxb2b.com
     * @description Styling for Icons (Variant Styling)
     */
    get iconShortStatus(){
        if( this.record.Issue_Status__c == "Approved" || this.record.Issue_Status__c == "Closed" ){
            return "success";
        }else{
            return "warning";
        }
    }

}
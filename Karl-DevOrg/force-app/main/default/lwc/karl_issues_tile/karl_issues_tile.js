import { LightningElement, api, track } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';

/**
 * @author Ben Harvie
 * @email ben.harvie@salesforce.com
 * @desc This LWC Component Builds the tiles as a part of the CREQ Component on Requests Master Data
 */

export default class Karl_issues_tile extends NavigationMixin(LightningElement) {
    @api recordId;
    @api record;
    @track recordPageUrl;

    /** 
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Standard Connected Call Back for Child
     * @modifier recordId to be modified based on the object in question
     */
     connectedCallback() {
        // Generate a URL to the relevant record
        this[NavigationMixin.GenerateUrl]({
            type: 'standard__recordPage',
            attributes: {
                recordId: this.record.Id,
                actionName: 'view',
            },
        }).then(url => {
            this.recordPageUrl = url;
        });
    }

    /** 
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Create On Click Event
     */
    handleOpenRecordClick() {
        const selectEvent = new CustomEvent('karlrecordview', {
            detail: this.record.Id
        });
        this.dispatchEvent(selectEvent);
    }

    /** 
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Styling for Icons (Full Name)
     */
    get iconFullStatus(){
        if( this.record.Issue_Status__c == "Approved" || this.record.Issue_Status__c == "Closed" ){
            return "utility:success";
        }else if( this.record.Issue_Status__c == "Not Applicable" ){
            return "utility:lower_flag";
        }else{
            return "utility:warning";
        }
    }

    /** 
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Styling for Icons (Variant Styling)
     */
    get iconShortStatus(){
        if( this.record.Issue_Status__c == "Approved" || this.record.Issue_Status__c == "Closed" ){
            return "success";
        }else{
            return "warning";
        }
    }

    /** 
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description management response decorator
     */
    get managementResponse(){
        if( this.record.Issue_Status__c == "Approved" || this.record.Issue_Status__c == "Closed" ){
            return "Management response is approved.";
        }else{
            return "Management response is pending.";
        }
    }

    /** 
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description qualification issue decorator
     */
    get managementResponseBoolean(){
        if( this.record.Issue_Status__c == "Approved" || this.record.Issue_Status__c == "Closed" ){
            return true;
        }else{
            return false;
        }
    }

    /** 
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description qualification issue decorator
     */
    get iemCaseBoolean(){
        if( this.record.KARL_KAI_IEM_Case__c != null || this.record.KARL_KAI_IEM_Case__c != undefined ){
            return true;
        }else{
            return false;
        }
    }
    
}
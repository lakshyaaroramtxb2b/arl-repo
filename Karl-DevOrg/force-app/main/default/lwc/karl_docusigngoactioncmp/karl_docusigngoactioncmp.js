import { LightningElement,api } from 'lwc';
import sendDocusignDocuments from '@salesforce/apex/KARL_DocuSignGoController.sendDocusignDocuments';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class Karl_docusigngoactioncmp extends LightningElement {
    @api recordId;
    connectedCallback(){
        sendDocusignDocuments({
            cycleAuditReportId: this.recordId
        })
        .then(result=>{
            const toastEvent = new ShowToastEvent({
                title: 'Envelope send Successfully',
                message: 'Envelope send Successfully',
                variant: 'success'
            });
            this.dispatchEvent(toastEvent);
        })
        .catch(error=>{
            console.log('error occurred : ',error.message || error.body.message);
        })
    }
}
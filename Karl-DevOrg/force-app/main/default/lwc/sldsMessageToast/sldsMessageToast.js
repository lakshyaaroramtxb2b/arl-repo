import { LightningElement, track, api} from 'lwc';
// Import custom labels
import close from '@salesforce/label/c.ESA_Close';

export default class SldsMessageToast extends LightningElement {
    label = {
        close
    }

    @track
    isHidden = true;

    @api
    message;

    @api
    type;

    connectedCallback() {
        if (this.message) {
            this.isHidden = false;
        }
    }

    @api
    show(message, type) {
        this.type = type || 'success';
        this.message = message;
        this.isHidden = false;
    }

    @api
    hide() {
        this.message = '';
        this.isHidden = true;
    }
}
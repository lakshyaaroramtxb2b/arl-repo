import { LightningElement,api } from 'lwc';

export default class Karl_AccessNotGranted extends LightningElement {
    @api title;
    @api subtitle;
    @api errormessage = false;

    get containerClass() {
        if (this.errormessage)
            return 'slds-card rcolor slds-m-around_large';
        else
            return 'slds-card bcolor slds-m-around_large';
    }
}
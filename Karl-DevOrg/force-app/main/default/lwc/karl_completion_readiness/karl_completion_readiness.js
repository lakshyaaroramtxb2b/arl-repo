import { LightningElement,api,track, wire } from 'lwc';
import { publish, MessageContext } from 'lightning/messageService'; 
import reportUpdated from '@salesforce/messageChannel/KARL_Report_Updated__c';
import getFinalizationReadinessInfo from '@salesforce/apex/KARL_FinalizationReadinessController.getFinalizationReadinessInfo';
import updateCycleAuditReport from '@salesforce/apex/KARL_FinalizationReadinessController.updateCycleAuditReport';
import KARL_Superuser from '@salesforce/customPermission/KARL_Superuser';
import KARL_Audit_Manager from '@salesforce/customPermission/KARL_Audit_Manager';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class Karl_completion_readiness extends LightningElement {
    @api recordId;
    @track recordInfoWrapper = {};
    @track showSpinner = false;

    @wire(MessageContext)
    messageContext;

    connectedCallback(){
        this.showSpinner = true;
        getFinalizationReadinessInfo({recordId : this.recordId})
        .then(result=>{
            this.recordInfoWrapper = result;
            this.showSpinner = false;
        })
        .catch(error=>{
            console.log('Error occurred = ',error.message || error.body.message);
            this.showSpinner = false;
        });
    }

    handleChange(event){
        var eventName = event.target.name;
        var beforeChangeValue = this.recordInfoWrapper[eventName];
        var fieldLabel = event.target.label;
        var fieldName = event.target.dataset.field;
        var fieldValue = event.target.checked;
        this.showSpinner = true;
        updateCycleAuditReport({
            fieldName : fieldName,
            fieldValue : fieldValue,
            recordId : this.recordId
        }).then(result=>{
            this.recordInfoWrapper[eventName] = fieldValue;
            this.showSpinner = false;
            const toastEvent = new ShowToastEvent({
                title: 'Updated Successfully',
                message: fieldLabel+' updated successfully',
                variant: 'success'
            });
            // Push message to visual LWC for refreshApex
            const payload = { update: 'success' };
            publish(this.messageContext, reportUpdated, payload);
            this.dispatchEvent(toastEvent);
        }).catch(error=>{
            this.recordInfoWrapper[eventName] = beforeChangeValue;
            this.showSpinner = false;
            var errorMessage = (error.message || error.body.message);
            const toastEvent = new ShowToastEvent({
                title: 'Error',
                message: errorMessage,
                variant: 'error'
            });
            this.dispatchEvent(toastEvent);
        });
    }

    /** 
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Return true if the user has the Custom Permission for ARL_Is_External_Auditor
     * @return returns undefined if not set, or true if auditor flag is set
     */
    get isExternalAuditor(){
        //console.log(' Is external auditor? >> ' + ARL_Is_External_Auditor);
        return ARL_Is_External_Auditor;
    }

    /** 
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Return true if the user does NOT have either of KARL_Superuser or KARL_Audit_Manager
     * custom permissions (provided by permission sets). Logic is backwards due to the requirement to 
     * disable fields based on non-SCEA Ops or Audit Manager permission sets.
     * @return returns false if matches, or true if not a KARL standard user.
     */
    get notKarlStandardUser(){
        //const karlStandardUser = KARL_Superuser || KARL_Audit_Manager ? false : true;
        //console.log(' Is KARL Standard User? >> ' + !karlStandardUser);
        return KARL_Superuser || KARL_Audit_Manager ? false : true;
    }

    /** 
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @desc Return true if the user has the Custom Permission for KARL_Superuser
     * Returns undefined if not set.
     */
    get isKarlAdmin(){
        //console.log(' Is KARL Superuser? >> ' + KARL_Superuser);
        return KARL_Superuser;
    }

    /** 
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @desc Return true if the user has the Custom Permission for KARL_Superuser
     * Returns undefined if not set.
     */
    get notKarlAdmin(){
        //console.log(' Is NOT KARL Superuser? >> ' + !KARL_Superuser);
        return !KARL_Superuser;
    }
}
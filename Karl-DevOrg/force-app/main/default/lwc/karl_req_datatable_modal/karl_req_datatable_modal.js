import { NavigationMixin } from 'lightning/navigation';
import { api, LightningElement, track, wire } from 'lwc';
import { getRecord, getFieldValue } from 'lightning/uiRecordApi';
import RECORD_NAME from '@salesforce/schema/Request_Item__c.Name';

/**
 * @author Ben Harvie
 * @email ben.harvie@salesforce.com
 * @description This LWC Component Builds a Modal for Create and Update requests on REQ
 */
 const fields = [
    RECORD_NAME
];
export default class Karl_req_datatable_modal extends NavigationMixin(LightningElement) {
    @api closeLabel = 'Cancel';
    @api submitLabel = 'Save';
    @api targetrecord;
    @track isRendered = false;
    @track recordName;
    @track records;
    @track wireData;

    recordLink;
    closeModal() {
        this.dispatchEvent(new CustomEvent('close'));
    }

    handleSubmit() {
        this.dispatchEvent(new CustomEvent('submit'));
    }
    @wire(getRecord, { recordId: '$targetrecord', fields }) wiredResult(result){
        console.log(JSON.stringify(result));
        if(result.data){
            this.wireData = result;
            this.recordName = result.data.fields.Name.value; // directly access the record name
            this.records = true;
        }else if(result.error){
            console.log(result.error);
        }
    }

    connectedCallback(){
        this[NavigationMixin.GenerateUrl]({
            type: 'standard__recordPage',
            attributes: {
                recordId: this.targetrecord,
                actionName: 'view'
            }
        }).then(url => {
            this.recordLink = url;
        });
    }
}
import {
    LightningElement, 
    wire,
    api,
    track
} from 'lwc';
import {
    refreshApex
} from '@salesforce/apex';
import {
    NavigationMixin
} from 'lightning/navigation';

/**
 * @author Ben Harvie
 * @email ben.harvie@salesforce.com
 * @description This LWC Component is an extension LWC for karl_arl_datatable which provides
 * quick action capabilities for productivity.
 */

// Import Apex Classes
import getAriReport from '@salesforce/apex/KARL_ARLReportController.getAriReport';
import getGusReport from '@salesforce/apex/KARL_ARLReportController.getGusReport';
import getUploadWorkflowReport from '@salesforce/apex/KARL_ARLReportController.getUploadWorkflowReport';

export default class Karl_arl_datatable_extension extends NavigationMixin(LightningElement) {
    // Data Management from Parent
    @api auditcycle;
    @api auditscope;
    @api areaofcompliance;
    @api assignedtome;
    @api typefilter;

    // Wire Data Input / Management
    @track error;
    @track ariTotalRows;
    @track ariRecords;
    @track gusTotalRows;
    @track gusRecords;
    @track evidenceTotalRows;
    @track evidenceRecords;

    // Wire Data Output
    @track filteredAriData = [];
    @track ariWireData; 
    @track filteredGusData = [];
    @track gusWireData; 
    @track filteredEvidenceData = [];
    @track evidenceWireData; 

    @track showModal = false;
    @track modalTitle = '';
    @track modalText = '';
    @track modalData = [];
    @track modalCols = [];

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Wire to collect Auditor (ARI) Statuses
     */
     @wire(getAriReport, { auditCycleId: '$auditcycle', auditScopeId: '$auditscope', areaOfCompliance: '$areaofcompliance', 
     filterAssigned: '$assignedtome'})  wiredARI(result) {
         this.ariWireData = result; // Save the full result, for diffing refreshApex later
 
         //console.log("KARL Quick Action >> Parameters are set - data feed updating!");
 
         if ( result.data && result.data.length ){
             // ! Check if the wire returns any data
             try {
                 this.ariRecords = true; // Specify boolean for if:true
                 this.filteredAriData = result.data; 
                 this.isLoading = false;
                 this.error = false;
                 this.ariTotalRows = result.data.length;
             } catch(e) {
                 console.log('KARL >> LWC Error', result.error);
                 this.isLoading = false;
             }
         } else if ( result.data && !result.data.length ){
             // ! Check if the wire is null
             //console.log('KARL >> LWC Empty', result.data);
             this.ariRecords = false; // specify boolean for if:false
             this.filteredAriData = result.data; 
             this.isLoading = false;
             this.ariTotalRows = result.data.length;
         } else if (result.error){
             // ! Check if the wire returns an error, and return the error
             if(!this.auditcycle){
                 // False error --> just waiting on parameters
                 //console.log('KARL >> LWC Parameters not set');
                 this.ariRecords = false; // specify boolean for if:false
                 this.isLoading = false;
             }else{
                 // Legitimate error
                 console.log('KARL >> LWC Error', result.error);
                 this.error = result.error;
                 this.isLoading = false;
                 this.ariRecords = false;
             }
         }
     }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Wire to collect GUS Statuses
     */
     @wire(getGusReport, { auditCycleId: '$auditcycle', auditScopeId: '$auditscope', areaOfCompliance: '$areaofcompliance', 
     filterAssigned: '$assignedtome'})  wiredGUS(result) {
         this.gusWireData = result; // Save the full result, for diffing refreshApex later
 
         //console.log("KARL Quick Action >> Parameters are set - data feed updating!");
 
         if ( result.data && result.data.length ){
             // ! Check if the wire returns any data
             try {
                 this.gusRecords = true; // Specify boolean for if:true
                 this.filteredGusData = result.data; 
                 this.isLoading = false;
                 this.error = false;
                 this.gusTotalRows = result.data.length;
             } catch(e) {
                 console.log('KARL >> LWC Error', result.error);
                 this.isLoading = false;
             }
         } else if ( result.data && !result.data.length ){
             // ! Check if the wire is null
             //console.log('KARL >> LWC Empty', result.data);
             this.gusRecords = false; // specify boolean for if:false
             this.filteredGusData = result.data; 
             this.isLoading = false;
             this.gusTotalRows = result.data.length;
         } else if (result.error){
             // ! Check if the wire returns an error, and return the error
             if(!this.auditcycle){
                 // False error --> just waiting on parameters
                 //console.log('KARL >> LWC Parameters not set');
                 this.gusRecords = false; // specify boolean for if:false
                 this.isLoading = false;
             }else{
                 // Legitimate error
                 console.log('KARL >> LWC Error', result.error);
                 this.error = result.error;
                 this.isLoading = false;
                 this.gusRecords = false;
             }
         }
     }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Wire to collect Evidence Workflow Statuses
     */
     @wire(getUploadWorkflowReport, { auditCycleId: '$auditcycle', auditScopeId: '$auditscope', areaOfCompliance: '$areaofcompliance', 
     filterAssigned: '$assignedtome'})  wiredEvidence(result) {
         this.evidenceWireData = result; // Save the full result, for diffing refreshApex later
 
         //console.log("KARL Quick Action >> Parameters are set - data feed updating!");
 
         if ( result.data && result.data.length ){
             // ! Check if the wire returns any data
             try {
                 this.evidenceRecords = true; // Specify boolean for if:true
                 this.filteredEvidenceData = result.data; 
                 this.isLoading = false;
                 this.error = false;
                 this.evidenceTotalRows = result.data.length;
             } catch(e) {
                 console.log('KARL >> LWC Error', result.error);
                 this.isLoading = false;
             }
         } else if ( result.data && !result.data.length ){
             // ! Check if the wire is null
             //console.log('KARL >> LWC Empty', result.data);
             this.evidenceRecords = false; // specify boolean for if:false
             this.filteredEvidenceData = result.data; 
             this.isLoading = false;
             this.evidenceTotalRows = result.data.length;
         } else if (result.error){
             // ! Check if the wire returns an error, and return the error
             if(!this.auditcycle){
                 // False error --> just waiting on parameters
                 //console.log('KARL >> LWC Parameters not set');
                 this.evidenceRecords = false; // specify boolean for if:false
                 this.isLoading = false;
             }else{
                 // Legitimate error
                 console.log('KARL >> LWC Error', result.error);
                 this.error = result.error;
                 this.isLoading = false;
                 this.evidenceRecords = false;
             }
         }
     }

     /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Open the ARI Modal
     */
     openAriModal(event){
        // Show the ARI Modal
        this.modalCols = [
            { 
                label: 'Audit Team', 
                fieldName: 'auditorTeam',
                wrapText: true, 
                initialWidth: 130 
            },
            { 
                label: 'Request', 
                fieldName: 'requestNumber',
                initialWidth: 110
            },
            { 
                label: 'Request Name', 
                fieldName: 'requestName',
                wrapText: true, 
                initialWidth: 130 
            },
            { 
                label: 'SFDC Status', 
                fieldName: 'sfdcStatus',
                wrapText: true, 
                initialWidth: 110 
            },
            { 
                label: 'Auditor Status', 
                fieldName: 'auditorStatus',
                wrapText: true, 
                initialWidth: 110 
            },
            { 
                label: 'GRC Assignee', 
                fieldName: 'grcAssignee',
                wrapText: true, 
                initialWidth: 140 
            },
            { 
                label: 'SFDC Comments', 
                fieldName: 'sfdcComments',
                wrapText: true
            },
            { 
                label: 'Auditor Comments', 
                fieldName: 'auditorComments',
                wrapText: true
            },
            {   
                type:  'button',
                fixedWidth: 150,
                hideDefaultActions: true,
                typeAttributes: {
                    iconName: 'utility:new_window',
                    label: 'Open CREQ',
                    name: 'openRequest', 
                    title: 'Open Record', 
                    disabled: false
                }
            }
        ];
        this.modalTitle = 'Auditor Request Follow-ups';
        this.modalText = 'The following Auditor Request Items (ARI) have a status = "Follow-ups"';
        this.modalData = this.filteredAriData;
        this.showModal = true;
     }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Open the GUS Modal
     */
     openGusModal(event){
        // Show the GUS Modal
        this.modalCols = [
            { 
                label: 'Request', 
                fieldName: 'requestNumber',
                initialWidth: 110
            },
            { 
                label: 'Request Name', 
                fieldName: 'requestName',
                wrapText: true, 
                initialWidth: 180 
            },
            { 
                label: 'SFDC Status', 
                fieldName: 'sfdcStatus',
                wrapText: true, 
                initialWidth: 110 
            },
            { 
                label: 'GRC Assignee', 
                fieldName: 'grcAssignee',
                wrapText: true, 
                initialWidth: 140 
            },
            { 
                label: 'GUS Ticket', 
                type: 'url',
                name: 'externalLink',
                fieldName: 'gusLink',
                wrapText: true, 
                initialWidth: 160,
                typeAttributes: {
                    label: {fieldName: 'gusID'},
                    target: '_blank'
                }
            },
            { 
                label: 'GUS Assignee', 
                fieldName: 'gusAssignee',
                wrapText: true, 
                initialWidth: 140 
            },
            { 
                label: 'Ticket Details', 
                fieldName: 'gusDetails',
                wrapText: true
            },
            {   
                type:  'button',
                fixedWidth: 150,
                hideDefaultActions: true,
                typeAttributes: {
                    iconName: 'utility:new_window',
                    label: 'Open CREQ',
                    name: 'openRequest', 
                    title: 'Open Record', 
                    disabled: false
                }
            }
            /* Disabled due to real-estate issues (ideally want to have these stacked)
            {   
                type:  'button',
                fixedWidth: 150,
                hideDefaultActions: true,
                typeAttributes: {
                    iconName: 'utility:new_window',
                    label: 'Open GUS',
                    name: 'openGUS', 
                    title: 'Open Record', 
                    disabled: false
                }
            }*/
        ];
        this.modalTitle = 'GUS Ready for Review';
        this.modalText = 'The following requests GUS tickets are marked as Ready for Review';
        this.modalData = this.filteredGusData;
        this.showModal = true;
     }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Open the Evidence Upload Modal
     */
     openEvidenceModal(event){
        // Show the Evidence Upload Workflow Modal
        this.modalCols = [
            { 
                label: 'Request', 
                fieldName: 'requestNumber',
                initialWidth: 110
            },
            { 
                label: 'Request Name', 
                fieldName: 'requestName',
                wrapText: true, 
                initialWidth: 180 
            },
            { 
                label: 'SFDC Status', 
                fieldName: 'sfdcStatus',
                wrapText: true, 
                initialWidth: 110 
            },
            { 
                label: 'GRC Assignee', 
                fieldName: 'grcAssignee',
                wrapText: true, 
                initialWidth: 140 
            },
            { 
                label: 'GUS Ticket', 
                type: 'url',
                fieldName: 'gusLink',
                wrapText: true, 
                initialWidth: 160,
                typeAttributes: {
                    label: {fieldName: 'gusID'},
                    target: '_blank'
                },
                cellAttributes: 
                { 
                    class: { fieldName: 'cellColor' }
                }
            },
            { 
                label: 'GUS Assignee', 
                fieldName: 'gusAssignee',
                wrapText: true, 
                initialWidth: 140,
                cellAttributes: 
                { 
                    class: { fieldName: 'cellColor' }
                } 
            },
            { 
                label: 'Ticket Details', 
                fieldName: 'gusDetails',
                wrapText: true
            },
            {   
                type:  'button',
                fixedWidth: 150,
                hideDefaultActions: true,
                typeAttributes: {
                    iconName: 'utility:new_window',
                    label: 'Open ER',
                    name: 'openRequest', 
                    title: 'Open Record', 
                    disabled: false
                }
            }
        ];
        this.modalTitle = 'Evidence Ready for Review';
        this.modalText = 'The following evidence requests have Evidence Upload records that are marked as Ready for Review';
        this.modalData = this.filteredEvidenceData;
        this.showModal = true;
     }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Close the modals
     */
     closeModal(event){
        // Close the modals
        this.showModal = false;
     }

     /**
      * @author Ben Harvie
      * @email ben.harvie@salesforce.com
      * @description Handle rowActions in the data table, e.g. to take the user to the resource page in a new window.
      * @param event - js event handle
      */
     handleRowAction(event){
         //console.log(JSON.stringify(event.detail.action) + ' on row ' + JSON.stringify(event.detail.row));
         if(event.detail.action.name==='openRequest') {
             this[NavigationMixin.GenerateUrl]({
                 type: 'standard__recordPage',
                 attributes: {
                     recordId: event.detail.row.Id,
                     actionName: 'view'
                 }
             }).then(url => {
                 //console.log('Target URL >> ' + url); // Log the target URL in the console
                 window.open(url, '_blank'); // open in a new tab/window
             });
         }else if(event.detail.action.name==='openGUS'){
            this[NavigationMixin.Navigate]({
                "type": "standard__webPage",
                "attributes": {
                    "url": event.detail.row.gusLink
                }
            });
         }
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Handle refreshing the datatable based on button click
     * @param event - js event handle (not used - simply a trigger)
     */
    @api
     handleRefresh(){
        refreshApex(this.ariWireData);
        refreshApex(this.gusWireData);
        refreshApex(this.evidenceWireData);
    }
    
}
import { LightningElement,wire,api,track } from 'lwc';
import getResults from '@salesforce/apex/CDI_LwcCustomLookupController.getResults';

export default class LwcCustomLookup extends LightningElement {
    @api objectName;
    @api fieldName ;
    @api selectRecordId = '';
    @api selectRecordName;
    @api secondaryfieldName;
    @api label;
    @api indexValue;
    @api searchRecords = [];
    @api required = false;
    @api LoadingText = false;
    @track txtclassname = 'slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click';
    @track messageFlag = false;
    @track iconFlag =  true;
    @track clearIconFlag = false;
    @track inputReadOnly = false;
   

    searchField(event) {
        var currentText = event.target.value;
        this.LoadingText = true;
        var checkDropDown=true;
        getResults({ ObjectName: this.objectName, fieldName: this.fieldName, secondaryFieldName:this.secondaryfieldName, value:currentText })
        .then(result => {
            this.searchRecords= result;
            this.LoadingText = false;
            
            this.txtclassname =  result.length > 0 ? 'slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click slds-is-open' : 'slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click';
            if(currentText.length > 0 && result.length == 0) {
                this.messageFlag = true;
            }
            else {
                this.messageFlag = false;
            }

            if(this.selectRecordId != null && this.selectRecordId.length > 0) {
                this.iconFlag = false;
                this.clearIconFlag = true;
            }
            else {
                this.iconFlag = true;
                this.clearIconFlag = false;
            }
        })
        .catch(error => {
            console.log('-------error-------------'+error);
            console.log(error);
        });
        const selectedEvent = new CustomEvent('selectedsearchfields', { detail: {checkDropDown,currentText}, });
        // Dispatches the event.
        this.dispatchEvent(selectedEvent);
    }
    
   setSelectedRecord(event) {
        var currentRecId = event.currentTarget.dataset.id;
        var selectName = event.currentTarget.dataset.name;
        var secrecNames=event.currentTarget.dataset.secrecname;
        this.txtclassname =  'slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click';
        this.iconFlag = false;
        var checkDropDown=false;
        this.clearIconFlag = true;
        this.selectRecordName = event.currentTarget.dataset.name;
        this.selectRecordId = currentRecId;
        this.inputReadOnly = true;
        var indexValue=this.indexValue;
        const selectedEvent = new CustomEvent('selected', { detail: {selectName, currentRecId,indexValue,secrecNames,checkDropDown}, });
        // Dispatches the event.
        this.dispatchEvent(selectedEvent);
    }
    
    resetData(event) {
        this.selectRecordName = "";
        this.selectRecordId = "";
        this.inputReadOnly = false;
        this.iconFlag = true;
        this.clearIconFlag = false;
        var checkDropDown=false;
        const selectedEvent = new CustomEvent('clear', { detail: {checkDropDown}, });
        // Dispatches the event.
        this.dispatchEvent(selectedEvent);
       
    }

}
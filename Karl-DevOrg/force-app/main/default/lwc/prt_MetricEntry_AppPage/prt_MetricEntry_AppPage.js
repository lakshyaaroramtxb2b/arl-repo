import { LightningElement, track, api } from 'lwc';
import findRecords from '@salesforce/apex/PRT_SLAMetricCreationController.findRecords';
import getEmptySLAMetricRecords from '@salesforce/apex/PRT_SLAMetricCreationController.getEmptySLAMetricRecords';
import createSLAMetricRecord from '@salesforce/apex/PRT_SLAMetricCreationController.createSLAMetricRecord';
import getSLAMetricRecords from '@salesforce/apex/PRT_SLAMetricCreationController.getSLAMetricRecords'; 
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class Prt_MetricEntry_AppPage extends LightningElement {
    @track slaMetricColumns = [
        {label: 'SLA', fieldName: 'SLAName', type: 'text'},
        {label: 'Value', fieldName: 'Value__c', type: 'number', cellAttributes: { alignment: 'left' }},
        {label: 'Target Value and Unit of Measure', fieldName: 'SLATargetValueAndUnitOfMeasure', type: 'text', cellAttributes: { alignment: 'left' }},
        {label: 'Comment', fieldName: 'Comment__c', type: 'text'},
        {label: 'Reporting Period', fieldName: 'SLAReportingPeriod', type: 'text'},
        {label: 'Due Date', fieldName: 'SLADueDate', type: 'date-local'},
        {label: 'Created Date', fieldName: 'CreatedDate', type: 'date-local'}
    ]
    @track data;
    @track slaMetricData;
    @track error;
    @api objectName = 'Operations__c';
    @api fieldName = 'Name';
    @api selectRecordId = '';
    @api selectRecordName;
    @api Label = 'Operations';
    @api searchRecords = [];
    @api required = false;
    @api iconName = 'action:new_account'
    @api LoadingText = false;
    @track inputPlaceholder = 'Type Operation Name Here...'
    @track txtclassname = 'slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click';
    @track messageFlag = false;
    @track iconFlag =  true;
    @track clearIconFlag = false;
    @track inputReadOnly = false;   
    @track slaInput;
    @track valueInput;
    @track commentInput;
    @track metricDataLength;
    @api metricEntryData = [];

    valueChange(event) {
        let targetId = event.target.dataset.id;
        console.log("Target Id: "+targetId);
        let targetValue = event.target.value;
        console.log("Target Value: "+targetValue);
        this.metricEntryData.forEach(function(data) {
            if(data.Id === targetId) {
                data.Value = targetValue;
                console.log("value: "+data.Value);
            }
        })
    }

    commentChange(event) {
        let targetId = event.target.dataset.id;
        console.log("Target Id: "+targetId);
        let targetValue = event.target.value;
        console.log("Target Value: "+targetValue);
        this.metricEntryData.forEach(function(data) {
            if(data.Id === targetId) {
                data.Comments = targetValue;
                console.log("value: "+data.Comments);
            }
        })
    }

    searchField(event) {
        var currentText = event.target.value;
        this.LoadingText = true;
        
        findRecords({ ObjectName: this.objectName, fieldName: this.fieldName, value: currentText})
        .then(result => {
            this.searchRecords= result;
            this.LoadingText = false;
            
            this.txtclassname =  result.length > 0 ? 'slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click slds-is-open' : 'slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click';
            if(currentText.length > 0 && result.length == 0) {
                this.messageFlag = true;
            }
            else {
                this.messageFlag = false;
            }

            if(this.selectRecordId != null && this.selectRecordId.length > 0) {
                this.iconFlag = false;
                this.clearIconFlag = true;
            }
            else {
                this.iconFlag = true;
                this.clearIconFlag = false;
            }
        })
        .catch(error => {
            console.log('-------error-------------'+error);
            console.log(error);
        });
        
    }
    
    setSelectedRecord(event) {
        var currentRecId = event.currentTarget.dataset.id;
        var selectName = event.currentTarget.dataset.name;
        let entryData = [];
        this.txtclassname =  'slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click';
        this.iconFlag = false;
        this.clearIconFlag = true;
        this.selectRecordName = event.currentTarget.dataset.name;
        this.selectRecordId = currentRecId;
        this.inputReadOnly = true;
        getEmptySLAMetricRecords({operationId: currentRecId})
        .then(result => {
            console.log("Result: "+JSON.stringify(result));
            if(result) {
                this.data = result;
                result.forEach(function(record) {
                    console.log("Record: "+record.Id+" "+record.Name);
                    entryData.push({"Id": record.Id, "Name": record.SLA__r.Name, "Value": "", "Comments": ""});
                });
                this.metricEntryData = entryData;
            }
        });

        getSLAMetricRecords({operationId: currentRecId})
        .then(result => {
            console.log('SLA Metric Records: '+result);
            console.log('SLA Metric Data: '+JSON.stringify(result));
            this.metricDataLength = result.length;
            result.forEach(function(record){
                let targetMetric;
                let measureVal;
                if(record.SLA__r.Target_Metric__c == undefined) {
                    targetMetric = '';
                }
                else {
                    targetMetric = record.SLA__r.Target_Metric__c;
                }
                if(record.SLA__r.Unit_of_Measure__c == undefined) {
                    measureVal = '';
                }
                else {
                    measureVal = record.SLA__r.Unit_of_Measure__c;
                }
                record.SLAName = record.SLA__r.Name;
                record.SLATargetValueAndUnitOfMeasure = targetMetric+' '+measureVal;
                record.SLAReportingPeriod = record.SLA__r.Reporting_Period__c;
                if(record.Due_Date__c != undefined) {
                    record.SLADueDate = record.Due_Date__c;
                }
                console.log("Record's Due Date: "+record.SLADueDate);
            });
            this.slaMetricData = result;
        });

        const selectedEvent = new CustomEvent('selected', { detail: {selectName, currentRecId}, });
        // Dispatches the event.
        this.dispatchEvent(selectedEvent);
    }

    refreshTable(event) {
        getSLAMetricRecords({operationId: this.selectRecordId})
        .then(result => {
            this.metricDataLength = result.length;
            result.forEach(function(record){
                let targetMetric;
                let measureVal;
                if(record.SLA__r.Target_Metric__c == undefined) {
                    targetMetric = '';
                }
                else {
                    targetMetric = record.SLA__r.Target_Metric__c;
                }
                if(record.SLA__r.Unit_of_Measure__c == undefined) {
                    measureVal = '';
                }
                else {
                    measureVal = record.SLA__r.Unit_of_Measure__c;
                }
                record.SLAName = record.SLA__r.Name;
                record.SLATargetValueAndUnitOfMeasure = targetMetric+' '+measureVal;
                record.SLAReportingPeriod = record.SLA__r.Reporting_Period__c;
                if(record.Due_Date__c != undefined) {
                    record.SLADueDate = record.Due_Date__c;
                }
                console.log("Record's Due Date: "+record.SLADueDate);
            });
            this.slaMetricData = result;
        });
    }
    
    resetData(event) {
        this.selectRecordName = "";
        this.selectRecordId = "";
        this.inputReadOnly = false;
        this.iconFlag = true;
        this.clearIconFlag = false;
        getEmptySLAMetricRecords({operationId: this.selectRecordId})
        .then(result => {
            this.data = null;
        })
        getSLAMetricRecords({operationId: this.selectRecordId})
        .then(result => {
            this.metricDataLength = result.length;
            this.slaMetricData = "";
        })
    }

    handleCreate(event) {
        let entryData = [];
        console.log('Values: '+JSON.stringify(this.metricEntryData));
        this.metricEntryData.forEach(function(entryData) {
            console.log('Id: '+entryData.Id);
            console.log('Name: '+entryData.Name);
            console.log('Value: '+entryData.Value);
            console.log('Comments: '+entryData.Comments);
        });
        createSLAMetricRecord({
            operationId: this.selectRecordId,
            metricData: this.metricEntryData
        })
        .then(result => {
            console.log('RESULT: '+result);
            if(result) {
                const toastEvent = new ShowToastEvent({
                    title: 'SUCCESS!!',
                    message: 'SLA Metric Record Suuccessfully Created.',
                    variant: 'success'
                });
                this.dispatchEvent(toastEvent);
                getEmptySLAMetricRecords({operationId: this.selectRecordId})
                .then(result => {
                    console.log("Result: "+JSON.stringify(result));
                    if(result) {
                        this.data = result;
                        result.forEach(function(record) {
                            console.log("Record: "+record.Id+" "+record.Name);
                            entryData.push({"Id": record.Id, "Name": record.SLA__r.Name, "Value": "", "Comments": ""});
                        });
                        this.metricEntryData = entryData;
                    }
                });
            }
            else {
                const toastEvent = new ShowToastEvent({
                    title: 'ERROR!!',
                    message: 'Please fill correct details',
                    variant: 'error'
                });
                this.dispatchEvent(toastEvent);
            }
        });
    }
}
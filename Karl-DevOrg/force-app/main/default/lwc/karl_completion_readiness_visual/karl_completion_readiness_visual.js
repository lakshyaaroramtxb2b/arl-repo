import { LightningElement, api, track, wire } from 'lwc';
import { getRecord, getFieldValue } from 'lightning/uiRecordApi';
import { refreshApex } from '@salesforce/apex';
import { subscribe, unsubscribe, APPLICATION_SCOPE, MessageContext } from 'lightning/messageService';

/**
 * @author Ben Harvie
 * @email ben.harvie@salesforce.com
 * @description This LWC Component builds the Visual representation for Cycle Audit Report Readiness
 */

import reportUpdated from '@salesforce/messageChannel/KARL_Report_Updated__c';
import TEMPLATES_APPROVED from '@salesforce/schema/KARL_Cycle_Audit_Report_Items__c.Approved_by_Legal__c';
import TEMPLATES_FORMATTED from '@salesforce/schema/KARL_Cycle_Audit_Report_Items__c.Audit_Letter_Temps_Formatted__c';
import DRAFT_CONFIRMED from '@salesforce/schema/KARL_Cycle_Audit_Report_Items__c.Audit_Report_Final_Draft_Confirmed__c';
import BASISOF_ASSERTION from '@salesforce/schema/KARL_Cycle_Audit_Report_Items__c.Basis_of_Assertion_BOA__c';
import MGMT_RESPONSES from '@salesforce/schema/KARL_Cycle_Audit_Report_Items__c.Management_Responses_Approved__c';
import READINESS_CONFIRMATION from '@salesforce/schema/KARL_Cycle_Audit_Report_Items__c.Auditor_Readiness_Confirmation__c';

const fields = [
    TEMPLATES_APPROVED, 
    TEMPLATES_FORMATTED, 
    DRAFT_CONFIRMED,
    BASISOF_ASSERTION,
    MGMT_RESPONSES,
    READINESS_CONFIRMATION
];

export default class Karl_cyclereport_readiness extends LightningElement {
    @api recordId;
    @track records;
    @track cycleReport;

    successIcon     = 'utility:success';
    successColor    = 'success';
    incompleteIcon  = 'utility:warning';
    incompleteColor = 'warning';

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Wire the record information
     */
    @wire(getRecord, { recordId: '$recordId', fields }) wiredResult(result){
        console.log(JSON.stringify(result));
        if(result.data){
            this.cycleReport = result;
            this.records = true;
        }else if(result.error){
            console.log(result.error);
        }
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Getters for Letters Approved
     */
    get lettersApproved() {
        return getFieldValue(this.cycleReport.data, TEMPLATES_APPROVED) ? this.successIcon : this.incompleteIcon;
    }
    get lettersApprovedVariant() {
        return getFieldValue(this.cycleReport.data, TEMPLATES_APPROVED) ? this.successColor : this.incompleteColor;
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Getters for Templates Formatted
     */
    get templatesFormatted(){
        return getFieldValue(this.cycleReport.data, TEMPLATES_FORMATTED) ? this.successIcon : this.incompleteIcon;
    }
    get templatesFormattedVariant() {
        return getFieldValue(this.cycleReport.data, TEMPLATES_FORMATTED) ? this.successColor : this.incompleteColor;
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Getters for Draft Confirmed
     */
    get draftConfirmed() {
        return getFieldValue(this.cycleReport.data, DRAFT_CONFIRMED) ? this.successIcon : this.incompleteIcon;
    }
    get draftConfirmedVariant() {
        return getFieldValue(this.cycleReport.data, DRAFT_CONFIRMED) ? this.successColor : this.incompleteColor;
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Getters for Auditor Readiness
     */
    get auditorReadiness(){
        return getFieldValue(this.cycleReport.data, READINESS_CONFIRMATION) ? this.successIcon : this.incompleteIcon;
    }
    get auditorReadinessVariant() {
        return getFieldValue(this.cycleReport.data, READINESS_CONFIRMATION) ? this.successColor : this.incompleteColor;
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Getters for Management Responses
     */
    get managementResponses(){
        return getFieldValue(this.cycleReport.data, MGMT_RESPONSES) ? this.successIcon : this.incompleteIcon;
    }
    get managementResponsesVariant() {
        return getFieldValue(this.cycleReport.data, MGMT_RESPONSES) ? this.successColor : this.incompleteColor;
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Getters for Basis of Assertion
     */
    get basisOfAssertion(){
        return getFieldValue(this.cycleReport.data, BASISOF_ASSERTION) ? this.successIcon : this.incompleteIcon;
    }
    get basisOfAssertionVariant() {
        return getFieldValue(this.cycleReport.data, BASISOF_ASSERTION) ? this.successColor : this.incompleteColor;
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Message subscription service (for refreshApex)
     */
    @wire(MessageContext)
    messageContext;

    // Encapsulate logic for Lightning message service subscribe and unsubsubscribe
    subscribeToMessageChannel() {
        if (!this.subscription) {
            this.subscription = subscribe(
                this.messageContext,
                reportUpdated,
                (message) => this.handleMessage(message),
                { scope: APPLICATION_SCOPE }
            );
        }
    }

    unsubscribeToMessageChannel() {
        unsubscribe(this.subscription);
        this.subscription = null;
    }

    // Handler for message received by component
    handleMessage(message) {
        this.handleRefresh(message);
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Callbacks to load the message channel
     */
    connectedCallback() {
        this.subscribeToMessageChannel();
    }
    disconnectedCallback() {
        this.unsubscribeToMessageChannel();
    }

	/** 
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @desc Refresh apex to capture changes made upstream
	 * @param event - js event handler
     */
	handleRefresh(event){
		refreshApex(this.cycleReport);
	}

}
import { LightningElement, api } from 'lwc';
// Import custom labels
import processing from '@salesforce/label/c.ESA_Processing';

export default class BusySpinner extends LightningElement {
    label = {
        processing
    }
    @api
    isBusy = false;

    @api
    showBusy() {
        this.isBusy = true;
    }

    @api 
    hideBusy() {
        this.isBusy = false;
    }

}
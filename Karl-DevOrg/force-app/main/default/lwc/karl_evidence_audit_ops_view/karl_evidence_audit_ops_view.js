import {
    LightningElement,
    track,
    api
} from 'lwc';
import {
    ShowToastEvent
} from 'lightning/platformShowToastEvent';
import {
    NavigationMixin
} from 'lightning/navigation';
import getEvidenceContentVersionRecords from '@salesforce/apex/KARL_EvidenceUploadController.getRecordsOnlayout';
import saveContentVersionRecords from '@salesforce/apex/KARL_EvidenceUploadController.saveContentVersionRecords';
import submitProvidedToAuditor from '@salesforce/apex/KARL_EvidenceUploadController.submitProvidedToAuditor';
import submitReturnToEngineer from '@salesforce/apex/KARL_EvidenceUploadController.submitReturnToEngineer';
import revokeFromAuditor from '@salesforce/apex/KARL_EvidenceUploadController.revokeFromAuditor';
import deleteFile from '@salesforce/apex/KARL_EvidenceUploadController.deleteFile';
import getContentDistributionForFileOrg from '@salesforce/apex/KARL_EvidenceUploadController.getContentDistributionForFileOrg';
import fetchPicklist from '@salesforce/apex/KARL_Utility.fetchPicklist';
import {
    showAjaxErrorMessage
}
from 'c/karlUtil';

export default class Karl_evidence_audit_ops_view extends NavigationMixin(LightningElement) {
    @api recordId;

    @track data = [];
    @track isTableEmpty = false;
    @track submitted = false;
    @track providedToAuditor = false;
    @track statusOptions = [];
    @track showSpinner = false;
    @track showProvideToAudModal = false;
    @track showReturnEngineerModal = false;
    @track showRevokeFromAudModal = false;
    @track isDisabled = true;
    @track bannerText;
    @track deleteFileId;
    @track showDeleteModal = false;

    provideToAuditorConfirmationMsg = 'Are you sure you want to provide the evidence to the Auditors ?';
    returnToEngineerConfirmationMsg = 'Are you sure you want to return the evidence to the Engineer ?';
    revokeFromAuditorConfirmationMsg = 'Are you sure you want to pull back the evidence request from the Auditors ?';
    deleteFileConfirmationMsg = 'Are you sure you want to delete the evidence attachment ? ';

    connectedCallback() {
        let fullUrl = window.location.href;
        let newURL = new URL(fullUrl).searchParams;
        if (!this.recordId) {
            this.recordId = newURL.get('id');
        }

        this.showSpinner = true;
        fetchPicklist({
                objectName: 'ContentVersion',
                fieldName: 'Status__c'
            })
            .then(result => {
                this.statusOptions = result;
            })
            .catch(error => {
                console.log('errorrr picklist>' + JSON.stringify(error));
                showAjaxErrorMessage(this, error);
            }).finally(() => {
                this.showSpinner = false;
            });
        this.loadColumns();
        this.refreshTableData();
    }

    /**
     * @author Mahima Aggarwal
     * @email mahima.aggarwal@mtxb2b.com
     * @desc This method loads columns
     */
    loadColumns() {
        this.columnConfiguration = [];

        this.columnConfiguration.push({
            heading: 'ATTACHMENT',
            fieldApiName: 'Attachment"',
            style: 'width:20%;'
        });
        this.columnConfiguration.push({
            heading: 'EVIDENCE NOTES',
            fieldApiName: 'Evidence_Notes',
            style: 'width:20%;'
        });
        this.columnConfiguration.push({
            heading: 'GRC NOTES',
            fieldApiName: 'Assignee_Notes',
            style: 'width:20%;'
        });
        this.columnConfiguration.push({
            heading: 'STATUS',
            fieldApiName: 'Status',
            style: 'width:20%;'
        });
        this.columnConfiguration.push({
            heading: 'UPLOADED BY',
            fieldApiName: 'Uploaded_By',
            style: 'width:20%;'
        });


    }

    /**
     * @author Mahima Aggarwal
     * @email mahima.aggarwal@mtxb2b.com
     * @desc This method refresh data
     */
    refreshTableData() {
        this.showSpinner = true;
        console.log('refreshTableDataCalled>>' + this.recordId);
        this.data = [];
        getEvidenceContentVersionRecords({
                evidenceId: this.recordId
            })
            .then(result => {
                if(!result){
                    return;
                }
                if(result.fileWrapperList){
                    this.data = result.fileWrapperList;
                }
                    
                    //console.log('data' + JSON.stringify(this.data));
                    if(result.fileWrapperList){
                        this.isTableEmpty = result.fileWrapperList.length === 0 ? true : false;
                    }
                    if(result.submitted){
                        this.submitted = result.submitted;
                    }
                    
                    this.providedToAuditor = result.providedToAuditor;
                    this.isDisabled = (!this.submitted) || this.providedToAuditor;
                    if (this.isDisabled) {
                        if (!this.submitted) {
                            this.bannerText = 'You cannot edit yet, as the evidence has not yet been submitted by Engineer';
                        } else if (this.providedToAuditor) {
                            this.bannerText = 'You cannot edit, as the evidence has been Provided to Auditor. Revoke first.';
                        }
                    }
                    let i = 0;
                    for (i = 0; i < this.data.length; i++) {
                        this.data[i].showNameEdit = false;
                        this.data[i].showDeleteIcon = (this.data[i].status != 'Approved') ? true : false;
                    }
                    //this.showPageOnLoad = true;
            })
            .catch(error => {
                console.log("error occured>> " + JSON.stringify(error));
                showAjaxErrorMessage(this, error);
            }).finally(() => {
                this.showSpinner = false;
            });
    }

    /**
     * @author Mahima Aggarwal
     * @email mahima.aggarwal@mtxb2b.com
     * @desc This method handles save of File to database
     */
    handleSaveClick(event) {
        console.log('data>' + JSON.stringify(this.data));
        saveContentVersionRecords({
                jsonMap: JSON.stringify(this.data)
            })
            .then(result => {
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Success',
                        message: 'Files Saved',
                        variant: 'success'
                    })
                );
                this.refreshTableData();
            })
            .catch(error => {
                console.log("error occured>> " + JSON.stringify(error));
                showAjaxErrorMessage(this, error);
            });
    }

    /**
     * @author Mahima Aggarwal
     * @email mahima.aggarwal@mtxb2b.com
     * @desc This method handles save of File and marking Cycle request item "Provided to Auditor" to database
     */
    handleProvidedToAuditor(event) {
        console.log('data>' + JSON.stringify(this.data));
        submitProvidedToAuditor({
                jsonMap: JSON.stringify(this.data),
                evidenceId: this.recordId
            })
            .then(result => {
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Success',
                        message: 'Evidence Successfully Provided To Auditor',
                        variant: 'success'
                    })
                );
                this.showProvideToAudModal = false;
                this.refreshTableData();
            })
            .catch(error => {
                this.showProvideToAudModal = false;
                console.log("error occured>> " + JSON.stringify(error));
                showAjaxErrorMessage(this, error);
            });
    }

    /**
     * @author Mahima Aggarwal
     * @email mahima.aggarwal@mtxb2b.com
     * @desc This method opens Provide to Audtor modal
     */
    openProvideToAudModal(event) {
        this.showProvideToAudModal = true;
    }

    /**
     * @author Mahima Aggarwal
     * @email mahima.aggarwal@mtxb2b.com
     * @desc This method closes Provide to Audtor modal
     */
    closeProvideToAudModal() {
        this.showProvideToAudModal = false;
    }

    /**
     * @author Mahima Aggarwal
     * @email mahima.aggarwal@mtxb2b.com
     * @desc This method handles marking Cycle request item "Ready for Review"
     to database
     */
    handleRevokeFromAuditor(event) {
        revokeFromAuditor({
                evidenceId: this.recordId
            })
            .then(result => {
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Success',
                        message: 'Evidence Successfully Revoked from Auditor',
                        variant: 'success'
                    })
                );
                this.showRevokeFromAudModal = false;
                this.refreshTableData();
            })
            .catch(error => {
                this.showRevokeFromAudModal = false;
                console.log("error occured>> " + JSON.stringify(error));
                showAjaxErrorMessage(this, error);
            });
    }

    /**
     * @author Mahima Aggarwal
     * @email mahima.aggarwal@mtxb2b.com
     * @desc This method opens Revoke from Auditor modal
     */
    openRevokeFromAudModal(event) {
        this.showRevokeFromAudModal = true;
    }

    /**
     * @author Mahima Aggarwal
     * @email mahima.aggarwal@mtxb2b.com
     * @desc This method closes Revoke from Auditor modal
     */
    closeRevokeFromAudModal() {
        this.showRevokeFromAudModal = false;
    }

    /**
     * @author Mahima Aggarwal
     * @email mahima.aggarwal@mtxb2b.com
     * @desc This method handles save of File and marking Evidence to unsubmitted
     */
    handleReturnToEngineer(event) {
        console.log('data>' + JSON.stringify(this.data));
        submitReturnToEngineer({
                jsonMap: JSON.stringify(this.data),
                evidenceId: this.recordId
            })
            .then(result => {
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Success',
                        message: 'Evidence Successfully Returned To Engineer',
                        variant: 'success'
                    })
                );
                this.showReturnEngineerModal = false;
                this.refreshTableData();
            })
            .catch(error => {
                this.showReturnEngineerModal = false;
                console.log("error occured>> " + JSON.stringify(error));
                showAjaxErrorMessage(this, error);
            });
    }

    /**
     * @author Mahima Aggarwal
     * @email mahima.aggarwal@mtxb2b.com
     * @desc This method opens Return To Engineer modal
     */
    openReturnEngineerModal(event) {
        this.showReturnEngineerModal = true;
    }

    /**
     * @author Mahima Aggarwal
     * @email mahima.aggarwal@mtxb2b.com
     * @desc This method closes Return To Engineer modal
     */
    closeReturnEngineerModal() {
        this.showReturnEngineerModal = false;
    }

    /**
     * @author Mahima Aggarwal
     * @email mahima.aggarwal@mtxb2b.com
     * @desc This method handles File fields change
     */
    handleChange(event) {
        var indexClicked = event.currentTarget.getAttribute("data-index");
        if (event.target.name == 'status') {
            this.data[indexClicked].status = event.detail.value;
        } else if (event.target.name == 'evidenceNotes') {
            this.data[indexClicked].evidenceNotes = event.target.value;
        } else if (event.target.name == 'assigneeNotes') {
            this.data[indexClicked].assigneeNotes = event.target.value;
        } else if (event.target.name == 'docName') {
            this.data[indexClicked].docName = event.target.value;
            this.data[indexClicked].showNameEdit = false;
        }
    }


    /**
     * @author Mahima Aggarwal
     * @email mahima.aggarwal@mtxb2b.com
     * @desc This method shows preview of file that is uploaded
     */
    handleDownloadFile(event) {
        getContentDistributionForFileOrg({
                contentDocumentId: event.currentTarget.dataset.id
            })
            .then(response => {
                this[NavigationMixin.Navigate]({
                    type: 'standard__webPage',
                    attributes: {
                        url: response.DistributionPublicUrl
                    },
                });
                //window.open(response.DistributionPublicUrl);
            })
            .catch(error => {
                console.log('error in file open>>' + JSON.stringify(error));
                showAjaxErrorMessage(this, error);
            })
    }

    /**
     * @author Mahima Aggarwal
     * @email mahima.aggarwal@mtxb2b.com
     * @desc This method shows text field to edit the file name
     */
    handleEditFileName(event) {
        var indexClicked = event.currentTarget.getAttribute("data-index");
        this.data[indexClicked].showNameEdit = true;
    }

    //Delete Functioanlity

    openDeleteModal(event) {
        this.deleteFileId = event.currentTarget.dataset.id
        this.showDeleteModal = true;
    }

    closeDeleteModal() {
        this.deleteFileId = '';
        this.showDeleteModal = false;
    }

    deleteFile(event) {
        console.log('File to delete: ' + this.deleteFileId);
        console.log('Record to delete: ' + this.recordId);
        deleteFile({
            recordId: this.deleteFileId,
            evidenceId: this.recordId
        }).then(result => {
            this.showDeleteModal = false;
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Success',
                    message: 'File successfully deleted',
                    variant: 'success'
                })
            );
            this.refreshTableData();
        }).catch(error => {
            this.showDeleteModal = false;
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Error',
                    message: 'Error occurred while deleting record.',
                    variant: 'error'
                })
            );
        })
    }
}
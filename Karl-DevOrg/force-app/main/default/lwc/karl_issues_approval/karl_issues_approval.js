import { LightningElement, api, track, wire } from 'lwc';
import { getRecord, getFieldValue, getFieldDisplayValue } from 'lightning/uiRecordApi';
import { refreshApex } from '@salesforce/apex';

/**
 * @author Ben Harvie
 * @email ben.harvie@salesforce.com
 * @description This LWC Component builds the Visual representation for Cycle Audit Report Readiness
 */

import APPROVED_GRCLT from '@salesforce/schema/KARL_Issue_Tracker__c.KARL_Approved_by_Orchestration_Lead__c';
import APPROVED_SCCS from '@salesforce/schema/KARL_Issue_Tracker__c.KARL_Approved_by_SCCS__c';
import APPROVED_GRCVP from '@salesforce/schema/KARL_Issue_Tracker__c.KARL_Approved_by_GRC_Executive__c';
import APPROVER_GRCLT from '@salesforce/schema/KARL_Issue_Tracker__c.KARL_KAI_Approver_GRCLT__r.Name';
import APPROVER_SCCS from '@salesforce/schema/KARL_Issue_Tracker__c.KARL_KAI_Approver_SCCS__r.Name';
import APPROVER_GRCVP from '@salesforce/schema/KARL_Issue_Tracker__c.KARL_KAI_Approver_VP_Approver__r.Name';
import Issue_Status__c from '@salesforce/schema/KARL_Issue_Tracker__c.Issue_Status__c';

const fields = [
    APPROVED_GRCLT, 
    APPROVED_SCCS, 
    APPROVED_GRCVP,
    APPROVER_GRCLT, 
    APPROVER_SCCS,
    APPROVER_GRCVP,
    Issue_Status__c
];

export default class Karl_cyclereport_readiness extends LightningElement {
    @api recordId;
    @track records;
    @track auditIssue;

    successIcon     = 'utility:success';
    successColor    = 'success';
    incompleteIcon  = 'utility:warning';
    incompleteColor = 'warning';

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Wire the record information
     */
    @wire(getRecord, { recordId: '$recordId', fields }) wiredResult(result){
        console.log(JSON.stringify(result));
        if(result.data){
            this.auditIssue = result;
            this.records = true;
        }else if(result.error){
            console.log(result.error);
        }
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Getters for GRC LT Approval
     */
    get grcltApproval() {
        return getFieldValue(this.auditIssue.data, APPROVED_GRCLT) ? this.successIcon : this.incompleteIcon;
    }
    get grcltApprover() {
        return getFieldValue(this.auditIssue.data, APPROVER_GRCLT);
    }
    get grcltApprovalVariant() {
        return getFieldValue(this.auditIssue.data, APPROVED_GRCLT) ? this.successColor : this.incompleteColor;
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Getters for Templates Formatted
     */
    get sccsApproval(){
        return getFieldValue(this.auditIssue.data, APPROVED_SCCS) ? this.successIcon : this.incompleteIcon;
    }
    get sccsApprover() {
        return getFieldValue(this.auditIssue.data, APPROVER_SCCS);
    }
    get sccsApprovalVariant() {
        return getFieldValue(this.auditIssue.data, APPROVED_SCCS) ? this.successColor : this.incompleteColor;
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Getters for Draft Confirmed
     */
    get grcvpApproval() {
        return getFieldValue(this.auditIssue.data, APPROVED_GRCVP) ? this.successIcon : this.incompleteIcon;
    }
    get grcvpApprover() {
        return getFieldValue(this.auditIssue.data, APPROVER_GRCVP);
    }
    get grcvpApprovalVariant() {
        return getFieldValue(this.auditIssue.data, APPROVED_GRCVP) ? this.successColor : this.incompleteColor;
    }

	/** 
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @desc Refresh apex to capture changes made upstream
	 * @param event - js event handler
     */
	handleRefresh(event){
		refreshApex(this.cycleReport);
	}

}
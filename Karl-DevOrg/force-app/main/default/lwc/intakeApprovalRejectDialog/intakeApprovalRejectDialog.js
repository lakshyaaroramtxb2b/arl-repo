import { LightningElement, track, wire } from 'lwc';
import { CurrentPageReference } from 'lightning/navigation';
import approveRequest from '@salesforce/apex/IntakeBulkApprovalController.approveRequest';
import rejectRequest from '@salesforce/apex/IntakeBulkApprovalController.rejectRequest';
import { registerListener, unregisterAllListeners } from 'c/pubsub';
import { fireEvent } from 'c/pubsub';

// Import custom labels
import comment from '@salesforce/label/c.ESA_Comment';
import approve from '@salesforce/label/c.ESA_Approve';
import reject from '@salesforce/label/c.ESA_Reject';

export default class IntakeApprovalRejectDialog extends LightningElement {
    // Expose label for use in templates
    label = {
        comment, approve, reject
    }

    @track
    actionConfig = {variant : 'brand', label : this.label.approve}
    @track
    isApprovalFlow
    @track
    approvalInfo
    @track
    userComment
    @track 
    approvalRequest

    @wire(CurrentPageReference) pageRef;

    showBusy() {
        fireEvent(this.pageRef, 'showBusy');
    }

    hideBusy() {
        fireEvent(this.pageRef, 'hideBusy');
    }


    show(error) {
        this.template.querySelector("c-slds-modal").show(error, 'error');
    }

    hide() {
        this.template.querySelector("c-slds-modal").hide();
    }

	connectedCallback() {
        registerListener('approveRequest', this.prepareApproveRequest, this);
        registerListener('rejectRequest', this.prepareRejectRequest, this);
	}
	disconnectedCallback() {
		// unsubscribe
		unregisterAllListeners(this);
    }

    updateComment(event) {
        this.userComment = event.target.value;
    }

    prepareAndShow(approvalRequest, isApprovalFlow) {
        this.isApprovalFlow = isApprovalFlow;
        this.approvalRequest = approvalRequest;
        this.approvalInfo = approvalRequest.securityRequest.Approver_Info__c;
        this.clean();
        if (isApprovalFlow) {
            this.actionConfig.label = this.label.approve;
            this.actionConfig.variant = "brand";
        } else {
            this.actionConfig.label = this.label.reject;
            this.actionConfig.variant = "destructive";
        }
        this.show();
    }
    
    prepareApproveRequest(approvalRequest) {
        this.prepareAndShow(approvalRequest, true);
    }

    prepareRejectRequest(approvalRequest) {
        this.prepareAndShow(approvalRequest, false);
    }

    fireUpdateApprovalRequests() {
        fireEvent(this.pageRef, "updateApprovalRequests", this.approvalRequest.processWorkItem.Id);
    }

    clean() {
        this.userComment = '';
        this.error = '';
    }

    processAction() {
        this.showBusy();
        if (this.isApprovalFlow) {
            this.approveRequest();
        } else {
            this.rejectRequest();
        }
        this.hide();
    }

    approveRequest() {
        approveRequest({'comment': this.userComment, 'id': this.approvalRequest.processWorkItem.Id}) 
			.then(() => {
                this.fireUpdateApprovalRequests();
			})
			.catch((error) => {
                this.hideBusy();
                this.show(error);
            })
    }

    rejectRequest() {
        rejectRequest({'comment': this.userComment, 'id': this.approvalRequest.processWorkItem.Id}) 
			.then(() => {
                this.fireUpdateApprovalRequests();
			})
			.catch((error) => {
                this.hideBusy();
				this.show(error);
			}) 
    }



}
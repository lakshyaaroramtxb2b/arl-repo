import { LightningElement, track, wire, api } from 'lwc';
import getEmptySLAMetricRecords from '@salesforce/apex/PRT_SLAMetricCreationController.getEmptySLAMetricRecords';
import createSLAMetricRecord from '@salesforce/apex/PRT_SLAMetricCreationController.createSLAMetricRecord';
import getSLAMetricRecords from '@salesforce/apex/PRT_SLAMetricCreationController.getSLAMetricRecords'; 
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class Prt_MetricEntry_RecordPage extends LightningElement {
    @track slaMetricColumns = [
        {label: 'SLA', fieldName: 'SLAName', type: 'text'},
        {label: 'Value', fieldName: 'Value__c', type: 'number', cellAttributes: { alignment: 'left' }},
        {label: 'Target Value and Unit of Measure', fieldName: 'SLATargetValueAndUnitOfMeasure', type: 'text', cellAttributes: { alignment: 'left' }},
        {label: 'Comment', fieldName: 'Comment__c', type: 'text'},
        {label: 'Reporting Period', fieldName: 'SLAReportingPeriod', type: 'text'},
        {label: 'Due Date', fieldName: 'SLADueDate', type: 'date-local'},
        {label: 'Created Date', fieldName: 'CreatedDate', type: 'date-local'}
    ]
    @track isInserted;
    @track slaMetricData;
    @api recordId;
    @track error;
    @track data;
    @track slaInput;
    @track valueInput;
    @track commentInput;
    @track currenRecordId;
    @track metricDataLength;
    @api metricEntryData = [];

    connectedCallback() {
        let entryData = [];
        console.log('Record Id: '+this.recordId);
        getEmptySLAMetricRecords({operationId: this.recordId})
        .then(result => {
            console.log('SLA Records: '+result);
            this.data = result;
            if(result) {
                this.data = result;
                result.forEach(function(record) {
                    console.log("Record: "+record.Id+" "+record.Name);
                    entryData.push({"Id": record.Id, "Name": record.SLA__r.Name, "Value": "", "Comments": ""});
                });
                this.metricEntryData = entryData;
            }
        })

        getSLAMetricRecords({operationId: this.recordId})
        .then(result => {
            this.metricDataLength = result.length;
            console.log('SLA Metric Records: '+result);
            result.forEach(function(record){
                let targetMetric;
                let measureVal;
                if(record.SLA__r.Target_Metric__c == undefined) {
                    targetMetric = '';
                }
                else {
                    targetMetric = record.SLA__r.Target_Metric__c;
                }
                if(record.SLA__r.Unit_of_Measure__c == undefined) {
                    measureVal = '';
                }
                else {
                    measureVal = record.SLA__r.Unit_of_Measure__c;
                }
                record.SLAName = record.SLA__r.Name;
                record.SLATargetValueAndUnitOfMeasure = targetMetric+' '+measureVal;
                record.SLAReportingPeriod = record.SLA__r.Reporting_Period__c;
                if(record.Due_Date__c != undefined) {
                    record.SLADueDate = record.Due_Date__c;
                }
            });
            this.slaMetricData = result;
        })
    }

    valueChange(event) {
        let targetId = event.target.dataset.id;
        console.log("Target Id: "+targetId);
        let targetValue = event.target.value;
        console.log("Target Value: "+targetValue);
        this.metricEntryData.forEach(function(data) {
            if(data.Id === targetId) {
                data.Value = targetValue;
                console.log("value: "+data.Value);
            }
        })
    }

    commentChange(event) {
        let targetId = event.target.dataset.id;
        console.log("Target Id: "+targetId);
        let targetValue = event.target.value;
        console.log("Target Value: "+targetValue);
        this.metricEntryData.forEach(function(data) {
            if(data.Id === targetId) {
                data.Comments = targetValue;
                console.log("value: "+data.Comments);
            }
        })
    }

    refreshTable(event) {
        getSLAMetricRecords({operationId: this.recordId})
        .then(result => {
            this.metricDataLength = result.length;
            result.forEach(function(record){
                let targetMetric;
                let measureVal;
                if(record.SLA__r.Target_Metric__c == undefined) {
                    targetMetric = '';
                }
                else {
                    targetMetric = record.SLA__r.Target_Metric__c;
                }
                if(record.SLA__r.Unit_of_Measure__c == undefined) {
                    measureVal = '';
                }
                else {
                    measureVal = record.SLA__r.Unit_of_Measure__c;
                }
                record.SLAName = record.SLA__r.Name;
                record.SLATargetValueAndUnitOfMeasure = targetMetric+' '+measureVal;
                record.SLAReportingPeriod = record.SLA__r.Reporting_Period__c;
                if(record.Due_Date__c != undefined) {
                    record.SLADueDate = record.Due_Date__c;
                }
            });
            this.slaMetricData = result;
        });
    }

    handleCreate(event) {
        let entryData = [];
        console.log('Values: '+JSON.stringify(this.metricEntryData));
        this.metricEntryData.forEach(function(entryData) {
            console.log('Id: '+entryData.Id);
            console.log('Name: '+entryData.Name);
            console.log('Value: '+entryData.Value);
            console.log('Comments: '+entryData.Comments);
        });
        createSLAMetricRecord({
            operationId: this.recordId, 
            metricData: this.metricEntryData
        })
        .then(result => {
            console.log('RESULT: '+result);
            if(result) {
                const toastEvent = new ShowToastEvent({
                    title: 'SUCCESS!!',
                    message: 'SLA Metric Record Suuccessfully Created.',
                    variant: 'success'
                });
                this.dispatchEvent(toastEvent);
                getEmptySLAMetricRecords({operationId: this.recordId})
                .then(result => {
                    console.log('SLA Records: '+result);
                    this.data = result;
                    if(result) {
                        this.data = result;
                        result.forEach(function(record) {
                            console.log("Record: "+record.Id+" "+record.Name);
                            entryData.push({"Id": record.Id, "Name": record.SLA__r.Name, "Value": "", "Comments": ""});
                        });
                        this.metricEntryData = entryData;
                    }
                })
            }
            else {
                const toastEvent = new ShowToastEvent({
                    title: 'ERROR!!',
                    message: 'Please fill correct details',
                    variant: 'error'
                });
                this.dispatchEvent(toastEvent);
            }
        });
    }
}
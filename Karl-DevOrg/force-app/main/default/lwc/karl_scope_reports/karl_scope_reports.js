import { NavigationMixin } from 'lightning/navigation';
import { LightningElement, api, wire, track } from 'lwc';
import { deleteRecord } from 'lightning/uiRecordApi';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { refreshApex } from '@salesforce/apex';
import KARL_Superuser from '@salesforce/customPermission/KARL_Superuser';

/**
 * @author Ben Harvie
 * @email ben.harvie@salesforce.com
 * @desc This LWC Component Builds Report Cards on the Audit Scope Management object.
 */

// Import Apex Classes
import getReportsData from '@salesforce/apex/KARL_Utility.getPrimaryReports';
import getRelatedData from '@salesforce/apex/KARL_Utility.getRelatedReports';

export default class Karl_scope_reports extends NavigationMixin(LightningElement) {
    @api objectApiName;
    @api recordId;
    
    // Metadata variables
    @api tileName;
    @api associationType;
    @api cardButton;
    @api cardIcon;

    // LWC Processing Variables
    @track parentId;
	@track primaryRecords;
	@track relatedRecords;
	@track error;
    @track showDeleteModal = false;
    @track showCreateModal = false;
    @track showUpdateModal = false;
    @track deleteRecordId;
    @track reportsData = [];
    @track relatedData = [];
    @track reportsWireData;
    @track relatedWireData;
    @track recordTarget;

    deleteRecordConfirmationMsg = 'Are you sure you want to delete the certification report record?';

    /** 
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @desc Wire the Certification Reports
     */
	@wire(getReportsData, {recordId: '$recordId'}) wiredPrimary(result) {
        this.reportsWireData = result;

        console.log("Primary Reports Data feed updating!");

		if ( result.data && result.data.length ){
			// ! Check if the wire returns any data
			// Error handle the response just in case there is an issue returning the data
			try {
				this.primaryRecords = true; // specify boolean for if:true
				this.reportsData    = result.data; // establish the data for reportsData
				this.error          = false;
			} catch(e) {
				console.log('KARL LWC Error', result.error)
			}
		} else if ( result.data && !result.data.length ){
			// ! Check if the wire is null
			this.primaryRecords = false; // specify boolean for if:false
            this.reportsData    = result.data; // establish the data for reportsData
            console.log('KARL LWC Empty', result.data)
		} else if (result.error){
			// ! Check if the wire returns an error, and return the error
			console.log('KARL LWC Error', result.error)
			this.error = result.error;
			this.primaryRecords = false;
		}
    }

    /** 
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @desc Wire the Included in Certification Reports
     */
	@wire(getRelatedData, {recordId: '$recordId', associationType: '$associationType', objectApi: '$objectApiName'}) wiredRelated(result) {
        this.relatedWireData = result;

        console.log("Related Reports Data feed updating!");

		if ( result.data && result.data.length ){
			// ! Check if the wire returns any data
			// Error handle the response just in case there is an issue returning the data
			try {
				this.relatedRecords = true; // specify boolean for if:true
				this.relatedData    = result.data; // establish the data for Parent Reports
				this.error          = false;
			} catch(e) {
				console.log('KARL LWC Error', result.error)
			}
		} else if ( result.data && !result.data.length ){
			// ! Check if the wire is null
			this.relatedRecords = false; // specify boolean for if:false
            this.relatedData    = result.data;
            console.log('KARL LWC Empty', result.data)
		} else if (result.error){
			// ! Check if the wire returns an error, and return the error
			console.log('KARL LWC Error', result.error)
			this.error = result.error;
			this.relatedRecords = false;
		}
    }

    /** 
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @desc Handle refresh
     */
    handleRefresh(event){
        refreshApex(this.reportsWireData);
        refreshApex(this.relatedWireData);
    }

    /** 
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @desc Open modal for editing
     */
    openCreateModal(event) {
        // Process information to the Modal
        this.showCreateModal = true;
    }

    /** 
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @desc Close the modal
     */
    closeCreateModal() {
        this.showCreateModal = false;
    }

    /** 
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @desc Handle creation from modal
     */
    handleCreateSubmit(event){
        this.showCreateModal = false;

        refreshApex(this.reportsWireData);
        refreshApex(this.relatedWireData);

        this.dispatchEvent(
            new ShowToastEvent({
                title: 'Success',
                message: 'Report mapped successfully.',
                variant: 'success'
            })
        )
    }

    /** 
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @desc Standard handleRecordView component for KARL LWC's to navigate to Record Pages
     */
	handleRecordView(event) {
		// Get the Target Record ID from the event (typically onClick event)
		const targetRecordId = event.detail;
		// Navigate to the record page
		this[NavigationMixin.Navigate]({
			type: 'standard__recordPage',
			attributes: {
				recordId: targetRecordId,
				actionName: 'view',
			},
		});
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @desc Open/Close modals for popup handling
     */
    openUpdateModal(event) {
        // Process information to the Modal
        this.recordTarget = event.detail;
        this.showUpdateModal = true;
    }

    closeUpdateModal() {
        this.showUpdateModal = false;
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @desc Handle submission of the control mapping form (modal)
     */
    handleUpdateSubmit(event){
        this.showUpdateModal = false;
        
        refreshApex(this.reportsWireData);
        refreshApex(this.relatedWireData);

        this.dispatchEvent(
            new ShowToastEvent({
                title: 'Success',
                message: 'Control mapping updated successfully.',
                variant: 'success'
            })
        )
    }

    /** 
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @desc Delete modals (popups)
     */
    openDeleteModal(event) {
        // Process information to the Modal
        this.deleteRecordId = event.detail;
        this.showDeleteModal = true;

        // Debug Log
        console.log("Record to delete: " + this.deleteRecordId);
    }

    closeDeleteModal() {
        this.showDeleteModal = false;
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @desc Handle deletion of the record from the picklist
     */
    deleteRecord(event) {      
        deleteRecord(this.deleteRecordId)
            .then(() => {
                this.showDeleteModal = false;

                refreshApex(this.reportsWireData);
                refreshApex(this.relatedWireData);

                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Success',
                        message: 'Record deleted',
                        variant: 'success'
                    })
                )
            })
            .catch(error => {
                this.showDeleteModal = false;
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Error deleting record',
                        message: error.body.message,
                        variant: 'error'
                    })
                )
            })
    }

    /** 
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @desc Return true if the user has the Custom Permission for KARL_Superuser
     * Returns undefined if not set.
     */
    get isKarlAdmin(){
        //console.log(' Is KARL Superuser? >> ' + KARL_Superuser);
        return KARL_Superuser;
    }

    /** 
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @desc Return true if associationType is Primary
     */
    get isPrimary(){
        if(this.associationType == 'Primary'){
            return true;
        }else{
            return false;  
        }
    }

    /** 
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @desc Return true if associationType is Parent
     */
    get isParent(){
        if(this.associationType == 'Parent'){
            return true;
        }else{
            return false;  
        }
    }

    /** 
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @desc Return true if associationType is Child
     */
    get isChild(){
        if(this.associationType == 'Child'){
            return true;
        }else{
            return false;  
        }
    }

    /** 
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @desc Return true if associationType is Child
     */
    get isChildMaster(){
        if(this.associationType == 'Child' && this.objectApiName === 'KARL_Audit_Scope_Master_Data__c'){
            return true;
        }else{
            return false;  
        }
    }

}
import { LightningElement, api } from 'lwc';

/**
 * @author Ben Harvie
 * @email ben.harvie@salesforce.com
 * @desc This LWC Component is a re-usable Lightning Card for text and icons (based on Lightning Page Editor configuration)
 */

export default class Karl_warning extends LightningElement {
    @api textField;
    @api iconDisplay;
    @api iconVariant;
    @api warningTheme;

    get warningClass(){
        let boxTheme = this.warningTheme;
        if(boxTheme === undefined){
            boxTheme = 'slds-theme_warning';
        }
        return 'slds-box ' + boxTheme;
    }

    get iconClass(){
        let iconTheme = this.iconVariant;
        if(iconTheme === undefined){
            iconTheme = 'base';
        }
        return iconTheme;
    }
}
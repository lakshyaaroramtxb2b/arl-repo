import { LightningElement, api, track } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import ARL_Is_External_Auditor from '@salesforce/customPermission/ARL_Is_External_Auditor';
import KARL_Superuser from '@salesforce/customPermission/KARL_Superuser';
import KARL_Audit_Manager from '@salesforce/customPermission/KARL_Audit_Manager';

/**
 * @author Ben Harvie
 * @email ben.harvie@salesforce.com
 * @description This LWC Component Builds the tiles as a part of the Controls Mapping on REQ, CREQ and ARI lightning pages
 */
export default class Karl_mapping_controls_tile extends NavigationMixin(LightningElement) {
    @api recordId;
    @api controlitem;
    @api controlsData;
    @track recordPageUrl;
    @track error;
    @track targetrecord;

    /** 
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Standard Connected Callback
     */
    connectedCallback() {
        // Targeting the Test__c in this instance (instead of the junction object)
        this[NavigationMixin.GenerateUrl]({
            type: 'standard__recordPage',
            attributes: {
                recordId: this.controlitem.Test__c,
                actionName: 'view',
            },
        }).then(url => {
            this.recordPageUrl = url;
        });
    }

    /** 
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Create On Click Event
     */
    handleOpenRecordClick() {
        // Targeting the Test__c in this instance (instead of the junction object)
        const selectEvent = new CustomEvent('karlrecordview', {
            detail: this.controlitem.Test__c
        });
        this.dispatchEvent(selectEvent);
    }

    /** 
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Create On Click Event for new window
     */
    handleOpenRecord() {
        // Targeting the Test__c in this instance (instead of the junction object)
        this[NavigationMixin.GenerateUrl]({
            type: 'standard__recordPage',
            attributes: {
                recordId: this.controlitem.Test__c,
                actionName: 'view',
            },
        }).then(url => {
            window.open(url, '_blank'); // open in a new tab/window
        });
    }

    /** 
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Edit junction object record on Button Click.
     */
    handleEditClick() {
        // Send REQCT ID via the Event system
        const selectEvent = new CustomEvent('karledit', {
            detail: this.controlitem.Id
        });
        this.dispatchEvent(selectEvent);
    }

    /** 
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Edit junction object record on Button Click.
     */
    handleDeleteClick() {
        // Send REQCT ID via the Event system
        const selectEvent = new CustomEvent('karldelete', {
            detail: this.controlitem.Id
        });
        this.dispatchEvent(selectEvent);
    }

    /** 
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Return true if the user has the Custom Permission for ARL_Is_External_Auditor
     * @return returns undefined if not set, or true if auditor flag is set
     */
    get isExternalAuditor(){
        //console.log(' Is external auditor? >> ' + ARL_Is_External_Auditor);
        return ARL_Is_External_Auditor;
    }

    /** 
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Return true if the user does NOT have either of KARL_Superuser or KARL_Audit_Manager
     * custom permissions (provided by permission sets). Logic is backwards due to the requirement to 
     * disable fields based on non-SCEA Ops or Audit Manager permission sets.
     * @return returns false if matches, or true if not a KARL standard user.
     */
    get notKarlStandardUser(){
        //const karlStandardUser = KARL_Superuser || KARL_Audit_Manager ? false : true;
        //console.log(' Is KARL Standard User? >> ' + !karlStandardUser);
        return KARL_Superuser || KARL_Audit_Manager ? false : true;
    }
}
import { NavigationMixin } from 'lightning/navigation';
import { LightningElement, api, wire, track } from 'lwc';
import { refreshApex } from '@salesforce/apex';
import getDocusignEnvelopeStatus from '@salesforce/apex/KARL_DocuSignEnvelopeStatusController.getDocusignEnvelopeStatus';

export default class Karl_docusign_envelope_status extends NavigationMixin(LightningElement) {
    @api recordId;
    @api objectApiName;
	@track records;
	@track error;
	@track wireData;
	@track docusignEvelopeStatus;

    /** 
     * @author Lakshya Arora
     * @email lakshya.arora@mtxb2b.com
     * @desc Wire the relevant audit firm stakehodlers
     * @param recordId - current record Id
     */
	@wire(getDocusignEnvelopeStatus, {cycleAuditReportId: '$recordId'}) wiredResult(result) {
		this.wireData = result;

        console.log('Debug Wire Audit Firm Stakeholders: ' + JSON.stringify(result));
		if ( result.data && result.data.length ){
			// ! Check if the wire returns any data
			// Error handle the response just in case there is an issue returning the data
			try {
				this.records = true; // specify boolean for if:true
				this.docusignEvelopeStatus = result.data; // establish the data as cReqs
				this.error = false;
			} catch(e) {
				console.log('KARL LWC Error', result.error)
			}
		} else if ( result.data && !result.data.length ){
			// ! Check if the wire is null
			this.records = false; // specify boolean for if:false
		} else if (result.error){
			// ! Check if the wire returns an error, and return the error
			console.log('KARL LWC Error', result.error)
			this.error = result.error;
			this.records = false;
		}
	}

    /** 
     * @author Lakshya Arora
     * @email lakshya.arora@mtxb2b.com
     * @desc Refresh apex to capture changes made upstream
	 * @param event - js event handler
     */
	handleRefresh(event){
		refreshApex(this.wireData);
	}

    /** 
     * @author Lakshya Arora
     * @email lakshya.arora@mtxb2b.com
     * @desc Standard handleRecordView component for KARL LWC's to navigate to Record Pages
	 * @param event - js event handler
     */
	handleRecordView(event) {
		// Get the Target Record ID from the event (typically onClick event)
		const targetRecordId = event.detail;
		// Navigate to the record page
		this[NavigationMixin.Navigate]({
			type: 'standard__recordPage',
			attributes: {
				recordId: targetRecordId,
				actionName: 'view',
			},
		});
	}


}
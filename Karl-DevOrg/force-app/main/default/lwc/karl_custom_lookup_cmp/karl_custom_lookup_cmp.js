import { LightningElement ,api,track} from 'lwc';
import getRecordsAsPerObjectAndFieldList from '@salesforce/apex/KARL_CustomLookupController.getRecordsAsPerObjectAndFieldList';

export default class Karl_custom_lookup_cmp extends LightningElement {
    @api label;
    @api inputPlaceholder = "Type to Search";
    @api isrequired;
    @api isdisabled;
    @api isclosewindow;
    @api objectname;
    @api filterField;
    @api secondaryLabel;

    isOpen = false;
    handlersAdded = false;
    hasFocus = false;

    @track isListEmpty;
    @track selectedRequestItem = null;
    @track requestItemList = [];
    @api parentRequestItemList;
    @track divbackgroundcolor;

    connectedCallback(){
        if(this.parentRequestItemList != null){
            var tempObj={};
            tempObj['label'] = this.parentRequestItemList.label;
            tempObj['value'] = this.parentRequestItemList.value;
            tempObj['secondarylabel'] = this.parentRequestItemList.secondaryField;
            if(tempObj.value && tempObj.label)
            this.selectedRequestItem = tempObj;
            else
            this.selectedRequestItem = null;
        }
    }
    
    dispatchRecordSelectEvent = _ => {
        if (this.selectedRequestItem) {
            this.template.querySelector('.input-container').classList.add('slds-hide');
        } else {     
            this.template.querySelector('.input-container').classList.remove('slds-hide');
            this.template.querySelector('input').reportValidity();
        }
        const recordSelectEvent = new CustomEvent('recordselect', {
            detail: {
                selectedRequestItem: this.selectedRequestItem
            }
        });
        this.dispatchEvent(recordSelectEvent);
    }

    getRecordList = _ => {
        let searchString = this.template.querySelector('input').value;
        if(searchString != null && searchString != undefined && searchString != ''){
            getRecordsAsPerObjectAndFieldList({ objectName: this.objectname,
                filterField: this.filterField,
                searchString: searchString,
                secondaryLabel: this.secondaryLabel })
            .then(response => {
                this.prepareData(response);
                this.template.querySelector('.slds-combobox').classList.add('slds-is-open');
                this.isOpen = true;
                this.isListEmpty = !(this.requestItemList.length > 0);
            }).catch(error => {
             console.log('error = ',error.message || error.body.message);
            })
        }
    }

    /**
     * Various UI Handlers for handling the Combobox hide and show events
     */

    selectRecord = event => {
        let id = event.currentTarget.dataset.id;
        this.requestItemList.some(record => {
            if (record.value === id) {
                this.selectedRequestItem = record;
                this.template.querySelector('.slds-combobox').classList.remove('slds-is-open');
                this.isOpen = false;
                this.dispatchRecordSelectEvent();
                return true;
            }
            return false;
        });
        event.preventDefault();

    }

    @api
    deselectRecordParent(){
        this.selectedRequestItem = null;
        this.template.querySelector('.slds-combobox').classList.remove('slds-is-open');
        this.template.querySelector('input').value="";
        this.dispatchRecordSelectEvent();
    }

   
    deselectRecord = event => {
        this.selectedRequestItem = null;
        this.getRecordList();
        this.dispatchRecordSelectEvent();
    }
    // handle key up event in input
    inputKeyUp = event => {
        let keyCode = event.keyCode;
        if (keyCode === 27) {
            this.template.querySelector('.slds-combobox').classList.remove('slds-is-open');
            this.isOpen = false;
        } else if (keyCode === 13) {
            if (this.requestItemList.length > 0) {
                this.selectedRequestItem = this.requestItemList[0];
                this.template.querySelector('.slds-combobox').classList.remove('slds-is-open');
                this.isOpen = false;
                this.dispatchRecordSelectEvent();
            }
        } else {
            this.getRecordList();
        }

    }
    // handle refresh event
    handleInputClicked = event => {
        if(!this.isdisabled){
            event.preventDefault();
            this.getRecordList();
        }
    }

    hideCombobox = event => {
        if (event.currentTarget === event.target) {
            this.template.querySelector('.slds-combobox').classList.remove('slds-is-open');
        }
    }

    comboboxDropdownClicked = event => {
        event.preventDefault();
    }

    renderedCallback() {
        if(this.isdisabled){
            this.divbackgroundcolor = 'background-color: #e9eaec';
        }
        else{
            this.divbackgroundcolor = 'background-color: white';
        }
        if(this.selectedRequestItem != null){
           
            this.template.querySelector('.input-container').classList.add('slds-hide');
        }
        else{
            this.template.querySelector('.input-container').classList.remove('slds-hide');
        }
      
        if (!this.handlersAdded) {
            this.template.querySelector('.slds-dropdown').addEventListener('focusout', this.hideCombobox)
            this.template.querySelector('.slds-dropdown').addEventListener('focusin', _ => {
                this.hasFocus = true;
            })
            this.template.querySelector('input').addEventListener('blur', event => {
                if (this.isOpen) {
                    this.hasFocus = false;
                    setTimeout(_ => {
                        if (!this.hasFocus) {
                            this.template.querySelector('.slds-combobox').classList.remove(
                                'slds-is-open');
                        }
                    }, 0);
                }
            })
            this.handlersAdded = true;
        }
    }

    prepareData(response){
        this.requestItemList = [];
        for(var i=0;i<response.length;i++){
            var tempObj = {};
            tempObj['label'] = response[i].label;
            tempObj['value'] = response[i].value;
            tempObj['secondarylabel'] = response[i].secondarylabel;
            this.requestItemList.push(tempObj);
        }
    }

    
    @api isValid() {
        if (this.isrequired) {
            this.template.querySelector('input').value = null;
            this.template.querySelector('input').reportValidity();
        }
    }
}
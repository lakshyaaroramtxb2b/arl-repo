import { LightningElement, api } from 'lwc';

/**
 * @author Ben Harvie
 * @email ben.harvie@salesforce.com
 * @description This LWC Component Builds a Modal for Create and Update requests Audit Scope Reports
 */

export default class Karl_scope_reports_modal extends LightningElement {
    @api closeLabel = 'Cancel';
    @api submitLabel = 'Save';
    @api targetrecord;
    @api parentrecord;
    @api associationtype;

    /** 
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @desc Close the model (issue event)
     */
    closeModal() {
        this.dispatchEvent(new CustomEvent('close'));
    }

    /** 
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @desc Submit the form (issue event)
     */
    handleSubmit() {
        this.dispatchEvent(new CustomEvent('submit'));
    }

    /** 
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @desc Return true if associationType is Primary
     */
    get isPrimary(){
        if(this.associationtype == 'Primary'){
            return true;
        }else{
            return false;  
        }
    }

    /** 
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @desc Return true if associationType is Parent
     */
    get isParent(){
        if(this.associationtype == 'Parent'){
            return true;
        }else{
            return false;  
        }
    }

    /** 
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @desc Return true if associationType is Child
     */
    get isChild(){
        if(this.associationtype == 'Child'){
            return true;
        }else{
            return false;  
        }
    }
}
import { NavigationMixin } from 'lightning/navigation';
import { LightningElement, api, wire, track } from 'lwc';

/**
 * @author Ben Harvie
 * @email ben.harvie@salesforce.com
 * @desc This LWC Component Builds the Evidence Workflow Component on the Cycle ARL Item object
 */

// Import Apex Classes
import getCreqEvidenceRecords from '@salesforce/apex/KARL_Utility.getCreqEvidenceRecords';

export default class Karl_evw_view extends NavigationMixin(LightningElement)  {
	@api recordId;
	@track records;
    @track error;
    @track karlError = false;
    wireData = [];

	/** 
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Wire the Evidence Workflow Items
     */
	@wire(getCreqEvidenceRecords, {recordId: '$recordId'}) wired({error, data}) {
		if ( data && data.length ){
			// ! Check if the wire returns any data
			// Error handle the response just in case there is an issue returning the data
			try {
				this.records = true; // specify boolean for if:true
                this.wireData = data; // establish the data as wireData
                this.error = false;
                this.karlError = data.length > 1 ? true : false; // If more than 1 received; display error
			} catch(e) {
				console.log('KARL LWC Error', error)
			}
		} else if ( data && !data.length ){
			// ! Check if the wire is null
			this.records = false; // specify boolean for if:false
		} else if (error){
			// ! Check if the wire returns an error, and return the error
			console.log('KARL LWC Error', error)
			this.error = error;
			this.records = false;
		}
	}

	/** 
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Standard handleRecordView component for KARL LWC's to navigate to Record Pages
     */
	handleRecordView(event) {
		// Get the Target Record ID from the event (typically onClick event)
		const targetRecordId = event.detail;
		// Navigate to the record page
		this[NavigationMixin.Navigate]({
			type: 'standard__recordPage',
			attributes: {
				recordId: targetRecordId,
				actionName: 'view',
			},
		});
	}

	/** 
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Standard handleRecordView component for KARL LWC's to navigate to Community Pages
     */
	handleCommunityView(event) {
		// Get the Target Record ID from the event (typically onClick event)
		const targetPage = event.detail;
		// Navigate to the record page
		this[NavigationMixin.Navigate]({
			type: 'standard__webPage',
			attributes: {
                url: targetPage
			}
		});
	}
}
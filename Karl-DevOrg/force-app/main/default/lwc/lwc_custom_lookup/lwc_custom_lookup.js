/* eslint-disable @lwc/lwc/no-async-operation */
import { LightningElement, wire, api, track } from 'lwc';
import getResults from '@salesforce/apex/PRT_CustomLookupController.fetchRecords';

export default class Lwc_custom_lookup extends LightningElement {
    @api objectName = 'User';
    @api fieldName = 'Name';
    @api selectRecordId = '';
    @api selectRecordName = '';
    @api Label;
    @api searchRecords = [];
    @api required = false;
    @api iconName = 'action:new_account'
    @api LoadingText = false;
    @track txtclassname = 'slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click';
    @track messageFlag = false;
    @track iconFlag = true;
    @track clearIconFlag = false;
    @track inputReadOnly = false;

    constructor() {
        super();
        if (this.selectRecordName !== '') {
            this.iconFlag = false;
            this.inputReadOnly = true;
            this.clearIconFlag = true;
        }
    }

    searchField(event) {
        var currentText = event.target.value;
        this.LoadingText = true;
        /*var interval;
        if (interval) {
            clearTimeout(interval);
        }*/
        window.clearTimeout(this.delayTimeout);
        this.delayTimeout = setTimeout(() => {
            
            getResults({ objectName: this.objectName, filterField: this.fieldName, searchString: currentText, value: '', userId: '' })
                .then(result => {
                    this.searchRecords = result;
                    this.LoadingText = false;

                    this.txtclassname = result.length > 0 ? 'slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click slds-is-open' : 'slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click';
                    if (currentText.length > 0 && result.length == 0) {
                        this.messageFlag = true;
                    } else {
                        this.messageFlag = false;
                    }

                    if (this.selectRecordId != null && this.selectRecordId.length > 0) {
                        this.iconFlag = false;
                        this.clearIconFlag = true;
                    } else {
                        this.iconFlag = true;
                        this.clearIconFlag = false;
                    }
                })
                .catch(error => {});
        }, 1000);

    }

    setSelectedRecord(event) {
        var userId = event.currentTarget.dataset.id;
        this.txtclassname = 'slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click';
        this.iconFlag = false;
        this.clearIconFlag = true;
        this.selectRecordName = event.currentTarget.dataset.name;
        this.selectRecordId = userId;
        this.inputReadOnly = true;
        const selectedEvent = new CustomEvent('selected', { detail: { userId }, });
        // Dispatches the event.
        this.dispatchEvent(selectedEvent);
    }

    resetData(event) {
        this.selectRecordName = "";
        this.selectRecordId = "";
        this.inputReadOnly = false;
        this.iconFlag = true;
        this.clearIconFlag = false;
    }

}
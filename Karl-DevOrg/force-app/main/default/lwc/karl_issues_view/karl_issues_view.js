import { NavigationMixin } from 'lightning/navigation';
import { LightningElement, api, wire, track } from 'lwc';
import { refreshApex } from '@salesforce/apex';

/**
 * @author Ben Harvie
 * @email ben.harvie@salesforce.com
 * @desc This LWC Component Builds the Audit Issues Card
 */

// Import Related CREQs object fields
import getScopeAuditIssues from '@salesforce/apex/KARL_AuditIssuesLwcController.getScopeAuditIssues';

export default class Karl_issues_view extends NavigationMixin(LightningElement) {
    @api recordId;
    @api objectApiName;
	@track records;
	@track error;
	@track wireData;
	@track auditIssues;

    /** 
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @desc Wire the relevant Audit Issues
     * @param recordId - current record Id
     */
	@wire(getScopeAuditIssues, {recordId: '$recordId', objectApiName: '$objectApiName'}) wiredResult(result) {
		this.wireData = result;

        console.log('Debug Wire: ' + JSON.stringify(result));
		if ( result.data && result.data.length ){
			// ! Check if the wire returns any data
			// Error handle the response just in case there is an issue returning the data
			try {
				this.records = true; // specify boolean for if:true
				this.auditIssues = result.data; // establish the data as cReqs
				this.error = false;
			} catch(e) {
				console.log('KARL LWC Error', result.error)
			}
		} else if ( result.data && !result.data.length ){
			// ! Check if the wire is null
			this.records = false; // specify boolean for if:false
		} else if (result.error){
			// ! Check if the wire returns an error, and return the error
			console.log('KARL LWC Error', result.error)
			this.error = result.error;
			this.records = false;
		}
	}

	/** 
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @desc Refresh apex to capture changes made upstream
	 * @param event - js event handler
     */
	handleRefresh(event){
		refreshApex(this.wireData);
	}

	/** 
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @desc Standard handleRecordView component for KARL LWC's to navigate to Record Pages
	 * @param event - js event handler
     */
	handleRecordView(event) {
		// Get the Target Record ID from the event (typically onClick event)
		const targetRecordId = event.detail;
		// Navigate to the record page
		this[NavigationMixin.Navigate]({
			type: 'standard__recordPage',
			attributes: {
				recordId: targetRecordId,
				actionName: 'view',
			},
		});
	}


}
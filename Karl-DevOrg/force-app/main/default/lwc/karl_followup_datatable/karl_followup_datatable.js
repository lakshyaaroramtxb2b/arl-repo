import {
    LightningElement, 
    api, 
    wire, 
    track
} from 'lwc';
import {
    refreshApex
} from '@salesforce/apex';
import {
    ShowToastEvent
} from 'lightning/platformShowToastEvent';
import {
    getRecordNotifyChange
} from 'lightning/uiRecordApi';
import {
    CurrentPageReference,
    NavigationMixin
} from 'lightning/navigation';
import {
    arrayRemove, getDefaultURLValue, scopeOptions, cycleOptions, teamOptions, setLoadingSpinner
} from 'c/karlUtil';
import Id from '@salesforce/user/Id';
import ARL_Is_External_Auditor from '@salesforce/customPermission/ARL_Is_External_Auditor';

/**
 * @author Ben Harvie
 * @email ben.harvie@salesforce.com
 * @description This LWC Component Builds a in-line editable datatable. Note that this is a unique
 * LWC for the KARL app and is not reusable for other purposes. The LWC is utilized on both
 * the community as well as standard salesforce environment.
 */

// Import Apex Classes
import getContactId from '@salesforce/apex/KARL_Utility.getContactId';
import getFollowupReport from '@salesforce/apex/KARL_FollowUpReportController.getReport';
import updateFollowup from '@salesforce/apex/KARL_FollowUpReportController.updateFollowup';

export default class Karl_followup_datatable extends NavigationMixin(LightningElement) {
    @api recordId;
    userId = Id;
    userContactId;

    // Metadata variables
    @api auditorsView = false;
    @api defaultCycle;
    @api defaultScope;
    @api defaultTeam;
    @api defaultSortBy;
    @api defaultSortDirection;

    // Wire Data Output
    @track auditCycleOptions = [];
    @track auditScopeOptions = [];
    @track auditTeamOptions = [];
    @track filteredData = [];
    @track wireData; // Full result (for refreshApex)

    // Wire Data Input / Management
    @track auditcycle;
    @track auditscope;
    @track auditteam;
    @track totalRows;
	@track error;
    @track records;

    fieldApis               = {
        KARL_Auditor_Comments__c: 'commentsAuditor',
        KARL_Salesforce_Comments__c: 'commentsSFDC',
        KARL_Follow_up_Status__c: 'followupStatus',
        KARL_Current_Assignment__c: 'followupAssign',
        }

    // Button Management
    @track openclosed;
    @track assignedtome;
    @track assignment;
    defaultOpenclosed   = 'all';
    defaultAssignedToMe = 'allAssigned';
    defaultAssignment   = 'currentAll';
    buttonsOpenclosed   = ['all','open','new','triaged','inProgress','readyForReview','closed','followUp'];
    buttonsAssignment   = ['currentSalesforce','currentAuditor','currentAll'];
    buttonsAssigned     = ['allAssigned','assignedToMe'];

    // Sorting variables
    @track sortDirection;
    @track sortBy;

    // Miscellaneous
    @track isLoading;
    @track togglecontrols = false;
    @track paramsSet = false;
    @track sfdcEdits = false;
    @track auditorEdits = false;
    @track saveButton = false;
    @track draftValues = [];
    @track picklistChanges = [];
    searchFields        = ['searchsubject','searchdesc'];
    currentPageReference;
    // Modal Details
    @track modalEditDetails;
    @track recordTarget;
    @track currentUserId;
    @track auditorUser;

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description utilize connectedCallback to generate on load
     */
    connectedCallback() {
        this.sfdcEdits      = ARL_Is_External_Auditor ? false : true; // editable if SFDC/Internal
        this.auditorUser    = ARL_Is_External_Auditor ? true : false; 

        this.connected = true;

        if(this.connectedQueue){
            this.setFilters();
        }

        // Utility Class Initialization
        scopeOptions().then((result => this.auditScopeOptions = result ));
        cycleOptions().then((result => this.auditCycleOptions = result ));
        teamOptions().then((result => this.auditTeamOptions = result ));

        // Load the columns of the datatable
        this.loadColumns();
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description utilize renderedCallBack to update toggle elements after they're loaded
     */
    renderedCallback(){
        // Utilize setButtonSelection so we also check for default values        
        this.setButtonSelection(this.buttonsAssigned, this.assignedtome, this.defaultAssignedToMe)
        this.setButtonSelection(this.buttonsAssignment, this.assignment, this.defaultAssignment)
        this.setButtonSelection(this.buttonsOpenclosed, this.openclosed, this.defaultOpenclosed)
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Using the CurrentPageReference component of NavigiationMixin, retrieve 
     * metadata about this page. 
     */
    @wire(CurrentPageReference)
    setCurrentPageReference(currentPageReference) {
        this.currentPageReference = currentPageReference;
        // Loading URL params (if not defined; return defaults from metadata)
        if(this.connected){
            this.setFilters();
        }else{
            this.connectedQueue = true;
        }
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description set the page filters
     */
    setFilters(){
        this.auditcycle     = this.getDefaultURLValue('c__cycleId', this.defaultCycle);
        this.auditscope     = this.getDefaultURLValue('c__scopeId', this.defaultScope);
        this.auditteam      = this.getDefaultURLValue('c__teamId', this.defaultTeam);
        this.sortBy         = this.getDefaultURLValue('c__sortBy', this.defaultSortBy);
        this.sortDirection  = this.getDefaultURLValue('c__sortDirection', this.defaultSortDirection);
        this.assignedtome   = this.getDefaultURLValue('c__assignedTo', this.defaultAssignedToMe);
        this.assignment     = this.getDefaultURLValue('c__assignment', this.defaultAssignment);
        this.openclosed     = this.getDefaultURLValue('c__state', this.defaultOpenclosed);
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description set the button shading using querySelector
     */
    setButtonSelection(buttonValues, setValue, defaultValue){
        if(buttonValues.indexOf(setValue) != -1){
            this.template.querySelector('button[value="' + setValue + '"]').classList.add('slds-button_brand');
        }else{
            this.template.querySelector('button[value="' + defaultValue + '"]').classList.add('slds-button_brand');
        }
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Update the currentPageReference state with changes on the URL parameters
     * @param stateChanges - the state (URL component) which is changing
     */
    getUpdatedPageReference(stateChanges) {
        return Object.assign({}, this.currentPageReference, {
            state: Object.assign({}, this.currentPageReference.state, stateChanges)
        });
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Update the page reference parameters based on this.* values
     */
    get updatedPageReference() {
        return this.getUpdatedPageReference({
            c__scopeId: this.auditscope,
            c__cycleId: this.auditcycle,
            c__teamId: this.auditteam,
            c__sortBy: this.sortBy,
            c__sortDirection: this.sortDirection,
            c__state: this.openclosed,
            c__assignedTo: this.assignedtome,
            c__assignment: this.assignment
        });
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Retrieve the URL parameters from the currentPageReference. Note we use this
     * instead of searchParams in accordance with Salesforce guidance.
     * @param getParam - the value from the URL
     * @param defaultvalue - default value (set in metadata or code)
     */
    getDefaultURLValue(getParam, defaultValue){
        // Use the currentPageReference to get the requested
        const urlParam = this.currentPageReference.state[getParam];

        if((urlParam != null || urlParam != undefined)){
            return urlParam;
        }else{
            // Nothing passed via the URL, so we'll set default value from metadata (if exists)
            return defaultValue;
        }

    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Set the spinner (visual UI) when something is loading
     */
    setLoadingSpinner(){
        if(this.auditscope != undefined && this.auditcycle != undefined && this.auditteam != undefined){
            this.isLoading = true;
        }else{
            this.isLoading = false;
        }
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Manage the columns via this method, which pushes to the datatable. Doing it this way
     * instead of via basic array allows us to use metadata/other flags to determine which columns
     * to display to the end-user.
     */
    loadColumns() {
        this.columns = [];

        this.columns.push({
            label: 'Subject', 
            fieldName: 'followupName', 
            type: 'button',
            editable: false,
            wrapText: true,
            sortable: true, 
            hideDefaultActions: true,
            initialWidth: 160,
            typeAttributes: {
                label: {fieldName: 'followupName'},
                variant: 'base',
                name: 'openModal', 
            }
        });
        /*this.columns.push({
            label: 'Subject', 
            fieldName: 'followupLink', 
            type: 'url',
            editable: false,
            wrapText: true,
            sortable: true, 
            hideDefaultActions: true,
            initialWidth: 160,
            typeAttributes: {
                label: {fieldName: 'followupName'},
                target: '_blank'
            }
        });*/
        if(this.auditscope == 'allScopes' || this.auditscope == 'noScopes'){
            this.columns.push({
                label: 'Scope', 
                fieldName: 'pscopeName', 
                wrapText: true,
                editable: false,
                sortable: true, 
                initialWidth: 80 
            });
        }
        this.columns.push({
            label: 'Date Created',
            type: 'date-local',
            fieldName: 'followupCreated', 
            wrapText: true,
            sortable: true,
            initialWidth: 100 
        });
        this.columns.push({
            label: 'Age',
            fieldName: 'followupAge', 
            wrapText: true,
            sortable: true,
            initialWidth: 50
        });
        this.columns.push({
            label: 'Type', 
            fieldName: 'followupType',
            initialWidth: 125, 
            wrapText: true,
            sortable: true,
            actions: [
                { label: 'All', checked: true, name: 'all' },
                { label: 'Control Question', checked: false, name: 'Control Question' },
                { label: 'Evidence Question', checked: false, name: 'Evidence Question' },
                { label: 'Walkthrough Question', checked: false, name: 'Walkthrough Question' },
                { label: 'General/Other Question', checked: false, name: 'General/Other Question' },
                { label: 'Informational', checked: false, name: 'Informational' }
            ] 
        });
        this.columns.push({
            label: 'Follow-up Description', 
            fieldName: 'followupDesc', 
            type: 'text',
            wrapText: true,
            initialWidth: 270,
            typeAttributes: {
                linkify: true
            },
            cellAttributes: {
                class: 'slds-scrollable'
            }
        });
        // NOTE: for this custom data type we need to set wrapText: true so that the input values display
        this.columns.push({
            label: 'Assignment', 
            fieldName: 'followupAssign',
            type: 'picklist',
            editable: true,
            sortable: true,
            wrapText: true,
            initialWidth: 130, 
            typeAttributes: {
                placeholder: 'Select Assignment...', 
                options: [
                    { label: 'Salesforce', value: 'Salesforce' },
                    { label: 'Auditor', value: 'Auditor' }
                ],
                field: 'KARL_Current_Assignment__c',
                value: { fieldName: 'followupAssign' },
                context: { fieldName: 'Id' },
                selected: this.picklistChanges
            }, 
            actions: [
                { label: 'Salesforce', checked: true, name: 'Salesforce' },
                { label: 'Auditor', checked: false, name: 'Auditor' }
            ]
        });
        this.columns.push({
            label: 'Status', 
            fieldName: 'followupStatus',
            type: 'picklist',
            editable: true,
            sortable: true,
            wrapText: true,
            initialWidth: 130, 
            typeAttributes: {
                placeholder: 'Select Status...', 
                options: [
                    { label: 'New', value: 'New' },
                    { label: 'Triaged', value: 'Triaged' },
                    { label: 'In Progress', value: 'In Progress' },
                    { label: 'Ready for Review', value: 'Ready for Review' },
                    { label: 'Follow-up', value: 'Follow-up' },
                    { label: 'Closed', value: 'Closed' }
                ],
                field: 'KARL_Follow_up_Status__c',
                value: { fieldName: 'followupStatus' },
                context: { fieldName: 'Id' },
                selected: this.picklistChanges
            }, 
            actions: [
                { label: 'All', checked: true, name: 'all' },
                { label: 'Open', checked: true, name: 'Open' },
                { label: 'New', checked: false, name: 'New' },
                { label: 'Triaged', checked: false, name: 'Triaged' },
                { label: 'In Progress', checked: false, name: 'In Progress' },
                { label: 'Ready for Review', checked: false, name: 'Ready for Review' },
                { label: 'Follow-up', checked: false, name: 'Follow-up' },
                { label: 'Closed', checked: false, name: 'Closed' }
            ]
        });
        this.columns.push({
            label: 'SFDC POC', 
            fieldName: 'pocGRC',
            initialWidth: 100, 
            sortable: true,
            wrapText: true
        });
        this.columns.push({
            label: 'Auditor POC', 
            fieldName: 'pocAuditor',
            initialWidth: 100, 
            sortable: true,
            wrapText: true
        });
        this.columns.push({
            label: 'SFDC Quick Comments', 
            fieldName: 'commentsSFDC',
            editable: this.sfdcEdits,
            sortable: true,
            wrapText: true,
            typeAttributes: {
                linkify: true
            }
        });
        this.columns.push({
            label: 'Auditor Quick Comments', 
            fieldName: 'commentsAuditor',
            editable: !this.sfdcEdits,
            sortable: true,
            wrapText: true,
            typeAttributes: {
                linkify: true
            }
        });
        /* hidden due to real-estate issues
        this.columns.push({
            label: 'Issue', 
            fixedWidth: 40,
            editable: false,
            sortable: true,
            wrapText: true,
            hideDefaultActions: true,
            cellAttributes: {
                iconName: { fieldName: 'potentialIssue' },
                iconAlternativeText: 'Potential Issue'
            }
        });*/
        if(!this.hideActionsCol){
            this.columns.push({
                type:  'button-icon',
                fixedWidth: 60,
                hideDefaultActions: true,
                typeAttributes: {
                    iconName: 'utility:new_window',
                    name: 'openRequest', 
                    title: 'Open Record', 
                    disabled: false
                }
            });
        }
    }

    // Retrieve the primary data table information
    @wire(getFollowupReport, { auditCycleId: '$auditcycle', auditScopeId: '$auditscope', auditTeamId: '$auditteam', 
    auditorView: '$auditorsView', filterState: '$openclosed', filterAssigned: '$assignedtome', filterAssignment: '$assignment'})  wiredARL(result) {
        this.wireData = result; // Save the full result, for diffing refreshApex later

        //console.log("KARL >> Parameters are set - data feed updating!");

        if ( result.data && result.data.length ){
            // ! Check if the wire returns any data
            try {
                this.paramsSet = true;
                this.records = true; // Specify boolean for if:true
                this.filteredData = result.data; // Initially unfiltered is filteredData.
                this.isLoading = false;
                this.error = false;
                this.sortData(this.sortBy, this.sortDirection);
                this.totalRows = result.data.length;
            } catch(e) {
                console.log('KARL >> LWC Error', result.error);
                this.isLoading = false;
            }
        } else if ( result.data && !result.data.length ){
            // ! Check if the wire is null
            //console.log('KARL >> LWC Empty', result.data);
            this.paramsSet = true;
            this.records = false; // specify boolean for if:false
            this.isLoading = false;
            this.totalRows = result.data.length;
        } else if (result.error){
            // ! Check if the wire returns an error, and return the error
            if(!this.auditcycle){
                // False error --> just waiting on parameters
                //console.log('KARL >> LWC Parameters not set');
                this.paramsSet = false;
                this.records = false; // specify boolean for if:false
                this.isLoading = false;
            }else{
                // Legitimate error
                console.log('KARL >> LWC Error', result.error);
                this.error = result.error;
                this.paramsSet = true;
                this.isLoading = false;
                this.records = false;
            }
        }
    }

    @wire(getContactId, {}) wiredContactId(contactResult){
        this.userContactId = contactResult.data;
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description event handler for handling changes to Audit Cycle combobox
     * @param event - js event handle
     */
    handleAuditCycleChange(event) {
        // TODO: Need to also reset the Header Actions selection
        this.setLoadingSpinner();
        this.auditcycle = event.detail.value;
        this[NavigationMixin.Navigate](this.updatedPageReference, true);
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description event handler for handling changes to Audit Scope combobox
     * @param event - js event handle
     */
    handleAuditScopeChange(event) {
        this.setLoadingSpinner();
        this.auditscope = event.detail.value;
        this.loadColumns();
        this[NavigationMixin.Navigate](this.updatedPageReference, true);
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description event handler for handling changes to Audit Team combobox
     * @param event - js event handle
     */
    handleAuditTeamChange(event) {
        this.setLoadingSpinner();
        this.auditteam = event.detail.value;
        this[NavigationMixin.Navigate](this.updatedPageReference, true);
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description event handler for handling onheaderaction events from the data table, for filtering.
     * @param event - js event handle
     */
    handleHeaderAction (event) {
        // Retrieves the name of the selected filter
        const actionName = event.detail.action.name;
        // Retrieves the current column definition
        const colName = event.detail.columnDefinition.fieldName;
        const filterColumns = this.columns;
        const activeFilter = this.activeFilter;

        // Exclude events that are for wrapText and clipText (default Salesforce header actions)
        if (actionName !== activeFilter && actionName != 'wrapText' && actionName != 'clipText') {
            filterColumns.map((col) => {
                if(col.fieldName === colName){
                    var actions = col.actions;

                    actions.forEach((action) => {
                        action.checked = action.name === actionName;
                    });
                }
            });
            this.activeFilter = actionName;
            this.updateDataTable(colName);
            this.columns = filterColumns;

            // Update the actions list to show which action is currently selected
            filterColumns.find(col => col.fieldName === colName).actions.forEach(action => action.checked = action.name === actionName);
            this.columns = [...filterColumns];
        }
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description update the Data Table based on the selected filters (in header actions)
     * @param columnName - the column from the header action
     */
    updateDataTable(columnName) {
        this.filteredData = this.wireData.data; // Reset to unfiltered data
        this.refreshDraftValues(); // Apply any changes we have stored in draftValues
        const rows = this.filteredData;
        const activeFilter = this.activeFilter;
        let filteredRows = rows;

        if (activeFilter !== 'all') {
            filteredRows = rows.filter(function (row) {
                if(row[columnName] !== '' && row[columnName] !== undefined){
                    return row[columnName].toUpperCase() === activeFilter.toUpperCase();
                }     
            });
        }
        this.filteredData = filteredRows;
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Update the sorting based on header action selections
     */
    doSorting(event){
        console.log('Sorting?');
        this.sortBy = event.detail.fieldName;
        this.sortDirection = event.detail.sortDirection;
        this.sortData(this.sortBy, this.sortDirection);
        this[NavigationMixin.Navigate](this.updatedPageReference, true);
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Sort the data based on selection; supporting method
     */
    sortData(fieldname, direction){
        let parseData = JSON.parse(JSON.stringify(this.filteredData));

        let keyValue = (a) => {
            return a[fieldname];
        }

        let isReverse = direction ==='asc'?1:-1;
        
        parseData.sort((x, y) => {
            x = keyValue(x) ? keyValue(x) : ''; // handling null values
            y = keyValue(y) ? keyValue(y) : '';
            // sorting values based on direction
            return isReverse * ((x > y) - (y > x));
        });
        this.filteredData = parseData;
        //this.refreshDraftValues();
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Handle searches from the inputs on the main component page
     * @param event - js event handle
     */
    handleSearch(event){
        const rows = JSON.parse(JSON.stringify(this.filteredData)); // raw unfiltered data to start with
        const searchString = event.target.value;
        const searchField = event.target.name;
        var regex = new RegExp(searchString.replace('/[|\\{}()[\]^$+*?.]/g', '\\$&'),'i'); // previously tried gi - i much more reliable
        let filteredRows = rows;

        if(searchString !== ''){
            // TODO: More tech debt here than I would like with all the if/else switches. Couldn't get it working.
            if( searchField === 'searchDesc' ){
                filteredRows = rows.filter(
                    row => regex.test(
                        row.followupDesc
                    )
                );
            } else if ( searchField === 'searchSubject' ){
                filteredRows = rows.filter(
                    row => regex.test(
                        row.followupName
                    )
                );
            } else {
                // No search configured.. nothing to do.
            }
        }else{
            // Reset the fields back to original filter
            filteredRows = JSON.parse(JSON.stringify(this.wireData.data));
        }
        // Update the filteredData to respect the filteredRows.
        this.filteredData = filteredRows;
        this.refreshDraftValues();
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Handle picklist changes / adding them in to the new draftValues
     * @param event - js event handle
     */
    handlePicklistChange(event) {
        event.stopPropagation();
        this.saveButton = true;
        let dataReceived = event.detail.data;
        let updatedItem = { Id: dataReceived.context, [dataReceived.field]: dataReceived.value };

        this.updateDraftValues(updatedItem);
        this.updateDataValues(updatedItem);
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Update draftValues on normal cell edits, since we're not using the OOB draftValues
     * @param event - js event handle
     */
    handleCellChange(event){
        this.saveButton = true;

        for(var fieldApi in this.fieldApis){
            if(this.fieldApis[fieldApi] in event.detail.draftValues[0]){
                event.detail.draftValues[0][fieldApi] = event.detail.draftValues[0][this.fieldApis[fieldApi]];
            }
        }

        this.updateDraftValues(event.detail.draftValues[0]);
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description refresh this.draftValues (which is a Salesforce detault) to reflect changes from picklist/cells. 
     * This is necessary for being able to reset the data values (e.g. when clicking cancel on edit)
     */
    refreshDraftValues(){
        this.draftValues.forEach(item => {
            this.updateDataValues(item);
        });
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description update the data values in the datatable
     * @param updateItem - the value which is changing that needs to be updated
     */
    updateDataValues(updateItem) {
        let changeData = [... JSON.parse(JSON.stringify(this.filteredData))];
        
        changeData.forEach(item => {
            if (item.Id === updateItem.Id) {
                for (let field in updateItem) {
                    if(this.fieldApis.hasOwnProperty(field) ){
                        item[this.fieldApis[field]] = updateItem[field];
                        this.picklistChanges.push("karlpicklist_" + item.Id);
                    }
                }
            }
        });

        this.filteredData = [...changeData];
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Update the draftValues array with the draft edits from inline editing (from picklist change)
     */
    updateDraftValues(updateItem) {
        let draftValueChanged = false;
        let copyDraftValues = [...this.draftValues];

        // Determine if there is already a draft change, so we don't duplicate
        copyDraftValues.forEach(item => {
            if (item.Id === updateItem.Id) {
                for (let field in updateItem) {
                    item[field] = updateItem[field];
                }
                draftValueChanged = true;
            }
        });

        // Update draftValues using spread operator
        if (draftValueChanged) {
            this.draftValues = [...copyDraftValues];
        } else {
            this.draftValues = [...copyDraftValues, updateItem];
        }
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Handle save of the inline edits via Apex controller
     * @event - js event handle (not used - simply a triggering function)
     */
    async handleSave(event) {
        const updatedFields = this.draftValues;
        const searchFields = this.searchFields;
        const onchangeEvent = new CustomEvent('change',{bubbles: true});
        // Prepare the record IDs for getRecordNotifyChange
        const notifyChangeIds = updatedFields.map(row => { 
            return { "recordId": row.Id }
        });

        await updateFollowup({data: updatedFields})
            .then(result => { 
                //console.log(JSON.stringify("Apex update result >> "+ result));
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Success',
                        message: 'KARL records updated successfully.',
                        variant: 'success'
                    })
                );
        
                // Refresh LDS cache and wires
                getRecordNotifyChange(notifyChangeIds);
                
                // Display fresh data in the datatable
                refreshApex(this.wireData).then(() => {
                    // Clear all draft values in the datatable
                    this.draftValues = [];
                    this.saveButton = false;
                    // Re-apply client-side search filters
                    for(let i = 0; i < searchFields.length; i++){
                        this.template.querySelector('lightning-input[data-field="' + searchFields[i] + '"]').value != '' ? this.template.querySelector('lightning-input[data-field="' + searchFields[i] + '"]').dispatchEvent(onchangeEvent) : null; 
                    }
                });

        }).catch(error => {
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Error updating or refreshing records',
                    message: error.body.message,
                    variant: 'error'
                })
            );
        });
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Handle refreshing the datatable based on button click
     * @param event - js event handle (not used - simply a trigger)
     */
    handleRefresh(event){
        const searchFields = this.searchFields;
        const onchangeEvent = new CustomEvent('change',{bubbles: true});
        refreshApex(this.wireData).then(() => {
            // Re-apply client-side search filters
            for(let i = 0; i < searchFields.length; i++){
                this.template.querySelector('lightning-input[data-field="' + searchFields[i] + '"]').value != '' ? this.template.querySelector('lightning-input[data-field="' + searchFields[i] + '"]').dispatchEvent(onchangeEvent) : null; 
            }
        });
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Handle click of the Cancel button, resetting data back in the datatable and draftValues
     * @param event - js event handle (not used - simply a trigger)
     */
    handleCancel(event) {
        this.saveButton = false;
        this.draftValues = [];
        this.filteredData = this.wireData.data;
        this.sortData(this.sortBy, this.sortDirection);
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Handle rowActions in the data table, e.g. to take the user to the resource page in a new window.
     * @param event - js event handle
     */
    handleRowAction(event){
        //console.log(JSON.stringify(event.detail.action) + ' on row ' + JSON.stringify(event.detail.row));
        if(event.detail.action.name==='openRequest') {
            this[NavigationMixin.GenerateUrl]({
                type: 'standard__recordPage',
                attributes: {
                    recordId: event.detail.row.Id,
                    actionName: 'view'
                }
            }).then(url => {
                //console.log('Target URL >> ' + url); // Log the target URL in the console
                window.open(url, '_blank'); // open in a new tab/window
            });
        }else if(event.detail.action.name==='openModal'){
            this.recordTarget = event.detail.row.Id;
            this.modalEditDetails = true;
        }
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Handle click on the button group for Open/Provided/Closed
     * @param event - onClick event js handle
     */
    handleOpenClosed(event){
        let buttonIds = this.buttonsOpenclosed;

        if(this.openclosed != event.target.value){
            this.setLoadingSpinner();  
            this.openclosed = event.target.value;
            
            // Remove the selected area of compliance from the buttonIds array
            buttonIds = buttonIds.filter(button => button !== this.openclosed);

            // Set the formatting of the selected buttonId
            this.template.querySelector('button[value="' + this.openclosed + '"]').classList.add('slds-button_brand'); 
            this.template.querySelector('button[value="' + this.openclosed + '"]').classList.remove('slds-button_neutral'); 

            // Reset formatting of other options
            for(let i = 0; i < buttonIds.length; i++){
                this.template.querySelector('button[value="' + buttonIds[i] + '"]').classList.add('slds-button_neutral'); 
                this.template.querySelector('button[value="' + buttonIds[i] + '"]').classList.remove('slds-button_brand');
            }

            this[NavigationMixin.Navigate](this.updatedPageReference, true);
        }
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Handle click on the button group for Assigned To Me
     * @param event - onClick event
     */
    handleAssigned(event){
        let buttonIds = this.buttonsAssigned;

        if(this.assignedtome != event.target.value){
            this.setLoadingSpinner();  
            this.assignedtome = event.target.value;
            
            // Remove the selected area of compliance from the buttonIds array
            buttonIds = buttonIds.filter(button => button !== this.assignedtome);

            // Set the formatting of the selected buttonId
            this.template.querySelector('button[value="' + this.assignedtome + '"]').classList.add('slds-button_brand'); 
            this.template.querySelector('button[value="' + this.assignedtome + '"]').classList.remove('slds-button_neutral'); 

            // Reset formatting of other options
            for(let i = 0; i < buttonIds.length; i++){
                this.template.querySelector('button[value="' + buttonIds[i] + '"]').classList.add('slds-button_neutral'); 
                this.template.querySelector('button[value="' + buttonIds[i] + '"]').classList.remove('slds-button_brand');
            }

            this[NavigationMixin.Navigate](this.updatedPageReference, true);
        }
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Handle click on the button group for Assigned To Me
     * @param event - onClick event
     */
    handleAssignment(event){
        let buttonIds = this.buttonsAssignment;

        if(this.assignment != event.target.value){
            this.setLoadingSpinner();  
            this.assignment = event.target.value;
            
            // Remove the selected area of compliance from the buttonIds array
            buttonIds = buttonIds.filter(button => button !== this.assignment);

            // Set the formatting of the selected buttonId
            this.template.querySelector('button[value="' + this.assignment + '"]').classList.add('slds-button_brand'); 
            this.template.querySelector('button[value="' + this.assignment + '"]').classList.remove('slds-button_neutral'); 

            // Reset formatting of other options
            for(let i = 0; i < buttonIds.length; i++){
                this.template.querySelector('button[value="' + buttonIds[i] + '"]').classList.add('slds-button_neutral'); 
                this.template.querySelector('button[value="' + buttonIds[i] + '"]').classList.remove('slds-button_brand');
            }

            this[NavigationMixin.Navigate](this.updatedPageReference, true);
        }
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Handle onclick event for New Follow-up with default field values passed
     * @param event - js event handle
     */
    handleNew(event){
        this.modalEditDetails = true;
        /*
        let defaultContact = '';
        // Manage switch between community and non-community users
        const pocFieldApi = ARL_Is_External_Auditor ? 'KARL_Followup_Auditor_POC__c' : 'KARL_Followup_SCEA_POC__c';
        if(this.userContactId !== undefined){
            defaultContact = ',' + pocFieldApi + '=' + this.userContactId;
        }
        
        // Avoid issues with allScopes and noScopes not existing in picklist
        let scopeValue = this.auditscope;
        if(this.auditscope == 'allScopes' || this.auditscope == 'noScopes'){
            scopeValue = '';
        }

        let defaultValues = '';
        if(this.auditteam !== undefined && this.auditcycle !== undefined && this.auditscope !== undefined){
            defaultValues = 'KARL_Followup_Audit_Team__c=' + this.auditteam +
                            ',KARL_Followup_Audit_Cycle__c=' + this.auditcycle +
                            ',KARL_Audit_Scope__c=' + scopeValue + defaultContact;
        }
        console.log('Default Values >> ' + defaultValues);
        console.log('Current User Contact >> ' + this.userContactId);
        this[NavigationMixin.GenerateUrl]({
            type: 'standard__objectPage',
            attributes: {
                objectApiName: 'KARL_Cycle_Follow_up__c',
                actionName: 'new'
            },
            state: {
                defaultFieldValues: defaultValues,
                nooverride: '1'
            }
        }).then(url => {
            //console.log('Target URL >> ' + url); // Log the target URL in the console
            window.open(url, '_blank'); // open in a new tab/window
        });*/
    }

    closeModals() {
        this.modalEditDetails = false;
        this.recordTarget = null;
    }
    
    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Handle submission of the control mapping form (modal)
     */
    handleModalSubmit(){
        this.closeModals();

        refreshApex(this.wireData).then(() => {
            this.refreshDraftValues();
        });  

        this.dispatchEvent(
            new ShowToastEvent({
                title: 'Success',
                message: 'Record updated successfully.',
                variant: 'success'
            })
        )
    }
}
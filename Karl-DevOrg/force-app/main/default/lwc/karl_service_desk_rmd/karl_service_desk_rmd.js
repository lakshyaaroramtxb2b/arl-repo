import { LightningElement, track,wire,api } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import createServiceDeskRecord from '@salesforce/apex/KARL_ServiceDeskController.createServiceDeskRecord';
import getRequestMasterDataInfo from '@salesforce/apex/KARL_ServiceDeskController.getRequestMasterDataInfo';
import KARL_SERVICE_DESK_OBJECT from '@salesforce/schema/KARL_Service_Desk__c';
import TYPE_FIELD from '@salesforce/schema/KARL_Service_Desk__c.Type__c';
import BUSINESS_UNIT_FIELD from '@salesforce/schema/KARL_Service_Desk__c.Business_Unit__c';
import AREA_OF_COMPLIANCE_FIELD from '@salesforce/schema/KARL_Service_Desk__c.Area_of_Compliance__c';
import { getPicklistValues } from 'lightning/uiObjectInfoApi';
import { getObjectInfo } from 'lightning/uiObjectInfoApi';
import {FlowAttributeChangeEvent, FlowNavigationNextEvent,FlowNavigationBackEvent } from 'lightning/flowSupport';

export default class Karl_service_desk_rmd extends LightningElement {
    //flow variables
    @api requestItemRecordId;
    @api ServiceDeskCreated;
    //flow variables end
    @track serviceDeskWrapper={};
    @track typeOptions=[];
    @track businessUnitOptions = [];
    @track isrequired = false;
    @track showSpinner = false;
    @track isShowPreviousModal;
    @track areaOfComplianceOptions=[];
    @track isdatafetch = false;

    connectedCallback(){
        if(this.requestItemRecordId != '' && this.requestItemRecordId != undefined && this.requestItemRecordId != null){
            getRequestMasterDataInfo( 
                { requestItemId : this.requestItemRecordId }
            )
            .then(result=>{
                this.serviceDeskWrapper = result;
                this.isdatafetch = true;
            })
            .catch(error=>{
                this.serviceDeskWrapper['newOrExisitng'] = 'New';
                console.log('error = ',error.message || error.body.message);
            })
        }
        else{
            this.serviceDeskWrapper['newOrExisitng'] = 'New';
            this.isdatafetch = true;
        }
    }
    
    
    defaultRecordTypeId;
    @wire(getObjectInfo, { objectApiName: KARL_SERVICE_DESK_OBJECT })
    getRecordTypeInfo({ error, data }) {
        if (data) {
            this.defaultRecordTypeId = data.defaultRecordTypeId;
        }
    }
    @wire(getPicklistValues, { recordTypeId: '$defaultRecordTypeId', fieldApiName: TYPE_FIELD })
    getTypePicklistOptions({ error, data }) {
        if (data) {
            this.typeOptions = data.values;
        }
    }

    @wire(getPicklistValues, { recordTypeId: '$defaultRecordTypeId', fieldApiName: BUSINESS_UNIT_FIELD})
    getPicklistbusinessUnitOptions({ error, data }) {
        if (data) {
            this.businessUnitOptions = data.values;
        }
    }

    @wire(getPicklistValues, { recordTypeId: '$defaultRecordTypeId', fieldApiName: AREA_OF_COMPLIANCE_FIELD})
    getDualPicklistAreaOfComplianceOptions({ error, data }) {
        if (data) {
            this.areaOfComplianceOptions = data.values;
        }
    }
    

    handleChange(event){
        this.serviceDeskWrapper[event.target.name] = event.target.value;
    }


    selectedControlUnitRecords(event){
        this.serviceDeskWrapper['controltestlist'] = event.detail.selRecords;
    }

    selectedAreaUnitRecords(event){
        this.serviceDeskWrapper['arearequirmentlist'] = event.detail.selRecords;
    }


    // handleCancel(){
    //     if(this.template.querySelector("c-karl_custom_lookup_cmp") != null)
    //     this.template.querySelector("c-karl_custom_lookup_cmp").deselectRecordParent();
    //     this.clearData();
    // }

    // clearData(){
    //     this.serviceDeskWrapper = {};
    //     this.serviceDeskWrapper.newOrExisitng = 'New';
    // }

   

    handleNext(event){
        if(!this.isValid()){
            return;
        }
        if(this.serviceDeskWrapper['areaOfCompliance'] == undefined || this.serviceDeskWrapper['areaOfCompliance'] == null || this.serviceDeskWrapper['areaOfCompliance'] == ''){
            this.serviceDeskWrapper['areaOfCompliance'] = [];
        }
        if(this.serviceDeskWrapper['controltestlist'] == undefined || this.serviceDeskWrapper['controltestlist'] == null || this.serviceDeskWrapper['controltestlist'] == ''){
            this.serviceDeskWrapper['controltestlist'] = [];
        }
        if(this.serviceDeskWrapper['arearequirmentlist'] == undefined || this.serviceDeskWrapper['arearequirmentlist'] == null || this.serviceDeskWrapper['arearequirmentlist'] == ''){
            this.serviceDeskWrapper['arearequirmentlist'] = [];
        }
        createServiceDeskRecord({
            serviceDeskData : JSON.stringify(this.serviceDeskWrapper),
            buttonLabel : event.target.label
        })
        .then(result=>{
            this.handleNextNavigation(result);
        }) 
        .catch(error => {
            var message = error.message || error.body.message;
            // const evt = new ShowToastEvent({
            //                 title: 'Submission Failed, Please contact your administrator'+message,               
            //                 variant: 'error',
            //                 mode: 'dismissable'
            //             });
            //             this.dispatchEvent(evt);
            this.handleNextNavigation(message);
        })
    }

    handleNextNavigation(message){
        const attributeChangeEvent = new FlowAttributeChangeEvent('ServiceDeskCreated', message);
        this.dispatchEvent(attributeChangeEvent); 
        const nextNavigationEvent = new FlowNavigationNextEvent();
        this.dispatchEvent(nextNavigationEvent); 
    }
    
    handleBackNavigation(){
        const backNavigationEvent = new FlowNavigationBackEvent();
        this.dispatchEvent(backNavigationEvent); 
    }

    handlePrevious(){
        this.isShowPreviousModal = true;
    }

    handlePreviousModalYesClick(){
        this.serviceDeskWrapper = [];
        this.handleBackNavigation();
    }
    handlePreviousModalNoClick(){
        this.isShowPreviousModal = false;
    }
    isValid() {
        var isValid = false;
        let isAllValid = [...this.template.querySelectorAll('lightning-input')].reduce((validSoFar, input) => {
            input.reportValidity();
            return validSoFar && input.checkValidity();
        }, true);  
        isAllValid &= [...this.template.querySelectorAll('lightning-combobox')].reduce((validSoFar, input) => {
            input.reportValidity();
            return validSoFar && input.checkValidity();
        }, true); 
        isAllValid &= [...this.template.querySelectorAll('lightning-radio-group ')].reduce((validSoFar, input) => {
            input.reportValidity();
            return validSoFar && input.checkValidity();
        }, true);  
        isAllValid &= [...this.template.querySelectorAll('lightning-textarea')].reduce((validSoFar, input) => {
            input.reportValidity();
            return validSoFar && input.checkValidity();
        }, true);
        if(isAllValid === 1){
            isValid = true;
        }    
        return isValid;
    }
}
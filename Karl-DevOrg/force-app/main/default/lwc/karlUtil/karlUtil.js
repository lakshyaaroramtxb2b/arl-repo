/**
 * @author Ben Harvie
 * @email ben.harvie@salesforce.com
 * @description Utility class for KARL Datatables
 */

import {
    ShowToastEvent
} from 'lightning/platformShowToastEvent';
/**
 * Reduces one or more LDS errors into a string[] of error messages.
 * @param {FetchResponse|FetchResponse[]} errors
 * @return {String[]} Error messages
 */
export function reduceErrors(errors) {
    if (!Array.isArray(errors)) {
        errors = [errors];
    }

    return (
        errors
        // Remove null/undefined items
        .filter(error => !!error)
        // Extract an error message
        .map(error => {
            // UI API read errors
            if (Array.isArray(error.body)) {
                return error.body.map(e => e.message);
            }
            // UI API DML, Apex and network errors
            else if (error.body && typeof error.body.message === 'string') {
                return error.body.message;
            }
            // JS errors
            else if (typeof error.message === 'string') {
                return error.message;
            }
            // Unknown error shape so try HTTP status text
            return error.statusText;
        })
        // Flatten
        .reduce((prev, curr) => prev.concat(curr), [])
        // Remove empty strings
        .filter(message => !!message)
    );


}

/**
 * Display a success toast message
 * @param {LightningElement} component component displaying the message
 * @param {String} message message string
 */
export function showSuccessMessage(component, message) {
    showMessage(component, {
        title: "Success",
        message: message,
        messageType: 'success',
        mode: 'dismissable'
    });
}

/**
 * Display an info toast message
 * @param {LightningElement} component component displaying the message
 * @param {String} message message string
 */
export function showInfoMessage(component, message) {
    showMessage(component, {
        title: "Info",
        message: message,
        messageType: 'info',
        mode: 'dismissable'
    });
}

/**
 * Display a Warning toast message
 * @param {LightningElement} component component displaying the message
 * @param {String} message message string
 */
export function showWarningMessage(component, message) {
    showMessage(component, {
        title: "Warning",
        message: message,
        messageType: 'warning',
        mode: 'pester'
    });
}

/**
 * Display an error toast message
 * @param {LightningElement} component component displaying the message
 * @param {String} message message string
 */
export function showErrorMessage(component, message) {
    showMessage(component, {
        title: "Error",
        message: message,
        messageType: 'error',
        mode: 'pester'
    });
}

/**
 * Display an ajax type error message. takes in generic Exception object as input
 * @param {LightningElement} component component displaying the message
 * @param {Error} message message string
 */
export function showAjaxErrorMessage(component, error) {
    showMessage(component, {
        title: "Error",
        message: (error) ? ((error.message) ? error.message : ((error.body) ? ((error.body.message) ? error.body.message : JSON.stringify(error)) : JSON.stringify(error))) : "Something went wrong!",
        messageType: 'error',
        mode: 'pester'
    });
}

/**
 * Display a toast message
 * 
 * @param {*} component firing component
 * @param {*} params message display options
 */
export function showMessage(component, {
    title,
    message,
    messageType,
    mode
}) {

    component.dispatchEvent(new ShowToastEvent({
        mode,
        title,
        message,
        variant: messageType,
    }));
}

/**
 * @author Ben Harvie
 * @email ben.harvie@salesforce.com
 * @description return Audit Scope picklist options using async/await and imperative apex
 */
/* Apex Imports */
import getAuditCycleScopes from '@salesforce/apex/KARL_Utility.getAuditCycleScopes';
export async function scopeOptions(){
    let auditScopeOptions = [];
    let auditScopes = [];

    await getAuditCycleScopes()
        .then((result) => {
            auditScopes = result;
            auditScopeOptions.push({label:'All', value:'allScopes'});
            auditScopeOptions.push({label:'--------------------------------------------------', value:'noScopes'});
            for(var scope in result){
                auditScopeOptions.push({label:scope, value:auditScopes[scope]});
            }
        })
        .catch((error) => {
            let errorCatch = error;
            auditScopeOptions.push({label:'Error', value:'Error'});
        })

    return auditScopeOptions;

};

/**
 * @author Ben Harvie
 * @email ben.harvie@salesforce.com
 * @description return Control Family picklist options using async/await and imperative apex
 */
/* Apex Imports */
import getControlFamilies from '@salesforce/apex/KARL_Utility.getControlFamilies';
export async function controlFamilyOptions(){
    let controlFamilyOptions = [];
    let controlFamilies = [];

    await getControlFamilies()
        .then((result) => {
            controlFamilies = result;
            controlFamilyOptions.push({label:'All', value:'allFamilies'});
            controlFamilyOptions.push({label:'--------------------------------------------------', value:'noFamilies'});
            for(var family in result){
                controlFamilyOptions.push({label:family, value:controlFamilies[family]});
            }
        })
        .catch((error) => {
            let errorCatch = error;
            controlFamilyOptions.push({label:'Error', value:'Error'});
        })

    return controlFamilyOptions;

};

/**
 * @author Ben Harvie
 * @email ben.harvie@salesforce.com
 * @description return Audit Scope picklist options using async/await and imperative apex
 */
/* Apex Imports */
import getAuditCycles from '@salesforce/apex/KARL_Utility.getAuditCycles';
export async function cycleOptions(){
    let auditCycleOptions = [];
    let auditCycles = [];

    await getAuditCycles()
        .then((result) => {
            auditCycles = result;
            auditCycles.forEach(field => {
                auditCycleOptions.push({label:field.Name, value:field.Id});
            });
        })
        .catch((error) => {
            let errorCatch = error;
            auditCycleOptions.push({label:'Error', value:'Error'});
        })

    return auditCycleOptions;

};

/**
 * @author Ben Harvie
 * @email ben.harvie@salesforce.com
 * @description return Audit Scope picklist options using async/await and imperative apex
 */
/* Apex Imports */
import getAuditTeams from '@salesforce/apex/KARL_Utility.getAuditTeams';
export async function teamOptions(){
    let auditTeamOptions = [];
    let auditTeams = [];

    await getAuditTeams()
        .then((result) => {
            auditTeams = result;
            auditTeams.forEach(field => {
                auditTeamOptions.push({label:field.Name, value:field.Id});
            });
        })
        .catch((error) => {
            let errorCatch = error;
            auditTeamOptions.push({label:'Error', value:'Error'});
        })

    return auditTeamOptions;

};

/**
 * @author Ben Harvie <ben.harvie@salesforce.com>
 * @description Retrieve the URL parameters from the currentPageReference. Note we use this
 * instead of searchParams in accordance with Salesforce guidance.
 * @param getParam - the value from the URL
 * @param defaultvalue - default value (set in metadata or code)
 * @param curPageReference - currentPageReference (from wire data function)
 * @return variable based on URL parameter or default
 */
 export function getDefaultURLValue(getParam, defaultValue, curPageReference){
    // Use the currentPageReference to get the requested
    const urlParam = curPageReference.state[getParam];

    if(urlParam != null || urlParam != undefined){
        if (urlParam.indexOf(',') > -1) { 
            return urlParam.split(',');
        }else{
            return urlParam;
        }
    }else{
        // Nothing passed via the URL, so we'll set default value from metadata (if exists)
        return defaultValue;
    }
};

/**
 * @author Ben Harvie <ben.harvie@salesforce.com>
 * @description Set the spinner (visual UI) when something is loading
 * @param {Array} checkVars - array to be checked for validity
 * @return boolean based on checkVars
 */
export function setLoadingSpinner(checkVars){
    let checkFlag = false;
    for(var i=0;i<checkVars.length;i++){
        if(checkVars[i] != undefined && checkVars[i] != null){
            checkFlag = true;
        }
    }
    return checkFlag;
};


/**
 * @author Ben Harvie <ben.harvie@salesforce.com>
 * @description remove an item from an array
 * @return array with item removed
 */
 export function arrayRemove(arr, value){
    return arr.filter(function(element){ 
        return element != value; 
    });
 }
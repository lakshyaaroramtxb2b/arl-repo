import {
    LightningElement,track,
    api
} from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import createServiceDeskEvidenceRecord from '@salesforce/apex/KARL_ServiceDeskController.createServiceDeskEvidenceRecord';
import instructionlink from '@salesforce/label/c.KARL_Evidence_Instructions';

export default class Karl_request_access_modal extends LightningElement {
    @api confirmationMessage = '';
    @api isDisabled = false;
    @api selectedRequestItem;
    @track serviceDeskWrapper ={};
    @track instructionlinkhref = instructionlink;
    @track showSpinner = false;
    connectedCallback(){
        this.serviceDeskWrapper['evidenceRequestId'] = this.selectedRequestItem.value;
        if(this.selectedRequestItem.cyclearlitemid != '' && this.selectedRequestItem.cyclearlitemid != null && this.selectedRequestItem.cyclearlitemid != undefined)
        this.serviceDeskWrapper['cycleArlItemId'] = this.selectedRequestItem.cyclearlitemid;
    }

    handleChange(event){
        this.serviceDeskWrapper[event.target.name] = event.target.value;
    }

    closeModal() {
        this.clearServiceDeskWrapper();
        this.dispatchEvent(new CustomEvent('close'));
    }

    handleYes() {   
        console.log('stringify = ',JSON.stringify(this.serviceDeskWrapper));
        this.showSpinner = true;
        if(!this.isValid()){
            this.showSpinner = false;
            return;
        }
        createServiceDeskEvidenceRecord( 
            {
                serviceDeskData : JSON.stringify(this.serviceDeskWrapper)
            }
        )
        .then( response =>{
            this.showSpinner = false;
            const evt = new ShowToastEvent({
                title: 'Service Desk Record Created Successfully',               
                variant: 'success',
                mode: 'dismissable'
            });
            this.dispatchEvent(evt);
            this.clearServiceDeskWrapper();
            this.dispatchEvent(new CustomEvent('yes'));
        })
        .catch(error=>{
            let errorMessage = error.message || error.body.message;
            this.showSpinner = false;
            const evt = new ShowToastEvent({
                title: 'Submission Failed, Please contact your administrator'+errorMessage,               
                variant: 'error',
                mode: 'dismissable'
            });
            this.dispatchEvent(evt);
        })
    }

    handleinstructionClick(){
        console.log('Download Instructions');
    }

    clearServiceDeskWrapper(){
        this.serviceDeskWrapper = {};
    }

    isValid() {
        var isValid = false;
        let isAllValid = [...this.template.querySelectorAll('lightning-combobox')].reduce((validSoFar, input) => {
            input.reportValidity();
            return validSoFar && input.checkValidity();
        }, true);  
        isAllValid &= [...this.template.querySelectorAll('lightning-radio-group ')].reduce((validSoFar, input) => {
            input.reportValidity();
            return validSoFar && input.checkValidity();
        }, true);  
        isAllValid &= [...this.template.querySelectorAll('lightning-textarea')].reduce((validSoFar, input) => {
            input.reportValidity();
            return validSoFar && input.checkValidity();
        }, true);
        isAllValid &= [...this.template.querySelectorAll('lightning-input')].reduce((validSoFar, input) => {
            input.reportValidity();
            return validSoFar && input.checkValidity();
        }, true);
    
        if(isAllValid === 1){
            isValid = true;
        }    
        return isValid;
    }
}
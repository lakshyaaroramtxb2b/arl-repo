import { NavigationMixin } from 'lightning/navigation';
import { LightningElement, api, wire, track } from 'lwc';
import { deleteRecord } from 'lightning/uiRecordApi';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { refreshApex } from '@salesforce/apex';
import ARL_Is_External_Auditor from '@salesforce/customPermission/ARL_Is_External_Auditor';

/**
 * @author Ben Harvie
 * @email ben.harvie@salesforce.com
 * @description This LWC Component Builds Requirements Card for REQ, CREQ and ARI objects.
 */

// Import Apex Classes
import getRequirementsMapping from '@salesforce/apex/KARL_Utility.getRequirementsMapping';
import getParentREQ from '@salesforce/apex/KARL_Utility.getParentREQ';
export default class Karl_mapping_reqs extends NavigationMixin(LightningElement) {

    @api objectApiName;
    @api recordId;
    @track parentId;
	@track records;
	@track error;
    @track showDeleteModal = false;
    @track showCreateModal = false;
    @track showUpdateModal = false;
    @track deleteRecordId;
    @track requirementsData = [];
    @track requirementsWireData;
    @track recordTarget;

    deleteRecordConfirmationMsg = 'Are you sure you want to delete the requirement mapping?';

    /** 
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Wire the Requirements Mapping
     */
	@wire(getRequirementsMapping, {objectApi: '$objectApiName', recordId: '$recordId'}) wiredRequirements(result) {
        this.requirementsWireData = result;

        console.log("Requirements Mapping Data feed updating!");

		if ( result.data && result.data.length ){
			// ! Check if the wire returns any data
			// Error handle the response just in case there is an issue returning the data
			try {
				this.records = true; // specify boolean for if:true
				this.requirementsData = result.data; // establish the data as requirementsData
				this.error = false;
			} catch(e) {
				console.log('KARL LWC Error', result.error)
			}
		} else if ( result.data && !result.data.length ){
			// ! Check if the wire is null
			this.records = false; // specify boolean for if:false
            console.log('KARL LWC Empty', result.data)
		} else if (result.error){
			// ! Check if the wire returns an error, and return the error
			console.log('KARL LWC Error', result.error)
			this.error = result.error;
			this.records = false;
		}
    }

    /** 
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Wire the Parent Record Id (for Object context)
     */
	@wire(getParentREQ, {objectApi: '$objectApiName', recordId: '$recordId'}) wiredParent(parentReq) {
        this.parentId = parentReq.data;
    }

    /** 
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description New junction object record on Button Click.
     */
    openCreateModal(event) {
        // Process information to the Modal
        this.showCreateModal = true;
    }

    closeCreateModal() {
        this.showCreateModal = false;
    }

    handleCreateSubmit(event){
        this.showCreateModal = false;

        refreshApex(this.requirementsWireData);

        this.dispatchEvent(
            new ShowToastEvent({
                title: 'Success',
                message: 'Requirement mapped successfully.',
                variant: 'success'
            })
        )
    }

    /** 
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Standard handleRecordView component for KARL LWC's to navigate to Record Pages
     */
	handleRecordView(event) {
		// Get the Target Record ID from the event (typically onClick event)
		const targetRecordId = event.detail;
		// Navigate to the record page
		this[NavigationMixin.Navigate]({
			type: 'standard__recordPage',
			attributes: {
				recordId: targetRecordId,
				actionName: 'view',
			},
		});
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Open/Close modals for popup handling
     */
    openUpdateModal(event) {
        // Process information to the Modal
        this.recordTarget = event.detail;
        this.showUpdateModal = true;
    }

    closeUpdateModal() {
        this.showUpdateModal = false;
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Handle submission of the requirements mapping form (modal)
     */
    handleUpdateSubmit(event){
        this.showUpdateModal = false;

        refreshApex(this.requirementsWireData);

        this.dispatchEvent(
            new ShowToastEvent({
                title: 'Success',
                message: 'Requirement mapping updated successfully.',
                variant: 'success'
            })
        )
    }

    /** 
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Edit junction object record on Button Click.
     */
	handleRecordEdit(event) {
		// Get the Target Record ID from the event (typically onClick event)
		const targetRecordId = event.detail;
		// Navigate to the record page
		this[NavigationMixin.Navigate]({
			type: 'standard__recordPage',
			attributes: {
                recordId: targetRecordId,
                objectApiName: 'Request_Item_Area__c',
				actionName: 'edit',
			},
		});
    }

    /** 
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Delete modals (popups)
     */
    openDeleteModal(event) {
        // Process information to the Modal
        this.deleteRecordId = event.detail;
        this.showDeleteModal = true;

        // Debug Log
        console.log("Record to delete: " + this.deleteRecordId);
    }

    closeDeleteModal() {
        this.showDeleteModal = false;
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Handle deletion of the record from the picklist
     */
    deleteRecord(event) {      
        deleteRecord(this.deleteRecordId)
            .then(() => {
                this.showDeleteModal = false;

                refreshApex(this.requirementsWireData);

                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Success',
                        message: 'Record deleted',
                        variant: 'success'
                    })
                )
            })
            .catch(error => {
                this.showDeleteModal = false;
                this.dispatchEvent(
                    new ShowToastEvent({
                        title: 'Error deleting record',
                        message: error.body.message,
                        variant: 'error'
                    })
                )
            })
    }

    /** 
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Return true if the user has the Custom Permission for ARL_Is_External_Auditor
     * @return Returns undefined if not set or true if set
     */
    get isExternalAuditor(){
        //console.log(' Is external auditor? >> ' + ARL_Is_External_Auditor);
        return ARL_Is_External_Auditor;
    }
}
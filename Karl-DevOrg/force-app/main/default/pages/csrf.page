<apex:page showHeader="false" sidebar="false" standardStylesheets="false">
<head>
<c:CorpHeadSection ></c:CorpHeadSection>
</head>
<body class="" id="wrapper">
           <div class="" id="pagewrap"> 
<c:SDLLiteBody ></c:SDLLiteBody>
            
            <div id="main">
            
   
   
   
   
   <div class="gs"> 
<div id="centerwrap">
<div id="container">
<div id="contentcontainer"> 

<span class="corner_nw"></span>
</div>                
<div id="content4r"> <a id="top" name="top"></a>
<h1>&nbsp;</h1>
<div border="1" bordercolor="black">

<!--start-->
<h2 style="font-size: 175%">Cross Site Request Forgery (CSRF)</h2><br>





<h2>What Is It?</h2>

<p>Web browsers allow GET and POST requests to be made between different web
sites. Cross-site request forgery (CSRF) occurs when a user visits a
malicious web page that makes their browser send requests to your
application that the user did not intend. This can be done with the
<var>src</var> attribute of the IMG, IFRAME or other tags and more
complicated requests, including POSTs, can be made using JavaScript. Because
the browser always sends the relevant cookies when making requests, requests
like this appear to originate from an authenticated user. The malicious site
isn&rsquo;t able to see the results of these requests, but if create, update
or delete functionality can be triggered, the malicious site may be able to
perform unauthorized actions.</p>

<h2>Sample Vulnerability</h2>

<p>Consider a hypotehtical contact management application at
https://example.com/.  Without CSRF protection, if a user visits a malicious
website while still logged in to example.com, the following HTML in the
malicious site&rsquo;s page can cause all of their contacts will be
deleted.</p>

<pre>&#60;iframe src="https://example.com/addressBook?action=deleteAll&amp;confirm=yes"&#62;</pre>

<h2>Is My Application Vulnerable?</h2>

<p>All web applications are vulnerable to CSRF by default. Unless you have
specifically engineered protections or are automatically protected by your
framework, your application is probably vulnerable. <strong>Applications
built on the Apex and Visualforce platforms are protected by
default.</strong> Anti-CSRF protections are available for most major
application platforms but are often not enabled by default.</p>

<h2>How Can I Test My Application?</h2>

<p>If you are not using a framework that provides CSRF protection, or
don&rsquo;t know, the best way to test your applications is to use a proxy like
<a href="http://portswigger.net/suite/">Burp</a> or <a
href="http://www.fiddler2.com/fiddler2/">Fiddler</a> to manually examine the
form data sent to your application.</p>

<p>Do actions that create, update or delete data or cause side-effects have
completely predictable parameters? To be protected from CSRF, forms targeting
these actions should include an un-guessable parameter. Removing or changing
this parameter should cause the form submission to fail.</p>

<p><img alt="Submission vulnerable to CSRF" src="{!URLFOR($Resource.securecoding, 'burp_00.PNG')}" width="604"
height="521" /></p>

<p>This screen shot of the Burp proxy shows an example of a request
vulnerable to CSRF. All of the parameters to the application
(<var>action</var>, <var>confirm</var>) are predictable.</p>

<p>The next screen shot has a parameter named <var>antiForgery</var> that
looks hard to guess:</p>

<p><img alt="Request with antiForgery parameter" src="{!URLFOR($Resource.securecoding, 'burp_01.PNG')}" width="604"
height="521" /></p>

<p>But if we log out as &ldquo;bob&rdquo; and log back in as
&ldquo;charlie&rdquo;, we see that the antiForgery value stays the same.
</p>

<p><img alt="Duplicate antiForgery value" src="{!URLFOR($Resource.securecoding, 'burp_02.PNG')}" width="604"
height="521" /></p>

<p>This application global anti-forgery token could be observed by one user and
used to attack other users. </p>

<p>A secure anti-CSRF mechanism should create a different and unpredictable
token <strong>for each user session</strong> &mdash; Bob should get a
different <var>antiforgery</var> value each time he logs in, and so should
Charlie. Be sure to use the capabilities of the proxy to test that actions
fail if the token is removed or changed, or a valid token for another user
is substituted.</p>

<p>Additional information on testing for CSRF can be found at: <a
href="http://www.owasp.org/index.php/Testing_for_CSRF_(OWASP-SM-005)">http://www.owasp.org/index.php/Testing_for_CSRF_(OWASP-SM-005)</a></p>


<h2>How Do I Protect My Application?</h2>

<ul>

<li><a href="#L1174">Apex and Visualforce Applications</a>

<li><a href="#L2342">General Guidance</a>

<li><a href="#L2438">ASP.NET</a>

<li><a href="#L2446">Java</a>

<li><a id="PHP" href="#L2454">PHP</a>

<li><a href="#L2463">Ruby on Rails</a>

</ul>


<p></p>
<hr />

<h3><a id="L1174"></a>Apex and Visualforce Applications</h3>

<p>Within the Force.com platform, we have implemented an anti-CSRF token to
prevent this attack. Every form includes a parameter containing a random
string of characters as a hidden field. When the user submits the form, the
platform checks the validity of this string and will not execute the command
unless the given value matches the expected value. This feature will protect
you when using all of the standard controllers and methods &mdash; but only
for POST requests. GET requests are not protected with the anti-forgery
token.</p>

<p>(By protecting only POST requests, Salesforce.com follows the convention
and recommendation that GET requests be <em>safe</em> in that they do not
alter significant data on the server side or have other significant
side-effects. That is, you should not be using GET requests to change
application state, and so GET requests should not need CSRF protection. See
section 9.1 of <a href="http://www.ietf.org/rfc/rfc2616.txt">RFC 2616, HTTP
1.1</a> for more discussion of this distinction.)</p>

<p>By making a non-safe action formulation with a GET request, the developer
might bypass our built-in defenses without realizing the risk. For example,
let&rsquo;s say you have a custom controller where you take the object ID as
an input parameter, then use that input parameter in your own SOQL call, and
then delete the resulting object (a non-safe, state-changing action).
Consider the following code snippet:</p>

<pre>
&#60;apex:page controller="myClass" action="{&#33;init}"&#62;&#60;/apex:page&#62;

public class myClass {
    public void init() {
        Id id = ApexPages.currentPage().getParameters().get('id');
        Account obj = [select id, Name FROM Account WHERE id = :id];
        delete obj;
        return ;
    }
}</pre>

<p>In this case, the developer has unknowingly bypassed the anti-CSRF
controls by writing code that changes state on a GET request. The
<var>id</var> parameter is read and used in the code. The anti-CSRF token is
never read or validated. An attacker web page may have sent the user to this
page via CSRF attack and could have provided any value they wish for the
<var>id</var> parameter.</p>

<p>Instead, use an <code>&#60;apex:form&#62;</code> block, as described <a
href="http://www.salesforce.com/us/developer/docs/pages/Content/pages_controller_custom.htm">in
the developer documentation</a>. Note how it is safe to use
<var>getParameters</var> to select the account, but that the <code>update
account;</code> statement &mdash; a non-safe state change &mdash; is done in
an <code>&#60;apex:form&#62;</code> action. The form is POSTed and tha
anti-forgery token validated implicitly.</p>



<pre>
public class MyController {

    private final Account account;

    public MyController() {
        account = [select id, name, site from Account
            where id =:ApexPages.currentPage().getParameters().get('id')];
    }

    public Account getAccount() {
        return account;
    }

    public PageReference save() {
        update account;
        return null;
    }
}

&#60;apex:page controller="myController" tabStyle="Account"&#62;
   &#60;apex:form&#62;
      &#60;apex:pageBlock title="Congratulations {&#33;$User.FirstName}"&#62;
         You belong to the Account Name: &#60;apex:inputField value="{&#33;account.name}"/&#62;
         &#60;apex:commandButton action="{&#33;save}" value="save"/&#62;
      &#60;/apex:pageBlock&#62;
   &#60;/apex:form&#62;
&#60;/apex:page&#62;
</pre>

<h3><a id="L2342"></a>General Guidance</h3>

<p>All requests that create, update or delete data or have side-effects
require protection against CSRF.</p>

<p>The most reliable method is to include an anti-CSRF token as a hidden
input with every application action. This token should be included in all
forms built by the genuine application and validated to be present and
correct before form data is accepted and acted upon. </p>

<p>Use the POST method for requests requiring protection to avoid disclosing
the token value in Referer headers.</p>

<p>Token values must be unique <em>per user session</em> and
unpredictable.</p>

<p><a>For more information and traditional defenses, see the following
articles:</a></p>

<ul>
  <li><a
    href="http://www.owasp.org/index.php/Cross-Site_Request_Forgery">http://www.owasp.org/index.php/Cross-Site_Request_Forgery</a></li>
  <li><a
    href="http://www.cgisecurity.com/csrf-faq.html">http://www.cgisecurity.com/csrf-faq.html</a></li>
  <li><a
    href="https://www.isecpartners.com/documents/XSRF_Paper.pdf">https://www.isecpartners.com/documents/XSRF_Paper.pdf</a></li>
</ul>
<br>
<h3><a id="L2438"></a>ASP.NET</h3>

<p>ASP.NET provides two strategies for preventing CSRF. Applications that use
the ASP.NET ViewState mechanism can protect against CSRF by setting a
<var>ViewStateUserKey</var> during <var>Page_Init</var>.</p>

<p>See the following articles at MSDN for more information on using
<var>ViewStateUserKey</var>:</p>

<ul>
  <li><a
    href="http://msdn.microsoft.com/en-us/library/system.web.ui.page.viewstateuserkey.aspx">http://msdn.microsoft.com/en-us/library/system.web.ui.page.viewstateuserkey.aspx</a></li>
  <li><a
    href="http://msdn.microsoft.com/en-us/library/ms972969.aspx">http://msdn.microsoft.com/en-us/library/ms972969.aspx</a></li>
</ul>

<p>Be sure to set the value to a per-user, unique and unpredictable
value.</p>

<p>ASP.NET applications that do not use the ViewState mechanism can use the
<var>AntiForgeryToken</var> feature from the <var>System.Web.Mvc</var>
package. See the following MSDN documentation:</p>

<ul>
  <li><a
    href="http://msdn.microsoft.com/en-us/library/system.web.mvc.htmlhelper.antiforgerytoken.aspx">http://msdn.microsoft.com/en-us/library/system.web.mvc.htmlhelper.antiforgerytoken.aspx</a></li>
</ul>

<p>If using the <var>AntiForgeryToken</var>, it must be added to and
validated for every sensitive action in the application. Again, a
&ldquo;sensitive&rdquo; action for these purposes is one that changes
server-side state, like creating, updating, or deleting data.</p>

<h3><a id="L2446"></a>Java</h3>

<p>Several libraries are available for protecting Java applications from
CSRF. The <strong>HDIV</strong> (HTTP Data Integrity Validator)
framework&rsquo;s Anti-Cross Site Request Forgery Token feature can be
easily integrated into <strong>Struts 1.x</strong>, <strong>Struts
2.x</strong>, <strong>Spring MVC</strong> and <strong>JSTL</strong>
applications. The Spring Webflow system includes a unique identifier with
each request, but this identifier is not sufficiently random to provide CSRF
protection, so use of HDIV is recommended.  Download it at: </p>

<ul>
  <li><a href="http://hdiv.org/">http://hdiv.org/</a></li>
</ul>

<p>The <strong>OWASP CSRFGuard Project</strong> also provides an anti-CSRF
token mechanism implemented as a filter and set of JSP tags applicable to a
wide range of J2EE applications. Download it at:</p>

<ul>
  <li><a
    href="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project">http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project</a></li>
</ul>
<br>
<h3><a id="L2454"></a>PHP</h3>

<p>General guidance on CSRF and PHP sample code demonstrating the
vulnerability and countermeasures can be found at:</p>

<ul>
  <li><a
    href="http://shiflett.org/articles/cross-site-request-forgeries">http://shiflett.org/articles/cross-site-request-forgeries</a></li>
</ul>

<p>The &ldquo;csrf-magic&rdquo; library can provide anti-CSRF tokens for
many PHP applications. Download it at:</p>

<ul>
  <li><a
  href="http://csrf.htmlpurifier.org/">http://csrf.htmlpurifier.org/</a></li>
</ul>
<br>
<h3><a id="L2463"></a>Ruby on Rails</h3>

<p>Ruby on Rails 2.0 provides a <var>protect_from_forgery</var> feature.
<em>This implementation does not meet Salesforce.com's requirements for CSRF
protection if used with the :secret option </em><em>because the token value
will be the same for all users</em>. See <a href="#L2342">General
Guidance</a>, above, for anti-CSRF token requirements.  Use of the
<var>protect_from_forgery</var> feature without the <var>:secret</var>
option with Ruby on Rails 3.3 and above creates a random token that meets
Salesforce.com security requirements. See the documentation for <a
href="http://api.rubyonrails.org/classes/ActionController/RequestForgeryProtection/ClassMethods.html"><var>ActionController</var><tt>::</tt><var>RequestForgeryProtection</var></a>
for more information.</p>







<!--end-->
            </div>
            <div class="clear"></div>
          </div>
                </div>
                <div class="spacer"></div>
            </div>
        </div>
                
   
            </div>

<!--new div </div>-->
                
                <span id="endofcontent"><!--**************--></span>
                
                
                                                    
            <div style="text-align:center">
            <c:CorpFooter ></c:CorpFooter>
            </div>
    </div>
    </body>  
  

</apex:page>
<apex:page sidebar="false" showHeader="false" controller="SecurityAssesmentControllerOLD">
  <head>
    <style type="text/css">
        ul.social_logins {
          list-style: none;
        }

        ul[role="alert"] {
            /* page messages are on here twice */
            display: none;
        }

        h7 { 
            border-bottom: solid 1px;
         }
    </style>
  </head>
  <apex:composition template="Security_Customers">
    <apex:define name="customers_content">
        <apex:stylesheet value="{!URLFOR($Resource.SecurityStatics,'libs/bootstrap-datetimepicker/css/datetimepicker.css')}" />
        <apex:stylesheet value="{!URLFOR($Resource.SecurityAssessments_SocialButtons,'styles.css')}" />
        <apex:includeScript value="{!URLFOR($Resource.SecurityStatics,'libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js')}" />
        <apex:includeScript value="{!URLFOR($Resource.moment_js,'')}" />
       <apex:outputPanel id="id_content" styleClass="span9" layout="block">

        <script>
          // These need to be here and not in a rerender so they'll actually get loaded
          window.fbAsyncInit = function() {
            FB.init({
              appId      : '272735413096813',
              xfbml      : true,
              version    : 'v2.6'
            });
          };

          (function(d, s, id){
             var js, fjs = d.getElementsByTagName(s)[0];
             if (d.getElementById(id)) {return;}
             js = d.createElement(s); js.id = id;
             js.src = "//connect.facebook.net/en_US/sdk.js";
             fjs.parentNode.insertBefore(js, fjs);
           }(document, 'script', 'facebook-jssdk'));

          function handle_fb_login(response) {
            if (response.status === 'connected') {
                do_fb_login(response.authResponse.accessToken);
            } else if (response.status === 'not_authorized') {
                // The person is logged into Facebook, but not your app.
            } else {
                // The person is not logged into Facebook, so we're not sure if
                // they are logged into this app or not.
            }
          }

            function handle_linkedin_login() {
                do_linkedin_login(IN.ENV.auth.oauth_token);
            }

            function googleLogin(googleUser) {
                 do_google_login(googleUser.getAuthResponse().id_token);
             }

            function initGoogle() {
                 gapi.load('auth2', function() {
                    gapi.auth2.init({
                       client_id: '{!gclientId}',
                       fetch_basic_profile: true
                    }).then(function(GoogleAuth) {
                        window.GoogleAuth=GoogleAuth;
                    });
                  });
             }

            // VF's output format apparently isn't compatible with its input format
             function fix_dates() {

                    var start_el = $('input[id$="id_start_time"]');
                    if ((start_el.length >0) && (start_el.val() != null)) {
                        start_el.val(moment.utc(start_el.val()).format('MM/DD/YYYY hh:mm A'));
                    }
                    var end_el = $('input[id$="id_end_time"]');
                    if ((end_el.length >0) && (end_el.val() != null)) {
                        end_el.val(moment.utc(end_el.val()).format('MM/DD/YYYY hh:mm A'));
                    }
             }
             
             function alertForHeroku(selectedValue) {
               if(selectedValue == 'Heroku_Services') {
                alert('Redirecting to Heroku Services..');
               }
             }
             
             function validateIpFormat(fieldId) {
                var ipformat = /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;  
                var inputField = document.getElementById(fieldId);
                var inputText = inputField.value;
                if(!inputText) return true;
                if(inputText.match(ipformat)){  
                  return true;  
                }  
                else {  
                  alert("You have entered an invalid IP address!");  
                  inputField.focus();
                  return false;  
               }  
             }
            
        </script>
         <script type="text/javascript" src="//platform.linkedin.com/in.js">
            api_key:   7849ods3fkq57v
            lang:      en_US
         </script>

        <script src="https://apis.google.com/js/platform.js?onload=initGoogle" ></script>

        <apex:form style="margin:0 0 50px" id="fullform">

        <fieldset>
          <legend>Scheduling a Security Assessment of Salesforce Environment</legend>
          <apex:pageMessages />
          <apex:outputPanel id="authorizeSection" rendered="{!show_authorize}">
              <section>

                <apex:actionFunction action="{!handleFBLogin}" name="do_fb_login" rerender="id_content">
                    <apex:param name="social_token" assignTo="{!social_token}" value="" />
                </apex:actionFunction>
                <apex:actionFunction action="{!handleLinkedInLogin}" name="do_linkedin_login" rerender="id_content">
                    <apex:param name="social_token" assignTo="{!social_token}" value="" />
                </apex:actionFunction>
                <apex:actionFunction action="{!handleGoogleLogin}" name="do_google_login" rerender="id_content">
                    <apex:param name="social_token" assignTo="{!social_token}" value="" />
                </apex:actionFunction>

                <script>
                    $(document).ready(function() {
                      if (!{!showSFDC}) {
                          $('#social_sfdc').hide();
                      }
                      if (!{!showFB}) {
                          $('#social_fb').hide();
                      }
                      if (!{!showLI}) {
                          $('#social_linkedin').hide();
                      }
                      if (!{!showGoogle}) {
                          $('#social_google').hide();
                      }
                    });
                </script>

                <div id="social_preload"></div>
                <ul id="social_logins">
                  <li id="social_sfdc" onclick="window.open('{!OauthAuthorizeURL}','_self')">
                     
                  </li>
                  <li id="social_fb" onclick="FB.login(handle_fb_login, {scope: 'public_profile,email'});">
                     
                  </li>
                  <li id="social_linkedin" onclick="IN.User.authorize(function(){ handle_linkedin_login(); })">
                     
                  </li>
                  <li id="social_google" onclick="window.GoogleAuth.signIn(new gapi.auth2.SigninOptionsBuilder()).then(googleLogin);">
                     
                  </li>
                </ul>

              </section>
         </apex:outputPanel>
         <apex:outputPanel id="saaWrapper"> <!-- you can't render the immediate parent apparently -->
             <apex:outputPanel id="saaSection" rendered="{!show_saa}">
                 <section >
                    <apex:actionRegion >
                        <h4>
                            Read and Accept our <a href="/servlet/servlet.FileDownload?file={!saaDocId}" target="_blank">Security Assessment Agreement </a>

                            <apex:inputcheckbox value="{!SAA_Accepted}">
                                <apex:actionSupport event="onchange" action="{!toggleSaa}" reRender="saaWrapper,pentestInfo" status="saa_accepted_status"/>
                            </apex:inputcheckbox>
                        </h4>
                        <div>
                        <p />
                        In the event a valid written Security Assessment Agreement (SAA) exists between Customer and Salesforce with respect to the same subject matter contemplated herein,
                        the terms and conditions of such existing SAA shall supersede the SAA terms and conditions electronically accepted by Customer herein.
                        </div>
                        <apex:actionStatus startText=" refreshing..." id="saa_accepted_status"/>
                    </apex:actionRegion>
                  </section>
             </apex:outputPanel>
         </apex:outputPanel>

          <apex:outputPanel id="targetChooser" rendered="{!show_env}">
              <apex:actionFunction action="{!setTargetEnv}" name="set_target_env" rerender="id_content">
                  <apex:param name="target_environment" assignTo="{!target_environment}" value="" />
              </apex:actionFunction>

              <div class="span4">
                   <apex:outputLabel value="Please select the environment to test" for="target_environment"/>
                   <apex:selectList onchange="alertForHeroku($(this).val()); set_target_env($(this).val());" size="1" value="{!target_environment}" id="target_environment" required="true" disabled="{!(record != null) && (record_editable==False)}">
                      <apex:selectOptions value="{!environments}"/>
                  </apex:selectList>
               </div>
          </apex:outputPanel>

          <apex:outputPanel id="pentestInfo" >
            <apex:outputPanel rendered="{!show_form}">
                    <section>

                        <legend >Contact Information</legend>
                         <div class="row">
                           <div class="span4">
                              <apex:outputLabel value="Member Id (AccountId)" for="member_id" rendered="{!isMarketingCloud}"/>
                              <apex:inputText value="{!member_id}" id="member_id" required="true" disabled="{!(record != null) && (record_editable==False)}" rendered="{!isMarketingCloud}"/>
                           </div>
                           <div class="span4">
                              <apex:outputLabel value="{!IF(isMarketingCloud ,'Account Name','Company Name')}" for="company_name" />
                              <apex:inputText value="{!company_name}" id="company_name" required="true" disabled="{!(record != null) && (record_editable==False)}" />
                           </div>
                           <div class="span4">
                              <apex:outputLabel value="Company Production Org Id" for="company_org_id" rendered="{!NOT(isMarketingCloud)}"/>
                              <apex:outputText value="{!company_org_id}" id="company_org_id" rendered="{!NOT(isMarketingCloud)}"/>
                           </div>
                         </div>

                        <h6>Your Contact Info</h6>
                        <div class="row">
                            <div class="span4">
                                <apex:outputLabel value="Name" for="pm_name"/>
                                <apex:outputText value="{!pm_name}" id="pm_name"/>
                            </div>
                             <div class="span4">
                                <apex:outputLabel value="Work Phone" for="pm_workphone"/>
                                <apex:inputText value="{!pm_workphone}" id="pm_workphone" required="true" disabled="{!(record != null) && (record_editable==False)}"/>
                                
                                
                            </div>
                        </div>
                        <div class="row">
                            <div class="span4">
                                <apex:outputLabel value="Email" for="pm_email"/>
                                <!-- linkedin doesn't always provide email -->
                                <apex:outputText value="{!pm_email}" id="pm_email" rendered="{!(pm_email != null) && (pm_email != '')}"/>
                                <apex:inputText value="{!pm_email}" id="pm_email2" rendered="{!(pm_email == null) || (pm_email == '')}" disabled="{!(record != null) && (record_editable==False)}"/>
                            </div>
                             <div class="span4">
                                <apex:outputLabel value="Cell Phone" for="pm_cellphone"/>
                                <apex:inputText value="{!pm_cellphone}" id="pm_cellphone" required="true" disabled="{!(record != null) && (record_editable==False)}"/>
                            </div>
                        </div> <br/>
                        <h6>Additional Email addresses where the confirmation email will be sent</h6>
                        <hr style="border-width: 2px; margin: 5px;" />
                        <div class="row">
                            <div class="span4">
                                <apex:outputLabel value="#1 Additional Email address" for="additional_Email1"/>
                                <apex:inputText value="{!additional_Email1}" id="additional_Email1" disabled="{!(record != null) && (record_editable==False)}"/>
                            </div>
                             <div class="span4">
                                <apex:outputLabel value="#2 Additional Email address" for="additional_Email2"/>
                                <apex:inputText value="{!additional_Email2}" id="additional_Email2" disabled="{!(record != null) && (record_editable==False)}"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="span4">
                                <apex:outputLabel value="#3 Additional Email address" for="additional_Email4"/>
                                <apex:inputText value="{!additional_Email3}" id="additional_Email3" disabled="{!(record != null) && (record_editable==False)}"/>
                            </div>
                             <div class="span4">
                                <apex:outputLabel value="#4 Additional Email address" for="additional_Email5"/>
                                <apex:inputText value="{!additional_Email4}" id="additional_Email4" disabled="{!(record != null) && (record_editable==False)}"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="span4">
                                <apex:outputLabel value="#5 Additional Email address" for="additional_Email5"/>
                                <apex:inputText value="{!additional_Email5}" id="additional_Email5" disabled="{!(record != null) && (record_editable==False)}"/>
                            </div>
                             <div class="span4">
                               
                            </div>
                        </div>
                        <br/>
                        <h6>Primary Testing Point of Contact</h6>
                        <hr style="border-width: 2px; margin: 5px;" />
                        <div class="row">
                            <div class="span4">
                                <apex:outputLabel value="Name" for="tester_name"/>
                                <apex:inputText value="{!tester_name}" id="tester_name" required="true" disabled="{!(record != null) && (record_editable==False)}"/>
                            </div>
                             <div class="span4">
                                <apex:outputLabel value="Work Phone" for="tester_workphone"/>
                                <apex:inputText value="{!tester_workphone}" id="tester_workphone" required="true" disabled="{!(record != null) && (record_editable==False)}"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="span4">
                                <apex:outputLabel value="Email" for="tester_email"/>
                                <apex:inputText value="{!tester_email}" id="tester_email" required="true" disabled="{!(record != null) && (record_editable==False)}"/>
                            </div>
                             <div class="span4">
                                <apex:outputLabel value="Cell Phone" for="tester_cellphone"/>
                                <apex:inputText value="{!tester_cellphone}" id="tester_cellphone" required="true" disabled="{!(record != null) && (record_editable==False)}"/>
                            </div>
                        </div>


                    </section>
                    <br/>

                  <apex:outputPanel rendered="{!mapSubEnvironments != null}">
                  <section>
                      <legend>Environment Information</legend>

                          <apex:repeat value="{!mapSubEnvironments}" var="dirKey">
                             <div class="row">
                                 <div class="span4">
                                     <apex:outputLabel value="{!dirKey}" />
                                 </div>
                                 <div class="span4">
                                     <apex:inputCheckbox value="{!mapSubEnvironments[dirKey]}"/>
                                 </div>
                             </div>
                          </apex:repeat>
                  </section>
                  </apex:outputPanel>
                  <section>

                    <legend >Testing Information</legend>
                      <div class="row">

                           <div class="span8">
                               <apex:outputLabel value="Detailed Overview of Tests to be Performed" for="overview_tests"/>
                              <apex:inputTextArea value="{!overview_tests}" id="overview_tests" styleClass="span8" required="true" disabled="{!(record != null) && (record_editable==False)}"/>
                           </div>

                      </div>

                      <div class="row">

                           <div class="span4">
                              <apex:outputLabel value="Start Date/Time (GMT)" for="id_start_time"/>
                               <div class="input-append date form_datetime" data-date="{!now}">
                                  <apex:inputText value="{!start_time}" id="id_start_time" required="true" disabled="{!(record != null) && (record_editable==False)}"/>
                                  <span class="add-on app-datepicker-icon"><i class="icon-th"></i></span>
                              </div>
                           </div>

                           <div class="span4">
                              <apex:outputLabel value="End Date/Time (GMT)" for="id_end_time"/>
                              <div class="input-append date form_datetime" data-date="{!now}">
                                <apex:inputText value="{!end_time}" id="id_end_time" required="true" disabled="{!(record != null) && (record_editable==False)}"/>
                                <span class="add-on app-datepicker-icon"><i class="icon-th"></i></span>
                              </div>
                           </div>
                         </div>

                        <div class="row">

                           <div class="span4">
                              <apex:outputLabel value="Test Environment" for="test_environment" rendered="{!isMarketingCloud}"/>
                              <apex:selectList value="{!test_environment}" size="1" required="true" rendered="{!isMarketingCloud}">
                                  <apex:selectOptions value="{!TestEnvironments}"/>
                              </apex:selectList>
                           </div>
                       </div>

                       <div class="row">

                           <div class="span4">
                               <apex:outputLabel value="{!IF(isMarketingCloud ,'Salesforce Member IDs ( Account IDs) where the Security Assessment is performed','Salesforce ORG IDs where the Security Assessment is performed')}" for="testing_orgs"/>
                              <apex:inputText value="{!testing_orgs}" id="testing_orgs" disabled="{!(record != null) && (record_editable==False)}"/>
                           </div>

                           <div class="span4">
                               <apex:outputLabel value="{!IF(isMarketingCloud ,'MC Usernames to be Used During the Test','Usernames to be Used During the Test')}" for="testing_users"/>
                              <apex:inputText value="{!testing_users}" id="testing_users" required="true" disabled="{!(record != null) && (record_editable==False)}"/>
                           </div>
                         </div>

                       <div class="row">

                           <div class="span4">
                               <apex:outputLabel value="Anticipated Server Load" for="server_load"/>
                              <apex:selectList value="{!server_load}" id="server_load" required="true" size="1" disabled="{!(record != null) && (record_editable==False)}">
                                 <apex:selectOptions value="{!server_load_entries}"/>
                              </apex:selectList>
                           </div>

                           <div class="span4">
                               <apex:outputLabel value="Tool(s) to be Used" for="tools_used"/>
                              <apex:inputText value="{!tools_used}" id="tools_used" required="true" disabled="{!(record != null) && (record_editable==False)}"/>
                           </div>
                         </div>

                      <div class="row">
                          <!-- <div class="span4">
                               <apex:outputLabel value="Public Source IP(s)" for="source_ip"/>
                              <apex:inputText value="{!source_ip}" id="source_ip" required="true" disabled="{!(record != null) && (record_editable==False)}"/>
                           </div> -->

                           <div class="span4">
                               <apex:outputLabel value="Environment to test" for="target_environment2"/>
                               <apex:inputText disabled="true" id="target_environment2" value="{!target_environment}"/>
                           </div>

                      </div>
                      <legend >Public IP Ranges</legend>
                      <div class="row">
                           <div class="span4">
                               <apex:outputLabel value="Public IP Address Start Range #1" for="publicIpStartRange1"/>
                              <apex:inputText value="{!publicIpStartRange1}" id="publicIpStartRange1" required="true" maxlength="15" onchange="validateIpFormat('{!$Component.publicIpStartRange1}')"/>
                           </div>

                           <div class="span4">
                               <apex:outputLabel value="Public IP Address End Range #1" for="publicIpEndRange1"/>
                               <apex:inputText id="publicIpEndRange1" value="{!publicIpEndRange1}" required="true" maxlength="15" onchange="validateIpFormat('{!$Component.publicIpEndRange1}')"/>
                           </div>
                      </div>
                      <div class="row">
                           <div class="span4">
                               <apex:outputLabel value="Public IP Address Start Range #2" for="publicIpStartRange2"/>
                              <apex:inputText value="{!publicIpStartRange2}" id="publicIpStartRange2" maxlength="15" onchange="validateIpFormat('{!$Component.publicIpStartRange2}')"/>
                           </div>

                           <div class="span4">
                               <apex:outputLabel value="Public IP Address End Range #2" for="publicIpEndRange2"/>
                               <apex:inputText id="publicIpEndRange2" value="{!publicIpEndRange2}" maxlength="15" onchange="validateIpFormat('{!$Component.publicIpEndRange2}')"/>
                           </div>
                      </div>
                      <div class="row">
                           <div class="span4">
                               <apex:outputLabel value="Public IP Address Start Range #3" for="publicIpStartRange3"/>
                              <apex:inputText value="{!publicIpStartRange3}" id="publicIpStartRange3" maxlength="15" onchange="validateIpFormat('{!$Component.publicIpStartRange3}')"/>
                           </div>

                           <div class="span4">
                               <apex:outputLabel value="Public IP Address End Range #3" for="publicIpEndRange3"/>
                               <apex:inputText id="publicIpEndRange3" value="{!publicIpEndRange3}" maxlength="15" onchange="validateIpFormat('{!$Component.publicIpEndRange3}')"/>
                           </div>
                      </div>
                      <div class="row">
                           <div class="span4">
                               <apex:outputLabel value="Public IP Address Start Range #4" for="publicIpStartRange4"/>
                              <apex:inputText value="{!publicIpStartRange4}" id="publicIpStartRange4" maxlength="15" onchange="validateIpFormat('{!$Component.publicIpStartRange4}')"/>
                           </div>

                           <div class="span4">
                               <apex:outputLabel value="Public IP Address End Range #4" for="publicIpEndRange4"/>
                               <apex:inputText id="publicIpEndRange4" value="{!publicIpEndRange4}" maxlength="15" onchange="validateIpFormat('{!$Component.publicIpEndRange4}')"/>
                           </div>
                      </div>
                      <div class="row">
                           <div class="span4">
                              <apex:outputLabel value="Public IP Address Start Range #5" for="publicIpStartRange5"/>
                              <apex:inputText value="{!publicIpStartRange5}" id="publicIpStartRange5" maxlength="15" onchange="validateIpFormat('{!$Component.publicIpStartRange5}')"/>
                           </div>

                           <div class="span4">
                               <apex:outputLabel value="Public IP Address End Range #5" for="publicIpEndRange5"/>
                               <apex:inputText id="publicIpEndRange5" value="{!publicIpEndRange5}" maxlength="15" onchange="validateIpFormat('{!$Component.publicIpEndRange5}')"/>
                           </div>
                      </div>
                      <div class="row">
                           <div class="span8">
                               <apex:outputLabel value="Additional Target Information (IP/Host)" for="additional_target_info"/>
                               <apex:inputTextArea value="{!additional_target_info}" id="additional_target_info" styleClass="span8" required="{!requires_additional_target_info}" disabled="{!(record != null) && (record_editable==False)}"/>
                           </div>
                      </div>

                      <script type="text/javascript">

                          $(".form_datetime").datetimepicker({
                              format: "mm/dd/yyyy HH:ii P",
                              showMeridian: true,
                              autoclose: true,
                              todayBtn: true
                          });

                       </script>
                  </section>
              <apex:commandButton action="{!save}" rendered="{!record == null}" value="Register" styleClass="btn-primary" disabled="{!(record != null) && (record_editable==False)}" rerender="fullform" oncomplete="fix_dates();"/>
              <apex:commandButton action="{!save}" rendered="{!record != null}" value="Update" styleClass="btn-primary" disabled="{!(record != null) && (record_editable==False)}" rerender="fullform" oncomplete="fix_dates();"/>
              </apex:outputPanel>
        </apex:outputPanel>
        </fieldset>

        </apex:form>
      </apex:outputPanel>



    </apex:define>

  </apex:composition>
</apex:page>
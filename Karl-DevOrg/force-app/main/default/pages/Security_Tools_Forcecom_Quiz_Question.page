<apex:page sidebar="false" showHeader="false" standardStylesheets="false" controller="SecToolsQuizQuestionController" action="{!init}">
 <apex:composition template="Security_Tools_Forcecom">
   <apex:define name="forcecom_tool_content">
     <script type="text/javascript">
       function show_example(menuEl,selector) {
         $('.code_example').hide();
         $(selector).show()
         
         $('.code_example_menu').removeClass('active');
         $(menuEl).closest('li').addClass('active');
       }
       
       function show_code_choice(menuEl,selector) {
         $('.code_choice').hide();
         $(selector).show();
         
         $('.code_choice_menu').removeClass('active');
         $(menuEl).closest('li').addClass('active');
       }
       
       function submit_multi() {
            var selected = '';
            $('#id_multiselect input:checked').each(function () {
               selected += ','+$(this).val();
              });
            if (selected != '') {
              // extra ,
              selected = selected.substring(1);
            }
            $('#id_submit_button').button('loading');
            submit_answer(selected);
      }
      
      function submit_single() {
            $('#id_submit_button').button('loading');
            submit_answer($('#id_singleselect input:checked').val());
      }
      
      function submit_code_choice() {
            $('#id_submit_button').button('loading');
            submit_answer($('#id_code_choice_inputs input:checked').val());
      }
     </script>
     
     <apex:pageBlock rendered="{!NOT(ISNULL(qId))}">
       <ul style="IF(AND(ISNULL(vf_code),ISNULL(apex_code),ISNULL(scontrol_code))'display:none;','')}" class="nav nav-tabs">
           <li style="{!IF(ISNULL(vf_code),'display:none;','')}" class="code_example_menu {!IF(NOT(ISNULL(vf_code)),'active','')}"><a href="#" onclick="show_example(this,'#id_vf_code')">VisualForce</a></li>
           <li style="{!IF(ISNULL(apex_code),'display:none;','')}" class="code_example_menu {!IF(AND(ISNULL(vf_code),NOT(ISNULL(apex_code))),'active','')}"><a href="#" onclick="show_example(this,'#id_apex_code')">Apex</a></li>
           <li style="{!IF(ISNULL(scontrol_code),'display:none;','')}" class="code_example_menu {!IF(AND(ISNULL(vf_code),ISNULL(apex_code)),'active','')}"><a href="#" onclick="show_example(this,'#scontrol_code')">S-Control</a></li>
       </ul>
     </apex:pageBlock>
     
     <apex:pageBlock rendered="{!OR(NOT(ISNULL(vf_code)),NOT(ISNULL(apex_code)),NOT(ISNULL(scontrol_code)))}">
      <apex:pageBlock rendered="{!NOT(ISNULL(vf_code))}">
       <pre id="id_vf_code" class="code_example prettyprint linenums">
{!SUBSTITUTE(vf_code,'#REMOVE#','')}
       </pre>
      </apex:pageBlock>
    
      <apex:pageBlock rendered="{!NOT(ISNULL(apex_code))}">
       <pre id="id_apex_code" class="code_example prettyprint linenums" style="{!IF(ISNULL(vf_code),'','display:none;')}">
{!SUBSTITUTE(apex_code,'#REMOVE#','')}
       </pre>
      </apex:pageBlock>

      <apex:pageBlock rendered="{!NOT(ISNULL(scontrol_code))}">
       <pre id="id_scontrol_code" class="code_example prettyprint linenums" style="{!IF(AND(ISNULL(vf_code),ISNULL(apex_code)),'','display:none;')}">
{!SUBSTITUTE(scontrol_code,'#REMOVE#','')}
       </pre>
      </apex:pageBlock>
     </apex:pageBlock>
     
  <apex:form >
    <apex:actionFunction name="submit_answer" action="{!submitAnswer}" oncomplete="">
      <apex:param assignTo="{!userAnswerString}" value="{!userAnswerString}" name="userAnswerString" />
    </apex:actionFunction>
  </apex:form>

  <apex:pageBlock id="answerBlock"  rendered="{!NOT(ISNULL(qId))}" mode="edit">
    <form>
      <fieldset>
        <apex:pageBlock rendered="{!qtype='MultiSelect'}">
          <div class="control-group">
            <p class="help-block">({!answeredCount+1} of {!totalQuestionCount}) Please select all vulnerability classes present in the example. Do not select any classes if example is securely written.</p>
            <div class="controls" id="id_multiselect">
                <apex:repeat var="opt" value="{!answerOptions}">
                  <label class="checkbox">
                    <input type="checkbox" name="optionsCheckboxList1" value="{!opt.value}">{!opt.label}</input>
                  </label>
                </apex:repeat>
            </div>
           </div>
           <script type="text/javascript">
             $(document).ready(function () {
               $('#id_submit_button').click(submit_multi);
             });
          </script>
               
                  
        </apex:pageBlock>
        
        <apex:pageBlock rendered="{!qtype='SingleSelect'}">
          <div class="control-group">
            <p class="help-block">({!answeredCount+1} of {!totalQuestionCount}) Please select the vulnerability class present in the example.  Do not select any class if example is securely written.</p>
            <div class="controls" id="id_singleselect">
              <apex:repeat var="opt" value="{!answerOptions}">
                <label class="radio">
                  <input type="radio" name="optionsRadios" id="" value="{!opt.value}">{!opt.label}</input>
                </label>
              </apex:repeat>
            </div>
          </div>
          
          <script type="text/javascript">
             $(document).ready(function () {
               $('#id_submit_button').click(submit_single);
             });
          </script>  
        </apex:pageBlock>
        
      <apex:pageBlock rendered="{!qtype='PickCorrectExample'}">
          <ul class="nav nav-tabs">
            <li class="code_choice_menu active"><a href="#" onclick="show_code_choice(this,'#id_code_choice_1')">1</a></li>
            <li class="code_choice_menu"><a href="#" onclick="show_code_choice(this,'#id_code_choice_2')">2</a></li>
            <li class="code_choice_menu"><a href="#" onclick="show_code_choice(this,'#id_code_choice_3')">3</a></li>
            <li class="code_choice_menu"><a href="#" onclick="show_code_choice(this,'#id_code_choice_4')">4</a></li>
          </ul>
          
          <pre id="id_code_choice_1" class="code_choice prettyprint linenums">{!SUBSTITUTE(example1,'#REMOVE#','')}</pre>
          <pre id="id_code_choice_2" class="code_choice prettyprint linenums" style="display:none;">{!SUBSTITUTE(example2,'#REMOVE#','')}</pre>
          <pre id="id_code_choice_3" class="code_choice prettyprint linenums" style="display:none;">{!SUBSTITUTE(example3,'#REMOVE#','')}</pre>
          <pre id="id_code_choice_4" class="code_choice prettyprint linenums" style="display:none;">{!SUBSTITUTE(example4,'#REMOVE#','')}</pre>

          <div class="control-group">
            <p class="help-block">({!answeredCount+1} of {!totalQuestionCount}) Please select the correctly secured version of the below example</p>
            <div class="controls" id="id_code_choice_inputs">
              <apex:repeat var="opt" value="{!fixMeOpts}">
                <label class="radio">
                  <input type="radio" name="optionsRadios" id="" value="{!opt.value}" />{!opt.label}
                </label>
              </apex:repeat>
            </div>
          </div>
          
          <script type="text/javascript">
             $(document).ready(function () {
               $('#id_submit_button').click(submit_code_choice);
             });
          </script>
      </apex:pageBlock>
      <div class="form-actions">
          <button class="btn btn-primary" type="button" id="id_submit_button" data-type="button" data-loading-text="Submitting..." name="Submit">Submit</button>
      </div>
      </fieldset>
    </form>
  </apex:pageBlock>
  <br /><br />
  
     <script type="text/javascript">
       $(document).ready(function() { 
           
           $('#menu_forcecom_quiz').addClass('active');
           
           prettyPrint();
 
       });  
     </script>
     
   </apex:define>
 </apex:composition>
</apex:page>
/**
 * @author Lakshya Arora
 * @email lakshya.arora@mtxb2b.com
 * @description This is helper class for KARL_AuditScopeTriggerHandler
 * @note As we have external sharing model as private for Audit Scope the trigger should have accessed to all records for maintaining the 1-1 relationship,
 * that's why inherited sharing is used as trigger runs in system mode
*/
public inherited sharing class KARL_AuditScopeTriggerHelper{
    public static string activeauditscope = 'An Active Audit Scope having same KARL Scope Name is already present';
   /**
    * @author Lakshya Arora
    * @email lakshya.arora@mtxb2b.com
    * @description Ensure that only 1 active audit scope per KARL Scope
    * @note Call on Before insert,Before Update
    * @param New List of KARL_Audit_Scope_Master_Data__c
    */
    public static void uniqueAuditScopeValidation(List<KARL_Audit_Scope_Master_Data__c> newList,Map<Id,KARL_Audit_Scope_Master_Data__c> oldMap){
        Set<String> karlScopeSet = new Set<String>();
        for(KARL_Audit_Scope_Master_Data__c auditScopeObj : newList){
            if(auditScopeObj.Active__c 
                && (oldMap == null || oldMap.get(auditScopeObj.Id).KARL_Scope__c != auditScopeObj.KARL_Scope__c || oldMap.get(auditScopeObj.Id).Active__c != auditScopeObj.Active__c)){
                    karlScopeSet.add(auditScopeObj.KARL_Scope__c);
            }
        }
        if(!karlScopeSet.isEmpty()){
            Set<String> existingActiveScopeIdSet = new Set<String>();
            for(KARL_Audit_Scope_Master_Data__c auditScopeObj : [SELECT Id,KARL_Scope__c 
                                                                FROM KARL_Audit_Scope_Master_Data__c
                                                                WHERE KARL_Scope__c IN :karlScopeSet
                                                                AND Active__c = TRUE
                                                                WITH SECURITY_ENFORCED]){
                existingActiveScopeIdSet.add(auditScopeObj.KARL_Scope__c);
            }
            Set<String> duplicateActiveScopeIdSet = new Set<String>();
            for(KARL_Audit_Scope_Master_Data__c auditScopeObj : newList){
                if(auditScopeObj.Active__c 
                    && (oldMap == null || oldMap.get(auditScopeObj.Id).KARL_Scope__c != auditScopeObj.KARL_Scope__c || oldMap.get(auditScopeObj.Id).Active__c != auditScopeObj.Active__c)){
                        if(existingActiveScopeIdSet.contains(auditScopeObj.KARL_Scope__c) || duplicateActiveScopeIdSet.contains(auditScopeObj.KARL_Scope__c)){
                            auditScopeObj.addError(activeauditscope);
                        }
                        else{
                            if(!duplicateActiveScopeIdSet.contains(auditScopeObj.KARL_Scope__c)){
                                duplicateActiveScopeIdSet.add(auditScopeObj.KARL_Scope__c);
                            }
                        }
                }
            }
        }
    }
}
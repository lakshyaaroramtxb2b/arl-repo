global class SC_SyncTrailheadScheduler implements Schedulable {

    global void execute(SchedulableContext sc) {

      SC_Training_Provider__c provider = SC_TrainingUtil.providerFor('Trailhead');
      SC_BatchCoursesCompletedSync b2 = new SC_BatchCoursesCompletedSync(provider);
      database.executebatch(b2, 1);
       
      // kill current job
      // system.abortJob(sc.getTriggerId());

   }
    
   global static void run(Integer mins) {

      schedule(mins);

   }
    
   private static void schedule(Integer mins) {
 
      datetime nextScheduleTime = system.now().addMinutes(mins);
      string minute = string.valueof(nextScheduleTime.minute());
      string second = string.valueof(nextScheduleTime.second ());
      string cronvalue = second+' '+minute+' * * * ?' ;
      string jobName = 'SC Trailhead Completion Sync ' + nextScheduleTime.format('hh:mm');     
      SC_SyncTrailheadScheduler p = new SC_SyncTrailheadScheduler();   
      system.schedule(jobName, cronvalue , p);  

   }
}
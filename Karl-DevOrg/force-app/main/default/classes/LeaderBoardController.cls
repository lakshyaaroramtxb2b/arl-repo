public class LeaderBoardController {
    
	public List<LeaderDCUser> userRecords {get; set;}
    public List<LeaderDCIncident> incidentRecords {get; set;}
    public List<AggregateResult> alertRecords {get; set;}
    
    public LeaderBoardController() { 
        
        userRecords = new List<LeaderDCUser>();
        incidentRecords = new List<LeaderDCIncident>();
        alertRecords = new List<AggregateResult>();
        
        List<User> groupUsers = [SELECT Id, Name 
                                 FROM User 
                                 WHERE Id IN (SELECT UserOrGroupId FROM GroupMember WHERE GroupId = '00G30000004bLPP')];
        
        for(User user : groupUsers) {
            
            //get leader statistics
            
            LeaderDCUser newUser = new LeaderDCUser();
            
            newUser.id = user.id;
            newUser.name = user.Name;
            newUser.incidentsCreated = GetIncidentsCreatedByUser(user);
            newUser.alertsCreated = GetAlertsCreatedByUser(user);
            
            newUser.eventsCreated = GetEventsCreatedByUser(user);
            
            if(newUser.incidentsCreated != 0)
            	newUser.effectiveRate = ((newUser.incidentsCreated / newUser.alertsCreated) * 100.0).setScale(2) + '%';
            else
            	newUser.effectiveRate = '';
            
            userRecords.add(newUser);
            
            //get incidents
            
            //LeaderDCIncident
            
            List<Case> cases = [SELECT Subject, createdDate, Detection_Alert__r.DetectionAlertCriteria__r.Name, status 
                                FROM Case 
                                WHERE CreatedDate = LAST_N_DAYS:0 AND IRCloud__Sub_Status__c = 'Malicious (Incident)' AND Detection_Alert__c != null AND Detection_Alert__r.DetectionAlertCriteria__r.CreatedById =: user.Id];
        
            for(Case c : cases) {
                
                LeaderDCIncident incident = new LeaderDCIncident();
                
                incident.subject = c.Subject;
                incident.id = c.Id;
                incident.csirtCaseId = c.id;
                incident.createdBy = user.Name;
                incident.status = c.status;
                incident.createdDate = c.createdDate;
                incident.alertCriteria = c.Detection_Alert__r.DetectionAlertCriteria__r.Name;
                incident.alertCriteriaName = c.Detection_Alert__r.DetectionAlertCriteria__r.Id;
                
                incidentRecords.add(incident);
            }
        }
        
        userRecords.sort();
        
        //Detection_Alert_Criteria__c
        
        alertRecords = [SELECT Count(DetectionAlertCriteria__c) total, DetectionAlertCriteria__r.Id alertCriteriaId, DetectionAlertCriteria__r.Name, DetectionAlertCriteria__r.createdBy.Name fullName
                        FROM Detection_Alert__c 
                        WHERE CreatedDate = LAST_N_DAYS:0
                        	AND DetectionAlertCriteria__r.CaseStructure__c = 'CSIRTAlert' 
                        	AND DetectionAlertCriteria__r.AlertStatus__c = 'Live'
                        GROUP BY DetectionAlertCriteria__r.Name, DetectionAlertCriteria__r.createdBy.Name, DetectionAlertCriteria__r.Id];
    } 
    
    public void getIncidents(User user) {
    	
    }
    
    public Integer GetEventsCreatedByUser(User user) {
        
        Integer c = [SELECT Count() 
					FROM Detection_Security_Event_Criteria__c 
					WHERE createdById =: user.Id];

    	return c;
    }
    
    public Integer GetIncidentsCreatedByUser(User user) {
        
        Integer c = [SELECT Count() 
                     FROM Case 
                     WHERE CreatedDate = LAST_N_DAYS:365
                      AND IRCloud__Sub_Status__c = 'Malicious (Incident)' 
                      AND Detection_Alert__c != null 
                      AND Detection_Alert__r.DetectionAlertCriteria__r.CreatedById =: user.Id];

    	return c;
    }
    
    public Integer GetAlertsCreatedByUser(User user) {
        
        Integer c = [SELECT Count() 
                     FROM Case 
                     WHERE CreatedDate = LAST_N_DAYS:365 
                      AND Detection_Alert__c != null 
                      AND Detection_Alert__r.DetectionAlertCriteria__r.CreatedById =: user.Id];

    	return c;
    }
}
/**
* @author Lakshya Arora
* @email lakshya.arora@mtxb2b.com
* @desc This is schedulable and stateful batch class to Generate Requests
*/
public with sharing class ARL_CreateCycleReqItemControllerBatch implements Database.Batchable<sObject>,Schedulable{
    public Database.QueryLocator start(Database.BatchableContext bc){       
        String query = 'SELECT Id, (SELECT Id, Area__c, Audit_Cycle__c,KARL_Audit_Team__c, Scope__c FROM Audit_Cycle_Coverages__r)'
                        +' FROM Audit_Cycle__c WHERE Active__c = TRUE WITH SECURITY_ENFORCED';
        return Database.getQueryLocator(query);
    }

    /**
    * @author Lakshya Arora
    * @email lakshya.arora@mtxb2b.com
    * @desc This method is for schedulable interface to execute.
    */
    public void execute(Database.BatchableContext bc, List<Audit_Cycle__c> auditCycleList) {
            Set<Id> auditCycleIdSet = new Set<Id>();
            for(Audit_Cycle__c auditCycleObj : auditCycleList){
                auditCycleIdSet.add(auditCycleObj.Id);
            }
            //Variables
            Set<String> areaOfCompliances = new Set<String>();
            Set<String> scopes = new Set<String>();
            List<Audit_Cycle_Coverage_Item__c> instAuditCycleCoverageItems = new List<Audit_Cycle_Coverage_Item__c>();
            Map<String, List<Audit_Cycle_Coverage_Item__c >> auditCycleCovItem = new Map<String, List<Audit_Cycle_Coverage_Item__c >>();
            Map<String, Cycle_Request_Item__c> cycleReqItems = new Map<String, Cycle_Request_Item__c>();
            Map<String, Cycle_Request_Item__c> exCycleReqItems = new Map<String, Cycle_Request_Item__c>();
            // Auditor Request Items
            Map<String, List<KARL_Auditor_Request_Item__c >> requestIdToAuditorReqItemsMap = new Map<String, List<KARL_Auditor_Request_Item__c >>();
            List<KARL_Auditor_Request_Item__c> instAuditorReqItemList = new List<KARL_Auditor_Request_Item__c>();
            // For Building Exclusion Lists
            Set<Id> existingCycleReqItemIdSet = new Set<Id>();
            Map<String, Audit_Cycle_Coverage_Item__c > exCycleAcci = new Map<String, Audit_Cycle_Coverage_Item__c >();
            Map<String, KARL_Auditor_Request_Item__c > exCycleAri = new Map<String, KARL_Auditor_Request_Item__c >();
            // Maps for building exclusion lists 
            // Map<Id,List<Audit_Cycle_Coverage_Item__c>> cycleReqItemIdAuditCycCovItemsMap = new Map<Id,List<Audit_Cycle_Coverage_Item__c>>();
            // Map<Id,List<KARL_Auditor_Request_Item__c>> auditorReqItemMap = new Map<Id,List<KARL_Auditor_Request_Item__c>>();
            Map<Id,Map<Id,List<Audit_Cycle_Coverage_Item__c>>> cycleReqItemIdAuditCycCovItemsMap = new Map<Id,Map<Id,List<Audit_Cycle_Coverage_Item__c>>>();
            Map<Id,Map<Id,List<KARL_Auditor_Request_Item__c>>> auditorReqItemMap = new Map<Id,Map<Id,List<KARL_Auditor_Request_Item__c>>>();
            Map<Id,Map<String, List<Audit_Cycle_Coverage__c>>> areaScopeAndAuditCycleCoverages = new Map<Id,Map<String, List<Audit_Cycle_Coverage__c>>>();

            List<Cycle_Request_Item__c> existingCycleReqItems = new List<Cycle_Request_Item__c>();
            Map<Id,List<Cycle_Request_Item__c>> auditCycleIdToExistingCRIMap = new Map<Id,List<Cycle_Request_Item__c>>();
            for(Cycle_Request_Item__c cycleReqItemObj : [SELECT id, Request__c, Audit_Cycle_Coverage__c,Audit_Cycle__c 
                                                         FROM Cycle_Request_Item__c 
                                                         WHERE Audit_Cycle__c IN : auditCycleIdSet 
                                                         WITH SECURITY_ENFORCED]){
                existingCycleReqItemIdSet.add(cycleReqItemObj.Id);
                if(!auditCycleIdToExistingCRIMap.containsKey(cycleReqItemObj.Audit_Cycle__c)){
                    auditCycleIdToExistingCRIMap.put(cycleReqItemObj.Audit_Cycle__c, new List<Cycle_Request_Item__c>());
                }
                auditCycleIdToExistingCRIMap.get(cycleReqItemObj.Audit_Cycle__c).add(cycleReqItemObj);
            }
    
            
            //Fetch all Child Audit Cycle Coverages
            for(Audit_Cycle_Coverage_Item__c exAuditCycleCovItem : [SELECT Id, Audit_Cycle_Coverage__c, Cycle_Request_Item__c,Audit_Cycle_Id__c 
                                                                    FROM Audit_Cycle_Coverage_Item__c 
                                                                    WHERE Cycle_Request_Item__c IN :existingCycleReqItemIdSet]){
                if(!cycleReqItemIdAuditCycCovItemsMap.containsKey(exAuditCycleCovItem.Audit_Cycle_Id__c)){
                    cycleReqItemIdAuditCycCovItemsMap.put(exAuditCycleCovItem.Audit_Cycle_Id__c, new Map<Id,List<Audit_Cycle_Coverage_Item__c>>());
                }
                if(!cycleReqItemIdAuditCycCovItemsMap.get(exAuditCycleCovItem.Audit_Cycle_Id__c).containsKey(exAuditCycleCovItem.Cycle_Request_Item__c)){
                    cycleReqItemIdAuditCycCovItemsMap.get(exAuditCycleCovItem.Audit_Cycle_Id__c).put(exAuditCycleCovItem.Cycle_Request_Item__c,new List<Audit_Cycle_Coverage_Item__c>());
                }
                cycleReqItemIdAuditCycCovItemsMap.get(exAuditCycleCovItem.Audit_Cycle_Id__c).get(exAuditCycleCovItem.Cycle_Request_Item__c).add(exAuditCycleCovItem);
            }

            
            //Fetch All Child Auditor Request Items
            for(KARL_Auditor_Request_Item__c exAuditReqItem : [SELECT Id, KARL_Audit_Team__c, KARL_Cycle_Request_Item__c,Audit_Cycle_Id__c 
                                                                    FROM KARL_Auditor_Request_Item__c 
                                                                    WHERE KARL_Cycle_Request_Item__c IN :existingCycleReqItemIdSet]){
                if(!auditorReqItemMap.containsKey(exAuditReqItem.Audit_Cycle_Id__c)){
                    auditorReqItemMap.put(exAuditReqItem.Audit_Cycle_Id__c, new Map<Id,List<KARL_Auditor_Request_Item__c>>());
                }
                if(!auditorReqItemMap.get(exAuditReqItem.Audit_Cycle_Id__c).containsKey(exAuditReqItem.KARL_Cycle_Request_Item__c)){
                    auditorReqItemMap.get(exAuditReqItem.Audit_Cycle_Id__c).put(exAuditReqItem.KARL_Cycle_Request_Item__c,new List<KARL_Auditor_Request_Item__c>());
                }
                auditorReqItemMap.get(exAuditReqItem.Audit_Cycle_Id__c).get(exAuditReqItem.KARL_Cycle_Request_Item__c).add(exAuditReqItem);                                                                    
            }
    
            for(Id auditCycleId : auditCycleIdToExistingCRIMap.keySet()){
                List<Cycle_Request_Item__c> existingCycleReqList = new List<Cycle_Request_Item__c>();
                existingCycleReqList.addAll(auditCycleIdToExistingCRIMap.get(auditCycleId));
                //Exclude Cycle Requests that are already created
                if( !existingCycleReqList.isEmpty() ){
                    // Build list of existing Cycle Requests Audit Cycle, so we can continually click "Generate Requests" to add in new requests.
                    for(Cycle_Request_Item__c exId:existingCycleReqList){
                        
                        // Get list of Audit Cycle Coverage Items that already exist for the CREQ
                        // Author: Ben Harvie
                        // GUS: W-7613465
                        List<Audit_Cycle_Coverage_Item__c> exAuditCycleCovItem =  new List<Audit_Cycle_Coverage_Item__c>();
                        if(cycleReqItemIdAuditCycCovItemsMap.containsKey(auditCycleId) && cycleReqItemIdAuditCycCovItemsMap.get(auditCycleId).containsKey(exId.Id)){
                            exAuditCycleCovItem = cycleReqItemIdAuditCycCovItemsMap.get(auditCycleId).get(exId.Id);
                        }
                        if ( exAuditCycleCovItem != null && !exAuditCycleCovItem.isEmpty() ){
                            for(Audit_Cycle_Coverage_Item__c exAcci:exAuditCycleCovItem){
                                // Add the ACCI to the exclusion list if already exists
                                exCycleAcci.put(exAcci.Audit_Cycle_Coverage__c+'-'+exAcci.Cycle_Request_Item__c, exAcci);
                                //System.debug('DEBUG - ' + exAcci.Audit_Cycle_Coverage__c+'-'+exAcci.Cycle_Request_Item__c);
                            }
                        }
                        
                        // Get list of Auditor Request Items that already exist for the CREQ
                        // Author: Ben Harvie
                        // GUS: W-8024790
                        List<KARL_Auditor_Request_Item__c> exAuditReqItem = new List<KARL_Auditor_Request_Item__c>();
                        if(auditorReqItemMap.containsKey(auditCycleId) && auditorReqItemMap.get(auditCycleId).containsKey(exId.Id)){
                            exAuditReqItem = auditorReqItemMap.get(auditCycleId).get(exId.Id);
                        }
                        if ( exAuditReqItem != null && !exAuditReqItem.isEmpty() ){
                            for(KARL_Auditor_Request_Item__c exAri:exAuditReqItem){
                                // Add the ARI to the exclusion list if already exists
                                exCycleAri.put(exAri.KARL_Cycle_Request_Item__c+'-'+exAri.KARL_Audit_Team__c, exAri);
                                //System.debug('DEBUG - ' + exAri.KARL_Cycle_Request_Item__c+'-'+exAri.KARL_Audit_Team__c);
                            }
                        }

                        // Add the CREQ to the exclusion list
                        exCycleReqItems.put(exId.Request__c, exId);
                    }
                }
            }
            
            
            for(Audit_Cycle__c auditCycle : auditCycleList){
                areaScopeAndAuditCycleCoverages.put(auditCycle.Id,new Map<String,List<Audit_Cycle_Coverage__c>>());
                //Collect all Area and Scopes from Audit Cycle Coverages
                for( Audit_Cycle_Coverage__c acc : auditCycle.Audit_Cycle_Coverages__r ){
                    areaOfCompliances.add(acc.Area__c);
                    scopes.add(acc.Scope__c );
                    if(!areaScopeAndAuditCycleCoverages.get(auditCycle.Id).containskey(acc.Area__c+'-'+acc.Scope__c)){
                        areaScopeAndAuditCycleCoverages.get(auditCycle.Id).put(acc.Area__c+'-'+acc.Scope__c, new List<Audit_Cycle_Coverage__c>());
                    }
                    areaScopeAndAuditCycleCoverages.get(auditCycle.Id).get(acc.Area__c+'-'+acc.Scope__c).add(acc);
                }
            }
            
            
            //Generating search string for area Of compliances
            String areaOfCompliancesStr = '';
            for(String s: areaOfCompliances){
                areaOfCompliancesStr += '\''+s+'\',';
            }
            areaOfCompliancesStr = areaOfCompliancesStr.removeEnd(',');
            
            if( String.isBlank(areaOfCompliancesStr)){
                areaOfCompliancesStr = '\'\'';
            }
            
            //Fetching Request Items and Request Item Controls based on the area and scopes
            List<Request_Item__c> requestItemList = database.query('SELECT Id, Request_Name__c,Type__c,KARL_GRC_Assignee__c, Areas_of_Compliance__c,Self_Collect_Link__c,(SELECT Id,Control_Scope__c FROM Request_Item_Controls__r where Control_Scope__c in: scopes ) FROM Request_Item__c  WHERE Areas_of_Compliance__c includes ('+areaOfCompliancesStr+') AND Active__c = TRUE' );
            
            //Generating Cycle Request Item and Audit Cycle Coverage Items
            //Ex: HitRust;Fedramp can be area for single request item
            for(Request_Item__c requestItem : requestItemList){
                
                Set<Id> auditTeamIdSet = new Set<Id>();
    
                //Iterating by Request Item Control
                //Ex: A request item can have multiple request item controls with scopes: HK, QUIP, PARDOT
                for( Request_Item_Control__c reCtrlTest : requestItem.Request_Item_Controls__r){
                   
                    //Splitting areas list in single area Ex: HitRust,Fedramp
                    for( String area : requestItem.Areas_of_Compliance__c.split(';') ){
                        
                        //Using this so we can bind Parent and Child record with single reference and later can be used to get parent record for childs
                        //Group Cycle Request Item Based on Request Item + Scope
                        //
                        // CHANGE: 	05/25/2020
                        // Author: 	Ben Harvie
                        // GUS: 	W-7613214
                        //string randomId = requestItem.id + '-' +reCtrlTest.Control_Scope__c;
                        string randomId = requestItem.id;
                        for(Id auditCycleId : areaScopeAndAuditCycleCoverages.keySet()){
                            //Checking if we have Area-Scope from Audit Cycle Coverage then create Cycle Request Item once and Multiple Audit Cycle Coverage Items
                            if( areaScopeAndAuditCycleCoverages.get(auditCycleId).containsKey(area+'-'+reCtrlTest.Control_Scope__c) ){
                                for( Audit_Cycle_Coverage__c acc : areaScopeAndAuditCycleCoverages.get(auditCycleId).get(area+'-'+reCtrlTest.Control_Scope__c) ){
                                                                
                                    //Create Cycle Request Items for once
                                    if( !cycleReqItems.containsKey(randomId) && !exCycleReqItems.containsKey(randomId)){
                                        Cycle_Request_Item__c cycleRequestItem = new Cycle_Request_Item__c();
                                        cycleRequestItem.Request__c = requestItem.Id;
                                        cycleRequestItem.Audit_Cycle_Coverage__c = acc.Id;
                                        cycleRequestItem.Audit_Cycle__c = acc.Audit_Cycle__c;
                                        if( requestItem.Type__c == 'Self Collection' ){
                                            cycleRequestItem.Cycle_Request_Status__c = 'Provided to Auditor';
                                            cycleRequestItem.Request_Link__c = requestItem.Self_Collect_Link__c;
                                        }else{
                                            cycleRequestItem.Cycle_Request_Status__c = 'New';
                                        }
                                        cycleRequestItem.KARL_Indexed_Request_Name__c = requestItem.Request_Name__c;
                                        cycleRequestItem.Request_Assignee__c = requestItem.KARL_GRC_Assignee__c;
                                        
                                        cycleReqItems.put(randomId, cycleRequestItem);
                                    }
                                    
                                    //Create Audit Cycle Coverage Item
                                    List<Audit_Cycle_Coverage_Item__c> accis = new List<Audit_Cycle_Coverage_Item__c>();
                                    if( auditCycleCovItem.containsKey(randomId) ){
                                        accis = auditCycleCovItem.get(randomId);
                                    }
                                    
                                    Audit_Cycle_Coverage_Item__c acci = new Audit_Cycle_Coverage_Item__c();
                                    acci.Audit_Cycle_Coverage__c = acc.Id;
                                    accis.add(acci);
                                    
                                    auditCycleCovItem.put(randomId, accis);

                                    //Create Auditor Request Item 
                                    //Added by Mahima Aggarwal 
                                    List<KARL_Auditor_Request_Item__c> auditorReqItemList = new List<KARL_Auditor_Request_Item__c>();
                                    if( requestIdToAuditorReqItemsMap.containsKey(randomId) ){
                                        auditorReqItemList = requestIdToAuditorReqItemsMap.get(randomId);
                                    }
                                    //System.debug('Processing ARI for ' + randomId);
                                    if(acc.KARL_Audit_Team__c != null 
                                        && !auditTeamIdSet.contains(acc.KARL_Audit_Team__c)){
                                        KARL_Auditor_Request_Item__c auditorReqItem = new KARL_Auditor_Request_Item__c();
                                        auditorReqItem.KARL_Audit_Team__c = acc.KARL_Audit_Team__c;
                                        auditorReqItem.KARL_Indexed_Request_Name__c = requestItem.Request_Name__c;
                                        if( requestItem.Type__c == 'Self Collection' ){
                                            auditorReqItem.KARL_Status__c = 'Not Started';
                                        }else{
                                            auditorReqItem.KARL_Status__c = 'Pending';
                                        }
                                        auditorReqItemList.add(auditorReqItem);

                                        auditTeamIdSet.add(acc.KARL_Audit_Team__c);
                                    }
                                    requestIdToAuditorReqItemsMap.put(randomId, auditorReqItemList);    
                                }
                            }
                        }
                    }
                }
            }
            System.debug('cycleReqItems.values()= '+cycleReqItems.values());
            //Insert Cycle Request Item
            try{
                if( Schema.sObjectType.Cycle_Request_Item__c.isCreateable() 
                    && Cycle_Request_Item__c.Request__c.getDescribe().isCreateable() 
                    && Cycle_Request_Item__c.Audit_Cycle_Coverage__c.getDescribe().isCreateable() 
                    && Cycle_Request_Item__c.Audit_Cycle__c.getDescribe().isCreateable() 
                    && Cycle_Request_Item__c.KARL_Indexed_Request_Name__c.getDescribe().isCreateable() 
                    && Cycle_Request_Item__c.Request_Assignee__c.getDescribe().isCreateable() 
                    && Cycle_Request_Item__c.Cycle_Request_Status__c.getDescribe().isCreateable() ){
                     insert cycleReqItems.values();
                }
            }
            catch(Exception e){
                throw new AuraHandledException(''+e.getMessage());
            }
            
               //Set child Audit Cycle Coverage Item records with Parent (Cycle Request Item Id)
               // This is the original loop - for new CREQs
            for( String randomNum : cycleReqItems.keySet() ){
                if( auditCycleCovItem.containsKey(randomNum) ){
                    for( Audit_Cycle_Coverage_Item__c acci : auditCycleCovItem.get(randomNum) ){
                        acci.Cycle_Request_Item__c = cycleReqItems.get(randomNum).Id;
                        instAuditCycleCoverageItems.add(acci);
                    }
                }
            }
            
            //Updet child Audit Cycle Coverage Item records with Parent (Cycle Request Item Id)
            // Author: Ben Harvie
            // GUS: W-7613465
            // Iterates through existing CREQ's and excludes already mapped ACCIs and ARIs
            for( String randomNum : exCycleReqItems.keySet() ){
                // First iterate through the ACCIs of the CREQ
                if( auditCycleCovItem.containsKey(randomNum) ){
                    for( Audit_Cycle_Coverage_Item__c acci : auditCycleCovItem.get(randomNum) ){
                        acci.Cycle_Request_Item__c = exCycleReqItems.get(randomNum).Id;
                        //System.debug('DEBUGGING - ' + acci.Audit_Cycle_Coverage__c+'-'+acci.Cycle_Request_Item__c);
                        if( !exCycleAcci.containsKey(acci.Audit_Cycle_Coverage__c+'-'+acci.Cycle_Request_Item__c) ){
                            instAuditCycleCoverageItems.add(acci);
                        }
                    }
                }
                // Second iterate through the ARIs of the CREQ (W-8024790)
                if( requestIdToAuditorReqItemsMap.containsKey(randomNum) ){
                    for( KARL_Auditor_Request_Item__c auditorReqItem : requestIdToAuditorReqItemsMap.get(randomNum) ){
                        auditorReqItem.KARL_Cycle_Request_Item__c = exCycleReqItems.get(randomNum).Id;
                        if( !exCycleAri.containsKey(auditorReqItem.KARL_Cycle_Request_Item__c+'-'+auditorReqItem.KARL_Audit_Team__c) ){
                            instAuditorReqItemList.add(auditorReqItem);
                        }
                    }
                }
            }
    
            //Set child Auditor Request Item records with Parent (Cycle Request Item Id)
            //Added By Mahima Aggarwal
               for( String randomNum : cycleReqItems.keySet() ){
                if( requestIdToAuditorReqItemsMap.containsKey(randomNum) ){
                    for( KARL_Auditor_Request_Item__c auditorReqItem : requestIdToAuditorReqItemsMap.get(randomNum) ){
                        auditorReqItem.KARL_Cycle_Request_Item__c = cycleReqItems.get(randomNum).Id;
                        instAuditorReqItemList.add(auditorReqItem);
                    }
                }
            }
    
            //Inserting Auditor Request Item records
            //Added by Mahima Aggarwal
            if(!instAuditorReqItemList.isEmpty()){
                try{
                    if( Schema.sObjectType.KARL_Auditor_Request_Item__c.isCreateable() 
                        && KARL_Auditor_Request_Item__c.KARL_Cycle_Request_Item__c.getDescribe().isCreateable() ){
                        insert instAuditorReqItemList;
                    }
                }
                catch(Exception e){
                    throw new AuraHandledException(''+e.getMessage());
                }
            }
            
            //Inserting Audit Cycle Coverage Items where they don't exist
            if( !instAuditCycleCoverageItems.isEmpty() ){            
                try{
                    if( Schema.sObjectType.Audit_Cycle_Coverage_Item__c.isCreateable() 
                        && Audit_Cycle_Coverage_Item__c.Cycle_Request_Item__c.getDescribe().isCreateable() ){
                        insert instAuditCycleCoverageItems;
                    }
                }
                catch(Exception e){
                    throw new AuraHandledException(''+e.getMessage());
                }
            }
            
            if( instAuditorReqItemList.isEmpty() && instAuditCycleCoverageItems.isEmpty() && !Test.isRunningTest() ){
                //If not request item and request item control test meets the criteria then throw error to user.
                throw new AuraHandledException('No records meet the criteria to generate or update the Cycle Request Item.');
            }
    }

    /**
    * @author Lakshya Arora
    * @email lakshya.arora@mtxb2b.com
    * @desc This method will call after all the operations get completed
    */
    public void finish(Database.BatchableContext bc) {
        System.debug(LoggingLevel.DEBUG, 'Batch Completed');
    }   

    /**
    * @author Lakshya Arora
    * @email lakshya.arora@mtxb2b.com
    * @desc This method will execute the batch
    */
    public void execute(SchedulableContext sc) {
        Database.executebatch(new KARL_GUSProductTagDATACREATIONBATCH(),200);
    }
}
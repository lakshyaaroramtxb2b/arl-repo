global with sharing class SiteGuestInsecureSettings implements Database.Batchable<sObject>, Database.Stateful, Database.AllowsCallouts {
    String query;
    List<String> fieldsList = new List<String>();
    Set<String> vulnSites;
    Map<String, Integer> countByType;
    Set<String> custOrgs;
    Set<String> premierOrgs;
    //global Map<String,guestUserWrapper> orgsInfo = new Map<String,guestUserWrapper>();
    //global Set<String> vulnerableOrgs = new Set<String>();
    //global Map<String, List<guestUserWrapper>> payloadMap = new Map<String, List<guestUserWrapper>>();
    //global List<List<guestUserWrapper>> resList = new List<List<guestUserWrapper>>();
    
    public void setQuery(String query, List<String> fieldsList, Set<String> vulnSites, Map<String, Integer> countByType, Set<String> custOrgs, Set<String> premierOrgs){
        if(vulnSites != null && vulnSites.size() != 0){
            //system.debug('inside if: '+vulnSites.size());
            //this.query = 'select organization_id__c, domain__c, url_path_prefix__c from guests_sfdc_sites__c where guests_all_orgs__c != null and organization_id__c in :vulnSites';
            this.query = query;
            //fieldsList param is null
            this.fieldsList = fieldsList;
            this.vulnSites = new Set<String>(vulnSites);
            this.countByType = new Map<String, Integer>(countByType);
            this.custOrgs = new Set<String>(custOrgs);
            this.premierOrgs = new Set<String>(premierOrgs);
        }else{
            //this.query = query+' and organization_id__c not in :custOrgs';
            this.query = query;
            this.fieldsList = new List<String>(fieldsList);
            //vulnSites param is null
            this.vulnSites = vulnSites;
            this.countByType = new Map<String, Integer>(countByType);
            this.custOrgs = new Set<String>(custOrgs);
            this.premierOrgs = new Set<String>(premierOrgs);
        }
    }
    global with sharing class guestUserWrapper{
        public guestUserWrapper(){}
        public guestUserWrapper(guestUserWrapper gw){
            this.orgid = gw.orgid;
            for(String s: gw.vulnTypes.keySet()){
                Set<Map<String,String>> lstcp = gw.vulnTypes.get(s).clone();
                this.vulnTypes.put(s,lstcp);
            }
            //this.vulnTypes = gw.vulnTypes;
        }
        public String orgid;
        public String entityName;
        public Map<String, Set<Map<String,String>>> vulnTypes = new Map<String, Set<Map<String,String>>>();
        public String email;
        public String ownername;
        public String source_type = 'siteGuestUser';
        public String username;
        public String lastUpdated;
        public Map<String, Integer> countByType = new Map<String, Integer>();
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc){
        
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext bc, List<SObject> scope){
        String orgid;
        Map<String,String> lst;
        Set<Map<String,String>> current;
        Map<String, guestUserWrapper> orgsInfo = new Map<String, guestUserWrapper>();
        Set<String> vulnerableOrgs = new Set<String>();
        Map<String, List<guestUserWrapper>> payloadMap = new Map<String, List<guestUserWrapper>>();
        List<List<guestUserWrapper>> resList = new List<List<guestUserWrapper>>();
        String entityName = scope[0].getsobjecttype().getdescribe().getname();
        
        //query sites for vuln orgs
        if(vulnSites != null){
            //for(SObject so : [select organization_id__c, domain__c, url_path_prefix__c from guests_sfdc_sites__c where organization_id__c in :vulnerableOrgs]){
            for(SObject so : scope){
                orgid = String.valueof(so.get('organization_id__c'));
                lst = new Map<String,String>{'organization_id__c'=>orgid,'Site_Status__c'=> String.valueof(so.get('Site_Status__c')),'domain__c'=> String.valueof(so.get('domain__c')),'url_path_prefix__c'=> String.valueof(so.get('url_path_prefix__c'))};
                vulnerableOrgs.add(orgid);
                guestUserWrapper guw;
               
                if(orgsInfo.containsKey(orgid) && orgsInfo.get(orgid).vulnTypes.containsKey('guests_sfdc_sites__c')){
                    guw = new guestUserWrapper(orgsInfo.get(orgid));
                    current = guw.vulnTypes.get('guests_sfdc_sites__c');
                    current.add(lst);
                    guw.vulnTypes.put('guests_sfdc_sites__c', current);
                }else{
                    guw = new guestUserWrapper();
                    guw.entityName = entityName;
                    guw.orgid = orgid;
                    guw.vulnTypes.put('guests_sfdc_sites__c', new Set<Map<String,String>>{lst});
                }
                orgsInfo.put(orgid, guw);
                //system.debug('vulntypes: '+guw.vulnTypes);
            }
            //vulnSites = null;
            //system.debug('inside vulnSites:'+orgsInfo.size());
        }else{
            for(SObject so : scope){
                guestUserWrapper guw = new guestUserWrapper();
                orgid = String.valueof(so.get('organization_id__c'));
                guw.orgid = orgid;
                guw.entityName = entityName;
                vulnerableOrgs.add(orgid);
                lst = new Map<String,String>();
                
                //add all corresponding fields to the set
                lst.clear();
                for(String field : fieldsList){
                    lst.put(field, String.valueof(so.get(field)));
                }
                if(orgsInfo.containsKey(orgid) && orgsInfo.get(orgid).vulnTypes.containsKey(entityName)){
                    guw = orgsInfo.get(orgid);
                    current = guw.vulnTypes.get(entityName);
                    current.add(lst);
                    guw.vulnTypes.put(entityName, current);
                } else{
                    guw.vulnTypes.put(entityName, new Set<Map<String,String>>{lst});
                } 
                
                orgsInfo.put(orgid,guw);
                //system.debug(guw.vulnTypes.keySet());
            }
        }
        
        //query admins for vuln orgs who are active employees
        for(SObject so : [select organization_id__c, first_name__c, last_name__c, email__c, last_update__c, username__c from guests_sfdc_admins__c where organization_id__c in :vulnerableOrgs and Employee__c != null]){
            orgid = String.valueof(so.get('organization_id__c'));
            String email = String.valueof(so.get('email__c')).tolowercase();
            //lst = new Set<String>();
            //lst = new Map<String,String>{'organization_id__c'=>orgid,'first_name__c'=> String.valueof(so.get('first_name__c')),'last_name__c'=> String.valueof(so.get('last_name__c')), 'email'=>email, 'last_update__c'=> String.valueof(so.get('last_update__c')), 'username__c'=> String.valueof(so.get('username__c'))};
            if(orgsInfo.containsKey(orgid)){
                guestUserWrapper gw = new guestUserWrapper(orgsInfo.get(orgid));
                gw.orgid = orgid;
                gw.email = email;
                gw.entityName = entityName;
                gw.ownername = String.valueof(so.get('first_name__c'))+' '+String.valueof(so.get('last_name__c'));
                gw.countByType = countByType;
                gw.username = String.valueof(so.get('username__c'));
                gw.lastUpdated = String.valueof(so.get('last_update__c'));
                
                //tag issues to admins
                List<guestUserWrapper> obj = new List<guestUserWrapper>();
                if(payloadMap != null && payloadMap.containsKey(email)){
                    obj = payloadMap.get(email);
                    obj.add(gw);
                    //payloadMap.put(email, obj);
                }else{
                    //gw.vulnTypes.put('guests_sfdc_admins__c', new Set<Set<String>>{lst});
                    obj.clear();
                    obj = new List<guestUserWrapper>{gw};
                }
                payloadMap.put(email, obj);
                //system.debug('guw obj: '+obj);

            }
            
        }
        
        //system.debug('vulnerableOrgs: '+vulnerableOrgs);
        system.debug('payloadMap: '+ payloadMap.size());
        system.debug('orgsInfo: '+orgsInfo.size());
        system.debug('countByType: '+countByType);
        
        for(String email: payloadMap.keySet()){
            resList.add(payloadMap.get(email));
        }
        chunkSerializer(resList);
    }
    
    global void finish(Database.BatchableContext BC){
        
    }
    
    /*public void storeResults(List<List<guestUserWrapper>> obj){
        List<guests_batch_results__c> resList = new List<guests_batch_results__c>();
        for(List<guestUserWrapper> gu: obj){
            guests_batch_results__c gbr = new guests_batch_results__c();
            gbr.email__c = gu[0].email;
            gbr.entity__c = gu[0].entityName;
            gbr.processed_res__c = JSON.serialize(gu);
            gbr.extid__c = gu[0].email+'+'+gu[0].entityName;
            resList.add(gbr);
        }
        Schema.SObjectField field = guests_batch_results__c.Fields.extid__c;
        Database.UpsertResult[] cr = Database.upsert(resList,field,false);
        //Database.SaveResult[] srList = Database.insert(resList, false);
    }*/
    
    public void chunkSerializer(List<List<guestUserWrapper>> obj){
        Integer recordsCount = obj.size();
        system.debug(recordsCount);
        Map<String, Object> pMap = new Map<String, Object>();
         if(recordsCount < 5){
             pMap.put('records', obj);
             pMap.put('source_type', 'siteGuestUser');
             //system.debug(JSON.serialize(pMap));
             CloudAMQPController.sendReq(JSON.serialize(pMap));
         } else{
             List<List<guestUserWrapper>> tempList = new List<List<guestUserWrapper>>();
             tempList.clear();
             pMap.clear();
             for(Integer i=0;i<recordsCount; i+=5){
                    for(Integer j=i;j<i+5 && j<recordsCount;j++){
                        //system.debug(obj[j]);
                        tempList.add(obj[j]);
                    }
                    pMap.put('records', tempList);
                    //payloadMap.put('query', query);
                    pMap.put('source_type', 'siteGuestUser');
                    CloudAMQPController.sendReq(JSON.serialize(pMap));
                    //system.debug(JSON.serialize(pMap));
                }
         }
    }
}
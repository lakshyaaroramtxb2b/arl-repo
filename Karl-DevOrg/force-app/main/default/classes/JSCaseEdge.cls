public class JSCaseEdge
{
    public String id;
    public String source;
    public String target;
    
    private String label = '';
    private Integer instaces = 0;
    public String color = 'black';
    public Integer size = 5;
    
    //custome edge render see js
    public String type = 't';
    
    public void addInstance()
    {
        this.instaces++;
        this.label = string.valueof(this.instaces);
    }
}
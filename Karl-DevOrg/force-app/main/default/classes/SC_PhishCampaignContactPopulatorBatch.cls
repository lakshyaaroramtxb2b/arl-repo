global class SC_PhishCampaignContactPopulatorBatch implements Database.Batchable<sObject>{

   global Database.QueryLocator start(Database.BatchableContext BC){
      return Database.getQueryLocator('SELECT Id, Contact__c, Employee_Number__c FROM MPhAT_Campaigns__c WHERE Contact__c = null AND Employee_Number__c != null');
   }

   global void execute(Database.BatchableContext BC, List<sObject> scope){
      MPhAT_Campaigns__c[] camps = (MPhAT_Campaigns__c[]) scope;
      Map<Id, MPhAT_Campaigns__c> by62Id = new Map<Id, MPhAT_Campaigns__c>();
      for (MPhAT_Campaigns__c c : camps) {
          by62Id.put(c.Employee_Number__c, c);
      }
      Contact[] conts = [SELECT Id, Org62_User_ID__c FROM Contact WHERE Org62_User_ID__c IN: by62Id.keySet()];
      for (Contact c : conts) {
          by62Id.get(c.Org62_User_ID__c).Contact__c = c.Id;
      }
      update by62Id.values();
    }

   global void finish(Database.BatchableContext BC){
   }

}
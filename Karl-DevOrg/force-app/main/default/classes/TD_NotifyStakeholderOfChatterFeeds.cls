public without sharing class TD_NotifyStakeholderOfChatterFeeds {
    
    Public static Boolean firstRun = true;
    
    Public static final String CHANGE_TEMPLATE_NAME = 'TD_Inform_Stakeholder_Of_Changes'; 
    
    Private static Map<Id, Trust_Deliverable__c> TDs;
    
    Private static EmailTemplate changeTemplate;
    
    Public static void emailStakeHolders (List<FeedItem> feedItems) {
        
        OrgWideEmailAddress owea = [SELECT Address,Id,DisplayName FROM OrgWideEmailAddress 
                                    WHERE DisplayName = :TD_EmailService.ORG_WIDE_EMAIL_ADDRESS];   
 
        try {   
           changeTemplate = [select Id, Subject, HtmlValue, Body from EmailTemplate where DeveloperName =: CHANGE_TEMPLATE_NAME];
        } catch (exception e) {system.debug('exception getting email template: ' + e);}
                
        
        Set<Id> parentIds = new Set<Id>();
        Map<Id, Id> stakeholderIdToDeliverableId = new Map<Id, Id>();
        
        for (FeedItem f: feedItems) parentIds.add(f.ParentId);
        
        // if the parentIds are for stokeholders need to extend and retrieve deliverable Ids instead
        for (Id parentId : parentIds) {
            // use the first id in the set to test 
            if (!isDeliverableId(parentId)) {
                Set<Id> realParentIds = new Set<Id>();              
                for (Trust_Deliverable_Stakeholder__c s : [select Id, Deliverable__c from Trust_Deliverable_Stakeholder__c
                                                           where Id IN :parentIds]) {
                     realParentIds.add(s.Deliverable__c);
                     stakeholderIdToDeliverableId.put(s.Id, s.Deliverable__c);                                          
                }  
                parentIds.clear();
                parentIds.addAll(realParentIds);                
            }
            break;
        }
        
        TDs = new Map<Id, Trust_Deliverable__c>([Select Id, Name, Due_Date__c, Status__c, Description__c,
                                                 Special_Instructions__c
                                                 from Trust_Deliverable__c 
                                                 where Id IN: parentIds]);
                                                                               
        Map<Id, FeedItem> FeedWithTrackChanges = new Map<Id, FeedItem>([Select Id, Body, InsertedBy.Name, CreatedBy.Name,
                                                                        (Select Id, FieldName, NewValue, OldValue
                                                                        from FeedTrackedChanges) from FeedItem
                                                                        where Id IN :feedItems Limit 500]);                                                                      

        List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();

        for (FeedItem f: feedItems) {
    
            if (f.ParentID != null) {
                
            Id realParentId;
            if (isDeliverableId(f.parentId)) {
                realParentId = f.ParentID;
            } else {
                realParentId = stakeholderIdToDeliverableId.get(f.ParentID);
            }
            
            // if this is a CreateRecordEvent and this is for Trust_Deliverable_Stakeholder__c get new stakeholder user
            Trust_Deliverable_Stakeholder__c newStakeholder = new Trust_Deliverable_Stakeholder__c();
            if (f.Type == 'CreateRecordEvent') {
                try {
                    newStakeholder = [select Stakeholder__r.Name, Stakeholder__r.Email from Trust_Deliverable_Stakeholder__c where Id = :f.ParentId];                      
                } catch (exception e) {}
            }            
    
            // get all subscribers but not the one being added if this is a createrecordevent
            List<EntitySubscription> followers = [select id, subscriberid, subscriber.name, subscriber.email 
                                                  from EntitySubscription where parentid = :realParentId];
    
            String[] toAddresses = new List<String>();
    
    
            for (EntitySubscription follower : followers) {
                if (follower.subscriber.email != newStakeholder.Stakeholder__r.Email)
                    toAddresses.add(follower.subscriber.email);
            }
    
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

            mail.setToAddresses(toAddresses);
            mail.setOrgWideEmailAddressId(owea.Id);
            mail.setReplyTo(TD_EmailService.ORG_WIDE_REPLYTO_EMAIL_ADDRESS);
    
            mail.setSubject('New Chatter Post ');
    
            if (f.Type == 'TrackedChange') {
               if (FeedWithTrackChanges.containsKey(f.Id)) {
                   FeedItem feedWTrackChanges = FeedWithTrackChanges.get(f.Id);
                   List<FeedTrackedChange> ft = feedWTrackChanges.FeedTrackedChanges;  

                   String fieldLabel = Trust_Deliverable__c.sObjectType.getDescribe().Fields.getMap().get(ft[0].FieldName.subStringAfter('.')).getDescribe().getLabel();               
                   String oldValue = '' + ft[0].OldValue;
                   String newValue = '' + ft[0].NewValue;  

                   if (Trust_Deliverable__c.sObjectType.getDescribe().Fields.getMap().get(ft[0].FieldName.subStringAfter('.')).getDescribe().getType().name() == 'Date') {
                        oldValue = date.valueOf(oldValue).format();
                        newValue = date.valueOf(newValue).format();
                   }

                   if (Trust_Deliverable__c.sObjectType.getDescribe().Fields.getMap().get(ft[0].FieldName.subStringAfter('.')).getDescribe().getType().name() == 'TEXTAREA') {
                        oldValue = 'Not Available';
                        newValue = TDs.containsKey(realParentId)? TDs.get(realParentId).Special_Instructions__c : '';
                   }
                   
                   String body = ''; 
                   
                   if (changeTemplate == null) { 
                       if(TDs.containsKey(realParentId)) body += 'Deliverable: ' + TDs.get(realParentId).name + '\n\n'; 
                       body += 'Field: ' + fieldLabel + ' modified by ';
                       body += feedWTrackChanges.InsertedBy.Name + '.\n\n';
                       body += ' Old value: ' + oldValue + '\n';
                       body += ' New value: ' + newValue;                   
                       mail.setPlainTextBody(body); 
                   } else {

                       body = formatHTMLbodyHelper(body, realParentId);
                       
                       String what = 'Field ' + fieldLabel + ' modified by ';
                       what += feedWTrackChanges.InsertedBy.Name;
                       body = body.replace('{!TypeOfChange}', what);
                       
                       body = body.replace('{!OldValue}', oldValue);
                       body = body.replace('{!NewValue}', newValue);
                       
                       
                       mail.setHtmlBody(body); 
                    
                   }
               }               

            } else if (f.Type == 'CreateRecordEvent') {
                   String body = ''; 

                   if (changeTemplate == null) { 
                       
                       if(TDs.containsKey(realParentId)) body += 'Deliverable: ' + TDs.get(realParentId).name + '\n\n'; 
                       body += 'New stakeholder added';
                       try {
                            body += ' by ' + FeedWithTrackChanges.get(f.Id).InsertedBy.Name;
                       } catch (exception e) {system.debug(e);}
                       body += '. \n\n';
                       
                       try {
                            body += 'Stakeholder: ' + newStakeholder.Stakeholder__r.Name;                       
                       } catch (exception e) {system.debug(e);}
                       mail.setPlainTextBody(body);
                   
                   } else {

                       body = formatHTMLbodyHelper(body, realParentId);

                       String what = 'New stakeholder added';
                       try {
                            what += ' by ' + FeedWithTrackChanges.get(f.Id).InsertedBy.Name;
                       } catch (exception e) {system.debug(e);}
                       body = body.replace('{!TypeOfChange}', what);
                       
                       body = body.replace('{!OldValue}', 'N/A');
                       
                       String who = '';
                       try {
                            who += newStakeholder.Stakeholder__r.Name;                       
                       } catch (exception e) {system.debug(e);}                                              
                       body = body.replace('{!NewValue}', who);
                       
                       mail.setHtmlBody(body); 
                    
                   }

            } else if (f.Title == TD_StakeholderChatterHelper.STAKEHOLDER_REMOVED_CHATTER_TITLE) {

                       String body = ''; 
                       body = formatHTMLbodyHelper(body, realParentId);

                       String what = TD_StakeholderChatterHelper.STAKEHOLDER_REMOVED_CHATTER_TITLE;
                       try {
                            what += ' by ' + FeedWithTrackChanges.get(f.Id).InsertedBy.Name;
                       } catch (exception e) {system.debug(e);}
                       body = body.replace('{!TypeOfChange}', what);
                       
                       String who = f.Body.replace((TD_StakeholderChatterHelper.STAKEHOLDER_REMOVED_CHATTER_TITLE + ':'), '').trim();
                       body = body.replace('{!OldValue}', who);
                       body = body.replace('{!NewValue}', 'N/A');
                       
                       mail.setHtmlBody(body); 

            } else if (f.Title == TD_StakeholderChatterHelper.STAKEHOLDER_IS_DONE_TITLE) {

                       String body = ''; 
                       body = formatHTMLbodyHelper(body, realParentId);

                       String what = TD_StakeholderChatterHelper.STAKEHOLDER_IS_DONE_TITLE;
                       body = body.replace('{!TypeOfChange}', what);
                       
                       String who = f.Body.replace((TD_StakeholderChatterHelper.STAKEHOLDER_IS_DONE_TITLE + ':'), '').trim();
                       body = body.replace('{!NewValue}', who + ' is done');
                       body = body.replace('{!OldValue}', 'N/A');
                       
                       mail.setHtmlBody(body); 

            } else if (f.Type == 'TextPost') {

                       String body = ''; 
                       body = formatHTMLbodyHelper(body, realParentId);

                       String what = 'Chatter Text Post from ' + FeedWithTrackChanges.get(f.Id).CreatedBy.Name;
                       body = body.replace('{!TypeOfChange}', what);
                       
                       body = body.replace('{!NewValue}', f.Body);
                       body = body.replace('{!OldValue}', 'N/A');
                       
                       mail.setHtmlBody(body); 

            } else if (f.Type == 'ContentPost') {

                       String body = ''; 
                       body = formatHTMLbodyHelper(body, realParentId);

                       String what = 'Chatter Content Post from ' + FeedWithTrackChanges.get(f.Id).CreatedBy.Name;
                       body = body.replace('{!TypeOfChange}', what);
                       
                       String post = f.body==null? f.ContentFileName : f.body + ' - ' + f.ContentFileName;
                       body = body.replace('{!NewValue}', post);
                       body = body.replace('{!OldValue}', 'N/A');
                       
                       mail.setHtmlBody(body); 

            } else {
               mail.setPlainTextBody(f.body==null? 'Chatter Post' : f.body); 
            }

            emails.add(mail);
            
            }
        }
        
        Messaging.sendEmail(emails);        
        
    }
    
    
    // check if ID is for deliverable
    private static Boolean isDeliverableId(Id id2Check) {
        return (id2Check.getSObjectType().getDescribe().getName() == Trust_Deliverable__c.sObjectType.getDescribe().getName());
    }
    
    private static String formatHTMLbodyHelper(String body, Id realParentId) {

       body = changeTemplate.HtmlValue;
       
       String recordLink = URL.getSalesforceBaseUrl().toExternalForm() + '/' + realParentId;
       body = body.replace('{!Trust_Deliverable__c.Link}', recordLink);
       
       if(TDs.containsKey(realParentId)) {
            body = body.replace('{!Trust_Deliverable__c.Name}', TDs.get(realParentId).name == Null? '' : TDs.get(realParentId).name);
            body = body.replace('{!Trust_Deliverable__c.Description__c}', TDs.get(realParentId).Description__c == Null? '' : TDs.get(realParentId).Description__c);
            body = body.replace('{!Trust_Deliverable__c.Due_Date__c}', TDs.get(realParentId).Due_Date__c == Null? '' : TDs.get(realParentId).Due_Date__c.format());
            body = body.replace('{!Trust_Deliverable__c.Status__c}', TDs.get(realParentId).Status__c == Null? '' : TDs.get(realParentId).Status__c);
            body = body.replace('{!Trust_Deliverable__c.Id}', TDs.get(realParentId).Id);
       }
       else { 
            body = body.replace('{!Trust_Deliverable__c.Name}', '');
            body = body.replace('{!Trust_Deliverable__c.Description__c}', '');
            body = body.replace('{!Trust_Deliverable__c.Due_Date__c}', '');
            body = body.replace('{!Trust_Deliverable__c.Status__c}', '');
            body = body.replace('{!Trust_Deliverable__c.Id}', '');
       }
       
       return body;
        
    }
    
    // this is called from Trust_Delivery__c trigger to intercept feeditem records that 
    // do not called the feeditem trigger    
    @future
    public static void emailStakeHolders(Set<Id> TrustDeliverableIds) {  
        
        firstRun = false; // to avoid multiple executions in the same transaction
            
        Try {
            List<FeedItem> feedItems = [Select Id, Body, ParentId, Type, InsertedBy.Name from FeedItem
                                       where ParentId IN :TrustDeliverableIds
                                       and Type IN ('TrackedChange', 'CreateRecordEvent')
                                       and CreatedDate >= :system.now().addSeconds(-10)
                                       order by CreatedDate Desc limit 5];
            System.debug(feedItems);
            if (!feedItems.isEmpty()) {
               TD_NotifyStakeholderOfChatterFeeds.emailStakeHolders(feedItems);             
            }
        } catch (exception e) {system.debug(e);}                
         
    }                               
    
            
        
}
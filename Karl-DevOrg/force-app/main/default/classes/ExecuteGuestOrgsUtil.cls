global with sharing class ExecuteGuestOrgsUtil{
    public ExecuteGuestOrgsUtil(){}
    
    public static void updateGuestOrgs(){
        Map<String, String> querymap= new Map<String, String>();
        //querymap.put('guests_standard_objs','select organization_id__c from guests_standard_objs__c');
        //querymap.put('guests_cust_objs','select organization_id__c from guests_cust_objs__c');
        //querymap.put('guests_public_groups','select organization_id__c from guests_public_groups__c');
        querymap.put('guests_owd_entityperms','select organization_id__c from guests_owd_entityperms__c');
        for(String key: querymap.keySet()){
            //system.debug(querymap.get(key));
            GuestAllOrgs gao= new GuestAllOrgs();
            gao.query=querymap.get(key);
            Database.executeBatch(gao, 2000);
        }
    }
    
    //populate lookup fields of custom entities
    public static void updateLookupFields(){
        Map<String, String> querymap= new Map<String, String>();
        querymap.put('guests_standard_objs','select id,organization_id__c from guests_standard_objs__c');
        querymap.put('guests_cust_objs','select id,organization_id__c from guests_cust_objs__c');
        querymap.put('guests_public_groups','select id,organization_id__c from guests_public_groups__c');
        querymap.put('guests_owd_entityperms','select id,organization_id__c from guests_owd_entityperms__c');
        for(String key: querymap.keySet()){
            //system.debug(querymap.get(key));
            GuestLookupUpdate gao= new GuestLookupUpdate();
            gao.query=querymap.get(key);
            Database.executeBatch(gao, 2000);
        }
    }
    
    //to delete records
    public static void deleteEntities(){
        DeleteCustEntities del = new DeleteCustEntities();
        del.query = 'select id from guests_all_orgs__c';
        Database.executeBatch(del, 2000);
    }
    
    //insert rollup summary details
    public static void rollupUpdate(){
        GuestRollupSummary gsr = new GuestRollupSummary();
        gsr.query = 'select id,organization_id__c from guests_all_orgs__c';
        Database.executeBatch(gsr, 2000);
    }
    

    public static void getVulnOrgs(String entityName, Set<String> custOrgs, Map<String, Integer> countByType, Set<String> premierOrgs){
        /*Set<String> vulnorgs = new Set<String>();
        for(SObject so: database.query('select organization_id__c from '+entityName+' where guests_all_orgs__c != null and (status__c =\'active\' or status__c =\'free\') and organization_id__c not in :custOrgs')){
            vulnorgs.add((String)so.get('organization_id__c'));
        }
        system.debug('method: '+vulnorgs.size());
        return vulnorgs;*/
        
        //Batch apex to identify set of vuln orgs across all entities
        GuestVulnOrgs gv = new GuestVulnOrgs();
        gv.vulnorgs = new Set<String>();
        gv.custOrgs = custOrgs;
        gv.premierOrgs = premierOrgs;
        gv.countByType = countByType;
        gv.entityName = entityName;
        Database.executeBatch(gv, 2000);
    }
    
    //Initiate batch jobs for querying insecure guest users
    public static void getInsecureSettings(){
        //reported customer orgs needs to be appended to this set. As data in entities is ephemeral no easier way to track the cust orgs than hardcoding
        Set<String> custOrgs = new Set<String>{'00D1N000001R9op', '00Dj0000000HyOE', '00DA0000000gQRw', '00D90000000iQtA', '00D410000011aN5', 
        '00DF00000006wJO', '00D36000000bvrX', '00DA0000000HVvL', '00Df2000001Kv2H', '00D80000000bvAU', '00D80000000Ks1s', '00DA0000000bhsv', '00D1r000002cPn9', '00DC0000000PlX2', '00D20000000Bb0L', 
        '00Df4000002aXKx', '00DA0000000C8TR', '00da0000000c9l3', '00d0y000001ezhz', '00D30000000mUoD', '00DF00000006nfK', '00D2E0000013KTm', '00DG0000000hJFg', 
        '00D0O000000sODN', '00D40000000Muj9', '00D1I000002KqwO', '00Db0000000YAsr', '00d1000000003y8', '00D1U000000xmv4', '00Db0000000ZJnW', 
        '00D700000009DNb', '00Dj0000001uzTo', '00Dj0000001tsrk', '00D30000001I8Ue', '00Db0000000cgiQ', '00D28000001DjQz', '00D7F000000yOEP', 
        '00D6A0000001O9v', '00D300000000wyI', '00D4J000000F3ru', '00Dd0000000edwY', '00DA0000000aPm0', '00D30000000pWNf', '00DE0000000L5C3', '00Di0000000hzmY', 
        '00D460000016Zs9', '00D500000007qaK', '00D36000000bQR2', '00D100000000IcF', '00D36000000vvMf', '00D1U0000015FxH', '00D80000000b8Nw',
        '00Db0000000aGhV', '00d1u0000012fcg', '00D2o000000ZsnE', '00D41000002lEpr', '00D1I000001Y28M', '00DA0000000KY2N', '00D50000000ZOHP',
        '00DD0000000p3s4', '00DA0000000Yusv', '00D1a000000aAKV', '00Db0000000dP8v', '00DA0000000Y6OZ', '00DA0000000JvoX', '00Db0000000HJrE',
        '00Df4000000231l', '00DA0000000A5Jp', '00D41000002jGio', '00D1a000000Zja2', '00Di0000000eFVt', '00D700000008ogE', '00D3X000002LtD3', 
        '00Dj00000028dvR', '00D37000000K0CP', '00D46000001UoSw', '00D7F000002EAot', '00D36000000b5qS', '00D46000000Xvge', '00D24000000Yk1o', 
        '00D20000000NLaE', '00D5I000000E0Kz', '00D61000000a94y', '00D41000000Xd4r'};
        //Below sfdc orgs created with demo status that miss alerts if we only monitor active status orgs. These are crucial to SFDC business operations so adding additional checks to make sure check any issues related to them are surfaced
        Set<String> premierOrgs = new Set<String>{'00D000000000062', '00D00000000hg76', '00Di0000000ZoSH', '00D300000000iTz', '00D30000000XsfG', '00Dd0000000f6kc', '00D70000000Jxye', '00DT0000000Dpvc', '00D30000000Jsbi', '00D300000000QuG', '00D50000000NtVr', '00Di0000000YCVE', '00D30000000YVMl', '00Dj0000000KnJw', '00D50000000a4gu', '00D36000000ZRHC', '00Dj0000000H56z', '00D460000000p04', '00D1I000001VxWE', '00D1I000001WM1N', '00D3000000010D', '00D36000000KF2j', '00Df4000003j0NT', '00Dt0000000GzQT', '00D61000000YBfm', '00D1U000000EaxM', '00D5A0000016PKk', '00D4T000000CsRn', '00D5w000000XghM', '00D30000000osTN', '00Df4000002aXKx'};

        Map<String, String> querymap= new Map<String, String>();
        Map<String, List<String>> Fieldsmap= new Map<String, List<String>>();
        //IN operator is case insensitive which returns erroneous results on organization_id__c so check with Unique Case Sensitive field of guests_all_orgs__r.organization_id__c
        querymap.put('guests_public_groups__c', 'select id,organization_id__c,group_name__c,name__c from guests_public_groups__c where guests_all_orgs__c != null and (status__c =\'active\' or guests_all_orgs__r.organization_id__c in :premierOrgs) and guests_all_orgs__r.organization_id__c not in :custOrgs');
        querymap.put('guests_standard_objs__c', 'select id,organization_id__c,count__c, gu_name__c, name__c,obj_name__c from guests_standard_objs__c where guests_all_orgs__c != null and (status__c =\'active\' or guests_all_orgs__r.organization_id__c in :premierOrgs) and guests_all_orgs__r.organization_id__c not in :custOrgs');
        querymap.put('guests_cust_objs__c', 'select id,organization_id__c,count__c, gu_name__c, name__c,master_label_norm__c from guests_cust_objs__c where guests_all_orgs__c != null and (status__c =\'active\' or guests_all_orgs__r.organization_id__c in :premierOrgs) and guests_all_orgs__r.organization_id__c not in :custOrgs');
        querymap.put('guests_owd_entityperms__c', 'select id,organization_id__c, entity_enum_or_id__c, external_sharing_model__c, p_name__c, sharing__c,create_perm__c,delete_perm__c,mad_perm__c,read_perm__c,update_perm__c,vad_perm__c from guests_owd_entityperms__c where guests_all_orgs__c != null and (status__c =\'active\' or guests_all_orgs__r.organization_id__c in :premierOrgs) and guests_all_orgs__r.organization_id__c not in :custOrgs');
        
        Fieldsmap.put('guests_public_groups__c',new List<String>{'organization_id__c','group_name__c','name__c'});
        Fieldsmap.put('guests_standard_objs__c',new List<String>{'organization_id__c','count__c','gu_name__c','name__c','obj_name__c'});
        Fieldsmap.put('guests_cust_objs__c',new List<String>{'organization_id__c','count__c','gu_name__c','name__c','master_label_norm__c'});
        Fieldsmap.put('guests_owd_entityperms__c',new List<String>{'organization_id__c', 'entity_enum_or_id__c', 'external_sharing_model__c','p_name__c','sharing__c','create_perm__c','delete_perm__c','mad_perm__c','read_perm__c','update_perm__c','vad_perm__c','entity_enum_or_id__c'});
        
        
        Map<String, Integer> countByType = new Map<String, Integer>();
        countByType.put('pubGroupsCount',[select count() from guests_public_groups__c where guests_all_orgs__c != null and (status__c ='active' or guests_all_orgs__r.organization_id__c in :premierOrgs) and guests_all_orgs__r.organization_id__c not in :custOrgs]);
        countByType.put('stdEntityCount',[select count() from guests_standard_objs__c where guests_all_orgs__c != null and (status__c ='active' or guests_all_orgs__r.organization_id__c in :premierOrgs) and guests_all_orgs__r.organization_id__c not in :custOrgs]);
        countByType.put('custEntityCount',[select count() from guests_cust_objs__c where guests_all_orgs__c != null and (status__c ='active' or guests_all_orgs__r.organization_id__c in :premierOrgs) and guests_all_orgs__r.organization_id__c not in :custOrgs]);
        countByType.put('owdPermsCount',[select count() from guests_owd_entityperms__c where guests_all_orgs__c != null and (status__c ='active' or guests_all_orgs__r.organization_id__c in :premierOrgs) and guests_all_orgs__r.organization_id__c not in :custOrgs]);
        //countByType.put('sitesCount',[select count() from guests_sfdc_sites__c where organization_id__c in :orgs and organization_id__c not in :custOrgs]);
        
        for(String key: querymap.keySet()){
            SiteGuestInsecureSettings sgi= new SiteGuestInsecureSettings();
            /*sgi.query = querymap.get(key);
            sgi.entityName = key;
            sgi.fieldsList = Fieldsmap.get(key);*/
            sgi.setQuery(querymap.get(key), Fieldsmap.get(key), null, countByType, custOrgs, premierOrgs);
            Database.executeBatch(sgi, 2000);
        }
        
        //get all vulnerable orgs to identify sites
        getVulnOrgs('guests_public_groups__c', custOrgs, countByType, premierOrgs);
        
        /*Set<String> orgs = new Set<String>();
        for(String key: querymap.keySet()){
            //orgs.addAll(getVulnOrgs(key, custOrgs));
            getVulnOrgs(key, custOrgs, countByType);
        }
        
        system.debug('vulnSites count: '+orgs.size());

        SiteGuestInsecureSettings sg = new SiteGuestInsecureSettings();
        sg.setQuery(null, null, orgs, countByType, custOrgs);
        Database.executeBatch(sg, 2000);*/
    }
}
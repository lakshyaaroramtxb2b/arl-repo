/***************IEM_GUSWorkToCaseQueuable****************
@Author Banshi
@Date 09/09/2019
@Modified by/ date - Swarnima singh Mandhata - T-09359 - 03/31/2020
@Description Queueable class to create case and case work records.
**********************************************/
public without sharing class IEM_GUSWorkToCaseQueuable implements Queueable {
    @TestVisible 
    private static list<ADM_Work_c__x> mockallWorkList = new list<ADM_Work_c__x>();
    @TestVisible 
    private static Map<String, String> mockGUSUserEmpMap = new Map<String, String>();
    
    private List<String> workIds = new List<String>();
    private List<ADM_Theme_Assignment_c__x> themeAssignmentList = new List<ADM_Theme_Assignment_c__x>();//Theme assignment records list to update back with Theme = primaryThemeId
    
    public IEM_GUSWorkToCaseQueuable(List<String> workIds, List<ADM_Theme_Assignment_c__x> themeAssignmentList) {
        this.workIds = workIds;
        this.themeAssignmentList = themeAssignmentList;
        
    }
    public void execute(QueueableContext context) {
        List<Case> newCaseList = new List<Case>();	
        List<Case_Work__c> newCaseWorkList = new List<Case_Work__c>();
        List<ADM_Work_c__x> newGusWorkList = new List<ADM_Work_c__x>();
        
        if(Test.isRunningTest()){
            newGusWorkList = mockallWorkList; //For test class as we can't create external object record through test class.
        }else{
            //Query to fetch all GUS work records of Intake theme assignment.
            newGusWorkList = [SELECT Id, Assignee__c ,CreatedById__c, CreatedDate__c, Details_c__c, Cloud_c__c, ExternalId, Product_Owner_c__c, Scrum_Team_Name_c__c, Scrum_Team_c__c, Team__c, Subject_c__c, Details_and_Steps_to_Reproduce_c__c  FROM ADM_Work_c__x WHERE ExternalId IN :workIds];
        }
        Set<String> gusUserIds = new Set<String>(); //set to store GUS user (External Object) record Ids.
        Set<String> productOwnerSet = new Set<String>(); //Set to store product owner names.
        Set<String> cloudNameSet = new Set<String>(); //set to store cloud names.
       	Map<String, Id> cloudNameIdMap = new Map<String, Id>(); //Map of Cloud map and Id.
        Map<String, String> gusUserEmpMap = new Map<String, String>(); //Map of GUS user and Employee.
        Map<String, String> userMap = new Map<String, String>(); //Map of user records.
        
        //Iterate all the GUS work records
        for(ADM_Work_c__x gusWork : newGusWorkList){
            if(String.isNotBlank(gusWork.Product_Owner_c__c)){
                productOwnerSet.add(gusWork.Product_Owner_c__c);
            }
            if(String.isNotBlank(gusWork.Assignee__c)){
                gusUserIds.add(gusWork.Assignee__c);
            }
            if(String.isNotBlank(gusWork.Product_Owner_c__c)){
                gusUserIds.add(gusWork.Product_Owner_c__c);
            }
        }
        if(!gusUserIds.isEmpty()){
            for(User__x usr : [SELECT Id, ExternalId,Email__c, External_ID_c__c FROM User__x Where ExternalId IN : gusUserIds AND Email__c != '']){
                if(String.isNotBlank(usr.Email__c)){
                    gusUserEmpMap.put(usr.Email__c, usr.ExternalId);
                }
            }
            //For test coverage only
            if(Test.isRunningTest()){
                gusUserEmpMap = mockGUSUserEmpMap;
            }
            if(!gusUserEmpMap.isEmpty()){
                for(Contact cont : [SELECT Id,Email FROM Contact Where Email IN :gusUserEmpMap.keySet() AND Contact.AccountId=:Label.ESA_Office_Hours_Account_ID]){
                    if(String.isNotBlank(cont.Email) && gusUserEmpMap.containsKey(cont.Email)){
                        userMap.put(gusUserEmpMap.get(cont.Email),cont.Id);
                    }
                }
            }
        }
        
        for(ADM_Work_c__x gusWork : newGusWorkList){
            if(String.isNotBlank(gusWork.Cloud_c__c)){
                cloudNameSet.add(gusWork.Cloud_c__c);
            }
        }
        for(ADM_Cloud_c__x cloud : [Select ExternalId,Name__c from ADM_Cloud_c__x Where Name__c IN :cloudNameSet]){
            cloudNameIdMap.put(cloud.Name__c,cloud.ExternalId);
        } 
        IEM_CaseOwer caseOwner = new IEM_CaseOwer(); //To get next case owner
        Id nextOwner = caseOwner.getNextCaseOwner();
        Integer index = 0;
        for(ADM_Work_c__x gusWork : newGusWorkList){
            if(index > 0 && String.isNotBlank(nextOwner)){
                nextOwner = caseOwner.getNextCaseOwner(nextOwner);
            }
            Case newcase = new Case();
            if(String.isNotBlank(nextOwner)){
                newCase.ownerId = nextOwner;
            }
            newCase.RecordTypeId = IEM_Constants.IEM_CASERECORDTYPEID;
            if(cloudNameIdMap.containsKey(gusWork.Cloud_c__c)){
                 newcase.Cloud__c = cloudNameIdMap.get(gusWork.Cloud_c__c);
            }
            if(userMap.containsKey(gusWork.Product_Owner_c__c)){
                 newcase.Issue_Owner__c = userMap.get(gusWork.Product_Owner_c__c);
            }
            if(userMap.containsKey(gusWork.Assignee__c)){
                 newcase.Requestor__c = userMap.get(gusWork.Assignee__c);
            }
            if(gusWork.Details_and_Steps_to_Reproduce_c__c !=null){
            	newcase.Description = (gusWork.Details_and_Steps_to_Reproduce_c__c).stripHtmlTags();
            }
            if(gusWork.Details_c__c !=null){
            	newcase.Description = (gusWork.Details_c__c).stripHtmlTags();
            }
            newcase.Team_Responsible__c = gusWork.Team__c;
            newcase.Subject = gusWork.Subject_c__c;
            if(gusWork!=null && gusWork.CreatedDate__c!=null)
            	newCase.Identified_Date__c = (gusWork.CreatedDate__c).date();
            newCase.Existing_Work_Story__c  = 'Yes';
            newCase.Issue_Source__c  = IEM_Constants.IEM_ISSUE_SOURCE_SLA;
            newCaseList.add(newcase);
            Case_Work__c  caseWork = new Case_Work__c();            
            if(gusWork!=null && gusWork.Subject_c__c!=null)
            	caseWork.Work_Subject__c = gusWork.Subject_c__c;            
            if(gusWork!=null && gusWork.ExternalId!=null)
            	caseWork.Work__c = gusWork.ExternalId;
            newCaseWorkList.add(caseWork);
            index++;
        }
        if( !(newCaseList.isEmpty()) ){
            INSERT newCaseList;
        }
        for(Integer i=0; i<newCaseList.size();i++){
            newCaseWorkList[i].Case__c = newCaseList[i].Id;
        }
        if( !(newCaseWorkList.isEmpty()) ){
            insert newCaseWorkList;
        }
        if( !(themeAssignmentList.isEmpty()) ){
            if(!Test.isRunningTest()){
                Database.updateAsync(themeAssignmentList);
            }
        } 
    }
    
}
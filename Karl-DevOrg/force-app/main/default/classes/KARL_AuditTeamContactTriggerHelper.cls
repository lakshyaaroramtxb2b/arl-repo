public with sharing class KARL_AuditTeamContactTriggerHelper {
    public static set<String> running;
    
    /**
     * @author Mahima Aggarwal
     * @email mahima.aggarwal@mtxb2b.com
     * @desc This method get triggered from auditor request item trigger.
    */
    public static void run(){
        //After Insert
        if( Trigger.isAfter && Trigger.isInsert ){
            shareAuditorReqItemBasedOnFirm((List<KARL_Audit_Team_Contacts__c >)Trigger.new);
        }

        //Before Delete
        if( Trigger.isBefore && Trigger.isDelete ){
            unShareAuditorReqItemBasedOnFirm((List<KARL_Audit_Team_Contacts__c >)Trigger.old);
        }
    }

    /**
     * @author Mahima Aggarwal
     * @email mahima.aggarwal@mtxb2b.com
     * @desc This method share all the auditor request items of the firm from which audit team 
     * contact belong
    */
    private static void shareAuditorReqItemBasedOnFirm(List<KARL_Audit_Team_Contacts__c > newList){
        Map<Id,Set<Id>> auditFirmIdToContactIdMap = new Map<Id,Set<Id>>();

        for(KARL_Audit_Team_Contacts__c auditTeamCon : [SELECT Id,KARL_Audit_Team__c,KARL_Contact__c,
                                                        KARL_Audit_Team__r.KARL_Audit_Firm__c
                                                        FROM KARL_Audit_Team_Contacts__c 
                                                        WHERE Id IN : newList]){ 
            if(auditTeamCon.KARL_Audit_Team__c != null && 
                auditTeamCon.KARL_Audit_Team__r.KARL_Audit_Firm__c != null){
                if(!auditFirmIdToContactIdMap.containsKey(auditTeamCon.KARL_Audit_Team__r.KARL_Audit_Firm__c)){
                    auditFirmIdToContactIdMap.put(auditTeamCon.KARL_Audit_Team__r.KARL_Audit_Firm__c,new Set<Id>());
                }
                auditFirmIdToContactIdMap.get(auditTeamCon.KARL_Audit_Team__r.KARL_Audit_Firm__c).add(auditTeamCon.KARL_Contact__c);  
            }
            
        }
        addRemoveAuditorReqItemSharing(auditFirmIdToContactIdMap,'Add');
    }

    /**
     * @author Mahima Aggarwal
     * @email mahima.aggarwal@mtxb2b.com
     * @desc This method unshare all the auditor request items of the firm from which audit team 
     * contact belong after audit team contact is deleted
    */
    private static void unShareAuditorReqItemBasedOnFirm(List<KARL_Audit_Team_Contacts__c> oldList){
        Map<String,Set<Id>> contactFirmIdToTeamIdMap = new Map<String,Set<Id>>();
        Set<Id> contactIdSet = new Set<Id>();
        Map<Id,KARL_Audit_Team_Contacts__c> auditTeamConIdToAuditTeamConMap = new Map<Id,KARL_Audit_Team_Contacts__c>();
        Map<Id,Set<Id>> auditFirmIdToContactIdMap = new Map<Id,Set<Id>>();
       
        for(KARL_Audit_Team_Contacts__c auditTeamCon : oldList){
            contactIdSet.add(auditTeamCon.KARL_Contact__c);
        }

        //used to get contact belong to how many teams of a particular firm
        for(KARL_Audit_Team_Contacts__c auditTeamCon : [SELECT Id,KARL_Audit_Team__c,KARL_Contact__c,
                                                        KARL_Audit_Team__r.KARL_Audit_Firm__c
                                                        FROM KARL_Audit_Team_Contacts__c 
                                                        WHERE KARL_Contact__c IN : contactIdSet AND
                                                        KARL_Audit_Team__r.KARL_Audit_Firm__c != null]){ 
            if(!contactFirmIdToTeamIdMap.containsKey(auditTeamCon.KARL_Contact__c + '-'+ auditTeamCon.KARL_Audit_Team__r.KARL_Audit_Firm__c)){
                contactFirmIdToTeamIdMap.put(auditTeamCon.KARL_Contact__c + '-'+ auditTeamCon.KARL_Audit_Team__r.KARL_Audit_Firm__c,new Set<Id>());
            }
            contactFirmIdToTeamIdMap.get(auditTeamCon.KARL_Contact__c + '-'+ auditTeamCon.KARL_Audit_Team__r.KARL_Audit_Firm__c).add(auditTeamCon.KARL_Audit_Team__c);  

            auditTeamConIdToAuditTeamConMap.put(auditTeamCon.Id,auditTeamCon);
        }

        System.debug('auditTeamConIdToAuditTeamConMap>>'+auditTeamConIdToAuditTeamConMap);

        //checking that if the deleting contact belong to only 1 team then access can be revoked
        for(KARL_Audit_Team_Contacts__c auditTeamCon : oldList){
            System.debug('auditTeamConID>>'+auditTeamCon.Id);
            KARL_Audit_Team_Contacts__c auditTeamContact = auditTeamConIdToAuditTeamConMap.get(auditTeamCon.Id);
            Set<Id> teamsOfContact = contactFirmIdToTeamIdMap.get(auditTeamContact.KARL_Contact__c + '-'+ auditTeamContact.KARL_Audit_Team__r.KARL_Audit_Firm__c);
            //if there is only one team then adding the contact along with its firm to map
            //this map is then used to revoke access using utility
            if(teamsOfContact != null && teamsOfContact.size() == 1){
                System.debug('auditTeamContact>>'+auditTeamContact);
                if(auditTeamContact.KARL_Audit_Team__c != null && 
                   auditTeamContact.KARL_Audit_Team__r.KARL_Audit_Firm__c != null){
                    if( !auditFirmIdToContactIdMap.containsKey(auditTeamContact.KARL_Audit_Team__r.KARL_Audit_Firm__c)){
                        auditFirmIdToContactIdMap.put(auditTeamContact.KARL_Audit_Team__r.KARL_Audit_Firm__c,new Set<Id>());
                    }
                    auditFirmIdToContactIdMap.get(auditTeamContact.KARL_Audit_Team__r.KARL_Audit_Firm__c).add(auditTeamContact.KARL_Contact__c);
                }
            }
        }
        addRemoveAuditorReqItemSharing(auditFirmIdToContactIdMap,'Remove');
    }

    /**
     * @author Mahima Aggarwal
     * @email mahima.aggarwal@mtxb2b.com
     * @desc Does sharing using batch
    */
    public static void addRemoveAuditorReqItemSharing(Map<Id,Set<Id>> auditFirmIdToContactIdMap,String action){
        if(auditFirmIdToContactIdMap.keySet() != null && !auditFirmIdToContactIdMap.keySet().isEmpty()){
            KARL_ShareAuditorReqItemToAuditFirmBatch objBatch = new KARL_ShareAuditorReqItemToAuditFirmBatch('Trigger',auditFirmIdToContactIdMap.keySet(),auditFirmIdToContactIdMap,action);
            Database.executebatch(objBatch,100);
        }
    }
}
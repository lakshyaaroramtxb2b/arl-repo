public class KARL_CycleAuditReportsTriggerHandler {
    public static void beforeUpdateOperations(List<KARL_Cycle_Audit_Report_Items__c> newList,Map<Id,KARL_Cycle_Audit_Report_Items__c> oldMap){
        KARL_CycleAuditReportsTriggerHelper.updatePublishDate(newList,oldMap);
		KARL_CycleAuditReportsTriggerHelper.validationForFinalizationReadiness(newList,oldMap);
        KARL_CycleAuditReportsTriggerHelper.moveToReadyToSignStatusOnFinalizationReadiness(newList,oldMap);
    }
    public static void afterUpdateOperations(List<KARL_Cycle_Audit_Report_Items__c> newList,Map<Id,KARL_Cycle_Audit_Report_Items__c> newMap,Map<Id,KARL_Cycle_Audit_Report_Items__c> oldMap){
        KARL_CycleAuditReportsTriggerHelper.createTaskAndChatter(newList,oldMap);
        KARL_CycleAuditReportsTriggerHelper.createChatterOnIssued(newList,oldMap);
        KARL_CycleAuditReportsTriggerHelper.createChatterOnFinalReview(newList,oldMap);
        KARL_CycleAuditReportsTriggerHelper.createChatterOnPublish(newList,oldMap);
        KARL_CycleAuditReportsTriggerHelper.sendEmailOnStatusChange(newList,oldMap);
        KARL_CycleAuditReportsTriggerHelper.lockRecordOnPublish(newList,oldMap);
    }
    
}
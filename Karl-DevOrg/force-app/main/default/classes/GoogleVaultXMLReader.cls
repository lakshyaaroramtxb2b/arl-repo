public class GoogleVaultXMLReader {

    public class Email {
        Public String frm;
        Public String to;
        Public String cc;
        Public String bcc;
        Public String subject;
        Public String datesent;
        Public String daterecvd;
        Public String label;
        Public String status;
        Public Id contactid;
        Public String cemail;
    }
    
    public Email[] parseEmails(string strXml){
        Email[] emails = new Email[0];
        Dom.Document doc = new Dom.Document();
        doc.load(strXml);
        Dom.XMLNode Envelope = doc.getRootElement();  
        Dom.XMLNode Body = Envelope.getChildElements()[0];
        string user_createResult = ''; 
        for(Dom.XMLNode child : Body.getChildElements()) {
            for(Dom.XMLNode subchild : child.getChildElements()) {

                if (subchild.getName() == 'Document'){
                Email email = new Email();
                for (Dom.XMLNode docu : subchild.getChildElements()) {
                    for (Dom.XMLNode tag : docu.getChildElements()){
                        if (tag.getName() == 'Tag'){
                            if (tag.getAttributeCount() > 0) { 
                                for (Integer i = 0; i< tag.getAttributeCount(); i++ ) {
                                    if (tag.getAttributeKeyAt(i) == 'TagName'){
                                        if (tag.getAttributeValue(tag.getAttributeKeyAt(i), tag.getAttributeKeyNsAt(i)) == 'Labels') {
                                            String[] labels = tag.getAttributeValue(tag.getAttributeKeyAt(i+2), tag.getAttributeKeyNsAt(i+2)).split(',');
                                            Set<String> labelSet = new Set<String>(labels);
                                            System.debug('LABELSET: ' + labelSet);
                                            if (labelSet.contains('^SPAM')) {
                                                email.label = 'SPAM';
                                            } else {
                                                email.label = 'INBOX';
                                            }
                                            
                                            if (labelSet.contains('^OPENED')) {
                                                email.status = 'Opened';
                                            } else {
                                                email.status = 'Unread';
                                            }
                             
                                        } else if (tag.getAttributeValue(tag.getAttributeKeyAt(i), tag.getAttributeKeyNsAt(i)) == '#From') {
                                            email.frm = tag.getAttributeValue(tag.getAttributeKeyAt(i+2), tag.getAttributeKeyNsAt(i+2));                            
                                        } else if (tag.getAttributeValue(tag.getAttributeKeyAt(i), tag.getAttributeKeyNsAt(i)) == '#To') {
                                            email.to = tag.getAttributeValue(tag.getAttributeKeyAt(i+2), tag.getAttributeKeyNsAt(i+2));
                                        } else if (tag.getAttributeValue(tag.getAttributeKeyAt(i), tag.getAttributeKeyNsAt(i)) == '#CC') {
                                            email.cc = tag.getAttributeValue(tag.getAttributeKeyAt(i+2), tag.getAttributeKeyNsAt(i+2));
                                        } else if (tag.getAttributeValue(tag.getAttributeKeyAt(i), tag.getAttributeKeyNsAt(i)) == '#BCC') {
                                            email.bcc = tag.getAttributeValue(tag.getAttributeKeyAt(i+2), tag.getAttributeKeyNsAt(i+2));
                                        } else if (tag.getAttributeValue(tag.getAttributeKeyAt(i), tag.getAttributeKeyNsAt(i)) == '#Subject') {
                                            email.subject = tag.getAttributeValue(tag.getAttributeKeyAt(i+2), tag.getAttributeKeyNsAt(i+2));
                                        } else if (tag.getAttributeValue(tag.getAttributeKeyAt(i), tag.getAttributeKeyNsAt(i)) == '#DateSent') {
                                            email.datesent = tag.getAttributeValue(tag.getAttributeKeyAt(i+2), tag.getAttributeKeyNsAt(i+2));
                                        } else if (tag.getAttributeValue(tag.getAttributeKeyAt(i), tag.getAttributeKeyNsAt(i)) == '#DateReceived') {
                                            email.daterecvd = tag.getAttributeValue(tag.getAttributeKeyAt(i+2), tag.getAttributeKeyNsAt(i+2));
                                        }
                           
                                    }
                                    
                                }  
                            }
                            
                        }
                         
                    }
                }
                
                 if (email.to.length() > 1) {
                     email.cemail = email.to.split('\\s')[0];
                 } else if (email.cc.length() > 1) {
                     email.cemail = email.cc.split('\\s')[0];
                 } else if (email.bcc.length() > 1) {
                     email.cemail = email.bcc.split('\\s')[0];
                 }
                 

                    emails.add(email);

            }
            }
        }
        System.debug('RETURNEMAILS');
        return emails;
    }
}
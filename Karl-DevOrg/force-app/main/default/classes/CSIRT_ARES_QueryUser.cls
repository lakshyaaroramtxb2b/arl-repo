@RestResource(urlMapping='/ARES/QueryUser/*')
global with sharing class CSIRT_ARES_QueryUser {
    global class QueryResponse {
        public String userid;
        public String orgid;
        public String status;
        public Map<String,String> result;
        
        public QueryResponse(String orgid, String userid){
            this.userid = userid;
            this.orgid = orgid;
            System.debug('Userid: '+ userid);
            System.debug('orgid: '+ orgid);

        }
        
        public void Errored(){
            this.status = 'ERROR';
            System.debug('Cannot Query ' + userid + ' in Org: ' + orgid);
        }
        
        public void Success(Map<String,String> btResult){
            this.status = 'SUCCESS';
            this.result = btResult;
        }
    }

    
	@HttpPost 
	global static QueryResponse queryUser(String customerOrgId, String usersId) {
        Map<String,String> btResult = new Map<String,String>();
		QueryResponse response = new QueryResponse(customerOrgId, usersId);
        
        try{
        	btResult = SfdcOps.getCustomerInfo(customerOrgId, usersId);
        }
        catch(Exception e){
        	response.Errored();
            return response;
        }
        
        response.Success(btResult);
        return response;
        
   }
}
public without sharing class TD_StakeholderChatterHelper {
 
    public static final String STAKEHOLDER_REMOVED_CHATTER_TITLE = 'Stakeholder Removed';     
    public static final String STAKEHOLDER_IS_DONE_TITLE = 'Stakeholder Done';     
    public static void updateDenormilizeData(List<Trust_Deliverable_Stakeholder__c> TD_stakeholders) {  
    	updateDenormilizeData(TD_stakeholders, false);  
	}    
	
	public static void updateDenormilizeData(List<Trust_Deliverable_Stakeholder__c> TD_stakeholders, Boolean isDelete) {    
         
        // set denormalize stakeholder info in deliverable 
        Map<Id, Id> accountableStakeholder = new Map<Id, Id>(); 
        Map<Id, Set<String>> deliverableStakeholders = new Map<Id, Set<String>>();
            
        for (Trust_Deliverable_Stakeholder__c tds : TD_stakeholders) {
        	// identify Accountable stakeholder
        	if (tds.RACI_Level__c != Null) {
	            if (tds.RACI_Level__c.contains(TD_Deliverable_Form.ACCOUNTABLE))
	                accountableStakeholder.put(tds.Deliverable__c, tds.Stakeholder__c); 
        	}
                
            // create map of deliverable with list of stakeholders    
            if(!deliverableStakeholders.containsKey(tds.Deliverable__c)) 
                deliverableStakeholders.put(tds.Deliverable__c, new set<String>());
            deliverableStakeholders.get(tds.Deliverable__c).add(tds.Stakeholder__c);    
        } 
        
        Map<Id, Trust_Deliverable__c> deliverablesToUpdate = new Map<Id, Trust_Deliverable__c>();
        set<String> existingStakeholders = new set<String>();
        
        // loop through all the parent deliverables and update denormalized data
        for (Trust_Deliverable__c d : [select Id, StakeHolders__c from Trust_Deliverable__c 
                                       where Id IN : deliverableStakeholders.keySet()]) {
            if (accountableStakeholder.containsKey(d.Id))                               
                d.Accountable_Stakeholder__c = isDelete? null : accountableStakeholder.get(d.Id); 
            if (d.StakeHolders__c != null) existingStakeholders.addAll(d.StakeHolders__c.split(':',0)); 
            if (isDelete) existingStakeholders.removeAll(deliverableStakeholders.get(d.Id));  
                else existingStakeholders.addAll(deliverableStakeholders.get(d.Id)); 
            d.StakeHolders__c = string.join(new list<String>(existingStakeholders), ':'); 
            deliverablesToUpdate.put(d.Id, d);                          
        }
        
        Database.SaveResult[] sr = database.update(deliverablesToUpdate.values(), false);
                        
        }
  

    public static void addStakeholderFromChatterFeed (Map<Id, Trust_Deliverable_Stakeholder__c> TD_stakeholders) {       
                
        // add apex share for stakeholder (must be done first to allow them to be added to entitysubscription)
        List<Trust_Deliverable__Share> td_shares = new List<Trust_Deliverable__Share>();
        for (Trust_Deliverable_Stakeholder__c tds : TD_stakeholders.values()) {
            Trust_Deliverable__Share td_share = new Trust_Deliverable__Share(
               ParentId = tds.Deliverable__c,
               UserOrGroupId = tds.Stakeholder__c,
               AccessLevel = 'edit',
               RowCause = Schema.Trust_Deliverable__Share.RowCause.Stakeholder__c
               );
            td_shares.add(td_share); 
        }
        Database.SaveResult[] td_shares_sr = database.insert(td_shares, false);
        
        // add stakeholder as a chatter follower (only if responsible or accountable)
        List<EntitySubscription> subs = new List<EntitySubscription>();
        for (Trust_Deliverable_Stakeholder__c tds : TD_stakeholders.values()) {
        	if (tds.RACI_Level__c != Null) {
                if (tds.RACI_Level__c.contains(TD_Deliverable_Form.RESPONSIBLE) || 
                    tds.RACI_Level__c.contains(TD_Deliverable_Form.ACCOUNTABLE)) {        	
		            EntitySubscription sub = new EntitySubscription(
		               ParentId = tds.Deliverable__c,
		               SubscriberId = tds.Stakeholder__c
		               );
	                subs.add(sub); 
                }
        	}
        }        
        Database.SaveResult[] subs_sr = database.insert(subs, false);
        
        // this event creates a chatter feed of CreateRecordEvent which does not call the feeditem trigger
        // since we need to nofify stakeholders of all feeditem we need to force that here
        TD_NotifyStakeholderOfChatterFeeds.emailStakeHolders(TD_stakeholders.keySet());                              
    
    }
        	
	public static void removeStakeholderFromChatterFeed (List<Trust_Deliverable_Stakeholder__c> TD_stakeholders) {       

        // build sets for matching stakeholders to deliverables that they are a part of
        Set<Id> deliverableIds = new Set<Id>();
        Set<Id> stakeholderIds = new Set<Id>();
        Map<Id, User> stakeholderUsers = new Map<Id, User>();
        Set<String> deliverableStakeholder = new Set<String>();
        for (Trust_Deliverable_Stakeholder__c tds : TD_stakeholders) {
            deliverableIds.add(tds.Deliverable__c); 
            stakeholderIds.add(tds.Stakeholder__c); 
            deliverableStakeholder.add(String.valueOf(tds.Deliverable__c) + String.valueOf(tds.Stakeholder__c));        
        } 
        try {
            stakeholderUsers = new Map<Id, User>([select Name from User where Id IN : stakeholderIds]);
        } catch (exception e) {}
              

        // remove stakeholder as a follower 
        List<EntitySubscription> subs = new List<EntitySubscription>();        
        if (!deliverableIds.isEmpty()) {
	        for (EntitySubscription sub : [select SubscriberId, ParentId from EntitySubscription 
	                                       where ParentId In :deliverableIds LIMIT 500]) {
	            String key = String.valueOf(sub.ParentId) + String.valueOf(sub.SubscriberId);                           
	            if (deliverableStakeholder.contains(key)) {
	                subs.add(sub);                          
	            }                           	                                        
	        }
        }

        if (!subs.isEmpty()) {
            Database.DeleteResult[] sub_sr = database.delete(subs, false);
        }

        // remove stakeholder from apex sharing
        List<Trust_Deliverable__Share> td_shares = new List<Trust_Deliverable__Share>();
        if (!deliverableIds.isEmpty()) {
            for (Trust_Deliverable__Share td_share : [select UserOrGroupId, ParentId from Trust_Deliverable__Share 
                                           where ParentId In :deliverableIds]) {
                String key = String.valueOf(td_share.ParentId) + String.valueOf(td_share.UserOrGroupId);                           
                if (deliverableStakeholder.contains(key)) {
                    td_shares.add(td_share);                          
                }                                                                       
            }
        }

        if (!td_shares.isEmpty()) {
            Database.DeleteResult[] td_share_sr = database.delete(td_shares, false);
        }
        
        // create feeditems as these are not automatically created on a delete
        List<FeedItem> td_feeds = new List<FeedItem>();
        for (Trust_Deliverable_Stakeholder__c tds : TD_stakeholders) {
        	FeedItem td_feed = new FeedItem (
        	   ParentId = tds.Deliverable__c,
        	   Type = 'TextPost',
        	   Title = STAKEHOLDER_REMOVED_CHATTER_TITLE,
        	   Body = stakeholderUsers.containsKey(tds.Stakeholder__c)? 
        	       (STAKEHOLDER_REMOVED_CHATTER_TITLE + ': ' + stakeholderUsers.get(tds.Stakeholder__c).Name) : ''
        	);
        	td_feeds.add(td_feed);
        }  

        if (!td_feeds.isEmpty()) {
            insert td_feeds;
        }
        
        // remove stakeholder from denormalized list of stakeholders
        set<Id> trustDeliverableIds = new set<Id>();
        Map<Id, Trust_Deliverable__c> deliverablesToUpdate = new Map<Id, Trust_Deliverable__c>();
        
        for (Trust_Deliverable_Stakeholder__c tds : TD_stakeholders) trustDeliverableIds.add(tds.Deliverable__c);
        
        if (!trustDeliverableIds.isEmpty()) {
	        for (Trust_Deliverable__c d : [select Id, StakeHolders__c from Trust_Deliverable__c 
	                                       where Id IN : trustDeliverableIds]) {
	            // convert string to set                            
	            set<String> existingStakeholders = new set<String>();
	            if (d.StakeHolders__c != null) existingStakeholders.addAll(d.StakeHolders__c.split(':',0));  
	            // remove deleted stakeholder from set
	            existingStakeholders.remove(d.StakeHolders__c); 
	            // convert string back to set and 
	            d.StakeHolders__c = string.join(new list<String>(existingStakeholders), ':'); 
	            deliverablesToUpdate.put(d.Id, d);                                                                      
	        } 
        }
        
        Database.SaveResult[] sr = database.update(deliverablesToUpdate.values(), false);                  
            
    }

}
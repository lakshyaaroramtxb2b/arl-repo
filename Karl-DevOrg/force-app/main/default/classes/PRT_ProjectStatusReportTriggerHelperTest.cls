/***********************************************************
* Test class for PRT_ProjectStatusReportTriggerHelper
* Created by    - Prashant Gupta
************************************************************/
@isTest
public class PRT_ProjectStatusReportTriggerHelperTest {
     @testSetup
    public static void testSetup(){
        List<User> userList = new List<User>();
        userList = PRT_TestDataFactory.createUsers(3,'System Administrator',true);
    }
    static testMethod void populateV2MOMandMeasureTest(){
        
        List<User> userList = new List<User>([SELECT Id FROM USER WHERE Email Like 'PRTUser%']);
        List<Program__c> programList = new List<Program__c>(PRT_TestDataFactory.createPrograms(3,true));
        List<Project__c> projectList = new List<Project__c>(PRT_TestDataFactory.createBussinessCase(2,false));
        
        for(integer i=0; i<2; i++){
            projectList[i].Program__c = programList[i].id;
            projectList[i].Status__c =PRT_Constants.PRT_PROJECT_STATUS_SCORED_BUSINESS_CASE;
            projectList[i].Mandate__c = '2';
            projectList[i].CSF_Impact__c = '2';
            projectList[i].Strategy_Alignment__c = '3';
            //projectList[i].V2MOM_and_Measure__c = 'testV2MOMandMeasure';
        }
        insert projectList;
        
        List<Project_Status_Report__c> psrList = new List<Project_Status_Report__c>();
        psrList =  PRT_TestDataFactory.createProjectStatusReport(2,false,'On Track','Green','Green');
        for(Integer i=0;i<2;i++){
            psrList[i].project__c = projectList[i].Id;
            psrList[i].program__c = programList[i].Id;
            psrList[i].Submitted__c = False;
           
        }
        insert psrList;
         
        test.startTest();
        
        for(Project__c pro : projectList){
            //pro.V2MOM_and_Measure__c = 'TestV2';
        }
        PRT_Constants.RECURSIVE_TRIGGER_CHECK = true;
        update projectList;
        
        
         List<Project_Status_Report__c> updatedPSR = new List<Project_Status_Report__c>();
         Map<id,Project__c> newMap = new Map<id,Project__c>(projectList);
         for(Project_Status_Report__c psr : [SELECT ID,
                                                Project__c FROM Project_Status_Report__c 
                                               WHERE Project__c IN :projectList]){
                
                    //psr.V2MOM_and_Measure__c =  newMap.get(psr.Project__c).V2MOM_and_Measure__c;
                    updatedPSR.add(psr);
                
            }
        
        PRT_Constants.RECURSIVE_TRIGGER_CHECK = true;
        update updatedPSR;

        
        
        test.stopTest();
                
        for(Project_Status_Report__c psr : [SELECT ID
                                            FROM Project_Status_Report__c 
                                            WHERE Project__c IN :projectList]){
             //System.assertEquals(psr.V2MOM_and_Measure__c,'TestV2');                                   
                                                
        }
      
    }
    
    static testMethod void checkRequiredFieldsTest(){
         List<User> userList = new List<User>([SELECT Id FROM USER WHERE Email Like 'PRTUser%']);
        List<Program__c> programList = new List<Program__c>(PRT_TestDataFactory.createPrograms(3,true));
        List<Project__c> projectList = new List<Project__c>(PRT_TestDataFactory.createBussinessCase(2,false));
        
        for(integer i=0; i<2; i++){
            projectList[i].Program__c = programList[i].id;
            projectList[i].Status__c =PRT_Constants.PRT_PROJECT_STATUS_SCORED_BUSINESS_CASE;
            projectList[i].Mandate__c = '2';
            projectList[i].CSF_Impact__c = '2';
            projectList[i].Strategy_Alignment__c = '3';
            //projectList[i].V2MOM_and_Measure__c = 'testV2MOMandMeasure';
        }
        insert projectList;
        
        List<Project_Status_Report__c> psrList = new List<Project_Status_Report__c>();
        psrList =  PRT_TestDataFactory.createProjectStatusReport(2,false,'On Track','Green','Green');
        for(Integer i=0;i<2;i++){
            psrList[i].project__c = projectList[i].Id;
            psrList[i].program__c = programList[i].Id;
            psrList[i].Submitted__c = True;           
        }
        insert psrList;
        test.startTest();
        psrList[0].Approved__c = true;
            update psrList;
        test.stopTest();  
    }
}
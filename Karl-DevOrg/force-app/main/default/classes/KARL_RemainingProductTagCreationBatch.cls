/**
* @author Lakshya Arora
* @email lakshya.arora@mtxb2b.com
* @desc This is schedulable and stateful batch class to create Internal Gus Product Tag Records.
* @note: WILL ONLY RUN AFTER POST PRODUCTION ONE TIME IFF KARL_GUSProductTagDATACREATIONBATCH fails to create all the data.
* Without sharing as it needs to access all the remaining active product tag
*/
public class KARL_RemainingProductTagCreationBatch implements Database.Batchable<SObject>,Database.Stateful, Database.AllowsCallouts,Schedulable{
        Set<Id> existingGusProductTagIdSet = new Set<Id>(); 
        public Database.QueryLocator start(Database.BatchableContext BC){       
            String query = 'SELECT Id,ExternalId,Description_c__c,Active_c__c,Name__c,Migration_PA_c__c,Migration_MFA_c__c,Team_c__c,Team_Tag_Key_c__c,Use_for_Automated_Tools_c__c'
            +' FROM ADM_Product_Tag_c__x'
            +' WHERE Active_c__c = true';
            return Database.getQueryLocator(query);
        }
    
    /**
* @author Lakshya Arora
* @email lakshya.arora@mtxb2b.com
* @desc This method is for schedulable interface to execute.
*/
    
    public void execute(Database.BatchableContext BC, List<ADM_Product_Tag_c__x> externalProductTagRecordList) {
        Map<String,Boolean> scrumTeamIdToActiveMap = new Map<String,Boolean>();
        Map<String,ADM_Product_Tag_c__x> externalIdToProductTagMap = new Map<String,ADM_Product_Tag_c__x>();
        List<KARL_Gus_Product_Tag__c> remainingProductTagList = new List<KARL_Gus_Product_Tag__c>();
        Set<String> scrumTeamExternalIdSet = new Set<String>();
        if(existingGusProductTagIdSet.isEmpty()){
            for(KARL_Gus_Product_Tag__c gusProdTag : [SELECT Id,Product_Tag_External_Id__c 
                                                        FROM KARL_Gus_Product_Tag__c 
                                                        WHERE Active__c = TRUE 
                                                        WITH SECURITY_ENFORCED
                                                        LIMIT 50000]){
            existingGusProductTagIdSet.add(gusProdTag.Product_Tag_External_Id__c);
            }
        }
            
        
       
        System.debug('size = '+existingGusProductTagIdSet.size());
        for(ADM_Product_Tag_c__x prodTag : externalProductTagRecordList){
            if(!existingGusProductTagIdSet.contains(prodTag.ExternalId)){
                externalIdToProductTagMap.put(prodTag.ExternalId,prodTag);
                scrumTeamExternalIdSet.add(prodTag.Team_c__c);
            }
        }
        if(!scrumTeamExternalIdSet.isEmpty()){
            //No Security Enforced due to External Object
            for(ADM_Scrum_Team_c__x scrumTeamObj : [SELECT Id,ExternalId ,Active_c__c 
                                                    FROM ADM_Scrum_Team_c__x 
                                                    WHERE ExternalId IN:scrumTeamExternalIdSet
                                                   ]){
                scrumTeamIdToActiveMap.put(scrumTeamObj.ExternalId,scrumTeamObj.Active_c__c);
            }
        }   
                
        
        for(String externalId : externalIdToProductTagMap.keySet()){
            KARL_GUS_Product_Tag__c gusProdTagObj = new KARL_GUS_Product_Tag__c();
            ADM_Product_Tag_c__x prodTag= externalIdToProductTagMap.get(externalId);
            gusProdTagObj.Name  = prodTag.Name__c;
                gusProdTagObj.Active__c   = prodTag.Active_c__c; 
                gusProdTagObj.Description__c  = prodTag.Description_c__c; 
                gusProdTagObj.Migration_MFA__c  = prodTag.Migration_MFA_c__c; 
                gusProdTagObj.Migration_PA__c  =  prodTag.Migration_PA_c__c;
                gusProdTagObj.Team__c  = prodTag.Team_c__c;
                gusProdTagObj.Team_Tag_Key__c  =  prodTag.Team_Tag_Key_c__c;
                gusProdTagObj.Use_for_Automated_Tools__c  =  prodTag.Use_for_Automated_Tools_c__c;
                if(scrumTeamIdToActiveMap.containsKey(prodTag.Team_c__c)){
                    gusProdTagObj.Active_Scrum_Team__c = scrumTeamIdToActiveMap.get(prodTag.Team_c__c);
                }
                gusProdTagObj.Product_Tag_External_Id__c = prodTag.ExternalId;
                existingGusProductTagIdSet.add(prodTag.ExternalId);
                remainingProductTagList.add(gusProdTagObj);
        }
        System.debug('remainingProductTagList= '+remainingProductTagList);
        System.debug('remainingProductTagList= '+remainingProductTagList.size());
        if(!remainingProductTagList.isEmpty())
        insert remainingProductTagList;        
    }
    
    public void finish(Database.BatchableContext BC) {
        System.debug(LoggingLevel.DEBUG, 'Batch Completed');
    }   
    
    public void execute(SchedulableContext sc) {
        Database.executebatch(new KARL_RemainingProductTagCreationBatch(),200);
    }
}
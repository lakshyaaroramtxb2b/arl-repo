@RestResource(urlMapping='/validate/*')
global without sharing class CxValidateReferral {
    //without sharing because we only return a boolean based on a 
    //randomly generated number. This is for CX to verify our referrals.
   
    @HttpGet
    global static String doGet() {
        
        RestRequest req = RestContext.request;
		RestResponse res = RestContext.response;
 
		String token = RestContext.request.headers.get('Token');
        if (!authenticateRequest(token)) {
            return outputMessage(handleAuthFailure());
        }
        
        res.addHeader('Content-Type', 'application/json');
		String external_code = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
        if (!String.isNotBlank(external_code)) {
            res.statusCode = 400;
            return outputMessage('no verification code provided');
        }        
        return outputMessage(testCode(external_code));
    }
    
    private static String testCode(String testCode) {
        CxReferral__c[] objs = [SELECT Id, code__c FROM CxReferral__c WHERE code__c=:testCode LIMIT 1];

        if (objs.size() > 0) {
            return 'valid';
        } else {
            return 'invalid';
        }
        
    } 
    
    private static boolean authenticateRequest(String token) {
        String authToken = 'hjKcP9JTvdF93WUoXE5SIQZ4f';
        if (String.isNotBlank(token)) {
            return authToken.equalsIgnoreCase(token.trim());        
        } else {
        	return false;
        }
    }

    private static String handleAuthFailure() {
        RestContext.response.statusCode = 401;
        return 'authentication failure';     
    }


    private static String outputMessage(String message) {
        JSONGenerator gen = JSON.createGenerator(true);
		gen.writeStartObject();
        gen.writeStringField('response', message);
        gen.writeEndObject();
		return gen.getAsString();
    }
    

}
public with sharing class GetDashRSSFeed 
{
    public String jsonString {get;set;}

    //Constructor
    public GetDashRSSFeed()
    {
        jsonString = prepareData();
    }

    //Temp Method to prepare the Data
    private String prepareData()
    {
         HttpRequest req = new HttpRequest();
         req.setEndpoint('http://www.theregister.co.uk/security/headlines.atom');
         req.setMethod('GET');
        
         Http http = new Http();
         HTTPResponse res = http.send(req);
         
         return res.getBody();
    }
}
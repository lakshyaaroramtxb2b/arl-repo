/*
 * schedule class to support the escalation and notification of trail completion
*/
global class SC_EscalationSchedule implements Schedulable {

    global void execute(SchedulableContext sc) {
        Database.executeBatch(new SC_EscalationBatch());
    }
}
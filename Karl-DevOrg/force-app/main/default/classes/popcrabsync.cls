public without sharing class popcrabsync {

    public popcrabsync() {}

    public void copyFromScanInfoToScanQueue() {
        
        List<Scan_Queue__c> sqToUpdate = new List<Scan_Queue__c>();
        for (Scan_Queue__c sq : [select Id, Queue_Size__c, Line_Count__c, Scan_Info__c, Scan_Start__c from Scan_Queue__c where Scan_Info__c != null]) {
            Scan_Info__c scanInfo = [select Queue_Size__c, Line_Count__c, Scan_Start__c from Scan_Info__c where Id =: sq.Id];
            sq.Queue_Size__c = scanInfo.Queue_Size__c;
            sq.Line_Count__c = scanInfo.Line_Count__c;
            sq.Scan_Start__c = scanInfo.Scan_Start__c;
            sqToUpdate.add(sq);
        }
        update sqToUpdate;
    }

}
@isTest
private class Metroid_Tests {
	
    static ESA_Security_Request__c request1;
    static ESA_Security_Request__c request2;
    static ESA_Security_Request__c request3;
    static ESA_Security_Request__c request4;
    static ESA_Service_Item__c serviceItem1;
    static ESA_Service_Item__c serviceItem2;
    static ESA_Service_Item_Category__c serviceItemCategory1;
    static ESA_Service_Item_Category__c serviceItemCategory2;
    static ESA_Security_Question__c question;
    static ESA_Setting__c es;

    static List<ESA_Security_Question__c> questionsInventory;    
    static ESA_Security_Question__c questionCheckbox;
    static ESA_Security_Question__c questionDate;
    static ESA_Security_Question__c questionEmail;
    static ESA_Security_Question__c questionPhone;
    static ESA_Security_Question__c questionPicklist;
    static ESA_Security_Question__c questionText;
    static ESA_Security_Question__c questionLongText;
    static ESA_Security_Question__c questionAttachment;
    
         
    static final String TYPE_CHECKBOX = 'Checkbox';
    static final String TYPE_DATE = 'Date';
    static final String TYPE_EMAIL = 'Email';
    static final String TYPE_PHONE = 'Phone';
    static final String TYPE_PICKLIST  = 'Picklist';
    static final String TYPE_TEXT = 'Text (255)';
    static final String TYPE_LONGTEXT = 'Text Area (Long)';
    static final String TYPE_ATTACHMENT = 'Attachment';
    
    static final Boolean ANSWER_CHECKBOX = true;
    static final Boolean ANSWER_CHECKBOX2 = false;
    static final Date ANSWER_DATE = system.now().date();
    static final Date ANSWER_DATE2 = system.now().date().addDays(1);
    static final String ANSWER_EMAIL = 'email@emailanswer.com';
    static final String ANSWER_EMAIL2 = 'email@emailanswermod.com';
    static final String ANSWER_PHONE = '5554441122';
    static final String ANSWER_PHONE2 = '5554442244';
    static final String ANSWER_PICKLIST = 'Value1';
    static final String ANSWER_PICKLIST2 = 'Value2';
    static final String ANSWER_TEXT = 'ANSWER_TEXT';
    static final String ANSWER_TEXT2 = 'ANSWER_TEXT_mod';
    static final String ANSWER_LONGTEXT = 'ANSWER_LONGTEXT';
    static final String ANSWER_LONGTEXT2 = 'ANSWER_LONGTEXT_mod';
    static final String ANSWER_ATTACHMENT = 'ANSWER_ATTACHMENT';

    static Case testCase = new Case(
        Description = 'The case description is this',
        Subject = 'The case subject is this'       
    );
    
    static CaseComment testCaseComment = new CaseComment(
        CommentBody = 'The case comment is this'
    );
    
    static {

    	es = new ESA_Setting__c(Name = 'ESA',Need_By_Date_Min_Business_Days__c = 5);
		es.Meeting_Hours_Monday__c = '13:30;14;14:30;15';
		es.Meeting_Hours_Tuesday__c = '15;15:30;16;16:30';
		es.Meeting_Hours_Wednesday__c = '13:30;14;14:30;15';
		es.Meeting_Hours_Thursday__c = '9;9:30;10;10:30';
		es.Meeting_Hours_Friday__c = '09:30;10;10:30;11';
		es.Enforce_Service_Item_Selection__c = true; 	
		insert es;



        serviceItemCategory1 = new ESA_Service_Item_Category__c(Name = 'Vendor Security',Office_hours_days__c = 'Monday;Tuesday;Thursday');
        insert serviceItemCategory1;

        serviceItemCategory2 = new ESA_Service_Item_Category__c(Name = 'Infrastructure Security',Office_hours_days__c = 'Wednesday;Friday');
        insert serviceItemCategory2;        

        serviceItem1 = new ESA_Service_Item__c(ItemCategory__c = serviceItemCategory1.Id,Name = 'New Vendor Request');
        insert serviceItem1;

        serviceItem2 = new ESA_Service_Item__c(ItemCategory__c = serviceItemCategory2.Id,Name = 'Network Architecture Review');
        insert serviceItem2;

        datetime dt = datetime.now();

        request1 = new ESA_Security_Request__c(Name__c = 'test class',Priority__c = 'high',Phone__c = '5555551144',Email__c = 'test@test.com',Description__c = 'test class is testing',Hash_Key__c = '1234567890',ESA_Service_Item__c = serviceItem1.Id);
        request1.Office_Hours__c = string.valueOf(dt.addDays(1));
        insert request1;
        request2 = new ESA_Security_Request__c(Name__c = 'test class',Priority__c = 'high',Phone__c = '5555551144',Email__c = 'test@test.com',Description__c = 'test class is testing',Hash_Key__c = '1234567890',ESA_Service_Item__c = serviceItem2.Id);
        request2.Office_Hours__c = string.valueOf(dt.addDays(7));
        insert request2;
        request3 = new ESA_Security_Request__c(Name__c = 'test class',Priority__c = 'high',Phone__c = '5555551144',Email__c = 'test@test.com',Description__c = 'test class is testing',Hash_Key__c = '1234567890',ESA_Service_Item__c = serviceItem1.Id);
        request3.Office_Hours__c = string.valueOf(dt.addmonths(-1).addDays(1));
        insert request3;
        request4 = new ESA_Security_Request__c(Name__c = 'test class',Priority__c = 'high',Phone__c = '5555551144',Email__c = 'test@test.com',Description__c = 'test class is testing',Hash_Key__c = '1234567890',ESA_Service_Item__c = serviceItem2.Id);
        request4.Office_Hours__c = string.valueOf(dt.addmonths(-1).addDays(7));
        insert request4;



    }

    static testMethod void AvailabilityTest1() {
		Metroid_Armory MA = new Metroid_Armory();
        MA.Scan_OH_Availability('New Vendor Request');
    }

    static testMethod void AvailabilityTest2() {
		Metroid_Armory MA = new Metroid_Armory();
        MA.Scan_OH_Availability('Network Architecture Review');  
    }    
	
    static testMethod void UtilizationTest1() {
		Metroid_Armory MA = new Metroid_Armory();
		MA.Scan_OH_Utilization(new List<string>{'Vendor Security','Application Security'},-1); 
    }

    static testMethod void UtilizationTest2() {
		Metroid_Armory MA = new Metroid_Armory();
		MA.Scan_OH_Utilization(new List<string>{'Infrastructure Security'},-1); 
    }


	static testMethod void SchedulerTest1() {
		String CRON_EXP = '0 0 0 15 3 ? 2022';
      	Test.startTest();
      	try{
      		String jobId = System.schedule('ScheduleApexClassTest', CRON_EXP,new Metroid_Scheduler());
      	} catch(exception e){
      		//boo
      	}
      	Test.stopTest();
	} 

    static testMethod void BatchAvailabilityTest1() {
        Test.StartTest();
        Metroid_Batch_Availability mba = new Metroid_Batch_Availability();
        ID batchprocessid = Database.executeBatch(mba);
        Test.StopTest();
    }       

    static testMethod void BatchUtilizationTest1() {
        Test.StartTest();
        Metroid_Batch_Utilization mbu = new Metroid_Batch_Utilization();
        ID batchprocessid = Database.executeBatch(mbu);
        Test.StopTest();
    }       

}
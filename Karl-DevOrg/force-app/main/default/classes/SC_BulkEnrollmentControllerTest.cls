/*
 * Tests for the controller used to power the bulk enrollment page.
 *
 */
@isTest
private class SC_BulkEnrollmentControllerTest {

/*    static testMethod void testBulkEnrollment() {
        SC_Training_Provider__c provider = new SC_Training_Provider__c();
        provider.Name = 'Trailhead';
        provider.Type__c = 'Trailhead';
        insert provider;

        Account acc = new Account(
            Name = 'TEST BULK ACCOUNT'
        );
        insert acc;

        Contact con = new Contact(
            AccountId = acc.Id,
            Company__c = 'Company',
            Copy_of_Original_Email__c = 'test.bulk.con.email@test.com',
            Email = 'test.bulk.con.email@test.com',
            Employee_Id__c = 'EID-1',
            Employee_Type__c = 'Contract',
            FirstName = 'TEST',
            LastName = 'TEST BULK CON',
            Manager_Email__c = 'test.test@test.com',
            Manager_ID__c = 'MID',
            JobCode__c = 'JC',
            Org62_User_Id__c = '01r3ATEST01OWMPQA4',
            RecordTypeId = [
                select Id
                from RecordType
                where Name = 'Salesforce Employee'
                and SobjectType = 'Contact' limit 1
            ].Id
        );
        insert con;

        Profile pro = [
            select Id
            from Profile
            where Name LIKE '%Portal User%'
            limit 1
        ];

        User testUser = new User(
            Alias = 'bulk',
            CommunityNickName = 'BulkTestUserForBulkEnroll',
            ContactId = con.Id,
            Email = 'test.bulk.con.email@test.com',
            EmailEncodingKey = 'ISO-8859-1',
            FirstName = 'BULK TEST',
            LastName = 'CONTACT',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            ProfileId = pro.Id,
            UserName = 'test.bulk.con.email@test.com',
            TimeZoneSidKey = 'America/New_York'
        );
        insert testUser;

        Training_Course__c tc = new Training_Course__c(
            Default_Due_Days__c = 30.0,
            Is_Active__c = true,
            Name = 'TEST_ONE_ON_ONE',
            Provider__c = 'Trailhead',
            Provider_Id__c = '1996',
            Tracked__c = true,
            URL__c = 'https://developer.salesforce.com/trailhead/module/one-on-one-meetings/'
        );
        insert tc;

        PageReference pageRef = Page.SC_BulkEnrollment;
        ApexPages.StandardController stdController =  new ApexPages.StandardController(tc);

        Test.setCurrentPage(pageRef);
        SC_BulkEnrollmentController scBulk = new SC_BulkEnrollmentController(stdController);
        scBulk.takenObj.Training_Course__c = tc.Id;
        List<SelectOption> emailTemplates = scBulk.emailTemplates;

        for (SelectOption opt :emailTemplates) {
            if (opt.getLabel() == 'SC_Training_Enrollment') {
                scBulk.emailTemplate = opt.getValue();
            }
        }

        scBulk.takenObj.Due_Date__c = System.today().addDays(20);
        scBulk.mandatoryCheckbox = true;
        scBulk.notifyCheckbox = true;
        scBulk.emailTextBox = con.Email + '\n123456789.test@test.com';

        Test.startTest();
        scBulk.processEmails();
        Test.stopTest();

        List<Training_Course_Taken__c> takenCourses = [
            select Id
            from Training_Course_Taken__c
            where Contact__c = :con.Id
        ];

        System.assert(scBulk.mandatoryCheckbox);
        System.assertEquals(
            1,
            takenCourses.size(),
            'Fail - No Training Course Taken Obj created under the test Contact'
        );

        System.assertEquals(
            1,
            scBulk.numOfBadEmails,
            'Fail - Bad Email Test'
        );

        System.assertEquals(
            1,
            scBulk.numOfContacts,
            'Fail - Number of Contacts Test'
        );
    }*/
}
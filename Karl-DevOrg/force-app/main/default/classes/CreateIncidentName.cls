public class CreateIncidentName {
    
    //Eventually filter this down to Just Incidents & Vulns
    private static List<Case> cases = [SELECT ResponseForce__Incident_Name_New__c FROM Case WHERE ResponseForce__Incident_Name_New__c != Null];
 
    private static Set<String> getExistingNames(){
        Set<String> incidentNames = new Set<String>();
        for (Case c : cases){
            incidentNames.add(c.ResponseForce__Incident_Name_New__c);
        }
        
        return incidentNames;
    }
    
    public static String getName(Case c){
        Map<String,Id> wordTypes = IRCloud.IRCloud_Utils.getRecordTypeMapForObjectGeneric(ResponseForce__Word_Dictionary__c.SObjectType);
    
        List<ResponseForce__Word_Dictionary__c> adjectives = [SELECT ResponseForce__Word__c FROM ResponseForce__Word_Dictionary__c WHERE RecordTypeId = :wordTypes.get('Adjective')];
        List<ResponseForce__Word_Dictionary__c> nouns = [SELECT ResponseForce__Word__c FROM ResponseForce__Word_Dictionary__c WHERE RecordTypeId = :wordTypes.get('Noun')];

        
        //Check if we have anything in the dictionary; if we don't then obviously we can't return anything.
        If (adjectives.size() == 0 || nouns.size() == 0){
            System.debug('No Adjectives or Nouns');
            return Null;
        }
        
            
        Set<String> incidentNames = getExistingNames();
        Long seed = 0L;
        String caseNumber = c.CaseNumber;
        for (Integer p = 0; p < caseNumber.getChars().size(); p++){
            seed += (caseNumber.getChars()[p] * p); 
        }
        
        Random i = new Random();
        Random a = new Random();
        Integer adjInt = a.nextInteger(adjectives.size()-1);
        Integer nounInt = i.nextInteger(nouns.size()-1);
        
        String incName;
        Boolean clear = False;
        Integer count = 0;
        
        //Need to check if this incident name already exists; if it does keep incrementing until we find a name that doesn't exist
        While (clear == False){
            String tempName = adjectives.get(adjInt + count).ResponseForce__Word__c + ' ' + nouns.get(nounInt + count).ResponseForce__Word__c;
            tempName = tempName.toUpperCase();
            System.debug('ADJ = ' + adjectives.get(adjInt + count).ResponseForce__Word__c);
            System.debug('TempName = ' + tempName);
            if (!incidentNames.contains(tempName)){
                incName = tempName;
                clear = True;
            }
            count++;
        }
        
        System.debug('CreateName - Incident Name ' + incName);
        return incName;
    }
}
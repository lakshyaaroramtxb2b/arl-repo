public class SessionTaggingUtils {
   @future(callout=true)
   public static void tag(String sid, String orgId, String userId, String instance, String ruleId) {
       String endpoint = 'callout:SessionTagger';
       Map<String, Object> bodyMap = new Map<String, Object>();
       bodyMap.put('orgId', orgId);
       bodyMap.put('userId', userId);
       List<String> rules = new List<String>();
       rules.add(ruleId);
       bodyMap.put('ruleIds', rules);
       String body = JSON.serializePretty(bodyMap);
       Http h = new Http();
       HttpRequest req = new HttpRequest();
       req.setHeader('X-SFDC-INSTANCE', instance);
       req.setHeader('Content-Type', 'application/json');
       req.setEndpoint(endpoint);
       req.setMethod('POST');
       req.setBody(body);
       HttpResponse res = h.send(req);   
    }
    
   @future(callout=true)
   public static void tagByMatching(String sid, String orgId, String userId, String instance, String ruleMatcher) {
       String endpoint = 'callout:SessionTagger';
       Map<String, Object> bodyMap = new Map<String, Object>();
       bodyMap.put('orgId', orgId);
       bodyMap.put('userId', userId);       
       bodyMap.put('ruleMatcher', ruleMatcher);
       String body = JSON.serializePretty(bodyMap);
       Http h = new Http();
       HttpRequest req = new HttpRequest();
       req.setHeader('X-SFDC-INSTANCE', instance);
       req.setHeader('Content-Type', 'application/json');
       req.setEndpoint(endpoint);
       req.setMethod('POST');
       req.setBody(body);
       HttpResponse res = h.send(req);   
    }    

}
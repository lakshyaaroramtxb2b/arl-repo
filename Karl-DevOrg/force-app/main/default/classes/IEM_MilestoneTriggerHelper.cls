/*
* Created by - Prashant Gupta (MTX Group Inc.)
* Helper class for IEM MILESTONE TRIGGER, 
* Most of the functional code for the trigger exists here and is directly called from Handler Class
*/
public class IEM_MilestoneTriggerHelper {
    @TestVisible private static List<ADM_Work_c__x> mockedRequests = new List<ADM_Work_c__x>(); //For the test class

    /*
     * Created by - Prashant Gupta
     * Date - 31-07-2019
     * This functon is used to milestone contact lookups by case lookups.
    */
    public static void updateMilestoneContacts(List<Milestone__c> newList, Map<Id,Milestone__c> oldMap){
        
        Map<Id,List<Milestone__c>> caseVsMilestoneMap = new Map<id,List<Milestone__c>>();
        for(Milestone__c milestone : newList){
            //  milestone.Case__c!=null && (milestone.Case_Action_Plan_Owner__c == null || milestone.Case_Issue_Owner__c ==null) &&
            if((Trigger.isInsert   
               || (oldMap != null && milestone.Case__c != oldMap.get(milestone.id).Case__c) )){
                   if(!caseVsMilestoneMap.containsKey(milestone.case__c)){
                       caseVsMilestoneMap.put(milestone.case__c, new List<Milestone__c>());
                   }
                caseVsMilestoneMap.get(milestone.case__c).add(milestone);
            }
        }
        if(caseVsMilestoneMap!=null && !caseVsMilestoneMap.isEmpty()){
            for(Case caseRec : [SELECT id, Action_Plan_Owner__c,Issue_Owner__c FROM Case WHERE id IN :caseVsMilestoneMap.keySet()
                               AND (Action_Plan_Owner__c!=null OR Issue_Owner__c!=null)]){
                if(caseVsMilestoneMap.containsKey(caseRec.id)){
                    for(Milestone__c milestone : caseVsMilestoneMap.get(caseRec.id)){
                        milestone.Case_Action_Plan_Owner__c = caseRec.Action_Plan_Owner__c;
                        milestone.Case_Issue_Owner__c = caseRec.Issue_Owner__c;
                    }
                 }
            }
        }
        
    }
    /*
     * Created by - Banshi
     * Date - 12-11-2019
     * If the Milestone's status is updated, the corresponding GUS Work status should be updated.
    */
   /* public static void updateGusWorkRecords(List<Milestone__c> newList, Map<id,Milestone__c> oldMap){
        Set<Id> gusWorkIds = new Set<Id>();
        Set<Id> milestoneIds = new Set<Id>();
        
        for(Milestone__c selectedMilestone : newList){
            if( selectedMilestone.Status__c != oldMap.get(selectedMilestone.Id).Status__c && (selectedMilestone.Gus_Work__c !=null || Test.isRunningTest()) ){
                gusWorkIds.add(selectedMilestone.Gus_Work__c);
                milestoneIds.add(selectedMilestone.Id);
                
            }
        }
        if(!gusWorkIds.isEmpty() && !System.isFuture() && !System.isBatch()){
            updateGusWorkRecordsStatus(gusWorkIds, milestoneIds);
        }
        
    }
    
    @future
    public static void updateGusWorkRecordsStatus(Set<Id> gusWorkIds, Set<Id> milestoneIds){
        List<Milestone__c> milestoneList = new List<Milestone__c>();
        Map<Id, String> milestoneMap = new Map<Id, String>();
        
        for(Milestone__c milestone : [Select Status__c From Milestone__c Where Id IN :milestoneIds]){
            milestoneMap.put(milestone.Id, milestone.Status__c);
        }
        List<ADM_Work_c__x> gusWorks = new List<ADM_Work_c__x>();
        List<ADM_Work_c__x> WorkList = new List<ADM_Work_c__x>();
        
        if(Test.isRunningTest()) {
            gusWorks = mockedRequests;
        }else{
            gusWorks = [Select Id,Status_c__c,Feature_ID_c__c From ADM_Work_c__x Where Id IN :gusWorkIds AND Feature_ID_c__c!=null];
        }
        for(ADM_Work_c__x gusWork : gusWorks){
            if(milestoneMap.containsKey(gusWork.Feature_ID_c__c))
            {
                String milestoneStatus = milestoneMap.get(gusWork.Feature_ID_c__c);
                String gusWorkStatusToUpdate;
                if(milestoneStatus == IEM_Constants.MILESTONE_STATUS_COMPLETED){
                    gusWorkStatusToUpdate = IEM_Constants.GUS_WORK_STATUS_CLOSED;
                }
                else if(milestoneStatus == IEM_Constants.MILESTONE_STATUS_PENDING){
                    gusWorkStatusToUpdate  = IEM_Constants.GUS_WORK_STATUS_NEW;
                }
                else if(milestoneStatus == IEM_Constants.MILESTONE_STATUS_IN_PROGRESS){
                    gusWorkStatusToUpdate = IEM_Constants.GUS_WORK_STATUS_IN_PROGRESS;
                }
                
                if(String.isNotEmpty(gusWorkStatusToUpdate) && gusWork.Status_c__c != gusWorkStatusToUpdate){
                    gusWork.Status_c__c = gusWorkStatusToUpdate;
                    WorkList.add(gusWork);
                }
            }
        }
        if(!WorkList.IsEmpty()){
            if(!Test.isRunningTest()){
                List<Database.SaveResult> result = Database.updateImmediate(WorkList);
            }
        }
    }
    */
    
}
public class DCEventDetailsControllerExt {
    
    private ID recId;
    private Detection_Security_Event_Criteria__c DCEventCriteria;  
    
    /*public DCEventDetailsController() {
        recId = ApexPages.currentPage().getParameters().get('id');
        DCEventCriteria = getRecord();
    }

    private Detection_Security_Event_Criteria__c getRecord()
    {
        return [SELECT Id 
                FROM Detection_Security_Event_Criteria__c 
                WHERE Id =: recId];
    }*/
    
    public DCEventDetailsControllerExt(ApexPages.StandardSetController Controller) {
        
        DCEventCriteria = (Detection_Security_Event_Criteria__c)Controller.getRecord();
    }
    
    public String getEventCount() {
        return '1';
    }
    
    public String getLastEventDate() {
        return '7/5/16 4:4:4';
    }
}
public class LeaderDCIncident {
	public String id {get; set;}
    public String csirtCaseId {get; set;}
    public String subject {get; set;}
    public String status {get; set;}
    public String alertCriteria {get; set;}
    public String alertCriteriaName {get; set;}
    public String createdBy {get; set;}
    public DateTime createdDate {get; set;}
}
public class TimeUtilities {
	public static Time GetElapsedTime(Time startTime, Time endTime) {
        if(startTime == null || endTime == null){
        	return Time.newInstance(0, 0, 0, 0);
        }

    	Integer elapsedHours = endTime.hour() - startTime.hour();
    	Integer elapsedMinutes = endTime.minute() - startTime.minute();
    	Integer elapsedSeconds = endTime.second() - startTime.second();
    	Integer elapsedMiliseconds = endTime.millisecond() - startTime.millisecond();

    	return Time.newInstance(elapsedHours, elapsedMinutes, elapsedSeconds, elapsedMiliseconds);
	}
}
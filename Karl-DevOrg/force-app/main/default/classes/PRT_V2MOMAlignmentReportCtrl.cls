public class PRT_V2MOMAlignmentReportCtrl {
    
    @TestVisible
    private static list<sObject> mockallV2MOMList = new list<sObject>();
    @TestVisible
    private static list<sObject> mockAllMethodList = new list<sObject>();
    @TestVisible
    private static list<sObject> mockAllOrg62List = new list<sObject>();
    
    //Get Account Records
    @AuraEnabled
    public static List<V2MOM_c__x> getV2Mom(String fromDate, String toDate){
        Set<Id> methodIds = new Set<Id>();
        List<V2MOM_c__x> v2momFinalList = new List<V2MOM_c__x>();
          
        Datetime fromDateVal = (DateTime)JSON.deserialize('"' + fromDate + '"', DateTime.class);
        Datetime toDateVal = (DateTime)JSON.deserialize('"' + toDate + '"', DateTime.class);
        List<String> v2momIds = new List<String>();
        Set<String> finalV2momIds = new Set<String>();
        Map<String,V2MOM_c__x> v2momMap = new Map<String,V2MOM_c__x>();
        Map<Id,Org62User__x> v2momUserMap = new Map<Id,Org62User__x>();
        
        Set<Id> userIds = new Set<Id>();
       /* List<V2MOM_c__x> v2MomList = [SELECT Id, Name__c, V2MOM_User_c__c  
                                  FROM V2MOM_c__x Where CreatedDate__c  >= :fromDateVal AND CreatedDate__c  <= :toDateVal LIMIT :Integer.valueOf(psize) OFFSET :(psize*pnumber)]; */
        if(test.isRunningTest()){
            V2MOM_c__x v2Mom = (V2MOM_c__x)mockallV2MOMList[0];
            v2momMap.put(v2Mom.ExternalId, v2Mom);
            if(String.isNotBlank(v2Mom.V2MOM_User_c__c)){
                userIds.add(v2Mom.V2MOM_User_c__c);
            }
        }else{
            for(V2MOM_c__x v2Mom: [SELECT Id,ExternalId, Name__c, V2MOM_User_c__c, Published_on_c__c   
                                   FROM V2MOM_c__x Where Published_on_c__c !=null AND CreatedDate__c  >= :fromDateVal AND CreatedDate__c  <= :toDateVal]){
                                       v2momMap.put(v2Mom.ExternalId, v2Mom);
                                       if(String.isNotBlank(v2Mom.V2MOM_User_c__c)){
                                           userIds.add(v2Mom.V2MOM_User_c__c);
                                       }
                                   }
        }
       
        if(!userIds.isEmpty()){
            if(!Test.isRunningTest()){
                for(Org62User__x user :[Select Id,ExternalId, Name__c  From Org62User__x Where Id IN :userIds]){
                    v2momUserMap.put(user.ExternalId, user);
                } 
            }else{
                Org62User__x user = (Org62User__x)mockAllOrg62List[0];
                v2momUserMap.put(user.ExternalId, user);
            }
        }
       for(V2MomMapping__c v2momMapping : [Select Id, Dependent_Method__c   From V2MomMapping__c Where Dependent_Method__c!= null]){
            methodIds.add(v2momMapping.Dependent_Method__c );
        } 
        if(!methodIds.isEmpty()){
            if(!Test.isRunningTest()){
                for(V2MOM_Method_c__x  method  : [SELECT ID, 
                                         V2MOM_c__c 
                                         FROM V2MOM_Method_c__x  
                                         WHERE ID NOT IN :methodIds AND V2MOM_c__c IN :v2momMap.keySet()]){
                                            finalV2momIds.add(method.V2MOM_c__c);
                                         }
            }else{
                V2MOM_Method_c__x  method = (V2MOM_Method_c__x)mockAllMethodList[0];
                finalV2momIds.add(method.V2MOM_c__c);
            }
        }
        if(!finalV2momIds.isEmpty()){
            for(String v2momId : finalV2momIds){
                if(v2momMap.containsKey(v2momId)){
                    V2MOM_c__x v2mom = v2momMap.get(v2momId);
                        
                    v2momFinalList.add(v2mom);
                }
            }
        }return v2momFinalList;
    }
 
}
public class SecContactOfficeHoursController {

    private static final Integer ADV_NOTICE_DAYS = 1;
    private static final Integer MAX_ADV_BOOKING_DAYS = 28;
    private static final Integer APPT_MEETING_MINUTES = 30;
    private static final Set<String> APPT_DAYS_OF_WEEK = new Set<String>{'Tue','Thu'};
    private static final Set<Integer> APPT_HOURS_OF_DAY = new Set<Integer>{10,15};
    private static final Set<Integer> APPT_MINUTES_OF_DAY = new Set<Integer>{0, 30};
    private static final Id ORG_WIDE_EMAIL_ADDRESS_ID = '0D2300000008OqS';
    private static final String OFFICE_HOURS_EMAIL = 'prodsecofficehours@l-60kl43xwzxziz771dj185u66v.3jsbieac.3.apex.salesforce.com';
    private OrgWideEmailAddress o;
    
    public Integer statusCode { get; set; }
    public String statusmsg { get; set; }
    public String uName {get;set;}
    public String uEmail {get;set;}
    public String uCompany {get;set;}
    public String uContactInfo {get;set;}
    public String uDescription {get;set;}
    public String uDate {get;set;}
    public String uAppName {get;set;}
    public List<AppointmentDay> appts {get;private set;}
    public String errorMessages { get; set; }

    
    public SecContactOfficeHoursController() {        
      Datetime earliest = datetime.now().addDays(ADV_NOTICE_DAYS);
      Datetime latest = datetime.now().addDays(MAX_ADV_BOOKING_DAYS);
      
      List<Datetime> possible = makeDtList();
      Map<String,AppointmentDay> admap = new Map<String,AppointmentDay>();
      appts = new List<AppointmentDay>();
           
      for (Datetime dt : possible) {
        String fmt = dt.format('yyyy-MM-dd');
        if (!admap.containsKey(fmt)) {
          AppointmentDay ad = new AppointmentDay(dt,new List<Appointment>());
          admap.put(fmt,ad); // for easy referencing
          appts.add(ad);  // for ordering
        }
        admap.get(fmt).appointments.add(new Appointment(dt));
      }
      
      o = [SELECT Address,Id,DisplayName FROM OrgWideEmailAddress WHERE Id=:ORG_WIDE_EMAIL_ADDRESS_ID];
      
      Map<String,List<Partner_Appointment__c>> apmap = new Map<String,List<Partner_Appointment__c>>();
      for (Partner_Appointment__c pa : [SELECT Date__c FROM Partner_Appointment__c
                                             WHERE Date__c >: earliest 
                                             AND Date__c <: latest]) {
        String fmt = pa.Date__c.format('yyyy-MM-dd');
        //throw new tmpException('bang');                                      
        if (admap.containsKey(fmt)) {
          // duh, i would hope this is a valid day to book
          admap.get(fmt).makeUnavailable(pa.Date__c);
        }
      }
    }
       
    public PageReference doBooking() {
      try {
        Datetime reqDate = datetime.valueOf(uDate);
        String day = reqDate.format('yyyy-MM-dd');
        String hour = String.valueOf(reqDate.hour());
        String minute = String.valueOf(reqDate.minute());
        
        /*
        if ([SELECT Id FROM Partner_Appointment__c WHERE 
             DAY_ONLY(Date__c) =:day AND 
             HOUR_IN_DAY(Date__c) =:hour].size() > 0) {
          statusmsg = 'Booking no longer available.  Sorry, please select another time';
          statusCode = 1;
          return null;
        }
        //v18.0 ONLY?! v18.0 ONLY?! DAMMITSOMUCH!
        */
        
        // ghetto workaround
        Datetime past = reqDate.addDays(-1);
        for (Partner_Appointment__c ghettoworkaround: [SELECT Id,Date__c FROM Partner_Appointment__c WHERE Date__c >:past]) {
          if (ghettoworkaround.Date__c.format('yyyy-MM-dd-HH-mm') == reqDate.format('yyyy-MM-dd-HH-mm')) {
            statusmsg = 'Booking no longer available.  Sorry, please select another time';
            statusCode = 1;
            return null;
          }
        }
        
        
        Boolean valid = False;
        for (Datetime p : makeDtList()) {
          if ((p.year() == reqDate.year()) && (p.month() == reqDate.month()) && (p.day() == reqDate.day()) &&
              (p.hour() == reqDate.hour()) && (p.minute() == reqDate.minute())) {
            valid = True;
          }
        }
        if (!valid) {
          statusmsg = 'Sorry, that date is not available. Please select another time';
          statusCode = 1;
          return null;
        }
        Partner_Appointment__c pa = new Partner_Appointment__c();
        pa.Name = uDate+' - '+uName;
        pa.Email__c = uEmail;
        pa.Name__c = uName;
        pa.Company__c = uCompany;
        pa.Description__c = uDescription;
        pa.ContactInfo__c = uContactInfo;
        pa.AppName__c = uAppName;
        pa.Date__c = reqDate;
        insert pa;
        Partner_Appointment__c pa2 = [Select Name, Email__c, Name__c, Company__c, AppName__c, Description__c, ContactInfo__c,
        Date__c, RefId__c FROM Partner_Appointment__c WHERE Id = :pa.Id LIMIT 1];
        sendInvitation(pa2);
        //scheduleReminder(pa);
      }
      catch (Exception e) {
        //statusmsg = 'Booking error.  Sorry, please try again';
        statusmsg = e.getTypeName()+e.getMessage();
        statusCode = 1;
        throw e;
        return null;
      }
      statusmsg = 'Booking complete.  A meeting invitation will be sent to "'+uEmail+'".';
      statusCode = 0;
      return null;
    }
    
    private List<Datetime> makeDtList() {
      List<datetime> all = new List<datetime>();
     
      /*
      Datetime last = Datetime.newInstance(date.today().addDays(MAX_ADV_BOOKING_DAYS),Time.newInstance(0,0,0,0));
      Datetime starttime = Datetime.newInstance(date.today().addDays(ADV_NOTICE_DAYS),Time.newInstance(0,0,0,0));
      BusinessHours bh = null;
      List<BusinessHours> bhl = [select Id, Name, IsActive, IsDefault, SundayStartTime, 
                                   SundayEndTime, MondayStartTime, MondayEndTime, TuesdayStartTime, 
                                   TuesdayEndTime, WednesdayStartTime, WednesdayEndTime, 
                                   ThursdayStartTime, ThursdayEndTime, FridayStartTime, 
                                   FridayEndTime, SaturdayStartTime, SaturdayEndTime, TimeZoneSidKey
                                 FROM BusinessHours
                                 WHERE isActive=True AND Name LIKE '[PartnerOfficeHours]%'];
      if (bhl.size()>0) {
        bh = bhl.get(0);
      }
      else {
        System.debug('Declare business hours and holidays for PartnerOfficeHours');
        return new List<Datetime>();
      }
      
      Datetime endtime = BusinessHours.add(bh.Id,starttime,APPT_MEETING_MINUTES*60*1000L);
      while (endtime < last) {
        //if (endtime.dayOfYear() == startime.dayOfYear()){
        all.add(endtime.addMinutes(-60));
        starttime = endtime;
        endtime = BusinessHours.add(bh.Id,starttime.addMinutes(APPT_MEETING_MINUTES),APPT_MEETING_MINUTES*60*1000L);
      }       
      */
      
      // Non-BusinessHours version
      Datetime now = datetime.now().addDays(ADV_NOTICE_DAYS);
      Integer daysAhead = 0+ADV_NOTICE_DAYS;
      while (daysAhead < MAX_ADV_BOOKING_DAYS) {
        if (APPT_DAYS_OF_WEEK.contains(now.format('EEE'))) {
          for (Integer hr : APPT_HOURS_OF_DAY) {
            for (Integer min : APPT_MINUTES_OF_DAY) {
                all.add(datetime.newInstance(now.year(),
                                             now.month(),
                                             now.day(),
                                             hr,
                                             min,
                                             0));
            }
          }
        }
        daysAhead++;
        now = now.addDays(1);
      } 
                                        
      return all;
    }
    public void sendInvitation(Partner_Appointment__c app) {
      
      Messaging.SingleEmailMessage mail= new Messaging.SingleEmailMessage();
      mail.setToAddresses(new String[] {app.Email__c,o.Address});
      mail.setOrgWideEmailAddressId(ORG_WIDE_EMAIL_ADDRESS_ID);
      mail.setReplyTo(OFFICE_HOURS_EMAIL);
      mail.setSubject('['+app.RefId__c+'] Partner Security Office Hours | '+ app.Company__c + ' | ' + new Appointment(app.Date__c).getuglyTime() + ' | Ref:' + String.valueOf(app.Id)); //cheating, we split on '|' later for handling responses 
      mail.setPlainTextBody('Description: '+app.Description__c+'\n\n\nLocation: '+app.ContactInfo__c + '\n\n\nApp Name: ' + app.AppName__c + '\n\n\nIf you wish to cancel or update the contact information for an appointment, please send an email to: securecloud@salesforce.com.');
      
      Messaging.EmailFileAttachment atta = new Messaging.EmailFileAttachment();
      atta.setBody(Blob.valueOf(getVCALENDAR(app,APPT_MEETING_MINUTES)));
      atta.setFileName('appointment_request.ics');
      atta.setContentType('text/calendar');
      atta.setInline(true);
      
      mail.setFileAttachments(new Messaging.EmailFileAttachment[]{atta});
      Messaging.sendEmail(new Messaging.Email[] {mail});
    }
    
    public void sendCancellation(Partner_Appointment__c app) {
      Messaging.SingleEmailMessage mail= new Messaging.SingleEmailMessage();
      mail.setToAddresses(new String[] {app.Email__c,o.Address});
      mail.setOrgWideEmailAddressId(ORG_WIDE_EMAIL_ADDRESS_ID);
      mail.setReplyTo(OFFICE_HOURS_EMAIL);
      mail.setSubject('['+app.RefId__c+'] Partner Security Office Hours | '+ app.Company__c + ' | ' + new Appointment(app.Date__c).getuglyTime() + ' | Ref:' + String.valueOf(app.Id)); //cheating, we split on '|' later for handling responses 
      mail.setPlainTextBody('Description: '+app.Description__c+'\n\n\nLocation: '+app.ContactInfo__c);
      
      Messaging.EmailFileAttachment atta = new Messaging.EmailFileAttachment();
      atta.setBody(Blob.valueOf(getVcalCancellation(app,APPT_MEETING_MINUTES)));
      atta.setFileName('appointment_request.ics');
      atta.setContentType('text/calendar');
      atta.setInline(true);
      
      mail.setFileAttachments(new Messaging.EmailFileAttachment[]{atta});
      Messaging.sendEmail(new Messaging.Email[] {mail});
    }
    
    /*
    // if no one takes appointment, email the group a reminder
    public void scheduleReminder(Partner_Appointment__c app){
        UnacceptedOfficeHoursController u = new UnacceptedOfficeHoursController();
        Datetime dt = app.Date__c;
        // schedule reminder for one hour before appointment
        String sch = '0 ' + String.valueOf(dt.minute()) + ' ' + String.valueOf(dt.hour()-1) + ' ' + String.valueOf(dt.day()) + ' ' + String.valueOf(dt.month()) + ' ? ' + String.valueof(dt.year());
        system.schedule(app.Company__c  + ' ' + String.valueOf(dt) + ' appointment reminder', sch, u); // add dt in there to ensure uniqueness of scheduled job
    }
    */
    
    private String Schedule(Boolean IsAllDayEvent, Datetime StartDateTime, Datetime EndDateTime)
    {
    String result;
    if (IsAllDayEvent)
    {
      result = 'DTSTART;VALUE=DATE:' + StartDateTime.formatGmt('yyyyMMdd') + CRLF;
      result += 'DTEND;VALUE=DATE:' + EndDateTime.AddDays(1).formatGmt('yyyyMMdd') + CRLF;
    }
    else
    {
      result = 'DTSTART:' + StartDateTime.formatGmt('yyyyMMdd\'T\'HHmmss\'Z\'') + CRLF;
      result += 'DTEND:' + EndDateTime.formatGmt('yyyyMMdd\'T\'HHmmss\'Z\'') + CRLF;
    }
    return result;
  }
  
  static String CRLF = '\r\n';
  private String SetField(String fieldName, String fieldValue)
  {
    if (fieldValue != null && fieldValue != '')
    {
      return fieldName + Escape(fieldValue) + CRLF;
    }
    return '';
  }
  
  private String SetDescriptionField(String fieldName, String fieldValue)
  {
    if (fieldValue != null && fieldValue != '')
    {
      return fieldName + Escape(fieldValue) + '\\n';
    }
    return '';
  }
  
  public String getVCALENDAR(Partner_Appointment__c pa,Integer minutes)
  {
    String dtstamp = 'DTSTAMP:' + Datetime.Now().formatGmt('yyyyMMdd\'T\'HHmmss\'Z\'') + CRLF;
    String result = 'BEGIN:VCALENDAR' + CRLF + 'PRODID:-//Force.com Labs Prodsec//iCalendar Export//EN' + CRLF + 'VERSION:2.0' + CRLF;
    result += 'CALSCALE:GREGORIAN' + CRLF;
    result += 'METHOD:REQUEST' + CRLF;
    result += 'X-MS-OLK-FORCEINSPECTOROPEN:TRUE' + CRLF;   
    result += 'X-WR-RELCALID:' + String.valueOf(Crypto.GetRandomLong()) + CRLF;
    result += 'BEGIN:VEVENT' + CRLF;
    result += 'UID:'+pa.Name__c + CRLF;
    result += Schedule(false, pa.Date__c, pa.Date__c.addMinutes(minutes));
    result += dtstamp;
    result += SetField('SUMMARY:', '['+pa.RefId__c+'] '+ pa.Description__c);
    result += SetField('LOCATION:', pa.ContactInfo__c);
    result += 'CLASS:PUBLIC' + CRLF;
    result += 'TRANSP:OPAQUE' + CRLF + 'X-MICROSOFT-CDO-BUSYSTATUS:BUSY' + CRLF;
    result += 'X-MICROSOFT-CDO-IMPORTANCE:1' + CRLF;
    result += 'X-MICROSOFT-DISALLOW-COUNTER:FALSE' + CRLF;
    result += 'X-MS-OLK-ALLOWEXTERNCHECK:TRUE' + CRLF;
    result += 'X-MS-OLK-AUTOFILLLOCATION:FALSE' + CRLF;
    result += 'X-MS-OLK-CONFTYPE:0' + CRLF;
    
    String addInfo = SetDescriptionField('Contact: ', pa.Name__c);
    addInfo += SetDescriptionField('Email: ', pa.Email__c);
    addInfo += SetDescriptionField('App Name: ', pa.AppName__c);
    addInfo += '\\n';
    result += SetField('DESCRIPTION:', addInfo+pa.Description__c);
    result += 'ATTENDEE;ROLE=REQ-PARTICIPANT;PARTSTAT=NEEDS-ACTION;CN='+o.DisplayName+':MAILTO:'+o.Address + CRLF; // prodsec team
    result += 'ATTENDEE;ROLE=REQ-PARTICIPANT;PARTSTAT=NEEDS-ACTION;CN='+pa.Name__c+':MAILTO:'+pa.Email__c + CRLF; // partner
    result += 'ORGANIZER;CN=Force.com Secure Cloud:MAILTO:' + OFFICE_HOURS_EMAIL  + CRLF; // make us the organizer so outlook replies work
    result += 'BEGIN:VALARM' + CRLF;
    result += 'TRIGGER:-PT15M' + CRLF;
    result += 'ACTION:DISPLAY' + CRLF;
    result += 'DESCRIPTION:Reminder' + CRLF;
    result += 'END:VALARM' + CRLF;
    result += 'END:VEVENT' + CRLF;
    result += 'END:VCALENDAR' + CRLF;
    return result;
  }
  
  String getVcalCancellation(Partner_Appointment__c pa,Integer minutes){
    String dtstamp = 'DTSTAMP:' + Datetime.Now().formatGmt('yyyyMMdd\'T\'HHmmss\'Z\'') + CRLF;
    String result = 'BEGIN:VCALENDAR' + CRLF + 'PRODID:-//Force.com Labs Prodsec//iCalendar Export//EN' + CRLF + 'VERSION:2.0' + CRLF;
    result += 'CALSCALE:GREGORIAN' + CRLF;
    result += 'METHOD:CANCEL' + CRLF;
    result += 'X-MS-OLK-FORCEINSPECTOROPEN:TRUE' + CRLF;   
    result += 'X-WR-RELCALID:' + String.valueOf(Crypto.GetRandomLong()) + CRLF;
    result += 'BEGIN:VEVENT' + CRLF;
    result += 'UID:'+pa.Name__c + CRLF;
    result += Schedule(false, pa.Date__c, pa.Date__c.addMinutes(minutes));
    result += dtstamp;
    result += SetField('SUMMARY:', '['+pa.RefId__c+'] ' + pa.Description__c);
    result += SetField('LOCATION:', pa.ContactInfo__c);
    result += 'CLASS:PUBLIC' + CRLF;
    result += 'TRANSP:OPAQUE' + CRLF + 'X-MICROSOFT-CDO-BUSYSTATUS:BUSY' + CRLF;
    result += 'X-MICROSOFT-CDO-IMPORTANCE:1' + CRLF;
    result += 'X-MICROSOFT-DISALLOW-COUNTER:FALSE' + CRLF;
    result += 'X-MS-OLK-ALLOWEXTERNCHECK:TRUE' + CRLF;
    result += 'X-MS-OLK-AUTOFILLLOCATION:FALSE' + CRLF;
    result += 'X-MS-OLK-CONFTYPE:0' + CRLF;
    
    String addInfo = SetDescriptionField('Contact: ', pa.Name__c);
    addInfo += SetDescriptionField('Email: ', pa.Email__c);
    addInfo += '\\n';
    result += SetField('DESCRIPTION:', addInfo+pa.Description__c);
    result += 'ATTENDEE;ROLE=REQ-PARTICIPANT;PARTSTAT=NEEDS-ACTION;CN='+o.DisplayName+':MAILTO:'+o.Address + CRLF; // prodsec team
    result += 'ATTENDEE;ROLE=REQ-PARTICIPANT;PARTSTAT=NEEDS-ACTION;CN='+pa.Name__c+':MAILTO:'+pa.Email__c + CRLF; // partner
    result += 'ORGANIZER;CN=Force.com Secure Cloud:MAILTO:' + OFFICE_HOURS_EMAIL  + CRLF; // make us the organizer so outlook replies work
    result += 'BEGIN:VALARM' + CRLF;
    result += 'TRIGGER:-PT15M' + CRLF;
    result += 'ACTION:DISPLAY' + CRLF;
    result += 'DESCRIPTION:Reminder' + CRLF;
    result += 'END:VALARM' + CRLF;
    result += 'END:VEVENT' + CRLF;
    result += 'END:VCALENDAR' + CRLF;
    return result;
  }
  
  private String Escape(String original)
  {
    return original.replace('\n','\\n').replace('\r','');
  }
    public class AppointmentDay {
      
      public String dayMonth {get;private set;}
      public List<Appointment> appointments {get;private set;}
      
      public AppointmentDay(datetime whentime, List<Appointment> apps) {
        dayMonth = whentime.format('MMM dd');
        appointments = apps;
      }
      public void makeUnavailable(datetime dt) {
        for (Appointment a : appointments) {
          if ((a.rt.year() == dt.year()) && (a.rt.month() == dt.month()) && (a.rt.day() == dt.day()) &&
              (a.rt.hour() == dt.hour()) && (a.rt.minute() == dt.minute())) {
              a.available = False;
          }
        }
      }
    }
    
    public class Appointment {
      public Datetime rt;
      public Boolean available {get;private set;}
      public Appointment(datetime dt) {
        rt = dt;
        available = True;
        // for some reason, some appointments that had already been made were showing up as available time slots
        // so, this will make sure that if there are any existing appointments, available will get set to false
        List<Partner_Appointment__c> existing = [SELECT Date__c FROM Partner_Appointment__c WHERE Date__c =: rt];
        if (existing.size() > 0) available = False;
                                            
      }
      public String getprettyTime() {
        Datetime endtime = rt.addMinutes(APPT_MEETING_MINUTES);
        //return String.valueOf(Math.mod(rt.hour(),12))+'-'+String.valueOf(Math.mod(endtime.hour(),12))+endtime.format(' a (z)');
        return String.valueOf(rt.format('h:mm a (z)'));
      }
      public String getuglyTime() {
        return rt.format('yyyy-MM-dd HH:mm:ss');
      }
    }

}
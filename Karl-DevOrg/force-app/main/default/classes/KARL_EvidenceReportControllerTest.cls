/**
* @author Lakshya Arora
* @email lakshya.arora@mtxb2b.com
* @description Test class for KARL_EvidenceReportController
*/
@isTest
public class KARL_EvidenceReportControllerTest {
    /**
* @author Lakshya Arora
* @email lakshya.arora@mtxb2b.com
* @desc method to test the getEvidenceReport method
*/   
    @isTest
    public static void getEvidenceReportTest(){
        User opsAdminUser = ARL_TestDataFactory.createOpsAdmin();
        System.runAs(opsAdminUser){
            Account acc = new Account(Name = 'salesforce.com - ESA Office Hours');
            insert acc;
            
            Contact con = new Contact(LastName = 'test', AccountId = acc.Id,Email = 'test@gmail.com');
            insert con;
            
            Contact con2 = new Contact(LastName = 'test2', AccountId = acc.Id,Email = 'test2@gmail.com');
            insert con2;
            
            Request_Item__c reqItem = ARL_TestDataFactory.createRequestItem();
            reqItem.Primary_Scope__c = 'AS';
            reqItem.KARL_Evidence_Submission_Workflow__c = true;
            reqItem.Create_GUS_Case__c = true;
            insert reqItem;
            
            Audit_Cycle__c auditCycle = ARL_TestDataFactory.createAuditCycle();
            insert auditCycle;
            
            KARL_Gus_Product_Tag__c prodTag = new KARL_Gus_Product_Tag__c();
            prodTag.Active__c = true;
            prodTag.Active_Scrum_Team__c = true;
            insert prodTag;
            
            List<KARL_Auto_Assignment_Configuration__c> autoAssignmentConfigList = new List<KARL_Auto_Assignment_Configuration__c>();
            for(Integer i=0;i<4;i++){
                KARL_Auto_Assignment_Configuration__c aacObj = new KARL_Auto_Assignment_Configuration__c();
                //aacObj.Audit_Cycle__c = auditCycle.Id;
                aacObj.Priorities__c = i+'';
                aacObj.Priority_0_Assignment_Date__c = System.today().addDays(5);
                aacObj.Priority_1_Assignment_Date__c = System.today().addDays(5);
                aacObj.Priority_2_Assignment_Date__c = System.today().addDays(5);
                aacObj.Priority_3_Assignment_Date__c = System.today().addDays(5);
                aacObj.Days_to_Complete__c = 10;
                aacObj.Default_product_Tag__c = prodTag.Id;
                autoAssignmentConfigList.add(aacObj);
            }
            autoAssignmentConfigList[1].Priority_0_Assignment_Date__c = System.today().addDays(5);
            autoAssignmentConfigList[2].Priority_0_Assignment_Date__c = System.today().addDays(5);
            autoAssignmentConfigList[3].Priority_0_Assignment_Date__c = System.today().addDays(5);
            insert autoAssignmentConfigList;
            
            List<Audit_Cycle_Coverage__c> auditCycleCoverageList = new List<Audit_Cycle_Coverage__c>();
            Map<Integer,String> priorityToScopeMap = new Map<Integer,String>();
            priorityToScopeMap.put(0,'AS');
            priorityToScopeMap.put(1,'AUST');
            priorityToScopeMap.put(2,'B2BC');
            priorityToScopeMap.put(3,'CHAT');
            for(Integer i=0;i<4;i++){
                Audit_Cycle_Coverage__c auditCycleCoverage = new Audit_Cycle_Coverage__c();
                auditCycleCoverage.Audit_Cycle__c = auditCycle.Id;
                auditCycleCoverage.Area__c = 'iRAP';
                auditCycleCoverage.Scope__c = priorityToScopeMap.get(i);
                auditCycleCoverage.Auto_Assignment_Configuration__c = autoAssignmentConfigList[i].Id;
                auditCycleCoverageList.add(auditCycleCoverage);
            }
            insert auditCycleCoverageList;
            
            KARL_Audit_Team__c auditTeam = ARL_TestDataFactory.createAuditTeam('test');
            insert auditTeam;
            
            KARL_Audit_Team_Contacts__c auditTeamCon = ARL_TestDataFactory.createAuditTeamContact(auditTeam.Id,con.Id);
            insert auditTeamCon;
            
            List<Cycle_Request_Item__c> cycleReqItemList = new List<Cycle_Request_Item__c>();
            for(Integer i=0;i<4;i++){
                Cycle_Request_Item__c cycleReqItem = ARL_TestDataFactory.createCycleRequestItem(reqItem.id,null);  
                cycleReqItem.Cycle_Request_Status__c = 'New';
                cycleReqItem.KARL_Update_GUS__c = true;
                cycleReqItem.Request_Tech_Details__c = 'tech details old';
                cycleReqItem.Request_Assignee__c = con.Id;
                cycleReqItem.Audit_Cycle__c = auditCycle.Id;
                cycleReqItem.Audit_Cycle_Coverage__c = auditCycleCoverageList[i].Id;
                cycleReqItem.Date_for_Work_Record_Creation__c = System.today();
                cycleReqItemList.add(cycleReqItem);
            }
            insert cycleReqItemList;
            List<KARL_Evidence_Request__c> karlEvidenceRequestList = new List<KARL_Evidence_Request__c>();
            for(Integer i=0;i<4;i++){
                KARL_Evidence_Request__c karlEvidenceRequestObj = new KARL_Evidence_Request__c();
                karlEvidenceRequestObj.Evidence_Upload_Date__c = System.today();
                karlEvidenceRequestObj.KARL_Cycle_ARL_Item__c = cycleReqItemList[i].Id; 
                karlEvidenceRequestObj.Evidence_Status__c = 'Pending Engineer';
                karlEvidenceRequestList.add(karlEvidenceRequestObj);
            }
            insert karlEvidenceRequestList;
            
            KARL_EvidenceReportController.FileInfoWrapper fileInfoWrapper = new KARL_EvidenceReportController.FileInfoWrapper();
            KARL_EvidenceReportController.FileInfoWrapper fileInfoWrapper1 = new KARL_EvidenceReportController.FileInfoWrapper('fileId','fileLink','fileName');
            
            List<KARL_EvidenceReportController.ReportWrapper> reportWrapperList =KARL_EvidenceReportController.getEvidenceReport(auditCycle.Id, 'AS', false, 
                                                                                                                                 'pendingEngineer', 'allAssigned', '');
            System.assert(!reportWrapperList.isEmpty());
        }
    }
}
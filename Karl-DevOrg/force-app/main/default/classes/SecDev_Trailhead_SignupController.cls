public with sharing class SecDev_Trailhead_SignupController {

public boolean msa {get; set;}
public boolean contactOK {get; set;}
public boolean done {get; set;}
public signupRequest sR {get; set;}

public string firstName {get; set;}
public string lastName {get; set;}
public string email {get; set;}
public string company {get; set;}
public string lastdate {get; set;}

public SecDev_Trailhead_SignupController(){
    if(sR==null){
        sR = new signupRequest();
        sR.country='US';
        sR.templateid = Trial_Id__c.getValues('SecDev_Trailhead').Trial_Id__c;    
    }
    lastdate = Trial_Id__c.getValues('SecDev_Trailhead').LastModifiedDate.date().format();
    verified = false;
}


public pageReference createSignup(){

    boolean goForward = true;

    /* reCaptcha Nonsense */
    HttpResponse r = makeRequest(baseUrl,'privatekey=' + privateKey + '&remoteip='  + remoteHost + '&challenge=' + challenge +'&response='  + response);
    if(r!= null){
        verified = (r.getBody().startsWithIgnoreCase('true'));
    } else {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, 'Uh oh, reCaptcha fail! Please try again.'));
        goForward = false;
    } 
    
    if(verified==false){
        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, 'Uh oh, reCaptcha fail! Please try again.'));
        goForward = false;    
    }

    if(firstName == '' || firstName == null){
        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, 'Please include a valid first name.'));
        goForward = false;
    } else {
        sR.firstName = firstName;
    }
    
    if(lastName == '' || lastName == null){
        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, 'Please include a valid last name.'));
        goForward = false;
    } else {
        sR.lastName = lastName;
    }
    
    if(email == '' || email == null){
        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, 'Please include a valid email or you will never recieve the login information.'));
        goForward = false;
    } else {
        sR.signupEmail = email;
    }
    
    if(company == '' || company == null){
        // not required
    } else {
        sR.company = company;
    }    
    
      
    if(msa == false){
        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, 'Please accept the service agreement or we cannot provison your environment.'));
        goForward = false;
    }
    
    if(contactOK == true){
        sR.Communication_Opt_Out__c = false;
    }
    
    if((goForward&&verified)||Test.isRunningTest()){
        sR.UserName=sR.firstName+sR.lastName+EncodingUtil.convertToHex(crypto.generateAesKey(128)).substring(1,6).toUpperCase()+'@SecureCodingTrailhead.Dev';
        system.debug('Here is the finalized trial signup: '+sR);
        try{
            insert sr;
            done = true;
        } catch(exception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, 'Something went wrong when we attempted to create your environment. Is your email address valid?'));
            verified = false;
        }
    }
    return null;
}


// reCaptcha Nonsense
private static String baseUrl = 'https://www.google.com/recaptcha/api/verify'; 
private static String privateKey = '6LeVTfsSAAAAAHK-sia-GzlenxieBaFF0Ly0B3_z';
public String publicKey { 
    get { return '6LeVTfsSAAAAAGl4L2TSunEat1JtN6U5tXEWxxXZ'; }
}
public Boolean verified { get; private set; } 

public String challenge { 
    get {
        return ApexPages.currentPage().getParameters().get('recaptcha_challenge_field');
    }
}

public String response  { 
    get {
        return ApexPages.currentPage().getParameters().get('recaptcha_response_field');
    }
}

public PageReference reset() {
    return null; 
}   

/* Private helper methods */
private static HttpResponse makeRequest(string url, string body)  {
    HttpResponse response = null;
    HttpRequest req = new HttpRequest();   
    req.setEndpoint(url);
    req.setMethod('POST');
    req.setBody (body);
    try {
        Http http = new Http();
        response = http.send(req);
        System.debug('reCAPTCHA response: ' + response);
        System.debug('reCAPTCHA body: ' + response.getBody());
    } catch(System.Exception e) {
        System.debug('ERROR: ' + e);
    }
    return response;
}   
        
private String remoteHost { 
    get { 
        String ret = '127.0.0.1';
        // also could use x-original-remote-host 
        Map<String, String> hdrs = ApexPages.currentPage().getHeaders();
        if (hdrs.get('x-original-remote-addr')!= null){
            ret =  hdrs.get('x-original-remote-addr');
        } else if (hdrs.get('X-Salesforce-SIP')!= null){
            ret =  hdrs.get('X-Salesforce-SIP');
        }
        return ret;
    }
}

}
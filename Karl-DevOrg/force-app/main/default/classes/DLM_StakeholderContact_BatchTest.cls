@istest
public class DLM_StakeholderContact_BatchTest {
     @testSetup
    public static void makeData(){
         List<contact> conList = new List<Contact>();
        List<Account> accountList = DLM_TestDataFactory.createAccounts(1, true);
        List<Contact> contactList = DLM_TestDataFactory.createContacts(2, accountList[0].id, true);
        
        for(contact con : contactList){
            con.Business_Unit__c = 'test busines unit';
            con.RecordTypeId = DLM_StakeholderContact_Batch.DLM_CONTACTRECORDTYPEID;
            conList.add(con);
        }
        conList[0].Org62_User_ID__c = 'QWERTYUIOPASDFGsJD';
        conList[1].Org62_User_ID__c = 'AJDUDHDLIHOIHDLIHD';
        conList[0].Is_Active__c = false;
        conList[1].Is_Active__c = true;
        
        update conList;
        
        List<IS_Standard__c> standardList = new List<IS_Standard__c>();
        
        IS_Standard__c standardRec = new IS_Standard__c();
        standardRec.Name = 'Test Std';
        standardRec.SFSS_Content_Owner_1__c = conList[0].id;
        standardRec.Standard_Hyperlink__c = 'www.google.com';
        standardList.add(standardRec);
        
        insert standardList;
        
        List<IS_Stakeholder__c> stakeList = DLM_TestDataFactory.createIsStakeholder(2,conList,standardList[0].id,false); 
        stakeList[1].Contact_Business_Unit__c = 'The BU Has Changed';
        insert stakeList;         
    }
    
    @istest
    public static void executeBatch(){
        
        Test.startTest();
        DLM_StakeholderContact_Batch bc = new DLM_StakeholderContact_Batch();
        Database.executeBatch(bc,200);
        Test.stopTest();
    }
    
    
}
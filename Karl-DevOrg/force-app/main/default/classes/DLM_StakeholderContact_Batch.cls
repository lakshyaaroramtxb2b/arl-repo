/*
* Class Name: DLM_StakeholderContact_Batch
* Description:Batch Utility to update stakeholder if contact is inactive or Bisuness unit change.
* Author/Date: Swarnima Singh Mandhata / 07-24-2020
*/
public class DLM_StakeholderContact_Batch implements Database.Batchable<sObject>,Database.Stateful, schedulable{
    Public static ID DLM_CONTACTRECORDTYPEID = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Salesforce Employee').getRecordTypeId();
    String query = 'SELECT Id, Name, IS_Standard__c, Contact__c,Contact__r.Business_Unit__c, Email__c, Contact_Business_Unit_Change__c, Stakeholder_Manager__c,'+ 
                    ' Contact_Business_Unit__c, Respond_Due_Date_7_Days__c, Contact__r.Is_Active__c, Contact_Is_Deactivated__c,Previous_Stakeholder__c,Contact__r.Name'+ 
                    ' FROM IS_Stakeholder__c'+
                    ' Where Contact__c != null AND Contact__r.RecordTypeId = \'' + DLM_CONTACTRECORDTYPEID +'\'';
    
    List<IS_Stakeholder__c> stakeholderList = new List<IS_Stakeholder__c>();
    
    public Database.QueryLocator start(Database.BatchableContext bc) {
        System.debug('query--> '+ query);
        return Database.getQueryLocator(query);
    }
    public void execute(Database.BatchableContext bc, List<IS_Stakeholder__c> sObjectList){
        
         for(sObject sObj : sObjectList) {
            IS_Stakeholder__c stakeholderRec = (IS_Stakeholder__c)sObj;
             if(stakeholderRec.Contact__r.Business_Unit__c != null){
                 if(stakeholderRec.Contact_Business_Unit__c != null && stakeholderRec.Contact_Business_Unit__c != stakeholderRec.Contact__r.Business_Unit__c){
                     stakeholderRec.Contact_Business_Unit__c = stakeholderRec.Contact__r.Business_Unit__c;
                     stakeholderRec.Contact_Business_Unit_Change__c = true;
                 }
                 else    {
                     stakeholderRec.Contact_Business_Unit__c = stakeholderRec.Contact__r.Business_Unit__c;
                 }
                 
             }
             if(stakeholderRec.Contact__r.Is_Active__c == false)    {
                 stakeholderRec.Contact_Is_Deactivated__c = true;
                 stakeholderRec.Previous_Stakeholder__c = stakeholderRec.Contact__r.Name;
             }
             else    {
                 stakeholderRec.Contact_Is_Deactivated__c = false;
             }
             
             stakeholderList.add(stakeholderRec);
         }
        
        if(!stakeholderList.isEmpty() && stakeholderList != null){
            update stakeholderList;
        }
        
    }
    public void finish(Database.BatchableContext bc){
         DLM_StakeholderContact_Batch batch = new DLM_StakeholderContact_Batch();
        if(!Test.isRunningTest()){
            System.scheduleBatch(batch, 'DLM_StakeholderContact_Batch',29);
        }
    }    
    
    public void execute(SchedulableContext sc) {
        
        DLM_StakeholderContact_Batch batch = new DLM_StakeholderContact_Batch();
        Database.executeBatch(batch,200);
        
    }
}
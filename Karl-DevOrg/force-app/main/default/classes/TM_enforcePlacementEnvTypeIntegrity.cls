/**
 * Trust Maturity Model v2
 * Sept 2015
 * Validate the Environment Type of the Placement, given its Cat+Level+Pri position.
 * For each Cat+Level+Pri position, you can have 1 placement with envType BU, or multiple placements with envType 'Parent - whatever'
 * 
 **/

public class TM_enforcePlacementEnvTypeIntegrity {
	private static final String businessUnitLabel = TM_Constants.BU_ENVIRONMENT_TYPE;
	
    
    public static void enforcePlacementEnvTypeIntegrity (List<TM_Placement__c> placementsFromTrigger) {
		Set<String> incomingPlacements = new Set<String>();
        
        // get a list of all the Cat+Level+Pri combinations of the new or updated Placememts
        for(TM_Placement__c p: placementsFromTrigger) {
            incomingPlacements.add(p.Category__c + ' ' + p.Level__c + ' ' + p.Priority__c);
            //System.debug('=============== incoming placement, current size ' + incomingPlacements.size() + ' :: ' + p.Category__c + ' ' + p.Level__c + ' ' + p.Priority__c + ' ' + p.Environment_Type__c);
        }
        
        // map <cat + level + pri, env types that exist at the catLevelPri position>
        map<String,String> foundPlacements = new map<String,String>();
        
    
        // create an inventory of which envTypes exist at which xy (cat+level+pri) position
        for(TM_Placement__c foundP: [SELECT Id, Category__c, Level__c, Priority__c, Environment_Type__c 
                            FROM TM_Placement__c 
                            WHERE Cat_Level_Pri__c IN :incomingPlacements
                                    ]) {
    
                                        String catLevelPri = foundP.Category__c + ' ' + foundP.Level__c + ' ' + foundP.Priority__c;
                                       
                                        String addToExistingEnvTypes;
                                        if (foundPlacements.get(catLevelPri) == null) {
                                            addToExistingEnvTypes = foundP.Environment_Type__c;
                                        } else  {
                                            addToExistingEnvTypes = foundPlacements.get(catLevelPri) + ' ' + foundP.Environment_Type__c;
                                        }
                                        
                                        foundPlacements.put(catLevelPri, addToExistingEnvTypes);
                                        //System.debug('=============== current inventory of found placements at ' + catLevelPri + ' is ' + addToExistingEnvTypes);
                                    }
        
    
        // If there were existing Placements, we need to validate the new or changed data against the existing data. 
        // If there weren't existing Placements, then there's no need to check. 
        if (!foundPlacements.isEmpty()) {
            
            // Validate the Environment Type of the Placement, given its Cat+Level+Pri position.
            // For each Cat+Level+Pri position, you can have 1 placement with envType BU, or multiple placements with envType 'Parent - whatever'
            // You cannot have a mix of BU and 'Parent's
            for(TM_Placement__c p2: placementsFromTrigger) {
            
                String envTypes = foundPlacements.get(p2.Category__c + ' ' + p2.Level__c + ' ' + p2.Priority__c);
                //System.debug('=============== Trigger Verif FoundPlacement ' + p2.Category__c + ' ' + p2.Level__c + ' ' + p2.Priority__c + ' ' + envTypes);
                
                // If the new/updated placement is of type Business Unit, no placement can already exist in the (cat + levelPriority) position
                if (p2.Environment_Type__c == businessUnitLabel) {
                    if (String.isNotBlank(envTypes)) {
                        //System.debug('=============== Trigger Error Thrown. Placement exists, new Placement cant be BU');
                        p2.addError('Error: Conflict with the Environment Type of existing Placement(s). This Placement cannot have Environment Type of Business Unit. Placement(s) with the same [Category + Level + Priority] already exist.');
                    }
                
                // If the new/updated placement has envType 'Parent', all existing placements must also be 'Parent'
                } else {
                    if (envTypes.contains(businessUnitLabel)) {
                        //System.debug('=============== Trigger Error Thrown. All existing placements must be type Parent');
                        p2.addError('Error: Conflict with the Environment Type of existing Placement(s). Placement(s) with the same [Category + Level + Priority] and Environment Type of Business Unit already exist.');
                    }
                }
        
            }
        }
        
    }
}
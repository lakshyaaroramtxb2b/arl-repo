@isTest
public class KARL_ChatterBeforeDueDateBatchTest {
	 /**
     * @author Lakshya Arora
     * @email lakshya.arora@mtxb2b.com
     * @desc Test data setup.
    */
    @testSetup
    private static void testSetup(){
        User opsAdminUser = ARL_TestDataFactory.createOpsAdmin();
        System.runAs(opsAdminUser){
            Contact c = new Contact();
            c.FirstName = 'Test Contact';
            c.LastName = 'Last Name';
            insert c;
            
            ADM_Work_c__x workRec = new ADM_Work_c__x();
            workRec.ExternalId = 'test';
            workRec.Due_Date_c__c = Datetime.now();
            
            Cycle_Request_Item__c cycleReqItem = ARL_TestDataFactory.createCycleRequestItem(null,null);
            cycleReqItem.Team__c = 'test';
            cycleReqItem.Cycle_Request_Status__c = 'New';
            cycleReqItem.Request_Tech_Details__c = 'testTechDetail';
            cycleReqItem.Request_GUS__c = workRec.ExternalId;
            cycleReqItem.GUS_Status__c = 'New';
            cycleReqItem.Request_Assignee__c = c.Id;
            insert cycleReqItem;
        }
    }
    
  /**
     * @author Lakshya Arora
     * @email lakshya.arora@mtxb2b.com
     * @desc method to have chatter before due date. Also can't use assert as can't insert feedItem using test class.
    */
	@isTest
    public static void testBatch(){
        String CRON_EXP = '0 0 0 15 3 ? *';
        List<User> opsAdminUserList = ARL_TestDataFactory.fetchUser('abc@b.com.d');
        if(!opsAdminUserList.isEmpty()){
            System.runAs(opsAdminUserList[0]){
                Test.startTest();
                KARL_ChatterBeforeDueDateBatch obj = new KARL_ChatterBeforeDueDateBatch();
                DataBase.executeBatch(obj,200);
                System.schedule('KARL_ChatterBeforeDueDateBatch',  CRON_EXP, new KARL_ChatterBeforeDueDateBatch()); 
                Test.stopTest();
                try{
                    //will through error 
                    KARL_ChatterBeforeDueDateBatch.start();
                }
                catch(Exception e){
                    
                } 
            }
        }
        
    }
}
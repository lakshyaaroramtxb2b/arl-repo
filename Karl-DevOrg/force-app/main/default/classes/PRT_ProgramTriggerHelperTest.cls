/* created by - Swarnima Singh Mandhata
 * Class: PRT_ProgramTriggerHelper
 * */
@isTest
public class PRT_ProgramTriggerHelperTest {
    @testSetup
    public static void testSetup(){
        
        List<User> userList = new List<User>();
        userList = PRT_TestDataFactory.createUsers(1,'System Administrator',true);
        System.runAs(userList[0]){
            PRT_Integration_Settings__c integrationSetting = new PRT_Integration_Settings__c();
            integrationSetting.GUS_Program_Integration__c = true;
            integrationSetting.Disable_Program_Trigger__c = false;
            insert integrationSetting; 
            List<Portfolio__c> portfolioDataList = PRT_TestDataFactory.createPortfolio(1,true);
        } 
    }
    static testMethod void testinsertGUSRecords(){
        Boolean triggerValue = PRT_Constants.RECURSIVE_TRIGGER_CHECK;
        Boolean batch = !System.isBatch();
        Boolean futureCheck = !System.isFuture();
        List<User> userList = new List<User>([SELECT Id FROM USER WHERE Email Like 'PRTUser%' LIMIT 1]);
        program__c prog = new program__c();
        prog.Name = 'testProgram';
        prog.Program_Status__c = 'Active';
        prog.Program_Owner__c = userList[0].id;
        prog.Program_Manager__c = userList[0].id;
        prog.Executive_Sponsor__c = userList[0].id;
        prog.Portfolio__c = null;
        //prog.Budget_Allocated__c = 23.00;
        prog.Program_Summary__c ='test text Summary';
        triggerValue = true;
        test.startTest();
        System.runAs(userList[0]){
            insert prog;
        }
        	
        test.stopTest();
    }
    static testMethod void testupdateGUSRecords(){
        Boolean triggerValue = PRT_Constants.RECURSIVE_TRIGGER_CHECK;   
        System.debug('triggerValue'+triggerValue);
        Boolean batch = !System.isBatch();
        Boolean futureCheck = !System.isFuture();
        List<User> userList = new List<User>([SELECT Id FROM USER WHERE Email Like 'PRTUser%' LIMIT 1]);
		
        program__c progRec = new program__c();
        progRec.Name = 'testProgramforupdate';
        progRec.Program_Status__c = 'Active';
        progRec.Program_Owner__c = userList[0].id;
        progRec.Program_Manager__c = userList[0].id;
        progRec.Executive_Sponsor__c = userList[0].id;
        progRec.Portfolio__c = null;
        progRec.Budget_Allocated__c = 23.00;
        progRec.Program_Summary__c ='test text Summary';
        insert progRec;
		
		List<Portfolio__c> portfolioDataList = new List<Portfolio__c>([SELECT Id FROM Portfolio__c LIMIT 1]);
        
        program__c prog = new program__c();
        prog.Name = 'testProgramforupdate';
        prog.Program_Status__c = 'Active';
        prog.Program_Owner__c = userList[0].id;
        prog.Program_Manager__c = userList[0].id;
        prog.Executive_Sponsor__c = userList[0].id;
        prog.Portfolio__c = portfolioDataList[0].Id;
        prog.Parent_Program__c = progRec.Id;
        prog.Budget_Allocated__c = 23.00;
        prog.Program_Summary__c ='test text Summary';
        insert prog;
        test.startTest();
            prog.Budget_Allocated__c = 33.00;
        	prog.Program_Status__c = 'Inactive';
        	//triggerValue = true;
        System.runAs(userList[0]){
            update prog;
        }
			
        test.stopTest();
    }
}
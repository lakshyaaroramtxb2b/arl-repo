public class CSIRT_ARES_InternalOrgIntegration {
    static String LOGIN_URL = 'https://login.salesforce.com/services/oauth2/token';
    static String ACCESS_TOKEN;
    static String INSTANCE_URL;
    static String USER_AGENT = 'ARES-SECORG/1.0 (SFDC CSIRT)';
    static String OrgName;
    
    public CSIRT_ARES_InternalOrgIntegration(String OrgName){
        OrgName = OrgName;
        Authenticate();
    }
    
    static void Authenticate(){
        ARES_OrgIntegration__c OrgIntegration = ARES_OrgIntegration__c.getInstance(OrgName);
        ARES_Integration_Org_User__c IntegrationUser = ARES_Integration_Org_User__c.getInstance(OrgName);
        
        Map<String, String> body = new Map<String, String>();
        body.put('grant_type', 'password');
        
        body.put('client_id', OrgIntegration.Client_Id__c);
        body.put('client_secret', OrgIntegration.Client_Secret__c);
        body.put('username', IntegrationUser.Username__c);
        body.put('password', IntegrationUser.Password__c);
        
        HttpRequest req = new HttpRequest();
        req.setMethod('POST');
        req.setEndpoint(LOGIN_URL);
        req.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        req.setHeader('User-Agent', USER_AGENT);
        
        String reqBody = 'grant_type='+body.get('grant_type')+'&client_id='+body.get('client_id')+'&client_secret='+body.get('client_secret')+'&username='+body.get('username')+'&password='+body.get('password');
        req.setBody(reqBody);
        
        Http http = new Http();
        HTTPResponse res = http.send(req);
        
        if(res.getStatusCode() == 200){
            JSONParser parser = JSON.createParser(res.getBody());
        	while (parser.nextToken() != null){
            	if((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'instance_url')){
                	parser.nextToken();
                	INSTANCE_URL = parser.getText();
            	}
            
            	if((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'access_token')){
                	parser.nextToken();
                	ACCESS_TOKEN = parser.getText();
            	}
        	}
        }
    }
    
    List <Object> QueryREST(String Query){
        //TODO: Figure out if it's really having such a generic method vs specific methods to return objects; specific methods we can cast fields to their types
        Map<String, String> return_map = new Map<String, String>();
        
        String query_encoded = EncodingUtil.urlEncode(query, 'UTF-8');
        String base_url = INSTANCE_URL + '/services/data/v39.0/query/?q=' + query_encoded;
        
        HttpRequest req = new HttpRequest();
        req.setMethod('GET');
        req.setEndpoint(base_url);
        req.setHeader('Authorization', 'Bearer ' + ACCESS_TOKEN);
        req.setHeader('User-Agent', USER_AGENT);
        
        Http http = new Http();
        HTTPResponse res = http.send(req);
        
        if(res.getStatusCode() == 200){
            Map <String, Object> root = (Map <String, Object>) JSON.deserializeUntyped(res.getBody());
            List <Object> records = (List<Object>) root.get('records');
            return records;
        } else {
            return null;
        }
    }
    
    public String GetAccountInfo(String OrgId){
        String OrgId_Escaped = String.escapeSingleQuotes(OrgId);
        String query = 'SELECT Id, Name, OwnerId, Organization_ID_DW__c FROM Account WHERE Organization_ID_DW__c = \'' + OrgId_Escaped + '\'';
        List<Object> records = QueryRest(query);
        
        Map <String, Object> record = New Map <String, Object>();
        
        if(records.size() > 1){
            // As of 24 AUG 2017 this should NEVER happen based on query of Org62:
            // $ cat AccountOrgIds.csv | sort | uniq | wc -l
            // 293010
            // $ cat AccountOrgIds.csv | sort | wc -l
            // 293010
            System.debug('Multiple Accounts Returned for OrgId: ' + OrgId_Escaped);
            return Null;
        } else if(records.size() == 0){
          	System.debug('No Account Found');
            record.put('status', 'None');
        } else {
            record = (Map <String, Object>) records[0];
            record.remove('attributes'); // Unneeded; Rest of Keys are utilized
            record.put('status', 'Found');
        }

        return JSON.serialize(record);
    }

    
    public String GetAccountSecurityContacts(Id AccountId){
        String query = 'SELECT Contact__r.Id, Contact__r.FirstName, Contact__r.LastName, Contact__r.Email, Prefs_JSON__c FROM HT_NT_PREFS__c WHERE Notification__r.Name = \'ProdSecSecAlert\' AND Contact__r.Account.Id = \'' + String.valueOf(AccountId) + '\'';
    	List<Object> records = QueryRest(query);
        List<Map<String, Object>> security_list = new List<Map<String, Object>>();
        
        if(records.size() > 0){
            for(Object record : records){
            	Map <String, Object> record_map = (Map <String, Object>) record;
            	String json_prefs = String.valueOf(record_map.get('Prefs_JSON__c'));
            	Map <String, Object> pref_map = (Map <String, Object>) JSON.deserializeUntyped(json_prefs);
            	if((Boolean) pref_map.get('Email') != True){
                	continue; // Only care about Contacts we can email
            	}
                Map<String, Object> contact_details = (Map <String, Object>) JSON.deserializeUntyped(String.valueOf(record_map.get('Contact__r')));
                contact_details.remove('attributes'); // Unneeded; Rest of Keys are utilized
                contact_details.put('email_pref', (Boolean) pref_map.get('Email'));
            	
                security_list.add(contact_details);
        	}
        } else {
            System.debug('No Security Contacts Found');
            Map<String, Object> not_found_map = new Map<String, Object>();
            not_found_map.put('Result', 'None');
            security_list.add(not_found_map);
        }

        return JSON.serialize(security_list);
    }
    
    
    public String GetAccountAdminContacts(Id AccountId){
        String query = 'SELECT Id, FirstName, LastName, Email, Primary_Contact__c FROM Contact WHERE SFDC_SysAdmin__c = True AND No_Longer_w_Company__c = False AND AccountId = \'' + String.valueOf(AccountId) + '\'';
		
        List<Object> records = QueryRest(query);
        List<Map<String, Object>> contact_list = new List<Map<String, Object>>();
        
        if(records.size() > 0){
        	for(Object record : records){
                Map <String, Object> record_map = (Map <String, Object>) record;
            	record_map.remove('attributes'); // Unneeded; Rest of Keys are utilized
                contact_list.add(record_map);
            }
        } else {
            System.debug('No Account Admin Contacts Found');
            Map<String, Object> not_found_map = new Map<String, Object>();
            not_found_map.put('Result', 'None');
            contact_list.add(not_found_map);
        }
 
        return JSON.serialize(contact_list);
    }
    
    
    static String JSONPostInstance(String url, String jsonbody, Boolean useAssignmentRules){
        HttpRequest req = new HttpRequest();
        req.setMethod('POST');
        req.setEndpoint(url);
        req.setHeader('Authorization', 'Bearer ' + ACCESS_TOKEN);
        req.setHeader('User-Agent', USER_AGENT);
        
        if(useAssignmentRules == True){
            req.setHeader('SForce-Auto-Assign', 'True');
        }
        
        req.setBody(jsonBody);
        
        Http http = new Http();
        HTTPResponse res = http.send(req);
        
        if(res.getStatusCode() == 200){
            return res.getBody();
        } else {
            return null;
        }
    }
    
    
    public void PostToChatter(Id ObjectId, String Message){
        String base_url = INSTANCE_URL + '/services/data/v39.0/chatter/feed-elements';
        
        ChatterItem chatter_msg = new ChatterItem();
        chatter_msg.body = Message;
        chatter_msg.feedElementType = 'FeedItem';
        chatter_msg.ObjectId = ObjectId;
        
        String chatter_json = JSON.serialize(chatter_msg);
        JSONPostInstance(base_url, chatter_json, False);
    }
    
    
    public Id CreateCustomerMalwareCase(Id AccountId, Id ContactId, String Subject, String Description){
        String base_url = INSTANCE_URL + '/services/data/v39.0/sobjects/Case/';
        
        Org62Case new_case = new Org62Case();
        new_case.RecordTypeId = '01230000000003eAAA'; // Hardcoded for now; May change in the future and/or convert to Query for RT first for FutureProofing.
        new_case.AccountId = AccountId;
        new_case.ContactId = ContactId;
        new_case.IsVisibleInSelfService = True;
        new_case.Subject = Subject;
        new_case.Description = Description;
        new_case.Reason = 'Setup & Security';
        new_case.FunctionalAreaCUSTOMOBJ = 'Security Alert';
        new_case.General_Application_AreaCUSTOMOBJ = 'Security';
        new_case.Origin = 'Trust';
        new_case.Severity_LevelCUSTOMOBJ = 'Level 4 - Medium';
        
        String case_json = JSON.serialize(new_case);
        case_json = case_json.replaceAll('CUSTOMOBJ', '__c');
        
        String json_response = JSONPostInstance(base_url, case_json, False);
        OrgJSONResponse resp;
        if(json_response != null){
            try{
                resp = (OrgJSONResponse)JSON.deserialize(json_response, OrgJSONResponse.class);
            } catch(Exception e){
                System.debug('Unexpected Result from Malware Case Creation - Case NOT Created');
                return null;
            }
        }
        
        return resp.id;
        
    }
    
    
    class ChatterItem{
        public String body;
        public String feedElementType;
        public Id ObjectId;
    }
    
    
    class Org62Case{
        public Id RecordTypeId;
        public Id OwnerId;
        public Id AccountId;
        public Id ContactId;
        public Boolean IsVisibleInSelfService;
        public string Subject;
        public string Description;
        public string Reason;
        public string FunctionalAreaCUSTOMOBJ;
        public string General_Application_AreaCUSTOMOBJ;
        public string Origin;
        public string Severity_LevelCUSTOMOBJ;
    }
    
    class OrgJSONResponse
    {
        public Id id;
        public string success;
        public string [] errors;
    }
}
/*created by- Swarnima Singh Mandhata
 * test class for PRT_V2MOMAlignmentViewController
 * date - 02/20/2020
*/

@isTest
public class PRT_V2MOMAlignmentViewControllerTest {
    @testSetup 
    static void testSetup(){
        List<User> userList = new List<User>();
        userList = PRT_TestDataFactory.createUsers(3,'System Administrator',true);
    }
    @isTest
    public static void getV2MomInformationTest(){
        List<User> userRecord = [SELECT Id,Name FROM USER WHERE Email Like 'PRTUser%'LIMIT 3];
        userRecord[0].FirstName = 'Test Measure';
        userRecord[0].EmployeeNumber = '725957';
        update userRecord;
        String userId1 = 'EX010';
        String userId3 = userRecord[0].id;
        String userId2 = 'EM101';
        string userId4 = userInfo.getUserId();
        String publishDate = String.ValueOf(date.today());
        
        String v2MomJSON = '{"attributes":{"type":"V2MOM_c__x","url":"/services/data/v48.0/sobjects/V2MOM_c__x/0054D000001WzcyQAC"},"V2MOM_User_c__c":"'+ userId3+'","ExternalId":"'+userId1+'","Name__c":"Test 123","Published_on_c__c":"'+publishDate+'"}';
        String measureJSON = '{"attributes":{"type":"Measure_c__x","url":"/services/data/v48.0/sobjects/Measure_c__x/0054D000001VnAMQA0"},"MeasureName_c__c":"test measure1","ExternalId":"'+userId2+'","V2MOM_c__c":"'+userId1+'"}';
        String methodJSON = '{"attributes":{"type":"V2MOM_Method_c__x","url":"/services/data/v48.0/sobjects/V2MOM_Method_c__x/0054D000001WzcyQAC"},"Name__c":"test method1","Description_c__c":"test desc","V2MOM_c__c":"'+userId1+'","ExternalId":"'+userId4+'"}';
    
        PRT_V2MOMAlignmentViewController.mockallV2MOMList.add((V2MOM_c__x)JSON.deserialize(v2MomJSON,V2MOM_c__x.class ));
        PRT_V2MOMAlignmentViewController.mockAllMeasureList.add((Measure_c__x)JSON.deserialize(measureJSON,Measure_c__x.class ));
        PRT_V2MOMAlignmentViewController.mockAllMethodList.add((V2MOM_Method_c__x)JSON.deserialize(methodJSON,V2MOM_Method_c__x.class ));
        
        List<V2MomMapping__c> vmapList = new List<V2MomMapping__c>();
        V2MomMapping__c vmap = new V2MomMapping__c();
        vmap.Controlling_Measure__c = userRecord[0].Id;
        vmap.Dependent_Method__c = userInfo.getUserId();
        vmapList.add(vmap);
        insert vmapList;

        test.startTest();
        PRT_V2MOMAlignmentViewController.getV2MomInformation();
        test.stopTest();
    }
    
    @isTest
    public static void createV2MappingTest(){
        List<User> userRecord = [SELECT Id,Name FROM USER WHERE Email Like 'PRTUser%'LIMIT 3];
        List<V2MomMapping__c> vmapList = new List<V2MomMapping__c>();
        Map<String,String> dependantIdVsControllingId = new Map<String,String>();
        V2MomMapping__c vmap = new V2MomMapping__c();
        vmap.Controlling_Measure__c = userRecord[0].Id;
        vmap.Dependent_Method__c = userInfo.getUserId();
        dependantIdVsControllingId.put(vmap.Dependent_Method__c,vmap.Controlling_Measure__c);
        vmapList.add(vmap);
        insert vmapList;
        test.startTest();
        PRT_V2MOMAlignmentViewController.createV2Mapping(dependantIdVsControllingId);
        test.stopTest();
        
    }
}
@IsTest
public class SyncEmployeesFromSForceTest {

    @IsTest
    static void testBatch() {
    	sfutil.ESASupportForceUtilTestCallOutMocker callOutMocker = new sfutil.ESASupportForceUtilTestCallOutMocker();
        Test.setMock(HttpCalloutMock.class, callOutMocker);
        
        Test.startTest();
        SyncEmployeesFromSForce batchjob = new SyncEmployeesFromSForce();
        Database.executeBatch(batchjob);
        Test.stopTest();
    }

    @IsTest
    static void testGetLatestEmployeeCreatedDate() {
     
        Datetime
            now        = Datetime.now(),
            yesterday  = now.addDays(-1),
            lastWeek   = now.addDays(-7),
            latestDate = null,
            epoch      = Datetime.newInstance(1970, 1, 1, 0, 0, 0);

        SyncEmployeesFromSForce batchjob = new SyncEmployeesFromSForce();

        // Test with no leads in the database
        latestDate = batchjob.getLatestEmployeeCreatedDate();
        System.assertEquals(epoch, latestDate);

        // Test with leads in the database
        insert new INTEG_Employee__c[]{
            new INTEG_Employee__c(Name = 'A', INTEG_Email__c = 'a@a.com', SForceContactLastModifiedDate__c = lastWeek),
            new INTEG_Employee__c(Name = 'B', INTEG_Email__c = 'b@b.com', SForceContactLastModifiedDate__c = yesterday)
        };

        latestDate = batchjob.getLatestEmployeeCreatedDate();
        System.assertEquals(yesterday, latestDate);
    }
}
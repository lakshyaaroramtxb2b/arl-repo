/**
* @author Lakshya Arora
* @email lakshya.arora@mtxb2b.com
* @description Test class for KARL_GenerateCycleAuditReportController
*/
@isTest
public class KARL_GenerateCycleReportCtrTest {
    
    /**
* @author Lakshya Arora
* @email lakshya.arora@mtxb2b.com
* @description This method test the functionality of cycle request item creation
*/
    @isTest
    private static void createCycleRequestItemsTest(){
        User opsAdminUser = ARL_TestDataFactory.createOpsAdmin();
        System.runAs(opsAdminUser){
            Map<Integer,String> priorityToScopeMap = new Map<Integer,String>();
            priorityToScopeMap.put(0,'AS');
            priorityToScopeMap.put(1,'AUST');
            priorityToScopeMap.put(2,'B2BC');
            priorityToScopeMap.put(3,'CHAT');
            
            Audit_Cycle__c auditCycle = ARL_TestDataFactory.createAuditCycle();
            insert auditCycle;
            KARL_Audit_Firm__c auditFirm = ARL_TestDataFactory.createAuditFirm('testAuditFirm');
            insert auditFirm;
            
            // Create two distinct audit teams
            KARL_Audit_Team__c auditTeam = ARL_TestDataFactory.createAuditTeam('testAuditTeam1');
            auditTeam.KARL_Audit_Firm__c = auditFirm.Id;
            insert auditTeam;
            
            List<KARL_Audit_Scope_Master_Data__c> auditScopeList = new List<KARL_Audit_Scope_Master_Data__c>();
            for(Integer i=0;i<4;i++){
                KARL_Audit_Scope_Master_Data__c auditScope = ARL_TestDataFactory.createAuditScope();
                auditScope.KARL_Scope__c = priorityToScopeMap.get(i);
                auditScopeList.add(auditScope);
            }
            insert auditScopeList;
            
            List<KARL_Audit_Scope_Reports__c> auditScopeReportList = new List<KARL_Audit_Scope_Reports__c>();
            for(Integer i=0;i<4;i++){
                KARL_Audit_Scope_Reports__c auditScopeReport = ARL_TestDataFactory.createAuditScopeReport(auditScopeList[i].Id,auditFirm.Id);
                auditScopeReportList.add(auditScopeReport);
            }
            insert auditScopeReportList;
            
            KARL_Gus_Product_Tag__c defaultProductTag = ARL_TestDataFactory.createKarlGusProductTag(true,true);
            insert defaultProductTag;
            
            List<KARL_Auto_Assignment_Configuration__c> autoAssignmentConfigList = ARL_TestDataFactory.createAutoAssignmentConfig(defaultProductTag.Id,4);
            autoAssignmentConfigList[1].Priority_0_Assignment_Date__c = System.today().addDays(5);
            autoAssignmentConfigList[2].Priority_0_Assignment_Date__c = System.today().addDays(-1);
            autoAssignmentConfigList[3].Priority_0_Assignment_Date__c = System.today();
            insert autoAssignmentConfigList;
            
            List<Audit_Cycle_Coverage__c> auditCycleCoverageList = new List<Audit_Cycle_Coverage__c>();
            
            for(Integer i=0;i<4;i++){
                Audit_Cycle_Coverage__c auditCycleCoverage = new Audit_Cycle_Coverage__c();
                auditCycleCoverage.Audit_Cycle__c = auditCycle.Id;
                auditCycleCoverage.Area__c = 'iRAP';
                auditCycleCoverage.Scope__c = priorityToScopeMap.get(i);
                auditCycleCoverage.Auto_Assignment_Configuration__c = autoAssignmentConfigList[i].Id;
                auditCycleCoverage.KARL_Audit_Team__c = auditTeam.Id;
                auditCycleCoverageList.add(auditCycleCoverage);
            }
            insert auditCycleCoverageList;
            
            KARL_GenerateCycleAuditReportController.createCycleRequestItems(auditCycle.Id);
            System.assertEquals([SELECT Id FROM KARL_Cycle_Audit_Report_Items__c].size(),4);
        }
    }
    
    /**
* @author Lakshya Arora
* @email lakshya.arora@mtxb2b.com
* @description This method test all the custom errors while generating the cycle request item records
*/
    @isTest
    private static void createCycleRequestItemsErrorsTest(){
        User opsAdminUser = ARL_TestDataFactory.createOpsAdmin();
        System.runAs(opsAdminUser){
            Audit_Cycle__c auditCycle = ARL_TestDataFactory.createAuditCycle();
            insert auditCycle;
            try{
                KARL_GenerateCycleAuditReportController.createCycleRequestItems(auditCycle.Id);
            }
            catch(Exception e){
                System.assert(e.getMessage().contains('Script-thrown exception'));
            }
        }
    }
}
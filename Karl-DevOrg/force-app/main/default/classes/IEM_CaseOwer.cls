/***************IEM_CaseOwer****************
@Author Banshi
@Date 09/09/2019
@Description Controller class to get next case owner based on round robin.
**********************************************/
public class IEM_CaseOwer {
    public static List<User> userList = new List<User>();//Ordered List of users.
    public static String lastOwnerId = '';//populate only if last owner doesn't exists in the group anymore.
    public static Id lastCaseOwnerId;
    public static Boolean excludeOwner = false;//populate only if last owner doesn't exists in the group anymore.
    
    // @purpose
    //      Method to get next case owner based on last case.
    // @param
    // @return 
    //  Id = Next Case Owner
    public Id getNextCaseOwner(){ 
        Id nextOwnerId;
        //Fetch IEM_Case_Analysts group Id
        List<Group> groupList = [SELECT Id FROM Group WHERE DeveloperName=:IEM_Constants.ISSUE_ANALYST_GROUP_NAME LIMIT 1];
        if(!groupList.isEmpty()){
            Set<Id> userIds = new Set<Id>(); //To store group userIds
            //Fetch all the group member
            for(GroupMember  gm :[select UserOrGroupId from GroupMember where groupId = :groupList[0].Id]){   
                userIds.add(gm.UserOrGroupId);
            }
            if(!userIds.isEmpty()){
                //Fetch the latest case.
                List<Case> caseList = [Select Id,OwnerId From Case Where RecordTypeId= :IEM_Constants.IEM_CASERECORDTYPEID ORDER By CreatedDate DESC LIMIT 1];
                if(!caseList.isEmpty()){
                    //it means last case owner doesn't exists into the group anymore.
                    if(!userIds.contains(caseList[0].OwnerId)){
                        lastOwnerId = caseList[0].OwnerId; //for escap purpose later on.
                    }
                    lastCaseOwnerId = caseList[0].OwnerId;
                    userIds.add(caseList[0].OwnerId); //put the ownerId to maintain the assignment sequence.
                }
                for(User u : [SELECT Id,Name FROM User WHERE Id IN :userIds ORDER BY Name ASC]){
                    userList.add(u);
                }
                if(!userList.isEmpty()){
                    nextOwnerId = getNextCaseOwner(lastCaseOwnerId);
                }
            }
        }
        return nextOwnerId;
    }
    
    // @purpose
    //      Method to get next case owner based on previous case owner Id.
    // @param
    // 		previousOwnerId = Previous case owner Id
    // @return 
    //  Id = Next Case Owner
    public Id getNextCaseOwner(Id previousOwnerId){
        Id nextOwnerId;
        Integer userIndex = 0; //default
        Integer listSize = userList.size();
        if(listSize > 0){
            if(previousOwnerId != null){
                for(integer i=0;i < listSize; i++){
                    if(previousOwnerId == userList[i].Id){
                        //if it is the last person then no need to increment the index by 1.
                        if(i < listSize - 1){
                            userIndex = i + 1;
                            break;
                        }
                    }
                } 
                //if lastOwnerId is not blank, it means this user doesn't exists into the group, so lets move for next user.
                if(String.isNotBlank(lastOwnerId) && userList[userIndex].Id == lastOwnerId){
                    if(userIndex == listSize - 1){
                        userIndex = 0;
                    }else{
                        userIndex = userIndex + 1;
                    }
                }
             }
            nextOwnerId = userList[userIndex].Id;
        }
        return nextOwnerId;
    }
}
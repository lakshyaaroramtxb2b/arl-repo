public with sharing class INTEG_EmployeeToContactMapper {
    public class INTEG_EmployeeToContactMapperException extends Exception {}

    public static final INTEG_Logging LOGGER;

    public RecordType rt;
    public static final Account SFDCACCOUNT;

    static {
        Id rcId = Schema.Sobjecttype.Account.getRecordTypeInfosByName().get('Salesforce Business Unit').getRecordTypeId();
        SFDCACCOUNT = [SELECT Id FROM Account WHERE Name='salesforce.com' and RecordTypeId=:rcId].get(0);

        LOGGER = INTEG_Logging.getLogger(INTEG_EmployeeToContactMapper.class);
    }

    // Mapping of INTEG__Employee__c fields to Contact
    public static final Map<String,String> FIELDMAP = new Map<String, String>{
        'INTEG_Active__c' => 'Is_Active__c',
        'INTEG_Business_Unit__c' => 'Business_Unit__c',
        'INTEG_Company__c' => 'Company__c',
        'INTEG_Continuous_Service_Date__c' => 'Continuous_Service_Date__c',
        'INTEG_Cost_Center__c' => '',
        'INTEG_Date_Of_Birth_Without_Year__c' => 'Date_of_Birth_Without_Year__c',
        'INTEG_Division__c' => 'Division__c',
        'INTEG_Email__c' => 'Email',
        'INTEG_Employee_ID__c' => 'Employee_ID__c',
        'INTEG_Employee_Type__c' => 'Employee_Type__c',
        'INTEG_First_Name__c' => 'FirstName',
        'INTEG_Grade_Group__c' => 'Grade_Group__c',
        'INTEG_Hire_Date__c' => 'Hire_Date__c',
        'INTEG_Is_Manager__c' => 'Is_Manager__c',
        'INTEG_Job_Family__c' => 'Job_Family__c',
        'INTEG_Job_Profile__c' => 'Job_Profile__c',
        'INTEG_Job_Profile_Start_Date__c' => 'Time_in_Job_Profile_Start_Date__c',
        'INTEG_Job_Title__c' => 'Job_Title__c',
        'INTEG_Last_Day_Worked__c' => 'Termination_Date__c',
        'INTEG_Last_Name__c' => 'LastName',
        'INTEG_Legal_First_Name__c' => 'Legal_Name_First_Name__c',
        'INTEG_Legal_Last_Name__c' => 'Legal_Name_Last_Name__c',
        'INTEG_Location__c' => 'Location__c',
        'INTEG_Manager__c' => '', // This is a FK to Employee__c so not used
        'INTEG_Manager_Email__c' => 'Manager_Email__c',
        'INTEG_Gov_Cloud_SPX_Production_Access__c' => 'Gov_Cloud_SPX_Production_Access__c',
        'INTEG_GIA2__c' => 'GIA2__c',
        'INTEG_Manager_ID__c' => 'Manager_ID__c',
        'INTEG_Manager_Level_1__c' => 'Management_Chain_Level_01__c',
        'INTEG_Manager_Level_2__c' => 'Management_Chain_Level_02__c',
        'INTEG_Manager_Level_3__c' => 'Management_Chain_Level_03__c',
        'INTEG_Manager_Level_4__c' => 'Management_Chain_Level_04__c',
        'INTEG_Manager_Level_5__c' => 'Management_Chain_Level_05__c',
        'INTEG_Manager_Level_6__c' => 'Management_Chain_Level_06__c',
        'INTEG_Manager_Level_7__c' => 'Management_Chain_Level_07__c',
        'INTEG_Manager_Level_8__c' => 'Management_Chain_Level_08__c',
        'INTEG_Manager_Level_9__c' => 'Management_Chain_Level_09__c',
        'INTEG_Manager_Level_10__c' => 'Management_Chain_Level_10__c',
        'INTEG_Manager_Level_11__c' => 'Management_Chain_Level_11__c',
        'INTEG_Manager_Level_12__c' => 'Management_Chain_Level_12__c',
        'INTEG_Manager_Level_13__c' => 'Management_Chain_Level_13__c',
        'INTEG_Manager_Level_14__c' => 'Management_Chain_Level_14__c',
        'INTEG_Manager_Level_15__c' => 'Management_Chain_Level_15__c',
        'INTEG_Manager_Title__c' => 'Manager_Title__c',
        'INTEG_Original_Hire_Date__c' => 'Original_Hire_Date__c',
        'Company_Agency_Name__c' => 'Company_Agency_Name__c',
        'INTEG_Phone__c' => 'Phone',
        'INTEG_Title__c' => 'Title',
        'Org62_User__c' => 'Org62User__c',
        'Duplicate__c' => 'Duplicate__c',
        'Id' => 'Employee__c',
        'Supportforce_Contact__c' => 'Supportforce_Contact__c',
        'INTEG_Is_On_Leave__c' => 'Is_on_Leave_Of_Absence__c',
        'INTEG_Org62UserId__c' => 'Org62_User_ID__c',
        'Federation_Identifier__c' => 'Federation_Identifier__c',
        'JobCode__c' => 'JobCode__c',
        'SupportForce_User__c' => 'SupportForce_User__c',
        'INTEG_Source__c' => '', // not used
        'IsContractor__c' => '' // not used
    };

    public INTEG_EmployeeToContactMapper() {
        rt = INTEG_EmployeeToContactMapper.recordTypeForContact();
    }

    public static RecordType recordTypeForContact() {
        /*
         * Returns the appropriate record type for the contacts used to house employeee records
         */
         RecordType[] res = [SELECT Id,Name FROM RecordType WHERE DeveloperName='Salesforce_Employee'
                             AND sobjecttype = 'Contact' AND IsActive=true AND NamespacePrefix = ''];
         if (res.size() == 0) {
             throw new INTEG_EmployeeToContactMapperException('Unable to find Contact RecordType with name Salesforce_Employee');
         }
         return res.get(0);
    }

    public static Boolean isMappable(INTEG_Employee__c emp) {
        return (emp.INTEG_Org62UserId__c != null);
    }

    public Map<INTEG_Employee__c, Contact> contactsForEmployees(INTEG_Employee__c[] emps) {
        // We've switched this to exclusively use org 62 user ID instead of email for contractors
        // and employee ID for full time
        Set<String> org62Ids = new Set<String>();
        for (INTEG_Employee__c emp : emps) {
            if (!isMappable(emp)) {
                LOGGER.warn('INTEG_Employee_ID__c is required to map employee to contact. Record ID '+emp.Id);
                continue;
            }
            org62Ids.add(emp.INTEG_Org62UserId__c);
        }
        Map<String, Contact> by62Id = new Map<String, Contact>();
        for (Contact c : [SELECT Id,CreatedDate,SystemModstamp,Org62_User_ID__c, Employee_ID__c, Email FROM Contact Where RecordTypeId=:rt.Id
                         AND Org62_User_ID__c IN: org62Ids and Org62_User_ID__c != null]) {
            by62Id.put(c.Org62_User_ID__c, c);
        }

        Map<INTEG_Employee__c, Contact> res = new Map<INTEG_Employee__c, Contact>();
        for (INTEG_Employee__c emp : emps) {
            Contact c = by62Id.get(emp.INTEG_Org62UserId__c);
            res.put(emp, c); // "c" may be null
        }

        return res;
    }

    public static Contact copyEmployeeFieldsToContact(INTEG_Employee__c emp, Contact cont) {
        for (String empKey : FIELDMAP.keySet()) {
            String contactKey = FIELDMAP.get(empKey);
            if (String.isBlank(contactKey)) {
                continue; // This field doesn't get mapped
            }
            emp.get(empKey);
            cont.put(contactKey, emp.get(empKey));
        }
        cont.Manager__c = emp.INTEG_Manager__r.Name;
        cont.Manager_First_Name__c = emp.INTEG_Manager__r.INTEG_First_Name__c;
        cont.Manager_Last_Name__c = emp.INTEG_Manager__r.INTEG_Last_Name__c;
        cont.Manager_Location__c = emp.INTEG_Manager__r.INTEG_Location__c;
        if (emp.INTEG_Last_Sync_Date__c != null) {
            cont.Last_Import_Date__c = emp.INTEG_Last_Sync_Date__c.date();
        }
        return cont;
    }

    public static Database.QueryLocator getEmployeesQueryLocator(DateTime modifiedSince) {
        // Used to get the QueryLocator for starting batch jobs
        String fields = String.join(new List<String>(FIELDMAP.keySet()),',');
        String timeField = 'SystemModStamp';
        if (Test.isRunningTest()) {
            timeField = 'CreatedDate'; // because we can't change SystemModStamp in tests
        }
        String soql = 'SELECT Name,SystemModStamp, CreatedDate,INTEG_Last_Sync_Date__c, INTEG_Manager__r.Name, INTEG_Manager__r.INTEG_First_Name__c, INTEG_Manager__r.INTEG_Last_Name__c, INTEG_Manager__r.INTEG_Location__c,'+fields+' FROM INTEG_Employee__c WHERE '+timeField+' >= '+modifiedSince.format('yyyy-MM-dd\'T\'hh:mm:ss\'z\'')+' AND (NOT INTEG_Org62UserId__c LIKE \'003%\')';

        return Database.getQueryLocator(soql);
    }

    public Integer updateContactsForEmployees(INTEG_Employee__c[] emps) {
        // we were getting errors about having the same contact in update list twice
        // This will map contact Id to employee Id so we know the problem records
        Map<Id,Id> updatedContactIds = new Map<Id,Id>();

        Contact[] updated = new List<Contact>();
        Map<INTEG_Employee__c,Contact> mapped = contactsForEmployees(emps);
        for (INTEG_Employee__c emp : mapped.keySet()) {
            if (!isMappable(emp)) {
                // the contact would be null here since this is missing the employee ID. Skip it
                LOGGER.debug('Skipping unmappable employee with ID '+emp.id+' first name '+emp.INTEG_First_Name__c);
                continue;
            }

            Contact c = mapped.get(emp);

            if (c != null) {
                if (updatedContactIds.containsKey(c.id)) {
                    LOGGER.error('Contact '+c.id+' would be updated due to employee records '+emp.id+' as well as  '+updatedContactIds.get(c.id));
                    continue;
                } else {
                    LOGGER.debug('Updating existing record for employee record ID '+emp.Id);
                    updatedContactIds.put(c.id,emp.id);
                }
            } else {
               // Mappable, but doesn't exist. Create new contact
                LOGGER.debug('Creating new contact for employee record '+emp.Id);
                c = new Contact(RecordTypeId = rt.Id, AccountId = SFDCACCOUNT.Id);
            }

            c = INTEG_EmployeeToContactMapper.copyEmployeeFieldsToContact(emp, c);
            c.Last_Import_Date__c = Date.today();
            updated.add(c);
        }

        if (updated.size() > 0) {
            String msg = String.format('Updating {0} Employee Contact records due to updated Employee__c records', new List<string>{String.valueOf(updated.size())});
            LOGGER.info(msg);
            checkReportErrors(Database.upsert(updated, False), updated);
        } else {
            LOGGER.info('No Employee Contact records to update due to Employee__c changes');
        }
        return updated.size();
    }

    public void checkReportErrors(Database.UpsertResult[] res, Contact[] attempted) {
        Map<String, String> empIdAndErrors = new Map<String, String>();
        Integer offset = 0;
        for (Database.UpsertResult r : res) {
            if (!r.isSuccess()) {
                empIdAndErrors.put(attempted.get(offset).Org62_User_ID__c, String.join(r.getErrors(),'\n'));
            }
            offset += 1;
        }

        if (empIdAndErrors.size() == 0) {
            return;
        }

        Datetime lastErrorTime = Datetime.now();

        INTEG_Employee__c[] emps = [SELECT Id, INTEG_Org62UserId__c FROM INTEG_Employee__c WHERE INTEG_Org62UserId__c IN: empIdAndErrors.keySet() LIMIT 9999];
        for (INTEG_Employee__c e : emps) {
            e.INTEG_Last_Data_Error_Time__c = lastErrorTime;
            e.INTEG_Last_Data_Error__c = empIdAndErrors.get(e.INTEG_Org62UserId__c);
        }
        update emps;


    }


}
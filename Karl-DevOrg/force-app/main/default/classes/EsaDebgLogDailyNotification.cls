/**
 * Daily Notification Email for List of people about ESA_DebugLog Entries
 * @author suresh.uppala
 */
global without sharing class EsaDebgLogDailyNotification implements Schedulable,
                                                              Database.Batchable<sObject>,
                                                              Database.AllowsCallouts,
                                                              Database.Stateful {

    private static final String DAILY = '0 30 5 * * ? *'; //Daily Once @5:30AM
    private static final String SOURCE_FILE = 'EsaDebugLogDailyNotification';
                                                                 
    public EsaDebgLogDailyNotification() {

    }

    public static String scheduleJob() {
        return scheduleJob(DAILY);
    }

    public static Id scheduleJob(String cronExpression) {
        return scheduleJob(cronExpression, null);
    }

    public static Id scheduleJob(String cronExpression, String eventName) {
        String jobName = SOURCE_FILE + ' ' + System.now();
        String jobId = System.schedule(jobName, cronExpression, new EsaDebgLogDailyNotification());
        return jobId;
    }

    global void execute(SchedulableContext sc) {
        Database.executeBatch(this);
    }

    global List<sObject> start(Database.BatchableContext context){
        User[] users = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
        return users;
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        try {
                
                List<ESA_Debug_Log__c> lstDebugLogs = new List<ESA_Debug_Log__c>();
                lstDebugLogs = [SELECT Id,Name,Exception_Details__c,Level__c,
                                Message__c,Source_File__c,StackTrace__c,TypeName__c,User__r.Name 
                                FROM ESA_Debug_Log__c 
                                WHERE CreatedDate=YESTERDAY
                                AND Level__c = 'Error'
                                ];
                Integer numberOfErrors = lstDebugLogs.size();

                List<String> toAddresses = new List<String>();
                for(DebugLogNotificationEmailIds__c emailIds:DebugLogNotificationEmailIds__c.getAll().values()) {
                    toAddresses.add(emailIds.EmailId__c);
                }
                String body;
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                mail.setToAddresses(toAddresses);
                mail.setReplyTo('noreply@salesforce.com');
                if(lstDebugLogs.size() == 0) {
                    return;
                    mail.setSubject('No Errors for the day.');  
                    body = 'No Errors for Today.\n\n';
                }else {
                    mail.setSubject('Esa_DebugLog Daily Notification - Number of Errors - '+lstDebugLogs.size());
                    body = 'Error Details for the day.\n\n';
                    for(ESA_Debug_Log__c esaDebug : lstDebugLogs){
                        body += 'Debug Id          :' + esaDebug.Name + '\n';
                        body += 'Level             :' + esaDebug.Level__c + '\n';
                        body += 'Error Type        :' + esaDebug.TypeName__c +'\n';
                        body += 'Exception Details :' + esaDebug.Exception_Details__c+'\n';
                        body += 'Stack Trace       :' + esaDebug.StackTrace__c+'\n';
                        body += 'Message           :' + esaDebug.Message__c+'\n';
                        body += 'Source File       :' + esaDebug.Source_File__c+'\n';
                        body += 'User              :' + esaDebug.User__r.Name+'\n\n';
                        body += '---------------------------\n\n';
                    }
                }
                
                mail.setPlainTextBody(body);
                if(!Test.isRunningTest()) {
                    Messaging.sendEmail(new Messaging.SingleEmailMessage[]{mail});
                }
        
                
            }catch(Exception exc) {
            
        }
    }

    global void finish(Database.BatchableContext BC){
       
    }                                         
                                                              
}
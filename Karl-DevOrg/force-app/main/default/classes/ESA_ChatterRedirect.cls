/*

    Developer Name: Jorge L Caceres - jcaceres@salesforce.com | January 2018
    
    Description: Overrides the chatter junction object view page to automatically redirect user to the related
                 security request
                 
*/

public class ESA_ChatterRedirect {
            
    private Id chatterJunctionId;

    public ESA_ChatterRedirect(ApexPages.StandardController controller) {    
        chatterJunctionId = controller.getId(); 
    }

    public PageReference redirect(){

        // get related security request and reroute user to the request itself
        try {
            ESA_Security_Request__c request = [SELECT Hash_Key__c, Entity_Code2__c FROM ESA_Security_Request__c 
                                               WHERE Chatter_Junction__c = :chatterJunctionId];
            PageReference newPage = new PageReference('https://securityorg.force.com/IntakeApp/s/');
            newPage.getParameters().put('Id', request.Hash_Key__c);
            newPage.getParameters().put('entityCode', request.Entity_Code2__c);            
            newPage.setRedirect(true);
            return newPage;                
        } catch (Exception e) {
            return null;
        }        
    
    }
             
}
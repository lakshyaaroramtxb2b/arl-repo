public class CAPTriggerBridge {

    @future(callout=true)
    public static void AsyncNotifyCustomer(Id capBreachRec)
    {
        CAPBreachRecordExtension capBreachExt = new CAPBreachRecordExtension(capBreachRec);
        capBreachExt.NotifyCustomer();
    }
    
}
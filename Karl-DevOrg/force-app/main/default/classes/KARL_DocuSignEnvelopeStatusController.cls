/** 
 * @author Lakshya Arora
 * @email lakshya.arora@mtxb2b.com
 * @description This class will handle the apex operations performed in karl_audit_firm_Stakeholder
 * Used in karl_audit_firm_Stakeholder
 */
public with sharing class KARL_DocuSignEnvelopeStatusController {
    /**
    * @author Lakshya Arora
    * @email lakshya.arora@mtxb2b.com
    * @description Fetch the audit firm stakeholders related to cycle audit report
    * @param cycleAuditReportId - record Id for Cycle Audit Reports
    * @return List of Envelope Status
    */
   @AuraEnabled(cacheable=true)
   public static List<EnvelopeWrapper> getDocusignEnvelopeStatus(Id cycleAuditReportId){
       List<EnvelopeWrapper> envelopeWrapperList = new List<EnvelopeWrapper>();
       for(dfsle__EnvelopeStatus__c evenlopeStatusObj : [SELECT Id,dfsle__Sent__c,dfsle__Status__c,dfsle__SenderName__c,dfsle__SenderEmail__c,
                                                            dfsle__Completed__c 
                                                            FROM dfsle__EnvelopeStatus__c 
                                                            WHERE dfsle__SourceId__c =: cycleAuditReportId
                                                            WITH SECURITY_ENFORCED
                                                            Order By dfsle__Sent__c DESC]){
            EnvelopeWrapper envelopeObj = new EnvelopeWrapper();      
            envelopeObj.Id = evenlopeStatusObj.Id;
            envelopeObj.status = evenlopeStatusObj.dfsle__Status__c;
            envelopeObj.sentDateTime  = '';
            if(evenlopeStatusObj.dfsle__Sent__c != null){
                envelopeObj.sentDateTime = evenlopeStatusObj.dfsle__Sent__c.format();
            }
            envelopeObj.senderName = evenlopeStatusObj.dfsle__SenderName__c;
            envelopeObj.senderEmail = evenlopeStatusObj.dfsle__SenderEmail__c;
            envelopeObj.completedDateTime = '';
            if(evenlopeStatusObj.dfsle__Completed__c != null){
                envelopeObj.completedDateTime = evenlopeStatusObj.dfsle__Completed__c.format();
            }
            envelopeWrapperList.add(envelopeObj);                                           
       }
       return envelopeWrapperList;
    }

    public class EnvelopeWrapper{
        @AuraEnabled public String Id;
        @AuraEnabled public String status;
        @AuraEnabled public String sentDateTime;
        @AuraEnabled public String senderName;
        @AuraEnabled public String senderEmail;
        @AuraEnabled public String completedDateTime;
    }
}
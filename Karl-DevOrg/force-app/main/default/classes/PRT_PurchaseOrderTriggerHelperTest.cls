/***********************************************************
* Test class for PRT_PurchaseOrderTriggerHelper
* Created by    - Prashant Gupta
************************************************************/
@isTest
public class PRT_PurchaseOrderTriggerHelperTest {
    @testSetup
    public static void testSetup(){
        List<User> userList = new List<User>();
        userList = PRT_TestDataFactory.createUsers(3,'System Administrator',true);
        
        
    }
    static testMethod void createInternalUserSharingTest(){
        List<User> userList = new List<User>([SELECT Id FROM USER WHERE Email Like 'PRTUser%']);
        List<Program__c> programList = new List<Program__c>(PRT_TestDataFactory.createPrograms(2,true));
        List<Project__c> projectList = new List<Project__c>(PRT_TestDataFactory.createProjects(2,false));
        for(integer i=0; i<2; i++){
            projectList[i].Program__c = programList[i].id;
            projectList[i].Project_Manager__c = userList[0].id;
            projectList[i].Executive_Sponsor_Internal__c = userList[1].id;
            projectList[i].Sponsor_Internal__c = userList[2].id;
        }
        insert projectList;
        test.startTest();
        List<Purchase_Order__c> purchaseOrderList = new List<Purchase_Order__c>(PRT_TestDataFactory.createPurchaseOrder(5,projectList[0].Id,true));
        
        
        for(Purchase_Order__c po : purchaseOrderList ){
            po.project__c = projectList[1].Id;
        }
        update purchaseOrderList;
        Map<id,List<Purchase_Order__c>> projectVsPurchaseOrderMap = new Map<id,List<Purchase_Order__c>>(); 
        for(Purchase_Order__c po : purchaseOrderList){
            
            if(!projectVsPurchaseOrderMap.containsKey(po.Project__c)){
                projectVsPurchaseOrderMap.put(po.Project__c, new List<Purchase_Order__c>());
            }
            projectVsPurchaseOrderMap.get(po.Project__c).add(po);
            
        }
        
        

        List<Purchase_Order__Share> posList= new List<Purchase_Order__Share>([SELECT Id,AccessLevel FROM Purchase_Order__Share WHERE AccessLevel ='Read']);
        System.assertEquals(posList.size()+'','10');
       List<Purchase_Order__Share> purchaseOrderSharingToDelete = new List<Purchase_Order__Share>([SELECT Id 
                                                                                                    FROM Purchase_Order__Share 
                                                                                                    WHERE RowCause = 'Manual' ]);
        
        System.assertEquals(purchaseOrderSharingToDelete.size()+'','10');
        
        test.stopTest();
        
        
    }
}
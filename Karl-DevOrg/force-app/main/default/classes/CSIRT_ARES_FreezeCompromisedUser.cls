@RestResource(urlMapping='/ARES/FreezeUser/*')
global with sharing class CSIRT_ARES_FreezeCompromisedUser {
    global class FreezeResponse {
        public String userid;
        public String orgid;
        public String result;
        
        public FreezeResponse(String orgid, String userid){
            this.orgid = orgid;
            System.debug('OrgId: '+ orgid);
            
            this.userid = userid;
			System.debug('UserId: '+ userid);
        }
        
        public void Errored(){
            this.result = 'ERROR';
            System.debug('Cannot Freeze ' + userid);
        }
        
        public void Success(){
            this.result = 'SUCCESS';
            System.debug('UserId ' + userid + ' successfully frozen.');
        }
    }
    
	@HttpPost 
	global static FreezeResponse freezeUser(String customerOrgId, String usersId) {
		FreezeResponse response = new FreezeResponse(customerOrgId, usersId);
        
        try{
        	List<String> ipList = new List<String> {'127.0.0.1'};
        	ProductSecurity.freezeCompromisedUser(customerOrgId, usersId, True, ipList);
        }
        catch(Exception e){
        	response.Errored();
            return response;
        }
        
		response.Success();
        return response;
        
   }
}
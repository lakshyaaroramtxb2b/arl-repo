@isTest
public class PRT_PortfolioTriggerHelperTest {
    @testSetup
    public static void testSetup(){
        Profile p = [SELECT Id FROM Profile WHERE Name='Nothing' LIMIT 1];
        
        User user = [SELECT Id,Name FROM User WHERE ProfileId =:p.Id and isActive = true LIMIT 1];
        System.runAs(user){
            PRT_Integration_Settings__c integrationSetting = new PRT_Integration_Settings__c();
            integrationSetting.GUS_Portfolio_Integration__c = true;
            insert integrationSetting; 
        } 
        List<User> userList = new List<User>();
        userList = PRT_TestDataFactory.createUsers(3,'System Administrator',true);
        
    }
    @isTest
    public static void testUpdateInsertGUSRecords(){
        Boolean triggerValue = PRT_Constants.RECURSIVE_TRIGGER_CHECK;
        Boolean batch = !System.isBatch();
        
        Boolean futureCheck = !System.isFuture();
        List<User> userList = new List<User>([SELECT Id FROM USER WHERE Email Like 'PRTUser%']);
        List<Portfolio__c> portfolioDataList = new List<Portfolio__c>();
        Portfolio__c portfolio = new Portfolio__c();
        portfolio.Portfolio_Goals__c = 'test Goals';
        portfolio.Portfolio_Manager__c =userList[0].id;
        portfolio.Portfolio_Owner__c = userList[0].id;
        portfolioDataList.add(portfolio);
        if(!portfolioDataList.isEmpty()){
             insert portfolioDataList;  
        }
        portfolio.Portfolio_Goals__c = 'test Goal Updated';
        portfolio.Portfolio_Active__c = true;
        update portfolio;
        
    }
    @isTest
    static void testInsertPortfolio(){
        List<User> userList = new List<User>([SELECT Id FROM USER WHERE Email Like 'PRTUser%']);
        
        Boolean checkTrigger = PRT_Constants.INTEGRATION_SETTING.GUS_Portfolio_Integration__c;
        Boolean triggerValue = PRT_Constants.RECURSIVE_TRIGGER_CHECK;
        List<Portfolio__c> portfolioDataList = PRT_TestDataFactory.createPortfolio(1,false);
        portfolioDataList[0].Portfolio_Active__c =true;
        portfolioDataList[0].Portfolio_Goals__c = 'test Goals';
        portfolioDataList[0].Portfolio_Manager__c =userList[0].id;
        portfolioDataList[0].Portfolio_Owner__c = userList[0].id;
        portfolioDataList[0].Portfolio_Health__c = 'On Track';
        portfolioDataList[0].Portfolio_Summary__c = 'test Summary';
        portfolioDataList[0].GUS_Portfolio__c = '';
        test.startTest();
        insert portfolioDataList;
        test.stopTest();
        
    }

}
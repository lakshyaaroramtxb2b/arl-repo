//Populate lookup fields on guest custom objects
global with sharing class GuestLookupUpdate implements Database.Batchable<sObject>, Database.Stateful{
    public String query;
     
    global Database.QueryLocator start(Database.BatchableContext bc){
        //return Database.getQueryLocator('select id,organization_id__c from guests_public_groups__c');
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext bc, List<SObject> scope){
        Set<String>orgset = new Set<String>();
        for(SObject so : scope){
            orgset.add((String)so.get('organization_id__c'));
        }
        Map<String, String> orgtoidmap = new Map<String,String>();
        //retrieve IDs to populate lookup fields
        for(guests_all_orgs__c recid :[select id,organization_id__c from guests_all_orgs__c where organization_id__c IN :orgset]){
            orgtoidmap.put((String)recid.organization_id__c, (String)recid.id);
        }
        List<SObject> toupdatelist = new List<SObject>();
        for(SObject so : scope){
            so.put('guests_all_orgs__c', orgtoidmap.get((String)so.get('organization_id__c')));
            toupdatelist.add(so);
        }
        system.debug(toupdatelist.size());
        List<Database.SaveResult> result = Database.update(toupdatelist, false);
        system.debug(result);
    }
    
    global void finish(Database.BatchableContext bc){}
}
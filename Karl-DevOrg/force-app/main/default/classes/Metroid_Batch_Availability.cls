global class Metroid_Batch_Availability implements Database.Batchable<String> {
	
	global Iterable<String> start(Database.BatchableContext BC) {
		return new List<String> { 'New Vendor Request', 'Network Architecture Review'};
	}

   	global void execute(Database.BatchableContext info, List<String> strings) {
		Metroid_Armory MA = new Metroid_Armory();
		 MA.Scan_OH_Availability(strings[0]);
	}
	
	global void finish(Database.BatchableContext info) {
		
	}
	
}
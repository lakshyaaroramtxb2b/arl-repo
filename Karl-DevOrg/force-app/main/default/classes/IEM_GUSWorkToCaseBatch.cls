/***************IEM_GUSWorkToCaseBatch****************
@Author Banshi
@Date 08/25/2019
@Modified by/ date - Swarnima singh Mandhata - T-09359 - 03/31/2020
@Description Batch class to create fetch GUS work records on regular basis on certain creteria and then create crossponding case records.
**********************************************/

public class IEM_GUSWorkToCaseBatch implements Database.Batchable<sObject>,Database.Stateful,Schedulable {
    @TestVisible
    private static list<ADM_Theme_c__x> mockallThemeList = new list<ADM_Theme_c__x>();//For test class
    @TestVisible
    private static list<ADM_Theme_Assignment_c__x> mockallThemeAssignmentList = new list<ADM_Theme_Assignment_c__x>();//For test class
    public String primaryThemeId; //GRCOPS-Primary theme Id
    
    public Database.QueryLocator start(Database.BatchableContext BC) {
        String intakeName = '%'+IEM_Constants.INTAKE_THEME_NAME_PREFIX+'%';
        List<String> intakeThemeIdSet = new List<String>();
        List<ADM_Theme_c__x> themeList = new List<ADM_Theme_c__x>();
        if(Test.isRunningTest()){
            themeList = mockallThemeList;
        }else{
            //Fetch themes
            themeList = [SELECT Id,ExternalId,Name__c FROM ADM_Theme_c__x WHERE Name__c LIKE :intakeName OR Name__c = :IEM_Constants.PRIMARY_THEME_NAME];
        }
        for(ADM_Theme_c__x theme : themeList){
            if(theme.Name__c == IEM_Constants.PRIMARY_THEME_NAME){
                primaryThemeId = theme.ExternalId;
            }
            else{
                intakeThemeIdSet.add(theme.ExternalId);
            }
        }
        String query= '';
        if(Test.isRunningTest()){
            query = 'SELECT Id FROM User LIMIT 1';
        }
        else{
            query = 'SELECT Id, Work_c__c FROM ADM_Theme_Assignment_c__x WHERE Theme_c__c  IN : intakeThemeIdSet AND Work_c__c!= null';
        }   
         return Database.getQueryLocator(query);    
    }
    
    public void execute(Database.BatchableContext BC, List<sObject> listOfAssignments) {
        List<String> workIds = new List<String>();
        List<ADM_Theme_Assignment_c__x> themeAssignmentList = new List<ADM_Theme_Assignment_c__x>();
        
        if(Test.isRunningTest()){
            themeAssignmentList =  mockallThemeAssignmentList;
        }else{
            for(ADM_Theme_Assignment_c__x themeAssignment :(List<ADM_Theme_Assignment_c__x>)listOfAssignments){
                workIds.add(themeAssignment.Work_c__c);
                themeAssignment.Theme_c__c = primaryThemeId;
                themeAssignmentList.add(themeAssignment);
            }
        }
        //Enqueue queueable job to create case and case work records and then update the theme assignment back to primary.
        System.enqueueJob(new IEM_GUSWorkToCaseQueuable(workIds, themeAssignmentList));
    }
    
    public void finish(Database.BatchableContext BC) {
        // finish code
        List<CronTrigger> cronList = new List<CronTrigger>([SELECT Id,CronJobDetail.Name,CronJobDetail.Id,State FROM CronTrigger where CronJobDetail.Name ='IEM_GUSWorkToCaseBatch'AND State !='COMPLETE']);
        if(cronList.isEmpty() || cronList==null){
                IEM_GUSWorkToCaseBatch batch = new IEM_GUSWorkToCaseBatch();
                System.scheduleBatch(batch, 'IEM_GUSWorkToCaseBatch',5);
            }
            else{
                System.debug('Batch Scheduling skipped, \'IEM_GUSWorkToCaseBatch\' Batch already scheduled');
            }
        
    }
    public void execute(SchedulableContext SC) {
        database.executebatch(new IEM_GUSWorkToCaseBatch());
    }
    
}
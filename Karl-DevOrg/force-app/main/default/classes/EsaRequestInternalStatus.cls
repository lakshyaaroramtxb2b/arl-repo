public class EsaRequestInternalStatus {
   
   private ESA_Security_Request__c esaRequest {get; set;}
   private EsaService esaSvc {get; set;}
   private Map<String, List<Object>> statusOptionsMap {get; set;}
   public String seletctedStatus {get; set;}
   
   public String errorMessage {get; set;}
   public Boolean displayFlag {get; set;}
   public String buttonMessage {get; set;}
   

   public EsaRequestInternalStatus(ApexPages.StandardController stdController) {
      esaSvc = new EsaService();
      esaRequest = (ESA_Security_Request__c)stdController.getRecord();
      esaRequest = [SELECT Esa_Entity__C,
                           Entity_Code__c,
                           Status__c,
                           Supportforce_Case_Number__c, 
                           Internal_Status__c,
                           ESA_Service_Item__r.Approval_Required__c
                    FROM ESA_Security_Request__c
                    WHERE Id=:esaRequest.Id];

      if(esaRequest.ESA_Service_Item__r.Approval_Required__c == true) {
         displayFlag = false;
         errorMessage = Label.Esa_Internal_Status_Exception_Approval;
         buttonMessage = 'Go Back';
      }else if(esaRequest.Supportforce_Case_Number__c != null) {
         displayFlag = false;
         errorMessage = Label.Esa_Internal_Status_Exception;
         buttonMessage = 'Go Back';
      } else {
         displayFlag = true;
         buttonMessage = 'Change Status';
      }

      seletctedStatus = esaRequest.Internal_Status__c;
   }
   
   public List<SelectOption> getInternalStatusValues() {
      Esa_Entity__c esaEntity = esaSvc.getEsaEntityInfo(esaRequest.Entity_Code__c); 
      statusOptionsMap = esaSvc.getInternalStatusOptions(esaEntity);
      List<SelectOption> options = new List<SelectOption>();
      options.add(new SelectOption('', ''));
       
      for(String st : (List<String>) statusOptionsMap.get(ESA_AppConstants.IN_PROGRESS_STATUS_KEY)) {
         options.add(new SelectOption(st, st));
      } 
      for(String st : (List<String>) statusOptionsMap.get(ESA_AppConstants.CLOSED_STATUS_KEY)) {
         options.add(new SelectOption(st, st));
      } 
          
      return options;
   }

   public void changeStatus() {
      esaRequest.Internal_Status__c = seletctedStatus;
   }

   public PageReference saveStatus() {
      
      PageReference p = new PageReference('/lightning/r/ESA_Security_Request__c/'+esaRequest.Id+'/view');
      
      if(displayFlag == false) return p;

      if(seletctedStatus == null) return null;

      if(esaRequest.Status__c == 'Pending') {
         ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL, 'InTake Request is in Pending status, Not possible to change the internal status ');
         ApexPages.addMessage(myMsg);
         return null;
      }

      esaRequest.Internal_Status__c = seletctedStatus;

      if(statusOptionsMap.get(ESA_AppConstants.CLOSED_STATUS_KEY).contains(seletctedStatus)) {
         esaRequest.Status__c = 'Closed';
         esaRequest.Case_Work_Closed_Date__c = Datetime.Now();
      }
      
      if(statusOptionsMap.get(ESA_AppConstants.IN_PROGRESS_STATUS_KEY).contains(seletctedStatus)) {
         esaRequest.Status__c = 'In Progress';
      }

      Database.SaveResult sr = Database.update(esaRequest);

      return p;
   }
}
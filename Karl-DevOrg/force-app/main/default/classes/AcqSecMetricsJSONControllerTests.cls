@isTest
public class AcqSecMetricsJSONControllerTests {
    public static testMethod void testGetJSON() {
        AcqSec_Metrics_JSON__c json = new AcqSec_Metrics_JSON__c (
	        name = 'test', data__c = 'testx'
        );
		insert json;
        String s1 = AcqSecMetricsJSONController.getJSON('test');
        System.assertEquals(s1,'testx');
        String s2 = AcqSecMetricsJSONController.getJSON('NF');
		System.assertEquals(s2, NULL);
    }
    public static testMethod void testGetJSONForAllAcquisitionsAsArray() {
        AcqSec_Metrics_JSON__c json = new AcqSec_Metrics_JSON__c (
	        name = 'test2', data__c = 'testx2', acquisition__c='testy2'
        );
		insert json;
        String s = AcqSecMetricsJSONController.getJSONForAllAcquisitionsAsArray('test2');
        String s2 = AcqSecMetricsJSONController.getJSONForAllAcquisitionsAsArray('NF');
    }
    public static testMethod void testGetJSONForAcquisition() {
        AcqSec_Metrics_JSON__c json = new AcqSec_Metrics_JSON__c (
	        name = 'test2', data__c = 'testx2', acquisition__c='testy2'
        );
		insert json;
        String s = AcqSecMetricsJSONController.getJSONForAcquisition('test2','testy2');
        String s2 = AcqSecMetricsJSONController.getJSONForAcquisition('testNF','testy2');
        String s3 = AcqSecMetricsJSONController.getJSONForAcquisition('test2','testyNF');
    }

}
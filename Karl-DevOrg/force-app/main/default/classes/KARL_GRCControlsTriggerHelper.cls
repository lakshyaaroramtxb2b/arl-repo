/**
* @author Ben Harvie
* @email ben.harvie@salesforce.com
* @description This is the trigger handler class for KARL_GRC_Controls__c
*/
public with sharing class KARL_GRCControlsTriggerHelper {
    public static Set<Id> controlId = new Set<Id>();

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description This method gets triggered from CCF Controls Object
    */
    public static void run(){
        // Before Update
        if( Trigger.isBefore && Trigger.isUpdate ){
            populateName();
        }
        // Before Insert
        else if( Trigger.isBefore && Trigger.isInsert ){
            populateName();
        }
        // After Update
        else if( Trigger.isAfter && Trigger.isUpdate ){
            updateRelated();
        }
    }
 
    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Populate the Name field with appropriate information
    */
    public static void populateName(){
        System.debug(LoggingLevel.DEBUG, '--- START Updating CCF Control Name ---');

        // Update the name of the records
        Map<Id, KARL_GRC_Controls__c> oldMap = (Map<Id, KARL_GRC_Controls__c>)Trigger.oldMap;
        for(KARL_GRC_Controls__c ccfControl : (List<KARL_GRC_Controls__c>)Trigger.New ){
            //KARL_GRC_Controls__c oldControl = Trigger.isUpdate ? oldMap.get(ccfControl.Id) : null;
			
            String newName = ccfControl.Object_Name__c + ' - ' + ccfControl.Short_Description__c;
            Integer maxLength = 80;
            if(newName.length() > maxLength){
                newName = newName.substring(0, maxLength);
            }
            
            if( Trigger.isInsert ||
              ( Trigger.isUpdate && (ccfControl.Name <> newName) ) ){
                ccfControl.Name = newName;
            }
        }
        System.debug(LoggingLevel.DEBUG, '--- END Updating CCF Control Name ---');
    }
 
    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Update the related Controls and Control Scopes when a change is made
    */
    public static void updateRelated(){
        System.debug(LoggingLevel.DEBUG, '--- START Updating Related Controls and Control Scopes ---');
        //System.debug(LoggingLevel.DEBUG, 'Control >> ' + (List<KARL_GRC_Controls__c>)Trigger.New);
        for(Id recursionCheck : Trigger.newMap.keySet()){
            if(controlId.contains(recursionCheck)){
                System.debug(LoggingLevel.DEBUG, 'Already processed this record >> ' + recursionCheck);
                return; // Kill the run - we've already processed this Id
            }else{
                System.debug(LoggingLevel.DEBUG, 'New record to process >> ' + recursionCheck);
                controlId.add(recursionCheck); // add Id to set so we don't run it again
            }
        }

        // Get Related Controls
        Map<Id, List<Control__c>> controlItems = new Map <Id, List<Control__c>>();
        for(Control__c cItem : [SELECT Id,Control_Description__c,Name,CCF_Control__c,KARL_CCF_Alignment__c FROM Control__c 
                                WHERE CCF_Control__c IN: (List<KARL_GRC_Controls__c>)Trigger.New
                                WITH SECURITY_ENFORCED]){
            List<Control__c> cItems = new List<Control__c>();
            if( controlItems.containsKey(cItem.CCF_Control__c) ){
                cItems = controlItems.get(cItem.CCF_Control__c);
            }
            cItems.add(cItem);
            
            controlItems.put(cItem.CCF_Control__c, cItems);              
        }
        
        // Get Related Control Scopes
        Map<Id, List<Control_Scope__c>> controlScopeItems = new Map <Id, List<Control_Scope__c>>();
        for(Control_Scope__c csItem : [SELECT Id,Modified_Description__c,Modified_Name__c,CCF_Control__c,KARL_CS_CCF_Alignment__c FROM Control_Scope__c 
                                WHERE CCF_Control__c IN: (List<KARL_GRC_Controls__c>)Trigger.New
                                WITH SECURITY_ENFORCED]){
            List<Control_Scope__c> csItems = new List<Control_Scope__c>();
            System.debug(LoggingLevel.DEBUG, 'Control Scope Item >> ' + csItem.Id);
            if( controlScopeItems.containsKey(csItem.CCF_Control__c) ){
                csItems = controlScopeItems.get(csItem.CCF_Control__c);
            }
            csItems.add(csItem);
            
            controlScopeItems.put(csItem.CCF_Control__c, csItems);              
        }
                                               
        System.debug('Related Controls >> ' + controlItems);
        System.debug('Related Control Scopes >> ' + controlScopeItems);

        // Lists for Update Tracking
        List<Control__c > controlsToUpdate = new List<Control__c>();
        List<Control_Scope__c > controlScopesToUpdate = new List<Control_Scope__c>();

        // Process Triggered CCF Updates
        for(KARL_GRC_Controls__c ccfControl : (List<KARL_GRC_Controls__c>)Trigger.New ){

            // Process each Control
            if(!controlItems.isEmpty()){
                if(controlItems.containskey(ccfControl.Id)){
                    for(Control__c controlItem : controlItems.get(ccfControl.Id)){
                        if(controlItem.KARL_CCF_Alignment__c 
                        && (controlItem.Control_Description__c <> ccfControl.Description__c || controlItem.Name <> ccfControl.Short_Description__c)){
                            System.debug('Variance for Control__c >> ' + controlItem.Id);
                            controlItem.Control_Description__c  = ccfControl.Description__c;
                            controlItem.Name                    = ccfControl.Short_Description__c;
                            controlsToUpdate.add(controlItem);
                        }
                    }
                }
            }

            // Process each Control Scope
            if(!controlScopeItems.isEmpty()){
                System.debug(LoggingLevel.DEBUG, 'Processing >> ' + ccfControl.Id);
                if(controlScopeItems.containskey(ccfControl.Id)){
                    for(Control_Scope__c controlScopeItem : controlScopeItems.get(ccfControl.Id)){
                        if(controlScopeItem.KARL_CS_CCF_Alignment__c 
                        && (controlScopeItem.Modified_Description__c <> ccfControl.Description__c || controlScopeItem.Modified_Name__c <> ccfControl.Short_Description__c)){
                            System.debug('Variance for Control_Scope__c >> ' + controlScopeItem.Id);
                            controlScopeItem.Modified_Description__c  = ccfControl.Description__c;
                            controlScopeItem.Modified_Name__c         = ccfControl.Short_Description__c;
                            controlScopesToUpdate.add(controlScopeItem);
                        }
                    }
                }
            }
        }

        // Update Controls
        if(!controlsToUpdate.isEmpty() && Schema.sObjectType.Control__c.isUpdateable() 
        && Control__c.Control_Description__c.getDescribe().isUpdateable()
        && Control__c.Name.getDescribe().isUpdateable()){ 
            update controlsToUpdate;
        }

        // Update Control Scopes
        if(!controlScopesToUpdate.isEmpty() && Schema.sObjectType.Control_Scope__c.isUpdateable() 
        && Control_Scope__c.Modified_Description__c.getDescribe().isUpdateable() 
        && Control_Scope__c.Modified_Name__c.getDescribe().isUpdateable()){ 
            update controlScopesToUpdate;
        }

        System.debug(LoggingLevel.DEBUG, '--- END Updating Related Controls and Control Scopes ---');
    }
}
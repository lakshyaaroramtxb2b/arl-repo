/*
 * Apex Schedule class used to sync Modules and Trails
 * This process will Start with SC_TrailSyncBatch move to SC_ModuleSyncBatch
 * and finish with SC_RelationshipSyncBatch
 */

global class SC_ModuleTrailSync implements Schedulable {

    global void execute(SchedulableContext sc) {

        Database.executeBatch(new SC_TrailSyncBatch(0));
    }
}
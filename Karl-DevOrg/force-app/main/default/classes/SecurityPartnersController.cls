public with sharing class SecurityPartnersController {
    private static final List<pattern> REDIRECT_PATTERNS;
    private static final String REDIRECT_MESSAGE;
    
    static {
        REDIRECT_PATTERNS = new pattern[]{ 
                    pattern.compile('^https?://(www\\.)?appexchange.com/securityreview'),
                    pattern.compile('^https?://appxsecurityreview\\.corp\\.salesforce\\.com'),
                    pattern.compile('^https?://(www\\.)?bigskytheory.com/static/re')
                  };
        REDIRECT_MESSAGE = 'Looking for the Agiliance survey tool? We have simplified our process to help partners provide the right information for the review more quickly.  Please log a case in the <a href="http://www.salesforce.com/partners/login/">partner portal</a> &lt;http://www.salesforce.com/partners/login/&gt;  to initiate your security review.';
    }
    
    public String getRedirectMessage() {
        String referrer = ApexPages.currentPage().getHeaders().get('Referer');
        if (ApexPages.currentPage().getParameters().get('redir') != null) {
            return REDIRECT_MESSAGE;
        }
        
        if ((referrer == null) || (referrer == '')) {
            return null;
        }
        for (Pattern p : REDIRECT_PATTERNS) {
            if (p.matcher(referrer).matches()) {
                return REDIRECT_MESSAGE;
            }
        }
        
        return null;
    }
}
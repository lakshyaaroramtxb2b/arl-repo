/**
 * @author Mahima Aggarwal
 * @email mahima.aggarwal@mtxb2b.com
 * @desc Test class for KARL_ShareAuditorReqItemToAuditFirmBatch
*/
@isTest
public class KARL_ShareAudReqItemToAuditFirmBatchTest {
	/**
     * @author Mahima Aggarwal
     * @email mahima.aggarwal@mtxb2b.com
     * @desc Test data setup.
    */
    @testSetup
    public static void testSetup(){
        UserRole userrole = [Select Id, DeveloperName From UserRole Where DeveloperName = 'CEO' Limit 1];

        User adminUser = [Select Id, UserRoleId From User Where Profile.Name='System Administrator' AND isActive = true Limit 1];
        
        adminUser.UserRoleId = userRole.Id;
        update adminUser;
        
        System.runAs(adminUser){
            Account acc = new Account(Name = 'salesforce.com - ESA Office Hours');
            insert acc;
            
            Contact con = new Contact(LastName = 'test',AccountId = acc.Id, Email = 'test@gmail.com');
            insert con;
            
            User communityUser = ARL_TestDataFactory.createCommunityUserwithSpecificCon(con.Id, 'a12345@bqww.com',true);
        }
    }
        
    /**
     * @author Mahima Aggarwal
     * @email mahima.aggarwal@mtxb2b.com
     * @desc Testing insert and update of Audit Team contacts
    */
    @istest
    public static void unitTest(){ 
        User communityUser = [Select id from User where FirstName = 'Community'];
        
        //Permission Set Assignment
        PermissionSet pSet = [SELECT Name FROM PermissionSet WHERE NAME = 'External_Auditor'];
        insert new PermissionSetAssignment(AssigneeId = communityUser.id, PermissionSetId = pSet.Id);
        
        Contact con = [SELECT Id FROM Contact LIMIT 1];
        System.runAs(communityUser){
            Request_Item__c reqItem = ARL_TestDataFactory.createRequestItem();
            reqItem.Primary_Scope__c = 'SS';
            insert reqItem;
            
            Cycle_Request_Item__c cycleReqItem = ARL_TestDataFactory.createCycleRequestItem(reqItem.id,null);
            cycleReqItem.Cycle_Request_Status__c = 'New';
            cycleReqItem.Request_Tech_Details__c = 'test';
            insert cycleReqItem;
            
            KARL_Audit_Firm__c auditFirm = ARL_TestDataFactory.createAuditFirm('testAuditFirm');
            insert auditFirm;
            
            KARL_Audit_Team__c auditTeam1 = ARL_TestDataFactory.createAuditTeam('test');
            auditTeam1.KARL_Audit_Firm__c = auditFirm.Id;
            insert auditTeam1;
            
            KARL_Audit_Team__c auditTeam2 = ARL_TestDataFactory.createAuditTeam('test');
            auditTeam2.KARL_Audit_Firm__c = auditFirm.Id;
            insert auditTeam2;
            
            KARL_Audit_Team_Contacts__c auditTeamCon1 = ARL_TestDataFactory.createAuditTeamContact(auditTeam1.Id,con.Id);
            
            KARL_Audit_Team_Contacts__c auditTeamCon2 = ARL_TestDataFactory.createAuditTeamContact(auditTeam2.Id,con.Id);
            
            KARL_Auditor_Request_Item__c auditorReqItem = ARL_TestDataFactory.createAuditorRequestItem(cycleReqItem.Id,auditTeam1.Id);
            insert auditorReqItem;
            
            Datetime yesterday = Datetime.now().addDays(-1);
        	Test.setCreatedDate(auditorReqItem.Id, yesterday);
        
            Map<Id,Set<Id>> auditFirmIdToContactIdMap = new Map<Id,Set<Id>>();
            auditFirmIdToContactIdMap.put(auditFirm.Id,new Set<Id>{auditTeamCon1.Id,auditTeamCon2.Id});
        
            Test.startTest();
            	KARL_ShareAuditorReqItemToAuditFirmBatch objBatchAdd = new KARL_ShareAuditorReqItemToAuditFirmBatch('Trigger',auditFirmIdToContactIdMap.keySet(),auditFirmIdToContactIdMap,'Add');
            	Database.executebatch(objBatchAdd,100);
            	KARL_ShareAuditorReqItemToAuditFirmBatch objBatchRemove = new KARL_ShareAuditorReqItemToAuditFirmBatch('Trigger',auditFirmIdToContactIdMap.keySet(),auditFirmIdToContactIdMap,'Remove');
            	Database.executebatch(objBatchRemove,100);
            
            	KARL_ShareAuditorReqItemToAuditFirmBatch objBatch1 = new KARL_ShareAuditorReqItemToAuditFirmBatch(0,true);
            	Database.executebatch(objBatch1,100);
            
            	KARL_ExecuteAudReqItemShareBatch.executeAuditorReqItemShareBatch(1);
            
            	String chron = '0 0 23 * * ?';        
        		String jobid = System.schedule('testScheduledApex', chron, new KARL_ShareAuditorReqItemToAuditFirmBatch());
            Test.stopTest(); 
        }
    }
}
/** 
 * @author Shraddha Patil
 * @email shraddhapatil@salesforce.com
 * @description This class provides method for generating RCM from Control  Scopes
 * Lightning Web Component resources.
 */
public with sharing  class KARL_RcmReportController {
    public static Set<Id> filteredControls = new Set<Id>();
    public static Map<String, List<String>> controlUniqueCcf        = new Map<String, List<String>>();
    public static Map<String, List<String>> controlUniqueOwners     = new Map<String, List<String>>();
    public static Map<String, List<String>> controlUniquePerformers = new Map<String, List<String>>();
    public static Map<String, List<String>> controlUniqueExecs      = new Map<String, List<String>>();
    public static Map<String, List<String>> controlUniqueSubcert    = new Map<String, List<String>>();
    
    /**
     * @author Shraddha Patil   
     * @email shraddhapatil@salesforce.com
     * @description Reusable method for generating the list of filteredRequests
     * @param auditScopeId - picklist value for audit scope
     */
    @AuraEnabled(cacheable=true)
    public static void generateData(String auditScopeId, String controlFamily, String [] activeStatus ){
        // Create filter strings 
        String scopeFilter       = '';
        String activeFilter      = '';
        String familyFilter      = '';

        // Create filter for Audit Scopes
        if(auditScopeId != 'allScopes' && auditScopeId != 'noScopes'){
            scopeFilter     = 'Scope_Name__c =: auditScopeId ';
        }else{
            scopeFilter     = 'Scope_Name__c !=: auditScopeId ';
        }  

        // Create filter for active status (activeReq or inactiveReq)
        Set<Boolean> statusList = new Set<Boolean>();
        for(String activeState : activeStatus){
            if(activeState == 'activeReq'){
                statusList.add(false); // reverse logic because we track inactive not active
            }else{
                statusList.add(true); // reverse logic because we track inactive not active
            }
        }
        if(statusList.size() == 1){
            activeFilter     = 'AND KARL_Inactive__c =: statusList ';
        }

        // Create filter for control family
        if(controlFamily != 'allFamilies' && controlFamily != 'noFamilies'){
            familyFilter      = 'AND Control_Name__r.Family_Name__c =: controlFamily ';
        }

        // Generate dataset for Control Ownership Details
        Map<Id, List<KARL_GRC_Controls__c> > ccfControl = new Map<Id, List<KARL_GRC_Controls__c> >();
        for( KARL_GRC_Controls__c ccfCtrl : Database.query('SELECT Id, Object_Name__c, Object_Level__c, ' +
                                                                    'Control_Owner__c, Control_Performer__c, ' +
                                                                    'Control_Executive__c, Control_Sub_Certifier__c ' +
                                                                    'FROM KARL_GRC_Controls__c ' +
                                                                    'WITH SECURITY_ENFORCED') ){
            if( !ccfControl.containsKey( ccfCtrl.Id )){
                ccfControl.put( ccfCtrl.Id , new List<KARL_GRC_Controls__c>());
            }
            ccfControl.get(ccfCtrl.Id).add(ccfCtrl);
        }
        System.debug(LoggingLevel.DEBUG, ccfControl);

        /* Step 1:  Populate Parent Control Scopes
                    We need to do this first due to order of processing; when we're going through the individual controls
                    as a part of step 2 we might come upon a control where the parent control hasn't been added to the map
                    yet, so we'd get a null reference error.
        */
        Boolean parentControl = true;
        for( Control_Scope__c rcmData : Database.query('SELECT Id, Parent_Control_Scope__c, Grouping_Control_Scope__c ' +
                                                                'FROM Control_Scope__c '+
                                                                'WHERE Grouping_Control_Scope__c =: parentControl AND '+ scopeFilter + activeFilter + familyFilter + 
                                                                'WITH SECURITY_ENFORCED')){
            if(!filteredControls.contains(rcmData.Id)){
                filteredControls.add(rcmData.Id); // Add control to dataset we care about (used for final query)
                // Create Map of List for each type -- important initialization step
                controlUniqueCcf.put(rcmData.Id, new List<String>());
                controlUniqueOwners.put(rcmData.Id, new List<String>());
                controlUniquePerformers.put(rcmData.Id, new List<String>());
                controlUniqueExecs.put(rcmData.Id, new List<String>());
                controlUniqueSubcert.put(rcmData.Id, new List<String>());
            }
        }
        System.debug(LoggingLevel.DEBUG, 'Parent Controls >> ' + filteredControls);

        /* Step 2:  Populate the rest of the data
                    Now that we have a data set of parents (step 1), we can populate all of the individual or child controls
                    into the Maps we define at the start of the class.
        */
        for( Control_Scope__c rcmData : Database.query('SELECT Id, Scope_Name__c, Triggered_Control_Identifier__c, ' +
                                                                'KARL_Triggered_Control_Name__c, Modified_Description__c, '+
                                                                'Parent_Control_Scope__c, Grouping_Control_Scope__c, CCF_Control__c,  '+
                                                                'CCF_Control__r.Object_Name__c, CCF_Control__r.Control_Performer__c, '+
                                                                'CCF_Control__r.Control_Owner__c, CCF_Control__r.Control_Executive__c, '+
                                                                'CCF_Control__r.Control_Family__c, CCF_Control__r.Control_Sub_Certifier__c '+
                                                                'FROM Control_Scope__c '+
                                                                'WHERE '+ scopeFilter + activeFilter + familyFilter + 
                                                                'WITH SECURITY_ENFORCED')){
            if(!filteredControls.contains(rcmData.Id)){
                filteredControls.add(rcmData.Id); // Add control to dataset we care about (used for final query)
                // Create Map of List for each type -- important initialization step
                // Note this may appear duplicative from the above step, but this is for all the individual or children
                controlUniqueCcf.put(rcmData.Id, new List<String>());
                controlUniqueOwners.put(rcmData.Id, new List<String>());
                controlUniquePerformers.put(rcmData.Id, new List<String>());
                controlUniqueExecs.put(rcmData.Id, new List<String>());
                controlUniqueSubcert.put(rcmData.Id, new List<String>());
            }

            // Step 2.1 - Check if CCF mapping exists (if not, do nothing)
            if(!String.isBlank(rcmData.CCF_Control__c)){
                // Step 2.2 - Manage individuals and children, populating the Map of Lists initialized above
                if(String.isBlank(rcmData.Parent_Control_Scope__c)){
                    // If not part of a group, add ownership directly to the current control (should be a singular item)
                    controlUniqueCcf.get(rcmData.Id).add(rcmData.CCF_Control__r.Object_Name__c);
                    controlUniqueOwners.get(rcmData.Id).add(rcmData.CCF_Control__r.Control_Owner__c);
                    controlUniquePerformers.get(rcmData.Id).add(rcmData.CCF_Control__r.Control_Performer__c);
                    controlUniqueExecs.get(rcmData.Id).add(rcmData.CCF_Control__r.Control_Executive__c);
                    controlUniqueSubcert.get(rcmData.Id).add(rcmData.CCF_Control__r.Control_Sub_Certifier__c);
                }else{
                    // When a part of a group (i.e. its a child), we want to add the ownership details to the parent
                    controlUniqueCcf.get(rcmData.Parent_Control_Scope__c).add(rcmData.CCF_Control__r.Object_Name__c);
                    controlUniqueOwners.get(rcmData.Parent_Control_Scope__c).add(rcmData.CCF_Control__r.Control_Owner__c);
                    controlUniquePerformers.get(rcmData.Parent_Control_Scope__c).add(rcmData.CCF_Control__r.Control_Performer__c);
                    controlUniqueExecs.get(rcmData.Parent_Control_Scope__c).add(rcmData.CCF_Control__r.Control_Executive__c);
                    controlUniqueSubcert.get(rcmData.Parent_Control_Scope__c).add(rcmData.CCF_Control__r.Control_Sub_Certifier__c);
                }
            }
        }
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Build the relevant report items for the reportwrapper, which get displayed via the LWC component 
     * Karl_ARL_Datatable. Note that this is a multi-object reportwrapper.
     * @param auditScopeId - picklist value for audit scope
     * @return Report Wrapper contents for Wire data feed
     */
    @AuraEnabled(cacheable=true)
    public static List<ReportWrapper> getReport(String auditScopeId, String controlFamily, String [] activeStatus ){  
        
        // Step 1: Generate dataset using generateData()
        generateData(auditScopeId, controlFamily, activeStatus);             

        // Step 2: Return Datatable Data
        return getRcmReport( auditScopeId );
    }

    /**
     @author Shraddha Patil   
     * @email shraddhapatil@salesforce.com
     * @description build RCM
     * @return aocFilter string
     */
    private static List<ReportWrapper> getRcmReport( String auditScopeId ){
        String baseUrl = URL.getSalesforceBaseUrl().toExternalForm();
        String baseRecordURL = baseUrl + '/one/one.app?#/sObject/'; // force rendering in Lightning instead of Classic
        String baseUrlSuffix = '/view'; // Required for Lightning record view
      
        List<ReportWrapper> items = new List<ReportWrapper>();

        // Create map of available picklist items (so we can get the label)
        list<Schema.PicklistEntry> scopePicklist = Control_Scope__c.Scope_Name__c.getDescribe().getPicklistValues();

        map<String,String> scopeLabels = new map<String,String>();
        for (Schema.PicklistEntry pe : scopePicklist){
            scopeLabels.put(pe.getValue(), pe.getLabel());
        }

        /* Generate the data set continuing with filteredControls - also only filtering those which are NOT children controls (Parent is null) */
        for( Control_Scope__c rcmData : Database.query('SELECT Id, Scope_Name__c, Triggered_Control_Identifier__c, Control_Description__c, ' +
                                                                'KARL_Triggered_Control_Name__c, Modified_Description__c, Use_L1_Language__c, '+
                                                                'toLabel(Control_Name__r.Family_Name__c), KARL_CS_CCF_Alignment__c, '+
                                                                'Parent_Control_Scope__c, Grouping_Control_Scope__c '+
                                                                'FROM Control_Scope__c '+
                                                                'WHERE Id in: filteredControls AND Parent_Control_Scope__c = null ' +
                                                                'WITH SECURITY_ENFORCED')){
       
            ReportWrapper rw = new ReportWrapper();
            rw.scope                = rcmData.Scope_Name__c;
            rw.controlId            = rcmData.Triggered_Control_Identifier__c;
            rw.controlName          = rcmData.KARL_Triggered_Control_Name__c;

            /* Populate the control description field based on criteria:
                #1: Are we using the CCF Override?
                #2: If not using CCF override, is CCF Alignment FALSE and the Modified Description is null. This is intended to capture
                    the baseline (L1) control description where we don't have anything in Modified Description - ultimately shouldn't 
                    in production but there is a chance of dirty data.
            */
            if(rcmData.Use_L1_Language__c || (!rcmData.KARL_CS_CCF_Alignment__c && String.isBlank(rcmData.Modified_Description__c))){
                rw.ctrlDescription      = rcmData.Control_Description__c;
            }else{
                rw.ctrlDescription      = rcmData.Modified_Description__c;
            }
            
            /* Create some empty strings (first assume that we aren't mapped) to prevent null reference errors */
            rw.ccfControlName       = '';
            rw.controlOwner         = '';
            rw.controlPerformer     = '';
            rw.controlExecutive     = '';
            rw.controlSubcertifier  = '';
            
            /* If the control is mapped with CCF then we'll join the data together for each value below */
            if(controlUniqueCcf.containsKey(rcmData.Id)){
                System.debug('HERE >> ' + controlUniqueCcf.get(rcmData.Id));
                rw.ccfControlName       = String.join(controlUniqueCcf.get(rcmData.Id), '\r\n');
            }
            if(controlUniqueOwners.containsKey(rcmData.Id)){
                rw.controlOwner         = String.join(controlUniqueOwners.get(rcmData.Id), '\r\n');
            }
            if(controlUniquePerformers.containsKey(rcmData.Id)){
                rw.controlPerformer     = String.join(controlUniquePerformers.get(rcmData.Id), '\r\n');
            }
            if(controlUniqueExecs.containsKey(rcmData.Id)){
                rw.controlExecutive     = String.join(controlUniqueExecs.get(rcmData.Id), '\r\n');
            }
            if(controlUniqueSubcert.containsKey(rcmData.Id)){
                rw.controlSubcertifier  = String.join(controlUniqueSubcert.get(rcmData.Id), '\r\n');
            }
            /* Taking the control family from the Control object instead of CCF so that it aligns with our picklist value set */
            rw.controlFamily        = rcmData.Control_Name__r.Family_Name__c;                                           

            items.add(rw);
        }
        return items;
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description This is a wrapper class to capture the ARL line items at a row level.
     * @return nothing
     */
    public class ReportWrapper{
        @AuraEnabled public String scope;
        @AuraEnabled public String controlId;
        @AuraEnabled public String controlName;
        @AuraEnabled public String ctrlDescription;
        @AuraEnabled public String ccfControlName;
        @AuraEnabled public String controlPerformer;
        @AuraEnabled public String controlOwner;
        @AuraEnabled public String controlExecutive;
        @AuraEnabled public String controlSubcertifier;
        @AuraEnabled public String controlFamily;

    }
}
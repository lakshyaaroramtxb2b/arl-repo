/* One time job to update Vlocity employees that were prematurely enrolled in mandatory courses */


public class SC_MassTCTupdater implements Database.Batchable<sObject>, Database.Stateful {

    private Set<String> emailAddresses = new Set<String>();
    private Set<String> coursesList = new Set<String>();
    private final Date AUG032020 = Date.newInstance(2020, 8, 3);

    public SC_MassTCTupdater () {
        for (ESA_Scratch__c s : [SELECT Name FROM ESA_Scratch__c]) {
            emailAddresses.add(s.Name);
        }
        coursesList.add('a1W3A000003EbrZUAS'); //Security Training 2019-2020
        coursesList.add('a1W3A000003vkfDUAQ'); //Develop Securely at Salesforce 2019-2020
        coursesList.add('a1W3A0000045nTIUAY'); //Security Awareness for Executives
        
    }

    public Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(
        [SELECT Id, Training_Course__c FROM Training_Course_Taken__c 
         WHERE Due_Date__c != null
         AND Date_Completed__c = null
         AND Training_Course__c IN : coursesList
         AND Contact__r.Email IN : emailAddresses]
        );
    }

    public void execute(Database.BatchableContext BC, List<sObject> scope) {
        for (Training_Course_Taken__c tct : (List<Training_Course_Taken__c>) scope) {
        	if (tct.Training_Course__c == 'a1W3A000003EbrZUAS') tct.Due_Date__c = AUG032020.addDays(21); 
        	if (tct.Training_Course__c == 'a1W3A000003vkfDUAQ') tct.Due_Date__c = AUG032020.addDays(30); 
        	if (tct.Training_Course__c == 'a1W3A0000045nTIUAY') tct.Due_Date__c = AUG032020.addDays(30);
            tct.Escalation1__c = null;
            tct.Escalation2__c = null;
            tct.Escalation3__c = null;
            tct.Escalation4__c = null;
            tct.Escalation5__c = null;
            tct.Last_Escalation__c = null;
            tct.Date_of_Escalation_to_Manager__c = null;
            tct.Date_of_Escalation_to_Trust_Engagement__c = null;
        }
        update scope; 
    }

    public void finish(Database.BatchableContext BC) {
    } 

    public static void run() {
        Database.executeBatch(new SC_MassTCTupdater());
    }

}
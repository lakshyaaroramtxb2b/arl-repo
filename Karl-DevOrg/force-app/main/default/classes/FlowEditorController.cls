public with sharing class FlowEditorController {
  public String xml {get; set;}
  public String debug {get;set;}
  public Flow__c f;
  
  public FlowEditorController(ApexPages.StandardController std) {
    f = [Select Name,Id,StartNode__c From Flow__c where Id=:std.getRecord().Id];
  }

  public String getJson() {
    return jsonify([Select Name,Id,Next__c,YesNext__c,NoNext__c,Flow__c,X__c,Y__c,
                           Text__c,isYesNo__c,SetTrue__c,SetFalse__c,AppendTo__c,
                           AppendVal__c,DecisionAndOr__c,DecisionRules__c,URL__c,
                           Type__c
                    FROM Node__c WHERE Flow__c =:f.Id]);
  }
  public PageReference loadXML() {
    Savepoint sp = Database.setSavepoint();
    List<Node__c> toUpdate = new List<Node__c>();
    List<Node__c> toInsert = new List<Node__c>();
    Set<String> processedNames = new Set<String>();
    
    Map<String,Node__c> allNodes = new Map<String,Node__c>();
    List<XMLDom.Element> els;
    XMLDom d= new XMLDom(xml);
    
    XMLDom.Element start = d.getElementByTagName('form').getElementByTagName('Start');
    //d.getElementByTagName('form').removeChild(start);
    /*for (XMLDom.Element elem: d.getElementByTagName('form').childNodes) {
          if (elem.nodeName == 'Start') {
            start = elem;
            break;
          }
    }*/
    if (start == null) {
      debug += 'Unable to find Start node';
      Database.rollback(sp);
      return null;
    }
    
    // Kill all current nodes.  Thank god for savepoints.
    delete [Select Id from Node__c Where Flow__c =:f.Id];
    
    // first, create all of the nodes
    Node__c n;
    XMLDom.Element tmpE;
    els = d.getElementByTagName('form').childNodes;
    if (els == null) {
      Database.rollback(sp);
      debug = 'No nodes found';
      return null;
    }
    
   
    for (XMLDom.Element e : els) {
      if (e.nodeName == 'Start'){
        // not a real node
        continue;
      }
      
      //if (processedNames.contains(e.getElementByTagName('name').getAttribute('value'))) {
        // we've already processed this node for whatever reason
      //  continue;
      //}
      n = new Node__c(X__c=Integer.valueOf(e.getAttribute('x')),
                           Y__c=Integer.valueOf(e.getAttribute('y')),
                           Name=e.getElementByTagName('name').getAttribute('value'),
                           Type__c = e.nodeName,
                           Flow__c = f.Id);
      if (n.Type__c == 'Question') {
        n.Text__c = e.getElementByTagName('text').getAttribute('value').replaceAll('(\\\\r)?\\\\n','\n');
        n.isYesNo__c = boolify(e.getElementByTagName('isYesNo').getAttribute('value'));
        
      } else if (n.Type__c == 'Variable') {
        tmpE = e.getElementByTagName('setTrue');
        if (tmpE != null) { n.SetTrue__c = tmpE.getAttribute('value').replaceAll('(\\\\r)?\\\\n','\n'); }
        
        tmpE = e.getElementByTagName('setFalse');
        if (tmpE != null) { n.SetFalse__c = tmpE.getAttribute('value').replaceAll('(\\\\r)?\\\\n','\n'); }
        
        tmpE = e.getElementByTagName('appendTo');
        if (tmpE != null) { n.AppendTo__c = tmpE.getAttribute('value').replaceAll('(\\\\r)?\\\\n','\n'); }
        
        tmpE = e.getElementByTagName('appendVal');
        if (tmpE != null) { n.AppendVal__c = tmpE.getAttribute('value').replaceAll('(\\\\r)?\\\\n','\n'); }

      } else if (n.Type__c == 'Decision') {
        tmpE = e.getElementByTagName('andOr');
        if (tmpE != null) { n.DecisionAndOr__c = tmpE.getAttribute('value'); }
        
        tmpE = e.getElementByTagName('rules');
        if (tmpE != null) { n.DecisionRules__c = tmpE.getAttribute('value').replaceAll('(\\\\r)?\\\\n','\n'); }
      } else if (n.Type__c == 'Url') {
        n.Type__c = 'URL';  // well, let's fix that
        tmpE = e.getElementByTagName('urlval');
        if (tmpE != null) { n.URL__c = tmpE.getAttribute('value');}
        
      } else {  
        debug = 'Unknown node type: '+n.Type__c;
        Database.rollback(sp);
        return null;
      }
      toInsert.add(n);
      allNodes.put(e.getAttribute('id'),n);
      // Let's index them by Element Id
      processedNames.add(n.Name);
    }
    insert toInsert;
    // OK, so all of the nodes have been created.  We just need to 
    // build all of their linkages
    Node__c curNode;
    String anId;
    
    XMLDom.Element anE;
    for (XMLDom.Element el : els) {
      if (el.nodeName == 'Start') {
        // skip
        continue;
      }
      curNode = allNodes.get(el.getAttribute('id'));
      if (curNode == null) {
          debug+='NULL NODE: '+el.getElementByTagName('name').getAttribute('value');
          Database.rollback(sp);
          return null;
      }
      if (el.nodeName == 'Variable') {
        anE = el.getElementByTagName('next');
        if (anE == null) {
          continue;  // it's not set
        }
        if ((anE.getAttribute('value') != null) && (anE.getAttribute('value') != '')){
          if (allNodes.containsKey(anE.getAttribute('value'))) {
            curNode.Next__c = allNodes.get(anE.getAttribute('value')).Id;
          }
        }
      } else if ((el.nodeName == 'Question') || (el.nodeName == 'Decision')) {
        anE = el.getElementByTagName('yesnext');
        if (anE != null) {
          if ((anE.getAttribute('value') != null) && (anE.getAttribute('value') != '')){
            if (allNodes.containsKey(anE.getAttribute('value'))) {
              curNode.YesNext__c = allNodes.get(anE.getAttribute('value')).Id;
            }
          }
        }
        anE = el.getElementByTagName('nonext');
        if (anE != null) {
          if ((anE.getAttribute('value') != null) && (anE.getAttribute('value') != '')){
            if (allNodes.containsKey(anE.getAttribute('value'))) {
              curNode.NoNext__c = allNodes.get(anE.getAttribute('value')).Id;
            }
          }
        }
      }
      //debug += '{'+curNode.Name+':('+curNode.Next__c+','+curNode.NoNext__c+','+curNode.YesNext__c+')},';
      toUpdate.add(curNode);
    }
    
    debug += '****************'+String.valueOf(toUpdate.size());
    
    update toUpdate;
    
    // links built.  Deal with the start node.
    f.StartNode__c = allNodes.get(start.getElementByTagName('node').getAttribute('value')).Id;
    update f;
    //debug = String.valueOf([Select count() from Node__c])+' nodes created';
    //debug = String.valueOf(allNodes.size());
    // !!!!!!!!!!!!!!!!!!!!!
    //Database.rollback(sp);
    return null;
  }
  
  public String Jsonify(List<Node__c> ns) {
    String json = '';
    for (Node__c n : ns) {
      json += '{name:\''+Esc(n.Name)+'\',x:';
      json += stringify(n.X__c)+',y:'+stringify(n.Y__c);
      json += ',id:\''+n.Id+'\'';
      if (n.Type__c == 'Question') {
        json += ',text:\''+Esc(n.Text__c)+'\',isYesNo:'+stringify(n.isYesNo__c)+',';
        json += 'type:\''+Esc(n.Type__c)+'\',yesnext:\''+stringify(n.YesNext__c)+'\'';
        json += ',nonext:\''+stringify(n.NoNext__c)+'\'},';
      } else if (n.Type__c == 'Variable') {
        json += ',setTrue:\''+Esc(n.SetTrue__c)+'\',setFalse:\''+Esc(n.SetFalse__c)+'\'';
        json += ',appendTo:\''+Esc(n.AppendTo__c)+'\',appendVal:\''+Esc(n.AppendVal__c)+'\',';
        json += 'type:\''+Esc(n.Type__c)+'\',next:\''+stringify(n.Next__c)+'\'},';
      } else if (n.Type__c == 'URL') {
        json += ',type:\'Url\',urlval:\''+Esc(n.URL__c)+'\'},';
      } else if (n.Type__c == 'Decision') {
        json += ',type:\''+Esc(n.Type__c)+'\',';
        json += 'rules:\''+Esc(n.DecisionRules__c)+'\',andOr:\''+Esc(n.DecisionAndOr__c)+'\'';
        json += ',yesnext:\''+stringify(n.YesNext__c)+'\',nonext:\''+stringify(n.NoNext__c)+'\'},';
      } 
    } 
    if (json == '') {
      return '[]';
    }
    if (f.StartNode__c != null) {
      return '[' + json +'{type:\'Start\',id:\'Start\',node:\''+stringify(f.StartNode__c)+'\',x:0,y:0}]';
    }
    return '['+json+']';
  }   
  public String Esc(String s) {
    if (s == null) {
      return '';
    }
    return s.replace('\\','\\\\').replace('\'','\\').replaceAll('(\r)?\n','\\\\n');
  }
    private Boolean boolify(String val) {
    pattern p = pattern.compile('[tT][rR][uU][eE]');
    return p.matcher(val).matches();
  }
  private String stringify(Boolean b) {
    if (b) {
      return 'true';
    }
    else {
      return 'false';
    }
  }
  private String stringify(Integer i) {
    if (i == null) {
      return '0';
    }
    else {
      return String.valueOf(i);
    }
  }
  private String stringify(Decimal i) {
    if (i == null) {
      return '0';
    }
    else {
      return String.valueOf(i);
    }
  }
  private String stringify(Id i) {
    if (i == null) {
      return '';
    }
    else {
      return String.valueOf(i);
    }
  }
}
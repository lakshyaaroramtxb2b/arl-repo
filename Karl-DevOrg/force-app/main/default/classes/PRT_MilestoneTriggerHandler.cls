/***********************************************************
 * Handler for PRT_MilestoneTrigger
* Created by 	- Prashant Gupta
* Date 		- 09-09-2019
************************************************************/
public class PRT_MilestoneTriggerHandler {
    
    Public Static Boolean MILESTONELENGTHvALIDATION = TRUE;
   
	/***********************************************************
	 * Before Insert method to be called from trigger
     * Created by 	- Prashant Gupta
     * Date 		- 09-09-2019
     ************************************************************/
    public static void beforeInsert(List<Milestone_PRT__c> newList){
        PRT_Utility.updateProgramLookups(newList,null);
    }
    
	/***********************************************************
	 * Before update method to be called from trigger
     * Created by 	- Prashant Gupta
     * Date 		- 09-09-2019
     ************************************************************/
    public static void beforeUpdate(List<Milestone_PRT__c> newList, Map<id,Milestone_PRT__c> oldMap){        
        PRT_Utility.updateProgramLookups(newList,oldMap);
        PRT_MilestoneTriggerHelper.milestoneDateUpdate(newList,oldMap);
        PRT_MilestoneTriggerHelper.checkRequiredFields(newList,oldMap);
        PRT_Utility.milestoneHistoryTracking(newList, oldMap, SObjectType.Milestone_PRT__c.fieldSets.getMap().get('History_Tracking'));
    }

    /***********************************************************
	 * Before Insert method to be called from trigger
     * Created by 	- Virendra Yadav
     * Date 		- 02-12-2019
     ************************************************************/
    public static void afterUpdate(List<Milestone_PRT__c> newList, Map<Id, Milestone_PRT__c> oldMap){
        if(PRT_Constants.INTEGRATION_SETTING.GUS_Milestone_Integration__c)
        	PRT_MilestoneTriggerHelper.updateGusEpicRecords(newList,oldMap);
        //if(MILESTONELENGTHvALIDATION)
        	//PRT_MilestoneTriggerHelper.milestoneValidationRule(newList,null);
    }
    /***********************************************************
	 * Before Insert method to be called from trigger
     * Created by 	- Virendra Yadav
     * Date 		- 02-12-2019
     ************************************************************/
    public static void afterInsert(List<Milestone_PRT__c> newList, Map<Id, Milestone_PRT__c> oldMap){
        if(PRT_Constants.INTEGRATION_SETTING.GUS_Milestone_Integration__c)
        	PRT_MilestoneTriggerHelper.insertGusEpicRecords(newList,null);
        //if(MILESTONELENGTHvALIDATION)
        	//PRT_MilestoneTriggerHelper.milestoneValidationRule(newList,null);
    }
}
public without sharing class CharterSLAs {
    public String objectName;
    public String fieldName1;
    public String fieldName2;
    
    // Bucket the events into these groups (hourly, daily, etc)
    public String period;
    
    // measure mean in these units
    public String precision;
    
    // don't pull events before this day
    public Datetime startDate;
    
    // If we run this code via executeanonymous (as source), we can return a value so
    // we raise it as a JSON string in an exception. Horrible hack, I know
    public Boolean resultsAsException = false;
    
    // just what it says
    public String extraWhere;
    
    // when manually providing records
    public SObject[] records;
    
    public void setRecords(SObject[] recs) {
        this.records = recs;
    }
    
    public Map<Datetime,Map<String,Map<String,Integer>>> percentageWithinSLAOverTime(Map<String,Integer> SLAs,String slaFieldName) {
    
        /* Calculate the time between two fields and see if that falls into an SLA, given 
         * as a map between SLA field values and a number. Break objects up 
         * into buckets defined by 'period' (hourly, weekly, etc).
         * An object falls into a bucket based on the value of field1.
         * 
         * For example, object=Case, field1=CreatedDate, field2=ClosedDate,
         * period=weekly, precision=minutes. The week-based bucket in which a Case falls is based on
         * its CreatedDate. This method would return a list of numbers like this:
         *
         * [ [1150675200000,57.20] ]
         *
         * where the first number represents noon GMT on the day at the end of the week and the second
         * represents the mean number of hours for all Cases with a ClosedDate falling
         * in that week
         *
         *
         * Required parameters:
         *   objectName - name of the object to query
         *   fieldName1 - name of the start date field
         *   fieldName2 - name of the end date field
         *   
         * Optional:
         *   startDate - don't query objects before this date. GMT
         *   period - compute mean after bucketing records into these groups. Hourly, daily, weekly, monthly, yearly. Default weekly
         *   precision - return mean in these terms (milliseconds, seconds, minutes, hours, days, weeks). Default days
         *
         */
        if ((objectName == null) && (records == null)) {
            throw new CharterException('Must supply object parameter');
        }
        if (fieldName1 == null) {
            throw new CharterException('Must supply field1 parameter');
        }
        if (fieldName2 == null) {
            throw new CharterException('Must supply field2 parameter');
        }
        // CRUD/FLS checks on the request. Will throw an exception if it 
        // doesn't work out
        checkAccess(objectName,new String[]{fieldName1,fieldName2});
        
        if (period == null) {
            period = 'monthly';
        }
        final Set<String> ALLOWED_PERIODS = new Set<String>{'hourly','daily','weekly','monthly','yearly'};
        if (!ALLOWED_PERIODS.contains(period)) {
            throw new CharterException('Invalid period '+period);
        }
        
        if (precision == null) {
            // this means the SLAs given are being measured in minutes
            precision = 'minutes';
        }
        final Set<String> ALLOWED_PRECISION = new Set<String>{'milliseconds', 'seconds', 'minutes', 'hours', 'days', 'weeks'};
        if (!ALLOWED_PRECISION.contains(precision)) {
            throw new CharterException('Invalid precision '+precision);
        }
        Long millisecondConversionFactor;
        if (precision == 'milliseconds') {
            millisecondConversionFactor = 1;
        } else if (precision == 'seconds') {
            millisecondConversionFactor = 1000;
        } else if (precision == 'minutes') {
            millisecondConversionFactor = 1000 * 60;
        } else if (precision == 'hours') {
            millisecondConversionFactor = 1000 * 60 * 60;
        } else if (precision == 'days') {
            millisecondConversionFactor = 1000 * 60 * 60 * 24;
        } else if (precision == 'weeks') {
            millisecondConversionFactor = 1000 * 60 * 60 * 24 * 7;
        }
       
       
        // DateBucket => {SLA_FIELD_VALUE => {'in_sla':5,'out_sla':30,'total':35}
        Map<Datetime,Map<String,Map<String,Integer>>> res = new Map<Datetime,Map<String,Map<String,Integer>>>();
        
        // fieldname1 (e.g. CreatedDate) needs to not be null, otherwise we can't know if it's in SLA. We could still know that something
        // is out of SLA with fieldName2 being null based on the time that has passed since fieldName1
        String sql = 'SELECT Id, '+fieldName1+','+fieldName2+','+slaFieldName+' FROM '+objectName+' WHERE isDeleted=false AND '+fieldName1+' != null';
        Set<String> slaNames = SLAs.keySet();
        sql += ' AND '+slaFieldName+' IN: slaNames';
        
        if (startDate != null) {
            sql += ' AND CreatedDate >: startDate';
        }
        
        if (extraWhere != null) {
            sql += ' AND '+extraWhere;
        }
        
        sql += ' ORDER BY '+fieldName2;
        System.debug(sql);
        
        if (records == null) {
            for (List<SObject> cl : Database.query(sql)) { 
                Map<Datetime,List<SObject>> bukkets = new Map<Datetime,List<SObject>>();
                // modify in place. Ugly but I don't want to do any copying
                dateSpanBucketize(bukkets,cl, fieldName1, fieldName2, period);                  
                Datetime now = Datetime.now();

                System.debug('BUCKET: '+fieldName1+fieldName2);
                System.debug(cl);
                System.debug(bukkets.get(lastDateOfMonth(Datetime.now())));
                for (Datetime bucket : bukkets.keySet()) {
                    if (!res.containsKey(bucket)) {
                        // Sev1,Sev2,Sev3,Total
                        res.put(bucket,new Map<String,Map<String,Integer>>());
                        for (String slaName : SLAs.keySet()) {
                            res.get(bucket).put(slaName,new Map<String,Integer>{'in_sla'=>0,'out_sla'=>0,'total'=>0});
                        }
                    }
                    
                    for (SObject c : bukkets.get(bucket)) {
                        String slaFieldValue = (String) c.get(slaFieldName);
                        Integer sla = SLAs.get(slaFieldValue);
                        // SLA shouldn't be null since we only queried for SLA values that we know about
                        
                        Datetime field2Date = (Datetime) c.get(fieldName2);
                        Datetime field1Date = (Datetime) c.get(fieldName1);
                        Map<String,Integer> tracker = res.get(bucket).get(slaFieldValue);
                        // Quick check for records that haven't been touched yet
                        if (field2Date == null) {
                            
                            // Well, they haven't gotten to this one yet. Make sure we're not already out of SLA. Millisecond comparison
                            if ((now.getTime() - field1Date.getTime()) > (sla * millisecondConversionFactor)) {
                                // out of SLA
                                System.debug('Out of SLA based on time lapsed -'+fieldName2+'-'+c.id);
                                System.debug(field2Date);
                                System.debug(now);
                                System.debug(field1Date);
                                System.debug(sla * millisecondConversionFactor);
                                tracker.put('out_sla', tracker.get('out_sla')+1);
                                tracker.put('total',tracker.get('total')+ 1);
                            } else {
                                tracker.put('in_sla', tracker.get('in_sla')+1);
                                tracker.put('total',tracker.get('total')+ 1);
                            }
                        } else {
                            if ((field2Date.getTime() - field1Date.getTime()) > (sla * millisecondConversionFactor)) {
                                tracker.put('out_sla',tracker.get('out_sla') + 1);
                            } else {
                                tracker.put('in_sla',tracker.get('in_sla') + 1);
                            }
                            tracker.put('total',tracker.get('total')+1);
                        }
                    }
                }
            }
        } else {
            Set<SObject> acceptable = new Set<SObject>(records);
            Set<SObject> toRemove = new Set<SObject>();
            
            for (SObject record : records) {
                // cleanup since we're not using SOQL null checks
                if (record.get(fieldName1) == null) {
                    toRemove.add(record);
                }
            }
            if (toRemove.size() > 0) {
                acceptable.removeAll(toRemove);
                records.clear();
                records.addAll(acceptable);
            }
            
            Map<Datetime,List<SObject>> bukkets = new Map<Datetime,List<SObject>>();
            // modify in place. Ugly but I don't want to do any copying
            dateSpanBucketize(bukkets,records, fieldName1, fieldName2, period);                  
            
            Datetime now = Datetime.now();
            for (Datetime bucket : bukkets.keySet()) {
                if (!res.containsKey(bucket)) {
                    // Sev1,Sev2,Sev3,Total
                    res.put(bucket,new Map<String,Map<String,Integer>>());
                    for (String slaName : SLAs.keySet()) {
                        res.get(bucket).put(slaName,new Map<String,Integer>{'in_sla'=>0,'out_sla'=>0,'total'=>0});
                    }
                }
                
                for (SObject c : bukkets.get(bucket)) {
                    String slaFieldValue = (String) c.get(slaFieldName);
                    Integer sla = SLAs.get(slaFieldValue);
                    if (sla == null) {
                        // no sla, skip. This is different from the SOQL version where we only return records
                        // when we have an SLA for them
                        continue;
                    }
                    
                    Datetime field2Date = (Datetime) c.get(fieldName2);
                    Datetime field1Date = (Datetime) c.get(fieldName1);
                    Map<String,Integer> tracker = res.get(bucket).get(slaFieldValue);
                    // Quick check for records that haven't been touched yet
                    if (field2Date == null) {
                        
                        // Well, they haven't gotten to this one yet. Make sure we're not already out of SLA. Millisecond comparison
                        if ((now.getTime() - field1Date.getTime()) > (sla * millisecondConversionFactor)) {
                            // out of SLA
                            System.debug('Out of SLA based on time lapsed -'+fieldName2+'-'+c.id);
                            System.debug(field2Date);
                            System.debug(now);
                            System.debug(field1Date);
                            System.debug(sla * millisecondConversionFactor);
                            tracker.put('out_sla', tracker.get('out_sla')+1);
                            tracker.put('total',tracker.get('total')+ 1);
                        } else {
                            tracker.put('in_sla', tracker.get('in_sla')+1);
                            tracker.put('total',tracker.get('total')+ 1);
                        }
                    } else {
                        if ((field2Date.getTime() - field1Date.getTime()) > (sla * millisecondConversionFactor)) {
                            tracker.put('out_sla',tracker.get('out_sla') + 1);
                        } else {
                            tracker.put('in_sla',tracker.get('in_sla') + 1);
                        }
                        tracker.put('total',tracker.get('total')+1);
                    }
                }
            }
        }
        
        if (resultsAsException) {
            throw new CharterException(JSON.serialize(res));
        }
        return res;
        
    }

    public Map<Long,Map<String,Map<String,Integer>>> percentageWithinSLAOverTimeLong(Map<String,Integer> SLAs,String slaFieldName) {
        boolean resAsExc = this.resultsAsException;
        this.resultsAsException = false;
        Map<Datetime,Map<String,Map<String,Integer>>> res = percentageWithinSLAOverTime(SLAs,slaFieldName);
        Map<Long,Map<String,Map<String,Integer>>> r = new Map<Long,Map<String,Map<String,Integer>>> ();
        for (Datetime k: res.keySet()) {
            r.put(k.getTime(),res.get(k));
        }
        
        if (resAsExc) {
            throw new CharterException(JSON.serialize(r));
        }
        return r;
    }
    
    public Map<Datetime,List<SObject>> dateSpanBucketize(Map<Datetime,List<SObject>> results,SObject[] objects, String startFieldName, String endFieldName, String period) {
        
        Datetime defaultStopDate;
        if (period == 'weekly') {
            defaultStopDate = closestSaturday(Datetime.now());
        } else if (period == 'monthly') {
            defaultStopDate = lastDateOfMonth(Datetime.now());
        } else if (period == 'yearly') {
            defaultStopDate = lastDateOfYear(Datetime.now());
        } else if (period == 'daily') {
            defaultStopDate = sameDay(Datetime.now());
        } else if (period == 'hourly') {
            defaultStopDate = sameHour(Datetime.now());
        }

        for (SObject o : objects) {
            Datetime startDate = (Datetime) o.get(startFieldName);
            Datetime bucket,stopDate;

            // Either the closed date or the end of the last period we're going to report (e.g. last day of this month)
            if (o.get(endFieldName) != null) { 
                stopDate = (Datetime) o.get(endFieldName);
                if (period == 'weekly') {
                    stopDate = closestSaturday(stopDate);
                } else if (period == 'monthly') {
                    stopDate = lastDateOfMonth(stopDate);
                } else if (period == 'yearly') {
                    stopDate = lastDateOfYear(stopDate);
                } else if (period == 'daily') {
                    stopDate = sameDay(stopDate);
                } else if (period == 'hourly') {
                    stopDate = sameHour(stopDate);
                }
            } else {
                stopDate = defaultStopDate;
            }
            
            // It's common for the stopDate to be less than the bucket date (e.g. last day of month). We want to make sure 
            // we at least put it in the period in which it was created
            Boolean firstRun = true;
            
            // pretty sure all of this won't be accurate for leap-X's
            if (period == 'weekly') {
                bucket = closestSaturday(startDate);
                while ((bucket <= stopDate) || (firstRun)){
                    if (!results.containsKey(bucket)) {
                        results.put(bucket,new List<Sobject>());
                    }
                    results.get(bucket).add(o);
                    bucket = bucket.addDays(7);
                    firstRun = false;
                }
            } else if (period == 'monthly') {
                bucket = lastDateOfMonth(startDate);
                while ((bucket <= stopDate) || (firstRun)){
                    if (!results.containsKey(bucket)) {
                        results.put(bucket,new List<Sobject>());
                    }
                    results.get(bucket).add(o);
                    bucket = lastDateOfMonth(bucket.addDays(1));
                    firstRun = false;
                }
            } else if (period == 'yearly') {
                bucket = lastDateOfYear(startDate);
                while ((bucket <= stopDate) || (firstRun)){
                    if (!results.containsKey(bucket)) {
                        results.put(bucket,new List<Sobject>());
                    }
                    results.get(bucket).add(o);
                    bucket = lastDateOfYear(bucket.addDays(10));
                    firstRun = false;
                }
            } else if (period == 'daily') {
                bucket = sameDay(startDate);
                while ((bucket <= stopDate) || (firstRun)){
                    if (!results.containsKey(bucket)) {
                        results.put(bucket,new List<Sobject>());
                    }
                    results.get(bucket).add(o);
                    bucket = bucket.addDays(1);
                    firstRun = false;
                }
            } else if (period == 'hourly') {
                bucket = sameHour(startDate);
                while ((bucket <= stopDate) || (firstRun)){
                    if (!results.containsKey(bucket)) {
                        results.put(bucket,new List<Sobject>());
                    }
                    results.get(bucket).add(o);
                    bucket = bucket.addHours(1);
                    firstRun = false;
                }
            } else {
                throw new CharterException('Unknown period '+period);
            }
        }

        return results;
    }
    public Datetime closestSaturday(Datetime subj) {
        Datetime sat;
        String dayOfWeekStr = subj.formatGMT('E');
        if (dayOfWeekStr == 'Sun') {
            sat = subj.addDays(6);
        } else if (dayOfWeekStr == 'Mon') {
            sat = subj.addDays(5);
        } else if (dayOfWeekStr == 'Tue') {
            sat = subj.addDays(4);
        } else if (dayOfWeekStr == 'Wed') {
            sat = subj.addDays(3);
        } else if (dayOfWeekStr == 'Thu') {
            sat = subj.addDays(2);
        } else if (dayOfWeekStr == 'Fri') {
            sat = subj.addDays(1);
        } else {
            sat = subj;
        }
        
        return datetime.newInstanceGmt(sat.yearGmt(),sat.monthGmt(),sat.dayGmt(),23,59,59);
    }
    public Datetime lastDateOfMonth(Datetime subj) {
        if (subj.dayGmt() == 31) {
            // This is the last day. Make a light clone
            return Datetime.newInstanceGmt(subj.yearGmt(),subj.monthGmt(),31,23,59,59);
        }
        
        // This is stupid. I would think that asking for an invalid date (e.g. Feb 31) would
        // throw an exception, but it just overflows into the next month
        Datetime attempt;
        for (Integer i : new Integer[]{31,30,29,28}) {
            attempt = Datetime.newInstanceGmt(subj.yearGmt(),subj.monthGmt(),i,23,59,59);
            if (attempt.monthGmt() == subj.monthGmt()) {
                return attempt;
            }
        }
        throw new CharterException('Can\'t figure out how many days are in '+subj.formatGmt('M'));
    }
    public Datetime lastDateOfYear(Datetime subj) {
        return Datetime.newInstanceGmt(subj.yearGmt(),12,31,23,59,59);
    }
    public Datetime sameDay(Datetime subj) {
        return Datetime.newInstanceGmt(subj.yearGmt(),subj.monthGmt(),subj.dayGmt(),23,59,59);
    }
    public Datetime sameHour(Datetime subj) {
        return Datetime.newInstanceGmt(subj.yearGmt(),subj.monthGmt(),subj.dayGmt(),subj.hourGmt(),59,59);
    }
    
    public void checkAccess(String objectName, String[] fieldNames) {
        Schema.SObjectType objType = Schema.getGlobalDescribe().get(objectName);
        if (objType == null) {
            // we now support non-SObjects
            return;
            //throw new CharterException('Unknown object type '+objType);
        } else if (objType.getDescribe().isAccessible() == false) {
            throw new CharterException('Unknown object type '+objType);
        }
        
        for (String f : fieldNames ) {
            Schema.SObjectField fieldObj = objType.getDescribe().fields.getMap().get(f);
            if (fieldObj == null) {
                throw new CharterException('Unknown field '+f);
            }
            if (fieldObj.getDescribe().isAccessible() == false) {
                throw new CharterException('Unknown field '+f);
            }
        }
    }
}
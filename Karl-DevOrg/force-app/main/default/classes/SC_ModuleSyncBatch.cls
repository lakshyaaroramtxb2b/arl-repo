/*
 * Apex Batch class used to sync Modules
 */

global class SC_ModuleSyncBatch implements
    Database.Batchable<SC_ModuleResponse.Data>,
    Database.AllowsCallouts,
    Database.Stateful {

    Integer offSet;
    Integer totalCount;

    global static final String MODULE = 'module_';
    global static final Integer OFF_SET_SIZE = 50;

    public SC_ModuleSyncBatch(Integer pOffset) {
        this.offSet = pOffset;
    }

    global Iterable<SC_ModuleResponse.Data> start(Database.BatchableContext info) {

        SC_TrailheadApi api = new SC_TrailheadApi();
        SC_ModuleResponse moduleResponse = api.getModules(this.offSet);
        this.totalCount = moduleResponse.total_count;

        return (Iterable<SC_ModuleResponse.Data>)moduleResponse.data;
    }

    global void execute(Database.BatchableContext info, SC_ModuleResponse.Data[] pScope) {

        List<Training_Course__c> upsertCourses = new List<Training_Course__c>();

        for (SC_ModuleResponse.Data module :pScope) {
            upsertCourses.add(new Training_Course__c(
                Sync_Id__c = SC_ModuleSyncBatch.MODULE + module.api_name,
                Name = module.title==null? null : module.title.left(80),
                Is_Active__c = !module.archived,
                Course_Level__c = 'Module',
                Last_Import_Date__c = System.today(),
                Provider__c = 'Trailhead',
                Provider_ID__c = module.api_name,
                URL__c = module.web_url
            ));
        }

        if (!upsertCourses.isEmpty()) {
            Database.upsert(
                upsertCourses,
                Training_Course__c.fields.Sync_Id__c
            );
        }
    }

    global void finish(Database.BatchableContext info) {
        this.offSet += SC_ModuleSyncBatch.OFF_SET_SIZE;
        if (this.totalCount > this.offSet) {
            Database.executeBatch(new SC_ModuleSyncBatch(this.offSet));
        } else if (!Test.isRunningTest()) {
            Database.executeBatch(new SC_RelationshipSyncBatch(0));
        }
    }
}
public with sharing class PRT_V2MOMAlignmentViewController {
    
    public PRT_V2MOMAlignmentViewController() {}
    @TestVisible
    private static list<sObject> mockallV2MOMList = new list<sObject>();
    @TestVisible
    private static list<sObject> mockAllMeasureList = new list<sObject>();
    @TestVisible
    private static list<sObject> mockAllMethodList = new list<sObject>();

    
    
    @AuraEnabled
    public static V2MomWrapper getV2MomInformation(){
       
        User userRecord = [select id,FullPhotoUrl, ContactID, EmployeeNumber, Name,contact.name, ManagerId, Manager.Employeenumber 
                           from User 
                           where id =:userInfo.getUserId()
                           LIMIT 1];
        system.debug('### userRecord.EmployeeNumber ' + userRecord.EmployeeNumber);
        system.debug('### userRecord.Name ' + userRecord.Name);
        List<V2MomMethodsWrapper> v2MomMethods = new List<V2MomMethodsWrapper>();
        List<V2MomMeasuresWrapper> v2MomMeasures = new List<V2MomMeasuresWrapper>();
        V2MomWrapper data = new V2MomWrapper(getV2MOMData(userRecord.EmployeeNumber), userRecord); 
        return data;
    }  
    public static List<V2MomMethodsWrapper> getV2MOMData(String employeeNumber){
        List<V2MomMethodsWrapper> methodData = new List<V2MomMethodsWrapper>();
        ID userID = null;
        if((employeeNumber!=null && employeeNumber!='') || Test.isRunningTest()){
            for(Org62User__x userRec: [SELECT ID,ExternalID, EmployeeNumber__c FROM Org62User__x WHERE EmployeeNumber__c = :employeeNumber ] ){
            userID = userRec.ExternalID;
            }
            
        }
        system.debug( '### line 36 userId ' + userId);
        V2MOM_c__x v2MOMRecord ;
        if(Test.isRunningTest()){
            v2MOMRecord = (V2MOM_c__x)mockallV2MOMList[0];           
        }else{
            Date startDayOfFiscalYear = Date.newInstance(date.today().year(), 2, 1);
            Date lastDayOfFiscalMonth = Date.newInstance(date.today().addYears(1).year(), 1, 31);
            for(V2MOM_c__x v2mom : [SELECT ID,ExternalId,V2MOM_User_c__c,CreatedDate__c FROM V2MOM_c__x WHERE V2MOM_User_c__c=:userID and Status_c__c = 'Published' Order by CreatedDate__c DESC LIMIT 1]){
                v2MOMRecord = v2mom;
                /*if(System.today().month()!=1){
                    if((v2mom.CreatedDate__c.year() == startDayOfFiscalYear.year() && v2mom.CreatedDate__c.month() != 1) || (v2mom.CreatedDate__c.year() == lastDayOfFiscalMonth.year() && v2mom.CreatedDate__c.month() == lastDayOfFiscalMonth.month())){
                        v2MOMRecord = v2mom;
                        system.debug('### line 50 v2MOMRecord ' + v2MOMRecord);
                    }
                }else if((v2mom.CreatedDate__c.year() == date.today().year() && v2mom.CreatedDate__c.month() == 1) || (v2mom.CreatedDate__c.year() == date.today().year()-1 && v2mom.CreatedDate__c.month()!=1)){
                    v2MOMRecord= v2mom;
                    system.debug('### line 50 v2MOMRecord ' + v2MOMRecord);
                }*/
                
            }
        }  
        System.debug('V2Mom Record' + v2MOMRecord);
        Map<id,List<Measure_c__x>> measuresMap  = new Map<id,List<Measure_c__x>>();
        Set<Id> measureIdSet = new Set<Id>();
        List<Measure_c__x> measureRecordList = new List<Measure_c__x>();
        if(Test.isRunningTest()){
            measureRecordList = mockAllMeasureList;
        }else{
            measureRecordList = [SELECT ID,V2MOM_c__c,MeasureName_c__c,Name__c, Method_c__c,ExternalId,DueDate_c__c,Comment_c__c,Status_c__c,Type_c__c
                                 FROM Measure_c__x 
                                 WHERE V2MOM_c__c = :v2MOMRecord.ExternalId ORDER BY Priority_c__c ASC];
        } 
        
        if((v2MOMRecord.ExternalId !=null && v2MOMRecord.ExternalId!='') || test.isRunningTest()){
            for(Measure_c__x measures : measureRecordList){
                if(!measuresMap.containsKey(measures.Method_c__c)){
                    measuresMap.put(measures.Method_c__c,new List<Measure_c__x>());
                    measureIdSet.add(measures.Id);
                }
                if(test.isRunningTest()){
                    measuresMap.put(userInfo.getUserId(),mockAllMeasureList);
                }else{
                    measuresMap.get(measures.Method_c__c).add(measures);
                    system.debug('>>> Method Map >> ' +  measuresMap.keySet());
                }
                
            }  
        }
       
        Map<id,id> dependentMethod_Vs_controllingMethodMap  = new Map<id,Id>();
        for(V2MOMMapping__c vMap : [SELECT ID, Controlling_Measure__c, Dependent_Method__c 
                                    FROM V2MOMMapping__c WHERE Dependent_Method__c IN : measuresMap.keySet() Order BY CreatedDate  ASC]){
                dependentMethod_Vs_controllingMethodMap.put(vMap.Dependent_Method__c,vMap.Controlling_Measure__c);
                                        System.debug('dependentMethod_Vs_controllingMethodMap View Controller' + dependentMethod_Vs_controllingMethodMap);
        }
        system.debug('line 51'+dependentMethod_Vs_controllingMethodMap);
        
        List<V2MOM_Method_c__x> v2MOMMethodList = new List<V2MOM_Method_c__x>();
        if(!test.isRunningTest()){
            Date startDayOfFiscalYear = Date.newInstance(date.today().year(), 2, 1);
            Date lastDayOfFiscalMonth = Date.newInstance(date.today().addYears(1).year(), 1, 31);
            for(V2MOM_Method_c__x v2MomMethodRecords:[SELECT ID, Name__c,CreatedDate__c,
                                                      Description_c__c,Priority_c__c,
                                                      ExternalId,MayEdit__c,MeasureCount_c__c
                                                      FROM V2MOM_Method_c__x 
                                                      WHERE V2MOM_c__c =:v2MOMRecord.ExternalId Order by Priority_c__c ASC ,CreatedDate__c DESC]){
                                                          v2MOMMethodList.add(v2MomMethodRecords);
                                                      }
            
            /*v2MOMMethodList = [SELECT ID, Name__c,
                                         Description_c__c,Priority_c__c,
                                         ExternalId,MayEdit__c,MeasureCount_c__c
                                         FROM V2MOM_Method_c__x 
                                         WHERE V2MOM_c__c =:v2MOMRecord.ExternalId];*/
        }else{
            v2MOMMethodList = mockAllMethodList;
        }
        
        if((v2MOMRecord.ExternalId!=null && v2MOMRecord.ExternalId!='')){
            for(V2MOM_Method_c__x methods : v2MOMMethodList){
                                             System.debug('??' + methods.ExternalId);
                                             V2MomMethodsWrapper methodRec = new V2MomMethodsWrapper();
                                             methodRec.methodName = methods.Name__c;
                                             methodRec.description = methods.Description_c__c;
                                             methodRec.priority = methods.Priority_c__c;
                                             methodRec.mayEdit = methods.MayEdit__c;
                                             methodRec.measureCount = methods.MeasureCount_c__c;
                                             methodRec.externalID = methods.ExternalId;
                                             methodRec.relatedMeasures = dependentMethod_Vs_controllingMethodMap.get(methods.ExternalId);
                                             List<V2MomMeasuresWrapper> wrapperList = new List<V2MomMeasuresWrapper>();
                                             if((measuresMap!=null && !measuresMap.isEmpty() && measuresMap.containsKey(methods.ExternalId))) {
                                                 for(Measure_c__x measure : measuresMap.get(methods.ExternalId)){
                                                     V2MomMeasuresWrapper wrap2 = new V2MomMeasuresWrapper();
                                                     wrap2.measureName = measure.MeasureName_c__c;
                                                     wrap2.name = measure.Name__c;
                                                     wrap2.dueDate = measure.DueDate_c__c;
                                                     wrap2.comment = measure.Comment_c__c;
                                                     wrap2.status = measure.Status_c__c;
                                                     wrap2.type = measure.Type_c__c;
                                                     wrapperList.add(wrap2);
                                                 }
                                             }
                                             methodRec.v2MomMeasures= wrapperList;
                                             methodData.add(methodRec);
                                  }
        }
        
        system.debug(methodData);
        return methodData;
    }
   
    /* function to create record in v2momMapping */
    @AuraEnabled
    public static void createV2Mapping(Map<String,String> dependentIdVsControllerIdMap){
        System.debug('dependentIdVsControllerIdMap.keySet()' + dependentIdVsControllerIdMap.keySet());
        List<V2MomMapping__c> v2MomRecordDeleteList = new List<V2MomMapping__c>(
                    [SELECT id FROM V2MomMapping__c
                    WHERE Dependent_Method__c IN :dependentIdVsControllerIdMap.keySet()]);
        if(!v2MomRecordDeleteList.isEmpty()){
            delete v2MomRecordDeleteList;
        }
        List<V2MomMapping__c> v2MomRecordInsertList = new List<V2MomMapping__c>();
        for(Id dependentId:dependentIdVsControllerIdMap.keySet()){
            if(dependentIdVsControllerIdMap.get(dependentId)!=null){
                V2MomMapping__c mapRecord = new V2MomMapping__c();
                mapRecord.Dependent_Method__c = dependentId;
                mapRecord.Controlling_Measure__c = dependentIdVsControllerIdMap.get(dependentId);
                v2MomRecordInsertList.add(mapRecord);
                System.debug('Record Inserted in List');  
            }
        }
        if(!v2MomRecordInsertList.isEmpty()){            
            insert v2MomRecordInsertList;
        }
        
    }
    
    /* function to publish the Alignment*/
    public static void publishV2Mom(){
        
    }
    
    public class V2MomWrapper{
        @AuraEnabled public List<v2MomMethodsWrapper> v2MomMethods;
        @AuraEnabled public User userRec;
        @AuraEnabled public String FullPhotoUrl;
        @AuraEnabled public String Name;
        
        public V2MomWrapper(List<v2MomMethodsWrapper> v2MomMethods,
                            User userRec){
                                this.V2MomMethods = V2MomMethods;
                                this.userRec = userRec;
                                
                            }
        
    }      
    public class V2MomMethodsWrapper{
        @AuraEnabled public String methodName;
        @AuraEnabled public String name;
        @AuraEnabled public String description;
        @AuraEnabled public String priority;
        @AuraEnabled public Decimal measureCount;
        @AuraEnabled public Boolean mayEdit;
        @AuraEnabled public Id relatedMethod;
        @AuraEnabled public Id relatedMeasures;
        @AuraEnabled public Id externalID;
        @AuraEnabled public List<v2MomMeasuresWrapper> v2MomMeasures;
    }
    public class V2MomMeasuresWrapper{
        @AuraEnabled public String measureName;
        @AuraEnabled public String name;
        @AuraEnabled public String comment;
        @AuraEnabled public Date dueDate;
        @AuraEnabled public String status;
        @AuraEnabled public String type;
    }
    
    
}
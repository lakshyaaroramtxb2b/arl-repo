public class SuppressionTest {

    	public void Test()
        {
            //Test single event
            //Test alert with multiple vents
            //Test if extracts event
            //test if none matching
            //test if all matching
            
            List<Detection_Security_Event__c> evts = new List<Detection_Security_Event__c>();
            
            //a
            Detection_Security_Event__c evt = new Detection_Security_Event__c();
            evt.DetectionSecurityEventCriteria__c = 'a353A000009fFLT';
            evt.Environment__c = 'TechOps';
            evt.EventTimestamp__c = datetime.now().formatGMT('yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'');
            evt.CollectedTimestamp__c = datetime.now().formatGMT('yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'');
            evt.FieldMapRaw__c = '{"Environment":"SFDC-IT","RegexMatch":"BRO_RADIUS","MacAddrSource":"","UserEmail":"joelyn.chin@salesforce.com","ResultMessage":"success","RemoteIP":"39.109.162.200","UserDepartment":"7219-Sales Comp Admin","SourceSystem":"10.200.11.160","UserLocationCountry":"SG","OfficeLocation":"SIN","IPSource":"10.200.9.243","Aggregator":"sfm0sednrllp001","UserLocationCity":"Singapore","NormalizedTimestamp":"2017-05-25T13:13:45.363+00:00","RawMsg":"<182>2017-05-25T13:13:02.870881+00:00 sfm0sednrllp001 RealSource: \\"10.200.11.160\\" Environment: \\"SFDC-IT\\" UUID: \\"AB21BF1C954C4EC1B861C542D95DFF39\\" RawMsg: <182>2017-05-25T13:13:00.458659+00:00 sin-sec-pcap-lp1 bro_radius: {\\"ts\\":1495717958.34786,\\"uid\\":\\"CJeTanKvcPTVizgx2\\",\\"id.orig_h\\":\\"10.200.9.243\\",\\"id.orig_p\\":4386,\\"id.resp_h\\":\\"10.200.11.24\\",\\"id.resp_p\\":1812,\\"username\\":\\"joelyn.chin\\",\\"mac\\":\\"\\",\\"remote_ip\\":\\"39.109.162.200\\",\\"result\\":\\"success\\"}","IPDestination":"10.200.11.24","User":"joelyn.chin","PortSource":"4386","PortDestination":"1812","UserLocationState":"CA","CollectedTimestamp":"2017-05-25T13:13:02.870881+00:00"}';

            evts.add(evt);
            
            //a
            evt = new Detection_Security_Event__c();
            evt.DetectionSecurityEventCriteria__c = 'a353A000009fFLT';
            evt.Environment__c = 'TechOps';
            evt.EventTimestamp__c = datetime.now().formatGMT('yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'');
            evt.CollectedTimestamp__c = datetime.now().formatGMT('yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'');
            evt.FieldMapRaw__c = '{"Environment":"SFDC-IT","RegexMatch":"BRO_RADIUS","MacAddrSource":"","UserEmail":"joelyn.chin@salesforce.com","ResultMessage":"success","RemoteIP":"39.109.162.200","UserDepartment":"7219-Sales Comp Admin","SourceSystem":"10.200.11.160","UserLocationCountry":"SG","OfficeLocation":"SIN","IPSource":"10.200.9.243","Aggregator":"sfm0sednrllp001","UserLocationCity":"Singapore","NormalizedTimestamp":"2017-05-25T13:13:45.363+00:00","RawMsg":"<182>2017-05-25T13:13:02.870881+00:00 sfm0sednrllp001 RealSource: \\"10.200.11.160\\" Environment: \\"SFDC-IT\\" UUID: \\"AB21BF1C954C4EC1B861C542D95DFF39\\" RawMsg: <182>2017-05-25T13:13:00.458659+00:00 sin-sec-pcap-lp1 bro_radius: {\\"ts\\":1495717958.34786,\\"uid\\":\\"CJeTanKvcPTVizgx2\\",\\"id.orig_h\\":\\"10.200.9.243\\",\\"id.orig_p\\":4386,\\"id.resp_h\\":\\"10.200.11.24\\",\\"id.resp_p\\":1812,\\"username\\":\\"joelyn.chin\\",\\"mac\\":\\"\\",\\"remote_ip\\":\\"39.109.162.200\\",\\"result\\":\\"success\\"}","IPDestination":"10.200.11.24","User":"joelyn.chin","PortSource":"4386","PortDestination":"1812","UserLocationState":"CA","CollectedTimestamp":"2017-05-25T13:13:02.870881+00:00"}';

            evts.add(evt);
            
            
            //b
            evt = new Detection_Security_Event__c();
            evt.DetectionSecurityEventCriteria__c = 'a353A000009fFLT';
            evt.Environment__c = 'TechOps';
            evt.EventTimestamp__c = datetime.now().formatGMT('yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'');
            evt.CollectedTimestamp__c = datetime.now().formatGMT('yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'');
            evt.FieldMapRaw__c = '{"Environment":"SFDC-IT","RegexMatch":"BRO_RADIUS","MacAddrSource":"","UserEmail":"amcneilly@salesforce.com","ResultMessage":"success","RemoteIP":"39.109.162.200","UserDepartment":"7219-Sales Comp Admin","SourceSystem":"10.200.11.160","UserLocationCountry":"SG","OfficeLocation":"SIN","IPSource":"10.200.9.243","Aggregator":"sfm0sednrllp001","UserLocationCity":"Singapore","NormalizedTimestamp":"2017-05-25T13:13:45.363+00:00","RawMsg":"<182>2017-05-25T13:13:02.870881+00:00 sfm0sednrllp001 RealSource: \\"10.200.11.160\\" Environment: \\"SFDC-IT\\" UUID: \\"AB21BF1C954C4EC1B861C542D95DFF39\\" RawMsg: <182>2017-05-25T13:13:00.458659+00:00 sin-sec-pcap-lp1 bro_radius: {\\"ts\\":1495717958.34786,\\"uid\\":\\"CJeTanKvcPTVizgx2\\",\\"id.orig_h\\":\\"10.200.9.243\\",\\"id.orig_p\\":4386,\\"id.resp_h\\":\\"10.200.11.24\\",\\"id.resp_p\\":1812,\\"username\\":\\"joelyn.chin\\",\\"mac\\":\\"\\",\\"remote_ip\\":\\"39.109.162.200\\",\\"result\\":\\"success\\"}","IPDestination":"10.200.11.24","User":"joelyn.chin","PortSource":"4386","PortDestination":"1812","UserLocationState":"CA","CollectedTimestamp":"2017-05-25T13:13:02.870881+00:00"}';

            evts.add(evt);
            
            INSERT evts;
        }
}
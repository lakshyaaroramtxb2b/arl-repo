/**
 * @File Name          : IEM_CaseVisibilityTriggerHelper.cls
 * @Description        : 
 * @Author             : Banshi
 * @Group              : 
 * @Last Modified By   : Banshi
 * @Last Modified On   : 11/14/2019, 12:54:24 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    11/14/2019   Banshi     Initial Version
**/
public class IEM_CaseVisibilityTriggerHelper {
    /*
* Created by - Banshi
* Date - 11/14/2019
* This functon is used to share case record with Community users
*/ 
    public static void createShareRecordsForCommunityUsers(List<Case_Visibility__c> newList){
        Map<Id, Id> caseUserIdsEditAccessMap = new Map<Id, Id>(); //Map to give edit access	
        Map<Id, Id> caseUserIdsReadAccessMap = new Map<Id, Id>(); //Map to give read access
        Map<Id, Set<Id>> caseUserExistingSharingMap = new Map<Id, Set<Id>>(); //Map for old sharing in case of update.
        Set<Id> contactIds = new Set<Id>();
        Map<Id, Id> contactUserMap = new Map<Id, Id>();
        System.debug('%%newList'+newList);
        for(Case_Visibility__c caseVisibility : newList){
            contactIds.add(caseVisibility.Contact__c);
        }
        if(!contactIds.isEmpty()){
            for(User u: [Select Id,ContactId from User where ContactId IN :contactIds AND IsActive= true]){
                contactUserMap.put(u.ContactId, u.Id);
            }
        }
        System.debug('%%contactUserMap'+contactUserMap);
        if(!contactUserMap.isEmpty()){
            for(Case_Visibility__c caseVisibility : newList){
                if(String.isNotBlank(caseVisibility.Contact__c) && contactUserMap.containsKey(caseVisibility.Contact__c)){
                    Id userId = contactUserMap.get(caseVisibility.Contact__c);
                    if(caseVisibility.Access_Level__c == 'Read'){
                        caseUserIdsReadAccessMap.put(caseVisibility.Case__c, userId);
                    }else{
                        caseUserIdsEditAccessMap.put(caseVisibility.Case__c, userId);
                    }
                }
                
                
            }
        }
        
    
        
        List<CaseShare> caseShareList = new List<CaseShare>();
        //Edit access
        for(Id caseId : caseUserIdsEditAccessMap.keySet()){
            caseShareList.add(IEM_CaseTriggerHelper.prepareShareObject(caseId,caseUserIdsEditAccessMap.get(caseId),'Edit'));
        }
        //Read access
        IEM_CaseVisibilityTriggerHelper.createCommunitySharingRecords(caseUserIdsReadAccessMap.keySet(),caseUserIdsReadAccessMap);
        /*for(Id caseId : caseUserIdsReadAccessMap.keySet()){
            caseShareList.add(IEM_CaseTriggerHelper.prepareShareObject(caseId,caseUserIdsReadAccessMap.get(caseId),'Read'));
        }*/
        
        System.debug('%%caseShareList'+caseShareList);
        try {
           
            insert caseShareList;
             System.debug('%%Insert'+caseShareList);
        } catch(DmlException e) {
             System.debug('%%no'+caseShareList);
            System.debug('%%The following exception has occurred: ' + e.getMessage());
        }
     }
	
     /*
* Created by - Banshi
* Date - 12/06/2019
* This functon is used to delete caseshare record
*/ 
    public static void deleteShareRecordsForCommunityUsers(List<Case_Visibility__c> oldList){
        System.debug('##oldList'+oldList);
        Map<Id, Id> caseUserIdsAccessMap = new Map<Id, Id>(); //Map to give edit access	
        Map<Id, Id> caseUserIdsReadAccessMap = new Map<Id, Id>(); //Map to give read access
        Map<Id, Set<Id>> caseUserExistingSharingMap = new Map<Id, Set<Id>>(); //Map for old sharing in case of update.
        Set<Id> contactIds = new Set<Id>();
        Map<Id, Id> contactUserMap = new Map<Id, Id>();
        for(Case_Visibility__c caseVisibility : oldList){
            contactIds.add(caseVisibility.Contact__c);
        }
        if(!contactIds.isEmpty()){
            for(User u: [Select Id,ContactId from User where ContactId IN :contactIds AND IsActive= true]){
                contactUserMap.put(u.ContactId, u.Id);
            }
        }
        if(!contactUserMap.isEmpty()){
            for(Case_Visibility__c caseVisibility : oldList){
                if(String.isNotBlank(caseVisibility.Contact__c) && contactUserMap.containsKey(caseVisibility.Contact__c)){
                    Id userId = contactUserMap.get(caseVisibility.Contact__c);
                    caseUserIdsAccessMap.put(caseVisibility.Case__c, userId);
                }
            }
        }
        List<CaseShare> sharingToDelete = new List<CaseShare>();
        System.debug('##caseUserIdsAccessMap'+caseUserIdsAccessMap);
        if(!caseUserIdsAccessMap.isEmpty()){
            //Create Share Records
            for(CaseShare shareRecord : [SELECT CaseAccessLevel,
                                         CaseId,Id,IsDeleted,LastModifiedById,
                                         LastModifiedDate,RowCause,UserOrGroupId 
                                         FROM CaseShare where CaseID IN :caseUserIdsAccessMap.keySet()
                                         AND UserOrGroupId IN :caseUserIdsAccessMap.values()
                                         AND RowCause = 'Manual'] ){
                                             System.debug('##CaseShare'+shareRecord);
                                             if(caseUserIdsAccessMap.containsKey(shareRecord.CaseId) 
                                                && shareRecord.UserOrGroupId == caseUserIdsAccessMap.get(shareRecord.CaseId)){
                                                    sharingToDelete.add(shareRecord);                      
                                                }
                                         }  
            
        }
        try {
             System.debug('##sharingToDelete'+sharingToDelete);
            if(sharingToDelete!=null && !sharingToDelete.isEmpty()){
                delete sharingToDelete;
            }
        } catch(DmlException e) {
            System.debug('The following exception has occurred: ' + e.getMessage());
        }
    }

        @future
        public static void createCommunitySharingRecords(Set<Id> caseIdList,Map<Id,Id> caseUserIdsReadAccessMap) {
            List<CaseShare> caseShareList = new List<CaseShare>();

            for(Id caseId : caseIdList) {
                CaseShare shareRecord = new CaseShare();
                shareRecord.CaseId = caseId;
                shareRecord.UserOrGroupId = caseUserIdsReadAccessMap.get(caseId);
                shareRecord.CaseAccessLevel = 'Read';
                caseShareList.add(shareRecord);
            }
            insert caseShareList;

        } 
}
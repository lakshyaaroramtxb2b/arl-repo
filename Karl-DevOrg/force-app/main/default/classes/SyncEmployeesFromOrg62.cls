/**
 * Retrieves Users(Employees) Information from Org62 and creates Employee__c in the target org
 * @author suresh.uppala
 */
global without sharing class SyncEmployeesFromOrg62 implements Schedulable,
                                                              Database.Batchable<sObject>,
                                                              Database.AllowsCallouts,
                                                              Database.Stateful {

    private static final String HOURLY = '0 0 0/1 1/1 * ? *'; //every hour
    private static final String SOURCE_FILE = 'SyncEmployeesFromOrg62';
    private static final Integer QUERY_LIMIT = 2000;

    private static final List<String> QUERY_FIELDS = new List<String> {'Id','Name','IsActive','Business_Unit__c','CompanyName','JobCode__c',
                                                                       'Cost_Center__c','Email','EmployeeNumber', 'FirstName','LastName','JobGrade__c',
                                                                       'IsManager__c','JobFamily__c','JobProfile__c', 'FederationIdentifier',
                                                                       'Title','WorkLocation__c','ManagerId','Manager.Name','Manager.Title',
                                                                       'ECOMM__c','Phone','lastModifiedDate'};


    private Integer numberOfRecordsForSync = 0,
                    numberOfInsertedEmployees = 0;
                    
    private String fullErrorText;
                                                                 
    public SyncEmployeesFromOrg62() {

    }

    public static String scheduleJob() {
        return scheduleJob(HOURLY);
    }

    public static Id scheduleJob(String cronExpression) {
        return scheduleJob(cronExpression, null);
    }
    
    public static void run() {
        datetime thisTime = system.now().addSeconds(15);
        integer minute = thisTime.minute();
        integer second = thisTime.second();
        integer hour = thisTime.hour();
        integer year = thisTime.year();
        integer month = thisTime.month();
        integer day = thisTime.day();
    
        String cronExpression = second + ' ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ? ' + year; 
        scheduleJob(cronExpression, null); 
         
    }

    public static Id scheduleJob(String cronExpression, String eventName) {
        String jobName = SOURCE_FILE + ' ' + System.now();
        String jobId = System.schedule(jobName, cronExpression, new SyncEmployeesFromOrg62());
        return jobId;
    }

    global void execute(SchedulableContext sc) {
        Database.executeBatch(this);
    }

    global List<sObject> start(Database.BatchableContext context){
        User[] users = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
        return users;
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        try {
                Datetime latestCreatedDate = getLatestEmployeeCreatedDate();
                List<sObject> lstSobject;
                if(Test.isRunningTest()) {
                    lstSobject = getTestSobjectData();
                } else {
                    lstSobject = sfutil.ESASupportForceUtil.fetchUsersFromOrg62(latestCreatedDate,QUERY_FIELDS,QUERY_LIMIT);    
                }
                numberOfRecordsForSync = lstSobject.size();
                List<INTEG_Employee__c> lstEmployees = new List<INTEG_Employee__c>();
                DateTime lastModifiedDateTime = null;

                Map<String,Blocked_Users_For_62Org_Sync__c> blockedUsers = Blocked_Users_For_62Org_Sync__c.getAll();
                Set<Id> alreadyInactiveOrg62Ids = getAlreadyInactiveOrg62Ids(collectIds(lstSobject));


                for(sObject sObj : lstSobject) {
                    User usr = (User) sObj;
                    if(blockedUsers!=null && blockedUsers.containsKey(usr.Email)) {
                        continue;
                    }
                    INTEG_Employee__c employee = new INTEG_Employee__c();
                    employee.Name = usr.Name;
                    // if employee was already marked as inactive in security org, leave them
                    // inactive regardless of what's in org62
                    //if (!alreadyInactiveOrg62Ids.contains(usr.Id)) {
                        //employee.INTEG_Active__c = usr.IsActive;
                   // }
                    //employee.INTEG_Business_Unit__c = usr.Business_Unit__c;
                    // if employee number has something other than only digits or is empty it is probably a contractor
                    if((String.isNotEmpty(usr.EmployeeNumber) && (!usr.EmployeeNumber.trim().isNumeric()))
                       || (String.isEmpty(usr.EmployeeNumber) && usr.JobProfile__c != 'Intern')) {
                        employee.isContractor__c = true;
                        employee.INTEG_Employee_Type__c = 'Contractor'; 
                    }else {
                        employee.isContractor__c = false;
                        If(usr.JobProfile__c == 'Intern') {
                            employee.INTEG_Employee_Type__c = 'Intern';
                        }else {
                            employee.INTEG_Employee_Type__c = 'Perm/FTE';
                        }
                    }
                    employee.INTEG_Cost_Center__c = usr.Cost_Center__c;
                    employee.INTEG_Email__c = usr.Email;
                    employee.INTEG_First_Name__c = usr.FirstName;
                    if(usr.JobGrade__c != null) {
                        employee.INTEG_Grade_Group__c = usr.JobGrade__c.length() == 1? '0'+ usr.JobGrade__c : usr.JobGrade__c;    
                    }
                    employee.INTEG_Is_Manager__c = usr.IsManager__c;
                    employee.INTEG_Job_Family__c = usr.JobFamily__c;
                    employee.INTEG_Job_Profile__c = usr.JobProfile__c;
                    employee.INTEG_Job_Title__c = usr.Title;
                    employee.INTEG_Last_Name__c = usr.LastName;
                    //Org62 Last sync date/time.
                    employee.INTEG_Last_Sync_Date__c     = DateTime.Now();
                    employee.INTEG_Legal_First_Name__c = usr.FirstName;
                    employee.INTEG_Legal_Last_Name__c = usr.LastName;
                    employee.INTEG_Location__c = usr.WorkLocation__c;
                    employee.INTEG_Manager_ID__c = usr.ManagerId;
                    employee.INTEG_Manager_Title__c = usr.Manager.Title;
                    employee.INTEG_Phone__c = usr.Phone;

                    //update org62 user LastModifed date.
                    employee.INTEG_Org62UserLastModifiedDate__c = usr.lastModifiedDate;
                    employee.INTEG_Org62UserId__c = usr.Id;
                    employee.Org62_User__c = usr.Id;
                    employee.INTEG_Title__c = usr.Title;
                    employee.ECOMM__c = usr.ECOMM__c;
                    employee.Federation_Identifier__c = usr.FederationIdentifier;
                    employee.JobCode__c = usr.JobCode__c;
                    lstEmployees.add(employee);
                    lastModifiedDateTime = usr.lastModifiedDate;
                } 

                Schema.SObjectField extFld = INTEG_Employee__c.INTEG_Org62UserId__c;
                Database.UpsertResult[] upsertResults = Database.upsert(lstEmployees,extFld,false);
                
                //update recent lastmodified date to custom setting.
                IntegrationLastSyncDate__c syncDate = IntegrationLastSyncDate__c.getValues('SyncEmployeesFromOrg62');
                syncDate.Org62LastSyncDate__c = lastModifiedDateTime;
                update syncDate;
                
                String finalErrorText = '';
                for (Database.UpsertResult sr : upsertResults) 
                {
                    if (sr.isSuccess()) 
                    {
                        numberOfInsertedEmployees++;
                    } else {
                        for(Database.Error err : sr.getErrors()) {
                            finalErrorText += err.getStatusCode() + ': ' + err.getMessage() + '\n';
                        }
                    }
                }
                Esa_DebugService.writeMessage('SyncEmployeesFromOrg62 Completed:' + getInfoText());
                if(string.isNotBlank(finalErrorText)) {
                    Esa_DebugService.writeMessage('SyncEmployeesFromOrg62.finish: Failed with errors');
                    Exception ex;
                    Esa_DebugService.writeException(ex, 1, SOURCE_FILE, finalErrorText);
                }
                
            }catch(Exception exc) {
            processException(exc);
        }
    }

    private Set<Id> collectIds(List<SObject> input) {
        return (new Map<Id, SObject>(input)).keySet();
    }

    private Set<Id> getAlreadyInactiveOrg62Ids(Set<Id> org62UserIds) {
        Set<Id> alreadyInactiveOrg62Ids = new Set<Id>();
        for (INTEG_Employee__c integEmp : [
            SELECT INTEG_Org62UserId__c
            FROM INTEG_Employee__c
            WHERE INTEG_Active__c = false
                AND INTEG_Org62UserId__c != null
                AND INTEG_Org62UserId__c IN :org62UserIds
        ]) {
            alreadyInactiveOrg62Ids.add(integEmp.INTEG_Org62UserId__c);
        }
        return alreadyInactiveOrg62Ids;
    }

    private String formatManager(String managerName, String managerNumber) {
        string manager = '';
        if (String.isNotBlank(managerName)) {
            manager += managerName;
            if (String.isNotBlank(managerNumber)) {
                manager += ' (' + managerNumber + ')';
            }
        }
        return manager;   
    }

    @TestVisible
    private List<Sobject> getTestSobjectData() {
        List<sObject> lstSobj = new List<sObject>();
        User U;
        U = new User(FirstName='fname1',lastName='lastName2');
        
        lstSobj.add((sObject)U);
        return lstSobj;
    }

    @TestVisible
    private Datetime getLatestEmployeeCreatedDate() {
    
        IntegrationLastSyncDate__c syncDate = IntegrationLastSyncDate__c.getValues('SyncEmployeesFromOrg62');
        
        if(syncDate == null || syncDate.Org62LastSyncDate__c == null) {
            List<INTEG_Employee__c> employee = [SELECT INTEG_Org62UserLastModifiedDate__c
                                                FROM INTEG_Employee__c
                                                WHERE INTEG_Org62UserLastModifiedDate__c != NULL
                                                ORDER BY INTEG_Org62UserLastModifiedDate__c DESC NULLS LAST
                                                LIMIT 1];
            if(employee.isEmpty()) {
                return Datetime.newInstance(1970, 1, 1, 0, 0, 0); //only executed first time we run batch job
            } else {
                return employee[0].INTEG_Org62UserLastModifiedDate__c;
            }
        } else {
            return syncDate.Org62LastSyncDate__c;
        }
       
    }
    
    global void finish(Database.BatchableContext BC){
        //email the given address that the job is complete
        List<String> toAddresses = new List<String>();
        for(EmployeeSyncNotificationEmailIds__c emailIds:EmployeeSyncNotificationEmailIds__c.getAll().values()) {
            toAddresses.add(emailIds.EmailId__c);
        }

        String body = 'Sync employees from 62 org complete.\n\n';
        if(String.isNotBlank(fullErrorText)) {
            body += 'No records inserted in target org. Insert failed with following error(s).' +
                    ' Please check the Events Debug Logs for more info:\n\n--------\n' + fullErrorText + '\n--------';
        } else {
            body += 'Number of records retrieved from Support Force org: ' + numberOfRecordsForSync;
            body += getInfoText();
        }

        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(toAddresses);
        mail.setReplyTo('noreply@salesforce.com');
        mail.setSubject('Email for Employee Sync from 62Org Org');
        mail.setPlainTextBody(body);
        if(!Test.isRunningTest()) {
            //Send email only for errors.
            if(String.isNotBlank(fullErrorText)) {
                Messaging.sendEmail(new Messaging.SingleEmailMessage[]{mail});
            }
        }
    }

    private String getInfoText() {
        return ('\nNumber of Employees Inserted/Updated in SecurityOrg : ' + numberOfInsertedEmployees);
    }

    private void processException(Exception exc) {
        Esa_DebugService.writeMessage('SyncEmployeesFromOrg62.finish: Failed with errors');
        if (exc instanceof DMLException) {
            String errorString = '';
            Integer numErrors = exc.getNumDml();
            fullErrorText = 'There were ' + numErrors + ' DML errors: \n' +
                                   errorString + '\n';
            Esa_DebugService.writeException(exc, 1, SOURCE_FILE, fullErrorText);
        } else {
            //we got an unexpected non-dmlexception, write it
            fullErrorText = exc.getMessage();
            Esa_DebugService.writeException(exc, 1, SOURCE_FILE, 'Unhandled exception');
        }
    }                                                         
}
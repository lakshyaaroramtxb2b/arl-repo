/**
 * @author Lakshya Arora
 * @email lakshya.arora@mtxb2b.com
 * @description Handle Docusign Go button functionalities
 */
public with sharing class KARL_DocuSignGoController {
    /**
    * @author Lakshya Arora
    * @email lakshya.arora@mtxb2b.com
    * @description Send envelope to signatories
    * param1 cycleAuditReportId -> Cycle audit report Id
    */
    @AuraEnabled
    public static void sendDocusignDocuments(Id cycleAuditReportId){
        List<KARL_Audit_Finalization_Signatories__c> signatoriesList = new List<KARL_Audit_Finalization_Signatories__c>();
        Set<Id> finalizationConfigIdSet = new Set<Id>();
        for(KARL_Cycle_Audit_Report_Items__c cycleAuditReportObj : [SELECT Id,
                                                                    (SELECT Id FROM Audit_Report_Finalization_Configurations__r),
                                                                    (SELECT Id,Contact__c,Contact__r.Name,Contact__r.Email FROM Audit_Finalization_Signatories1__r)
                                                                 FROM KARL_Cycle_Audit_Report_Items__c 
                                                                 WHERE Id =: cycleAuditReportId
                                                                 WITH SECURITY_ENFORCED]){
            if(!cycleAuditReportObj.Audit_Finalization_Signatories1__r.isEmpty()){
                signatoriesList.addAll(cycleAuditReportObj.Audit_Finalization_Signatories1__r);
            }
            for(KARL_Audit_Report_Finalization_Config__c arfcObj : cycleAuditReportObj.Audit_Report_Finalization_Configurations__r){
                finalizationConfigIdSet.add(arfcObj.Id);
            }
        }
        if(!finalizationConfigIdSet.isEmpty()){
            Map<String,String> docusignTemplateIdToNameMap = new Map<String,String>();
            for(KARL_Audit_Signature_Templates__c signatureTemplateObj : [SELECT Id,Name,KARL_Template_ID__c 
                                                                            FROM KARL_Audit_Signature_Templates__c
                                                                            WHERE Audit_Report_Finalization_Configuration__c IN:finalizationConfigIdSet
                                                                            WITH SECURITY_ENFORCED] ){
                if(!docusignTemplateIdToNameMap.containsKey(signatureTemplateObj.KARL_Template_ID__c)){
                    docusignTemplateIdToNameMap.put(signatureTemplateObj.KARL_Template_ID__c,signatureTemplateObj.Name);
                }                                                                
            }
            if(!docusignTemplateIdToNameMap.isEmpty()){
                sendEnvelope(cycleAuditReportId,signatoriesList,docusignTemplateIdToNameMap);
                KARL_Cycle_Audit_Report_Items__c cycleAuditReport = new KARL_Cycle_Audit_Report_Items__c(); 
                cycleAuditReport.Id = cycleAuditReportId;
                cycleAuditReport.Status__c = KARL_Constants.REPORT_STATUS_In_Progress_With_Docusign;
                update cycleAuditReport;
            }
        }
        
    }

    /**
    * @author Lakshya Arora
    * @email lakshya.arora@mtxb2b.com
    * @description Send envelope functionality
    * param1 cycleAuditReportId -> Cycle audit report Id
    */
    private static void sendEnvelope(Id cycleAuditReportId,List<KARL_Audit_Finalization_Signatories__c> signatoriesList,Map<String,String> docusignTemplateIdToNameMap){
        Id mySourceId= cycleAuditReportId;
        // Create an empty envelope
        dfsle.Envelope myEnvelope = dfsle.EnvelopeService.getEmptyEnvelope(new dfsle.Entity(mySourceId));
        List<dfsle.UUID> docusignTemplateList = new List<dfsle.UUID>();
        List<dfsle.Recipient> envelopeRecipients = new List<dfsle.Recipient>();
        Map<dfsle.UUID,String> templateIdToNameMap = new Map<dfsle.UUID,String>();
        
        //add documents
        for(String templateId : docusignTemplateIdToNameMap.keySet()){
            templateIdToNameMap.put(dfsle.UUID.parse(templateId),docusignTemplateIdToNameMap.get(templateId));
        }
        List<dfsle.Document> documentList = new List<dfsle.Document>();
        for(dfsle.UUID templateId : templateIdToNameMap.keySet()){
            documentList.add(dfsle.Document.fromTemplate(
                templateId, 
                templateIdToNameMap.get(templateId)));
        }
        myEnvelope = myEnvelope.withDocuments(documentList);

        // add recipients
        for(KARL_Audit_Finalization_Signatories__c signatoryObj : signatoriesList){
            envelopeRecipients.add(dfsle.Recipient.fromSource(signatoryObj.Contact__r.Name, signatoryObj.Contact__r.Email, null, 'Need To Sign', new dfsle.Entity(mySourceId)));
        }
        myEnvelope = myEnvelope.withRecipients(envelopeRecipients);
        
        if(!Test.isRunningTest()){
            // Send the envelope
            myEnvelope = dfsle.EnvelopeService.sendEnvelope(
                myEnvelope, // The envelope to send
                true); // Send now?
        }
       
    }

}
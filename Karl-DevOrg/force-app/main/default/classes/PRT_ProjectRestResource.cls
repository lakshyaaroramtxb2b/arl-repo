/**
 * @File Name          : PRT_ProjectRestResource.cls
 * @Description        : 
 * @Author             : virendra.yadav 
 * @Group              : 
 * @Last Modified By   : virendra.yadav 
 * @Last Modified On   : 1/3/2020, 2:34:28 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    1/3/2020   virendra.yadav      Initial Version
**/
@RestResource(urlMapping='/ProjectValidation/*')
global with sharing class PRT_ProjectRestResource {
    
    @TestVisible
    private static list<sObject> mockallV2MOMList = new list<sObject>();
    @TestVisible
    private static list<sObject> mockAllMeasureList = new list<sObject>();
    @TestVisible
    private static list<sObject> mockAllMethodList = new list<sObject>();
    
    @HttpGet
    global static String doGet() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        String projectId = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
        String errorMessage = '';
        Project__c projectObject;
        try {
            projectObject =  [SELECT Name, Id, V2MOM_Method__c, V2MOM_Measure__c FROM Project__c WHERE Id =: projectId WITH SECURITY_ENFORCED];    
        } catch (Exception e) {
                return e.getMessage(); 
        }
        if(projectObject!=null && projectObject.V2MOM_Method__c != null || projectObject.V2MOM_Measure__c != null) {
            errorMessage = checkV2MOMProjectManagerValidation(projectObject);
        } else {
            errorMessage = 'V2MOM_Method__c/V2MOM_Measure__c is required to project';
        }
        return errorMessage;
    }

    public static String checkV2MOMProjectManagerValidation(Project__c projectObject) {
        String v2momId = '';
        String message = '';
        if(projectObject.V2MOM_Measure__c != null) {
            List<Measure_c__x> measureObjectList = new List<Measure_c__x>();
            try {
                if(!test.isRunningTest()){
                	measureObjectList = [SELECT Id, V2MOM_c__c, Method_c__c FROM Measure_c__x WHERE ExternalId =: projectObject.V2MOM_Measure__c WITH SECURITY_ENFORCED];
                }else{
                    measureObjectList = mockAllMeasureList;
                }
            } catch (Exception e) {
                return e.getMessage(); 
            }
            if(!measureObjectList.isEmpty()) {
                v2momId = measureObjectList[0].V2MOM_c__c;
            }
        } else {
            List<V2MOM_Method_c__x> methodObjectList = new List<V2MOM_Method_C__X>();
            try {
                if(!Test.isRunningTest())
                	methodObjectList = [SELECT Id, ExternalId, V2MOM_c__c, Name__c FROM V2MOM_Method_c__x WHERE ExternalId =: projectObject.V2MOM_Method__c WITH SECURITY_ENFORCED];
                else
                    methodObjectList = mockAllMethodList;
            } catch (Exception e) {
                return e.getMessage();                
            }
            if(!methodObjectList.isEmpty()) {
                v2momId = methodObjectList[0].V2MOM_c__c;
            }
        }
        List<V2MOM_c__x> v2momObjectList = new List<V2MOM_c__x>();
        if(v2momId!='' && v2momId!=null){
            try {
                if(!Test.isRunningTest()){
                	v2momObjectList = [SELECT V2MOM_User_c__c, User_Profile_c__c, Status_c__c, OwnerId__c, Name__c, Id, ExternalId FROM V2MOM_c__x WHERE Id =: v2momId WITH SECURITY_ENFORCED ];
                }else{
                    v2momObjectList = mockallV2MOMList;
                }
            } catch (Exception e) {
                return e.getMessage(); 
            }
        }
        if(!v2momObjectList.isEmpty() && v2momObjectList[0].V2MOM_User_c__c == null) {
            message = 'V2MOM is not assigned with any user.';
        }
        return message;
    }
}
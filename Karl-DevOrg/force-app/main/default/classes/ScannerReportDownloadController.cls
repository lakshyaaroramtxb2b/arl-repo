public without sharing class ScannerReportDownloadController {
public string bodyresponse {get;set;}
public ScannerReportDownloadController()
{
	//init();
}

public PageReference init()
{
	string AttachId='';
	AttachId=ApexPages.currentPage().getParameters().get('id');
	string csrftokenFromGet='';
	csrftokenFromGet=ApexPages.currentPage().getParameters().get('csrf');//EncodingUtil.urlDecode(ApexPages.currentPage().getParameters().get('csrf'),'UTF-8');
	system.debug('csrftokenFromGet:'+csrftokenFromGet);
	string csrftokenGenerated=CloudScannerMainController.genCSRF(AttachId);
    string csrfpackage =PackageScannerMainController.genCSRF(AttachId);
	//bodyresponse=csrftokenFromGet+':'+csrftokenGenerated;
	if(csrftokenGenerated.equals(csrftokenFromGet) || csrfpackage.equals(csrftokenFromGet) )
		{
			
		
	Attachment att=null;
	for(Attachment at:[select id,Name,body,parentid from Attachment where id = :AttachId])
    {
        att=at;
    }
    if(att==null)
    {
    	bodyresponse='OOPS Attachment not found!';
    	return null;
    }
    else
    {
    	Boolean AttachmentAlreadyCreated=false;
    	WrapperAttachment__c Wa=new WrapperAttachment__c();
    	for(WrapperAttachment__c waitem:[select id from WrapperAttachment__c where Original_Attachment_Id__c = :att.id and OwnerId = :Userinfo.getUserId() ])
    	{
    		Wa=waitem;
    		AttachmentAlreadyCreated=true;
    	}
    	if(AttachmentAlreadyCreated==false)
    	{
    		Wa.Original_Attachment_Id__c=att.id;
    		insert Wa;
    		Attachment newAtt=new Attachment();
    		newAtt.Name=att.Name;
    		newAtt.parentid=Wa.id;
    		newAtt.body=att.body;
    		insert newAtt;
    		
    		Return new PageReference('/servlet/servlet.FileDownload?file='+newAtt.id);
    	}
    	else
    	{
    		Id returnAtt=null;
    		for(Attachment waitem:[select id from Attachment where parentid=:Wa.id])
    			returnAtt=waitem.id;
    		if(returnAtt!=null)
    			Return new PageReference('/servlet/servlet.FileDownload?file='+returnAtt);
    		bodyresponse='File not found.';
    		return null;
    	}
    }
    bodyresponse='File not found.';
    return null;
        }
    else
    {
        bodyresponse='CSRF token mismatch';
    		return null;
        
    }
}
}
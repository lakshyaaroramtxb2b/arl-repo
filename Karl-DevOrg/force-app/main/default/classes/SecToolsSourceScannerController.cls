global class SecToolsSourceScannerController {
   
    public static string CAPTCHA_API = 'https://www.google.com/recaptcha/api/siteverify';  //recaptcha v2
    private static string CAPTCHA_SECRET = '6LfDUCIUAAAAAEyhg4dh70R8WITygUhKlH_lid6R';
    public static integer MAX_QUEUE_BACKLOG = 600;
    public static string OK='OK';
    public static string FAILURE='NOT OK:';    
    public static Boolean IsDelayed {get;set;}
    public static final String DELAYED_MESSAGE {get;set;}
    
    public static Integer MIN_SCAN_WAIT_HOURS = 1;
    public static string allowed_hostname = URL.getSalesforceBaseUrl().getHost();
    
    public String statusMessage { get; private set; }
    
    static {
        // Set this to manually put the delay warning up. A queue size check can also set this dynamically
        IsDelayed = False;
        DELAYED_MESSAGE = 'At this time the Force.com Security Source Code Scanner is experiencing delays.  Expect delays as we work through this issue.';
    }
    
    public SecToolsSourceScannerController() {
        
        
        Integer queueLen = [SELECT count() FROM Scan_Queue__c WHERE Status__c = 'Waiting to scan' AND Job_Type__c = 'Portal'];
        if (queueLen > MAX_QUEUE_BACKLOG) {
            IsDelayed = True;
        }
        
        try {
            List<Popcrab_cportal_status__c> statusMsgs = [SELECT message__c 
                                                            FROM Popcrab_cportal_status__c 
                                                            WHERE 
                                                                ExpiryDateTime__c >: System.now() AND 
                                                                publish__c=true 
                                                            ORDER BY CreatedDate desc LIMIT 1];
            if (statusMsgs != null && !statusMsgs.isEmpty()) {
                this.statusMessage = formatMessage(statusMsgs[0].message__c);                           
            } else {
                this.statusMessage = '';
            }
        } catch(Exception e) {
                System.debug(e.getMessage());
                this.statusMessage='';                
        }  
    }
    
    public static String getCaptchaResult(String msg) {
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(CAPTCHA_API);
        

        String payload = 'secret=' +CAPTCHA_SECRET + '&response=' + EncodingUtil.urlEncode(msg,'UTF-8');
        req.setMethod('POST');
        req.setHeader('Content-Type','application/x-www-form-urlencoded');
        req.setBody(payload);
        CaptchaResponse cr;
        
        try {
            httpResponse resp = h.send(req);
            
            if(resp.getStatusCode()== 200) {
                JSONParser parser = JSON.createParser(resp.getBody());
                cr = (CaptchaResponse)parser.readValueAs(CaptchaResponse.class);
                if (cr.success==true && cr.hostname == allowed_hostname) {
                    return OK;
                } else {
                    return FAILURE + ' could not verify ' + resp.getBody();
                }
                
            } else {
                return FAILURE + 'Received status code: ' + String.valueOf(resp.getStatusCode());
            }
            
        } catch(Exception e) {
            return FAILURE + ' exception: ' + e.getMessage();
        }
    }
    
    @RemoteAction
    global static Map<Boolean,String> scheduleScan(String username,String friendlyName, String scantype, String challenge) {
        if( String.isEmpty(challenge) ) {
            return new Map<Boolean,String>{False=>'A recaptcha challenge was not supplied'};
        }
        if(username == null)
        {
            return new Map<Boolean,String>{False=>'The username submitted is not a valid salesforce.com username'};
        }
        String captchaResult = getCaptchaResult(challenge);
        
        if (captchaResult != OK) {
             System.debug('Captcha result: ' + captchaResult);
             return new Map<Boolean,String>{False=>'Failed to validate captcha: ' + captchaResult};
        }
        
        Pattern validEmail = Pattern.compile('^.+@.+$');
        Pattern whiteSpace = Pattern.compile('^\\s*$');
        Matcher emailMatcher = validEmail.matcher(username);
        if ((username == null) || 
            (validEmail.matcher(username).matches() == False) || (whiteSpace.matcher(username).matches()==True)){
            return new Map<Boolean,String>{False=>'The username submitted is not a valid salesforce.com username'};
        }
        
        // Enforce throttling and only allow scans every MIN_SCAN_WAIT_HOURS
        Datetime newest_allowed = Datetime.now().addHours(0-MIN_SCAN_WAIT_HOURS);
        List<CodeScan__c> latest = [SELECT Id,Created__c FROM CodeScan__c WHERE (Created__c != null) 
                                           AND (Created__c >:newest_allowed) 
                                           AND (Username__c =: username)
                                           LIMIT 1];
                                           
        if (latest.size() > 0){
            // They have one pending or completed already
            // Might want to give nextAllowed in user's timezone...
            String nextAllowed = latest.get(0).Created__c.addHours(MIN_SCAN_WAIT_HOURS).format('MMM d, h:m a');
            String message = 'Due to high volume, the number of scans for each username is limited.' ;
            message += ' Please submit the application again after '+nextAllowed+'.  We apologize for any inconvenience.';
            return new Map<Boolean,String>{False=>message};
        }
        
        
        CodeScan__c scan = new CodeScan__c();
        scan.Username__c = username;
        scan.ScanType__c = scantype;
        scan.Name = friendlyName;
        insert scan;
       
        return new Map<Boolean,String>{True=>'Scan scheduled succesfully'};
    }

    public class CaptchaResponse {
            /* sample response
            "success": true|false,
            "challenge_ts": timestamp,  // timestamp 
            "hostname": string,         // the hostname 
            "error-codes": [...]        // optional
            */
        public boolean success;
        public Datetime challenge_ts;
        public string hostname;
        
    }
    
    private String formatMessage(string msg) {
        return '<div class="alert alert-warning">' +
                   '<p>Scanner News:</p>' + 
                   '<p>'+ system.today().format() + ' - ' + msg + '</p>' +
                  '</div>';     
    }
    
}
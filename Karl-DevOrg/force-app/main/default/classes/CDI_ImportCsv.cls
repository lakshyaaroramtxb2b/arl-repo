public class CDI_ImportCsv {
    public Blob csvFileBody{get;set;}
    public string csvAsString{get;set;}
    public String[] csvFileLines{get;set;}
    public List<CDI_Data_Asset__c> dataassets{get;set;}
    public String shouldTest {get; set;}
    public Set<CDI_Data_Asset__c> insertparentdataassets{get;set;}
    private static final Integer Data_Asset_Name = 0;
    private static final Integer Data_Asset_Description = 1;
    private static final Integer Data_Asset_Type = 2;
    private static final Integer Parent_Data_Asset = 3;
    private static final Integer Data_Retention_Period =4;
    private static final Integer Source_System_Name = 5;
    private static final Integer Source_Data_Asset_Code =6;
    private static final Integer Link_To_Balancing_Argument_Document =7;
    private static final Integer Link_To_Excepn_For_Basis_For_Processing =8;
    private static final Integer IsEncrypted = 9;
    private static final Integer Is_Personal_Data_Limited_To_Purpose = 10;
    private static final Integer Has_DPIA_Been_Performed = 11;
    private static final Integer Can_Consumers_Access_Data = 12;
    private static final Integer Crypto_Algorithm = 13;
    private static final Integer Data_Store = 14;
    private static final Integer Data_Classification = 15;
    private static final Integer Data_Category = 16;
    private static final Integer Data_Identification_Category = 17;
    private static final Integer Basis_For_Processing = 18;
    private static final Integer Time_Unit = 19;
    private static final Integer Retention_Process = 20;
    private static final Integer Customer_Data_Category = 21;
    private static final Integer GUS_Team = 22;
    private static final Integer IsDeleted = 23;
    private static final Integer Comments = 24;
    public CDI_ImportCsv(){
        csvFileLines = new String[]{};
            }
    
    public PageReference importCSVFile(){
        DateTime startDatetime=DateTime.now();
        String errors=''; 
        Integer rowsFailed=0;
        Integer rowsSucceeded=0;
        try
        {
            Integer successRowCount=0;
            Integer errorRowCount=0;
            csvAsString = csvFileBody.toString();
            csvAsString = csvAsString.replace('"','');
            csvAsString = String.escapeSingleQuotes(csvAsString);
            csvFileLines = csvAsString.split('\n');
            dataassets = new List<CDI_Data_Asset__c>();
            insertparentdataassets=new Set<CDI_Data_Asset__c>();
            List<String> externalids=new List<String>();
            
            Map<String, Boolean> parentdataassetmap=new Map<String, Boolean>();
            List<CDI_Data_Asset__c> parentdataassets=[SELECT Name,IsDeleted__c,Data_Store__r.Name FROM CDI_Data_Asset__c where Data_Store__r.Id=:ApexPages.CurrentPage().getparameters().get('id') AND (Data_Asset_Type__c=:'Table' OR Data_Asset_Type__c=:'File')];
            
            //Map Data Asset Name with isDeleted Field to Check, if the Data Asset is Deleted 
            for(CDI_Data_Asset__c dataasset:parentdataassets)
            {
                parentdataassetmap.put(dataasset.Data_Store__r.Name+'/'+dataasset.Name,dataasset.IsDeleted__c);
            }
            
            Map<String, Boolean> cryptoalgorithmmap=new Map<String, Boolean>();
            List<CDI_Crypto_Algorithm__c> cryptoalgorithms=[SELECT Name,IsDeleted__c FROM CDI_Crypto_Algorithm__c];
            
            //Map Crypto Algorithm Name with isDeleted Field to Check, if the Crypto Algorithm is Deleted
            for(CDI_Crypto_Algorithm__c cryptoalgorithm:cryptoalgorithms)
            {
                cryptoalgorithmmap.put(cryptoalgorithm.Name,cryptoalgorithm.IsDeleted__c);
            }
            
            Map<String, Boolean> datastoremap=new Map<String, Boolean>();
            List<CDI_Data_Store__c> datastores=[SELECT Id,Name,IsDeleted__c FROM CDI_Data_Store__c Where Id=:ApexPages.CurrentPage().getparameters().get('id')];
            
            //Map Data Store Name with isDeleted Field to Check, if the Data Store is Deleted
            for(CDI_Data_Store__c datastore:datastores)
            {
                datastoremap.put(datastore.Name,datastore.IsDeleted__c);
            }
            
            Map<String, Boolean> dataclassificationmap=new Map<String, Boolean>();
            List<CDI_Data_Classification__c> dataclassifications=[SELECT Name,IsDeleted__c FROM CDI_Data_Classification__c];
            
            //Map Data Classification Name with isDeleted Field to Check, if the Data Classification is Deleted
            for(CDI_Data_Classification__c dataclassification:dataclassifications)
            {
                dataclassificationmap.put(dataclassification.Name,dataclassification.IsDeleted__c);
            }
            
            Map<String, Boolean> datacategorymap=new Map<String, Boolean>();
            List<CDI_Data_Category__c> datacategories=[SELECT Name,IsDeleted__c FROM CDI_Data_Category__c];
            
            //Map Data Category Name with isDeleted Field to Check, if the Data Category is Deleted
            for(CDI_Data_Category__c datacategory:datacategories)
            {
                datacategorymap.put(datacategory.Name,datacategory.IsDeleted__c);
            }
            
            Map<String, Boolean> dataidentificationcategorymap=new Map<String, Boolean>();
            List<CDI_Data_Identification_Category__c> dataidentificationcategories=[SELECT Name,IsDeleted__c FROM CDI_Data_Identification_Category__c];
            
            //Map Data Identification Category Name with isDeleted Field to Check, if the Data Identification Category is Deleted
            for(CDI_Data_Identification_Category__c dataidentificationcategory:dataidentificationcategories)
            {
                dataidentificationcategorymap.put(dataidentificationcategory.Name,dataidentificationcategory.IsDeleted__c);
            }            
            
            Map<String, Boolean> basisforprocessingmap=new Map<String, Boolean>();
            List<CDI_Basis_For_Processing__c> basisforprocessings=[SELECT Name,IsDeleted__c FROM CDI_Basis_For_Processing__c];
            
            //Map Basis For Processing Name with isDeleted Field to Check, if the Basis For Processing is Deleted
            for(CDI_Basis_For_Processing__c basisforprocessing:basisforprocessings)
            {
                basisforprocessingmap.put(basisforprocessing.Name,basisforprocessing.IsDeleted__c);
            }
            
            Map<String, Boolean> timeunitmap=new Map<String, Boolean>();
            List<CDI_Time_Unit__c> timeunits=[SELECT Name,IsDeleted__c FROM CDI_Time_Unit__c];
            
            //Map Time Unit Name with isDeleted Field to Check, if the Time Unit is Deleted
            for(CDI_Time_Unit__c timeunit:timeunits)
            {
                timeunitmap.put(timeunit.Name,timeunit.IsDeleted__c);
            }
            
            Map<String, Boolean> retentionprocessmap=new Map<String, Boolean>();
            List<CDI_Retention_Process__c> retentionprocesses=[SELECT Name,IsDeleted__c FROM CDI_Retention_Process__c];
            
            //Map Retention Process Name with isDeleted Field to Check, if the Retention Process is Deleted
            for(CDI_Retention_Process__c retentionprocess:retentionprocesses)
            {
                retentionprocessmap.put(retentionprocess.Name,retentionprocess.IsDeleted__c);
            }
            
            Map<String, Boolean> customerdatacategorymap=new Map<String, Boolean>();
            List<CDI_Customer_Data_Category__c> customerdatacategories=[SELECT Name,IsDeleted__c FROM CDI_Customer_Data_Category__c];
            
            //Map Customer Data Category Name with isDeleted Field to Check, if the Customer Data Category is Deleted
            for(CDI_Customer_Data_Category__c customerdatacategory:customerdatacategories)
            {
                customerdatacategorymap.put(customerdatacategory.Name,customerdatacategory.IsDeleted__c);
            }
            
            Map<String, Boolean> gusteammap=new Map<String, Boolean>();
            List<CDI_GUS_Team__c> gusteams=[SELECT Name,IsDeleted__c FROM CDI_GUS_Team__c];
            
            //Map GUS Team Name with isDeleted Field to Check, if the GUS Team is Deleted
            for(CDI_GUS_Team__c gusteam:gusteams)
            {
                gusteammap.put(gusteam.Name,gusteam.IsDeleted__c);
            }
            
            //Iterate over CSV records
            for(Integer i=1;i<csvFileLines.size();i++)
            {   
                String[] csvRecordData = csvFileLines[i].split(',');
                CDI_Data_Asset__c dataasset =new CDI_Data_Asset__c();
                CDI_Data_Asset__c insertparentdataasset =new CDI_Data_Asset__c();
                try
                {
                    //Check if Data Store record is Empty
                    if(!String.isEmpty(csvRecordData[Data_Store].trim()))
                    {
                        //Check if Data Store record is Deleted
                        if(datastoremap.get(csvRecordData[Data_Store].trim()) == FALSE)
                        {
                            dataasset.Data_Store__r=new CDI_Data_Store__c(External_Id__c=csvRecordData[Data_Store].trim());
                        }
                        
                        else if(datastoremap.get(csvRecordData[Data_Store].trim()) == TRUE)
                        {
                            throw new CDI_CustomException('The value provided for "Data Store" in row number '+(i+1)+' is invalid or does not exist');
                        }
                        
                        //If Data Store Name not present in the Map, throw an Exception
                        else
                        {
                            throw new CDI_CustomException('The value provided for "Data Store" in row number '+(i+1)+' "'+csvRecordData[Data_Store].trim()+'" does not match with the Data Store where import operation is being performed' );
                        }                        
                    }
                    
                    //If Parent Data Asset is Not Empty, the Data Asset Type is Table or File
                    if(!String.isEmpty(csvRecordData[Data_Store].trim()) && !String.isEmpty(csvRecordData[Parent_Data_Asset].trim()))
                    {
                        //Check if Duplicate Data Asset exists
                        if(!externalids.contains(csvRecordData[Data_Store].trim()+'/'+csvRecordData[Parent_Data_Asset].trim()+'/'+csvRecordData[Data_Asset_Name].trim()))
                        {
                            externalids.add(csvRecordData[Data_Store].trim()+'/'+csvRecordData[Parent_Data_Asset].trim()+'/'+csvRecordData[Data_Asset_Name].trim());
                            dataasset.External_Id__c=csvRecordData[Data_Store].trim()+'/'+csvRecordData[Parent_Data_Asset].trim()+'/'+csvRecordData[Data_Asset_Name].trim();
                        }
                        
                        // If Duplicate Data Asset Exists throw an Exception
                        else
                        {
                            throw new CDI_CustomException('Duplicate "Data Asset" value "'+csvRecordData[Data_Asset_Name].trim()+'" found in row number '+(i+1));
                        }
                        
                        //If Parent Data Asset doesn't exist add it in the map as a new one
                        if(!parentdataassetmap.containsKey(csvRecordData[Data_Store].trim()+'/'+csvRecordData[Parent_Data_Asset].trim()))
                        {
                            parentdataassetmap.put(csvRecordData[Data_Store].trim()+'/'+csvRecordData[Parent_Data_Asset].trim(),FALSE);
                        }
                        
                        insertparentdataasset.Name=csvRecordData[Parent_Data_Asset].trim();
                        insertparentdataasset.External_Id__c=csvRecordData[Data_Store].trim()+'/'+csvRecordData[Parent_Data_Asset].trim();
                        insertparentdataasset.Data_Store__r=new CDI_Data_Store__c(External_Id__c=csvRecordData[Data_Store].trim());                        
                        
                        //If child Data Asset Type is Field, parent Data Asset Type is File
                        if(csvRecordData[Data_Asset_Type].trim() =='Field')
                        {
                            insertparentdataasset.Data_Asset_Type__c='File';
                        }
                        
                        //If child Data Asset Type is Column, parent Data Asset Type is Table
                        else if(csvRecordData[Data_Asset_Type].trim() =='Column')
                        {
                            insertparentdataasset.Data_Asset_Type__c='Table';
                        }
                        
                        //If child Data Asset Type is not Field or Column, throw an Exception
                        else
                        {
                            throw new CDI_CustomException('"Data Asset Type" value "'+csvRecordData[Data_Asset_Type].trim()+'" found in row number '+(i+1)+' is not valid value.');
                        }
                    }
                    
                    //If Parent Data Asset is Empty, the Data Asset Type is Column or Field
                    else if(!String.isEmpty(csvRecordData[Data_Store].trim()) && String.isEmpty(csvRecordData[Parent_Data_Asset].trim()))
                    {
                        if(!externalids.contains(csvRecordData[Data_Store].trim()+'/'+csvRecordData[Data_Asset_Name].trim()))
                        {
                            externalids.add(csvRecordData[Data_Store].trim()+'/'+csvRecordData[Data_Asset_Name].trim());
                            dataasset.External_Id__c=csvRecordData[Data_Store].trim()+'/'+csvRecordData[Data_Asset_Name].trim();
                        }
                        else
                        {
                            throw new CDI_CustomException('Duplicate "Data Asset" value "'+csvRecordData[Data_Asset_Name].trim()+'" found in row number '+(i+1));
                        }
                    }
                    
                    //Add parent Data Assets to List
                    insertparentdataassets.add(insertparentdataasset);
                    
                    dataasset.Name=csvRecordData[Data_Asset_Name].trim();
                    
                    dataasset.Data_Asset_Description__c=csvRecordData[Data_Asset_Description].trim();
                    
                    dataasset.Data_Asset_Type__c=csvRecordData[Data_Asset_Type].trim();
                    
                    if(!String.isEmpty(csvRecordData[Parent_Data_Asset].trim()))
                    {
                        //Check if Parent Data Asset is Deleted or Not
                        if(parentdataassetmap.get(csvRecordData[Data_Store].trim()+'/'+csvRecordData[Parent_Data_Asset].trim()) == FALSE)
                        {
                            dataasset.Parent_Data_Asset__r=new CDI_Data_Asset__c(External_Id__c=csvRecordData[Data_Store].trim()+'/'+csvRecordData[Parent_Data_Asset].trim());
                        }
                        //If Parent Data Asset is Deleted, throw an Exception
                        else if(parentdataassetmap.get(csvRecordData[Data_Store].trim()+'/'+csvRecordData[Parent_Data_Asset].trim()) == TRUE)
                        {
                            throw new CDI_CustomException('The value provided for "Parent Data Asset" in row number '+(i+1)+' does not exist');
                        }
                    }
                    
                    if(!String.isEmpty(csvRecordData[Data_Retention_Period].trim()))
                    {    
                        //Validate if the value entered in Data Retention Period is a valid number or not
                        try
                        {
                            dataasset.Data_Retention_Period__c=Integer.valueOf(csvRecordData[Data_Retention_Period].trim());
                        }
                        catch(Exception e)
                        {
                            throw new CDI_CustomException('The value provided for "Data Retention Period" in row number '+(i+1)+' is invalid');
                        }
                    }
                    
                    dataasset.Source_System_Name__c=csvRecordData[Source_System_Name].trim();
                    
                    dataasset.Source_Data_Asset_Code__c=csvRecordData[Source_Data_Asset_Code].trim();
                    
                    dataasset.Link_To_Balancing_Argument_Document__c=csvRecordData[Link_To_Balancing_Argument_Document].trim();
                    
                    dataasset.Link_To_Excepn_For_Basis_For_Processing__c=csvRecordData[Link_To_Excepn_For_Basis_For_Processing].trim();
                    
                    //Check if value entered is Boolean Value
                    if(csvRecordData[IsEncrypted].trim().equalsIgnoreCase('TRUE')||csvRecordData[IsEncrypted].trim().equalsIgnoreCase('FALSE'))
                    {
                        dataasset.IsEncrypted__c=Boolean.valueOf(csvRecordData[IsEncrypted].trim());
                    }
                    else
                    {
                        throw new CDI_CustomException('The value provided for "IsEncrypted" in row number '+(i+1)+' is invalid.');   
                    }
                    
                    //Check if value entered is Boolean Value
                    if(csvRecordData[Is_Personal_Data_Limited_To_Purpose].trim().equalsIgnoreCase('TRUE')||csvRecordData[Is_Personal_Data_Limited_To_Purpose].trim().equalsIgnoreCase('FALSE'))
                    {
                        dataasset.Is_Personal_Data_Limited_To_Purpose__c=Boolean.valueOf(csvRecordData[Is_Personal_Data_Limited_To_Purpose].trim());
                    }
                    else
                    {
                        throw new CDI_CustomException('The value provided for "Is Personal Data Limited To Purpose" in row number '+(i+1)+' is invalid.');   
                    }
                    
                    //Check if value entered is Boolean Value
                    if(csvRecordData[Has_DPIA_Been_Performed].trim().equalsIgnoreCase('TRUE')||csvRecordData[Has_DPIA_Been_Performed].trim().equalsIgnoreCase('FALSE'))
                    {        
                        dataasset.Has_DPIA_Been_Performed__c=Boolean.valueOf(csvRecordData[Has_DPIA_Been_Performed].trim());
                    }
                    else
                    {
                        throw new CDI_CustomException('The value provided for "Has DPIA Been Performed" in row number '+(i+1)+' is invalid.');   
                    }
                    
                    //Check if value entered is Boolean Value
                    if(csvRecordData[Can_Consumers_Access_Data].trim().equalsIgnoreCase('TRUE')||csvRecordData[Can_Consumers_Access_Data].trim().equalsIgnoreCase('FALSE'))
                    {
                        dataasset.Can_Consumers_Access_Data__c=Boolean.valueOf(csvRecordData[Can_Consumers_Access_Data].trim());
                    }
                    else
                    {
                        throw new CDI_CustomException('The value provided for "Can Consumers Access Data" in row number '+(i+1)+' is invalid.');   
                    }
                    
                    if(!String.isEmpty(csvRecordData[Crypto_Algorithm].trim()))
                    {
                        //Check if Crypto Algorithm Value is Deleted
                        if(cryptoalgorithmmap.get(csvRecordData[Crypto_Algorithm].trim()) == FALSE)
                        {
                            dataasset.Crypto_Algorithm__r=new CDI_Crypto_Algorithm__c(External_Id__c=csvRecordData[Crypto_Algorithm].trim()); 
                        }
                        
                        //If Deleted throw an Exception
                        else if(cryptoalgorithmmap.get(csvRecordData[Crypto_Algorithm].trim()) == TRUE)
                        {
                            throw new CDI_CustomException('The value provided for "Crypto Algorithm" in row number '+(i+1)+' is invalid or does not exist');
                        }
                        
                        //If Value is Invalid throw an Exception
                        else
                        {
                            throw new CDI_CustomException('The value provided for "Crypto Algorithm" in row number '+(i+1)+' is invalid or does not exist');
                        }
                    }
                    
                    if(!String.isEmpty(csvRecordData[Data_Classification].trim()))
                    {
                        //Check if Data Classification Value is Deleted
                        if(dataclassificationmap.get(csvRecordData[Data_Classification].trim()) == FALSE)
                        {                        
                            dataasset.Data_Classification__r=new CDI_Data_Classification__c(External_Id__c=csvRecordData[Data_Classification].trim());    
                        }
                        
                        //If Deleted throw an Exception
                        else if(dataclassificationmap.get(csvRecordData[Data_Classification].trim()) == TRUE)
                        {
                            throw new CDI_CustomException('The value provided for "Data Classification" in row number '+(i+1)+' is invalid or does not exist');
                        }
                        
                        //If Value is Invalid throw an Exception
                        else
                        {
                            throw new CDI_CustomException('The value provided for "Data Classification" in row number '+(i+1)+' is invalid or does not exist');
                        }
                    }
                    
                    if(!String.isEmpty(csvRecordData[Data_Category].trim()))
                    {
                        //Check if Data Category Value is Deleted
                        if(datacategorymap.get(csvRecordData[Data_Category].trim()) == FALSE)
                        {                          
                            dataasset.Data_Category__r=new CDI_Data_Category__c(External_Id__c=csvRecordData[Data_Category].trim());  
                        }
                        
                        //If Deleted throw an Exception
                        else if(datacategorymap.get(csvRecordData[Data_Category].trim()) == TRUE)
                        {
                            throw new CDI_CustomException('The value provided for "Data Category" in row number '+(i+1)+' is invalid or does not exist');
                        }
                        
                        //If Value is Invalid throw an Exception
                        else
                        {
                            throw new CDI_CustomException('The value provided for "Data Category" in row number '+(i+1)+' is invalid or does not exist');
                        }
                    }
                    
                    if(!String.isEmpty(csvRecordData[Data_Identification_Category].trim()))
                    {  
                        //Check if Data Identification Category Value is Deleted
                        if(dataidentificationcategorymap.get(csvRecordData[Data_Identification_Category].trim()) == FALSE)
                        {
                            dataasset.Data_Identification_Category__r=new CDI_Data_Identification_Category__c(External_Id__c=csvRecordData[Data_Identification_Category].trim());  
                        }
                        
                        //If Deleted throw an Exception
                        else if(dataidentificationcategorymap.get(csvRecordData[Data_Identification_Category].trim()) == TRUE)
                        {
                            throw new CDI_CustomException('The value provided for "Data Identification Category" in row number '+(i+1)+' is invalid or does not exist');
                        }
                        
                        //If Value is Invalid throw an Exception
                        else
                        {
                            throw new CDI_CustomException('The value provided for "Data Identification Category" in row number '+(i+1)+' is invalid or does not exist');
                        }                        
                    }
                    
                    if(!String.isEmpty(csvRecordData[Basis_For_Processing].trim()))
                    {
                        //Check if Basis For Processing Value is Deleted
                        if(basisforprocessingmap.get(csvRecordData[Basis_For_Processing].trim()) == FALSE)
                        {                        
                            dataasset.Basis_For_Processing__r=new CDI_Basis_For_Processing__c(External_Id__c=csvRecordData[Basis_For_Processing].trim());
                        }
                        
                        //If Deleted throw an Exception
                        else if(basisforprocessingmap.get(csvRecordData[Basis_For_Processing].trim()) == TRUE)
                        {
                            throw new CDI_CustomException('The value provided for "Basis For Processing" in row number '+(i+1)+' is invalid or does not exist');
                        }
                        
                        //If Value is Invalid throw an Exception
                        else
                        {
                            throw new CDI_CustomException('The value provided for "Basis For Processing" in row number '+(i+1)+' is invalid or does not exist');
                        }                    
                    }    
                    
                    if(!String.isEmpty(csvRecordData[Time_Unit].trim()))
                    {
                        //Check if Time Unit Value is Deleted
                        if(timeunitmap.get(csvRecordData[Time_Unit].trim()) == FALSE)
                        {                        
                            dataasset.Time_Unit__r=new CDI_Time_Unit__c(External_Id__c=csvRecordData[Time_Unit].trim());
                        }
                        
                        //If Deleted throw an Exception
                        else if(timeunitmap.get(csvRecordData[Time_Unit].trim()) == TRUE)
                        {
                            throw new CDI_CustomException('The value provided for "Time Unit" in row number '+(i+1)+' is invalid or does not exist');
                        }
                        
                        //If Value is Invalid throw an Exception
                        else
                        {
                            throw new CDI_CustomException('The value provided for "Time Unit" in row number '+(i+1)+' is invalid or does not exist');
                        }                        
                    }
                    
                    if(!String.isEmpty(csvRecordData[Retention_Process].trim()))
                    {
                        //Check if Retention Process Value is Deleted
                        if(retentionprocessmap.get(csvRecordData[Retention_Process].trim()) == FALSE)
                        {                        
                            dataasset.Retention_Process__r=new CDI_Retention_Process__c(External_Id__c=csvRecordData[Retention_Process].trim());
                        }
                        
                        //If Deleted throw an Exception
                        else if(retentionprocessmap.get(csvRecordData[Retention_Process].trim()) == TRUE)
                        {
                            throw new CDI_CustomException('The value provided for "Retention Process" in row number '+(i+1)+' is invalid or does not exist');
                        }
                        
                        //If Value is Invalid throw an Exception
                        else
                        {
                            throw new CDI_CustomException('The value provided for "Retention Process" in row number '+(i+1)+' is invalid or does not exist');
                        }                    
                    }
                    
                    if(!String.isEmpty(csvRecordData[Customer_Data_Category].trim()))
                    {
                        //Check if Customer Data Category Value is Deleted
                        if(customerdatacategorymap.get(csvRecordData[Customer_Data_Category].trim()) == FALSE)
                        {
                            dataasset.Customer_Data_Category__r=new CDI_Customer_Data_Category__c(External_Id__c=csvRecordData[Customer_Data_Category].trim());
                        }
                        
                        //If Deleted throw an Exception
                        else if(customerdatacategorymap.get(csvRecordData[Customer_Data_Category].trim()) == TRUE)
                        {
                            throw new CDI_CustomException('The value provided for "Customer Data Category" in row number '+(i+1)+' is invalid or does not exist');
                        }
                        
                        //If Value is Invalid throw an Exception
                        else
                        {
                            throw new CDI_CustomException('The value provided for "Customer Data Category" in row number '+(i+1)+' is invalid or does not exist');
                        }                    
                    }
                    
                    if(!String.isEmpty(csvRecordData[GUS_Team].trim()))
                    {
                        //Check if GUS Team Value is Deleted
                        if(gusteammap.get(csvRecordData[GUS_Team].trim()) == FALSE)
                        {
                            dataasset.GUS_Team__r=new CDI_GUS_Team__c(External_Id__c=csvRecordData[GUS_Team].trim());
                        }
                        
                        //If Deleted throw an Exception
                        else if(gusteammap.get(csvRecordData[GUS_Team].trim()) == TRUE)
                        {
                            throw new CDI_CustomException('The value provided for "GUS Team" in row number '+(i+1)+' is invalid or does not exist');
                        }
                        
                        //If Value is Invalid throw an Exception
                        else
                        {
                            throw new CDI_CustomException('The value provided for "GUS Team" in row number '+(i+1)+' is invalid or does not exist');
                        }                    
                    }
                    
                    //Check if value entered is Boolean Value
                    if(csvRecordData[IsDeleted].trim().equalsIgnoreCase('TRUE')||csvRecordData[IsDeleted].trim().equalsIgnoreCase('FALSE'))
                    {
                        dataasset.IsDeleted__c=Boolean.valueOf(csvRecordData[IsDeleted].trim());
                    }
                    else
                    {
                        throw new CDI_CustomException('The value provided for "IsDeleted" in row number '+(i+1)+' is invalid.');   
                    }
                    //Check if the last index Value of Array exists
                    if(Comments<csvRecordData.size())
                    {
                        dataasset.Comments__c=csvRecordData[Comments].trim();
                    }
                    
                    //Add Data Assets to List
                    dataassets.add(dataasset);
                }
                
                catch(Exception e)
                {
                    ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.Severity.ERROR, 'Error is: ' + e.getMessage()); 
                    errors=errors+'Error is: ' + e.getMessage()+'\n';
                    ApexPages.addMessage(errorMessage); 
                    errorRowCount++;
                }
            }
            
            Schema.SObjectField externalId=CDI_Data_Asset__c.Fields.External_Id__c;
            
            //Import Data into CDI Application
            if(shouldTest == 'FALSE')
            {
                //Insert Parent Data Assets
                Database.SaveResult[] parentresults = Database.insert(new List<CDI_Data_Asset__c>(insertparentdataassets),FALSE);
                
                //Upsert Child Data Assets
                Database.UpsertResult[] results = Database.upsert(dataassets,externalId,FALSE); 
                
                for (Integer j = 0; j < results.size(); j++) {
                    if (!results[j].isSuccess())
                    {
                        errorRowCount++;
                        ApexPages.Message errorMessage=new ApexPages.Message(ApexPages.Severity.ERROR, 'Import of Record During Update Process Failed. Error is: ' + results[j].getErrors());
                        ApexPages.addMessage(errorMessage);
                        errors=errors+'Import of Record During Update Process Failed. Error is: ' + results[j].getErrors()+'\n';
                        
                    } 
                    else
                    {
                        successRowCount++;    
                    }
                }
            }
            
            //Test Import
            if(shouldTest == 'TRUE')
            {
                ApexPages.Message infoMessage = new ApexPages.Message(ApexPages.severity.INFO,'Data Assets Import is Tested. If any invalid entries are present, please check them in the error log below, if any.');
                ApexPages.addMessage(infoMessage);
                ApexPages.Message statisticssucceededrowsmessage = new ApexPages.Message(ApexPages.severity.INFO,'Total No of Succeeded Rows: '+(csvFileLines.size()-errorRowCount-1));
                ApexPages.addMessage(statisticssucceededrowsmessage);
            }
            
            //Import Data into CDI Application
            else
            {
                ApexPages.Message infoMessage = new ApexPages.Message(ApexPages.severity.INFO,'Data Assets are Imported into the CDI Application. If any invalid entries are present, they are not imported. Please check them in the error log below, if any.');
                ApexPages.addMessage(infoMessage);
                ApexPages.Message statisticssucceededrowsmessage = new ApexPages.Message(ApexPages.severity.INFO,'Total No of Succeeded Rows: '+successRowCount);
                ApexPages.addMessage(statisticssucceededrowsmessage);
            }
            ApexPages.Message statisticsfailedrowsmessage = new ApexPages.Message(ApexPages.severity.INFO,'Total No of Failed Rows: '+errorRowCount);
            ApexPages.addMessage(statisticsfailedrowsmessage);
            rowsFailed=errorRowCount;
            rowsSucceeded=successRowCount;
        }
        
        catch (Exception e)
        {
            ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.Severity.ERROR, 'An error has occured while during the import:' + e.getMessage());
            ApexPages.addMessage(errorMessage);
            errors=errors+'An error has occured while during the import:' + e.getMessage()+'\n';
        }
        
        //Check if Batch Import Record be created or not. Batch Import Record Not created for Test Import
        if(shouldTest == 'FALSE')
        {
            CDI_Batch_Import__c batchImport=new CDI_Batch_Import__c();
            batchImport.Rows_Failed__c=rowsFailed;
            batchImport.Rows_Succeeded__c=rowsSucceeded;
            batchImport.Errors__c=errors;
            batchImport.Start_Time__c=startDatetime;
            batchImport.Data_Store__c=ApexPages.CurrentPage().getparameters().get('id');
            batchImport.End_Time__c=DateTime.now();
            
            //Inset Batch Import Record
            insert batchImport;
        }
        return null;
    }
}
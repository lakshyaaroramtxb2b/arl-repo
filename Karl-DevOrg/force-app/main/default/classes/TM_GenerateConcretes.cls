/*
 
    Developer Name: Jorge L Caceres - jcaceres@salesforce.com | September 2015    

    Description: Generate concreate objectives
    
*/

public class TM_GenerateConcretes {
    
    private static final String BU_ENVIRONMENT_TYPE = TM_Constants.BU_ENVIRONMENT_TYPE;
    private static final String BU_ACCOUNT_REC_TYPE = TM_Constants.BU_ACCOUNT_REC_TYPE;
    private static final String BU_ACCOUNT_REC_TYPE_ID = TM_Constants.BU_ACCOUNT_REC_TYPE_ID;
    private static final String TMM_QUEUE_NAME = TM_Constants.TMM_QUEUE_NAME;
    private static final Id TMM_QUEUE_ID = TM_Constants.TMM_QUEUE_ID;
    private static final Map<String, String> SCS_TO_TMM_STATUS = TM_Constants.SCS_TO_TMM_STATUS;
    private static final Map<String, Integer> TMM_STATUS_SEQ = TM_Constants.TMM_STATUS_SEQ;
        
    private static List<TM_Placement__c> allPlacements;
    
    // load all static variables
    static {
        allPlacements = new List<TM_Placement__c> ();
        for (TM_Placement__c p : [Select TM_AbstractObjective__c, Category__c, Environment_Type__c,
                                  Level__c, Priority__c From TM_Placement__c]) {
            allPlacements.add(p);
        }    
    }
    
    public static void generateConcretesForAccount (List<Account> accounts, Map<Id, Account>oldAccounts) {
                        
        List<TM_Objective__c> concreteObjectives = new List<TM_Objective__c>();

        List<Account> accountsToProcess = new List<Account>();
        
        // if updating accounts then only update if switching TMM tracking from false to true and concretes is empty for acct
        if (oldAccounts == null) accountsToProcess = accounts; // not an update trigger
        else {
            // identified accounts changed
            List<Account> accountsChanged = new List<Account>();
            for (Account a : accounts) {
                if (a.TMM_Tracking__c && !oldAccounts.get(a.Id).TMM_Tracking__c) {                  
                    accountsChanged.add(a);  
                }                                       
            }
            // from those only select the ones without concretes objectives
            if (!accountsChanged.isEmpty()) {
                Set<Id> accountIdsWithConcretes = new Set<Id>();
                List<AggregateResult> groupedResults = [Select Count(Id), Business_Unit__c acctId from TM_Objective__c
                                  where Business_Unit__c IN :accountsChanged group by Business_Unit__c];                          
                for (AggregateResult ar : groupedResults) accountIdsWithConcretes.add((Id)ar.get('acctId')); 
                for (Account a : accountsChanged) {
                     if (!accountIdsWithConcretes.contains(a.Id)) accountsToProcess.add(a); 
                }
            }                 
        }
        
        set<Id> accountsProcessed = new set<Id>();
        for (Account a : accountsToProcess) {

            if (isAccountForTMM(a)) {

                for (TM_Placement__c p : allPlacements) {
                    if (p.Environment_Type__c == BU_ENVIRONMENT_TYPE) {
                        TM_Objective__c co = new TM_Objective__c();
                        co.OwnerId = TMM_QUEUE_ID;
                        co.TM_Placement__c = p.Id;
                        co.Business_Unit__c = a.Id;
                        concreteObjectives.add(co);
                        accountsProcessed.add(a.Id);
                    }
                }
            }                        
        }
        
        if (!concreteObjectives.isEmpty()) { 
            insert concreteObjectives;
        }
 
        // identify the related environments and build concretes for those
        if (!accountsProcessed.isEmpty()) {
            List<IRCLOUD__Environment__c> environments = new List<IRCLOUD__Environment__c>();
            for (IRCLOUD__Environment__c environment : [Select Id, IRCLOUD__Account__c, RecordTypeId, IRCLOUD__Parent_Environment__c
                                               from IRCLOUD__Environment__c where IRCLOUD__Account__c IN : accountsProcessed]) {
                environments.add(environment);                                      
            }
            generateConcretesForEnvironment(environments, null);                                    
        }
        
    }
    
    public static void generateConcretesForEnvironment (List<IRCLOUD__Environment__c> environments, Map<Id, IRCLOUD__Environment__c> oldEnvironments) {
        // create map of corresponding accounts and record types 
        Set<Id> accountIds = new Set<Id>();        
        Set<Id> recordTypeIds = new Set<Id>();        
        for (IRCLOUD__Environment__c e : environments) {
            accountIds.add(e.IRCLOUD__Account__c);
            recordTypeIds.add(e.RecordTypeId);
        }
        Map<Id, Account> relatedAccounts = new Map<Id, Account>([Select Id, RecordTypeId, TMM_Tracking__c from Account 
                                                                 where Id IN :accountIds]);
        Map<Id, RecordType> relatedRecordTypes = new Map<Id, RecordType>([Select Id, Name, DeveloperName from RecordType 
                                                                 where Id IN :recordTypeIds]);
        
        // identify environments that already have concrete objectives to skip those        
        Set<Id> environmentIdsWithConcretes = new Set<Id>();
        List<AggregateResult> groupedResults = [Select Count(Id), CSIRT_Environment__c environmentId from TM_Objective__c
                          where CSIRT_Environment__c IN :environments group by CSIRT_Environment__c];                          
        for (AggregateResult ar : groupedResults) environmentIdsWithConcretes.add((Id)ar.get('environmentId')); 
                        
        // loop through all placements and generate objectives for all environments
        List<TM_Objective__c> concreteObjectives = new List<TM_Objective__c>();
        for (IRCLOUD__Environment__c e : environments) {
            if (isAccountForTMM(relatedAccounts.get(e.IRCLOUD__Account__c)) && 
               (!environmentIdsWithConcretes.contains(e.Id)) &&
               (e.IRCLOUD__Parent_Environment__c == null)) {
                for (TM_Placement__c p : allPlacements) {
                    if (p.Environment_Type__c == relatedRecordTypes.get(e.RecordTypeId).Name) {
                        TM_Objective__c co = new TM_Objective__c();
                        co.OwnerId = TMM_QUEUE_ID;
                        co.TM_Placement__c = p.Id;
                        co.CSIRT_Environment__c = e.Id;
                        concreteObjectives.add(co);
                    }
                }
            }                       
        }
        
        if (!concreteObjectives.isEmpty()) {
            insert concreteObjectives;
        }
            
        
    }
    
    public static void generateConcretesForPlacement (List<TM_Placement__c> placements) {
        
        // generate objectives for any business units or environments currently with objectives
        Set<String> environmentTypes = new Set<String>();               
        for (TM_Placement__c p : placements) {
            if (p.Environment_Type__c != null) environmentTypes.add(p.Environment_Type__c);
        }

        // select all distinct account and environments currently with objectives to add new placements
        List<AggregateResult> groupedResults;

        Set<Id> environmentIds = new Set<Id>(); 
        groupedResults = [Select Count(Id), CSIRT_Environment__c envId from TM_Objective__c
                          where CSIRT_Environment__r.RecordType.Name IN :environmentTypes group by CSIRT_Environment__c];                          
        for (AggregateResult ar : groupedResults) environmentIds.add((Id)ar.get('envId'));                  
        Map<Id, IRCLOUD__Environment__c> relatedEnvironments = new Map<Id, IRCLOUD__Environment__c>([
                                                                 Select Id, RecordType.Name 
                                                                 from IRCLOUD__Environment__c 
                                                                 where Id IN :environmentIds]);
 
        Set<Id> accountIds = new Set<Id>();        
        groupedResults = [Select Count(Id), Business_Unit__c acctId from TM_Objective__c
                          where Business_Unit__c != null group by Business_Unit__c];                          
        for (AggregateResult ar : groupedResults) accountIds.add((Id)ar.get('acctId'));                  
        Map<Id, Account> relatedAccounts = new Map<Id, Account>([Select Id, RecordTypeId, TMM_Tracking__c from Account 
                                                                 where Id IN :accountIds]);
                
        // loop through the existing accounts and environments and add new objectives if required
        List<TM_Objective__c> concreteObjectives = new List<TM_Objective__c>();
        for (TM_Placement__c p : placements) {
            if (p.Environment_Type__c == BU_ENVIRONMENT_TYPE) {
                // add to all existing accounts
                for (Account a : relatedAccounts.values()) {
                    if (isAccountForTMM(a)) {
                        TM_Objective__c co = new TM_Objective__c();
                        co.OwnerId = TMM_QUEUE_ID;
                        co.TM_Placement__c = p.Id;
                        co.Business_Unit__c = a.Id;
                        concreteObjectives.add(co);
                    }                                           
                }               
            } else {
                // add to all existing matching environments
                for (IRCLOUD__Environment__c e : relatedEnvironments.values()) {
                    if (p.Environment_Type__c == e.RecordType.Name) {
                        TM_Objective__c co = new TM_Objective__c();
                        co.OwnerId = TMM_QUEUE_ID;
                        co.TM_Placement__c = p.Id;
                        co.CSIRT_Environment__c = e.Id;
                        concreteObjectives.add(co);
                    }                   
                }               
            }        
        }
        
        if (!concreteObjectives.isEmpty()) {
            insert concreteObjectives;
        }
            
                
    }
        
    public static Id getSecurityControlStatusId (Id objectiveId) {
        Id scsId;

        try {
            
            TM_Objective__c objective = [Select TM_Placement__r.TM_AbstractObjective__r.Security_Control_ID__c, 
                                         TM_Placement__r.Environment_Type__c, Environment__c, 
                                         CSIRT_Environment__c, Business_Unit__c
                                         from TM_Objective__c where Id = :objectiveId];
            
            scsId = [select Id from Security_Control_Status__c where
                   Security_Control__r.Name = :objective.TM_Placement__r.TM_AbstractObjective__r.Security_Control_ID__c  and
                   Environment__r.RecordType.Name = :objective.TM_Placement__r.Environment_Type__c and
                   (Environment__c = :objective.Environment__c or Environment__r.Account__c = :objective.Business_Unit__c)
                   ].Id;
            
        } catch (exception e) {
            
        } 
        return scsId;   
    }
    
    private static boolean isAccountForTMM(Account account) {
        return (account.RecordTypeId == BU_ACCOUNT_REC_TYPE_ID && account.TMM_Tracking__c);
    }
    
    

}
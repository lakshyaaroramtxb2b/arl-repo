public class LeaderIndvidualController {
    
    public List<LeaderIndvidualRecord> userRecords {get; set;}

    public String selected_list_dateRange{get; set;}
    
    String userId;
    User selUser;
    Integer dayRange = 7;
    
    public void Change()
	{
        if(selected_list_dateRange != null)
        	dayRange = Integer.valueOf(selected_list_dateRange);
        
        System.debug('dayRange : ' + dayRange);
                
                Date dR = System.today() - dayRange;
               
                List<Case> cases = [SELECT Subject, createdDate, Detection_Alert__r.DetectionAlertCriteria__r.Name, status, IRCloud__Sub_Status__c, IRCloud__Case_Adjudication__c, IRCloud__Case_Summary__c, Owner.Name
                                    FROM Case 
                                    WHERE CreatedDate > :dR 
                                        /*AND IRCloud__Sub_Status__c = 'Malicious (Incident)'*/
                                        AND Detection_Alert__c != null 
                                        AND Detection_Alert__r.DetectionAlertCriteria__r.CreatedById =: selUser.Id
                                     ORDER BY CreatedDate];
                
                System.debug(cases.size());
                
                for(Case c : cases) {
                    
                    LeaderIndvidualRecord rec = new LeaderIndvidualRecord();
                    
                    System.debug(c.Subject);
                    
                    rec.id = selUser.Id;
                    rec.Name = selUser.Name;
                    rec.subject = c.Subject;
                    rec.caseId = c.Id;
					rec.createdDate = c.CreatedDate;
                    rec.status = c.Status;
                    rec.subStatus = c.IRCloud__Sub_Status__c;
                   	rec.caseOwner = c.Owner.Name;
                    rec.csirtAdjudication = c.IRCloud__Case_Adjudication__c;
                    rec.csirtSummary = c.IRCloud__Case_Summary__c;
                    
                    userRecords.add(rec);
                } 
    }
    
    public LeaderIndvidualController()
    {
        userRecords = new List<LeaderIndvidualRecord>();
        
        userId = ApexPages.currentPage().getParameters().get('userId');
        
        if(userId != null) {
        
            selUser = [SELECT Id, Name 
                                     FROM User 
                                     WHERE Id =: userId LIMIT 1][0];
            
            if(selUser != null) {
                
                Change();  
            }
        }
    }
}
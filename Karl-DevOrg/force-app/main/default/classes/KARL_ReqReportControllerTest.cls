/**
* @author Ben Harvie
* @email ben.harvie@salesforce.com
* @description This is a test class for KARL_ARLReportController, which is used throughout
* the KARL application within LWCs.
*/
@isTest

public with sharing class KARL_ReqReportControllerTest {
    /**
    * @author Ben Harvie
    * @email ben.harvie@salesforce.com
    * @description Test data setup.
    */
	@testSetup
    private static void testSetup(){
        User ospAdmin = ARL_TestDataFactory.createNamedOpsAdmin('TestSetup', 'User', 'testsetup@example.com');
        
        System.runAs(ospAdmin){
            /**
             * Controls Setup
             */
            // Create empty control shell
            Control__c control = ARL_TestDataFactory.createControl();
            insert control;

            // Map to the control shell
            Control_Scope__c controlScope = ARL_TestDataFactory.createControlScope();
            controlScope.Control_Name__c = control.id; // Map to the created control
            controlScope.Scope_Name__c  = 'HK';
            insert controlScope;
            
            // Create control tests
            Control_Test__c controlTest1 = ARL_TestDataFactory.createControlTest(controlScope.id);
            controlTest1.Test_Name__c = 'test control test 1';
            insert controlTest1;
            
            Control_Test__c controlTest2 = ARL_TestDataFactory.createControlTest(controlScope.id);
            controlTest2.Test_Name__c = 'test control test 2';
            insert controlTest2;
            
            /**
             * Requests Master Data Setup
             */
            Request_Item__c reqItem1 = ARL_TestDataFactory.createRequestItem();
            reqItem1.Request_Name__c = 'Request 1';
            reqItem1.Primary_Scope__c = 'HK';
            reqItem1.Areas_of_Compliance__c  = 'SOC 2';
            reqItem1.Create_GUS_Case__c = true;
            insert reqItem1;

            Request_Item__c reqItem2 = ARL_TestDataFactory.createRequestItem();
            reqItem2.Request_Name__c = 'Request ';
            reqItem2.Primary_Scope__c = 'HK';
            reqItem2.Areas_of_Compliance__c  = 'HITRUST';
            reqItem2.Create_GUS_Case__c = true;
            insert reqItem2;
            
            // Map the request items to control tests
            Request_Item_Control__c requestControlMapping1 = ARL_TestDataFactory.createRequestItemControl(reqItem1.id,controlTest1.Id);
            insert requestControlMapping1;
            
            Request_Item_Control__c requestControlMapping2 = ARL_TestDataFactory.createRequestItemControl(reqItem2.id,controlTest2.Id);
            insert requestControlMapping2;

            /**
             * Area Requirement Setup
             */
            Area_Requirement__c areaRequirement1 = ARL_TestDataFactory.createAreaRequirement('1.1.1', 'PCI DSS');
            insert areaRequirement1;

            // More than 1, so we test recursion doesn't block additional items
            Area_Requirement__c areaRequirement2 = ARL_TestDataFactory.createAreaRequirement('2.1.1', 'PCI DSS');
            insert areaRequirement2;

            Request_Item_Area__c areaMap1 = ARL_TestDataFactory.mapAreaRequirement(reqItem1.Id, areaRequirement1.Id);
            insert areaMap1;

            Request_Item_Area__c areaMap2 = ARL_TestDataFactory.mapAreaRequirement(reqItem1.Id, areaRequirement2.Id);
            insert areaMap2;
        }
    }

    /**
    * @author Ben Harvie
    * @email ben.harvie@salesforce.com
    * @description This method tests the generation of the REQ Datatable as an Ops Admin
    */
    @isTest
    private static void masterDataReportOpsAdmin(){

        User u = ARL_TestDataFactory.createOpsAdmin();
       
        // Run the test as the SCEA Ops Admin
        System.runAs(u) {
            Test.startTest();
                Map<String, String> auditScopes = KARL_Utility.getAuditCycleScopes();

                // Set Variables
                String auditScopeId         = auditScopes.get('Heroku');
                String [] filterAssigned    = new List<String>{''};
                String [] areaOfCompliance  = new List<String>{'allAoc'};
                String [] mappingFilter     = new List<String>{''};
                String filterType           = 'typeAll';
                String [] activeStatus      = new List<String>{'activeReq'};
                String [] qaReview          = new List<String>{''};
                String [] gusEnabled        = new List<String>{''};
                String [] placeholderFilter = new List<String>{''};

                // Generate report (Salesforce internal)
                List<KARL_ReqReportController.ReportWrapper> reportItemsInternal = KARL_ReqReportController.getReport(
                    auditScopeId, 
                    areaOfCompliance, 
                    mappingFilter,
                    filterAssigned, 
                    filterType, 
                    activeStatus, 
                    qaReview,
                    gusEnabled,
                    placeholderFilter);
                system.assertEquals(2, reportItemsInternal.size(), 'Incorrect number of items on the internal report'); // Expecting 2x REQs

            Test.stopTest();
        }
    }

    /**
    * @author Ben Harvie
    * @email ben.harvie@salesforce.com
    * @description This method tests the generation of the REQ Datatable as a Audit Manager
    */
    @isTest
    private static void masterDataReportAuditMgr(){

        User u = ARL_TestDataFactory.createAuditManager();

        // Run the test as the SCEA Audit Mgr
        System.runAs(u) {
            Test.startTest();
                Map<String, String> auditScopes = KARL_Utility.getAuditCycleScopes();
            
                // Set Variables
                String auditScopeId         = 'allScopes';
                String [] filterAssigned    = new List<String>{''};
                String [] areaOfCompliance  = new List<String>{'SOC'};
                String [] mappingFilter     = new List<String>{''};
                String filterType           = 'typeSample';
                String [] activeStatus      = new List<String>{'activeReq'};
                String [] qaReview          = new List<String>{''};
                String [] gusEnabled        = new List<String>{''};
                String [] placeholderFilter = new List<String>{''};

                // Generate report (Salesforce internal)
                List<KARL_ReqReportController.ReportWrapper> reportItemsInternal = KARL_ReqReportController.getReport(
                    auditScopeId, 
                    areaOfCompliance, 
                    mappingFilter,
                    filterAssigned, 
                    filterType, 
                    activeStatus, 
                    qaReview,
                    gusEnabled,
                    placeholderFilter);
                system.assertEquals(1, reportItemsInternal.size(), 'Incorrect number of items on the internal report'); // Expecting 1x REQs

            Test.stopTest();
        }
    }

    /**
    * @author Ben Harvie
    * @email ben.harvie@salesforce.com
    * @description This method tests the generation of the REQ Datatable as a Display User
    */
    @isTest
    private static void masterDataReportDisplay(){

        User u = ARL_TestDataFactory.createDisplayUser();

        // Run the test as the SCEA Audit Mgr
        System.runAs(u) {
            Test.startTest();
                Map<String, String> auditScopes = KARL_Utility.getAuditCycleScopes();
            
                // Set Variables
                String auditScopeId         = 'allScopes';
                String [] filterAssigned    = new List<String>{''};
                String [] areaOfCompliance  = new List<String>{'SOC'};
                String [] mappingFilter     = new List<String>{''};
                String filterType           = 'typeSample';
                String [] activeStatus      = new List<String>{'activeReq'};
                String [] qaReview          = new List<String>{''};
                String [] gusEnabled        = new List<String>{''};
                String [] placeholderFilter = new List<String>{''};

                // Generate report (Salesforce internal)
                List<KARL_ReqReportController.ReportWrapper> reportItemsInternal = KARL_ReqReportController.getReport(
                    auditScopeId, 
                    areaOfCompliance, 
                    mappingFilter,
                    filterAssigned, 
                    filterType, 
                    activeStatus, 
                    qaReview,
                    gusEnabled,
                    placeholderFilter);
                system.assertEquals(1, reportItemsInternal.size(), 'Incorrect number of items on the internal report'); // Expecting 1x REQs

            Test.stopTest();
        }
    }
    
    /**
    * @author Ben Harvie
    * @email ben.harvie@salesforce.com
    * @description This method tests the update of the Request Name
    */
    @isTest
    private static void updateReqRecord(){
        // Setup Ops Admin
        User u = ARL_TestDataFactory.createAuditManager();
       
        // Run the test as the SCEA Ops Admin
        System.runAs(u) {
            Test.startTest();
                // Collect one of the REQs
                Request_Item__c reqItem = [SELECT id, Request_Name__c FROM Request_Item__c LIMIT 1];

                String newName = reqItem.Request_Name__c + '-test';

                List<Object> jsonList = (List<Object>)JSON.deserializeUntyped('[{"Id": "' + reqItem.Id + '", ' + + 
                                                                                '"Request_Name__c": "' + newName + '", ' +
                                                                                '"Update_Current_Audit_Cycle_s__c": "Yes"}]');

                KARL_ReqReportController.updateRequest(jsonList);

                Request_Item__c reqItemUpdated = [SELECT id, Request_Name__c FROM Request_Item__c WHERE Id =: reqItem.Id];
                system.assertEquals(newName, reqItemUpdated.Request_Name__c, 'Name not updated'); 

            Test.stopTest();
        }
    }
}
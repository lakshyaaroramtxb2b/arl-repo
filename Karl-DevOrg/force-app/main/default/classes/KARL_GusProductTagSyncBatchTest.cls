@isTest
public class KARL_GusProductTagSyncBatchTest {
    /**
     * @author Lakshya Arora
     * @email lakshya.arora@mtxb2b.com
     * @desc Test data setup.
    */
    @testSetup
    public static void makeData(){
        User opsAdminUser = ARL_TestDataFactory.createOpsAdmin();
        System.runAs(opsAdminUser){
            KARL_GUS_Product_Tag__c karlGusProdTag = new KARL_GUS_Product_Tag__c();
            karlGusProdTag.Product_Tag_External_Id__c = 'TAG123';
            insert karlGusProdTag;
        }
    }
    /**
     * @author Lakshya Arora
     * @email lakshya.arora@mtxb2b.com
     * @desc method to test the product tag sync functionality.
    */
    @isTest
    public static void productTagSyncBatchTest(){
        List<User> opsAdminUserList = ARL_TestDataFactory.fetchUser('abc@b.com.d');
        if(!opsAdminUserList.isEmpty()){
            System.runAs(opsAdminUserList[0]){
                KARL_GusProductTagSyncBatch obj = new KARL_GusProductTagSyncBatch();
                ADM_Scrum_Team_c__x admScrumTeam = new ADM_Scrum_Team_c__x();
                admScrumTeam.ExternalId = 'TEAM123';
                ADM_Product_Tag_c__x admProductTag = new ADM_Product_Tag_c__x();
                admProductTag.ExternalId = 'TAG123';
                admProductTag.Team_c__c = 'TEAM123';
                admProductTag.Active_c__c = true;
                
                ADM_Product_Tag_c__x admProductTag1 = new ADM_Product_Tag_c__x();
                admProductTag1.ExternalId = 'TAG1234';
                admProductTag1.Team_c__c = 'TEAM123';
                admProductTag1.Active_c__c = true;
                
                
                List<ADM_Product_Tag_c__x> productTagListForTestClass = new List<ADM_Product_Tag_c__x>();
                productTagListForTestClass.add(admProductTag);
                productTagListForTestClass.add(admProductTag1);
                obj.scrumTeamIdToActiveMapForTestClass.put('TEAM123',true); 
                
                Test.startTest();
                //need to call explicitly as no data for external Data
                Database.QueryLocator ql = obj.start(null);
                obj.Finish(null);
                obj.execute(null, productTagListForTestClass);
                obj.execute(null);
                Test.stopTest();
                List<KARL_GUS_Product_Tag__c> gusProductTagList = new List<KARL_GUS_Product_Tag__c>([SELECT Id,Active__c,Product_Tag_External_Id__c FROM KARL_GUS_Product_Tag__c WITH SECURITY_ENFORCED]);
                System.assert(gusProductTagList.size() == 2);
                System.assert(gusProductTagList[0].Active__c);
                System.assert(gusProductTagList[1].Active__c);
            }
        }
    }
}
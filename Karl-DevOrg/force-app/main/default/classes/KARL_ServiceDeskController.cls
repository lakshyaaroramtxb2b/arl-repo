/**
 * @author Lakshya Arora
 * @email lakshya.arora@mtxb2b.com
 * @description Handle karl_Service_desk_rmd LWC component operations
 * @note -> call without sharing methods from KARL_ServiceDeskControllerHelper
 */
public with sharing class KARL_ServiceDeskController{
    /**
    * @author Lakshya Arora
    * @email lakshya.arora@mtxb2b.com
    * @description create KARL_Service_Desk__c Record of Request Master Data Record Type
    * param serviceDeskData -> KARL_Service_Desk__c record info from lwc component
    */
    @AuraEnabled
    public static String createServiceDeskRecord(String serviceDeskData,String buttonLabel){ 
        ServiceDeskWrapper serviceDeskWrapperObj = (ServiceDeskWrapper)JSON.deserialize(serviceDeskData, ServiceDeskWrapper.Class);
        KARL_Service_Desk__c serviceDeskObj = new KARL_Service_Desk__c();
        if(String.isNotBlank(serviceDeskWrapperObj.requestNameId))
        serviceDeskObj.Request_Name__c = serviceDeskWrapperObj.requestNameId;
        serviceDeskObj.New_or_Existing__c = serviceDeskWrapperObj.newOrExisitng;
        if(!serviceDeskWrapperObj.areaOfCompliance.isEmpty())
        serviceDeskObj.Area_of_Compliance__c = convertListToMultipicklistString(serviceDeskWrapperObj.areaOfCompliance);
        serviceDeskObj.Type__c = serviceDeskWrapperObj.type;
        serviceDeskObj.Notes__c = serviceDeskWrapperObj.dataUpdateReason;
        serviceDeskObj.Business_Unit__c = serviceDeskWrapperObj.businessUnit;
        serviceDeskObj.New_Request_Name__c = serviceDeskWrapperObj.requestNameText;
        if(!serviceDeskWrapperObj.controltestlist.isEmpty()){
            String controlTestName = '';
            String controlTestIds = '';
            for(JunctionObjectWrapper controlJWObj : serviceDeskWrapperObj.controltestlist){
                controlTestName = controlJWObj.recName +',' +controlTestName;
                controlTestIds = controlJWObj.recId + ','+controlTestIds;
            }
            controlTestName = controlTestName.removeEnd(',');
            controlTestIds = controlTestIds.removeEnd(',');
            serviceDeskObj.Control_Test__c = controlTestName;
            serviceDeskObj.Control_Number_Ids__c = controlTestIds;
            
        }
        if(!serviceDeskWrapperObj.arearequirmentlist.isEmpty()){
            String areaReqName = '';
            String areaReqIds = '';
            for(JunctionObjectWrapper areaJWObj : serviceDeskWrapperObj.arearequirmentlist){
                areaReqName = areaJWObj.recName +',' +areaReqName;
                areaReqIds = areaJWObj.recId + ',' + areaReqIds; 
            }
            areaReqName = areaReqName.removeEnd(',');
            areaReqIds = areaReqIds.removeEnd(',');
            serviceDeskObj.Area_Requirement__c = areaReqName;
            serviceDeskObj.Area_Requirement_Ids__c = areaReqIds;
        }
        serviceDeskObj.Request_Description__c = serviceDeskWrapperObj.requestDescription;
        if(buttonLabel == 'Save'){
            serviceDeskObj.Status__c = KARL_Constants.STATUS_DRAFT;
        }
        else{
            serviceDeskObj.Status__c = KARL_Constants.STATUS_SUBMIT_FOR_APPROVAL;
        }
        serviceDeskObj.RecordTypeId = KARL_Constants.REQUEST_MASTER_DATA_RECORDTYPEID;
        if(serviceDeskWrapperObj.newOrExisitng == 'New'){
            List<KARL_Audit_Scope_Master_Data__c> auditScopeList = new List<KARL_Audit_Scope_Master_Data__c>();
            auditScopeList = Karl_ServiceDeskControllerHelper.getAuditScopeByKARLScope(serviceDeskWrapperObj.businessUnit);
            if(!auditScopeList.isEmpty()){
                serviceDeskObj.GRC_Assignee__c = auditScopeList[0].KARL_BU_Lead__c;
            }
        }
        else{
            serviceDeskObj.GRC_Assignee__c = serviceDeskWrapperObj.grcAssigneeId;
        }
        //Don't need security enforced as accessing User Object
        for(User userinfoObj : [SELECT Id,ContactId FROM User WHERE Id =: Userinfo.getUserID()]){
            if(userinfoObj.contactId != null)
            serviceDeskObj.Requested_By__c = userinfoObj.contactId;
        }
        try{
            insert serviceDeskObj;
            if(buttonLabel == 'Save'){
                return 'Service Desk Record Saved Successfully.';
            }
            else{
                return 'Service Desk Record Created Successfully and '+KARL_Constants.STATUS_SUBMIT_FOR_APPROVAL+'.';
            }
        }
        catch(Exception e){
            return 'Error occurred: Please contact your System administrator';
        }
    }

    /**
    * @author Lakshya Arora
    * @email lakshya.arora@mtxb2b.com
    * @description create KARL_Service_Desk__c Record of Evidence Request Record Type
    * param >serviceDeskData -> KARL_Service_Desk__c record info from karl_request_access_modal  lwc component
    */
    @AuraEnabled
    public static void createServiceDeskEvidenceRecord(String serviceDeskData){
        String grcAssigneeId; 
        ServiceDeskEvidenceWrapper serviceDeskEvidenceWrapperObj = (ServiceDeskEvidenceWrapper)JSON.deserialize(serviceDeskData, ServiceDeskEvidenceWrapper.Class);
        KARL_Service_Desk__c serviceDeskObj = new KARL_Service_Desk__c();
        if(String.isNotBlank(serviceDeskEvidenceWrapperObj.evidenceRequestId))
        serviceDeskObj.Evidence_Request_Name__c = serviceDeskEvidenceWrapperObj.evidenceRequestId;
        serviceDeskObj.Notes__c = serviceDeskEvidenceWrapperObj.describeAccessRequest;
        serviceDeskObj.Status__c = KARL_Constants.STATUS_SUBMIT_FOR_APPROVAL;
        serviceDeskObj.RecordTypeId = KARL_Constants.EVIDENCE_REQUEST_RECORDTYPEID;
        if(String.isNotBlank(serviceDeskEvidenceWrapperObj.cycleArlItemId)){
            for(Cycle_Request_Item__c cycleReqItemObj : Karl_ServiceDeskControllerHelper.getCycleARLItemById(serviceDeskEvidenceWrapperObj.cycleArlItemId)){
                grcAssigneeId = cycleReqItemObj?.Request__r.KARL_GRC_Assignee__c != null ? cycleReqItemObj?.Request__r.KARL_GRC_Assignee__c : cycleReqItemObj?.Audit_Cycle_Coverage__r?.Cycle_Audit_Report_Items__r?.Audit_Reports_Master_Data__r?.KARL_Audit_Scope__r.KARL_BU_Lead__c;
            }
        }
        if(String.isNotBlank(grcAssigneeId)){
            serviceDeskObj.GRC_Assignee__c = grcAssigneeId;
        }
        //Don't need security enforced as accessing User Object
        for(User userinfoObj : [SELECT Id,ContactId FROM User WHERE Id =: Userinfo.getUserID()]){
            if(userinfoObj.contactId != null)
            serviceDeskObj.Requested_By__c = userinfoObj.contactId;
        }
        try{
            insert serviceDeskObj;
        }
        catch(Exception e){
            if(e.getMessage().contains('GRC Assignee User field')){
                throw new AuraHandledException('Error Occurred,Please contact your administrator : GRC Assignee User Not found');
            }
            else{
                throw new AuraHandledException('Error Occurred,Please contact your administrator :'+e.getMessage());
            }
        }
    }


    /**
    * @author Lakshya Arora
    * @email lakshya.arora@mtxb2b.com
    * @description get Junction objects for control unit and area requirement
    * param >serviceDeskData -> Id of request Item record
    */
    @AuraEnabled
    public static ServiceDeskWrapper getRequestMasterDataInfo(String requestItemId){
        ServiceDeskWrapper serviceDeskWrapperObj = new ServiceDeskWrapper();
        for(Request_Item__c requestItemObj : [SELECT Id,Request_Name__c,KARL_GRC_Assignee__c,Type__c,Areas_of_Compliance__c,Primary_Scope__c,Request_Description__c,
                                                (SELECT Id,Area_Requirement__c,Area_Requirement__r.Name FROM Request_Item_Areas__r),
                                                (SELECT Id,Full_Control_Scope__c,Test__c FROM Request_Item_Controls__r)
                                            FROM Request_Item__c 
                                            WHERE Id =: requestItemId
                                            WITH SECURITY_ENFORCED]){
                                                serviceDeskWrapperObj.requestNameId = requestItemObj.Id;
                                                serviceDeskWrapperObj.requestNameText = requestItemObj.Request_Name__c;
                                                serviceDeskWrapperObj.grcAssigneeId = requestItemObj.KARL_GRC_Assignee__c;
                                                serviceDeskWrapperObj.type = requestItemObj.Type__c;
                                                if(requestItemObj.Areas_of_Compliance__c != null){
                                                    serviceDeskWrapperObj.areaOfCompliance = new List<String>();
                                                    serviceDeskWrapperObj.areaOfCompliance.addAll(requestItemObj.Areas_of_Compliance__c.split(';'));
                                                }
                                                if(!requestItemObj.Request_Item_Controls__r.isEmpty()){
                                                    serviceDeskWrapperObj.controltestlist = new List<JunctionObjectWrapper>();
                                                    for(Request_Item_Control__c reqItemControlObj : requestItemObj.Request_Item_Controls__r){
                                                        serviceDeskWrapperObj.controltestlist.add(new JunctionObjectWrapper(reqItemControlObj.Test__c,reqItemControlObj.Full_Control_Scope__c));
                                                    }
                                                }
                                                if(!requestItemObj.Request_Item_Areas__r.isEmpty()){
                                                    serviceDeskWrapperObj.arearequirmentlist = new List<JunctionObjectWrapper>();
                                                    for(Request_Item_Area__c reqItemAreaObj : requestItemObj.Request_Item_Areas__r){
                                                        serviceDeskWrapperObj.arearequirmentlist.add(new JunctionObjectWrapper(reqItemAreaObj.Area_Requirement__c,reqItemAreaObj.Area_Requirement__r.Name));
                                                    }
                                                }
                                                serviceDeskWrapperObj.businessUnit = requestItemObj.Primary_Scope__c;
                                                serviceDeskWrapperObj.requestDescription = requestItemObj.Request_Description__c;
                                                serviceDeskWrapperObj.newOrExisitng = 'Existing';
                                            }
            
        return serviceDeskWrapperObj;
    }

    /**
    * @author Lakshya Arora
    * @email lakshya.arora@mtxb2b.com
    * @description Convert list to comma separated string
    * param >serviceDeskData -> return  comma separated string
    */
    private static String convertListToMultipicklistString(List<String> inputList){
        if(!inputList.isEmpty()){
            inputList.sort();
            String multipicklistString = String.join(inputList,';');
            return multipicklistString;
        }
        return null;
    }

    public class ServiceDeskEvidenceWrapper{
        @AuraEnabled public String evidenceRequestId;
        @AuraEnabled public String describeAccessRequest;
        @AuraEnabled public String cycleArlItemId;
    }

    public class ServiceDeskWrapper{
        @AuraEnabled public String requestNameId;
        @AuraEnabled public String requestNameRecordName;
        @AuraEnabled public String requestNameRecordSecondarField;
        @AuraEnabled public String newOrExisitng;
        @AuraEnabled public String dataUpdateReason;
        @AuraEnabled public String requestDescription;
        @AuraEnabled public String type;
        @AuraEnabled public List<String> areaOfCompliance;
        @AuraEnabled public String businessUnit;
        @AuraEnabled public String grcAssigneeId;
        @AuraEnabled public String requestNameText;
        @AuraEnabled public List<JunctionObjectWrapper> controltestlist;
        @AuraEnabled public List<JunctionObjectWrapper> arearequirmentlist; 
    }

    public class JunctionObjectWrapper{
        @AuraEnabled public String recId;
        @AuraEnabled public String recName;
        public JunctionObjectWrapper(){

        }
        public JunctionObjectWrapper(String recId,String recName){
            this.recId = recId;
            this.recName = recName;
        }
    }

}
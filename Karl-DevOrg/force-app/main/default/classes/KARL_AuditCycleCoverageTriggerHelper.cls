public class KARL_AuditCycleCoverageTriggerHelper {

/*
* Created by - Swarnima Singh Mandhata
* This functon is used to identify the uniqueness of record for every auto-Assignment config.
*/

/* commented by Lakshya Arora as per Issue I-52756
    public static void checkDuplicativeConfigItems(List<Audit_Cycle_Coverage__c> newList, Map<id,Audit_Cycle_Coverage__c> oldMap){
        Set<Id> autoAssignmentConfigIdSet = new Set<Id>(); 
        Map<Id,List<Audit_Cycle_Coverage__c>> autoConfigIdToAuditCycleCoverageMap = new Map<Id,List<Audit_Cycle_Coverage__c>>();
        for(Audit_Cycle_Coverage__c audit: newList){
            if((oldMap == null && audit.Area__c != null && audit.KARL_Audit_Team__c != null && audit.Scope__c != null) ||
               (oldMap != null && (audit.Area__c != oldMap.get(audit.id).Area__c 
                                  || audit.KARL_Audit_Team__c != oldMap.get(audit.id).KARL_Audit_Team__c
                                  || audit.Scope__c != oldMap.get(audit.id).Scope__c)) && audit.Auto_Assignment_Configuration__c != null){
                                      autoAssignmentConfigIdSet.add(audit.Auto_Assignment_Configuration__c);
                                  }
        }
            System.debug('autoAssignmentConfigIdSet--> '+ autoAssignmentConfigIdSet);
            
            for(Audit_Cycle_Coverage__c auditCoverage: [SELECT id,Auto_Assignment_Configuration__c,Area__c,KARL_Audit_Team__c,Scope__c 
                                                        FROM Audit_Cycle_Coverage__c 
                                                        WHERE Auto_Assignment_Configuration__c IN: autoAssignmentConfigIdSet]){
                                                            if(!autoConfigIdToAuditCycleCoverageMap.containsKey(auditCoverage.Auto_Assignment_Configuration__c)){
                                                                autoConfigIdToAuditCycleCoverageMap.put(auditCoverage.Auto_Assignment_Configuration__c, new List<Audit_Cycle_Coverage__c>());
                                                            }
                                                            autoConfigIdToAuditCycleCoverageMap.get(auditCoverage.Auto_Assignment_Configuration__c).add(auditCoverage);
                                                        }
        for(Audit_Cycle_Coverage__c auditCycle: newList){
            if(autoConfigIdToAuditCycleCoverageMap.containsKey(auditCycle.Auto_Assignment_Configuration__c)){
                for(Audit_Cycle_Coverage__c existingdata: autoConfigIdToAuditCycleCoverageMap.get(auditCycle.Auto_Assignment_Configuration__c)){
                    if(auditCycle.Area__c == existingData.Area__c && auditCycle.KARL_Audit_Team__c == existingData.KARL_Audit_Team__c
                      && auditCycle.Scope__c == existingData.Scope__c){
                        auditCycle.addError('This Audit Cycle already Linked with Auto Configuration.');
                    }
                }
                
            }
            
        }
            
            
    }*/
}
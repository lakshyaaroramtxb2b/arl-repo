public class DevelopSecureWebAppsReleaseNotes {
	public Knowledge_Article__kav kb {get; private set;}
    
    public void load() {
        // May need to filter down to RecordType for Secure Trailhead - dgatchell
        kb = [SELECT Title, Body__c FROM Knowledge_Article__kav 
                WHERE Language = 'en_US' 
                AND PublishStatus = 'Online' 
                AND UrlName = 'Develop-Secure-Web-Apps-Release-Notes'];        
    }
}
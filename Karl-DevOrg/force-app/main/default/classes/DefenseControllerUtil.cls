/**
 *  An utility class which serves to assist in handling apex services offered by
 *  Defense Integration framework
 **/
public class DefenseControllerUtil {

	public static Map<String, Object> getHeader() {
        Map<String, Object> header = new Map<String, Object>();
		String sigAlgorithm = 'RS256';
        header.put('alg', sigAlgorithm);
        header.put('typ', 'JWT');
        header.put('kid', UserInfo.getOrganizationId().substring(0,15) + '_' + 'md_cert');
        Long expTime = (System.currentTimeMillis() / 1000L) + 60L;
        header.put('exp', expTime);
        String randomString = String.valueof(Crypto.getRandomLong());
        header.put('nonce', randomString.substring(0, 10));
        header.put('iat', 0);
        return header;
	}
    
	public static String base64UrlEncode(Blob input) {
       String output = EncodingUtil.base64Encode(input);
       output = output.replace('+', '-');
       output = output.replace('/', '_');
       while (output.endsWith('=')) {
           output = output.subString(0,output.length()-1);
       }
       return output;
   	}
    
	public static void addAttributeIfNonNull(String key, Object attribute, Map<String, Object> targetMap) {
      if (attribute != null) {
          targetMap.put(key, attribute);
      }
	}
    
    // Check to see if the specified cert exists
    public static Boolean checkCertExists(String certName) {
        try {
            Crypto.signWithCertificate('RSA-SHA256', Blob.valueOf(''), certName);
            return true;
        }
        catch (Exception ex) {
            return false;
        }
    }
 
}
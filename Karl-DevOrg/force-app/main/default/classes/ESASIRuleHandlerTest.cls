@isTest
public class ESASIRuleHandlerTest {
    
    @isTest
    static void testIsValidIP() {
        System.assert(ESASIRuleHandler.isValidIP(null));
        // Private IP
        System.assert(ESASIRuleHandler.isValidIP('192.168.0.1'));
        // Public IP
        System.assert(ESASIRuleHandler.isValidIP('66.231.80.0'));
        // Invalid IP
        System.assert(!ESASIRuleHandler.isValidIP('192.0.1'));
        // Invalid IP
        System.assert(!ESASIRuleHandler.isValidIP('192.0.1.test'));
    }
    
    @isTest
    static void testIsPublicIP() {
        // Public IP
        System.assert(ESASIRuleHandler.isPublicIP(null));
        System.assert(ESASIRuleHandler.isPublicIP('66.231.80.0'));
        System.assert(ESASIRuleHandler.isPublicIP('198.245.80.0'));
        System.assert(ESASIRuleHandler.isPublicIP('13.210.4.0'));
        System.assert(ESASIRuleHandler.isPublicIP('85.222.159.255'));
        System.assert(ESASIRuleHandler.isPublicIP('182.50.79.255'));
        // Private IP
        System.assert(!ESASIRuleHandler.isPublicIP('127.0.0.1'));
        System.assert(!ESASIRuleHandler.isPublicIP('192.168.0.1'));
        System.assert(!ESASIRuleHandler.isPublicIP('172.168.0.12'));
        System.assert(!ESASIRuleHandler.isPublicIP('10.168.10.1'));
        // Invalid IP
        System.assert(!ESASIRuleHandler.isPublicIP('192.0.1.df'));
    }
    
    @isTest
    static void testIsIPRange() {
        System.assert(ESASIRuleHandler.isIPRange(null, null, 1));
        System.assert(ESASIRuleHandler.isIPRange('192.168.0.1', null, 1));
        System.assert(!ESASIRuleHandler.isIPRange('192.168.0.test', null, 1));
        System.assert(ESASIRuleHandler.isIPRange(null, '192.168.0.10', 1));
        System.assert(!ESASIRuleHandler.isIPRange(null, '192.168.0.test', 1));
        System.assert(ESASIRuleHandler.isIPRange('192.168.0.1', '192.168.0.10', 1));
        System.assert(ESASIRuleHandler.isIPRange('80.151.155.239', '80.151.155.240', 1));
        // Check when start and end has same IP
        System.assert(ESASIRuleHandler.isIPRange('80.151.155.239', '80.151.155.239', 1));
        // Check invalid range
        System.assert(!ESASIRuleHandler.isIPRange('172.168.0.1', '192.168.0.10', 1));
        
        // Check whether first 1,2,3 parts are same
        System.assert(ESASIRuleHandler.isIPRange('80.151.155.239', '80.152.155.240', 1));
        System.assert(ESASIRuleHandler.isIPRange('80.151.155.239', '80.151.156.240', 2));
        System.assert(ESASIRuleHandler.isIPRange('80.151.155.239', '80.151.156.240', 1));
        System.assert(ESASIRuleHandler.isIPRange('80.151.155.239', '80.151.155.240', 2));
        System.assert(ESASIRuleHandler.isIPRange('80.151.155.239', '80.151.155.240', 3));
        System.assert(!ESASIRuleHandler.isIPRange('80.151.155.239', '80.152.155.240', 2));
        System.assert(!ESASIRuleHandler.isIPRange('80.151.155.239', '80.151.156.240', 3));
    }
}
public class CDI_SyncExternalObjects_Weekly implements Schedulable {
    public void execute(SchedulableContext sc){
       
        CDI_SyncGusTeamExternalObjects team=new CDI_SyncGusTeamExternalObjects();
        Database.executeBatch(team);
        CDI_SyncExternalGusUser user=new CDI_SyncExternalGusUser();
        Database.executeBatch(user);
        CDI_Gus_Service_Sync.syncGusService();
    }
}
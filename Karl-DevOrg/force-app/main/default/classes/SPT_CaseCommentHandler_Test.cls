@isTest
public class SPT_CaseCommentHandler_Test {
    @TestSetup
    static void makeData(){
        List<Account> accountList = SPT_TestUtility.createAccountRecords('SPT Test Account', 5, true);
        List<Contact> contactList = SPT_TestUtility.createContactRecords('SPT Test Contact', 5, false);
        List<Case> caseList = SPT_TestUtility.createCaseRecords('In Progress', 'USD', 'Email', 5, false);

        Profile p = [SELECT Id
                     FROM Profile 
                     WHERE Name = 'System Administrator'];
        User u = new User(Alias = 'standt', Email='systemAdmin@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Test User', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='systemAdmin@testorg.com');

        for(Integer i=0; i<contactList.size(); i++) {
            Contact contactRecord = contactList.get(i);
            contactRecord.AccountId = accountList.get(i).Id;
        }
        if(!contactList.isEmpty()) {
            INSERT contactList;
        }

        for(Integer i=0; i<caseList.size(); i++) {
            Case caseRecord = caseList.get(i);
            caseRecord.ContactId = contactList.get(i).Id;
        }
        if(!caseList.isEmpty()) {
            INSERT caseList;
        }
    }

    @isTest static void getCaseCommentBodyTest() {
        List<User> userList = [SELECT Id, Name
                               FROM User limit 1];
        
        System.runAs(userList.get(0)) {
            List<Case> caseList = new List<Case>([SELECT Id
                                                  FROM Case
                                                  LIMIT 1]);
            String templateBody = SPT_CaseCommentHandler.getCaseCommentBody(caseList.get(0).Id, 'Example Name');
            String templateBodyText = SPT_CaseCommentHandler.getTemplateBody('Example Name');
            System.assertEquals(true, templateBody != null);

            // NULL Test
            templateBody = SPT_CaseCommentHandler.getCaseCommentBody(caseList.get(0).Id, 'Template 1');
            templateBodyText = SPT_CaseCommentHandler.getTemplateBody('Template 1');
            System.assertEquals(null, templateBody);
        }
    }

    @isTest static void getAllPublishedTemplateTypesTest() {
        List<String> templateTypes = SPT_CaseCommentHandler.getAllPublishedTemplateTypes();
        System.assertEquals(18, templateTypes.size());
    }

    @isTest static void createCaseCommentRecordTest() {
        List<Id> caseIdList = new List<Id>();
        List<Case> caseList = new List<Case>([SELECT Id
                                              FROM Case
                                              LIMIT 1]);
        caseIdList.add(caseList.get(0).Id);
        SPT_CaseCommentHandler.createCaseCommentRecord(caseIdList, 'Hi Jim, Your Status is In Progress', true, false);
        List<CaseComment> caseCommentList = new List<CaseComment>([SELECT Id, ParentId
                                                                   FROM CaseComment
                                                                   WHERE ParentId =: caseList.get(0).Id]);
        System.assertEquals(1, caseCommentList.size());
    }
    
    @isTest static void createCaseCommentRecordTestBulk() {
        List<Id> caseIdList = new List<Id>();
        List<Case> caseList = new List<Case>([SELECT Id
                                              FROM Case
                                              LIMIT 1]);
        caseIdList.add(caseList.get(0).Id);
        SPT_CaseCommentHandler.createCaseCommentRecord(caseIdList, 'Hi {!Contact.FirstName}, Your Status is In Progress', true, true);
        List<CaseComment> caseCommentList = new List<CaseComment>([SELECT Id, ParentId
                                                                   FROM CaseComment
                                                                   WHERE ParentId =: caseList.get(0).Id]);
        System.assertEquals(1, caseCommentList.size());
    }
}
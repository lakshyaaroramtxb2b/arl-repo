/**
 * Trust Maturity Model v2
 * March 2016
 * Written By: Jorge L Caceres (J-team member J3)
 * 
 * Test class for the following functionality living in TM_BUsharingUtil
 *  
**/

@isTest
private class TEST_TM_BUsharingUtil {
    
    // create test common data
    private static Account buzUnit1;
    private static Account buzUnit2;
    
    private static IRCloud__Environment__c env1;
    private static IRCloud__Environment__c env2;
    private static IRCloud__Environment__c env3;
     
    private static TM_AbstractObjective__c absObjective1;
    private static TM_AbstractObjective__c absObjective2;
    private static TM_AbstractObjective__c absObjective3;
    private static TM_AbstractObjective__c absObjective4;
    private static TM_AbstractObjective__c absObjective5;

    private static TM_Placement__c placement1;
    private static TM_Placement__c placement2;
    private static TM_Placement__c placement3;
    private static TM_Placement__c placement4;
    private static TM_Placement__c placement5;
    
    @testSetup 
    static void generateTestData() {
 
        buzUnit1 = TEST_TM_Util.generateAccount(TM_Constants.BU_ACCOUNT_REC_TYPE_ID);
        buzUnit2 = TEST_TM_Util.generateAccount(TM_Constants.BU_ACCOUNT_REC_TYPE_ID);
        insert new Account[] { buzUnit1, buzUnit2 };        

        env1 = TEST_TM_Util.generateEnvironment(TM_Constants.PARENT_INFRA_IT_ENVIRONMENT_REC_TYPE_ID, buzUnit1.Id);
        env2 = TEST_TM_Util.generateEnvironment(TM_Constants.PARENT_INFRA_PROD_ENVIRONMENT_REC_TYPE_ID, buzUnit1.Id);
        env3 = TEST_TM_Util.generateEnvironment(TM_Constants.OTHER_ENVIRONMENT_REC_TYPE_ID, buzUnit2.Id);
        insert new IRCloud__Environment__c[] { env1, env2, env3 };        
        
        absObjective1 = TEST_TM_Util.generateTMAbstractObjective();
        absObjective2 = TEST_TM_Util.generateTMAbstractObjective();
        absObjective3 = TEST_TM_Util.generateTMAbstractObjective();
        absObjective4 = TEST_TM_Util.generateTMAbstractObjective();
        absObjective5 = TEST_TM_Util.generateTMAbstractObjective();
        insert new TM_AbstractObjective__c[] { absObjective1, absObjective2, absObjective3, absObjective4, absObjective5 };        
        
        placement1 = TEST_TM_Util.generateTMPlacement(absObjective1.Id, 'SIR', '1', 1, TM_Constants.BU_ENVIRONMENT_TYPE); 
        placement2 = TEST_TM_Util.generateTMPlacement(absObjective2.Id, 'SIR', '1', 1, TM_Constants.BU_ENVIRONMENT_TYPE); 
        placement3 = TEST_TM_Util.generateTMPlacement(absObjective3.Id, 'SIR', '1', 1, TM_Constants.PARENT_INFRA_IT_ENVIRONMENT_NAME); 
        placement4 = TEST_TM_Util.generateTMPlacement(absObjective4.Id, 'SIR', '1', 1, TM_Constants.PARENT_INFRA_PROD_ENVIRONMENT_NAME); 
        placement5 = TEST_TM_Util.generateTMPlacement(absObjective5.Id, 'SIR', '1', 1, TM_Constants.OTHER_ENVIRONMENT_NAME); 
        insert new TM_Placement__c[] { placement1, placement2, placement3, placement4, placement5 };                      
    }

    static testMethod void testCreationOfGroups() {
        // test call to public method
        List<Account> newBuzUnits = new List<Account>([select Id, Name from Account]);
        System.assertEquals(2, newBuzUnits.size());
        
        test.startTest();

        // enable TMM for buz units to trigger account sharing setup
        system.runAs(new User(Id = UserInfo.getUserId())) {                
            for (Account acct : newBuzUnits) {
                acct.TMM_Tracking__c = true;
            }
            update newBuzUnits;
        }

        test.stopTest();      

        // verify that groups were created
        List<String> groupNames = new List<String>();
        List<String> observerGroupNames = new List<String>();
        for (Account acct : newBuzUnits) {
            groupNames.add(TM_BUsharingUtil.getBUDoersName(acct.Id));
            groupNames.add(TM_BUsharingUtil.getBUObserversName(acct.Id));
            observerGroupNames.add(TM_BUsharingUtil.getBUObserversName(acct.Id));
        }
        
        List<Group> newBuzUnitGroups = new List<Group>([select Id, Name, DeveloperName, Type from Group where DeveloperName IN :groupNames]);
        Map<String, Group> newBuzUnitGroupsMap = new Map<String, Group>();
        for (Group g : newBuzUnitGroups) newBuzUnitGroupsMap.put(g.DeveloperName, g);        

        System.assertEquals(4, newBuzUnitGroups.size());
        
        // verify public methods to get groupId
        for (Account acct : newBuzUnits) {
            system.assertEquals(newBuzUnitGroupsMap.get(TM_BUsharingUtil.getBUDoersName(acct.Id)).Id, TM_BUsharingUtil.getBUDoersGroupId(acct.Id));    
            system.assertEquals(newBuzUnitGroupsMap.get(TM_BUsharingUtil.getBUObserversName(acct.Id)).Id, TM_BUsharingUtil.getBUObserversGroupId(acct.Id));    
        }
        system.assertEquals(null, TM_BUsharingUtil.getBUObserversGroupId(newBuzUnitGroups[0].Id));    
        
        
        // verify that doers have been added to observer groups
        List<GroupMember> newGroupMembers = new List<GroupMember>([select GroupId, UserOrGroupId from GroupMember 
                                                                   where (GroupId = :newBuzUnitGroupsMap.get(observerGroupNames[0]).Id
                                                                   or GroupId = :newBuzUnitGroupsMap.get(observerGroupNames[1]).Id)]);
        Map<Id, GroupMember> newGroupMembersMap = new Map<Id, GroupMember>();
        for (GroupMember gm : newGroupMembers) newGroupMembersMap.put(gm.GroupId, gm);        

        System.assertEquals(2, newGroupMembers.size());

        Id acctDoersGroupId;
        Id acctObserversGroupId;          
        for (Account acct : newBuzUnits) {
            acctDoersGroupId = newBuzUnitGroupsMap.get(TM_BUsharingUtil.getBUDoersName(acct.Id)).Id;
            acctObserversGroupId = newBuzUnitGroupsMap.get(TM_BUsharingUtil.getBUObserversName(acct.Id)).Id;            
            System.assert(newGroupMembersMap.containsKey(acctObserversGroupId));
            System.assertEquals(acctDoersGroupId, newGroupMembersMap.get(acctObserversGroupId).UserOrGroupId);
        }
        
        // verify that sharing rules were created for both buz units
        List<AccountShare> newBuzUnitRules = new List<AccountShare>([select AccountId, UserOrGroupId, AccountAccessLevel from AccountShare where AccountId IN :newBuzUnits]);
        System.assertEquals(4, newBuzUnitRules.size());
        Integer bu1shares = 0;
        Integer bu2shares = 0;        
        for (AccountShare ashare : newBuzUnitRules) {
            if (ashare.AccountId == newBuzUnits[0].Id) bu1shares++;
            if (ashare.AccountId == newBuzUnits[1].Id) bu2shares++;         
        }
        System.assertEquals(2, bu1shares);
        System.assertEquals(2, bu2shares);        
        
    }
    

    static testMethod void testCreationOfObjectiveShares() {
        // assert test data is as expected        
        List<Account> newBuzUnits = new List<Account>([select Id, Name from Account]);
        System.assertEquals(2, newBuzUnits.size());

        System.assertEquals(5, [select count() from TM_Placement__c]);
        
        test.startTest();

        // enable TMM for buz units to trigger account sharing setup
        system.runAs(new User(Id = UserInfo.getUserId())) {                
            for (Account acct : newBuzUnits) {
                acct.TMM_Tracking__c = true;
            }
            update newBuzUnits;
        }
        List<String> groupNames = new List<String>();
        for (Account acct : newBuzUnits) {
            groupNames.add(TM_BUsharingUtil.getBUDoersName(acct.Id));
            groupNames.add(TM_BUsharingUtil.getBUObserversName(acct.Id));
        }
        List<Group> newBuzUnitGroups = new List<Group>([select Id, Name, DeveloperName, Type from Group where DeveloperName IN :groupNames]);
        Map<String, Group> newBuzUnitGroupsMap = new Map<String, Group>();
        for (Group g : newBuzUnitGroups) newBuzUnitGroupsMap.put(g.DeveloperName, g);        
        System.assertEquals(4, newBuzUnitGroups.size());

        test.stopTest();      
        
        // verify objectives created properly
        Map<Id, TM_Objective__c> newObjectivesMap = new Map<Id, TM_Objective__c>([select Id, Name, Business_Unit__c, Business_Unit_Id_Source__c
                                                                                  from TM_Objective__c where Business_Unit__c IN :newBuzUnits]);
        System.assertEquals(4, newObjectivesMap.size());
        Integer bu1objectives = 0;
        Integer bu2objectives = 0;        
        for (TM_Objective__c objective : newObjectivesMap.values()) {
            if (objective.Business_Unit__c == newBuzUnits[0].Id) bu1objectives++;
            if (objective.Business_Unit__c == newBuzUnits[1].Id) bu2objectives++;         
        }
        System.assertEquals(2, bu1objectives);
        System.assertEquals(2, bu2objectives);  
        
        // verify shares were created properly
        List<TM_Objective__Share> newObjectiveShares = new List<TM_Objective__Share>([select AccessLevel, ParentId, RowCause, UserOrGroupId 
                                                                                      from TM_Objective__Share where ParentId IN :newObjectivesMap.values()
                                                                                      and RowCause = 'Manual']);
        System.assertEquals(8, newObjectiveShares.size());
        Integer bu1objectiveShares = 0;
        Integer bu2objectiveShares = 0;        
        Integer bu3objectiveShares = 0;
        Integer bu4objectiveShares = 0;        
        for (TM_Objective__Share objectiveShare : newObjectiveShares) {
            System.assert(isObjectiveShareCorrect(objectiveShare, 
                          newObjectivesMap.get(objectiveShare.ParentId), 
                          newBuzUnitGroupsMap));
            if (objectiveShare.ParentId == newObjectivesMap.values()[0].Id) bu1objectiveShares++;
            if (objectiveShare.ParentId == newObjectivesMap.values()[1].Id) bu2objectiveShares++;         
            if (objectiveShare.ParentId == newObjectivesMap.values()[2].Id) bu3objectiveShares++;
            if (objectiveShare.ParentId == newObjectivesMap.values()[3].Id) bu4objectiveShares++;         
        }
        System.assertEquals(2, bu1objectiveShares);
        System.assertEquals(2, bu2objectiveShares);
        System.assertEquals(2, bu3objectiveShares);
        System.assertEquals(2, bu4objectiveShares);
        
        // verify user access
        User user1 = TEST_TM_Util.generateUser(TEST_TM_Util.TM_TMM_EXTERNAL_PROFILE_ID);
        User user2 = TEST_TM_Util.generateUser(TEST_TM_Util.TM_TMM_EXTERNAL_PROFILE_ID);
        User user3 = TEST_TM_Util.generateUser(TEST_TM_Util.TM_TMM_EXTERNAL_PROFILE_ID);
        system.runAs(new User(Id = UserInfo.getUserId())) {
            insert new User[] { user1, user2, user3 };
        } 
        PermissionSetAssignment permUser1 = new PermissionSetAssignment (AssigneeId = user1.Id, 
                                            PermissionSetId = TEST_TM_Util.TM_VALIDATOR_PERMSET_ID);
        PermissionSetAssignment permUser2 = new PermissionSetAssignment (AssigneeId = user2.Id, 
                                            PermissionSetId = TEST_TM_Util.TM_IMPLEMENTER_PERMSET_ID);
        system.runAs(new User(Id = UserInfo.getUserId())) {
            insert new PermissionSetAssignment[] { permUser1, permUser2 };
        } 
        
        // add user1 to buz unit 1 as observers and user 2 to buz unit 2 as doer
        GroupMember gm1 = new GroupMember(UserOrGroupId = user1.Id, GroupId = newBuzUnitGroupsMap.get(TM_BUsharingUtil.getBUObserversName(newBuzUnits[0].Id)).Id);
        GroupMember gm2 = new GroupMember(UserOrGroupId = user2.Id, GroupId = newBuzUnitGroupsMap.get(TM_BUsharingUtil.getBUDoersName(newBuzUnits[1].Id)).Id);
        system.runAs(new User(Id = UserInfo.getUserId())) {
            insert new GroupMember[] { gm1, gm2 }; 
        }  
        
        // user 1 should only see the objectives for buz unit 1 and not have update crud
        system.runAs(user1) {
            List<TM_Objective__c> BUobjectives = [select Business_Unit_Id_Source__c from TM_Objective__c];
            system.assertEquals(2, BUobjectives.size());
            for (TM_Objective__c o : BUobjectives) system.assertEquals(id.valueOf(o.Business_Unit_Id_Source__c), newBuzUnits[0].Id);
            try {
                update BUobjectives;
                system.assert(true, 'Update should have failed');   
            }   catch (exception e) {}
        } 
        
        // user 2 should only see the objectives for buz unit 2 (3 objectives, 2 from BU 1 from env) and have update crud
        system.runAs(user2) {
            List<TM_Objective__c> BUobjectives = [select Business_Unit_Id_Source__c from TM_Objective__c];
            system.assertEquals(3, BUobjectives.size());
            for (TM_Objective__c o : BUobjectives) system.assertEquals(id.valueOf(o.Business_Unit_Id_Source__c), newBuzUnits[1].Id);
            try {
                update BUobjectives;
            }   catch (exception e) {
                system.assert(true, 'Update should NOT have failed');                   
            }
        } 
        
        // user 3 should not see any objectives
        system.runAs(user3) {
            system.assertEquals(0, [select count() from TM_Objective__c]);
        } 
                     
                              
    }   

    static testMethod void testCreateOfGroupsWhenMissing () {
        // assert test data is present
        List<Account> newBuzUnits = new List<Account>([select Id, Name from Account]);
        System.assertEquals(2, newBuzUnits.size());

        List<String> groupNames = new List<String>();
        groupNames.add(TM_BUsharingUtil.getBUDoersName(newBuzUnits[0].Id));
        groupNames.add(TM_BUsharingUtil.getBUObserversName(newBuzUnits[0].Id));
        
        test.startTest();

        // enable TMM for buz units for first account to trigger account sharing setup
        system.runAs(new User(Id = UserInfo.getUserId())) { 
            newBuzUnits[0].TMM_Tracking__c = true;
            update newBuzUnits[0];              
        }

        test.stopTest();      
        
        // delete all data created after asserting it was created
        system.assertNotEquals(0, [select count() from GroupMember where GroupId IN :([select Id from Group where DeveloperName IN : groupNames])]);
        delete [select Id from TM_Objective__c where Business_Unit_Id_Source__c = :newBuzUnits[0].Id];
        system.runAs(new User(Id = UserInfo.getUserId())) { 
            delete [select Id from GroupMember where GroupId IN :([select Id from Group where DeveloperName IN : groupNames])];
            delete [select Id from Group where DeveloperName IN : groupNames];
        } 
        TM_BUsharingUtil.allGroupsMap = null;       
        system.assertEquals(0, [select count() from TM_Objective__c where Business_Unit_Id_Source__c = :newBuzUnits[0].Id]);
        system.assertEquals(0, [select count() from GroupMember where GroupId IN :([select Id from Group where DeveloperName IN : groupNames])]);
        system.assertEquals(0, [select count() from Group where DeveloperName IN : groupNames]);
        
        // update account then assert groups are recreated (only groups as groupmembers are created in future)
        update newBuzUnits[0];
        system.assertNotEquals(0, [select count() from Group where DeveloperName IN : groupNames]);
 
    }
    
    private static Boolean isObjectiveShareCorrect(TM_Objective__Share objectiveShare, TM_Objective__c objective, Map<String, Group> newBuzUnitGroupsMap) {
        // verify that for read the assigned group is the buz unit obververs group and for edit it is the doers group
        if (objectiveShare.AccessLevel == 'Read') {
            return (objectiveShare.UserOrGroupId == newBuzUnitGroupsMap.get(TM_BUsharingUtil.getBUObserversName(Id.valueOf(objective.Business_Unit_Id_Source__c))).Id);
        } else if (objectiveShare.AccessLevel == 'Edit') {
            return (objectiveShare.UserOrGroupId == newBuzUnitGroupsMap.get(TM_BUsharingUtil.getBUDoersName(Id.valueOf(objective.Business_Unit_Id_Source__c))).Id);            
        } else return false;
    }
}
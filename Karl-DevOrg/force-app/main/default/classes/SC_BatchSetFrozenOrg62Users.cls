/* 
Developer: Jake 9/9/18 <jake.hebert@callawaycloudconsulting.com>
Batch to sync frozen contacts with Org62 user records
JIRA SFS-103
*/
public class SC_BatchSetFrozenOrg62Users implements Database.Batchable<sObject>, Database.Stateful{
    
    Map<String, List<String>> errorsWithIds = new Map<String, List<String>>();
    
    public Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([
            SELECT Org62_User_Id__c, Is_Frozen__c
            FROM Contact
            WHERE RecordType.DeveloperName = 'Salesforce_Employee'
            AND Org62_User_Id__c LIKE '005%']);
    }

    public void execute(Database.BatchableContext BC, List<Contact> contacts) {

        Map<Id, Contact> contactMap = new Map<Id, Contact>();
        for (Contact contact : contacts) {
            if (SC_TrainingUtil.isValidUserId(contact.Org62_User_Id__c)) {
                contactMap.put(contact.Org62_User_Id__c, contact);
            } else {
                // If there is a bad User Id, add a message to the "errorsWithIds" map
                String errMsg = 'Bad Org62 User Id';
                if (!errorsWithIds.containsKey(errMsg)) {
                    errorsWithIds.put(errMsg, new String[0]);
                }
                errorsWithIds.get(errMsg).add(contact.Id);
            }
        }
        Contact[] needsUpdate = new Contact[0];
        for (Org62UserLogin__x userLogin : [SELECT UserId__c, IsFrozen__c FROM Org62UserLogin__x WHERE UserId__c IN :contactMap.keySet()]) {
            Contact contact = contactMap.get(userLogin.UserId__c);
            if (contact.Is_Frozen__c != userLogin.IsFrozen__c) {
                contact.Is_Frozen__c = userLogin.IsFrozen__c;
                needsUpdate.add(contact);
            }
        }
        List<Database.SaveResult> srExecuteList = Database.update(needsUpdate,false);
        // Use static method "appendErrorMap" in ErrorHelper class to process results   
        // Passing params: SaveResult, List of sObjects that were updated, Map to append results to
        errorsWithIds = ErrorHelper.appendErrorMap(srExecuteList,needsUpdate,errorsWithIds); 
    }
    
    public void finish(Database.BatchableContext BC) {

        // Send email for failed records
        if (errorsWithIds.size() > 0) {       
            String toEmails = Label.SC_Frozen_Users_Batch_Failure_Emails;
            List<String> toEmailList = toEmails.split(',');
            String emailBody = ErrorHelper.getErrorSummary(errorsWithIds);
            String emailSubj = 'Frozen User Update Batch Errors: ' + String.valueOf(Datetime.now());

            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
            email.setToAddresses(toEmailList);
            email.setSubject(emailSubj);
            email.setPlainTextBody(emailBody);
            if (!Test.isRunningTest()) {
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { email });
            }
        }
    }
}
/**
 * Update Employee Managers information to Contact Object
 * Execute batch : SyncContact_EmployeesManagersInfo.scheduleJob('0 30 00 * * ? *');
 */
global without sharing class SyncContact_EmployeesManagersInfo implements Schedulable,
                                                              Database.Batchable<sObject>,
                                                              Database.AllowsCallouts,
                                                              Database.Stateful {

    private static final String DAILY = '0 30 00 * * ?'; 
    
    private static final String SOURCE_FILE = 'SyncContact_EmployeesManagersInfo';
    
    public SyncContact_EmployeesManagersInfo() {

    }

    public static String scheduleJob() {
        return scheduleJob(DAILY);
    }      
    
    public static Id scheduleJob(String cronExpression) {
        return scheduleJob(cronExpression, null);
    }

    public static Id scheduleJob(String cronExpression, String eventName) {
        String jobName = SOURCE_FILE + ' ' + System.now();
        String jobId = System.schedule(jobName, cronExpression, new SyncContact_EmployeesManagersInfo());
        return jobId;
    }

    global void execute(SchedulableContext sc) {
        Database.executeBatch(this,1);
    }

    global Database.QueryLocator start(Database.BatchableContext context){
        String query = 'SELECT ID,Manager_First_Name__c,Manager_Email__c,'
                       +'Manager_Last_Name__c,Manager_ID__c,Email,'
                       +'Management_Chain_Level_01__c,Management_Chain_Level_02__c,'
                       +'Management_Chain_Level_03__c,Management_Chain_Level_04__c,'
                       +'Management_Chain_Level_05__c,Management_Chain_Level_06__c,'
                       +'Management_Chain_Level_07__c,Management_Chain_Level_08__c,'
                       +'Management_Chain_Level_09__c,Management_Chain_Level_10__c'
                       +' FROM Contact WHERE Is_Active__c = true '
                       +' AND Email LIKE  \'%@salesforce.com\''
                       +' AND RecordTypeId = \'01230000001GfV5\'';

        return Database.getQueryLocator(query);
    }                                                      
   
   global void execute(Database.BatchableContext BC, List<sObject> scope){
        try {
          String managerId, eId, Integ_Manager_Id;
          List<String> lstManagers;
          Set<String> empEmails = new Set<String>();
          for(sObject sObj : scope) {
            Contact conobj = (Contact) sObj;
            empEmails.add(conobj.Email);
          }

          List<INTEG_Employee__c> lstEmployees =  [SELECT Id,INTEG_Email__c, INTEG_Manager_Email__c,
                                                          Name,INTEG_Manager__c,INTEG_Manager_ID__c,
                                                          INTEG_Org62UserId__c,INTEG_Employee_ID__c,
                                                          INTEG_Manager__r.INTEG_First_Name__c,
                                                          INTEG_Manager__r.INTEG_Last_Name__c,
                                                          INTEG_Manager_Level_1__c,INTEG_Manager_Level_10__c,
                                                          INTEG_Manager_Level_2__c,INTEG_Manager_Level_3__c,
                                                          INTEG_Manager_Level_4__c,INTEG_Manager_Level_5__c,
                                                          INTEG_Manager_Level_6__c,INTEG_Manager_Level_7__c,
                                                          INTEG_Manager_Level_8__c,INTEG_Manager_Level_9__c 
                                                   FROM INTEG_Employee__c 
                                                   WHERE INTEG_Email__c IN :empEmails];
          
          Map<String,INTEG_Employee__c> mapEmployees = new Map<String,INTEG_Employee__c>();

          for(INTEG_Employee__c empl : lstEmployees) {
            mapEmployees.put(empl.INTEG_Email__c,empl);
          }

          Contact cont;
          INTEG_Employee__c employee;
          List<Contact> lstContacts = new List<Contact>();
          
          for(sObject s : scope) {
            cont = (Contact)s;
            employee = mapEmployees.get(cont.Email);
            if(employee != null) {
              cont.Manager_First_Name__c = employee.INTEG_Manager__r.INTEG_First_Name__c;
              cont.Manager_Email__c = employee.INTEG_Manager_Email__c;
              cont.Manager_Last_Name__c = employee.INTEG_Manager__r.INTEG_Last_Name__c;
              cont.Manager_ID__c = employee.INTEG_Manager_ID__c;
              cont.Management_Chain_Level_01__c = employee.INTEG_Manager_Level_1__c;
              cont.Management_Chain_Level_02__c = employee.INTEG_Manager_Level_2__c;
              cont.Management_Chain_Level_03__c = employee.INTEG_Manager_Level_3__c;
              cont.Management_Chain_Level_04__c = employee.INTEG_Manager_Level_4__c;
              cont.Management_Chain_Level_05__c = employee.INTEG_Manager_Level_5__c;
              cont.Management_Chain_Level_06__c = employee.INTEG_Manager_Level_6__c;
              cont.Management_Chain_Level_07__c = employee.INTEG_Manager_Level_7__c;
              cont.Management_Chain_Level_08__c = employee.INTEG_Manager_Level_8__c;
              cont.Management_Chain_Level_09__c = employee.INTEG_Manager_Level_9__c;
              cont.Management_Chain_Level_10__c = employee.INTEG_Manager_Level_10__c;
              lstContacts.add(cont);
            }

          }

         if(lstContacts.size() > 0) update lstContacts;
          
        }catch(exception ex) {
          Esa_DebugService.writeException(ex, 1, SOURCE_FILE,'SyncContact_EmployeesManagersInfo.finish: Failed with errors :Unhandled exception');
        }  
   }

  
       global void finish(Database.BatchableContext BC){

}                                                  
}
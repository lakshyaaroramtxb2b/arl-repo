public class SC_Email_External_CommunicationTUtils {
    
    
    public static List<EmailTemplate> listOfEmailTemplates;
    
    public static List<EmailTemplate> createEmailTemplates(Boolean isTrue){
        listOfEmailTemplates = new List<EmailTemplate>();
        
        EmailTemplate firstEmailTemplate = new EmailTemplate();
        firstEmailTemplate.isActive = true;
        firstEmailTemplate.Name = 'Test Email';
        firstEmailTemplate.DeveloperName = 'Test_Email_First';
        firstEmailTemplate.TemplateType = 'text';
        firstEmailTemplate.FolderId = UserInfo.getUserId(); 
        listOfEmailTemplates.add(firstEmailTemplate);
        
        EmailTemplate secondEmailTemplate = new EmailTemplate();
        secondEmailTemplate.isActive = true;
        secondEmailTemplate.Name = 'Test Email5';
        secondEmailTemplate.DeveloperName = 'Test_Email_Second';
        secondEmailTemplate.TemplateType = 'text';
        secondEmailTemplate.FolderId = UserInfo.getUserId();
        listOfEmailTemplates.add(secondEmailTemplate);
        
        EmailTemplate thirdEmailTemplate = new EmailTemplate();
        thirdEmailTemplate.isActive = true;
        thirdEmailTemplate.Name = 'Test Email2';
        thirdEmailTemplate.DeveloperName = 'Test_Email_Third';
        thirdEmailTemplate.TemplateType = 'text';
        thirdEmailTemplate.FolderId = UserInfo.getUserId();      
        listOfEmailTemplates.add(thirdEmailTemplate);
        
        EmailTemplate overdueEmailTemplate = new EmailTemplate();
        overdueEmailTemplate.isActive = true;
        overdueEmailTemplate.Name = 'Test Email2';
        overdueEmailTemplate.DeveloperName = 'Test_Email_Overdue';
        overdueEmailTemplate.TemplateType = 'text';
        overdueEmailTemplate.FolderId = UserInfo.getUserId();      
        listOfEmailTemplates.add(overdueEmailTemplate);
        
        
        //insert listOfEmailTemplates;
        if(isTrue) {
            insert listOfEmailTemplates;
            return listOfEmailTemplates;
        } else{
            return null;
        }
    }
    
    
    public static SC_CISO_CampaignMember__c createCampaignMemberData() {
        SC_CISO_CampaignMember__c cmpData = new SC_CISO_CampaignMember__c(Name = 'Test User', Email__c = 'abcd.test12@gmail.com',
                                                                          FirstName__c = 'Test', LastName__c = 'LastName', Status__c = 'abc');
        
        insert cmpData;
        return cmpData;
    }
    
}
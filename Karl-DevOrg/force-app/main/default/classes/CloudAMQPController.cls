public with sharing class CloudAMQPController {
    public static void connectAMQP(String exchngName, String method, String payload) {
        HttpRequest req = new HttpRequest();
        try{
            if(exchngName != null && method != null){
                req.setTimeout(120000);
                req.setMethod(method);
                req.setHeader('Content-Type', 'application/json');
                //this path is different for dev
                //req.setEndpoint('callout:cloudamqp_nm_dev/api/exchanges/rgmvoyvj/amq.default/publish');
                //prod
                req.setEndpoint('callout:cloudamqp_nm/api/exchanges/tkosawwe/amq.default/publish');
                if(payload != null){
                    req.setBody(payload);
                }
                System.debug('sending to API: '+req.getEndpoint());
                Http http = new Http();
                //System.debug('req: '+payload);
                http.send(req);
            } 
        } catch(Exception ex) {
            System.debug('Exception occured'+ex.getMessage()+ex.getStackTraceString());
        }
    }

    private Static String reqSerializer(String payload) {
        JSONGenerator generator = JSON.createGenerator(false);
        generator.writeStartObject();
        generator.writeStringField('routing_key','alerts');
        generator.writeFieldName('properties');
        generator.writeStartObject();
        generator.writeEndObject();
        generator.writeStringField('payload', payload);
        generator.writeStringField('payload_encoding', 'string');
        generator.writeEndObject();
        //System.debug('generator.getAsString(): '+generator.getAsString());
        return generator.getAsString();
    }

    @future(callout=true)
    public Static void sendRequest(String payload){
        String serializePayload = reqSerializer(payload);
        connectAMQP('amq.default','POST',serializePayload);
    }
    
    //to call from batch job as future methods can't be called directly
    public Static void sendReq(String payload){
        String serializePayload = reqSerializer(payload);
        connectAMQP('amq.default','POST',serializePayload);
    }
}
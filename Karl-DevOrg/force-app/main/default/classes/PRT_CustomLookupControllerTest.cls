@isTest
public class PRT_CustomLookupControllerTest {
    @testSetup
    public static void testSetup(){
        List<User> userList = new List<User>();
        userList = PRT_TestDataFactory.createUsers(2,'System Administrator',true);
    }
    static testMethod void fetchRecordsTest(){
        User userRecord = [SELECT Id,Name FROM USER WHERE Email Like 'PRTUser%' LIMIT 1];
        userRecord.FirstName = 'Test Measure';
        update userRecord;
        System.debug('User Name' + userRecord.name);
        
        test.startTest();
        //PRT_CustomLookupController.RecordsData recordDataList = new PRT_CustomLookupController.RecordsData('test','test');
        PRT_CustomLookupController.fetchRecords('User','Name','est','',String.valueOf(userRecord.id));
        test.stopTest();
        
    }
    static testMethod void fetchRecordsTest1(){
        User userRecord = [SELECT Id,Name FROM USER WHERE Email Like 'PRTUser%' LIMIT 1];
        userRecord.FirstName = 'Test Measure';
        userRecord.EmployeeNumber = '725957';
        update userRecord;
        System.debug('user Record' + userRecord);
        test.startTest();
        //PRT_CustomLookupController.RecordsData recordDataList = new PRT_CustomLookupController.RecordsData('test','test');
        PRT_CustomLookupController.fetchRecords('Measure_c__x','MeasureName_c__c','trustCurrentAlerts','',String.valueOf(userRecord.id));
        test.stopTest();
        
    }
    static testMethod void deleteV2MappingTest(){
        List<User> userRecord = [SELECT Id,Name FROM USER WHERE Email Like 'PRTUser%' LIMIT 2];
        List<V2MomMapping__c> vmapList = new List<V2MomMapping__c>();
        V2MomMapping__c vmap = new V2MomMapping__c();
        vmap.Controlling_Measure__c = userRecord[0].Id;
        vmap.Dependent_Method__c = userRecord[1].id;
        vmapList.add(vmap);
        insert vmapList;
        test.startTest(); 
        PRT_CustomLookupController.deleteV2Mapping(vmapList[0].Dependent_Method__c,vmapList[0].Controlling_Measure__c);      
        test.stopTest(); 
    }

}
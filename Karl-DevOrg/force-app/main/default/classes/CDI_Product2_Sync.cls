public class CDI_Product2_Sync {
    public static void syncProduct2(){
    	Set<String> setpL2 = new Set<String>(); 
        Set<String> setBL1 = new Set<String>();
        DateTime starttime=DateTime.now();
        List <Org62Product2__x> product=new List <Org62Product2__x>();
        for(Org62Product2__x productLine:[select L1_Business_Line_c__c,L2_Product_Line_c__c from Org62Product2__x where L1_Business_Line_c__c!='Cross Cloud Allocations' AND L1_Business_Line_c__c!= 'Other' AND L1_Business_Line_c__c!='Professional Services']){
            product.add(productLine);
            //System.debug('Product Line:'+productLine);
        }
		//List <Org62Product2__x> product=[select L1_Business_Line_c__c,L2_Product_Line_c__c from Org62Product2__x];
		List <CDI_Product__c> cdiProductList=new List<CDI_Product__c>();
        //import Business Line 1 as Products
		for(Org62Product2__x bL1 : product){
   		if (!setBL1.contains(bL1.L1_Business_Line_c__c)){
        	setBL1.add(bL1.L1_Business_Line_c__c);
            }
        }
        setBL1.remove(null);
        for (String productName : setBL1){
            CDI_Product__c cdiProd=new CDI_Product__c();
       		cdiProd.name=productName;
            //cdiProd.Source_System_Name__c='Org 62';
            //cdiProd.Source_Product_Code__c='';
       		String name=cdiProd.name;
            //cdiProd.CDI_DB_External_Id__c=cdiProd.Name;
            System.debug('Product Added for creation:'+cdiProd.Name);
            cdiProductList.add(cdiProd);
        }
        
        Schema.SObjectField blName=CDI_Product__c.Fields.Name;
		Database.upsert(cdiProductList,blName,false);
        System.debug('Import of Business Line is completed.');
        //Fetch Product Line 2 and create relationship with Parent Product
  		for (Org62Product2__x pL2 : product){
            if ((!setpL2.contains(pL2.L2_Product_Line_c__c)) && (setBL1.contains(pL2.L1_Business_Line_c__c))){
                setpL2.add(pL2.L2_Product_Line_c__c);
                //system.debug('Product Line2 '+setpL2);
                CDI_Product__c cdiProd=new CDI_Product__c();
    			cdiProd.name=pL2.L2_Product_Line_c__c;
                //cdiProd.CDI_DB_External_Id__c=cdiProd.name;
                System.debug('Business Line for Product Line'+cdiProd.Name+' is '+pL2.L1_Business_Line_c__c);
                String parentId=[select id from CDI_Product__c where name=:pL2.L1_Business_Line_c__c LIMIT 1].id;
                Id parentProductId=Id.valueOf(parentId);
                cdiProd.Parent_Product__c=parentProductId;
                //cdiProd.Source_System_Name__c='Org 62';
            	//cdiProd.Source_Product_Code__c='';
                cdiProductList.add(cdiProd);
        	}
        }
      	Schema.SObjectField plName=CDI_Product__c.Fields.Name;
		Database.upsert (cdiProductList,plName,false);
        
    }
 
}
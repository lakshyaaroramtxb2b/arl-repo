@isTest

public class Esa_Cancel_Appointment_Test {

    @isTest
    static void cancelAppointmentTest() {

        ESA_Entity__c esEntity = new ESA_Entity__c(Name='EnterPriseSec',EntityCode__c='EnteSec',Work_Items_Org__c='SupportForce',Default_Entity__c=true);
        esEntity.MeetingHours_Monday__c = '10;10:30;11;11:30';
        esEntity.MeetingHours_Tuesday__c = '15;15:30;16;16:30';
        esEntity.MeetingHours_Wednesday__c = '13:30;14;14:30;15';
        esEntity.MeetingHours_Thursday__c = '9;9:30;10;10:30';
        esEntity.MeetingHours_Friday__c = '09:30;10;10:30;11';
        esEntity.Meeting_Minutes__c = 30;
        esEntity.Need_By_Date_Min_Business_Days__c = 5;
        esEntity.Google_Calendar_Name__c = 'suresh.uppala@salesforce.com';
        insert esEntity;

        ESA_Service_Item_Category__c SCategory = new ESA_Service_Item_Category__c();
        SCategory.Name = 'test cat 1';
        SCategory.ESA_Entity__c = esEntity.id;
        SCategory.Case_Queue__c = 'ASA1';
        insert SCategory;


        ESA_Service_Item__c serviceItem = new ESA_Service_Item__c();
        serviceItem.ItemCategory__c = SCategory.Id;
        serviceItem.Name = 'test Service Item1';
        serviceItem.Office_Hours_Days__c = 'Wednesday';
        insert serviceItem;


        ESA_Security_Request__c request = new ESA_Security_Request__c();
        request.Name__c = 'test class';
        request.Priority__c = 'high';
        request.Name__c = 'test class';
        request.Phone__c = '5555551144';
        request.Email__c = 'test@test.com';
        request.Description__c = 'test class is testing';
        request.Hash_Key__c = '1234567890';
        request.GCal_EventId__c = 'kg8kp0bkbcdi5ditdcosshq91o';
        request.Office_Hours__c = '2014-02-08 18:00:00';
        request.ESA_Service_Item__c = serviceItem.Id;
        request.Supportforce_Case_Number__c = '234567';
        insert request;

        Test.startTest();

        CancelApptTestCallOutMocker  callOutMocker = new CancelApptTestCallOutMocker();
        Test.setMock(HttpCalloutMock.class, callOutMocker);

        String apptStatus  = Esa_Cancel_Appointment.cancel_Appointment(request.Id);

        system.assertEquals('Appointment canceled.',apptStatus,'appointment got deleted :');

        request = [SELECT GCal_EventId__c,Office_Hours__c FROM ESA_Security_Request__c WHERE Id=:request.Id];
        system.assertEquals(request.GCal_EventId__c,null,'Google Calendar Id :');
        system.assertEquals(request.Office_Hours__c,null,'Office hours value :');

        apptStatus  = Esa_Cancel_Appointment.cancel_Appointment(request.Id);
        system.assertEquals('No appointment scheduled for this request',apptStatus,'No appointment scheduled :');

        Test.stopTest();

    }


    public class CancelApptTestCallOutMocker implements HttpCalloutMock {
        public String customResponse;
        public Integer customResponseCode;

        public String validResponse = 'Success';

        public HTTPResponse respond(HTTPRequest req) {
            // Create a fake response
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            if (req.getEndpoint().toUpperCase().contains('APEXREST')) {
                res.setBody(validResponse);
            }
            If (customResponseCode==null) {
              res.setStatusCode(200);
            } else {
                res.setStatusCode(customResponseCode);
            }

            return res;
        }
    }
}
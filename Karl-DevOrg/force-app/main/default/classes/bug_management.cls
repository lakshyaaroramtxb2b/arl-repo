global class bug_management Implements Schedulable {
    
    global void execute(SchedulableContext sc)
        {
            start();
        }
    
    //Pull Security bugs from GUS
     public static map<string,list<string>> theme_work;
    public bug_management(){
        
    }
   
    public static void start(){
        
        Prodsec_bug_manages__c  mc = Prodsec_bug_manages__c.getInstance('00e30000001D19S');
        try{
        String query;
        map<string,ADM_Scrum_Team_c__x> team_object;
      
        
        //To maintain the state of bug with GUS; Closing it before syncing
        /**
        list<bug_management__c> Closed_bugs = new list<bug_management__c>();
        Closed_bugs =[Select id, Name FROM bug_management__c Where Closed__c = 0];
        for(bug_management__c x:Closed_bugs)
            x.Closed__c =1;
        upsert Closed_bugs Name; **/
        
        set<id> set_ids = new set<id>();
        
        //List of team ids where its Product type is Product
        
        team_object = new map<string,ADM_Scrum_Team_c__x>();
        
        //query = 'SELECT id from ADM_Scrum_Team_c__x where cloud__r.Cloud_Type_c__c Like \'%Product%\'';
        String filter_cloud_type = '%product%'; //Cloud_LU__r.Cloud_Type__c in GUS
        set<string> cloud_name = new set<string>();
        
        
        //
        for(ADM_Scrum_Team_c__x val: [SELECT id, ExternalId, cloud__r.Name__c, cloud__r.Primary__r.Name__c from ADM_Scrum_Team_c__x where cloud__r.Cloud_Type_c__c Like :filter_cloud_type AND (NOT cloud__r.Primary__r.Name__c LIKE '%infra%')])
        {
            //system.debug(val.cloud__r.Primary__r.Name__c);
            //system.debug(val.get('cloud__c'));
            team_object.put(string.valueof(val.get('ExternalId')),val);
            cloud_name.add(val.cloud__r.Name__c);
            //system.debug(val);
        }
        
   
        
        map<string, string> sec_eng = new map<string,string>();
        for(string x: cloud_name)
        {
            string name =string.valueof([SELECT Primary__r.Name__c FROM ADM_Cloud_c__x WHERE Name__c = :x][0].Primary__r.Name__c);
            sec_eng.put(x,name);
        }
       
        
      //Main Query to filter Bugs from GUS
        /*
         * Impact ids
         * Security PreProduction- a0OB0000000UXLyMAO 
         * Security Production- a0O900000004EEwEAM
         * 
         * TrustAssessments API User -005B0000000hUQAIA2 
         * */
		
 		//AND cloud_c__c IN :cloud_name
 		string filter =' WHERE Record_Type_c__c = \'bug\' AND Closed_c__c = 0 ' + 
            'AND Impact_c__c IN (\'a0O900000004EEwEAM\', \'a0OB0000000UXLyMAO\') AND CreatedById__c != \'005B0000000hUQAIA2\' AND Priority_c__c IN (\'P0\',\'P1\',\'P2\')';
		
        integer count = database.countQuery ('SELECT count() FROM ADM_Work_c__x' + filter);
        system.debug(count);
            
        query = 'SELECT ExternalId,Name__c,WorkId_and_Subject_c__c,Cloud_c__c,Status_c__c, Assignee__r.Name__c,Priority_c__c,Absolute_Age__c , Team__c FROM ADM_Work_c__x' + filter + 'Order By ExternalId LIMIT 2000';
		
        list<ADM_Work_c__x> bug_object = new list<ADM_Work_c__x>() ;
        bug_object = database.query(query);
        
        //When count is more than 2000
        if(count>2000)
        {
            system.debug('More than 2000 bugs');
            string query2 = 'SELECT ExternalId,Name__c,WorkId_and_Subject_c__c,Cloud_c__c,Status_c__c, Assignee__r.Name__c,Priority_c__c,Absolute_Age__c, Team__c FROM ADM_Work_c__x' + filter +' AND ExternalId   > ' +'\'' + bug_object[1999].ExternalId  + '\'' +' LIMIT 2000';
			list<ADM_Work_c__x> list2 =database.query(query2);
            system.debug(list2.size());
            bug_object.addall(list2);
            
        }
        system.debug(bug_object.size());
            
            
            
        list<ADM_Work_c__x> GUS_list = new list<ADM_Work_c__x>();
        for(ADM_Work_c__x x:bug_object){
            if(team_object.containsKey(string.valueof(x.get('Team__c')))){
                GUS_list.add(x);
            }  
         }
        
         //insert_operation(final_list,team_object);
        // system.debug(status);
        //system.debug(final_list);
       // system.debug(final_list.size());
    
       
       // try{
        list<id> bug_ids= new list<id>();    
        list<bug_management__c> bug_list = new list<bug_management__c>();
        
        for(ADM_Work_c__x x:GUS_list)
        {
            bug_management__c val = new bug_management__c();
            val.Gus_work_id__c = string.valueof(x.get('ExternalId'));
            val.Name = string.valueof(x.get('Name__c'));
            val.Bug_Title__c = string.valueof(x.get('WorkId_and_Subject_c__c')).split(':',2)[1];
            val.Cloud_name__c = string.valueof(x.get('Cloud_c__c'));
            val.Status__c = string.valueof(x.get('Status_c__c'));
            val.Assigned_to__c= string.valueof(x.Assignee__r.Name__c);
            val.Priority__c  = string.valueof(x.get('Priority_c__c'));
            val.Age__c = integer.valueof(x.get('Absolute_Age__c'));
            val.Prodsec_owner__c = sec_eng.get(string.valueof(x.Cloud_c__c));
            val.closed__c = 0;
            // Adding RF_BLOCKER For core products 
            if((val.Cloud_name__c.startsWith('MC ')) || (val.Cloud_name__c.startsWith('CC ')) || (val.Cloud_name__c.contains('Pardot')) || (val.Cloud_name__c.contains('Marketing')) )
              val.RF_Blocker__c = False;
         	else
           		val.RF_Blocker__c = True;
            
            bug_ids.add(val.Gus_work_id__c);
            
            
            bug_list.add(val);
            
            
        } 
            //Inserting themes
            theme_work = new map<string,list<string>>();
            system.debug(bug_ids.size());
            //There is a cap in size of Connect API query. Breaking list into different batches for queries
            list<list<id>> sub_list = new list<list<id>>();
            integer mod=0;
            list<id> sub = new list<id>();
            for(id x: bug_ids)
            {
              sub.add(x);
              if(mod == 299){
                sub_list.add(sub);
                sub = new list<id>();
                mod=0;
              }
              else
              	mod = mod+1;
            }
            if(sub!= null)
            	sub_list.add(sub);
            system.debug(sub_list.size());
            
            for(list<id> i: sub_list){
                theme_work_r(i);
            }
            
            system.debug(theme_work);
            /* Done Inserting Theme */
            for(bug_management__c x:bug_list){
                if(theme_work.get(x.Name)!=null)
                {
                string allstring = string.join(theme_work.get(x.Name),'; ');
                x.Theme_list__c =allstring;
               
                }
               
            
            String Action_item ='';
            if((x.get('Theme_list__c'))!=null)
            { 
            if(!((string.valueof(x.get('Theme_list__c'))).contains('ProdSec Triaged'))){
                Action_item='Please Triage;';
            }}
            else 
                Action_item='Please Triage;';
            //closing Prodsec Reviewed bugs & bugs not needed based on themes
            if((x.get('Theme_list__c'))!=null)
            { 
                //InfrasecSABug
            if(((string.valueof(x.get('Theme_list__c'))).contains('ProdSec Reviewed')) || ((string.valueof(x.get('Theme_list__c'))).contains('InfrasecSABug')) || ((string.valueof(x.get('Theme_list__c'))).contains('CC-NetSec')) ){
                x.closed__c = 1;
            }
            }
            String Priority =string.valueof(x.Priority__c);
            switch on Priority{
                
                When 'P0'{
                    Action_item = Action_item + 'P0 Alert;';
                    
                    if(x.Age__c >= (7/2) )
                    Action_item = Action_item + 'Bug is Over Half Life;'; 
                    
                }
                
                When 'P1'{
                     if(x.Age__c >= (30/2) )
                         Action_item = Action_item + 'Bug is Over Half Life;'; 
                    
                }
                When 'P2'{
                     if(x.Age__c >= (90/2) )
                         Action_item = Action_item + 'Bug is Over Half Life;'; 
                    
                }  }
                
                x.Action_item__c = Action_item;
               
            }
          
         list<bug_management__c> bugs = new list<bug_management__c>();
         //Extract remaing open bugs and close it-
         for(bug_management__c z:[SELECT id, closed__c FROM bug_management__c where closed__c = 0 AND Gus_work_id__c NOT in :bug_ids])
         {
             z.closed__c = 1;
             bugs.add(z);
         }
         upsert bugs id;
            
            
            upsert bug_list Name;
        
         
         
         
            
         
         //theme_assignment.theme_insert(set_ids);
            
            
        mc.last_sync__c = System.now();
        upsert mc; 
      }
       catch(exception e){
          system.debug(e);
        }
                          
            //return true;
       // }
        //catch(exception e){
        //    system.debug(e);
//        return false;
      //  }
     }
    // Add all the theme to the bug
    public static void theme_work_r(list<string> query_list){
        try{
       

        system.debug(query_list.size());
        string Theme_ids = null;
        list<string> themes;
       
        for(ADM_Theme_Assignment_c__x z:Database.query('SELECT Theme_c__r.Name__c, Work_c__r.Name__c  FROM ADM_Theme_Assignment_c__x WHERE Work_c__c IN :query_list'))
        { 
           
           if(z.Theme_c__r.Name__c!=null && z.Work_c__r.Name__c!=null)
           {
           if(theme_work.containsKey(z.Work_c__r.Name__c ))
           		themes = theme_work.get(z.Work_c__r.Name__c );
           else
                themes = new list<string>();
           themes.add(string.valueof(z.Theme_c__r.Name__c));
           theme_work.put(string.valueof(z.Work_c__r.Name__c),themes);
           }
        }
        
       
    }
    
    catch(exception e)
    {
        system.debug(e);
    }
    }
        
}
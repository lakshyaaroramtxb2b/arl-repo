/*
 *
 * Batch apex for syncing completed courses. It is expected that this is called with
 * a scope size of <10 due to callout limits (e.g. Database.executeBatch(cls, 5) )
 *
 */

public class SC_BatchCoursesCompletedSync implements Database.Batchable<SC_TrainingBase.CoursesCompletedState>, Database.Stateful, Database.AllowsCallouts { 
    SC_TrainingBase.ProviderConnector connector;
    SC_Training_Provider__c provider;
    String[] sync62OrgIds; // when set, sync is just run for specified 62 org ids
    Integer timeDelay = 1;
  
    public SC_BatchCoursesCompletedSync(SC_Training_Provider__c prov) {
        provider = prov;
        this.sync62OrgIds = SC_TrainingTrailheadConnector.sync62OrgIds;
        
        // on the weekend set to retrieve last 7 days else retrieve last 2 days, or use override
        Date weekend = DateTime.now().date().toStartOfWeek().addDays(-1);
        if (DateTime.now().date() == weekend) {
            provider.Last_Completed_Course_Sync__c = Datetime.now().addDays(-7);   
        } else {
            provider.Last_Completed_Course_Sync__c = Datetime.now().addDays(-2);               
        }
        update provider;
    }
  
    public SC_BatchCoursesCompletedSync(SC_Training_Provider__c prov, DateTime lastSyncStart) {
        provider = prov;
        this.sync62OrgIds = SC_TrainingTrailheadConnector.sync62OrgIds;
        
        provider.Last_Completed_Course_Sync__c = lastSyncStart;
        update provider; 
    }

    public Iterable<SC_TrainingBase.CoursesCompletedState> start(Database.batchableContext info){
        SC_TrainingTrailheadConnector.sync62OrgIds = this.sync62OrgIds;
        
        connector = SC_TrainingUtil.connectorForProvider(provider);
        Iterable<SC_TrainingBase.CoursesCompletedState> states = connector.getCoursesCompletedStateIterator();
        return states;
    }     
    public void execute(Database.batchableContext info, List<SC_TrainingBase.CoursesCompletedState> states){ 
        If (states == null) {
            timeDelay = 15;}
        else {
            timeDelay = 1;
            SC_TrainingTrailheadConnector.sync62OrgIds = this.sync62OrgIds;
            // This should sync whatever records are necessary for this cycle
            Integer result = connector.syncCompleted(states);
    
            if (result == -1) {
                /* 
                * A service may not always tell us the number of total records we'll need to retrieve.
                * In that case, this code path is used to bail out of the job when it's done.
                * It'd be nice if batch apex's scope iterator was lazy eval'd but unfortunately
                * apex appears to walk it out completely before launching the job (which is why
                * we don't bother with a real custom iterator)
                */
                System.abortJob(info.getJobId());
            }
        }
    }     

    public void finish(Database.batchableContext info){
        //provider.Last_Completed_Course_Sync__c = Datetime.now();
        //update provider;
        //SC_SyncTrailheadScheduler.run(timeDelay);     
    } 

    public static void run(){
      SC_Training_Provider__c provider = SC_TrainingUtil.providerFor('Trailhead');
      SC_BatchCoursesCompletedSync b2 = new SC_BatchCoursesCompletedSync(provider);
      database.executebatch(b2, 1);
    } 
}
/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TIAlertUtilsTest {

    static testMethod void testTaskSLADates() {
        // Set up test data
        // Create Threat Intel Case
        Map<String,Schema.RecordTypeInfo> rtMapByName = Schema.SObjectType.Case.getRecordTypeInfosByName();
        
        // create salesforce account as temporary fix for failing trigger method on case, due to bad data
        
        List<Account> sfdcAccounts = [SELECT Id FROM Account WHERE Name = 'salesforce.com'];
        
        if (sfdcAccounts.isEmpty()) {
        	// create account so case insertion does not fail
        	
        	try {
        		insert new Account(Name = 'salesforce.com');
        	} catch (DMLException ex) {
        		System.assert(false, 'Failed insertion of SFDC Account: ' + ex.getDMLMessage (0)); // should not get here
        	}
        }
        
        // Create training course as temporary fix for failing trigger method on case...
        
        List<Training_Course__c> detectionEverywhereCourses = [SELECT Id From Training_Course__c WHERE Name = 'Detection Everywhere' LIMIT 1];
        
        if (detectionEverywhereCourses.isEmpty()) {
        	// create account so case insertion does not fail
        	
        	try {
        		insert new Training_Course__c(Name = 'Detection Everywhere');
        	} catch (DMLException ex) {
        		System.assert(false, 'Failed insertion of SFDC Account: ' + ex.getDMLMessage (0)); // should not get here
        	}
        }        
         
        System.assert(rtMapByName.containsKey('TI Alert'), 'Failed to locate Case Record Type with Name TI Alert');
        
        Case theTestCase = new Case(Status = 'New', recordTypeId = rtMapByName.get('TI Alert').getRecordTypeId());                
        
        try {
        	insert theTestCase;
        } catch (DMLException ex) {
        	System.assert(false, 'Failed insertion of Test Case: ' + ex.getDMLMessage (0)); // should not get here
        }
        
        // Create a task
        
        Task theTestTask = new Task ( WhatId = theTestCase.id, 
                                      OwnerId = UserInfo.getUserId(), SLA_Offset__c = 16,
                                      Subject = 'Test TI Task', 
                                      SLA_1_Notification_Offset__c = 8,
                                      SLA_2_Notification_Offset__c = 4);
                                      
        try {
        	insert theTestTask;
        } catch (DMLException ex) {
        	System.assert(false, 'Failed insertion of Test Task: ' + ex.getDMLMessage (0) ); // should not get here
        } 
        
        List<BusinessHours> tiBusinessHours = [ select id from
                                                       BusinessHours where 
                                                       Name = :TIAlertUtils.THREAT_INTEL_BUSINESS_HOURS_LABEL ];   
                                                       
        System.assert(!tiBusinessHours.isEmpty(), 'Failed to location Busines Hours record with label: ' + TIAlertUtils.THREAT_INTEL_BUSINESS_HOURS_LABEL); 
        
        TIAlertUtils.setTaskSLADates(new List<Id> { theTestCase.id });  
        
        theTestTask = [ select SLA_Due_Date_Time__c,
                               SLA_1_Notification_Date__c,
                               SLA_2_Notification_Date__c from 
                               Task where
                               id = :theTestTask.id ];
                               
        System.assert(theTestTask.SLA_Due_Date_Time__c != null, 'SLA Due Date Time not properly set');  
        System.assert(theTestTask.SLA_1_Notification_Date__c != null, 'SLA 1 Notification Date not properly set');
        System.assert(theTestTask.SLA_2_Notification_Date__c != null, 'SLA 1 Notification Date not properly set');                                                                 
    }
}
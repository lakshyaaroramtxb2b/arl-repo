@isTest
public class IEM_GUSWorkToCaseBatchTest {
    testmethod static void testEnrollment(){
        
        Contact contact = new Contact();
        contact.LastName = 'Test';
        contact.Employee_ID__c = 'MTX101';
        INSERT contact;
        
        Case newcase = new Case();
        newcase.RecordTypeId = IEM_Constants.IEM_CASERECORDTYPEID;
        newcase.Cloud__c = 'EntSecTools';
        newcase.Description = 'Test';
        newcase.Team_Responsible__c = 'TRUST Enterprise Security Tools Development';
        newcase.Subject = 'Test Subject';
        INSERT newcase;
             
        ADM_Work_c__x work = new ADM_Work_c__x();
        work.Subject_c__c = 'Test Subject';
        work.Theme_c__c = 'GRCOPS-INTAKE';
        work.Details_c__c = 'Test';
        work.Product_Owner_c__c ='EX101';
        work.Assignee__c = 'EX101';
        IEM_GUSWorkToCaseQueuable.mockallWorkList.add(work);
        
        IEM_GUSWorkToCaseQueuable.mockGUSUserEmpMap.put('MTX101', 'EX101');
        
        ADM_Theme_c__x theme1 = new ADM_Theme_c__x();
        theme1.ExternalId = '100';
        theme1.Name__c = IEM_Constants.PRIMARY_THEME_NAME;
        
        ADM_Theme_c__x theme2 = new ADM_Theme_c__x();
        theme1.ExternalId = '101';
        theme2.Name__c = IEM_Constants.INTAKE_THEME_NAME_PREFIX;
        
        IEM_GUSWorkToCaseBatch.mockallThemeList.add(theme1);
        IEM_GUSWorkToCaseBatch.mockallThemeList.add(theme2);
        
        ADM_Theme_Assignment_c__x themeAssignment1 = new ADM_Theme_Assignment_c__x();
        themeAssignment1.Work_c__c = '121';
        
        IEM_GUSWorkToCaseBatch.mockallThemeAssignmentList.add(themeAssignment1);
        Test.startTest();
       /* IEM_GUSWorkToCaseBatch batch = new IEM_GUSWorkToCaseBatch();
        DataBase.executeBatch(batch);  */
        SchedulableContext sc = null;
        IEM_GUSWorkToCaseBatch IEMGus = new IEM_GUSWorkToCaseBatch();
        IEMGus.execute(sc);
        Test.stopTest();
        List<Case> caseList= [Select Id From Case];
        system.assertEquals(caseList.size(), 2);
    }
}
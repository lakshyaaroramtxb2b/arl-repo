public with sharing class SSLRedirector {

    public SSLRedirector() {}
    public SSLRedirector(ApexPages.StandardSetController controller) {

    }
    
    public PageReference CheckSecure() {
        String url = Site.getCurrentSiteUrl();
        if ((url == null) || (url == '') || (url == ' ')) {
            // we're not on a site
            System.debug('CheckSSL: Not on a site: '+url);
            return null;
        }
        
        if (url.contains('developer-edition')) {
            // I don't think SSL is supported on DE Sites
            System.debug('CheckSSL: SSL not supported on DE sites: '+url);
            return null; 
        }
        

        // now get the "as-is" url with path and parameters
        url += ApexPages.currentPage().getUrl().split('/').get(2);
        
        if (!url.startswith('https://')) {
            pattern p = pattern.compile('^http://(.*?)\\.force.com/(.*)$');
            Matcher m = p.matcher(url);
            if (m.matches()) {
                // group 0 is always the whole string
                String newUrl = 'https://'+m.group(1)+'.secure.force.com/'+m.group(2);
                System.debug('CheckSSL: Redirecting ['+url+'] to ['+newUrl+']');
                return new PageReference(newUrl);
            }
            System.debug('CheckSSL: Unmatched Link: '+url);
            return null;
        }
        else {
            System.debug('CheckSSL: Already SSL: '+url);
            return null;
        }
    }

}
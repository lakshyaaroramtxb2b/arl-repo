/**
* @author Vishnu Kumar
* @email vishnu.kumar@mtxb2b.com
* @description This is helper class for CycleRequestItemTrigger.
*/
public with sharing class ARL_CreateCycleReqItemController {
    
    /**
    * @author Vishnu Kumar
    * @email vishnu.kumar@mtxb2b.com
    * @description This method creates cycle request items.
    * @param auditCycleId - record id for the audit cycle to generate requests for
    */
    @AuraEnabled
    public static void createCycleRequestItems(String auditCycleId){  
        
        //If user is External Auditor then throw the error to user
        // Updated in W-8024793
        if( FeatureManagement.checkPermission('ARL_Is_External_Auditor') ){
			throw new AuraHandledException('You don\'t have permission to generate cycle request items.');            
        } 
        ARL_CreateCycleReqItemControllerBatch.execute(null, [SELECT id, (SELECT Id, Area__c, Audit_Cycle__c,KARL_Audit_Team__c, Scope__c FROM Audit_Cycle_Coverages__r) 
                                                                FROM Audit_Cycle__c WHERE id=: auditCycleId 
                                                                WITH SECURITY_ENFORCED]);
        
    }
}
/**
 * @author: ralph@callaway.cloud
 */
public class SC_Unenroll_Batch implements Database.Batchable<sObject>, Database.Stateful {
    
    SC_Unenroller unenroller;
    String query;
    Options opts;
    Id courseId;
    Set<String> emails = new Set<String>();
    
    public class Options {
        private String filter;
        private Id courseId;
        private Set<String> emails = new Set<String>();
        public Boolean excludeFutureEnrollments = false;

        public Options(String filter) {
            System.assert(String.isNotEmpty(filter), 'filter is required');
            this.filter = filter;
        }

        public Options(Id courseId, Set<String> emails) {
            this('Training_Course__c = ' + DynamicSOQLHelper.format(courseId) + 
                ' AND Contact__r.Email IN :emails');
            System.assert(!emails.isEmpty(), 'emails must have one or more values');
            System.assertNotEquals(null, 'courseId is required');
            this.emails = emails;
            this.courseId = courseId;
        }
    }

    public SC_Unenroll_Batch(Options opts) {
        System.assertNotEquals(null, opts, 'opts is required');
        this.opts = opts;
        this.courseId = this.opts.courseId;
        this.emails = this.opts.emails;
        this.query = 'SELECT ' + 
                'Block_Enrollment__c, ' + 
                'Contact__r.Email, ' + 
                'Date_of_Escalation_to_Manager__c, ' +
                'Date_of_Escalation_to_Trust_Engagement__c, ' +
                'Due_Date__c, ' +
                'Escalation1__c, ' +
                'Escalation2__c, ' +
                'Escalation3__c, ' +
                'Escalation4__c, ' +
                'Escalation5__c, ' +
                'Last_Escalation__c, ' +
                'Enrollment_Notification_Date__c, ' +
                'RecordTypeId, ' +
                'Training_Course__c, ' +
                'Id ' +
            'FROM Training_Course_Taken__c ' +
            'WHERE ' + opts.filter + ' ' +
            'ORDER BY Training_Course__c'; 
        SC_Unenroller.Options unenrollerOpts = new SC_Unenroller.Options();
        unenrollerOpts.excludeFutureEnrollments = this.opts.excludeFutureEnrollments;
        this.unenroller = new SC_Unenroller(unenrollerOpts);
    }
    
    public Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }

    public void execute(Database.BatchableContext BC, List<Training_Course_Taken__c> enrollments) {
        SC_Unenroller.Unenrollment[] results = unenroller.unenroll(enrollments);
        logResults(results);
    }
    
    public void finish(Database.BatchableContext BC) {
        logMissingEmails(emails);
        Map<Id, Training_Course__c> courses = new Map<Id, Training_Course__c>([
            SELECT Name FROM Training_Course__c WHERE Id IN :results.keySet()]);
        Attachment[] attachments = new Attachment[0];
        String[] courseNames = new String[0];
        for (Id courseId : results.keySet()) {
            Training_Course__c course = courses.get(courseId);
            courseNames.add(course.Name);
            Attachment attachment = new Attachment();
            attachment.Body = Blob.valueOf(String.join(results.get(courseId), '\n'));
            attachment.ContentType = 'text/csv';
            attachment.Name = course.Name + ' Unenrollment Results ' + DateTime.now().format() + '.csv';
            attachment.ParentId = course.Id;
            attachments.add(attachment);    
        }
        insert attachments;
        Id[] attachmentIds = new Id[0];
        for (Attachment attachment : attachments) {
            attachmentIds.add(attachment.Id);
        }
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        email.setTargetObjectId(UserInfo.getUserId());
        email.setSubject(String.join(courseNames, '/') + ' Unenrollment Result ' + DateTime.now().format());
        email.setPlainTextBody('See attachments for results. Historical results are available in the training course attachment related list');
        email.setSaveAsActivity(false);
        email.setEntityAttachments(attachmentIds);
        email.setCCAddresses(Label.SC_TrainingBatchCC.split('\\s*,\\s*'));
        if (!Test.isRunningTest()) {
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { email });    
        }
    }

    String[] headerRow = new String[] {
        'Result',
        'Error/Details',
        'Contact Email',
        'Contact Id',
        'Enrollment Id'
    };
    Map<Id, String[]> results = new Map<Id, String[]>();
    private void initResults(Id courseId) {
        if (!results.containsKey(courseId)) {
            results.put(courseId, new String[] { String.join(headerRow, ',') });    
        }
    }
    private void logResults(SC_Unenroller.Unenrollment[] unenrollments) {
        for (SC_Unenroller.Unenrollment unenrollment : unenrollments) { 
            String[] row = new String[0];
            row.add(String.valueOf(unenrollment.result));
            row.add('"' + String.join(unenrollment.errors,'\n') + '"');
            row.add(unenrollment.oldRec.Contact__r.Email);
            row.add(unenrollment.oldRec.Contact__c);
            row.add(unenrollment.oldRec.Id);
            emails.remove(unenrollment.oldRec.Contact__r.Email);
            initResults(unenrollment.oldRec.Training_Course__c);
            results.get(unenrollment.oldRec.Training_Course__c).add(String.join(row, ','));
        }
    }
    private void logMissingEmails(Set<String> missedEmails) {
        for (String email : missedEmails) {
            String[] row = new String[0];
            row.add('NOT_FOUND');
            row.add('No enrollment found with email');
            row.add(email);
            System.assertNotEquals(null, courseId, 'No missed email logging if course id not specified');
            initResults(courseId);
            results.get(courseId).add(String.join(row, ','));
        }
    }


}
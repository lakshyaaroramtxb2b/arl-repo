public class  CDI_Extentionxlsx {
  @AuraEnabled(cacheable=true)
  public Static List<wrapperClass> getDataAssets(String recordId) {
      List<wrapperClass> dataAssetData= new List<wrapperClass>();

        for(CDI_Data_Asset__c da:[SELECT Id,Name,Data_Asset_Description__c,Data_Asset_Type__c,Parent_Data_Asset__r.Name,Data_Retention_Period__c,Source_System_Name__c,Source_Data_Asset_Code__c,Link_To_Balancing_Argument_Document__c,Link_To_Excepn_For_Basis_For_Processing__c,IsEncrypted__c,Is_Personal_Data_Limited_To_Purpose__c,Has_DPIA_Been_Performed__c,Can_Consumers_Access_Data__c,Data_Classification__r.Name,Data_Category__r.Name,Data_Identification_Category__r.Name,Crypto_Algorithm__r.Name,Data_Store__r.Name,Basis_For_Processing__r.Name,Time_Unit__r.Name,Retention_Process__r.Name,Customer_Data_Category__r.Name,GUS_Team__r.Name,IsDeleted__c, Status__c,Comments__c FROM CDI_Data_Asset__c where Data_Store__c=:recordId])
      {
                wrapperClass wc = new wrapperClass();
                wc.Name=da.Name==null?'':da.Name;
                wc.Description = da.Data_Asset_Description__c==null?'':da.Data_Asset_Description__c;
                wc.DataAssetType=da.Data_Asset_Type__c== null?'':da.Data_Asset_Type__c;
                wc.ParentDataAsset = da.Parent_Data_Asset__r.Name == null?'':da.Parent_Data_Asset__r.Name;
                wc.Sourcesystemname=da.Source_System_Name__c== null?'':da.Source_System_Name__c;
                wc.Sourcedataassetcode=da.Source_Data_Asset_Code__c==null?'':da.Source_Data_Asset_Code__c;
                wc.Linktobalancingarguments=da.Link_To_Balancing_Argument_Document__c==null?'':da.Link_To_Balancing_Argument_Document__c;
                wc.Linktoexcpnforbasisforprocessing=da.Link_To_Excepn_For_Basis_For_Processing__c==null?'':da.Link_To_Excepn_For_Basis_For_Processing__c;
                wc.IsEncrypted=da.IsEncrypted__c;
                wc.DataRetentionPeriod=da.Data_Retention_Period__c;
                wc.Canconsumeraccessdata=da.Can_Consumers_Access_Data__c;
                wc.DataClassification=da.Data_Classification__r.Name==null?'':da.Data_Classification__r.Name;
                wc.DataCategory=da.Data_Category__r.Name==null?'':da.Data_Category__r.Name;
                wc.DataStore=da.Data_Store__r.Name==null?'':da.Data_Store__r.Name;
                wc.DataIdentificationCategory=da.Data_Identification_Category__r.Name==null?'':da.Data_Identification_Category__r.Name;
                wc.CustomerDataCategory=da.Customer_Data_Category__r.Name==null?'':da.Customer_Data_Category__r.Name;
                wc.CryptoAlgorithm=da.Crypto_Algorithm__r.Name==null?'':da.Crypto_Algorithm__r.Name;
                wc.IsPersonalDataLimitedToPurpose=da.Is_Personal_Data_Limited_To_Purpose__c;
                wc.IsDeleted=da.IsDeleted__c;      
                wc.BasisForProcessing=da.Basis_For_Processing__r.Name==null?'':da.Basis_For_Processing__r.Name;
                wc.HasDPIABeenPerformed=da.Has_DPIA_Been_Performed__c;
                wc.TimeUnit=da.Time_Unit__r.Name==null?'':da.Time_Unit__r.Name;
                wc.RetentionProcess=da.Retention_Process__r.Name==null?'':da.Retention_Process__r.Name;
                wc.GusTeam=da.GUS_Team__r.Name==null?'':da.GUS_Team__r.Name;
                wc.Status=da.Status__c==null?'':da.Status__c;
                wc.Comments=da.Comments__c==null?'':da.Comments__c;
                dataAssetData.add(wc);
      }
      return dataAssetData;
}
public class wrapperClass{
  
      @AuraEnabled public string Name;
      @AuraEnabled public string Description;
      @AuraEnabled public string DataAssetType;
      @AuraEnabled public string ParentDataAsset;
      @AuraEnabled public string Sourcesystemname;
      @AuraEnabled public string Sourcedataassetcode;
      @AuraEnabled public string Linktobalancingarguments;
      @AuraEnabled public string Linktoexcpnforbasisforprocessing;
      @AuraEnabled public boolean IsEncrypted;
      @AuraEnabled public decimal DataRetentionPeriod;
      @AuraEnabled public boolean Canconsumeraccessdata;
      @AuraEnabled public string DataClassification;
      @AuraEnabled public string DataCategory;
      @AuraEnabled public string DataStore;
      @AuraEnabled public string DataIdentificationCategory;
      @AuraEnabled public string CustomerDataCategory;
      @AuraEnabled public string CryptoAlgorithm;
      @AuraEnabled public boolean IsPersonalDataLimitedToPurpose;
      @AuraEnabled public boolean IsDeleted;     
      @AuraEnabled public string BasisForProcessing;
      @AuraEnabled public boolean HasDPIABeenPerformed;
      @AuraEnabled public string TimeUnit;
      @AuraEnabled public string RetentionProcess;
      @AuraEnabled public string GusTeam;
      @AuraEnabled public string Status;
      @AuraEnabled public string Comments;
  }
}
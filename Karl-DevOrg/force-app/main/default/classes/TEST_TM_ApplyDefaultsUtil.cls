/**
 * Trust Maturity Model v3
 * Dec 2015
 * Written By: Jorge L Caceres (J-team member J3)
 * 
 * Test class for the following functionality living in TM_ApplyDefaultsUtil
 *  
**/

@isTest
private class TEST_TM_ApplyDefaultsUtil {
    
    // create test common data
    private static Account buzUnit1;
    private static Account buzUnit2;
    
    private static IRCLOUD__Environment__c bu1_env1;
    private static IRCLOUD__Environment__c bu1_env2;
    private static IRCLOUD__Environment__c bu2_env1;
    
    private static User adminUser;
    private static User regularUser;
    
    private static TM_AbstractObjective__c absObjective1;
    private static TM_AbstractObjective__c absObjective2;
    private static TM_AbstractObjective__c absObjective3;
    private static TM_AbstractObjective__c absObjective4;
    private static TM_AbstractObjective__c absObjective5;

    private static TM_Placement__c placement1;
    private static TM_Placement__c placement2;
    private static TM_Placement__c placement3;
    private static TM_Placement__c placement4;
    private static TM_Placement__c placement5;

    private static TM_Objective_Default__c def1;
    private static TM_Objective_Default__c def2;
    private static TM_Objective_Default__c def3;
        
    static {
        
        absObjective1 = TEST_TM_Util.generateTMAbstractObjective();
        absObjective2 = TEST_TM_Util.generateTMAbstractObjective();
        absObjective3 = TEST_TM_Util.generateTMAbstractObjective();
        absObjective4 = TEST_TM_Util.generateTMAbstractObjective();
        absObjective5 = TEST_TM_Util.generateTMAbstractObjective();
        insert new TM_AbstractObjective__c[] { absObjective1, absObjective2, absObjective3, absObjective4, absObjective5 };        
        
        placement1 = TEST_TM_Util.generateTMPlacement(absObjective1.Id, 'SIR', '1', 1, TM_Constants.BU_ENVIRONMENT_TYPE); 
        placement2 = TEST_TM_Util.generateTMPlacement(absObjective2.Id, 'SIR', '1', 1, TM_Constants.BU_ENVIRONMENT_TYPE); 

        placement3 = TEST_TM_Util.generateTMPlacement(absObjective3.Id, 'SIR', '1', 1, [SELECT Name FROM RecordType WHERE sObjectType = 'IRCLOUD__Environment__c' 
                                                                                        AND DeveloperName = :TM_Constants.PARENT_INFRA_IT_ENVIRONMENT_NAME 
                                                                                        order by NamespacePrefix DESC limit 1].Name); 
        
        placement4 = TEST_TM_Util.generateTMPlacement(absObjective4.Id, 'SID', '1', 1, [SELECT Name FROM RecordType WHERE sObjectType = 'IRCLOUD__Environment__c' 
                                                                                        AND DeveloperName = :TM_Constants.PARENT_INFRA_PROD_ENVIRONMENT_NAME 
                                                                                        order by NamespacePrefix DESC limit 1].Name); 

        placement5 = TEST_TM_Util.generateTMPlacement(absObjective5.Id, 'SID', '1', 1, [SELECT Name FROM RecordType WHERE sObjectType = 'IRCLOUD__Environment__c' 
                                                                                        AND DeveloperName = :TM_Constants.OTHER_ENVIRONMENT_NAME 
                                                                                        order by NamespacePrefix DESC limit 1].Name); 



        insert new TM_Placement__c[] { placement1, placement2, placement3, placement4, placement5 };        

        buzUnit1 = TEST_TM_Util.generateAccount(TM_Constants.BU_ACCOUNT_REC_TYPE_ID);        
        buzUnit2 = TEST_TM_Util.generateAccount(TM_Constants.BU_ACCOUNT_REC_TYPE_ID);
        insert new Account[] { buzUnit1, buzUnit2 };
        
        bu1_env1 = TEST_TM_Util.generateEnvironment(TM_Constants.PARENT_INFRA_IT_ENVIRONMENT_REC_TYPE_ID, buzUnit1.Id);
        bu1_env2 = TEST_TM_Util.generateEnvironment(TM_Constants.PARENT_INFRA_PROD_ENVIRONMENT_REC_TYPE_ID, buzUnit1.Id);

        bu2_env1 = TEST_TM_Util.generateEnvironment(TM_Constants.OTHER_ENVIRONMENT_REC_TYPE_ID, buzUnit2.Id);
        insert new IRCLOUD__Environment__c[] { bu1_env1, bu1_env2, bu2_env1 };        

        adminUser = TEST_TM_Util.generateUser();
        regularUser = TEST_TM_Util.generateUser();
        // avoid mixed DML errors
        system.runAs(new User(Id = UserInfo.getUserId())) {
            insert new User[] { adminUser, regularUser };
            insert new PermissionSetAssignment(PermissionSetId = TM_Constants.PERM_SET_ADMIN_ID, AssigneeId = adminUser.Id);        
            insert new PermissionSetAssignment(PermissionSetId = TM_Constants.PERM_SET_VALIDATOR_ID, AssigneeId = regularUser.Id);        
        } 

        // enable TMM for buz units to trigger concrete objectives generation
        List<Account> newBuzUnits = new List<Account>([select Id, Name from Account]);
        System.assertEquals(2, newBuzUnits.size());
        system.runAs(new User(Id = UserInfo.getUserId())) {                
            for (Account acct : newBuzUnits) {
                acct.TMM_Tracking__c = true;
            }
            update newBuzUnits;
        }
                      
        // assign default validators
        
        def1 = new TM_Objective_Default__c(Business_Unit__c = buzUnit1.Id, Category__c = 'SIR', 
                                           Validation_Owner_Default__c = adminUser.Id);
        
        def2 = new TM_Objective_Default__c(Business_Unit__c = buzUnit1.Id, Category__c = 'SID', 
                                           Validation_Owner_Default__c = regularUser.Id);
                                                                   
        def3 = new TM_Objective_Default__c(Business_Unit__c = buzUnit2.Id, Category__c = 'SID', 
                                           Validation_Owner_Default__c = regularUser.Id);

        insert new TM_Objective_Default__c[] { def1, def2, def3 };  
    }
       
    static testMethod void testAdminsOnly() {
                              
        // assign 1 objective to test override logic
        TM_Objective__c obj1 = [SELECT Id FROM TM_Objective__c 
                                WHERE Business_Unit__c = :buzUnit1.Id 
                                AND TM_Placement__r.Category__c = 'SIR'
                                limit 1];
        obj1.ValidationOwner__c = regularUser.Id;
        update obj1;

        assertInitialData();
        
        // assert initial assignments          
        system.assertEquals(0, [SELECT count() FROM TM_Objective__c WHERE ValidationOwner__c = :adminUser.Id]);
        system.assertEquals(1, [SELECT count() FROM TM_Objective__c WHERE ValidationOwner__c = :regularUser.Id]);

        test.startTest();
        
        // call the webservice method to apply defaults
        system.runAs(regularUser) {
            TM_ApplyDefaultsUtil.applyDefaults(def1.Id, false);        
            TM_ApplyDefaultsUtil.applyDefaults(def2.Id, false); 
        }     
                                                                               
        test.stopTest();
        
        // assert nothing changed          
        system.assertEquals(0, [SELECT count() FROM TM_Objective__c WHERE ValidationOwner__c = :adminUser.Id]);
        system.assertEquals(1, [SELECT count() FROM TM_Objective__c WHERE ValidationOwner__c = :regularUser.Id]);
                                                                   
    }
       
    static testMethod void testApplyDefaults() {
                              
        // assign 1 objective to test override logic
        TM_Objective__c obj1 = [SELECT Id FROM TM_Objective__c 
                                WHERE Business_Unit__c = :buzUnit1.Id 
                                AND TM_Placement__r.Category__c = 'SIR'
                                limit 1];
        obj1.ValidationOwner__c = regularUser.Id;
        update obj1;

        assertInitialData();

        test.startTest();
        
        // call the webservice method to apply defaults
        system.runAs(adminUser) {
            TM_ApplyDefaultsUtil.applyDefaults(def1.Id, false);        
            TM_ApplyDefaultsUtil.applyDefaults(def2.Id, false); 
        }     
                                                                               
        test.stopTest();
        
        // assert defaults were applied as expected              
        system.assertEquals(2, [SELECT count() FROM TM_Objective__c WHERE ValidationOwner__c = :adminUser.Id]);
        system.assertEquals(2, [SELECT count() FROM TM_Objective__c WHERE ValidationOwner__c = :regularUser.Id]);
                                                                   
    }
       
    static testMethod void testOverrideDefaults() {
                              
        // assign 1 objective to test override logic
        TM_Objective__c obj1 = [SELECT Id FROM TM_Objective__c 
                                WHERE Business_Unit__c = :buzUnit1.Id 
                                AND TM_Placement__r.Category__c = 'SIR'
                                limit 1];
        obj1.ValidationOwner__c = regularUser.Id;
        update obj1;
        
        assertInitialData();

        test.startTest();
        
        // call the webservice method to apply defaults
        system.runAs(adminUser) {
            TM_ApplyDefaultsUtil.applyDefaults(def1.Id, true);        
            TM_ApplyDefaultsUtil.applyDefaults(def2.Id, true); 
        }     
                                                                               
        test.stopTest();
        
        // assert defaults were applied as expected              
        system.assertEquals(3, [SELECT count() FROM TM_Objective__c WHERE ValidationOwner__c = :adminUser.Id]);
        system.assertEquals(1, [SELECT count() FROM TM_Objective__c WHERE ValidationOwner__c = :regularUser.Id]);
                                                                   
    }
       
    static testMethod void testFailedStatusCanNotOverride() {
                      
        // change status to completed of 1 objective to test 
        TM_Objective__c obj1 = [SELECT Id FROM TM_Objective__c 
                                WHERE CSIRT_Environment__c = :bu2_env1.Id 
                                AND TM_Placement__r.Category__c = 'SID'
                                limit 1];
        system.runAs(adminUser) {
            obj1.ValidationOwner__c = adminUser.Id;
            obj1.Status__c = 'Working: Validating';
            update obj1;
            obj1.Status__c = 'Failed';
            update obj1;
        }
        
        assertInitialData();        

        test.startTest();
        
        // call the webservice method to override defaults        
        system.runAs(adminUser) {
            TM_ApplyDefaultsUtil.applyDefaults(def3.Id, true);
        }        
                                                                               
        test.stopTest();
        
        // assert the single SID did not get assign to regularUser    
        System.assertEquals(0, [SELECT count() FROM TM_Objective__c WHERE ValidationOwner__c = :regularUser.Id]);
                                                                   
    }
       
    static testMethod void testNAStatusCanNotOverride() {
                      
        // change status to completed of 1 objective to test 
        TM_Objective__c obj1 = [SELECT Id FROM TM_Objective__c 
                                WHERE CSIRT_Environment__c = :bu2_env1.Id 
                                AND TM_Placement__r.Category__c = 'SID'
                                limit 1];
        system.runAs(adminUser) {
            obj1.ValidationOwner__c = adminUser.Id;
            obj1.Status__c = 'Working: Validating';
            update obj1;
            obj1.Status__c = 'Not Applicable';
            update obj1;
        }
        
        assertInitialData();        

        test.startTest();
        
        // call the webservice method to override defaults
        system.runAs(adminUser) {
            TM_ApplyDefaultsUtil.applyDefaults(def3.Id, true);
        }        
                                                                               
        test.stopTest();
        
        // assert the single SID did not get assign to regularUser    
        System.assertEquals(0, [SELECT count() FROM TM_Objective__c WHERE ValidationOwner__c = :regularUser.Id]);
                                                                   
    }
       
    static testMethod void testWorkingValidatingStatusCanOverride() {
                      
        // change status to completed of 1 objective to test 
        TM_Objective__c obj1 = [SELECT Id FROM TM_Objective__c 
                                WHERE CSIRT_Environment__c = :bu2_env1.Id 
                                AND TM_Placement__r.Category__c = 'SID'
                                limit 1];
        system.runAs(adminUser) {
            obj1.ValidationOwner__c = adminUser.Id;
            obj1.Status__c = 'Working: Validating';
            update obj1;
        }
        
        assertInitialData();        

        test.startTest();
        
        // call the webservice method to override defaults
        system.runAs(adminUser) {
            TM_ApplyDefaultsUtil.applyDefaults(def3.Id, true);
        }        
                                                                               
        test.stopTest();
        
        // assert the single SID did get assign to regularUser    
        System.assertEquals(1, [SELECT count() FROM TM_Objective__c WHERE ValidationOwner__c = :regularUser.Id]);
                                                                   
    }
    
    private static void assertInitialData() {
        
        System.assertEquals(2, [SELECT count() FROM IRCLOUD__Environment__c WHERE IRCloud__Account__c = :buzUnit1.Id]);
        System.assertEquals(1, [SELECT count() FROM IRCLOUD__Environment__c WHERE IRCloud__Account__c = :buzUnit2.Id]);

        System.assertEquals(2, [SELECT count() FROM TM_Objective__c WHERE Business_Unit__c = :buzUnit1.Id]);
        System.assertEquals(2, [SELECT count() FROM TM_Objective__c WHERE Business_Unit__c = :buzUnit2.Id]);
        System.assertEquals(1, [SELECT count() FROM TM_Objective__c WHERE CSIRT_Environment__c = :bu1_env1.Id]);
        System.assertEquals(1, [SELECT count() FROM TM_Objective__c WHERE CSIRT_Environment__c = :bu1_env2.Id]);
        System.assertEquals(1, [SELECT count() FROM TM_Objective__c WHERE CSIRT_Environment__c = :bu2_env1.Id]);
        
    }
}
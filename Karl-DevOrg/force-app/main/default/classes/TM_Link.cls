/*

    Developer Name: Jorge L Caceres - jcaceres@salesforce.com | September 2015    

    Description: Handles redirection from TMM site to Security Org standard layout for Objectives
    
*/

public class TM_Link {
    
    public TM_Link () {}

    // redirect to salesforce standard salesforce layout
    public PageReference redirectToUUID () {
        String uuid = ApexPages.currentPage().getParameters().get('uuid');
        String referer = ApexPages.currentPage().getHeaders().get('Referer');
        try {
            TM_AbstractObjective__c a = [SELECT Id FROM TM_AbstractObjective__c WHERE UUID__c = :uuid.deleteWhitespace()];
            PageReference abstractPage = (new ApexPages.StandardController(a)).View();
            abstractPage.getParameters().put('retURL', referer);
            abstractPage.getParameters().remove('uuid');
            // keep users within the TMM site/community 
            If (Site.getSiteType() == 'ChatterNetwork' && 
                abstractPage.getURL().startsWith('/' + string.valueOf(a.Id).left(15)) &&
                String.isEmpty(Site.getPathPrefix())) 
                abstractPage = new PageReference('/TMM' + abstractPage.getURL());
            abstractPage.setRedirect(true);
            return abstractPage;            
        } catch (Exception e) {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,
                                 'Invalid UUID Link. Please click the back page button to return to your previous page.'));
            return null;
        }
        return null;
    }
    
    
}
@isTest
public class BPO_TestUtil {
    
    // Status Options
    public static String STATUS_NOT_STARTED = 'Not Started';
    public static String STATUS_NA = 'Not Applicable';
    public static String STATUS_IN_PROGRESS = 'In Progress';
    public static String STATUS_COMPLETED = 'Completed';    
    
    public static List<BPO_Assessment__c> createBPOAssessments(Integer count, Integer taskCount, Integer taskItemCount) {
        BPO_Site__c bpoSite = new BPO_Site__c (
            name='BPO Site', 
            Type_of_Services_Provided__c='Sales', 
            Status__c='Red', 
            Country__c='Poland', City__c='Krakow');
        insert bpoSite;
        
        List<BPO_Assessment__c> bpoAssessments = new List<BPO_Assessment__c>();
        for (Integer i=0;i<count;i++) {
            BPO_Assessment__c bpoAssessment = new BPO_Assessment__c(
                name='BPO Assessment' + i, 
                BPO_Site__c=bpoSite.id, 
                purpose_of_assessment__c='Existing Site', 
                Start_Date__c=System.today()); 
            
            bpoAssessments.add(bpoAssessment);
        }                
        insert bpoAssessments; 
        
        List<BPO_Task__c> tasks = new List<BPO_Task__c>();
        for (BPO_Assessment__c assessment : bpoAssessments) {            
            for (Integer i=0;i<taskCount;i++) {
                BPO_Task__c task = new BPO_Task__c(BPO_Assessment__c=assessment.id);
                tasks.add(task);
            }                        
        }
        insert tasks;
        
        List<BPO_Task_Item__c> taskItems = new List<BPO_Task_Item__c>();
        for (BPO_Task__c task : tasks) {
            for (Integer i=0;i<taskItemCount;i++) {
                BPO_Task_Item__c taskItem = new BPO_Task_Item__c(BPO_Task__c=task.id, Status__c='In Progress', Response_LongText__c='Some Text', Response_Date__c=Date.newInstance(2017, 1, 1), Response_Boolean__c=true);
                taskItems.add(taskItem);
            } 
        }
        insert taskItems;
        
        return bpoAssessments;
    }  
}
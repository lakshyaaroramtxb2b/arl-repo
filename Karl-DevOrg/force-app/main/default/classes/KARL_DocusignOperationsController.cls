/** 
* @author Lakshya Arora
* @email lakshya.arora@mtxb2b.com
* @description This class will handle docusign documents
* Used in KARL_DownloadDocusignReportCmp,KARL_SendReportToAuditors
*/
public with sharing class KARL_DocusignOperationsController {
    /**
* @author Lakshya Arora
* @email lakshya.arora@mtxb2b.com
* @description Fetch the latest docusign file from notes and attachment
* @param cycleAuditReportId - record Id for Cycle Audit Reports
* @return DocusignOperationWrapper which contains return message, successCond and FileLink
*/
    @AuraEnabled
    public static DocusignOperationWrapper downloadLatestDocusignDocument(Id recordId){
        DocusignOperationWrapper  DocusignOperationWrapperObj = new DocusignOperationWrapper();
        List<KARL_Cycle_Audit_Report_Items__c> cycleAuditReportList = getCycleAuditReportItemListByRecordId(recordId);
        if(cycleAuditReportList[0].Status__c != KARL_Constants.REPORT_STATUS_PUBLISHED
         && cycleAuditReportList[0].Status__c != KARL_Constants.REPORT_STATUS_ISSUED
         && cycleAuditReportList[0].Status__c != KARL_Constants.REPORT_STATUS_FINAL_REVIEW){
            return new DocusignOperationWrapper('Report not available for download until Issued',false,new List<String>());
        }
        
        Map<String,String> baseUrlMap = new Map<String,String>();
        baseUrlMap = KARL_Utility.getBaseURL();
        String baseRecordURL = '';
        String baseUrlSuffix = '';
        if(!baseUrlMap.isEmpty()){
            baseRecordURL = baseUrlMap.get('baseRecordURL');
            baseUrlSuffix = baseUrlMap.get('baseUrlSuffix');
        }
        
        //Does not need Security Enforced as accessing the file
        Set<Id> contentDocumentIdSet = new Set<Id>();
        for(ContentDocumentLink cdlObjList : getContentDocumentByLinkedEntityId(recordId)){
            contentDocumentIdSet.add(cdlObjList.ContentDocumentId);
        }
        if(!contentDocumentIdSet.isEmpty()){
            List<String> fileLinkList = new List<String>();
            //Does not need Security Enforced as accessing the file
            for(ContentVersion contentVersionObj : getContentVersionByContentDocumentIdSet(contentDocumentIdSet)){
                fileLinkList.add(baseRecordURL+contentVersionObj.Id+ baseUrlSuffix);
            }
            if(!fileLinkList.isEmpty())
                return new DocusignOperationWrapper('File downloaded successfully',true,fileLinkList);  
        }                  
        
        return new DocusignOperationWrapper('No docusign document found',false,new List<String>());                                   
    }
    
    /**
* @author Lakshya Arora
* @email lakshya.arora@mtxb2b.com
* @description send all signed documents to auditors
* @param cycleAuditReportId - record Id for Cycle Audit Reports
*/
    @AuraEnabled
    public static DocusignOperationWrapper sendSignedDocumentToAuditors(Id recordId,List<String> contentDocumentIdList){
        Set<Id> contentDocumentIdSet = new Set<Id>();
        for(String contentDocumentId : contentDocumentIdList){
            contentDocumentIdSet.add(Id.valueOf(contentDocumentId));
        }
        FINAL String GRCICDEMAILADDRESS = System.label.GRC_ICD;
        List<KARL_Cycle_Audit_Report_Items__c> cycleAuditReportList = getCycleAuditReportItemListByRecordId(recordId);
        String orgWideDisplayName = System.label.SCEA_KARL;
        List<OrgWideEmailAddress> orgwideEmailAddressList = KARL_Utility.getOrgwideEmailAddressIdByDisplayName(ORGWIDEDISPLAYNAME);
        Set<String> toAddressIdSet = new Set<String>{GRCICDEMAILADDRESS};
        if(cycleAuditReportList[0].Audit_Reports_Master_Data__r.KARL_Audit_Scope__r.KARL_BU_Lead__c != null){
            toAddressIdSet.add(cycleAuditReportList[0].Audit_Reports_Master_Data__r.KARL_Audit_Scope__r.KARL_BU_Lead__c);
        }
        if(!orgwideEmailAddressList.isEmpty()){ 
            List<KARL_Audit_Firm_Stakeholders__c> auditFirmStakeholderList = KARL_AuditFirmStakeholdersController.getAuditFirmStakeholders(recordId);
            if(!auditFirmStakeholderList.isEmpty()){
                for(KARL_Audit_Firm_Stakeholders__c auditFirmStakeHolderObj : auditFirmStakeholderList){
                    toAddressIdSet.add(auditFirmStakeHolderObj.Stakeholder__c);
                }
                if(!contentDocumentIdSet.isEmpty()){
                    List<Messaging.EmailFileAttachment> attachmentList = new List<Messaging.EmailFileAttachment>();
                    //Does not need Security Enforced as accessing the file
                    for(ContentVersion contentVersionObj : getContentVersionByContentDocumentIdSet(contentDocumentIdSet)){
                        Blob documentBody = contentVersionObj.versiondata;
                        Messaging.EmailFileAttachment attachment = new Messaging.EmailFileAttachment();
                        attachment.setFileName(contentVersionObj.Title);
                        attachment.setBody(documentBody);
                        attachment.setContentType('application/pdf');
                        attachmentList.add(attachment);
                    }
                    if(!attachmentList.isEmpty()){
                        EmailTemplate emailTemplateObj = [SELECT id,name,developername,Subject,htmlvalue
                                                          FROM EmailTemplate 
                                                          WHERE developername ='KARL_Docusign_Document_Of_Cycle_Audit_Report'];
                        List<Messaging.SingleEmailMessage> mailList = new List<Messaging.SingleEmailMessage>();
                        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
                        String htmlBody = emailTemplateObj.htmlvalue;
                        htmlBody = htmlBody.replace('@[BULeadName]', cycleAuditReportList[0].Audit_Reports_Master_Data__r.KARL_Audit_Scope__r.KARL_BU_Lead__r.Name);
                        htmlBody = htmlBody.replace('@[ownerName]', cycleAuditReportList[0].Audit_Cycle__r.Owner.Name);
                        message.setSaveAsActivity(false); 
                        message.setTemplateId(emailTemplateObj.id);
                        message.setHtmlBody(htmlBody);
                        message.setSubject(emailTemplateObj.Subject);
                        List<String> toAddressList = new List<String>();
                        toAddressList.addAll(toAddressIdSet);
                        message.setToAddresses(toAddressList); 
                        message.setCCAddresses(new List<String>{orgwideEmailAddressList[0].Address}); 
                        message.setOrgWideEmailAddressId(orgwideEmailAddressList[0].Id);
                        message.setFileAttachments(attachmentList);
                        mailList.add(message);
                        
                        if(!mailList.isEmpty()){
                            Messaging.SendEmailResult[] emailResults =  Messaging.sendEmail( mailList, false);
                            for(Messaging.SendEmailResult result : emailResults){
                                if(!result.isSuccess()){
                                    System.debug(LoggingLevel.DEBUG,'Error Received');
                                    for(Database.Error error : result.getErrors()){
                                        System.debug(LoggingLevel.DEBUG,error.getMessage());
                                        return new DocusignOperationWrapper(error.getMessage(),false);
                                    }
                                }
                                else{
                                    update cycleAuditReportList;
                                    return new DocusignOperationWrapper('Mail Sent successfully',true);
                                }
                            } 
                        }     
                    }
                }
                return new DocusignOperationWrapper('No signed documents Found',false);
            }
            return new DocusignOperationWrapper('No Stakeholders Found',false); 
        }
        return new DocusignOperationWrapper('Org Email Address not found; Please contact your administrator',false); 
    }
    
    /**
* @author Lakshya Arora
* @email lakshya.arora@mtxb2b.com
* @description Fetch all the docusign file from notes and attachment
* @param cycleAuditReportId - record Id for Cycle Audit Reports
* @return Map of content version name vs file link
*/
    @AuraEnabled
    public static List<FileWrapper> getAllFiles(Id cycleAuditReportId){
        List<FileWrapper> fileWrapperList = new List<FileWrapper>();
        Map<String,String> baseUrlMap = new Map<String,String>();
        baseUrlMap = KARL_Utility.getBaseURL();
        String baseRecordURL = '';
        String baseUrlSuffix = '';
        if(!baseUrlMap.isEmpty()){
            baseRecordURL = baseUrlMap.get('baseRecordURL');
            baseUrlSuffix = baseUrlMap.get('baseUrlSuffix');
        }
        
        //Does not need Security Enforced as accessing the file
        Set<Id> contentDocumentIdSet = new Set<Id>();
        for(ContentDocumentLink cdlObjList : getContentDocumentByLinkedEntityId(cycleAuditReportId)){
            contentDocumentIdSet.add(cdlObjList.ContentDocumentId);
        }
        if(!contentDocumentIdSet.isEmpty()){
            List<String> fileLinkList = new List<String>();
            //Does not need Security Enforced as accessing the file
            for(ContentVersion contentVersionObj : getContentVersionByContentDocumentIdSet(contentDocumentIdSet)){
                fileWrapperList.add(new FileWrapper(contentVersionObj.ContentDocumentId,contentVersionObj.Title,baseRecordURL+contentVersionObj.Id+baseUrlSuffix,false));
            }
        }                  
        return fileWrapperList;                                   
    }
    
    
    /**
* @author Lakshya Arora
* @email lakshya.arora@mtxb2b.com
* @description get List of  Cycle Audit Report Item records by record id
* @param recordId - record Id for Cycle Audit Report item
* @return - List of Cycle audit report items filter by record id
*/
    private static List<KARL_Cycle_Audit_Report_Items__c> getCycleAuditReportItemListByRecordId(Id recordId){
        return [SELECT Id,Status__c,Audit_Cycle__r.Owner.Name,
                Audit_Reports_Master_Data__r.KARL_Audit_Scope__r.KARL_BU_Lead__c,Audit_Reports_Master_Data__r.KARL_Audit_Scope__r.KARL_BU_Lead__r.Name
                FROM KARL_Cycle_Audit_Report_Items__c 
                WHERE Id =:recordId 
                WITH SECURITY_ENFORCED];
    }
    
    /**
* @author Lakshya Arora
* @email lakshya.arora@mtxb2b.com
* @description get List of ContentDocumentLink associated with cycle audit report item record
* @param recordId - record Id for Cycle Audit Report item
* @return - List of ContentDocumentLink filter by LinkedEntityId
*/
    private static List<ContentDocumentLink> getContentDocumentByLinkedEntityId(Id recordId){
        //Does not need Security Enforced as accessing the file
        return [SELECT Id,ContentDocumentId,LinkedEntityId 
                FROM ContentDocumentLink 
                WHERE LinkedEntityId =: recordId];
    }
    
    /**
* @author Lakshya Arora
* @email lakshya.arora@mtxb2b.com
* @description get List of ContentVersion 
* @param recordId - record Id of ContentDocumentId
* @return - List of ContentVersion filter by ContentDocumentId
*/
    private static List<ContentVersion> getContentVersionByContentDocumentIdSet(Set<Id> contentDocumentIdSet){
        //Does not need Security Enforced as accessing the file
        return [SELECT Id, ContentDocumentId, Title, FileExtension,FileType,Versiondata 
                FROM ContentVersion
                WHERE ContentDocumentId IN: contentDocumentIdSet];
    }
    
    public class DocusignOperationWrapper{
        @AuraEnabled public String message;
        @AuraEnabled public Boolean isSuccess;
        @AuraEnabled public List<String> fileLinkList;
        public DocusignOperationWrapper(){
            
        }
        public DocusignOperationWrapper(String message,Boolean isSuccess){
            this.message = message;
            this.isSuccess = isSuccess;
        }
        public DocusignOperationWrapper(String message,Boolean isSuccess,List<String> fileLinkList){
            this.message = message;
            this.isSuccess = isSuccess;
            this.fileLinkList = fileLinkList;
        }
    }
    
    public class FileWrapper{
        @AuraEnabled public String fileId;
        @AuraEnabled public String fileName;
        @AuraEnabled public String fileLink;
        @AuraEnabled public Boolean isSelected;
        public FileWrapper(){
            
        }
        public FileWrapper(String fileId,String fileName,String fileLink,Boolean isSelected){
            this.fileId = fileId;
            this.fileName = fileName;
            this.fileLink = fileLink;
            this.isSelected = isSelected;
        }
    }
}
/**
 * Move the Security org Case Status (In Progress or Waiting on Customer Office Hours ) to Security org.
** SPT_SyncSecOrgCaseStatusToSptCase_Old syncStatus = new SPT_SyncSecOrgCaseStatusToSptCase_Old();
* syncStatus.execute(null);
*/
public class SPT_SyncSecOrgCaseStatusToSptCase_Old implements Schedulable, Database.Batchable<sObject>, Database.Stateful {
    
    public static SchedulableContext scCtx;
    public void execute(SchedulableContext ctx) {

        List<AsyncApexJob> lstAsyncJobs = [SELECT Status 
                                           FROM AsyncApexJob 
                                           WHERE Status NOT IN ('Completed','Aborted','Failed')
                                           AND ApexClass.Name = 'SPT_SyncSecOrgCaseStatusToSptCase_Old'];

        if(lstAsyncJobs.size() == 0){
            Database.executeBatch((Database.Batchable<SObject>) 
            Type.forName('SPT_SyncSecOrgCaseStatusToSptCase_Old').newInstance(), 100);
            scCtx = ctx;
        }
        
    }

    public Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([
            SELECT Id,
                   Enterprise_Security_Case_Number__c,
                   Status 
            FROM Case
            WHERE RecordTypeId =: SPT_Constants.SEC_RECORDTYPE_ID
            AND (Status = :Label.Esa_Case_Waiting_OH_Status
            OR Status = 'In Progress')
        ]);
    }
    

    public void execute(Database.BatchableContext BC, List<Case> lstSecCases) {
        try {
            syncCaseStatus(lstSecCases);
        } catch (Exception e) {
            Esa_DebugService.WriteException(e, 'SPT_SyncSecOrgCaseStatusToSptCase_Old', 'Exception in execute method');
        } finally {
            
        }
    }
    
    private void abortCurrentAndScheduleNextExecution(SchedulableContext ctx, Integer intervalMins) {
        if (ctx != null) {
           System.abortJob(ctx.getTriggerId());
        }
        
        Datetime nextScheduleTime = System.now().addMinutes(intervalMins);
        String minute = String.valueof(nextScheduleTime.minute());
        String second = String.valueof(nextScheduleTime.second ());
        String cronvalue = second + ' ' + minute + ' * * * ?';
        String jobName = SPT_SyncSecOrgCaseStatusToSptCase_Old.class.getName() + nextScheduleTime.format('yyyy-MM-dd-HH:mm:ss');  
   
        SPT_SyncSecOrgCaseStatusToSptCase_Old p = new SPT_SyncSecOrgCaseStatusToSptCase_Old();   
        System.schedule(jobName, cronvalue , p); 
    }

    private void syncCaseStatus(List<Case> lstSecCases){
        
        Set<String> setCaseNumbers = new Set<String>();
        Map<String,String> mapCaseStatus = new Map<String, String>();
        system.debug('##########'+lstSecCases.size());

        for(Case secCase : lstSecCases){
            setCaseNumbers.add(secCase.Enterprise_Security_Case_Number__c);
            mapCaseStatus.put(secCase.Enterprise_Security_Case_Number__c, secCase.Status);
        }

        if(setCaseNumbers.size() > 0) {
            List<Case__x> lstSptCases = [SELECT ExternalId,
                                                Status__c,
                                                CaseNumber__c
                                        FROM Case__x
                                        WHERE CaseNumber__c IN :setCaseNumbers];

            List<Case__x> lstSptStatusChangeCases = new List<Case__x>();

            for(Case__x sptCase : lstSptCases) {
                
                if(!mapCaseStatus.containsKey(sptCase.CaseNumber__c)) continue;
                
                if(sptCase.Status__c != 'Closed' && sptCase.Status__c != mapCaseStatus.get(sptCase.CaseNumber__c)) {
                    sptCase.Status__c = mapCaseStatus.get(sptCase.CaseNumber__c);
                    lstSptStatusChangeCases.add(sptCase);
                }
            }

            if(lstSptStatusChangeCases.size() > 0){
                List<Database.SaveResult> sr = Database.updateImmediate(lstSptStatusChangeCases);
            }

        }
        
    }

    public void finish(Database.BatchableContext BC) {
        if(!Test.isRunningTest()) {
        abortCurrentAndScheduleNextExecution(scCtx, 5);
           //SPT_SyncSecOrgCaseStatusToSptCase_Old batch = new SPT_SyncSecOrgCaseStatusToSptCase_Old();
           //System.scheduleBatch(batch, 'SPT_SyncSecOrgCaseStatusToSptCase_Old', 5);
        }
    }
    
}
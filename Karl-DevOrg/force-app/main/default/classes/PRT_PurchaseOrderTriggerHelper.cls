/*
* Created by - Prashant Gupta (MTX)
*/
public class PRT_PurchaseOrderTriggerHelper {
    /*
    * Created by - Prashant Gupta (MTX)
    * This function is used to create Sharing records for Internal Users
    */
	public static void createInternalUserSharing(List<Purchase_Order__c> newList, Map<id,Purchase_Order__c> oldMap){
        Set<Id> projectIdSet = new Set<Id>();
        Map<id,Set<Purchase_Order__c>> projectVsPurchaseOrderSet = new Map<id,Set<Purchase_Order__c>>(); 
        for(Purchase_Order__c purchaseO : newList){
            if(purchaseO.Project__c !=null && 
               	(oldMap==null || purchaseO.Project__c!=oldMap.get(purchaseO.id).Project__c)){
                    if(!projectVsPurchaseOrderSet.containsKey(purchaseO.Project__c)){
                        projectVsPurchaseOrderSet.put(purchaseO.Project__c, new Set<Purchase_Order__c>());
                    }
                    projectVsPurchaseOrderSet.get(purchaseO.Project__c).add(purchaseO);
            }
        }
        List<Purchase_Order__Share> purchaseOrderShareList = new List<Purchase_Order__Share>();
        if(projectVsPurchaseOrderSet!=null && !projectVsPurchaseOrderSet.isEmpty()){
            for(Project__c pro : [SELECT Id, Project_Manager__c,Executive_Sponsor_Internal__c,Sponsor_Internal__c
                                    FROM Project__c WHERE id in: projectVsPurchaseOrderSet.keySet()]){
            	for(Purchase_Order__c record: projectVsPurchaseOrderSet.get(pro.id)){
                    if(pro.Executive_Sponsor_Internal__c!=record.OwnerID && pro.Executive_Sponsor_Internal__c!=null){
                    	purchaseOrderShareList.add(PRT_Utility.preparePurchaseOrderShareObject(record.id,pro.Executive_Sponsor_Internal__c,'Read'));
                    }
                    if(pro.Sponsor_Internal__c!=record.OwnerID && pro.Sponsor_Internal__c!=null){
                    	purchaseOrderShareList.add(PRT_Utility.preparePurchaseOrderShareObject(record.id,pro.Sponsor_Internal__c,'Read'));
                    }
                    if(pro.Project_Manager__c!=record.OwnerID && pro.Project_Manager__c!=null){
                    	purchaseOrderShareList.add(PRT_Utility.preparePurchaseOrderShareObject(record.id,pro.Project_Manager__c,'Read'));
                    }
                }   
        	}
            List<Purchase_Order__Share> purchaseOrderSharingToDelete = new List<Purchase_Order__Share>([SELECT Id 
                                                                             FROM Purchase_Order__Share 
                                                                             WHERE RowCause = 'Manual' ]);
            if(purchaseOrderSharingToDelete!=null && !purchaseOrderSharingToDelete.isEmpty()){
            	delete purchaseOrderSharingToDelete;                
            }
            if(purchaseOrderShareList!=null && !purchaseOrderShareList.isEmpty()){
                insert purchaseOrderShareList;
            }
        }
    }
}
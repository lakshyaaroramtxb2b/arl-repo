@isTest
public class INTEG_EmployeeToContactMapperTest {
    

    @isTest
    private static Account createAccount() {
        RecordType busUnit = [SELECT Id FROM RecordType WHERE SObjectType='Account' AND Name='Salesforce Business Unit'];
        Account sfdcAccount = new Account(Name='salesforce.com', RecordTypeId=busUnit.Id);
        insert sfdcAccount;                
        return sfdcAccount; 
    }
    
    @isTest 
    public static void testConstructor() {
        RecordType rt = [SELECT Id FROM RecordType WHERE SObjectType='Contact' 
                         AND DeveloperName='Salesforce_Employee' AND IsActive=true AND NamespacePrefix = ''];
        Account sfdcAccount = createAccount();                
        INTEG_EmployeeToContactMapper cls = new INTEG_EmployeeToContactMapper();
        System.assert(cls.rt.Id == rt.Id);
        System.assert(INTEG_EmployeeToContactMapper.sfdcAccount.Id == sfdcAccount.Id);
    }
    
    @isTest 
    public static void testRecordTypeForContact() {
        // tested in constructor test
    }
    
    @isTest
    public static void testContactsForEmployees() {
        Account sfdcAccount = createAccount();
        INTEG_EmployeeToContactMapper cls = new INTEG_EmployeeToContactMapper();
        
        // Normal case
        // System.assert(res != null);
        // System.assert(res.Id == c.Id);
        INTEG_Employee__c emp = new INTEG_Employee__c(INTEG_Org62UserId__c='005123451234512345');
        Contact c = new Contact(AccountId=sfdcAccount.Id, Org62_User_ID__c='005123451234512345',FirstName='Joe',LastName='User',Email='JoeUser@user.com', RecordTypeId=cls.rt.Id);
        insert c;
        
        // No match, should be null
        INTEG_Employee__c emp2 = new INTEG_Employee__c(INTEG_Org62UserId__c='005678906789067890');
        
        // Unmatchable
        INTEG_Employee__c emp3 = new INTEG_Employee__c(INTEG_Org62UserId__c = null);
        
        Map<INTEG_Employee__c, Contact> res = cls.contactsForEmployees(new INTEG_Employee__c[] {emp, emp2, emp3});
        
        System.assert(null == res.get(emp3));
        System.assert(null == res.get(emp2));
        System.assert(null != res.get(emp));
        System.assert(c.Id == res.get(emp).Id);
    }
    
    @isTest
    public static void testIsMappable() {
        createAccount();    
        INTEG_Employee__c emp = new INTEG_Employee__c();
        System.assert(false == INTEG_EmployeeToContactMapper.isMappable(emp));
        emp.INTEG_Org62UserId__c='';
        System.assert(false == INTEG_EmployeeToContactMapper.isMappable(emp));
        emp.INTEG_Org62UserId__c=null;        
        System.assert(false == INTEG_EmployeeToContactMapper.isMappable(emp));
        emp.INTEG_Org62UserId__c='005123451234512345';
        System.assert(true == INTEG_EmployeeToContactMapper.isMappable(emp));
    }
    
    @isTest
    public static void testCopyEmployeeFieldsToContact() {
        createAccount();
        Date adate = Datetime.now().date();
        
        INTEG_Employee__c mgr = new INTEG_Employee__c(Name='blah',INTEG_Location__c='alocation');
        insert mgr;
        
        INTEG_Employee__c emp = new INTEG_Employee__c(
            INTEG_Active__c=true,
            INTEG_Business_Unit__c='integ_business_unit__c',
            INTEG_Company__c='integ_company__c',
            INTEG_Continuous_Service_Date__c=adate,
            INTEG_Cost_Center__c='integ_cost_center__c',
            INTEG_Date_Of_Birth_Without_Year__c='integ_date_of_birth_without_year__c',
            INTEG_Division__c='integ_division__c',
            INTEG_Email__c='integ_email__c@email.com',
            INTEG_Employee_ID__c='integ_empld__c',
            INTEG_Employee_Type__c='integ_employee_type__c',
            INTEG_First_Name__c='integ_first_name__c',
            INTEG_Grade_Group__c='integ_grade_group__c',
            INTEG_Hire_Date__c=adate,
            INTEG_Is_Manager__c=true,
            INTEG_Job_Family__c='integ_job_family__c',
            INTEG_Job_Profile__c='integ_job_profile__c',
            INTEG_Job_Profile_Start_Date__c=adate,
            INTEG_Job_Title__c='integ_job_title__c',
            INTEG_Last_Day_Worked__c=adate,
            INTEG_Last_Name__c='integ_last_name__c',
            INTEG_Last_Sync_Date__c=adate,
            INTEG_Legal_First_Name__c='integ_legal_first_name__c',
            INTEG_Legal_Last_Name__c='integ_legal_last_name__c',
            INTEG_Location__c='integ_location__c',
            INTEG_Manager__c=mgr.Id,
            INTEG_Manager_Email__c='integ_manager_email__c@email.com',
            INTEG_Manager_ID__c='integ_manager_id__c',
            INTEG_Manager_Level_1__c='integ_manager_level_1__c',
            INTEG_Manager_Level_10__c='integ_manager_level_10__c',
            INTEG_Manager_Level_2__c='integ_manager_level_2__c',
            INTEG_Manager_Level_3__c='integ_manager_level_3__c',
            INTEG_Manager_Level_4__c='integ_manager_level_4__c',
            INTEG_Manager_Level_5__c='integ_manager_level_5__c',
            INTEG_Manager_Level_6__c='integ_manager_level_6__c',
            INTEG_Manager_Level_7__c='integ_manager_level_7__c',
            INTEG_Manager_Level_8__c='integ_manager_level_8__c',
            INTEG_Manager_Level_9__c='integ_manager_level_9__c',
            INTEG_Manager_Title__c='integ_manager_title__c',
            INTEG_Original_Hire_Date__c=adate,
            INTEG_Perm_Check_ID__c='integ_perm_check_id__c',
            INTEG_Phone__c='integ_phone__c',
            INTEG_Source__c='integ_source__c',
            SPTFRC_Supportforce_ID__c='sptfrc_spfce_id__c',
            INTEG_Title__c='integ_title__c',
            WRKDAY_Workday_ID__c='wrkday_workday_id__c',
            INTEG_Is_On_Leave__c=True,
            INTEG_Org62UserId__c='005123451234512345'
        );
        insert emp;
        
        Contact cont = new Contact();
        
        cont = INTEG_EmployeeToContactMapper.copyEmployeeFieldsToContact(emp, cont);
        for (String empKey : INTEG_EmployeeToContactMapper.FIELDMAP.keySet()) {
            String contKey = INTEG_EmployeeToContactMapper.FIELDMAP.get(empKey);
            if (!String.isBlank(contKey)) {
                System.assert(cont.get(contKey) == emp.get(empKey));
            }
        }
        
        System.assert(cont.Manager__c == emp.INTEG_Manager__r.Name);
        System.assert(cont.Manager_Location__c == emp.INTEG_Manager__r.INTEG_Location__c);
        System.assert(cont.Is_on_Leave_Of_Absence__c == True);
        System.assert(cont.Org62_User_ID__c == 'sptfrc_spfce_id__c');
    }
    
    @isTest
    public static void testGetEmployeesQueryLocator() {
        createAccount();
        DateTime modifiedSince = DateTime.now();
        Database.QueryLocator ql = INTEG_EmployeeToContactMapper.getEmployeesQueryLocator(modifiedSince);
        System.assert(ql != null);
    }
    
    @isTest
    public static void testUpdateContactsForEmployees() {
        Account sfdcAccount = createAccount();
        
        Datetime old = Datetime.now().addDays(-1);

        INTEG_Employee__c tooOld = new INTEG_Employee__c(INTEG_Org62UserId__c='005123451234512345',INTEG_Location__c='abcloc',INTEG_First_Name__c='tooOld',INTEG_Last_Name__c='last');
        insert tooOld;
        Test.setCreatedDate(tooOld.Id, old);
        
        INTEG_Employee__c tooOldWithHistory = new INTEG_Employee__c(INTEG_Org62UserId__c='005123451234512346',INTEG_Location__c='abcdloc',INTEG_First_Name__c='tooOldWithHistory',INTEG_Last_Name__c='last');
        insert tooOldWithHistory;
        update tooOldWithHistory;
        Test.setCreatedDate(tooOldWithHistory.Id, old);
        
        DateTime now = DateTime.now().addHours(-1);
        INTEG_EmployeeToContactMapper cls = new INTEG_EmployeeToContactMapper();
        
        INTEG_Employee__c hasExistingContact = new INTEG_Employee__c(INTEG_Org62UserId__c='005123451234512348',INTEG_Location__c='aloc',INTEG_First_Name__c='hasExistingContact',INTEG_Last_Name__c='last');
        insert hasExistingContact;
        Contact existingContact = new Contact(AccountId=sfdcAccount.Id,Org62_User_ID__c='005123451234512348', FirstName='joe', LastName='user', RecordTypeId=cls.rt.Id,Last_Import_Date__c=Date.today().addDays(-10));
        insert existingContact;
        Test.setCreatedDate(existingContact.Id,old);
        
        INTEG_Employee__c unmappable = new INTEG_Employee__c(Name='steve', INTEG_First_Name__c='unmappable',INTEG_Last_Name__c='last');
        insert unmappable;
        
        INTEG_Employee__c toCreate = new INTEG_Employee__c(Name='ed', INTEG_Org62UserId__c='005123451234512349', INTEG_Location__c='anotherloc',INTEG_First_Name__c='toCreate',INTEG_Last_Name__c='last');
        insert toCreate;
        
        INTEG_Employee__c newContractor = new INTEG_Employee__c(Name='contractor', INTEG_Employee_ID__c='CONT', INTEG_Location__c='contloc',INTEG_First_Name__c='con',INTEG_Last_Name__c='tractor',
                                                                 IsContractor__c=true, INTEG_Email__c = 'contractor@contcompany.com', INTEG_Org62UserId__c='005123451234512310');
        insert newContractor;
 
        // Missing first and last name so it can't be saved as a contact
        INTEG_Employee__c cantCreate = new INTEG_Employee__c(Name='ed', INTEG_Org62UserId__c='005123451234512311', INTEG_Location__c='anotherloc');
        insert cantCreate;
        
        Database.QueryLocator ql = INTEG_EmployeeToContactMapper.getEmployeesQueryLocator(now);
        
        INTEG_Employee__c[] emps = new List<INTEG_Employee__c>();
        for (INTEG_Employee__c emp : Database.query(ql.getQuery())) {
            emps.add(emp);
        }

        Integer res = cls.updateContactsForEmployees(emps);
        System.debug(res);
        System.assert(res == 4);

        existingContact = [SELECT Id, Location__c,Last_Import_Date__c FROM Contact WHERE Id=:existingContact.Id];
        System.assert(existingContact.Location__c == 'aloc',existingContact.Location__c);
        System.assert(existingContact.Last_Import_Date__c == Date.today());
        
        // the toCreate          
        Contact newCont = [SELECT Id,AccountId FROM Contact WHERE Org62_User_ID__c='005123451234512349' AND Location__c='anotherloc'];
        System.assert(newCont != null);
        System.assert(newCont.AccountId == sfdcAccount.Id);

        
        
        // the contractor
        Contact contractor = [SELECT Id,AccountId FROM Contact WHERE Org62_User_ID__c='005123451234512310' AND Location__c='contloc'];
        System.assert(contractor != null);
        System.assert(contractor.AccountId == sfdcAccount.Id);
 
        cantCreate = [SELECT INTEG_Last_Data_Error__c, INTEG_Last_Data_Error_Time__c FROM INTEG_Employee__c
                      WHERE Id=:cantCreate.Id];
        System.assert(cantCreate.INTEG_Last_Data_Error__c != null);
        System.assert(cantCreate.INTEG_Last_Data_Error_Time__c != null);
        
    }
    
    @isTest
    public static void testCheckReportErrors() {
        createAccount();
        
        INTEG_Employee__c empBad = new INTEG_Employee__c(INTEG_Org62UserId__c='005123451234512345');
        INTEG_Employee__c empGood = new INTEG_Employee__c(INTEG_Org62UserId__c='005123451234512346');
        insert new INTEG_Employee__c[]{empBad, empGood};
        
        Contact bad = new Contact(Org62_User_ID__c='005123451234512345');
        Contact good = new Contact(FirstName='joe',LastName='user',Org62_User_ID__c='005123451234512346');
        Contact[] upserted = new Contact[]{bad, good};
        Database.UpsertResult[] res = Database.upsert(upserted, False);
        
        INTEG_EmployeeToContactMapper cls = new INTEG_EmployeeToContactMapper();
        cls.checkReportErrors(res, upserted);
        empBad = [SELECT Id, INTEG_Last_Data_Error__c, INTEG_Last_Data_Error_Time__c FROM INTEG_Employee__c 
                  WHERE Id=:empBad.id];
        empGood = [SELECT Id, INTEG_Last_Data_Error__c, INTEG_Last_Data_Error_Time__c FROM INTEG_Employee__c 
                  WHERE Id=:empGood.id];
        System.assert(empBad.INTEG_Last_Data_Error_Time__c != null);
        System.assert(empBad.INTEG_Last_Data_Error__c != null);
        System.assert(empGood.INTEG_Last_Data_Error_Time__c == null);
        System.assert(empGood.INTEG_Last_Data_Error__c == null);
    }
    
}
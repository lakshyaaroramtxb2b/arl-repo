global with sharing class TRAC_Metrics_Controller {
    global class ITTicket {
        // placeholder for deserialization
        public Integer id;
        public String product_id;
        public Datetime date_opened;
        public Datetime date_closed;
        public Integer severity;
        public String current_status;
    }
    
    public String getChartMeanTimeOverTimeCode() {
        return [SELECT Body FROM ApexClass WHERE Name= 'ChartMeanTimeOverTime'].get(0).body;
    }

    public String getCharterCountsCode() {
        return [SELECT Body FROM ApexClass WHERE Name= 'CharterCounts'].get(0).body;
    }

    public String getCharterSLAsCode() {
        return [SELECT Body FROM ApexClass WHERE Name= 'CharterSLAs'].get(0).body;
    }
    
    
    /* The Paula list */
    
    @RemoteAction
    global static Map<Long,Map<String,Map<String,Double>>> meanTimeToDiscovery() {
        ChartMeanTimeOverTime charter = new ChartMeanTimeOverTime();
        charter.objectName = 'Case';
        charter.fieldName1 = 'Incident_Date__c';
        charter.fieldName2 = 'CreatedDate';
        charter.precision = 'hours';
        charter.period    = 'monthly';
        charter.extraWhere = 'RecordType.DeveloperName = \'CSIRT_Security_Incident\' AND (Incident_Severity__c = \'Severity 1 (High impact)\' OR Incident_Severity__c = \'Severity 2 (Medium impact)\' OR Incident_Severity__c = \'Severity 3 (Low impact)\')';
        return charter.meanTimeByFieldValueOverTimeLong('Incident_Severity__c');
    }

    @RemoteAction    
    global static Map<Long,Map<String,Map<String,Double>>> meanTimeToResponse() {
        ChartMeanTimeOverTime charter = new ChartMeanTimeOverTime();
        charter.objectName = 'Case';
        charter.fieldName1 = 'CreatedDate';
        charter.fieldName2 = 'Investigation_Date__c';
        charter.precision = 'hours';
        charter.period    = 'monthly';
        //charter.includeGroupingVals = new Set<String>{'Severity 1 (High impact)','Severity 2 (Medium impact)','Severity 3 (Low impact)'};
        charter.extraWhere = 'RecordType.DeveloperName = \'CSIRT_Security_Incident\' AND (Incident_Severity__c = \'Severity 1 (High impact)\' OR Incident_Severity__c = \'Severity 2 (Medium impact)\' OR Incident_Severity__c = \'Severity 3 (Low impact)\')';
        return charter.meanTimeByFieldValueOverTimeLong('Incident_Severity__c');
    }
    
    @RemoteAction    
    global static Map<Long,Map<String,Map<String,Double>>> meanTimeToResolution() {
        ChartMeanTimeOverTime charter = new ChartMeanTimeOverTime();
        charter.objectName = 'Case';
        charter.fieldName1 = 'CreatedDate';
        charter.fieldName2 = 'Containment_Date__c';
        charter.precision = 'hours';
        charter.period    = 'monthly';
        charter.extraWhere = 'RecordType.DeveloperName = \'CSIRT_Security_Incident\' AND (Incident_Severity__c = \'Severity 1 (High impact)\' OR Incident_Severity__c = \'Severity 2 (Medium impact)\' OR Incident_Severity__c = \'Severity 3 (Low impact)\')';
        return charter.meanTimeByFieldValueOverTimeLong('Incident_Severity__c');
    }
    
    @RemoteAction    
    global static Map<Long,Map<String,Map<String,Double>>> meanTimeToClosure() {
        ChartMeanTimeOverTime charter = new ChartMeanTimeOverTime();
        charter.objectName = 'Case';
        charter.fieldName1 = 'CreatedDate';
        charter.fieldName2 = 'ClosedDate';
        charter.precision = 'days';
        charter.period    = 'monthly';
        charter.extraWhere = 'RecordType.DeveloperName = \'CSIRT_Security_Incident\' AND (Incident_Severity__c = \'Severity 1 (High impact)\' OR Incident_Severity__c = \'Severity 2 (Medium impact)\' OR Incident_Severity__c = \'Severity 3 (Low impact)\')';
        return charter.meanTimeByFieldValueOverTimeLong('Incident_Severity__c');
    }

    @RemoteAction    
    global static Map<Datetime,Map<String,Map<String,Integer>>> respondedWithinSLA() {
        // Numbers are in minutes
        Map<String,Integer> SLAs = new Map<String,Integer>{'Severity 1 (High impact)'=>30,'Severity 2 (Medium impact)'=>120,'Severity 3 (Low impact)'=>240};
        CharterSLAs charter = new CharterSLAs();
        charter.objectName = 'Case';
        charter.fieldName1 = 'CreatedDate';
        charter.fieldName2 = 'Investigation_Date__c';
        charter.precision = 'minutes';
        charter.period    = 'monthly';
        charter.extraWhere = 'RecordType.DeveloperName = \'CSIRT_Security_Incident\'';
        return charter.percentageWithinSLAOverTime(SLAs,'Incident_Severity__c');
    }
    
    @RemoteAction    
    global static Map<Datetime,Map<String,Map<String,Integer>>> resolvedWithinSLA() {
        // Numbers are in minutes
        Map<String,Integer> SLAs = new Map<String,Integer>{'Severity 1 (High impact)'=>48,'Severity 2 (Medium impact)'=>48,'Severity 3 (Low impact)'=>48};
        CharterSLAs charter = new CharterSLAs();
        charter.objectName = 'Case';
        charter.fieldName1 = 'CreatedDate';
        charter.fieldName2 = 'Containment_Date__c';
        charter.precision = 'hours';
        charter.period    = 'monthly';
        charter.extraWhere = 'RecordType.DeveloperName = \'CSIRT_Security_Incident\'';
        return charter.percentageWithinSLAOverTime(SLAs,'Incident_Severity__c');
    }
   
    @RemoteAction    
    global static Map<Datetime,Map<String,Map<String,Integer>>> closedWithinSLA() {
        // Numbers are in minutes
        Map<String,Integer> SLAs = new Map<String,Integer>{'Severity 1 (High impact)'=>30,'Severity 2 (Medium impact)'=>30,'Severity 3 (Low impact)'=>30};
        CharterSLAs charter = new CharterSLAs();
        charter.objectName = 'Case';
        charter.fieldName1 = 'CreatedDate';
        charter.fieldName2 = 'ClosedDate';
        charter.precision = 'days';
        charter.period    = 'monthly';
        charter.extraWhere = 'RecordType.DeveloperName = \'CSIRT_Security_Incident\'';
        System.debug(charter.percentageWithinSLAOverTime(SLAs,'Incident_Severity__c'));
        return charter.percentageWithinSLAOverTime(SLAs,'Incident_Severity__c');
    }
    
        
    /* Generic charting. These are for ad-hoc charting */
    @RemoteAction
    global static Map<Long,Map<String,Map<String,Double>>> meanTimeOverTime(String objectName, String field1, String field2, String period, String precision) { 
        ChartMeanTimeOverTime charter = new ChartMeanTimeOverTime();
        charter.objectName = objectName;
        charter.fieldName1 = field1;
        charter.fieldName2 = field2;
        charter.period = period;
        charter.precision = precision;
        if (objectName.toLowerCase() == 'case') {
            charter.extraWhere = 'RecordType.DeveloperName = \'CSIRT_Security_Incident\' AND (Incident_Severity__c = \'Severity 1 (High impact)\' OR Incident_Severity__c = \'Severity 2 (Medium impact)\' OR Incident_Severity__c = \'Severity 3 (Low impact)\')';
        }
        return charter.meanTimeByFieldValueOverTimeLong('Incident_Severity__c');
    }
    
    global static List<String> getAvailableObjects() {
        Set<String> RELEVANT_OBJS = new Set<String>{'csirt_incident_task__c','case','csirt_external_case__c'};
    
        Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();
        List<String> res = new List<String>();
        for (String objName : gd.keySet()) {
            if (!RELEVANT_OBJS.contains(objName)) {
                // we probably don't care about this and it clutters the list
                continue;
            }
            if (gd.get(objName).getDescribe().isAccessible()) {
                res.add(objName);
            }
        }
        return res;
    }
    
    @RemoteAction
    global static List<String> fieldsForObject(String objName) {
        List<String> res = new List<String>();
        Schema.SObjectType objdesc = Schema.getGlobalDescribe().get(objName);
        Map<String, Schema.SObjectField> fields = objdesc.getDescribe().fields.getMap();
        
        SObject blah = objdesc.newSObject();
        Datetime now = Datetime.now();
        for (String field : fields.keySet()) {
            if (fields.get(field).getDescribe().isAccessible()) {
                // ok, they can see it. Is it a date field?
                try {
                    blah.put(field,now);
                } catch (Exception e) {
                    // not a datetime field
                    continue;
                }
                // OK, it is a datetime field and is accessible
                res.add(field);
            }
        }
        return res;    
    }
    
    @RemoteAction
    global static Map<Long,Map<String,Map<String,Double>>> ItMeanTimeToClosure(ITTicket[] records) {
        SObject[] converted = new SObject[]{};
        for (ITTicket ticket : records) {
            SObject obj = new Case();
            //obj.put('CreatedDate',ticket.date_opened);
            obj.put('charter_field1__c',ticket.date_opened);
            //obj.put('ClosedDate',ticket.date_closed);
            obj.put('charter_field2__c',ticket.date_closed);
            obj.put('Status',ticket.current_status);
            obj.put('Priority',String.valueOf(ticket.severity));
            obj.put('Subject',ticket.product_id);
            converted.add(obj);
        }
        ChartMeanTimeOverTime charter = new ChartMeanTimeOverTime();
        charter.setRecords(converted);
        //charter.fieldName1 = 'CreatedDate';
        charter.fieldName1 = 'charter_field1__c';
        //charter.fieldName2 = 'ClosedDate';
        charter.fieldName2 = 'charter_field2__c';
        charter.precision = 'days';
        charter.period    = 'monthly';
        return charter.meanTimeByFieldValueOverTimeLong('Priority');
    } 
    
    @RemoteAction
    global static Map<Datetime,Map<String,Map<String,Integer>>> ItClosedWithinSLA(ITTicket[] records) {
        SObject[] converted = new SObject[]{};
        for (ITTicket ticket : records) {
            SObject obj = new Case();
            //obj.put('CreatedDate',ticket.date_opened);
            obj.put('charter_field1__c',ticket.date_opened);
            //obj.put('ClosedDate',ticket.date_closed);
            obj.put('charter_field2__c',ticket.date_closed);
            obj.put('Status',ticket.current_status);
            obj.put('Priority',String.valueOf(ticket.severity));
            obj.put('Subject',ticket.product_id);
            converted.add(obj);
        }
        
        Map<String,Integer> SLAs = new Map<String,Integer>{'0'=>7,'1'=>30,'2'=>90};
        CharterSLAs charter = new CharterSLAs();
        charter.setRecords(converted);
        //charter.fieldName1 = 'CreatedDate';
        charter.fieldName1 = 'charter_field1__c';
        //charter.fieldName2 = 'ClosedDate';
        charter.fieldName2 = 'charter_field2__c';
        charter.precision = 'days';
        charter.period    = 'monthly';
        return charter.percentageWithinSLAOverTime(SLAs,'Priority');
    }
    
    @RemoteAction
    global static Map<Long,Map<String,Integer>> ItTicketsOpenedByMonth(ITTicket[] records) {
        SObject[] converted = new SObject[]{};
        for (ITTicket ticket : records) {
            SObject obj = new Case();
            //obj.put('CreatedDate',ticket.date_opened);
            obj.put('charter_field1__c',ticket.date_opened);
            //obj.put('ClosedDate',ticket.date_closed);
            obj.put('charter_field2__c',ticket.date_closed);
            obj.put('Status',ticket.current_status);
            obj.put('Priority',String.valueOf(ticket.severity));
            obj.put('Subject',ticket.product_id);
            converted.add(obj);
        }
        
        ChartMeanTimeOverTime charter = new ChartMeanTimeOverTime();
        charter.setRecords(converted);
        //charter.fieldName1 = 'CreatedDate';
        charter.fieldName1 = 'charter_field1__c';
        charter.precision = 'days';
        charter.period    = 'monthly';
        return charter.countByFieldValueOverTimeLong('Priority');
    }
    
    @RemoteAction
    global static Map<Long,Map<String,Integer>> ItTicketsRemainingOpenByMonth(ITTicket[] records) {
        SObject[] converted = new SObject[]{};
        for (ITTicket ticket : records) {
            SObject obj = new Case();
            //obj.put('CreatedDate',ticket.date_opened);
            obj.put('charter_field1__c',ticket.date_opened);
            //obj.put('ClosedDate',ticket.date_closed);
            obj.put('charter_field2__c',ticket.date_closed);
            obj.put('Status',ticket.current_status);
            obj.put('Priority',String.valueOf(ticket.severity));
            obj.put('Subject',ticket.product_id);
            converted.add(obj);
        }
        
        CharterCounts charter = new CharterCounts();
        charter.setRecords(converted);
        //charter.fieldName1 = 'CreatedDate';
        charter.fieldName1 = 'charter_field1__c';
        //charter.fieldName2 = 'ClosedDate';
        charter.fieldName2 = 'charter_field2__c';
        charter.period    = 'monthly';
        return charter.countByDateSpanValueOverTimeLong('Priority');
    }
}
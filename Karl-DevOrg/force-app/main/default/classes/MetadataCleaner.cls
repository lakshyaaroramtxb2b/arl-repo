public class MetadataCleaner {
    private static MetadataService.MetadataPort metadataService;

    @future(callout=true)
    public static void wipeNamedCredentialAndExternalDataSourceLoginCredentials(){

        try {
            MetadataObjects metadataToUpdate = getMetadataRecords(new String[]{'NamedCredential','ExternalDataSource'});
            if(metadataToUpdate != null) updateRecords(metadataToUpdate);
        } catch (Exception ex) {
            System.debug(LoggingLevel.ERROR,'MetadataCleaner EXCEPTION Message: '+ex.getMessage());
            System.debug(LoggingLevel.ERROR,'MetadataCleaner EXCEPTION StackTrace: '+ex.getStackTraceString());
        }
    }

    private static MetadataObjects getMetadataRecords(String[] metadataTypes){
        final Integer METADATA_API_VERSION = Integer.valueOf(new MetadataService.MetadataPort().endpoint_x.substringAfterLast('/'));
        MetadataService.ListMetadataQuery[] queries = new MetadataService.ListMetadataQuery[]{};
        String[] namedCredentialFullNames = new String[]{};
        String[] externalDataSourceFullNames = new String[]{};
        MetadataObjects metadataRecords = new MetadataObjects();
        metadataService = createService();
        System.debug('MetadataCleaner metadataService: '+metadataService);

        //Set metadata types
        for (String type : metadataTypes) {
            MetadataService.ListMetadataQuery queryList = new MetadataService.ListMetadataQuery();
            queryList.type_x = type;
            queries.add(queryList);
        }
        //Get metadata records based on metadata types
        MetadataService.FileProperties[] metadataList = metadataService.listMetadata(queries, METADATA_API_VERSION);

        //No metadata records?
        if(metadataList == null || metadataList.isEmpty())
            return null;
        System.debug('MetadataCleaner metadataList: '+metadataList);

        //Get fullname from each record
        for (MetadataService.FileProperties metadata : metadataList) {
            if(metadata.type_x == 'NamedCredential') { namedCredentialFullNames.add(metadata.fullName); }
            else if(metadata.type_x == 'ExternalDataSource') { externalDataSourceFullNames.add(metadata.fullName); }
        }
        System.debug('MetadataCleaner namedCredentialFullNames: '+namedCredentialFullNames);
        System.debug('MetadataCleaner externalDataSourceFullNames: '+externalDataSourceFullNames);

        //Use fullname to get all metadata settings from each record
        if(!namedCredentialFullNames.isEmpty()){
            metadataRecords.namedCredential = (MetadataService.NamedCredential[]) metadataService.readMetadata('NamedCredential', namedCredentialFullNames).getRecords();
        }
        if(!externalDataSourceFullNames.isEmpty()){
            metadataRecords.externalDataSource = (MetadataService.ExternalDataSource[]) metadataService.readMetadata('ExternalDataSource', externalDataSourceFullNames).getRecords();
        }

        System.debug('MetadataCleaner metadataRecords: '+metadataRecords);

        return metadataRecords;
    }

    private static void updateRecords(MetadataObjects metadataRecords){
        MetadataService.Metadata[] metadataToUpdate = new MetadataService.Metadata[]{};
        MetadataService.Metadata[] itemsToUpdateInBatches = new MetadataService.Metadata[]{};
        MetadataService.SaveResult[] updatedItems = new MetadataService.SaveResult[]{};
        Integer itemsCounter = 0;

        //Update record values
        for (Integer i = 0; i < metadataRecords.namedCredential.size(); i++) {
            if(metadataRecords.namedCredential[i].username != null && !metadataRecords.namedCredential[i].username.contains('_invalid')) {
                metadataRecords.namedCredential[i].username += '_invalid';
            }
            metadataRecords.namedCredential[i].password = 'invalid_password';
        }
        metadataToUpdate.addAll(metadataRecords.namedCredential);

        for (Integer i = 0; i < metadataRecords.externalDataSource.size(); i++) {
            if(metadataRecords.externalDataSource[i].username != null && !metadataRecords.externalDataSource[i].username.contains('_invalid')) {
                metadataRecords.externalDataSource[i].username += '_invalid';
            }
            metadataRecords.externalDataSource[i].password = 'invalid_password';
        }
        metadataToUpdate.addAll(metadataRecords.externalDataSource);


        //Update Metadata in batches of 10 items (10 is governor limit for Metadata API to process updates)
        for (Integer i = 0; i < metadataToUpdate.size(); i++) {
            itemsToUpdateInBatches.add(metadataToUpdate[i]);
            itemsCounter++;

            //Run update when 10 items inside list
            if(itemsCounter == 10) {
                System.debug('MetadataCleaner itemsToUpdateInBatches: '+itemsToUpdateInBatches);
                updatedItems.addAll(metadataService.updateMetadata(itemsToUpdateInBatches));
                itemsToUpdateInBatches.clear();
                itemsCounter = 0;
            }
        }
        //Check if list has remain items to update after leaving the loop
        if(!itemsToUpdateInBatches.isEmpty()) updatedItems.addAll(metadataService.updateMetadata(itemsToUpdateInBatches));
        System.debug('MetadataCleaner itemsToUpdateInBatches: '+itemsToUpdateInBatches);
        handleSaveResults(updatedItems);
    }

    private static MetadataService.MetadataPort createService()
    {
        MetadataService.MetadataPort service = new MetadataService.MetadataPort();
        service.SessionHeader = new MetadataService.SessionHeader_element();
        service.SessionHeader.sessionId = UserInfo.getSessionId();
        //NOTE: Check Prateek Sharma comment if we receive INVALID_SESSION_ID: https://andyinthecloud.com/2013/10/27/introduction-to-calling-the-metadata-api-from-apex/comment-page-3/#comment-37780
        //NOTE2: Check this link if we receive same exception: https://salesforce.stackexchange.com/questions/98068/metadata-api-invalid-session-id
        return service;
    }

    private static void handleSaveResults(MetadataService.SaveResult[] saveResults)
    {
        for (MetadataService.SaveResult saveResult : saveResults) {
            // Nothing to see?
            //if(saveResult==null || saveResult.success)
              //  return;
            // Construct error message and throw an exception
            if(saveResult.errors != null)
            {
                List<String> messages = new List<String>();
                messages.add(
                    (saveResult.errors.size()==1 ? 'Error ' : 'Errors ') + 'occured processing component ' + saveResult.fullName + '.');

                for(MetadataService.Error error : saveResult.errors)
                    messages.add(
                        error.message + ' (' + error.statusCode + ').' +
                        ( error.fields!=null && error.fields.size()>0 ?
                            ' Fields ' + String.join(error.fields, ',') + '.' : '' ) );

                if(messages.size()>0)
                    throw new MetadataServiceException(String.join(messages, ' '));
            }
            if(!saveResult.success)
                throw new MetadataServiceException('Request failed with no specified error.');
        }
    }

    private class MetadataObjects{
        MetadataService.NamedCredential[] namedCredential;
        MetadataService.ExternalDataSource[] externalDataSource;
    }

    public class MetadataServiceException extends Exception { }
}
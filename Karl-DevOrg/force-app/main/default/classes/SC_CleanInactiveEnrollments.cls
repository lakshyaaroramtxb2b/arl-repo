/**
 * @author: ralph@callaway.cloud
 */
public class SC_CleanInactiveEnrollments {
    
    private String filter;

    public SC_CleanInactiveEnrollments() {
        this.filter = 
            '(Contact__r.Is_Active__c = false ' +
            'OR Contact__r.Is_Frozen__c = true ' + 
            'OR Contact__r.Is_On_Leave_Of_Absence__c = true) ' +
            'AND (RecordTypeId = ' + 
                DynamicSOQLHelper.format(TrainingCourseTaken_Constants.RT_ID_MANDATORY) + ' ' +
            'OR Date_of_Escalation_to_Manager__c != null ' +
            'OR Date_of_Escalation_to_Trust_Engagement__c != null ' +
            'OR Due_Date__c != null ' +
            'OR Escalation1__c != null ' +
            'OR Escalation2__c != null ' +
            'OR Escalation3__c != null ' +
            'OR Escalation4__c != null ' +
            'OR Escalation5__c != null ' +
            'OR Last_Escalation__c != null ' +
            'OR Enrollment_Notification_Date__c != null) ' +
            'AND Training_Course__r.Tracked__c = true';
    }

    public Integer run() {
        Integer candidates = getCandidateCount(filter);
        if (candidates > 0) {
            SC_Unenroll_Batch.Options opts = new SC_Unenroll_Batch.Options(filter);
            Database.executeBatch(new SC_Unenroll_Batch(opts));
        }
        return candidates;
    }

    /**
     * Returns number of enrollments matching filter
     * @param  filter where clause
     * @return        Number of enrollments that match filter
     */
    private Integer getCandidateCount(String filter) {
        String query = 
            'SELECT COUNT(Id) cnt FROM Training_Course_Taken__c ' +
            'WHERE ' + filter;
        AggregateResult agg = Database.query(query);
        return (Integer) agg.get('cnt'); 
    }
}
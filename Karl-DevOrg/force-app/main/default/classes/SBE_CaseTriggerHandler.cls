/*
* Created by - Banshi (MTX Group Inc.)
* Handler class for SBE CASE TRIGGER, 
*/
public class SBE_CaseTriggerHandler {
	/*
	 * 	Method is used to track all method calls on beore update
     *  This method takes input of List of Case
     */
    public static void beforeUpdate(List<Case> caseList, Map<id,Case> oldMap, Map<id,Case> newMap){
        SBE_CaseTriggerHelper.updateExtensionStatus(caseList, oldMap);
    }
    
    /*
	 * 	Method is used to track all method calls on after update
     *  This method takes input of List of Case and oldMap
     */
    public static void afterUpdate(List<Case> caseList, Map<Id, Case> oldMap){
        SBE_CaseTriggerHelper.updateSBERecords(caseList, oldMap);
        SBE_CaseTriggerHelper.syncApprovalToSBE(caseList, oldMap);
    } 
}
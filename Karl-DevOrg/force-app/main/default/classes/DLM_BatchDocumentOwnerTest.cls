@isTest
public class DLM_BatchDocumentOwnerTest {
    @isTest
	static void DLM_BatchDocumentOwnerChangeTestBusinessUnit() {
        List<IS_Document__c> listOfDoc = DLM_TestDataFactory.createIsDocumentWithContacts(1,true);
        Set<Id> contactIds = new Set<Id>();
        for(IS_Document__c doc: listOfDoc){
            contactIds.add(doc.Content_Owner__c);
            contactIds.add(doc.Content_Custodian__c);
            
        }
        List<Contact> listToUpdate = new List<Contact>();
        Integer i=1;
        for(Contact c : [select id from Contact where id In :contactIds]){
            c.Business_Unit__c = '123';
            c.Manager_ID__c = listOfDoc[0].Id;
            listToUpdate.add(c);
        }
        update listToUpdate;
        List<Contact> contactMangers =  DLM_TestDataFactory.createContactsManager(1,listOfDoc[0].Id, true);
        
        Test.startTest();
        DLM_BatchDocumentOwnerChange bc = new DLM_BatchDocumentOwnerChange();
        Database.executeBatch(bc);
        Test.stopTest();
        listOfDoc = [Select Custodian_Business_Unit__c,Content_Business_Unit__c From IS_Document__c];
        for(IS_Document__c doc : listOfDoc){
            System.assertEquals('123', doc.Custodian_Business_Unit__c);
            System.assertEquals('123', doc.Content_Business_Unit__c);
        }
        
    }
    
	@isTest
	static void DLM_BatchDocumentOwnerChangeTest() {
        List<IS_Document__c> listOfDoc = DLM_TestDataFactory.createIsDocumentWithContacts(1,true);
        Set<Id> contactIds = new Set<Id>();
        for(IS_Document__c doc: listOfDoc){
            contactIds.add(doc.Content_Owner__c);
            contactIds.add(doc.Content_Custodian__c);
            
        }
        List<Contact> listToUpdate = new List<Contact>();
        Integer i=1;
        for(Contact c : [select id from Contact where id In :contactIds]){
           // c.Business_Unit__c = '123';
            c.Is_Active__c = false;
            c.Manager_ID__c = listOfDoc[0].Id;
            listToUpdate.add(c);
        }
        update listToUpdate;
        List<Contact> contactMangers =  DLM_TestDataFactory.createContactsManager(1,listOfDoc[0].Id, true);
        contactMangers[0].Business_Unit__c = '123'; 
        update contactMangers[0];
        Test.startTest();
        DLM_BatchDocumentOwnerChange bc = new DLM_BatchDocumentOwnerChange();
        Database.executeBatch(bc);
        Test.stopTest();
        listOfDoc = [Select Custodian_Business_Unit__c,Content_Business_Unit__c From IS_Document__c];
            
        for(IS_Document__c doc : listOfDoc){
            System.assertEquals('123', doc.Custodian_Business_Unit__c);
            System.assertEquals('123', doc.Content_Business_Unit__c);
        }   
    }
}
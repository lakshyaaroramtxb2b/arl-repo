/**
 * @author Ben Harvie
 * @email ben.harvie@salesforce.com
 * @description Controller for KARL_ExecuteFollowUpShareBatch aura(quick action) - modelled from MTX developed code
 */
public with sharing class KARL_ExecuteFollowUpShareBatch {
    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description This method runs the auditor request item sharing batch
     * @param minutesLength - how far back the batch job should search for records to share
     */
    @AuraEnabled
    public static void executeFollowUpShareBatch(Integer minutesLength){
        KARL_ShareFollowUpToAuditFirmBatch objBatch = new KARL_ShareFollowUpToAuditFirmBatch(minutesLength);
        Database.executebatch(objBatch,100);
    }
}
@isTest
public class IEM_CaseQuestionnaireControllerTest {
	
    @testSetup static void methodName() {
	}
    @isTest
    static void getRecordTypeIDTest() {
        ID caseRecordTypeID = Schema.SObjectType.Case.getRecordTypeInfosByName().get('IEM').getRecordTypeId();
    	system.assertEquals(IEM_CaseQuestionnaireController.getRecordId(),caseRecordTypeID);
    }	
    
    @isTest
    static void getrequiredFieldsSetTest() {
        List<String> requiredfields = IEM_CaseQuestionnaireController.getrequiredFieldsSet(null,null);
    	system.assertEquals(requiredfields.size() > 0,true);
    }
    @isTest
    static void getDependenciesTest() {
		Map<String,Map<String,String>> dependenciesMap = new Map<String,Map<String,String>>();
        dependenciesMap = IEM_CaseQuestionnaireController.getDependencies();
        System.assert(!dependenciesMap.isEmpty());
    }    
    @isTest
    static void getHelpURLTest() {
        IEM_CaseQuestionnaireController.getHelpURL('Intake Form');
        System.assert(IEM_CaseQuestionnaireController.getHelpURL('Intake Form')!=null && IEM_CaseQuestionnaireController.getHelpURL('Intake Form')!='');
    }
    @isTest
    static void createUserSharingAccessTest() {
		Map<String,Map<String,String>> dependenciesMap = new Map<String,Map<String,String>>();
        Profile profile = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User userRec = new User(Alias = 'standt', Email='standarduser@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = profile.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='standarduser994@testorg.com');
        insert userRec;
        List<Case> caseList = IEM_TestDataFactory.createCaseRecords(1,true);
        test.startTest();
        System.runAs(userRec) {
            IEM_CaseQuestionnaireController.createUserSharingAccess(caseList[0].id);
            List<CaseShare> caseShareList = new List<CaseShare>([SELECT id 
                                                                 FROM CaseShare 
                                                                 WHERE CaseId = :caseList[0].id
                                                                 AND UserOrGroupId = :userRec.id
                                                                 AND CaseAccessLevel = 'Edit']);
            system.debug('??' + caseShareList);
            system.assertEquals(1, caseShareList.size());
        }
        test.stopTest();
    }
    @isTest
    static void getOwnerAvailableSlotsTest() {
        Map<String,Map<String,String>> dependenciesMap = new Map<String,Map<String,String>>();
        Profile profile = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User userRec = new User(Alias = 'standt', Email='standarduser@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = profile.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='standarduser994@testorg.com');
        insert userRec;
        id currentUserId = UserInfo.getUserId();
        Set<Id> userIdSet = new Set<id>();
        userIdSet.add(userRec.id);
        userIdSet.add(currentUserId);
        
        //Fetch IEM_Case_Analysts group Id
        List<Group> groupList = [SELECT Id FROM Group WHERE DeveloperName='GRC_IEM_Case_Analysts' LIMIT 1];
		IEM_TestDataFactory.createGroupMemberRecords(userIdSet, groupList[0].id);
        for(GroupMember  gm :[select UserOrGroupId from GroupMember where groupId = :groupList[0].Id]){   
                userIdSet.add(gm.UserOrGroupId);
            }
        test.startTest();
        System.runAs(userRec) {
        	/*List<Case> caseList = IEM_TestDataFactory.createCaseRecords(3,true);
            id nextOwnerId= IEM_CaseQuestionnaireController.getNextCaseOwner();            
            system.assert(userIdSet.contains(nextOwnerId));
            */
            id nextOwnerId = userRec.Id;
            IEM_CaseQuestionnaireController.OwnerAvailability ownerWrapper = new IEM_CaseQuestionnaireController.OwnerAvailability();
            Event appointmentKept = new Event(subject = 'Office Hours',ownerid=nextOwnerId,
                                             StartDateTime = system.now().addhours(1), EndDateTime = system.now().addhours(5));
            insert appointmentKept;
            ownerWrapper = IEM_CaseQuestionnaireController.getOwnerAvailableSlots();
            system.assertEquals(!ownerWrapper.slots.isEmpty(), true);
        }
        Test.stopTest();
    }
    @isTest
    static void createEventTest() {
		Map<String,Map<String,String>> dependenciesMap = new Map<String,Map<String,String>>();
        Profile profile = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User userRec = new User(Alias = 'standt', Email='standarduser@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = profile.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='standarduser994@testorg.com');
        insert userRec;
        List<Case> caseList = IEM_TestDataFactory.createCaseRecords(1,true);
        test.startTest();
        System.runAs(userRec) {
            Boolean result = IEM_CaseQuestionnaireController.createEvent(DateTime.now(),userRec.id,caseList[0].id);
            system.assertEquals(True, result);
        }
        test.stopTest();
    }
    @isTest
    static void getIssueOwnerTest() {
        IEM_CaseQuestionnaireController.getIssueOwner('a7O4D0000006uziUAA');
    }
    
    
}
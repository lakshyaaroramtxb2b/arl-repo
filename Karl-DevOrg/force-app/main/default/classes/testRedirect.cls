public with sharing class testRedirect{
    public Pagereference doRedirect(){
        return new Pagereference (ApexPages.CurrentPage().getParameters().get('retURL'));
    }
}
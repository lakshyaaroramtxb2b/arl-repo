global class TestScheduledApexMethod implements Schedulable {

    //public static String CRON_EXP = '0 0 * * * ?';

    global static void scheduleIt() {
        TestScheduledApexMethod sm = new TestScheduledApexMethod();
        
        //System.schedule('Auto CAP 1', '0 0 * * * ?', sm);
        //System.schedule('Auto CAP 2', '0 10 * * * ?', sm);
        //System.schedule('Auto CAP 3', '0 20 * * * ?', sm);
        //System.schedule('Auto CAP 4', '0 30 * * * ?', sm);
        //System.schedule('Auto CAP 5', '0 40 * * * ?', sm);
        //System.schedule('Auto CAP 6', '0 50 * * * ?', sm);
        
        System.schedule('Auto CAP 6', '0 16 * * * ?', sm);
    }
 
    global void execute(SchedulableContext sc) {
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
		
        String[] toAddresses = new String[] {'amcneilly@salesforce.com'};
        mail.setToAddresses(toAddresses);
        mail.setSubject('Scheduled Job');
		mail.setPlainTextBody('Working');
        
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });

        CAPTriggerBridge.AsyncNotifyCustomer('a1630000001olE7');
    }  
}
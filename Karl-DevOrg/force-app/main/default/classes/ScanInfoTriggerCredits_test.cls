@isTest
private class ScanInfoTriggerCredits_test {
    static testMethod void positiveTest1(){
        POPCRAB_Version__c pv=new POPCRAB_Version__c();
        pv.Released__c=true;
        pv.Released_On__c=Datetime.newInstance(1960, 2, 19).date();
        insert pv;
        Scan_Info__c si=new Scan_Info__c();
        si.Name='test';
        si.Status__c='New';
        si.OrgID__c='00D30000000Jsbi';
        insert si;
        
        Account a=new Account(Name='00D58000000amwxEAA');
        insert a;
        Id tempid='00D30000000Jsbi';
        Contributing_Org__c co=new Contributing_Org__c();
        co.Account__c=a.id;
        co.Org_Id__c=tempid;
        co.Credits__c=3;
        insert co;
        
        si.Status__c='Done';
        update si;
        
        co=[select id,Credits__c from Contributing_Org__c where id=:co.id];
        system.assertEquals(co.Credits__c,2);
    }
    static testMethod void positiveTest2(){
        POPCRAB_Version__c pv=new POPCRAB_Version__c();
        pv.Released__c=true;
        pv.Released_On__c=Datetime.newInstance(1960, 2, 19).date();
        insert pv;
        Account a=new Account(Name='00D58000000amwxEAA');
        insert a;
        Id tempid='00D30000000Jsbi';
        Contributing_Org__c co=new Contributing_Org__c();
        co.Account__c=a.id;
        co.Org_Id__c=tempid;
        co.Credits__c=3;
        insert co;
        
        Scan_Info__c si=new Scan_Info__c();
        si.Name='test';
        si.Status__c='Done';
        si.OrgID__c='00D30000000Jsbi';
        insert si;
        
        
        co=[select id,Credits__c from Contributing_Org__c where id=:co.id];
        system.assertEquals(co.Credits__c,2);
    }
    static testMethod void NegTest1(){
        POPCRAB_Version__c pv=new POPCRAB_Version__c();
        pv.Released__c=true;
        pv.Released_On__c=Datetime.newInstance(1960, 2, 19).date();
        insert pv;
        Scan_Info__c si=new Scan_Info__c();
        si.Name='test';
        si.Status__c='New';
        si.OrgID__c='00D30000000Jsbi';
        insert si;
        
        Account a=new Account(Name='00D58000000amwxEAA');
        insert a;
        Id tempid='00D30000000Jsbi';
        Contributing_Org__c co=new Contributing_Org__c();
        co.Account__c=a.id;
        co.Org_Id__c=tempid;
        co.Credits__c=3;
        //insert co;
        
        si.Status__c='Done';
        update si;
        
        //co=[select id,Credits__c from Contributing_Org__c where id=:co.id];
        //system.assertEquals(co.Credits__c,2);
    }
    static testMethod void NegTest2(){
        POPCRAB_Version__c pv=new POPCRAB_Version__c();
        pv.Released__c=true;
        pv.Released_On__c=Datetime.newInstance(1960, 2, 19).date();
        insert pv;
        Scan_Info__c si=new Scan_Info__c();
        si.Name='test';
        si.Status__c='New';
        si.OrgID__c='00D30000000Jsbi';
        insert si;
        
        Account a=new Account(Name='00D58000000amwxEAA');
        insert a;
        Id tempid='00D30000000Jsbi';
        Contributing_Org__c co=new Contributing_Org__c();
        co.Account__c=a.id;
        co.Org_Id__c=tempid;
        co.Credits__c=0;
        insert co;
        
        si.Status__c='Done';
        update si;
        
        co=[select id,Credits__c from Contributing_Org__c where id=:co.id];
        system.assertEquals(co.Credits__c,0);
    }
    static testMethod void NegTest3(){
        POPCRAB_Version__c pv=new POPCRAB_Version__c();
        pv.Released__c=true;
        pv.Released_On__c=Datetime.newInstance(1960, 2, 19).date();
        insert pv;
        Scan_Info__c si=new Scan_Info__c();
        si.Name='test';
        si.Status__c='New';
        si.OrgID__c='00D58000000amwx';
        insert si;
        
        Account a=new Account(Name='00D58000000amwxEAA');
        insert a;
        Id tempid='00D30000000Jsbi';
        Contributing_Org__c co=new Contributing_Org__c();
        co.Account__c=a.id;
        co.Org_Id__c='00D58000000amwxEAA';
        co.Credits__c=1;
        insert co;
        
        si.Status__c='Done';
        update si;
        si.Name='test2';
        si.Status__c='Done';
        update si;
        
        co=[select id,Credits__c from Contributing_Org__c where id=:co.id];
        system.assertEquals(co.Credits__c,0);
    }
}
@isTest
public class SPT_CaseCloseComponentController_Test {
    @TestSetup
    static void makeData(){
        List<Case> caseList = SPT_TestUtility.createCaseRecords('New', 'USD', 'Web', 1, true);
    }

    @isTest
    public static void getCloseStatusValuesTest() {
        List<String> closeStatusList = SPT_CaseCloseComponentController.getCloseStatusValues(false);
        System.assertEquals(5, closeStatusList.size());
        
        List<String> allStatusList = SPT_CaseCloseComponentController.getCloseStatusValues(true);
        System.assertEquals(12, allStatusList.size());
    }

    @isTest
    public static void saveCaseStatusTest() {
        List<Id> caseIdList = new List<Id>();
        Case caseRecord = [SELECT Id, Status FROM Case];
        caseIdList.add(caseRecord.Id);
        String notUpdatedCase = SPT_CaseCloseComponentController.saveCaseStatus(caseIdList, 'In Progress');
        List<case> caseList = [SELECT Id, Status FROM Case];
        System.assertEquals('In Progress', caseList.get(0).Status);
    }
}
/**
* @author: Hardik
* @description: Batch Class to send all emails
*/

global class SC_Email_External_Communication_Batch implements Database.Batchable<sObject>, Database.Stateful {
    // Maps Declaration - Initialised in Constructor - Used in execute method
    global Map<String, SC_Email_External_Communication.StatusRecords> mapOfStatus;
    global Map<String, EmailTemplate> mapOfEmailTemplate; 
    global Map<String, OrgWideEmailAddress> mapOfOrgWideDefEmails;
    global Map<Id, List<String>> mapOfstatusAE;

    // Constructor to create Maps (called from SC_Email_External_Communication class @Method-> sendEmails)
    public SC_Email_External_Communication_Batch (String statusRec, 
            List<EmailTemplate> templates,  
            List<OrgWideEmailAddress> orgWideDefEmails,
            List<SC_Email_External_Communication.AccountEmails> aeEmailRecords) {
                                                      
        mapOfStatus = new Map<String, SC_Email_External_Communication.StatusRecords>();
        mapOfEmailTemplate = new Map<String,EmailTemplate>();
        mapOfOrgWideDefEmails = new Map<String, OrgWideEmailAddress>();
        mapOfstatusAE = new Map<Id, List<String>>();
        
        List<SC_Email_External_Communication.StatusRecords> statusRecordList = (List<SC_Email_External_Communication.StatusRecords>) 
            JSON.deserialize(statusRec, List<SC_Email_External_Communication.StatusRecords>.class);
        
        for(SC_Email_External_Communication.StatusRecords rec : statusRecordList) {
            mapOfStatus.put(rec.status, rec);
        }
        
        for(SC_Email_External_Communication.AccountEmails aeEml : aeEmailRecords) {
            mapOfstatusAE.put(aeEml.id, aeEml.aeEmail.split(','));
        }  
        
        for(EmailTemplate et : templates) {
            mapOfEmailTemplate.put(et.Name, et); 
        }
        
        for(OrgWideEmailAddress owd : orgWideDefEmails) {
            mapOfOrgWideDefEmails.put(owd.Address, owd); 
        }
    }
    
    
    // Query to be passed to start method
    public static final String BASE_SELECT =
        'SELECT Id, Name, CompanyOrAccount__c, Email__c, FirstName__c, LastName__c, Status__c, Account_Emails__c , SuccessOrError__c ' +
        ' FROM SC_CISO_CampaignMember__c WHERE SuccessOrError__c = \'Pending\'';
    
    
    // start method for batch class
    public Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(BASE_SELECT);
    }
    
    // execute method for batch class
    public void execute(Database.BatchableContext BC, List<SC_CISO_CampaignMember__c> listOfCM) {
        List<SC_CISO_CampaignMember__c> listOfCampaignMember = new List<SC_CISO_CampaignMember__c>();
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        Contact cnt = [select id, Email from Contact where email != null limit 1];
        
        for(SC_CISO_CampaignMember__c cpm : listOfCM) {
            // Adding CC addresses in the mail
            String ccAddress;
            if(mapOfStatus.get(cpm.Status__c) != null)
                ccAddress = mapOfStatus.get(cpm.Status__c).selectedCCAddresses;
            
            List<String> ccAddList = new List<String>();
            if(!String.isEmpty(ccAddress)){
                ccAddList = ccAddress.split(',');
            }
            
            // Adding AE Email in CC Address
            if(!String.isEmpty(cpm.Account_Emails__c)) {
                ccAddList.addAll(cpm.Account_Emails__c.split(','));   
            }

            // SingleEmailMessage
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setTemplateID(mapOfEmailTemplate.get(mapOfStatus.get(cpm.Status__c).selectedEmailTemplate).Id); 
            mail.setwhatId(cpm.Id);
            mail.setTargetObjectId(cnt.Id);
            mail.toAddresses = new String[] {cpm.Email__c };
            mail.setCcAddresses(ccAddList);
            mail.setSaveAsActivity(false);
            mail.setOrgWideEmailAddressId(mapOfOrgWideDefEmails.get(mapOfStatus.get(cpm.Status__c).selectedFromAddress).Id);            
            mails.add(mail);
        }
                
        //merging email fields to get full email and rollback without sending email
        List<Messaging.SendEmailResult> results = new List<Messaging.SendEmailResult>();
        Savepoint sp = Database.setSavepoint();
        try{
            results = Messaging.sendEmail(mails);
        }
        catch (Exception ex) {
            List<SC_CISO_CampaignMember__c> listOfCMUpdate = new List<SC_CISO_CampaignMember__c>();
            
            for(SC_CISO_CampaignMember__c cmp : listOfCM) {
                String errMsg = ex.getMessage();
                cmp.SuccessOrError__c = errMsg.split('error:')[1];
                listOfCMUpdate.add(cmp);
            }
            
            if(!listOfCMUpdate.isEmpty()){
                update listOfCMUpdate;
                return;
            }
        }
        Database.rollback(sp);
        
        List<Messaging.SingleEmailMessage> msgListToBeSend = new List<Messaging.SingleEmailMessage>(); // list to send final emails
        
        for (Messaging.SingleEmailMessage email : mails) {
            Messaging.SingleEmailMessage emailToSend = new Messaging.SingleEmailMessage();
            
            emailToSend.setPlainTextBody(email.getPlainTextBody());
            emailToSend.setHTMLBody(email.getHTMLBody());
            emailToSend.setSubject(email.getSubject());
            emailToSend.setCcAddresses(email.getCcAddresses());
            emailToSend.setToAddresses(email.getToAddresses());
            emailToSend.setOrgWideEmailAddressId(email.getOrgWideEmailAddressId());
            
            msgListToBeSend.add(emailToSend);
            //System.debug('emailToSend: ' + emailToSend);
        }
        
        
        // Send email w/o targetObjectId
        Messaging.SendEmailResult[] finalResults;
        try {    
            finalResults = Messaging.sendEmail(msgListToBeSend);
        } 
        catch (Exception ex) {
            List<SC_CISO_CampaignMember__c> listOfCMUpdate = new List<SC_CISO_CampaignMember__c>();
            
            for(SC_CISO_CampaignMember__c cmp : listOfCM) {
                String errMsg = ex.getMessage();
                cmp.SuccessOrError__c = errMsg.split('error:')[1];
                listOfCMUpdate.add(cmp);
            }
            
            if(!listOfCMUpdate.isEmpty()){
                update listOfCMUpdate;
            }
        }
        
        // Updating SuccessOrError field on SC_CISO_CampaignMember__c object with Success/Fail value
        Integer index = 0;
        if(results != null) {
            for(Messaging.SendEmailResult res : results) {
                listOfCM[index].SuccessOrError__c = (res.isSuccess()) ? 'Success' : 'Fail';
                ++index;
            }
        }
        
        upsert listOfCM;
    }
    
    
    // finish method for batch class
    public void finish(Database.BatchableContext BC) {
        //AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,TotalJobItems, CreatedBy.Email FROM AsyncApexJob WHERE Id = :BC.getJobId()];   
    }
}
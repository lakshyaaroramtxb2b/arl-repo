public class EsaCaseCommentTriggerHandler{
   
   /**
    * Update Case Status to InProgress When Case Comment added by Case Owner.
    * @param caseComments [description]
    */
   public static void updateCaseStatus(List<CaseComment> caseComments){
        
        Set<Id> caseIdSet = new Set<Id>();
        List<Case> lstCases = new List<Case>();

        List<User> lstGroupUsers = [SELECT Id, Name, Email
                                    FROM User
                                    WHERE Isactive = true
                                    AND ID IN (SELECT UserorGroupId 
                                               FROM GroupMember 
                                               WHERE Group.DeveloperName = :Label.Esa_EntSec_Public_Group
                                               AND Group.Type='Regular')];

        Set<Id> setGroupUserIds = new Set<Id>();
        List<String> lstGroupUserNames = new List<String>();
        for(User usr : lstGroupUsers){
            setGroupUserIds.add(usr.Id);
            lstGroupUserNames.add('Original_Author: '+usr.Name);
        }
        
        Boolean authorFlag = false;
        for(CaseComment cComment : caseComments) {

            if(setGroupUserIds.contains(cComment.createdById)) continue;

            authorFlag = false;

            for(String authorName : lstGroupUserNames){
                if(cComment.commentBody.contains(authorName)) authorFlag = true;    
            }
            
            if(authorFlag) continue;

            if(!cComment.CommentBody.contains(Label.ESA_Intake_Bot)) {
                caseIdSet.add(cComment.ParentId);   
            }
        }
       
        if(caseIdSet.size() == 0) return;
        //updateCaseStatusToInProgress(caseIdSet);
        
        List<String> lstCaseStaus= String.ValueOf(Label.Esa_Case_Status).split(',');
        
        for(Case caseRecord : [SELECT Id, Status 
                               FROM Case
                               WHERE Id IN :caseIdSet
                               AND RecordTypeId =: SPT_Constants.SEC_RECORDTYPE_ID
                               AND OwnerID =: SPT_Constants.TRUST_APPLICATION_SECURITY_ASSURANCE
                               AND Status IN :lstCaseStaus]) {
                caseRecord.Status = 'In Progress';
                lstCases.add(caseRecord);
        }

        if(lstCases.size() > 0){
            update lstCases;
        } 

   }
   
   @future(callout=true)
   Public static void updateCaseStatusToInProgress(Set<Id> caseIdSet){
      List<String> lstCaseStaus= String.ValueOf(Label.Esa_Case_Status).split(',');
        List<Case> lstCases = new List<Case>();
        for(Case caseRecord : [SELECT Id, Status 
                               FROM Case
                               WHERE Id IN :caseIdSet
                               AND RecordTypeId =: SPT_Constants.SEC_RECORDTYPE_ID
                               AND OwnerID =: SPT_Constants.TRUST_APPLICATION_SECURITY_ASSURANCE
                               AND Status IN :lstCaseStaus]) {
                caseRecord.Status = 'In Progress';
                lstCases.add(caseRecord);
        }

        if(lstCases.size() > 0){
            update lstCases;
        }

   }
   
   /**
    * Send email notification to case contact when a comment is added.
    * @param caseComments 
    * 
    * TODO: eliminate hard-coded constants
    */
    
    Public static void emailCaseContact(List<CaseComment> caseComments) {
        Set<Id> entSecTypes = new Set<Id>{Id.valueOf(SPT_Constants.SEC_RECORDTYPE_ID)};
		Set<Id> aPIusers = new Set<Id>{'0053A00000Cr6X8QAJ'};           
        Set<Id> caseIds = new Set<Id>();
        List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
        
        for(CaseComment cc :caseComments) {
            caseIds.add(cc.ParentId);
        }
        Map<Id, Case> cases = new Map<Id, Case>([SELECT Id, RecordTypeId, ContactId, CreatedById 
                                                FROM Case WHERE Id IN : caseIds]);   
        
        for(CaseComment cc :caseComments) {
            Case c = cases.get(cc.ParentId);
            
            if (cc.IsPublished && entSecTypes.contains(c.RecordTypeId) && !aPIusers.contains(cc.CreatedById)) {
            	emails.add(createCCEmail(cc, c));		    
            }    
        }
        
        if (!emails.isEmpty()) {
            Messaging.sendEmail(emails);
        }
    }
    
    Private static Messaging.SingleEmailMessage createCCEmail(CaseComment cc, Case c){
    	Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        email.setTemplateId('00X4V000002WfL6');
        email.setOrgWideEmailAddressId('0D23A000000CbPV');
        email.setTargetObjectId(c.ContactId);
        email.setWhatId(c.Id);
        return email;
    }

}
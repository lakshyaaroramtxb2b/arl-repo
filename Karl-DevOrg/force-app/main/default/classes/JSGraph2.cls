public class JSGraph2
{
    //TODO need to convert to dictionary
    public List<JSEventNode> nodes;
    public Set<JSEventEdge> edges;
    
    public JSGraph2()
    {
    	nodes = new List<JSEventNode>();
		edges = new Set<JSEventEdge>();
    }
    
    public Integer getNodeCount()
    {
        return nodes.size();
    }
    
    public Integer getEdgeCount()
    {
        return edges.size();
    }
    
    public void addNode(JSEventNode node)
    {
        node.id = 'n' + this.getNodeCount();
        nodes.add(node);
    }
    
    //key is same as label. we keep label as its used by graph lib
    public JSEventNode getNodeByKey(String key)
    {
        for(JSEventNode n : nodes)
        {
            if(n.key.equalsIgnoreCase(key))
                return n;
        }
        
        return null;
    }
    
    public JSEventEdge getEdge(JSEventNode n0, JSEventNode n1)
    {
        for(JSEventEdge e : edges)
        {
            String edgeTargetId = e.target.toLowerCase();
            String edgeSourceId= e.source.toLowerCase();

            //not a directed check
            if( (n0.id.equalsIgnoreCase(edgeTargetId) && n1.id.equalsIgnoreCase(edgeSourceId)) ||
                (n1.id.equalsIgnoreCase(edgeTargetId) && n0.id.equalsIgnoreCase(edgeSourceId)))
            {
                return e;
            }
        }
        
        return null;
    }
    
    public JSEventEdge addEdge(JSEventNode node0, JSEventNode node1)
    {
		JSEventEdge edge = new JSEventEdge();
        
        edge.id = 'e' + this.getEdgeCount();
    	edge.source = node0.id;
		edge.target = node1.id;
        
        edge.addInstance();
        
        edges.add(edge);
        
        return edge;
    }
}
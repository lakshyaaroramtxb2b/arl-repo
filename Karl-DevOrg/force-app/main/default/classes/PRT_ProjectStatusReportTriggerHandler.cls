/*
* Created by - Prashant Gupta
* Date - 19-09-2019
* Handler for PRT_ProjectStatusReportTrigger
*/
public class PRT_ProjectStatusReportTriggerHandler {
    
    /*
    * Created by - Prashant Gupta
    * before insert mothod being called from Trigger
    */
	public static void beforeInsert(List<Project_Status_Report__c> newList){ 
        PRT_ProjectStatusReportTriggerHelper.updateReportName(newList,null);
        PRT_ProjectStatusReportTriggerHelper.populatePortfolioManager(newList,null);
    }
    /*
    * Created by - Prashant Gupta
    * after insert mothod being called from Trigger
    */
	public static void afterInsert(List<Project_Status_Report__c> newList){ 
        PRT_ProjectStatusReportTriggerHelper.updatePRTFieldsOnProject(newList,null);
 		PRT_ProjectStatusReportTriggerHelper.updatePathToGreen(newList, null);
    }
    /*
    * Created by - Prashant Gupta
    * before update mothod being called from Trigger
    */
    public static void beforeUpdate(List<Project_Status_Report__c> newList, Map<id,Project_Status_Report__c> oldMap){ 
        PRT_ProjectStatusReportTriggerHelper.checkRequiredFields(newList, oldMap);
        PRT_ProjectStatusReportTriggerHelper.updateReportName(newList,oldMap);
        PRT_ProjectStatusReportTriggerHelper.populatePortfolioManager(newList,oldMap);
      
    }
    
    /*
    * Created by - Prashant Gupta
    * after update mothod being called from Trigger
    */
    public static void afterUpdate(List<Project_Status_Report__c> newList, Map<id,Project_Status_Report__c> oldMap){ 
        PRT_ProjectStatusReportTriggerHelper.updatePRTFieldsOnProject(newList,oldMap);
        PRT_ProjectStatusReportTriggerHelper.updatePathToGreen(newList, null);
    }
}
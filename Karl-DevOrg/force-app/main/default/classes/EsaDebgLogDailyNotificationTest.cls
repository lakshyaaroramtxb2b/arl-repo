@isTest
public class EsaDebgLogDailyNotificationTest {
	@IsTest
    static void testBatch() {
    	List<ESA_Debug_Log__c> lstDebugLogs = new List<ESA_Debug_Log__c>();
    	ESA_Debug_Log__c esaDebug = new ESA_Debug_Log__c();
    	esaDebug.Exception_Details__c='Exception1';
    	esaDebug.Message__c='NullPointer';
    	esaDebug.Source_File__c = 'EsaDebgLogDailyNotificationTest';
    	esaDebug.StackTrace__c = 'Class.sfutil.ESASupportForceUtil.updateCase: line 256, column 1';
    	esaDebug.TypeName__c = 'sfutil.ESASupportForceUtil.LoginException';
    	esaDebug.Level__c = 'Error';
    	lstDebugLogs.add(esaDebug);
    	esaDebug = new ESA_Debug_Log__c();
    	esaDebug.Exception_Details__c='Exception2';
    	esaDebug.Message__c='NullPointer';
    	esaDebug.Source_File__c = 'EsaDebgLogDailyNotificationTest';
    	esaDebug.StackTrace__c = 'Class.sfutil.ESASupportForceUtil.updateCase: line 256, column 1';
    	esaDebug.TypeName__c = 'sfutil.ESASupportForceUtil.LoginException';
    	esaDebug.Level__c = 'Error';
    	lstDebugLogs.add(esaDebug);

    	insert lstDebugLogs;

    	Datetime yesterday = Datetime.now().addDays(-1);
		Test.setCreatedDate(lstDebugLogs[0].Id, yesterday);
		Test.setCreatedDate(lstDebugLogs[1].Id, yesterday);

    	EmployeeSyncNotificationEmailIds__c emailsInfo = new EmployeeSyncNotificationEmailIds__c();
    	emailsInfo.Name='suppala';
    	emailsInfo.EmailId__c = 'suppala@salesforce.com';
    	insert emailsInfo;

        
        Test.startTest();
        EsaDebgLogDailyNotification batch = new EsaDebgLogDailyNotification();
        Database.executeBatch(batch);
        String JobId = EsaDebgLogDailyNotification.scheduleJob();
        Test.stopTest();
    }

    @IsTest
    static void testBatchWithOutErrors() {
    	EmployeeSyncNotificationEmailIds__c emailsInfo = new EmployeeSyncNotificationEmailIds__c();
    	emailsInfo.Name='suppala';
    	emailsInfo.EmailId__c = 'suppala@salesforce.com';
    	insert emailsInfo;

    	Test.startTest();
        EsaDebgLogDailyNotification batch = new EsaDebgLogDailyNotification();
        Database.executeBatch(batch);
        String JobId = EsaDebgLogDailyNotification.scheduleJob();
        Test.stopTest();
    } 
}
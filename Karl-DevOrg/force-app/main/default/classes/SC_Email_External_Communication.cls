/**
* @author: Hardik Ranjan
* @description: Controller Class for all SC_Email_ExternalCommunication Component
*/

public with sharing class SC_Email_External_Communication {
    // Static variables
    private static Integer Limit_Records = 200;
    private static Integer Batch_Size = 200;
    private static Integer Query_Limit = 50000;
    private static String Object_Api_Name = 'SC_CISO_CampaignMember__c';
    private static Integer Delete_Batch_Size = 2000;
    
    /* 
    * Method Name: getCampaignMemberData
    * @param: no params
    * @return type : List<SC_CISO_CampaignMember__c>
    * @description : get the list of all the data loaded in SC_CISO_CampaignMember__c Object 
    */
    @AuraEnabled
    public static List<SC_CISO_CampaignMember__c> getAllCampaignMemberData(){
        
        String query = 'SELECT Id, Name, CompanyOrAccount__c, Email__c, SuccessOrError__c, LastModifiedById, OwnerId, ' + 
            ' FirstName__c, LastName__c, Status__c, Account_Emails__c, CreatedById, CurrencyIsoCode ' +
            ' FROM SC_CISO_CampaignMember__c ';
        
        query += ' ORDER BY Status__c ';
        query += ' LIMIT ' + Query_Limit;
        
        return Database.query(query); 
    }
    
    /* 
    * @Author : Vishnu Kumar
    * @Method Name: getCampaignMemberDataTemplate
    * @param: no params
    * @return type : List<SC_CISO_CampaignMember__c>
    * @description : This method returns the all the fields of SC_CISO_CampaignMember__c object which are editable.
    */
    @AuraEnabled
    public static String getCampaignMemberDataTemplate(){
        String fields = '';
        
        //Getting all fields of the SC_CISO_CampaignMember__c object.
        Map<String, Schema.SObjectField> fList = Schema.SObjectType.SC_CISO_CampaignMember__c.fields.getMap();
        for( String fieldName : fList.keySet() ){
            //Get only editable fields
            //Dont include: successorerror__c
            if( fList.get(fieldName).getDescribe().isUpdateable() 
               && !fieldName.equalsIgnoreCase('SuccessOrError__c')
               && !fieldName.equalsIgnoreCase('OwnerID')
               && !fieldName.equalsIgnoreCase('CurrencyIsoCode')) {
                   fields += fList.get(fieldName).getDescribe().label + ',';
            }
        }
        fields = fields.removeEnd(',');
        return fields; 
    }
    
    /* 
    * Method Name: fetchEmailTemplates
    * @param: no params
    * @return type : List<EmailTemplate>
    * @description : get the list of all the Email Templates in folder from metadata
    */
    @AuraEnabled
    public static List<EmailTemplate> fetchEmailTemplates(){
        Set<String> setOfFolderName = new Set<String>();
        
        for(SC_EscalationTemplatesSearchedFolders__mdt folderName : [
            SELECT DeveloperName, MasterLabel 
            FROM SC_EscalationTemplatesSearchedFolders__mdt]){
                setOfFolderName.add(folderName.MasterLabel);
        }
        
        String query = 'SELECT Name, DeveloperName, FolderName FROM EmailTemplate WHERE IsActive=true ';
        if(!setOfFolderName.isEmpty()){
            query+= ' AND Folder.Name IN : setOfFolderName ';
        }
        query += ' ORDER BY Name';
        return Database.query(query);
    }
    
    
    /* 
    * Method Name: getOrgWideEmailAddresses
    * @param: no params
    * @return type : List<String>
    * @description : get the list of all the Org Wide Email Addresses
    */
    @AuraEnabled 
    public static List<OrgWideEmailAddress> getOrgWideEmailAddresses() {
        List<OrgWideEmailAddress> listOfOrgEmails = [
            SELECT Id, IsAllowAllProfiles, DisplayName, Address 
            FROM OrgWideEmailAddress 
            LIMIT :Query_Limit];
        return listOfOrgEmails;    
    }
    
    /* 
    * Method Name: deleteData
    * @param: empty 
    * @return type : EmailConfirmation {Wrapper}
    * @description : Delete all the records from SC_CISO_CampaignMember__c Object
    */
    @AuraEnabled 
    public static EmailConfirmation deleteData() {
        SC_Email_Ext_Communication_Delete_Batch deleteBatch = new SC_Email_Ext_Communication_Delete_Batch();
        
        Database.executeBatch(deleteBatch, Delete_Batch_Size);  
        
        return fetchData();
    }
    
    /* 
    * @Author : Vishnu kumar
    * @Method Name: getAllCampaignMemberNotMarkedAsPending
    * @param: no params
    * @return type : Boolean
    * @description : This method updates all the Campaign Member Status to Pending and returns true if there are no more
    * record to be marked as Pending.
    */
    @AuraEnabled
    public static boolean updateCampaignMemberStatusAsPending(){
        //Query all the Campaign Members where status is not pending and status is blank
        String query = 'SELECT Id FROM SC_CISO_CampaignMember__c WHERE SuccessOrError__c != \'Success\' AND SuccessOrError__c != \'Pending\' LIMIT 10000';
        List<SC_CISO_CampaignMember__c> campMembrs = Database.query(query); 
        
        //Update the record status to Pending
        for( SC_CISO_CampaignMember__c cm : campMembrs ){
            cm.SuccessOrError__c = 'Pending';
        }
        update campMembrs;
        
        //Verifying if there are more records to update or not
        //Must user limit 1 because we dont need total count here, we just need if there are any records or not
        campMembrs = [Select id FROM SC_CISO_CampaignMember__c WHERE SuccessOrError__c != 'Success' AND SuccessOrError__c != 'Pending' LIMIT 1];
        
        return campMembrs.isEmpty();
    }
    
    /* 
    * Method Name: sendEmails
    * @param: StatusRecords Wrapper class from frontend 
    * @return type : void
    * @description : Run the batch class to send all emails
    */
    @AuraEnabled 
    public static EmailConfirmation sendEmails(String statusRecords, List<AccountEmails> aeEmailRecords) {
        SC_Email_External_Communication_Batch emailBatch = 
            new SC_Email_External_Communication_Batch(statusRecords, fetchEmailTemplates(), getOrgWideEmailAddresses(), aeEmailRecords);
        
        Database.executeBatch(emailBatch, Batch_Size);  
        
        return fetchData();
    }
    
    /* 
    * Method Name: fetchData
    * @param: no params
    * @return type : EmailConfirmation {Wrapper}
    * @description : get all the data to display on frontend in a wrapper
    */
    @AuraEnabled 
    public static EmailConfirmation fetchData() {
        EmailConfirmation emailConf = new EmailConfirmation();        
        emailConf.listOfEmailTemplates = fetchEmailTemplates();
        emailConf.listOfOrgWideDefault = getOrgWideEmailAddresses(); 
        emailConf.listOfCampaignMembers = getCampaignMemberData(false);
        emailConf.countData = getRecordsCounts();
        emailConf.noOfRecordsDisplayed = (emailConf.listOfCampaignMembers.size() > Limit_Records ? Limit_Records : emailConf.listOfCampaignMembers.size());
        
        List<StatusRecords> statusRepList = new List<StatusRecords>();
        Map<String, Integer> mapOfStatus = new Map<String, Integer>();
        emailConf.staticRecordsList = getRecordsByStatus();
        for(AsyncApexJob job: [SELECT ApexClass.Name, CompletedDate, ExtendedStatus, JobItemsProcessed, 
                             JobType, MethodName, NumberOfErrors, Status, TotalJobItems 
                             FROM AsyncApexJob 
                             WHERE JobType = 'BatchApex' 
                             AND ApexClass.Name = 'SC_Email_Ext_Communication_Delete_Batch'
                             AND (Status = 'Queued' OR Status = 'Preparing' OR Status = 'Processing' OR Status = 'Holding')
                             AND CreatedById = :UserInfo.getUserId()]){
            emailConf.deletionInProgress = true;
            break;
        }
        return emailConf;  
    }
    
    /* 
    * Method Name: fetchErrorRecords
    * @param: no params
    * @return type : EmailConfirmation {Wrapper}
    * @description : get all the data to display on frontend in a wrapper
    */
    @AuraEnabled 
    public static EmailConfirmation fetchErrorRecords() {
        EmailConfirmation emailConf = new EmailConfirmation();        
        emailConf.listOfCampaignMembers = getCampaignMemberData(true);
        emailConf.noOfRecordsDisplayed = (emailConf.listOfCampaignMembers.size() > Limit_Records ? Limit_Records : emailConf.listOfCampaignMembers.size());
        return emailConf;  
    }
    
    /* 
    * Method Name: getCampaignMemberData
    * @param: no params
    * @return type : List<SC_CISO_CampaignMember__c>
    * @description : get the list of all the data loaded in SC_CISO_CampaignMember__c Object with Filters
    */
    @AuraEnabled
    public static List<SC_CISO_CampaignMember__c> getCampaignMemberData(Boolean showErrorRec){
        List<String> listOfNoErrorStatus = new List<String> {'Success', 'Pending', ''};
        String errorStatus = 'Error';
        
        String query = 'SELECT Id, Name, CompanyOrAccount__c, Email__c,SuccessOrError__c, ' + 
            ' FirstName__c, LastName__c, Status__c, Account_Emails__c ' +
            ' FROM SC_CISO_CampaignMember__c ';
        
        if(showErrorRec){
            query += ' WHERE SuccessOrError__c NOT IN :listOfNoErrorStatus';
        }
        query += ' ORDER BY Status__c ';
        query += 'LIMIT ' + Limit_Records;
                
        return Database.query(query); 
    }
    
    /* 
    * Method Name: getRecordsCounts
    * @param: no params
    * @return type : CountData Wrapper
    * @description : get the list of records group by SuccessOrError__c
    */
    @AuraEnabled
    public static CountData getRecordsCounts(){
        CountData countDataWrapper = new CountData();
        
        // Get status count
        for(AggregateResult ar : [SELECT Count(Id) total, SuccessOrError__c status FROM SC_CISO_CampaignMember__c Group By SuccessOrError__c]){
            String status = (String)ar.get('status');
            
            if(status == 'Pending' ){
                countDataWrapper.emailPending = (Integer)ar.get('total');
            }
            else if(status == 'Success' ){
                countDataWrapper.emailSuccess = (Integer)ar.get('total');
            }
            else if(String.isBlank(status) ){
                countDataWrapper.emailNoSent = (Integer)ar.get('total');
            }
            else if(String.isNotBlank(status) && status != 'Success' && status != 'Pending'){
                countDataWrapper.emailFailed = (Integer)ar.get('total');
            }
        }
        
        countDataWrapper.totalRecords = countDataWrapper.emailPending + countDataWrapper.emailSuccess + countDataWrapper.emailNoSent + countDataWrapper.emailFailed;
        
        return countDataWrapper;
    }
    
    /* 
    * Method Name: getRecordsByStatus
    * @param: no params
    * @return type : List<StatusRecords>
    * @description : get the list of records group by Status__c
    */
    @AuraEnabled    
    public static List<StatusRecords> getRecordsByStatus(){
        List<StatusRecords> statusRepList = new List<StatusRecords>();
        for(AggregateResult ar : [SELECT Count(Id) total, Status__c status FROM SC_CISO_CampaignMember__c Where SuccessOrError__c != 'Success' Group By Status__c]){
            StatusRecords statusRep = new StatusRecords();
            statusRep.status = (String)ar.get('status');
            statusRep.numberOfRec = (Integer)ar.get('total');
            statusRepList.add(statusRep);  
        }
        return statusRepList;
    }
    
    /* 
    * Method Name: getAllListViewDetails
    * @param: no params
    * @return type : List<ListView>
    * @description : get the record of list view with Name = 'All'
    */
    @AuraEnabled
    public static List<ListView> getAllListViewDetails() {
        List<ListView> listviews = [SELECT Id, Name FROM ListView WHERE SobjectType = :Object_Api_Name AND Name = 'All'];
        return listviews;
    }
    
    /*
    * Wrapper Class for displaying all data on Frontend
    * 
    * @Author Hardik
    */    
    public class EmailConfirmation {
        @AuraEnabled public List<SC_CISO_CampaignMember__c> listOfCampaignMembers;
        @AuraEnabled public List<EmailTemplate> listOfEmailTemplates;
        @AuraEnabled public List<StatusRecords> staticRecordsList;
        @AuraEnabled public List<OrgWideEmailAddress> listOfOrgWideDefault;
        @AuraEnabled public CountData countData;
        @AuraEnabled public Integer noOfRecordsDisplayed;
        @AuraEnabled public Boolean deletionInProgress =false;

        public EmailConfirmation() {
            listOfCampaignMembers = new List<SC_CISO_CampaignMember__c>();
            listOfEmailTemplates = new List<EmailTemplate>();
            staticRecordsList = new List<StatusRecords>();
            listOfOrgWideDefault = new List<OrgWideEmailAddress>();
            countData = new countData();
            noOfRecordsDisplayed = 0;
        }
    }
    
    /*
    * Child Wrapper Class to get all selected data on the fronend
    * @Author Hardik
    */ 
    public class StatusRecords {
        @AuraEnabled public String status;
        @AuraEnabled public Integer numberOfRec;
        @AuraEnabled public String selectedEmailTemplate;
        @AuraEnabled public String selectedFromAddress;
        @AuraEnabled public String selectedCCAddresses;
        
        public StatusRecords() {
            status = '';
            numberOfRec = 0;       
            selectedCCAddresses = '';
            selectedFromAddress = '';
            selectedEmailTemplate = '';
        }
    }
    
    /*
    * Child Wrapper Class for count values
    * @Author Banshi
    */ 
    public class CountData {
        @AuraEnabled public Integer totalRecords;
        @AuraEnabled public Integer emailNoSent;
        @AuraEnabled public Integer emailPending;
        @AuraEnabled public Integer emailSuccess;
        @AuraEnabled public Integer emailFailed;
        
        public CountData() {
            this.totalRecords = 0;
            this.emailNoSent = 0;
            this.emailPending = 0;
            this.emailSuccess = 0;
            this.emailFailed = 0;
        }
    }
    
    /*
    * Wrapper Class to send Account Emails to backend
    * @Author Hardik
    */ 
    public class AccountEmails {
        @AuraEnabled public String aeEmail {set;get;}
        @AuraEnabled public String id {set;get;}
        
        public AccountEmails() {
            id = '';
            aeEmail = '';
        }
    }
}
public class ESA_CaseReminderHandlerMDT {
    
    public static void updateReminderValues(List<Case> lstCases){
        
        //get the unique id of cases
        Set <id> caseIds = new Set <id>();
   
        for (case cs: lstCases){
            caseIds.add(cs.id);
            
        }
        //get the reminders metadata
		Case_Reminder_Config__mdt[] caseReminderMDT = [SELECT Reminder_1_Email_Template__c, Reminder_2_Email_Template__c,Reminder_3_Email_Template__c,
                                                    Reminder_4_Email_Template__c, Closing_Reminder__c, Reminder_1_Days__c, Reminder_2_Days__c,
                                                    Reminder_3_Days__c,Reminder_4_Days__c,Closing_Days__c,Team_Record_Types__c, Status__c 
                                                     FROM Case_Reminder_Config__mdt];
        //set today value
        date todayDate = system.today();
        datetime todayDateTime = datetime.now();
        
        //loop through cases
        for (Case cs: lstCases){
            
            if (caseIds.contains(cs.Id)){
                //loop trough the reminders to get the actual one for our case
                for (Case_Reminder_Config__mdt cr: caseReminderMDT){
                    //check the recordtype 
                    if (cr.Team_Record_Types__c.contains(cs.RecordTypeId)){
                        //check the status on the metadata
                        if (cs.Status == cr.Status__c){
                            // conditions for our 4 reminders and close reminder
                            if (cr.Reminder_1_Days__c != null && cs.Reminder1__c == null){
                                
                                cs.Reminder1__c = todayDate + integer.valueof(cr.Reminder_1_Days__c);
                                cs.Reminder1_Email_Template__c = cr.Reminder_1_Email_Template__c;
                                //cs.Reminder_1_datetime__c = todayDateTime;
                                //condition if this is the last reminder for this record type
                                if (cr.Reminder_2_Days__c == null && cr.Closing_Days__c != null){
                                    cs.Prepare_close__c = true;
                                }
                         
                            }
                            
                            else if (cr.Reminder_2_Days__c != null && cs.Reminder1__c != null && cs.Reminder2__c ==null && 
                                     cs.Reminder1__c <= cs.Last_Reminder__c && cs.Send_Close__c == false){
                                         
                                         cs.Reminder2__c = todayDate + integer.valueof(cr.Reminder_2_Days__c);
                                         cs.Reminder2_Email_Template__c = cr.Reminder_2_Email_Template__c;
                                		 //cs.Reminder_2_datetime__c = todayDateTime;
                                         if (cr.Reminder_3_Days__c == null && cr.Closing_Days__c != null){
                                    		cs.Prepare_close__c = true;
                                }
                                     }
                            else if (cr.Reminder_3_Days__c != null && cs.Reminder1__c != null && cs.Reminder2__c != null && 
                                     cs.Reminder2__c <= cs.Last_Reminder__c && cs.Reminder3__c == null && cs.Send_Close__c == false){
                                         
                                         cs.Reminder3__c = todayDate + integer.valueof(cr.Reminder_3_Days__c);
                                         cs.Reminder3_Email_Template__c = cr.Reminder_3_Email_Template__c;
                                		 //cs.Reminder_3_datetime__c = todayDateTime;
                                         if (cr.Reminder_4_Days__c == null && cr.Closing_Days__c != null){
                                        	 cs.Prepare_close__c = true;
                                }
                                         
                                         
                                     }
                             else if (cr.Reminder_4_Days__c != null && cs.Reminder1__c != null && cs.Reminder2__c != null &&
                                     cs.Reminder3__c != null && cs.Reminder3__c <= cs.Last_Reminder__c && cs.Send_Close__c == false){
                                         
                                         cs.Reminder4__c = todayDate + integer.valueof(cr.Reminder_4_Days__c);
                                         cs.Reminder4_Email_Template__c = cr.Reminder_4_Email_Template__c;
                                		 //cs.Reminder_4_datetime__c = todayDateTime;
                             			 if (cr.Closing_Days__c != null){
                                        	 cs.Prepare_close__c = true;
                                }
                                     }
                            
                            else if (cs.Prepare_close__c == true && cr.Closing_Days__c != null){
                                
                                cs.Closing_Date__c = todayDate + integer.valueof(cr.Closing_Days__c);
                                cs.Closing_template__c = cr.Closing_Reminder__c;
                                //cs.Closing_datetime__c = todayDateTime;
                                cs.Send_Close__c = true;
                                
                            }
                            
                        }
                        //change status means removing values
                        else {
                            
                            cs.Reminder1__c = null;
                            cs.Reminder2__c = null;
                            cs.Reminder3__c = null;
                            cs.Reminder4__c = null;
                            cs.Closing_Date__c = null;
                            
                        }
                        
                        
                    }
                    
                    
                }
            
                
                
                
            }
            
            
            
        }
        
        
        
    }

}
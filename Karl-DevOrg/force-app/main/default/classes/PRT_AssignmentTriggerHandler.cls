/***********************************************************
 * Handler class for PRT_AssignmentTrigger
 * Created by 	- Harinath Ch
 * Date 		- 06th August 2019
 ************************************************************/
public class PRT_AssignmentTriggerHandler {
    /***********************************************************
     * before insert Method to be called from trigger
     * Created by 	- Harinath Ch
     ************************************************************/
	public static void beforeInsert(List<Assignment__c> assignmentList){
        //PRT_AssignmentTriggerHelper.checkAllocationBeforeInsert(assignmentList);
        
        PRT_AssignmentTriggerHelper.createUpdateAssignmentForMilestone(assignmentList,null);
        PRT_AssignmentTriggerHelper.populateAssignmentName(assignmentList,null);
    }
    public static void afterInsert(List<Assignment__c> assignmentList){
        PRT_AssignmentTriggerHelper.createAssignmentWeekRecord(assignmentList);
    }
    public static void beforeUpdate(List<Assignment__c> assignmentList, Map<Id,Assignment__c> idToAssignmentMap){
        PRT_AssignmentTriggerHelper.createUpdateAssignmentForMilestone(assignmentList,idToAssignmentMap);
        PRT_AssignmentTriggerHelper.populateAssignmentName(assignmentList,idToAssignmentMap);
       
    }
     public static void afterUpdate(List<Assignment__c> assignmentList, Map<Id,Assignment__c> idToAssignmentMap){
        //PRT_AssignmentTriggerHelper.createAssignmentWeekRecord(assignmentList);
         PRT_AssignmentTriggerHelper.updateAssignmentWeekRecord(assignmentList,idToAssignmentMap);
        
    }
}
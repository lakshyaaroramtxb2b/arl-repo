/***********************************************************
* Handler class for DLM_Is_StakeholderTriggerHandler
* Created by    - Swarnima Singh Mandhata
* Date      - 20 July 2020
************************************************************/
public class DLM_Is_StakeholderTriggerHelper {
    
    public static void preventDuplicateStakeholders(List<IS_Stakeholder__c> newList){
        Map<Id,List<IS_Stakeholder__c>> standredIdWithStackholderMap = new Map<Id,List<IS_Stakeholder__c>>();
        Set<Id> isStandredSet = new Set<Id>();
        for(IS_Stakeholder__c isStakeRec: newList){
            isStandredSet.add(isStakeRec.IS_Standard__c);
        }
        system.debug('isStandredSet>>>' + isStandredSet);
        
        for(IS_Stakeholder__c stack : [SELECT id,Contact__c,Role__c,IS_Standard__c FROM IS_Stakeholder__c WHERE IS_Standard__c IN: isStandredSet] ){
            if(!standredIdWithStackholderMap.containsKey(stack.IS_Standard__c))
                standredIdWithStackholderMap.put(stack.IS_Standard__c, new List<IS_Stakeholder__c>());
            standredIdWithStackholderMap.get(stack.IS_Standard__c).add(stack);
        }
        for(IS_Stakeholder__c isStakeRec: newList){
            if(standredIdWithStackholderMap.containsKey(isStakeRec.IS_Standard__c)){
                for(IS_Stakeholder__c existingData : standredIdWithStackholderMap.get(isStakeRec.IS_Standard__c)){
                    if(isStakeRec.Role__c == existingData.Role__c && isStakeRec.Contact__c == existingData.Contact__c){
                        isStakeRec.addError('This stakeholder already exists');
                    }
                }   
            }
        }
    }
    
    public static void autoPoupulateManager(List<IS_Stakeholder__c> newList, Map<Id,IS_Stakeholder__c> oldMap){
        //Map<Id,Id> stakeholderIdToContactIdMap = new Map<Id,Id>();
        Set<Id> contactIdSet = new Set<Id>();
        Map<Id,String> contactManagerToContactIdMap = new Map<Id,String>();
        Map<String,id> contactIdToContactMap = new Map<String,id>();
        List<IS_Stakeholder__c> stakeholderUpdateList = new List<IS_Stakeholder__c>();
        for(IS_Stakeholder__c stakeRec: newList){
            if((stakeRec.Contact__c != null && oldMap == null) || (stakeRec.Contact__c != null && oldMap.get(stakeRec.Id).Contact__c != stakeRec.Contact__c)){
                //stakeholderIdToContactIdMap.put(stakeRec.Id, stakeRec.Contact__c);
                contactIdSet.add(stakeRec.Contact__c);
                
            }
        }
        System.debug('#### contactIdSet--> '+ contactIdSet);
        for(Contact con : [SELECT id, Manager_ID__c FROM Contact 
                           WHERE Manager_ID__c != null AND Id IN: contactIdSet]){
            contactManagerToContactIdMap.put(con.Id,con.Manager_ID__c);
                              
        }
         System.debug('#### contactManagerToContactIdMap--> '+ contactManagerToContactIdMap);
        
        for(Contact con : [SELECT id, Org62_User_ID__c FROM Contact 
                           WHERE Org62_User_ID__c IN: contactManagerToContactIdMap.values()]){
                               contactIdToContactMap.put(con.Org62_User_ID__c,con.id);
                           }
        System.debug('contactIdToContactMap-> '+ contactIdToContactMap);
        for(IS_Stakeholder__c stakeRec: newList){ 
            if(contactManagerToContactIdMap.containsKey(stakeRec.Contact__c)){
                 stakeRec.Stakeholder_Manager__c = contactIdToContactMap.get(contactManagerToContactIdMap.get(stakeRec.Contact__c));
            System.debug('stakeRec.Stakeholder_Manager__c--> '+ stakeRec.Stakeholder_Manager__c);
            }
           
        }  
        System.debug('stakeRec -->' + newList);
    } 
}
global with sharing class GuestVulnOrgs implements Database.Batchable<sObject>, Database.Stateful, Database.AllowsCallouts{
    global Set<String> vulnorgs = new Set<String>();
    global Set<String> custOrgs = new Set<String>();
    global Set<String> premierOrgs = new Set<String>();
    global Map<String, Integer> countByType = new Map<String, Integer>();
    public String entityName;
    global Database.QueryLocator start(Database.BatchableContext bc){
        return Database.getQueryLocator('select organization_id__c from '+entityName+' where guests_all_orgs__c != null and (status__c =\'active\' or guests_all_orgs__r.organization_id__c in :premierOrgs) and guests_all_orgs__r.organization_id__c not in :custOrgs');
    }
    global void execute(Database.BatchableContext bc, List<SObject> scope){
        for(SObject so : scope){
            vulnorgs.add((String)so.get('organization_id__c'));
        }
    }
    global void finish(Database.BatchableContext BC){
        system.debug(this.entityName+' - vulorgs size: '+vulnorgs.size());
        if(this.entityName == 'guests_public_groups__c'){
            GuestVulnOrgs gv = new GuestVulnOrgs();
            gv.vulnorgs = this.vulnorgs;
            gv.premierOrgs = premierOrgs;
            gv.custOrgs = this.custOrgs;
            gv.countByType = this.countByType;
            gv.entityName = 'guests_standard_objs__c';
            Database.executeBatch(gv, 2000);
        }
        else if(this.entityName == 'guests_standard_objs__c'){
            GuestVulnOrgs gv = new GuestVulnOrgs();
            gv.vulnorgs = this.vulnorgs;
            gv.premierOrgs = premierOrgs;
            gv.custOrgs = this.custOrgs;
            gv.countByType = this.countByType;
            gv.entityName = 'guests_cust_objs__c';
            Database.executeBatch(gv, 2000);
        }
        else if(this.entityName == 'guests_cust_objs__c'){
            GuestVulnOrgs gv = new GuestVulnOrgs();
            gv.vulnorgs = this.vulnorgs;
            gv.premierOrgs = premierOrgs;
            gv.custOrgs = this.custOrgs;
            gv.countByType = this.countByType;
            gv.entityName = 'guests_owd_entityperms__c';
            Database.executeBatch(gv, 2000);
        }
        else if(this.entityName == 'guests_owd_entityperms__c'){
            //call to SiteGuestInsecureSettings batch job for processing sites
            countByType.put('sitesCount',[select count() from guests_sfdc_sites__c where guests_all_orgs__r.organization_id__c in :this.vulnorgs and guests_all_orgs__r.organization_id__c not in :this.custOrgs]);
            SiteGuestInsecureSettings sg = new SiteGuestInsecureSettings();
            String query = 'select organization_id__c, Site_Status__c, domain__c, url_path_prefix__c from guests_sfdc_sites__c where guests_all_orgs__c != null and guests_all_orgs__r.organization_id__c in :vulnSites';
            sg.setQuery(query, null, vulnorgs, countByType, custOrgs, premierOrgs);
            Database.executeBatch(sg, 2000);
        }
        
    }
}
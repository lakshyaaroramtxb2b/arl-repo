/**
 * Esa Intake Request Trigger Helper
 */
public class EsaIntakeRequestTriggerHelper {

    /**
     * Close Case/Work Record - If Intake Request closed with approval flow
     * @param triggerNew 
     * @param triggerOld
     */
    public static void closeCaseWorkRecord(Esa_Security_Request__c triggerNew, Esa_Security_Request__c triggerOld){
        if(triggerNew.Status__c == 'Closed' && triggerNew.Status__c != triggerOld.Status__c){
            closeCaseWorkRecord(triggerNew.Id, triggerNew.Internal_Status__c);
        }
    }

    @future (callout=true)
    private static void closeCaseWorkRecord(String securityRequestId, String status){
        EsaService eService  = new EsaService();
        String comments = 'Your Request is ' + status;
        EsaService.EsaServiceResponse esaResponse = eService.closeInTakeRequest( securityRequestId, 'Closed', comments);
    }

}
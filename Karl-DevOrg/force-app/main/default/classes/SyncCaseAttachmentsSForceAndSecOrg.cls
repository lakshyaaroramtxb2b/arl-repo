public class SyncCaseAttachmentsSForceAndSecOrg {

    private Case secOrgCase;

    public SyncCaseAttachmentsSForceAndSecOrg(){

    }

    public SyncCaseAttachmentsSForceAndSecOrg(ApexPages.StandardController stdController) {
        secOrgCase = (Case)stdController.getRecord();
        secOrgCase = [SELECT Id,
                             SupportForce_Case_Id__c 
                      FROM Case WHERE Id=:secorgCase.Id]; 
    }

    public PageReference cloneCaseFilesFromSForce() {
    
        //getCaseFilesFromCombinedAttachmentsWithREST('5004u00002ISq6KAAT', '5004V000010vOX6');
        //return null;

        if(String.isNotBlank(secOrgCase.SupportForce_Case_Id__c)){
            getCaseFilesFromCombinedAttachmentsWithREST(secOrgCase.SupportForce_Case_Id__c, secOrgCase.Id);
        }
        PageReference curPage = New PageReference('/lightning/r/Case/'+secOrgCase.Id+'/view');
        return curPage;

    }

    
    private void getCaseFilesFromCombinedAttachmentsWithREST(String supportForceCaseId, String secOrgCaseId) {
        String sessionId = authenticateWithSOAP();
        System.debug('SOAP Authentication Done');

        // NOTES: Fetches all the combined attachments, notes, files in one call
       HTTPResponse response = makeRESTQuery(sessionId, 
                                                'SELECT+id,'
                                                + '(SELECT+Id,ParentId,Title,FileExtension,ContentUrl,ContentSize,RecordType+FROM+CombinedAttachments)'
                                                + 'FROM+Case+WHERE+id=\''+supportForceCaseId+'\'');


        System.debug('Response-' + response);
        if (response.getStatusCode() == 200) {
            List<GoogleDoc> lstGoogleDocs = new List<GoogleDoc>();
            List<Attachment> lstAttachments = new List<Attachment>();
            List<ContentVersion> lstContentversion = new List<ContentVersion>();
            List<Note> lstNotes = new List<Note>();
            Database.UpsertResult[] upsertResults;
            System.debug('Body JSON - ' + response.getBody());
            Map<String, Object> responseMap = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
            system.debug('######responseMap ## '+responseMap);
            for (Object supportForceCase : (List<Object>) responseMap.get('records')) {
                Map<String,Object> supportForceCaseMap = (Map<String,Object>) supportForceCase;

                Map<String,Object> combinedAttachmentsMap = (Map<String,Object>) supportForceCaseMap.get('CombinedAttachments');
                if(combinedAttachmentsMap != null) {
                
                for (Object attachment : (List<Object>) combinedAttachmentsMap.get('records')) {
                    Map<String,Object> combinedAttachment = (Map<String,Object>) attachment;
                    switch on (String) combinedAttachment.get('RecordType') {
                        when  'Attachment' {
                           if(lstAttachments.size() == 0) {
                              List<Attachment> attachments = cloneAttachment(sessionId, combinedAttachment, secOrgCaseId);
                              lstAttachments.addAll(attachments);
                              system.debug('########lstAttachments Size ####'+lstAttachments.size()); 
                           }
                        }
                        when 'File' {
                            if(lstContentversion.size() == 0) {
                               list<ContentVersion> contentVersions = cloneFiles(sessionId, combinedAttachment, secOrgCaseId);
                               lstContentversion.addAll(contentVersions);
                               system.debug('########lstContentversion Size ####'+lstContentversion.size());
                            }
                        }
                        when 'Note' {
                            if(lstNotes.size() == 0) {
                               List<Note> notes = cloneNotes(sessionId, combinedAttachment, secOrgCaseId);
                               lstNotes.addAll(notes);
                               system.debug('########lstNotes Size ####'+lstNotes.size());
                            }
                        }
                        when 'Google Doc'{
                            if(lstGoogleDocs.size() == 0) {
                               List<GoogleDoc> googleDocs = cloneGoogleDoc(sessionId, combinedAttachment, secOrgCaseId);
                               lstGoogleDocs.addAll(googleDocs);
                               system.debug('########lstGoogleDocs Size ####'+lstGoogleDocs.size());
                            }
                        }
                    }
                 }
                }
            }  

            Schema.SObjectField fieldInfo;

            if(lstGoogleDocs.size() > 0) {
                fieldInfo = GoogleDoc.Fields.Name;
                upsertResults = Database.Upsert(lstGoogleDocs, fieldInfo, false);
            }

            if(lstNotes.size() > 0){
                fieldInfo = Note.Fields.Title;
                upsertResults = Database.Upsert(lstNotes, fieldInfo, false); 
            }

            if(lstContentversion.size() > 0){
                Database.SaveResult[] SaveResult = Database.Insert(lstContentversion, false);
                set<Id> setConVerIds = new Set<Id>();
                for(ContentVersion cont : lstContentversion){
                    setConVerIds.add(cont.Id);
                }
                lstContentversion = [SELECT Id,
                                            ContentDocumentId 
                                     FROM ContentVersion 
                                     WHERE ID IN:setConVerIds];
                fieldInfo = ContentVersion.Fields.ContentDocumentId;     
                List<ContentDocumentLink> lstContDocLinks = [SELECT ID 
                                                             FROM ContentDocumentLink 
                                                             WHERE LinkedEntityId=:secOrgCaseId];

                if(lstContDocLinks.size() > 0){
                    delete lstContDocLinks;
                }           
                for(ContentVersion cont : lstContentversion){
                    ContentDocumentLink cDe = new ContentDocumentLink();
                    cDe.ContentDocumentId = cont.ContentDocumentId;
                    cDe.LinkedEntityId = secOrgCaseId; 
                    cDe.ShareType = 'I'; // Inferred permission, checkout description of ContentDocumentLink object for more details
                    cDe.Visibility = 'InternalUsers';
                    insert cDe;
                }
            }

            if(lstAttachments.size() > 0){
                fieldInfo = Attachment.Fields.Name;
                upsertResults = Database.Upsert(lstAttachments, fieldInfo, false);
                List<Database.Error> err = upsertResults[0].getErrors();
                system.debug('@@@@@@@@@@@'+err.size());
                if(err.size()>0) {
                   system.debug('@@@@@@@@@@@'+err[0].getMessage());
                }
            }

        }     
    }

    // Copy the Google Doc's from Supportforce to Security Org    
    private List<GoogleDoc> cloneGoogleDoc(String sessionId, Map<String,Object> combinedAttachment, String secOrgCaseId){
        
        String query = 'SELECT+id,Name,URL+FROM+GoogleDoc'
                        +'+WHERE+ParentId=\'' + combinedAttachment.get('ParentId') + '\'';
        System.debug(query);
        HTTPResponse response = makeRESTQuery(sessionId, query);
        //System.debug('Body JSON - ' + response.getBody());
        Map<String, Object> responseMap = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());

        List<GoogleDoc> lstGoogleDocs = new List<GoogleDoc>();
        for (Object gDoc : (List<Object>) responseMap.get('records')) {         
            Map<String,Object> gDocMap = (Map<String,Object>) gDoc;
            String objUrl = (String) ((Map<String,Object>) gDocMap.get('attributes')).get('url');
            response = makeRESTCall(sessionId, objUrl+ '/Body');

            GoogleDoc clonedGDoc = new GoogleDoc();
            clonedGDoc.Name = (String) gDocMap.get('Name');
            clonedGDoc.url = (String) gDocMap.get('Url');
            clonedGDoc.ParentId = secOrgCaseId;

            lstGoogleDocs.add(clonedGDoc);
        }
        
        return lstGoogleDocs;
        
    }


    // Copy Attachments
    private List<Attachment> cloneAttachment(String sessionId, Map<String,Object> combinedAttachment, String secOrgCaseId) {
        System.debug('Clone Attachment');
        // NOTES: Improve the logic and get the attachment based on combined attachment id not parent id
        String query = 'SELECT+id,Name+FROM+Attachment'
                        +'+WHERE+ParentId=\'' + combinedAttachment.get('ParentId') + '\'';                        
        System.debug(query);

        HTTPResponse response = makeRESTQuery(sessionId, query);
        Map<String, Object> responseMap = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
        List<Attachment> clonedAttachments = new List<Attachment>();
        for (Object attachment : (List<Object>) responseMap.get('records')) {         
            Map<String,Object> attachmentMap = (Map<String,Object>) attachment;
            String objUrl = (String) ((Map<String,Object>) attachmentMap.get('attributes')).get('url');
            response = makeRESTCall(sessionId, objUrl+ '/Body');

            Attachment clonedAttachment = new Attachment();
            clonedAttachment.Name = (String) attachmentMap.get('Name');
            clonedAttachment.Body = response.getBodyAsBlob();
            clonedAttachment.ParentId = secOrgCaseId;

            clonedAttachments.add(clonedAttachment);
        }

        return clonedAttachments;
    }


    // Copy Notes
    private List<Note> cloneNotes(String sessionId, Map<String,Object> combinedAttachment, String secOrgCaseId) {
        System.debug('Clone Notes');
        String query = 'SELECT+Title,Body+FROM+Note'
                        +'+WHERE+ParentId=\'' + combinedAttachment.get('ParentId') + '\'';                        
        System.debug(query);

        HTTPResponse response = makeRESTQuery(sessionId, query);
        Map<String, Object> responseMap = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
        List<Note> clonedNotes = new List<Note>();
        for (Object note : (List<Object>) responseMap.get('records')) {         
            Map<String,Object> notesMap = (Map<String,Object>) note;
            Note notDoc = new Note();
            notDoc.Title = (String) notesMap.get('Title');
            notDoc.Body = (String) notesMap.get('Body');
            notDoc.ParentId = secOrgCaseId;
            clonedNotes.add(notDoc);
        }

        return clonedNotes;
    }


    //Copy the Files
    private List<ContentVersion> cloneFiles(String sessionId, Map<String,Object> combinedAttachment, String secOrgCaseId) {
        String query = 'SELECT+FileType,Title,VersionData+FROM+ContentVersion'
                        +'+WHERE+ContentDocumentId=\'' + combinedAttachment.get('Id') + '\'';                        
        
        HTTPResponse response = makeRESTQuery(sessionId, query);
        System.debug('Body Files JSON - ' + response.getBody());
        Map<String, Object> responseMap = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
        List<ContentVersion> clonedFlies = new List<ContentVersion>();
        String fileName;
        for (Object fileInfo : (List<Object>) responseMap.get('records')) {         
            Map<String,Object> fliesMap = (Map<String,Object>) fileInfo;
            String objUrl = (String) ((Map<String,Object>) fliesMap.get('attributes')).get('url');
            system.debug('#####'+objUrl);
            response = makeRESTCall(sessionId, objUrl+ '/VersionData');
            fileName = (String) fliesMap.get('Title') + '.'+(String) fliesMap.get('FileType');
            ContentVersion clonedFile = new ContentVersion();
            clonedFile.ContentLocation = 'S'; 
            clonedFile.Title = (String) fliesMap.get('Title');
            clonedFile.PathOnClient = fileName;
            clonedFile.VersionData = response.getBodyAsBlob();
            clonedFlies.add(clonedFile);
        }
        return clonedFlies;
    }

    private static HTTPResponse makeRESTQuery(String sessionId, String query) {
        HttpRequest req = new HttpRequest();
        String endPointUrl = 'callout:SupportForceNC/services/data/v48.0/query?' 
                                +'q=' + query;
        system.debug('End Point URL *****'+endPointUrl);
        req.setEndpoint(endPointUrl);
        req.setHeader('Content-Type', 'application/json');
        req.setHeader('Authorization', 'Bearer ' + sessionId);
        req.setMethod('GET');

        Http http = new Http();
        return http.send(req);
    }

    private HTTPResponse makeRESTCall(String sessionId, String relativeUrl) {
        HttpRequest req = new HttpRequest();
        String endPointUrl = 'callout:SupportForceNC' + relativeUrl;
        req.setEndpoint(endPointUrl);
        //req.setHeader('Content-Type', 'application/json');
        req.setHeader('Authorization', 'Bearer ' + sessionId);
        req.setMethod('GET');

        Http http = new Http();
        return http.send(req);
    }

    private String authenticateWithSOAP() {
        String NS_SOAP = 'http://schemas.xmlsoap.org/soap/envelope/';
            String NS_SF = 'urn:partner.soap.sforce.com';
            HttpRequest soapreq = new HttpRequest();
            soapreq.setMethod('POST');
            soapreq.setTimeout(60000);
            soapreq.setEndpoint('callout:SupportForceNC/services/Soap/u/45.0');
            soapreq.setHeader('Content-Type', 'text/xml;charset=UTF-8');
            soapreq.setHeader('SOAPAction', '""');
            soapreq.setBody('<Envelope xmlns="http://schemas.xmlsoap.org/soap/envelope/"><Header/>' +
                        '<Body><login xmlns="urn:partner.soap.sforce.com"><username>' +
                        '{!HTMLENCODE($Credential.UserName)}' +
                        '</username><password>' +
                        '{!HTMLENCODE($Credential.Password)}' +
                        '</password></login></Body></Envelope>');
            HttpResponse res =  new Http().send(soapreq);

            if(res.getStatusCode() != 200) {
                Dom.Document responseDocument = res.getBodyDocument();
                Dom.Xmlnode rootElm = responseDocument.getRootElement(); // soapenv:Envelope
                Dom.Xmlnode bodyElm = rootElm.getChildElement('Body', NS_SOAP); // soapenv:Body
                Dom.Xmlnode faultElm = bodyElm.getChildElement('Fault', NS_SOAP); // soapenv:Fault
                String exceptionText = 'Login Failure';
                if (faultElm != null) {
                    Dom.Xmlnode faultStringElm = faultElm.getChildElement('faultstring', null);
                    if (faultStringElm!=null && faultStringElm.getText() != null) {
                        exceptionText = faultStringElm.getText();
                    }
                }
                throw new CalloutException();
            }

            String sessionId;
            Dom.Document responseDocument = res.getBodyDocument();
            Dom.Xmlnode rootElm = responseDocument.getRootElement(); // soapenv:Envelope
            Dom.Xmlnode bodyElm = rootElm.getChildElement('Body', NS_SOAP); // soapenv:Body
            Dom.Xmlnode loginResponseElm = bodyElm.getChildElement('loginResponse', NS_SF); // loginResponse
            if(!Test.isRunningTest()) {
                Dom.Xmlnode resultElm = loginResponseElm.getChildElement('result', NS_SF); // result
                Dom.Xmlnode sessionIdElm = resultElm.getChildElement('sessionId', NS_SF); // sessionId
                Dom.Xmlnode servierUrl = resultElm.getChildElement('servierUrl', NS_SF); // serverURL
                sessionId = sessionIdElm.getText();
            }
            return sessionId;
    }
}
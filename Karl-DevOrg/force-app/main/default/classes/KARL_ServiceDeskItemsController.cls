public with sharing class KARL_ServiceDeskItemsController {
    @TestVisible private static Id testContactId;
    @AuraEnabled(cacheable=true)
    public static List<ServiceDeskInfoWrapper> getSubmittedServiceDeskInfo(){
        // Step 1: Determine where we are coming from, to generate the links appropriately for Community vs. Salesforce
        Network grcNetwork;
        String auditorloginurl = '';
        String auditorcommunityHomeUrl = '';
        Integer index;
        Id contactId;
        if(!Test.isRunningTest()){
            grcNetwork = [SELECT Id FROM Network WHERE Name ='GRC Community' WITH SECURITY_ENFORCED];
            auditorloginurl = Network.getLoginUrl(grcNetwork.Id);
            index = auditorloginurl.lastIndexOf('/login');
            auditorcommunityHomeUrl = auditorloginurl.substring(0, index + 1);
        }

        String baseUrl = '';
        String baseRecordURL = '';
        String baseUrlSuffix = '';
        // Check if we're on the KARL Community or not
        if (Site.getSiteId() != null) {
            baseUrl = auditorcommunityHomeUrl; // we're on the community
            baseRecordURL = baseUrl + 'detail/';
        } else {
            baseUrl = URL.getSalesforceBaseUrl().toExternalForm();
            baseRecordURL = baseUrl + '/one/one.app?#/sObject/'; // force rendering in Lightning instead of Classic
            baseUrlSuffix = '/view'; // Required for Lightning record view
        }

        User userinfoObj = [SELECT Id,ContactId FROM User WHERE Id =:UserInfo.getUserId()];
        //Step 2 -> Return list of service Desk Info Wrapper
        if(!Test.isRunningTest()){
            contactId = userinfoObj.ContactId;
        }
        else{
            contactId = testContactId;
        }
        List<ServiceDeskInfoWrapper> serviceDeskInfoWrapperList = new List<ServiceDeskInfoWrapper>();
        for(KARL_Service_Desk__c serviceDeskObj : [SELECT Id,Name,Request_Name__r.Request_Name__c,GRC_Assignee__r.Name,Status__c,Subject__c
                                                    FROM KARL_Service_Desk__c 
                                                    WHERE Requested_By__c =: contactId
                                                    AND RecordTypeId =: KARL_Constants.REQUEST_MASTER_DATA_RECORDTYPEID
                                                    WITH SECURITY_ENFORCED
                                                    ORDER BY LastModifiedDate DESC]){
            ServiceDeskInfoWrapper serviceDeskInfoWrapperObj = new ServiceDeskInfoWrapper();
            serviceDeskInfoWrapperObj.Id = serviceDeskObj.Id;
            serviceDeskInfoWrapperObj.serviceDeskLink = baseRecordURL + serviceDeskObj.Id + baseUrlSuffix; 
            serviceDeskInfoWrapperObj.serviceDeskName = serviceDeskObj.Name;
            serviceDeskInfoWrapperObj.requestName = serviceDeskObj.Subject__c;
            serviceDeskInfoWrapperObj.status = serviceDeskObj.Status__c;
            serviceDeskInfoWrapperObj.grcAssigneeName = serviceDeskObj.GRC_Assignee__r.Name;
            serviceDeskInfoWrapperList.add(serviceDeskInfoWrapperObj);
        }
        return serviceDeskInfoWrapperList;
    }

    public class ServiceDeskInfoWrapper{
        @AuraEnabled public String Id;
        @AuraEnabled public String serviceDeskName;
        @AuraEnabled public String serviceDeskLink;
        @AuraEnabled public String requestName;
        @AuraEnabled public String status;
        @AuraEnabled public String grcAssigneeName;
    }
}
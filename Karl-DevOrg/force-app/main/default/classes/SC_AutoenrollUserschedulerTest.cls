@isTest
public class SC_AutoenrollUserschedulerTest {
    public static String generateOrg62Id() {
        return String.valueOf(Math.random()).rightPad(18,'0').substring(0, 18);
    }

    public static testMethod void testAll() {
        SC_Training_Provider__c prov = new SC_Training_Provider__c(Name='Trailhead',Type__c='Trailhead');
        insert prov;
        
        User owner = [SELECT Id FROM User WHERE IsActive=True AND Profile.Name = 'System Administrator' AND Profile.UserLicense.LicenseDefinitionKey = 'SFDC' AND UserRoleId != null LIMIT 1];
        Account acct = new Account(name='blah', OwnerId=owner.Id);
        insert acct;
        //mixed dml
        System.RunAs(owner) {
            insertEmailTeamplate();
        }
        Contact onedoez = new Contact(
            AccountId = acct.Id,
            Is_Active__c = True,
            RecordTypeId = Contact_Constants.RT_ID_SALESFORCE_EMPLOYEE,
            Email = 'johndoe@doe.com',
            FirstName = '1',
            LastName = 'doez',
            Hire_Date__c = System.today().addDays(-100),
            Org62_User_ID__c = SC_AutoenrollUserschedulerTest.generateOrg62Id(),
            Birthdate = Date.newInstance(1990,1,1)
        );
        Contact twodoez = new Contact(
            AccountId = acct.Id,
            Is_Active__c = True,
            RecordTypeId = Contact_Constants.RT_ID_SALESFORCE_EMPLOYEE,
            Email = 'janedoe@doe.com',
            FirstName = '2',
            LastName = 'doez',
            Hire_Date__c = System.today().addDays(-2),
            Org62_User_ID__c = SC_AutoenrollUserschedulerTest.generateOrg62Id(),
            Birthdate = Date.newInstance(2000,1,1)
        );
        Contact threedoez = new Contact(
            AccountId = acct.Id,
            Is_Active__c = True,
            RecordTypeId = Contact_Constants.RT_ID_SALESFORCE_EMPLOYEE,
            Email = 'earldoe@doe.com',
            FirstName = '3',
            LastName = 'doez',
            Hire_Date__c = System.today().addDays(-2),
            Org62_User_ID__c = SC_AutoenrollUserschedulerTest.generateOrg62Id(),
            Birthdate = Date.newInstance(2000,1,1)
        );
        Contact fourdoez = new Contact(
            AccountId = acct.Id,
            Is_Active__c = True,
            RecordTypeId = Contact_Constants.RT_ID_SALESFORCE_EMPLOYEE,
            Email = 'suedoe@doe.com',
            FirstName = '4',
            LastName = 'doez',
            Hire_Date__c = System.today(),
            Org62_User_ID__c = SC_AutoenrollUserschedulerTest.generateOrg62Id(),
            Birthdate = Date.newInstance(1990,1,1)
        );
        Contact fivedoez = new Contact(
            AccountId = acct.Id,
            Is_Active__c = True,
            RecordTypeId = Contact_Constants.RT_ID_SALESFORCE_EMPLOYEE,
            Email = 'marydoe@doe.com',
            FirstName = '5',
            LastName = 'doez',
            Hire_Date__c = System.today().addDays(-2),
            Org62_User_ID__c = SC_AutoenrollUserschedulerTest.generateOrg62Id(),
            Birthdate = Date.newInstance(1990,1,1)
        );

        Contact[] cts = new Contact[]{onedoez, twodoez, threedoez, fourdoez, fivedoez};                                       
        insert cts;
        
        EmailTemplate template = [SELECT DeveloperName FROM EmailTemplate LIMIT 1];
        
        Training_Course__c[] courses = new Training_Course__c[]{
            new Training_Course__c(Provider__c='Trailhead',Auto_Enroll_Users_Hired_After__c=System.today().addDays(-5),Url__c='http://one',Provider_ID__c='1',Name='One',Default_Due_Days__c=15, Tracked__c=True, Default_Enrollment_Template_DevelopName__c=template.DeveloperName),
            new Training_Course__c(Provider__c='Trailhead',Auto_Enroll_Users_Hired_After__c=System.today().addDays(-5),Url__c='http://two',Provider_ID__c='2',Name='Two',Default_Due_Days__c=10, Tracked__c=True, Default_Enrollment_Template_DevelopName__c=template.DeveloperName),
            // invalid, no template
            new Training_Course__c(Provider__c='Trailhead',Auto_Enroll_Users_Hired_After__c=System.today().addDays(-5),Url__c='http://three',Provider_ID__c='3',Name='Three',Default_Due_Days__c=5, Tracked__c=True),
            // invalid, no default due days
            new Training_Course__c(Provider__c='Trailhead',Auto_Enroll_Users_Hired_After__c=System.today().addDays(-5),Url__c='http://four',Provider_ID__c='4',Name='Four',Tracked__c=True, Default_Due_Days__c=null,Default_Enrollment_Template_DevelopName__c=template.DeveloperName),
            // course with where clause but no hired after date
            new Training_Course__c(Provider__c='Trailhead',Auto_Enroll_Where_Clause_10K__c='Contact.Email=\''+fivedoez.Email+'\'',Url__c='http://five',Provider_ID__c='5',Name='Five',Default_Due_Days__c=10, Tracked__c=True, Default_Enrollment_Template_DevelopName__c=template.DeveloperName),
            // course with custom where clause and a hired after after date
            new Training_Course__c(Provider__c='Trailhead',Auto_Enroll_Where_Clause_10K__c='Contact.Birthdate=1990-01-01', Auto_Enroll_Users_Hired_After__c=System.today().addDays(-5),Url__c='http://six',Provider_ID__c='6',Name='Six',Default_Due_Days__c=10, Tracked__c=True, Default_Enrollment_Template_DevelopName__c=template.DeveloperName)
        };
        insert courses;
        
        Id[] courseIds = new Id[]{};
        for (Training_Course__c course : courses) {
            courseIds.add(course.Id);
        }
        
        Set<Id> ids = new Set<Id>();
        for (Contact c : cts) { ids.add(c.Id); }
        
        // let's enroll 2 ahead of time. This one should get switched to mandatory and have the due_date__c shortened
        RecordType voluntary = [SELECT Id FROM RecordType WHERE SObjectType='Training_Course_Taken__c' AND DeveloperName='Voluntary_Enrollment'];
        Training_Course_Taken__c taken2 = new Training_Course_Taken__c(
            Training_Course__c = courses.get(0).Id,
            Contact__c = twodoez.Id, 
            Date_Started__c = System.today().addDays(-1),
            RecordTypeId = voluntary.Id,
            Due_Date__c = System.today().addDays(60),
            Attendee_Email__c = twodoez.Email
        );
        insert taken2;
        
        // 3 already completed the course. It shouldn't be touched
        // let's enroll 2 ahead of time. This one should get switched to mandatory and have the due_date__c shortened
        Training_Course_Taken__c taken3 = new Training_Course_Taken__c(
            Training_Course__c = courses.get(0).Id,
            Contact__c = threedoez.Id, 
            Date_Started__c = System.today().addDays(-1),
            Date_Completed__c = System.today().addDays(-1),
            Status__c = 'Course Completed',
            RecordTypeId = voluntary.Id,
            Due_Date__c = System.today().addDays(60),
            Attendee_Email__c = threedoez.Email
        );
        insert taken3;
                 
        Test.startTest();
            //SC_ContactToUser.upsertUsersAsync(ids);
        Test.stopTest(); // since it's @future due to operating on setup objects
         
        SC_AutoenrollUserscheduler s = new SC_AutoenrollUserscheduler();
        s.execute(null);   
                    
        User[] theDoes = [SELECT Id,FirstName,Contact.Hire_Date__c FROM User WHERE LastName='doez' ORDER BY FirstName ASC];
        System.assert(theDoes.size() == 5, theDoes);
        
        // 1 has been around for a while and shouldn't be autoenrolled
        
        // They were hired before the cutoff
        Id cid = onedoez.Id;
        System.assert(0 == Database.countQuery('SELECT count() FROM Training_Course_Taken__c WHERE Contact__c=: cid AND Training_Course__c IN:courseIds'));                                                                      
        
        // the voluntary one that should be switched to mandatory
        cid = twodoez.Id;
        Integer count = Database.countQuery('SELECT count() FROM Training_Course_Taken__c WHERE Contact__c=:cid AND Training_Course__c IN:courseIds');
        System.assert(2 == count, count);
        Training_Course_Taken__c taken2new = [SELECT RecordTypeId,Due_Date__c FROM Training_Course_Taken__c WHERE Id=:taken2.Id];
        System.assert(voluntary.Id != taken2new.RecordTypeId);                                                                      
        System.assert(taken2new.Due_Date__c != taken2.Due_Date__c, taken2new.Due_Date__c);
        
        // This one should be left alone since it was completed.  it will not be added to 4 due to the where clause
        cid = threedoez.Id;
        System.assert(2 == Database.countQuery('SELECT count() FROM Training_Course_Taken__c WHERE Contact__c=:cid AND Training_Course__c IN:courseIds'));
        Training_Course_Taken__c taken3new = [SELECT Training_Course__c,RecordTypeId,Date_Completed__c,Status__c,Due_Date__c FROM Training_Course_Taken__c WHERE Id=:taken3.Id];
        System.assert(taken3.Date_Completed__c == taken3new.Date_Completed__c);
        System.assert(taken3.Due_Date__c == taken3new.Due_Date__c);
        System.assert(taken3.Status__c == taken3new.Status__c);
        System.assert(taken3.RecordTypeId == taken3new.RecordTypeId);
        System.assert(taken3.Training_Course__c == taken3new.Training_Course__c);
        
        // fourdoez just got hired today and hasn't met his 1 day enrollment delay
        cid = fourdoez.Id;
        System.assert(0 == Database.countQuery('SELECT count() FROM Training_Course_Taken__c WHERE Contact__c=:cid AND Training_Course__c IN:courseIds'));
        
        cid = fivedoez.Id;
        System.assert(4 == Database.countQuery('SELECT count() FROM Training_Course_Taken__c WHERE Contact__c=:cid AND Training_Course__c IN:courseIds'));
    }

    static void setup() {
        SC_Training_Provider__c trainingProvider = new SC_Training_Provider__c(Name='Trailhead',Type__c='Trailhead');
        insert trainingProvider;

        User owner = [SELECT Id FROM User WHERE IsActive=TRUE AND Profile.Name = 'System Administrator' AND Profile.UserLicense.LicenseDefinitionKey = 'SFDC' AND UserRoleId != NULL LIMIT 1];
        Account acct = new Account(Name='blah', OwnerId=owner.Id);
        insert acct;
        //mixed dml
        System.RunAs(owner) {
            insertEmailTeamplate();
        }

        List<Contact> contacts = new List<Contact>();

        for (Integer i = 0; i < 4; i++) {
            contacts.add(new Contact(
                    AccountId = acct.Id,
                    Is_Active__c = true,
                    RecordTypeId = Contact_Constants.RT_ID_SALESFORCE_EMPLOYEE,
                    Email = 'johndoe@doe.com'+i,
                    FirstName = 'Test' + i,
                    LastName = 'Contact',
                    Hire_Date__c = System.today().addDays(-20 * i),
                    Org62_User_ID__c = SC_AutoenrollUserschedulerTest.generateOrg62Id()
            ));
        }

        insert contacts;

        Test.startTest();
        //SC_ContactToUser.upsertUsersAsync(new Map<Id, Contact>(contacts).keySet());
        Test.stopTest(); // since it's @future due to operating on setup objects
    }

    @IsTest
    static void testGetNotTaken_noAutoEnrollWhere() {
        setup();
        EmailTemplate template = [SELECT DeveloperName FROM EmailTemplate LIMIT 1];

        Training_Course__c trainingCourse = new Training_Course__c(
                Provider__c='Trailhead',
                Auto_Enroll_Users_Hired_After__c=System.today().addDays(-45),
                URL__c='http://one',
                Provider_ID__c='1',
                Name='One',
                Default_Due_Days__c=15,
                Tracked__c=true,
                Default_Enrollment_Template_DevelopName__c=template.DeveloperName);
        insert trainingCourse;

        SC_AutoenrollUserscheduler autoenrollUserscheduler = new SC_AutoenrollUserscheduler();
//        User[] users = autoenrollUserscheduler.getNotTaken(trainingCourse);

//        System.assertEquals(2, users.size(), '3 users have been hired within the past 45 days, one was hire today and is not eligible');
    }

    @IsTest
    static void testGetNotTaken_noAutoEnrollHireDate() {
        setup();
        EmailTemplate template = [SELECT DeveloperName FROM EmailTemplate LIMIT 1];

        Training_Course__c trainingCourse = new Training_Course__c(
                Provider__c='Trailhead',
                Auto_Enroll_Where_Clause_10K__c='FirstName LIKE \'%1%\' OR FirstName LIKE \'%2%\'',
                URL__c='http://one',
                Provider_ID__c='1',
                Name='One',
                Default_Due_Days__c=15,
                Tracked__c=true,
                Default_Enrollment_Template_DevelopName__c=template.DeveloperName);
        insert trainingCourse;

        SC_AutoenrollUserscheduler autoenrollUserscheduler = new SC_AutoenrollUserscheduler();
//        User[] users = autoenrollUserscheduler.getNotTaken(trainingCourse);

//        System.assertEquals(2, users.size(), 'Only include users 1 and 2');
    }

    @IsTest
    static void testGetNotTaken_bothAutoEnrollDateAndWhereClause() {
        setup();
        EmailTemplate template = [SELECT DeveloperName FROM EmailTemplate LIMIT 1];

        Training_Course__c trainingCourse = new Training_Course__c(
                Provider__c='Trailhead',
                Auto_Enroll_Users_Hired_After__c=System.today().addDays(-25),
                Auto_Enroll_Where_Clause_10K__c='FirstName LIKE \'%1%\' OR FirstName LIKE \'%2%\'',
                URL__c='http://one',
                Provider_ID__c='1',
                Name='One',
                Default_Due_Days__c=15,
                Tracked__c=true,
                Default_Enrollment_Template_DevelopName__c=template.DeveloperName);
        insert trainingCourse;

        SC_AutoenrollUserscheduler autoenrollUserscheduler = new SC_AutoenrollUserscheduler();
        // User[] users = autoenrollUserscheduler.getNotTaken(trainingCourse);
        
        //System.assertEquals(1, users.size(), 'User 2 was hired more than 25 days ago');
    }

    @IsTest
    static void testGetNotTaken_invalidAutoEnrollDateAndWhereClause_expectException() {
        setup();
        EmailTemplate template = [SELECT DeveloperName FROM EmailTemplate LIMIT 1];

        Training_Course__c trainingCourse = new Training_Course__c(
                Provider__c='Trailhead',
                Auto_Enroll_Users_Hired_After__c=System.today().addDays(-25),
                Auto_Enroll_Where_Clause_10K__c='THIS where IS and INVALID from SOQL',
                URL__c='http://one',
                Provider_ID__c='1',
                Name='One',
                Default_Due_Days__c=15,
                Tracked__c=true,
                Default_Enrollment_Template_DevelopName__c=template.DeveloperName);
        insert trainingCourse;

        SC_AutoenrollUserscheduler autoenrollUserscheduler = new SC_AutoenrollUserscheduler();

        Boolean exceptionThrown = false;

        try {
            // User[] users = autoenrollUserscheduler.getNotTaken(trainingCourse);
        } catch (QueryException e) {
            exceptionThrown = true;
        }
        System.assert(exceptionThrown, 'Exception should be thrown due to bad where clause');
    }

    @future
    private static void insertEmailTeamplate()
    {
        EmailTemplate newtemplate = new EmailTemplate(Name = 'test', FolderId = UserInfo.getUserId(), TemplateType= 'text', DeveloperName='test');
        insert newtemplate;
    }
}
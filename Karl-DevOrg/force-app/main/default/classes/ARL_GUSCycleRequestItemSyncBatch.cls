/**
* @author Mahima Aggarwal
* @email mahima.aggarwal@mtxb2b.com
* @description This batch class sync data from GUS to Cycle Request Item.
*/
public class ARL_GUSCycleRequestItemSyncBatch implements Database.Batchable<sObject>,Schedulable {
    @testVisible
    private static List<ADM_Work_c__x> admWorkList=new List<ADM_Work_c__x>();
    @testVisible
    private static List<ADM_Scrum_Team_c__x> admscrunTeamList=new List<ADM_Scrum_Team_c__x>();
     @testVisible
    private static List<GusUser__x> gusUserList=new List<GusUser__x>();
    
    /**
    * @author Mahima Aggarwal
    * @email mahima.aggarwal@mtxb2b.com
    * @description This is start method for batch and it queries the all Cycle Request item where Request_GUS__c is not null.
    */
    public Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([SELECT Id,Request_GUS__c,Team__c,Cycle_Request_Status__c,Request_Tech_Details__c,GUS_Status__c,
                                         Request_Assignee__c,Status_Start_Time__c
                                         FROM Cycle_Request_Item__c WHERE Request_GUS__c != null]);
    }
    
    /**
    * @author Mahima Aggarwal
    * @email mahima.aggarwal@mtxb2b.com
    * @description Update the cycle request items with GUS records.
    */
    public void execute(Database.BatchableContext BC, List<Cycle_Request_Item__c> cycleReqItemList){       
        doSync( cycleReqItemList );
    }
    
    /**
    * @author Mahima Aggarwal
    * @email mahima.aggarwal@mtxb2b.com
    * @description This method fetch data from GUS and update the cycle request item.
    */
    private void doSync(List<Cycle_Request_Item__c> cycleReqItemList){
        Map<String,Cycle_Request_Item__c> idCycleRequestItemMap = new Map<String,Cycle_Request_Item__c>();
        Map<String,String> idToOldStatusMap = new Map<String,String>();
        Map<String,datetime> idToStartTimeMap = new Map<String,datetime>();
        Map<Id,String> idToTeamIdMap = new Map<Id,String>();
        List<KARL_GUS_SLA_Tracking__c> gusSlaTrackingList = new List<KARL_GUS_SLA_Tracking__c>();
        
        //Updating correponding evidence record with the GUS details so that
        //this field can be refrenced into evidence upload community
        //Note: This piece of code can be removed after we have proper sharing in place for evidence upload community
        //Added by Mahima Aggarwal 10/29/2020
        Set<Id> cycleReqItemIds = new Set<Id>();
        Map<Id,Id> cycleReqIdToEvidenceIdMap = new Map<Id,Id>();
        List<KARL_Evidence_Request__c> evidenceReqUpdateList = new List<KARL_Evidence_Request__c>();
        Map<String,String> cycleRequestIdToGusWorkIdMap = new Map<String,String>();
        Set<String> idGusStringSet = new Set<String>();
        Set<Id> contactIdSet = new Set<Id>();
        Set<String> teamIdSet = new Set<String>();
        
        for(Cycle_Request_Item__c cri : cycleReqItemList){
            cycleReqItemIds.add(cri.Id);
        }
        
        for(KARL_Evidence_Request__c evidenceReq : [SELECT Id, KARL_Submitted__c ,KARL_Cycle_ARL_Item__c
                                                    FROM KARL_Evidence_Request__c
                                                    WHERE KARL_Cycle_ARL_Item__c IN : cycleReqItemIds]){
                                                        cycleReqIdToEvidenceIdMap.put(evidenceReq.KARL_Cycle_ARL_Item__c,evidenceReq.Id);
                                                    }
        //end of Mahima Changes 10/29/2020
        
        for(Cycle_Request_Item__c cycleReqItem : cycleReqItemList){
            idCycleRequestItemMap.put(cycleReqItem.Request_GUS__c,cycleReqItem);
            
            if(cycleReqItem.Team__c != null){
                if(!idToTeamIdMap.containsKey(cycleReqItem.id)){
                    idToTeamIdMap.put(cycleReqItem.id,cycleReqItem.Team__c);
                    teamIdSet.add(cycleReqItem.Team__c);
                }
            }
            if(cycleReqItem.Request_GUS__c != null && cycleReqItem.Request_Assignee__c != null){
                contactIdSet.add(cycleReqItem.Request_Assignee__c);
            }
            if(!cycleRequestIdToGusWorkIdMap.containsKey(cycleReqItem.Id) && cycleReqItem.Request_GUS__c != null){
                cycleRequestIdToGusWorkIdMap.put(cycleReqItem.Id,cycleReqItem.Request_GUS__c);
                idGusStringSet.add(cycleReqItem.Request_GUS__c);
            }
        } 
        
        //Fetch all GUS work records
        for(ADM_Work_c__x admWork : getAdmList(idCycleRequestItemMap.keySet())){
            Cycle_Request_Item__c cycleReqItem = idCycleRequestItemMap.get(admWork.ExternalId);
            cycleReqItem.Request_Tech_Details__c = admWork.Details_c__c;
           
            if(!idToOldStatusMap.containsKey(cycleReqItem.Id) && cycleReqItem.GUS_Status__c != admWork.Status_c__c){
                System.debug(LoggingLevel.DEBUG, 'admWork.Status_c__c--> '+ admWork.Status_c__c);
                idToOldStatusMap.put(cycleReqItem.Id, admWork.Status_c__c);
                System.debug(LoggingLevel.DEBUG, 'cycleReqItem.GUS_Status__c--> '+ cycleReqItem.GUS_Status__c);
                if(cycleReqItem.Status_Start_Time__c != null){
                    idToStartTimeMap.put(cycleReqItem.Id, cycleReqItem.Status_Start_Time__c);
                }
                if(String.isNotEmpty(admWork.Status_c__c)){
                    cycleReqItem.Status_Start_Time__c = datetime.now();
                }
            }
            
            cycleReqItem.GUS_Status__c = admWork.Status_c__c;
            cycleReqItem.Team__c = admWork.Team__c;   
            cycleReqItem.Assigned_To__c = admWork.Assignee_c__c;
            cycleReqItem.GUS_URL_Direct_Link__c = admWork.DisplayUrl;
            
            //Added by Mahima 10/29/2020
            Id evidenceReqId = cycleReqIdToEvidenceIdMap.get(cycleReqItem.Id);
            if(evidenceReqId != null){
                KARL_Evidence_Request__c evReq = new KARL_Evidence_Request__c();
                evReq.Id = evidenceReqId;
                evReq.GUS_Details__c = admWork.Details_c__c;
                
                evidenceReqUpdateList.add(evReq);
            }
        }

        //Get last modified on work records. Added by Swarnima 04/12/2021.
        Map<String,String> gusRecordToLastModifiedIdMap = new Map<String,String>();
        if(test.isRunningTest()){
            gusRecordToLastModifiedIdMap.put(admWorkList[0].ExternalId,admWorkList[0].LastModifiedById__c);
        }else{
            if(cycleRequestIdToGusWorkIdMap != null && !cycleRequestIdToGusWorkIdMap.isEmpty() && !idGusStringSet.isEmpty()){
                for(ADM_Work_c__x admWorkRec: [SELECT Id, ExternalId, LastModifiedById__c   
                                               FROM ADM_Work_c__x
                                               WHERE ExternalId IN:idGusStringSet AND LastModifiedById__c   != null]){
                                                   if(!gusRecordToLastModifiedIdMap.containsKey(admWorkRec.ExternalId)){
                                                       gusRecordToLastModifiedIdMap.put(admWorkRec.ExternalId,admWorkRec.LastModifiedById__c );
                                                   }
                                               }
            }
        }
        System.debug(LoggingLevel.DEBUG, 'gusRecordToLastModifiedIdMap--> '+ gusRecordToLastModifiedIdMap);
        System.debug(LoggingLevel.DEBUG, 'idToOldStatusMap--> '+ idToOldStatusMap);
        
        //Insert feed Item 
        Map<Id,String> gusUserIdOrContactIdToNameMap = new Map<Id,String>();
        Map<String,String> teamIdToProductOwnerIdMap = new Map<String,String>();
        Map<String,String> teamIdToScrumMasterIdMap = new Map<String,String>();
        Set<Id> assignedToIdSet = new Set<Id>();
        
        List<FeedItem__x> feedItemList = new List<FeedItem__x>();
        if(!idToOldStatusMap.isEmpty()){
            Set<Cycle_Request_Item__c> requestItemSet = new Set<Cycle_Request_Item__c>();
            for(Cycle_Request_Item__c reqItem:[SELECT id,Request_GUS__c,Team__c,Cycle_Request_Status__c,Request_Tech_Details__c,GUS_Status__c,
                                               Request_Assignee__c,Status_Start_Time__c 
                                               FROM Cycle_Request_Item__c WHERE Id IN:idToOldStatusMap.keySet()]){
                                                   requestItemSet.add(reqItem);
                                               }
            for(Cycle_Request_Item__c itemReq: requestItemSet){
                if(idToOldStatusMap.containsKey(itemReq.id)){
                    
                    //Get Product owner and Scrum Master from Team 
                    if(test.isRunningTest()){
                        teamIdToProductOwnerIdMap.put(admscrunTeamList[0].externalId,admscrunTeamList[0].Product_Owner_c__c);
                        teamIdToScrumMasterIdMap.put(admscrunTeamList[0].externalId,admscrunTeamList[0].Scrum_Master_c__c );
                        assignedToIdSet.add(admscrunTeamList[0].Product_Owner_c__c);
                        assignedToIdSet.add(admscrunTeamList[0].Scrum_Master_c__c);
                    }
                   if(!teamIdSet.isEmpty()){
                        for(ADM_Scrum_Team_c__x scrumTeam: [SELECT id,externalId, Product_Owner_c__c,Scrum_Master_c__c 
                                                            FROM ADM_Scrum_Team_c__x
                                                            WHERE externalId IN:teamIdSet]){
                                                                if(String.isNotEmpty(scrumTeam.Product_Owner_c__c) && !teamIdToProductOwnerIdMap.containsKey(scrumTeam.externalId)){
                                                                    assignedToIdSet.add(scrumTeam.Product_Owner_c__c);
                                                                    teamIdToProductOwnerIdMap.put(scrumTeam.externalId,scrumTeam.Product_Owner_c__c);
                                                                }
                                                                if(String.isNotEmpty(scrumTeam.Scrum_Master_c__c) && !teamIdToScrumMasterIdMap.containsKey(scrumTeam.externalId)){
                                                                    assignedToIdSet.add(scrumTeam.Scrum_Master_c__c);
                                                                    teamIdToScrumMasterIdMap.put(scrumTeam.externalId,scrumTeam.Scrum_Master_c__c);
                                                                }
                                                            }
                    }
                    System.debug(LoggingLevel.DEBUG, 'teamIdToProductOwnerIdMap --> '+ teamIdToProductOwnerIdMap);
                    
                    if(!gusRecordToLastModifiedIdMap.isEmpty()){
                        for(String idList: gusRecordToLastModifiedIdMap.values()){
                            assignedToIdSet.add(idList);
                        }
                    }
                    System.debug(LoggingLevel.DEBUG, 'assignedToIdSet--> '+ assignedToIdSet);
                    
                    if(test.isRunningTest()){
                       gusUserIdOrContactIdToNameMap.put(gusUserList[0].ExternalId,gusUserList[0].Name__c);
                    }
                    if(!assignedToIdSet.isEmpty() || !contactIdSet.isEmpty()){
                        for(GusUser__x gusUserObj : [SELECT Id,Name__c,ExternalId,ContactId__c 
                                                     FROM GusUser__x 
                                                     WHERE ExternalId IN:assignedToIdSet
                                                     OR ContactId__c IN:contactIdSet 
                                                     WITH SECURITY_ENFORCED]){
                                                         if(assignedToIdSet.contains(gusUserObj.ExternalId)){
                                                             gusUserIdOrContactIdToNameMap.put(gusUserObj.ExternalId,gusUserObj.Name__c);
                                                         }
                                                         if(contactIdSet.contains(gusUserObj.ContactId__c)){
                                                             gusUserIdOrContactIdToNameMap.put(gusUserObj.ContactId__c,gusUserObj.Name__c);
                                                         }
                                                     }
                    }
                    
                    for(Cycle_Request_Item__c criObj : [SELECT Id,Assigned_To__c,LastModifiedById,LastModifiedDate,Cycle_Request_Status__c,Request_Assignee__c,
                                                        Period_Of_Days__c,Request_Gus__c,GUS_Status__c,Audit_Cycle_Coverage__r.Auto_Assignment_Configuration__r.Enable_Chatter_Posts__c
                                                        FROM Cycle_Request_Item__c 
                                                        WHERE Id IN: idToOldStatusMap.keySet()
                                                        WITH SECURITY_ENFORCED]){
                                                            if(criObj.GUS_Status__c != idToOldStatusMap.get(criObj.id)){
                                                                
                                                                KARL_GUS_SLA_Tracking__c slaTrackingObj  = new KARL_GUS_SLA_Tracking__c();
                                                                if(gusUserIdOrContactIdToNameMap.containsKey(criObj.Assigned_To__c))
                                                                    slaTrackingObj.Assignee__c = gusUserIdOrContactIdToNameMap.get(criObj.Assigned_To__c);
                                                                if(cycleRequestIdToGusWorkIdMap.containsKey(criObj.Id) && gusRecordToLastModifiedIdMap.containsKey(cycleRequestIdToGusWorkIdMap.get(criObj.Id))
                                                                   && gusUserIdOrContactIdToNameMap.containsKey(gusRecordToLastModifiedIdMap.get(cycleRequestIdToGusWorkIdMap.get(criObj.Id)))){
                                                                        slaTrackingObj.KARL_Changed_By__c = gusUserIdOrContactIdToNameMap.get(gusRecordToLastModifiedIdMap.get(cycleRequestIdToGusWorkIdMap.get(criObj.Id)));
                                                                   }
                                                                slaTrackingObj.Cycle_ARL_Item__c = criObj.Id;
                                                                slaTrackingObj.Date_Changed__c = criObj.LastModifiedDate.date();
                                                                slaTrackingObj.End_Status__c = idToOldStatusMap.get(criObj.id);
                                                                slaTrackingObj.Start_Status__c = criObj.GUS_Status__c;
                                                                slaTrackingObj.KARL_End_Date__c = datetime.now();
                                                                if(idToStartTimeMap.containskey(criObj.id)){
                                                                    slaTrackingObj.Start_Date__c =  idToStartTimeMap.get(criObj.id);
                                                                }
                                                                
                                                                // if(!idToOldStatusMap.isEmpty() && idToOldStatusMap.containsKey(criObj.Id))
                                                                //     slaTrackingObj.Start_Status__c = idToOldStatusMap.get(criObj.Id);
                                                                gusSlaTrackingList.add(slaTrackingObj);

                                                            }
                                                            
                                                            String gusUserName = '';
                                                            String productOwner = '';
                                                            String scrumTeamOwner = '';
                                                            String gusAssignee = '';
                                                            if(gusUserIdOrContactIdToNameMap.containsKey(criObj.Request_Assignee__c)){
                                                                gusUserName = gusUserIdOrContactIdToNameMap.get(criObj.Request_Assignee__c);
                                                            }
                                                            if(gusUserIdOrContactIdToNameMap.containsKey(criObj.Assigned_To__c)){
                                                                gusAssignee = gusUserIdOrContactIdToNameMap.get(criObj.Assigned_To__c);
                                                            }
                                                            
                                                            if(criObj.Audit_Cycle_Coverage__r.Auto_Assignment_Configuration__r.Enable_Chatter_Posts__c){
                                                                System.debug(LoggingLevel.DEBUG, 'Chatter FeedItem: Creating FeedItem');
                                                                FeedItem__x workFeedItem = new FeedItem__x();
                                                                
                                                                if(idToTeamIdMap.containsKey(criObj.id) && teamIdToProductOwnerIdMap.containsKey(idToTeamIdMap.get(criObj.id)) 
                                                                && gusUserIdOrContactIdToNameMap.containsKey(teamIdToProductOwnerIdMap.get(idToTeamIdMap.get(criObj.id)))){
                                                                    productOwner = gusUserIdOrContactIdToNameMap.get(teamIdToProductOwnerIdMap.get(idToTeamIdMap.get(criObj.id)));
                                                                }
                                                                if(idToTeamIdMap.containsKey(criObj.id) && teamIdToScrumMasterIdMap.containsKey(idToTeamIdMap.get(criObj.id)) 
                                                                && gusUserIdOrContactIdToNameMap.containsKey(teamIdToScrumMasterIdMap.get(idToTeamIdMap.get(criObj.id)))){
                                                                    scrumTeamOwner = gusUserIdOrContactIdToNameMap.get(teamIdToScrumMasterIdMap.get(idToTeamIdMap.get(criObj.id)));
                                                                }
                                                                String chatterBody = '';
                                                                if(String.isNotEmpty(gusUserName)){
                                                                    chatterBody = '@'+gusUserName;
                                                                }
                                                                if(String.isNotEmpty(productOwner)){
                                                                    chatterBody += ' @'+productOwner;
                                                                }
                                                                if(String.isNotEmpty(scrumTeamOwner)){
                                                                    chatterBody += ' @'+scrumTeamOwner;
                                                                }
                                                                
                                                                chatterBody += ' ,please coordinate to provide this Security Compliance request in accordance within '+criObj.Period_Of_Days__c+'  business days. If you have any questions, please reach out to. ';
                                                                if(String.isNotEmpty(gusAssignee)){
                                                                    chatterBody += ' @'+gusAssignee;
                                                                }
                                                                workFeedItem.Body__c = chatterBody;
                                                                workFeedItem.ParentId__c = criObj.Request_Gus__c;
                                                                workFeedItem.Status__c = 'Published';
                                                                feedItemList.add(workFeedItem);
                                                            }else{
                                                                System.debug(LoggingLevel.DEBUG, 'Chatter FeedItem: Chatter Posts Disabled in Auto-Assignment Configuration');
                                                                break;
                                                            }
                                                        }
                }
            }
            
            //Update cycle request item records.
            if(! idCycleRequestItemMap.values().isEmpty()){
                update idCycleRequestItemMap.values();
            }
            
            //Added by Mahima Aggarwal 10/29/2020
            if( !evidenceReqUpdateList.isEmpty() && Schema.sObjectType.KARL_Evidence_Request__c.isUpdateable() 
               && KARL_Evidence_Request__c.GUS_Details__c.getDescribe().isUpdateable()){
                   update evidenceReqUpdateList;
               }
            
            if(!feedItemList.isEmpty() && !Test.isRunningTest()){
                List<Database.SaveResult> saveResultList = Database.insertImmediate(feedItemList);
                System.debug(LoggingLevel.DEBUG, 'inserted feedItemList = '+feedItemList);
            }
            System.debug(LoggingLevel.DEBUG, 'feedItemList = '+feedItemList);

            System.debug(LoggingLevel.DEBUG, 'gusSlaTrackingList-- > before insert ' + gusSlaTrackingList);
            if(!gusSlaTrackingList.isEmpty() && Schema.sObjectType.KARL_GUS_SLA_Tracking__c.isCreateable() ){
                insert gusSlaTrackingList;
            } 
        }
    }
    
    /**
    * @author Mahima Aggarwal
    * @email mahima.aggarwal@mtxb2b.com
    * @description This method fetch data from GUS and if test is running then get test data for.
    */
    private List<ADM_Work_c__x> getAdmList(Set<String> externalIdList){
        if(Test.isRunningTest()){
            return admWorkList;
        }
        else{
            List<ADM_Work_c__x> admList  = [SELECT ExternalId,LastModifiedDate__c,Status_c__c,
                                            Feature_ID_c__c, DisplayUrl,Assignee_c__c,Details_c__c, Team__c 
                                            From ADM_Work_c__x WHERE ExternalId IN :externalIdList];
            return admList;
        }
    }
    
    /**
    * @author Mahima Aggarwal
    * @email mahima.aggarwal@mtxb2b.com
    * @description This method schedule the batch class after 5 minutes.
    */
    public void finish(Database.BatchableContext BC) {
        ARL_GUSCycleRequestItemSyncBatch batch = new ARL_GUSCycleRequestItemSyncBatch();
        if(!Test.isRunningTest()){
            System.scheduleBatch(batch, 'ARL_GUSCycleRequestItemSyncBatch',5);
        }
    }
    
    /**
    * @author Mahima Aggarwal
    * @email mahima.aggarwal@mtxb2b.com
    * @description Execute method for schedule job.
    */
    public void execute(SchedulableContext SC) {
        database.executebatch(new ARL_GUSCycleRequestItemSyncBatch(),200);
    }
}
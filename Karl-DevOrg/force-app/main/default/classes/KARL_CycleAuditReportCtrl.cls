/**
* @author Lakshya Arora
* @email lakshya.arora@mtxb2b.com
* @description Class used in karl_cycle_audit_report_datatable
*/
public with sharing class KARL_CycleAuditReportCtrl {
        /**
    * @author Lakshya Arora
    * @email lakshya.arora@mtxb2b.com
    * @description Method return list of cycle audit report and it's related object records based on the filters
    * @param auditCycleId -> Id of Audit_Cycle__c
    * @param reportStatusFilter -> List of status filter
    * @return List of list of cycle audit report and it's related object records
    */
    @AuraEnabled(cacheable=true)
    public static List<ReportWrapper> getCycleAuditReport(String auditCycleId, String[] reportStatusFilter, String[] reportAssigneeFilter){
        List<ReportWrapper> items = new List<ReportWrapper>(); 
        List<String> queryStatusFilterList = new List<String>();
        Map<String,String> statusFilterMap = new Map<String,String>();
        statusFilterMap.put('all', 'All');
        statusFilterMap.put('pending','Pending');
        statusFilterMap.put('readyToSign', 'Ready to Sign');
        statusFilterMap.put('inProgress', 'In Progress with Docusign');
        statusFilterMap.put('docusignComplete', 'DocuSign Completed');
        statusFilterMap.put('auditorsFinalizing', 'xFinalization');
        statusFilterMap.put('finalReview', 'Final Review');
        statusFilterMap.put('published','Published');
     
        // Step 1: Determine where we are coming from, to generate the links appropriately for Community vs. Salesforce
        Network karlNetwork;
        String auditorloginurl = '';
        String auditorcommunityHomeUrl = '';
        Integer index;
        if(!Test.isRunningTest()){
            karlNetwork = [SELECT Id FROM Network WHERE Name ='KARL Community' WITH SECURITY_ENFORCED];
            auditorloginurl = Network.getLoginUrl(karlNetwork.Id);
            index = auditorloginurl.lastIndexOf('/login');
            auditorcommunityHomeUrl = auditorloginurl.substring(0, index + 1);
        }

        String baseUrl = '';
        String baseRecordURL = '';
        String baseUrlSuffix = '';
        // Check if we're on the KARL Community or not
        if (Site.getSiteId() != null) {
            baseUrl = auditorcommunityHomeUrl; // we're on the community
            baseRecordURL = baseUrl + 'detail/';
        } else {
            baseUrl = URL.getSalesforceBaseUrl().toExternalForm();
            baseRecordURL = baseUrl + '/one/one.app?#/sObject/'; // force rendering in Lightning instead of Classic
            baseUrlSuffix = '/view'; // Required for Lightning record view
        }

        String queryStatusFilter = '';
        for(String filterValue : reportStatusFilter){
            if(statusFilterMap.containsKey(filterValue)){
                String statusValue = statusFilterMap.get(filterValue);
                if(statusValue == 'All'){
                    queryStatusFilter = '';
                    break;
                }else if(statusValue == 'xFinalization'){
                    queryStatusFilterList.add('Request Issuance');
                    queryStatusFilterList.add('Issued');
                }else{
                    queryStatusFilterList.add(statusValue);
                }
            }
        }
        if(!queryStatusFilterList.isEmpty()){
            queryStatusFilter = ' AND Status__c IN: queryStatusFilterList ';
        }
        String queryAssignedFilter = '';
        for(String assignedValue : reportAssigneeFilter){
            if(assignedValue != 'allReports'){
                String currentUserEmail = userInfo.getUserEmail();
                queryAssignedFilter = ' AND (Audit_Reports_Master_Data__r.KARL_Audit_Scope__r.KARL_BU_Lead__r.Email =: currentUserEmail OR ' +
                                    'Audit_Reports_Master_Data__r.KARL_Audit_Scope__r.KARL_GRC_LT__r.Email =: currentUserEmail) ';
            }
        }
        
        // Build Map of Qualification Scopes
        Map<String, Set<String>> qualificationScopes = new Map<String, Set<String>>();
        for(KARL_Issue_Tracker__c qualificationIssue : [SELECT Id, Name, KARL_KAI_Impacted_Scope__c, KARL_High_Impact__c, KARL_Impacted_Frameworks__c
                                                            FROM KARL_Issue_Tracker__c 
                                                            WHERE KARL_Audit_Cycle__c =: auditCycleId AND KARL_High_Impact__c = true
                                                            WITH SECURITY_ENFORCED]){
            // Because Impacted_Frameworks is a multi-select, we need to add as a list
            if(!qualificationScopes.containsKey(qualificationIssue.KARL_KAI_Impacted_Scope__c)){
                qualificationScopes.put(qualificationIssue.KARL_KAI_Impacted_Scope__c, New Set<String>());
            }
            String[] scopePicklistValues = qualificationIssue.KARL_Impacted_Frameworks__c.split(';');
            for(String scopePicklistValue : scopePicklistValues){
                qualificationScopes.get(qualificationIssue.KARL_KAI_Impacted_Scope__c).add(scopePicklistValue);      
            }                                    
        }

        // Build list of sub-scopes (e.g. inclusion reports)
        Map<String, List<String>> scopesList = new Map<String, List<String>>();
        for(KARL_Audit_Scope_Reports__c subScopes : [SELECT Id, KARL_Audit_Scope__r.KARL_Scope__c, KARL_Report_Type__c, 
                                                                (SELECT KARL_Parent_Audit_Scope__c, KARL_Parent_Audit_Scope__r.KARL_Scope__c 
                                                                FROM KARL_Audit_Scope_Report__r)
                                                            FROM KARL_Audit_Scope_Reports__c
                                                            WITH SECURITY_ENFORCED]){
            if(!scopesList.containsKey(subScopes.Id)){
                scopesList.put(subScopes.KARL_Audit_Scope__r.KARL_Scope__c, New List<String>());
            }
            for(KARL_Audit_Scope_In_Report__c subReport : subScopes.KARL_Audit_Scope_Report__r){
                scopesList.get(subScopes.KARL_Audit_Scope__r.KARL_Scope__c).add(subReport.KARL_Parent_Audit_Scope__r.KARL_Scope__c);

                // Also add the scope to Qualification Scopes Set<>() if the parent is included
                if(qualificationScopes.containsKey(subScopes.KARL_Audit_Scope__r.KARL_Scope__c) 
                && qualificationScopes.get(subScopes.KARL_Audit_Scope__r.KARL_Scope__c).contains(subScopes.KARL_Report_Type__c)){
                    if(!qualificationScopes.containsKey(subReport.KARL_Parent_Audit_Scope__r.KARL_Scope__c)){
                        qualificationScopes.put(subReport.KARL_Parent_Audit_Scope__r.KARL_Scope__c, New Set<String>());
                    }
                    qualificationScopes.get(subReport.KARL_Parent_Audit_Scope__r.KARL_Scope__c).add(subScopes.KARL_Report_Type__c);   
                }
            }
        }
        System.debug(LoggingLevel.DEBUG, 'Qualified Scopes >> ' + qualificationScopes);

        // Step 3: Query all KARL_Cycle_Audit_Report_Items__c  (filtered by  audit Cycle and report status)
        for (KARL_Cycle_Audit_Report_Items__c  cycleAuditReportObj : Database.query('SELECT Id, Name, Status__c,Scope__c,Area_of_Compliance__c,Audit_Cycle__c,Audit_Cycle__r.Name,'+
                                                                'Report_Issuance_Target_Date__c,Management_Responses_Approved__c,Auditor_Readiness_Confirmation__c,Audit_Report_Final_Draft_Confirmed__c,Approved_by_Legal__c, '+
                                                                'Audit_Reports_Master_Data__c, BU_Lead__c, GRC_LT__c, VP_Approved__c '+
                                                                'FROM KARL_Cycle_Audit_Report_Items__c  '+
                                                                'WHERE Audit_Cycle__c =: auditCycleId ' + queryStatusFilter + queryAssignedFilter + 
                                                                'WITH SECURITY_ENFORCED')){
            
            ReportWrapper rw    = new ReportWrapper();
            // Primary Information
            rw.Id                   = cycleAuditReportObj.Id;
            rw.reportName           = cycleAuditReportObj.Name;
            rw.reportRecordLink     = baseRecordURL + cycleAuditReportObj.Id + baseUrlSuffix; 
            rw.scope                = cycleAuditReportObj.Scope__c;
            //rw.area               = cycleAuditReportObj.Area_of_Compliance__c;
            rw.reportStatus         = cycleAuditReportObj.Status__c;
            if((rw.reportStatus == 'Ready to Sign' || rw.reportStatus == 'Published') && cycleAuditReportObj.VP_Approved__c == true){
                rw.reportLabel          = 'slds-badge slds-theme_success';
                rw.reportStatusIcon     = 'utility:success';
                //rw.reportStatus         = 'Ready to Sign: Approved';
            }else if((rw.reportStatus == 'Ready to Sign' || rw.reportStatus == 'Published') && cycleAuditReportObj.VP_Approved__c == false){
                rw.reportLabel          = 'slds-badge slds-theme_warning';
                rw.reportStatusIcon     = 'utility:warning';
                //rw.reportStatus         = 'Ready: Pending Approval';
            }
            /*else if(rw.reportStatus == 'Pending' || rw.reportStatus == 'Final Review'){
                rw.reportLabel          = 'slds-badge slds-theme_warning';
                rw.reportStatusIcon     = 'utility:warning';
            }*/
            rw.auditCycleName       = cycleAuditReportObj.Audit_Cycle__r.Name;
            //rw.auditCycleLink     = baseRecordURL + cycleAuditReportObj.Audit_Cycle__c + baseUrlSuffix;
            rw.targetDate           = cycleAuditReportObj.Report_Issuance_Target_Date__c;
            rw.responseApprovedText     = cycleAuditReportObj.Management_Responses_Approved__c == true ? 'Approved' : 'Pending';
            rw.responseApprovedIcon     = cycleAuditReportObj.Management_Responses_Approved__c == true ? 'utility:success' : 'utility:warning';
            rw.readinessConfirmationText    = cycleAuditReportObj.Auditor_Readiness_Confirmation__c == true ? 'Auditors Ready' : 'Pending Auditors';
            rw.readinessConfirmationIcon    = cycleAuditReportObj.Auditor_Readiness_Confirmation__c == true ? 'utility:success' : 'utility:warning';
            rw.finalDraftText               = cycleAuditReportObj.Audit_Report_Final_Draft_Confirmed__c == true ? 'Final Draft' : 'Drafting';
            rw.finalDraftIcon               = cycleAuditReportObj.Audit_Report_Final_Draft_Confirmed__c == true ? 'utility:success' : 'utility:warning';
            // Contacts
            rw.contactBuLead        = cycleAuditReportObj.BU_Lead__c;
            rw.contactGrcLt         = cycleAuditReportObj.GRC_LT__c;

            // Issues inclusion
            if(qualificationScopes.containsKey(cycleAuditReportObj.Scope__c) 
            && qualificationScopes.get(cycleAuditReportObj.Scope__c).contains(cycleAuditReportObj.Area_of_Compliance__c)){
                rw.qualifiedReport = 'Yes';
                rw.qualifiedLabel  = 'slds-badge slds-theme_error';
            }else{
                rw.qualifiedReport = 'No';
                rw.qualifiedLabel  = 'slds-badge slds-theme_success';
            }
                
            // Add to list for reporting
            items.add(rw);
        }
        return items;
    }

    public class ReportWrapper{
        @AuraEnabled public String Id;
        @AuraEnabled public String reportName;
        @AuraEnabled public String reportRecordLink;
        @AuraEnabled public String scope;
        //@AuraEnabled public String area;
        @AuraEnabled public String reportStatus;
        @AuraEnabled public String auditCycleName;
        //@AuraEnabled public String auditCycleLink;
        @AuraEnabled public String reportLabel;
        @AuraEnabled public String reportStatusIcon;
        @AuraEnabled public String contactBuLead;
        @AuraEnabled public String contactGrcLt;
        @AuraEnabled public String qualifiedReport;
        @AuraEnabled public String qualifiedLabel;
        @AuraEnabled public String responseApprovedText;
        @AuraEnabled public String responseApprovedIcon;
        @AuraEnabled public String readinessConfirmationText;
        @AuraEnabled public String readinessConfirmationIcon;
        @AuraEnabled public String finalDraftText;
        @AuraEnabled public String finalDraftIcon;
        @AuraEnabled public Date targetDate;
    }
}
/**
* @author Lakshya Arora
* @email lakshya.arora@mtxb2b.com
* @description Test class for KARL_CycleAuditReportCtrl
*/
@isTest
public class KARL_CycleAuditReportCtrlTest {
    /**
* @author Lakshya Arora
* @email lakshya.arora@mtxb2b.com
* @desc method to create the test data.
*/   
    @testSetup
    private static void makeData(){
        User opsAdminUser = ARL_TestDataFactory.createOpsAdmin();
        System.runAs(opsAdminUser){
            Audit_Cycle__c auditCycle = ARL_TestDataFactory.createAuditCycle();
            insert auditCycle;
            KARL_Audit_Scope_Master_Data__c auditScope = ARL_TestDataFactory.createAuditScope();
            insert auditScope;
            KARL_Audit_Firm__c auditFirm = ARL_TestDataFactory.createAuditFirm('Test Audit Firm');
            insert auditFirm;
            KARL_Audit_Scope_Reports__c auditScopeReport = ARL_TestDataFactory.createAuditScopeReport(auditScope.Id,auditFirm.Id);
            insert auditScopeReport;
            KARL_Cycle_Audit_Report_Items__c  cycleAuditReportObj = new KARL_Cycle_Audit_Report_Items__c ();
            cycleAuditReportObj.Audit_Reports_Master_Data__c = auditScopeReport.Id;
            cycleAuditReportObj.Audit_Cycle__c = auditCycle.Id;
            cycleAuditReportObj.Status__c = 'Pending';
            cycleAuditReportObj.Auditor_Readiness_Confirmation__c = true;
            cycleAuditReportObj.Management_Responses_Approved__c = true;
            cycleAuditReportObj.Audit_Report_Final_Draft_Confirmed__c = true;
            cycleAuditReportObj.Approved_by_Legal__c = true;
            cycleAuditReportObj.Audit_Letter_Temps_Formatted__c = true;
            cycleAuditReportObj.Basis_of_Assertion_BOA__c = true;
            insert cycleAuditReportObj;
        }
    }
    
    /**
* @author Lakshya Arora
* @email lakshya.arora@mtxb2b.com
* @desc method to test the getCycleAuditReport functionality.
*/
    @isTest
    private static void getCycleAuditReportTest(){
        List<User> opsAdminUserList = ARL_TestDataFactory.fetchUser('abc@b.com.d');
        if(!opsAdminUserList.isEmpty()){
            System.runAs(opsAdminUserList[0]){
                List<Audit_Cycle__c> auditCycleList = new List<Audit_Cycle__c>([SELECT Id FROM Audit_Cycle__c]);
                String[] statusArray = new String[]{'pending'};
                    String [] reportassigneeArray = new String[]{'allReports'};
                    Test.startTest();
                System.assert(!KARL_CycleAuditReportCtrl.getCycleAuditReport(auditCycleList[0].Id,statusArray,reportassigneeArray).isEmpty());
                statusArray[0] = 'all';
                System.assert(!KARL_CycleAuditReportCtrl.getCycleAuditReport(auditCycleList[0].Id,statusArray,reportassigneeArray).isEmpty());
                statusArray[0] = 'inProgress';
                System.assert(KARL_CycleAuditReportCtrl.getCycleAuditReport(auditCycleList[0].Id,statusArray,reportassigneeArray).isEmpty());
                reportassigneeArray[0] = '';
                KARL_CycleAuditReportCtrl.getCycleAuditReport(auditCycleList[0].Id,statusArray,reportassigneeArray);
                KARL_CycleAuditReportCtrl.getCycleAuditReport(auditCycleList[0].Id,new String[]{'auditorsFinalizing'},reportassigneeArray);
               // System.assert(KARL_CycleAuditReportCtrl.getCycleAuditReport(auditCycleList[0].Id,statusArray,reportassigneeArray).isEmpty());
                Test.stopTest();
            }
        }
    }
}
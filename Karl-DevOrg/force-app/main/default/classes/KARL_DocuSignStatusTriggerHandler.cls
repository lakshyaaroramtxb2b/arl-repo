public with sharing class KARL_DocuSignStatusTriggerHandler {
    
      /**
     * @author Swarnima Singh Mandhata
     * @email swarnima.singh@mtxb2b.com
     * @description This method get triggered from Docusign Status trigger.
    */
    public static void run(){
      
        //Before Update
        if( Trigger.isBefore && Trigger.isUpdate ){
           updateCycleRequestItemStatus((List<dfsle__EnvelopeStatus__c >)Trigger.new , (Map<Id, dfsle__EnvelopeStatus__c>)Trigger.oldMap);
        }
        //Before Insert
        else if( Trigger.isBefore && Trigger.isInsert ){
            updateCycleRequestItemStatus((List<dfsle__EnvelopeStatus__c >)Trigger.new , null);
         
        }
    }
    public static void updateCycleRequestItemStatus(List<dfsle__EnvelopeStatus__c > newList, Map<Id,dfsle__EnvelopeStatus__c> oldMap){
        System.debug('oldMap--> '+ oldMap);
		List<KARL_Cycle_Audit_Report_Items__c> updateCycleRequestList = new List<KARL_Cycle_Audit_Report_Items__c>();
        Map<String,String> sourceIdToStatusMap = new Map<String,String>();
        List<KARL_Cycle_Audit_Report_Items__c> cycleAuditReportList = new List<KARL_Cycle_Audit_Report_Items__c>();
        for(dfsle__EnvelopeStatus__c docStatus: newList){
            if((oldMap==null && docStatus.dfsle__Status__c == 'Sent') 
               ||(oldmap != null && docStatus.dfsle__Status__c == 'Completed' && oldMap.get(docStatus.id).dfsle__Status__c != docStatus.dfsle__Status__c)){

                if(!sourceIdToStatusMap.containsKey(docStatus.dfsle__SourceId__c)){
                    sourceIdToStatusMap.put(docStatus.dfsle__SourceId__c, docStatus.dfsle__Status__c);
                }
            }
        }
        system.debug('sourceIdToStatusMap--> '+ sourceIdToStatusMap);
        if(!sourceIdToStatusMap.isEmpty()){
            cycleAuditReportList= [SELECT id,Status__c FROM KARL_Cycle_Audit_Report_Items__c WHERE Id IN:sourceIdToStatusMap.keySet()];
        }
        
        if(!cycleAuditReportList.isEmpty()){
            for(KARL_Cycle_Audit_Report_Items__c cycleAuditReport: cycleAuditReportList){
                if(sourceIdToStatusMap.containsKey(cycleAuditReport.Id)){
                    if(sourceIdToStatusMap.get(cycleAuditReport.Id) == 'Sent'){
                        cycleAuditReport.Status__c = 'In Progress with Docusign';
                        updateCycleRequestList.add(cycleAuditReport);
                    }else if(sourceIdToStatusMap.get(cycleAuditReport.Id) == 'Completed'){
                        cycleAuditReport.Status__c = 'DocuSign Completed';
                        updateCycleRequestList.add(cycleAuditReport);
                    }
                }
            }
        }
        if(!updateCycleRequestList.isEmpty()){
            System.debug('updateCycleRequestList--> '+ updateCycleRequestList);
            update updateCycleRequestList;
        }
        
    }


}
public class CDI_SyncGUSServices implements Schedulable {
   public void execute(SchedulableContext sc){
        List<Gus_Service_c__x> extGusServiceList=[select Name__c,IsDeleted__c,Capabilities_c__c,ADM_Cloud_c__c,CreatedDate__c,Description_c__c,Service_Id_c__c,Service_Owner_c__c,Service_Status_c__c,Team_c__c,Team_Contact_c__c from Gus_Service_c__x where IsDeleted__c=false];
        List<CDI_Service__c> cdiServiceList=new List<CDI_Service__c>();
        for (Gus_Service_c__x extService:extGusServiceList){
            CDI_Service__c cdiService=new CDI_Service__c();
            cdiService.Name=extService.Name__c;
            cdiService.IsDeleted__c=extService.IsDeleted__c;
            cdiService.Capability__c=extService.Capabilities_c__c;
            //cdiService.CreatedDate=extService.CreatedDate__c;
            cdiService.Service_Description__c=extService.Description_c__c;
            cdiService.Source_Service_Code__c=extService.Service_Id_c__c;
            cdiService.Source_System_Name__c='Service Category';
            cdiService.Status__c='Active';
            String Name=cdiService.Name+cdiService.Source_System_Name__c+cdiService.Source_Service_Code__c;
            cdiServiceList.add(cdiService);
        }
        Database.upsert (cdiServiceList,false); 
    }

}
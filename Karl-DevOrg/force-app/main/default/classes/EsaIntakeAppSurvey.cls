public class EsaIntakeAppSurvey {
    @InvocableMethod
    public static void EsaIntakeAppSurvey() {
        
        //time variable to retrieve entities and invitations
        datetime dateToSend = date.today().addDays(-180);
        
        //get community id for intakeapp
        list <network> communityIntake = [SELECT id, name from network WHERE name ='Intake App' limit 1];
    
        //get survey id for our survey, by name
        list <Survey> intakeSurvey = [SELECT id, DeveloperName from Survey WHERE name = 'Intake app - Entity Survey'];
        
        //Get all active entities created more than 6 months ago
        list <ESA_Entity__c> entities = [SELECT id, OwnerId, Active__c FROM ESA_Entity__c WHERE Active__c = true and CreatedDate <: dateToSend];
		
        //get all owners, no duplicates
        set <id> owners = new set <id>();
        
        for (ESA_Entity__c entity : entities) {
            owners.add(entity.OwnerId);
        }
        system.debug(owners);
        //get recordTypeID for salesforce employee contact
        List <RecordType> crecordType = [select id from RecordType where DeveloperName = 'Salesforce_Employee' LIMIT 1];
        
        //get the users and emails
        list <User> users = [SELECT id, ContactId, Email FROM user WHERE id iN: owners];
        list <String> userEmails = new list <String> ();
        
        for (User userC: users){
            userEmails.add(userC.email);     
        }
        system.debug(userEmails);
        //get the actual unique contacts
        list <contact> contacts = [SELECT id, email FROM contact WHERE RecordTypeId in: crecordType and email IN:userEmails];
        system.debug(contacts);
        //create set and map that will help with contacts id and id and email
        set <id> contactsUnique = new set <id>();
        Map<Id, String> contactsEmail = new Map<Id, String> ();
        system.debug(contactsUnique);
        for (Contact contactI: contacts){
            contactsUnique.add(contactI.id);
            contactsEmail.put(contactI.id,contactI.email);
        }
        
        //get all our surveys sent past 6 months
        
        list <SurveyInvitation> invitations = [SELECT Name,ParticipantId FROM SurveyInvitation WHERE Name like 'Intake App survey%' and CreatedDate >: dateToSend];
        
        
        //actual contacts with our surveys
        set <id> participantsUnique = new set <id>();
        for (SurveyInvitation si: invitations){
            participantsUnique.add(si.ParticipantId);
        }
        
        //create a list of contacts to send survey
        list <string> contactToSend = new list <string>();
        
        //check if the invitations contacts are or not in our entities contacts
        for (id cU: contactsUnique){
            if (!participantsUnique.contains(cU)){
                contactToSend.add(cU);
            }
            
        }
        system.debug('hola');
        //create our invitations
        list <SurveyInvitation> invitationsToSend = new list <SurveyInvitation>();
        list <SurveySubject> surveySubjectToSend = new list <SurveySubject>();
     	system.debug(contactToSend);
        for (string cS: contactToSend){
            
            SurveyInvitation SI = new SurveyInvitation ();
            SI.CommunityId = communityIntake[0].id;
            SI.Name = 'Intake App survey' + ' ' + System.Now().format();
            SI.Participant_Email__c = contactsEmail.get(cS);
            SI.ParticipantId = cS;
            SI.SurveyId = intakeSurvey[0].id;
            SI.Survey_Developer_Name__c = intakeSurvey[0].DeveloperName;
            invitationsToSend.add(SI);
                
        }
        insert invitationsToSend;
        
        
        Map<String, Object> params = new Map<String, Object>();
        
        
        for (SurveyInvitation IS:invitationsToSend ){
            SurveySubject SB = new SurveySubject();
            SB.Name = 'Intake Survey';
            SB.ParentId = IS.id;
            SB.SubjectId = IS.ParticipantId;
            surveySubjectToSend.add(SB);
            params.put('Invitation',IS.id);
            Flow.Interview.Send_Survey_Email_IntakeApp Send_Survey_Email_IntakeApp = new Flow.Interview.Send_Survey_Email_IntakeApp(params);
			Send_Survey_Email_IntakeApp.start();
   
        }
        insert surveySubjectToSend;
       
        
}

}
/**
 * Trust Maturity Model
 * Feb 2016
 * Written By: jchen@salesforce.com
 * 
 * Internal tool used to simplify the user management and provisioning of Trust Maturity Model users
 * 
**/

public class TM_UserManagementToolController {
  
    private static Profile tmmProfile = [SELECT Id FROM Profile WHERE Name = 'Trust Maturity External' LIMIT 1];
    private static Account sfdcAcct = [SELECT Id FROM Account WHERE Name = 'salesforce.com' LIMIT 1];
    private static RecordType sfdcEmployeeContactRT = [SELECT Id FROM RecordType WHERE sObjectType = 'Contact' AND DeveloperName = 'Salesforce_Employee' ORDER BY NamespacePrefix DESC LIMIT 1];        
    private static RecordType accountBusinessUnitRT = [SELECT Id FROM RecordType WHERE sObjectType = 'Account' AND DeveloperName = 'Salesforce_Business_Unit' ORDER BY NamespacePrefix DESC LIMIT 1];
    private static EmailTemplate userProvisioningCompletedEmailTemplate = [SELECT Id, Name FROM EmailTemplate WHERE DeveloperName = 'TM_User_Provisoning_Completed' LIMIT 1];
    
    private List<PermissionSet> tmPermsetsFromOrg;
    private List<Group> tmPublicGroupsFromOrg;
    private List<Account> tmBUsFromOrg;    // does not include AllBUs entry which was manually added
    private static String fakeAllBUsEntryFromOrg = 'AllBUs';
    private Map<String, Id> publicGroupMap = new Map<String, Id>();
    private Map<Id, String> businessUnitMap = new Map<Id, String>();
    
    public Map<Id, User> userEmailResultsMap {get; private set;}
    public Map<Id, List<PermissionSetAssignment>> userPermsets {get; private set;}
    public Map<Id, List<GroupMember>> userPublicGroups {get; private set;}
    //public Boolean showFieldEmptyError {get; set;}    
    public Boolean showUserSearchResults {get; set;}    
    public Boolean showNoUserSearchResults {get; set;}
    public Boolean showUserCreationResults {get; set;}
    public Boolean showUserCreationResultsError {get; set;}
    public List<String> userCreationResultsErrorDetail {get; set;}
    public Boolean showUserProvisioningResults {get; set;}
    public Boolean showUserProvisioningResultsError {get; set;}
    public List<String> userProvisioningResultsErrorDetail {get; set;}
    public Boolean sendProvisioningNotificationEmailCheckbox {get; set;}
    
    public String userEmailSearch {get; set;}
    public String getUserEmailSearch() {
        return userEmailSearch;
    }
    public void setUserEmailSearch() {
        userEmailSearch = String.escapeSingleQuotes(userEmailSearch);
    }
    
    public String newUserAlias {get; set;}
    public String getNewUserAlias() {
        return newUserAlias;
    }
    public void setNewUserAlias() {
        newUserAlias = String.escapeSingleQuotes(newUserAlias);
    }
    
 /**   public String newUserEmail {get; set;}
    public String getNewUserEmail() {
        return newUserEmail;
    }
    public void setNewUserEmail() {
        newUserEmail = String.escapeSingleQuotes(newUserEmail);
    }
    public String newUserUsername {get; set;}
    public String getNewUserUsername() {
        return newUserUsername;
    }
    public void setNewUserUsername() {
        newUserUsername = String.escapeSingleQuotes(newUserUsername);
    }
    
    **/
    
    public String usernameToBeProvisioned {get; set;}
    public String getUsernameToBeProvisioned() {
        return usernameToBeProvisioned;
    }
    public void setUsernameToBeProvisioned() {
        //usernameToBeProvisioned = String.escapeSingleQuotes(usernameToBeProvisioned) + '@salesforce.com.security';
        usernameToBeProvisioned = String.escapeSingleQuotes(usernameToBeProvisioned);
    }
    
    // tm permset variables
    public List<SelectOption> tmPermsetsAvailable;
    public List<SelectOption> getTmPermsetsAvailable() {
        return tmPermsetsAvailable;
    }
    public void setTmPermsetsAvailable() {
        tmPermsetsAvailable = getTmPermsetOptions();
    }
    
    public String[] tmPermsetsSelected;
    public String[] getTmPermsetsSelected() {
        return tmPermsetsSelected;
    }
    public void setTmPermsetsSelected(String[] tmPermsetsSelected) {
        this.tmPermsetsSelected = tmPermsetsSelected;
    }
    
    // tm public group variables
    public List<SelectOption> tmPublicGroupsAvailable;
    public List<SelectOption> getTmPublicGroupsAvailable() {
        return tmPublicGroupsAvailable;
    }
    public void setTmPublicGroupsAvailable() {
        tmPublicGroupsAvailable = getTmPublicGroupOptions();
    }
    
    public String[] tmPublicGroupsSelected;
    public String[] getTmPublicGroupsSelected() {
        return tmPublicGroupsSelected;
    }
    public void setTmPublicGroupsSelected(String[] tmPublicGroupsSelected) {
        this.tmPublicGroupsSelected = tmPublicGroupsSelected;
    }
    
    // tm BU variables
    public List<SelectOption> tmBUsAvailable;
    public List<SelectOption> getTmBUsAvailable() {
        return tmBUsAvailable;
    }
    public void setTmBUsAvailable() {
        tmBUsAvailable = getTmBUOptions();
    }
    
    public String[] tmBUsSelected;
    public String[] getTmBUsSelected() {
        return tmBUsSelected;
    }
    public void setTmBUsSelected(String[] tmBUsSelected) {
        this.tmBUsSelected = tmBUsSelected;
    }
    
    boolean isRunningInASandbox;
    public static Boolean runningInASandbox() {
        return [SELECT Id, IsSandbox, InstanceName FROM Organization LIMIT 1].IsSandbox;
    }

    /*
     * Controller, woo
     */
    public TM_UserManagementToolController() {
        isRunningInASandbox = runningInASandbox();
        
        setTmPermsetsAvailable();
        setTmPublicGroupsAvailable();
        setTmBUsAvailable();
        
        //showFieldEmptyError = false;
        
        showUserSearchResults = false;
        showNoUserSearchResults = false;
        
        showUserCreationResults = false;
        showUserCreationResultsError = false;
        userCreationResultsErrorDetail = new List<String>();
        
        showUserProvisioningResults = false;
        showUserProvisioningResultsError = false;
        userProvisioningResultsErrorDetail = new List<String>();
        sendProvisioningNotificationEmailCheckbox = false;
    }
    
    
    /*
     * ====== USER INQUIRY ==================================================================================================
     */
    
    
    /*
     * Called by the 'User Inquiry' section of the corresponding VF Page
     * - Takes in a user's email of the form bob.smith@salesforce.com
     * - Returns a list of users and their TM-related properties
     */
    public PageReference doUserSearch() {        
        // make a list of users that match the email provided
        //userEmailResultsMap = new Map<Id, User>([SELECT Id, Name, Email, ProfileId, Username, IsActive FROM User WHERE Email = :userEmailSearch AND UserType = 'Standard']);
        userEmailResultsMap = new Map<Id, User>([SELECT Id, Name, Email, ProfileId, Username, IsActive FROM User WHERE (Email = :userEmailSearch OR Username = :userEmailSearch) AND UserType = 'Standard']);
        
        // if there are users returned by the search...
        if (!userEmailResultsMap.isEmpty()) {
            // make a list of the TM permsets each user has
            userPermsets = new Map<Id, List<PermissionSetAssignment>>();  
            for (PermissionSetAssignment psa: [SELECT Id, PermissionSet.Label, AssigneeId 
                                               FROM PermissionSetAssignment 
                                               WHERE (PermissionSet.Name LIKE 'TM_%') 
                                               AND AssigneeId IN :userEmailResultsMap.keySet()]) {
                                                   
                                                   // if the user already has a permset, add it to the existing list
                                                   if (userPermsets.containsKey(psa.AssigneeId)) {
                                                       List<PermissionSetAssignment> newList = new List<PermissionSetAssignment>(userPermsets.get(psa.AssigneeId));
                                                       newList.add(psa);
                                                       userPermsets.remove(psa.AssigneeId);
                                                       userPermsets.put(psa.AssigneeId, newList);
                                                       // if the user doesn't already have a permset, just make a new entry
                                                   } else {
                                                       userPermsets.put(psa.AssigneeId, new List<PermissionSetAssignment>{psa});
                                                   }            
                                                   
                                               }
            
            // if user resulting from email search doesn't have TM permsets, 
            // we need to add them with blank values in the map so there won't be problems with rendering it 
            // (aka looking for a key that doesn't exist in the permset map)
            for (String userKey: userEmailResultsMap.keySet()) {
                if (!userPermsets.containsKey(userKey)) {
                    userPermsets.put(userKey, new List<PermissionSetAssignment>());
                }            
            }
            
            // make a list of the TM Public Groups each user has
            userPublicGroups = new Map<Id, List<GroupMember>>();
            for (GroupMember gm : [SELECT Id, UserOrGroupId, Group.Id, Group.Name, Group.Type, Group.DeveloperName
                                   FROM GroupMember 
                                   WHERE (Group.DeveloperName LIKE 'TM_%' OR Group.DeveloperName LIKE 'TMM_%')
                                   AND UserOrGroupId IN :userEmailResultsMap.keySet()]) {
                                       
                                       //System.debug('pubGroup userOrGroupId ' + gm.UserOrGroupId);
                                       
                                       // if the user already has a public group, add it to the existing list
                                       if (userPublicGroups.containsKey(gm.UserOrGroupId)) {
                                           List<GroupMember> newList = new List<GroupMember>(userPublicGroups.get(gm.UserOrGroupId));
                                           newList.add(gm);
                                           userPublicGroups.remove(gm.UserOrGroupId);
                                           userPublicGroups.put(gm.UserOrGroupId, newList);
                                           // if the user doesn't already have a public group, just make a new entry
                                       } else {
                                           userPublicGroups.put(gm.UserOrGroupId, new List<GroupMember>{gm});
                                       }    
                                   }
            
            // if user resulting from email search doesn't have a TM related public group, 
            // we need to add them with blank values in the map so there won't be problems with rendering it 
            // (aka looking for a key that doesn't exist in the map)
            for (String userKey: userEmailResultsMap.keySet()) {
                if (!userPublicGroups.containsKey(userKey)) {
                    userPublicGroups.put(userKey, new List<GroupMember>());
                }   
            }
            
            // there are search results, so let's display them. 
            // set both variables again because the user may have clicked the search button multiple times, and the previous values were
            showUserSearchResults = true;
            showNoUserSearchResults = false;
            
            // otherwise if there are no users returned by the search...
        } else {
            showUserSearchResults = false;
            showNoUserSearchResults = true;
        }
            
        return null;
    }


    /*
     * ====== USER CREATION ==================================================================================================
     */
    
    
    /*
    * Called by the 'User Creation' section of the corresponding VF Page
    * - Takes in a security org username of the form bob.smith@salesforce.com.security
    * - Creates a user with the Trust Maturity External profile
    */
    public PageReference doUserCreation() {  
        
/**        // email needs to be @salesforce.com email
        if (newUserEmail.substringAfter('@') != 'salesforce.com') {
            userCreationResultsErrorDetail.add('Email must be a @salesforce.com email');
        }
        
        // username needs to end with @salesforce.com.security
        if ((newUserUsername.substringAfter('@') != 'salesforce.com.security') && (newUserUsername.substringAfter('@') != 'salesforce.com.security.fllsndbx')) {
            userCreationResultsErrorDetail.add('Username must end with @salesforce.com.security');
        }
**/     

        
        String username;
        if (isRunningInASandbox) {
            username = newUserAlias + '@salesforce.com.security.fllsndbx';
        } else {
            username = newUserAlias + '@salesforce.com.security';
        }
        String email = newUserAlias + '@salesforce.com';
        
        // username must be unique (requirement across all salesforce)
        // user email must be unique (required for sec org, federation ID purposes)
        integer existingUserCount = [SELECT count() FROM User WHERE (Username = :username OR Email = :email) AND UserType = 'Standard'];
        if (existingUserCount != 0) {
            userCreationResultsErrorDetail.add('Username or email is already in use');
        }   
        
        // if there are no validation errors so far, try to create the user
        if (userCreationResultsErrorDetail.isEmpty()) {
            User newUser = createNewTMUser(email, username);
        }
        
        // display results
        if (userCreationResultsErrorDetail.isEmpty()) {
            // if we still didn't hit any errors, display the success message
            showUserCreationResults = true;  
            showUserCreationResultsError = false;
            sendInternalTMNotificationEmail(email, username, true);

        } else {
            // otherwise show the error(s)
            showUserCreationResults = false;
            showUserCreationResultsError = true;
        }
        
        return null;   
    }


    
    /*
     * ====== USER PROVISIONING ==================================================================================================
     */
    
    
    /*
    * Called by the 'User Provisioning' section of the corresponding VF Page
    * - Takes in a security org username of the form bob.smith@salesforce.com.security, as well as a list of TM-related permsets and TM-related Public Groups
    * - Provisions the user with the specified permsets and public groups
    */
    public PageReference doUserProvisioning() {  
        
        // check if user already exists in the org. usernames are globally unique
        User userToBeProvisioned = null;
        try {
            userToBeProvisioned = [SELECT Id, Name, Email, ProfileId, Username, IsActive FROM User WHERE Username = :usernameToBeProvisioned AND UserType = 'Standard' AND IsActive = true];
        } catch (Exception e) {
            //user doesn't exist, throw an error
            userProvisioningResultsErrorDetail.add('Error: No active user exists with this username. Please verify that this user is provisionable.'); 
        }
        
        //System.debug('do user provisioning, passed validation, attempt provsioning');
        
        // if user exists, attempt the provisioning
        if (userProvisioningResultsErrorDetail.isEmpty()) {
            // delete the User's existing TM-related Permsets and Public Groups
            deleteUserPermsetsPublicGroups(userToBeProvisioned);
            
            // add the selected TM-related Permsets and Public Groups to the User
            addUserPermsetsPublicGroups(userToBeProvisioned);
            
        }
                
        // display results
        if (userProvisioningResultsErrorDetail.isEmpty()) {
            // if we still didn't hit any errors, display the success message
            showUserProvisioningResults = true;  
            showUserProvisioningResultsError = false;   
            sendInternalTMNotificationEmail(usertoBeProvisioned.Email, usertoBeProvisioned.Username, false);
            
            // send the notification email to the user
            if (sendProvisioningNotificationEmailCheckbox) {
                sendProvisioningNotificationEmail(usertoBeProvisioned);
            }
            

        } else {
            // otherwise show the error(s)
            showUserProvisioningResults = false;
            showUserProvisioningResultsError = true;
        }
        
        
            
        return null;   
    }
    
    
    /*
     * ====== HELPER METHODS ==================================================================================================
     */
    
    
    /*
     * Get a list of TM-related Permsets from the org that are allowed to be provisioned through this tool
     */
    private List<SelectOption> getTmPermsetOptions() {
        List<SelectOption> options = new List<SelectOption>();
        tmPermsetsFromOrg = [SELECT Id, Label, Name 
                                                       FROM PermissionSet 
                                                       WHERE Name LIKE 'TM_%' AND Name != 'TM_Admin'
                                                       ORDER BY Name ASC];
        
        for (PermissionSet ps: tmPermsetsFromOrg) {
            options.add(new SelectOption(ps.Id, ps.Label));
        }
        return options; 
    }
    
    /*
     * Get a list of TM-related Public Groups from the org that are allowed to be provisioned through this tool
     */
    private List<SelectOption> getTmPublicGroupOptions() {
        List<SelectOption> options = new List<SelectOption>();
        tmPublicGroupsFromOrg = [SELECT Id, DeveloperName, Name, Type 
                                                   FROM Group 
                                                   WHERE Type='Regular' 
                                                   AND (DeveloperName LIKE 'TM_BU_%' OR DeveloperName = 'TM_All_BU_Validators' OR DeveloperName = 'TM_View_All_SFDC_Business_Unit_Accounts') 
                                                   AND (DeveloperName != 'TM_Admins' AND DeveloperName != 'TM_Implementers' AND DeveloperName != 'TM_Validators')
                                                   ORDER BY Name ASC];
        
        for (Group pg: tmPublicGroupsFromOrg) {
            options.add(new SelectOption(pg.Id, pg.Name));
            publicGroupMap.put(pg.DeveloperName, pg.Id);
        }
        return options; 
    }
    
    
    /*
     * Get a list of TM-related Business Units from the org that are allowed to be provisioned through this tool
     */
    private List<SelectOption> getTmBUOptions() {
        List<SelectOption> options = new List<SelectOption>();
        tmBUsFromOrg = [SELECT Id, Name, TMM_Tracking__c 
                                            FROM Account 
                                            WHERE TMM_Tracking__c = True AND RecordTypeId = :accountBusinessUnitRT.Id AND Account_Status__c ='Active' AND IsDeleted = False
                                            ORDER BY Name ASC];
        
        for (Account a: tmBUsFromOrg) {
            options.add(new SelectOption(a.Id, a.Name));
            businessUnitMap.put(a.Id, a.Name);
        }
        options.add(new SelectOption(fakeAllBUsEntryFromOrg, 'All Business Units'));
        return options; 
    }
    
    
    /*
     * Create a new TM user, with profile = TMM External and user details pulled from the User's contact record
     * Note: fedId aka email must be unique in the org
     */
    private User createNewTMUser(String email, String username) {
        List<Contact> c = [SELECT Id, FirstName, LastName, Email, Title, Department, Acquired_Company__c FROM Contact WHERE Email = :email AND RecordTypeId = :sfdcEmployeeContactRT.Id AND AccountId = :sfdcAcct.Id];
        //List<Contact> c = [SELECT Id, FirstName, LastName, Email, Title, Department, Acquired_Company__c FROM Contact WHERE Email ='jchen@salesforce.com' AND RecordTypeId = :sfdcEmployeeContactRT.Id AND AccountId = :sfdcAcct.Id];
        
        User u;
        String alias = null;
        if (newUserAlias.length() > 8) {
            alias = newUserAlias.substring(0, 8);
        } else {
            alias = newUserAlias;
        }

        if (c.size() != 1) {
            userCreationResultsErrorDetail.add('A 1-1 match with a Contact record was not found');
        } else {
            
            try {
                u = new User();
                u.FirstName = c[0].FirstName;
                u.LastName = c[0].LastName;
                u.Title = c[0].Title;
                u.CompanyName = c[0].Acquired_Company__c;
                u.Department = c[0].Department;
                
                u.Alias = alias; 
                u.Email = email;
                u.Username = username;
                u.CommunityNickname = alias + String.valueOf(DateTime.now().millisecondGMT());
    
                u.ProfileId = tmmProfile.Id;
                
                u.FederationIdentifier  = email;
        
                u.TimeZoneSidKey = 'America/Los_Angeles';
                u.LocaleSidKey = 'en_US';
                u.EmailEncodingKey = 'ISO-8859-1';
                u.LanguageLocaleKey = 'en_US';
                
                insert u;
                
            } catch (Exception e) {
                userCreationResultsErrorDetail.add(e.getMessage());
            }
        }
        
        //Datetime timeUserProvisioned = Datetime.now(); 
        return u;
    }
    
    
    
    /*
     * Delete the User's existing TM-related Permsets and Public Groups
     */
    private void deleteUserPermsetsPublicGroups(User userToBeProvisioned) {
        List<PermissionSetAssignment> userPermsetsToDelete = [SELECT Id, PermissionSet.Label, AssigneeId 
                                            FROM PermissionSetAssignment 
                                            WHERE (PermissionSet.Name LIKE 'TM_%' AND PermissionSet.Name != 'TM_Admin') 
                                            AND AssigneeId = :userToBeProvisioned.Id];
        
        delete userPermsetsToDelete;
        
        List<GroupMember> userPublicGroupsToDelete = [SELECT Id, UserOrGroupId, Group.Id, Group.Name, Group.Type, Group.DeveloperName
                               FROM GroupMember 
                               WHERE (Group.DeveloperName LIKE 'TM_%')
                               AND UserOrGroupId = :userToBeProvisioned.Id
                               AND Group.Type='Regular'];
        
        delete userPublicGroupsToDelete;
        
        //System.debug('do user provisioning, finishing deleting');
    }

    /*
     * Add the selected TM-related Permsets and Public Groups to the User
     */
    private void addUserPermsetsPublicGroups(User userToBeProvisioned) {
        // add the new Permsets for the User
        List<PermissionSetAssignment> newPermsets = new List<PermissionSetAssignment>();
        for (String ps: tmPermsetsSelected) {
            newPermsets.add(new PermissionSetAssignment(AssigneeId = userToBeProvisioned.Id, PermissionSetId = ps)); 
        }      
        insert newPermsets;
        
        
        // add the new Public Groups for the User
        List<GroupMember> newGroupMembers = new List<GroupMember>();
        for (String pg: determinePublicGroups()) {
           newGroupMembers.add(new GroupMember(UserOrGroupId = userToBeProvisioned.Id, GroupId = pg));
        }
        try {
            insert newGroupMembers; 
        } catch (DmlException de) { 
            userProvisioningResultsErrorDetail.add('Error: Problem inserting Group Members. Please verify the existence of all the selected Public Groups.');
        }
    }
    
    /**
     * Determine which Public Groups the user needs, based on the TM Permset
     * 
     *  - TM Basic Read Only Permset = no PGs
     *  - TM Trust Observer Permset = 'View All SFDC Business Units' PG
     *  - TM Business Unit Observer Permset = whatever PGs were selected, except 'All BUs' PG
     *  - TM Implementer Permset = whatever PGs were selected, except 'All BUs' PG
     *  - TM Validator Permset = whatever PGs were selected, including 'All BUs' PG. If 'All BUs' PG was selected, clear the other PG selections and just keep 'All BUs' PG
     * 
    **/
    private List<String> determinePublicGroups() {        
        String PG_ViewAllSFDCBusinessUnits = '00G300000047FeeEAE';
        String PG_AllBUValidators = '00G300000047G5NEAU';
        
        List<Id> publicGroupsToProvision = new List<Id>();
        
        for (String ps: tmPermsetsSelected) {
            //System.debug('selected permset iteration ' + ps);
            if (ps == '0PS300000008UkkGAE') {       // TM Basic Read Only Permset
                                                // no public groups associated w this permset
                    
            } else if (ps == '0PS300000008UklGAE') {                            // TM Trust Observer Permset 
                publicGroupsToProvision.add(PG_ViewAllSFDCBusinessUnits);   // public group = View All SFDC Business Units
                    
            } else if (ps == '0PS300000008VBMGA2') {         // TM Business Unit Observer Permset
                for (String bu: tmBUsSelected) {
                    if (bu != fakeAllBUsEntryFromOrg) {                    
                        publicGroupsToProvision.add(turnAccountIdIntoPublicGroupId(bu, true));
                    }
                }
                
            } else if (ps == '0PS300000008VBNGA2' ) {     // TM Implementer Permset
                for (String bu: tmBUsSelected) {
                    if (bu != fakeAllBUsEntryFromOrg) {                    
                        publicGroupsToProvision.add(turnAccountIdIntoPublicGroupId(bu, false));
                    }
                }
            } else if (ps == '0PS300000008UkmGAE') {     // TM Validator Permset
                boolean allBUs = false;
                
                // if All BUs wasn't selected, provision the public groups chosen
                for (String bu: tmBUsSelected) {
                    if (bu != fakeAllBUsEntryFromOrg) {                    
                        publicGroupsToProvision.add(turnAccountIdIntoPublicGroupId(bu, false));
                    } else {
                        allBUs = true;
                    }                    
                }
                
                // if All BUs was selected, provision just the All BUs public group
                if (allBUs == true) {
                    publicGroupsToProvision.clear();
                    publicGroupsToProvision.add(PG_AllBUValidators);
                }   
            } 
            
            
        }  
        return publicGroupsToProvision;
    }
    
    private String turnAccountIdIntoPublicGroupId (String acctId, boolean isObserver) {
        /**String acctName = businessUnitMap.get(acctId);         // gets the account name
        if (acctName.contains('Predictive Intelligence')){   // Predictive Intelligence name is different on the account vs public group
            acctName = 'Predictive Intelligence';
        }
        acctName = acctName.replace(' ', '_');
        acctName = acctName.replace('.', '_');
        String buDevNamePrefix = 'TM_BU_' + acctName;
        String buDevName;
        
        if (isObserver) {
            buDevName = buDevNamePrefix + '_Observers';
        } else {
            buDevName = buDevNamePrefix + '_Doers';
        }

        //System.debug('bu equivalent public group devname ' + buDevName);
        return publicGroupMap.get(buDevName);   // returns public group Id
    **/
        
        //updated after Jorge's change to modify the public group dev names to have acctId instead of Name
        if (isObserver) {
            return TM_BUsharingUtil.getBUObserversGroupId(acctId);
        } else {
            return TM_BUsharingUtil.getBUDoersGroupId(acctId);
        }
        
         
    }
    
    private void sendProvisioningNotificationEmail (User endUser) {
        Messaging.reserveSingleEmailCapacity(3);
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        
        //mail.setOrgWideEmailAddressId('0D2n000000000Wq');
        mail.setSenderDisplayName('Salesforce Trust Maturity');
        
        String[] bccAddresses = new String[] {'jchen@salesforce.com'};           
        mail.setBccAddresses(bccAddresses);
        mail.setBccSender(true);  // BCC the email sender on the email        
        
        mail.setReplyTo('no-reply@salesforce.com');     
        mail.setUseSignature(false);
        mail.setSaveAsActivity(false);
        
        mail.setTargetObjectId(endUser.Id);  // The ID of the contact, lead, or user to which the email will be sent
        mail.setTemplateId(userProvisioningCompletedEmailTemplate.Id);

        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
    
    private void sendInternalTMNotificationEmail (String endUserEmail, String endUserUsername, boolean isUserCreationRelated) {
        String delimiter = '<br/>';
        String currentUser = UserInfo.getName();
        String currentUserEmail = UserInfo.getUserEmail();
        String permsetsForEmail = null;
        String busForEmail = null;
        String publicGroupsForEmail = null;
        
        // formulate the email contents depending on whether the action committed was user creation or user provisioning
        String action;
        if (isUserCreationRelated) {
            action = 'User Creation';
            permsetsForEmail = 'n/a';
            busForEmail = 'n/a';
            publicGroupsForEmail = 'n/a';
        } else {
            action = 'User Provisioning';

            // get a list of human readble names so we can put them in the notification email
            for (String idSelected: tmPermsetsSelected) {
                for (PermissionSet permsetFromOrg: tmPermsetsFromOrg) {
                    if (idSelected == permsetFromOrg.Id) {
                        if (permsetsForEmail == null) {
                            permsetsForEmail = permsetFromOrg.Label;
                        } else {
                            permsetsForEmail = permsetsForEmail + delimiter + permsetFromOrg.Label;
                        }
                    }   
                }       
            }
            
            for (String idSelected: tmBUsSelected) {
                if (idSelected != fakeAllBUsEntryFromOrg) {
                    for (Account busFromOrg: tmBUsFromOrg) {
                        if (idSelected == busFromOrg.Id) {
                            if (busForEmail == null) {
                                busForEmail = busFromOrg.Name;
                            } else {
                                busForEmail = busForEmail + delimiter + busFromOrg.Name;
                            }
                        }
                    }       
                } else {
                    if (busForEmail == null) {
                        busForEmail = fakeAllBUsEntryFromOrg;
                    } else {
                        busForEmail = busForEmail + delimiter + fakeAllBUsEntryFromOrg;
                    }
                    
                }
            }
            
            for (String idSelected : determinePublicGroups()) {
                for (Group g : tmPublicGroupsFromOrg){
                    if (idSelected == g.Id) {
                        if (publicGroupsForEmail == null) {
                            publicGroupsForEmail = g.Name;
                        } else {
                            publicGroupsForEmail = publicGroupsForEmail + delimiter + g.Name;
                        }
                    }
                }
            }         
        }
        
        // construct the actual email object for sending
        Messaging.reserveSingleEmailCapacity(3);
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        
        String[] toAddresses = new String[] {'jchen@salesforce.com', currentUserEmail};
        mail.setToAddresses(toAddresses);

        //mail.setOrgWideEmailAddressId('0D2n000000000Wq');
        mail.setSenderDisplayName('Salesforce Trust Maturity');
        
        mail.setBccSender(false);  // BCC the email sender on the email        
        mail.setReplyTo('no-reply@salesforce.com');     
        mail.setUseSignature(false);
        mail.setSaveAsActivity(false);
    
        mail.setSubject('TM User Mgmt Notification: ' + currentUser + ' performed ' + action + ' on ' + endUserEmail);        
        mail.setHtmlBody(
            '<b>====== Trust Maturity Model - User Management Activity Notification ====== </b><p> <b>Admin User: </b>' + currentUser + 
            '</p><p><b>Action: </b> ' + action +
            '</p>' + '----------------------------------------' +
            '<p><b>End User Email: </b>'+ endUserEmail +
            '</p><p><b>End User Username: </b>'+ endUserUsername + 
            '</p><p><b>Permsission Sets Selected/Provisioned: </b><br/>'+ permsetsForEmail + 
            '</p><p><b>Business Units Selected: </b><br/>'+ busForEmail + 
            '</p><p><b>Public Groups Provisioned: </b><br/>'+ publicGroupsForEmail +
            '</p>'
        );
        
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
}
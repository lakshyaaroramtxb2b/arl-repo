global with sharing class RegexBenchmarkController {

    private Detection_Field_Extraction__c fieldExtraction;
	private ID recId;
    
    public RegexBenchmarkController() {
        recId = ApexPages.currentPage().getParameters().get('id');
        fieldExtraction = getRecord();
    }

    public Detection_Field_Extraction__c getDetectionFieldExtraction() {
        return fieldExtraction;
    }
    
    public String getBenchmarkMatched() {
        return '';
    }
        
    public PageReference save() {
        update fieldExtraction;
        return null;
    }
    
    private Detection_Field_Extraction__c getRecord()
    {
        return [SELECT Id FROM Detection_Field_Extraction__c 
                   WHERE Id =: recId];
    }
}
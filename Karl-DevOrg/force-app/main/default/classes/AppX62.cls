public with sharing class AppX62 {
    
    /*
     * These creds are for an api user in 62 with access to the Program__c object.  Creds are supposedly
     * cycled every 4 months, so this password will need to be updated accordingly.  Contact for this 
     * api user acct is Ari Rubinstein (arubinstein@salesforce.com).
     */
     
    public static string getUsername(){
        try {   
            return AppX62__c.getAll().values()[0].Username__c;
        } catch (Exception e){
            return 'notset';
        }
    }
    public static string getPassword(){
        try {   
            return AppX62__c.getAll().values()[0].Password__c;
        } catch (Exception e){
            return 'notset';
        }
    }
    
    
    /**
     * Logs into 62 and returns a connection object with the current session
     */
    private static partnerSoapSforceCom.Soap login(){
        partnerSoapSforceCom.Soap conn = new partnerSoapSforceCom.Soap();
        partnerSoapSforceCom.LoginResult lr = conn.login(getUsername(), getPassword());
        if (lr.sessionId != null){
            //we have logged in
            partnerSoapSforceCom.SessionHeader_element sessionheader = new partnerSoapSforceCom.SessionHeader_element();
            sessionheader.sessionId = lr.sessionId;
            conn.endpoint_x = lr.serverUrl;
            conn.SessionHeader = sessionheader;
            return conn; //return the connection - we have logged in
        } else {
            System.debug('Failed login. Null session');
            System.debug(lr);
            return null;
        }
    }
    
    public class PartnerAppOppInfo {
        public string Stage {get;set;}
        public string AppType {get;set;}
        public string EnrollmentStage {get;set;}
        public string AppName {get;set;}
        public boolean isValidForBurpLicense(){
            /*
             * stages will most likely never change, but if they do, this logic will have to change
             * set to return true if something changes with the datamodel
             * 
             * stage names right now are like this:
             * 515 - Blah
             * 550 - blah blah
             */   
        
            String[] stageNameParts = Stage.split(' ');
            if (stageNameParts == null || stageNameParts.size() == 0) return false;
            //try to parse our first piece of the stage name
            try {
                integer i = integer.valueOf(stageNameParts[0]);
                if ( (i >= 1 && i <= 5) || i >= 515){
                    //app is valid since they have signed a contract
                    return true;
                } else {
                    return false;
                }
            } catch (Exception e){
                return false;
            }
            
            return false;
       
        }
    }
    
    /**
     * Container class for lookup of qualified apps
     */
    public class PartnerAccountInfo {
        public string AccountId {get;set;}
        public string AccountName {get;set;}
        public List<PartnerAppOppInfo> PartnerAppOpps = new List<PartnerAppOppInfo>();      
        public boolean hasValidAppsForBurpLicense(){
            for (PartnerAppOppInfo ap : PartnerAppOpps) if (ap.isValidForBurpLicense()) return true;
            return false;
        }
    }
    
    /**
     * Returns a list of qualified partner applications for a partnerforce user
     */
    public static PartnerAccountInfo getQualifiedAppsForPartnerforceUser(String partnerforceUser){
        //make sure we have a partnerforce username 
        if (partnerforceUser == null || partnerforceUser == '' || !partnerforceUser.toLowerCase().endsWith('@partnerforce.com')) return null;
        
        partnerSoapSforceCom.Soap session = login();
        if (session == null) {
            System.debug('CRITICAL: getQualifiedAppsForPartnerforceUser() returning null due to invalid session');
            return null;
        }
        PartnerAccountInfo info = null;
        
        
        try { 
            //wrap so we don't show errors to the user. 
            //We should really do correct exception handling eventually if this will be used anywhere else
            
            info = new PartnerAccountInfo();
            
            
            //find 62 account info from partnerforce email
            string soqlAccount ='SELECT Id, Name From Account WHERE Id IN (SELECT AccountId FROM User WHERE Username = \''+String.escapeSingleQuotes(partnerforceUser)+'\') LIMIT 1';
            
            partnerSoapSforceCom.QueryResult q = session.query(soqlAccount);
            if (q.records == null || q.records.size() != 1) {
                System.debug('getQualifiedAppsForPartnerforceUser: No Account found for user: '+partnerforceUser);
                return null;
            }
            
            info.AccountId = q.records[0].Id;
            info.AccountName = q.records[0].Name;
            if (info.AccountId == null || info.AccountName == null) {
                System.debug('getQualifiedAppsForPartnerforceUser: No  accountId on info');
                return null; //We don't have an account Id
            }

            //find opportunities of ISV type related to our account
            string soqlOpportunities = 'SELECT Planned_Pricing__c, Name, Id, Composite__c';
            soqlOpportunities += ' FROM Application__c WHERE Account__c = \''+String.escapeSingleQuotes(info.AccountId)+'\'';

            partnerSoapSforceCom.QueryResult qOpp = session.query(soqlOpportunities);
            if (qOpp.records == null || qOpp.records.size() == 0) return info;
            
            for (sobjectPartnerSoapSforceCom.sObject_x oppRecord : qOpp.records){
                PartnerAppOppInfo pinfo = new PartnerAppOppInfo();
                pinfo.AppName = oppRecord.Name;
                pinfo.AppType = 'Native';
                
                pinfo.Stage = '515 probably ok';
                pinfo.EnrollmentStage = '';
                info.PartnerAppOpps.add(pinfo);
            }
            return info;
        } catch (Exception e){
            System.debug('CRITICAL: getQualifiedAppsForPartnerforceUser: exception');
            System.debug(e);
            return null;
        }
        System.debug('getQualifiedAppsForPartnerforceUser: Unknown error?');
        return null;
    }
    
    public static void debug(String s){
        System.debug(LoggingLevel.ERROR,'ARI: ' + s);
    }
    public static void debug(Object o){
        debug('Contents of object: ' + JSON.serializePretty(o));
    }
    public static void debug(List<Object> a){
        debug('Contents of List:');
        for (Object o : a){
            debug(o);
        }
        debug('End of List.');
        
    }
    
    public static void emailAMessage(String to, String subject, String body){
      
            Messaging.reserveSingleEmailCapacity(1);
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String[] toAddresses = new String[] {to};
            mail.setToAddresses(toAddresses);
            mail.setSubject(subject);
            
            mail.setPlainTextBody(body);
            Messaging.sendEmail(new Messaging.SingleEmailMessage[]{mail});
    
    }
    
    
    public static void debug(Map<String, Object> a){
        debug('Contents of Map:');
        for (String o : a.keySet()){
            debug(o);
            debug(a.get(o));
        }
        debug('End of Map.');

    }
    
}
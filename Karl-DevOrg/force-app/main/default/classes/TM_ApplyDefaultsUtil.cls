/*

    Developer Name: Jorge L Caceres - jcaceres@salesforce.com | June 2016
    
    Description: Applies defaults to concrete objectives

*/

global with sharing class TM_ApplyDefaultsUtil {
        
    webservice static void applyDefaults(Id defaultRecId, Boolean overrideExisting) {

        if (TM_Constants.Current_User_Is_Admin) {
            update new TM_Objective_Default__c(Id = defaultRecId, Status__c = 'Being Applied');        
            applyDefaultsFuture(defaultRecId, overrideExisting);
        } else {
            update new TM_Objective_Default__c(Id = defaultRecId, Status__c = UserInfo.getName() + ' Not Authorized.');                    
        }
        
    }
    
    @future    
    private static void applyDefaultsFuture(Id defaultRecId, Boolean overrideExisting) {
        
        // retrieve default record
        for (TM_Objective_Default__c defaultRec : [SELECT Id, Business_Unit__c, Category__c, Validation_Owner_Default__c
                                                  FROM TM_Objective_Default__c
                                                  WHERE Id = :defaultRecId]) {

            Id bizUnitId = defaultRec.Business_Unit__c;
            String categoryName = defaultRec.Category__c;
            
            // apply defaults to selected objectives
            List<TM_Objective__c> objectivesToUpdate = new List<TM_Objective__c>();
            
            
            String objQuery = 'SELECT Id, TM_Placement__r.Category__c, ValidationOwner__c FROM TM_Objective__c ' +
                              'WHERE (Business_Unit__c = :bizUnitId ' +
                              'OR CSIRT_Environment__r.IRCloud__Account__c = :bizUnitId) ' +
                              'AND TM_Placement__r.Category__c = :categoryName ' +
                              'AND Status__c NOT IN (\'Failed\', \'Completed\', \'Not Applicable\') ' +
                              '{override}';
            if (!overrideExisting) objQuery = objQuery.replace('{override}', 'AND ValidationOwner__c = null'); 
                else objQuery = objQuery.replace('{override}', '');    
            
            for (TM_Objective__c objective : Database.query(objQuery)) {
                objective.ValidationOwner__c = defaultRec.Validation_Owner_Default__c;
                objectivesToUpdate.add(objective);
            }        
            
            // update db
            if (!objectivesToUpdate.isEmpty()) update objectivesToUpdate;            
            defaultRec.Status__c = 'Completed ' + DateTime.now().format('MM/dd/yyyy HH:mm');
            update defaultRec;
                                                      
        }
    }
        
}
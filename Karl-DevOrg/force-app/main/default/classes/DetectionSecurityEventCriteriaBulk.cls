global class DetectionSecurityEventCriteriaBulk implements Database.Batchable<sObject> {
	
    global String Query;
    global String val;
    
    global DetectionSecurityEventCriteriaBulk(String itagID, String itagVal, String ival)
    {
        Query = 'SELECT Id, tagValue__c FROM Detection_Log_Tagging_Value__c WHERE TagName__c = \'' + itagID + '\' AND Detection_Log_Tagging__c = \'' + itagVal + '\'';
    
        val = ival;
        
    	FeedItem post = new FeedItem();
        post.ParentId = 'a353A000000HDNv';
        post.Body = Query;
        insert post;  
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC)
	{
		return Database.getQueryLocator(Query);
	}
    
    global void execute(Database.BatchableContext BC, List<Sobject> sobjs)
	{
        try
        {
            if(sobjs.size() == 1) {
                
                Detection_Log_Tagging_Value__c dltv = (Detection_Log_Tagging_Value__c)sobjs[0];
                
                dltv.tagValue__c = val;
                
                update dltv;
            }
            
            FeedItem post = new FeedItem();
            post.ParentId = 'a353A000000HDNv';
            post.Body = 'DONE';
            insert post;   
        }
        catch(Exception e) {
            
            FeedItem post = new FeedItem();
            post.ParentId = 'a353A000000HDNv';
            post.Body = 'BULK ' + e.getMessage();
            insert post;            
        }
    }
    
    global void finish(Database.BatchableContext BC)
	{
    } 
}
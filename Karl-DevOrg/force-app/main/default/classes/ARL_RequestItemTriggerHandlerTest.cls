/**
* @author Mahima Aggarwal
* @email mahima.aggarwal@mtxb2b.com
* @description Test class for Requests Master Data
*/
@isTest
public class ARL_RequestItemTriggerHandlerTest {
    
    /**
* @author Mahima Aggarwal
* @email mahima.aggarwal@mtxb2b.com
* @description Test data setup.
*/
    @testSetup
    private static void testSetup(){
        User opsAdminUser = ARL_TestDataFactory.createOpsAdmin();
        System.runAs(opsAdminUser){
            Account acc = new Account(Name = 'salesforce.com - ESA Office Hours');
            insert acc;
            
            Contact con = new Contact(LastName = 'test', AccountId = acc.Id,Email = 'test@gmail.com');
            insert con;
            
            Contact con2 = new Contact(LastName = 'test2', AccountId = acc.Id,Email = 'test2@gmail.com');
            insert con2;
            
            Request_Item__c reqItem1 = ARL_TestDataFactory.createRequestItem();
            reqItem1.Update_Current_Audit_Cycle_s__c = 'No';
            reqItem1.Primary_Scope__c = 'SS';
            reqItem1.Type__c = 'Samples';
            reqItem1.Priority__c = 'Wave 1'; 
            insert reqItem1;
            
            Request_Item__c reqItem2 = ARL_TestDataFactory.createRequestItem();
            reqItem2.Update_Current_Audit_Cycle_s__c = 'No';
            reqItem2.Primary_Scope__c = 'SS';
            reqItem2.Type__c = 'Samples';
            reqItem2.Priority__c = 'Wave 1'; 
            insert reqItem2;
            
            Audit_Cycle__c auditCycle = ARL_TestDataFactory.createAuditCycle();
            insert auditCycle;
            
            KARL_Gus_Product_Tag__c defaultProductTag = ARL_TestDataFactory.createKarlGusProductTag(true,true);
            insert defaultProductTag;
            
            List<KARL_Auto_Assignment_Configuration__c> autoAssignmentConfigList = ARL_TestDataFactory.createAutoAssignmentConfig(defaultProductTag.Id,4);
            autoAssignmentConfigList[1].Priority_0_Assignment_Date__c = System.today().addDays(2);
            autoAssignmentConfigList[2].Priority_0_Assignment_Date__c = System.today().addDays(-1);
            autoAssignmentConfigList[3].Priority_0_Assignment_Date__c = System.today();
            insert autoAssignmentConfigList;
            
            List<Audit_Cycle_Coverage__c> auditCycleCoverageList = new List<Audit_Cycle_Coverage__c>();
            Map<Integer,String> priorityToScopeMap = new Map<Integer,String>();
            priorityToScopeMap.put(0,'AS');
            priorityToScopeMap.put(1,'AUST');
            priorityToScopeMap.put(2,'B2BC');
            priorityToScopeMap.put(3,'CHAT');
            for(Integer i=0;i<4;i++){
                Audit_Cycle_Coverage__c auditCycleCoverage = new Audit_Cycle_Coverage__c();
                auditCycleCoverage.Audit_Cycle__c = auditCycle.Id;
                auditCycleCoverage.Area__c = 'iRAP';
                auditCycleCoverage.Scope__c = priorityToScopeMap.get(i);
                auditCycleCoverage.Auto_Assignment_Configuration__c = autoAssignmentConfigList[i].Id;
                auditCycleCoverageList.add(auditCycleCoverage);
            }
            insert auditCycleCoverageList;
            
            
            KARL_Audit_Team__c auditTeam = ARL_TestDataFactory.createAuditTeam('test');
            insert auditTeam;
            
            KARL_Audit_Team_Contacts__c auditTeamCon = ARL_TestDataFactory.createAuditTeamContact(auditTeam.Id,con.Id);
            insert auditTeamCon;
            
            List<Cycle_Request_Item__c> cycleReqItemList = new List<Cycle_Request_Item__c>();
            for(Integer i=0;i<4;i++){
                Cycle_Request_Item__c cycleReqItem = ARL_TestDataFactory.createCycleRequestItem(reqItem1.id,null);  
                cycleReqItem.Cycle_Request_Status__c = 'New';
                cycleReqItem.KARL_Update_GUS__c = true;
                cycleReqItem.Request_Tech_Details__c = 'tech details old';
                cycleReqItem.Request_Assignee__c = con.Id;
                cycleReqItem.Audit_Cycle__c = auditCycle.Id;
                cycleReqItem.Audit_Cycle_Coverage__c = auditCycleCoverageList[i].Id;
                cycleReqItem.Request_GUS__c = null;
                cycleReqItemList.add(cycleReqItem);
            }
            insert cycleReqItemList;
        }
    }
    
    /**
* @author Mahima Aggarwal
* @email mahima.aggarwal@mtxb2b.com
* @description In this test, the test is updating the request item and asserting with Request Item Version Item (as an Ops Admin)
*/
    @istest
    public static void unitTest_opsAdmin(){ 
        List<User> opsAdminUserList = ARL_TestDataFactory.fetchUser('abc@b.com.d');
        if(!opsAdminUserList.isEmpty()){
            System.runAs(opsAdminUserList[0]){
                Request_Item__c reqItem = [SELECT id FROM Request_Item__c LIMIT 1];
                
                // Updating request item will create new Request Item Version
                reqItem.Request_Description__c = 'test12345';
                reqItem.Request_Tech_Details__c = 'test12345';
                reqItem.Areas_of_Compliance__c = 'SOC 1';
                reqItem.Request_Name__c = 'test12345';	
                reqItem.Update_Current_Audit_Cycle_s__c = 'Yes';
                update reqItem; 
                
                List<Request_Item_Version__c> reqVersion = [SELECT id FROM Request_Item_Version__c];
                
                // Expecting 3 versions because two were created when Request Item was created and another when Request Item got updated
                system.assertEquals(3, reqVersion.size() );   
            }
        }
    }
    
    /**
* @author Ben Harvie
* @email ben.harvie@salesforce.com
* @description In this test, the test is updating the request item and asserting with Request Item Version Item (as an Audit Mgr)
*/
    @istest
    public static void unitTest_AuditMgr(){
        List<User> opsAdminUserList = ARL_TestDataFactory.fetchUser('abc@b.com.d');
        if(!opsAdminUserList.isEmpty()){
            System.runAs(opsAdminUserList[0]){
            Request_Item__c reqItem = [SELECT id FROM Request_Item__c LIMIT 1];
            
            // Updating request item will create new Request Item Version
            reqItem.Request_Description__c = 'test12345';
            reqItem.Request_Tech_Details__c = 'test12345';
            reqItem.Areas_of_Compliance__c = 'SOC 1';
            reqItem.Request_Name__c = 'test12345';	
            reqItem.Update_Current_Audit_Cycle_s__c = 'Yes';
            update reqItem; 
            
            List<Request_Item_Version__c> reqVersion = [SELECT id FROM Request_Item_Version__c];
            
            // Expecting 3 versions because two were created when Request Item was created and another when Request Item got updated
            system.assertEquals(3, reqVersion.size() );   
        }
        }
    }
    
    /**
* @author Lakshya Arora
* @email lakshya.arora@mtxb2b.com
* @description Testing of wave change and new gus work record creation
*/
    @istest
    private static void createGusRecordsOnWaveChangeTest(){ 
        List<User> opsAdminUserList = ARL_TestDataFactory.fetchUser('abc@b.com.d');
        if(!opsAdminUserList.isEmpty()){
            System.runAs(opsAdminUserList[0]){
                List<Request_Item__c> reqItemList = new List<Request_Item__c>();
                for(Request_Item__c reqItemObj : [SELECT id,Priority__c FROM Request_Item__c]){
                    reqItemObj.Priority__c = 'Wave 2';
                    reqItemList.add(reqItemObj);
                }
                
                Test.startTest();
                update reqItemList; 
                Test.stopTest();
                System.assert(![SELECT Id FROM KARL_GUS_SLA_Tracking__c].isEmpty());
            }
        }
    }
}
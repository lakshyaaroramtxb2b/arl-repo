/**
 * Created by hari ch on 06-08-2019.
 * update Prashant Gupta
 */
global class PRT_ProjectStatusReportAndQuipBatch implements 
            Database.Batchable<sObject>, schedulable{
                
    
    public static final String PRT_CR_STATUS_APPROVED = 'Approved';
    public static final String PRT_CR_STATUS_PROJECTKICKOFF = 'Project Kickoff';
    public static final String PRT_CR_STATUS_PLANNING = 'Planning';
    public static final String PRT_CR_STATUS_MONITORING = 'Monitoring';
    public static final String PRT_CR_STATUS_COMPLETIONSO = 'Completion Sign-off';
    public static final String PRT_CR_STATUS_EXECUTING = 'Executing';
                
    global Integer recordsProcessed = 0;
    // collect the batches of Project records to be passed to execute
    
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        Set<String> statusSet = new Set<String>();
        statusSet.add(PRT_CR_STATUS_PROJECTKICKOFF);
        statusSet.add(PRT_CR_STATUS_PLANNING);
        statusSet.add(PRT_CR_STATUS_MONITORING);
        statusSet.add(PRT_CR_STATUS_COMPLETIONSO);
        statusSet.add(PRT_CR_STATUS_EXECUTING);
        String query = 'SELECT ID, Name,Program__c FROM Project__c'+
         ' WHERE Status__c IN :statusSet';  
        return Database.getQueryLocator(query);
    }
    
    // process each batch of records and create Project Status Report of each project
    global void execute(Database.BatchableContext bc, List<Project__c> scope){
        Map<id,Project_Status_Report__c> projectIdVsPSRMap = new Map<id,Project_Status_Report__c>();
        List<Project_Status_Report__c> projectStatusReportList =new  List<Project_Status_Report__c>();
        for(Project_Status_Report__c proSR : [SELECT id,Overall_Project_Status__c,Budget_Status__c,
                                              Schedule_Status__c,Project__c FROM Project_Status_Report__c WHERE Project__c IN :scope
                                             AND Week_ending__c = THIS_WEEK]){
            projectIdVsPSRMap.put(proSR.Project__c,proSR);                                          
        }
        system.debug('??projectIdVsPSRMap'+ projectIdVsPSRMap);
        for(Project__c pro: scope){
            system.debug('??pro' +pro);
            Project_Status_Report__c projectStatusReport = new Project_Status_Report__c();
            projectStatusReport.Name = pro.Name + '-' +'Week of: ' +Date.today().toStartofWeek().addDays(5).format();
            if(projectStatusReport.Name.length()>80){
                        projectStatusReport.Name = projectStatusReport.Name.substring(0, 80);
                    }
            projectStatusReport.Program__c = pro.Program__c;
            projectStatusReport.Project__c = pro.id;
            projectStatusReport.Week_ending__c =Date.today().toStartofWeek().addDays(5);
            if(projectIdVsPSRMap!=null && !projectIdVsPSRMap.isEmpty() && projectIdVsPSRMap.containsKey(pro.Id)){
                projectStatusReport.Overall_Project_Status__c = projectIdVsPSRMap.get(pro.id).Overall_Project_Status__c;
                projectStatusReport.Budget_Status__c = projectIdVsPSRMap.get(pro.id).Budget_Status__c;
                projectStatusReport.Schedule_Status__c = projectIdVsPSRMap.get(pro.id).Schedule_Status__c;
                projectStatusReport.Latest_Overall_Project_Status__c = projectIdVsPSRMap.get(pro.id).Overall_Project_Status__c;
            }
            projectStatusReportList.add(projectStatusReport);
        }
        insert projectStatusReportList;
    }
    
    // execute any post-processing operations
    global void finish(Database.BatchableContext bc){
        AsyncApexJob job = [SELECT Id, Status, NumberOfErrors, 
                            JobItemsProcessed,
                            TotalJobItems, CreatedBy.Email
                            FROM AsyncApexJob
                            WHERE Id = :bc.getJobId()];
        // call some utility to send email
        //EmailUtils.sendMessage(job, recordsProcessed);
    }    
    global void execute(SchedulableContext sc) {
      database.executebatch(new PRT_ProjectStatusReportAndQuipBatch());
   }
}
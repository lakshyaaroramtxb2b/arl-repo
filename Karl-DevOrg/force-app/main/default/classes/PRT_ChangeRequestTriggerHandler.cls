/***********************************************************
* Handler Class for Change Request
* Created by 	- Prashant Gupta
************************************************************/
public class PRT_ChangeRequestTriggerHandler {
	/***********************************************************
	 * Before Insert method to be called from Trigger
     * Created by 	- Prashant Gupta
     * Date 		- 09-09-2019
     ************************************************************/
    public static void beforeInsert(List<Change_Request__c> newList){
        PRT_Utility.updateProgramLookups(newList,null);
     }
    
	/***********************************************************
	 * Before Update method to be called from Trigger
     * Created by 	- Prashant Gupta
     * Date 		- 09-09-2019
     ************************************************************/
    public static void beforeUpdate(List<Change_Request__c> newList, Map<id,Change_Request__c> oldMap){        
        PRT_Utility.updateProgramLookups(newList,oldMap);
        PRT_ChangeRequestTriggerHelper.checkRequiredFields(newList,oldMap);
    }
    
	/***********************************************************
	 * After Insert method to be called from Trigger
     * Created by 	- Prashant Gupta
     * Date 		- 09-09-2019
     ************************************************************/
    public static void afterInsert(List<Change_Request__c> newList){
        PRT_ChangeRequestTriggerHelper.createInternalUserSharing(newList,null);
    }
    
	/***********************************************************
	 * After Update method to be called from Trigger
     * Created by 	- Prashant Gupta
     * Date 		- 09-09-2019
     ************************************************************/
    public static void afterUpdate(List<Change_Request__c> newList, Map<id,Change_Request__c> oldMap){        
        PRT_ChangeRequestTriggerHelper.createInternalUserSharing(newList,oldMap);   
        PRT_ChangeRequestTriggerHelper.updateNewBudgetToProjectOnApproval(newList,oldMap);
        PRT_ChangeRequestTriggerHelper.createMilestoneToProjectOnApproval(newList,oldMap);
    }
}
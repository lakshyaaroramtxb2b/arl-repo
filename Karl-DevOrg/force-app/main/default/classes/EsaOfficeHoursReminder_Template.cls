public class EsaOfficeHoursReminder_Template {
    //invocable method for the process builder
    //the parameters is the created request id
    @InvocableMethod
    public static void templateEmailSender (List <ESA_Security_Request__c> requests){
        
        set <id> entitiesId = new set <id>();
        String templateLabel = Label.Office_Hours_Standard_Template;
        
        //we go through the requests and add them to a list
        //also adding the ids of the entities from those requests
        for (ESA_Security_Request__c req : requests){
            entitiesId.add(req.ESA_Entity__c);
        }
        
        //from each of the entities, we get their ids and custom template name
        //we depend on the user input of the correct developer name, we should double check them
        Map<Id, String> entities = new Map<Id, String> ();
        for (ESA_Entity__c entity : [SELECT Id, Custom_Template_Unique_Name__c FROM ESA_Entity__c WHERE id IN: entitiesId]) {
            entities.put(entity.Id, entity.Custom_Template_Unique_Name__c);
        }
        
        //create a list to send all the emails at once
        List<Messaging.SingleEmailMessage> allmsg = new List<Messaging.SingleEmailMessage>();
        //get the standard email template
        EmailTemplate et = [SELECT Id FROM EmailTemplate WHERE DeveloperName =:templateLabel];
        EmailTemplate template;
        
        //create a contact list that later is going to contain all contacts to be deleted
        list <contact> contacts = new List <Contact> ();

        //first check, entity record
        //we loop through the requests and then the entities. If the entity and request entity is the same, we do the logic
        if (!entities.isEmpty()){
            for (ESA_Security_Request__c req : requests){
                //added the satus condition, because deleting the paused flow interview was imposible
                if (req.Status__c != 'Closed'){
                    if (entities.containsKey(req.ESA_Entity__c)) {
                        if (String.isNotBlank(entities.get(req.ESA_Entity__c))){
                            //in this case, if the custom template is not blank, it means entity has custom template
                            EmailTemplate etc = [SELECT Id FROM EmailTemplate WHERE Name =:entities.get(req.ESA_Entity__c)];
                            template = etc;
                        }
                        else{
                            template = et;
                            }
                        //create mail variable
                        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                        mail.setUseSignature(false);
                        
                        //get the email sender and assign it
                        OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'esa.coordinator@salesforce.com'];
                        if ( owea.size() > 0 ) {
                            mail.setOrgWideEmailAddressId(owea.get(0).Id); 
                                                }
                        //create the address variable
                        string [] toaddress;
                        
                        //get the user from the requestor and create a contact because it is needeed for the email
                        User user = [select email, firstName, lastName, ContactId from USER where id =: req.Requestor__c];
                        Contact contact = [select email, firstName, lastName from Contact where id =: user.ContactId];
                        //contact tempContact = new Contact (email = user.email, firstName = user.FirstName, lastName = user.LastName);
                        
                        toaddress = new String []{contact.email};
                        
                        //we have to insert the contact, that will be deleted later
                        //insert tempContact;
                        //contacts.add(contact);

                        mail.setToAddresses(toaddress);
                        mail.setTemplateId(template.id);
                        mail.setTargetObjectId(contact.Id); 
                        mail.setWhatId(req.id);
                        mail.setSaveAsActivity(false);
                        allmsg.add(mail);
                        
                    }
                }
            }
        }
        

        
        try {
            //if(!Test.isRunningTest()) {
                Messaging.sendEmail(allmsg);
                
            //}
            //delete contacts;
            return;
    } catch (Exception e) {
        System.debug(e.getMessage());
}
        
        
        
    }
    

}
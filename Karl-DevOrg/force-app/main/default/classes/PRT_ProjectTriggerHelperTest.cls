/***********************************************************
* Test class for PRT_ProjectTriggerHelper
* Created by 	- Prashant Gupta
************************************************************/
@isTest
public class PRT_ProjectTriggerHelperTest {
    @testSetup
    public static void testSetup(){
        List<User> userList = new List<User>();
        userList = PRT_TestDataFactory.createUsers(3,'System Administrator',true);
        System.runAs(userList[0]){
            PRT_Integration_Settings__c integrationSetting = new PRT_Integration_Settings__c();
            integrationSetting.GUS_Project_Integration__c = true;
            insert integrationSetting;
        } 
    }
    static testMethod void insertUpdateProjectTest() {
        List<User> userList = new List<User>([SELECT Id FROM USER WHERE Email Like 'PRTUser%']);
        List<Program__c> programList = new List<Program__c>(PRT_TestDataFactory.createPrograms(3,true));
        List<Project__c> projectList = new List<Project__c>(PRT_TestDataFactory.createProjects(2,false));
        for(integer i=0; i<2; i++){
            projectList[i].Program__c = programList[i].id;
            projectList[i].Project_Manager__c = userList[0].id;
            projectList[i].Executive_Sponsor_Internal__c = userList[1].id;
            projectList[i].Sponsor_Internal__c = userList[2].id;
        }
        insert projectList;
        test.startTest(); 
        List<Milestone_PRT__c> milestoneList= new List<Milestone_PRT__c>();
        //List<Risk__c> riskList= new List<Risk__c>();
        List<Assignment__c> assignmentList= new List<Assignment__c>();
        List<Purchase_Order__c> purchaseOrderList= new List<Purchase_Order__c>();
        
        milestoneList = PRT_TestDataFactory.createMilestone(10,projectList[0].id,true);
        //riskList = PRT_TestDataFactory.createRisk(10,projectList[0].id,true);
        assignmentList = PRT_TestDataFactory.createAssignment(10,projectList[0].id,milestoneList[0].id,true);
        purchaseOrderList = PRT_TestDataFactory.createPurchaseOrder(10,projectList[0].id,true);
        
        projectList[0].Program__c = programList[2].id;
        projectList[0].Sponsor_Internal__c = userList[1].id;
        update projectList;
        
        for(Assignment__c rec : assignmentList){
            rec.Project__c = projectList[1].id;
        }
        update assignmentList;
        
        for(Purchase_Order__c rec : purchaseOrderList){
            rec.Project__c = projectList[1].id;
        }
        update purchaseOrderList;
        
        test.stopTest();
        
    }
    
    static testMethod void createProjectShareTest() {
        List<User> userList = new List<User>([SELECT Id FROM USER WHERE Email Like 'PRTUser%']);
        List<Program__c> programList = new List<Program__c>(PRT_TestDataFactory.createPrograms(3,true));
        List<Project__c> projectList = new List<Project__c>(PRT_TestDataFactory.createProjects(2,false));
        for(integer i=0; i<2; i++){
            projectList[i].Program__c = programList[i].id;
            projectList[i].Project_Manager__c = userList[0].id;
            projectList[i].Executive_Sponsor_Internal__c = userList[1].id;
            projectList[i].Sponsor_Internal__c = userList[2].id;
        }
        insert projectList;
        test.startTest();
        List<Milestone_PRT__c> milestoneList= new List<Milestone_PRT__c>();
        //List<Risk__c> riskList= new List<Risk__c>();
        List<Assignment__c> assignmentList= new List<Assignment__c>();
        List<Purchase_Order__c> purchaseOrderList= new List<Purchase_Order__c>();
        List<Change_Request__c> changeRequestList = new List<Change_Request__c>();
        
        milestoneList = PRT_TestDataFactory.createMilestone(10,projectList[0].id,true);
        //riskList = PRT_TestDataFactory.createRisk(10,projectList[0].id,true);
        assignmentList = PRT_TestDataFactory.createAssignment(10,projectList[0].id,milestoneList[0].id,true);
        purchaseOrderList = PRT_TestDataFactory.createPurchaseOrder(10,projectList[0].id,true);
        changeRequestList = PRT_TestDataFactory.createChangeRequest(1,projectList[0].id,false);
        
        for(Change_Request__c co : changeRequestList) {
            co.Budget__c = true;
            co.Budget_Change_Reason__c = 'test scope';
            co.Proposed_Budget__c = 'test scope reason';
            co.New_Budget_Amount__c = 500;
        }
        INSERT changeRequestList;
        
        for(Assignment__c rec : assignmentList){
            rec.Project__c = projectList[1].id;
        }
        PRT_Constants.RECURSIVE_TRIGGER_CHECK = true;
        update assignmentList;
        
        for(Purchase_Order__c rec : purchaseOrderList){
            rec.Project__c = projectList[1].id;
        }
        PRT_Constants.RECURSIVE_TRIGGER_CHECK = true;
        update purchaseOrderList;
        
        projectList[0].Program__c = programList[2].id;
        projectList[1].Project_Manager__c = userList[1].id;
        projectList[0].Executive_Sponsor_Internal__c = userList[1].id;
        projectList[1].Sponsor_Internal__c = userList[2].id;
        PRT_Constants.RECURSIVE_TRIGGER_CHECK = true;
        update projectList;
        List<Purchase_Order__Share> purchaseOrderShareList = new List<Purchase_Order__Share>([SELECT ID
                                                                                              FROM Purchase_Order__Share 
                                                                                              WHERE ParentID IN :purchaseOrderList]);
        
        System.assert(purchaseOrderShareList!=null);
        test.stopTest();
        
    }
    
    static testMethod void updateProjectRanksTest() {
        Set<Integer> overriderank = new Set<Integer>();
        List<PRT_ProjectWrapper> projectWrapperList = new List<PRT_ProjectWrapper>();
        List<Project__c> updatedProjectList = new List<Project__c>();
        List<User> userList = new List<User>([SELECT Id FROM USER WHERE Email Like 'PRTUser%']);
        List<Program__c> programList = new List<Program__c>(PRT_TestDataFactory.createPrograms(3,true));
        List<Project__c> projectList = new List<Project__c>(PRT_TestDataFactory.createBussinessCase(3,false));
        for(integer i=0; i<3; i++){
            projectList[i].Program__c = programList[i].id;
            projectList[i].Project_Manager__c = userList[0].id;
            projectList[i].Executive_Sponsor_Internal__c = userList[1].id;
            projectList[i].Sponsor_Internal__c = userList[2].id;
            projectList[i].Mandate__c = '1';
            projectList[i].CSF_Impact__c =  '1';
            projectList[i].Strategy_Alignment__c =  '1';
            projectList[i].Status__c =PRT_Constants.PRT_PROJECT_STATUS_SCORED_BUSINESS_CASE;
            projectList[i].Rank_Override__c = 1;
            projectList[i].Portfolio_Owner_Comments__c = 'test head';
            projectList[i].Portfolio_Manager__c = userList[1].Id;
        }
        PRT_Constants.RECURSIVE_TRIGGER_CHECK =true;
        insert projectList;
        
        test.startTest();
        List<Milestone_PRT__c> milestoneList= new List<Milestone_PRT__c>();
        List<Risk__c> riskList= new List<Risk__c>();
        List<Assignment__c> assignmentList= new List<Assignment__c>();
        List<Purchase_Order__c> purchaseOrderList= new List<Purchase_Order__c>();
        
        milestoneList = PRT_TestDataFactory.createMilestone(10,projectList[0].id,true);
        riskList = PRT_TestDataFactory.createRisk(10,projectList[0].id,true);
        assignmentList = PRT_TestDataFactory.createAssignment(10,projectList[0].id,milestoneList[0].id,true);
        purchaseOrderList = PRT_TestDataFactory.createPurchaseOrder(10,projectList[0].id,true);         
        for(integer i=0; i<3; i++){
            projectList[i].Status__c =PRT_Constants.PRT_PROJECT_STATUS_SCORED_BUSINESS_CASE;
            projectList[i].Program__c = programList[i].id;
            projectList[i].Mandate__c = PRT_TestDataFactory.generateRandomNumberString();
            projectList[i].CSF_Impact__c = PRT_TestDataFactory.generateRandomNumberString();
            projectList[i].Strategy_Alignment__c = PRT_TestDataFactory.generateRandomNumberString();
            projectList[i].Portfolio_Owner_Comments__c = 'Test';
            
        }
        
        
        projectList[0].Rank_Override__c = 1;
        projectList[0].rank__c = 1 ;
        overriderank.add(1);
        PRT_Constants.RECURSIVE_TRIGGER_CHECK = true;
        //UPDATE projectList;
        for(Integer i=1;i<3;i++){
            projectWrapperList.add(new PRT_ProjectWrapper(projectList[i]) );
        }
        
        projectWrapperList.sort();
        integer i = 1;
        for(PRT_ProjectWrapper pw:projectWrapperList){
            if(overriderank.contains(i)){
                while(overriderank.contains(++i)){                        
                }
            }
            pw.project.Rank__c = i;
            updatedProjectList.add(pw.project);
            i++;
        }
        if(updatedProjectList!=null && !updatedProjectList.isEmpty()){
            PRT_Constants.RECURSIVE_TRIGGER_CHECK = true;
            update updatedProjectList;
        }
        
        test.stopTest();
        
        
        System.assert(projectList[0].rank__c == 1);
        System.assert(projectList[1].rank__c == 2);
        System.assert(projectList[2].rank__c == 3);
        
    }
    
    static testMethod void updateProgramLookupsTest(){
        List<User> userList = new List<User>([SELECT Id FROM USER WHERE Email Like 'PRTUser%']);
        List<Program__c> programList = new List<Program__c>(PRT_TestDataFactory.createPrograms(3,true));
        List<Project__c> projectList = new List<Project__c>(PRT_TestDataFactory.createBussinessCase(2,false));
        for(integer i=0; i<2; i++){
            projectList[i].Program__c = programList[i].id;
            projectList[i].Program_Business_Case__c = programList[i].id;
            projectList[i].recordTypeId =Schema.SObjectType.Project__c.getRecordTypeInfosByName().get('Business Case').getRecordTypeId();
            projectList[i].Status__c =PRT_Constants.PRT_PROJECT_STATUS_SCORED_BUSINESS_CASE;
            projectList[i].Mandate__c = '1';
            projectList[i].CSF_Impact__c = '1';
            projectList[i].Strategy_Alignment__c ='1';
            projectList[i].Rank_Override__c = 1;
            projectList[i].Portfolio_Owner_Comments__c = 'test head';
            projectList[i].Portfolio_Manager__c = userList[1].Id;
        }
        
        insert projectList;
        
        test.startTest();
        
        for(integer i=0; i<2; i++){
            projectList[i].Status__c =PRT_Constants.PRT_PROJECT_STATUS_SCORED_BUSINESS_CASE;
            projectList[i].RecordTypeId = Schema.SObjectType.Project__c.getRecordTypeInfosByName().get('Project').getRecordTypeId();
            projectList[i].Program__c = programList[i].id;
            
        }
        
        PRT_Constants.RECURSIVE_TRIGGER_CHECK = true;
        update projectList;
        
        test.stopTest();
        Integer i=0;
        for(Project__c pro : [SELECT Id,Program__c,Program_Business_Case__c
                              FROM Project__c
                              WHERE Id IN: projectList]){
                                  System.assertEquals(pro.Program__c,programList[ i++].id);  
                                  
                              }
        
        
        
    }
    
    static testMethod void convertProjectTest(){
        List<User> userList = new List<User>([SELECT Id FROM USER WHERE Email Like 'PRTUser%']);
        List<Portfolio__c> portfolioList = PRT_TestDataFactory.createPortfolio(1,false);
        for(Portfolio__c portfolio : portfolioList) {
            portfolio.Portfolio_Owner__c = userList.get(0).Id;
        }
        INSERT portfolioList;
        List<Program__c> programList = new List<Program__c>(PRT_TestDataFactory.createPrograms(2,true));
        List<Project__c> projectList = new List<Project__c>(PRT_TestDataFactory.createBussinessCase(2,false));
        for(integer i=0; i<2; i++){
            projectList[i].Program_Business_Case__c = programList[i].id;
            projectList[i].recordTypeId =PRT_Constants.PRT_PROJECT_BUSINESSCASE_RECORDTYPEID;
            projectList[i].Status__c ='Approved';
            projectList[i].Portfolio__c = portfolioList.get(0).Id;
        }
        
        insert projectList;
        test.startTest();
        for(Project__c pro : projectList){
            pro.Status__c =  PRT_Constants.PRT_PROJECT_STATUS_ROC;
            pro.Portfolio__c = portfolioList[0].id;
            pro.Problem_Summary__c = 'test data summary';
            pro.Proposed_Solution__c='test proposed solution data';
            pro.Success_Measures__c = 'test success measure';
            pro.HC_vs_Proserv_Justification__c = 'test data';
            pro.Impact__c = 'test data';
            pro.Business_Benefits__c = 'test data BB';
            pro.Approve_Funding__c = true;
            pro.Allocated_Proserv_Headcount__c = 0;
            pro.HC_Allocated__c = 0;
            pro.Budget_Allocated__c = 897.98;
            pro.Budget_ID__c = 'bud Id 001';
            if(pro.Program_Business_Case__c==null){
                pro.Program_Business_Case__c = programList[0].id;
            }
            pro.SoW__c = 'test data sow';
            pro.HC_Requests__c= 0;
            pro.Proserv_Requests__c = 0;
            pro.CSF_Mappings__c = 'test data';
        }
        PRT_Constants.RECURSIVE_TRIGGER_CHECK = true;
        update projectList;
        test.stopTest();
        
        for(Project__c pro : [SELECT Id,RecordTypeId,Status__c, Portfolio_Owner__c
                              FROM Project__c 
                              WHERE Id IN: projectList ]){
                                  System.assertEquals(pro.RecordTypeId ,PRT_Constants.PRT_PROJECT_PROJECT_RECORDTYPEID);
                                  //System.assertEquals(pro.Status__c, PRT_Constants.PRT_Project_St);
                                  System.assertEquals(pro.Portfolio_Owner__c, userList.get(0).Id);
                              }      
    } 
    
    static testMethod void checkRequiredFieldsTest() {
        List<User> userList = new List<User>([SELECT Id FROM USER WHERE Email Like 'PRTUser%']);
        List<Program__c> programList = new List<Program__c>(PRT_TestDataFactory.createPrograms(3,true));
        List<Project__c> projectList = new List<Project__c>(PRT_TestDataFactory.createProjects(2,false));
        for(integer i=0; i<2; i++){
            projectList[i].Program__c = programList[i].id;
            projectList[i].Project_Manager__c = userList[0].id;
            projectList[i].Executive_Sponsor_Internal__c = userList[1].id;
            projectList[i].Sponsor_Internal__c = userList[2].id;
            if(i==1) {
                projectList[i].RecordTypeId = PRT_Constants.PRT_PROJECT_BUSINESSCASE_RECORDTYPEID;
            }
        }
        insert projectList;
        test.startTest(); 
        List<Milestone_PRT__c> milestoneList= new List<Milestone_PRT__c>();
        List<Risk__c> riskList= new List<Risk__c>();
        List<Assignment__c> assignmentList= new List<Assignment__c>();
        List<Purchase_Order__c> purchaseOrderList= new List<Purchase_Order__c>();
        
        milestoneList = PRT_TestDataFactory.createMilestone(10,projectList[0].id,true);
        riskList = PRT_TestDataFactory.createRisk(10,projectList[0].id,true);
        assignmentList = PRT_TestDataFactory.createAssignment(10,projectList[0].id,milestoneList[0].id,true);
        purchaseOrderList = PRT_TestDataFactory.createPurchaseOrder(10,projectList[0].id,true);
        
        projectList[1].Program__c = programList[2].id;    
        projectList[1].Status__c = PRT_Constants.PRT_CR_STATUS_PROJECTKICKOFF;
        projectList[1].Name = '';
        projectList[1].Program_Business_Case__c = null;
        projectList[1].Portfolio__c = null;
        projectList[1].Problem_Summary__c = null;
        projectList[1].Proposed_Solution__c = null;
        projectList[1].Success_Measures__c = null;
        projectList[1].HC_vs_Proserv_Justification__c = null;
        projectList[1].Impact__c = null;
        projectList[1].Business_Benefits__c = null;
        projectList[1].Approve_Funding__c = false;
        projectList[1].HC_Allocated__c = null;
        projectList[1].Budget_Allocated__c = null;
        projectList[1].Budget_ID__c = null;
        projectList[1].Project_Manager__c = null;
        
        projectList[0].Program__c = programList[2].id;    
        projectList[0].Status__c = PRT_Constants.PRT_CR_STATUS_PROJECTKICKOFF;
        projectList[0].Sponsor_Internal__c = null;
        projectList[0].Executive_Sponsor_Internal__c = null;
        projectList[0].Vision__c = '';
        projectList[0].Project_Overview_Purpose__c = '';
        projectList[0].Scope__c = '';
        projectList[0].Objectives__c = '';
        projectList[0].Deliverables__c = '';
        projectList[0].Definition_of_Done__c = '';
        projectList[0].Business_Benefits__c = '';
        projectList[0].Impact_Metrics__c = '';
        projectList[0].Dependencies__c = '';
        projectList[0].Assumptions__c = '';
        projectList[0].Limitations__c = '';
        projectList[0].Automation_Opportunities__c = '';
        
        try{
            update projectList;
        }catch(exception e){
            //System.assert(e.getMessage().contains('Project_Manager__c'));
        }
        for(Assignment__c rec : assignmentList){
            rec.Project__c = projectList[1].id;
        }
        update assignmentList;
        
        for(Purchase_Order__c rec : purchaseOrderList){
            rec.Project__c = projectList[1].id;
        }
        update purchaseOrderList;
        
        test.stopTest();
        
    }
    
    static testMethod void testUpdateInsertGUSRecords(){
        List<User> userList = new List<User>([SELECT Id FROM USER WHERE Email Like 'PRTUser%']);
        List<Program__c> programList = new List<Program__c>(PRT_TestDataFactory.createPrograms(1,true));
        List<Portfolio__c> portfolioList = new List<Portfolio__c>(PRT_TestDataFactory.createPortfolio(1, true));
        test.startTest();
        List<Project__c> projectList = new List<Project__c>(PRT_TestDataFactory.createProjects(1,false));
        projectList[0].Program__c = programList[0].id;
        projectList[0].Portfolio__c = portfolioList[0].Id;
        projectList[0].Project_Manager__c = userList[0].id;
        projectList[0].Executive_Sponsor_Internal__c = userList[1].id;
        projectList[0].Sponsor_Internal__c = userList[2].id;
        projectList[0].Status__c = PRT_Constants.PRT_CR_STATUS_PROJECTKICKOFF;
        projectList[0].Project_Start_Date__c = date.today();
        projectList[0].Project_Overview_Purpose__c = 'test project OverView';
        projectList[0].Customer_POC__c = 'test customer Poc';
        INSERT projectList;
        projectList[0].Customer_POC__c = 'test POC Update';
        //projectList[0].Status__c = 'Project Kickoff';
        projectList[0].Vision__c = 'test data';
        projectList[0].Business_Benefits__c = 'test data';
        projectList[0].Scope__c = 'test data';
        projectList[0].Objectives__c = 'test data';
        projectList[0].Deliverables__c = 'test data';
        projectList[0].Definition_of_Done__c = 'test data';
        projectList[0].Impact_Metrics__c = 'test data';
        projectList[0].Dependencies__c = 'test data';
        projectList[0].Assumptions__c = 'test data';
        projectList[0].Limitations__c = 'test data';
        projectList[0].Automation_Opportunities__c = 'test data';
        projectList[0].Projected_Date_of_Completion__c = date.today()+45;
        projectList[0].Overall_Project_Health__c = 'On Track';
        projectList[0].Budget_Status__c = 'Green';
        projectList[0].Schedule_Status__c = 'Green';
        projectList[0].Project_Health_Comments__c = 'test data';
        update projectList;
        test.stopTest();      
    }
    
    static testMethod void populateSponsorOnRiskTest(){
        List<User> userList = new List<User>([SELECT Id FROM USER WHERE Email Like 'PRTUser%']);
        List<Risk__c> riskList= new List<Risk__c>();
        List<Project__c> projectList = new List<Project__c>(PRT_TestDataFactory.createProjects(1,false));
        projectList[0].Sponsor_Internal__c = userList[0].id;
        insert projectList;
        riskList = PRT_TestDataFactory.createRisk(10,projectList[0].id,false);
        riskList[0].Project_Sponsor__c = userList[1].id;
        insert riskList;
        
        test.startTest();
        projectList[0].Sponsor_Internal__c =userList[1].id;
        update projectList;
        System.assert(projectList[0].Sponsor_Internal__c!=null);
        test.stopTest();   
    }
    
    static testMethod void createPSR_OnKickoffTest() {
        List<Project__c> projectList = new List<Project__c>(PRT_TestDataFactory.createProjects(1,true));
        List<User> userList = new List<User>([SELECT Id FROM USER WHERE Email Like 'PRTUser%']);
        List<Program__c> programList = new List<Program__c>(PRT_TestDataFactory.createPrograms(1,true));
        List<Portfolio__c> portfolioList = new List<Portfolio__c>(PRT_TestDataFactory.createPortfolio(1, true));
        
        projectList[0].Status__c = PRT_Constants.PRT_CR_STATUS_PROJECTKICKOFF;
        projectList[0].Program__c = programList[0].id;
        projectList[0].Portfolio__c = portfolioList[0].Id;
        projectList[0].Project_Manager__c = userList[0].id;
        projectList[0].Executive_Sponsor_Internal__c = userList[1].id;
        projectList[0].Sponsor_Internal__c = userList[2].id;
        projectList[0].Project_Start_Date__c = date.today();
        projectList[0].Project_Overview_Purpose__c = 'test project OverView';
        projectList[0].Customer_POC__c = 'test customer Poc';
        projectList[0].Customer_POC__c = 'test POC Update';
        projectList[0].Vision__c = 'test data';
        projectList[0].Business_Benefits__c = 'test data';
        projectList[0].Scope__c = 'test data';
        projectList[0].Objectives__c = 'test data';
        projectList[0].Deliverables__c = 'test data';
        projectList[0].Definition_of_Done__c = 'test data';
        projectList[0].Impact_Metrics__c = 'test data';
        projectList[0].Dependencies__c = 'test data';
        projectList[0].Assumptions__c = 'test data';
        projectList[0].Limitations__c = 'test data';
        projectList[0].Automation_Opportunities__c = 'test data';
        projectList[0].Projected_Date_of_Completion__c = date.today()+45;
        projectList[0].Overall_Project_Health__c = 'On Track';
        projectList[0].Budget_Status__c = 'Green';
        projectList[0].Schedule_Status__c = 'Green';
        projectList[0].Project_Health_Comments__c = 'test data';
        UPDATE projectList;
        
        List<Project__c> projectList2 = new List<Project__c>(PRT_TestDataFactory.createProjects(1,true));
        List<Milestone_PRT__c> milestoneList = PRT_TestDataFactory.createMilestone(1,projectList2[0].id,true);
        projectList2[0].Status__c = PRT_CONSTANTS.PRT_PROJECT_CLOSURE_PROCESS;
        try {
         	UPDATE projectList2;   
        }
        catch(exception e) {
            System.assert(e.getMessage().contains('milestones are complete'));
        }
    }
}
public class PhishRegister {

    public PhishingQuizConstants qc = new PhishingQuizConstants();
    public String name {get;set;}
    public String company {get;set;}
    public String email {get;set;}
    public String notification {get;set;}
    public Boolean showSubmit {get;set;}
    private PhishingQuizUser__c user;
    
    public PhishRegister() {
        showSubmit = True;
    }
    
    private void sendNotificationEmail() {
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(new String[] {user.Email__c});
        mail.setReplyTo('noreply@salesforce.com');
        mail.setSenderDisplayName('Salesforce.com Product Security');
        mail.setSubject('Salesforce.com Phishing and Malware Identification Quiz');
        mail.setUseSignature(false);
        mail.setPlainTextBody(qc.emailBody(user,false));
        mail.setHtmlBody(qc.emailBody(user,true));
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
    }
        
    public PageReference register() {
        Boolean isDemo = ApexPages.CurrentPage().getParameters().containsKey('demo');
        
        Pattern validEmail = Pattern.compile('^[A-Za-z0-9\\._%\\+\\-]+@[A-Za-z0-9\\.\\-]+\\.[A-Za-z]{2,4}$');
        Pattern whiteSpace = Pattern.compile('^\\s*$');
        Matcher emailMatcher = validEmail.matcher(email);
        if ((email == null) || (name==null) || (company==null) ||
            (validEmail.matcher(email).matches() == False) || (whiteSpace.matcher(name).matches()==True) || 
            (whiteSpace.matcher(company).matches()==True)){
            this.notification = 'Please fill out all fields';
            this.showSubmit = True;
            return null;
        }
        String key = '';
        for (Integer i=1;i<=2;i++) {
            key += String.valueOf(Math.abs(Crypto.getRandomLong()));
        }
        if ((qc.oneValidUserPerEmail) && (!isDemo)) {
            datetime oldestAllowed = datetime.now().addDays(0-qc.quizValidDays);
            List<PhishingQuizUser__c> activeUsers = [select Id,created__c from PhishingQuizUser__c where Email__c=:email 
                                                 and created__c >=:oldestAllowed limit 1];
            if (activeUsers.size() > 0) {
                this.notification = 'An active quiz is already associated with this email address';
                Double daysRemaining = qc.quizValidDays - ((((Double)datetime.now().getTime() - activeUsers.get(0).created__c.getTime())/1000)/(60*60*24));
                this.notification += ' ('+ String.valueOf(Math.round(daysRemaining))+' days until expiration)';
                this.showSubmit = True;
                return null;
            }
        }
        Datetime t = System.now();
        //Date d = Date.newInstance(t.year(),t.month(),t.day());
        
        user = new PhishingQuizUser__c(Name=name,
                               Email__c=email,
                               Company__c=company,
                               Key__c=key,
                               Created__c=t);
        insert user;
        if(!isDemo)
        {
            sendNotificationEmail();
            this.notification = 'An email has been sent to '+user.Email__c+' containing your quiz access key';
            this.showSubmit = False;                          
            return null;
        }
        else
        {
            return new PageReference(qc.quizQuestion.getUrl()+'?sId='+user.Key__c);
        }
    }
    
}
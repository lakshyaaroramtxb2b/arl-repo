public class PRT_SLADueDateBatch implements Database.Batchable<sObject>, Schedulable{

    @TestVisible
    private static List<SLA__c> mockSLAList = new List<SLA__c>();
    
    public Database.QueryLocator start(Database.BatchableContext BC){
        Date todayDate = Date.today();
        String query = 'SELECT Id, Reporting_Period__c, Reporting_Start_Date__c, Due_Date__c, Operation__c, Operation__r.Id FROM SLA__c WHERE Due_Date__c =: todayDate';
        System.debug('Query: '+query);
        if(Test.isRunningTest()){
            return Database.getQueryLocator([SELECT Id FROM User LIMIT 1]);    
        }else{
            return Database.getQueryLocator(query);
        }
    }

    public void execute(Database.BatchableContext bc, List<SObject> slaList) {
        Date todayDate = Date.today();
        if(Test.isRunningTest()){
            slaList = mockSLAList;
        }
        System.debug('SLA List: '+slaList);
        Set<Id> slaIdSet = new Set<Id>();
        Set<Id> newSlaIdSet = new Set<Id>();
        List<SLA__c> updateSLADueDateList = new List<SLA__c>();
        
        for(SLA__c slaRecord : (List<SLA__c>)slaList) {
            slaIdSet.add(slaRecord.Id);
        }
        
        List<SLA_Metric__c> slaMetricList = new List<SLA_Metric__c>([SELECT Id, Due_Date__c, SLA__c, Comment__c, Value__c FROM SLA_Metric__c
                                                                     WHERE SLA__c IN: slaIdSet
                                                                     AND Due_Date__c =: todayDate]);   
        for(SLA_Metric__c metric : slaMetricList) {
            String comment = String.valueOf(metric.Comment__c);
            String value = String.valueOf(metric.Value__c);
            System.debug('value: '+value+'comment: '+comment);
            if(String.isEmpty(comment) || String.isEmpty(value)) {
                newSlaIdSet.add(metric.SLA__c);
            }
        }
        
        List<SLA__c> newSlaList = new List<SLA__c>([SELECT Id, Reporting_Period__c, Due_Date__c
                                                    FROM SLA__c 
                                                    WHERE Id IN: newSlaIdSet]);
        
        
        if(newSlaList!=null && !newSlaList.isEmpty()) {
            for(SLA__c slaRecord : (List<SLA__c>)newSlaList) {
                updateSLADueDateList.add(updateSLADueDate(slaRecord));
            }
        }

        System.debug('Updated SLA List: '+updateSLADueDateList);

        if(updateSLADueDateList!=null && !updateSLADueDateList.isEmpty()) {
            UPDATE updateSLADueDateList;
        }
    }  

    public static SLA__c updateSLADueDate(SLA__c slaRec) {
        if(slaRec.Reporting_Period__c == 'Daily') {
            slaRec.Due_Date__c = Date.today().addDays(1);
        }
        else if(slaRec.Reporting_Period__c == 'Weekly') {
            String currentDay = DateTime.now().format('E');
            if(currentDay == 'Mon')
                slaRec.Due_Date__c = Date.today().addDays(4);
            else if(currentDay == 'Tue')
                slaRec.Due_Date__c = Date.today().addDays(3);
            else if(currentDay == 'Wed')
                slaRec.Due_Date__c = Date.today().addDays(2);
            else if(currentDay == 'Thu')
                slaRec.Due_Date__c = Date.today().addDays(1);
            else if(currentDay == 'Fri')
                slaRec.Due_Date__c = Date.today().addDays(7);
            else if(currentDay == 'Sat')
                slaRec.Due_Date__c = Date.today().addDays(6);
            else if(currentDay == 'Sun')
                slaRec.Due_Date__c = Date.today().addDays(5);
        }
        else if(slaRec.Reporting_Period__c == 'Monthly') {
            Date firstDateOfMonth = Date.today().toStartOfMonth(); 
            Date lastDateOfMonth = firstDateOfMonth.addMonths(1).addDays(-1);
            slaRec.Due_Date__c = lastDateOfMonth;

        }
        else if(slaRec.Reporting_Period__c == 'Quarterly') {
            Date fiscalQuarterLastDate = [Select EndDate From Period Where type = 'Quarter' and StartDate = THIS_FISCAL_QUARTER].EndDate;
            slaRec.Due_Date__c = fiscalQuarterLastDate;
        }
        else if(slaRec.Reporting_Period__c == 'Annually') {
            Date fiscalYearLastDate = [Select EndDate From Period Where type = 'Year' and StartDate = THIS_FISCAL_YEAR].EndDate;
            slaRec.Due_Date__c = fiscalYearLastDate;
        }
        return slaRec;
    }
 
    public void finish(Database.BatchableContext bc){
        
    } 

    public void execute(SchedulableContext SC) {
        Database.executebatch(new PRT_SLADueDateBatch());
    }
}
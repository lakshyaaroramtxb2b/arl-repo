global with sharing class ZipUploadBacklogChecker {
    //check for ZU job errors
    public static void run(BatchNotifier bn) { 
    
     Backlog_Configuration__c config = [SELECT Max_Working_Queue_Size__c,
                                               Max_Backlog_Size__c,
                                               Max_Unworked_Scan_Hours__c,
                                               Max_Allowed_Scan_Hours__c,
                                               Max_Queue_Wait_Hours__c
                                            FROM Backlog_Configuration__c
                                            WHERE Active__c = True LIMIT 1];
                                            
     Zip_Upload__c [] longQueueWait  = [SELECT Name,CreatedDate,Id,WorkState__c FROM Zip_Upload__c WHERE 
                                             WorkState__C != 'Done Moving Files' 
                                             AND WorkState__c != 'Never'
                                             AND WorkState__c != '' 
                                             AND CreatedDate <: Datetime.now().addHours(0-Math.round(config.Max_Queue_Wait_Hours__c))
                                             AND CreatedDate = LAST_N_DAYS:30];

     Zip_Upload__c [] queueLen  = [SELECT Name,CreatedDate,Id,WorkState__c FROM Zip_Upload__c WHERE 
                                             WorkState__c != 'Done Moving Files' 
                                             AND WorkState__c != 'Never'
                                             AND WorkState__c != ''
                                             AND CreatedDate = LAST_N_DAYS:30];
     
     Zip_Upload__c [] unknownStateLen  = [SELECT Name,CreatedDate,Id,WorkState__c FROM Zip_Upload__c WHERE
                                             WorkState__C != 'New'
                                             AND WorkState__c != 'Passed To Worker'
                                             AND WorkState__c != 'Moving Files'
                                             AND WorkState__c != 'Done Moving Files'
                                             AND WorkState__c != 'Never'
                                             AND WorkState__c != ''
                                             AND CreatedDate = LAST_N_DAYS:14];
 
     if (queueLen.size() > config.Max_Backlog_Size__c) {
         bn.addNotifier(Notifier.Severity.ERROR,'ZipUploadBacklogChecker : Unstarted scan queue length of '+String.valueOf(queueLen.size())+' exceeds max size of '+String.valueOf(config.Max_Backlog_Size__c),makeExtra(queueLen));
     }
     
     if (longQueueWait.size() > 0) {
         bn.addNotifier(Notifier.Severity.ERROR,'ZipUploadBacklogChecker : ' + String.valueOf(longQueueWait.size())+' scan(s) waiting in queue (not in progress) longer than allowable '+String.valueOf(config.Max_Queue_Wait_Hours__c)+' hours',makeExtra(longQueueWait));
		 revert(longQueueWait);
     }

     if (unknownStateLen.size() > 0) {
         bn.addNotifier(Notifier.Severity.ERROR,'ZipUploadBacklogChecker : ' + String.valueOf(unknownStateLen.size())+' scan(s) in in unknown states',makeExtra(unknownStateLen));
     }

     bn.addNotifier(Notifier.Severity.DEBUG,'ZipUploadBacklogChecker : complete: queueLen['+String.valueOf(queueLen.size())+'] longQueueWait['+String.valueOf(longQueueWait.size())+'] unknownStateLen['+String.valueOf(unknownStateLen.size())+']');    
    }
   
    private static List<String> makeExtra(List<Zip_Upload__c> objs) {
        List<String> ret = new List<String>();
        for (Zip_Upload__c c : objs) {
            ret.add(c.CreatedDate.format() + '  ' + c.Name + '  ' + c.WorkState__c);
        }
        return ret;
    }
    
     private static void revert(List<Zip_Upload__c> objs) {
        for (Zip_Upload__c  c : objs) {
            
            if (c.WorkState__c == 'Moving Files') {
                //we need to reset these fields so the job is not associated to a scanner
                c.WorkState__c = 'New';
                c.Comments__c = 'auto-resubmitted from Moving Files state';
                }
        }
             
        update objs;

        return;
    }
    
    public static testMethod void doTest() {
        delete [select Id from Backlog_Configuration__c];
        insert new Backlog_Configuration__c(Max_Working_Queue_Size__c=2,
                                       Max_Backlog_Size__c=6,
                                       Max_Unworked_Scan_Hours__c=4,
                                       Max_Allowed_Scan_Hours__c=8,
                                       Max_Queue_Wait_Hours__c=1,
                                       Active__c = True);
        List<Zip_Upload__c> testSet = new List<Zip_Upload__c>();
        /*
        // longQueueWait
        testSet.add(new Zip_Upload__c(name='longQueueWait-Test',Created__c=Datetime.now().addHours(-3),Username__c='test@test.org'));
        
        // overlyLongScan
        testSet.add(new Zip_Upload__c(name='overlyLongScan-Test',Created__c=Datetime.now().addHours(-9),inProgress__c=True,ScanStarted__c=True,Username__c='test@test.org'));
        
        // tooOldUnworked
        testSet.add(new Zip_Upload__c(name='tooOldUnworked-Test',Created__c=Datetime.now().addHours(-5),inProgress__c=True,Username__c='test@test.org'));
        
        // workingLen, trigger sets Created__c
        testSet.add(new Zip_Upload__c(name='workingLen-Test1',ScanStarted__c=True,Username__c='test@test.org'));
        testSet.add(new Zip_Upload__c(name='workingLen-Test2',ScanStarted__c=True,Username__c='test@test.org'));
        testSet.add(new Zip_Upload__c(name='workingLen-Test3',ScanStarted__c=True,Username__c='test@test.org'));
        
        // queueLen, trigger sets Created__c
        testSet.add(new Zip_Upload__c(name='queueLen-Test1',Username__c='test@test.org'));
        testSet.add(new Zip_Upload__c(name='queueLen-Test2',Username__c='test@test.org'));
        testSet.add(new Zip_Upload__c(name='queueLen-Test3',Username__c='test@test.org'));
        testSet.add(new Zip_Upload__c(name='queueLen-Test4',Username__c='test@test.org'));
        testSet.add(new Zip_Upload__c(name='queueLen-Test5',Username__c='test@test.org'));
        testSet.add(new Zip_Upload__c(name='queueLen-Test6',Username__c='test@test.org'));
        testSet.add(new Zip_Upload__c(name='queueLen-Test7',Username__c='test@test.org'));
        
        insert testSet;
        
        run();
        */
    }   
}
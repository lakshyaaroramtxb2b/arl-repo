public class FreezeAccountController
{
    public String userId {set;get;}
    public String orgId {set;get;}
    public String ipAddr {set;get;}
    
    
    
    public void FreezeAccount()
    {   
        try
        {
            List<String> myList = new List<String> {ipAddr};
        
            // No longer a valid reference
            //ProductSecurity.freezeCompromisedUser(orgId, userId, myList);
        }
        catch(DmlException ex)
        {
            ApexPages.addMessages(ex);
        }
        
        ApexPages.Message sucessMsg = new ApexPages.Message(ApexPages.severity.info, 'Account has been locked');
        ApexPages.addMessage(sucessMsg);
    }       
}
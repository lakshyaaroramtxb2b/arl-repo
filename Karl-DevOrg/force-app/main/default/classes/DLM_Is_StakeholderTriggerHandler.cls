/***********************************************************
* Handler class for DLM_Is_StakeholderTrigger
* Created by    - Swarnima Singh Mandhata
* Date      - 20 July 2020
************************************************************/
public class DLM_Is_StakeholderTriggerHandler {
    public static void afterUpdate(List<IS_Stakeholder__c> newList, Map<id,IS_Stakeholder__c> oldMap){
        //DLM_Is_StakeholderTriggerHelper.isStakeholderContact(newList, oldMap);
    } 
    public static void beforInsert(List<IS_Stakeholder__c> newList){
        DLM_Is_StakeholderTriggerHelper.preventDuplicateStakeholders(newList);
        DLM_Is_StakeholderTriggerHelper.autoPoupulateManager(newList, null);
    }
     public static void beforeUpdate(List<IS_Stakeholder__c> newList,Map<id,IS_Stakeholder__c> oldMap){
        DLM_Is_StakeholderTriggerHelper.autoPoupulateManager(newList, oldMap);
    }
}
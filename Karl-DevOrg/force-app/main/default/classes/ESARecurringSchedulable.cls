public abstract class ESARecurringSchedulable implements Schedulable {

    private void abortCurrentAndScheduleNextExecution(SchedulableContext ctx, Integer intervalMins) {
        if (ctx != null) {
           System.abortJob(ctx.getTriggerId());
        }
        
        Datetime nextScheduleTime = System.now().addMinutes(intervalMins);
        String minute = String.valueof(nextScheduleTime.minute());
        String second = String.valueof(nextScheduleTime.second ());
        String cronvalue = second + ' ' + minute + ' * * * ?';
        String jobName = String.valueOf(this).split(':')[0] + nextScheduleTime.format('yyyy-MM-dd-HH:mm:ss');  
     
        System.schedule(jobName, cronvalue , this);
    }

    public void execute(SchedulableContext ctx) {
        try {
            executeRecurring(ctx);
        } catch (Exception e) {
            Esa_DebugService.WriteException(e, 'ESARecurringSchedulable', 'Exception in execute method');
        } finally {
            // Runs every 10 minutes
            abortCurrentAndScheduleNextExecution(ctx, getRecurringInterval() > 0 ? getRecurringInterval() : 10);
        }
    }

    abstract void executeRecurring(SchedulableContext ctx);

    abstract Integer getRecurringInterval();

}
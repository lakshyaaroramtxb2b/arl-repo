@isTest
public class SC_TrainingTrailheadConnectorTest {
    /*// Enroll a user in a training course
    static SC_Training_Provider__c provider;
    static Training_Course__c course;
    static Training_Course__c course2;
    static Training_Course__c otherProviderCourse;
    static Training_Course__c untrackedCourse;
    
    static User user1;
    static User user2;
    
    static Training_Course_Taken__c taken; // for user1 and course
    
    static Account account;
    
    

    static {
        setUpStatic();
    }
    
    public static void setUpStatic() {
        provider = new SC_Training_Provider__c();
        provider.Name = 'Trailhead';
        provider.Type__c = 'Trailhead';
        insert provider;
        
        course = SC_TrainingTestUtil.createCourseForProvider(provider);
        course.Provider_ID__c = '1234';
        course.Name = 'a known value';
        course.Tracked__c = true;
        update course;
        
        course2 = SC_TrainingTestUtil.createCourseForProvider(provider);
        course2.Provider_ID__c = '12345';
        course2.Name = 'another known value';
        course2.Tracked__c = true;
        update course2;
        
        // should be updated but Tracked__c shouldn't be changed
        untrackedCourse = SC_TrainingTestUtil.createCourseForProvider(provider);
        untrackedCourse.Provider_ID__c = '789';
        untrackedCourse.Name = 'untracked';
        untrackedCourse.Tracked__c = false;
        update untrackedCourse;
        
        // should be updated but Tracked__c shouldn't be changed
        otherProviderCourse = SC_TrainingTestUtil.createCourseForProvider(provider);
        otherProviderCourse.Provider_ID__c = 'otherprovider';
        otherProviderCourse.Provider__c = null;
        otherProviderCourse.Name = 'otherprovider';
        otherProviderCourse.Tracked__c = true;
        update otherProviderCourse;
        
        account = new Account(Name='users');
        insert account;
        // This 62 org ID has to jive with our test mock data
        user1 = SC_TrainingTestUtil.createPortalUser(account.Id, '00561000000hm59AAA');
        user2 = SC_TrainingTestUtil.createPortalUser(account.Id, '00561000000hm5BLAH'); // the real user 2 ID in the data is '00561000000hm59BCD'
        //mixed dml
        System.RunAs(user2) {
            insertEmailTeamplate();
        }

        taken = SC_TrainingTestUtil.createCourseTakenForCourse(user1.ContactId, course);
    }
    
    public static testMethod void test_enroll() {
        System.assert(provider != null);
        //public Training_Course_Taken__c enroll(Training_Course__c course, Contact contact, Id templateId, Date dueDate)
        SC_TrainingTrailheadConnector connector = (SC_TrainingTrailheadConnector) SC_TrainingUtil.connectorForProvider(provider);
        System.assert(connector != null);
        
        Contact c = [SELECT Id FROM Contact WHERE Id=: user2.ContactId];
        EmailTemplate template = [SELECT Id FROM EmailTemplate LIMIT 1];
        
        // User2 should not have a course taken for this course
        Training_Course_Taken__c courseTaken = connector.enroll(course, c, template.Id , Datetime.now().dateGMT().addDays(15), true);
        System.assert(courseTaken.Due_Date__c == Datetime.now().dateGMT().addDays(15));
        System.assert(courseTaken.Contact__c == c.Id);
        System.assert(courseTaken.RecordTypeId == SC_TrainingTestUtil.mandatoryType.Id);
        Id cid = courseTaken.Id;
        Integer count = Database.countQuery('SELECT count() FROM Task WHERE WhatId=: cid');
        System.assert(count > 0);
        
        // User2 should not have a course taken for this course either. Voluntary enrollment
        courseTaken = connector.enroll(course2, c, template.Id , null, false);
        System.assert(courseTaken.Due_Date__c == null);
        System.assert(courseTaken.Contact__c == c.Id);
        System.assert(courseTaken.RecordTypeId == SC_TrainingTestUtil.voluntaryType.Id);
        cid = courseTaken.Id;
        count = Database.countQuery('SELECT count() FROM Task WHERE WhatId=: cid');
        System.assert(count > 0);
        
        // User1 should  have a course taken for this course. It should be switched to mandatory and have the due date set
        taken.RecordTypeId = SC_TrainingTestUtil.voluntaryType.Id;
        taken.Due_Date__c = null;
        update taken;
        c = [SELECT Id FROM Contact WHERE Id=: user1.ContactId];
        courseTaken = connector.enroll(course, c, template.Id , Datetime.now().dateGMT().addDays(15), true);
        Id corId = course.Id;
        Id contId = c.Id;
        cid = courseTaken.Id;
        System.assert(1 == Database.countQuery('SELECT count() FROM Training_Course_Taken__c WHERE Contact__c =: contId AND Training_Course__c =: corId'));
        System.assert(courseTaken.Due_Date__c == Datetime.now().dateGMT().addDays(15));
        System.assert(courseTaken.Contact__c == c.Id);
        System.assert(courseTaken.RecordTypeId == SC_TrainingTestUtil.mandatoryType.Id);
        count = Database.countQuery('SELECT count() FROM Task WHERE WhatId=: cid');
        System.assert(count > 0);
        
        // Let's set it to be completed and make sure it's not touched
        courseTaken.Date_Completed__c = Datetime.now().dateGMT();
        courseTaken.Due_Date__c = null; // since it was voluntary
        courseTaken.Status__c = 'Course Completed';
        courseTaken.RecordTypeId = SC_TrainingTestUtil.voluntaryType.Id;
        update courseTaken;
        courseTaken = [SELECT Id, Date_Completed__c, Status__c, Training_Course__c, Due_Date__c, Contact__c, RecordTypeId FROM Training_Course_Taken__c WHERE Id=:courseTaken.Id];
        connector.enroll(course, c, template.Id , Datetime.now().dateGMT().addDays(15), true);
        Training_Course_Taken__c courseTaken2 = [SELECT Id, Status__c,Date_Completed__c,Training_Course__c, Due_Date__c, Contact__c, RecordTypeId FROM Training_Course_Taken__c WHERE Id=:courseTaken.Id];
        System.assert(1 == Database.countQuery('SELECT count() FROM Training_Course_Taken__c WHERE Contact__c =: contId AND Training_Course__c =: corId'));
        System.assert(courseTaken.Due_Date__c == null);
        System.assert(courseTaken.Contact__c == c.Id);
        System.assert(courseTaken.RecordTypeId == SC_TrainingTestUtil.voluntaryType.Id);
        System.assert(courseTaken.Date_Completed__c == courseTaken2.Date_Completed__c);
        System.assert(courseTaken.Status__c == courseTaken2.Status__c);
        System.assert(courseTaken.Training_Course__c == courseTaken2.Training_Course__c);
        
        // And switch it back again
        courseTaken.Date_Completed__c = null;
        courseTaken.Due_Date__c = null; // since it was voluntary
        courseTaken.Status__c = 'Course Started';
        courseTaken.RecordTypeId = SC_TrainingTestUtil.mandatoryType.Id;
        update courseTaken;
        
        // Leave of absence contact should have courses taken created but not have due dates or tasks
        delete taken;
        c.Is_On_Leave_Of_Absence__c = True;
        update c;
        courseTaken = connector.enroll(course, c, template.Id , Datetime.now().dateGMT().addDays(15), true);
        System.assert(courseTaken.Due_Date__c == null);
        System.assert(courseTaken.Contact__c == c.Id);
        System.assert(courseTaken.RecordTypeId == SC_TrainingTestUtil.mandatoryType.Id);
        cid = courseTaken.Id;
        count = Database.countQuery('SELECT count() FROM Task WHERE WhatId=: cid');
        System.assert(count == 0);

    }
    // Enroll a list of users in a training course
    public static testMethod void enroll_batch() {
        System.assert(course != null, 'course failed to save on setup');
        System.assert(course2 != null, 'cours2e failed to save on setup');
        System.assert(provider != null, 'provider failed to save on setup');
        System.assert(user1 != null, 'user1 failed to save on setup');
        System.assert(user2 != null, 'user2 failed to save on setup');

        Training_Course_Taken__c courseTaken;
        EmailTemplate template = [SELECT Id FROM EmailTemplate LIMIT 1];

        SC_TrainingTrailheadConnector connector = (SC_TrainingTrailheadConnector) SC_TrainingUtil.connectorForProvider(provider);
        System.assert(connector != null, 'the connector failed to load');
        
        SC_TrainingTrailheadConnector.BatchEnrollmentState[] states = new SC_TrainingTrailheadConnector.BatchEnrollmentState[]{
            (SC_TrainingTrailheadConnector.BatchEnrollmentState) connector.getBatchEnrollmentStateIterator(
                new Contact[]{ new Contact(Id = user2.ContactId) }
            ).iterator().next()
        };
        
        // User2 should not have a course taken for this course
        Id user2ContactId = user2.ContactId;
        System.assert(0 == [SELECT count() FROM Training_Course_Taken__c WHERE Contact__c = :user2ContactId AND Training_Course__c = :course.Id]);

        connector.enroll(course, states, template.Id , Datetime.now().dateGMT().addDays(15), true);

        // refresh courseTaken after connector.enroll()
        courseTaken = [
            SELECT Id, Due_Date__c, Contact__c, RecordTypeId
            FROM Training_Course_Taken__c
            WHERE Contact__c = :user2.ContactId
            AND Training_Course__c = :course.Id
        ];
        System.assert(courseTaken.Due_Date__c == Datetime.now().dateGMT().addDays(15));
        System.assert(courseTaken.Contact__c == user2.ContactId);
        System.assert(courseTaken.RecordTypeId == SC_TrainingTestUtil.mandatoryType.Id);
        System.assert([SELECT count() FROM Task WHERE WhatId = :courseTaken.Id] > 0);
        
        // User2 should not have a course taken for this course either. Voluntary enrollment
        System.assert(0 == [SELECT count() FROM Training_Course_Taken__c WHERE Contact__c = :user2.ContactId AND Training_Course__c = :course2.Id]);

        connector.enroll(course2, states, template.Id , null, false);

        // refresh courseTaken after connector.enroll()
        courseTaken = [
            SELECT Id, Due_Date__c, Contact__c, RecordTypeId
            FROM Training_Course_Taken__c
            WHERE Contact__c = :user2.ContactId
            AND Training_Course__c = :course2.Id
        ];
        System.assert(courseTaken.Due_Date__c == null);
        System.assert(courseTaken.Contact__c == user2.ContactId);
        System.assert(courseTaken.RecordTypeId == SC_TrainingTestUtil.voluntaryType.Id);

        System.assert([SELECT count() FROM Task WHERE WhatId = :courseTaken.Id] > 0);
        
        // User1 should  have a course taken for this course. It should be switched to mandatory and have the due date set
        taken.RecordTypeId = SC_TrainingTestUtil.voluntaryType.Id;
        taken.Due_Date__c = null;
        update taken;

        states = new SC_TrainingTrailheadConnector.BatchEnrollmentState[]{
            (SC_TrainingTrailheadConnector.BatchEnrollmentState) connector.getBatchEnrollmentStateIterator(
                new Contact[]{ new Contact(Id = user1.ContactId) 
            }).iterator().next()
        };        
        connector.enroll(course, states, template.Id , Datetime.now().dateGMT().addDays(15), true);

        // refresh courseTaken after connector.enroll()
        courseTaken = [
            SELECT Id, Training_Course__c, Due_Date__c, Contact__c, RecordTypeId
            FROM Training_Course_Taken__c
            WHERE Contact__c = :user1.ContactId
            AND Training_Course__c = :course.Id
        ];
        System.assert(1 == [SELECT count() FROM Training_Course_Taken__c WHERE Contact__c = :user1.ContactId AND Training_Course__c = :course.Id]);
        System.assert(courseTaken.Due_Date__c == Datetime.now().dateGMT().addDays(15));
        System.assert(courseTaken.Contact__c == user1.ContactId);
        System.assert(courseTaken.RecordTypeId == SC_TrainingTestUtil.mandatoryType.Id);
        System.assert([SELECT count() FROM Task WHERE WhatId = :courseTaken.Id] > 0);
        
        // Now we'll do a voluntary enrollment and switch it back
        states = new SC_TrainingTrailheadConnector.BatchEnrollmentState[]{
            (SC_TrainingTrailheadConnector.BatchEnrollmentState) connector.getBatchEnrollmentStateIterator(
                new Contact[]{ new Contact(Id = user1.ContactId) }
            ).iterator().next()
        };
        connector.enroll(course, states, template.Id , null, false);

        // refresh courseTaken after connector.enroll()
        courseTaken = [
            SELECT Id, Date_Completed__c,Status__c,Training_Course__c,Due_Date__c, Contact__c, RecordTypeId
            FROM Training_Course_Taken__c
            WHERE Id = :courseTaken.Id
        ];
        System.assert(1 == [SELECT count() FROM Training_Course_Taken__c WHERE Contact__c = :user1.ContactId AND Training_Course__c = :course.Id]);
        System.assert(courseTaken.Due_Date__c == null);
        System.assert(courseTaken.Contact__c == user1.ContactId);
        System.assert(courseTaken.Training_Course__c == course.Id);
        System.assert(courseTaken.RecordTypeId == SC_TrainingTestUtil.voluntaryType.Id);
        System.assert(courseTaken.Training_Course__c == courseTaken.Training_Course__c);
        System.assert([SELECT count() FROM Task WHERE WhatId = :courseTaken.Id] > 1);
        
        // Let's set it to be completed and make sure it's not touched
        courseTaken.Date_Completed__c = Datetime.now().dateGMT();
        courseTaken.Due_Date__c = null; // since it was voluntary
        courseTaken.Status__c = 'Course Completed';
        courseTaken.RecordTypeId = SC_TrainingTestUtil.voluntaryType.Id;
        update courseTaken;
        states = new SC_TrainingTrailheadConnector.BatchEnrollmentState[]{
            (SC_TrainingTrailheadConnector.BatchEnrollmentState) connector.getBatchEnrollmentStateIterator(
                new Contact[]{ new Contact(Id = user1.ContactId) }
            ).iterator().next()
        };
        connector.enroll(course, states, template.Id , Datetime.now().dateGMT().addDays(15), true);
        
        // refresh courseTaken after connector.enroll()
        courseTaken = [
            SELECT Id, Date_Completed__c, Status__c,Training_Course__c, Due_Date__c, Contact__c, RecordTypeId
            FROM Training_Course_Taken__c
            WHERE Id = :courseTaken.Id
        ];
        System.assert(1 == [SELECT count() FROM Training_Course_Taken__c WHERE Contact__c = :user1.ContactId AND Training_Course__c = :course.Id]);
        System.assert(courseTaken.Due_Date__c == null);
        System.assert(courseTaken.Contact__c == user1.ContactId);
        System.assert(courseTaken.RecordTypeId == SC_TrainingTestUtil.voluntaryType.Id);
        System.assert(courseTaken.Date_Completed__c == courseTaken.Date_Completed__c);
        System.assert(courseTaken.Status__c == courseTaken.Status__c);
        System.assert(courseTaken.Training_Course__c == courseTaken.Training_Course__c);
        
        // And switch it back again
        courseTaken.Date_Completed__c = null;
        courseTaken.Due_Date__c = null; // since it was voluntary
        courseTaken.Status__c = 'Course Started';
        update courseTaken;
        
        // Leave of Absence contact should have courses taken created but not have due dates or tasks
        delete taken;
        update new Contact(
            Id = user1.ContactId,
            Is_On_Leave_Of_Absence__c = True
        );

        connector.enroll(course, states, template.Id , Datetime.now().dateGMT().addDays(15), true);
        courseTaken = [
            SELECT Id, Due_Date__c, Contact__c, RecordTypeId
            FROM Training_Course_Taken__c
            WHERE Contact__c = :user1.ContactId
            AND Training_Course__c = :course.Id
        ];
        System.assert(courseTaken.Due_Date__c == null);
        System.assert(courseTaken.Contact__c == user1.ContactId);
        System.assert(courseTaken.RecordTypeId == SC_TrainingTestUtil.mandatoryType.Id);
        System.assert([SELECT count() FROM Task WHERE WhatId = :courseTaken.Id] == 0);

        // they're already enrolled as mandatory, check that the due date isn't changed and they're not re-enrolled
        update new Contact(
            Id = user1.ContactId,
            Is_On_Leave_Of_Absence__c = False
        );

        courseTaken.Due_Date__c = Datetime.now().dateGMT().addDays(15);
        courseTaken.RecordTypeId = SC_TrainingTestUtil.mandatoryType.Id;
        update courseTaken;

        Integer beforeEnrollTaskCount = [SELECT count() FROM Task WHERE WhatId = :courseTaken.Id];
        
        Test.startTest();
        connector.enroll(course, states, template.Id , Datetime.now().dateGMT().addDays(30), true);
        Test.stopTest();
        System.assert(1 == [SELECT count() FROM Training_Course_Taken__c WHERE Contact__c = :user1.ContactId AND Training_Course__c = :course.Id]);

        courseTaken = [SELECT Id, Due_Date__c, Contact__c, RecordTypeId FROM Training_Course_Taken__c WHERE Id = :courseTaken.Id];
        System.assert(courseTaken.Due_Date__c == Datetime.now().dateGMT().addDays(15), courseTaken.Due_Date__c);
        System.assert(courseTaken.Contact__c == user1.ContactId);
        System.assert(courseTaken.RecordTypeId == SC_TrainingTestUtil.mandatoryType.Id);

        System.assert(beforeEnrollTaskCount == [SELECT count() FROM Task WHERE WhatId = :courseTaken.Id]);
    }
    
    // Unenroll a user in a training course
    public static testMethod void test_unenroll() {
        // SC_TrainingTrailheadConnector.unenroll(Training_Course__c course, Contact user) 
    }
    
    // Unenroll a list of users in a training course
    public static testMethod void test_unenroll_list() {
        // SC_TrainingTrailheadConnector.unenroll(Training_Course__c course, states)
        System.assert(provider != null);

        SC_TrainingTrailheadConnector connector = (SC_TrainingTrailheadConnector) SC_TrainingUtil.connectorForProvider(provider);
        System.assert(connector != null);
        
        Contact c = [SELECT Id, Email FROM Contact WHERE Id=: user2.ContactId];
        
        SC_TrainingTrailheadConnector.BatchUnenrollmentState[] states = new SC_TrainingTrailheadConnector.BatchUnenrollmentState[]{(SC_TrainingTrailheadConnector.BatchUnenrollmentState) connector.getBatchUnenrollmentStateIterator(new Contact[]{c}).iterator().next()};
        
        Id corId, contId, cid;
        corId = course.Id;
        contId = c.Id;
        // User2 should not have a course taken for this course
        System.assert(0 == Database.countQuery('SELECT count() FROM Training_Course_Taken__c WHERE Contact__c =: contId AND Training_Course__c =: corId'));
        connector.unenroll(course, states);
        Training_Course_Taken__c courseTaken = [SELECT Id, Due_Date__c, Contact__c, RecordTypeId FROM Training_Course_Taken__c WHERE Contact__c =: contId AND Training_Course__c =: corId];
        System.assert(courseTaken.Due_Date__c == null);
        System.assert(courseTaken.Contact__c == c.Id);
        System.assert(courseTaken.RecordTypeId == SC_TrainingTestUtil.voluntaryType.Id);
        cid = courseTaken.Id;
        
        // User1 should  have a course taken for this course. It should be switched to mandatory and have the due date set
        taken.RecordTypeId = SC_TrainingTestUtil.mandatoryType.Id;
        taken.Due_Date__c = Datetime.now().dateGMT().addDays(10);
        update taken;
        c = [SELECT Id, Email FROM Contact WHERE Id=: user1.ContactId];
        states = new SC_TrainingTrailheadConnector.BatchUnenrollmentState[]{(SC_TrainingTrailheadConnector.BatchUnenrollmentState) connector.getBatchUnenrollmentStateIterator(new Contact[]{c}).iterator().next()};        
        connector.unenroll(course, states);
        corId = course.Id;
        contId = c.Id;
        courseTaken = [SELECT Id, Due_Date__c, Contact__c, RecordTypeId FROM Training_Course_Taken__c WHERE Contact__c =: contId AND Training_Course__c =: corId];
        cid = courseTaken.Id;
        System.assert(1 == Database.countQuery('SELECT count() FROM Training_Course_Taken__c WHERE Contact__c =: contId AND Training_Course__c =: corId'));
        System.assert(courseTaken.Due_Date__c == null);
        System.assert(courseTaken.Contact__c == c.Id);
        System.assert(courseTaken.RecordTypeId == SC_TrainingTestUtil.voluntaryType.Id);
    }

    // Sync courses completed on or after provided date from the training provider to this org
    public static testMethod void test_syncCompleted() {
        // We didn't track this course for syncCourses but we want to here. We use very similar test data
        // and this will be the course that they've started but it's our first time seeing the Training_Course_Taken__c for
        Training_Course__c course3 = SC_TrainingTestUtil.createCourseForProvider(provider);
        course3.Provider_ID__c = '1234';
        course3.Name = 'a known value';
        course3.Tracked__c = true;
        course3.Course_Level__c = 'Module';
        update course3;
        
        System.assert(provider != null);

        SC_TrainingTrailheadConnector connector = (SC_TrainingTrailheadConnector) SC_TrainingUtil.connectorForProvider(provider);
        System.assert(connector != null);

        Contact user1Contact = new Contact(
            Id = user1.ContactId,
            Org62_User_ID__c = '00561000000hm59AAA'
        );
        update user1Contact;
        
        // Two users. One that exists and another one that doesn't exist yet.
        // The first (main) user has 1) an existing, tracked course taken record that needs to be updated to show complete
        // a tracked course taken that needs to be created to show in-progress, then a third that's not tracked
        String response = '{"total_count": 2, "data": ' +
            '[{"username": "sobyx+sfdc.2aaa9a593@trailhead.salesforce.com", "first_name": "brian", "last_name": "soby", "country": null, "company": null, "modules": [' +
                '{' +
                    '"status": "Completed", "archived": false, "badge_title": "Some New Course badge", "api_name": "1234",' +
                    '"description_html": "Some New Course Description", "first_attempted_at": "2017-02-28T20:33:19.712Z", ' +
                    '"finished_at": "2017-03-28T20:33:19.712Z", "completed": true, "title": "Some New Course", "updated_at": "2017-02-24T18:50:49.804Z", ' +
                    '"last_attempted_at": "2017-03-28T20:33:19.712Z", "id": 1234, "built_date": "2017-02-23T21:30:06Z", ' +
                    '"web_url": "https://acme.com/course", "badge_icon": "https://developer.salesforce.com/badge", "created_at": "2016-06-30T18:21:33.224Z"' +
                '},' +
                '{' +
                    '"status": "In-Progress", "archived": false, "badge_title": "Some New Course badge 2", "description_html": "Some New Course Description 2", ' +
                    '"first_attempted_at": "2017-02-28T20:33:19.712Z", "finished_at": null, "completed": false, "title": "Some New Course 2", ' +
                    '"updated_at": "2017-02-24T18:50:49.804Z", "last_attempted_at": "2017-02-28T20:33:19.712Z", "id": 234, "built_date": "2017-02-23T21:30:06Z", ' +
                    '"web_url": "https://acme.com/course2", "badge_icon": "https://developer.salesforce.com/badge2", "created_at": "2016-06-30T18:21:33.224Z"' +
                '},' +
                '{' +
                    '"status": "In-Progress", "archived": true, "badge_title": "Some New Course badge 2", "description_html": "Some New Course Description 2", ' +
                    '"first_attempted_at": "2017-02-28T20:33:19.712Z", "finished_at": null, "completed": false, "title": "an untracked course", ' +
                    '"updated_at": "2017-02-24T18:50:49.804Z", "last_attempted_at": "2017-02-28T20:33:19.712Z", "id": 789, "built_date": "2017-02-23T21:30:06Z", ' +
                    '"web_url": "https://acme.com/untracked", "badge_icon": "https://developer.salesforce.com/badge2", "created_at": "2016-06-30T18:21:33.224Z"' +
                '}' +
                '], ' +
                '"email": "sobyx+sfdc@hotmail.com", "postal_code": null, "avatar": null, "salesforce_ids": [{"user_id": "00561000000hm59AAA", "org_id": "00D000000000062EAA"}], "id": 5251524}, ' +
            '{"username": "other_user@salesforce.com", "first_name": "brian", "last_name": "soby", ' +
            '"country": null, "company": null, "modules": [{"status": "Completed", "archived": false, "badge_title": "Some New Course badge", ' +
            '"description_html": "Some New Course Description", "first_attempted_at": "2017-02-28T20:33:19.712Z", "finished_at": "2017-02-28T20:33:19.712Z", ' +
            '"completed": true, "title": "Some New Course", "updated_at": "2017-02-24T18:50:49.804Z", "last_attempted_at": "2017-02-28T20:33:19.712Z", ' +
            '"id": 234, "built_date": "2017-02-23T21:30:06Z", "web_url": "https://acme.com/course", "badge_icon": "https://developer.salesforce.com/badge", ' +
            '"created_at": "2016-06-30T18:21:33.224Z"}, {"status": "In-Progress", "archived": false, "badge_title": "Some New Course badge 2", ' +
            '"description_html": "Some New Course Description 2", "first_attempted_at": "2017-02-28T20:33:19.712Z", "finished_at": null, ' +
            '"completed": false, "title": "Some New Course 2", "updated_at": "2017-02-24T18:50:49.804Z", "last_attempted_at": "2017-02-28T20:33:19.712Z", ' +
            '"id": 1234, "built_date": "2017-02-23T21:30:06Z", "web_url": "https://acme.com/course2", "badge_icon": "https://developer.salesforce.com/badge2", ' +
            '"created_at": "2016-06-30T18:21:33.224Z"}, {"status": "In-Progress", "archived": true, "badge_title": "Some New Course badge 2", ' +
            '"description_html": "Some New Course Description 2", "first_attempted_at": "2017-02-28T20:33:19.712Z", "finished_at": null, ' +
            '"completed": false, "title": "an untracked course", "updated_at": "2017-02-24T18:50:49.804Z", "last_attempted_at": "2017-02-28T20:33:19.712Z", ' +
            '"id": 789, "built_date": "2017-02-23T21:30:06Z", "web_url": "https://acme.com/untracked", "badge_icon": "https://developer.salesforce.com/badge2", ' +
            '"created_at": "2016-06-30T18:21:33.224Z"}], "email": "other_user@salesforce.com", "postal_code": null, "avatar": null, ' +
            '"salesforce_ids": [{"user_id": "00561000000hm59BCD", "org_id": "00D000000000062EAA"}], "id": 99999999}]}';

        SC_TrainingTestUtil.MockHttpResponseGenerator mock = new SC_TrainingTestUtil.MockHttpResponseGenerator(response);
        Test.setMock(HttpCalloutMock.class, mock);
        
        Test.startTest(); 
        // This will make a callout to figure out how many records there are. Two records from our test set should
        // only have one state created
        SC_TrainingBase.CoursesCompletedState[] states = (SC_TrainingBase.CoursesCompletedState[])connector.getCoursesCompletedStateIterator();
        System.assert(1 == states.size());
        System.assert(0 == ((SC_TrainingTrailheadConnector.CoursesCompletedState) states.get(0)).offset);
        
        // get a fresh one
        states = (SC_TrainingBase.CoursesCompletedState[])connector.getCoursesCompletedStateIterator();
        Integer ret = connector.syncCompleted(states);
        Test.stopTest();

        System.assert(ret == 0);
        
        user1Contact = [
            SELECT Id, Org62_User_ID__c,
            (
                SELECT
                    Id,
                    Date_Completed__c,
                    Date_Started__c,
                    Last_Import_Date__c,
                    Progress__c,
                    Status__c,
                    Training_Course__c,
                    Training_Course__r.Provider_ID__c
                FROM Training_Courses_Taken__r
                WHERE Training_Course__r.Provider__c = 'Trailhead'
            )
            FROM Contact
            WHERE Id =: user1.contactId
        ];

        for (Training_Course_Taken__c courseTaken : user1Contact.Training_Courses_Taken__r) {
            if (courseTaken.Training_Course__r.Provider_ID__c == '1234') {
                // This is the existing Training_Course_Taken__c that should have been updated to complete
                System.assert(courseTaken.Date_Completed__c != null);
                System.assert(courseTaken.Last_Import_Date__c == Datetime.now().dateGMT());
                System.assert(courseTaken.Progress__c == 100);
                System.assert(courseTaken.Status__c == 'Course Completed');
                System.assert(courseTaken.Training_Course__c == course.Id);
                
            } else if (courseTaken.Training_Course__r.Provider_ID__c == '234') {
                // This is a tracked course but our first time seeing a record of them being in progress. A new
                // Training_Course_Taken__c should have been created
                // This is the existing Training_Course_Taken__c that should have been updated to complete
                System.assert(courseTaken.Date_Completed__c == null);
                System.assert(courseTaken.Last_Import_Date__c == Datetime.now().dateGMT());
                System.assert(courseTaken.Progress__c != 100);
                System.assert(courseTaken.Status__c == 'Course Started');
                System.assert(courseTaken.Training_Course__c == course3.Id);
            } else {
                // stray training_course_token__c
                System.assert(false);
            }
        }                                 
        Contact user2Contact = [
            SELECT Id, Org62_User_ID__c,
            (
                SELECT 
                    Id,
                    Date_Completed__c,
                    Date_Started__c,
                    Last_Import_Date__c,
                    Progress__c,
                    Status__c,
                    Training_Course__c,
                    Training_Course__r.Provider_ID__c
                FROM Training_Courses_Taken__r
                WHERE Training_Course__r.Provider__c = 'Trailhead'
            )
            FROM Contact
            WHERE Id =: user2.contactId
        ];
        System.assert(0 == user2Contact.Training_Courses_Taken__r.size());
    }

    @future
    private static void insertEmailTeamplate()
    {
        EmailTemplate newtemplate = new EmailTemplate(Name = 'test', FolderId = UserInfo.getUserId(), TemplateType= 'text', DeveloperName='test');
        insert newtemplate;
    }
*/
}
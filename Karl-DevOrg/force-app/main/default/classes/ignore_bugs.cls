public class ignore_bugs {
    public List<bug_management__c> bug_wrapper;
    public boolean displayPopup {get; set;} 
    public bug_management__c current_bug;
    public string wrap_ref{get; set;}
    public string current_user {get; set;}
    public string state {get;set;}
    public string cloud {get; set;}
    public string comment {get; set;}
    public boolean input_RF {get;set;}
    public boolean input_wror {get;set;}
    public boolean input_ignore {get;set;}
    public string[] Selected_value {get;set;}
    public boolean RF {get; set;}
    public boolean WRAP {get; set;}
    public boolean LATEST {get; set;}
    public boolean HALF_LIFE {get; set;}
    
    //String[] Selected_value{get;set;} = new String[]{};

    //public list<string> Selected_value {get; set;}
    public Map<string,bug_management__c> bug_map;
    
    
    
    public PageReference  refresh(){
        system.debug(Selected_value);
        return null;
    }
    
    public PageReference refresh_gus(){
        
        try{
        
        bug_management.start();

        }
        catch(exception e){
        ApexPages.getMessages().clear();
        ApexPages.Message val = new ApexPages.Message(ApexPages.Severity.ERROR,string.valueof(e));
        ApexPages.addMessage(val);
        }
        
        return null;
        
    }
    

    
    private void insert_bug(bug_management__c x){
        bug_wrapper.add(x); 
    }
    
    
   //Returns the list back to the user 
    public List<bug_management__c> getbug_list(){
        
        List<bug_management__c> first_list = new List<bug_management__c>();
        try
        {
      
        
        bug_wrapper = new List<bug_management__c>();
        bug_map = new Map<string,bug_management__c>();    
        string value = 'SELECT id, Gus_work_id__c, Name, Bug_Title__c, Cloud_name__c, Age__c, Status__c, Priority__c ,Assigned_to__c,Action_item__c,ignore__c, Theme_list__c, Comment__c, RF_blocker__c, WROR__c, Prodsec_owner__c FROM bug_management__c' +
            ' Where Closed__c = 0 AND RF_Blocker__c = TRUE AND comment_5__c = FALSE AND Action_item__c includes (\'Bug is Over Half Life\') ORDER BY Prodsec_owner__c ';    
            
        
       
        first_list = database.query(value);
        
       for(bug_management__c x: first_list ){
            if(String.isNotBlank(string.valueof(x.Theme_list__c)))
           		if((x.Theme_list__c).contains('Prodsec Reviewed'))
               		continue;
            if(string.isNotBlank(x.Comment__c)){
            	string local_com = x.Comment__c.escapeHtml4();
            	x.comment__c = local_com.replaceall('~', '<br/><br/>');
            }
            if(string.isNotBlank(x.Action_item__c)){
                string action = x.Action_item__c.escapeHtml4();
                x.Action_item__c = action.replaceall(';', '<br/><br/>');
                }
            
            bug_map.put(string.valueof(x.get('Id')), x);
            
            }
        
     
            
               }
       catch(exception e){
                  system.debug(e);
              }
        //Message on the page
        Prodsec_bug_manages__c  mc = Prodsec_bug_manages__c.getInstance('00e30000001D19S');
        ApexPages.getMessages().clear();
        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Last Sync with GUS :' + string.valueof(mc.get('last_sync__c')) );
        ApexPages.addMessage(myMsg);
        
        

        return bug_map.values();
        
        
    }
    

    
    
    
    
    public void comment_method(){
        displayPopup = true; 
        current_bug=bug_map.get(wrap_ref);
        input_RF=boolean.valueof(current_bug.get('RF_Blocker__c'));
        input_wror=boolean.valueof(current_bug.get('WROR__c'));
        input_ignore=boolean.valueof(current_bug.get('ignore__c'));
        comment = null;
        system.debug(current_bug);

        
    }
    
    public List<SelectOption> getOptionsList()
    {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('RF','SHOW ONLY RF BLOCKER BUGS'));
        options.add(new SelectOption('WROR','SHOW ONLY WRAP ESCALATION BUGS'));
        options.add(new SelectOption('LATEST','SHOW ONLY BUGS WITH NO LATEST UPDATES IN PAST 5 Days'));
        options.add(new SelectOption('Half_life','SHOW ONLY BUGS WITH AGE MORE THAN HALF OF SLA'));
        return options;
    } 
    
    
    public void close_update() {  
        try{
        if(string.isBlank(comment))
            update new bug_management__c(id = id.valueof(wrap_ref),RF_Blocker__c=input_RF,WROR__c=input_wror);
        else
        {
         string old_comment = current_bug.Comment__c;
         if(!string.isBlank(old_comment)){
             old_comment= old_comment.replaceall('<br/><br/>','~').unescapeHtml4();
             comment = userinfo.getName() + '- ' + comment + ' (' + date.today().format() +')'  + '~' + old_comment;}
		else
            comment = userinfo.getName() + '- ' + comment + ' (' + date.today().format() +')';

            
        update new bug_management__c(id = id.valueof(wrap_ref),RF_Blocker__c=input_RF,WROR__c=input_wror,ignore__c=input_ignore, Comment__c=comment, Comment_date__c=date.today());
           
        }
        displayPopup = false; 
        ApexPages.getMessages().clear();
        ApexPages.Message val = new ApexPages.Message(ApexPages.Severity.INFO,'Updated the status for ' + current_bug.get('Name'));
        ApexPages.addMessage(val);
        }
        catch(exception e)
        {
            system.debug(e);
         	displayPopup = false; 
            ApexPages.getMessages().clear();
        	ApexPages.Message val = new ApexPages.Message(ApexPages.Severity.ERROR,'Updated Failed for ' + current_bug.get('Name'));
        	ApexPages.addMessage(val);
            
        }
     
    }   
    
        
    
    public void closePopup() {        
        displayPopup = false;  
     
    }     
    
     public void showPopup() { 
        
        displayPopup = true; 
    }
    


}
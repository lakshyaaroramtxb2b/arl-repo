public with sharing class TD_Deliverable_Form {

    // properties
    public Trust_Deliverable__c deliverable {get; set;}
    public List<StakeholderRACI> stakeholders {get; set;}
    
    public static final List<SelectOption> RACI_LEVELS {get; private set;}
    public static final List<SelectOption> VALID_STAKEHOLDERS {get; private set;}
    private static Boolean IS_MOBILE {set;
        get { if (IS_MOBILE == null) {              
                try  {        
                    IS_MOBILE = isAgentMobile(ApexPages.currentPage().getHeaders().get('User-Agent'));  
                } catch (exception e) {
                    IS_MOBILE = false;
                }
              }
              return IS_MOBILE;         
        }
    }
    public Boolean hasErrors {get; private set;} {hasErrors = false;}
    
    // variables
    private static final String NONE = '--None--';
    public static final String RESPONSIBLE = 'Responsible';
    public static final String ACCOUNTABLE = 'Accountable';
    public static final String STATUS_ONHOLD = 'On Hold';
    public static final String STATUS_CANCELLED = 'Cancelled';
    private ApexPages.StandardController stdController;
    private Id deliverableId;
    private String retURL;
    private String userAgent;
    
    private static Map<Id, User> validStakeholderMap = new Map<Id, User>();
    
    // static variable initialization block
    static { 
        RACI_LEVELS = new List<SelectOption>();     
        List<Schema.PicklistEntry> pl = 
           Trust_Deliverable_Stakeholder__c.RACI_Level__c.getDescribe().getPicklistValues();
        for (Schema.PicklistEntry e : pl) {
           RACI_LEVELS.add(new SelectOption(e.getLabel(), IS_MOBILE? e.getLabel().substring(0,1) : (e.getLabel() + ' ')));
        }

        VALID_STAKEHOLDERS = new List<SelectOption>();
        VALID_STAKEHOLDERS.add(new SelectOption(NONE, NONE));  
        for (User u : [select Id, Name, Email from User 
                        where Id IN (select Stakeholder__c 
                        from Trust_Valid_Stakeholder__c) ORDER BY Name]) {           
           VALID_STAKEHOLDERS.add(new SelectOption(u.Id, u.Name));
           validStakeholderMap.put(u.Id, u);                            
        }  
    }
    
    // instance class variables
    private map<String, String> queryParms;
    private map<String, String> httpHeaders;

    private TD_gmailInterface googleAPI = new TD_gmailInterface();
    
    // constructors    
    public TD_Deliverable_Form() {
        
    }
    
    public TD_Deliverable_Form(ApexPages.StandardController stdController) {
        
        this.stdController = stdController;
        
        // get query parms and http headers
        try  {        
            queryParms = ApexPages.currentPage().getParameters();  
        } catch (exception e) {
            queryParms = new map<String, String>();
        } 
        
        try  {        
            httpHeaders = ApexPages.currentPage().getHeaders();  
        } catch (exception e) {
            httpHeaders = new map<String, String>();
        } 

        if (queryParms.containsKey('retURL')) retURL = queryParms.get('retURL'); 
        
        // if retURL is blank try to get the referer instead
        if (string.isBlank(retURL)) {
            if (httpHeaders.containsKey('Referer')) retURL = httpHeaders.get('Referer');    
        }
        
        // get existing data
        
        // deliverable
        deliverable = new Trust_Deliverable__c();
        if (queryParms.containsKey('Id')) {
            deliverableId = Id.valueOf(queryParms.get('Id').trim());
            try {
                deliverable = [select Id, Status__c, Name, Due_Date__c, Description__c, Special_Instructions__c, GCal_EventId__c  
                                from Trust_Deliverable__c
                                where Id = :deliverableId];
            } catch (exception e) {
                system.debug('Exception thrown and caught: ' + e);
            }
        }
        
        // stakeholders
        stakeholders = new List<StakeholderRACI>();
        if (deliverable.Id != null) {
            try {
                for (Trust_Deliverable_Stakeholder__c s: [select Id, Stakeholder__c, RACI_Level__c, 
                               Stakeholder__r.Name from Trust_Deliverable_Stakeholder__c 
                               where Deliverable__c = : deliverable.Id]) {
                stakeholders.add(new StakeholderRACI(s));
                    
                }
            } catch (exception e) {
                system.debug('Exception thrown and caught: ' + e);
            }           
        }
        addStakeholder();
    }
    
    // action methods
    public pageReference redirectIfNotMobile() {
        if (!IS_MOBILE) return Page.TD_Deliverable_Form;
        else return null;
    }
    
    
    public void addStakeholder() {
        stakeholders.add(new StakeholderRACI());
    }
    
    public pageReference save() {
        hasErrors = false;
        if (validate()) {
            try {

                // first try to update google calendar
                try {
                    deliverable.GCal_EventId__c = updateGoogleCalendarEvent();  
                } catch (exception e) {
                    system.debug(e);             
                }    

                upsert deliverable;
                saveStakeholders();
                
                // reload the page or return if retURL specified
                if (IS_MOBILE) return null; // navigation handled in page
                PageReference newPage;
                if(string.isBlank(retURL)) {
                    newPage = page.TD_Deliverable_Form;
                    if (deliverableId != null) newPage.getParameters().put('Id', deliverableId);
                    newPage.setRedirect(true);
                } else { 
                    newPage = new PageReference(retURL);
                }
                return newPage;
                
                
            } catch (DmlException e) {
                ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.FATAL, 
                    e.getDmlMessage(0));
                ApexPages.addMessage(errorMsg);
            } catch (exception e) {
                ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.FATAL, 
                    'Exception received while trying to save. \n Exception: \n' + e);
                ApexPages.addMessage(errorMsg);                                             
            }
        }

        ApexPages.currentPage().getParameters().put('wheretogo', 'errorsfound');
        hasErrors = true;
        return null;        
        
    }
    
    public PageReference cancel() {
        PageReference newPage;
        if (IS_MOBILE) return null; // navigation handled in page
        if (string.isBlank(retURL)) {
            newPage = page.TD_Deliverable_Form;
            if (deliverableId != null) newPage.getParameters().put('Id', deliverableId);
            newPage.setRedirect(true);
        } else { 
            newPage = new PageReference(retURL);
        }
        return newPage;
     }
    
    // service methods
    private Boolean validate() {
        Boolean valid = true;
        set<String> uniqueIds = new set<String>();

        // no duplicate stakeholders
        uniqueIds = new set<String>();
        for (StakeholderRACI s : stakeholders) {
            if (uniqueIds.contains(s.stakeholderId)) {
                ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.FATAL, 'Please remove any duplicate stakeholders. ');
                ApexPages.addMessage(errorMsg);
                valid = false;
            } else {
                if(s.stakeholderId != NONE) uniqueIds.add(s.stakeholderId);
            }
        }

        // only 1 accountable stakeholder
        Integer accountable_Count = 0;
        for (StakeholderRACI s : stakeholders) {
            if ((s.stakeholderId != NONE) && string.join(s.raciLevel,';').contains(ACCOUNTABLE)) accountable_Count++;
        }
        
        if (accountable_Count > 1) {
            ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.FATAL, 'Only one stakeholder can be set as \'' + ACCOUNTABLE + '\'. ');
            ApexPages.addMessage(errorMsg);
            valid = false;
        } else if (accountable_Count == 0) {
            ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.FATAL, 'At least one stakeholder must be set as \'' + ACCOUNTABLE + '\'. ');
            ApexPages.addMessage(errorMsg);
            valid = false;
        } 
        
        // at least one raci level must be specified
        for (StakeholderRACI s : stakeholders) {
            if ((s.stakeholderId != NONE) && string.isBlank(string.join(s.raciLevel,';'))) {
                ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.FATAL, 'Please specify at least one RACI Level for each stakeholder.');
                ApexPages.addMessage(errorMsg);
                valid = false;
                break;
            }
        }
            

        return valid;
    }
    
    private void saveStakeholders() {
        List<Trust_Deliverable_Stakeholder__c> upsertStakeholders = new List<Trust_Deliverable_Stakeholder__c>();
        List<Trust_Deliverable_Stakeholder__c> deleteStakeholders = new List<Trust_Deliverable_Stakeholder__c>();
        for (StakeholderRACI s : stakeholders) {
            if (s.stakeholderId != NONE) {
                Trust_Deliverable_Stakeholder__c upsertStakeholder;
                // if this is an update use the current stakeholder record else this is a new one and delete the old one
                if (id.valueOf(s.stakeholderId) == s.stakeholder.Stakeholder__c) {
                    upsertStakeholder = s.stakeholder;
                } else {
                    upsertStakeholder = new Trust_Deliverable_Stakeholder__c(Deliverable__c = deliverable.Id);
                    if (s.stakeholder.Id != null) deleteStakeholders.add(s.stakeholder);
                }
                upsertStakeholder.Stakeholder__c = s.stakeholderId;
                upsertStakeholder.RACI_Level__c = string.join(s.raciLevel,';');
                upsertStakeholders.add(upsertStakeholder);
            } else {
                if (s.stakeholder.Id != null) deleteStakeholders.add(s.stakeholder);
            }
        }
        if (!upsertStakeholders.isEmpty()) upsert upsertStakeholders;           
        if (!deleteStakeholders.isEmpty()) delete deleteStakeholders;           
    }
    
    // from http://salesforce.stackexchange.com/questions/33514/detect-if-visualforce-page-is-in-salesforce1
    private static boolean isAgentMobile(String userAgentString) {
        //Using RegEx, figure out if the user is on a mobile device based on the user-agent string
        if (userAgentString == null)  return false;
        Pattern p = Pattern.compile('Mobile|iP(hone|od|ad)|Android|BlackBerry|IEMobile|Kindle|NetFront|Silk-Accelerated|(hpw|web)OS|Fennec|Minimo|Opera M(obi|ini)|Blazer|Dolfin|Dolphin|Skyfire|Zune');
        Matcher pm = p.matcher( userAgentString );  
    return pm.find();
    }

    private string updateGoogleCalendarEvent() {        
        // depending on the status either delete or update the gcal event
        if (deliverable.Status__c == STATUS_ONHOLD ||
            deliverable.Status__c == STATUS_CANCELLED) {
                system.debug('deliverable.GCal_EventId__c' + deliverable.GCal_EventId__c);
                deleteGoogleCalendarEvent(deliverable.GCal_EventId__c);
                return null;
                
        } else {
              
            Datetime eventDate = deliverable.Due_Date__c;
            Set<String> email = new Set<String>();
            for (StakeholderRACI s : stakeholders) {
                if (s.stakeholderId != NONE) {
                    email.add(validStakeholderMap.get(s.stakeholderId).Email);
                }
            }
            
            String description = deliverable.Description__c + deliverableLink();
            
            googleAPI.insertOreditCalendarEvent(
                          deliverable.Name, 
                          new List<String>(email), 
                          description, 
                          deliverable.Due_Date__c, 
                          deliverable.GCal_EventId__c);
    
            return googleAPI.eventIDString;        
        }       
    }

    private void deleteGoogleCalendarEvent(String event_id) {        
        if (event_id != null) googleAPI.deleteEvent(event_id);
    }

    @future (callout=true)
    public static void deleteGoogleCalendarEvents(Set<Id> deliverableIds) {
        
        TD_gmailInterface googleAPI = new TD_gmailInterface(); 
        for (Trust_Deliverable__c deliverable : [select GCal_EventId__c from Trust_Deliverable__c
                                                   where Id IN : deliverableIds all rows]) {       
            if (deliverable.GCal_EventId__c != null) googleAPI.deleteEvent(deliverable.GCal_EventId__c);
        }
    }
    
    // internal classes
    private class StakeholderRACI {
        // properties
        public Trust_Deliverable_Stakeholder__c stakeholder;
        public String stakeholderId {get; set;}
        public List<String> raciLevel {get; set;}
        
        // constructors
        public StakeholderRACI(Trust_Deliverable_Stakeholder__c s) {
            stakeholder = s;
            stakeholderId = string.valueOf(s.Stakeholder__c);
            if (!string.isBlank(s.RACI_Level__c)) {
                raciLevel = s.RACI_Level__c.split(';');
            } else {
                raciLevel = new List<String>();
            }
        }
        public StakeholderRACI() {
            stakeholder = new Trust_Deliverable_Stakeholder__c();
            stakeholderId = NONE;
            raciLevel = new List<String>();
        }
    }
    
    private String deliverableLink() {
        return deliverable.Id == null? '' :
        '\n\nAccess this Trust Deliverable to review and take action by selecting one of the links below:\n\n'
        + 'https://security.my.salesforce.com' + '/' + string.valueOf(deliverable.Id).substring(0,15)
        + '\n'
        + 'com.salesforce.salesforce1://entity/view?entityId=' + string.valueOf(deliverable.Id).substring(0,15) + ' Salesforce1 (iOS only)';            
    }   

}
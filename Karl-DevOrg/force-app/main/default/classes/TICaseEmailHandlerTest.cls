/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TICaseEmailHandlerTest {

    static testMethod void testTIAlertCaseWithDuplicateNoKeywordsNonMatchingNegativeTest() {
        TICaseEmailHandler.caseEmailHandlerConfigList = (List<TI_Case_Email_Handler_Config__mdt>) JSON.deserialize('[{"Case_Recordtype_Name__c":"TI Alert","Priority__c":"High","Combine_into_single_Case__c":false,"Email_Source__c":"(?:(?i)alerts@radian6.com$|no-reply@salesforce.com$)","Is_Active__c":true,"Keywords__c":"","Response_Message__c":"Test Response Message"}]', List<TI_Case_Email_Handler_Config__mdt>.class);            
    	// false test on matching source
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        email.Subject = 'Test Alert Email';
        email.toAddresses = new List<String> {'test.email.handler@test.com'};
        email.FromAddress = 'testAlertEmail@test.com';
        email.plainTextBody = 'Test Alert Email';
        email.HtmlBody = '<div>Test Alert Email</div>';
        Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();
        TICaseEmailHandler mailer = new TICaseEmailHandler(); 
                 
        //run test
        Test.startTest();
            mailer.handleInboundEmail(email, envelope);
        Test.stopTest();
        
        List<Case> emailCases = [select Id from Case where recordType.Name = 'TI Alert'];
        
        System.assertEquals(0, emailCases.size());   
        
        //code coverage
        
        TICaseEmailHandler.msgToString(email); 
    }
    
    static testMethod void testTIAlertCaseWithDuplicateNoKeywordsNonMatching() {
        TICaseEmailHandler.caseEmailHandlerConfigList = (List<TI_Case_Email_Handler_Config__mdt>) JSON.deserialize('[{"Case_Recordtype_Name__c":"TI Alert","Priority__c":"High","Combine_into_single_Case__c":false,"Email_Source__c":"(?:(?i)alerts@radian6.com$|no-reply@salesforce.com$)","Is_Active__c":true,"Keywords__c":"","Response_Message__c":"Test Response Message"}]', List<TI_Case_Email_Handler_Config__mdt>.class);    
        
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        email.Subject = 'Test Alert Email';
        email.toAddresses = new List<String> {'test.email.handler@test.com'};
        email.FromAddress = 'alerts@radian6.com';
        email.plainTextBody = 'Test Alert Email';
        email.HtmlBody = '<div>Test Alert Email</div>';
        Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();
        TICaseEmailHandler mailer = new TICaseEmailHandler();
                 
        //run test
        Test.startTest();
            // send
            mailer.handleInboundEmail(email, envelope);
            mailer.handleInboundEmail(email, envelope);
        Test.stopTest();
        
        List<Case> emailCases = [select Description, origin, priority from Case where recordType.Name = 'TI Alert'];
        
        System.assertEquals(2, emailCases.size());   
        
        System.assertEquals('Test Alert Email', emailCases[0].Description);
        System.assertEquals('High', emailCases[0].priority);
        System.assertEquals('Email', emailCases[0].origin);
        
        List<EmailMessage> caseEmails = [ select Subject, TextBody, HtmlBody, fromAddress from EmailMessage where ParentId = :emailCases[0].id ];
        
        System.assertEquals(1, caseEmails.size()); 
        System.assertEquals('Test Alert Email', caseEmails[0].Subject);  
        System.assertEquals('Test Alert Email', caseEmails[0].TextBody);  
        System.assertEquals('<div>Test Alert Email</div>', caseEmails[0].HtmlBody);  
        System.assertEquals('alerts@radian6.com', caseEmails[0].fromAddress);  
    }    
    
    static testMethod void testTIFitCaseWithDuplicateKeywords() {
        TICaseEmailHandler.caseEmailHandlerConfigList = (List<TI_Case_Email_Handler_Config__mdt>) JSON.deserialize('[{"Case_Recordtype_Name__c":"TI FIT","Priority__c":"High","Combine_into_single_Case__c":false,"Email_Source__c":"(?:(?i)service@secureworks.com$|(?i)intell-live-threat-system@fox-it.com$)","Is_Active__c":true,"Keywords__c":"Fit1 Keyword Match|Fit2 Keyword Match","Response_Message__c":"Test Response Message"}]', List<TI_Case_Email_Handler_Config__mdt>.class);    

        Messaging.InboundEmail email1 = new Messaging.InboundEmail();
        email1.Subject = 'Test FIT Email';
        email1.toAddresses = new List<String> {'test.email.handler@test.com'};
        email1.FromAddress = 'service@secureworks.com';
        email1.plainTextBody = 'Test FIT Email with No Keyword Match';
        email1.HtmlBody = '<div>Test FIT Email with No Keyword Match</div>';
        
        Messaging.InboundEmail email2 = new Messaging.InboundEmail();
        email2.Subject = 'Test FIT Email';
        email2.toAddresses = new List<String> {'test.email.handler@test.com'};
        email2.FromAddress = 'service@secureworks.com';
        email2.plainTextBody = 'Test FIT Email with Fit1 Keyword Match';
        email2.HtmlBody = '<div>Test FIT Email with Fit1 Keyword Match</div>';
        
        Messaging.InboundEmail email3 = new Messaging.InboundEmail();
        email3.Subject = 'Fit2 Keyword Match';
        email3.toAddresses = new List<String> {'test.email.handler@test.com'};
        email3.FromAddress = 'service@secureworks.com';
        email3.plainTextBody = 'Test FIT Email';
        email3.HtmlBody = '<div>Test FIT Email</div>';        

                
        TICaseEmailHandler mailer = new TICaseEmailHandler();
        Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();
                 
        //run test
        Test.startTest();
            // send
            mailer.handleInboundEmail(email1, envelope);
            mailer.handleInboundEmail(email2, envelope);
            mailer.handleInboundEmail(email3, envelope);
        Test.stopTest();
        
        List<Case> emailCases = [select Description, origin from Case where recordType.Name = 'TI FIT'];
        
        System.assertEquals(2, emailCases.size());   
        
        System.assertEquals('Test FIT Email with Fit1 Keyword Match', emailCases[0].Description);
        System.assertEquals('Email', emailCases[0].origin);
        
        System.assertEquals('Test FIT Email', emailCases[1].Description);
        System.assertEquals('Email', emailCases[1].origin);        
        
        List<EmailMessage> caseEmails = [ select Subject, TextBody, HtmlBody, fromAddress from EmailMessage where ParentId = :emailCases[0].id ];
        
        System.assertEquals(1, caseEmails.size()); 
        System.assertEquals('Test FIT Email', caseEmails[0].Subject);  
        System.assertEquals('Test FIT Email with Fit1 Keyword Match', caseEmails[0].TextBody);  
        System.assertEquals('<div>Test FIT Email with Fit1 Keyword Match</div>', caseEmails[0].HtmlBody);  
        System.assertEquals('service@secureworks.com', caseEmails[0].fromAddress);  
        
        caseEmails = [ select Subject, TextBody, HtmlBody, fromAddress from EmailMessage where ParentId = :emailCases[1].id ];
        
        System.assertEquals(1, caseEmails.size()); 
        System.assertEquals('Fit2 Keyword Match', caseEmails[0].Subject);  
        System.assertEquals('Test FIT Email', caseEmails[0].TextBody);  
        System.assertEquals('<div>Test FIT Email</div>', caseEmails[0].HtmlBody);  
        System.assertEquals('service@secureworks.com', caseEmails[0].fromAddress);                  
    } 
    
    static testMethod void testTIRFICaseWithoutDuplicateNoKeywords() {
        TICaseEmailHandler.caseEmailHandlerConfigList = (List<TI_Case_Email_Handler_Config__mdt>) JSON.deserialize('[{"Case_Recordtype_Name__c":"TI RFI","Priority__c":"High","Combine_into_single_Case__c":true,"Email_Source__c":".*(?<!(?i)no-reply|(?i)noreply)@salesforce.com","Is_Active__c":true,"Keywords__c":"","Response_Message__c":"Test Response Message"}]', List<TI_Case_Email_Handler_Config__mdt>.class);    

        Messaging.InboundEmail email1 = new Messaging.InboundEmail();
        email1.Subject = 'Test RFI Email';
        email1.toAddresses = new List<String> {'test.email.handler@test.com'};
        email1.FromAddress = 'test1.user@salesforce.com';
        email1.plainTextBody = 'Test RFI Email 1';
        email1.HtmlBody = '<div>Test RFI Email 1</div>';
        
        Messaging.InboundEmail email2 = new Messaging.InboundEmail();
        email2.Subject = 'Test RFI Email';
        email2.toAddresses = new List<String> {'test.email.handler@test.com'};
        email2.FromAddress = 'test2.user@salesforce.com';
        email2.plainTextBody = 'Test RFI Email 2';
        email2.HtmlBody = '<div>Test RFI Email 2</div>';        
                
        TICaseEmailHandler mailer = new TICaseEmailHandler();
        Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();
                 
        //run test
        Test.startTest();
            // send
            mailer.handleInboundEmail(email1, envelope);
            mailer.handleInboundEmail(email2, envelope);
        Test.stopTest();
        
        List<Case> emailCases = [select Description, origin from Case where recordType.Name = 'TI RFI'];
        
        System.assertEquals(1, emailCases.size());   
        
        System.assertEquals('Test RFI Email 1', emailCases[0].Description);
        System.assertEquals('Email', emailCases[0].origin);        
        
        List<EmailMessage> caseEmails = [ select Subject, TextBody, HtmlBody, fromAddress from EmailMessage where ParentId = :emailCases[0].id ];
        
        System.assertEquals(2, caseEmails.size()); 
        System.assertEquals('Test RFI Email', caseEmails[0].Subject);  
        System.assertEquals('Test RFI Email 1', caseEmails[0].TextBody);  
        System.assertEquals('<div>Test RFI Email 1</div>', caseEmails[0].HtmlBody);  
        System.assertEquals('test1.user@salesforce.com', caseEmails[0].fromAddress);  
         
        System.assertEquals('Test RFI Email', caseEmails[1].Subject);  
        System.assertEquals('Test RFI Email 2', caseEmails[1].TextBody);  
        System.assertEquals('<div>Test RFI Email 2</div>', caseEmails[1].HtmlBody);  
        System.assertEquals('test2.user@salesforce.com', caseEmails[1].fromAddress);          
    }        
    
    // Test Setup across all tests
    // create custom setting
    @testSetup static void setup() {
    	// Create Salesforce Account and Training Course due to failing trigger 
    	
    	insert new Account (name = 'salesforce.com');
    	insert new Training_Course__c( Name = 'Detection Everywhere');    	
    }
}
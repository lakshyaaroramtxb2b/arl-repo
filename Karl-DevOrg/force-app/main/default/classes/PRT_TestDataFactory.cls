/*
* Created by - Prashant Gupta (MTX)
*/

/* Portfolio Data*/
@isTest
public class PRT_TestDataFactory {
    
    public static List<Portfolio__c> createPortfolio(Integer num, Boolean doInsert){
        List<Portfolio__c> portfolioList= new List<Portfolio__c>();
        for(integer i = 0; i<num ;i++){
            Portfolio__c port = new Portfolio__c();
            //pro.recordTypeId = PRT_Constants.PRT_PROJECT_PROJECT_RECORDTYPEID;
            port.name = 'Test Project'+i;
            
            portfolioList.add(port);
        }
        if(doInsert){
            insert portfolioList;
        }
        return portfolioList;
    }
	
    /*
     * Created by - Prashant Gupta
     *  create Project record for test
     */
    public static List<Project__c> createProjects(Integer num, Boolean doInsert){
        List<Project__c> projectList= new List<Project__c>();
        for(integer i = 0; i<num ;i++){
            Project__c pro = new Project__c();
            pro.recordTypeId = PRT_Constants.PRT_PROJECT_PROJECT_RECORDTYPEID;
            pro.name = 'Test Project'+i;
            
            projectList.add(pro);
        }
        if(doInsert){
            insert projectList;
        }
        return projectList;
    }
    
    
    /*
     * Created by - Prashant Gupta
     *  create Business Case record for test
     */
    public static List<Project__c> createBussinessCase(Integer num, Boolean doInsert){
        List<Project__c> projectList= new List<Project__c>();
        for(integer i = 0; i<num ;i++){
            Project__c pro = new Project__c();
            pro.recordTypeId = PRT_Constants.PRT_PROJECT_BUSINESSCASE_RECORDTYPEID;
            Pro.name = 'Test Project'+i;
            projectList.add(pro);
        }
        if(doInsert){
            insert projectList;
        }
        return projectList;
    }

    
    /*
     * Created by - Prashant Gupta
     *  create milestone record for test
     */    
    public static List<Milestone_PRT__c> createMilestone(Integer num,id ProjectId, Boolean doInsert){
        List<Milestone_PRT__c> milestoneList= new List<Milestone_PRT__c>();
        for(integer i = 0; i<num ;i++){
            Milestone_PRT__c mile = new Milestone_PRT__c();
            mile.name = 'Test  Milestone '+i;
            mile.Description__c = 'Test Description ' +i;
            mile.Project__c = ProjectId;
            milestoneList.add(mile);
        }
        if(doInsert){
            insert milestoneList;
        }
        return milestoneList;
    }
    
    
    /*
     * Created by - Prashant Gupta
     *  create Risk record for test
     */
    public static List<Risk__c> createRisk(Integer num,id ProjectId, Boolean doInsert){
        List<Risk__c> riskList= new List<Risk__c>();
        for(integer i = 0; i<num ;i++){
            Risk__c rec = new Risk__c();
            rec.name = 'Test  risk '+i;
            rec.Project__c = ProjectId;
            riskList.add(rec);
        }
        if(doInsert){
            insert riskList;
        }
        return riskList;
    }
    
    
    /*
     * Created by - Prashant Gupta
     *  create Purchase Order record for test
     */
    public static List<Purchase_Order__c> createPurchaseOrder(Integer num,id ProjectId, Boolean doInsert){
        List<Purchase_Order__c> purchaseOrderList= new List<Purchase_Order__c>();
        for(integer i = 0; i<num ;i++){
            Purchase_Order__c rec = new Purchase_Order__c();
            rec.name = 'Test  Purchase_Order__c '+i;
            rec.Project__c = ProjectId;
            purchaseOrderList.add(rec);
        }
        if(doInsert){
            insert purchaseOrderList;
        }
        return purchaseOrderList;
    }
    
    
    /*
     * Created by - Prashant Gupta
     *  create Assignement record for test
     */
    public static List<Assignment__c> createAssignment(Integer num,id ProjectId,id milestoneId, Boolean doInsert){
        List<Assignment__c> assignmentList= new List<Assignment__c>();
        for(integer i = 0; i<num ;i++){
            Assignment__c rec = new Assignment__c();
            //rec.Name = 'Test  Assignment__c '+i;
            rec.Milestone__c = milestoneId;
            rec.Project__c = ProjectId;
            AssignmentList.add(rec);
        }
        if(doInsert){
            insert assignmentList;
        }
        return assignmentList;
    }
    
    /*
     * Created by - Swarnima Mandhata
     *  create Assignment Week record for test
     */
    
     public static List<Assignment_Week__c> createAssignmentWeek(Integer num,Id assignmentId,Id ContactId,Boolean doInsert){
        List<Assignment_Week__c> assignmentWeekList= new List<Assignment_Week__c>();
        for(integer i = 0; i<num ;i++){
            Assignment_Week__c rec = new Assignment_Week__c();
            //rec.Name = 'Test  AssignmentWeek__c '+i;
            rec.Assignment__c = assignmentId;
            rec.Contact__c = ContactId;
            assignmentWeekList.add(rec);
        }
        if(doInsert){
            insert assignmentWeekList;
        }
        return assignmentWeekList;
    }
    
    
    /*
     * Created by - Prashant Gupta
     *  create User record for test
     */
    public static List<User> createUsers(Integer num,String profileName, Boolean doInsert){
        UserRole userRole = [SELECT Id FROM UserRole LIMIT 1];
        List<User> userList = new List<User>();
        Profile profile = [SELECT Id FROM Profile WHERE Name=:profileName];
        //Create Admin User
        for(integer i = 0; i<num;i++){
        	userList.add(new User(Alias = 'sta23', Email='PRTUser'+i+'@testorg.com', 
                             EmailEncodingKey='UTF-8', LastName='Testing'+i, LanguageLocaleKey='en_US', 
                             LocaleSidKey='en_US', ProfileId = profile.Id, UserRoleId = userRole.Id,
                                  employeenumber='112',
                             TimeZoneSidKey='America/Los_Angeles', UserName='standarduser'+i+'994'+i+'@test'+i+'org.com'));
        }
        if(doInsert){
            insert userList;
        }
        return userList;
    }
    
    
    /*
     * Created by - Prashant Gupta
     *  create Project Status record for test
     */
    public static List<Project_Status_Report__c> createProjectStatusReport(Integer num, Boolean doInsert,
                                                                            String OverallProjectStatus,
                                                                           String BudgetStatus,
                                                                           String ScheduleStatus){
                                                                              
                List<Project_Status_Report__c> projectStatusReportList= new List<Project_Status_Report__c>();
                 for(integer i = 0; i<num ;i++){
                    Project_Status_Report__c projectStatusReport = new Project_Status_Report__c();
                     projectStatusReport.Name = 'Test' + '-' +'Week of: ' +Date.today().toStartofWeek().addDays(12).format();
                       projectStatusReport.Week_ending__c = System.today();
                         projectStatusReport.Overall_Project_Status__c =  OverallProjectStatus;
                          projectStatusReport.Budget_Status__c = BudgetStatus;
                          projectStatusReport.Schedule_Status__c = ScheduleStatus;
                          projectStatusReport.Latest_Overall_Project_Status__c =OverallProjectStatus;
                     	 
                         projectStatusReportList.add(projectStatusReport);
                  }
              if(doInsert){
                   insert projectStatusReportList;
               }
          return projectStatusReportList;
                                                                             
    }
    
    
    /*
     * Created by - Prashant Gupta
     *  create Program record for test
     */
    public static List<Program__c> createPrograms(Integer num, Boolean doInsert){
        List<Program__c> programList= new List<Program__c>();
        for(integer i = 0; i<num ;i++){
            Program__c pro = new Program__c();
            Pro.name = 'Test Project'+i;
            programList.add(pro);
        }
        if(doInsert){
            insert programList;
        }
        return programList;
    }
    
    
    /*
     * Created by - Prashant Gupta
     *  create contact record for test
     */
     public static List<Contact> createContact(Integer num, Boolean doInsert){
        List<Contact> contactList= new List<Contact>();
        for(integer i = 0; i<num ;i++){
            Contact con = new Contact();
           	con.LastName = 'Test' + i;
            
            contactList.add(con);
        }
        if(doInsert){
            insert contactList;
        }
        return contactList;
    }
    
    
    /*
     * Created by - Prashant Gupta
     *  create change Request record for test
     */
	
 	public static List<Change_Request__c> createChangeRequest(Integer num,id ProjectId, Boolean doInsert){
        List<Change_Request__c> changeRequestList= new List<Change_Request__c>();
        for(integer i = 0; i<num ;i++){
            Change_Request__c rec = new Change_Request__c();
          
            rec.Project__c = ProjectId;
            rec.Name__c = 'test' + i;
            rec.Start_Date__c = date.today();
            rec.Due_Date__c = date.today() +1;
            rec.Milestone_Description__c  = 'test desc';
            changeRequestList.add(rec);
        }
        if(doInsert){
            insert changeRequestList;
        }
        return changeRequestList;
    }   
    
    
    /*
     * Created by - Prashant Gupta
     *  create Milestone_Dependency__c record for test
     */
    public static List<Milestone_Dependency__c> createMilestoneDependency(Integer num,Boolean doInsert){
        List<Milestone_Dependency__c> milestoneDependencyList= new List<Milestone_Dependency__c>();
        for(integer i = 0; i<num ;i++){
            Milestone_Dependency__c rec = new Milestone_Dependency__c();
            milestoneDependencyList.add(rec);
        }
        if(doInsert){
            insert milestoneDependencyList;
        }
        return milestoneDependencyList;
    } 
    
    /*
     * Created by - Prashant Gupta
     * Generate a Random Stirng on Numbers
     */
    public static Integer generateRandomNumber(){
        Integer randomNumber = Integer.valueof((Math.random() * 10));
		System.debug('randomNumber  is'+randomNumber);
        return randomNumber;
    }
    /*
     * Created by - Prashant Gupta
     * Generate a Random Stirng
     */
    public static String generateRandomNumberString(){
        Integer randomNumber = Integer.valueof((Math.random() * 4));
		System.debug('randomNumber  is'+randomNumber);
        return String.valueOf(randomNumber);
        //System.debug('>>>' + String.valueOf(randomNumber));
    }
}
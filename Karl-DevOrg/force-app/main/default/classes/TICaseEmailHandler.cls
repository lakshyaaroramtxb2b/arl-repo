/**
 * Class: TICaseEmailHandler
 *
 * Purpose: Email Handler for the Salesforce Threat Intelligence team, Reads the custom setting TI Case
            Email Handler Config to determine how to create cases for the TI team based regex matching on 
            from address and keyword search.  Also, will "roll-up" multiple emails to a single case.
 * 
 * 
 * Change History:
 *
 * Developer                     Date        Flag     Description
 * -----------------------------------------------------------------------
 * Brian Clift                  10/03/2017   CSIR-4   Initial development
 *
 * Free Fly Security
 * +1 970.825.2460
 *
 */


/**
 * Email services are automated processes that use Apex classes
 * to process the contents, headers, and attachments of inbound
 * email.
 */
global class TICaseEmailHandler implements Messaging.InboundEmailHandler {

    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
        result.success = true;
        
        result.message = matchAndCreateCases(email);
        
        return result;
    }
    
    /** Name: matchAndCreateCases
        Purpose: If email matches regex criteria of custom setting records "TI Case Email Handler"
                 then create appropriate case(s)... more than one case may be created.
                 Also, if record is matched and setting attribute "Combine into single Case" is true, 
                 then locate an existing case created in the last 24 hours and add the email to the existing case.
        Return: String representing the message response... if more than one case is created, return the last non empty configured
                message response
        Input: email - The incoming email message being processed by the email handler
    */
    private String matchAndCreateCases(Messaging.InboundEmail email) {
    	list<Case> newCaseList = new list<Case>();
    	String successMessage = '';
    	Map< String, Schema.RecordTypeInfo > caseRecordTypeMap = Case.sObjectType.getDescribe().getRecordTypeInfosByName();
    	String KEYWORD_PATTERN_PREFIX = '.*(?:';
    	 
    	for ( TI_Case_Email_Handler_Config__mdt config : caseEmailHandlerConfigList ) {
    	    if (caseRecordTypeMap.get(config.Case_Recordtype_Name__c) != null) {
    	    	// match Source
    	    	if (Pattern.compile(config.Email_Source__c).matcher(email.fromAddress).matches()) {
    	    		// match Keywords on subject or body if described
    	    		Boolean keyWordmatch = false;
    	    		
    	    		if (String.isNotBlank(config.Keywords__c)) {
    	    		    String keywordRegEx = KEYWORD_PATTERN_PREFIX;
    	    		    
    	    		    for (String keyword : config.Keywords__c.split('\\|')) {
    	    		        keywordRegEx += (keywordRegEx == KEYWORD_PATTERN_PREFIX ? '' : '|') + '(?i).*' +keyword.trim() + '.*$'; 
    	    		    }
    	    		    
    	    		    keywordRegEx += ')';    	    		    
    	    		    
    	    			Pattern keyWordPattern = Pattern.compile(keywordRegEx);
    	    			    	    			
    	    			if ( ( String.isNotBlank( email.subject ) && keyWordPattern.matcher( email.subject ).matches() ) ||
    	    			    ( String.isNotBlank( email.plainTextBody ) && keyWordPattern.matcher( email.plainTextBody.replaceAll('[\\x00-\\x1F\\x80-\\xFF]','') ).matches() ) || 
    	    			    ( String.isNotBlank( email.htmlBody ) && keyWordPattern.matcher( email.htmlBody.replaceAll('[\\x00-\\x1F\\x80-\\xFF]','') ).matches() ) ) {
                            // check if should combine this email into a single existing case
                            keyWordMatch = true;
    	    			}
    	    		} 
    	    		
    	    		if (keyWordMatch || String.isBlank(config.Keywords__c)) {
    	    	        Id existingCaseId = null;
                
                        if (config.Combine_into_single_Case__c) {
                         	existingCaseId = findRecentCaseBySubjectAndType(email.subject, caseRecordTypeMap.get(config.Case_Recordtype_Name__c).getRecordTypeId());                            	
                        } 
                            
                        if (existingCaseId == null) {
                         	// create new case
                           	Case theCase = new Case( recordTypeId = caseRecordTypeMap.get(config.Case_Recordtype_Name__c).getRecordTypeId(),
                           	                         description = String.isNotEmpty(email.plainTextBody) ? email.plainTextBody : email.htmlBody,
                           	                         subject = email.subject,
                           	                         Priority = config.Priority__c,
                           	                         origin = 'Email' );
                            	                        
                           	newCaseList.add(theCase);
                        } else {   
                           	newCaseList.add(new Case(Id = existingCaseId));                        
                        }   
                        
                        if (String.isBlank(successMessage) && String.isNotBlank(config.Response_Message__c)) {
                        	successMessage = config.Response_Message__c;
                        } 	    			
    	    		}
    	    	}
    	    }	
    	}
    	
    	// Attach Email to Case
    	if (!newCaseList.isEmpty()) {
    		try {
    			upsert newCaseList;
    			
    			List<EmailMessage> emailsToCreate = new List<EmailMessage>();
    			String toAddresses = '';
    			
    			if (email.ToAddresses != null && !email.ToAddresses.isEmpty()) {
    			    for (String address : email.ToAddresses) {
    				    toAddresses += (String.isEmpty(toAddresses) ? '' : ',') + address;
    			    }
    			}
    			
    			for (Case theCase : newCaseList) {
                    EmailMessage emailMsg = new EmailMessage(); 
                    emailMsg.fromAddress = email.fromAddress;
    			    emailMsg.ToAddress = toAddresses;
    			    emailMsg.Subject = email.Subject;
    			    emailMsg.HtmlBody = email.HtmlBody;
    			    emailMsg.TextBody = email.plainTextBody;
    			    emailMsg.MessageDate = System.now(); 
    			    emailMsg.Status = '0';
    			    emailMsg.ParentId = theCase.id;
    			    emailMsg.fromName = email.fromName;
    			     
    			    emailsToCreate.add(emailMsg);			
    			}
    			
    			insert emailsToCreate;
    		} catch (DMLException ex) {
                System.debug('**** DML Exception occured creating TI Case from Email... From Address: ' + email.fromaddress + ' Email Message\n: ' + msgToString(email) + ' DML Message: ' + ex.getDMLMessage(0));
    		}
    	}
    	
    	return successMessage;
    }
   
    /** Name: findRecentCaseBySubjectAndType
        Purpose: locate a case created within the past 24 hours with a matching subject and record type id
        Return: a corresponding case id or null
        Input: subject - the incoming mail subject that should match a case subject
               recordtypeid - the record type of the case in which to match
    */    
    private Id findRecentCaseBySubjectAndType( String mailSubject, Id caseRecordTypeId ) {
    	Id foundCaseId;
    	
    	List<Case> cases = [ select Id from 
    	                            Case where 
    	                            recordtypeId = :caseRecordTypeId and 
    	                            subject = :mailSubject and
    	                            createddate >= :System.now().addDays(-1)  
    	                            limit 1 ];
    	
    	if (!cases.isEmpty()) {
    		foundCaseId = cases[0].id;
    	}
    	
    	return foundCaseId;
    }
    
    @TestVisible
    private static String msgToString(Messaging.InboundEmail email) { 
        String msg = '';
        
        if (email.headers != null && !email.headers.isEmpty()) {
            for (Messaging.InboundEmail.Header h : email.headers) {
                msg += h.name+':'+h.value+'\n\n';
            }
        }
        
        if (email.plainTextBody != null) {
            msg += email.plainTextBody+'\n\n\n\n';
        }
        
        if (email.htmlBody != null) {
            msg += email.htmlBody;
        }
        
        return msg;
    }   
    
    @TestVisible static  List<TI_Case_Email_Handler_Config__mdt> caseEmailHandlerConfigList {
        get {
                if (caseEmailHandlerConfigList == null) {
                    caseEmailHandlerConfigList = [ select Case_Recordtype_Name__c,
                                                          Combine_into_single_Case__c,
                                                          Email_Source__c, 
                                                          Keywords__c,
                                                          Priority__c,
                                                          Response_Message__c from TI_Case_Email_Handler_Config__mdt where
                                                          Is_Active__c = true ];
                }
                
                return   caseEmailHandlerConfigList;                                                        
        } set;
    } 
}
public class GuestUserListController {

    public List<User> getGuestUsers() {
        return [SELECT Id, Username, LastName, FirstName, Name, CompanyName, 
                  Division, Department, Title, Street, City, State, PostalCode, 
                  Country, Email,  IsActive, TimeZoneSidKey, UserRoleId, LocaleSidKey, 
                  ProfileId, UserType, LanguageLocaleKey, IsPortalEnabled 
                FROM User WHERE isActive=True and UserType='Guest'];
    }

}
public virtual with sharing class HW_RecordsStore {
    
    public with sharing class EmailsRecords{
        public String email;
        public String ownername;
        public List<RecordsWithReducedAttributes> recsList;
        public Integer count;
        public Map<String, Integer> recsCount;

    }

    public with sharing class RecordsWithReducedAttributes{
        public String UserName;
        public String Email;
        public String Role;
        public String FirstName;
        public String LastName;
        public String OrgName;
        public String ManagerEmail;
        public String ManagerName;
        public String AddonName;
        public String Status;
        public String OrgID;
        public String AddonID;
        public String AppName;
        public String AppID;
        public String Query;
        public String AlertType;
        //public Integer RecCount;
    }

    public with sharing class UserTypes{
        public Set<String> users;
        public Map<String, String> usernameMap;
        public Map<String, Set<String>> adminUserOrgs;
        public Map<String, Set<String>> managerMap;
        public Map<String, Integer> recsCount;
    }

    public UserTypes usersCollection(){
        //query all users of Heroku
         Set<String> users = new Set<String>();
         Map<String, String> usernameMap = new Map<String, String>();
         for(Heroku_Warden_users__c user: [SELECT email__c, Employee__r.INTEG_First_Name__c, Employee__r.INTEG_Last_Name__c FROM Heroku_Warden_users__c 
                                  WHERE nonSFEmail__c = false AND Employee__r.INTEG_Active__c = true AND
                                  LastModifiedDate >= LAST_N_DAYS:1]) {
                                      users.add(user.email__c);
                                      usernameMap.put(user.email__c, user.Employee__r.INTEG_First_Name__c+' '+user.Employee__r.INTEG_Last_Name__c);
                                  }
        
        //query all orgs associated to each admin user
        Map<String, Set<String>> adminUserOrgs = new Map<String, Set<String>>();
        for(Heroku_Warden_users__c rec: [SELECT Id, organizationId__c, role__c, email__c FROM Heroku_Warden_users__c WHERE role__c = 'admin' AND email__c IN :users]){
             if(adminUserOrgs.containsKey(rec.email__c)){
                 Set<String> tmp = adminUserOrgs.get(rec.email__c);
                 tmp.add(rec.organizationId__c);
                 adminUserOrgs.put(rec.email__c, tmp);
             }
             else {
                 adminUserOrgs.put(rec.email__c, new Set<String>{rec.organizationId__c});
             }    
         }

         //query records to identify managers for existing users
         Map<String, Set<String>> managerMap = new Map<String, Set<String>>();
         for(Heroku_Warden_users__c rec: [SELECT email__c, Employee__r.INTEG_Manager_Email__c, Employee__r.INTEG_Active__c from Heroku_Warden_users__c where Employee__c != null AND LastModifiedDate >= LAST_N_DAYS:1 AND email__c IN :users]){
             if(managerMap.containsKey(rec.Employee__r.INTEG_Manager_Email__c)){
                 Set<String> tmp = managerMap.get(rec.Employee__r.INTEG_Manager_Email__c);
                 tmp.add(rec.email__c);
                 managerMap.put(rec.Employee__r.INTEG_Manager_Email__c, tmp);
             }
             else{
                 managerMap.put(rec.Employee__r.INTEG_Manager_Email__c, new Set<String>{rec.email__c});
             }
         }

         //count by each vulntype
         Map<String, Integer> recordsCount = new Map<String, Integer>();
         recordsCount.put('no2fassoCount', [SELECT COUNT() FROM Heroku_Warden_users__c WHERE Employee__c != null AND Employee__r.INTEG_Active__c = true AND federated__c = false AND two_factor_authentication__c = false AND LastModifiedDate >= LAST_N_DAYS:1]);
         recordsCount.put('inactiveUsersCount',[SELECT COUNT() FROM Heroku_Warden_users__c WHERE Employee__c != null AND Employee__r.INTEG_Active__c = false AND LastModifiedDate >= LAST_N_DAYS:1]);
         recordsCount.put('externalUsersCount', [SELECT COUNT() FROM Heroku_Warden_users__c WHERE external_email__c = true AND role__c = 'admin' AND LastModifiedDate >= LAST_N_DAYS:1]);
         recordsCount.put('addonsCount', [SELECT COUNT() FROM Heroku_Warden_addons__c WHERE Heroku_Warden_addonservices__r.status__c != 'approved' AND Heroku_Warden_addonservices__r.authorized__c = false AND LastModifiedDate >= LAST_N_DAYS:1]);
         
         UserTypes ut = new UserTypes();
         ut.users = users;
         ut.usernameMap = usernameMap;
         ut.adminUserOrgs = adminUserOrgs;
         ut.managerMap = managerMap;
         ut.recsCount = recordsCount;
        return ut;
    }
}
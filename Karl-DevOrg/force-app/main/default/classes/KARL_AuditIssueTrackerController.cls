/** 
 * @author Lakshya Arora
 * @email lakshya.arora@mtxb2b.com
 * @description This class provides the methods for generating the Audit Issue Tracker Lightning Web Components.
 * Note that this was modelled on KARL_ARLReportController class. 
 */
public with sharing class KARL_AuditIssueTrackerController {

    /**
     * @author Lakshya Arora
     * @email lakshya.arora@mtxb2b.com
     * @description Build the relevant issue items for the AuditIssueWrapper, which get displayed via the LWC component 
     * karl_audit_issue_tracker. 
     * @param cycleAuditReportId - record Id for Cycle Audit Reports
     * @return AuditIssueWrapper contents for Wire data feed
     */
    @AuraEnabled(cacheable=true)
    public static List<AuditIssueWrapper> getAuditIssueTrackingInfo(Id cycleAuditReportId){
      List<KARL_Cycle_Audit_Report_Items__c > cycleAuditReportList = new List<KARL_Cycle_Audit_Report_Items__c >([SELECT Id,Audit_Cycle__c 
                                                                        FROM KARL_Cycle_Audit_Report_Items__c  
                                                                        WHERE Id =:cycleAuditReportId 
                                                                        WITH SECURITY_ENFORCED LIMIT 1 ]);
      List<AuditIssueWrapper> auditIssueWrapperList = new List<AuditIssueWrapper>();
      if(!cycleAuditReportList.isEmpty()){
        Map<String,String> baseUrlMap = new Map<String,String>();
        baseUrlMap = KARL_Utility.getBaseURL();
        String baseRecordURL = '';
        String baseUrlSuffix = '';
        if(!baseUrlMap.isEmpty()){
            baseRecordURL = baseUrlMap.get('baseRecordURL');
            baseUrlSuffix = baseUrlMap.get('baseUrlSuffix');
        }
        Id auditCycleId = cycleAuditReportList[0].Audit_Cycle__c;
        for(Audit_Cycle__c auditCycleObj : [SELECT Id,
                                                (SELECT Id,Name,KARL_KAI_Impacted_Scope__c,KARL_KAI_Issue_Type__c,Issue_Status__c,KARL_KAI_SCEA_POC__c,KARL_KAI_SCEA_POC__r.Name
                                                FROM Issues_Tracker__r) 
                                            FROM Audit_Cycle__c 
                                            WHERE Id =:auditCycleId 
                                            WITH SECURITY_ENFORCED]){
            for(KARL_Issue_Tracker__c issueTrackerObj : auditCycleObj.Issues_Tracker__r){
                AuditIssueWrapper auditIssueWrapperObj = new AuditIssueWrapper();
                auditIssueWrapperObj.issueId = issueTrackerObj.Id;
                auditIssueWrapperObj.issueName = issueTrackerObj.Name;
                auditIssueWrapperObj.issueLink = baseRecordURL + issueTrackerObj.Id + baseUrlSuffix;
                // auditIssueWrapperObj.impactedScope = issueTrackerObj.KARL_KAI_Impacted_Scope__c;
                auditIssueWrapperObj.issueType = issueTrackerObj.KARL_KAI_Issue_Type__c;
                auditIssueWrapperObj.issueResponseStatus = issueTrackerObj.Issue_Status__c;
                auditIssueWrapperObj.grcPocName = issueTrackerObj.KARL_KAI_SCEA_POC__r.Name;
                auditIssueWrapperObj.grcPocLink = baseRecordURL + issueTrackerObj.KARL_KAI_SCEA_POC__c + baseUrlSuffix;
                auditIssueWrapperList.add(auditIssueWrapperObj);
            }
        }  
        
      }
      return auditIssueWrapperList;
    }

    public class AuditIssueWrapper{
        @AuraEnabled public String issueId;
        @AuraEnabled public String issueName;
        @AuraEnabled public String issueLink;
        // @AuraEnabled public String impactedScope;
        @AuraEnabled public String issueType;
        @AuraEnabled public String issueResponseStatus;
        @AuraEnabled public String grcPocName;
        @AuraEnabled public String grcPocLink;
    }
}
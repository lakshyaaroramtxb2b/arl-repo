/**
 * Trust Maturity Model v2
 * Nov 2015
 * Written By: Joanna Chen
 * 
 * This class is called by a Before Delete trigger on Environment.
 * 
 * Enforce the Trust Maturity Model constraint that an Environment cannot be deleted if it has TM Objectives associated with it.
 * There exists similar behavior for the Environment and Security Control Status objects.
**/


public class TM_preventEnvDeleteIfObjectivesExist {

    public static void preventEnvDeleteIfObjectivesExist (Set<Id> envsIdsFromTrigger){
        List<Environment__c> envs = new List<Environment__c>();
    
        envs = [SELECT Id,
            (SELECT Id FROM TM_Objectives__r)
            FROM Environment__c 
            WHERE Id IN :envsIdsFromTrigger];
    

        for (Environment__c e: envs) {
            if (e.TM_Objectives__r.size() > 0) {
                Trigger.oldMap.get(e.Id).addError('Cannot delete an Environment if it has TM Objectives associated with it.');
            }

        }   
    
    }

}
/*

    Developer Name: Jorge L Caceres - jcaceres@salesforce.com | February 2017
    
    Description: Controller for either BPO_Remediation_Tasks or BPO_Site_Tasks page.
                 Given an ID the page will display a list of opened remediation tasks

*/

public with sharing class BPO_Remediation_Tasks {

    public Id parentId {get; private set;}

    public List<Task> remediationTasks {get; private set;}

    public BPO_Remediation_Tasks(ApexPages.StandardController controller) {
    
        parentId = controller.getId();
        remediationTasks = new List<Task>();
        Set<Id> relatedTaskIds = new Set<Id>();
        
        String objectName;
        String query = 'SELECT Id FROM BPO_Task__c WHERE {objectName} = :parentId';

        String query1 ='SELECT Id, ActivityDate, Owner.Name, Status, Subject, TaskSubType, Who.Name, WhatId, What.Name ' +
                       'FROM Task ' +
                       'WHERE IsClosed = False ' +
                       'AND WhatId IN :relatedTaskIds ' +
                       'ORDER BY Subject ';
        String query2 = 'SELECT Id, ActivityDate, Owner.Name, Status, Subject, TaskSubType, Who.Name, WhatId, What.Name ' +
                       'FROM Task ' +
                       'WHERE IsClosed = False ' +
                       'AND WhatId IN (SELECT Id FROM BPO_Task__c WHERE {objectName} = :parentId) ' +
                       'ORDER BY Subject ';
       
        if (parentId.getSObjectType().getDescribe().getName() == BPO_Assessment__c.sObjectType.getDescribe().getName()) {
            objectName = 'BPO_Assessment__c';
        } else if (parentId.getSObjectType().getDescribe().getName() == BPO_Site__c.sObjectType.getDescribe().getName()) {
            objectName = 'BPO_Assessment__r.BPO_Site__c';        
        }
        
        query = query.replace('{objectName}', objectName);
        for (BPO_Task__c t : Database.query(query)) {
            relatedTaskIds.add(t.Id);    
        }        
        for (Task t : Database.query(query1)) {
            remediationTasks.add(t);    
        }        

    }

}
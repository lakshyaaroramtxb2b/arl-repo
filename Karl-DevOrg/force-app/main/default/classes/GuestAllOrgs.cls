//upserts orgids into guests_all_orgs__c objects
global with sharing class GuestAllOrgs implements Database.Batchable<sObject>, Database.Stateful{
    public String query;
    global Database.QueryLocator start(Database.BatchableContext bc){
        //return Database.getQueryLocator('select organization_id__c from guests_standard_objs__c limit 1000');
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext bc, List<SObject> scope){
        //Set<SObject> standard_orgs_set = new Set<SObject>();
        Set<String> orgsset = new Set<String>();
        for(SObject so: scope){
            orgsset.add((String)so.get('organization_id__c'));
        }
        List<guests_all_orgs__c> allorgs = new List<guests_all_orgs__c>();
        for(String org : orgsset){
            guests_all_orgs__c guest = new guests_all_orgs__c();
            guest.organization_id__c = org;
            allorgs.add(guest);
        }
        Schema.SObjectField field = guests_all_orgs__c.Fields.organization_id__c;
        Database.UpsertResult[] cr = Database.upsert(allorgs,field,false);
        //system.debug(cr);
    }
    global void finish(Database.BatchableContext bc){
        AsyncApexJob job = [select id,status, numberoferrors,jobitemsprocessed,totaljobitems,createdby.email from AsyncApexJob where id=:bc.getJobId()];
        system.debug(job);
    }
}
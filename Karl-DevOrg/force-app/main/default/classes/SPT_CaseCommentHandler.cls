/*
* Created by - Himanshu Shrimali (MTX Group Inc.)
* SPT_CaseComment Controller Class
*/
public With Sharing class SPT_CaseCommentHandler {
    /*
    * Created by - Himanshu Shrimali
    * Date - 28-11-2019
    * This method returns the Case Comment Body for CaseComment object.
    */
    @AuraEnabled
    public static String getCaseCommentBody(String caseId, String templateLabel) {
        Map<String, Set<String>> objectToFieldsListMap = new Map<String, Set<String>>();
        Map<String, String> mergeFieldToValueMap = new Map<String, String>();
        Map<Id, Map<String, String>> caseToMergeFieldToValueMap = new Map<Id, Map<String, String>>();
        List<SPT_Case_Comment_Template__mdt> caseCommentTemplateList = new List<SPT_Case_Comment_Template__mdt>();
        List<String> caseIdList = new List<String>();
        caseIdList.add(caseId);
        
        // List of published case comments
        caseCommentTemplateList = [SELECT Id, Template_Body__c 
                                   FROM SPT_Case_Comment_Template__mdt
                                   WHERE Published__c = true 
                                   AND MasterLabel =: templateLabel];
        
        if(caseCommentTemplateList != null && !caseCommentTemplateList.isEmpty()) { 
            // Store the template body in a string    	
            String templateBody = caseCommentTemplateList.get(0).Template_Body__c;     
            List<String> templateBodyList = new List<String>();
            if(templateBody != null) {
                templateBodyList = templateBody.split('');
            }
            else {
                templateBody = '';
                return templateBody;
            }
            System.debug('Template Body: '+templateBody+' template List: '+templateBodyList);
            
            // Collect all the merge fields enclosed in {!...} in a map of object name and their respective fields
            for(Integer i=0; i<templateBody.length(); i++) {
                if(templateBodyList.get(i) == '{' && templateBodyList.get(i+1) == '!') {
                    i += 2;
                    String apiName = '';
                    Boolean firstPoint = true;
                    List<String> apiNamesList = new List<String>();
                    while(i<templateBody.length() && templateBodyList.get(i) != '}') {
                        if(firstPoint && templateBodyList.get(i) != '.') {
                            apiName += templateBodyList.get(i);   
                        }
                        else if(!firstPoint){
                            apiName += templateBodyList.get(i); 
                        }
                        else {
                            firstPoint = false;
                            apiNamesList.add(apiName);
                            apiName = '';
                        }
                        i++;
                    }
                    if(!apiNamesList.isEmpty()) {
                        apiNamesList.add(apiName);  
                        if(!objectToFieldsListMap.containsKey(apiNamesList.get(0))) {
                            objectToFieldsListMap.put(apiNamesList.get(0), new Set<String>());
                        }
                        objectToFieldsListMap.get(apiNamesList.get(0)).add(apiNamesList.get(1));
                    }           
                }
            }
            
            // Get all the values of the repective merge fields
            for(String key : objectToFieldsListMap.keySet()) {
                caseToMergeFieldToValueMap.putAll(getFieldValuesOfObjects(objectToFieldsListMap.get(key), key, caseIdList));
            }
            mergeFieldToValueMap = caseToMergeFieldToValueMap.values().get(0);

            // Replace the merge fields with their respective value in the template body
            for(String mergeField : mergeFieldToValueMap.keySet()) {
                String replaceKey = '{!'+mergeField+'}';
                String replaceValue;
                if(mergeFieldToValueMap.get(mergeField) == null) {
                    replaceValue = '';
                } 
                else {
                    if(mergeFieldToValueMap.get(mergeField) == '' && mergeField.contains('Name')) 
                        replaceValue = '';
                    else 
                        replaceValue = mergeFieldToValueMap.get(mergeField);
                }
                System.debug('Key - '+replaceKey+' Value - '+replaceValue);
                templateBody = templateBody.replace(replaceKey, replaceValue);
            }
            return templateBody;        
        }  
        return null;
    }

    /*
    * Created by - Himanshu Shrimali
    * Date - 28-11-2019
    * This method returns the Map of merge fields to their exact value to be replaced.
    */
    public static Map<Id, Map<String, String>> getFieldValuesOfObjects(Set<String> fieldApiNamesList, String objectApi, List<String> objectRecordIdList) {
        Map<String, String> mergeFieldToValueMap = new Map<String, String>();
        Map<Id, Map<String, String>> caseToMergeFieldToValueMap = new Map<Id, Map<String, String>>();
        List<sObject> sObjectList = new List<sObject>();
        String fieldsToRetrieve = '';
        
        // Storing fields to retrieve for the dynamic query
        for(String field : fieldApiNamesList) {
            fieldsToRetrieve += field + ', ';
        }
        fieldsToRetrieve = fieldsToRetrieve.removeEnd(', ');
        
        // When object is User
        if(objectApi == 'User') {
            String userId = SPT_Constants.CURRENT_USER_ID;
            sObjectList = Database.query('SELECT '+fieldsToRetrieve+' FROM USER WHERE Id =: userID WITH SECURITY_ENFORCED');
        }
        // When object is something else like Contact
        else {
            sObjectList = Database.query('SELECT '+fieldsToRetrieve+' FROM '+objectApi+' WHERE Id IN :objectRecordIdList WITH SECURITY_ENFORCED');
        }

        // Getting the exact values of the merge fields and storing them in a Map
        for(sObject record : sObjectList) {
            mergeFieldToValueMap = new Map<String, String>();
            for(String fieldApiName : fieldApiNamesList) {
                if(fieldApiName.contains('.')) {
                    List<String> fieldApiNameList = fieldApiName.split('\\.');
                    System.debug('=== splited array ===='+fieldApiNameList);
                    sObject relatedSObject = record.getSObject(fieldApiNameList.get(0));
                    if(relatedSObject != null) {
                        String value = String.valueOf(relatedSObject.get(fieldApiNameList.get(1)));
                        if(!String.isBlank(value)) {
                     		value = value.length() > 1000 ? value.substring(0, 994) + ' ...' : value;   
                    	}
                        mergeFieldToValueMap.put(objectApi+'.'+fieldApiName, value);   
                    }  
                    else {
                        mergeFieldToValueMap.put(objectApi+'.'+fieldApiName, '');
                    }
                }
                else {
                    String value = String.valueOf(record.get(fieldApiName));
                    if(!String.isBlank(value)) {
                     	value = value.length() > 1000 ? value.substring(0, 994) + ' ...' : value;   
                    }
                    mergeFieldToValueMap.put(objectApi+'.'+fieldApiName, value);    
                }
            }
            caseToMergeFieldToValueMap.put(record.Id, mergeFieldToValueMap);
        }
        return caseToMergeFieldToValueMap;
    }
    
    /*
    * Created by - Himanshu Shrimali
    * Date - 05-12-2019
    * This method returns the list of published template types
    */    
    @AuraEnabled
    public static List<String> getAllPublishedTemplateTypes() {
        List<String> templateTypesList = new List<String>();
        List<SPT_Case_Comment_Template__mdt> caseCommentTemplateList = new List<SPT_Case_Comment_Template__mdt>([SELECT Id, MasterLabel 
                                                                        FROM SPT_Case_Comment_Template__mdt
                                                                        WHERE Published__c = true
                                                                        ORDER BY MasterLabel ASC]);
        for(SPT_Case_Comment_Template__mdt caseCommentTemplate : caseCommentTemplateList) {
            templateTypesList.add(caseCommentTemplate.MasterLabel);
        }
        
        return templateTypesList;        
    }
    
    /*
    * Created by - Himanshu Shrimali
    * Date - 05-12-2019
    * This method creates a case comment record for a particular case with a template body.
    */
    @AuraEnabled 
    public static String createCaseCommentRecord(List<String> caseIdList, String templateBody, Boolean commentType, Boolean bulkMode) {
        System.debug('=========== BULK MODE ==========='+bulkMode);
        Map<Id, String> caseIdToTemplateBodyMap = new Map<Id, String>();
        System.debug('templateBody' + templateBody);
            Map<String, Set<String>> objectToFieldsListMap = new Map<String, Set<String>>();
        if(bulkMode && templateBody!=null) {
            Map<Id, Map<String, String>> caseToMergeFieldToValueMap = new Map<Id, Map<String, String>>();
            Map<String, String> mergeFieldToValueMap = new Map<String, String>();
            List<String> templateBodyList = templateBody.split('');

            for(Integer i=0; i<templateBody.length(); i++) {
                if(templateBodyList.get(i) == '{' && templateBodyList.get(i+1) == '!') {
                    i += 2;
                    String apiName = '';
                    Boolean firstPoint = true;
                    List<String> apiNamesList = new List<String>();
                    while(i<templateBody.length() && templateBodyList.get(i) != '}') {
                        if(firstPoint && templateBodyList.get(i) != '.') {
                            apiName += templateBodyList.get(i);   
                        }
                        else if(!firstPoint){
                            apiName += templateBodyList.get(i); 
                        }
                        else {
                            firstPoint = false;
                            apiNamesList.add(apiName);
                            apiName = '';
                        }
                        i++;
                    }
                    if(!apiNamesList.isEmpty()) {
                        apiNamesList.add(apiName);  
                        if(!objectToFieldsListMap.containsKey(apiNamesList.get(0))) {
                            objectToFieldsListMap.put(apiNamesList.get(0), new Set<String>());
                        }
                        objectToFieldsListMap.get(apiNamesList.get(0)).add(apiNamesList.get(1));
                    }           
                }
            }
			system.debug('objectToFieldsListMap' +objectToFieldsListMap);
            // Get all the values of the repective merge fields
            for(String key : objectToFieldsListMap.keySet()) {
                caseToMergeFieldToValueMap.putAll(getFieldValuesOfObjects(objectToFieldsListMap.get(key), key, caseIdList));
            }
            System.debug('Size : '+caseToMergeFieldToValueMap.size());
            System.debug('Case Ids: '+caseToMergeFieldToValueMap.keySet());
            System.debug('Complete Map: '+caseToMergeFieldToValueMap);

            // Replace the merge fields with their respective value in the template body
            for(Id caseId : caseToMergeFieldToValueMap.keySet()) {
                mergeFieldToValueMap = caseToMergeFieldToValueMap.get(caseId);
                System.debug('===== Map Values ====== '+mergeFieldToValueMap);
                String resolvedTemplateBody = templateBody;
                System.debug('New Template Body: '+resolvedTemplateBody);
                for(String mergeField : mergeFieldToValueMap.keySet()) {
                    String replaceKey = '{!'+mergeField+'}';
                    String replaceValue;
                    if(mergeFieldToValueMap.get(mergeField) == null) {
                        replaceValue = '';
                    } 
                    else {
                        if(mergeFieldToValueMap.get(mergeField) == '' && mergeField.contains('Name')) 
                            replaceValue = '';
                        else 
                            replaceValue = mergeFieldToValueMap.get(mergeField);
                    }
                    System.debug('Key - '+replaceKey+' Value - '+replaceValue);
                    resolvedTemplateBody = resolvedTemplateBody.replace(replaceKey, replaceValue);
                }
                caseIdToTemplateBodyMap.put(caseId, resolvedTemplateBody);
            }
        }

        List<CaseComment> caseCommentList = new List<CaseComment>();
        System.debug('Case Ids List: '+caseIdList);
        System.debug('Case Ids List: '+templateBody);
        for(String recordId : caseIdList) {
            CaseComment caseCommentRecord = new CaseComment();
            caseCommentRecord.ParentId = recordId;
            caseCommentRecord.IsPublished = commentType ? false : true;
            if(templateBody.length() <= 4000) {
                if(bulkMode && (objectToFieldsListMap!=null && !objectToFieldsListMap.isEmpty() && objectToFieldsListMap.size()>0))
                    caseCommentRecord.CommentBody = caseIdToTemplateBodyMap.get((Id)recordId);
                else 
                    caseCommentRecord.CommentBody = templateBody;
            }
            else {
                if(bulkMode && (objectToFieldsListMap!=null && !objectToFieldsListMap.isEmpty() && objectToFieldsListMap.size()>0)) 
                    caseCommentRecord.CommentBody = caseIdToTemplateBodyMap.get((Id)recordId).substring(0, 3975) + ' ...';
                else
                    caseCommentRecord.CommentBody = templateBody.substring(0, 3975)+' ...';
            }
            caseCommentList.add(caseCommentRecord);
        }
        Set<Id> commentsInsertedSet = new Set<Id>();

        Database.SaveResult[] srList = Database.insert(caseCommentList, false);
        for(Database.SaveResult sr : srList) {
            if(sr.isSuccess()) {
                // When the case comment doesn't get insert, store it's id to display case's id.
                commentsInsertedSet.add(sr.Id);
                System.debug('======= Comment Id ======== '+sr.Id);
            }
        }

        List<CaseComment> commentInsertedList = new List<CaseComment>([SELECT Id, ParentId FROM CaseComment WHERE Id IN :commentsInsertedSet]);
        List<Id> notUpdatedCaseList = new List<Id>();
        Set<Id> parentIdSet = new Set<Id>();
        for(CaseComment comment : commentInsertedList) {
            parentIdSet.add(comment.ParentId);
        }
        for(CaseComment comment : caseCommentList) {
            if(!parentIdSet.contains(comment.ParentId)) {
                notUpdatedCaseList.add(comment.ParentId);
            }
        }
        System.debug('Not Updated Case List: '+notUpdatedCaseList);

        String caseIdString = '';
        for(Id caseId : notUpdatedCaseList) {
            caseIdString = caseIdString + caseId + ', ';
        }
        caseIdString = caseIdString.removeEnd(', ');

        return caseIdString;
    }

    /*
    * Created by - Himanshu Shrimali
    * Date - 04-03-2020
    * This method provides template body of a particular template type
    */    
    @AuraEnabled
    public static String getTemplateBody(String templateLabel) {
        List<SPT_Case_Comment_Template__mdt> caseCommentTemplateList = new List<SPT_Case_Comment_Template__mdt>();
        
        // List of published case comments
        caseCommentTemplateList = [SELECT Id, Template_Body__c 
                                   FROM SPT_Case_Comment_Template__mdt
                                   WHERE Published__c = true 
                                   AND MasterLabel =: templateLabel];
        
        if(caseCommentTemplateList != null && !caseCommentTemplateList.isEmpty()) { 
            // Store the template body in a string    	
            String templateBody = caseCommentTemplateList.get(0).Template_Body__c;     
            List<String> templateBodyList = new List<String>();
            if(templateBody != null) {
                templateBodyList = templateBody.split('');
            }
            else {
                templateBody = '';
            }
            return templateBody;
        }
        return null;
    }
}
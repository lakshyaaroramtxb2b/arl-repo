public class SerializedSignaturesController {

   private static final String ALL_INSTANCES = '*';
   private static final String ALL_PRODUCTION = 'PRODUCTION';
   private static final String ALL_SANDBOX = 'SANDBOX';
    
   private Integer getCounter = null;
   private Integer publishCounter = null;   
   
   private String getId;
   private String getResultId;
   private String publishId;
   private String publishResultId;   
   
   private String getResult = '';
   private String publishResult = '';
    
   private List<String> instancesChosenPublish;
   private List<String> instancesChosenGet;
   
   private List<SfdcAuditableAction> getAuditableRequests;
   private List<SfdcAuditableAction> publishAuditableRequests;
   private List<SfdcAuditableAction> deleteAuditableRequests;
   
   private boolean isPollingGet = false;
   private boolean isPollingPublish = false;
   
   public boolean getIsPollingGet() {
       return isPollingGet;
   }
   
   public Integer getGetCounter() {
       return getCounter;
   }
   
   public boolean getIsPollingPublish() {
       return isPollingPublish;
   }
   
   public Integer getPublishCounter() {
       return publishCounter;
   }   
   
   public void initGetAuditableRequests() {
       getAuditableRequests = [SELECT Action,ChadHeader,ContentType,CreatedById,CreatedDate,DeveloperName,Id,IsDeleted,Language,LastModifiedById,LastModifiedDate,MasterLabel,Method,OperationType,QueryString,RequesterIp,RequestIntent,SystemModstamp,TrackingIdentifier FROM SfdcAuditableAction where action = 'malwareSignaturesPublication' and method = 'GET' order by createddate desc];   
       if (getAuditableRequests.size() > 0) {
           getId = getAuditableRequests.get(0).Id;
           getResult();
       }
   }
   
   public void initPublishAuditableRequests(){
       publishAuditableRequests = [SELECT Action,ChadHeader,ContentType,CreatedById,CreatedDate,DeveloperName,Id,IsDeleted,Language,LastModifiedById,LastModifiedDate,MasterLabel,Method,OperationType,QueryString,RequesterIp,RequestIntent,SystemModstamp,TrackingIdentifier FROM SfdcAuditableAction where action = 'malwareSignaturesPublication' and method = 'PUT' order by createddate desc];       
       if (publishAuditableRequests.size() > 0) {
           publishId = publishAuditableRequests.get(0).Id;
           publishResult();
       }
   }
   
   public void initDeleteAuditableRequests() {
       deleteAuditableRequests = [SELECT Action,ChadHeader,ContentType,CreatedById,CreatedDate,DeveloperName,Id,IsDeleted,Language,LastModifiedById,LastModifiedDate,MasterLabel,Method,OperationType,QueryString,RequesterIp,RequestIntent,SystemModstamp,TrackingIdentifier FROM SfdcAuditableAction where action = 'malwareSignaturesPublication' and method = 'DELETE' ];       
   }
   
    public String getEndpoint() {
        return 'https://security.my.salesforce.com/services/admin/v38.0/malwareSignaturesPublication';
    }

    public SerializedSignaturesController() {
        // Init Default Auditable Objects
        initGetAuditableRequests();
        initPublishAuditableRequests();
        initDeleteAuditableRequests();
    }
    
    /**
     * Select Options for instances
     */
    public List<SelectOption> getAllInstances() {
        List<String> allInstances = malwareDefense.MalwareRuleUtil.getAllInstances();
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption(ALL_INSTANCES, 'ALL'));
        options.add(new SelectOption(ALL_PRODUCTION, 'PRODUCTION'));
        options.add(new SelectOption(ALL_SANDBOX, 'SANDBOX'));
        if(allInstances == null) { return options; }
        for(String inst : allInstances) {
            options.add(new SelectOption(inst, inst));
        }        
        
        return options;
    }
   
   /**
    *  Select Options for get Requests
    */
   public List<SelectOption> getGetRequestOptions() {
       List<SelectOption> options = new List<SelectOption>();
       for (SfdcAuditableAction thisGet : getAuditableRequests) {
          String thisDate = string.valueOfGmt(thisGet.createdDate);
          options.add(new SelectOption(thisGet.Id,thisDate + ' / ' + thisGet.Id ));
       }
       return options;
   }
   
   /**
    *  Select options for get Request Results
    */
   public List<SelectOption> getGetRequestResultsOptions() {
       if (getId != null) {
           List<SelectOption> options = new List<SelectOption>();
           List<String> expectedRuleIds = getExpectedRuleIds();
           for (SfdcAuditableActionResult thisGet : [select Id, body, endpoint, createdDate, Message from sfdcauditableactionresult where RequestId = :getId]) {
              String thisDate = string.valueOfGmt(thisGet.createdDate);
              Integer bodyLength = (thisGet.body.toString()).length();
              String errMsg = getErrMsg(thisGet, expectedRuleIds);
              options.add(new SelectOption(thisGet.Id, thisGet.endpoint + ' / ' + thisDate + ' [SIZE:' + bodyLength + '][MESSAGE:' + thisGet.Message + '] ' + errMsg));
           }
           return options;
       }
       return null;
   }   
   
    
   /**
    *  Select Options for publish Requests
    */
   public List<SelectOption> getPublishRequestOptions() {
       List<SelectOption> options = new List<SelectOption>();
       for (SfdcAuditableAction thisGet : publishAuditableRequests) {
          String thisDate = string.valueOfGmt(thisGet.createdDate);
          options.add(new SelectOption(thisGet.Id,thisDate + ' / ' + thisGet.Id ));
       }
       return options;
   }
   
   /**
    *  Select options for publish Request Results
    */
   public List<SelectOption> getPublishRequestResultsOptions() {
       if (publishId != null) {
           List<SelectOption> options = new List<SelectOption>();
           List<String> expectedRuleIds = getExpectedRuleIds();
           for (SfdcAuditableActionResult thisPut : [select Id, body, endpoint, createdDate, message from sfdcauditableactionresult where RequestId = :publishId]) {
                     String thisDate = string.valueOfGmt(thisPut.createdDate);
              Integer bodyLength = (thisPut.body.toString()).length();
              String errMsg = getErrMsg(thisPut, expectedRuleIds);
              options.add(new SelectOption(thisPut.Id, thisPut.endpoint + ' / ' + thisDate + ' [SIZE:' + bodyLength + '][MESSAGE:' + thisPut.Message + '] ' + errMsg));
           }
           return options;
       }
       return null;
   }      
    
   /////////////////////////////////////////////////////////////////////////////////////////////////    
    
    /**
     * GET getter / setters 
     */   
    public void setGetId(String getId) {
        this.getId = getId;
        getResult = '';
    }

    public String getGetId() {
        return this.getId;
    }
    
    public void setInstancesChosenGet(List<String> instancesChosenGet) {
        this.instancesChosenGet = instancesChosenGet;
    }
    
    public List<String> getInstancesChosenGet() {
        return this.instancesChosenGet;
    }

    public void setGetResultId(String getResultId) {
        this.getResultId = getResultId;
        getResult = '';
    }         
    
    public String getGetResultId() {
        return getResultId;
    }             
    
    /**
     * PUBLISH getter / setters
      */
    public void setPublishId(String publishId) {
        this.publishId = publishId;
        publishResult = '';
    }

    public String getPublishId() {
        return this.publishId;
    }
    
    public void setInstancesChosenPublish(List<String> instancesChosenPublish) {
        this.instancesChosenPublish = instancesChosenPublish;
    }
    
    public List<String> getInstancesChosenPublish() {
        return this.instancesChosenPublish;
    }

    public void setPublishResultId(String publishResultId) {
        this.publishResultId = publishResultId;
        publishResult = '';
    }         

    public String getPublishResultId() {
        return publishResultId;
    }
    
    /////////////////////////////////////////////////////////////////////////////////////////////////
  
   /**
     * alert for new instances
     */
    
    //see comment at top of UnupdatedInstanceAlertJob re:why to handle variable number of PreviousInstanceList rather than assuming 1
    public String getNewInstanceAlert() {
        String alert = '';
        String prefix = '';
        
        List<PreviousInstanceList__c> pinstlists = [select newInstanceAlert__c from PreviousInstanceList__c];
        for(PreviousInstanceList__c p : pinstlists) {
            if(p.newInstanceAlert__c != null && p.newInstanceAlert__c.length() > 0) {
                alert += prefix + p.newInstanceAlert__c;
                prefix = ' \n';
            }
        }
        
        return alert;
    }
    
    public boolean getNewInstanceAlertPresent() {
        return (getNewInstanceAlert().length() > 0);
    }
    
    public void clearNewInstanceAlert() {
        if(true) {
            List<PreviousInstanceList__c> pinstlists = [select id, Instances__c, newInstanceAlert__c from PreviousInstanceList__c];
            for(PreviousInstanceList__c p : pinstlists) {
                p.newInstanceAlert__c = '';
            }
            
            if(pinstlists.size() > 0) { update pinstlists; }
        }
    }
   
   /////////////////////////////////////////////////////////////////////////////////////////////////
  
   /**
     * instance selection
     */
    
    public boolean getInstanceSelectionInvalidPublish() {
        return instanceSelectionInvalid(getInstancesChosenPublish());
    }
    
    public boolean getInstanceSelectionInvalidGet() {
        return instanceSelectionInvalid(getInstancesChosenGet());
    }
    
    public String getInstanceSelectionErrorPublish() {
        return instanceSelectionErrorMessage(getInstancesChosenPublish());
    }
    
    public String getInstanceSelectionErrorGet() {
        return instanceSelectionErrorMessage(getInstancesChosenGet());
    }
    
    private boolean instanceSelectionInvalid(List<String> instances) {
        return (instanceSelectionErrorMessage(instances) != null);
    }
    
    private String instanceSelectionErrorMessage(List<String> instances) {
        if(instances == null || instances.size() == 0) { return 'ERROR: Must select at least one instance'; }
        
        if(instances.size() == 1) { return null; }
        
        for(String inst : instances) {
            if(inst.equals(ALL_INSTANCES) || inst.equals(ALL_PRODUCTION) || inst.equals(ALL_SANDBOX)) {
               return 'ERROR: May only select one option for instances when choosing ALL, PRODUCTION, or SANDBOX';
           }
        }
        
        return null;
    }
    
    private String getInstanceHeaderPublish() {
        return formInstanceHeader(getInstancesChosenPublish());
    }
    
    private String getInstanceHeaderGet() {
        return formInstanceHeader(getInstancesChosenGet());
    }
    
    private String formInstanceHeader(List<String> instancesChosen) {
        
        String instHeader = '';
        String prefix = '';
        final String delimiterPrefix = ';';
        
        if(instanceSelectionInvalid(instancesChosen)) { return instHeader; }
        
        for(String inst : instancesChosen) {
            instHeader += prefix + inst;
            prefix = delimiterPrefix;
        }
        
        return instHeader;
    }
    
    /////////////////////////////////////////////////////////////////////////////////////////////////
    
    public String getSerializedSignatures() {
        return new SignatureProcessor().getSerializedSignatures();
    }
    
    public String getRawSignatures() {
        List<Map<String, Object>> sigs = new SignatureProcessor().getJsonSignatures();
        String rs = JSON.serializePretty(JSON.deserializeUntyped(JSON.serialize(sigs)));
        return rs;
    }   
    
    public String getHeader() {
        Map<String, Object> header = DefenseControllerUtil.getHeader();
        String rs = JSON.serializePretty(JSON.deserializeUntyped(JSON.serialize(header)));
        return rs;
    }      
    
    public void publish() {
        if(getInstanceSelectionInvalidPublish()) { return; }
        String signedSigs = new SignatureProcessor().getSerializedSignatures();
        String sid = UserInfo.getSessionId();
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setHeader('Authorization', 'Bearer ' + sid);
        req.setHeader('X-SFDC-INSTANCE', getInstanceHeaderPublish());
        req.setEndpoint(getEndpoint());
        req.setMethod('PUT');
        req.setBody(signedSigs);
        HttpResponse res = h.send(req);
        System.debug(res.getBody());
        publishId = (Id) res.getBody().substring(1,16);
        publishResult = '';
        isPollingPublish = true;
        publishCounter = 0;
        initPublishAuditableRequests();
    }
    
    public void retrieve() {
        if(getInstanceSelectionInvalidGet()) { return; }
        String sid = UserInfo.getSessionId();
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setHeader('Authorization', 'Bearer ' + sid);
        req.setHeader('X-SFDC-INSTANCE', getInstanceHeaderGet());
        req.setEndpoint(getEndpoint());
        req.setMethod('GET');
        HttpResponse res = h.send(req);
        System.debug(res.getBody());
        getId = (Id) res.getBody().substring(1,16);
        getResult = '';
        getCounter = 0;
        isPollingGet = true;
        initGetAuditableRequests();
    }
    
    public String getPublishResult() {
        if(publishResultId == null) { return ''; }
        return getSerializedAuditableResults([select body, endpoint from sfdcauditableactionresult where Id = :publishResultId]);
    } 
    
    public String getGetResult() {
        if(getResultId == null) { return ''; }
        return getSerializedAuditableResults([select body, endpoint from sfdcauditableactionresult where Id = :getResultId]);
    } 
    
    private String getErrMsg(SfdcAuditableActionResult result, List<String> expectedRuleIds) {
        Map<String, Map<String, Object>> responseRules = new Map<String, Map<String, Object>>();
        
        try {
            List<Object> ruleList = (List<Object>)JSON.deserializeUntyped(getSerializedAuditableResultBody(result));
            for(Object ruleObj : ruleList) {
                Map<String, Object> rule = (Map<String, Object>) ruleObj;
                if(rule.get('clientRuleId') == null) { return 'WARNING: cannot determine equivalence because response rules do not have clientRuleId field'; }
                responseRules.put((String) rule.get('clientRuleId'), rule);
            }
        } catch (Exception e) {
            return 'ERROR: error parsing response : ' + e.getMessage();
        }
        
        if(responseRules.keySet().size() != expectedRuleIds.size()) { return 'ERROR: number of rules does not match' ; }
        for(String id : expectedRuleIds) {
            if(responseRules.get(id) == null) { return 'ERROR: rule with clientRuleId ' + id + ' not found in response'; }
        }
        
        /* Would like to do some equality comparison between rules as well, but apex's instanceOf seems to be broken when used with Maps,
         * so it makes comparing rules very difficult. Thankfully, the only time we'd expect a rule on the security org to differ from the
         * same rule in a response would be if the rule has been edited on the org since it was last deployed, which is a very uncommon use case
         */ 
        
        return '';
    }
    
    private List<String> getExpectedRuleIds() {
        List<malwaresignature__c>  rules = [select id from malwaresignature__c where approvalStatus__c = 'Approved/Implemented'];
        List<String> ruleIds = new List<String>();
       
        for (MalwareSignature__c thisRule : rules) {
            ruleIds.add(thisRule.id);
        }
        
        return ruleIds;
    }    
    
    public String getSerializedAuditableResults(List<SfdcAuditableActionResult> results) {
        String resultString = '';

        for (SfdcAuditableActionResult thisResult : results) {
            resultString += thisResult.endpoint + ':\n' + getSerializedAuditableResultBody(thisResult);
       }  
       return resultString; 
   }
    
    public String getSerializedAuditableResultBody(SfdcAuditableActionResult result) {
        String resultBody = result.body.toString();
        try {
           String des = ((String) JSON.deserializeUntyped(resultBody)); 
           return des.replace('\\\n','\n') + '\n\n';                
        }
        catch (Exception ex) {
           return resultBody.unescapeUnicode();            
        }
    }
   
   public void getResult() {
       if (getId != null) {
            List<SfdcAuditableActionResult> result = Database.query('select endpoint, body from sfdcauditableactionresult where requestid = \'' + getId + '\'');
            getResult = getSerializedAuditableResults(result);
            if (getResult == '') {
                getResult = 'Loading...';
            }
       }    
   }
   
   public void publishResult() {
       if (publishId != null) {
           List<SfdcAuditableActionResult> result = Database.query('select endpoint, body from sfdcauditableactionresult where requestid = \'' + publishId + '\'');
           publishResult = getSerializedAuditableResults(result);
           if (publishResult == '') {
               publishResult = 'Loading...';
           }
       }   
   }

   public void getPoll() {
       if (!isPollingGet) return;
       initGetAuditableRequests();       
       getCounter++;   
       getResult();
       if (getCounter == 4) {
           isPollingGet = false;
           getCounter = null;
       }           
   }
   
   public void publishPoll() {
       if (!isPollingPublish) return;
       initPublishAuditableRequests();
       publishCounter++;
       publishResult();
       if (publishCounter == 4) {
           isPollingPublish = false;
           publishCounter = null;
       }
       
   }
   
    // Check presence of certs
    public Boolean getMdCertExists() {
        return DefenseControllerUtil.checkCertExists('md_cert');
    }

    public Boolean getCsirt1CertExists() {
        return DefenseControllerUtil.checkCertExists('csirt_1');
    }   
   
   // CREATE DEFAULT OBJECTS
   public void init() {
       Integer count = [select id from malwaresignature__c].size();
   
       if (count == 0) {
           MalwareSignature__c one = new MalwareSignature__c();
           one.doIC__c = 'Enforced if IC is not performed';
           one.message__c = '1440x808, en-US, -3';
           one.width__c = 1440;
           one.height__c = 808;
           one.language__c = 'en-US';
           one.offset__c = -3;
           one.approvalstatus__c = 'Approved/Implemented';
           one.block__c = '{ "response": "(#requestParam(\'nav\') == \'export\' or #requestParam(\'export\') != null or #requestParam(\'nav\') == \'csv\' or #requestParam(\'csv\') != null or #requestParam(\'nav\') == \'api\' or #requestParam(\'api\') != null or #requestParam(\'async_export\') != null) ? MASK : IGNORE", "bit": "UserPermissionsBit.exportReport"}';
           insert one;
    
           MalwareSignature__c two = new MalwareSignature__c();
           two.doIC__c = 'Enforced if IC is not performed';
           two.message__c = '1366x673';
           two.width__c = 1366;
           two.height__c = 673;
           two.approvalstatus__c = 'Approved/Implemented';
           two.block__c = '{ "response": "(#requestParam(\'nav\') == \'export\' or #requestParam(\'export\') != null or #requestParam(\'nav\') == \'csv\' or #requestParam(\'csv\') != null or #requestParam(\'nav\') == \'api\' or #requestParam(\'api\') != null or #requestParam(\'async_export\') != null) ? MASK : IGNORE", "bit": "UserPermissionsBit.exportReport"}';
           insert two;       
           
           MalwareSignature__c three = new MalwareSignature__c();
           three.doIC__c = 'Enforced if IC is not performed';
           three.message__c = 'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0)';
           three.userAgent__c = 'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0)';
           three.block__c = '{ "response": "(#requestParam(\'nav\') == \'export\' or #requestParam(\'export\') != null or #requestParam(\'nav\') == \'csv\' or #requestParam(\'csv\') != null or #requestParam(\'nav\') == \'api\' or #requestParam(\'api\') != null or #requestParam(\'async_export\') != null) ? LOGONLY : IGNORE", "bit": "UserPermissionsBit.exportReport"}';
           three.approvalstatus__c = 'Approved/Implemented';
           insert three;    
       }
   }
}
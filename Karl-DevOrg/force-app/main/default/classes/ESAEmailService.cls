/* 

    Developer Name: Jorge L Caceres - jcaceres@salesforce.com | February 2015
    
    Description:
        Action based on email content.
        
*/
global class ESAEmailService implements Messaging.InboundEmailHandler {
    
    private static final String DECLINED_EVENT = 'Declined:';    
    private static final String CANCELED_EVENT = 'Canceled event:';    
    private static final String REQUEST_ID_MARKER = '?Id=';    
    private static final String FROM_ADDRESS_MARKER = 'has declined this invitation';
    private static final String ALPHANUMERIC = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' 
                                             + 'abcdefghijklmnopqrstuvwxyz'
                                             + '0123456789';  
                                             
                                         
    
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
               
        system.debug('email subject -> ' + email.subject);
        system.debug('email body -> ' + email.plainTextBody);
        system.debug('email fromAddress -> ' + email.fromAddress);        
        system.debug('email envelope -> ' + envelope);        
         
        // scan email subject and determine if this is an event declined email
        if ((email.subject!=null) && (email.subject.contains(DECLINED_EVENT) || email.subject.contains(CANCELED_EVENT))) {
            // if it is then scan body and find request id
            Integer id_index_begin = email.plainTextBody.indexOf(REQUEST_ID_MARKER);
            Integer id_index_end = 0;
            String request_id;
            if (id_index_begin > 0) {
                id_index_begin += REQUEST_ID_MARKER.length();
                String temp = email.plainTextBody.mid(id_index_begin, email.plainTextBody.length() - id_index_begin);
                id_index_end = temp.indexOfAnyBut(ALPHANUMERIC);
                if (id_index_end > 0) {
                    request_id = temp.left(id_index_end);
                    cancelReservation(request_id, getFromAddress(email));
                }
            }
        }        

        
        sendDebugEmail(email, envelope);
        
        return null;        
    }
    
    private static String getFromAddress(Messaging.InboundEmail mail) {
        String emailBody = mail.plainTextBody;
        String fromAddress = '';
        Integer from_index_end = emailBody.indexOf(FROM_ADDRESS_MARKER);
        if (from_index_end > 0) {
            // extract actual email address from this index
            fromAddress = emailBody.substring(0, from_index_end);
            fromAddress = fromAddress.substringBetween('itemprop=\"email\" content=\"', '\"');          
        }
        if (fromAddress == null) {
            from_index_end = emailBody.indexOf('From:');
            if (from_index_end > 0) {
                // extract actual email address from this index
                fromAddress = emailBody.substringBetween('From:', 'Date:');          
                fromAddress = fromAddress.substringBetween('<', '>');          
            }
        }
        if (fromAddress == null) fromAddress = mail.fromAddress; 
        system.debug('fromAddress => ' + fromAddress);
        return fromAddress;
    }
    
    private static void cancelReservation(String request_id, String fromAddress) {
        
        system.debug('request_id => ' + request_id);
        system.debug('fromAddress => ' + fromAddress);
        
        try {
            
           // get request which must match the sender email address as well as the request Id
           ESA_Security_Request__c request = [SELECT Id, Office_Hours__c,
                                                     Email__c, Name__c, 
                                                     Supportforce_CaseId__c,
                                                     Supportforce_Case_Number__c,
                                                     Hash_Key__c, GCal_EventId__c,
                                                     Esa_Entity__r.Google_Calendar_Name__c
                                              FROM ESA_Security_Request__c 
                                              WHERE Hash_Key__c = :request_id.trim() 
                                              AND Email__c = :fromAddress.trim()];


           // continue only if OH is present
           if (!string.isBlank(request.Office_Hours__c)) {
                // try to remove event from calendar (callouts must be done before DB updates)
                try {
                    system.debug('GCal_EventId__c => ' + request.GCal_EventId__c);
                    String calendarName = request.Esa_Entity__r.Google_Calendar_Name__c;

                    if(request.GCal_EventId__c == null || calendarName == null) return;
                    
                    esagutil.ESAGoogleOAuthUtil googleAPI = new esagutil.ESAGoogleOAuthUtil(calendarName);
                    googleAPI.deleteEvent(request.GCal_EventId__c);
                } catch (exception e) {sendDebugEmail(e);system.debug('exception during gcal cancel: ' + e);}
            
               // send cancellation confirmation email         
               OrgWideEmailAddress owea = [SELECT Address,Id,DisplayName FROM OrgWideEmailAddress 
                                               WHERE DisplayName = : ESA_AppConstants.ORG_WIDE_EMAIL_ADDRESS];   
               
               string description = '';
               description += 'Your Office Hours Appointment on ';
               description += Datetime.valueOfGmt(request.Office_Hours__c).format('EEEE') + ', ';
               description += Datetime.valueOfGmt(request.Office_Hours__c).formatLong();            
               description += ', has been cancelled \n';
               description += 'because you declined the calendar invite.\n\n';
               description += 'Regards,\n\n';
               description += 'ESA Coordinator';
               
               Messaging.SingleEmailMessage mail= new Messaging.SingleEmailMessage();
               mail.setToAddresses(new String[] {fromAddress});
               mail.setCcAddresses(new String[] {owea.Address});
               mail.setOrgWideEmailAddressId(owea.Id);
               mail.setReplyTo(owea.Address);
               mail.setSubject('Office Hours Cancellation');
               mail.setPlainTextBody('Office Hours Cancellation Notification' 
                            + '\n\n' + description);
               Messaging.sendEmail(new Messaging.Email[] {mail});

              if(request.Supportforce_Case_Number__c != null) {
                String requestorInfo = request.Name__c + ' ('+ request.Email__c +')';
                updateExternalRecordComments(request.Supportforce_Case_Number__c, request.Supportforce_CaseId__c, requestorInfo);
              }
                                                  
               // clear OH data
               request.Office_Hours__c = null;
               request.Reservation_Key__c = null;
               request.Office_Hours_Remote__c = false;
               request.GCal_EventId__c = null;
               request.G2M_JoinURL__c = null;
               request.G2M_MeetingId__c = null; 
               update request;   
           }   
                                       
       } catch (exception e) {sendDebugEmail(e);system.debug('exception during cancel reservation: ' + e);}    
            
    }

    @future (callout=true)
    public static void updateExternalRecordComments(String externalNumber, String externalId, String requestorInfo) {
      Database.SaveResult sr;
      if(externalNumber.startsWithIgnoreCase('W-') == false) {
        CaseComment__x sfCaseComment = new CaseComment__x();
        string comment = 'Requestor canceled the office hours';
        comment += 'Requester info: '+ requestorInfo;
        sfCaseComment.CommentBody__c = (comment.length() < 4000)? comment : comment.abbreviate(4000);
        sfCaseComment.IsPublished__c = true;
        sfCaseComment.ParentId__c = externalId;
        if(!test.isRunningTest()) {
          sr = Database.insertImmediate(sfCaseComment);
        }
      }else {
        FeedItem__x workFeedItem = new FeedItem__x();
        String comment = '\nRequester Cancelled the Office Hours \n\n';
        comment += 'Requester info: '+ requestorInfo;
        workFeedItem.Body__c = comment;
        workFeedItem.ParentId__c = externalId;
        workFeedItem.Status__c = 'Published';
         if(!test.isRunningTest()) {
         sr = Database.insertImmediate(workFeedItem);
         }
      }
    }
    
    private static void sendDebugEmail (Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
 
        string emailBody = '';
 
        emailBody += 'Subject: \n\n';
        emailBody += email.subject==null? '' : email.subject; 
 
        emailBody += '\n\nPlainTextBody: \n\n';
        emailBody += email.plainTextBody==null? '' : email.plainTextBody;

        emailBody += '\n\nHtmlBody : \n\n';
        emailBody += email.htmlBody==null? '' : email.htmlBody;

        emailBody += '\n\nEnvelope from address: \n\n';
        emailBody += envelope.fromAddress==null? '' : envelope.fromAddress;
        
        sendDebugEmail(emailBody);
                                
    }     
    
    private static void sendDebugEmail (Exception e) {
        string emailBody = 'Exception: \n\n';
        emailBody += e.getMessage();
        
        sendDebugEmail(emailBody);
                                
    }     
    
    private static void sendDebugEmail (String emailBody) {
        
       try {
                            
            
           // send cancellation confirmation email         
           OrgWideEmailAddress owea = [SELECT Address,Id,DisplayName FROM OrgWideEmailAddress 
                                           WHERE DisplayName = :ESA_AppConstants.ORG_WIDE_EMAIL_ADDRESS]; 
                                                 
           Messaging.SingleEmailMessage mail= new Messaging.SingleEmailMessage();
           mail.setToAddresses(new String[] {'jcaceres@salesforce.com','suresh.uppala@salesforce.com'});
           mail.setOrgWideEmailAddressId(owea.Id);
           mail.setReplyTo(owea.Address);
           mail.setSubject('ESA Debug Email');
           mail.setPlainTextBody(emailBody);
           Messaging.sendEmail(new Messaging.Email[] {mail});
           
       } catch (exception e) {system.debug(e);}
        
        
    }
    
    
}
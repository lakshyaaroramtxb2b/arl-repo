public without sharing class THValidService {

    public static String getOrgValidty(DateTime dt) {
        Integer recCount = [SELECT count() FROM Trailhead_Validity__c Where Recent_Validity_Time__c < :dt];
        if(recCount > 0) {
          return 'true';
        }else {
           return 'false';
        }
    }

}
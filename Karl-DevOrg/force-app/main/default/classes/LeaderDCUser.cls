global class LeaderDCUser implements Comparable {
    
    public String id {get; set;}
    public String name {get; set;}
    public Decimal incidentsCreated {get; set;}
    public Decimal alertsCreated {get; set;}
    public Decimal eventsCreated {get; set;}
    public String effectiveRate {get; set;}
    
    public LeaderDCUser() {
    }
    
    // Compare opportunities based on the opportunity amount.
    global Integer compareTo(Object compareTo) {

        LeaderDCUser compareToOppy = (LeaderDCUser)compareTo;
        // The return value of 0 indicates that both elements are equal.
        
        Integer returnValue = 0;

        if (this.incidentsCreated > compareToOppy.incidentsCreated) {
            // Set return value to a positive value.
            returnValue = -1;
        } else if (this.incidentsCreated < compareToOppy.incidentsCreated) {
            // Set return value to a negative value.
            returnValue = 1;
        }
        else{
            //sub sort by next value
            //same value as returnValue = 0
            
            if (this.alertsCreated > compareToOppy.alertsCreated) {
                // Set return value to a positive value.
                returnValue = -1;
            } else if (this.alertsCreated < compareToOppy.alertsCreated) {
                // Set return value to a negative value.
                returnValue = 1;
            }
        }
        
        return returnValue;      
    }
}
public with sharing class Integ_DeDupEmployee {
   
     
    private void dedupDuplicateEmailContacts() {
        
        AggregateResult[] dups = [SELECT count(Id), Email email FROM Contact
                                  WHERE createddate = LAST_N_DAYS:30
                                  group by Email having count(Id) > 1];
                
        Set<String> emails = new Set<String>();
        for (AggregateResult dup : dups) {
            emails.add((string)dup.get('email'));
        }
        
        Set<Id> dupIds = new Set<Id>();
        List<Contact> dupContacts = new List<Contact>();
        for (Contact c : [SELECT Id, Email 
                                    FROM Contact 
                                    WHERE Email IN : emails
                                    order by Email, CreatedDate desc]) {
            dupContacts.add(c);
            dupIds.add(c.Id);
        }
        Map<Id, User> portalUsers = new Map<Id, User>([SELECT Id FROM User WHERE ContactId IN : dupIds]);

        Contact lastContact;
        String lastEmail;
        List<Contact> dupsToDelete = new List<Contact>();
        for (Contact c : dupContacts) {
            if (c.Email == lastEmail) {
                // only delete if no portal user exist (only one should be deleted)
                if (!portalUsers.containsKey(c.Id)) dupsToDelete.add(c);    
                if (!portalUsers.containsKey(lastContact.Id)) dupsToDelete.add(lastContact);    
            } else {
                lastEmail = c.Email;
                lastContact = c;    
            }           
        }        
        delete dupsToDelete;
        
    }

    private void mergeDuplicateEmailContacts() {
    
        AggregateResult[] dups = [SELECT count(Id), Email email FROM Contact
                                  WHERE createddate = LAST_N_DAYS:30
                                  group by Email having count(Id) > 1];
        
        Set<String> emails = new Set<String>();
        for (AggregateResult dup : dups) {
            emails.add((string)dup.get('email'));
        }
        
        String lastEmail;
        Contact lastContact;
        List<Contact> dupsToDelete = new List<Contact>();
        Map<Contact, Contact> dupsToMerge = new Map<Contact, Contact>();
        for (Contact c : [SELECT Id, Email 
                                    FROM Contact
                                    WHERE Email IN : emails
                                    order by Email, CreatedDate desc]) {
            if (c.Email == lastEmail) {
                dupsToMerge.put(lastContact, c);    
            } else {
                lastEmail = c.Email;
                lastContact = c;    
            }           
        }
        
        for (Contact masterC : dupsToMerge.keySet()) {
            try {
                merge masterC dupsToMerge.get(masterC);
            } catch (DmlException e) {
                // Process exception
                System.debug('An unexpected error has occurred: ' + e.getMessage()); 
            }        
        }
                    
    }
     
    private void dedupDuplicateEmail() {
    
        AggregateResult[] dups = [SELECT count(Id), INTEG_Email__c email FROM INTEG_Employee__c group by INTEG_Email__c having count(Id) > 1];
        
        Set<String> emails = new Set<String>();
        for (AggregateResult dup : dups) {
            emails.add((string)dup.get('email'));
        }
        
        String lastEmail;
        List<INTEG_Employee__c> dupsToDelete = new List<INTEG_Employee__c>();
        for (INTEG_Employee__c e : [SELECT Id, INTEG_Email__c 
                                    FROM INTEG_Employee__c 
                                    WHERE INTEG_Email__c IN : emails
                                    order by INTEG_Email__c, CreatedDate desc]) {
            if (e.INTEG_Email__c == lastEmail) {
                dupsToDelete.add(e);    
            } else {
                lastEmail = e.INTEG_Email__c;    
            }           
        }
        
        delete dupsToDelete;
        
    }
    
    private void dedupDuplicateOrg62Id() {
    
        AggregateResult[] dups = [SELECT count(Id), INTEG_Org62UserId__c org62Id FROM INTEG_Employee__c group by INTEG_Org62UserId__c having count(Id) > 1];
        
        Set<String> org62Ids = new Set<String>();
        for (AggregateResult dup : dups) {
            org62Ids.add((string)dup.get('org62Id'));
        }
        
        String lastId;
        List<INTEG_Employee__c> dupsToDelete = new List<INTEG_Employee__c>();
        for (INTEG_Employee__c e : [SELECT Id, INTEG_Org62UserId__c 
                                    FROM INTEG_Employee__c 
                                    WHERE INTEG_Org62UserId__c IN : org62Ids
                                    order by INTEG_Org62UserId__c, CreatedDate desc]) {
            if (e.INTEG_Org62UserId__c == lastId) {
                dupsToDelete.add(e);    
            } else {
                lastId = e.INTEG_Org62UserId__c;    
            }           
        }
        
        delete dupsToDelete;
        
    }
}
@isTest
public class PRT_StaffCapacityControllerTest {
    @TestSetup
    static void makeData(){
        List<Program__c> programListCreate = PRT_TestDataFactory.createPrograms(1,true);
        List<Portfolio__c> portfolioDataList = PRT_TestDataFactory.createPortfolio(1,true);
        List<Contact> contactListRecord = PRT_TestDataFactory.createContact(1,true);
        List<project__c> projectRecordList= PRT_TestDataFactory.createProjects(1,false);
        projectRecordList[0].Portfolio__c = portfolioDataList[0].Id;      
        INSERT projectRecordList;
        List<Milestone_PRT__c> milestoneListRecord = PRT_TestDataFactory.createMilestone(1, projectRecordList[0].id, true);
        
        List<Assignment__c> assignmentListRecord = PRT_TestDataFactory.createAssignment(1, projectRecordList[0].id, milestoneListRecord[0].id, false);
        assignmentListRecord[0].Contact__c = contactListRecord[0].Id;
        assignmentListRecord[0].Project__c = projectRecordList[0].Id;
        assignmentListRecord[0].Program__c = programListCreate[0].Id;
        INSERT assignmentListRecord;

        List<Assignment_Week__c> assignmentWeekListRecord = PRT_TestDataFactory.createAssignmentWeek(1,assignmentListRecord[0].id,contactListRecord[0].id,false);
        assignmentWeekListRecord[0].Number_of_Hours__c = 40;
        assignmentWeekListRecord[0].Week_Start_Date__c = Date.today().toStartOfWeek();
        assignmentWeekListRecord[0].Week_End_Date__c = Date.today().toStartOfWeek().addDays(7);
        INSERT assignmentWeekListRecord;
    }

    @isTest
    public static void getCapacityDataTest() {
        List<Portfolio__c> portfolioList = new List<Portfolio__c>([SELECT Id, Name FROM Portfolio__c]);
        PRT_StaffCapacityController.mockPortfolioList.addAll(portfolioList);

        List<PRT_StaffCapacityController.StaffCapacityData> staffCapacityList = PRT_StaffCapacityController.getCapacityData(String.valueOf(Date.today()), 'Week', 'AND Assignment__r.Project_Name__c = \'Test Project0\'', 0, ' Test0');
        System.assertNotEquals(staffCapacityList, null);
        staffCapacityList = PRT_StaffCapacityController.getCapacityData(String.valueOf(Date.today()), 'Month', ' AND Assignment__r.Project_Name__c = \'Test Project0\'', 0, ' Test0');
        System.assertNotEquals(staffCapacityList, null);
        staffCapacityList = PRT_StaffCapacityController.getCapacityData(String.valueOf(Date.today()), 'Fiscal Quarter', 'AND Assignment__r.Project_Name__c = \'Test Project0\'', 0, ' Test0');
        System.assertNotEquals(staffCapacityList, null);
        staffCapacityList = PRT_StaffCapacityController.getCapacityData(String.valueOf(Date.today()), 'Fiscal Year', 'AND Assignment__r.Project_Name__c = \'Test Project0\'', 40, ' Test0');
        System.assertEquals(staffCapacityList, null);
    }
    
    @isTest
    public static void getProjectDataTest() {
        List<Portfolio__c> portfolioList = new List<Portfolio__c>([SELECT Id, Name FROM Portfolio__c]);
        PRT_StaffCapacityController.mockPortfolioList.addAll(portfolioList);
        List<PRT_StaffCapacityController.ProjectData> projectDataList = PRT_StaffCapacityController.getProjectData(String.valueOf(Date.today()), 'Week', 'AND Assignment__r.Project__r.Name = \'Test Project0\'', 0, 'Test Project0');
        System.assertNotEquals(projectDataList, null);
        projectDataList = PRT_StaffCapacityController.getProjectData(String.valueOf(Date.today()), 'Month', 'AND Assignment__r.Project_Name__c = \'Test Project0\'', 0, 'Test Project0');
        System.assertNotEquals(projectDataList, null);
        projectDataList = PRT_StaffCapacityController.getProjectData(String.valueOf(Date.today()), 'Fiscal Quarter', 'AND Assignment__r.Project_Name__c = \'Test Project0\'', 0, 'Test Project0');
        System.assertNotEquals(projectDataList, null);
        projectDataList = PRT_StaffCapacityController.getProjectData(String.valueOf(Date.today()), 'Fiscal Year', 'AND Assignment__r.Project_Name__c = \'Test Project0\'', 40, 'Test Project0');
        System.assertEquals(projectDataList, null);
    }
    
    @isTest
    public static void getFilterPicklistValuesTest() {
        List<Portfolio__c> portfolioList = new List<Portfolio__c>([SELECT Id, Name FROM Portfolio__c]);
        PRT_StaffCapacityController.mockPortfolioList.addAll(portfolioList);
        List<PRT_StaffCapacityController.FilterValues> employeeFilterValuesList = PRT_StaffCapacityController.getFilterPicklistValues('Employee');
        System.assertEquals(employeeFilterValuesList.size(), 6);
        List<PRT_StaffCapacityController.FilterValues> projectFilterValuesList = PRT_StaffCapacityController.getFilterPicklistValues('Project');        
        System.assertEquals(employeeFilterValuesList.size(), 6);
    }
    
    @isTest
    public static void getCapacityRangeTest() {
        List<PRT_StaffCapacityController.CapacityRangeProperties> capacityRangeProperties = PRT_StaffCapacityController.getCapacityRange();
        System.assertEquals(capacityRangeProperties.size(), 4);
    }
    
    @isTest 
    public static void getAssignmentDataTest() {
        List<Assignment__c> assignmentList = [SELECT Id FROM Assignment__c];
        List<Assignment__c> data = PRT_StaffCapacityController.getAssignmentData(String.valueOf(assignmentList.get(0).Id), ' Test0');
        System.assertNotEquals(data, null);
    }
}
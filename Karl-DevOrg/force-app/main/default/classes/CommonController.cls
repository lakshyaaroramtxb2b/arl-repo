/**
 * Lightning Controller Class
 * @author Raghu
 */
public without sharing class CommonController {

    @AuraEnabled
    public static Boolean isSessionActive() {
        // Request might not even reach here when session in invalid
        return UserInfo.getSessionId() != null;
    }
}
public with sharing class SC_BulkAttendanceController {
    
    public Attachment attendanceFile { get; set; }
    public boolean userHasPermissionToUpload;
    private Training_Course__c course;
    private integer emailColumn;
    private integer dateColumn;
    public final string ERROR_STATUS = 'Error';
    private final string SUCCESS_STATUS = 'Success';
    private final string COURSE_COMPLETED_STATUS = 'Course Completed';

    public SC_BulkAttendanceController(ApexPages.StandardController sc) {
        course = (Training_Course__c) sc.getRecord();
        attendanceFile = new Attachment();
        if (course.Provider__c != TrainingCourse_Constants.PROVIDER_MANUAL) {
            ApexPages.addMessage(new ApexPages.Message(
                ApexPages.Severity.ERROR,
                'Are you sure you want to upload completion dates for this course? ' +
                'Generally only courses with their provider listed as "Manual" require using this tool ' +
                'and this course has "' + course.Provider__c + '" listed as it\'s provider. ' +
                'This course should support automatic syncing of completion dates.'));
        }
    }

    public boolean getUserHasPermissionToUpload(){
        if(userHasPermissionToUpload==null){
            userHasPermissionToUpload = Schema.sObjectType.Training_Course_Taken__c.isUpdateable()
                    && Schema.sObjectType.Training_Course_Taken__c.fields.Status__c.isUpdateable()
                    && Schema.sObjectType.Training_Course_Taken__c.fields.Date_Completed__c.isUpdateable();
        }
        return userHasPermissionToUpload;
    }

    public void submit() {
        // Check permissions
        if(!getUserHasPermissionToUpload()){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,
                    'You do not have permission to update attendance information.'));
            return;
        }

        // Parse CSV. Get out email and completion date for attendee
        string fileBody = attendanceFile.Body.toString();
        CsvReader reader = new CsvReader(fileBody);
        if(reader.numberOfLines()>1001){
            ApexPages.addMessage(
                    new ApexPages.Message(ApexPages.Severity.ERROR,
                    'Cannot upload more than 1000 rows')
            );
            return;
        }

        uploadResult[] results = new uploadResult[]{};
        set<string> emails = new set<string>();
        if(validHeaders(reader.readLine())){
            string[] line;
            do{
                line = reader.readLine();
                if(line<>null && line.size()>=2){
                    uploadResult attendee = new uploadResult(line[emailColumn],line[dateColumn]);
                    results.add(attendee);
                    emails.add(attendee.email);
                }
            }while (line<>null&&line.size()>1 && line[0]<>'');
        }else{
            return;
        }

        // Get relevant enrollments
        Map<string,Training_Course_Taken__c> emailEnrollmentMap = new Map<string, Training_Course_Taken__c>{};
        for(Training_Course_Taken__c enrollment : [
                SELECT Attendee_Email__c, Status__c, Date_Completed__c, Contact__r.Email
                FROM Training_Course_Taken__c
                WHERE Training_Course__c = :course.Id AND Contact__r.Email in :emails]){
            emailEnrollmentMap.put(enrollment.Contact__r.Email,enrollment);
        }

        // Update SObjects from Upload
        for(uploadResult result : results){
            if(result.status == ERROR_STATUS) continue;

            if(!emailEnrollmentMap.containsKey(result.email)){
                result.status = ERROR_STATUS;
                result.error = 'No attendee found for this email.';
            }else{
                Training_Course_Taken__c enrollment = emailEnrollmentMap.get(result.email);
                enrollment.Status__c = COURSE_COMPLETED_STATUS;
                enrollment.Date_Completed__c = result.completionDate;
            }
        }

        Training_Course_Taken__c[] enrollmentUpdates = emailEnrollmentMap.values();

        Database.SaveResult[] saveResults = Database.update(enrollmentUpdates,false);
        Map<string,string> dmlFailures = new Map<string,string>();
        for(Integer i = 0; i < saveResults.size(); i++){
            if(!saveResults[i].success){
                dmlFailures.put(enrollmentUpdates[i].Contact__r.Email,saveResults[i].getErrors()[0].getMessage());
            }
        }

        for(uploadResult result : results){
            if(result.status == ERROR_STATUS){
                continue;
            }else if(dmlFailures.containsKey(result.email)){
                result.status = ERROR_STATUS;
                result.error = dmlFailures.get(result.email);
            }else{
                result.status = SUCCESS_STATUS;
            }
        }

        ApexPages.addMessage(
                new ApexPages.Message(ApexPages.Severity.INFO,'Attendance updated. Check your email for results')
        );

        sendResultsEmail(results);

        // Clear file so a new one can be uploaded
        attendanceFile = new Attachment();
    }

    private boolean validHeaders(string[] input){
        System.debug('input: '+input);
        boolean valid = true;
        string errorMessage = '';
        if(input.size()<>2){
            valid = false;
            errorMessage = 'File should have two columns: "Email" and "Completion Date".';
        }
        for(integer i = 0; i < input.size(); i++){
            System.debug('header column: '+input[i]);
            if(input[i].toLowerCase()=='email'){
                emailColumn = i;
            }
            if(input[i].toLowerCase()=='completion date'){
                dateColumn = i;
            }
        }

        if(emailColumn == null){
            valid = false;
            errorMessage += '  Could not find column "Email".';
        }

        if(dateColumn == null){
            valid = false;
            errorMessage += '  Could not find column "Completion Date".';
        }

        if(!valid){
            ApexPages.addMessage(
                    new ApexPages.Message(ApexPages.Severity.ERROR,errorMessage)
            );
        }
        return valid;
    }

    private class uploadResult {
        public string   email           {get; set;}
        public string   dateString      {get; set;}
        public date     completionDate  {get; set;}
        public string   status          {get; set;}
        public string   error           {get; set;}

        public string headerString(){
            return 'Email, Completion Date, Status, Error\n';
        }

        public string outputAsCsv(){
            return email + ',' + dateString + ',' + status + ',' + error + '\n';
        }

        private uploadResult(string email, string dateString){
            this.email = email;
            this.dateString = dateString;
            this.error = '';
            try{
                completionDate = Date.valueOf(dateString);
            }catch(exception ex){
                status = 'Error';
                error = ex.getMessage();
            }
        }

        private uploadResult(){}
    }

    private void sendResultsEmail(uploadResult[] results){

        try{
            Messaging.reserveSingleEmailCapacity(1);
        }catch(Exception ex){
            ApexPages.addMessage(
                    new ApexPages.Message(
                            ApexPages.Severity.WARNING,
                            'Cannot send results email. Details: ' + ex.getMessage()
                    )
            );
        }

        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        email.setSubject('Bulk Attendance Update Results for "' + course.Name + '"');
        email.setPlainTextBody('See attached file for results');
        email.setTargetObjectId(UserInfo.getUserId());
        email.setSaveAsActivity(false);

        Messaging.EmailFileAttachment attachment = new Messaging.EmailFileAttachment();
        string bodyString = new uploadResult().headerString();
        for(uploadResult result : results){
            bodyString += result.outputAsCsv();
        }
        attachment.body = Blob.valueOf(bodyString);
        attachment.contentType = 'text/csv';
        attachment.fileName = 'bulkAttendanceResults' + DateTime.now().getTime() + '.csv';
        email.setFileAttachments(new Messaging.EmailFileAttachment[] { attachment });
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { email });
    }
}
/**
 * @author: ralph@callaway.cloud
 * @description: batch enroller
 */
public class SC_Enrollment_Batch implements Database.Batchable<sObject>, Database.Stateful {
    
    public static final String BASE_SELECT =
        'SELECT ' +
            'Email, ' +
            'Is_Active__c, ' +
            'Is_Frozen__c, ' + 
            'Is_On_Leave_Of_Absence__c, ' +
            'Org62_User_Id__c, ' +
            'RecordTypeId, ' +
            'Id ' +
        'FROM Contact ';
    
    Set<String> enrolleeEmails = new Set<String>();
    String query;
    SC_Enroller enroller;
    Id courseId;
    Boolean logInvalid = false;
    Boolean logSkipped = false;
    
    /**
     * Constructor for a list of emails
     * @param  enrolleeEmails list of emails to enroll
     * @param  opts           enrollment options
     * @return                batch instance
     */
    public SC_Enrollment_Batch(String[] enrolleeEmails, SC_Enroller.Options opts) {
        this(new Set<String>(enrolleeEmails), opts);
    }

    /**
     * Constructor for a set of emails
     * @param  enrolleeEmails set of emails to enroll
     * @param  opts           enrollment options
     * @return                batch instance
     */
    public SC_Enrollment_Batch(Set<String> enrolleeEmails, SC_Enroller.Options opts) {
        this('Email IN :enrolleeEmails', opts);
        this.enrolleeEmails = enrolleeEmails;
        System.assert(!enrolleeEmails.isEmpty(), 'enrolleeEmails must have at least one value');
        logInvalid = true;
        logSkipped = true;
    }

    /**
     * Constructor for a contact where clause
     * @param  contactFilter contact where clause (don't include 'WHERE', just the part after that)
     * @param  opts          enrollment options
     * @return               [description]
     */
    public SC_Enrollment_Batch(String contactFilter, SC_Enroller.Options opts) {
        System.assertNotEquals(null, contactFilter, 'contactFilter required');
        enroller = new SC_Enroller(opts);
        this.query = BASE_SELECT + ' WHERE ' + contactFilter;
        this.courseid = opts.courseId;
    }
    
    public Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }

    public void execute(Database.BatchableContext BC, List<Contact> contacts) {
        // if processing a list of emails track which ones we've processed
        for (Contact contact : contacts) {
            enrolleeEmails.remove(contact.Email);
        }

        // process enrollments
        SC_Enroller.Enrollment[] enrollments = enroller.enroll(contacts);

        // log results
        logResults(enrollments);
    }
    
    public void finish(Database.BatchableContext BC) {
        logMissingEmails(enrolleeEmails);
        Training_Course__c course = [SELECT Name, Last_Enrollment__c FROM Training_Course__c WHERE Id = :courseId];
        course.Last_Enrollment__c = DateTime.now();
        update course;
        Attachment attachment = new Attachment();
        attachment.Body = Blob.valueOf(String.join(results, '\n'));
        attachment.ContentType = 'text/csv';
        attachment.Name = course.Name + ' Enrollment Results ' + DateTime.now().format() + '.csv';
        attachment.ParentId = course.Id;
        insert attachment;
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        email.setTargetObjectId(UserInfo.getUserId());
        email.setSubject(course.Name + ' Enrollment Result ' + DateTime.now().format());
        email.setPlainTextBody('See attachment for results. Historical results are available in the training course attachment related list');
        email.setSaveAsActivity(false);
        email.setCCAddresses(Label.SC_TrainingBatchCC.split('\\s*,\\s*'));
        email.setEntityAttachments(new Id[] { attachment.Id });
        if (!Test.isRunningTest()) {
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { email });    
        }
    }

    String[] headerRow = new String[] {
        'Result',
        'Error/Details',
        'Contact Email',
        'Contact Id',
        'Enrollment Id',
        'Enrollment Record Type',
        'Enrollment Notification Date',
        'Enrollment Due Date',
        'Notification Sent'
    };
    List<String> results = new List<String> {
        String.join(headerRow, ',')
    };
    private void logResults(SC_Enroller.Enrollment[] enrollments) {
        for (SC_Enroller.Enrollment enrollment : enrollments) {
            if (enrollment.result == SC_Enroller.Result.INVALID && !logInvalid) continue;
            if (enrollment.result == SC_Enroller.Result.SKIPPED && !logSkipped) continue;
            String[] row = new String[0];
            row.add(String.valueOf(enrollment.result));
            row.add('"' + String.join(enrollment.errors,'\n') + '"');
            row.add(enrollment.contact.Email);
            row.add(enrollment.contact.Id);
            if (enrollment.newRec != null) {
                row.add(enrollment.newRec.Id);
                row.add((enrollment.newRec.RecordTypeId == TrainingCourseTaken_Constants.RT_ID_MANDATORY) ? 
                    'Mandatory Enrollment' : 'Voluntary Enrollment');
                row.add(String.valueOf(enrollment.newRec.Enrollment_Notification_Date__c));
                row.add(String.valueOf(enrollment.newRec.Due_Date__c));
                row.add(String.valueOf(enrollment.notified));
            }
            results.add(String.join(row,','));
        }
    }    
    private void logMissingEmails(Set<String> missedEmails) {
        for (String email : missedEmails) {
            String[] row = new String[0];
            row.add('NOT_FOUND');
            row.add('No valid contact (record type = Salesforce Employee and valid Org 62 User Id) found with email');
            row.add(email);
            results.add(String.join(row, ','));
        }
    }
    
}
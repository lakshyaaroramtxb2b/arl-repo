/**
 * @author Lakshya Arora
 * @email lakshya.arora@mtxb2b.com
 * @description Batch class for providing access to Evidence Request records
*/
public class KARL_AccessOnEvidenceRequestBatch implements Database.Batchable<sObject>,Database.Stateful, schedulable{
    public String evidenceId;
    
    /**
     * @author Lakshya Arora
     * @email lakshya.arora@mtxb2b.com
     * @description Set the evidence Id (public string)
    */
    public KARL_AccessOnEvidenceRequestBatch(String eviodenceId){
        if(String.isNotEmpty(eviodenceId)){
             this.evidenceId = eviodenceId;
        }
       
    }

    /**
     * @author Lakshya Arora
     * @email lakshya.arora@mtxb2b.com
     * @description Start Batch Method
    */
	public Database.QueryLocator start(Database.BatchableContext BC){
        Date todaysDate = date.today();
        
        return Database.getQueryLocator([SELECT Id, ParentId, IsDeleted, RowCause, AccessLevel, UserOrGroupId 
                                                          FROM KARL_Evidence_Request__Share
                                                          WHERE ParentId  =: evidenceId AND RowCause = 'Manual' AND AccessLevel= 'Read']);
    }

    /**
     * @author Lakshya Arora
     * @email lakshya.arora@mtxb2b.com
     * @description Execute Batch Method
    */
    public void execute(Database.BatchableContext BC,List<KARL_Evidence_Request__Share> scopeList){
        List<KARL_Evidence_Request__Share> deleteShareRecordsList = new List<KARL_Evidence_Request__Share>();
        for(KARL_Evidence_Request__Share evidenceShare: scopeList){
            deleteShareRecordsList.add(evidenceShare);
        }
        
        if(!deleteShareRecordsList.isEmpty() && !Test.isRunningTest()){
            delete deleteShareRecordsList;
        }
    }

    /**
     * @author Lakshya Arora
     * @email lakshya.arora@mtxb2b.com
     * @description Finish Batch Method
    */
    public void finish(Database.BatchableContext BC){
        
    }

    /**
     * @author Lakshya Arora
     * @email lakshya.arora@mtxb2b.com
     * @description Schedule Batch Job
    */
    public void execute(SchedulableContext SC) {
        if(!Test.isRunningTest()){
            database.executebatch(new KARL_AccessOnEvidenceRequestBatch(evidenceId),200);
        }
    }
}
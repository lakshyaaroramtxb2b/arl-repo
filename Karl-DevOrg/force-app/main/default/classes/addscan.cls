public class addscan {
   
    public Boolean IS_DELAYED = False;
    public String scantype {get; set;} 
    public static Integer MIN_SCAN_WAIT_HOURS = 2;
    public String btn { get; set; }
    public String enteredText {get; set;}  
    public String friendlyName {get;set;}  
    
    public addscan() {
        Integer queueLen = [SELECT count() FROM Scan_Queue__c WHERE Status__c = 'In queue' or Status__c = 'Waiting to scan' or Status__c = 'Downloading code' or Status__c = 'Scanning'];
        if (queueLen > 5) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'At this time the Force.com Security Source Code Scanner is experiencing delays.  Expect delays as we work through this issue.'));
        }
    }
    public PageReference process() {
        if(enteredText == null)
        {
            return null;
        }
        Pattern validEmail = Pattern.compile('^.+@.+$');
        Pattern whiteSpace = Pattern.compile('^\\s*$');
        Matcher emailMatcher = validEmail.matcher(enteredText);
        if ((enteredText == null) || 
            (validEmail.matcher(enteredText).matches() == False) || (whiteSpace.matcher(enteredText).matches()==True)){
            //PageReference p = new PageReference('/sourcescanner#invalid');
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'The username submitted is not a valid Salesforce username.'));
            //return p;
            return null;
        }
        
        // Enforce throttling and only allow scans every MIN_SCAN_WAIT_HOURS
        Datetime newest_allowed = Datetime.now().addHours(0-MIN_SCAN_WAIT_HOURS);
        List<CodeScan__c> latest = [SELECT Id,Created__c FROM CodeScan__c WHERE (Created__c != null) 
                                           AND (Created__c >:newest_allowed) 
                                           AND (Username__c =: enteredText)
                                           LIMIT 1];
                                           
        if (latest.size() > 0){
            // They have one pending or completed already
            // Might want to give nextAllowed in user's timezone...
            String nextAllowed = latest.get(0).Created__c.addHours(MIN_SCAN_WAIT_HOURS).format('MMM d, h:m a');
            String message = 'Due to high volume, an application can only be scanned every '+String.valueOf(MIN_SCAN_WAIT_HOURS);
            message += ' hours.  Please submit the application again after '+nextAllowed+'.  We apologize for any inconvenience.';
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,message));
            return null;
        }
        
        // Don't allow user to submit a new scan if they have one in the queue already.
        List<CodeScan__c> current = [SELECT Id,Created__c FROM CodeScan__c WHERE (Created__c != null)
                                            AND (Username__c =: enteredText)
                                            AND (WorkState__c = 'New' OR WorkState__c = 'Passed To Worker' or WorkState__c = 'Download Started')
                                            LIMIT 1];
        
        if (current.size() > 0){
            String message = 'Due to high volume, users may only have one scan pending at any given time.  Please wait for your current scan to finish.';
            message += '  We apologize for any inconvenience.';
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, message));
            return null;
        }
        
        // The checks above for throttling will let two scans submitted at exactly the same time through.  (This happened on 10/21/2011 at about 9:30 am.)
        // To avoid this, perhaps we should put the throttling logic in a trigger.
        
        CodeScan__c scan = new CodeScan__c();
        scan.Username__c = enteredText;
        scan.ScanType__c = scantype;
        scan.Name = friendlyName;
        insert scan;
        PageReference pa = new PageReference('/sourcescanner#complete');
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Thank you for submitting a request for code to be scanned.  You will receive a report to the email address we have on record.  If you encounter any issues with the results, please email securecloud [at] salesforce [dot] com.'));
        //return pa;
        return null;
    }
    
        public  void setUser(String s)
    { enteredText = s; }
    
    public  string getUser()
    { return enteredText; }

}
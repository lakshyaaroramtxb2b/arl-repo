public class EsaHandlerIntakeCalendar {
    
    public static string calendarLinkTemplate = 'https://calendar.google.com/calendar/r/eventedit/';


    public static string encodingBase64 (string event, string calId){

        calId = calId + '@g';
        string calendarEvent = event + ' ' + calId;
        string calendarEventBase64 = EncodingUtil.base64Encode(Blob.valueof(calendarEvent));
        string eventOnCalendar = calendarLinkTemplate + calendarEventBase64;

        return eventOnCalendar;

    }

    public static void CronstructEventURL (list <ESA_Security_Request__c> requests){
  
        
        set <id> entityId = new set <id>();
        for (ESA_Security_Request__c request : requests){
            entityId.add(request.ESA_Entity__c);
        }
      
        
        Map<Id, String> entityCalendarMap = new Map<Id, String> ();
        for (ESA_Entity__c entity : [SELECT Id, Google_Calendar_Name__c FROM ESA_Entity__c WHERE id IN: entityId]) {
            entityCalendarMap.put(entity.Id, entity.Google_Calendar_Name__c);
        }
        

        //Edit request
        if (!entityCalendarMap.isEmpty()){
            for (ESA_Security_Request__c req : requests){
            	if (entityCalendarMap.containsKey(req.ESA_Entity__c) && req.Office_Hours__c != null) {
                	string eventid = req.GCal_EventId__c;
                    string calendar = entityCalendarMap.get(req.ESA_Entity__c).substringBefore('@');
                    req.CalEvent__c = encodingBase64(eventId, calendar);
                }
             }
                
         }
           
     }
}
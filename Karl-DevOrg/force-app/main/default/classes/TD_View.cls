/*

    Developer Name: Jorge L Caceres - jcaceres@salesforce.com | November 2015    

    Description: Handles either marking a deliverable done or redirecting user to view it
    
*/

public class TD_View {

    private Trust_Deliverable__c deliverable;
    
    public TD_View () {}

    public TD_View(ApexPages.StandardController controller) {    
        deliverable = (Trust_Deliverable__c) controller.getRecord();        
    }

    // redirect to salesforce standard salesforce layout
    public PageReference redirectOrMarkDone () {
        PageReference viewPage = (new ApexPages.StandardController(deliverable)).View();
        viewPage.getParameters().put('nooverride', '1');

        try {
            String done = ApexPages.currentPage().getParameters().get('done');
            if (done=='1') {
                TD_EmailService.markComplete(deliverable.Id, UserInfo.getUserEmail());
                return null;  
            }
            return viewPage;
        } catch (Exception e) {
            return viewPage;
        }
    }
    
    
}
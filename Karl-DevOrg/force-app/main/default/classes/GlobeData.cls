public with sharing class GlobeData 
{
    public String jsonString {get;set;}
    public List<SFDC_Internal_Network__c> conts {get; set;}
    
    //Constructor
    public GlobeData()
    {
        jsonString = prepareData();
    }

    //Temp Method to prepare the Data
    private String prepareData()
    {
        String strIPnum = ApexPages.currentPage().getParameters().get('ipNum');
        
        if(strIPnum != null)
        { 
            Integer ipNum = integer.valueof(strIPnum);
            
            conts = [SELECT Id, City__c, Network__c, IP_Start__c, IP_End__c, Lat__c, Lng__c, Description__c FROM SFDC_Internal_Network__c WHERE IP_Start__c <= :ipNum AND IP_End__c >= :ipNum];
            
            return JSON.serialize(conts);
        }
        
        return '[]';
    }
}
@isTest                                
public class FDCSETrialSignupControllerTest {
    // Trial Success
    static testmethod void testFDCSESignupSuccess() {
        insert new Trial_Id__c(name='FDCSE',Trial_Id__c='0TT500000004xdw');
        FDCSETrialSignupController dfsc = new FDCSETrialSignupController();
        dfsc.msa = true;
        dfsc.contactOK = true;
        dfsc.firstName = 'Bob';
        dfsc.lastName = 'Bob'; 
        dfsc.company = 'Salesforce.com';
        dfsc.email = 'ktobener@salesforce.com' ;
        dfsc.createSignup();
        system.debug(dfsc.publickey);
        dfsc.reset();
    }
    // Trial Fail
    static testmethod void testFDCSESignupFail() {
        insert new Trial_Id__c(name='FDCSE',Trial_Id__c='0TT500000004xdw');
        FDCSETrialSignupController dfsc = new FDCSETrialSignupController();
        dfsc.msa = false;
        dfsc.contactOK = false;
        dfsc.firstName = '';
        dfsc.lastName = ''; 
        dfsc.company = '';
        dfsc.email = '' ;
        dfsc.createSignup();
    }    
}
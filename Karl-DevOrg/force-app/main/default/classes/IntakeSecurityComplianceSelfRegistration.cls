global class IntakeSecurityComplianceSelfRegistration extends IntakeWhiteListedSelfRegistration implements Auth.ConfigurableSelfRegHandler {
    global override String getEntityCode() {
        return 'VAPExternal';
    }
        
    global override String[] getPermissionSetNames() {
        return new String[]{'ESA_Security_Request_Customer'};
    }
}
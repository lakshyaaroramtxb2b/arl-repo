@isTest
public class SPT_LookupController_Test {
    @TestSetup
    static void makeData(){
        List<Case> parentCaseRecord = SPT_TestUtility.createCaseRecords('New', 'USD', 'Web', 1, false);
        for(Case caseRec : parentCaseRecord) {
            caseRec.Enterprise_Security_Case_Number__c = '132456';
        }
        INSERT parentCaseRecord;
        
        List<Case> parentCaseRecord2 = SPT_TestUtility.createCaseRecords('New', 'USD', 'Phone', 1, false);
        for(Case caseRec : parentCaseRecord2) {
            caseRec.Enterprise_Security_Case_Number__c = '794564';
        }
        INSERT parentCaseRecord2;
        
        List<Case> caseRecord = SPT_TestUtility.createCaseRecords('New', 'USD', 'Email', 1, false);
        caseRecord.get(0).ParentId = parentCaseRecord.get(0).Id;
        caseRecord.get(0).Enterprise_Security_Case_Number__c = '456794';
        INSERT caseRecord;
    }

    @isTest
    public static void getResultsTest() {
        Test.startTest();
        List<SPT_LookupController.SObJectResult> caseList = SPT_LookupController.getResults('Case', 'Origin', 'Email');
        System.assertEquals(1, caseList.size());
        Test.stopTest();
    }

    @isTest
    public static void getCaseNumberTest() {
        List<Case> caseList = new List<Case>([SELECT Id, ParentId, Parent.CaseNumber FROM Case WHERE Origin = 'Email']);
        Test.startTest();
        List<SPT_LookupController.ParentCaseWrapper> parentCaseList = SPT_LookupController.getCaseNumber(caseList.get(0).Id);
        System.assertEquals(1, parentCaseList.size());
        Test.stopTest();
    }
    @isTest
    public static void getCaseDetailTest() {
        List<Case> caseList = new List<Case>([SELECT Id, ParentId, Parent.CaseNumber FROM Case WHERE Origin = 'Email']);
        Test.startTest();
        SPT_LookupController.CaseDetailWrapper parentCaseList = SPT_LookupController.getCaseDetail(caseList.get(0).Id);
        //System.assertEquals(1, parentCaseList.size());
        System.assert(parentCaseList.caseNumber != null);
        Test.stopTest();
    }
    @isTest
    public static void deleteCaseTest() {
        List<Case> caseList = new List<Case>([SELECT Id, ParentId, Parent.CaseNumber FROM Case WHERE Origin = 'Email']);
        Test.startTest();
        SPT_LookupController.deleteCase(caseList.get(0).Id);
        Test.stopTest();
        caseList = [SELECT Id, ParentId, Parent.CaseNumber FROM Case WHERE Origin = 'Email'];
        system.assertEquals(0, caseList.size());
    }


    @isTest
    public static void updateParentCaseTest() {
        List<Case> caseList = new List<Case>([SELECT Id, ParentId, Parent.CaseNumber FROM Case WHERE Origin = 'Email']);
        List<Case> parentCaseList = new List<Case>([SELECT Id, ParentId, Parent.CaseNumber FROM Case WHERE Origin = 'Phone']);
        Test.startTest();
        SPT_LookupController.updateParentCase(caseList.get(0).Id, parentCaseList.get(0).Id);
        caseList = new List<Case>([SELECT Id, ParentId, Parent.CaseNumber FROM Case WHERE Origin = 'Email']);
        System.assertEquals(parentCaseList.get(0).Id, caseList.get(0).ParentId);
        Test.stopTest();
    }
}
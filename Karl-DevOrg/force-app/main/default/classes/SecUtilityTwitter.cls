global with sharing class SecUtilityTwitter {

    public SecUtilityTwitter(SecContactEmailController controller) {}
    global SecUtilityTwitter(ApexPages.StandardController stdController) {}
    global SecUtilityTwitter() {}
    
    static final string TIMELINE_URL = 'http://api.twitter.com/1/statuses/user_timeline.json';
    
    @RemoteAction
    global static String getUserTimeline(String paramString) {
        HttpRequest req = new HttpRequest();
        req.setMethod('GET');
        req.setEndpoint(TIMELINE_URL+'?'+paramString);
        Http http = new Http();
        HTTPResponse res = http.send(req);
        return res.getBody();
    }
}
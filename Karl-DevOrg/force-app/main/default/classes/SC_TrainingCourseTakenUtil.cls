/*
 * Apex class to support Trail enrollment with common methods
 */
public class SC_TrainingCourseTakenUtil {

    public static void deleteRelationships(Training_Course_Taken__c[] enrollments) {
        delete [
            SELECT Id 
            FROM Training_Course_Taken_Relationship__c 
            WHERE Related_Module_Taken__c IN :enrollments
                OR Related_Path_Taken__c IN :enrollments];
    }

    public static Map<Id, List<Training_Course__c>> getPathToModuleMap(Set<Id> pPathIds) {
        Map<Id, List<Training_Course__c>> pathToModuleMap =
            new Map<Id, List<Training_Course__c>>();

        for (Training_Course_Relationship__c relationship :[
            select Related_Module__c, Related_Path__c
            from Training_Course_Relationship__c
            where Related_Path__c in :pPathIds
        ]) {
            if (pathToModuleMap.containsKey(relationship.Related_Path__c)) {
                pathToModuleMap.get(relationship.Related_Path__c).add(
                    new Training_Course__c(
                        Id = relationship.Related_Module__c
                ));
            } else {
                pathToModuleMap.put(
                    relationship.Related_Path__c,
                    new Training_Course__c[] {new Training_Course__c(
                        Id = relationship.Related_Module__c
                )});
            }
        }

        return pathToModuleMap;
    }
}
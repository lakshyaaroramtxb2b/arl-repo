@isTest
public class SPT_CaseCommentTriggerHandler_Test {
    @TestSetup
    static void makeData(){
        Case__x externalCaseRecord = new Case__x();
        Database.insertAsync(externalCaseRecord);

        Case internalCaseRecord = new Case();
        internalCaseRecord.SupportForce_Case_Id__c = externalCaseRecord.ExternalId;
        internalCaseRecord.RecordTypeId = SPT_Constants.SEC_RECORDTYPE_ID;
        internalCaseRecord.Status = 'New';
        internalCaseRecord.Origin = 'Email';
        internalCaseRecord.CurrencyIsoCode = 'USD';
        INSERT internalCaseRecord;
    }

    @isTest static void processCaseCommentsTest() {
        List<Case> internalCaseList = new List<Case>([SELECT Id FROM Case]);

        CaseComment caseCommentRecord = new CaseComment();
        caseCommentRecord.ParentId = internalCaseList.get(0).Id;
        caseCommentRecord.CommentBody = 'Test Comment';
        INSERT caseCommentRecord;
    }
}
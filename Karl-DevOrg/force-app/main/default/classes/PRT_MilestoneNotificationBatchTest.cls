@IsTest
public class PRT_MilestoneNotificationBatchTest {
    @TestSetup
    public static void makeData() {
        List<User> userList = new List<User>(PRT_TestDataFactory.createUsers(3, 'System Administrator', true));
    }
    
    @isTest
    static void milestoneNotificationTest() {
        List<User> userList = new List<User>([SELECT Id FROM USER WHERE Email Like 'PRTUser%']);
        
        List<Project__c> projectList = new List<Project__c>(PRT_TestDataFactory.createProjects(1, false));
        projectList[0].Status__c =  PRT_Constants.PRT_CR_STATUS_PROJECTKICKOFF;
        projectList[0].Projected_Date_of_Completion__c = date.today()+1;
        projectList[0].Project_Start_Date__c = date.today();
        projectList[0].Project_Manager__c = userList[0].Id;
        projectList[0].Sponsor_Internal__c = userList[1].Id;
        projectList[0].Portfolio_Manager__c = userList[2].Id;
        INSERT projectList;
        
        List<Milestone_PRT__c> milestoneList = new List<Milestone_PRT__c>(PRT_TestDataFactory.createMilestone(3,projectList[0].id,false));
        milestoneList[0].Status__c = 'Not Started';
        milestoneList[0].Start_Date__c = Date.today().addDays(-1);
        
        milestoneList[1].Status__c = 'Not Started';
        milestoneList[1].Start_Date__c = Date.today().addDays(3);
        
        milestoneList[2].Status__c = 'Not Started';
        milestoneList[2].Start_Date__c = null;
        INSERT milestoneList;
        
        Test.startTest();
            Database.executeBatch(new PRT_MilestoneNotificationBatch());
        Test.stopTest();                                                                           
    }
}
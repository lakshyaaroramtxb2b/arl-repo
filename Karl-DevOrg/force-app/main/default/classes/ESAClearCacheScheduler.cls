/**
* Executes every 1 day to clear cache accross ESA related classes
*/
public with sharing class ESAClearCacheScheduler extends ESARecurringSchedulable {
   
   public Integer getRecurringInterval() {
       // Clear chache every 1 day
       return 24*60; // Calculate mins
   }

   public void executeRecurring(SchedulableContext ctx) {
       SForceCaseChatterFollowHelper.clearCache();
       // Add more here...
   }
}
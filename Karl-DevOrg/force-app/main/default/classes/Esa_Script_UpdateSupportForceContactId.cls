/**
 * Class to update all active employees with related supportforce Contact
 * Esa_Script_UpdateSupportForceContactId.updateContactIdinfo();
 */

public class Esa_Script_UpdateSupportForceContactId {
    public static void updateContactIdinfo(){
        List<INTEG_Employee__c> lstEmployees = [SELECT Id,
                                                       Supportforce_Contact__c,
                                                       Federation_Identifier__c,
                                                       INTEG_Active__c,
                                                       INTEG_Email__c 
                                                FROM INTEG_Employee__c 
                                                WHERE INTEG_Active__c = true 
                                                AND LastModifiedById !='0053A00000D0DAEQA3'
                                                AND Supportforce_Contact__c = null ORDER BY CreatedDate ASC Limit 125];


        system.debug('###############'+lstEmployees.size());
        
        Set<String> fedIds = collectFederationIds(lstEmployees);
        Set<String> emails = collectEmails(lstEmployees);

        Map<String,String> mapContactIdsWithEmails = new Map<String,String>();
        Map<String,String> mapContactIdsWithFedIds = new Map<String,String>();
        
        List<Contact__x> lstCoontacts = [SELECT ExternalId,
                                                     Email__c,
                                                     Federation_ID_c__c 
                                              FROM Contact__x 
                                              WHERE RecordTypeId__c IN :(new String[] {
                                                    ContactX_Constants.RT_ID_DEACTIVE,
                                                    ContactX_Constants.RT_ID_INTERNAL,
                                                    ContactX_Constants.RT_ID_INTERNAL_CONTRACTOR })
                                              AND (Federation_ID_c__c IN :fedIds OR Email__c IN :emails)];

        for(Contact__x supportForceContact : lstCoontacts) {

              if(String.isNotBlank(supportForceContact.Email__c)){
                 mapContactIdsWithEmails.put(supportForceContact.Email__c, supportForceContact.ExternalId);
              }
              if(String.isNotBlank(supportForceContact.Federation_ID_c__c)){
                 mapContactIdsWithFedIds.put(supportForceContact.Federation_ID_c__c, supportForceContact.ExternalId);
              }                                  
        }

    
        //update employees with contact Id info
        
        for(INTEG_Employee__c employee : lstEmployees){
            if(mapContactIdsWithEmails.get(employee.INTEG_Email__c) != null){
                employee.Supportforce_Contact__c = mapContactIdsWithEmails.get(employee.INTEG_Email__c);
            }else if(mapContactIdsWithFedIds.get(employee.Federation_Identifier__c) != null) {
                employee.Supportforce_Contact__c = mapContactIdsWithFedIds.get(employee.Federation_Identifier__c);
            }
        }

        Database.update(lstEmployees,false); 
   
    }
    
    
     /**
     * Collect unique emails
     * @param  integEmps Array of INTEG_Employee__c to collect from
     * @return           Set of unique emails
     */
    public static set<String> collectEmails(INTEG_Employee__c[] integEmps) {
        Set<String> emails = new Set<String>();
        for (INTEG_Employee__c integEmp : integEmps) {
            if (String.isNotBlank(integEmp.INTEG_Email__c)) {
                emails.add(integEmp.INTEG_Email__c);
            }
        }
        return emails;
    }
    
    /**
     * Collect unique emails
     * @param  integEmps Array of INTEG_Employee__c to collect from
     * @return           Set of unique Federation Ids
     */
    public static Set<String> collectFederationIds(Integ_Employee__c[] integEmps) {
        Set<String> emails = new Set<String>();
        for (INTEG_Employee__c integEmp : integEmps) {
            if (String.isNotBlank(integEmp.Federation_Identifier__c)) {
                emails.add(integEmp.Federation_Identifier__c);
            }
        }
        return emails;
    }

}
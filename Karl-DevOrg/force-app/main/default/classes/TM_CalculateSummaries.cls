public class TM_CalculateSummaries implements Schedulable { 

    private static final String BU_ACCOUNT_REC_TYPE_ID = TM_Constants.BU_ACCOUNT_REC_TYPE_ID;

    public void execute(SchedulableContext SC) { 
        
        // aggregate query to summarize completed objectives by Buz/Year/Month
        List<AggregateResult> completedObjs = [SELECT Business_Unit_Source__c Business_Unit, 
                                               CALENDAR_YEAR(Last_Completed_Date__c) Completed_Year, 
                                               CALENDAR_MONTH(Last_Completed_Date__c) Completed_Month, 
                                               COUNT(Id) Completed_Objectives
                                               FROM TM_Objective__c 
                                               WHERE TM_Placement__r.Level__c = '1' 
                                               AND Status__c = 'Completed' 
                                               AND TMM_Tracking__c = true
                                               GROUP BY Business_Unit_Source__c, CALENDAR_YEAR(Last_Completed_Date__c), 
                                               CALENDAR_MONTH(Last_Completed_Date__c) 
                                               ORDER BY Business_Unit_Source__c, CALENDAR_YEAR(Last_Completed_Date__c), 
                                               CALENDAR_MONTH(Last_Completed_Date__c)];
        
        // aggregate query to summarize n/a objectives by Buz/Year/Month
        List<AggregateResult> naObjs = [SELECT Business_Unit_Source__c Business_Unit, 
                                               CALENDAR_YEAR(Date_of_NA_Status__c ) NA_Year, 
                                               CALENDAR_MONTH(Date_of_NA_Status__c ) NA_Month, 
                                               COUNT(Id) NA_Objectives
                                               FROM TM_Objective__c 
                                               WHERE TM_Placement__r.Level__c = '1' 
                                               AND Date_of_NA_Status__c !=null
                                               AND TMM_Tracking__c = true
                                               GROUP BY Business_Unit_Source__c, CALENDAR_YEAR(Date_of_NA_Status__c ), 
                                               CALENDAR_MONTH(Date_of_NA_Status__c ) 
                                               ORDER BY Business_Unit_Source__c, CALENDAR_YEAR(Date_of_NA_Status__c ), 
                                               CALENDAR_MONTH(Date_of_NA_Status__c )];
        
        // aggregate query to summarize abstract objectives by Buz/Level
        List<AggregateResult> abstractObjs = [SELECT Business_Unit_Source__c Business_Unit, 
                                               TM_Placement__r.Level__c Level, 
                                               COUNT(Id) Abs_Objectives
                                               FROM TM_Objective__c 
                                               WHERE TM_Placement__r.Level__c = '1' 
                                               AND TMM_Tracking__c = true
                                               GROUP BY Business_Unit_Source__c, TM_Placement__r.Level__c
                                               ORDER BY Business_Unit_Source__c, TM_Placement__r.Level__c];
        
        // create abstract objective map by buz unit
        Map<String, Integer> buzAbsObjectivesMap = new Map<String, Integer>();
        for (AggregateResult ar : abstractObjs) {
            String buz = (String) ar.get('Business_Unit');
            Integer absObjectives = (Integer) ar.get('Abs_Objectives');
            buzAbsObjectivesMap.put(buz, absObjectives);
        }
        
        // create summary records
        List<TM_Objective_Summary__c > objsSummary = new List<TM_Objective_Summary__c>();
        for (AggregateResult ar : completedObjs) {
            String buz = (String) ar.get('Business_Unit');
            TM_Objective_Summary__c newSumm = new TM_Objective_Summary__c(
            Name = buz + ' '
                   + String.valueOf((Integer) ar.get('Completed_Year')) + '/' 
                   + String.valueOf((Integer) ar.get('Completed_Month')),
            Business_Unit_Source__c = (String) ar.get('Business_Unit'),
            Completed_Objectives__c = (Integer) ar.get('Completed_Objectives'),
            Number_of_BU_Level_1_Objectives__c  = buzAbsObjectivesMap.get(buz),
            Beginning_Date__c = Date.newInstance((Integer) ar.get('Completed_Year'), 
                                                 (Integer) ar.get('Completed_Month'), 01)
            );
            newSumm.Date__c = newSumm.Beginning_Date__c.addMonths(1).addDays(-1);
            newSumm.Number_of_Completed_and_NA_Objectives__c = newSumm.Completed_Objectives__c;    
            objsSummary.add(newSumm);
        }
        
        // create summary records for missing months
        Set<String> buzUnits = new Set<String>();
        Map<String, TM_Objective_Summary__c> objsSummaryMap = new Map<String, TM_Objective_Summary__c>();
        for (TM_Objective_Summary__c sum : objsSummary) {
            buzUnits.add(sum.Business_Unit_Source__c);
            objsSummaryMap.put(sum.Business_Unit_Source__c + String.valueOf(sum.Beginning_Date__c), sum);
        }
        for (Account buzAccts : [SELECT Name FROM Account WHERE TMM_Tracking__c = true 
                                 AND RecordTypeId = :BU_ACCOUNT_REC_TYPE_ID]) {
            buzUnits.add(buzAccts.Name);                         
        }
        
        Date begDate = Date.newInstance(2015, 11, 01);  // from TMM birth
        Date endDate = Date.today().toStartOfMonth();   // to this month's first day
    
        for (String buz : buzUnits) {
            for (Date bd = begDate; bd <= endDate; bd = bd.addMonths(1)) {
              String key = buz + String.valueOf(bd);
                if (!objsSummaryMap.containsKey(key)) {
                    TM_Objective_Summary__c missingSumm = new TM_Objective_Summary__c(
                        Name = buz + ' ' 
                               + String.valueOf(bd.Year()) + '/' 
                               + String.valueOf(bd.Month()),
                        Business_Unit_Source__c = buz,
                        Completed_Objectives__c = 0,
                        Number_of_Completed_and_NA_Objectives__c = 0,
                        Number_of_BU_Level_1_Objectives__c  = buzAbsObjectivesMap.get(buz),
                        Beginning_Date__c = bd,
                        Date__c = bd.addMonths(1).addDays(-1));
                    objsSummary.add(missingSumm);
                    objsSummaryMap.put(missingSumm.Business_Unit_Source__c + String.valueOf(missingSumm.Beginning_Date__c), missingSumm);
                }
            }
        }
        
        // add number of na objectives to completed total
        for (AggregateResult ar : naObjs) {
            String buz = (String) ar.get('Business_Unit');
            Date bd = Date.newInstance((Integer) ar.get('NA_Year'), 
                                       (Integer) ar.get('NA_Month'), 01);
            Integer naObjectives = (Integer) ar.get('NA_Objectives');
            String key = buz + String.valueOf(bd);
            if (objsSummaryMap.containsKey(key)) 
                objsSummaryMap.get(key).Number_of_Completed_and_NA_Objectives__c = 
                          objsSummaryMap.get(key).Completed_Objectives__c + naObjectives;
        }
        
        // calculate velocity and generate projected records
        List<String> sortedSummary = new List<String>(); 
        sortedSummary.addAll(objsSummaryMap.keySet());
        sortedSummary.sort();
        
        String lastBU;
        TM_Objective_Summary__c lastSummary;
        Decimal summCompleted;
        Integer months;
        for (String key : sortedSummary) {
            TM_Objective_Summary__c objSumm = objsSummaryMap.get(key);
            if (!objSumm.Business_Unit_Source__c.equals(lastBU)) {
                if (String.isNotEmpty(lastBU)) generateProjections(lastSummary, objsSummary, summCompleted);
                lastBU = objSumm.Business_Unit_Source__c;
                summCompleted = 0;
                months = 0;            
            }
            summCompleted += objSumm.Completed_Objectives__c;
            months++;
            objSumm.Completed_Objectives_Cumulative__c = summCompleted;
            objSumm.Velocity__c = (summCompleted / months).setScale(2);               
            lastSummary = objSumm;
        }
        if (String.isNotEmpty(lastBU)) generateProjections(lastSummary, objsSummary, summCompleted);
        
        // delete current records then add the new ones
        try {
            delete [select Id from TM_Objective_Summary__c];
        } catch (Exception e) {}
        
        insert objsSummary;
    }
    
    private void generateProjections(TM_Objective_Summary__c lastRealSummary, List<TM_Objective_Summary__c> objsSummary, Decimal summCompleted) {
        TM_Objective_Summary__c lastSummary = lastRealSummary.clone(false, true, true);
        // generate 6 additional summary records projecting the completed objects based on the last velocity        
        for (Integer p=1; p <= 6; p++) {
            TM_Objective_Summary__c projSummary = lastSummary.clone(false, true, true); 
            projSummary.Beginning_Date__c = lastSummary.Beginning_Date__c.addMonths(1);
            projSummary.Date__c = projSummary.Beginning_Date__c.addMonths(1).addDays(-1);
            projSummary.Name = projSummary.Business_Unit_Source__c + ' ' 
                               + String.valueOf(projSummary.Beginning_Date__c.Year()) + '/' 
                               + String.valueOf(projSummary.Beginning_Date__c.Month());
            // calculated project for month being process
            Decimal projection;
            if ((summCompleted + lastSummary.Velocity__c) > lastSummary.Number_of_BU_Level_1_Objectives__c) {
                projection = lastSummary.Number_of_BU_Level_1_Objectives__c - summCompleted;
            } else if ((summCompleted + lastSummary.Velocity__c) <= lastSummary.Number_of_BU_Level_1_Objectives__c) {
                projection = lastSummary.Velocity__c;
            } else {
                projection = 0;
            }
            summCompleted += projection;
            projSummary.Completed_Objectives__c = projection;            
            projSummary.Completed_Objectives_Cumulative__c = summCompleted;
            projSummary.Number_of_Completed_and_NA_Objectives__c = projSummary.Completed_Objectives__c; 
            objsSummary.add(projSummary);
            lastSummary = projSummary;
        }               
    }

}
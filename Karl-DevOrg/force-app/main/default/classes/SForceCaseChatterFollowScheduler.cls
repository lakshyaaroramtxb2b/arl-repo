public with sharing class SForceCaseChatterFollowScheduler extends ESARecurringSchedulable {
    
    public Integer getRecurringInterval() {
        // Case trigger will take care of all new and recently updated cases
        // This scheduler runs and re executes any failed cases or unhandled cases
        return 6*60; // Every 6hrs
    }

    public void executeRecurring(SchedulableContext ctx) {
        List<Case> entSecCases = SForceCaseChatterFollowHelper.getEntSecCasesToManageChatterSubscription();
        if (entSecCases != null && entSecCases.size() > 0) {
            System.enqueueJob(new SForceCaseChatterFollowHelper.ManageChatterSubscriptionJob(entSecCases)); 
        }
    }
}
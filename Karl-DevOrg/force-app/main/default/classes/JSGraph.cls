public class JSGraph
{
    //TODO need to convert to dictionary
    public List<JSCaseNode> nodes;
    public Set<JSCaseEdge> edges;
    
    public JSGraph()
    {
    	nodes = new List<JSCaseNode>();
		edges = new Set<JSCaseEdge>();
    }
    
    public Integer getNodeCount()
    {
        return nodes.size();
    }
    
    public Integer getEdgeCount()
    {
        return edges.size();
    }
    
    public void addNode(JSCaseNode node)
    {
        node.id = 'n' + this.getNodeCount();
        nodes.add(node);
    }
    
    //key is same as label. we keep label as its used by graph lib
    public JSCaseNode getNodeByKey(String key)
    {
        for(JSCaseNode n : nodes)
        {
            if(n.key.equalsIgnoreCase(key))
                return n;
        }
        
        return null;
    }
    
    public JSCaseEdge getEdge(JSCaseNode n0, JSCaseNode n1)
    {
        for(JSCaseEdge e : edges)
        {
            String edgeTargetId = e.target.toLowerCase();
            String edgeSourceId= e.source.toLowerCase();

            //not a directed check
            if( (n0.id.equalsIgnoreCase(edgeTargetId) && n1.id.equalsIgnoreCase(edgeSourceId)) ||
                (n1.id.equalsIgnoreCase(edgeTargetId) && n0.id.equalsIgnoreCase(edgeSourceId)))
            {
                return e;
            }
        }
        
        return null;
    }
    
    public JSCaseEdge addEdge(JSCaseNode node0, JSCaseNode node1)
    {
		JSCaseEdge edge = new JSCaseEdge();
        
        edge.id = 'e' + this.getEdgeCount();
    	edge.source = node0.id;
		edge.target = node1.id;
        
        edge.addInstance();
        
        edges.add(edge);
        
        return edge;
    }
}
@isTest
private class GRC_DeploymentUtilityTest {
    @testSetup 
    static void setup() {
        
        List<Case> cases = new List<Case>();
        // insert 10 cases
        for (Integer i=0;i<10;i++) {
            Case newcase = new Case();
            newcase.RecordTypeId = IEM_Constants.IEM_CASERECORDTYPEID;
            newcase.Cloud__c = 'EntSecTools';
            newcase.Description = 'Test';
            newcase.Team_Responsible__c = 'TRUST Enterprise Security Tools Development';
            newcase.Subject = 'Test Subject';
            cases.add(newcase);
        }
        insert cases;
    }
    @isTest
    static void populateExternalFieldsTest() {        
        Test.startTest();
        GRC_DeploymentUtility.populateExternalFields();
        Test.stopTest();
    }
    @isTest
    static void updateEsaHVCPToCCPProfileTest() {  
        User admin = IEM_TestDataFactory.createUser(); 
        User portalUser = new User();        
        System.runAs(admin){
            //Create Account   
            Account a = new Account(Name='salesforce.com - ESA Office Hours');
            insert a; 
            //Create Contact
            Contact c = new Contact(LastName = 'Contact Last Name', AccountId = a.id);
            insert c;
            //Create poratl User
            portalUser = IEM_TestDataFactory.createPortalUser(c);
            insert portalUser;    
            
            //PermissionSet ps = [SELECT Id FROM PermissionSet WHERE Name = 'GRC_IEM_Case_Read'];
            //insert new PermissionSetAssignment(AssigneeId = portalUser.id, PermissionSetId = ps.Id);
        }
        Test.startTest();
        GRC_DeploymentUtility.updateEsaHVCPToCCPProfile();
        Test.stopTest();
    }
    
}
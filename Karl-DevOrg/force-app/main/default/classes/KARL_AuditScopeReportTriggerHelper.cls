/**
* @author Ben Harvie
* @email ben.harvie@salesforce.com
* @description This is trigger handler class for KARL_Audit_Scope_Reports__c.
*/
public with sharing class KARL_AuditScopeReportTriggerHelper {
    public static Set<Id> recordReportIds = new Set<Id>();
    public static Set<Id> recordBuIds = new Set<Id>();
    public static Set<Id> recordCarIds = new Set<Id>();
    
    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description This method gets triggered from Audit Scope Report Trigger
    */
    public static void run(){
        // After Update
        if( Trigger.isAfter && Trigger.isUpdate ){
            updateReportNameIndex();
            updateBuName();
            updateCycleAuditReports();
        }
        // After Insert
        else if( Trigger.isAfter && Trigger.isInsert ){
            updateReportNameIndex();
            updateBuName();
        }
    }
    
    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description This method updates the Scope Name Index as a label instead of API Value
    */
    private static void updateReportNameIndex(){
        //Logic to Stop Recursion
        for(Id recursionCheck : Trigger.newMap.keySet()){
            if(recordReportIds.contains(recursionCheck)){
                System.debug(LoggingLevel.DEBUG, 'Already processed this record >> ' + recursionCheck);
                return; // Kill the run - we've already processed this Id
            }else{
                System.debug(LoggingLevel.DEBUG, 'New record to process >> ' + recursionCheck);
                recordReportIds.add(recursionCheck); // add Id to set so we don't run it again
            }
        }

        List<KARL_Audit_Scope_Reports__c> auditScopeReports = new List<KARL_Audit_Scope_Reports__c>();

        for( KARL_Audit_Scope_Reports__c  asrItems : [SELECT Id,KARL_Audit_Scope__r.Name,KARL_Triggered_Report_Name__c,toLabel(KARL_Report_Type__c) 
                                                        FROM KARL_Audit_Scope_Reports__c 
                                                        WHERE Id in: (List<KARL_Audit_Scope_Reports__c >)Trigger.New ]){
            // Update only if different
            if( asrItems.KARL_Triggered_Report_Name__c != (asrItems.KARL_Audit_Scope__r.Name + ' - ' + asrItems.KARL_Report_Type__c) ){
                // Set the Triggered Report Name in the Audit Scope Report record
                KARL_Audit_Scope_Reports__c asrItem = new KARL_Audit_Scope_Reports__c();
                asrItem.Id = asrItems.Id;
                asrItem.KARL_Triggered_Report_Name__c = asrItems.KARL_Audit_Scope__r.Name + ' - ' + asrItems.KARL_Report_Type__c;
                auditScopeReports.add(asrItem);
            }
        }
        
        // Make the update accounting for sharing and permission sets
		if( !auditScopeReports.isEmpty() && Schema.sObjectType.KARL_Audit_Scope_Reports__c.isUpdateable() 
          	&& KARL_Audit_Scope_Reports__c.KARL_Triggered_Report_Name__c.getDescribe().isUpdateable() ){
            update auditScopeReports;
        }
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description This method updates the Business Unit Name
    */
    private static void updateBuName(){
        //Logic to Stop Recursion
        for(Id recursionCheck : Trigger.newMap.keySet()){
            if(recordBuIds.contains(recursionCheck)){
                System.debug(LoggingLevel.DEBUG, 'Already processed this record >> ' + recursionCheck);
                return; // Kill the run - we've already processed this Id
            }else{
                System.debug(LoggingLevel.DEBUG, 'New record to process >> ' + recursionCheck);
                recordBuIds.add(recursionCheck); // add Id to set so we don't run it again
            }
        }

        List<KARL_Audit_Scope_Reports__c> auditScopeReports = new List<KARL_Audit_Scope_Reports__c>();

        for( KARL_Audit_Scope_Reports__c  asrItems : [SELECT Id,Custom_Report_Name__c,Report_Name__c,KARL_Audit_Scope__r.Name
                                                        FROM KARL_Audit_Scope_Reports__c 
                                                        WHERE Id in: (List<KARL_Audit_Scope_Reports__c >)Trigger.New ]){
            // Update only if different
            if( !asrItems.Custom_Report_Name__c && 
            asrItems.Report_Name__c != asrItems.KARL_Audit_Scope__r.Name ){
                // Set the Audit Scope Name as Report Name
                KARL_Audit_Scope_Reports__c asrItem = new KARL_Audit_Scope_Reports__c();
                asrItem.Id = asrItems.Id;
                asrItem.Report_Name__c = asrItems.KARL_Audit_Scope__r.Name;
                auditScopeReports.add(asrItem);
            }
        }
        
        // Make the update accounting for sharing and permission sets
		if( !auditScopeReports.isEmpty() && Schema.sObjectType.KARL_Audit_Scope_Reports__c.isUpdateable() 
          	&& KARL_Audit_Scope_Reports__c.KARL_Triggered_Report_Name__c.getDescribe().isUpdateable() ){
            update auditScopeReports;
        }
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description This method updates the Cycle Audit Report records with the latest Custom Report Name.
    */
    private static void updateCycleAuditReports(){
        //Logic to Stop Recursion
        for(Id recursionCheck : Trigger.newMap.keySet()){
            if(recordCarIds.contains(recursionCheck)){
                System.debug(LoggingLevel.DEBUG, 'Already processed this record >> ' + recursionCheck);
                return; // Kill the run - we've already processed this Id
            }else{
                System.debug(LoggingLevel.DEBUG, 'New record to process >> ' + recursionCheck);
                recordCarIds.add(recursionCheck); // add Id to set so we don't run it again
            }
        }

        Map<Id, String> asrItems = new Map <Id, String>();
    
        for(KARL_Audit_Scope_Reports__c asrItem : (List<KARL_Audit_Scope_Reports__c >)Trigger.New){
            if(!asrItems.containsKey(asrItem.Id)){
                asrItems.put(asrItem.Id, asrItem.Report_Name__c);
            }
        }

        List<KARL_Cycle_Audit_Report_Items__c> carReports = new List<KARL_Cycle_Audit_Report_Items__c>();

        for( KARL_Cycle_Audit_Report_Items__c carItems : [SELECT Id,Name,Audit_Cycle__r.Name,Area_of_Compliance__c,Audit_Reports_Master_Data__c
                                                            FROM KARL_Cycle_Audit_Report_Items__c 
                                                            WHERE Audit_Reports_Master_Data__c IN: (List<KARL_Audit_Scope_Reports__c >)Trigger.New 
                                                            AND Audit_Cycle__r.Active__c = true]){
            // Update only if different
            String carName = carItems.Audit_Cycle__r.Name + ' - ' + asrItems.get(carItems.Audit_Reports_Master_Data__c) + ' - ' + carItems.Area_of_Compliance__c;
            Integer maxLength = 80;
            if(carName.length() > maxLength){
                carName = carName.substring(0, maxLength);
            }
            System.debug(LoggingLevel.DEBUG, 'updateCycleAuditReports >> Name Comparison: ' + carName + ' vs ' + carItems.Name);
            if( carName != carItems.Name ){
                System.debug(LoggingLevel.DEBUG, 'updateCycleAuditReports >> Names differed, updating!');
                // Update the CAR Name
                KARL_Cycle_Audit_Report_Items__c carReport = new KARL_Cycle_Audit_Report_Items__c();
                carReport.Id = carItems.Id;
                carReport.Name = carName;
                carReports.add(carReport);
            }
        }
        
        // Make the update accounting for sharing and permission sets
		if( !carReports.isEmpty() && Schema.sObjectType.KARL_Cycle_Audit_Report_Items__c.isUpdateable() 
          	&& KARL_Cycle_Audit_Report_Items__c.Name.getDescribe().isUpdateable() ){
            update carReports;
        }
    }

}
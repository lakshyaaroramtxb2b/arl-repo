/**
@description: Class for getting typeahead values for the contact in Quip GRC Charter
@author: Virendra Yadav
*/
@RestResource(urlMapping='/QuipContactRest/*')
global with sharing class PRT_ContacRestResource {
    @HttpGet
    global static List<Contact> doGet() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        String typeAheadValue = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
        typeAheadValue = EncodingUtil.urlDecode(typeAheadValue, 'UTF-8');

        List<Contact> returnDataList = new List<Contact>();
        try {
            returnDataList = new List<Contact>([SELECT Name, ID from Contact WHERE Name LIKE :typeAheadValue WITH SECURITY_ENFORCED]);
        } catch (Exception e) {
            System.debug('Error Occured' + e.getMessage());
        }
        return returnDataList;
    }
}
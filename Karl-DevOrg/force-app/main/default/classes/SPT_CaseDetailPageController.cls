public class SPT_CaseDetailPageController {
    
    // Stores the intake app user id
    public Static String outboundUserId = Label.SPT_OutboundUserId;

    // Below method checks whether case field values are currently synced or not
    @AuraEnabled
    public static boolean validateCaseUpdate(String caseID, Map<string,String> fieldsData){
        Map<Id, String> secOwnerIdToEmpNumMap = new Map<Id, String>();
        Map<String, Id> sfEmpNumToOwnerIdMap = new Map<String, Id>();
        Map<Id, String> contactIdToEmailMap = new Map<Id, String>();
        Map<String, Id> sfEmailToExternalIdMap = new Map<String, Id>();
        Map<String, Id> secQueueIdToNameMap = new Map<String, Id>();
        Map<Id, String> sfQueueNameToIdMap = new Map<Id, String>();
        List<User> secUserList = new List<User>();
        List<SupportForceUser__x> sfUserList = new List<SupportForceUser__x>();
        List<Contact__x> sfContactList = new List<Contact__x>();
        List<String> queueNamesList = new List<String>{'Trust - Application Security Assurance', 'Trust - Enterprise Security Infra', 'Trust - Mergers and Acquisitions'};
        List<Id> sfQueueIdsList = new List<Id>{(Id)SPT_Constants.SF_TRUST_APPLICATION_QUEUE_ID, (Id)SPT_Constants.SF_TRUST_ENTERPRISE_QUEUE_ID, (Id)SPT_Constants.SF_TRUST_MERGERS_QUEUE_ID};
        Case internalCaseRec;
        Id parentCaseId = null;
        Id ownerId;
        Id queueOwnerId;
        Id contactId;
        Id externalId = null;

        // Variable to be used for dynamic query
        String extraFields = 'Id, OwnerId, ContactId, ParentId, Parent.SupportForce_Case_Id__c';
        String whereClause = 'WHERE Id = \''+caseID+'\'';

        // Stores the parentCaseId, ownerId and contactId
        for(Case rec : Database.query(SPT_Utility.getInternalCaseRecords(extraFields, whereClause))) {
            internalCaseRec = rec;
            externalId = rec.SupportForce_Case_Id__c;
            if(rec.ParentId != null)
                parentCaseId = rec.Parent.SupportForce_Case_Id__c;
            if(rec.ContactId != null) 
                contactId = rec.ContactId;
            String ownerIdString = String.valueOf(rec.OwnerId);
            if(ownerIdString.substring(0, 3) != '00G')
                ownerId = rec.OwnerId;
        }

        // // Creating a map of Queue Name To it's Id in security org.
        // for(Group queueRecord : [SELECT Id, Type, Name FROM Group WHERE Type = 'Queue' AND Name IN :queueNamesList ORDER BY Name ASC]) {
        //     secQueueIdToNameMap.put(queueRecord.Id, queueRecord.Name);
        // }

        // // Creating a map of Queue Id To it's Name in SF org.
        // for(Integer i=0; i<queueNamesList.size(); i++) {
        //     sfQueueNameToIdMap.put(sfQueueIdsList.get(0), queueNamesList.get(i));
        // }

        // Below 2 for loops create owner field mapping based on it's employee number
        if(ownerId != null) {
            for(User secUserRecord : [SELECT Id, EmployeeNumber FROM User WHERE Id = :ownerId]) {
                if(secUserRecord.EmployeeNumber != null) {
                    secOwnerIdToEmpNumMap.put(secUserRecord.Id, secUserRecord.EmployeeNumber);
                }            
            }
            System.debug('Owner Map 1: '+secOwnerIdToEmpNumMap);
        }

        if(secOwnerIdToEmpNumMap!=null && secOwnerIdToEmpNumMap.values().size()>0) {
            if(Test.isRunningTest())
                sfUserList = [SELECT Id, ExternalId, EmployeeNumber__c FROM SupportForceUser__x];
            else 
                sfUserList = [SELECT Id, ExternalId, EmployeeNumber__c FROM SupportForceUser__x WHERE EmployeeNumber__c IN :secOwnerIdToEmpNumMap.values()];
            for(SupportForceUser__x sfUserRecord : sfUserList) {
                sfEmpNumToOwnerIdMap.put(sfUserRecord.EmployeeNumber__c, sfUserRecord.ExternalId);
            }
            System.debug('Owner Map 2: '+sfEmpNumToOwnerIdMap);
        }

        // Below 2 for loops create contact field mapping based on it's email
        for(Contact secContact : [SELECT Id, Email FROM Contact WHERE Id = :contactId]) {
            contactIdToEmailMap.put(secContact.Id, secContact.Email);
        }
        System.debug('Customer Map 1: '+contactIdToEmailMap);

        if(Test.isRunningTest()) 
            sfContactList = [SELECT Id, ExternalId, Email__c FROM Contact__x];
        else 
            sfContactList = [SELECT Id, ExternalId, Email__c FROM Contact__x WHERE Email__c IN :contactIdToEmailMap.values() AND RecordTypeId__c IN :SPT_Constants.SF_CONTACTS_RECORD_TYPE_ID];
        for(Contact__x sfContact : sfContactList) {
            sfEmailToExternalIdMap.put(sfContact.Email__c, sfContact.ExternalId);
        }
        System.debug('Customer Map 2: '+sfEmailToExternalIdMap);

        // Variable to be used for dynamic query 
        extraFields = 'Id, LastModifiedDate__c, LastModifiedById__c, OwnerId__c, ContactId__c, ParentId__c';
        if(Test.isRunningTest())
            whereClause = '';
        else
            whereClause = 'WHERE Id = \''+externalId+'\'';
        // Below for loop check the change in field values of case and accordingly return true or false
        for(Case__x externalCaseRec : Database.query(SPT_Utility.getExternalCaseRecords(extraFields, whereClause))) {
            System.debug('External CaseRec' +externalCaseRec);
            System.debug('Internal CaseRec: '+internalCaseRec);
            // [01/15/20 KK] If the fields are different, and the last modified by user (in supportforce) is NOT the intake app user, update the record.
            if(SPT_Utility.isFieldValueChanged(internalCaseRec, externalCaseRec) && externalCaseRec.LastModifiedById__c!=outboundUserId) {
                return false;
            }

            //If owner field is not different, update the record
            // if(secOwnerIdToEmpNumMap!=null && secOwnerIdToEmpNumMap.containsKey(ownerId)
            //    && sfEmpNumToOwnerIdMap!=null && sfEmpNumToOwnerIdMap.containsKey(secOwnerIdToEmpNumMap.get(ownerId))
            //    && externalCaseRec.OwnerId__c != sfEmpNumToOwnerIdMap.get(secOwnerIdToEmpNumMap.get(ownerId))) {
            //     return false;
            // }
            
            // If contact field is not different, update the record
            if(contactIdToEmailMap!=null && contactIdToEmailMap.containsKey(contactId)
               && sfEmailToExternalIdMap!=null && sfEmailToExternalIdMap.containsKey(contactIdToEmailMap.get(contactId))
               && externalCaseRec.ContactId__c != sfEmailToExternalIdMap.get(contactIdToEmailMap.get(contactId))) {
                return false;
            }
            
            // If parent case field is not different, update the record
            if(externalCaseRec.ParentId__c != parentCaseId) {
                return false;
            }
        }
        // [01/15/20 KK] Default value. If true, update the security and supportforce records with the security update.
        return true;
    }

    // Below method sync the security org wrt to the supportforce org.
    @AuraEnabled
    public static boolean refreshRecordCont(String caseID){        
        ID externalId =null;
        List<Case> internalCaseList = new List<Case>();
        // Fields used for dynamic query
        String extraFields = 'Id';
        String whereClause = 'WHERE Id = \''+caseID+'\'';
        // Store the externalId of the current case
        for(Case rec : Database.query(SPT_Utility.getInternalCaseRecords(extraFields, whereClause))) {
            externalID = rec.SupportForce_Case_Id__c;   
        }
        // Fields used for dynamic query
        extraFields = 'Id, ContactId__c, OwnerId__c, RecordTypeId__c, LastModifiedDate__c, ParentId__c';
        if(Test.isRunningTest())
            whereClause = '';
        else 
            whereClause = 'WHERE Id = \''+externalId+'\'';
        List<Case__x> caseRecList = new List<Case__x>();
        caseRecList = Database.query(SPT_Utility.getExternalCaseRecords(extraFields, whereClause)); 
        System.debug('External Case Record: '+caseRecList);
		SPT_CaseAndCaseCommentBatch batch = new SPT_CaseAndCaseCommentBatch();
        // Performs the sync of internal and external case record
        batch.processCaseRecords(caseRecList);
        return true;
    }

    @AuraEnabled
    public static String getEntSecRecordTypeId(){
        return SPT_Constants.SEC_RECORDTYPE_ID;
    }

    @AuraEnabled
    public static List<String> getLayoutFields(){
        List<String> fieldsList = new List<String>();
        List<String> LayoutNameList = new List<String>{'Case-Enterprise Security Case Layout'};
        List<Metadata.Metadata> layouts = Metadata.Operations.retrieve(Metadata.MetadataType.Layout, LayoutNameList);
        Metadata.Layout layout = (Metadata.Layout)layouts.get(0);
        for (Metadata.LayoutSection section : layout.layoutSections) {
            for (Metadata.LayoutColumn column : section.layoutColumns) {
                if (column.layoutItems != null) {
                    for (Metadata.LayoutItem item : column.layoutItems) {
                        System.debug(item.field);
                        if(item.field != 'Description') {
                            fieldsList.add(item.field);
                        }
                    }
                }
            }
        }
        System.debug('Fields List: '+fieldsList.contains('Description'));
        return fieldsList;
    }

    @AuraEnabled
    public static DescriptionWrapper getDescriptionValue(String recordId, String showType){
        System.debug('Record Id: '+recordId+' Show Type: '+showType);
        List<Case> caseList = new List<Case>([SELECT Id, Description FROM Case WHERE Id =: recordId]);
        String descriptionVal = caseList.get(0).Description;
        if(descriptionVal!=null && descriptionVal.length() <= 1000) {
            descriptionVal = descriptionVal.replaceAll('\n', '<br/>');
            System.debug('Description Value: '+descriptionVal);
            DescriptionWrapper wrapper = new DescriptionWrapper(descriptionVal, true);
            return wrapper;
        }
        else if(descriptionVal!=null){
            if(showType == 'Show Less') {
                descriptionVal = descriptionVal.substring(0, 950).replaceAll('\n', '<br/>');
                System.debug('Description Value: '+descriptionVal);
                DescriptionWrapper wrapper = new DescriptionWrapper(descriptionVal + ' ... ', false);
                return wrapper;
            }
            else {
                descriptionVal = descriptionVal.replaceAll('\n', '<br/>');
                System.debug('Description Value: '+descriptionVal);
                DescriptionWrapper wrapper = new DescriptionWrapper(descriptionVal, false);
                return wrapper;
            }
        }
        else {
            descriptionVal = '';
            DescriptionWrapper wrapper = new DescriptionWrapper(descriptionVal, true);
            return wrapper;
        }
    }

    public class DescriptionWrapper {
        @AuraEnabled public String descriptionVal;
        @AuraEnabled public Boolean contentLess;

        public DescriptionWrapper(String descriptionVal, Boolean contentLess) {
            this.descriptionVal = descriptionVal;
            this.contentLess = contentLess;
        }
    }
}
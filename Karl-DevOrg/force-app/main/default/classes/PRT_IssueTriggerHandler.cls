public class PRT_IssueTriggerHandler {
    public static void beforeInsert(List<Issues__c> newList) {
        PRT_IssueTriggerHelper.updateSlippageCommentOfProject(newList, null);
        PRT_IssueTriggerHelper.updateBecameIssueFromRisk(newList, null);
    }

    public static void beforeUpdate(List<Issues__c> newList, Map<Id, Issues__c> oldMap) {
        PRT_IssueTriggerHelper.updateSlippageCommentOfProject(newList, oldMap);
        PRT_IssueTriggerHelper.updateBecameIssueFromRisk(newList, oldMap);
    }
}
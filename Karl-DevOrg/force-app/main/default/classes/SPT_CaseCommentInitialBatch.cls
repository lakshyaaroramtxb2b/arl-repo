global class SPT_CaseCommentInitialBatch implements Database.Batchable<sObject> , Schedulable, Database.Stateful{
    
    global SPT_CaseCommentInitialBatch() {}
    
    // Used for testing
     //String recId = '5007000001jGZbJAAW';
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        String query = 'select id, ExternalID from case__x where recordtypeid__c = \'' + SPT_Constants.SF_RECORDTYPE_ID + '\'';
        //String query = 'select id, ExternalID from case__x where id = \'' + recId + '\'';
        if(Test.isRunningTest()){
            return Database.getQueryLocator([SELECT Id FROM User LIMIT 1]);
        }
        else {
            return Database.getQueryLocator(query);
        }
    }

    // This method process each batch of records
    global void execute(Database.BatchableContext BC, List<sObject> supportforceCaseList) {
        if(Test.isRunningTest()){
            List<sObject> externalObjectList = new List<sObject>();
            Case__x externalCaseRecord = new Case__x();
            externalCaseRecord.RecordTypeId__c = SPT_Constants.SF_RECORDTYPE_ID;
            externalCaseRecord.Status__c = 'New';
            externalCaseRecord.Priority__c = 'Low';
            externalCaseRecord.Description__c = 'Test Description';
            
            CaseComment__x externalCaseCommentRecord = new CaseComment__x();
            externalCaseCommentRecord.CommentBody__c = 'Test Comment Body';
            externalCaseCommentRecord.IsPublished__c = true;
            externalCaseCommentRecord.ParentId__c = externalCaseRecord.Id;
            externalObjectList.add(externalCaseRecord);
            processCaseComments(externalObjectList);
        }
        else {
            processCaseComments(supportforceCaseList);
        }
    }
    
    private void processCaseComments(List<Case__x> supportforceCaseList) {
        
        //System.debug(supportforceCaseList);
        List<CaseComment> commentsToInsert = new List<CaseComment>();
        
        List<Id> sfCaseIdSet = new List<Id>();
        for(Case__x sfCase: supportforceCaseList) {
            sfCaseIdSet.add(sfCase.ExternalID);
        }
        //System.debug(sfCaseIdSet);
        
        List<Case> secCases = [select id, SupportForce_Case_Id__c from Case where SupportForce_Case_Id__c IN :sfCaseIdSet];
        
        //System.debug(secCases);
        Map<Id, Id> sfCaseToSecCaseMap = new Map<Id, Id>();
        
        for (Case secCase: secCases) {
            sfCaseToSecCaseMap.put(secCase.SupportForce_Case_Id__c, secCase.Id);
        }
        //System.debug(sfCaseToSecCaseMap);
        
        List<CaseComment__x> sfComments = new List<CaseComment__x>();
        if(Test.isRunningTest()) {
            Case__x externalCaseRecord = new Case__x();
            externalCaseRecord.RecordTypeId__c = SPT_Constants.SF_RECORDTYPE_ID;
            externalCaseRecord.Status__c = 'New';
            externalCaseRecord.Priority__c = 'Low';
            externalCaseRecord.Description__c = 'Test Description';
            
            CaseComment__x externalCaseCommentRecord = new CaseComment__x();
            externalCaseCommentRecord.CommentBody__c = 'Test Comment Body';
            externalCaseCommentRecord.IsPublished__c = true;
            externalCaseCommentRecord.ParentId__c = externalCaseRecord.Id;
            sfComments.add(externalCaseCommentRecord);
        }
        else {
            sfComments = new List<CaseComment__x>([SELECT Id, ExternalId, ParentId__c, CommentBody__c, CreatedById__c, CreatedDate__c, IsPublished__c, LastModifiedDate__c from CaseComment__x where parentid__c IN :sfCaseIdSet]);
        } 
        
        Set<Id> sfCaseCommentExternalIdSet = new Set<Id>();
        // Creating a map of supportforce case Id to actual case Id
        for (CaseComment comment: [SELECT Id, CommentBody, ParentId, Parent.SupportForce_Case_Id__c FROM CaseComment
                                   WHERE Parent.SupportForce_Case_Id__c IN: sfCaseIdSet]) {
            String caseCommentId = comment.CommentBody.substringBetween('Record_Id: ', '\n');
            if (caseCommentId != null || caseCommentId != '') {
                sfCaseCommentExternalIdSet.add(caseCommentId);
                
            }
                                       System.debug('>>' +comment.id);
                                       System.debug('>>' +comment.parentId);
                                       System.debug('>>' +comment.Parent.SupportForce_Case_Id__c);
        }
        // Map ids to user names
        Set<Id> sfCaseCommentUserIdSet = new Set<Id>();
        Map <Id, String> sfUserIdToNameMap = new Map<Id, String>();
        
        for (CaseComment__x sfComment: sfComments) {
            sfCaseCommentUserIdSet.add(sfComment.CreatedById__c);
        }
        
        for (SupportForceUser__x sfUser: [SELECT Id, Name__c, ExternalId FROM SupportForceUser__x WHERE ExternalId IN: sfCaseCommentUserIdSet]) {
            sfUserIdToNameMap.put(sfUser.ExternalId, sfUser.Name__c);
        }
        
        // Build comments
        for (CaseComment__x sfComment: sfComments) {
            if(!sfCaseCommentExternalIdSet.contains(sfComment.externalID)){
                CaseComment comment = new CaseComment();
                comment.ParentId = sfCaseToSecCaseMap.get(sfComment.ParentId__c);
                String caseCommentUserName = sfUserIdToNameMap.get(sfComment.CreatedById__c);
                String metadata = '\n<<metadata>>\nRecord_Id: ' + sfComment.ExternalId + '\nOriginal_Author: ' + caseCommentUserName + '\nPublished_Date: ' + sfComment.CreatedDate__c;
                Integer metadataLength = metadata.length();
                String text = sfComment.CommentBody__c.replaceAll('\n','--n--');
                Integer newLineCharLength = text.countMatches('--n--')*4; 
                Integer requiredLength = 3700 - metadataLength - newLineCharLength;
                String templateBody;
                if(sfComment.CommentBody__c.length() <= requiredLength) {
                    templateBody = sfComment.CommentBody__c + metadata;
                }
                else {
                    templateBody = sfComment.CommentBody__c.substring(0, requiredLength-5) + ' ...' + metadata;
                }
                comment.CommentBody = templateBody;
                comment.IsPublished = sfComment.IsPublished__c;
                // If commentbody > 4,000 characters, we can't transfer it
                if(comment.CommentBody.length()<=4000 && comment.parentID!=null){
                    commentsToInsert.add(comment);
                }
            }
        }

        if (commentsToInsert.size() > 0 && !commentsToInsert.isEmpty()) {
            //insert commentsToInsert;
            Database.SaveResult[] srList = Database.insert(commentsToInsert, false);
            for(Database.SaveResult result : srList){
                    system.debug('?>>' + result);
                }
            System.debug('SaveResult 1: '+srList);
            String backlash = '\\';
            String backlashReplacement = '---';
            List<CaseComment> reinsertCommentsList = new List<CaseComment>(); 
            for(CaseComment comment : commentsToInsert) {
                if(comment.Id == null) {
                    String templateBody = comment.CommentBody.substringBefore('\n<<metadata>>');
                    String metadataBody = comment.CommentBody.substringAfter('\n<<metadata>>');
                    String newTemplateBody = templateBody.replaceAll('\\<.*?>','');
                    comment.CommentBody = newTemplateBody + '\n<<metadata>>' + metadataBody;
                    System.debug('Updated comment2: '+comment.CommentBody);
                    reinsertCommentsList.add(comment);
                }
            }

            if(reinsertCommentsList != null && !reinsertCommentsList.isEmpty()) {
                Database.SaveResult[] resultList = Database.insert(reinsertCommentsList, true);
                System.debug('SaveResult 2: '+resultList);
                for(Database.SaveResult result : resultList){
                    system.debug('?>>' + result);
                }
            }
        }
        //System.debug(commentsToInsert);
    }
    
    public void finish(Database.BatchableContext BC) {}

    // Required when we implement Database.Stateful
    public void execute(SchedulableContext SC) {}
}
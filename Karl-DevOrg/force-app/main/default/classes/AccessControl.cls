public class AccessControl {
    public class AccessControlDmlException extends Exception {}
    
    // Shortcut function
    private Map<String,Schema.SObjectField> getFieldMap(SObject someObj){
        return someObj.getSObjectType().getDescribe().fields.getMap();
    }
    
    /* This is used because the get*Fields() functions return lists of
     * fields and not just their names as strings.  It returns a set
     * so we can call contains()
     */ 
    private Set<String> fieldsToStringSet(List<Schema.SObjectField> fields) {
        Set<String> fieldNames;
        for (Schema.SObjectField f:fields) {
            fieldNames.add(f.getDescribe().getName());
        }
        return fieldNames;
    }
    
    /* Return a list of sobject fields that are viewable by this user 
     * (i.e. isAccessible() returns true)
     * This is the optimized version when the fieldMap is already availabl
     */
    public List<Schema.SObjectField> getViewableFields(SObject someObj,Map<String,Schema.SObjectField> fieldsMap) {
        List<Schema.SObjectField> fields;
        for(String key:fieldsMap.keySet()) {
            if(fieldsMap.get(key).getDescribe().isAccessible()) {
                fields.add(fieldsMap.get(key));
            }
        }
        return fields;
    }
    /* Return a list of sobject fields that are viewable by this user 
     */ 
    public List<Schema.SObjectField> getViewableFields(SObject someObj) {
        Map<String,Schema.SObjectField> fieldsMap = getFieldMap(someObj);
        return getViewableFields(someObj,fieldsMap);
    }
        
    /* Returns a list of sobject fields that are updateable by this user.
     * This is the optimized version when the fieldMap is already available
     */ 
    public List<Schema.SObjectField> getUpdateableFields(SObject someObj,Map<String,Schema.SObjectField> fieldsMap) {
        List<Schema.SObjectField> fields;
        for(String key:fieldsMap.keySet()) {
            if(fieldsMap.get(key).getDescribe().isUpdateable()) {
                fields.add(fieldsMap.get(key));
            }
        }
        return fields;
    }
    /* Returns a list of sobject fields that are updateable by this user.
     */ 
    public List<Schema.SObjectField> getUpdateableFields(SObject someObj) {
        Map<String,Schema.SObjectField> fieldsMap = getFieldMap(someObj);
        return getUpdateableFields(someObj,fieldsMap);
    }
    
    
    
    /* Returns a list of sobject fields that are createable by this user
     * This is the optimized version when the fieldMap is already available
     */ 
    public List<Schema.SObjectField> getCreatableFields(SObject someObj,Map<String,Schema.SObjectField> fieldsMap) {
        List<Schema.SObjectField> fields;
        for(String key:fieldsMap.keySet()) {
            if(fieldsMap.get(key).getDescribe().isCreateable()) {
                fields.add(fieldsMap.get(key));
            }
        }
        return fields;
    }
    public List<Schema.SObjectField> getCreatableFields(SObject someObj) {
        Map<String,Schema.SObjectField> fieldsMap = getFieldMap(someObj);
        return getCreatableFields(someObj,fieldsMap);
    }
    
    /* Check to see if the user can create this object. Throw exception if not.
     * If they can, null out any fields that they are not allowed to create
     * This will probably throw an exception for non-nullable fields
     */
    public void insertAsUser(SObject someObj) {
        Schema.DescribeSObjectResult d = someObj.getSObjectType().getDescribe();
        if (!d.isCreateable()) throw new AccessControlDmlException('User cannot create SObject Type');
        
        Set<String> cfields = fieldsToStringSet(getCreatableFields(someObj));
        Map<String,Schema.SObjectField> fieldsMap = getFieldMap(someObj);
        for (String f : fieldsMap.keySet()) {
            if (!cfields.contains(f)) {
                someObj.put(f,null);
            }
        }
        insert someObj;     
    }
    
    /* Check to see if the user can update this object. Throw exception if not.
     * If they can, restore any fields that they changed but are not allowed to
     * Note that this causes Obj.getSObjectType().getDescribe().fields.getMap()
     * to be called 
     */
    public void updateAsUser(SObject someObj) {
        Schema.DescribeSObjectResult d = someObj.getSObjectType().getDescribe();
        if (!d.isUpdateable()) throw new AccessControlDmlException('User cannot update SObject Type');
        
        Map<String,Schema.SObjectField> fieldsMap = getFieldMap(someObj);
        SObject existingRecord = getObject(someObj,fieldsMap);
        if (existingRecord == null) {
            throw new AccessControlDmlException('Record does not exist');
        }
        
        Set<String> updateableFields = fieldsToStringSet(getUpdateableFields(someObj,fieldsMap));
        
        for (String f : fieldsMap.keySet()) {
            if (!updateableFields.contains(f)) {
                // Reset the field to its current value
                someObj.put(f,existingRecord.get(f));
            }
        }
        insert someObj;     
    }
    
    /* Check to see if the user can delete this object. Throw exception if not.
     */
    public void deleteAsUser(SObject someObj) {
        Schema.DescribeSObjectResult d = someObj.getSObjectType().getDescribe();
        if (!d.isDeletable()) throw new AccessControlDmlException('User cannot delete SObject Type');
        delete someObj;
    }
    
    /* Gets an object and all of its fields.  Note that this will probably break
     * on objects with a huge number of fields.  It accepts fieldsMap as an 
     * optimization.
     *
     * This is private because it doesn't not respect FLS, CRUD, or sharing
     */ 
    private SObject getObject(sObject obj,Map<String,Schema.SObjectField> fieldsMap) {
        Set<String> allFields = fieldsMap.keySet();
        String Soql = 'SELECT ';
        for (String field : allFields) {
            Soql += field+',';
        }
        Soql = Soql.substring(0,Soql.length()-1); // chop the extra ,
        Soql += ' FROM '+obj.getSObjectType().getDescribe().getName()+' WHERE Id: obj.Id';
        List<SObject> res = Database.query(Soql);
        if (res.isEmpty() == True) {
            return null;
        }
        else { 
            return res.get(0);
        }
    }
}
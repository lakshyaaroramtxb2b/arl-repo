public class NetworkForceUpdateExtension {

    private Networks__c network;
    private ApexPages.StandardController stdController;
    
    public NetworkForceUpdateExtension(ApexPages.StandardController stdController) {
        this.stdController = stdController;
        
        network = [SELECT Id, Name, Network_Description__c, Environment__c, Network_Name__c, Country__c, Start_IP__c, End_IP__c FROM Networks__c WHERE Id =:this.stdController.getId()];
    }
    
    public pagereference forceUpdate()
    {
        Pagereference pg =  new Pagereference('/apex/Find_Networks?ip=' + network.Start_IP__c + '&forceUpdate=true');
        pg.setRedirect(true);
        
        return pg;
    }
}
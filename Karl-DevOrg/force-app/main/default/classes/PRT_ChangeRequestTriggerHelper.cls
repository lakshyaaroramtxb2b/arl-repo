/***********************************************************
* Helper class for PRT_ChangeRequestTrigger
* Created by 	- Prashant Gupta
************************************************************/
public class PRT_ChangeRequestTriggerHelper {
    /***********************************************************
    * This method updates the internal sharing for the Change requests.
    * Created by 	- Prashant Gupta
    ************************************************************/
	public static void createInternalUserSharing(List<Change_Request__c> newList, Map<id,Change_Request__c> oldMap){
        Set<Id> projectIdSet = new Set<Id>();
        Map<id,Set<Change_Request__c>> projectVsChangeRequestOrderSet = new Map<id,Set<Change_Request__c>>(); 
        for(Change_Request__c purchaseO : newList){
            if(purchaseO.Project__c !=null && 
               	(oldMap==null || purchaseO.Project__c!=oldMap.get(purchaseO.id).Project__c)){
                    if(!projectVsChangeRequestOrderSet.containsKey(purchaseO.Project__c)){
                        projectVsChangeRequestOrderSet.put(purchaseO.Project__c, new Set<Change_Request__c>());
                    }
                    projectVsChangeRequestOrderSet.get(purchaseO.Project__c).add(purchaseO);
            }
        }
        //create sharing records based on sponsor and executive sponsor
        List<Change_Request__Share> changeRequestShareList = new List<Change_Request__Share>();
        if(projectVsChangeRequestOrderSet!=null && !projectVsChangeRequestOrderSet.isEmpty()){
            for(Project__c pro : [SELECT Id, Project_Manager__c,Executive_Sponsor_Internal__c,Sponsor_Internal__c
                                    FROM Project__c WHERE id in: projectVsChangeRequestOrderSet.keySet()]){
            	for(Change_Request__c record: projectVsChangeRequestOrderSet.get(pro.id)){
                    if(pro.Executive_Sponsor_Internal__c!=record.OwnerID && pro.Executive_Sponsor_Internal__c!=null){
                    	changeRequestShareList.add(PRT_Utility.prepareChangeRequestShareObject(record.id,pro.Executive_Sponsor_Internal__c,'Read'));
                    }
                    if(pro.Sponsor_Internal__c!=record.OwnerID && pro.Sponsor_Internal__c!=null){
                    	changeRequestShareList.add(PRT_Utility.prepareChangeRequestShareObject(record.id,pro.Sponsor_Internal__c,'Read'));
                    }
                    if(pro.Project_Manager__c!=record.OwnerID && pro.Project_Manager__c!=null){
                        changeRequestShareList.add(PRT_Utility.prepareChangeRequestShareObject(record.id,pro.Project_Manager__c,'Read'));
                    }
                }   
        	}
            //Fetch any existing sharing
            List<Change_Request__Share> changeRequestSharingToDelete = new List<Change_Request__Share>([SELECT Id 
                                                                             FROM Change_Request__Share 
                                                                             WHERE RowCause = 'Manual']);
            //Delete exiting sharing
            if(changeRequestSharingToDelete!=null && !changeRequestSharingToDelete.isEmpty()){
            	delete changeRequestSharingToDelete;                
            }
            //Insert New Sharing
            if(changeRequestShareList!=null && !changeRequestShareList.isEmpty()){
                insert changeRequestShareList;
            }
        }
    }
    
    public static void updateNewBudgetToProjectOnApproval(List<Change_Request__c> newList, Map<id,Change_Request__c> oldMap){
        Map<id,Project__c> projectMap = new Map<id,Project__c>();
        
        for(Change_Request__c purchaseO : newList){
            if(purchaseO.Project__c !=null && purchaseO.Status__c == PRT_Constants.PRT_CR_STATUS_APPROVED
               && purchaseO.Budget__c == true
               && (oldMap==null || purchaseO.Status__c!=oldMap.get(purchaseO.id).Status__c)){                   
                   Project__c pro = new Project__c() ;
                   pro.id = purchaseO.Project__c;
                   pro.Budget_Allocated__c = purchaseO.New_Budget_Amount__c;
                   projectMap.put(pro.id,pro);
            }
        }
        if(projectMap!=null && !projectMap.isEmpty()){
            update projectMap.values();
        }
    }
    public static void createMilestoneToProjectOnApproval(List<Change_Request__c> newList, Map<id,Change_Request__c> oldMap){
        List<Milestone_PRT__c> milestoneList = new List<Milestone_PRT__c>();
        for(Change_Request__c purchaseO : newList){
            if(purchaseO.Project__c !=null && purchaseO.Status__c == PRT_Constants.PRT_CR_STATUS_APPROVED
               && purchaseO.Scope__c == true
               && (oldMap==null || purchaseO.Status__c!=oldMap.get(purchaseO.id).Status__c)){                   
                   Milestone_PRT__c rec = new Milestone_PRT__c() ;
                   rec.Project__c = purchaseO.Project__c;
                   rec.Name = purchaseO.Name__c;
                   rec.Due_Date__c = purchaseO.Due_Date__c;
                   rec.Start_Date__c = purchaseO.Start_Date__c;
                   rec.Description__c = purchaseO.Milestone_Description__c;
                   milestoneList.add(rec);
            }
        }
        if(milestoneList!=null && !milestoneList.isEmpty()){
            PRT_MilestoneTriggerHandler.MILESTONELENGTHvALIDATION = false;
            insert milestoneList;
        }
    }
    public static void checkRequiredFields(List<Change_Request__c> newList, Map<id,Change_Request__c> oldMap){
        Map<String,Integer> statusVsNumber = new Map<String,Integer>();
        statusVsNumber.put(PRT_Constants.PRT_CR_STATUS_DRAFT,1);
        statusVsNumber.put(PRT_Constants.PRT_CR_STATUS_SUBMITTED,2);
        statusVsNumber.put(PRT_Constants.PRT_CR_STATUS_APPROVED,3);
        statusVsNumber.put(PRT_Constants.PRT_CR_STATUS_REJECTED,4);
        System.debug('Map Of ChangeRequest Status' + statusVsNumber);

        String type='Change_Request__c';
        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Schema.SObjectType leadSchema = schemaMap.get(type);
        Map<String, Schema.SObjectField> fieldMap = leadSchema.getDescribe().fields.getMap();
        boolean hasError = False;

        for(Change_Request__c changeReq:newList){
            String error= 'Please populate these fields : ';
            if(oldmap!=null && changeReq.Status__c !=oldMap.get(changeReq.id).Status__c){
                if(statusVsNumber.get(changeReq.status__c) >= 2){
                    if(changeReq.Project__c == null){
                        error += fieldMap.get('Project__c').getDescribe().getLabel() + ', ';
                        hasError = True;
                    }
                    if(changeReq.Subject__c == null){
                        error += fieldMap.get('Subject__c').getDescribe().getLabel() + ', ';
                        hasError = True;
                    }
                }
                if(statusVsNumber.get(changeReq.status__c) >= 3){
                    if(changeReq.Scope__c) {
                        if(changeReq.Proposed_Scope__c == null){
                            error += fieldMap.get('Proposed_Scope__c').getDescribe().getLabel() + ', ';
                            hasError = True;
                        }
                        if(changeReq.Scope_Change_Reason__c == null){
                            error += fieldMap.get('Scope_Change_Reason__c').getDescribe().getLabel() + ', ';
                            hasError = True;
                        }
                        
                    }
                    if(changeReq.Allocation__c) {
                         if(changeReq.Proposed_Allocation__c == null){
                            error += fieldMap.get('Proposed_Allocation__c').getDescribe().getLabel() + ', ';
                            hasError = True;
                        }
                        if(changeReq.Allocation_Change_Reason__c == null){
                            error += fieldMap.get('Allocation_Change_Reason__c').getDescribe().getLabel() + ', ';
                            hasError = True;
                        }
                    }
                    if(changeReq.Budget__c) {
                        if(changeReq.Proposed_Budget__c == null){
                            error += fieldMap.get('Proposed_Budget__c').getDescribe().getLabel() + ', ';
                            hasError = True;
                        }
                        if(changeReq.Budget_Change_Reason__c == null){
                            error += fieldMap.get('Budget_Change_Reason__c').getDescribe().getLabel() + ', ';
                            hasError = True;
                        }    
                    }            
                }
                
                if(statusVsNumber.get(changeReq.status__c) == 4){
                    if(changeReq.Approver_Comments__c == null){
                        error += fieldMap.get('Approver_Comments__c').getDescribe().getLabel() + ', ';
                        hasError = True;
                    }
                }
                if(hasError){
                    if(error.endsWith(', ')){
                        error= error.substringBeforeLast(', ');
                    }
                    changeReq.addError(error);
                }  
            }
        }
    }
}
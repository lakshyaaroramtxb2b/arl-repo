public class ESA_IntakeCancelAppointment {

    
    @future(callout=true)
    public static void webServiceCallout (String reqId){
     
        Esa_Cancel_Appointment.cancel_Appointment(reqId);
   
    }
    
    
    
    public static void cancelAppoOnReq (list <Esa_Security_Request__c> requests){
        
        //get the unique id of cases
        Set <Id> reqIds = new Set <Id>();
   
        for (Esa_Security_Request__c req: requests){
            reqIds.add(req.id);   
        }
        
        
        
        //loop through cases
        for (Esa_Security_Request__c req: requests){
            
            if (reqIds.contains(req.Id)){
                
                if (req.status__c == 'Closed' 
                    && req.Cancel_Office_Hours__c == true
                    && req.GCal_EventId__c != null){
                    
                    if (system.isBatch() == false && system.isFuture() == false){
                            webServiceCallout(req.Id);
                    }
                    //cs.Cancel_Office_Hours__c = false;
                    
            
                        
                    }
                    
                }
            }
        }
        
    }
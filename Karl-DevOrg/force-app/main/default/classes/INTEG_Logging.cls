global class INTEG_Logging {
    global enum Level {DEBUG, INFO, WARN, ERROR, CRITICAL}
    
    private static final Map<String,Integer> LEVEL_MAP = new Map<String,Integer>{'DEBUG'=>0,
                                                                                 'INFO'=>1,
                                                                                 'WARN'=>2,
                                                                                 'ERROR'=>3,
                                                                                 'CRIT'=>4
                                                                                };
    
    @TestVisible private INTEG_LoggingConfig__mdt config;
    @TestVisible private Integer maxCallouts;
    @TestVisible private boolean sendCalloutInTest = false; // will use mock

    global static INTEG_Logging getLogger(Type t) {
        String className = t.getName();
        
        // Check exact match
        INTEG_LoggingConfig__mdt[] configs = [select ClassPrefix__c,SumoLogicURL__c,Minimum_Level__c from INTEG_LoggingConfig__mdt WHERE ClassPrefix__c =:className AND SumoLogicURL__c != null];
        if (configs.size() > 0) {
            return new INTEG_Logging(configs.get(0));
        }
        
        // Check class prefix
        if (className.contains('_')) {
            String prefix = className.split('_').get(0);
            configs = [select ClassPrefix__c,SumoLogicURL__c,Minimum_Level__c from INTEG_LoggingConfig__mdt WHERE ClassPrefix__c =: prefix AND SumoLogicURL__c != null];
            if (configs.size() > 0) {
                return new INTEG_Logging(configs.get(0));
            }
        }
        
        // Check default configuration
        configs = [select ClassPrefix__c,SumoLogicURL__c from INTEG_LoggingConfig__mdt WHERE IsDefault__c = true AND SumoLogicURL__c != null];
        if (configs.size() > 0) {
            return new INTEG_Logging(configs.get(0));
        }
        
        // Return the null logger
        System.debug(System.LoggingLevel.ERROR,'No logger found for '+className);
        return new INTEG_Logging(null);
        
    }
    
    global INTEG_Logging(INTEG_LoggingConfig__mdt logConfig) {
        config = logConfig;
        maxCallouts = Limits.getLimitCallouts();
    }
    
    global void log(Level lvl, List<Map<String,String>> data) {
        if (config == null) {
            System.debug(System.LoggingLevel.ERROR, 'Unable to log message due to lack of configuration');
            return;
        }
        Integer currentCallouts = Limits.getCallouts();
        if (currentCallouts >= maxCallouts) {
            System.debug(System.LoggingLevel.ERROR, 'Unable to log due to callout hard limits');
            return;
        }
        
        // Only use half of the available callout limits
        if (((currentCallouts+1) / (double) maxCallouts) > .5) {
            // Quick check to see if this is the callout that would put us over. If so, 
            // we will do a callout with a different error so there's a record of hitting the limit
            if (( currentCallouts / (double) maxCallouts) <= .5) {
                System.debug(System.LoggingLevel.ERROR, 'Adding soft limit warning for '+String.valueOf(currentCallouts)+' of '+String.valueOf(maxCallouts));
                lvl = Level.ERROR;
                data.add(0, new Map<String,String> {'message'=>'Hit soft callout limit of 50% of '+String.valueOf(maxCallouts)});
            } else {
                System.debug(System.LoggingLevel.ERROR, 'Unable to log due to callout soft limit of 50% of total');
                return;
            }
        }
        
        if (data.size() == 0) {
          return;
        }
        
        if (lvl == null) {
            System.debug(System.LoggingLevel.ERROR, 'Invalid null logging level');
            return;
        }
        
        if (!LEVEL_MAP.containsKey(config.Minimum_Level__c)) {
            System.debug(System.LoggingLevel.ERROR, 'Unknown minimum level '+config.Minimum_Level__c+' in configuration');
            return;
        }
        
        String levelString;
        if (lvl == Level.DEBUG) { levelString = 'DEBUG';}
        else if (lvl == Level.INFO) { levelString = 'INFO';}
        else if (lvl == Level.WARN) { levelString = 'WARN';}
        else if (lvl == Level.ERROR) { levelString = 'ERROR';}
        else if (lvl == Level.CRITICAL) { levelString = 'CRIT';}
        
        if (LEVEL_MAP.get(config.Minimum_Level__c) > LEVEL_MAP.get(levelString)) {
            // This doesn't meet the minimum level
            return;
        }
        
        DateTime now = Datetime.now();
        
        String body = '';
        for (Map<String,String> entry : data) {
            body += now.format('yyyy-MM-dd HH:mm:ss,SSS ZZZZ');
            body += ' '+levelString+' ';
            body += JSON.serialize(entry)+'\n';
        }
        HttpRequest req = new HttpRequest();
        req.setEndpoint(config.SumoLogicURL__c);
        req.setMethod('POST');
        req.setBody(body);
        Http http = new Http();
        HttpResponse res;
        try{
            if (sendCalloutInTest || !Test.isRunningTest()) {
                res = http.send(req);           
            }
        }catch(exception e){
            System.debug(System.LoggingLevel.ERROR, 'Unable to log to SumoLogic'+e.getMessage());
        }            

        if (res != null && res.getStatusCode() != 200){
            System.debug(System.LoggingLevel.ERROR, 'Bad response code logging to SumoLogic: '+String.valueOf(res.getStatusCode()));
        }
    }
    
    global void log(Level lvl, Map<String,String> data) {
        List<Map<String,String>> listVersion = new List<Map<String,String>>();
        listVersion.add(data);
        log(lvl, listVersion);
    }
    
    // Single record versions
    global void debug(Map<String, String> data) {
        System.debug(data);
        log(Level.DEBUG, data);
    }
    global void info(Map<String, String> data) {
        System.debug(System.LoggingLevel.INFO, data);
        log(Level.INFO, data);
    }
    global void warn(Map<String, String> data) {
        System.debug(System.LoggingLevel.WARN, data);
        log(Level.WARN, data);
    }
    global void error(Map<String, String> data) {
        System.debug(System.LoggingLevel.ERROR, data);
        log(Level.ERROR, data);
    }
    global void critical(Map<String, String> data) {
        // no critical in apex logging
        System.debug(System.LoggingLevel.ERROR, data);
        log(Level.CRITICAL, data);
    }
    
    // Single record versions with basic strings
    global void debug(String data) {
        System.debug(data);
        Map<String,String> mapData = new Map<String,String>{'message'=>data};
        log(Level.DEBUG, mapData);
    }
    global void info(String data) {
        System.debug(System.LoggingLevel.INFO, data);
        Map<String,String> mapData = new Map<String,String> {'message'=>data};
        log(Level.INFO, mapData);
    }
    global void warn(String data) {
        System.debug(System.LoggingLevel.WARN, data);
        Map<String,String> mapData = new Map<String,String> {'message'=>data};
        log(Level.WARN, mapData);
    }
    global void error(String data) {
        System.debug(System.LoggingLevel.ERROR, data);
        Map<String,String> mapData = new Map<String,String> {'message'=>data};
        log(Level.ERROR, mapData);
    }
    global void critical(String data) {
        // no critical in apex logging
        System.debug(System.LoggingLevel.ERROR, data);
        Map<String,String> mapData = new Map<String,String> {'message'=>data};
        log(Level.CRITICAL, mapData);
    }
    
    
    // Single record versions with strings to be formatted with given parameters
    global void debug(String message, String[] args, Map<String, String> data) {
        System.debug(data);
        if (data == null) {
          data = new Map<String,String>();
        }
        data.put('message', String.format(message, args));
        log(Level.DEBUG, data);
    }
    global void info(String message, String[] args, Map<String, String> data) {
        System.debug(System.LoggingLevel.INFO, data);
        if (data == null) {
          data = new Map<String,String>();
        }
        data.put('message', String.format(message, args));
        log(Level.INFO, data);
    }
    global void warn(String message, String[] args, Map<String, String> data) {
        System.debug(System.LoggingLevel.WARN, data);
        if (data == null) {
          data = new Map<String,String>();
        }
        data.put('message', String.format(message, args));
        log(Level.WARN, data);
    }
    global void error(String message, String[] args, Map<String, String> data) {
        System.debug(System.LoggingLevel.ERROR, data);
        if (data == null) {
          data = new Map<String,String>();
        }
        data.put('message', String.format(message, args));
        log(Level.ERROR, data);
    }
    global void critical(String message, String[] args, Map<String, String> data) {
        // no critical in apex logging
        System.debug(System.LoggingLevel.ERROR, data);
        if (data == null) {
          data = new Map<String,String>();
        }
        data.put('message', String.format(message, args));
        log(Level.CRITICAL, data);
    }
    
    
    // List versions
    global void debug(List<Map<String, String>> data) {
        System.debug(data);
        log(Level.DEBUG, data);
    }
    global void info(List<Map<String, String>> data) {
        System.debug(System.LoggingLevel.INFO, data);
        log(Level.INFO, data);
    }
    global void warn(List<Map<String, String>> data) {
        System.debug(System.LoggingLevel.WARN, data);
        log(Level.WARN, data);
    }
    global void error(List<Map<String, String>> data) {
        System.debug(System.LoggingLevel.ERROR, data);
        log(Level.ERROR, data);
    }
    global void critical(List<Map<String, String>> data) {
        System.debug(System.LoggingLevel.ERROR, data);
        log(Level.CRITICAL, data);
    }
}
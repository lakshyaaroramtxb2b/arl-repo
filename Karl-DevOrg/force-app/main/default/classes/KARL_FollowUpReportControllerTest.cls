/**
* @author Ben Harvie
* @email ben.harvie@salesforce.com
* @description This is a test class for KARL_FollowUpReportController, used for the Follow-up
* LWC component.
*/
@isTest
public with sharing class KARL_FollowUpReportControllerTest {
    /**
    * @author Ben Harvie
    * @email ben.harvie@salesforce.com
    * @description Test data setup.
    */
	@testSetup
    private static void testSetup(){
        /**
         * User to generate the test records
         */
        UserRole userrole = [SELECT Id, DeveloperName FROM UserRole WHERE DeveloperName = 'CEO' LIMIT 1];

        User adminUser = [SELECT Id, UserRoleId FROM User 
                            WHERE Profile.Name='System Administrator' AND isActive = true LIMIT 1];
        
        adminUser.UserRoleId = userRole.Id;
        update adminUser;

        System.runAs(adminUser){
            /**
             * Audit Firm and Team Setup
             */
            KARL_Audit_Firm__c auditFirm = ARL_TestDataFactory.createAuditFirm('testAuditFirm');
            insert auditFirm;
            
            // Create two distinct audit teams
            KARL_Audit_Team__c auditTeam1 = ARL_TestDataFactory.createAuditTeam('testAuditTeam1');
            auditTeam1.KARL_Audit_Firm__c = auditFirm.Id;
            insert auditTeam1;

            /**
             * Audit Cycle Setup
             */
            // Create shell audit cycle
            Audit_Cycle__c auditCycle = ARL_TestDataFactory.createAuditCycle();
            insert auditCycle;
            
            // Configure audit cycle coverage
            Audit_Cycle_Coverage__c acc1 = ARL_TestDataFactory.createAuditCycleCoverage(auditCycle.Id);
            acc1.Area__c = 'HITRUST';
            acc1.Scope__c  = 'SS';
            acc1.KARL_Audit_Team__c = auditTeam1.id; // Map this to audit team 1
            insert acc1;

            /**
             * Community User Setup
             */
            Account acc = new Account(Name = 'salesforce.com - ESA Office Hours');
            insert acc;
            
            Contact con = new Contact(LastName = 'test', AccountId = acc.Id, Email = 'test@gmail.com');
            insert con;

            ARL_TestDataFactory.createCommunityUserwithSpecificCon(con.Id, 'a12345@bqww.com', true);

            KARL_Audit_Team_Contacts__c auditTeamCon1 = ARL_TestDataFactory.createAuditTeamContact(auditTeam1.Id, con.Id);
            insert auditTeamCon1;

            /**
             * Follow-up Creation
             */
            KARL_Cycle_Follow_up__c followUp = ARL_TestDataFactory.createFollowUp(auditTeam1.Id,auditCycle.Id);
            insert followUp;
        }
    }
    
    /**
    * @author Ben Harvie
    * @email ben.harvie@salesforce.com
    * @description This method tests the generation of the ARL Reports using the ARLReportController
    */
    @isTest
    private static void noFilters(){
        User u = ARL_TestDataFactory.createOpsAdmin();
        KARL_Audit_Team__c auditTeam = [SELECT Id FROM KARL_Audit_Team__c LIMIT 1];

        // Run the test as the SCEA Ops Admin
        System.runAs(u) {
            Test.startTest();
                // Run tests for drop down menus data collection
                List<KARL_Audit_Team__c> auditTeams = KARL_Utility.getAuditTeams();
                System.assertEquals(1, auditTeams.size(), 'Incorrect number of audit teams' ); // Expect 1x

                List<Audit_Cycle__c> auditCycles = KARL_Utility.getAuditCycles();
                System.assertEquals(1, auditCycles.size(), 'Incorrect number of audit cycles' ); // Expect 1x

                Map<String, String> auditScopes = KARL_Utility.getAuditCycleScopes();
                System.assert(auditScopes!=null && auditScopes.size()>0, 'Picklist returned empty');
                
                List<KARL_FollowUpReportController.ReportWrapper> followUpsReport = KARL_FollowUpReportController.getReport(auditCycles[0].Id, 'SS', auditTeam.Id, false, 'all', 'allAssigned', 'currentAll');
                system.assertEquals(1, followUpsReport.size(), 'Incorrect number of items on the followup report [No Filters]'); // Expecting 1x ARI

            Test.stopTest();
        }
    }
    
    /**
    * @author Ben Harvie
    * @email ben.harvie@salesforce.com
    * @description This method tests the generation of the ARL Reports using the ARLReportController
    */
    @isTest
    private static void noFilters_Auditor(){
        // Setup the community user (External Auditor)
        User communityUser = [SELECT id, contactId FROM User WHERE UserName = 'a12345@bqww.com' LIMIT 1];
        ARL_TestDataFactory.applyExternalAuditorPerms(communityUser.Id);

        User u = ARL_TestDataFactory.createOpsAdmin();

        Audit_Cycle__c auditCycle = [SELECT Id FROM Audit_Cycle__c LIMIT 1];
        KARL_Audit_Team__c auditTeam = [SELECT Id FROM KARL_Audit_Team__c LIMIT 1];
       
        // Setup the permissions SCEA Ops Admin
        System.runAs(u) {

            Test.startTest();
                KARL_Cycle_Follow_up__c followUp = ARL_TestDataFactory.createFollowUp(auditTeam.Id,auditCycle.Id);
                insert followUp;
            Test.stopTest();
        }

        // Run the test as the Community User
        System.runAs(communityUser) {
            // Run tests for drop down menus data collection
            List<KARL_Audit_Team__c> auditTeams = KARL_Utility.getAuditTeams();
            System.assertEquals(1, auditTeams.size(), 'Incorrect number of audit teams' ); // Expect 1x

            List<Audit_Cycle__c> auditCycles = KARL_Utility.getAuditCycles();
            System.assertEquals(1, auditCycles.size(), 'Incorrect number of audit cycles' ); // Expect 1x

            Map<String, String> auditScopes = KARL_Utility.getAuditCycleScopes();
            System.assert(auditScopes!=null && auditScopes.size()>0, 'Picklist returned empty');
            
            List<KARL_FollowUpReportController.ReportWrapper> followUpsReport = KARL_FollowUpReportController.getReport(auditCycles[0].Id, 'SS', auditTeam.Id, true, 'all', 'allAssigned', 'currentAll');
            system.assertEquals(2, followUpsReport.size(), 'Incorrect number of items on the followup report [No Filters]'); // Expecting 2x Follow-up (the one created in setup + 1 created in unit test which kicks off sharing rule)
        }
    }
    
    /**
    * @author Ben Harvie
    * @email ben.harvie@salesforce.com
    * @description This method tests the generation of the ARL Reports using the ARLReportController
    */
    @isTest
    private static void filtersApplied(){
        User u = ARL_TestDataFactory.createOpsAdmin();
        KARL_Audit_Team__c auditTeam = [SELECT Id FROM KARL_Audit_Team__c LIMIT 1];

        // Run the test as the SCEA Ops Admin
        System.runAs(u) {
            Test.startTest();
                // Run tests for drop down menus data collection
                List<KARL_Audit_Team__c> auditTeams = KARL_Utility.getAuditTeams();
                System.assertEquals(1, auditTeams.size(), 'Incorrect number of audit teams' ); // Expect 1x

                List<Audit_Cycle__c> auditCycles = KARL_Utility.getAuditCycles();
                System.assertEquals(1, auditCycles.size(), 'Incorrect number of audit cycles' ); // Expect 1x

                Map<String, String> auditScopes = KARL_Utility.getAuditCycleScopes();
                System.assert(auditScopes!=null && auditScopes.size()>0, 'Picklist returned empty');
                
                List<KARL_FollowUpReportController.ReportWrapper> followUpsReport = KARL_FollowUpReportController.getReport(auditCycles[0].Id, 'SS', auditTeam.Id, false, 'new', 'allAssigned', 'currentSalesforce');
                system.assertEquals(1, followUpsReport.size(), 'Incorrect number of items on the followup report [Filters]'); // Expecting 1x ARI

            Test.stopTest();
        }
    }
    
    /**
    * @author Ben Harvie
    * @email ben.harvie@salesforce.com
    * @description This method tests updating a followup
    */
    @isTest
    private static void updateFollowup(){
        User u = ARL_TestDataFactory.createOpsAdmin();
        KARL_Audit_Team__c auditTeam = [SELECT Id FROM KARL_Audit_Team__c LIMIT 1];

        // Run the test as the SCEA Ops Admin
        System.runAs(u) {
            Test.startTest();
                // Run tests for drop down menus data collection
                List<KARL_Audit_Team__c> auditTeams = KARL_Utility.getAuditTeams();
                List<Audit_Cycle__c> auditCycles = KARL_Utility.getAuditCycles();
                Map<String, String> auditScopes = KARL_Utility.getAuditCycleScopes();


                KARL_Cycle_Follow_up__c followupItem_1 = [SELECT Id, KARL_Follow_up_Status__c FROM KARL_Cycle_Follow_up__c LIMIT 1];

                List<Object> jsonList = (List<Object>)JSON.deserializeUntyped('[{"Id": "' + followupItem_1.Id + '", "KARL_Follow_up_Status__c": "Triaged"}]');

                KARL_FollowUpReportController.updateFollowup(jsonList);
                
                KARL_Cycle_Follow_up__c followupItem_2 = [SELECT Id, KARL_Follow_up_Status__c FROM KARL_Cycle_Follow_up__c LIMIT 1];
                system.assertEquals('Triaged', followupItem_2.KARL_Follow_up_Status__c, 'Status not updated'); 

            Test.stopTest();
        }
    }

}
/**
* @author Ben Harvie
* @email ben.harvie@salesforce.com
* @description This is a test class for KARL_AuditIssuesLwcController
*/
@isTest
public with sharing class KARL_AuditIssuesLwcControllerTest {
    /**
* @author Ben Harvie
* @email ben.harvie@salesforce.com
* @description Test data setup.
*/
    @testSetup
    private static void testSetup(){
        User opsAdminUser = ARL_TestDataFactory.createOpsAdmin();
        System.runAs(opsAdminUser){
            /**
* Audit Cycle Setup
*/
            // Create shell audit cycle
            Audit_Cycle__c auditCycle = ARL_TestDataFactory.createAuditCycle();
            insert auditCycle;
            
            /**
* Issue Creation
*/
            KARL_Issue_Tracker__c issue = ARL_TestDataFactory.createAuditIssue(auditCycle.Id);
            insert issue;
            
            KARL_Audit_Scope_Master_Data__c auditScope = ARL_TestDataFactory.createAuditScope();
            insert auditScope;
            KARL_Audit_Firm__c auditFirm = ARL_TestDataFactory.createAuditFirm('Test Audit Firm');
            insert auditFirm;
            KARL_Audit_Scope_Reports__c auditScopeReport = ARL_TestDataFactory.createAuditScopeReport(auditScope.Id,auditFirm.Id);
            insert auditScopeReport;
            KARL_Cycle_Audit_Report_Items__c cycleAuditReporItem = ARL_TestDataFactory.createCycleAuditScopeReport(auditScopeReport.Id,auditCycle.Id);
            cycleAuditReporItem.Scope__c = 'SS';
            insert cycleAuditReporItem;            
        }
    }
    
    /**
* @author Ben Harvie
* @email ben.harvie@salesforce.com
* @description This method tests the generation of the ARL Reports using the ARLReportController
*/
    @isTest
    private static void issueLwc(){
        List<User> opsAdminUserList = ARL_TestDataFactory.fetchUser('abc@b.com.d');
        if(!opsAdminUserList.isEmpty()){
            System.runAs(opsAdminUserList[0]){
                Test.startTest();
                KARL_Cycle_Audit_Report_Items__c reportItem     = [SELECT Id FROM KARL_Cycle_Audit_Report_Items__c LIMIT 1];
                
                List<KARL_Issue_Tracker__c> issueItems = KARL_AuditIssuesLwcController.getScopeAuditIssues(reportItem.id, 'KARL_Cycle_Audit_Report_Items__c');
                system.assertEquals(1, issueItems.size(), 'Incorrect number of versions'); // Expecting 2x version record
                
                Test.stopTest();
            }
        }
        
    }
}
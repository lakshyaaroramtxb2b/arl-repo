/***********************************************************
* Helper for PortfolioTrigger
* Created by 	- Prashant Gupta
* Date - 01-12-2019
************************************************************/
public class PRT_PortfolioTriggerHelper {
    
/***********************************************************
* Handler class for PRT_ProjectTrigger
* Created by 	- Prashant Gupta
************************************************************/
    public static void updateInsertGUSRecords(List<Portfolio__c> newList, Map<id,Portfolio__c> oldMap){
        Set<ID> portfolioToCreateIDSet = new Set<ID>();
        Set<ID> userIDSet = new Set<ID>();
        for(Portfolio__c portfolioRec : newList){
            if(portfolioRec.Portfolio_Active__c){
                portfolioToCreateIDSet.add(portfolioRec.ID);
                if(portfolioRec.Portfolio_Owner__c!=null)
                    userIDSet.add(portfolioRec.Portfolio_Owner__c);
                if(portfolioRec.Portfolio_Manager__c!=null)
                    userIDSet.add(portfolioRec.Portfolio_Manager__c);
            }            
        }        
        if(portfolioToCreateIDSet!=null && !portfolioToCreateIDSet.isEmpty()){
            PRT_GUSSyncHelper.createUpdatePortfolioInGUS(portfolioToCreateIDSet,userIDSet);
        }        
    }   
}
@isTest
public class SPT_CaseDataTableController_Test {
    @TestSetup
    static void makeData(){
        List<Case> caseList = SPT_TestUtility.createCaseRecords('New', 'USD', 'Web', 1, true);
    }

    @isTest
    public static void getAllCasesTest() {
        List<String> statusList = new List<String>();
        statusList.add('New');
        //List<Case> caseList = SPT_CaseDataTableController.getAllCases(statusList, '123', true);
        //System.assertEquals(0, caseList.size());
    }
    @isTest
    public static void getCaseRecords() {
        List<String> statusList = new List<String>();
        statusList.add('New');
        SPT_CaseDataTableController.DataTableResponse caseList = SPT_CaseDataTableController.getCaseRecords(statusList, '123', true,'Case','SPT_CaseListView', UserInfo.getUserId());
        
    }

    @isTest
    public static void updateCasesTest() {
        List<Case> caseList = [SELECT Id, Status FROM Case];
        caseList.get(0).Status = 'In Progress';
        Boolean isUpdated = SPT_CaseDataTableController.updateCases(caseList);
        List<case> updatedCaseList = [SELECT Id, Status FROM Case];
        System.assertEquals('In Progress', updatedCaseList.get(0).Status);
    }

    @isTest
    public static void getAllStatusTest() {
        List<SPT_CaseDataTableController.FilterOptionsWrapper> wrapperList = new List<SPT_CaseDataTableController.FilterOptionsWrapper>();
        wrapperList = SPT_CaseDataTableController.getAllStatus();
        //System.assertEquals(13, wrapperList.size());
        System.assert(wrapperList.size() > 10);
    }
    
    @isTest 
    public static void checkNumberTest() {
        Boolean val = SPT_CaseDataTableController.checkNumber('123456');
        System.assertEquals(true, val);
    }
}
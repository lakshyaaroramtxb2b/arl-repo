/**
 * Trust Maturity Model v2
 * March 2016
 * Written By: Jorge L Caceres (J-team member J3)
 * 
 * Test class for testing conversion routines in TM_BUsharingUtil
 * (needs see all data)
**/

@isTest
private class TEST_TM_BUsharingUtilOneTimeConvertions {

    @isTest(SeeAllData=true)
    static void testConvertOldGroupNames () {
        // identify groups that need to be modified
        List<Group> tmGroups = [select Id, Name, DeveloperName from Group 
                                where (DeveloperName like 'TM_BU%') and (NOT(DeveloperName like 'TM_BU_001%'))];

        system.runAs(new User(Id = UserInfo.getUserId())) { 
            TM_BUsharingUtil.convertOldGroupNames();    
        }  
        
        for (Group g : [select Name, DeveloperName from Group where DeveloperName like 'TM_BU%']) system.debug('>>> new group: ' + g.Name + ' / ' + g.DeveloperName);
        system.assertEquals(0,[select count() from Group where (DeveloperName like 'TM_BU%') and (NOT(DeveloperName like 'TM_BU_001%'))]);
            
    }

    @isTest(SeeAllData=true)
    static void testGenerateObjectiveShares () {
        // verify that there are no manual shares  
        DateTime runTime = system.now().addMinutes(-2);  
        system.assertEquals(0, [select count() from TM_Objective__Share where RowCause = 'Manual' and LastModifiedDate >= :runTime]);
        
        test.startTest();
        
        TM_BUsharingUtil.convertObjectives();

        test.stopTest();
        
        system.assertNotEquals(500, [select count() from TM_Objective__Share where RowCause = 'Manual' and LastModifiedDate >= :runTime]);
            
    }
}
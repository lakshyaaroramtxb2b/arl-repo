public class CDI_SyncGusTeamExternalObjects implements Database.Batchable<sObject>,Database.Stateful{
    public DateTime starttime=DateTime.now();
    public DateTime lastmodifieddate;
    public Integer successRowCount = 0;
    public Integer errorRowCount = 0;
    public String errors='';
     public Database.QueryLocator start(Database.BatchableContext bc) {
         List<ADM_Scrum_Team_c__x> gusteams=[Select LastModifiedDate__c From ADM_Scrum_Team_c__x  ORDER BY LastModifiedDate__c DESC limit 1];
         lastmodifieddate=gusteams[0].LastModifiedDate__c;
           List<CDI_Batch_Import__c>  batchimports=[Select ExternalUserLastModifiedTime__c from CDI_Batch_Import__c Where Object_Type__c='GUS Team' ORDER BY ExternalUserLastModifiedTime__c DESC limit 1];
         String query;
         if(batchimports.size()>0 && batchimports[0].ExternalUserLastModifiedTime__c !=null  && lastmodifieddate>batchimports[0].ExternalUserLastModifiedTime__c)
         {
             DateTime externaldatetime=batchimports[0].ExternalUserLastModifiedTime__c;
             
             query='Select ExternalId,Name__c,Active_c__c,IsDeleted__c,Team_Email_Group_ID_c__c From ADM_Scrum_Team_c__x where ' + 'LastModifiedDate__c > '+externaldatetime.formatGmt('yyyy-MM-dd\'T\'HH:mm:ss.SSSZ')+'';
                 
         }
           else
           {
               query='Select ExternalId,Name__c,Active_c__c,IsDeleted__c,Team_Email_Group_ID_c__c From ADM_Scrum_Team_c__x';
           }
           
        return Database.getQueryLocator(query);
       
    }
    public void execute(Database.BatchableContext bc, List<ADM_Scrum_Team_c__x> externalgusteams){
        System.debug('GUS Team Sync Process Started.');
         List<CDI_GUS_Team__c> Internalgusteams= new List<CDI_GUS_Team__c>();
        for(ADM_Scrum_Team_c__x externalgusteam:externalgusteams)
        {
            CDI_GUS_Team__c internalgusteam= new CDI_GUS_Team__c();
            internalgusteam.Name=externalgusteam.Name__c;
            internalgusteam.External_Id__c=externalgusteam.Team_Email_Group_ID_c__c;
            //internalgusteam.GUS_Team_DL_EMail__c=externalgusteam.Team_Email_Group_ID_c__c;
            If(externalgusteam.Active_c__c == True)
            {
                internalgusteam.Status__c='Active';
            }
            else
            {
                 internalgusteam.Status__c='Rejected';
            }
            internalgusteam.IsDeleted__c=externalgusteam.IsDeleted__c;
            internalgusteam.Source_GUS_Team_Code__c=externalgusteam.ExternalId;
            internalgusteam.Source_System_Name__c='Gus';
            Internalgusteams.add(internalgusteam);
        } 
        Schema.SObjectField externalId=CDI_GUS_Team__c.Fields.External_Id__c;
        Database.UpsertResult[] results=Database.upsert(Internalgusteams,externalId,false); 
         for (Integer j = 0; j < results.size(); j++) {
                    if (!results[j].isSuccess())
                    {
                        errorRowCount=errorRowCount + 1;
                        Database.Error error=results.get(j).getErrors().get(0);
                        errors=error.getMessage();  
                    } 
                    else
                    {
                        successRowCount=successRowCount + 1;    
                    }
                }
                System.debug('Sucess Row Count'+successRowCount);
                System.debug(errors);
        
    }
     public void finish(Database.BatchableContext bc){
            System.debug('Sucess Row Count Inside Finish Method '+successRowCount);
            List<ADM_Scrum_Team_c__x> gusteamdata=[Select LastModifiedDate__c From ADM_Scrum_Team_c__x  ORDER BY LastModifiedDate__c DESC limit 1];
            CDI_Batch_Import__c batchImport = new CDI_Batch_Import__c();
           batchImport.Start_Time__c=starttime;
           batchImport.End_Time__c=DateTime.now();
           batchImport.Object_Type__c='GUS Team';
           batchImport.Rows_Failed__c=errorRowCount;
           batchImport.Rows_Succeeded__c=successRowCount;
           batchImport.Errors__c=errors;
           batchImport.ExternalUserLastModifiedTime__c=gusteamdata[0].LastModifiedDate__c;
           System.debug(gusteamdata[0].LastModifiedDate__c);
           System.debug('Finish '+batchImport.ExternalUserLastModifiedTime__c);
            insert batchImport;
     }
}
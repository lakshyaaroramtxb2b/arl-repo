public with sharing class SPT_CaseCloseComponentController {
    @AuraEnabled
    public static List<String> getCloseStatusValues(Boolean allValues) {
        List<String> closeStatusList = new List<String>();
        List<CaseStatus> statusList = new List<CaseStatus>();
        if(allValues) {
            statusList = [SELECT Id, IsClosed, MasterLabel FROM CaseStatus];
        }
        else {
            statusList = [SELECT Id, IsClosed, MasterLabel FROM CaseStatus WHERE IsClosed = true];   
        }
        for(CaseStatus stat : statusList) {
            closeStatusList.add(stat.MasterLabel);
        }
        System.debug('List: '+closeStatusList);
        List<SPT_Status_Field_Mapping__mdt> statusMetadataList = new List<SPT_Status_Field_Mapping__mdt>([SELECT Id, MasterLabel, Security_Field_Value__c FROM SPT_Status_Field_Mapping__mdt]);
        System.debug('Metadata Val: '+statusMetadataList);
        List<String> entSecStatusList = new List<String>();
        for(SPT_Status_Field_Mapping__mdt statusObj : statusMetadataList) {
            entSecStatusList.add(statusObj.Security_Field_Value__c);
        }
        System.debug('EntSec Val: '+entSecStatusList);
        List<String> entSecClosedStatusList = new List<String>();
        for(String closedVal : closeStatusList) {
            if(entSecStatusList.contains(closedVal)) {
                entSecClosedStatusList.add(closedVal);
            }
        }
        return entSecClosedStatusList;
    }

    @AuraEnabled
    public static String saveCaseStatus(List<String> caseIdList, String statusVal, Boolean emailFlag){
        
        List<Case> caseList = new List<Case>([SELECT Id, Status, Enterprise_Security_Case_Number__c,OwnerId, Don_t_send_notification_email__c FROM Case WHERE Id IN: caseIdList]);
        List<Case> updateCaseList = new List<Case>();
        String notUpdatedCaseIds = '';
        String OwnerId = '';
        for(Case record : caseList) {
            if(record.Status == statusVal) {
                notUpdatedCaseIds += record.Enterprise_Security_Case_Number__c + ', ';
            }
            else {
                record.Status = statusVal;
                record.Don_t_send_notification_email__c = emailFlag;
                OwnerId = record.OwnerId;
                
                if(Userinfo.getUserId() != Label.ESA_Integ_Backend_User && 
                    OwnerId.startsWith('00G')){
                    record.OwnerId = UserInfo.getUserId();
                }

                updateCaseList.add(record);
            }
        }
        notUpdatedCaseIds = notUpdatedCaseIds.removeEnd(', ');
        if(updateCaseList != null && !updateCaseList.isEmpty()) {
            UPDATE updateCaseList;
        }   
        return notUpdatedCaseIds;
    }
}
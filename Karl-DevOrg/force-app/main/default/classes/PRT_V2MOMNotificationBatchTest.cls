/* created By Swarnima Singh Mandhata*/
@isTest
public class PRT_V2MOMNotificationBatchTest {
    /*public static String measureJSON = '{"attributes":{"type":"Measure_c__x","url":"/services/data/v48.0/sobjects/Measure_c__x/0054D000001VnAMQA0"},"MeasureName_c__c":"test measure1","ExternalId":"0054D000001VnAMQA0","V2MOM_c__c":"0054D000001VnAMQA0"}';
  public static String org62JSON = '{"attributes":{"type":"Org62User__x","url":"/services/data/v48.0/sobjects/Org62User__x/0054D000001YKEcQAO"},"EmployeeNumber__c":"77456","ExternalId":"0054D000001YKEcQAO"}';
  public static String v2MomJSON = '{"attributes":{"type":"V2MOM_c__x","url":"/services/data/v48.0/sobjects/V2MOM_c__x/0054D000001WzcyQAC"},"V2MOM_User_c__c":"0054D000001YKEcQAO","ExternalId":"0054D000001WzcyQAC","Name__c":"Test 123","Published_on_c__c":"2020-04-11"}';*/
    @testSetup
    public static void testSetup(){
        List<User> userList = new List<User>();
        
        userList = PRT_TestDataFactory.createUsers(3,'System Administrator',false);
        for(integer i=0; i<2; i++){
            userList[i].EmployeeNumber = '77456';
            userList[i].FirstName = 'Test' + i;
            userList[i].LastName = 'Name' + i;
        }
        insert userList;
        System.runAs(userList[0]){ 
            
            CollaborationGroup ColabGroup = new CollaborationGroup();
            ColabGroup.Name = 'Test Head of GRC';
            ColabGroup.CollaborationType = 'Public';
            insert ColabGroup; 
        } 
    }
    public static testMethod void testExecuteMethod(){
        List<User> userRecord = new List<User>([SELECT Id FROM USER WHERE Email Like 'PRTUser%']);
        List<sObject> org62List = new List<sObject>();
        String userId = userRecord[0].id;
        String userId1 = userInfo.getUserId();
        String publishDate = String.ValueOf(date.today());
        
        String org62JSON = '{"attributes":{"type":"Org62User__x","url":"/services/data/v48.0/sobjects/Org62User__x/0054D000001YKEcQAO"},"EmployeeNumber__c":"77456","ExternalId":"'+ userId+'"}';
        String v2MomJSON = '{"attributes":{"type":"V2MOM_c__x","url":"/services/data/v48.0/sobjects/V2MOM_c__x/0054D000001WzcyQAC"},"V2MOM_User_c__c":"'+ userId+'","ExternalId":"'+userId1+'","Name__c":"Test 123","Published_on_c__c":"'+publishDate+'"}';
        String measureJSON = '{"attributes":{"type":"Measure_c__x","url":"/services/data/v48.0/sobjects/Measure_c__x/0054D000001VnAMQA0"},"MeasureName_c__c":"test measure1","ExternalId":"'+userId1+'","V2MOM_c__c":"'+userId1+'"}';
        String methodJSON = '{"attributes":{"type":"V2MOM_Method_c__x","url":"/services/data/v48.0/sobjects/V2MOM_Method_c__x/0054D000001WzcyQAC"},"Name__c":"test method1","Description_c__c":"test desc","V2MOM_c__c":"'+userId1+'","ExternalId":"'+userId1+'"}';
        
        PRT_V2MOMNotificationBatch.mockAllOrg62List.add((Org62User__x)JSON.deserialize(org62JSON,Org62User__x.class ));
        PRT_V2MOMNotificationBatch.mockallGusV2MomList.add((V2MOM_c__x)JSON.deserialize(v2MomJSON,V2MOM_c__x.class ));
        PRT_V2MOMNotificationBatch.mockAllMeasureList.add((Measure_c__x)JSON.deserialize(measureJSON,Measure_c__x.class ));
        PRT_V2MOMNotificationBatch.mockAllMethodList.add((V2MOM_Method_c__x)JSON.deserialize(methodJSON,V2MOM_Method_c__x.class ));
        
        List<Contact> contactList = PRT_TestDataFactory.createContact(1,false);
        contactList[0].Employee_ID__c = '77456';
        insert contactList;
        
        V2MomMapping__c v2momMappingRec = new V2MomMapping__c();
        v2momMappingRec.Dependent_Method__c = '0054D000001WzcyQAC'; //extracted from jSON.
        insert v2momMappingRec;
     
        Test.startTest();
        PRT_V2MOMNotificationBatch pib = new PRT_V2MOMNotificationBatch();
        Id batchId = Database.executeBatch(pib);
        Test.stopTest();
    }
    public static testMethod void testExecuteMethod1(){
        List<User> userRecord = new List<User>([SELECT Id FROM USER WHERE Email Like 'PRTUser%']);
        List<sObject> org62List = new List<sObject>();
        String userId = userRecord[0].id;
        String userId1 = userInfo.getUserId();
        String publishDate = String.ValueOf(date.today()-4);
        
        String org62JSON = '{"attributes":{"type":"Org62User__x","url":"/services/data/v48.0/sobjects/Org62User__x/0054D000001YKEcQAO"},"EmployeeNumber__c":"77456","ExternalId":"'+ userId+'"}';
        String v2MomJSON = '{"attributes":{"type":"V2MOM_c__x","url":"/services/data/v48.0/sobjects/V2MOM_c__x/0054D000001WzcyQAC"},"V2MOM_User_c__c":"'+ userId+'","ExternalId":"'+userId1+'","Name__c":"Test 123","Published_on_c__c":"'+publishDate+'"}';
        String measureJSON = '{"attributes":{"type":"Measure_c__x","url":"/services/data/v48.0/sobjects/Measure_c__x/0054D000001VnAMQA0"},"MeasureName_c__c":"test measure1","ExternalId":"'+userId1+'","V2MOM_c__c":"'+userId1+'"}';
        String methodJSON = '{"attributes":{"type":"V2MOM_Method_c__x","url":"/services/data/v48.0/sobjects/V2MOM_Method_c__x/0054D000001WzcyQAC"},"Name__c":"test method1","Description_c__c":"test desc","V2MOM_c__c":"'+userId1+'","ExternalId":"'+userId1+'"}';
        
        PRT_V2MOMNotificationBatch.mockAllOrg62List.add((Org62User__x)JSON.deserialize(org62JSON,Org62User__x.class ));
        PRT_V2MOMNotificationBatch.mockallGusV2MomList.add((V2MOM_c__x)JSON.deserialize(v2MomJSON,V2MOM_c__x.class ));
        PRT_V2MOMNotificationBatch.mockAllMeasureList.add((Measure_c__x)JSON.deserialize(measureJSON,Measure_c__x.class ));
        PRT_V2MOMNotificationBatch.mockAllMethodList.add((V2MOM_Method_c__x)JSON.deserialize(methodJSON,V2MOM_Method_c__x.class ));
        
        List<Contact> contactList = PRT_TestDataFactory.createContact(1,false);
        contactList[0].Employee_ID__c = '77456';
        insert contactList;
        
        V2MomMapping__c v2momMappingRec = new V2MomMapping__c();
        v2momMappingRec.Dependent_Method__c = '0054D000001WzcyQAC'; //extracted from jSON.
        insert v2momMappingRec;
     
        Test.startTest();
        PRT_V2MOMNotificationBatch pib = new PRT_V2MOMNotificationBatch();
        Id batchId = Database.executeBatch(pib);
        Test.stopTest();
    }
     public static testMethod void testExecuteMethod2(){
        List<User> userRecord = new List<User>([SELECT Id FROM USER WHERE Email Like 'PRTUser%']);
        List<sObject> org62List = new List<sObject>();
        String userId = userRecord[0].id;
        String userId1 = userInfo.getUserId();
        String publishDate = String.ValueOf(date.today()-5);
        
        String org62JSON = '{"attributes":{"type":"Org62User__x","url":"/services/data/v48.0/sobjects/Org62User__x/0054D000001YKEcQAO"},"EmployeeNumber__c":"77456","ExternalId":"'+ userId+'"}';
        String v2MomJSON = '{"attributes":{"type":"V2MOM_c__x","url":"/services/data/v48.0/sobjects/V2MOM_c__x/0054D000001WzcyQAC"},"V2MOM_User_c__c":"'+ userId+'","ExternalId":"'+userId1+'","Name__c":"Test 123","Published_on_c__c":"'+publishDate+'"}';
        String measureJSON = '{"attributes":{"type":"Measure_c__x","url":"/services/data/v48.0/sobjects/Measure_c__x/0054D000001VnAMQA0"},"MeasureName_c__c":"test measure1","ExternalId":"'+userId1+'","V2MOM_c__c":"'+userId1+'"}';
        String methodJSON = '{"attributes":{"type":"V2MOM_Method_c__x","url":"/services/data/v48.0/sobjects/V2MOM_Method_c__x/0054D000001WzcyQAC"},"Name__c":"test method1","Description_c__c":"test desc","V2MOM_c__c":"'+userId1+'","ExternalId":"'+userId1+'"}';
        
        PRT_V2MOMNotificationBatch.mockAllOrg62List.add((Org62User__x)JSON.deserialize(org62JSON,Org62User__x.class ));
        PRT_V2MOMNotificationBatch.mockallGusV2MomList.add((V2MOM_c__x)JSON.deserialize(v2MomJSON,V2MOM_c__x.class ));
        PRT_V2MOMNotificationBatch.mockAllMeasureList.add((Measure_c__x)JSON.deserialize(measureJSON,Measure_c__x.class ));
        PRT_V2MOMNotificationBatch.mockAllMethodList.add((V2MOM_Method_c__x)JSON.deserialize(methodJSON,V2MOM_Method_c__x.class ));
        
        List<Contact> contactList = PRT_TestDataFactory.createContact(1,false);
        contactList[0].Employee_ID__c = '77456';
        insert contactList;
        
        V2MomMapping__c v2momMappingRec = new V2MomMapping__c();
        v2momMappingRec.Dependent_Method__c = '0054D000001WzcyQAC'; //extracted from jSON.
        insert v2momMappingRec;
     
        Test.startTest();
        PRT_V2MOMNotificationBatch pib = new PRT_V2MOMNotificationBatch();
        Id batchId = Database.executeBatch(pib);
        Test.stopTest();
    }
    
}
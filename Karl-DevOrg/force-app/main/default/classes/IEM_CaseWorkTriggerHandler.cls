/***************IEM_CaseWorkTriggerHandler****************
@Author Banshi
@Date 08/25/2019
@Description Apex handler class to populate Case Work object Subject field value.
**********************************************/

public class IEM_CaseWorkTriggerHandler {
    /*
	* 	Method is used to track all method calls on after Insert
	*  This method takes input of List of Cases
	*/
    public static void afterInsert(List<Case_Work__c> newList){
       IEM_CaseWorkTriggerHelper.populateExternalObjectFields(newList,null);
    }
    /*
	* 	Method is used to track all method calls on after update
	*  This method takes input of List of Case and oldMap
	*/
    public static void afterUpdate(List<Case_Work__c> newList, Map<Id,Case_Work__c> oldMap){
       IEM_CaseWorkTriggerHelper.populateExternalObjectFields(newList,oldMap);
    }
    
}
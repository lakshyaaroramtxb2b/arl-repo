public with sharing class TD_Deliverable_Calendar {

	public list<DeliverableEvent> events {get;set;} 
	public String listViewName {get; private set;}   
	
	private set<Id> setControllerIds; 
	
	//The calendar plugin is expecting dates is a certain format. We can use this string to get it formatted correctly
	private  static final String DATE_FORMAT = 'EEE, d MMM yyyy';
	
	//constructors
    public TD_Deliverable_Calendar(ApexPages.StandardSetController controller) {
    	// if instantiated from a setController create set with ids to retrieve only those deliverables
    	// (there is no way to get field values directly from the setController that I know of that is why it is 
    	//  necessary to query using this set to get the fields from the records   
        
        Boolean firstPage = true;
        controller.setPageSize(50);
        setControllerIds = new set<Id>();    
        listViewName = '';
        
        for (SelectOption so : controller.getListViewOptions()) {
            if (so.getValue() == controller.getFilterId()) {
            	listViewName = so.getLabel();
            	break;
            }
        }
             
        if (controller.getResultSize() > 0) {
        	do {        		
                if (firstPage) controller.first();
                    else controller.next();
                firstPage = false;

				for (sObject sObj : controller.getRecords()) {
					Trust_Deliverable__c td = (Trust_Deliverable__c) sObj;
					setControllerIds.add(td.Id);
				}				
        	} while (!controller.getCompleteResult());
        }
    }
	    
	public PageReference pageLoad() {
	    events = new list<DeliverableEvent>();
	
	    // retrieve events for the calendar
	    Date dateLimit = system.now().addYears(-1).date();
	    String query = 'select Id, Name, Due_Date__c, Description__c, Status__c, ' 
                     + 'CreatedBy.Name, CreatedDate, Accountable_Stakeholder__r.Name ' 
                     + 'from Trust_Deliverable__c where Due_Date__c > :dateLimit ';
        query += (setControllerIds != null)? 'AND Id IN : setControllerIds' : '';
	    system.debug('query : ' + query);
	    for(Trust_Deliverable__c deliverable : database.query(query)) {
	
	        DeliverableEvent de = new DeliverableEvent();                
	
	        String classType = 'Trust Deliverable';
	
	        de.title = deliverable.Name;
	        de.allDay = true;
	        de.startString = prettyDate(deliverable.Due_Date__c);
	        de.endString = prettyDate(deliverable.Due_Date__c);
	        de.url = '/' + deliverable.Id;
	        de.accountable = deliverable.Accountable_Stakeholder__r.Name;
            de.isAccountable = deliverable.Accountable_Stakeholder__c == UserInfo.getUserId();
            de.status = deliverable.Status__c;
	        de.description = deliverable.Description__c;
	        de.createdBy = deliverable.CreatedBy.Name;
	        de.createdOnString = prettyDate(deliverable.CreatedDate);
            if (deliverable.Status__c.toLowerCase() == 'completed' ) {            
                de.className = 'completed';
            } else if (deliverable.Status__c.toLowerCase() == 'cancelled' ) {            
                de.className = 'cancelled';
            } else if (deliverable.Due_Date__c < system.today()) {            
                de.className = 'cancelled';
            } else if (deliverable.Due_Date__c <= system.today().addDays(+4)) {            
                de.className = 'indanger';
            } else if (deliverable.Status__c.toLowerCase() == 'started' ) {
                de.className = 'started';
            } else if (deliverable.Status__c.toLowerCase() == 'not started' ) {
	        	de.className = 'notstarted';
	        } else if (deliverable.Status__c.toLowerCase() == 'on hold' ) {	        
		        de.className = 'onhold';
	        } else {
                de.className = 'allok';
            }
	        events.add(de);
        }
	    return null;
	}    

    private static String prettyDate(Date uglyDate) {
        return prettyDate(DateTime.newInstance(uglyDate, Time.newInstance(0,0,0,0)) );
    }
        
    private static String prettyDate(DateTime uglyDateTime) {
        return uglyDateTime.format(DATE_FORMAT);
    }
    
	//Class to hold calendar event data
	public class DeliverableEvent{
	    public String title {get;set;}
	    public Boolean allDay {get;set;}
	    public String startString {get;private set;}
	    public String endString {get;private set;}
	    public String url {get;set;}
        public String accountable {get;set;}
        public Boolean isAccountable {get;set;}
        public String status {get;set;}
	    public String description {get;set;}
	    public String createdBy {get;set;}
	    public String createdOnString {get;set;}
	    public String modifiedBy {get;set;}
	    public String modifiedOnString {get;set;}
        public String color {get;set;}
        public String textColor {get;set;}	    
        public String className {get;set;}      
	}
	
    
    
}
global class ScheduledTagUpdate {
    
	global void execute(SchedulableContext SC) {
        
        try {
            
            List<cachedTagUpdate__c> records = [SELECT Id, TagName__c, LogTaggingRecordId__c, tagValue__c
                                                FROM cachedTagUpdate__c];
            
            for(cachedTagUpdate__c rec : records) {
                
                Detection_Log_Tagging_Value__c dltv = [SELECT Id, TagName__c, Detection_Log_Tagging__c, tagValue__c
                    FROM Detection_Log_Tagging_Value__c 
                    WHERE TagName__c =: rec.tagName__c AND Detection_Log_Tagging__c =: rec.LogTaggingRecordId__c  LIMIT 1];
                            
            	dltv.tagValue__c = rec.tagValue__c;
            
            	update dltv;
                
            }
        }
        catch(Exception e) {
            
			FeedItem post = new FeedItem();
			post.ParentId = 'a353A000000HDNv';
			post.Body = 'FUTURE ' + e.getMessage();
            insert post;    
       	} 
	}
    
}
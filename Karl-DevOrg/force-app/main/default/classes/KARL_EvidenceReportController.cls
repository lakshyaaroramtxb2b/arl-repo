/** 
 * @author Ben Harvie
 * @email ben.harvie@salesforce.com
 * @description This class provides the methods for generating the Followup Lightning Web Components.
 * Note that this was modelled on KARL_ARLReportController class. 
 */
public with sharing class KARL_EvidenceReportController {
    
    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Build the relevant report items for the reportwrapper, which get displayed via the LWC component 
     * Karl_Followup_Datatable. Note that this is a multi-object reportwrapper.
     * @param auditCycleId - record Id for audit cycle
     * @param auditScopeId - picklist value for audit scope
     * @param auditTeamId - record Id for the audit team (for auditorView = true)
     * @param auditorView - boolean value set by LWC metadata configuration
     * @param filterState - filter values for Type
     * @param filterAssigned - filter values for Assigned
     * @param filterAssignment - filter values for Assignment (Salesforce or Auditor) -- picklist
     * @return Report Wrapper contents for Wire data feed
     */
    @AuraEnabled(cacheable=true)
    public static List<ReportWrapper> getEvidenceReport(String auditCycleId, String auditScopeId,Boolean auditorView, 
                                                String filterState, String filterAssigned, String filterAssignment){

        List<ReportWrapper> items = new List<ReportWrapper>();
        // Step 1: Build Custom Query for State Toggle (Cycle_Request_Status__c)
        Map<String, String> stateMapInternal = new Map<String, String>{
            'all' => '',
            'pendingEngineer' => 'Pending Engineer',
            'readyForReview' => 'Ready for Review',
            'providedToAuditor' => 'Provided to Auditor',
            'returnedToEngineer' => 'Returned to Engineer'
        };

        String queryFilter = '';
        String queryState = stateMapInternal.get(filterState);
        if(filterState != 'all'){
            queryFilter = 'AND Evidence_Status__c =: queryState ';
        }
        String queryAssigned = '';
        if(filterAssigned != 'allAssigned'){
            String currentUserEmail = userInfo.getUserEmail();
            queryAssigned = 'AND (KARL_Cycle_ARL_Item__r.Request_Assignee__r.Email =: currentUserEmail ) ';
        }
        
        
        // Step 3: Determine where we are coming from, to generate the links appropriately for Community vs. Salesforce
        Network karlNetwork;
        String auditorloginurl = '';
        String auditorcommunityHomeUrl = '';
        Integer index;
        if(!Test.isRunningTest()){
            karlNetwork = [SELECT Id FROM Network WHERE Name ='KARL Community' WITH SECURITY_ENFORCED];
            auditorloginurl = Network.getLoginUrl(karlNetwork.Id);
            index = auditorloginurl.lastIndexOf('/login');
            auditorcommunityHomeUrl = auditorloginurl.substring(0, index + 1);
        }

        String baseUrl = '';
        String baseRecordURL = '';
        String baseUrlSuffix = '';
        // Check if we're on the KARL Community or not
        if (Site.getSiteId() != null) {
            baseUrl = auditorcommunityHomeUrl; // we're on the community
            baseRecordURL = baseUrl + 'detail/';
        } else {
            baseUrl = URL.getSalesforceBaseUrl().toExternalForm();
            baseRecordURL = baseUrl + '/one/one.app?#/sObject/'; // force rendering in Lightning instead of Classic
            baseUrlSuffix = '/view'; // Required for Lightning record view
        }

        // List<FileInfoWrapper> fileinfolist = new List<FileInfoWrapper>();
        // fileinfolist.add(new FileInfoWrapper('0690500000043ezAAA',baseRecordURL+'0690500000043ezAAA'+ baseUrlSuffix,'File 1'));
        // fileinfolist.add(new FileInfoWrapper('0690500000043ezAAA',baseRecordURL+'0690500000043ezAAA'+ baseUrlSuffix,'File 2'));
        // fileinfolist.add(new FileInfoWrapper('0690500000043ezAAA',baseRecordURL+'0690500000043ezAAA'+ baseUrlSuffix,'File 3'));
        // fileinfolist.add(new FileInfoWrapper('0690500000043ezAAA',baseRecordURL+'0690500000043ezAAA'+ baseUrlSuffix,'File 4'));
        Set<Id> evidencerequestIdSet = new Set<Id>();
        // Step 1: Query all Follow-ups (filtered for Audit Team) this audit Cycle
        for (KARL_Evidence_Request__c evidenceRequestObj : Database.query('SELECT Id, Name, CreatedDate,Evidence_Upload_Date__c, '+
                                                                'KARL_Request_Name__c,KARL_Cycle_ARL_Item__c,KARL_Cycle_ARL_Item__r.Name, '+
                                                                'Evidence_Status__c, KARL_Cycle_ARL_Item__r.Request_Assignee__c, KARL_Cycle_ARL_Item__r.Request_Assignee__r.Name '+
                                                                'FROM KARL_Evidence_Request__c '+
                                                                'WHERE KARL_Cycle_ARL_Item__r.Audit_Cycle__c =: auditCycleId ' + queryFilter +
                                                                'AND KARL_Primary_Scope__c =: auditScopeId ' + queryAssigned +
                                                                'WITH SECURITY_ENFORCED')){
            evidencerequestIdSet.add(evidenceRequestObj.Id);
            
            ReportWrapper rw    = new ReportWrapper();
            // Primary Information
            rw.Id = evidenceRequestObj.Id;
            rw.requestName = evidenceRequestObj.KARL_Request_Name__c;
            rw.cycleArlItemId = '';
                rw.evidenceRequestItemName = '';
                rw.evidenceRequestItemLink = '';
                rw.requestAssigneeId = '';
                rw.requestAssigneeName = '';
                rw.requestAssigneeLink = '';
            if(evidenceRequestObj.KARL_Cycle_ARL_Item__c != null){
                rw.cycleArlItemId = evidenceRequestObj.KARL_Cycle_ARL_Item__c;
                rw.evidenceRequestItemName = evidenceRequestObj.Name; //used for evidence name
                rw.evidenceRequestItemLink = baseRecordURL + evidenceRequestObj.Id + baseUrlSuffix; //used for evidence request link
                if(evidenceRequestObj.KARL_Cycle_ARL_Item__r.Request_Assignee__c != null){
                    rw.requestAssigneeId = evidenceRequestObj.KARL_Cycle_ARL_Item__r.Request_Assignee__c;
                    rw.requestAssigneeName = evidenceRequestObj.KARL_Cycle_ARL_Item__r.Request_Assignee__r.Name;
                    rw.requestAssigneeLink = baseRecordURL + evidenceRequestObj.KARL_Cycle_ARL_Item__r.Request_Assignee__c + baseUrlSuffix; 
                }
            }
            rw.evidenceStatus = evidenceRequestObj.Evidence_Status__c;
            if(evidenceRequestObj.Evidence_Upload_Date__c != null){
                rw.overdue = String.valueOf(evidenceRequestObj.Evidence_Upload_Date__c);
            }
            else{
                rw.overdue = '';
            }
            items.add(rw);
        }

        if(!evidencerequestIdSet.isEmpty()){
            Map<Id,Id> contentDocumentIdToevidenceRecordIdMap = new Map<Id,Id>();
            Map<Id,List<FileInfoWrapper>> evidenceRecIdToListOfFileInfoWrapperMap = new Map<Id,List<FileInfoWrapper>>();
            for(ContentDocumentLink cdlObj : [SELECT Id,ContentDocumentId,LinkedEntityId 
                                                FROM ContentDocumentLink 
                                                WHERE LinkedEntityId IN: evidencerequestIdSet]){
                contentDocumentIdToevidenceRecordIdMap.put(cdlObj.ContentDocumentId,cdlObj.LinkedEntityId);
            }
            if(!contentDocumentIdToevidenceRecordIdMap.isEmpty()){
                for(ContentVersion cvObj : [SELECT Id, ContentDocumentId, Title, FileExtension 
                                                    FROM ContentVersion
                                                    WHERE ContentDocumentId IN:contentDocumentIdToevidenceRecordIdMap.keySet()]){
                    Id evidenceRecId = contentDocumentIdToevidenceRecordIdMap.get(cvObj.ContentDocumentId);
                    if(!evidenceRecIdToListOfFileInfoWrapperMap.containsKey(evidenceRecId)){
                        evidenceRecIdToListOfFileInfoWrapperMap.put(evidenceRecId,new List<FileInfoWrapper>());
                    }
                    evidenceRecIdToListOfFileInfoWrapperMap.get(evidenceRecId).add(new FileInfoWrapper(cvObj.Id,baseRecordURL+cvObj.Id+ baseUrlSuffix,cvObj.Title+'.'+cvObj.FileExtension));
                }
            }

            if(!evidenceRecIdToListOfFileInfoWrapperMap.isEmpty()){
                for(ReportWrapper rwObj : items){
                    if(evidenceRecIdToListOfFileInfoWrapperMap.containsKey(rwObj.Id)){
                        rwObj.fileinfolist = evidenceRecIdToListOfFileInfoWrapperMap.get(rwObj.Id);
                    }
                }
            }
        }

        return items;
    }

    public class ReportWrapper{
        @AuraEnabled public String Id;
        @AuraEnabled public String requestName;
        @AuraEnabled public String cycleArlItemId;
        @AuraEnabled public String evidenceRequestItemName;
        @AuraEnabled public String evidenceRequestItemLink;
        @AuraEnabled public String evidenceStatus;
        @AuraEnabled public String requestAssigneeId;
        @AuraEnabled public String requestAssigneeName;
        @AuraEnabled public String requestAssigneeLink;
        @AuraEnabled public String overdue;
        @AuraEnabled public List<FileInfoWrapper> fileinfolist;
    }

    public class FileInfoWrapper{
        @AuraEnabled public String fileId;
        @AuraEnabled public String fileLink;
        @AuraEnabled public String fileName;
        @testVisible
        FileInfoWrapper(){

        }
        @testVisible
        FileInfoWrapper(String fileId,String fileLink,String fileName){
            this.fileId = fileId;
            this.fileLink = fileLink;
            this.fileName = fileName;
        }
    }
}
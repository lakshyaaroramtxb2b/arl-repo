/***********************************************************
* Helper Class For handling Sync Functionalities with GUS
* Created by    - Prashant Gupta
* Date - 01-12-2019
************************************************************/
public class PRT_GUSSyncHelper {
    @testVisible
    private static list<User__x> mockallGusUserList = new list<User__x>();
    @testVisible
    private static list<PPM_Portfolio_c__x> mockallGusPortfolioList = new list<PPM_Portfolio_c__x>();
    @testVisible
    private static list<PPM_Program_c__x> mockallGusProgramList = new list<PPM_Program_c__x>();
    @testVisible
    private static list<PPM_Project_c__x> mockallGusProjectList = new list<PPM_Project_c__x>();
    @testVisible
    private static list<ADM_Epic_c__x> mockallGusEpicGusList = new list<ADM_Epic_c__x>();
    
    
    
    @future
    public static void createUpdatePortfolioInGUS(Set<Id> portfolioList, Set<ID> userIDSet ){
        List<PPM_Portfolio_c__x> newPortfolioList = new List<PPM_Portfolio_c__x>();
        List<PPM_Portfolio_c__x> updatedPortfolioList = new List<PPM_Portfolio_c__x>();
        Map<String,ID> employeeIDVsUserIDMap = new Map<String,Id>();
        for(User userRec : [SELECT ID, EmployeeNumber FROM USER WHERE id in: userIDSet]){
            if(userRec.EmployeeNumber!=null || Test.isRunningTest())
                employeeIDVsUserIDMap.put(userRec.EmployeeNumber, null);
        }
        if(Test.isRunningTest()){
            User__x userRec = new User__x();
            if(!mockallGusUserList.isEmpty())    {
               userRec =mockallGusUserList[0];
            }
            if(employeeIDVsUserIDMap!=null && employeeIDVsUserIDMap.containsKey(userRec.EmployeeNumber__c)){
                employeeIDVsUserIDMap.put(userRec.EmployeeNumber__c, userRec.ExternalId);
            }
            
        }else{
            for(User__x userRec : [SELECT ID,ExternalID, EmployeeNumber__c From User__x WHERE EmployeeNumber__c IN :employeeIDVsUserIDMap.keySet()]){
                if(employeeIDVsUserIDMap!=null && employeeIDVsUserIDMap.containsKey(userRec.EmployeeNumber__c)){
                    employeeIDVsUserIDMap.put(userRec.EmployeeNumber__c, userRec.ExternalId);
                }
            }
            
        }
        
        Map<id,String> IdVsPortfolioGoalsMap = new Map<id,String>();
        for(Portfolio__c portfolio : [SELECT ID,Name,Portfolio_Goals__c,GUS_Portfolio__c,Portfolio_Health__c,Portfolio_Manager__r.EmployeeNumber,
                                     Portfolio_Owner__r.EmployeeNumber,Portfolio_Summary__c 
                                     FROM Portfolio__c 
                                     WHERE id IN :portfolioList]){            
                PPM_Portfolio_c__x port = new PPM_Portfolio_c__x();
                port.Portfolio_Health_c__c = portfolio.Portfolio_Health__c;
                if(employeeIDVsUserIDMap.containsKey(portfolio.Portfolio_Manager__r.EmployeeNumber))
                    port.Portfolio_Manager_c__c = employeeIDVsUserIDMap.get(portfolio.Portfolio_Manager__r.EmployeeNumber);
                if(employeeIDVsUserIDMap.containsKey(portfolio.Portfolio_Owner__r.EmployeeNumber))
                    port.Portfolio_Owner_c__c = employeeIDVsUserIDMap.get(portfolio.Portfolio_Owner__r.EmployeeNumber);
                port.Portfolio_Summary_c__c = portfolio.Portfolio_Summary__c;
                port.Name__c = portfolio.Name;
                if(portfolio.GUS_Portfolio__c !=null){
                    port.ExternalId = portfolio.GUS_Portfolio__c;
                    port.Portfolio_Goals_c__c = portfolio.Portfolio_Goals__c;
                    updatedPortfolioList.add(port);
                }else{      
                    port.Portfolio_Goals_c__c = portfolio.ID;              
                    newPortfolioList.add(port);
                }
                IdVsPortfolioGoalsMap.put(portfolio.id,portfolio.Portfolio_Goals__c);
                                         
        }
        Set<Id> result = new Set<Id>();
        if(newPortfolioList!=null && !newPortfolioList.isEmpty()){            
            result = updateExternalRecords(newPortfolioList, 'Insert');
        }
        if(updatedPortfolioList!=null && !updatedPortfolioList.isEmpty()){               
            updateExternalRecords(updatedPortfolioList, 'Update');
        }
        if(result!=null && !result.isEmpty()){
            reupdatePortfolioRecords(result, IdVsPortfolioGoalsMap);
        }
    } 
    
   
    public static void reupdatePortfolioRecords(SET<id> result, Map<id,String> IdVsPortfolioGoalsMap){
        List<Portfolio__c> portfolioRecordList = new List<Portfolio__c>();
        List<PPM_Portfolio_c__x> externalRecordsList = new List<PPM_Portfolio_c__x>();
        if(test.isRunningTest()){
            PPM_Portfolio_c__x rec = mockallGusPortfolioList[0];
            portfolioRecordList.add(new Portfolio__c(id=rec.Portfolio_Goals_c__c, GUS_Portfolio__c = rec.ExternalID));
            if(IdVsPortfolioGoalsMap.containsKey(rec.Portfolio_Goals_c__c)){
                rec.Portfolio_Goals_c__c = IdVsPortfolioGoalsMap.get(rec.Portfolio_Goals_c__c);
                externalRecordsList.add(rec);
            }
        }else{
            for(PPM_Portfolio_c__x rec : [SELECT ID,ExternalID,Portfolio_Goals_c__c From PPM_Portfolio_c__x WHERE ID IN :result]){
                portfolioRecordList.add(new Portfolio__c(id=rec.Portfolio_Goals_c__c, GUS_Portfolio__c = rec.ExternalID));
                if(IdVsPortfolioGoalsMap.containsKey(rec.Portfolio_Goals_c__c)){
                    rec.Portfolio_Goals_c__c = IdVsPortfolioGoalsMap.get(rec.Portfolio_Goals_c__c);
                    externalRecordsList.add(rec);
                }
            }
            
        }
        
        if(portfolioRecordList!=null && !portfolioRecordList.isEmpty()){
            updateExternalRecords(externalRecordsList, 'Update');
            Update portfolioRecordList;
        }
    }
    
     
    @future
    public static void createUpdateProgramInGUS(Set<Id> programList, Set<Id> userIDSet){
        List<PPM_Program_c__x> newProgramList = new List<PPM_Program_c__x>();
        List<PPM_Program_c__x> updateProgramList = new List<PPM_Program_c__x>();
        Map<String,ID> employeeIDVsUserIDMap = new Map<String,Id>();
        for(User userRec : [SELECT ID, EmployeeNumber FROM USER WHERE id in: userIDSet]){
            if(userRec.EmployeeNumber!=null)
                employeeIDVsUserIDMap.put(userRec.EmployeeNumber, null);
        }
        if(Test.isRunningTest()){
            User__x userRec = mockallGusUserList[0];
            if(employeeIDVsUserIDMap!=null && employeeIDVsUserIDMap.containsKey(userRec.EmployeeNumber__c)){
                    employeeIDVsUserIDMap.put(userRec.EmployeeNumber__c, userRec.ExternalId);
                }
        }else{
            for(User__x userRec : [SELECT ID,ExternalID, EmployeeNumber__c From User__x WHERE EmployeeNumber__c IN :employeeIDVsUserIDMap.keySet()]){
                if(employeeIDVsUserIDMap!=null && employeeIDVsUserIDMap.containsKey(userRec.EmployeeNumber__c)){
                    employeeIDVsUserIDMap.put(userRec.EmployeeNumber__c, userRec.ExternalId);
                }
            }   
        }
       
        Map<id,String> IdVsProgramGoalsMap = new Map<id,String>();
        
        for(Program__c programRecord : [SELECT Id, Portfolio__c,Portfolio__r.GUS_Portfolio__c, 
                                        Executive_Sponsor__c,
                                        Name,
                                        Executive_Sponsor__r.EmployeeNumber, 
                                        Program_Goals__c, 
                                        Program_Health__c, 
                                        Program_Health_Color__c, 
                                        Program_Health_Comments__c,
                                        Program_Owner__r.EmployeeNumber,
                                        Program_Manager__c, Parent_Program__r.Name,
                                        Program_Manager__r.EmployeeNumber,
                                        Program_Owner__c, Program_Summary__c,
                                        Parent_Program__r.GUS_Program__c,
                                        GUS_Program__c
                                        FROM Program__c 
                                        WHERE id IN :programList]){            
                PPM_Program_c__x prog = new PPM_Program_c__x();
                prog.Portfolio_c__c = programRecord.Portfolio__r.GUS_Portfolio__c;
                if(employeeIDVsUserIDMap.containsKey(programRecord.Executive_Sponsor__r.EmployeeNumber))
                    prog.Executive_Sponsor_c__c = employeeIDVsUserIDMap.get(programRecord.Executive_Sponsor__r.EmployeeNumber);
                if(employeeIDVsUserIDMap.containsKey(programRecord.Program_Owner__r.EmployeeNumber))
                    prog.Program_Owner_c__c = employeeIDVsUserIDMap.get(programRecord.Program_Owner__r.EmployeeNumber);
                if(employeeIDVsUserIDMap.containsKey(programRecord.Program_Manager__r.EmployeeNumber))
                    prog.Program_Manager_c__c = employeeIDVsUserIDMap.get(programRecord.Program_Manager__r.EmployeeNumber);
                STring name = '';                
                if(programRecord.Parent_Program__c!=null){                  
                    name = programRecord.Parent_Program__r.Name + '.' + programRecord.Name;
                }else{
                    name = programRecord.Name;   
                }
                prog.Name__c = Name;
                prog.Program_Health_c__c = programRecord.Program_Health__c;
                prog.Program_Health_Comments_c__c = programRecord.Program_Health_Comments__c;
                prog.Program_Summary_c__c = programRecord.Program_Summary__c;
                if(programRecord.GUS_Program__c !=null){
                    prog.ExternalId = programRecord.GUS_Program__c;
                    prog.Program_Goals_c__c = programRecord.Program_Goals__c;
                    updateProgramList.add(prog);
                }else{                    
                    prog.Program_Goals_c__c = programRecord.id;
                    newProgramList.add(prog);
                }
                                            
                IdVsProgramGoalsMap.put(programRecord.id,programRecord.Program_Goals__c);
        }
        Set<Id> result = new Set<Id>();
        if(newProgramList!=null && !newProgramList.isEmpty()){            
            result = updateExternalRecords(newProgramList, 'Insert');
        }
        if(updateProgramList!=null && !updateProgramList.isEmpty()){               
            updateExternalRecords(updateProgramList, 'Update');
        }
        system.debug('updateProgramList' + result);
        if(result!=null && !result.isEmpty()){
            reupdateProgramRecords(result, IdVsProgramGoalsMap);
        }
        
    }
    
    public static void reupdateProgramRecords(SET<id> result, Map<id,String> IdVsProgramGoalsMap){
        List<Program__c> programRecordList = new List<Program__c>();
        List<PPM_Program_c__x> externalRecordsList = new List<PPM_Program_c__x>();
        if(Test.isRunningTest()){
            PPM_Program_c__x rec = mockallGusProgramList[0];
            programRecordList.add(new Program__c(id=rec.Program_Goals_c__c, GUS_Program__c = rec.ExternalID));
            if(IdVsProgramGoalsMap.containsKey(rec.Program_Goals_c__c)){
                rec.Program_Goals_c__c = IdVsProgramGoalsMap.get(rec.Program_Goals_c__c);
                externalRecordsList.add(rec);
            }
        }
        for(PPM_Program_c__x rec : [SELECT ID,ExternalID,Program_Goals_c__c From PPM_Program_c__x WHERE ID IN :result]){
            system.debug('>>>' + rec.Program_Goals_c__c);
            programRecordList.add(new Program__c(id=rec.Program_Goals_c__c, GUS_Program__c = rec.ExternalID));
            if(IdVsProgramGoalsMap.containsKey(rec.Program_Goals_c__c)){
                rec.Program_Goals_c__c = IdVsProgramGoalsMap.get(rec.Program_Goals_c__c);
                externalRecordsList.add(rec);
            }
        }
        if(programRecordList!=null && !programRecordList.isEmpty()){
            updateExternalRecords(externalRecordsList, 'Update');
            Update programRecordList;
        }
    }
    
    @future
    public static void createUpdateProjectInGUS(Set<Id> projectList, Set<Id> userId, Set<Id> programId, Set<Id> portfolioId){
        List<PPM_Project_c__x> newProjectList = new List<PPM_Project_c__x>();
        List<PPM_Project_c__x> updatedProjectList = new List<PPM_Project_c__x>();
        Map<String,ID> employeeIDVsUserIDMap = new Map<String,Id>();
        for(User userRec : [SELECT ID, EmployeeNumber FROM USER WHERE id in: userId]){
            if(userRec.EmployeeNumber!=null)
                employeeIDVsUserIDMap.put(userRec.EmployeeNumber, null);
        }
        if(Test.isRunningTest()){
            User__x userRec =mockallGusUserList[0];
            if(employeeIDVsUserIDMap!=null && employeeIDVsUserIDMap.containsKey(userRec.EmployeeNumber__c)){
                employeeIDVsUserIDMap.put(userRec.EmployeeNumber__c, userRec.ExternalId);
            }
            
        }else{
            for(User__x userRec : [SELECT ID,ExternalID, EmployeeNumber__c From User__x WHERE EmployeeNumber__c IN :employeeIDVsUserIDMap.keySet()]){
                if(employeeIDVsUserIDMap!=null && employeeIDVsUserIDMap.containsKey(userRec.EmployeeNumber__c)){
                    employeeIDVsUserIDMap.put(userRec.EmployeeNumber__c, userRec.ExternalId);
                }
            }
            
        }
        // updated by -Swarnima on 21/05/2020 - Line 248 - added Slippage_Comments__c,Slippage_Reason__c to query.
        for(Project__c project : [SELECT Id, Name, Program__r.GUS_Program__c,Project_Start_Date__c,Project_Health_Color__c,
                                  Project_Overview_Purpose__c,Customer_POC__c,Project_Health_Comments__c,
                                  Gus_Project__c,Status__c,Project_Manager__r.EmployeeNumber,Deliverables__c,Delivery_Team_Priority__c,
                                  Executive_Sponsor_Internal__r.EmployeeNumber,Overall_Project_Health__c,Portfolio__r.GUS_Portfolio__c,Delivery_Scrum_Team__c, 
                                  Project_Manager__c, Executive_Sponsor_Internal__c, Slippage_Comments__c, Slippage_Reason__c, Path_to_Green__c
                                  FROM Project__c 
                                  WHERE id IN :projectList AND RecordType.name = 'Project' ]){
            
                PPM_Project_c__x projRecord = new PPM_Project_c__x();
                projRecord.Name__c = project.Name;
                projRecord.Program_c__c = project.Program__r.GUS_Program__c;
                projRecord.Portfolio_c__c = project.Portfolio__r.GUS_Portfolio__c;
                projRecord.Slippage_Comments_c__c = project.Slippage_Comments__c == null ? project.Slippage_Comments__c : project.Slippage_Comments__c.replaceAll('\\<.*?>',' ');
                projRecord.Slippage_Reason_c__c = project.Slippage_Reason__c;
                projRecord.Path_to_Green_c__c = project.Path_to_Green__c;                           
                projRecord.Business_Case_c__c = project.Project_Overview_Purpose__c == null ? project.Project_Overview_Purpose__c : project.Project_Overview_Purpose__c.replaceAll('\\<.*?>',' ') ;   //.replaceAll('\\<.*?>',' ');
                //projRecord.Project_Health_Color_c__c = project.Project_Health_Color__c;
                projRecord.Customer_POC_c__c = project.Customer_POC__c;
                //projrecord.Delivery_Team_Lookup_c__c = project.Delivery_Scrum_Team__c;  
                projRecord.Delivery_Team_Priority_c__c = project.Delivery_Team_Priority__c;
                projRecord.Project_Deliverables_c__c = project.Deliverables__c == null ?project.Deliverables__c: project.Deliverables__c.replaceAll('\\<.*?>',' ') ;  //replaceAll('\\<.*?>',' ');
                projRecord.Project_Health_Comments_c__c = project.Project_Health_Comments__c == null ? project.Project_Health_Comments__c : project.Project_Health_Comments__c.replaceAll('\\<.*?>',' '); //replaceAll('\\<.*?>',' ');
                projRecord.Project_Summary_c__c = project.Project_Overview_Purpose__c == null ? project.Project_Overview_Purpose__c : project.Project_Overview_Purpose__c.replaceAll('\\<.*?>',' '); //replaceAll('\\<.*?>',' ');
                projRecord.Actual_Start_Date_c__c = project.Project_Start_Date__c;
                                      if(project.Overall_Project_Health__c!=null){
                                          if(project.Overall_Project_Health__c == 'On Track'){
                                              projRecord.Project_Health_c__c = 'On Track';
                                          }if(project.Overall_Project_Health__c == 'Watch'){
                                              projRecord.Project_Health_c__c = 'Watch';
                                          }if(project.Overall_Project_Health__c == 'Blocked'){
                                              projRecord.Project_Health_c__c = 'Blocked';
                                          }       
                                      }
                if(employeeIDVsUserIDMap.containsKey(project.Project_Manager__r.EmployeeNumber))
                    projRecord.Project_Manager_c__c = employeeIDVsUserIDMap.get(project.Project_Manager__r.EmployeeNumber);
                if(employeeIDVsUserIDMap.containsKey(project.Executive_Sponsor_Internal__r.EmployeeNumber))
                    projRecord.Executive_Sponsor_c__c = employeeIDVsUserIDMap.get(project.Executive_Sponsor_Internal__r.EmployeeNumber);
                                      
                
                if(project.Gus_Project__c !=null){
                    System.debug('>>project.Gus_Project__c>>' + project.Gus_Project__c );
                    projRecord.ExternalId = project.Gus_Project__c;
                    projRecord.Vendor_Technology_RFC_Standards_c__c = null;
                    updatedProjectList.add(projRecord);
                    System.debug('>>> Update project list >>>' + updatedProjectList);
                }else{      
                    projRecord.Vendor_Technology_RFC_Standards_c__c = project.ID;              
                    newProjectList.add(projRecord);
                }                
        }
        Set<Id> result = new Set<Id>();
        if(newProjectList!=null && !newProjectList.isEmpty()){            
            result = updateExternalRecords(newProjectList, 'Insert');
        }
        if(updatedProjectList!=null && !updatedProjectList.isEmpty()){               
            updateExternalRecords(updatedProjectList, 'Update');
        }
        if(result!=null && !result.isEmpty()){
            reupdateProjectRecords(result);
        }
    } 
    
    public static void reupdateProjectRecords(SET<id> result){
        List<Project__c> projectRecordList = new List<Project__c>();
        List<PPM_Project_c__x> externalRecordList = new List<PPM_Project_c__x>();
        if(test.isRunningTest()){
            PPM_Project_c__x rec = mockallGusProjectList[0];
            projectRecordList.add(new Project__c(id=rec.Vendor_Technology_RFC_Standards_c__c, GUS_Project__c = rec.ExternalID,GUS_Project_Number__c =rec.Project_Number_c__c ));
            rec.Vendor_Technology_RFC_Standards_c__c = null;
            externalRecordList.add(rec);
        }else{
            for(PPM_Project_c__x rec : [SELECT ID,ExternalID,Project_Number_c__c,Vendor_Technology_RFC_Standards_c__c From PPM_Project_c__x WHERE ID IN :result]){
                projectRecordList.add(new Project__c(id=rec.Vendor_Technology_RFC_Standards_c__c, GUS_Project__c = rec.ExternalID,GUS_Project_Number__c =rec.Project_Number_c__c ));
                rec.Vendor_Technology_RFC_Standards_c__c = null;
                externalRecordList.add(rec);
            }
        }
        System.debug('>>>> 1 >>>' + externalRecordList);
        System.debug('>>>>> project List>>??>> ' + projectRecordList);
        if(projectRecordList!=null && !projectRecordList.isEmpty()){
            updateExternalRecords(externalRecordList, 'Update');
            Update projectRecordList;
        }
    }
    
    public static Set<id> updateExternalRecords(List<SObject> records, String operation){
        List<Database.SaveResult> result = new List<Database.SaveResult>();
        if(operation=='Insert'){
            if(!Test.isRunningTest()){
                result = Database.insertImmediate(records);               
            }
        }else{
            if(!Test.isRunningTest()){
                result = Database.updateImmediate(records);
            }            
        }
        System.debug('##result##'+result);
        // Iterate through each returned result
        Set<Id> recordIDSet = new Set<ID>();
        for (Database.SaveResult resultRecord : result) {
            if (resultRecord.isSuccess()) {
                // Operation was successful, so get the ID of the record that was processed
                System.debug('?? '+ resultRecord.getId());
                recordIDSet.add(resultRecord.getId());
            }
        }
        return recordIDSet;
    }

    /*
    * Created by - Virendra Yadav
    * Modified by-Swarnima - added ternary in line 280.
    * Date : 25-12-2019
    * This future functon is used to create GUS work records
    * This fuctionality has been moved to future since external objects cannot be queried in trigger context.
    */
    @future
    public static void createEpicRecordsOnGUS(Set<Id> projectIds){
        List<ADM_Epic_c__x> GUS_list = new List<ADM_Epic_c__x>();   
        Set<Id> setProjectId = projectIds;
        if(setProjectId!=null){
            for(Milestone_PRT__c milestone : [SELECT ID,Completion_Date__c, Name, Start_Date__c, Description__c, Status__c, Due_Date__c, Project__c, Project__r.Gus_Project__c, Milestone_Health__c FROM Milestone_PRT__c WHERE Project__c IN :setProjectId AND GUS_Epic__c = null]){                
                ADM_Epic_c__x epicObject = new ADM_Epic_c__x();
                epicObject.Name__c = milestone.Name;
                epicObject.End_Date_c__c = milestone.Completion_Date__c;
                epicObject.Start_Date_c__c = milestone.Start_Date__c;
                epicObject.Description_c__c = milestone.Description__c==null ? milestone.Description__c : milestone.Description__c.replaceAll('\\<.*?>',' ');
                epicObject.Health_c__c = milestone.Status__c;
                epicObject.Success_Criteria_c__c = milestone.Id;
                epicObject.Project_c__c = milestone.Project__r.Gus_Project__c;
                epicObject.Planned_End_Date_c__c = milestone.Due_Date__c;
                epicObject.Epic_Health_Comments_c__c = milestone.Milestone_Health__c;
                GUS_list.add(epicObject);
            }
        }
        System.debug('>>>>>>>>>GUS LIST'+GUS_list);
        if(GUS_list!=null && !GUS_list.isEmpty()){
            Set<Id> gusEpicRecordIds = new Set<Id>();
            if(!Test.isRunningTest()){
                List<Database.SaveResult> result = Database.insertImmediate(GUS_list);
                System.debug('>>>>> saveresult >>> ' +result );   
                
                // Iterate through each returned result
                for (Database.SaveResult gusEpic : result) {
                    if (gusEpic.isSuccess()) {
                        // Operation was successful, so get the ID of the record that was processed
                        gusEpicRecordIds.add(gusEpic.getId());
                    }
                }
            }else{
                if(!mockallGusEpicGusList.isEmpty())    {
                    gusEpicRecordIds.add(mockallGusEpicGusList[0].id);
                }
            } 
            if(!gusEpicRecordIds.isEmpty()){
                List<Milestone_PRT__c> milestoneList = new List<Milestone_PRT__c>();
                if(Test.isRunningTest()){
                    ADM_Epic_c__x gusEpic = mockallGusEpicGusList[0];
                    Milestone_PRT__c m = new Milestone_PRT__c(Id=gusEpic.Success_Criteria_c__c);
                    m.GUS_Epic__c = gusEpic.ExternalId;
                    milestoneList.add(m);
                }
                for(ADM_Epic_c__x gusEpic : [Select Id,ExternalId,Success_Criteria_c__c From ADM_Epic_c__x Where Id IN :gusEpicRecordIds AND Success_Criteria_c__c!=null]){
                    Milestone_PRT__c m = new Milestone_PRT__c(Id=gusEpic.Success_Criteria_c__c);
                    m.GUS_Epic__c = gusEpic.ExternalId;
                    milestoneList.add(m);
                }
                System.debug('>>>>>>>>>MILESTONE'+milestoneList);   
                update milestoneList;
            }
        }
    }
    
    @future
   public static void syncProgramName(Set<Id> programIDSet){
       
       List<PPM_Program_c__x> GUSProgramList = new List<PPM_Program_c__x>();
       for(Program__c prog: [SELECT ID , Name, Parent_Program__c,GUS_Program__c, Parent_Program__r.Name FROM Program__c WHERE id IN :programIDSet]){
           if(prog.Parent_Program__c !=null){
               String name = prog.Parent_Program__r.Name + '.' + prog.Name;
               System.debug('Name' + name);
               PPM_Program_c__x rec = new PPM_Program_c__x(externalID=prog.GUS_Program__c, name__c = name);
               GUSProgramList.add(rec);
           }
       }
       if(GUSProgramList!=null && !GUSProgramList.isEmpty()){
        PRT_GUSSyncHelper.updateExternalRecords(GUSProgramList, 'Update');
       }
   } 
    
}
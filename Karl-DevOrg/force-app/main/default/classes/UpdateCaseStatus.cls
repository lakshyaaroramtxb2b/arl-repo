/****
*** This is for one time use to update all security request status as per new model
**** Author : Suresh Uppala
***/

public class UpdateCaseStatus {
    
    public static Integer numberOfRecordsForSync = 0,
                   numberOfInsertedEmployees = 0;
    public static Set<Id> caseIds = new Set<Id>();
    public static Set<Id> workIds = new Set<Id>();
    public static Map<Id,Case__x> mapCases = new Map<Id,Case__x>();
    public static Map<Id,ADM_Work_c__x> mapWork = new Map<Id,ADM_Work_c__x>();
    public static List<ESA_Security_Request__c> lstSecuirityRequests;
    public static List<Case__x> lstCases = new List<Case__x>();
    public static List<ADM_Work_c__x> lstWork = new List<ADM_Work_c__x>();
    
    public static void updateCaseStatusValue() {
    lstSecuirityRequests = new List<ESA_Security_Request__c>();

    lstSecuirityRequests = [SELECT Id, 
                                   Status__c,
                                   Supportforce_CaseId__c
                            FROM ESA_Security_Request__c
                            WHERE Supportforce_CaseId__c !=null
                            ORDER BY CreatedDate DESC];

    String caseId;

    Integer repeatCount = (lstSecuirityRequests.size()/300)+1;
    Integer x = 0,y = 0;
        
    caseIds = new Set<Id>();
    workIds = new Set<Id>();
    for(y=0; x<lstSecuirityRequests.size(); x++){
        caseId = lstSecuirityRequests[x].Supportforce_CaseId__c;
        if(caseId != null) {
                if(caseId.startsWith('500')) {
                            caseIds.add(caseId);
                }else {
                            workIds.add(caseId);
                }
        }

        if(y == 299) {
                updateRecords();
                caseIds = new Set<Id>();
                workIds = new Set<Id>();
                y = 0;
        } 
            y++;
    }
        
    updateRecords();
        
        List<Database.SaveResult> saveResults = Database.update(lstSecuirityRequests,false);

       

    }

    private static void updateRecords() {
        if(caseIds.size() > 0) {
            lstCases = [SELECT ExternalId,Status__c,
                               IsClosed__c
                        FROM Case__x
                        WHERE ExternalId IN : caseIds];
            for(Case__x ca : lstCases) {
                    mapCases.put(ca.ExternalId,ca);
            }
        }

        if(workIds.size() > 0) {
            lstWork = [SELECT ExternalId,Status_c__c,
                              Closed_c__c
                       FROM ADM_Work_c__x
                       WHERE ExternalId IN : workIds];
            for(ADM_Work_c__x ca : lstWork) {
                    mapWork.put(ca.ExternalId,ca);
            }
        }

        for(ESA_Security_Request__c eRequest : lstSecuirityRequests) {
            if(mapCases.containsKey(eRequest.Supportforce_CaseId__c)) {

                Case__x ca = mapCases.get(eRequest.Supportforce_CaseId__c);
                if(ca.IsClosed__c == true) {
                    eRequest.Status__c = 'Closed';
                } else if(ca.Status__c != 'New' && ca.Status__c != 'InComplete') {
                    eRequest.Status__c = 'In Progress';
                }

            }else if(mapWork.containsKey(eRequest.Supportforce_CaseId__c)) {

                ADM_Work_c__x work = mapWork.get(eRequest.Supportforce_CaseId__c);
                if(work.Closed_c__c == 1) {
                    eRequest.Status__c = 'Closed';
                }else if(work.Status_c__c != 'New') {
                    eRequest.Status__c = 'In Progress';
                }
            }
        }
        
    }    

}
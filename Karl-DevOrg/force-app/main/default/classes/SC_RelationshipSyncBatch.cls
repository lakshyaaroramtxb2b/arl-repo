/*
 * Apex Batch class used to sync Modules and Trails
 */

global class SC_RelationshipSyncBatch implements
    Database.Batchable<SC_TrailResponse.Data>,
    Database.AllowsCallouts,
    Database.Stateful {

    Integer offSet;
    Integer totalCount;

    global static final String MODULE = 'module_';
    global static final Integer OFF_SET_SIZE = 50;
    global static final String TRAIL = 'trail_';

    public SC_RelationshipSyncBatch(Integer pOffset) {
        this.offSet = pOffset;
    }

    global Iterable<SC_TrailResponse.Data> start(Database.BatchableContext info) {

        SC_TrailheadApi api = new SC_TrailheadApi();
        SC_TrailResponse trailResponse = api.getTrails(this.offSet);
        this.totalCount = trailResponse.total_count;

        return (Iterable<SC_TrailResponse.Data>)trailResponse.data;
    }

    global void execute(Database.BatchableContext info, SC_TrailResponse.Data[] pScope) {

        Map<String, Training_Course__c> courseMap = new Map<String, Training_Course__c>();
        List<Training_Course_Relationship__c> upsertRelationshipList =
            new List<Training_Course_Relationship__c>();

        for (Training_Course__c course :[
            select Id, Sync_Id__c, Org_Wide_Email_Id__c
            from Training_Course__c
            where Sync_Id__c != null
        ]) {
            courseMap.put(course.Sync_Id__c, course);
        }

        for (SC_TrailResponse.Data trail :pScope) {
            for (SC_TrailResponse.Modules module :trail.modules) {
                if (courseMap.containsKey(SC_RelationshipSyncBatch.MODULE + module.api_name) &&
                    courseMap.containsKey(SC_RelationshipSyncBatch.TRAIL + trail.api_name)
                ) {
                    upsertRelationshipList.add(new Training_Course_Relationship__c(
                        Related_Module__c = courseMap.get(SC_RelationshipSyncBatch.MODULE + module.api_name).Id,
                        Related_Path__c = courseMap.get(SC_RelationshipSyncBatch.TRAIL + trail.api_name).Id,
                        Sync_Id__c = 'rel_' + trail.api_name + ' | ' + module.api_name,
                        Name = String.valueOf(trail.title + ' ' + module.title).abbreviate(80)
                    ));
                }
            }
        }
        if (!upsertRelationshipList.isEmpty()) {
            Database.upsert(
                upsertRelationshipList,
                Training_Course_Relationship__c.fields.Sync_Id__c
            );
        }
    }

    global void finish(Database.BatchableContext info) {
        this.offSet += SC_RelationshipSyncBatch.OFF_SET_SIZE;
        if (this.totalCount > this.offSet) {
            Database.executeBatch(new SC_RelationshipSyncBatch(this.offSet));
        }
    }
}
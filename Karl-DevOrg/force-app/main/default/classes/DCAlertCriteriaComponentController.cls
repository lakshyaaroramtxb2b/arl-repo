global class DCAlertCriteriaComponentController {
	
    private Detection_Alert_Criteria__c  DCAlertCriteria;  
    
	public void setDetectionAlertCriteria (String inID) {

        DCAlertCriteria = [SELECT Id FROM Detection_Alert_Criteria__c WHERE Id =: inID];
	}
    
    public String getDetectionAlertCriteria() {
        
		return DCAlertCriteria.Id;
	}

	public String getAlertCount() {
        
        Integer c = [SELECT COUNT() 
                     FROM Detection_Alert__c 
                     WHERE DetectionAlertCriteria__c =: DCAlertCriteria.Id
                      AND CreatedDate = LAST_N_DAYS:1];
        
        return string.valueOf(c);
    }
    
    public Time GetElapsedTime(Time startTime, Time endTime)
    {
        if(startTime == null || endTime == null)
            return Time.newInstance(0, 0, 0, 0);
    
        Integer elapsedHours = endTime.hour() - startTime.hour();
        Integer elapsedMinutes = endTime.minute() - startTime.minute();
        Integer elapsedSeconds = endTime.second() - startTime.second();
        Integer elapsedMiliseconds = endTime.millisecond() - startTime.millisecond();
    
        return Time.newInstance(elapsedHours, elapsedMinutes, elapsedSeconds, elapsedMiliseconds);
    }
    
    public Time GetElapsedTime(DateTime startDate, DateTime endDate)
    {
         if(startDate == null || endDate == null)
             return Time.newInstance(0, 0, 0, 0);
         return GetElapsedTime(startDate.time(), endDate.time());
    }
        
    public String getLastAlertDate() {

        List<Detection_Alert__c> rc = [SELECT CreatedDate 
                       FROM Detection_Alert__c  
                       WHERE DetectionAlertCriteria__c =: DCAlertCriteria.Id
                        AND CreatedDate = LAST_N_DAYS:1
                       ORDER BY CreatedDate DESC LIMIT 1];
        
        if(rc != null && rc.size() > 0) {
            
        	return string.valueof(GetElapsedTime(datetime.now(), rc[0].CreatedDate).second() + ' minutes ago');
        }
        
        return 'n/a';
    }
    
    @RemoteAction
    global static Set<Data> getChartData(String DCAlertCriteriaId) {

        Set<Data> tmpData = new Set<Data>();
        Set<Data> data = new Set<Data>();

        for( AggregateResult  ar : [SELECT DAY_ONLY(CreatedDate)x, Count(id)y
		 							FROM Detection_Alert__c
		 							WHERE DetectionAlertCriteria__c =: DCAlertCriteriaId
 										AND CreatedDate = LAST_N_DAYS:7
		 							GROUP BY DAY_ONLY(CreatedDate)
		 							ORDER BY DAY_ONLY(CreatedDate)] )
        {
			String edate = string.valueof(ar.get('x')).split(' ')[0];
            
            tmpData.add(new Data(edate, Integer.valueOf(ar.get('y'))));
        }
        
        DateTime cDate = Date.today().addDays(-7);
		
        for(Integer x = 0; x < 8; x++) {
            
            String dString = cDate.format('YYYY-MM-dd');
            Boolean found = false;
            
            for(Data d : tmpData) {
                if(d.name == dString) {
                    data.add(new Data(dString, d.data1));
                    found = true;
                    break;
                }
            }
            
            if(!found) {
            	data.add(new Data(dString, 0));
            }
 
            cDate = cDate.addDays(1); 
        }  

		return data;
	}
}
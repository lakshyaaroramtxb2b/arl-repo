/**
* @author Lakshya Arora
* @email lakshya.arora@mtxb2b.com
* @description Test class for KARL_GUSProductTagDATACREATIONBATCH
*/
@isTest
public class KARL_GUSProductTagDATACREATIONBATCHTest {
    /**
* @author Lakshya Arora
* @email lakshya.arora@mtxb2b.com
* @desc Test the functionality of Gus Product Tag Creation after post deployment
*/
    @isTest
    public static void productTagRecordCreationBatchTest(){
        User opsAdminUser = ARL_TestDataFactory.createOpsAdmin();
        System.runAs(opsAdminUser){
            KARL_GUSProductTagDATACREATIONBATCH obj = new KARL_GUSProductTagDATACREATIONBATCH();
            ADM_Scrum_Team_c__x admScrumTeam = new ADM_Scrum_Team_c__x();
            admScrumTeam.ExternalId = 'TEAM123';
            ADM_Product_Tag_c__x admProductTag = new ADM_Product_Tag_c__x();
            admProductTag.ExternalId = 'TAG123';
            admProductTag.Team_c__c = 'TEAM123';
            admProductTag.Active_c__c = true;
            
            ADM_Product_Tag_c__x admProductTag1 = new ADM_Product_Tag_c__x();
            admProductTag1.ExternalId = 'TAG1234';
            admProductTag1.Team_c__c = 'TEAM123';
            admProductTag1.Active_c__c = true;
            
            List<ADM_Product_Tag_c__x> productTagListForTestClass = new List<ADM_Product_Tag_c__x>();
            productTagListForTestClass.add(admProductTag);
            productTagListForTestClass.add(admProductTag1);
            obj.scrumTeamIdToActiveMapTest.put('TEAM123',true); 
            
            Test.startTest();
            //need to call explicitly as no data for external Data
            Database.QueryLocator ql = obj.start(null);
            obj.Finish(null);
            obj.execute(null, productTagListForTestClass);
            obj.execute(null);
            Test.stopTest();
            List<KARL_GUS_Product_Tag__c> gusProductTagList = new List<KARL_GUS_Product_Tag__c>([SELECT Id,Active__c,Product_Tag_External_Id__c FROM KARL_GUS_Product_Tag__c WITH SECURITY_ENFORCED]);
            System.assertEquals(gusProductTagList.size(),2);
        }
    }
}
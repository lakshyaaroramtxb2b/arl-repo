public class FreezeCompromisedUserTesterController {

   // PW only mode 
   public String pwInstancesChosenPublish {get; set;}
   public String pwUserId {get; set;}
   public String pwUserName {get; set;}   
   public String pwOrgId {get; set;}

   // Freeze mode
   public String instancesChosenPublish {get; set;}
   public String userId{get; set;}
   public String userName {get; set;}   
   public String orgId{get; set;}
   public String ips{get; set;}
   
   public List<Map<String, Object>> getFreezeHistory() {
        List<SfdcAuditableActionResult> freezeHistory = [SELECT Body,CreatedDate,EndPoint,Id,IsCompleted,IsDeleted,LastModifiedById,LastModifiedDate,Message,RequestId,Status,SystemModstamp, Request.Action, Request.Body, Request.CreatedById FROM SfdcAuditableActionResult where request.action like 'freezeCompromisedUser%' order by createddate desc];   
        List<Map<String, Object>> result = new List<Map<String, Object>>();
        for (SfdcAuditableActionResult thisResult: freezeHistory) {
            Map<String, Object> thisResultMap = new Map<String,Object>();
            thisResultMap.put('CreatedBy', thisResult.Request.CreatedById);
            thisResultMap.put('CreatedDate', thisResult.createdDate);
            thisResultMap.put('Endpoint', thisResult.EndPoint);
            thisResultMap.put('RequestAction', thisResult.Request.Action);
            thisResultMap.put('Status', thisResult.Status);            
            thisResultMap.put('Message', thisResult.Message);   
            thisResultMap.put('RequestBody', thisResult.Request.Body.toString());                                 
            thisResultMap.put('ResultBody', thisResult.Body.toString());                     
            result.add(thisResultMap);
        }
        return result;
   }
   
   public List<SelectOption> getAllInstances() {
        List<String> allInstances = malwareDefense.MalwareRuleUtil.getAllInstances();
        List<SelectOption> options = new List<SelectOption>();
        if(allInstances == null) { return options; }
        for(String inst : allInstances) {
            options.add(new SelectOption(inst, inst));
        }        
        
        return options;
    }
    
    public String getEndpoint() {
        return 'https://security.my.salesforce.com/services/admin/v40.0/freezeCompromisedUser/';
    }    
    
    public void forceICAndPasswordChangeOnly() {
    
        if (String.isBlank(pwUserId) || String.isBlank(pwUserName) || String.isBlank(pwOrgId) || String.isBlank(pwInstancesChosenPublish) ) {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR ,'Missing Argument, make sure you provide every required fields.' );
            ApexPages.addMessage(myMsg);
            return;
        }
    
        try {
            Map<String, Object> bodyMap = new Map<String, Object>();
            bodyMap.put('userId', pwUserId);
            bodyMap.put('username', pwUserName);
            bodyMap.put('forceICAndPasswordChangeOnly', true);
            String body = JSON.serializePretty(bodyMap);
            String sid = UserInfo.getSessionId();
            Http h = new Http();
            HttpRequest req = new HttpRequest();
            req.setHeader('Authorization', 'Bearer ' + sid);
            req.setHeader('X-SFDC-INSTANCE', pwInstancesChosenPublish);
            req.setHeader('Content-Type', 'application/json');
            req.setEndpoint(getEndpoint() + pwOrgId);
            req.setMethod('POST');
            req.setBody(body);
            HttpResponse res = h.send(req);
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Force IC And Password Request Submitted with Request ID : ' + res.getBody());
            ApexPages.addMessage(myMsg);
        }
        catch (Exception ex) {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,ex.getMessage());
            ApexPages.addMessage(myMsg);
        }
    }
    
    public void freezeUser() {
        try {
        
            if (String.isBlank(userId) || String.isBlank(userName) || String.isBlank(orgId) || String.isBlank(InstancesChosenPublish) ) {
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR ,'Missing Argument, make sure you provide every required fields.' );
                ApexPages.addMessage(myMsg);
                return;
            }
        
            Map<String, Object> bodyMap = new Map<String, Object>();
            bodyMap.put('userId', userId);
            bodyMap.put('username', userName);
            if (ips != null && ips.length() > 0) {
                List<String> ipList = ips.split(',');
                if (ipList.size() > 0) {
                    bodyMap.put('ips', ipList);
                }
            }
            else {
                bodyMap.put('ips', new List<String>());                
            }
            
            bodyMap.put('forceICAndPasswordChangeOnly', false);
             
            String body = JSON.serializePretty(bodyMap);
            String sid = UserInfo.getSessionId();
            Http h = new Http();
            HttpRequest req = new HttpRequest();
            req.setHeader('Authorization', 'Bearer ' + sid);
            req.setHeader('X-SFDC-INSTANCE', instancesChosenPublish);
            req.setHeader('Content-Type', 'application/json');
            req.setEndpoint(getEndpoint() + orgId);
            req.setMethod('POST');
            req.setBody(body);
            HttpResponse res = h.send(req);
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Freeze User Request Submitted with Request ID : ' + res.getBody());
            ApexPages.addMessage(myMsg);
        }
        catch (Exception ex) {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,ex.getMessage());
            ApexPages.addMessage(myMsg);
        }    
    }    

}
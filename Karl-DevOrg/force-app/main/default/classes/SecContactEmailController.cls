public with sharing class SecContactEmailController {
    public String uName {get;set;}
    public String uEmail {get;set;}
    public String uSubject {get;set;}
    public String uDescription {get;set;}
    public Boolean uCopyMe {get;set;}

    public Boolean mailSent {get;set;}
    
    // For org wide email ID
    private SecToolsQuizConstants qc;
        
    
    public SecContactEmailController() {
        mailSent = False;
        qc = new SecToolsQuizConstants();
    }
    
    public PageReference doEmail() {
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(new String[] {[select id, Address from OrgWideEmailAddress WHERE Id=:qc.ORG_WIDE_EMAIL_ADDRESS_ID].Address});
        if (uCopyMe) {
            mail.setCcAddresses(new String[] {uEmail});
        }

        mail.setOrgWideEmailAddressId(qc.ORG_WIDE_EMAIL_ADDRESS_ID);
        mail.setSubject('[security.force.com] '+uSubject);
        mail.setUseSignature(false);
        mail.setReplyTo(uEmail);
        String prefix = uName+' ('+uEmail+') wrote:';
        mail.setPlainTextBody(prefix+'\n\n\n'+uDescription);
        mail.setHtmlBody(prefix+'<br /><br /><br />'+uDescription);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
    
        mailSent = True;
        return null;
    }

}
/*
 * batch class to support the escalation and notification of trail completion
*/
public class SC_EscalationBatch implements Database.Batchable<sObject>, Database.Stateful {
    
    String query;
    List<String> emailErrorsLog = new List<String>();

    public SC_EscalationBatch() {
        this(null);
    }

    public SC_EscalationBatch(Id courseId) {
        // checking to see if we need to send an error email.
        Set<Id> validProfileIds = SC_EscalationProfileEngine.validProfileIds;
        SC_EscalationProfileEngine.shouldSendErrorEmail();
        query = 
            'SELECT ' +
                'con.Manager_Email__c, ' +
                'con.Org62_User_Id__c, ' +
                'course.Name, ' +
                'course.Training_Escalation_Profile__c, ' +
                'course.Url__c, ' +
                'course.Org_Wide_Email_Id__c, ' +
                'Course_Level__c, ' +
                'Date_of_Escalation_to_Manager__c, ' +
                'Due_Date__c, ' +
                'Escalation1__c, ' +
                'Escalation2__c, ' +
                'Escalation3__c, ' +
                'Last_Escalation__c, ' +
                'Id ' +
            'FROM Training_Course_Taken__c tct, tct.Contact__r con, tct.Training_Course__r course ' +
            'WHERE course.Training_Escalation_Profile__c != null ' +
            'AND course.Training_Escalation_Profile__c IN ' + 
                DynamicSOQLHelper.format(SC_EscalationProfileEngine.validProfileIds) + ' ' +
            'AND course.Tracked__c = True ' +
            'AND con.Is_Active__c = true ' +
            'AND con.Is_Frozen__c = false ' +
            'AND con.Is_On_Leave_Of_Absence__c = false ' +
            'AND con.Org62_User_Id__c != null ' +
            'AND con.Org62_User_Id__c LIKE \'005%\' ' +
            'AND con.Email != null ' +
            'AND Block_Enrollment__c = false ' +
            'AND Contact__c != null ' +
            'AND Due_Date__c <= ' + 
                DynamicSOQLHelper.format(SC_EscalationProfileEngine.upperDateBound) + ' ' +
            'AND Is_Complete_Combined__c = false ' +
            'AND RecordTypeId = ' + 
                DynamicSOQLHelper.format(TrainingCourseTaken_Constants.RT_ID_MANDATORY) + ' ';
        if (String.isNotBlank(courseId)) {
            query += 'AND Training_Course__c = ' + DynamicSOQLHelper.format(courseId) + ' ';
        }
        query += 'ORDER BY Training_Course__c';
        
    }


    public Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }

    public void execute(Database.BatchableContext BC, Training_Course_Taken__c[] enrollments) {

        Messaging.SingleEmailMessage[] emails = new Messaging.SingleEmailMessage[0];
        Training_Course_Taken__c[] enrollmentsToUpdate = new Training_Course_Taken__c[0];
        for (Training_Course_Taken__c enrollment : enrollments) {
            // skip if we don't have a valid org62 user id
            if (!SC_TrainingUtil.isValidUserId(enrollment.Contact__r.Org62_User_Id__c)) continue;

            Training_Escalation_Profile__c escProfile = SC_EscalationProfileEngine.profileMap.get(
                            enrollment.Training_Course__r.Training_Escalation_Profile__c);

            // overdue enrollments first
            if (
                escProfile.Overdue_Reminder_Frequency_Days__c != null &&
                enrollment.Due_Date__c < Date.today() &&
                postDueNeeded(enrollment.Last_Escalation__c, escProfile.Overdue_Reminder_Frequency_Days__c)
            ) {
                // send overdue email and task.
                if (SC_EscalationProfileEngine.emailTemplateMap.containsKey(
                    escProfile.Overdue_Reminder_Email_Template__c)
                ) {
                    sendNotification(emails, enrollmentsToUpdate,
                        enrollment,
                        SC_EscalationProfileEngine.emailTemplateMap.get(escProfile.Overdue_Reminder_Email_Template__c),
                        null,
                        'Overdue',
                        escProfile.Overdue_Reminder_Include_Manager__c);
                }

            } else if (
                escProfile.Third_Reminder_Days__c != null &&
                enrollment.Escalation3__c == null &&
                escalationNeeded(enrollment.Due_Date__c, escProfile.Third_Reminder_Days__c)
            ) {
                // send third escalation
                if (SC_EscalationProfileEngine.emailTemplateMap.containsKey(
                    escProfile.Third_Reminder_Email_Template__c)
                ) {
                    sendNotification(emails, enrollmentsToUpdate,
                        enrollment,
                        SC_EscalationProfileEngine.emailTemplateMap.get(escProfile.Third_Reminder_Email_Template__c),
                        Training_Course_Taken__c.Escalation3__c,
                        'Third Reminder',
                        escProfile.Third_Reminder_Include_Manager__c);
                }
            
            } else if (
                escProfile.Second_Reminder_Days__c != null &&
                enrollment.Escalation2__c == null &&
                enrollment.Escalation3__c == null &&
                escalationNeeded(enrollment.Due_Date__c, escProfile.Second_Reminder_Days__c)
            ) {
                // send second escalation
                if (SC_EscalationProfileEngine.emailTemplateMap.containsKey(
                    escProfile.Second_Reminder_Email_Template__c)
                ) {
                    sendNotification(emails, enrollmentsToUpdate,
                        enrollment,
                        SC_EscalationProfileEngine.emailTemplateMap.get(escProfile.Second_Reminder_Email_Template__c),
                        Training_Course_Taken__c.Escalation2__c,
                        'Second Reminder',
                        escProfile.Second_Reminder_Include_Manager__c);
                }

            } else if (
                escProfile.First_Reminder_Days__c != null &&
                enrollment.Escalation1__c == null &&
                enrollment.Escalation2__c == null &&
                enrollment.Escalation3__c == null &&
                escalationNeeded(enrollment.Due_Date__c, escProfile.First_Reminder_Days__c)
            ) {
                // send first escalation
                if (SC_EscalationProfileEngine.emailTemplateMap.containsKey(
                    escProfile.First_Reminder_Email_Template__c)
                ) {
                    sendNotification(emails, enrollmentsToUpdate,
                        enrollment,
                        SC_EscalationProfileEngine.emailTemplateMap.get(escProfile.First_Reminder_Email_Template__c),
                        Training_Course_Taken__c.Escalation1__c,
                        'First Reminder',
                        escProfile.First_Reminder_Include_Manager__c);
                }
            }
        }

        update enrollmentsToUpdate;
        Messaging.SendEmailResult[] results = Messaging.sendEmail(emails, false);
        for(Messaging.SendEmailResult result: results) {
            if(!result.isSuccess()) {
                logEmailError(result);
            }
        }
    }
    
    private void logEmailError(Messaging.SendEmailResult result) {
        for(Messaging.SendEmailError e : result.getErrors()) {       
            String emailError = '';
            emailError += 'Target Object Id: ';
            emailError += e.getTargetObjectId();
            emailError += ' Error: ';
            emailError += e.getMessage();
            emailErrorsLog.add(emailError);
        }
    }

    private void sendNotification(
            Messaging.SingleEmailMessage[] emails,
            Training_Course_Taken__c[] enrollmentsToUpdate,
            Training_Course_Taken__c enrollment,
            EmailTemplate emailTemplate,
            Schema.SObjectField auditField,
            String escalationIdentifier,
            Boolean includeManager) {

        enrollment.Last_Escalation__c = DateTime.now();
        if (auditField != null) enrollment.put(auditField, Date.today());
        enrollmentsToUpdate.add(enrollment);

        Messaging.SingleEmailMessage email = makeEmail(enrollment, emailTemplate);
        emails.add(email);

        if (includeManager && enrollment.Contact__r.Manager_Email__c != null) {
            email.setCCAddresses(new String[] { enrollment.Contact__r.Manager_Email__c });
            if (enrollment.Date_of_Escalation_to_Manager__c == null) {
                enrollment.Date_of_Escalation_to_Manager__c = Date.today();
            }
        }
    }

    private Boolean postDueNeeded(DateTime lastEscalation, Decimal postDueFrequency) {
        return lastEscalation == null || 
            lastEscalation.date().addDays(postDueFrequency.intValue()) <= Date.today();
    }

    private Boolean escalationNeeded(Date dueDate, Decimal escalationDays) {
        return dueDate <= Date.today().addDays(escalationDays.intValue());
    }

    private Messaging.SingleEmailMessage makeEmail(
        Training_Course_Taken__c pCourseTaken,
        EmailTemplate pEmailTemplate
    ) {
        Messaging.SingleEmailmessage email = new Messaging.SingleEmailmessage();
        if (String.isBlank(pCourseTaken.Training_Course__r.Org_Wide_Email_Id__c)) {
            email.setOrgWideEmailAddressId(SC_Constants.SC_ORG_WIDE_EMAIL_ID);
        } else {
            email.setOrgWideEmailAddressId(pCourseTaken.Training_Course__r.Org_Wide_Email_Id__c);
        }
        email.setTargetObjectId(pCourseTaken.Contact__c);
        email.setSaveAsActivity(true);
        email.setCharset('UTF-8');
        email.setTemplateId(pEmailTemplate.Id);
        email.setWhatId(pCourseTaken.Id);
        return email;
    }

    private String nullVal(String input, String valIfNull) {
        return input == null ? valIfNull : input;
    }
    
    public void finish(Database.BatchableContext BC) { 
        if (!emailErrorsLog.isEmpty()) {
            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
            email.setTargetObjectId(UserInfo.getUserId());
            email.setSubject('Escalation Email Failures ' + DateTime.now().format());
            email.setPlainTextBody(String.join(emailErrorsLog, '\n'));
            email.setSaveAsActivity(false);
            email.setCCAddresses(Label.SC_TrainingBatchCC.split('\\s*,\\s*'));
            if (!Test.isRunningTest()) {
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { email });    
            }
        }
    }
}
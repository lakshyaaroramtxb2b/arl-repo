public class SquawkerController {
    
    public String selectedRuleId {get;set;}
    
    public void SquawkerController()
    {
    }
    
    public Integer currentPage
    {
        get{
            return setRules.getPageNumber();
        }
        set;
    }
    
    public Integer lastPage
    {
        get{
            return (Integer)Math.ceil(Double.valueOf(setRules.getResultSize()) / setRules.getPageSize());
        }
        set;
    }
    
    public ApexPages.StandardSetController setRules {
        get {
            if(setRules == null) 
            {
                setRules = new ApexPages.StandardSetController(Database.getQueryLocator(
                   [SELECT Id, Name, Description__c, ObjectToWatch__c, ObjectToWatchFriendly__c, 
                	AlertLogic__c, GroupsToNotify__c, Last_Edit_Date__c, LastModifiedBy.Name, Groups_To_Notify_Friendly__c 
                    FROM SquawkerRule__c
                    WHERE active__c = true
                    ORDER BY Name]));
            }
            return setRules;
        }
        set;
    }
    
    public String pageKey
    {
        get{
            return '(Page ' + currentPage + ' of ' + lastPage +')';
        }
        set;
    }
    
    public void Next()
    {
        if(currentPage < lastPage)
        	setRules.setPageNumber(++currentPage);
    }
    
    public void Previous()
    {
        if(currentPage > 1)
        	setRules.setPageNumber(--currentPage);
    }

    // Initialize setCon and return a list of records
    public List<SquawkerRule__c> getRules() {

		List<SquawkerRule__c> cRules = (List<SquawkerRule__c>)setRules.getRecords();
        
        if(cRules.size() == 0)
        	ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'No active rules'));
 
        return cRules;
    }
    
    public PageReference DeleteRule() {
        SquawkerRule__c selectedRule = [SELECT Id, Name, Description__c, ObjectToWatch__c, ObjectToWatchFriendly__c, 
         AlertLogic__c, GroupsToNotify__c, Last_Edit_Date__c, LastModifiedBy.Name, Groups_To_Notify_Friendly__c, active__c 
         FROM SquawkerRule__c
         WHERE Id =: selectedRuleId];
        
        //hide rule treates it as deleted but keeps the record for histroic purposes
        selectedRule.active__c = false;
        
        System.debug('Delete Rule');
        System.debug(selectedRule);
        
        update selectedRule;
        
        //reload current page
        PageReference cPageRef = new PageReference('/apex/Squawk_ViewRules');
		cPageRef.setRedirect(true);
      	return cPageRef;
    }
}
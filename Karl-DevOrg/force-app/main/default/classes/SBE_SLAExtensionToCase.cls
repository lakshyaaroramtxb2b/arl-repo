/***************IEM_SBERequestToCase****************
@Author Banshi
@Date 12/02/2019
@Modified by/ date - Swarnima singh Mandhata - T-09359 - 03/31/2020
@Description Batch class to fetch SBE record on regular basis on certain creteria and then create crossponding case records.
**********************************************/

public class SBE_SLAExtensionToCase implements Database.Batchable<sObject> {
    @TestVisible
    private static Map<String, ADM_Work_c__x> mockGusWorkMap = new Map<String, ADM_Work_c__x>();// Mock GUS work map    
    @TestVisible
    private static list<TM_Security_Work_SLA_Extension_c__x> mockallSBEList = new list<TM_Security_Work_SLA_Extension_c__x>();//For test class
    @TestVisible 
    private static Map<String, String> mockGUSUserEmpMap = new Map<String, String>();
    public Database.QueryLocator start(Database.BatchableContext BC) {
        if(Test.isRunningTest()){
            return Database.getQueryLocator([SELECT Id FROM User LIMIT 1]);    
        }else{
            return Database.getQueryLocator([Select Id 
                                             From TM_Security_Work_SLA_Extension_c__x 
                                             Where TM_SLA_Extension_Status_c__c ='Review by I&EM analyst'  AND  LastModifiedDate__c >=LAST_N_DAYS:1]);    
        }
    }
    public void execute(Database.BatchableContext BC, List<sObject> securityGusRecord)
    {
        List<Case> newCaseList = new List<Case>();  
        List<TM_Security_Work_SLA_Extension_c__x> gusRecords = new List<TM_Security_Work_SLA_Extension_c__x>();
        if(Test.isRunningTest()){
            gusRecords =  mockallSBEList;
        }else{
            gusRecords = [SELECT Id,Security_Case_Number_c__c,SA_Approver_c__c,TM_Work_Details_and_Steps_to_Reproduce_c__c ,CreatedById__c ,
                                 TM_Corresponding_Work_c__c ,TM_Requested_Remediation_Date_c__c ,TM_SLA_Extension_Status_c__c,Technical_Description_of_Issue__c,
                                 Due_Date_Justification__c,Describe_Technical_Work_to_Remediate__c,TM_Definition_of_Done_c__c,TM_Approved_by_Business_c__c,
                                 TM_Approved_Remediation_Date_c__c,TM_Approved_by_Security_c__c,TM_Date_Submitted_for_Approval_c__c,TM_Resubmission_Date_c__c ,
                                 TM_Severity_Rating_Mapping_c__c ,TM_Date_Approved_c__c ,TM_Review_by_I_EM_analyst_begins_c__c,TM_Submitted_for_Business_Approval_c__c ,
                                 TM_Remediation_Plan_Author_c__c ,TM_Issue_Data_Classification_c__c ,TM_Link_to_Internal_Knowledge_Article_c__c ,TM_Work_Subject_c__c,
                                 TM_Remediation_Plan_Owner_c__c,TM_Work_Cloud_c__c ,ExternalId,LastModifiedDate__c 
                                 FROM TM_Security_Work_SLA_Extension_c__x WHERE ID IN:securityGusRecord];
        }
        if(!gusRecords.isEmpty()){
            Set<String> gusUserIds = new Set<String>(); //set to store GUS user (External Object) record Ids.
            Set<String> gusWorkIds = new Set<String>(); //set to store GUS Work (External Object) record Ids.
            Map<String, ADM_Work_c__x> gusWorkMap = new Map<String, ADM_Work_c__x>();// GUS work map
            Set<String> productOwnerSet = new Set<String>(); //Set to store product owner names.
            Set<String> cloudNameSet = new Set<String>(); //set to store cloud names.
            Map<String, Id> cloudNameIdMap = new Map<String, Id>(); //Map of Cloud map and Id.
            Map<String, String> gusUserEmpMap = new Map<String, String>(); //Map of GUS user and Employee.
            Map<String, String> userMap = new Map<String, String>(); //Map of user records.
            Map<String, String> contactMap = new Map<String, String>(); //Map of contact records.
            Map<String, TM_Security_Work_SLA_Extension_c__x> SLAMap = new Map<String, TM_Security_Work_SLA_Extension_c__x>();
            Set<String> existingSBERecordIds = new Set<String>();
            
            Map<Id, Case > caseMap = new Map<Id, Case >();
            //Iterate all the GUS work records
            for(TM_Security_Work_SLA_Extension_c__x SBERecord : (List<TM_Security_Work_SLA_Extension_c__x>)gusRecords){
                if( SBERecord.TM_SLA_Extension_Status_c__c  == IEM_Constants.SBE_STATUS_IEM_ANALYST  ){
                    System.debug('##SBERecord##'+SBERecord.TM_Work_Subject_c__c);
                    SLAMap.put(SBERecord.ExternalId, SBERecord);
                    if(String.isNotBlank(SBERecord.TM_Corresponding_Work_c__c)){
                        gusWorkIds.add(SBERecord.TM_Corresponding_Work_c__c);
                    }
                    if(String.isNotBlank(SBERecord.TM_Remediation_Plan_Owner_c__c)){
                        String planOwnerId = (SBERecord.TM_Remediation_Plan_Owner_c__c).substringBetween('href="', '" target=');
                        if(String.isNotBlank(planOwnerId)){
                            gusUserIds.add(planOwnerId);
                        }
                    }
                    if(String.isNotBlank(SBERecord.TM_Remediation_Plan_Author_c__c )){
                        gusUserIds.add(SBERecord.TM_Remediation_Plan_Author_c__c );
                    }
                    if(String.isNotBlank(SBERecord.CreatedById__c)){
                        gusUserIds.add(SBERecord.CreatedById__c);
                    }
                    if(String.isNotBlank(SBERecord.SA_Approver_c__c)){
                        gusUserIds.add(SBERecord.SA_Approver_c__c);
                    }
                }
            }
            if(!gusWorkIds.isEmpty()){
                if(Test.isRunningTest()){
                    gusWorkMap = mockGusWorkMap;
                }else{
                    for(ADM_Work_c__x gusWork : [Select ID,Subject_c__c,Name__c, ExternalId, Team__c  From ADM_Work_c__x Where ExternalId IN :gusWorkIds]){
                        gusWorkMap.put(gusWork.ExternalId, gusWork);
                    }
                }
            }
            if(!SLAMap.isEmpty()){
                for(Case c: [Select Id,SLA_Extension_Id__c From Case Where SLA_Extension_Id__c IN :SLAMap.keySet()]){
                    existingSBERecordIds.add(c.SLA_Extension_Id__c);
                }
            }
            if(!gusUserIds.isEmpty()){
                for(User__x usr : [SELECT Id,Email__c,ExternalId, EmployeeNumber__c, External_ID_c__c FROM User__x Where ExternalId IN : gusUserIds AND (EmployeeNumber__c != '' OR Email__c != '')]){
                    String key = (String.isNotBlank(usr.EmployeeNumber__c)) ? usr.EmployeeNumber__c : usr.Email__c;
                    if(String.isNotBlank(key)){
                        gusUserEmpMap.put(key, usr.ExternalId);
                    }
                } 
            }
            //For test coverage only
            if(Test.isRunningTest()){
                gusUserEmpMap = mockGUSUserEmpMap;
            }
            if(!gusUserEmpMap.isEmpty()){
                for(Contact cont : [SELECT Id, Email , Employee_ID__c FROM Contact Where (Employee_ID__c IN :gusUserEmpMap.keySet() OR Email  IN :gusUserEmpMap.keySet()) AND (Contact.AccountId=:Label.ESA_Office_Hours_Account_ID OR Contact.AccountId = :Label.Salesforce_com_Account_ID)]){
                    String key = (String.isNotBlank(cont.Employee_ID__c)) ? cont.Employee_ID__c : cont.Email;
                    if(String.isNotBlank(key) && gusUserEmpMap.containsKey(key)){
                        contactMap.put(gusUserEmpMap.get(key),cont.Id);
                    }
                }
                for(User usr : [SELECT Id,Email, EmployeeNumber FROM User Where (EmployeeNumber IN :gusUserEmpMap.keySet()  OR Email IN :gusUserEmpMap.keySet()) ]){
                    String key = (String.isNotBlank(usr.EmployeeNumber)) ? usr.EmployeeNumber : usr.Email;
                    if(String.isNotBlank(key) && gusUserEmpMap.containsKey(key)){
                        userMap.put(gusUserEmpMap.get(key),usr.Id);
                    }
                }
            }
            System.debug('##contactMap##'+contactMap);
            IEM_CaseOwer caseOwner = new IEM_CaseOwer(); //To get next case owner
            Id nextOwner = caseOwner.getNextCaseOwner();
            Integer index = 0;
            System.debug('##existingSBERecordIds##'+existingSBERecordIds);
            for(TM_Security_Work_SLA_Extension_c__x SBERecord : (List<TM_Security_Work_SLA_Extension_c__x>)gusRecords){
                if( SBERecord.TM_SLA_Extension_Status_c__c  == IEM_Constants.SBE_STATUS_IEM_ANALYST  && !existingSBERecordIds.contains(SBERecord.ExternalId) ){
                    if(index > 0 && String.isNotBlank(nextOwner)){
                        nextOwner = caseOwner.getNextCaseOwner(nextOwner);
                    }
                    Case newcase = new Case();
                    newcase = SBE_MappingUtility.mapSLAToCase(newcase,SBERecord);
                    newCase.Status = IEM_Constants.IEM_CASE_STATUS_INTERNAL_REVIEW;
                    newCase.IEM_Case_Type__c = IEM_Constants.IEM_EXTENSION;
                    newCase.Action_Plan_Status__c = IEM_Constants.IEM_SUBMITTED;
                    newCase.Severity_Assessment_Status__c  = IEM_Constants.IEM_PRE_ASSIGNED;
                    newCase.Issue_Source__c  = IEM_Constants.IEM_ISSUE_SOURCE_SLA;
                    // newCase.Reported_Work_IDs__c = SBERecord.TM_Corresponding_Work_c__c;
                    newCase.Existing_Work_Story__c  = IEM_Constants.IEM_YES;
                    newCase.Security_Standard_Affected__c  = IEM_Constants.IEM_SECURITY_STANDARD_AFFECTED;
                    newCase.Action_Plan_Submission_Date__c = System.now();
                    newCase.Submission_Date__c = System.now();
                    newCase.Investigation_Date_IEM__c = System.now();
                    newCase.Issue_Validation_Date__c = System.now();
                    newCase.Severity_Assessment_Submission_Date__c = System.now();
                    newCase.Action_Plan_Submission_Date__c = System.now();
                    newCase.Internal_Review_Start_Date__c  = System.now();
                    if(String.isNotBlank(nextOwner)){
                        newCase.ownerId = nextOwner;
                    }
                    if(String.isNotBlank(SBERecord.TM_Remediation_Plan_Owner_c__c)){
                        String planOwnerId = (SBERecord.TM_Remediation_Plan_Owner_c__c).substringBetween('href="', '" target=');
                        if(String.isNotBlank(planOwnerId)){
                            Id finalPlanOwnerId = planOwnerId; //15 digit to 18 digit.
                            if(contactMap.containsKey(finalPlanOwnerId)){
                                newcase.Issue_Owner__c = contactMap.get(finalPlanOwnerId);
                            }
                        }
                    }
                    System.debug('##SBERecord.TM_Remediation_Plan_Owner_c__c##'+SBERecord.TM_Remediation_Plan_Owner_c__c);
                    System.debug('##SBERecord.TM_Remediation_Plan_Author_c__c##'+SBERecord.TM_Remediation_Plan_Author_c__c);
                    if(contactMap.containsKey(SBERecord.TM_Remediation_Plan_Author_c__c)){
                        newcase.Action_Plan_Owner__c = contactMap.get(SBERecord.TM_Remediation_Plan_Author_c__c);
                    }
                    if(String.isNotBlank(SBERecord.CreatedById__c) && contactMap.containsKey(SBERecord.CreatedById__c)){
                        newcase.Requestor__c = contactMap.get(SBERecord.CreatedById__c);
                    }
                    if(String.isNotBlank(SBERecord.SA_Approver_c__c) && userMap.containsKey(SBERecord.SA_Approver_c__c)){
                        newcase.Security_Assurance_SME_User__c = userMap.get(SBERecord.SA_Approver_c__c);
                        newcase.Severity_Assessor_User__c  = userMap.get(SBERecord.SA_Approver_c__c);
                    }
                    if(String.isNotBlank(SBERecord.TM_Corresponding_Work_c__c) ) {
                        if(gusWorkMap.containsKey(SBERecord.TM_Corresponding_Work_c__c)){
                            if( gusWorkMap.get(SBERecord.TM_Corresponding_Work_c__c).Team__c != null){
                                newcase.Team_Responsible__c  = gusWorkMap.get(SBERecord.TM_Corresponding_Work_c__c).Team__c;
                            }
                            if( gusWorkMap.get(SBERecord.TM_Corresponding_Work_c__c).Name__c != null){
                                newCase.Reported_Work_IDs__c  = gusWorkMap.get(SBERecord.TM_Corresponding_Work_c__c).Name__c;
                            }
                        }
                    }
                    newCaseList.add(newCase);
                    index++;
                }
            }
            System.debug('##newCaseList##'+newCaseList);
            if( !(newCaseList.isEmpty()) ){
                insert newCaseList;
                List<Case_Work__c> newCaseWorkList = new List<Case_Work__c>();
                List<TM_Security_Work_SLA_Extension_c__x> SBERecordsToUpdate = new List<TM_Security_Work_SLA_Extension_c__x>();
                
                for(Case insertedCase : [Select Id,SLA_Extension_Id__c,CaseNumber From Case Where Id IN :newCaseList]){
                    if(SLAMap.containsKey(insertedCase.SLA_Extension_Id__c)){
                        TM_Security_Work_SLA_Extension_c__x SBERecord = SLAMap.get(insertedCase.SLA_Extension_Id__c);
                        if(String.isNotBlank(SBERecord.TM_Corresponding_Work_c__c) && gusWorkMap.containsKey(SBERecord.TM_Corresponding_Work_c__c) ) {
                            Case_Work__c  caseWork = new Case_Work__c();                                 
                            caseWork.Work__c = SBERecord.TM_Corresponding_Work_c__c;
                            caseWork.Case__c = insertedCase.Id;
                            caseWork.Work_Subject__c = gusWorkMap.get(SBERecord.TM_Corresponding_Work_c__c).Subject_c__c;
                            caseWork.Work_Name__c = gusWorkMap.get(SBERecord.TM_Corresponding_Work_c__c).Name__c;
                            newCaseWorkList.add(caseWork);
                        }
                        SBERecord.Security_Case_Number_c__c = insertedCase.CaseNumber;
                        SBERecordsToUpdate.add(SBERecord);
                    }
                }
                insert newCaseWorkList;
                try{
                    if( !(SBERecordsToUpdate.isEmpty()) ){
                        if(!Test.isRunningTest()){
                            Database.updateAsync(SBERecordsToUpdate);
                        }
                    } 
                }catch(Exception e){}
            }
        }
        
    }
    
    public void finish(Database.BatchableContext BC) {
        // finish code
        SBE_SLAExtensionToCase batch = new SBE_SLAExtensionToCase();
        if(!Test.isRunningTest()){
            System.scheduleBatch(batch, 'SBE_SLAExtensionToCase',5);
        }
        
    }
    
}
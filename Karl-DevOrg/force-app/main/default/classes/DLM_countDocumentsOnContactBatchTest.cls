@isTest
public class DLM_countDocumentsOnContactBatchTest {
    @testSetup
    public static void makeData(){
         List<IS_Document__c> DocList = new List<IS_Document__c>();
        List<Contact> contactList = new List<Contact>();
        List<IS_Document__c> dlmDocumentList = DLM_TestDataFactory.createIsDocumentWithContacts(2, false);
        Contact con = new Contact();
        con.LastName = 'Test Data';
        con.RecordTypeId = DLM_TestDataFactory.DLM_CONTACTRECORDTYPEID;
        con.Is_Active__c = true;
        con.Org62_User_ID__c = 'qwertyuiopasdfghjk';
        contactList.add(con);
        
        Contact con1 = new Contact();
        con1.LastName = 'Test23 Data1';
        con1.RecordTypeId = DLM_TestDataFactory.DLM_CONTACTRECORDTYPEID;
        con1.Is_Active__c = true;
        con1.Org62_User_ID__c = 'mnbvcxzasdfghjklpo';
        contactList.add(con1);

        Insert contactList;
        
          for(IS_Document__c doc: dlmDocumentList){
            doc.Content_Owner__c = contactList[0].id;
            doc.Content_Custodian__c = contactList[1].id;
            DocList.add(doc); 
        }
        insert DocList;
    }
    
    @isTest
    public static void executeBatch(){
        DLM_countDocumentsOnContactBatch batch = new DLM_countDocumentsOnContactBatch();
        Database.executeBatch(batch,200);
    }
}
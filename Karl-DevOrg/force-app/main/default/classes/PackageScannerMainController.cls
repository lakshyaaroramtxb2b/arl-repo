public without sharing class PackageScannerMainController {


    public String Scanorgid { get; set; }
    public String ScanPackageId {get; set;}
    public String ScanPVID {get; set;}
    public String Scanprofile { get; set; }
    public String ScanList {get; set;}
    
    public map<string,list<string>> OrgtoPackageId ; // Map to get Package ID from Org Id
    public map<string, list<chimera__PackageVersion__c>> PidtoPvid;//Map to get Package Version Id object list from Package Id
    public map <string,chimera__PackageVersion__c> PvidObject;// Map to get object from Package Version Id string
    public map<string,string> ListingtoPackage;// Map to get Package from listing
    
    public list<string> Orgids;
    public String searchterm { get; set; }
    public String username { get; set; }
    public list<CloudScanWrapper> Scans {get;set;}
    public boolean ScansFound {get;set;}
    public integer PendingScans {get;set;}
    public integer CompletedScans {get;set;}
    public integer RemainingCredits {get;set;}
    public boolean ShowScannerNews {get;set;}
    public string ScannerNewsText {get;set;}
    public List<chimera__AllPackages__c> packageIdList;
    public set<string> AllpackageVersions;
    public map<string,chimera__PackageVersion__c> Package_collection {get; set;}
    private map <string,CloudScanWrapper> csw;
    private map<Scan_Queue__c,string> ScanQueueInfoMap;
    private Id AccountId;
    private static string salt='BaSsOfsso5gBUupELXUsiiuIJqy3OMuKMqP0qNbh';
    private List <Scan_Queue__c> ScanQueue;
    private List <CodeScan__c> CodeScans;
    private map<string,String> OrgIDKindMap;
    private map<id,string> CodeScanScanQueue;
    
    
    public List<string> PackageIds;
    
    private map<id,string> ScanInfoPDFMap;
    private map<id,string> ScanInfoXMLMap;
    
    public PackageScannerMainController()
    {
        init();
    }
    
    public PageReference TZsync(){

        Chimera.SyncTZ.initate();
   
        return null;
    }
    
    public void init()
    {
        // This is sync the latest packages and Org from TZ
       //
       
       Scanorgid = '';
        //system.debug([SELECT Id FROM ESA_Security_Request__c WHERE Requestor__c =:userid AND ESA_Service_Item__r.ItemCategory__r.ESA_Entity__r.EntityCode__c= :'partner' AND CreatedDate = LAST_N_DAYS:30
                                        //   AND (Status__c = 'Incomplete' OR (Status__c = 'In Progress' AND DAY_ONLY(Office_Hours_Reservation__c) >= :anchorDate))
                                    //       order BY CreatedDate desc LIMIT 1]);
        
        if(chimera__GeneralPortalSettings__c.getInstance('ScannerNewsDisabled')==null || chimera__GeneralPortalSettings__c.getInstance('ScannerNewsDisabled').chimera__Value__c=='Yes')
            ShowScannerNews=false;
        else
            ShowScannerNews=true;
        ScannerNewsText=chimera__GeneralPortalSettings__c.getInstance('ScannerNewsText')!=null?chimera__GeneralPortalSettings__c.getInstance('ScannerNewsText').chimera__Value__c:'';
        ScansFound=false;
        GetAccountDetails();
        
        //AccountId = '0013A00001UCxODQA1';
        
        
        getOrgIds();
        
        
        getListings();
        //getListings();
       
        Packages(Orgids);
        
        
            
            
        csw=new map <string,CloudScanWrapper>();
        ScanQueueInfoMap=new map<Scan_Queue__c,string>();
        
        ScanQueue=new List<Scan_Queue__c>();
        
        ScanInfoPDFMap=new map<id,string>();
        ScanInfoXMLMap=new map<id,string>();
        system.debug('orgid:'+OrgIDKindMap);
        string cloudscan = 'CloudScan';
        for(Scan_Queue__c cq:[SELECT id,  Scan_Info__c,Comments__c, Status__c, CodeScan__r.id,Scan_Info_Status__c, Zip_upload__r.id FROM Scan_Queue__c where CodeScan__r.OrgId__c IN :OrgIDKindMap.keyset() OR (Zip_Upload__r.PackageVersionId__c IN :AllpackageVersions AND Friendly_Name__c = :CloudScan) ])
        {
            if(cq.Scan_Info__c==null)
                cq.Scan_Info__c='01pQ00000000000';//hacks - Invalid Id
            ScanQueueInfoMap.put(cq,cq.Scan_Info__c);
            ScanQueue.add(cq);
        }
        system.debug('ScanQueueInfoMap:'+ScanQueueInfoMap);
        for(Attachment at:[select id,Name,parentid from Attachment where parentId IN :ScanQueueInfoMap.values()])
        {
            if(at.Name.contains('.pdf') ||  at.Name.contains('.html.zip'))//.html.zip
                ScanInfoPDFMap.put(at.parentid,at.id);
            if(at.Name.contains('.xml') || at.Name.contains('.xml.zip'))//.xml.zip
                ScanInfoXMLMap.put(at.parentid,at.id);
        }
        //Salesforce cannot check null in VF page https://success.salesforce.com/issues_view?id=a1p30000000T3k2AAC
        //Need to do some hacks
        for(Scan_Queue__c queue:ScanQueueInfoMap.keyset())
        {
            CloudScanWrapper cswtemp=new CloudScanWrapper();
            if(queue.CodeScan__r.id!=null)
             cswtemp.CodeScanId=queue.CodeScan__r.id;
            if(queue.Zip_upload__r.id!=null)
             cswtemp.CodeScanId=queue.Zip_upload__r.id; 
            cswtemp.ScanQueueId=queue.id;
            cswtemp.SIStatus=queue.Scan_Info_Status__c;
            cswtemp.SQComments=queue.Comments__c;
            cswtemp.SQstatus=queue.Status__c;
            if(ScanInfoPDFMap.get(ScanQueueInfoMap.get(queue))!=null)
                cswtemp.pdfid=ScanInfoPDFMap.get(ScanQueueInfoMap.get(queue));
            else
                cswtemp.pdfid='NONE';
            if(ScanInfoXMLMap.get(ScanQueueInfoMap.get(queue))!=null)
                cswtemp.xmlid=ScanInfoXMLMap.get(ScanQueueInfoMap.get(queue));
            else
                cswtemp.xmlid='NONE';
            if(queue.CodeScan__r.id!=null)
                csw.put(''+queue.CodeScan__r.id,cswtemp);
            	//csw.put(''+queue.CodeScan__r.id+':'+queue.id,cswtemp);
            	
            if(queue.Zip_upload__r.id!=null)
            {
                csw.put(''+queue.Zip_upload__r.id,cswtemp);
                system.debug(queue.Zip_upload__r.id);
            }
                //csw.put(''+queue.Zip_upload__r.id+':'+queue.id,cswtemp);
        }
        //for(CodeScan__c cs:[select id, Username__c, CreatedDate, OrgID__c, WorkState__c,Comments__c from CodeScan__c where OrgId__c IN :OrgIDKindMap.keyset() AND CreatedDate > 2016-01-01T00:00:00Z ORDER BY CreatedDate ASC] )
        for(CodeScan__c cs:[select id, Username__c, CreatedDate, OrgID__c, WorkState__c,Comments__c from CodeScan__c where OrgId__c IN :OrgIDKindMap.keyset() ORDER BY CreatedDate DESC ] )
        {
            boolean isFound=false;
            //for(String nm:csw.keyset())
           // {
           string nm = cs.id;
                if(csw.containsKey(nm))
                {
                    csw.get(nm).status=cs.WorkState__c;
                    csw.get(nm).username=cs.Username__c;
                    csw.get(nm).createddate=cs.CreatedDate;
                    csw.get(nm).OrgID=cs.OrgID__c;
                    csw.get(nm).CSComments=cs.Comments__c;
                    csw.get(nm).status=cs.WorkState__c;
                    csw.get(nm).CodeScanId=cs.id;
                    isFound=true;
                }
            //}
            if(isFound==false)
            {
                //Get only code scans after 2018
               if(cs.CreatedDate.year()>=2019)
                {
                    CloudScanWrapper cswtemp=new CloudScanWrapper();
                    cswtemp.status=cs.WorkState__c;
                    cswtemp.username=cs.Username__c;
                    cswtemp.createddate=cs.CreatedDate;
                    cswtemp.OrgID=cs.OrgID__c;
                    cswtemp.CSComments=cs.Comments__c;
                    cswtemp.status=cs.WorkState__c;
                    cswtemp.CodeScanId=cs.id;
                    csw.put(''+cs.id,cswtemp);
                }
            }
        }
        //** For PVID
        
       
         for(Zip_upload__c cs:[select id, Requestor__c ,CreatedDate, PackageVersionId__c , WorkState__c,Comments__c from Zip_upload__c where PackageVersionId__c IN :AllpackageVersions AND Friendly_Name__c = :CloudScan ORDER BY CreatedDate ASC] )
        {
            boolean isFound=false;
            for(String nm:csw.keyset())
            {
                if(nm.contains(cs.id))
                {
                    csw.get(nm).status=cs.WorkState__c;
                    csw.get(nm).username=cs.Requestor__c;
                    csw.get(nm).createddate=cs.CreatedDate;
                    csw.get(nm).PVID=cs.PackageVersionId__c;
                    csw.get(nm).CSComments=cs.Comments__c;
                    csw.get(nm).CodeScanId=cs.id;
                    isFound=true;
                }
            }
            if(isFound==false)
            {
                //Only scans after 2019
               if(cs.CreatedDate.year()>=2019)
                {
                    CloudScanWrapper cswtemp=new CloudScanWrapper();
                    cswtemp.status=cs.WorkState__c;
                    cswtemp.username=cs.Requestor__c;
                    cswtemp.createddate=cs.CreatedDate;
                    cswtemp.PVID=cs.PackageVersionId__c;
                    cswtemp.CSComments=cs.Comments__c;
                    cswtemp.CodeScanId=cs.id;
                    csw.put(''+cs.id,cswtemp);
                }
            }
        }
        
        
        
        
        
        //End for PVID
        scans = new List<CloudScanWrapper>();
        for(string cs:csw.keyset())
        {
            csw.get(cs).SetStatus();
            //scans.add(csw.get(cs));
        }
        CountScans(csw);
        
        scans=SortScans(csw); 
        //Scans=SortScans(csw);
        if(Scans.size()>0)
            ScansFound=true;
    }
    
    public class CloudScanWrapper{
    public string CodeScanId {get;set;}
    public string ScanQueueId {get;set;}
    public string username {get;set;}
    public string status {get;set;}
    public string SIStatus {get;set;}
    public string SQStatus {get;set;}
    public string ExtStatus {get;set;}
    public string SQComments {get;set;}
    public string CSComments {get;set;}
    public string Comments {get;set;}
    public string OrgID {get;set;}
    public datetime createddate {get;set;}
    public string pdfid {get;set;}
    public string xmlid {get;set;}
    public string PVID {get;set;}
    //public string csrfpdfid {get;set;}
    //public string csrfxmlid;
    public CloudScanWrapper()
        {
            pdfid='NONE';
            xmlid='NONE';
        }
        public string getcsrfxmlid()
        {
            return EncodingUtil.urlEncode(genCSRF(xmlid),'UTF-8');
        }
         public string getcsrfpdfid()
        {
            return EncodingUtil.urlEncode(genCSRF(pdfid),'UTF-8');
        }
        /*
        "job requested" <-- Code scan status is everything other than "never" or "download completed"
        "job accepted -- waiting to scan" <--CodeScan status is "download completed"
        "job rejected" <-- Code Scan status is "never", in which case you may want to display the comment field.
        
        "scanning" <-- SQ is Waiting for scan to finish
        "Scan completed. Waiting for report generation" SQ is done 
        "failed" <-- SQ is failed you may want to display the scan queue comment field. to explain the failure.
        "paused" SQ is paused
        
        if(SQ is done)
        {
            
            'Scan completed. Waiting for report generation'  =>  If Scan_Info.Status = Null | 'New'
            'Done'  == >  If Scan_Info.Status = 'Done'
            'Scan completed. Report Generation Failed' ==> If Scan_Info.Status = 'Failed'
            'Scan completed. Report Generation in Process' ==> For other values of Scan_Info.Status
        }
        */
        public void SetStatus()
        {
            ExtStatus='';
            if(status!=null)
            {
                if(status=='Never')
                   {
                       ExtStatus='Job Rejected';
                       Comments=CSComments;
                   }
                if(status=='Download Completed')
                    ExtStatus='Job Accepted - Waiting To Scan';
            }
            if(SQstatus!=null)
            {
                if(SQstatus=='Waiting for scan to finish')
                    ExtStatus='Scanning';
                if(SQstatus=='Done')
                    ExtStatus='Scan completed. Waiting for report generation';
                if(SQstatus=='Failed')
                    {
                        ExtStatus='Failed';
                        Comments=SQComments;
                    }
                if(SQstatus=='Paused')
                    ExtStatus='Paused';
            }
            if(SQstatus=='Done')
            {
                if(SIStatus=='')
                    ExtStatus='Scan completed. Waiting for report generation';
                else if(SIStatus=='Done')
                    ExtStatus='Done';
                else if(SIStatus=='Failed')
                    ExtStatus='Scan completed. Report Generation Failed';
                else
                    ExtStatus='Scan completed. Report Generation in Process';
            }
            if(ExtStatus=='')
                ExtStatus='Job Requested';
        }
    
    }
    public void CountScans(map<string,CloudScanWrapper> Scans)
    {
        PendingScans=0;
        CompletedScans=0;
        for(string str:Scans.keyset())
        {
            if(Scans.get(str).Extstatus=='Done' || Scans.get(str).Extstatus=='Job Rejected' || Scans.get(str).Extstatus=='Failed' )
                CompletedScans++;
            else 
                PendingScans++;
        }
    
    }
    
    
    
    
    
    public list<CloudScanWrapper> SortScans(map<string,CloudScanWrapper> UnsortedScan)
    {
        //return new map<string,CloudScanWrapper>();
        // behold the Vinay's Sort!! *everyone claps* *claps* *claps* *claps* *claps* *claps* (child crying in background)
        list<CloudScanWrapper> SortedScan=new list<CloudScanWrapper>();
        map<string,CloudScanWrapper> TempScan=UnsortedScan.clone();
        while(TempScan.size()>0)
        {
            string leastname=null; //more like highestname .. too lazy to change name
            for(String str:TempScan.keyset())
            {
                if(TempScan.get(str).createddate!=null)
                {
                if(leastname==null || TempScan.get(str).createddate.getTime()>TempScan.get(leastname).createddate.getTime())
                    leastname=str;
                }
            }
            SortedScan.add(UnsortedScan.get(leastname));
            TempScan.remove(leastname);
        }
        return SortedScan;
    }
    
//
//  A method to get all org for select option  
//            

    public List<SelectOption> getOrgIds() 
    {
            //Old code - Remove later
            /*OrgIDKindMap=new map<id,string>();
            List<SelectOption> options = new List<SelectOption>();
            Id userId = UserInfo.getUserId();
            Id AccountId=null;
            List<User> users = [select AccountId from User where Id=:userId];
            if(users[0].accountid==null)
            {
                AccountId='001Q000000uuiXV';//001Q000000uuiXV
            }
            else
                AccountId=users[0].accountid;
            if(AccountId!=null)
            {
                Account acc=[select Name from Account where id=:AccountId];
                SelectOption option = new SelectOption(acc.Name,Acc.Name+' (Publisher Org)');
                OrgIDKindMap.put(acc.Name,Acc.Name+' (Publisher Org)');
                options.add(option);
                
                for(Contributing_Org__c co:[select Org_Id__c from Contributing_Org__c where Account__c=:AccountId])
                {
                      option = new SelectOption(co.Org_Id__c,co.Org_Id__c+' (Contributing Org)');
                      OrgIDKindMap.put(co.Org_Id__c,co.Org_Id__c+' (Contributing Org)');
                      options.add(option);
                      
                } 
            }*/
        
        
			// To extract ID for org in Security Org        
        
        
        
            Orgids = new list<string>();
            OrgIDKindMap=new map<string,string>();
            List<SelectOption> options = new List<SelectOption>();
        	
        	//AccountId = '0013A00001UCxODQA1';
        	//This is an empty value required for change action support to work when there is only one org
        	//
        	options.add(new SelectOption('',''));
            if(AccountId!=null)
            {
                //Account acc=[select Name from Account where id=:AccountId];
                SelectOption option;
                //OrgIDKindMap.put(acc.Name,Acc.Name+' (Publisher Org)');
                //options.add(option);
                
                
                for(Contributing_Org__c co:[select Id, Org_Id__c, Publisher_Org__c, Credits__c from Contributing_Org__c where Account__c=:AccountId ORDER BY Publisher_Org__c DESC])
                {
                     
                    
                      Orgids.add(String.valueOf(co.Org_Id__c));
                     string OrgType='';
                      string orgId=String.valueOf(co.Org_Id__c).substring(0, 15);
                     
                   	  
                      //Orgtype+=' - '+co.Credits__c+' Scans Available';
                      option = new SelectOption(co.Org_Id__c,co.Org_Id__c);
                      OrgIDKindMap.put(orgId,orgId+OrgType);
                      options.add(option);
                      
                   	  
                      
                }
            }
        system.debug(orgIds);
        
       
        return options;
    }
//Gets Listing based on account ID of current user
	Public List<selectOption> getListings()
    {
        List<Selectoption> Listings = new List<SelectOption>();
        PackageIds = new List<string>();
        //First value is empty
        Listings.add(new SelectOption('',''));
        
        ListingtoPackage = new Map<string,string>();
        
        if(AccountId!=null){
            for(chimera__Listing__c i:[SELECT chimera__PackageID__c,chimera__TZName__c,Name FROM chimera__Listing__c WHERE chimera__Account__c =:AccountId]){
                
                string ListingName= i.chimera__TZName__c.abbreviate(40);
                ListingtoPackage.put(ListingName,i.chimera__PackageID__c);//To get Package ID from TZName
                PackageIds.add(i.chimera__PackageID__c);
                SelectOption option = new SelectOption(ListingName,ListingName);
                Listings.add(option);
                
            }            
            
        }
        

        
        Return Listings;
        
    }
    
    // 
    // Method to get all PackageIds for a selected org in Select options
    // 
    public List<SelectOption> getPackageIds()
    {
        
        List<selectOption> packageoptions = new List<selectOption>();
        string pvid;
        
        if(ScanList!=Null)
        {
           
            //list<string> package_id = OrgtoPackageId.get(Scanorgid);
            string packageId = ListingtoPackage.get(ScanList);
            if(packageid!=Null)
            {
                 packageoptions.add(new SelectOption('',''));// To add a default value so it will trigger action
                 packageoptions.add(new SelectOption(packageId,packageId));
                
            }
            else
            {
              packageoptions.add(new SelectOption('No Packages','No packages tied to a listing'));
            }
            /*
            list<chimera__PackageVersion__c package_object = Package_Collection.get(Scanorgid);
            if(package_object!=null)
            {
            if(package_object.chimera__MajorVersion__c!=null || package_object.chimera__MinorVersion__c!=null)
            	pvid = package_object.name + ' (' +package_object.chimera__MajorVersion__c +'.' +package_object.chimera__MinorVersion__c+ ') - ' + 
                	package_object.chimera__Credits__c + ' Scans Available';
            else if(package_object.name!=null)
                pvid = package_object.name + package_object.chimera__Credits__c + ' Scans Available';
            
           
            packageoptions.add(new SelectOption(package_object.name,pvid));
            }
            else
            {
            packageoptions.add(new SelectOption('No Packages','No packages tied to a listing in this Org')); 
            }
*/
        }
        else
            packageoptions.add(new SelectOption('No Packages','No packages tied to a listing in this Org'));
        
        
        return Packageoptions;
    }

    
    //
    //Method to get all Package Versions for a given Package ID
    //
    public List<selectOption> getPackageVersion(){
        string PVID;
        List<selectOption> PVIDoptions = new List<selectOption>();
       	List<chimera__PackageVersion__c> PackageVersionCollection= PidtoPvid.get(Scanpackageid);
        
        if(PackageVersionCollection!=null){
            
            for(chimera__PackageVersion__c package_object:PackageVersionCollection){
                	if(package_object.chimera__Major_Version__c!=null || package_object.chimera__Minor_Version__c!=null || package_object.chimera__Patch_Version__c!=null){
            			pvid = package_object.name + ' (' +package_object.chimera__Major_Version__c +'.' +package_object.chimera__Minor_Version__c+ '.' +package_object.chimera__Patch_Version__c + ') - ' + 
                		package_object.chimera__Credits__c + ' Scans Available';
            }
                	else if(package_object.name!=null)
                		pvid = package_object.name +' - ' +package_object.chimera__Credits__c + ' Scans Available';
               
                PVIDoptions.add(new SelectOption(package_object.name,pvid));
            
            }
        }
        else{
                PVIDoptions.add(new SelectOption('No Packages','Please Select the Package ID')); 
        }
                 
   
        return PVIDoptions;
    }
    
    
    
    //
    //To create a two Map on page load for drop downs.
    //
    Public void Packages(List<string> orgs){
        
        system.debug(orgs);
      
        
        
        //Package_collection = new map<string,chimera__PackageVersion__c>();
        OrgtoPackageId = new map<string, list<string>>();
        PidtoPvid = new map<string, list<chimera__PackageVersion__c>>();
       
        
        string org_string= tostring(orgs);
        //
        //Query to extract top 3 package Versions for each Package ID
        //
        packageIdList = new List<chimera__AllPackages__c>();
        //packageIdList = [SELECT Name ,chimera__Developer_Org_Id__c,(SELECT Name, chimera__Credits__c, chimera__MajorVersion__c,chimera__PackageId__c , chimera__MinorVersion__c, chimera__DevelopmentOrganizationId__c, LastModifiedDate,
                                //  chimera__PackageType__c,chimera__VersionName__c FROM chimera__AllPackageVersions__r ORDER BY chimera__MajorVersion__c DESC, chimera__MinorVersion__c DESC LIMIT 3 ) FROM chimera__AllPackages__c WHERE chimera__Developer_Org_Id__c IN :orgs];
        
        packageIdList = [SELECT Name ,chimera__Developer_Org_Id__c,(SELECT Name, chimera__Credits__c, chimera__Major_Version__c,chimera__PackageId__c , chimera__Minor_Version__c, chimera__DevelopmentOrganizationId__c, LastModifiedDate,chimera__Patch_Version__c,
                                  chimera__PackageType__c,chimera__VersionName__c FROM chimera__AllPackageVersions__r Order By chimera__Major_Version__c DESC NULLS LAST, chimera__Minor_Version__c DESC NULLS LAST, chimera__Patch_Version__c DESC NULLS LAST LIMIT 10 ) FROM chimera__AllPackages__c WHERE Name IN :PackageIds];
        
        
        system.debug(packageIdList);
        //system.debug(Packages);
        for(chimera__AllPackages__c i:packageIdList){
            //
            //Map to add Package Id from Org
            //
            if(OrgtoPackageId.get(i.chimera__Developer_Org_Id__c)!= Null)
            {
                OrgtoPackageId.get(i.chimera__Developer_Org_Id__c).add(i.Name);
            }
            else
                OrgtoPackageId.put(i.chimera__Developer_Org_Id__c, new list<string>{i.Name});
            
            //
            //Map to add Package Id to PVID
            //
			if(PidtoPvid.get(i.Name)!=Null)
            {
                 PidtoPvid.get(i.Name).addall(i.chimera__AllPackageVersions__r);
            }
            else
            {
                PidtoPvid.put(i.Name, new list<chimera__PackageVersion__c>());// Instantiation
                PidtoPvid.get(i.Name).addall(i.chimera__AllPackageVersions__r);
            }

        system.debug(OrgtoPackageId);
        system.debug(PidtoPvid);
            
        }
        
        list<chimera__PackageVersion__c> PVIDs = new list<chimera__PackageVersion__c>();
        for(list<chimera__PackageVersion__c> x: pidtoPvid.values())
        {
            PVIDs.addall(x);
        }
        
        
        
        // 
        // This is to create a set of Package Version so that we show scan result 
        // 
        AllpackageVersions = new set<string>();
        PvidObject = new map<string,chimera__PackageVersion__c>(); //To initiate scans only for
        for (chimera__PackageVersion__c i : PVIDs)
        {
            AllpackageVersions.add(i.Name);
            PvidObject.put(i.Name,i);
        }
            
       /*     
        for(chimera__PackageVersion__c x:Packages)
        {
            Allpackages.add(x.Name);
            string Org15char =x.chimera__DevelopmentOrganizationId__c.substring(0, 15);
            if(Package_collection.get(Org15char)!= Null)
            {
            
            //To ensure that only the latest version of package for the org is displayed
            if((Package_collection.get(Org15char)).LastModifiedDate <= x.LastModifiedDate)
                Package_collection.put(Org15char,x);
            }
            else
                Package_collection.put(Org15char,x);
        } 
        
       
        system.debug(Package_collection);*/
       
    }
    
    
    public List<SelectOption> getScanProfiles() 
    {
        List<SelectOption> options = new List<SelectOption>();
        SelectOption option = new SelectOption('PortalAll','Security and Quality Rules');
        options.add(option);
        option = new SelectOption('PortalSecurity','Security Rules');
        options.add(option);
        option = new SelectOption('PortalQuality','Quality Rules');
        options.add(option);
        return options;
    }
    public PageReference SubmitScan() {
        
        string PackageVersion_id = ScanPVID;
        Integer Credits;
        chimera__PackageVersion__c FoundPackage =PvidObject.get(PackageVersion_id);
  
        if(FoundPackage!=null)
        {
           Credits=Integer.valueof(FoundPackage.chimera__Credits__c);
        }
        if(FoundPackage==null)
            {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'You should select a Package Version ID to initiate a scan'));
                return null;
            }
        if(Credits<1)
            {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'You do not have enough credits to perform this scan'));
                return null;
            }
        
        Zip_Upload__c cs=new Zip_Upload__c();
        cs.Friendly_Name__c	 ='CloudScan';
        //cs.X62OrgUsername__c= UserInfo.getUserEmail();
        cs.PackageVersionId__c	 = FoundPackage.Name;
        //cs.Username__c=PackageVersion_id;
        //cs.Account_Type__c='SecurityReview';
        cs.Scan_Type__c=Scanprofile;
        //cs.Contributing_Org__c=FoundCO.id;
        cs.workState__c = 'New';
        cs.QueueType__c ='PartnerPortal';
        insert cs;
        //Update of package credits
        FoundPackage.chimera__Credits__c--;
        update FoundPackage;
        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.CONFIRM,'Scan has been submitted! Job ID: '+cs.id));
        //W-6165132 fix
        //init();
        //
        //
        PageReference pageRef = new PageReference(ApexPages.currentPage().getUrl());
		pageRef.setRedirect(true);
		return pageRef;
        //return null;
    }
    public void GetAccountDetails()
    {
        Id userId = UserInfo.getUserId();
        List<User> users = [select AccountId, Account.Total_Popcrab_Portal_Credits__c from User where Id=:userId];
        if(users[0].accountid==null)
        {
            AccountId='001Q000000uuiXV';//001Q000000uuiXV
            RemainingCredits=45;
        }
        else
        {
            AccountId=users[0].accountid;
           // RemainingCredits=Integer.Valueof(users[0].Account.Total_Popcrab_Portal_Credits__c);
        }
    }
    public PageReference Search() {
        ScansFound=false;
        map<string,CloudScanWrapper> TempScan=new map<string,CloudScanWrapper>();
        for(string str:csw.keyset())
        {
            if(csw.get(str).CodeScanId==searchterm ||  csw.get(str).OrgID==searchterm || csw.get(str).PVID==searchterm)
                TempScan.put(str,csw.get(str));
        }
        if(searchterm=='')
             TempScan=csw.clone();
        Scans=SortScans(TempScan);
        if(Scans.size()>0)
            ScansFound=true;
        return null;
    }
    public PageReference reset() {
        init();
        searchterm='';
        return null;
    }
    public static string genCSRF(string attid)
    {
        return EncodingUtil.base64Encode(Crypto.generateMac('hmacSHA512', Blob.valueOf(Userinfo.getSessionId()), Blob.valueOf(salt+attid)));
    }
    
    
    //converts list to string; API likes string of object Ids
    public string tostring(List<string> contents){
        
        string newStr= '\'' + String.join(contents,'\',\'') + '\'';
        return newStr;

    }
    
}
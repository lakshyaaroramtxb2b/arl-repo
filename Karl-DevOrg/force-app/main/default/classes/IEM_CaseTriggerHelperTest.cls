@isTest
public class IEM_CaseTriggerHelperTest {
    
    @testSetup
    public static void testSetup(){
        User admin = IEM_TestDataFactory.createUser(); 
        
        List<User> portalUser = new List<User>();        
        System.runAs(admin){
            //Create Accounts
            List<Account> accounts = new List<Account>();
            accounts= IEM_TestDataFactory.createAccounts(1,true);
            //Create Contacts
            List<Contact> contacts = new List<Contact>();
            contacts= IEM_TestDataFactory.createContacts(4,accounts[0].id,false);
            contacts[0].LastName = 'Test user M 1';
            contacts[0].Employee_ID__c = 'E-208283764';
            contacts[0].title = 'Director';
            contacts[1].LastName = 'Test user M 2';
            contacts[1].Employee_ID__c = 'E-208283765';
            contacts[1].title = 'Senior Director';
            contacts[2].LastName = 'Test user M 3';
            contacts[2].Employee_ID__c = 'E-208283766';
            contacts[2].title = 'VP';
            
            contacts[3].LastName = 'Issue Owner';
            contacts[3].Employee_ID__c = 'E-208283767';
            contacts[3].title = 'VP';
            contacts[3].Management_Chain_Level_03__c = 'Test user M 1 (E-208283764)';
            contacts[3].Management_Chain_Level_02__c = 'Test user M 2 (E-208283765)';
            contacts[3].Management_Chain_Level_01__c = 'Test user M 3 (E-208283766)';
            contacts[0].recordTypeid = IEM_Constants.IEM_CONTACTRECORDTYPEID;
            contacts[1].recordTypeid = IEM_Constants.IEM_CONTACTRECORDTYPEID;
            contacts[2].recordTypeid = IEM_Constants.IEM_CONTACTRECORDTYPEID;
            contacts[3].recordTypeid = IEM_Constants.IEM_CONTACTRECORDTYPEID;
            contacts[0].Org62_User_ID__c = '2F01I4D0000009R4yq';
            contacts[1].Org62_User_ID__c = '2F01I4D0000009R5yq';
            contacts[2].Org62_User_ID__c = '2F01I4D0000009R6yq';
            contacts[3].Org62_User_ID__c = '2F01I4D0000009R7yq';
            insert contacts;
            //Create poratl User
            portalUser.addAll(IEM_TestDataFactory.createPortalUserList(contacts,true));                       
        }
        
    }
    @isTest
    static void createCaseTest() {
		Map<String,Map<String,String>> dependenciesMap = new Map<String,Map<String,String>>();
        Profile profile = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User userRec = new User(Alias = 'standt', Email='standarduser'+System.now().millisecond()+'@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = profile.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='standarduser'+System.now().millisecond()+'@testorg.com');
        
        
        //Create Accounts
        List<Account> accounts = new List<Account>();
        accounts= IEM_TestDataFactory.createAccounts(1,true);
        //Create Contacts
        List<Contact> contacts = new List<Contact>();
        contacts= IEM_TestDataFactory.createContacts(2,accounts[0].id,false);
        Contacts[0].LastName = 'Issue_Owner__c';
        Contacts[1].LastName = 'Action_Plan_Owner__c';
        insert contacts;
        
        
        test.startTest();
        System.runAs(userRec) {   
            //Create Case    
        	List<Case> caseList = IEM_TestDataFactory.createCaseRecords(1,false);
            caseList[0].Action_Plan_Owner__c = contacts[0].id;
            caseList[0].Issue_Owner__c = contacts[1].id;
            insert CaseList;
        	List<Milestone__c> milestoneList = IEM_TestDataFactory.createMilestoneRecords(5,caseList[0].id,true);
            for(Milestone__c milestoneRec : [SELECT id,Case_Action_Plan_Owner__c,Case_Issue_Owner__c 
                                             FROM Milestone__c 
                                             WHERE id IN:milestoneList]){
                system.assertEquals(milestoneRec.Case_Action_Plan_Owner__c,caseList[0].Action_Plan_Owner__c);
                system.assertEquals(milestoneRec.Case_Issue_Owner__c,caseList[0].Issue_Owner__c);
            }
        }
        test.stopTest();
    }
    @isTest
	static void caseCreateUpdateTest() {
        List<user> portalUser = [SELECT id,contactID from User where Username LIKE '%testPortalIEMHVCPUser%'];
		Map<String,Map<String,String>> dependenciesMap = new Map<String,Map<String,String>>();        
        
        //Create Accounts
        List<Account> accounts = new List<Account>();
        accounts= IEM_TestDataFactory.createAccountsESA(1,true);
        //Create Contacts
        List<Contact> contacts = new List<Contact>();
        contacts= IEM_TestDataFactory.createContacts(2,accounts[0].id,false);
        Contacts[0].LastName = 'Issue_Owner__c';
        Contacts[1].LastName = 'Action_Plan_Owner__c';
        insert contacts;
        
        //Create Case         
        List<Case> caseList = IEM_TestDataFactory.createIntakeIssueCase(5,contacts,false);        
            caseList[0].Requestor__c = portalUser[0].contactID;
            caseList[1].Requestor__c = portalUser[0].contactID;
            caseList[2].Requestor__c = portalUser[0].contactID;
            caseList[3].Requestor__c = portalUser[0].contactID;
            caseList[4].Requestor__c = portalUser[0].contactID;
        System.debug('^^CaseList^^'+CaseList);
        insert CaseList;
        
        test.startTest();
            caseList[0].Status = IEM_Constants.IEM_CASE_STATUS_INVESTIGATING;
            caseList[1].Status = IEM_Constants.IEM_CASE_STATUS_VALIDATING_RESOLUTION;
            caseList[2].Status = IEM_Constants.IEM_CASE_STATUS_CLOSURE;
        	caseList[2].Closed_Reason__c = 'Invalid';
            caseList[3].Status = IEM_Constants.IEM_CASE_STATUS_MONITORING;
            caseList[4].Status = IEM_Constants.IEM_CASE_STATUS_INTERNAL_REVIEW;
            update caseList;
        
            caseList[4].Status = IEM_Constants.IEM_CASE_STATUS_MONITORING;
        	List<Milestone__c> milestoneList = IEM_TestDataFactory.createMilestoneRecords(5,caseList[0].id,true);
            caseList[0].Security_Decision__c = IEM_Constants.IEM_SECURITY_DECISION_APPROVED;
            caseList[0].Business_Decision__c = IEM_Constants.IEM_BUSINESS_DECISION_REJECTED;
            caseList[0].Action_Plan_Status__c = IEM_Constants.IEM_CASE_STATUS_SUBMITTED;
            caseList[0].Root_Cause__c  = 'IEM Root Cause';
            caseList[0].Estimated_Remediation_Date__c   = Date.today().addDays(5);
            caseList[0].Action_Plan__c   = 'IEM Action Plan';
            caseList[0].Definition_of_Done__c   = 'IEM Definition_of_Done__c ';
            caseList[0].Validity__c = 'Valid';    
            caseList[0].Requestor__c = portalUser[1].contactID;
        	caseList[0].Activity_Status__c  = 'Active';
        
        	
            caseList[1].Threats_associated_with_Issue__c = 'IEM Action Plan';
            caseList[1].Assessor_s_Comments__c  = 'IEM Action Plan';
            caseList[1].Assessor_s_Recommendations__c  = 'IEM Action Plan';
            caseList[1].Risk_associated_with_Issue__c = 'IEM Action Plan';
            caseList[1].Vulnerabilities_associated_with_Issue__c = 'IEM Action Plan';
            caseList[1].Highest_Data_Classification__c = 'Public';
            caseList[1].Likelihood__c = 'Frequent';
            caseList[1].BIA_Rating__c = 'High';
            caseList[1].Severity_Assessment_Status__c = IEM_Constants.IEM_COMPLETED;
            caseList[1].Validity__c = 'Valid';
            caseList[1].Activity_Status__c  = 'Active';
        
        
            caseList[1].Security_Assurance_SME_User__c = userinfo.getUserId();
            caseList[1].Severity_Assessor_User__c = userinfo.getUserId();
        
            //caseList[2].Status = IEM_Constants.IEM_CASE_STATUS_SECURITY_DECISION;
            caseList[2].Status = IEM_Constants.IEM_CASE_STATUS_ACTION_PLAN;
        	caseList[2].Issue_Identified_By__c = IEM_Constants.IEM_IIB_INFRASTRUCTURE_SECURITY;
        	//CaseList[2].Business_Justification__c = 'Start There is a jutification needed and I am too lazy to provided that, Sorry just kidding,Just Testing End';
            update caseList;
        	system.assert(True,[SELECT id,Business_Reviewer__c from Case where id in : caseList]!=null);
        test.stopTest();
    }
    @isTest
	static void caseApprovalAlgoTest() {
        List<user> portalUser = [SELECT id,contactID from User where Username LIKE '%testPortalIEMHVCPUser%' AND contact.LastName= 'Issue Owner'];
		Map<String,Map<String,String>> dependenciesMap = new Map<String,Map<String,String>>();        
        
        //Create Accounts
        List<Account> accounts = new List<Account>();
        accounts= IEM_TestDataFactory.createAccounts(1,true);
        //Create Contacts
        List<Contact> contacts = new List<Contact>();
        contacts= IEM_TestDataFactory.createContacts(2,accounts[0].id,false);
        Contacts[0].LastName = 'Issue_Owner__c';
        Contacts[1].LastName = 'Action_Plan_Owner__c';
        insert contacts;
        
        //Create Case         
        List<Case> caseList = IEM_TestDataFactory.createIntakeIssueCase(2,contacts,false); 
        insert CaseList;
        
        test.startTest();
        	caseList[0].Issue_Owner__c = portalUser[0].ContactId;
        	caseList[1].Issue_Owner__c = portalUser[0].ContactId;
        	caseList[1].Threats_associated_with_Issue__c = 'IEM Action Plan';
            caseList[1].Assessor_s_Comments__c  = 'IEM Action Plan';
            caseList[1].Assessor_s_Recommendations__c  = 'IEM Action Plan';
            caseList[1].Risk_associated_with_Issue__c = 'IEM Action Plan';
            caseList[1].Vulnerabilities_associated_with_Issue__c = 'IEM Action Plan';
            caseList[1].Highest_Data_Classification__c = 'Public';
            caseList[1].Likelihood__c = 'Frequent';
            caseList[1].BIA_Rating__c = 'High';
            caseList[1].Severity_Assessment_Status__c = IEM_Constants.IEM_COMPLETED;
            caseList[1].Validity__c = 'Valid';
            update caseList;
        system.assert(True,[SELECT id,Business_Reviewer__c from Case where id in : caseList]!=null);
        test.stopTest();
    }
    @isTest
    static void setResidualRiskTest() {
        List<user> portalUser = [SELECT id,contactID from User where Username LIKE '%testPortalIEMHVCPUser%'];
        Map<String,Map<String,String>> dependenciesMap = new Map<String,Map<String,String>>();        
        
        //Create Accounts
        List<Account> accounts = new List<Account>();
        accounts= IEM_TestDataFactory.createAccounts(1,true);
        //Create Contacts
        List<Contact> contacts = new List<Contact>();
        contacts= IEM_TestDataFactory.createContacts(2,accounts[0].id,false);
        Contacts[0].LastName = 'Issue_Owner__c';
        Contacts[1].LastName = 'Action_Plan_Owner__c';
        insert contacts;
        
        //Create Case         
        List<Case> caseList = new List<Case>();        
        for(integer i=0; i<5; i++ ){
            Case caseRec = new Case(Subject ='Intake Issue Number '+i, Status='Submitted', 
                                    recordtypeId= IEM_Constants.IEM_CASERECORDTYPEID); 
            caseRec.Description = 'Intake Issue Number '+i;         
            
            caseList.add(caseRec);
        }     
        insert caseList;
        test.startTest(); 
        List<String> BIARating = new List<String>{'Critical','High','Medium','Low','Minimal'};
        List<String> Likelyhood = new List<String>{'Likely','Possible','Unlikely','Frequent','Rare'};
        List<String> operationalEffectiveness = new List<String>{
            Label.IEM_Optimized_Label,
            Label.IEM_Effective_Label,
            Label.IEM_Adequate_Label,
            Label.IEM_Inadequate_Label,
            Label.IEM_Inadequate_Label};
                integer i = 0;
                for(String bia : BIARating){
                            caseList[i].BIA_Rating__c = bia;
                            caseList[i].Likelihood__c  = 'Likely';
                            caseList[i].Operational_Effectiveness__c  = Label.IEM_Optimized_Label;
                    		
                            i++;
                }
            
            update CaseList;
        for(Case caseRec : [SELECT Id, Residual_Severity__c FROM Case WHERE id in: caseList]){
            system.debug('??' + caseRec);
        }
        test.stopTest();
    }
    @isTest
	static void createExternalGusRecordsTest() {
        List<user> portalUser = [SELECT id,contactID from User where Username LIKE '%testPortalIEMHVCPUser%'];
		Map<String,Map<String,String>> dependenciesMap = new Map<String,Map<String,String>>();        
        
        //Create Accounts
        List<Account> accounts = new List<Account>();
        accounts= IEM_TestDataFactory.createAccounts(1,true);
        //Create Contacts
        List<Contact> contacts = new List<Contact>();
        contacts= IEM_TestDataFactory.createContacts(2,accounts[0].id,false);
        Contacts[0].LastName = 'Issue_Owner__c';
        Contacts[1].LastName = 'Action_Plan_Owner__c';
        insert contacts;
        
        //Create Case         
        List<Case> caseList = IEM_TestDataFactory.createIntakeIssueCase(2,contacts,false);        
            caseList[0].Requestor__c = portalUser[0].contactID;
            caseList[0].Issue_Owner__c = portalUser[0].contactID;
            caseList[1].Requestor__c = portalUser[0].contactID;
            caseList[1].Issue_Owner__c = portalUser[0].contactID;
            caseList[0].Action_Plan_Owner__c = portalUser[0].contactID;
            caseList[1].Action_Plan_Owner__c = portalUser[0].contactID;
       		caseList[1].Reported_Case_Owner__c = userinfo.getUserId();
        insert CaseList;
        
        test.startTest();
        	List<Milestone__c> milestoneList = IEM_TestDataFactory.createMilestoneRecords(5,caseList[0].id,false);
        	milestoneList.addAll(IEM_TestDataFactory.createMilestoneRecords(5,caseList[1].id,false));
        	insert milestoneList;
            caseList[0].Status = System.Label.IEM_Case_Status_Risk_Assessment_Label;
            caseList[0].Security_Assurance_SME_User__c = userinfo.getUserId();
            caseList[0].Severity_Assessor_User__c = userinfo.getUserId();
        
            caseList[1].Status = System.Label.PRT_Case_Status_Monitoring_Label;
            caseList[1].Security_Assurance_SME_User__c = userinfo.getUserId();
            caseList[1].Severity_Assessor_User__c = userinfo.getUserId();
       		caseList[1].Reported_Case_Owner__c = userinfo.getUserId();
        	update caseList;
        	
        	system.assert(True,[SELECT id,Business_Reviewer__c from Case where id in : caseList]!=null);
        test.stopTest();
    }
    
    @isTest
	static void createShareRecordsForInternalUsersTest() {
        UserRole userRole = [SELECT Id FROM UserRole LIMIT 1];       
        List<user> portalUser = [SELECT id,contactID from User where Username LIKE '%testPortalIEMHVCPUser%'];
		Map<String,Map<String,String>> dependenciesMap = new Map<String,Map<String,String>>();        
        
        //Create Accounts
        List<Account> accounts = new List<Account>();
        accounts= IEM_TestDataFactory.createAccounts(1,true);
        //Create Contacts
        List<Contact> contacts = new List<Contact>();
        contacts= IEM_TestDataFactory.createContacts(2,accounts[0].id,false);
        Contacts[0].LastName = 'Issue_Owner__c';
        Contacts[1].LastName = 'Action_Plan_Owner__c';
        insert contacts;
        
        //Create Case         
        List<Case> caseList = IEM_TestDataFactory.createIntakeIssueCase(2,contacts,false);        
            caseList[0].Requestor__c = portalUser[0].contactID;
            caseList[0].Issue_Owner__c = portalUser[0].contactID;
            caseList[1].Requestor__c = portalUser[0].contactID;
            caseList[1].Issue_Owner__c = portalUser[0].contactID;
            caseList[0].Action_Plan_Owner__c = portalUser[0].contactID;
            caseList[1].Action_Plan_Owner__c = portalUser[0].contactID;
            caseList[0].Severity_Assessor_User__c = userinfo.getUserId();
       		caseList[1].Reported_Case_Owner__c = userinfo.getUserId();
       		caseList[1].Reported_Case_Owner__c = userinfo.getUserId();
        insert CaseList;
        
        test.startTest();
        	List<Milestone__c> milestoneList = IEM_TestDataFactory.createMilestoneRecords(5,caseList[0].id,false);
        	milestoneList.addAll(IEM_TestDataFactory.createMilestoneRecords(5,caseList[1].id,false));
        	insert milestoneList;
            caseList[0].Status = System.Label.IEM_Case_Status_Risk_Assessment_Label;
            caseList[0].Security_Assurance_SME_User__c = userinfo.getUserId();
            caseList[0].Severity_Assessor_User__c = portalUser[0].id;
        
            caseList[1].Status = System.Label.PRT_Case_Status_Monitoring_Label;
            caseList[1].Security_Assurance_SME_User__c = userinfo.getUserId();
            caseList[1].Severity_Assessor_User__c = portalUser[0].id;
       		caseList[1].Reported_Case_Owner__c = userinfo.getUserId();
        	update caseList;
        	
        	system.assert(True,[SELECT id,Business_Reviewer__c from Case where id in : caseList]!=null);
        test.stopTest();
    }
    
     @isTest
	static void updateSBERecordsTest() {
        UserRole userRole = [SELECT Id FROM UserRole LIMIT 1];       
        List<user> portalUser = [SELECT id,contactID from User where Username LIKE '%testPortalIEMHVCPUser%'];
		Map<String,Map<String,String>> dependenciesMap = new Map<String,Map<String,String>>();        
        
        //Create Accounts
        List<Account> accounts = new List<Account>();
        accounts= IEM_TestDataFactory.createAccounts(1,true);
        //Create Contacts
        List<Contact> contacts = new List<Contact>();
        contacts= IEM_TestDataFactory.createContacts(2,accounts[0].id,true);
        
        //Create Case         
        List<Case> caseList = IEM_TestDataFactory.createIntakeIssueCase(2,contacts,false);        
            caseList[0].Requestor__c = portalUser[0].contactID;
            caseList[0].Issue_Owner__c = portalUser[0].contactID;
            caseList[0].Action_Plan_Owner__c = portalUser[0].contactID;
            caseList[0].Severity_Assessor_User__c = userinfo.getUserId();
            caseList[0].SLA_Extension_Id__c = 'EX101';
       		insert CaseList;
            
        TM_Security_Work_SLA_Extension_c__x workSLA = new TM_Security_Work_SLA_Extension_c__x();
        //workSLA.TM_Work_Subject_c__c = 'Test Subject';
        workSLA.ExternalId= 'EX101';
        workSLA.TM_Definition_of_Done_c__c= 'Completed';
        workSLA.Describe_Technical_Work_to_Remediate__c= 'Nothing';
        workSLA.Due_Date_Justification__c= 'No';
        workSLA.Technical_Description_of_Issue__c= 'No';
        workSLA.TM_SLA_Extension_Status_c__c = IEM_Constants.SBE_STATUS_DENIED;
        //workSLA.TM_Work_Cloud_c__c = 'Very High (EVP)';
        IEM_CaseTriggerHelper.mockedRequests.add(workSLA);
        
        test.startTest();
        	
            caseList[0].Status = IEM_Constants.IEM_CASE_STATUS_INTERNAL_REVIEW;
        	update caseList;
        	
        test.stopTest();
    }
    
     @isTest
	static void syncApprovalToSBETest() {
        UserRole userRole = [SELECT Id FROM UserRole LIMIT 1];       
        List<user> portalUser = [SELECT id,contactID from User where Username LIKE '%testPortalIEMHVCPUser%'];
		Map<String,Map<String,String>> dependenciesMap = new Map<String,Map<String,String>>();        
        
        //Create Accounts
        List<Account> accounts = new List<Account>();
        accounts= IEM_TestDataFactory.createAccounts(1,true);
        //Create Contacts
        List<Contact> contacts = new List<Contact>();
        contacts= IEM_TestDataFactory.createContacts(2,accounts[0].id,true);
        
        //Create Case         
        List<Case> caseList = IEM_TestDataFactory.createIntakeIssueCase(2,contacts,false);        
        caseList[0].Requestor__c = portalUser[0].contactID;
        caseList[0].Issue_Owner__c = portalUser[0].contactID;
        caseList[0].Action_Plan_Owner__c = portalUser[0].contactID;
        caseList[0].Severity_Assessor_User__c = userinfo.getUserId();
        caseList[0].SLA_Extension_Id__c = 'EX101';
        caseList[0].Business_Decision__c = IEM_Constants.SBE_STATUS_EXTENSION_APPROVED;
         caseList[0].Business_Approver__c  = portalUser[0].Id;
        insert CaseList;
        test.startTest();
        	
            caseList[0].Status = IEM_Constants.IEM_CASE_STATUS_SECURITY_DECISION;
            caseList[0].Action_Plan_Status__c = IEM_Constants.IEM_IN_PROGRESS;
            caseList[0].Security_Reviewer__c = userinfo.getUserId();
           
        
        	update caseList;
        	
        test.stopTest();
    }
}
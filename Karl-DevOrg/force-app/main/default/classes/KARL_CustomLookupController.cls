/**
 * @author Lakshya Arora
 * @email lakshya.arora@mtxb2b.com
 * @description Handle karl_custom_lookup LWC component operations
 */
public with sharing class KARL_CustomLookupController {
    /**
    * @author Lakshya Arora
    * @email lakshya.arora@mtxb2b.com
    * @description get at max 20 records  of the object filter by field value
    * param1 objectName -> name of object
    * param2 filterField -> Field use for query filter -> Text field
    * param3 searchString -> Search string value which will use in filterfield -> Text input
    * param4 secondaryLabel -> Secondary field in query 
    * @return List of Wrapper - RecordsData
    */
    @AuraEnabled
    public static List<RecordsData> getRecordsAsPerObjectAndFieldList(String objectName, String filterField, String searchString,String secondaryLabel) {
        List<RecordsData> recordsDataList = new List<RecordsData>();
        objectName = String.escapeSingleQuotes(objectName.trim());
        filterField = String.escapeSingleQuotes(filterField.trim());
        searchString = String.escapeSingleQuotes(searchString.trim());
        secondaryLabel = String.escapeSingleQuotes(secondaryLabel.trim());
        String query = 'SELECT Id,' + filterField;
        if(String.isNotBlank(secondaryLabel)){
            query  += ','+secondaryLabel;
        }
        query+=' FROM '+objectName;
        query += ' WHERE '+filterField+
                 ' LIKE ' + '\'' + searchString + '%\' ';
        query += ' WITH SECURITY_ENFORCED LIMIT 20';       
        for(SObject obj : Database.query(query)) {
            String secondaryFieldValue = '';
            if(String.isNotBlank(secondaryLabel)){
                secondaryFieldValue = (String)obj.get(secondaryLabel);
            }
            recordsDataList.add( new RecordsData((String)obj.get(filterField), (String)obj.get('id'),secondaryFieldValue));
        }
        return recordsDataList;
    }
 
    public class RecordsData {
        @AuraEnabled public String label;
        @AuraEnabled public String value;
        @AuraEnabled public String secondarylabel;
        public RecordsData(String label, String value,String secondarylabel) {
            this.label = label;
            this.value = value;
            this.secondarylabel = secondarylabel;
        }
    }
}
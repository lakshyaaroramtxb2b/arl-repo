public class CDI_Gus_UDDSyncEntity implements Database.Batchable<sObject>,Database.Stateful {
    DateTime starttime=DateTime.now();
    Integer rowsPassed=0;
    Integer rowsFailed=0;
    String errors='';
    String gusTeamId=null;
    Datetime lastExecution;
	Id datastoreId;  
    public Database.QueryLocator start(Database.BatchableContext bc) {
        String query;
        List<CDI_Batch_Import__c>  batchimports=[Select name,Start_Time__c from CDI_Batch_Import__c Where Object_Type__c='GUS UDD Entities' ORDER BY End_Time__c DESC limit 1];
        if (batchimports!=null && batchimports.size()>0){
            lastExecution = batchimports[0].Start_Time__c; 
            System.debug('Last Execution of UDD Entities on:'+lastExecution);
        }
        else {
            lastExecution = null; 
            System.debug('Last Execution of UDD Entities not found');
        }
        if(lastExecution!=null && batchimports.size()>0){
            query='select name__c,externalid,Description_c__c,TeamOwner_c__r.name__c,ProductOwner_c__c,IsDeleted__c,InternalDescription_c__c from MDS_Entity_c__x where EntityType_c__c<>'+'\'Canonical\''+' AND (Status_c__c='+'\'Approved\''+' OR Status_c__c='+'\'Existing\')'+' AND Type_c__c='+'\'Core\''+' AND LastModifiedDate__c >'+ lastExecution.formatGmt('yyyy-MM-dd\'T\'HH:mm:ss.SSSZ')+' ORDER BY Name__c,ExternalID DESC';
        }
        else{
            query='select name__c,externalid,Description_c__c,TeamOwner_c__r.name__c,ProductOwner_c__c,IsDeleted__c,InternalDescription_c__c from MDS_Entity_c__x where EntityType_c__c<>'+'\'Canonical\''+' AND (Status_c__c='+'\'Approved\''+' OR Status_c__c='+'\'Existing\')'+' AND Type_c__c='+'\'Core\''+ ' ORDER BY Name__c,ExternalID DESC';
        }
                 //Get Core DB DataStore id
    
    String dataStore=[select id from CDI_Data_Store__c where Name='Core DB' LIMIT 1].id;
    if (dataStore==null){
            CDI_Data_Store__c newDS=new CDI_Data_Store__c();
            newDS.Name='Core DB';
            newDS.Status__c='Active';
            insert newDs;
            System.debug('Datastore created');
            //parentDataAsset.Data_Store__c=newDs.Id;
            datastoreId=Id.valueOf(newDs.Id);
        }
        else{
            String dsId=string.valueOf(dataStore);
        	datastoreId=Id.valueOf(dsId);
        }
        return Database.getQueryLocator(query);
    }

    public void execute(Database.BatchableContext bc, List<MDS_Entity_c__x> entityList){
        // Extract List of Entities
        
        Set <String>  gusUsers=new Set<String>();
   		for (MDS_Entity_c__x entity: entityList){
          gusUsers.add(entity.ProductOwner_c__c);
        }
        
 
        Set<CDI_Data_Asset__c> dataAssetList=new Set<CDI_Data_Asset__c>();
        List <CDI_Data_Asset_Contact__c> dataAssetContactList=new List<CDI_Data_Asset_Contact__c>();  
        Set<String> externalIds=new Set<String>();
        
        //Fetch List of CDI GUS Team
        List<CDI_Gus_Team__c> gusTeamList=[select id,name from CDI_GUS_Team__c];
		Map <String, String> gusTeamInfo=new Map <String,String>();
		for (CDI_Gus_Team__c gusTeam:gusTeamList){
            gusTeamInfo.put(gusTeam.name,gusTeam.id);
        }
        
    	List <GusUser__x> gusEmailList=[select name__c,email__c from GusUser__x where name__c in :gusUsers ];
    	Map<String, String> emailList=new Map<String,String>();
        for(GusUser__x email: gusEmailList){
            emailList.put(email.name__c, email.email__c);  
        }
        
        // Add Entity List to Data Asset List.
       	for (MDS_Entity_c__x entity : entityList){
            CDI_Data_Asset__c parentDataAsset=new CDI_Data_Asset__c();
            //System.debug('Before ExternalId if Statement:'+parentDataAsset);
            if (!externalIds.contains('Core DB/'+entity.Name__c)){
                parentDataAsset.Name=entity.Name__c;
        		String Name = entity.Name__c;
        		parentDataAsset.Data_Asset_Description__c=entity.Description_c__c;
        		parentDataAsset.Source_Data_Asset_Code__c=entity.ExternalId;
        		parentDataAsset.Source_System_Name__c='UDD';
        		parentDataAsset.Data_Asset_Type__c='Table';
        		parentDataAsset.Status__c='Active';
        		parentDataAsset.IsDeleted__c=entity.IsDeleted__c;
            	parentDataAsset.Comments__c=entity.InternalDescription_c__c;
            	//System.debug('Team Owner:'+entity.TeamOwner_c__r.name__c);
                if (entity.TeamOwner_c__r.name__c!=null){
                    if(gusTeamInfo.ContainsKey(entity.TeamOwner_c__r.name__c)){
                            //System.debug('Gus Team Id'+gusTeamId);
                    		parentDataAsset.Gus_Team__c=gusTeamInfo.get(entity.TeamOwner_c__r.name__c);
                    		//System.debug('Parent Data Asset Gus Team:'+parentDataAsset.GUS_Team__c);
    					}
              
            		}

				parentDataAsset.Data_Store__c=datastoreId;	
         		parentDataAsset.External_Id__c='Core DB/'+Name;
         		externalIds.add('Core DB/'+Name);
               	dataAssetList.add(parentDataAsset); 
                //System.debug('In ExternalId if Statement:'+parentDataAsset);
                CDI_Data_Asset_Contact__c dataAssetContact=new CDI_Data_Asset_Contact__c();
       			dataAssetContact.GUS_User__r=new CDI_GUS_User__c(External_Id__c=emailList.get(entity.ProductOwner_c__c));
        		dataAssetContact.Data_Asset__r=new CDI_Data_Asset__c(External_Id__c=parentDataAsset.External_Id__c);  
        		dataAssetContactList.add(dataAssetContact);
                //System.debug(dataAssetContact.Data_Asset__r);
        		//System.debug(dataAssetContact.GUS_User__r);
        		//System.debug(dataAssetContact);
            }

        }
        
        //Upsert records in CDI Data Asset
    	System.debug('Data Asset List before upsert:'+dataAssetList.size());
    	Schema.SObjectField externalId=CDI_Data_Asset__c.Fields.External_Id__c;
        List<CDI_Data_Asset__c> upsertDataAssetList=new List<CDI_Data_Asset__c>();
		upsertDataAssetList.addAll(dataAssetList);
    	List<Database.UpsertResult> upsertAssetResult=Database.upsert( upsertDataAssetList,externalId,false);

    	Database.upsert(dataAssetContactList,false);
    	System.debug('Number of rows processed:'+upsertAssetResult.size());
    	for(Integer index = 0; index < upsertAssetResult.size(); index++) {
    	if(upsertAssetResult[index].isSuccess()) {
        	rowsPassed++; 
        }
            else {
        	rowsFailed++;
            Database.Error error=upsertAssetResult.get(index).getErrors().get(0);
            errors=error.getMessage();
            }
        }
        System.debug('Number of rows passed:'+rowsPassed);
   		System.debug('Number of rows failed:'+rowsFailed);
	
		}	
    public void finish(Database.BatchableContext bc){
        // Crete Batch Import Records. 
   		CDI_Batch_Import__c batchImport = new CDI_Batch_Import__c();
   		batchImport.Start_Time__c=starttime;
   		batchImport.End_Time__c=DateTime.now();
   		batchImport.Object_Type__c='GUS UDD Entities';
        batchImport.Errors__c=errors;
   		batchImport.Rows_Succeeded__c=rowsPassed;
   		batchImport.Rows_Failed__c=rowsFailed;
   		System.debug('Finish GUS UDD Entity Sync');
   		insert batchImport;
    }

}
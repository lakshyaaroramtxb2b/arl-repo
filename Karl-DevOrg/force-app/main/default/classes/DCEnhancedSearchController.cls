@RestResource(urlMapping='/JSCaseGraph')
global with sharing class DCEnhancedSearchController {

    static Integer searchDepth = 0;
    static Integer kMaxSearchDepth = 90;
    
    static void JSCaseGraphController()
    {
    }
    
	@HttpGet
    global static String doGet() {
        return 'Worked';
    }
    
	@RemoteAction
	global static String getCase(String sTerm) {
        
        JSGraph2 graph = new JSGraph2();
        
        try
        {
            //get seed case
            /*Case c = [SELECT Id, CaseNumber, Detection_Logic__r.Name, Status, CreatedDate, Subject, Alert_Name__c, Alert_Source_IP__c, Destination_IP_Address__c 
                      FROM Case 
                      WHERE CaseNumber = :caseID LIMIT 1];*/
            
            List<List <sObject>> searchList = [FIND :sTerm IN ALL FIELDS
                   						RETURNING Detection_Security_Event__c(Id, DetectionSecurityEventCriteria__c, FieldMapRaw__c)];

			Detection_Security_Event__c[] searchResults = (Detection_Security_Event__c[])searchList[0];
            
            //DEBUG
            System.debug('SearchResults size ' + searchResults.size());
             
            //for(Integer i = 0; i < searchResults.size() || i < 5; i++) {
            //	 System.debug('searchResults ' + searchResults[i] );
            //}
            //####
            
            if(searchResults.size() < 1)
                return '-1';
            
            digestEvents(searchResults, graph);
            
            /*
            System.debug('Root ID :' + c.Id);
            System.debug('Root IPs ' + c.Alert_Source_IP__c + ', ' + c.Destination_IP_Address__c);
            
            Set<ID> processedCases = new Set<ID>();
            
            //seed case
            digestAlert(c, processedCases, graph, true);
            
            //done return JSON result
            return JSON.serializePretty(graph);*/
            
            return JSON.serializePretty(graph);
    	}
        catch (exception e)
    	{
        	return 'ERROR: ' + e.getMessage() + ',' + e.getStackTraceString();
    	}
	}
    
    //only create node if we dont yet have a node with that key
    /*public static JSEventNode createOrGetNode(Case c, JSEventNode.ENodeType nodeType, JSGraph graph)
    {
        String key = getCaseKey(c, nodeType);
        
        JSEventNode node = graph.getNodeByKey(key);
        
        if(node == null)
        {
            node = new JSEventNode(c, key, nodeType);
            
            graph.addNode(node);
        }
        else
        {
            node.addCaseRef(c);
        }
        
        return node;
    }*/
    
    public static JSEventNode createOrGetNode(Detection_Security_Event__c evt, JSEventNode.ENodeType nodeType, JSGraph2 graph)
    {
        String key = getEventKey(evt, nodeType);
        
        JSEventNode node = graph.getNodeByKey(key);
        
        //node.SetLabel('abc123');
        
        if(node == null)
        {
            node = new JSEventNode(evt, key, nodeType);
            
            graph.addNode(node);
        }
        else
        {
            node.addEventRef(evt);
        }
        
        return node;
    }
    
    public static void digestEvents(Detection_Security_Event__c[] evts, JSGraph2 graph)
    {
        /*
            JSEventNode n0 = createOrGetNode(evts[0], JSEventNode.ENodeType.SOURCE_ENDPOINT, graph);
            JSEventNode n1 = createOrGetNode(evts[0], JSEventNode.ENodeType.EVENT, graph);
            JSEventEdge e0 = graph.getEdge(n0, n1);
		*/
        
        for(Integer i = 0; i < evts.size() && i < 5; i++) {
        	JSEventNode n0 = createOrGetNode(evts[i], JSEventNode.ENodeType.EVENT, graph);
        	JSEventNode n1 = createOrGetNode(evts[i], JSEventNode.ENodeType.EVENT, graph);
        
       		JSEventEdge e0 = graph.getEdge(n0, n1);
        
            //e1 = graph.addEdge(n0, n1);
            //mapNodeRelationShips(n0, n1, n2, e0, e1);
        }
        
        //build graph from current alert
        
        /*
        JSEventNode n0 = createOrGetNode(c, JSEventNode.ENodeType.SOURCE_ENDPOINT, graph);
        JSEventNode n1 = createOrGetNode(c, JSEventNode.ENodeType.EVENT, graph);
        
        JSCaseEdge e0 = graph.getEdge(n0, n1);
        
        if(e0 == null)
        	e0 = graph.addEdge(n0, n1);
        else
            e0.addInstance();
        
        JSEventNode n2 = createOrGetNode(c, JSEventNode.ENodeType.DESTINATION_ENDPOINT, graph);
        
        JSCaseEdge e1 = graph.getEdge(n1, n2);
        
        if(e1 == null)
        	e1 = graph.addEdge(n1, n2);
		else
            e1.addInstance();
        
        mapNodeRelationShips(n0, n1, n2, e0, e1);
        
        //we want to show the path of the alert
        if(searchDepth == 1) //becouse we increment at start of function
        {
            e0.color = 'red';
            e1.color = 'red';
            
            n0.size *= 2;
            n1.size *= 2;
            n2.size *= 2;
        }
        
        //recusive
        if(recursive)
        {
            //We currently only search for matches with a SRC and DST that are not full so that we can split into 3 parts
            //should change so it look for events with at least one part not null
            
            //simular cases by event
            //need a way to prevent endless loop. keep track of caseIds processed and not reprocesses?
            List<Case> simCasesByEvent = [SELECT Id, CaseNumber, Status, CreatedDate, Detection_Logic__r.Name, Subject, Alert_Name__c, Alert_Source_IP__c, Destination_IP_Address__c 
                                          FROM Case 
                                          WHERE public static void digestAlert(Detection_Security_Event__c[] e, Set<ID> processedCases, JSGraph graph, Boolean recursive)
    {
        //governer limit protection
        searchDepth++;
        
        if(searchDepth > (kMaxSearchDepth))
        	return;
        //
        
        System.debug('digestAlert caseid = ' + c.Id);
        
        processedCases.add(c.Id);
        
        //build graph from current alert
        
        JSEventNode n0 = createOrGetNode(c, JSEventNode.ENodeType.SOURCE_ENDPOINT, graph);
        JSEventNode n1 = createOrGetNode(c, JSEventNode.ENodeType.EVENT, graph);
        
        JSCaseEdge e0 = graph.getEdge(n0, n1);
        
        if(e0 == null)
        	e0 = graph.addEdge(n0, n1);
        else
            e0.addInstance();
        
        JSEventNode n2 = createOrGetNode(c, JSEventNode.ENodeType.DESTINATION_ENDPOINT, graph);
        
        JSCaseEdge e1 = graph.getEdge(n1, n2);
        
        if(e1 == null)
        	e1 = graph.addEdge(n1, n2);
		else
            e1.addInstance();
        
        mapNodeRelationShips(n0, n1, n2, e0, e1);
        
        //we want to show the path of the alert
        if(searchDepth == 1) //becouse we increment at start of function
        {
            e0.color = 'red';
            e1.color = 'red';
            
            n0.size *= 2;
            n1.size *= 2;
            n2.size *= 2;
        }
        
        //recusive
        if(recursive)
        {
            //We currently only search for matches with a SRC and DST that are not full so that we can split into 3 parts
            //should change so it look for events with at least one part not null
            
            //simular cases by event
            //need a way to prevent endless loop. keep track of caseIds processed and not reprocesses?
            List<Case> simCasesByEvent = [SELECT Id, CaseNumber, Status, CreatedDate, Detection_Logic__r.Name, Subject, Alert_Name__c, Alert_Source_IP__c, Destination_IP_Address__c 
                                          FROM Case 
                                          WHERE // Alert_Name__c =: c.Alert_Name__c AND Remove this to get all alerts ?//
                                                ((Alert_Source_IP__c =: c.Alert_Source_IP__c AND Alert_Source_IP__c <> '') OR (Destination_IP_Address__c =: c.Destination_IP_Address__c AND Destination_IP_Address__c <> '') 
                                                 OR 
                                                 (Destination_IP_Address__c =: c.Alert_Source_IP__c AND Destination_IP_Address__c <> '') OR (Alert_Source_IP__c =: c.Destination_IP_Address__c AND Alert_Source_IP__c <> ''))
                                                AND CreatedDate = LAST_N_DAYS:30
                                         		AND Id NOT IN: processedCases];
            
            System.debug('ID :' + c.Id);
            System.debug('For IPs ' + c.Alert_Source_IP__c + ', ' + c.Destination_IP_Address__c);
            System.debug('Query result count = ' +  simCasesByEvent.size());
    
            for(Case ac : simCasesByEvent)
            {
                digestAlert(ac, processedCases, graph, true);
            }
        }
    }
                                                ((Alert_Source_IP__c =: c.Alert_Source_IP__c AND Alert_Source_IP__c <> '') OR (Destination_IP_Address__c =: c.Destination_IP_Address__c AND Destination_IP_Address__c <> '') 
                                                 OR 
                                                 (Destination_IP_Address__c =: c.Alert_Source_IP__c AND Destination_IP_Address__c <> '') OR (Alert_Source_IP__c =: c.Destination_IP_Address__c AND Alert_Source_IP__c <> ''))
                                                AND CreatedDate = LAST_N_DAYS:30
                                         		AND Id NOT IN: processedCases];
            
            System.debug('ID :' + c.Id);
            System.debug('For IPs ' + c.Alert_Source_IP__c + ', ' + c.Destination_IP_Address__c);
            System.debug('Query result count = ' +  simCasesByEvent.size());
    
            for(Case ac : simCasesByEvent)
            {
                digestAlert(ac, processedCases, graph, true);
            }
        }*/
    }
    
    public Static void mapNodeRelationShips(JSEventNode n0, JSEventNode n1, JSEventNode n2,
                                           	JSCaseEdge e0, 	JSCaseEdge e1)
    {
        n0.relatedNodeKeys.add(n1.key);
        n0.relatedNodeKeys.add(n2.key);
        n0.relatedEdgeKeys.add(e0.id);
        n0.relatedEdgeKeys.add(e1.id);
        
        n1.relatedNodeKeys.add(n0.key);
        n1.relatedNodeKeys.add(n2.key);
        n1.relatedEdgeKeys.add(e0.id);
        n1.relatedEdgeKeys.add(e1.id);
        
        n2.relatedNodeKeys.add(n0.key);
        n2.relatedNodeKeys.add(n1.key);
        n2.relatedEdgeKeys.add(e0.id);
        n2.relatedEdgeKeys.add(e1.id);
    }
    
    public static void increaseEdgeCount()
    {
		
    }
    
    private static String genNullKey(Case c, JSEventNode.ENodeType nodeType)
    {
        return c.CaseNumber + ':' + nodeType + ':::IS_A_NULL_KEY';
    }
    
    /*public static String getCaseKey(Case c, JSEventNode.ENodeType nodeType)
    {
        if(nodeType == JSEventNode.ENodeType.SOURCE_ENDPOINT)
        {
            //DESTINATION
            //ENDPOINT
            
            if(!String.isBlank(c.Alert_Source_IP__c) )
            	return c.Alert_Source_IP__c;
            else
                return genNullKey(c, nodeType);
        }
        else if(nodeType == JSEventNode.ENodeType.DESTINATION_ENDPOINT)
        {
            //SOURCE
            //ALERT OR INCIDENT
            
            if(!String.isBlank(c.Destination_IP_Address__c) )
            	return c.Destination_IP_Address__c;
            else
                return genNullKey(c, nodeType);
        }
        else if(nodeType == JSEventNode.ENodeType.EVENT)
        {
            //SOURCE
            //ALERT OR INCIDENT

            if(!String.isBlank(c.Alert_Name__c) )
            	return c.Alert_Name__c;
            else if (!String.isBlank(c.Subject) )
                return c.Subject;
            else
                return genNullKey(c, nodeType);
        }

        return null;
    }*/
    
    public static String getEventKey(Detection_Security_Event__c evt, JSEventNode.ENodeType nodeType)
    {
        return evt.Id;
    }
    
    public static String generateKeyForNullCase()
    {
    	Blob b = Crypto.GenerateAESKey(128);
		String h = EncodingUtil.ConvertTohex(b);
        
		return h.SubString(0,8)+ '-' + h.SubString(8,12) + '-' + h.SubString(12,16) + '-' + h.SubString(16,20) + '-' + h.substring(20);
    }
}
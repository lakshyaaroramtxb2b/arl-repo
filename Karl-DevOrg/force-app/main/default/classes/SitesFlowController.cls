/* 
    This is a Site controller.  Public info only please.
*/

public class SitesFlowController {
    public Flow__c flow {get;private set;}
    public List<NodeContainer> nc {get;private set;}
    public NodeContainer currentNode {get;private set;}
    
    public String questionAnswer {get;set;}
    private Map<String,Boolean> tfMap = new Map<String,Boolean>();
    private Map<String,String> strMap = new Map<String,String>();
    
    public List<SelectOption> flowChoices {get;private set;}
    public String flowChoice {get;set;}
    
    public SitesFlowController() {
        String fidstr = ApexPages.CurrentPage().getParameters().get('Id');
        Id fid = null;
        try {
            fid = (Id) fidstr;
        }
        catch (Exception e) {
        }
        Flow__c f = null;
        if (fid != null) {
            f = [SELECT Id,Public__c FROM Flow__c WHERE Id =: fid];
            if (f != null) {
                if (f.Public__c != True) {
                    f = null; // We only allow public Flow__c's in this controller since it's for Sites
                }
            }
        }
        setup(f);
    }
    public SitesFlowController(Flow__c flow) { // for testing
        setup(flow);
    }
    
    public void setup(Flow__c flow) {
        if (flow == null) {
            flowChoices = new List<SelectOption>();
            for (Flow__c fl : [SELECT Id,Name FROM Flow__c WHERE Public__c = True]) {
                flowChoices.add(new SelectOption(fl.Id,fl.Name));
            }
            return;
            
        }
        flow = [SELECT Name,StartNode__c,Id FROM Flow__c WHERE Id=: flow.Id];
        List<Node__c> nodes = [SELECT Id,Name,AppendTo__c,AppendVal__c,DecisionAndOr__c,DecisionRules__c,
                                      isYesNo__c,Next__c,NoNext__c,YesNext__c,SetFalse__c,SetTrue__c,
                                      Text__c,Type__c,URL__c FROM Node__c where Flow__c=:flow.Id];
        System.debug('Loaded '+Integer.valueOf(nodes.size())+' nodes');
        NodeContainer tmp;
        nc = new List<NodeContainer>();
        for (Node__c n : nodes) {
            tmp = new NodeContainer(n);
            nc.add(tmp);
            if (flow.StartNode__c == n.Id) {
                currentNode = tmp;
                System.debug('Flow "'+flow.Name+'" - loading start node "'+tmp.n.Name);
            }
        }
        if (currentNode == null) {
            System.debug('Flow "'+flow.Name+'" - start node (Id:'+flow.StartNode__c+') not found');
        }
        setupNode(currentNode);
    }
    
    public PageReference nodeChange() {
        System.debug('Changing node');
        NodeContainer next;
        Boolean userInteraction = False;
        while (!userInteraction) {
            if (currentNode.isQuestion()) {
                next = getContainerForId(currentNode.getQuestionNext(questionAnswer));
                setupNode(next);
            }
            else if (currentNode.isDecision()) {
                next = getContainerForId(currentNode.makeDecision(tfMap,strMap));
                setupNode(next);
            }
            else if (currentNode.isVariable()) {
                tfMap = currentNode.updateTfMap(tfMap);
                strMap = currentNode.updateStrMap(strMap);
                next = getContainerForId(currentNode.getVariableNext());
                setupNode(next);
            }
            if (next.isUserInteractive()) {
                    userInteraction = True;
            }
        }
        if (next.isUrl()) {
            return new PageReference(next.getUrl(tfMap,strMap));
        }
        return null; // Question node or other interactive node that staying on this page
            
    }
    
    private PageReference setupNode(NodeContainer c) {
        System.debug('Setting up node: '+c.n.Name);
        if (c.isUrl()) {
            return new PageReference(c.getUrl(tfMap,strMap));
        }
        else if (c.isQuestion()) {
            setupQuestionNode(c);
        }
        else if (c.isDecision()) {
            setupDecisionNode(c);
        }
        else if (c.isVariable()) {
            setupVariableNode(c);
        }
        return null;
    }
        
    private NodeContainer getContainerForId(Id nid) {
        for (NodeContainer b : nc) {
            if (b.n.Id == nid) {
                return b;
            }
        }
        throw new FlowException('Node not found: '+String.valueOf(nid));
    }
    private void setupQuestionNode(NodeContainer cur) {
        // nothing needed really
        currentNode = cur;
    }
    private void setupVariableNode(NodeContainer cur) {
        // nothing needed really
        currentNode = cur;
    }
    private void setupUrlNode(NodeContainer cur) {
        // nothing needed really
        currentNode = cur;
    }
    private void setupDecisionNode(NodeContainer cur) {
        // nothing needed really
        currentNode = cur;
    }

    public List<SelectOption> getAnswerOptions() {
        return currentNode.getAnswerOptions();
    }
    public String getQuestion() {
        if (currentNode.isQuestion()) {
            return currentNode.getQuestion();
        }
        return null;
    }
    public PageReference startFlow() {
        PageReference pr = new PageReference(ApexPages.CurrentPage().getUrl());
        pr.getParameters().clear();
        pr.getParameters().put('id',flowChoice);
        pr.setRedirect(true);
        return pr;
    }
    

    static testMethod void runTests() {
        Flow__c f = new Flow__c(Name='blah');
        insert f;
        Node__c qn = new Node__c(Type__c='Question',
                         Text__c='Hello?',
                         isYesNo__c=True,
                         Flow__c=f.Id,
                         X__c=1,
                         Y__c=2);
        insert qn;
        f.StartNode__c = qn.Id;
        update f;
        
        Node__c vn = new Node__c(Type__c='Variable',
                         SetTrue__c='blah',
                         Flow__c=f.Id,
                         X__c=1,
                         Y__c=2);
        insert vn;
        Node__c dn = new Node__c(Type__c='Decision',
                         DecisionAndOr__c='OR',
                         DecisionRules__c='blah == True',
                         Flow__c=f.Id,
                         X__c=1,
                         Y__c=2);
        insert dn;
        Node__c un = new Node__c(Type__c='URL',
                         URL__c='http://blah.com',
                         Flow__c=f.Id,
                         X__c=1,
                         Y__c=2);
        insert un;
        qn.YesNext__c=vn.Id;
        qn.NoNext__c=un.Id;
        vn.Next__c=dn.Id;
        dn.YesNext__c=un.Id;
        dn.NoNext__c=un.Id;
        update qn;
        update vn;
        update dn;
     
        SitesFlowController fc = new SitesFlowController((Flow__c)f);
        
                         
    }
}
/*
 * Modified By -Swarnima Singh Mandhata - T-09359 - 02/31/2020
*/
public class IEM_ExtenalObjectFieldsBatch implements Database.Batchable<sObject> {
    public Database.QueryLocator start(Database.BatchableContext bc) {
        // collect the batches of records or objects to be passed to execute
        String query = 'SELECT id,Cloud__r.Name__c,Team_Responsible__r.Name__c,Security_Assessment_Record__r.Name__c,GUS_Cloud_Name__c,GUS_Team_Name__c,GUS_Security_Assessment_Name__c FROM Case Where (GUS_Cloud_Name__c = null OR GUS_Team_Name__c = null OR GUS_Security_Assessment_Name__c =null) and (Cloud__c != null OR Team_Responsible__c != null OR Security_Assessment_Record__c != null) ORDER By CreatedDate DESC';
        
        return Database.getQueryLocator(query);
    }
    public void execute(Database.BatchableContext bc, List<sObject> records){
        List<Case> caseList = new List<Case>();
        // process each batch of records
        for(Case newCase : (List<Case>)records){
            newCase.GUS_Team_Name__c  = newCase.Team_Responsible__r.Name__c;
            newCase.GUS_Cloud_Name__c   = newCase.Cloud__r.Name__c;
            newCase.GUS_Security_Assessment_Name__c   = newCase.Security_Assessment_Record__r.Name__c;
            caseList.add(newCase);
        }
        update caseList;
    }    
    public void finish(Database.BatchableContext bc){
        // execute any post-processing operations
    }    
}
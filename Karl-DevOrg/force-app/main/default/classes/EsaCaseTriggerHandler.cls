public class EsaCaseTriggerHandler {

    /**
     * Update Case Status - If Case had the Office Hours Appointment.
     * @return [description]
     */
    public static void updateCaseStatus(List<Case> lstCases){

        Set<String> setSecOrgCaseNumbers = new Set<String>();
        Set<String> setSptCaseNumbers = new Set<String>();
        Map<String,String> mapIntakeReuestInfo = new Map<String,String>();

        for(Case cas : lstCases){
            if(String.isNotBlank(cas.Enterprise_Security_Case_Number__c) && 
                            cas.RecordTypeId == SPT_Constants.SEC_RECORDTYPE_ID &&
                            cas.OwnerID == SPT_Constants.TRUST_APPLICATION_SECURITY_ASSURANCE){
                setSecOrgCaseNumbers.add(cas.Enterprise_Security_Case_Number__c);
            }
        }
        
        if(setSecOrgCaseNumbers.size() > 0) {
            for(Esa_Security_Request__c securityRequest :[SELECT Id,
                                                                 Supportforce_Case_Number__c
                                                          FROM Esa_Security_Request__c
                                                          WHERE Entity_Code__c = :Label.Esa_EntSec_EntityCode
                                                          AND Office_Hours__c != null
                                                          AND Supportforce_Case_Number__c IN : setSecOrgCaseNumbers]){

                mapIntakeReuestInfo.put(securityRequest.Supportforce_Case_Number__c, securityRequest.Id);
                setSptCaseNumbers.add(securityRequest.Supportforce_Case_Number__c);
            }


            for(Case cas : lstCases){
                if(mapIntakeReuestInfo.containsKey(cas.Enterprise_Security_Case_Number__c)){
                    cas.ESA_Security_Request__c = mapIntakeReuestInfo.get(cas.Enterprise_Security_Case_Number__c);
                }
            }

            List<String> lstCaseStatus = String.valueOf(Label.Esa_Existing_Case_Status).split(',');
            if(lstCaseStatus.size() == 0) return;

            for(Case cas : lstCases){
                if(lstCaseStatus.contains(cas.Status) 
                    && setSptCaseNumbers.contains(cas.Enterprise_Security_Case_Number__c)){

                    //cas.Status = Label.Esa_Case_Waiting_OH_Status;
                }
            }

        }

    }


    public static void updateIntakeRquestInfo(List<Cases> lstCases){

    }
    
    public static void updateExternalCaseStatus(List<Case> lstCases) {
        Set<String> sptCaseNumbers = new Set<String>();
        for(Case cas : lstCases){
            if(cas.Status == Label.Esa_Case_Waiting_OH_Status){
                sptCaseNumbers.add(cas.Enterprise_Security_Case_Number__c);
            }
        }

        if(sptCaseNumbers.size() > 0){
            List<Case__x> lstSptCases = [SELECT ExternalId,Status__c
                                         FROM Case__x
                                         WHERE CaseNumber__c IN :sptCaseNumbers];
            for(Case__x cas : lstSptCases){
                cas.Status__c = Label.Esa_Case_Waiting_OH_Status;
            }
            if(lstSptCases.size() > 0) {
                List<Database.SaveResult> sr = Database.updateImmediate(lstSptCases);
            }
        }
    }
}
/** 
 * @author Lakshya Arora
 * @email lakshya.arora@mtxb2b.com
 * @description This class will handle the apex operations performed in karl_audit_firm_Stakeholder
 * Used in karl_audit_firm_Stakeholder
 */
public with sharing class KARL_AuditFirmStakeholdersController {
     /**
     * @author Lakshya Arora
     * @email lakshya.arora@mtxb2b.com
     * @description Fetch the audit firm stakeholders related to cycle audit report
     * @param cycleAuditReportId - record Id for Cycle Audit Reports
     * @return List of AuditFirmStakeholder objects
     */
    @AuraEnabled(cacheable=true)
    public static List<KARL_Audit_Firm_Stakeholders__c> getAuditFirmStakeholders(Id recordId){
        List<KARL_Audit_Firm_Stakeholders__c> auditFirmStakeholderList = new List<KARL_Audit_Firm_Stakeholders__c>();
        String auditFirmId = '';
        KARL_Cycle_Audit_Report_Items__c cycleAuditReportItem = [SELECT Id,Audit_Reports_Master_Data__r.KARL_Audit_Firm__c 
                                                                FROM KARL_Cycle_Audit_Report_Items__c
                                                                WHERE Id =: recordId
                                                                WITH SECURITY_ENFORCED];
        auditFirmId = cycleAuditReportItem.Audit_Reports_Master_Data__r?.KARL_Audit_Firm__c;
        if(auditFirmId != '' && auditFirmId != null){
            for(KARL_Audit_Firm_Stakeholders__c auditFirmStakeholderObj : [SELECT Id,Stakeholder__r.Name,Audit_Firm__r.Name,Stakeholder__r.Email,Stakeholder__c 
                                                                            FROM KARL_Audit_Firm_Stakeholders__c 
                                                                            WHERE Audit_Firm__c =:auditFirmId
                                                                            AND Stakeholder__c != null
                                                                            WITH SECURITY_ENFORCED]){
                auditFirmStakeholderList.add(auditFirmStakeholderObj);
            }
        }
        return auditFirmStakeholderList;
    }
}
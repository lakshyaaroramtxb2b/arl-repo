/**
* @author Vishnu Kumar
* @email vishnu.kumar@mtxb2b.com
* @desc This class provides the method for generation for Audit Cycle.
*/
public with sharing class ARL_AuditCycleReportController {
	
    /**
    * @author Vishnu Kumar
    * @email vishnu.kumar@mtxb2b.com
    * @desc This method generates the report.
    */
    @AuraEnabled(cacheable=true)
    public static List<ReportWrapper> getReport(String auditCycleId){  
        
        List<ReportWrapper> items = new List<ReportWrapper>();
        
        //Step 1: Fetch Audit Cycle and Cycle Request Items
        Audit_Cycle__c auditCycle = [Select id, Name, (SELECT Id, Request__c FROM Cycle_Request_Items__r) 
                                     from Audit_Cycle__c where id=: auditCycleId ];
        
        //Step 2: Get all Request Item Ids from Cycle Request Items
        Set<Id> requestItemIds = new Set<Id>();
        for( Cycle_Request_Item__c cri : auditCycle.Cycle_Request_Items__r ){
            requestItemIds.add( cri.Request__c );
        }
        
        //Step 3: Fetching all the Request Item Control Tests as we dont a direct linking between Request Item Control Tests and Audit_Cycle_Coverage_Item__c
        //Map Structure as below
        //Key: RequestId_Scope and Value: Request Item Control Test
        Map<String, Request_Item_Control__c> requestScopeAndRequestItemControl = new Map<String, Request_Item_Control__c>();
        for( Request_Item_Control__c reqItemCtrl : [Select Id,
                                                    Name,
                                                    Request__c,
                                                    Test__c,
                                                    Test__r.Name,
                                                    Test__r.Test_Group__r.Name,
                                                    Full_Control_Scope__c,
                                                    KARL_Control_Scope_Label__c,
                                                    Test__r.Control_Scope__r.Scope_Name__c,
                                                    Test__r.Control_Scope__r.Control_Name__r.Name 
                                                    FROM Request_Item_Control__c 
                                                    WHERE Request__c in: requestItemIds]){
                                                        
            requestScopeAndRequestItemControl.put(reqItemCtrl.Request__c+'_'+reqItemCtrl.Test__r.Control_Scope__r.Scope_Name__c, reqItemCtrl);
        }
        
        // Also get Request Master Data Information
        // added by Ben Harvie 05/27/2020
        Map<String, Request_Item__c > requestItem = new Map<String, Request_Item__c >();
        for( Request_Item__c  reqItem : [Select Id, Name, Request_Name__c, Type__c
                                                 FROM Request_Item__c  
                                                 WHERE Id in: requestItemIds]){
                                                        
            requestItem.put(reqItem.Id, reqItem);
            //System.debug('DEBUGGER - ' + reqItem.Id);
        }
        // Create empty map for existing REQ to prevent duplication
        Map<String, Request_Item__c > exRequestItem = new Map<String, Request_Item__c >();
        
        //Step 4: Fetching Related Audit Cycle Coverage and Audit Cycle Coverage Item
        List<Audit_Cycle_Coverage__c> auditCycleCoverages = [Select id,Area__c,Scope__c, 
                                                             (Select id, Cycle_Request_Item__c, Cycle_Request_Item__r.Name,Cycle_Request_Item__r.Request__c ,Cycle_Request_Item__r.Request_Description__c, Cycle_Request_Item__r.Cycle_Request_Status__c  
                                                              from Audit_Cycle_Coverage_Item__r ) 
                                                             from Audit_Cycle_Coverage__c where Audit_Cycle__c =: auditCycleId];
        
        //Step 5: Making the report data
        for( Audit_Cycle_Coverage__c acc : auditCycleCoverages ){
            for( Audit_Cycle_Coverage_Item__c acci : acc.Audit_Cycle_Coverage_Item__r ){
                
                //Key: Request Id and Scope
                string reqScopeKey = acci.Cycle_Request_Item__r.Request__c+'_'+acc.Scope__c;
                
                if( requestScopeAndRequestItemControl.containsKey(reqScopeKey) && !exRequestItem.containsKey(reqScopeKey)){
                    Request_Item_Control__c reqItemCtrl = requestScopeAndRequestItemControl.get(reqScopeKey);
                    // Get information about the request item as well
                    Request_Item__c reqItem = requestItem.get(reqItemCtrl.Request__c);
                    //System.debug(reqItemCtrl.Request__c);
                    
                    ReportWrapper rw = new ReportWrapper();
                    //Information derived from Request Item Control Test (Request_Item_Control__c)
                    rw.contolName = reqItemCtrl.Test__r.Control_Scope__r.Control_Name__r.Name;
                    rw.fullControlScope = reqItemCtrl.Full_Control_Scope__c;
                    rw.ControlScopeLabel = reqItemCtrl.KARL_Control_Scope_Label__c;
                    rw.testNumber = reqItemCtrl.Test__r.Name;
                    rw.testNumberId = reqItemCtrl.Test__c;
                    rw.testGroupName = reqItemCtrl.Test__r.Test_Group__r.Name;
                    rw.requestItem = reqItem.Name;
                    rw.requestItemId = reqItem.Id;
                    rw.requestName = reqItem.Request_Name__c;
                    rw.requestType = reqItem.Type__c;
                    rw.requestItemControlNumber = reqItemCtrl.Name;
                    rw.requestItemControlNumberId = reqItemCtrl.Id;
                    
                        
                    //Information from Audit Cycle Coverage
                    rw.scopeName = acc.Scope__c;
                    rw.area = acc.Area__c;
                    
                    //Information fromAudit Cycle Coverage Item or Cycle Request Item
                    rw.requestDescription = acci.Cycle_Request_Item__r.Request_Description__c;
                    rw.cycleRequestItem = acci.Cycle_Request_Item__r.Name;
                    rw.cycleRequestItemId = acci.Cycle_Request_Item__c;
                    rw.cycleRequestStatus = acci.Cycle_Request_Item__r.Cycle_Request_Status__c;
                    
                    items.add(rw);
                    
                    // Store value so we don't duplicate things
                    exRequestItem.put(acci.Cycle_Request_Item__r.Request__c+'_'+acc.Scope__c, reqItem);
                }
            }
        }
        
        return items;
    }
    
    /**
    * @author Vishnu Kumar
    * @email vishnu.kumar@mtxb2b.com
    * @desc This method checks if the current execution in commnuity or not.
    */
    @AuraEnabled
    public static boolean isCommunity(){
        Id siteId = Site.getSiteId();
        if (siteId != null) {
            return true;
        }
        return false;
    }
    
    /**
    * @author Vishnu Kumar
    * @email vishnu.kumar@mtxb2b.com
    * @desc This is wrapper class to contains the report data at row level.
    */
    public class ReportWrapper{
        @AuraEnabled public string contolName;
        @AuraEnabled public string fullControlScope;
        @AuraEnabled public string ControlScopeLabel;
        @AuraEnabled public string scopeName;
        @AuraEnabled public string testNumber;
        @AuraEnabled public string testNumberId;
        @AuraEnabled public string testGroupName;
        @AuraEnabled public string requestItem;
        @AuraEnabled public string requestItemId;
        @AuraEnabled public string requestItemControlNumber;
        @AuraEnabled public string requestItemControlNumberId;
        @AuraEnabled public string area;
        @AuraEnabled public string cycleRequestItem;
        @AuraEnabled public string cycleRequestItemId;
        @AuraEnabled public string cycleRequestStatus;
        @AuraEnabled public string requestName;
        @AuraEnabled public string requestType;
        @AuraEnabled public string requestDescription;
    }
}
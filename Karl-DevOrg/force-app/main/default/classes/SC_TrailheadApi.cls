/*
 * Trailhead api class used for connection with ORG 62
 *
 */

public class SC_TrailheadApi {

    private static final String API_TOKEN = [SELECT API_Token__c FROM Trailhead_Validity__c LIMIT 1].API_Token__c;
    private static final Map<String, String> HTTP_REQUEST_HEADERS =
        new Map<String, String> {
            'Content-Type' => 'application/json; charset=utf-8',
            'X-Api-Key' => SC_TrailheadApi.API_TOKEN
    };
    private static final String URL_BASE = 'callout:Trailhead_Org62';

    public SC_TrailheadApi() {}


    public SC_TrailResponse getTrails(Integer pFetchSize) {

        String reqUrl = SC_TrailheadApi.URL_BASE + '/v1/trails?offset=';

        reqUrl += (pFetchSize != null && pFetchSize >= 0) ? String.valueOf(pFetchSize) : String.valueOf(0);

        SC_TrailResponse resps = SC_TrailResponse.parse(getResponse(reqUrl, 'GET'));

        return resps;
    }

    public SC_ModuleResponse getModules(Integer pFetchSize) {

        String reqUrl = SC_TrailheadApi.URL_BASE + '/v1/modules?offset=';

        reqUrl += (pFetchSize != null && pFetchSize >= 0) ? String.valueOf(pFetchSize) : String.valueOf(0);

        SC_ModuleResponse resps = SC_ModuleResponse.parse(getResponse(reqUrl, 'GET'));

        return resps;
    }

    private String getResponse(String pUrl, String pMethod) {

        Http http = new Http();
        HttpRequest req = new HttpRequest();
        req.setTimeout(100000);
        req.setEndpoint(pUrl);
        req.setMethod(pMethod);

        for (String str :SC_TrailheadApi.HTTP_REQUEST_HEADERS.keySet()) {
            req.setHeader(str, SC_TrailheadApi.HTTP_REQUEST_HEADERS.get(str));
        }

        HttpResponse res = http.send(req);

        if (res.getStatusCode() >= 300) {
            throw new TrailheadApiException(res.getBody());
        }

        return res.getBody();
    }

    public class TrailheadApiException extends Exception {}
}
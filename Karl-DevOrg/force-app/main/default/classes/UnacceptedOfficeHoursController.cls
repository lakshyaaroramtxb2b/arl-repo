global with sharing class UnacceptedOfficeHoursController implements Schedulable {
    private static final String REMINDER_EMAIL = 'securecloud@salesforce.com';
    // private static final String REMINDER_EMAIL = 'neal.harris@salesforce.com'; // for testing only
    private static final String ORG_WIDE_EMAIL_ADDRESS_ID = '0D2300000008OqS';
    global void execute(SchedulableContext ctx)     {
        for (Partner_Appointment__c p : [SELECT ProdSec_Member__c, Date__c, Company__c FROM Partner_Appointment__c WHERE ProdSec_Member__c = null]){
            Datetime dt = p.Date__c;
            // check which appointments for today don't have a ProdSec team member attached to them.  Send an email reminder for each of those.
            if (dt.date() == Datetime.now().date()){
                Messaging.SingleEmailMessage notif = new Messaging.SingleEmailMessage();
                notif.setSubject('Unfilled office hours - ' + dt.format('h:mm a'));
                notif.setOrgWideEmailAddressId(ORG_WIDE_EMAIL_ADDRESS_ID);
                notif.setToAddresses(new String[] {REMINDER_EMAIL});
                notif.setPlainTextBody(p.Company__c + ' has requested an office hours appointment today at ' + dt.format('h:mm a') + '.  So far, no one has responded to the request.');
                Messaging.sendEmail(new Messaging.Email[] {notif});
            }
        }
    }
}
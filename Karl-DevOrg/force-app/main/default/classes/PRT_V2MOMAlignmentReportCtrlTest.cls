/* Created by - Swarnima Singh Mandhata
 * Date - 02/25/2020
 * Class - PRT_V2MOMAlignmentReportCtrl
 * */
@isTest
public class PRT_V2MOMAlignmentReportCtrlTest {
    @testSetup 
    static void testSetup(){
        List<User> userList = new List<User>();
        userList = PRT_TestDataFactory.createUsers(3,'System Administrator',true);
    }
    @isTest
    public static void getV2MomTest(){
        List<User> userRecord = new List<User>([SELECT Id FROM USER WHERE Email Like 'PRTUser%']);
        String fromDate = '2019-02-05';
        String toDate = '2019-02-09';
        
        List<sObject> v2MomTestList = new List<sObject>();
        Id userId1 =userInfo.getUserId();
        Id userId = userRecord[0].id;
        String publishDate = String.ValueOf(date.today());
        
        String org62JSON = '{"attributes":{"type":"Org62User__x","url":"/services/data/v48.0/sobjects/Org62User__x/0054D000001YKEcQAO"},"EmployeeNumber__c":"77456","ExternalId":"'+ userId+'"}';
        String v2MomJSON = '{"attributes":{"type":"V2MOM_c__x","url":"/services/data/v48.0/sobjects/V2MOM_c__x/0054D000001WzcyQAC"},"V2MOM_User_c__c":"'+ userId+'","ExternalId":"'+userId1+'","Name__c":"Test 123","Published_on_c__c":"'+publishDate+'"}';
        String measureJSON = '{"attributes":{"type":"Measure_c__x","url":"/services/data/v48.0/sobjects/Measure_c__x/0054D000001VnAMQA0"},"MeasureName_c__c":"test measure1","ExternalId":"'+userId1+'","V2MOM_c__c":"'+userId1+'"}';
        String methodJSON = '{"attributes":{"type":"V2MOM_Method_c__x","url":"/services/data/v48.0/sobjects/V2MOM_Method_c__x/0054D000001WzcyQAC"},"Name__c":"test method1","Description_c__c":"test desc","V2MOM_c__c":"'+userId1+'","ExternalId":"'+userId1+'"}';
        
       
        PRT_V2MOMAlignmentReportCtrl.mockallV2MOMList.add((V2MOM_c__x)JSON.deserialize(v2MomJSON,V2MOM_c__x.class ));
        PRT_V2MOMAlignmentReportCtrl.mockAllOrg62List.add((Org62User__x)JSON.deserialize(org62JSON,Org62User__x.class ));
        PRT_V2MOMAlignmentReportCtrl.mockAllMethodList.add((V2MOM_Method_c__x)JSON.deserialize(methodJSON,V2MOM_Method_c__x.class));
        
        V2MomMapping__c v2momMappingRec = new V2MomMapping__c();
        v2momMappingRec.Dependent_Method__c = userInfo.getUserId();
        insert v2momMappingRec;
        
        test.startTest();
        List<V2MOM_c__x> v2momList = PRT_V2MOMAlignmentReportCtrl.getV2Mom(fromDate,toDate);
        System.assertEquals(v2momList!=null, true);
        test.stopTest();
        
    }
    
}
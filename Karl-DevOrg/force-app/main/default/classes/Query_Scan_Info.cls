public with sharing class Query_Scan_Info {

    String my_query_head = 'SELECT Id, Username__c, X62OrgUsername__c, WorkState__c, comments__c, CreatedDate FROM CodeScan__c';
    String my_query_tail = ' AND CodeScan__c.CreatedDate > LAST_YEAR ORDER BY CreatedDate DESC LIMIT 100'; 
    
    // Query string as supplied by user   
    public String query { get; set; }
    
    //getter and setter for codescan id
    public String csid { get; set;}
    
    //query message (for error codes)
    public String queryMessage {get; set; }
    
    //success of code scan query
    public Boolean querySuccess {get; set;}

    //scans are the objects returned with button query
    public List <CodeScan__c> scans {get;set;}

    // total lines in line count query   
    public String totalLineCount { get; private set; }
        
    //scan queue objects are the objects returned in details query. Only 1 object is returned for each Code Scan
    public Scan_Queue__c sq {get; set;}
        
    public Query_Scan_Info() {
    
        this.queryMessage = 'Select field to query. Email queries that start with an \'@\' followed by a fully qualified domain (@foo.com) in the search box will also calculated total lines scanned in last 12 months.';
        this.querySuccess = false;
   }
        
    private void makeQuery(String param, String fieldName, Boolean exact) {
        String match;
        sq = null; //reset scan queue data when rendering code scan objects
        
        if (param != null && param.length() > 0) {
            if (exact) {
                match = ' WHERE ' + fieldName + '=\'' + String.escapeSingleQuotes(param) + '\' ';
            } else {
                match = ' WHERE ' + fieldName + ' LIKE \'%' + String.escapeSingleQuotes(param) + '%\' ';
            }
            
            try {
                system.debug('query: ' + my_query_head + match + my_query_tail);
                scans = Database.query(my_query_head + match + my_query_tail);
                system.debug(scans);
                if (scans.size() == 0) {
                    querySuccess = false;
                    queryMessage = 'No results returned';
                    return;
                } else {
                    querySuccess = true;
                    queryMessage = 'Found scan job(s) matching search criteria. Select \'Get Status\' to determine scan status for each job of interest.';
                    return;
                    
                } 
            } catch(Exception e) {
                querySuccess = false;
                queryMessage = 'Error encountered when performing query:\n' + e.getMessage();
                return;   
            }
        } else {
            querySuccess = false;
            queryMessage ='Query called with empty string';
            return;
        }
    }
    
    //query scan queue for associated code scan
    public void scanStatus() {
      List <Scan_Queue__c> sq_list = null;
      if (csid != null && csid.length() > 0 ) {
          try {
                sq_list = [SELECT Id, Status__c, Comments__c, UserType__c,
                Friendly_Name__c, Line_Count__c, CreatedDate, 
                Scan_Info__c, Scan_Info__r.Total_Security_and_Code_Quality_Issues__c,
                CodeScan__r.Id 
                FROM Scan_Queue__c WHERE CodeScan__r.Id = :csid
                ORDER BY CreatedDate DESC LIMIT 1];
          } catch(Exception e) {
              sq_list = null;
              system.debug(e.getMessage());
              }
          
          if (sq_list != null && sq_list.size() > 0 ) {
              sq = sq_list[0];
          } else {
              sq = null;
          }
      
      } else {
          sq = null;
          system.debug('No code scan id supplied');
      }    
    
    }
    
    /*
        Line of Code query
    */
    private Integer codeQuery(String emailDomain) {    
       if (String.isEmpty(emailDomain) || !emailDomain.startsWith('@')) {
           return -1;
           } else { 
           try {
               AggregateResult[] totalLines = [SELECT SUM(Line_Count__c)total FROM Scan_Info__c WHERE ( 
               (Scan_Info__c.Status__c='Done') AND
               (Scan_Info__c.Line_Count__c > 0) AND
               (Scan_Info__c.Scan_Type__c='Portal') AND 
               (Scan_Info__c.Email__c LIKE :('%' + emailDomain)) AND 
               (Scan_Info__c.CreatedDate = LAST_N_DAYS:365)
               )];
               return Integer.valueOf(totalLines[0].get('total'));
               
               } catch(Exception e) {
                    system.debug('Error running aggregate query');
                    return -1;
               }
            }      
     }
    
    
    
    /*
        Code Scan queries
    */
    

    
    public void unameQuery() {
            makeQuery(query,'Username__c', false);
    }
    
    public void emailQuery() {
        makeQuery(query,'X62OrgUsername__c', false);
        
        Integer lines = codeQuery(query);
        if (lines > -1) {
            this.totalLineCount  = 'Line Count: ' + String.valueOf(lines);
           } else {
            this.totalLineCount = 'Could not count lines (does your email address start with \'@\'?)';
           }
        queryMessage += '. ' + totalLineCount;
        
    }

    public void idQuery() {
            makeQuery(query,'Id', true);    
    }


}
global class AppXBurpRequestHandler implements Messaging.InboundEmailHandler {
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
                   String headerinfo = '';
            for (Messaging.InboundEmail.Header info : email.headers){
                headerinfo += info.name + ': ' + info.value + '\r\n';
            }
             AppX62.emailAMessage('arubinstein@salesforce.com', 'asd', headerinfo + '\r\n\r\n' + email.subject + '\r\n\r\n' + email.plainTextBody);

        String refid = getRefIdFromEmail(email.plainTextBody);
        if (refid != null){
/*            WebAppScanner__c webapp = [SELECT Id, Status__c FROM WebAppScanner__c WHERE EmailToken__c = :refid AND Status__c = 'New' LIMIT 1];
            
            webapp.Status__c = 'Not-Approved';
            webapp.Reason_for_Denying__c = 'Bounced';
            update webapp;
*/
                    
            AppX62.emailAMessage('arubinstein@salesforce.com', 'asd', refid + '\r\n\r\n' + headerinfo + '\r\n\r\n' + email.subject + '\r\n\r\n' + email.plainTextBody);
        }
        result.success = true;
        return result;
    }
    public static String getRefIdFromEmail(String textContext){
        Matcher m = Pattern.compile('(?ims)ref:(.*):ref').matcher(textContext);
        while (m.find()){
            if (m.groupCount() == 1){
                return m.group(1);
            }
        }
        return null;
    }
}
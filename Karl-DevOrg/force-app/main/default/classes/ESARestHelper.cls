public with sharing class ESARestHelper {
    public final static String METHOD_GET = 'GET';
    public final static String METHOD_POST = 'POST';
    public final static String METHOD_PUT = 'PUT';
    public final static String METHOD_PATCH = 'PATCH';
    public final static String METHOD_DELETE = 'DELETE';
    public final static String REQ_HEADER_JSON = '{"Content-Type":"application/json", "charset":"UTF-8", "Accept":"application/json"}';
    public final static String CALLOUT_SFDC_NC_BASE_URL = 'callout:{0}/services/data/v49.0';
    public final static String SFORCE_NAMED_CREDENTIAL = 'SupportForceNC';
    public final static String SECORG_NAMED_CREDENTIAL = 'SecurityOrg';

    public ESARestHelper() {

    }

    public static String getCalloutUrlForSfdcRest(String namedCredential) {
        return String.format(CALLOUT_SFDC_NC_BASE_URL, new List<String>{namedCredential});
    }

    public static String authenticateWithSOAP(String namedCredentials) {

        if (String.isBlank(namedCredentials)) {
            return null;
        }

        String NS_SOAP = 'http://schemas.xmlsoap.org/soap/envelope/';
            String NS_SF = 'urn:partner.soap.sforce.com';
            HttpRequest soapreq = new HttpRequest();
            soapreq.setMethod(METHOD_POST);
            soapreq.setTimeout(60000);
            soapreq.setEndpoint('callout:'+namedCredentials+'/services/Soap/u/49.0');
            soapreq.setHeader('Content-Type', 'text/xml;charset=UTF-8');
            soapreq.setHeader('SOAPAction', '""');
            soapreq.setBody('<Envelope xmlns="http://schemas.xmlsoap.org/soap/envelope/"><Header/>' +
                        '<Body><login xmlns="urn:partner.soap.sforce.com"><username>' +
                        '{!HTMLENCODE($Credential.UserName)}' +
                        '</username><password>' +
                        '{!HTMLENCODE($Credential.Password)}' +
                        '</password></login></Body></Envelope>');
            HttpResponse res =  new Http().send(soapreq);

            if(res.getStatusCode() != 200) {
                Dom.Document responseDocument = res.getBodyDocument();
                Dom.Xmlnode rootElm = responseDocument.getRootElement(); // soapenv:Envelope
                Dom.Xmlnode bodyElm = rootElm.getChildElement('Body', NS_SOAP); // soapenv:Body
                Dom.Xmlnode faultElm = bodyElm.getChildElement('Fault', NS_SOAP); // soapenv:Fault
                String exceptionText = 'Login Failure';
                if (faultElm != null) {
                    Dom.Xmlnode faultStringElm = faultElm.getChildElement('faultstring', null);
                    if (faultStringElm!=null && faultStringElm.getText() != null) {
                        exceptionText = faultStringElm.getText();
                    }
                }
                throw new CalloutException();
            }
            
            String sessionId;
            Dom.Document responseDocument = res.getBodyDocument();
            Dom.Xmlnode rootElm = responseDocument.getRootElement(); // soapenv:Envelope
            Dom.Xmlnode bodyElm = rootElm.getChildElement('Body', NS_SOAP); // soapenv:Body
            Dom.Xmlnode loginResponseElm = bodyElm.getChildElement('loginResponse', NS_SF); // loginResponse
            if(!Test.isRunningTest()) {
                Dom.Xmlnode resultElm = loginResponseElm.getChildElement('result', NS_SF); // result
                Dom.Xmlnode sessionIdElm = resultElm.getChildElement('sessionId', NS_SF); // sessionId
                sessionId = sessionIdElm.getText();
            }
            return sessionId;
    }

    public static String callRESTAPI(String sessionId, String path, String method, String headers, String body) {
        HttpRequest req = new HttpRequest();
        req.setEndpoint(path);
        req.setMethod(method);
        if (!String.isBlank(body)) {
            req.setBody(body);
        }

        return makeRequest(req, sessionId, headers);        
    }

    public static String callRESTQuery(String sessionId, String path, String headers, String query) {
        HttpRequest req = new HttpRequest();
        req.setEndpoint(path + '/query?q=' + query);
        req.setMethod(METHOD_GET);
        
        return makeRequest(req, sessionId, headers);
    }

    private static String makeRequest(HttpRequest req, String sessionId, String headers) {
        setCommonHeaders(req, sessionId, headers);
        Http http = new Http();
        HTTPResponse res = http.send(req);
        return handleResponse(req, res);
    }

    private static void setCommonHeaders(HttpRequest req, String sessionId, String headers) {
        if (!String.isBlank(sessionId)) {
            req.setHeader('Authorization', 'Bearer ' + sessionId);
        }

        if (!String.isBlank(headers)) {
            Map<String, Object> headerMap = (Map<String, Object>) JSON.deserializeUntyped(headers);
            for (String key : headerMap.keySet()) {
                req.setHeader(key, String.valueOf(headerMap.get(key)));
            }
        }
    }

    private static Boolean isRedirect(HTTPResponse res) {
        Integer statusCode = res.getStatusCode();
        return  statusCode >=300 && statusCode <= 307 && statusCode != 306;
    }

    private static Boolean isServerError(HTTPResponse res) {
        Integer statusCode = res.getStatusCode();
        return  statusCode >= 500;
    }

    private static String handleResponse(HttpRequest req, HTTPResponse res) {
        
        boolean redirect = false;
        if (isRedirect(res)) {
            do {
                redirect = false; // reset the value each time
                String loc = res.getHeader('Location'); // get location of the redirect
                if(loc == null) {
                    redirect = false;
                    continue;
                }
                req.setEndpoint(loc);
                res = (new Http()).send(req);
                redirect= isRedirect(res);
            } while (redirect && Limits.getCallouts() != Limits.getLimitCallouts());
        }

        if (isServerError(res)) {
            CalloutException e = new CalloutException();
            e.setMessage(res.getBody());
            throw e;
        } 
        return res.getBody();
    }
}
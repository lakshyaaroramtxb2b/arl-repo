/*
Developer: Ralph Callaway <ralph@callaway.cloud>
Description:
	Cosntants for the Training_Course__c object
*/

public class TrainingCourse_Constants {
	public static final String COURSE_LEVEL_MODULE = 'Module';
	public static final String COURSE_LEVEL_TRAIL = 'Trail';

	public static final String PROVIDER_MANUAL = 'Manual';
}
/**
 * @author: ralph@callaway.cloud
 */
public class SC_Unenroller {

    private Options opts;

    public SC_Unenroller(Options opts) {
        System.assertNotEquals(null, opts, 'opts is required');
        this.opts = opts;
    }
    
    public Unenrollment[] unenroll(Training_Course_Taken__c[] enrollments) {
        Unenrollment[] unenrollments = new Unenrollment[0];
        Unenrollment[] updateNeeded = new Unenrollment[0];
        Training_Course_Taken__c[] tcts = new Training_Course_Taken__c[0];
        for (Training_Course_Taken__c enrollment : enrollments) {
            Unenrollment unenrollment = new Unenrollment(enrollment, opts);
            unenrollments.add(unenrollment);
            if (unenrollment.hasChanges) {
                updateNeeded.add(unenrollment);
                tcts.add(unenrollment.newRec);
            }
        }

        Database.SaveResult[] saveResults = Database.update(tcts, false);
        for (Integer i = 0; i < saveResults.size(); i++) {
            updateNeeded[i].setSaveResult(saveResults[i]);
        }
        return unenrollments;
    }

    public Enum Result {
        ERROR,
        NO_CHANGE,
        UNENROLLED
    }

    public class Unenrollment {
        public SC_Unenroller.Result result = SC_Unenroller.Result.NO_CHANGE;
        public String[] errors = new String[0];
        public Training_Course_Taken__c oldRec;
        public Training_Course_Taken__c newRec;
        public Boolean hasChanges;

        public Unenrollment(Training_Course_Taken__c enrollment, Options opts) {
            this.oldRec = enrollment;
            this.newRec = new Training_Course_Taken__c(
                Block_Enrollment__c = (enrollment.Block_Enrollment__c || opts.excludeFutureEnrollments),
                Date_of_Escalation_to_Manager__c = null,
                Date_of_Escalation_to_Trust_Engagement__c = null,
                Due_Date__c = null,
                Enrollment_Notification_Date__c = null,
                Escalation1__c = null, 
                Escalation2__c = null,
                Escalation3__c = null,
                Escalation4__c = null,
                Escalation5__c = null,
                Last_Escalation__c = null,
                RecordTypeId = TrainingCourseTaken_Constants.RT_ID_VOLUNTARY,
                Id = enrollment.Id);
            this.hasChanges = hasChanges(newRec, oldRec);
            this.result = hasChanges ? SC_Unenroller.Result.UNENROLLED : SC_Unenroller.Result.NO_CHANGE;
        }

        private Boolean hasChanges(Training_Course_Taken__c newRec, Training_Course_Taken__c oldRec) {
            return 
                newRec.Block_Enrollment__c != oldRec.Block_Enrollment__c ||
                newRec.Date_of_Escalation_to_Manager__c != oldRec.Date_of_Escalation_to_Manager__c ||
                newRec.Date_of_Escalation_to_Trust_Engagement__c != oldRec.Date_of_Escalation_to_Trust_Engagement__c ||
                newRec.Due_Date__c != oldRec.Due_Date__c ||
                newRec.Enrollment_Notification_Date__c != oldRec.Enrollment_Notification_Date__c ||
                newRec.Escalation1__c != oldRec.Escalation1__c ||
                newRec.Escalation2__c != oldRec.Escalation2__c ||
                newRec.Escalation3__c != oldRec.Escalation3__c ||
                newRec.Escalation4__c != oldRec.Escalation4__c ||
                newRec.Escalation5__c != oldRec.Escalation5__c ||
                newRec.Last_Escalation__c != oldRec.Last_Escalation__c ||
                newRec.RecordTypeId != oldRec.RecordTypeId;
        }

        private void setSaveResult(Database.SaveResult saveResult) {
            if (!saveResult.isSuccess()) {
                this.result = SC_Unenroller.Result.ERROR;
                for (Database.Error err : saveResult.getErrors()) {
                    this.errors.add(String.valueOf(err));
                }
            }
        }
    }

    public class Options {
        public Boolean excludeFutureEnrollments = false;
    }
    
}
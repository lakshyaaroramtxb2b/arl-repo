public class IEM_MilestoneTriggerHandler {
    /*
     *  Method is used to track all method calls on after Insert
     *  This method takes input of List of Cases
     */
    public static void beforeInsert(List<Milestone__c> newList){
        IEM_MilestoneTriggerHelper.updateMilestoneContacts(newList,null);
    }
    /*
     *  Method is used to track all method calls on after Update
     *  This method takes input of List of Cases
     */
    /*public static void afterUpdate(List<Milestone__c> newList, Map<Id, Milestone__c> oldMap){
        IEM_MilestoneTriggerHelper.updateGusWorkRecords(newList,oldMap);
    }*/
}
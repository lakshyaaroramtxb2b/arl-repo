public with sharing class ControlPanelController {
    public class GroupWrapper {
        public Monitor_Group__c grp {get;private set;}
        public GroupWrapper(Monitor_Group__c g){
            this.grp = g;
        }
        public List<Monitored_System__c> getSystems() {
            return [SELECT Name,Description__c,API_URL__C FROM Monitored_System__c 
                           WHERE Active__c = True AND Monitor_Group__c = :this.grp.Id];
        }
    }
    
    public List<GroupWrapper> getGroups() {
        GroupWrapper [] l= new GroupWrapper []{};
        for (Monitor_Group__c g : [SELECT Name,Description__c FROM Monitor_Group__c]) {
            l.add(new GroupWrapper(g));
        }
        return l;
    }
}
public with sharing class ESAgtmOAuthUtil {
    
/* 

    Developer Name: Jorge L Caceres | January 2015
    
    Description:
    Manages creating gotomeetings for office hour appointments
    
    Documentation reference:
    https://developer.citrixonline.com/page/direct-login
    https://developer.citrixonline.com/api/gotomeeting-rest-api/apimethod/create-meeting
    https://developer.citrixonline.com/api/gotomeeting-rest-api/apimethod/delete-meeting
    https://developer.citrixonline.com/api/gotomeeting-rest-api/apimethod/start-meeting
    
    
    
*/

    private String CONSUMER_KEY = 'KQGeImTsqqGS6U5YqN9j8ASf8IFL6cpe';
    private String CONSUMER_SECRET = 'wpYLK2UBzjFCMang';
    private String CALLBACK_URL = 'https://security.secure.force.com/esa/';
    
    private String GTM_USER_ID = 'esa.coordinator@salesforce.com';
    private String GTM_PASSWORD = 'unNFZRq6V8JCkH6Uvp@7';
    private String ACCESS_TOKEN;
  
    private final String G2M_API_URL = 'https://api.citrixonline.com';
    private final String GET_ACCESS_TOKEN_URL = '/oauth/access_token';
    private final String G2M_MEETING_URL = '/G2M/rest/meetings';
    
    private class LoginException extends Exception {}
    
    public class GotoMeeting {

        public String joinURL;
        public String meetingid;
        public String conferenceCallInfo;
    	
    }    
    
    public ESAgtmOAuthUtil() {
    	// try to get custom setting if not there do upsert later due to restriction of callouts before db commits
        try {
            ESA_GotoMeeting_Setting__c gtmSecrets = [select Consumer_Key__c, Consumer_Secret__c, 
                                   Password__c, User_Id__c, Access_Token__c 
                                   from ESA_GotoMeeting_Setting__c where name = 'secret'];
            CONSUMER_KEY = gtmSecrets.Consumer_Key__c;
            CONSUMER_SECRET = gtmSecrets.Consumer_Secret__c;
            GTM_USER_ID = gtmSecrets.User_Id__c;
            GTM_PASSWORD = gtmSecrets.Password__c;
            ACCESS_TOKEN = gtmSecrets.Access_Token__c;                    
        } catch (exception e) {
 
        }
    }
    
    private void upsertSecrets() {
    	system.debug('CONSUMER_KEY: ' + CONSUMER_KEY);
        system.debug('CONSUMER_SECRET: ' + CONSUMER_SECRET);
        system.debug('GTM_USER_ID: ' + GTM_USER_ID);
        system.debug('GTM_PASSWORD: ' + GTM_PASSWORD);
        system.debug('ACCESS_TOKEN: ' + ACCESS_TOKEN);
        upsert new ESA_GotoMeeting_Setting__c(
            name = 'secret',
            Consumer_Key__c = CONSUMER_KEY,
            Consumer_Secret__c = CONSUMER_SECRET,
            User_Id__c = GTM_USER_ID,
            Password__c = GTM_PASSWORD,
            Access_Token__c = ACCESS_TOKEN
        );
    }
    
    private void refreshAccessToken() {
        String queryString = '?grant_type=password';
        queryString += '&user_id=' + GTM_USER_ID;
        queryString += '&password=' + GTM_PASSWORD;
        queryString += '&client_id=' + CONSUMER_KEY;
         
        HttpRequest req = new HttpRequest();
        req.setMethod('GET');   
        req.setTimeout(60000);
        String endP = G2M_API_URL + GET_ACCESS_TOKEN_URL + queryString;
        req.setEndpoint(endP);
        setHeaders(req, null);
        HttpResponse res =  new Http().send(req);
        Set<Integer> validResponseCodes = new Set<Integer>{200};
        if(!validResponseCodes.contains(res.getStatusCode()))
        {
            throw new LoginException('Non valid response from server. Response: ' + res.getStatusCode()); 
        }        

        JSONParser parser = JSON.createParser(res.getBody());        

        String access_token;
        String refresh_token;
        String organizer_key;
        
        while (parser.nextToken() != null) {
            if ((parser.getCurrentToken() == JSONToken.FIELD_NAME)){

                if (parser.getText() == 'access_token') {
                    parser.nextToken();
                    access_token = parser.getText();
                 }

                if (parser.getText() == 'refresh_token') {
                    parser.nextToken();
                    refresh_token = parser.getText();
                 }

                if (parser.getText() == 'organizer_key') {
                    parser.nextToken();
                    organizer_key = parser.getText();
                 }

            }
        }
        
        this.ACCESS_TOKEN = access_token; 
                        
    }  
    
    public GotoMeeting createMeeting(String subject, Datetime startTime, Datetime endTime) {

        JSONGenerator generator = JSON.createGenerator(true);
        generator.writeStartObject();
            
        generator.writeStringField('subject', subject);
        generator.writeDateTimeField('starttime', startTime);         
        generator.writeDateTimeField('endtime', endTime);         
        generator.writeStringField('passwordrequired', 'false');
        generator.writeStringField('conferencecallinfo', 'Hybrid');
        generator.writeStringField('timezonekey', '');
        generator.writeStringField('meetingtype', 'Scheduled');
            
        generator.writeEndObject();
            
        String jsonString = generator.getAsString();
        
        Boolean upsertSecret = false;
         
        HttpRequest req = new HttpRequest();
        req.setMethod('POST');   
        req.setTimeout(60000);
        String endP = G2M_API_URL + G2M_MEETING_URL;
        req.setEndpoint(endP);
        setHeaders(req, ACCESS_TOKEN);
        req.setBody(jsonString);
        HttpResponse res =  new Http().send(req);
        if (res.getStatusCode() == 403) {
        	refreshAccessToken(); 
            req.setHeader('Authorization', 'OAuth oauth_token=' + ACCESS_TOKEN);        	
        	res = new Http().send(req);
        	upsertSecret = true;
        	}
        Set<Integer> validResponseCodes = new Set<Integer>{200,201};
        if(!validResponseCodes.contains(res.getStatusCode()))
        {
            throw new LoginException('Non valid response from server. Response: ' + res.getStatusCode() + '' + res.getBody()); 
        }        

        JSONParser parser = JSON.createParser(res.getBody());        

        String joinURL;
        String uniqueMeetingId;
        String meetingid;
        String conferenceCallInfo;
        
        while (parser.nextToken() != null) {
            if ((parser.getCurrentToken() == JSONToken.FIELD_NAME)){

                if (parser.getText() == 'joinURL') {
                    parser.nextToken();
                    joinURL = parser.getText();
                 }

                if (parser.getText() == 'uniqueMeetingId') {
                    parser.nextToken();
                    uniqueMeetingId = parser.getText();
                 }

                if (parser.getText() == 'conferenceCallInfo') {
                    parser.nextToken();
                    conferenceCallInfo = parser.getText();
                 }

                if (parser.getText() == 'meetingid') {
                    parser.nextToken();
                    meetingid = parser.getText();
                 }

            }
        }
        
        GotoMeeting gtm = new GotoMeeting();
            gtm.joinURL = joinURL;
            gtm.meetingid = meetingid;
            gtm.conferenceCallInfo = conferenceCallInfo;
        
        // any DB update must be done after all callouts
        if (upsertSecret) upsertSecrets();
                
        return gtm;
    	
    }
    
    public String cancelMeeting(String meetingid) {
        String queryString = '/' + meetingid ;
        
        Boolean upsertSecret = false;
         
        HttpRequest req = new HttpRequest();
        req.setMethod('DELETE');   
        req.setTimeout(60000);
        String endP = G2M_API_URL + G2M_MEETING_URL + queryString;
        req.setEndpoint(endP);
        setHeaders(req, ACCESS_TOKEN);
        HttpResponse res =  new Http().send(req);
        if (res.getStatusCode() == 403) {
            refreshAccessToken(); 
            req.setHeader('Authorization', 'OAuth oauth_token=' + ACCESS_TOKEN);            
            res = new Http().send(req);
            upsertSecret = true;
            }
        Set<Integer> validResponseCodes = new Set<Integer>{200,204};
        if(!validResponseCodes.contains(res.getStatusCode()))
        {
            throw new LoginException('Non valid response from server. Response: ' + res.getStatusCode()); 
        }        

        JSONParser parser = JSON.createParser(res.getBody());        

        String hostURL;
        
        while (parser.nextToken() != null) {
            if ((parser.getCurrentToken() == JSONToken.FIELD_NAME)){

                if (parser.getText() == 'hostURL') {
                    parser.nextToken();
                    hostURL = parser.getText();
                 }

            }
        }
        
        // any DB update must be done after all callouts
        if (upsertSecret) upsertSecrets();
                
        return hostURL;
                        
    }
        
    public String getStartURL(String meetingid) {
        String queryString = '/' + meetingid + '/start';
         
        HttpRequest req = new HttpRequest();
        req.setMethod('GET');   
        req.setTimeout(60000);
        String endP = G2M_API_URL + G2M_MEETING_URL + queryString;
        req.setEndpoint(endP);
        setHeaders(req, ACCESS_TOKEN);
        HttpResponse res =  new Http().send(req);
        if (res.getStatusCode() == 403) {
            refreshAccessToken(); 
            req.setHeader('Authorization', 'OAuth oauth_token=' + ACCESS_TOKEN);            
            res = new Http().send(req);
            }
        Set<Integer> validResponseCodes = new Set<Integer>{200};
        if(!validResponseCodes.contains(res.getStatusCode()))
        {
            throw new LoginException('Non valid response from server. Response: ' + res.getStatusCode()
            + ' ' + res.getBody()); 
        }        

        JSONParser parser = JSON.createParser(res.getBody());        

        String hostURL;
        
        while (parser.nextToken() != null) {
            if ((parser.getCurrentToken() == JSONToken.FIELD_NAME)){

                if (parser.getText() == 'hostURL') {
                    parser.nextToken();
                    hostURL = parser.getText();
                 }

            }
        }
                
        return hostURL;
                        
    }
            
    private static void setHeaders(HttpRequest req, String access_code) {
        req.setHeader('Content-Type', 'application/json; charset=UTF-8');        
        req.setHeader('Accept', 'application/json');
        req.setHeader('X-PrettyPrint', 'true');
        req.setHeader('Accept-Encoding', 'gzip,deflate,sdch');
        if(access_code != null) req.setHeader('Authorization', 'OAuth oauth_token=' + access_code);
    }
     
}
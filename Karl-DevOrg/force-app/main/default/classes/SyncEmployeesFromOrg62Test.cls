@IsTest
public class SyncEmployeesFromOrg62Test {

    @IsTest
    static void testBatch() {
        sfutil.ESASupportForceUtilTestCallOutMocker callOutMocker = new sfutil.ESASupportForceUtilTestCallOutMocker();
        Test.setMock(HttpCalloutMock.class, callOutMocker);
        
        Test.startTest();
        SyncEmployeesFromOrg62 batch = new SyncEmployeesFromOrg62();
        Database.executeBatch(batch);
        Test.stopTest();
    }

    @IsTest
    static void testGetLatestEmployeeCreatedDate() {
     
        Datetime
            now        = Datetime.now(),
            yesterday  = now.addDays(-1),
            lastWeek   = now.addDays(-7),
            latestDate = null,
            epoch      = Datetime.newInstance(1970, 1, 1, 0, 0, 0);

        SyncEmployeesFromOrg62 batch = new SyncEmployeesFromOrg62();

        // Test with no leads in the database
        latestDate = batch.getLatestEmployeeCreatedDate();
        System.assertEquals(epoch, latestDate);

        // Test with leads in the database
        insert new INTEG_Employee__c[]{
            new INTEG_Employee__c(Name = 'A', INTEG_Email__c = 'a@a.com', SupportForceContactCreatedDate__c = lastWeek),
            new INTEG_Employee__c(Name = 'B', INTEG_Email__c = 'b@b.com', SupportForceContactCreatedDate__c = yesterday)
        };

        latestDate = batch.getLatestEmployeeCreatedDate();
        System.assertEquals(yesterday, latestDate);
    }
}
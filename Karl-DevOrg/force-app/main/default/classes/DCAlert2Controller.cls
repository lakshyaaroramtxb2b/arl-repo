public with sharing class DCAlert2Controller {
    
    private final Detection_Alert__c DCAlert;
    
    public DCAlert2Controller()
    {
        DCAlert = [SELECT Id, Name FROM Detection_Alert__c
				 WHERE Id = :ApexPages.currentPage().getParameters().get('id')];
    }
    
    public Detection_Alert__c getDCAlert() {
        return DCAlert;
    }

}
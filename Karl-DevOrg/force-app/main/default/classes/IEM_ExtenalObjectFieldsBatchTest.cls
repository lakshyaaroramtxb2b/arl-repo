@isTest
private class IEM_ExtenalObjectFieldsBatchTest {
    @testSetup 
    static void setup() {
        List<Case> cases = new List<Case>();
        // insert 10 cases
        for (Integer i=0;i<10;i++) {
            Case newcase = new Case();
            newcase.RecordTypeId = IEM_Constants.IEM_CASERECORDTYPEID;
            newcase.Cloud__c = 'EntSecTools';
            newcase.Description = 'Test';
            newcase.Team_Responsible__c = 'TRUST Enterprise Security Tools Development';
            newcase.Subject = 'Test Subject';
            cases.add(newcase);
        }
        insert cases;
    }
    @isTest
    static void ExternalFieldUpdateTest() {        
        Test.startTest();
        IEM_ExtenalObjectFieldsBatch uca = new IEM_ExtenalObjectFieldsBatch();
        Id batchId = Database.executeBatch(uca);
        Test.stopTest();
    }
    
}
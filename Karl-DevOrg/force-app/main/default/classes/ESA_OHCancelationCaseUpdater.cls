public class ESA_OHCancelationCaseUpdater {
    
    
    public static void caseUpdater (List <Esa_Security_Request__c> requestsL){
        
        
        Set <Id> reqIds = new Set <Id>();
        
        for (Esa_Security_Request__c req: requestsL){
            reqIds.add(req.Id);   
        }
        
       
        List <Case> requestCaseL = new List <Case> ();
        Map <Id, Esa_Security_Request__c> requestMap = new Map <Id, Esa_Security_Request__c> (requestsL);
        
        for (Case cs: [SELECT Id, Cancel_Office_Hours__c, ESA_Security_Request__c
                       FROM Case 
                       WHERE ESA_Security_Request__c IN: reqIds]){
            
            Esa_Security_Request__c req = requestMap.get(cs.ESA_Security_Request__c);
            if (req.Office_Hours_Reservation__c == null){
                
                cs.Cancel_Office_Hours__c = false;
                requestCaseL.add(cs);
            }
            
            
        }
        update requestCaseL;
        

        
    }
    

}
global with sharing class ScanQueueBacklogChecker {
    
    public static void run(BatchNotifier bn) { 
    
        Backlog_Configuration__c config = [SELECT Max_Working_Queue_Size__c,
                                               Max_Backlog_Size__c,
                                               Max_Unworked_Scan_Hours__c,
                                               Max_Allowed_Scan_Hours__c,
                                               Max_Queue_Wait_Hours__c,
                                               StuckScanDelay__c,
                                               Scan_Failure_Retry_Delay__c
                                            FROM Backlog_Configuration__c
                                            WHERE Active__c = True LIMIT 1];    

       
        //submit error
        Scan_Queue__c [] submitError = [SELECT Name, LastModifiedDate, CreatedDate, Status__c, Comments__c, Scan_Start__c, scanner_Id__c, guid__c FROM Scan_Queue__c WHERE Status__c='Paused-Submit' 
                                        AND (Comments__c='Missing xml file' OR Comments__c = 'stuck scan')
                                        AND LastModifiedDate < :Datetime.now().addHours(0-Math.round(config.Scan_Failure_Retry_Delay__c))
                                        AND CreatedDate = LAST_N_DAYS:14 LIMIT 20];
                                        
        //result Error        
        Scan_Queue__c [] resultError = [SELECT Name, LastModifiedDate, CreatedDate, Status__c, Comments__c, Scan_Start__c, scanner_Id__c, guid__c FROM Scan_Queue__c WHERE (Status__c='Paused-Results' or Status__c='Paused-Status')
                                        AND Comments__c='Missing xml file' 
                                        AND LastModifiedDate < :Datetime.now().addHours(0-Math.round(config.Scan_Failure_Retry_Delay__c))
                                        AND CreatedDate = LAST_N_DAYS:14 LIMIT 20];

        //failed Scans        
        Scan_Queue__c [] failedScans = [SELECT Name, LastModifiedDate, CreatedDate, Status__c, Comments__c, Scan_Start__c, scanner_Id__c, guid__c FROM Scan_Queue__c WHERE Status__c='Failed' 
                                        AND Comments__c='Failed' 
                                        AND LastModifiedDate < :Datetime.now().addHours(0-Math.round(config.Scan_Failure_Retry_Delay__c))
                                        AND CreatedDate = LAST_N_DAYS:14 LIMIT 20];        
        
        // too many scan in queue        
        Scan_Queue__c [] queueLen    = [SELECT Name,CreatedDate,Id,Status__c FROM Scan_Queue__c WHERE Status__c = 'Waiting to scan' AND CreatedDate = LAST_N_DAYS:14];   

        // scans (not started) in queue for too long
        Scan_Queue__c [] tooOldUnworked   = [SELECT Name,CreatedDate,Id,Status__c FROM Scan_Queue__c WHERE Status__c = 'Waiting to scan'
                                                           AND CreatedDate = LAST_N_DAYS:14 AND CreatedDate <: Datetime.now().addHours(0-Math.round(config.Max_Unworked_Scan_Hours__c))];
        
        // scans that are active and running for too long (since when they were added to the queue - not since they started)
        Scan_Queue__c [] overlyLongScan   = [SELECT Name,CreatedDate,Scan_Start__c,Id,Status__c FROM Scan_Queue__c WHERE Status__c ='Waiting for scan to finish'
                                                           AND CreatedDate = LAST_N_DAYS:14 AND Scan_Start__c <: Datetime.now().addHours(0-Math.round(config.Max_Allowed_Scan_Hours__c))];

         
         if (failedScans.size() > 0) {
             bn.addNotifier(Notifier.Severity.ERROR, 'ScanQueueBacklogChecker : Found failed scan list of length ' + String.valueOf(failedScans.size()) + ' that were failed at least ' + String.valueOf(config.Scan_Failure_Retry_Delay__c) + ' hours.', makeExtra(failedScans));
             }
             
         if (submitError.size() > 0) {
             bn.addNotifier(Notifier.Severity.ERROR, 'ScanQueueBacklogChecker : Reverting Paused-Submit scan list of length ' + String.valueOf(failedScans.size()) + ' that were failed at least ' + String.valueOf(config.Scan_Failure_Retry_Delay__c) + ' hours.', makeExtra(submitError));
             revert(submitError);
             }
         if (resultError.size() > 0) {
             bn.addNotifier(Notifier.Severity.ERROR, 'ScanQueueBacklogChecker : Reverting Paused-Result scan list of length ' + String.valueOf(failedScans.size()) + ' that were failed at least ' + String.valueOf(config.Scan_Failure_Retry_Delay__c) + ' hours.', makeExtra(resultError));
             revert(resultError);
             }          
             
         if (queueLen.size() > config.Max_Backlog_Size__c) {
             bn.addNotifier(Notifier.Severity.ERROR,'ScanQueueBacklogChecker : Unstarted Scan queue length of '+String.valueOf(queueLen.size())+' exceeds max size of '+String.valueOf(config.Max_Backlog_Size__c),makeExtra(queueLen));
         }
         
         if (tooOldUnworked.size() > 0) {
             bn.addNotifier(Notifier.Severity.ERROR,'ScanQueueBacklogChecker : ' + String.valueOf(tooOldUnworked.size())+' scan(s) waiting for scan start longer than allowable '+String.valueOf(config.Max_Unworked_Scan_Hours__c)+' hours',makeExtra(tooOldUnworked));
         }

         if (overlyLongScan.size() > 0) {
             bn.addNotifier(Notifier.Severity.ERROR,'ScanQueueBacklogChecker : ' + String.valueOf(overlyLongScan.size())+' scan(s) running longer than allowable '+String.valueOf(config.Max_Allowed_Scan_Hours__c)+' hours',makeExtra(overlyLongScan));
         }

         
         bn.addNotifier(Notifier.Severity.DEBUG,'ScanInfoBacklogChecker : complete:'
                                                        + ' queueLen['+String.valueOf(queueLen.size())+']'
                                                        + ' tooOldUnworked['+String.valueOf(tooOldUnworked.size())+']'
                                                        + ' overlyLongScan['+String.valueOf(overlyLongScan.size())+']'
                                                        + ' failedScansLen[ '+ String.valueOf(failedScans.size()) + ']'                   
                                                        );
    }
   
  
    private static List<String> makeExtra(List<Scan_Queue__c> objs) {
        List<String> ret = new List<String>();
        for (Scan_Queue__c c : objs) {
            ret.add(c.CreatedDate.format() + '  ' + c.Name + '  ' + c.Status__c);
        }
        return ret;
    }
    
     private static void revert(List<Scan_queue__c> objs) {
        for (Scan_Queue__c  c : objs) {
            
            if (c.Status__c == 'Paused-Submit') {
                //we need to reset these fields so the job is not associated to a scanner
                c.Status__c = 'Waiting to scan';
                c.Scan_Start__c = null;
                c.scanner_Id__c = null;
                c.guid__c = null;
                c.project_name__c = null;
                c.Comments__c = 'auto-resubmitted from Paused-Submit state';

                }
            if (c.Status__c == 'Paused-Result' || c.Status__c == 'Paused-Status') {
                //do NOT reset the fields, as we want to pull the results from the scanner again
                //and so need the guid and scanner info
                c.Status__c = 'Scanning';
                c.Comments__c = 'auto-resubmitted from Paused-Result state';
                }
        }
             
        update objs;

        return;
    }
    
}
public with sharing class CDI_ExportExcel {
    
    public CDI_Data_Asset__c[] dataassets {get;set;}
    public Integer dataassetSize {get;set;}
    public String cryptoalgorithmvalue {get;set;}
    public String datastorevalue {get;set;}
    public String dataclassificationvalue {get;set;}
    public String datacategoryvalue {get;set;}
    public String dataidentificationcategoryvalue {get;set;}
    public String basisforprocessingvalue {get;set;}
    public String timeunitvalue {get;set;}
    public String retentionprocessvalue {get;set;}
    public String customerdatacategoryvalue {get;set;}
    public String gusteamvalue {get;set;}
    public String xmlheader {get;set;}
    public String endfile{get;set;}
    public List<wrapper> lstwrapper {get; set;}
    public class wrapper{
        public string name {get; set;}
        public string description{get; set;} 
        public string dataAssetType{get;set;}
        public string parentDataAsset{get; set;}
        public string sourcesystemname{get; set;}
        public string sourcedataassetcode{get; set;}
        public string linktobalancingarguments{get; set;}
        public string linktoexcpnforbasisforprocessing{get; set;}
        public boolean isEncrypted{get; set;}
        public decimal dataRetentionPeriod{get; set;}
        public boolean canconsumeraccessdata{get; set;}
        public string dataClassification{get; set;}
        public string dataCategory{get; set;}
        public string dataStore{get; set;}
        public string dataIdentificationCategory{get; set;}
        public string customerDataCategory{get; set;}
        public string cryptoAlgorithm{get; set;}
        public boolean isPersonalDataLimitedToPurpose{get; set;}
        public boolean isDeleted{get; set;}      
        public string basisForProcessing{get; set;}
        public boolean hasDPIABeenPerformed{get; set;}
        public string timeUnit{get; set;}
        public string retentionProcess{get; set;}
        public string gusTeam{get; set;}
        public string status{get; set;}
        public string comments{get; set;}
    }
    
    public CDI_ExportExcel() {
        String currentdatastoreid=ApexPages.CurrentPage().getparameters().get('datastoreId');
        xmlheader ='<?xml version="1.0" encoding="UTF-8"?><?mso-application progid="Excel.Sheet"?>';
        endfile = '</Workbook>';
        dataassets = [SELECT Name,Data_Asset_Description__c,Data_Asset_Type__c,Parent_Data_Asset__r.Name,Data_Retention_Period__c,Source_System_Name__c,Source_Data_Asset_Code__c,Link_To_Balancing_Argument_Document__c,Link_To_Excepn_For_Basis_For_Processing__c,IsEncrypted__c,Is_Personal_Data_Limited_To_Purpose__c,Has_DPIA_Been_Performed__c,Can_Consumers_Access_Data__c,Data_Classification__r.Name,Data_Category__r.Name,Data_Identification_Category__r.Name,Crypto_Algorithm__r.Name,Data_Store__r.Name,Basis_For_Processing__r.Name,Time_Unit__r.Name,Retention_Process__r.Name,Customer_Data_Category__r.Name,GUS_Team__r.Name,IsDeleted__c,Status__c,Comments__c From CDI_Data_Asset__c WHERE Data_Store__c =: currentdatastoreid WITH SECURITY_ENFORCED];
        dataassetSize = dataassets.size();
        cryptoalgorithmvalue='';
        lstwrapper=new List<wrapper>();
        List<CDI_Crypto_Algorithm__c>  cryptoalgorithms=[SELECT Name From CDI_Crypto_Algorithm__c];
        IF(cryptoalgorithms.size()==0)
        {
            cryptoalgorithmvalue=',';
        }
        else {
            for(CDI_Crypto_Algorithm__c ca:cryptoalgorithms)
            {
                
                System.debug(ca.Name);
                cryptoalgorithmvalue=cryptoalgorithmvalue+','+ca.Name;
            }
        }
        datastorevalue='';
        List<CDI_Data_Store__c>  datastores=[SELECT Name From CDI_Data_Store__c];
        if(datastores.size()==0)
        {
            datastorevalue=',';
        }
        else
        {
            for(CDI_Data_Store__c ds:datastores)
            {
                System.debug(ds.Name);
                datastorevalue=datastorevalue+','+ds.Name;
            }
        }
        dataclassificationvalue='';
        List<CDI_Data_Classification__c>  dataclassifications=[SELECT Name From CDI_Data_Classification__c];
        if(dataclassifications.size()==0)
        {
            dataclassificationvalue=',';
        }
        else{
            for(CDI_Data_Classification__c dc:dataclassifications)
            {
                System.debug(dc.Name);
                dataclassificationvalue=dataclassificationvalue+','+dc.Name;
            }
        }
        
        datacategoryvalue='';
        List<CDI_Data_Category__c>  datacategorys=[SELECT Name From CDI_Data_Category__c];
        if(datacategorys.size()==0)
        {
            datacategoryvalue=',';
        }
        else
        {
            for(CDI_Data_Category__c cdc:datacategorys)
            {
                System.debug(cdc.Name);
                datacategoryvalue=datacategoryvalue+','+cdc.Name;
            }
        }
        
        dataidentificationcategoryvalue='';
        List<CDI_Data_Identification_Category__c>  dataidentifiationcategorys=[SELECT Name From CDI_Data_Identification_Category__c];
        if(dataidentifiationcategorys.size()==0)
        {
            dataidentificationcategoryvalue=',';
        }
        else
        {
            for(CDI_Data_Identification_Category__c cdic:dataidentifiationcategorys)
            {
                System.debug('identification:;;;;'+cdic.Name);
                dataidentificationcategoryvalue=dataidentificationcategoryvalue+','+cdic.Name;
            }
        }
        
        basisforprocessingvalue='';
        List<CDI_Basis_For_Processing__c>  basisforprocessings=[SELECT Name From CDI_Basis_For_Processing__c];
        if(basisforprocessings.size()==0)
        {
            basisforprocessingvalue=',';
        }
        else
        {
            for(CDI_Basis_For_Processing__c bfp:basisforprocessings)
            {
                System.debug(bfp.Name);
                basisforprocessingvalue=basisforprocessingvalue+','+bfp.Name;
            } 
        }
        
        timeunitvalue='';
        List<CDI_Time_Unit__c>  timeunits=[SELECT Name From CDI_Time_Unit__c];
        if(timeunits.size()==0)
        {
            timeunitvalue=',';
        }
        else
        {
            for(CDI_Time_Unit__c tu:timeunits)
            {
                System.debug(tu.Name);
                timeunitvalue=timeunitvalue+','+tu.Name;
            } 
        }
        
        retentionprocessvalue='';
        List<CDI_Retention_Process__c>  retentionprocess=[SELECT Name From CDI_Retention_Process__c];
        if(retentionprocess.size()==0)
        {
            retentionprocessvalue=',';
        }
        else
        {
            for(CDI_Retention_Process__c rp:retentionprocess)
            {
                System.debug(rp.Name);
                retentionprocessvalue=retentionprocessvalue+','+rp.Name;
            }  
        }
        
        customerdatacategoryvalue='';
        
        List<CDI_Customer_Data_Category__c>  customerdatacategories=[SELECT Name From CDI_Customer_Data_Category__c];
        if(customerdatacategories.size()==0)
        {
            customerdatacategoryvalue=',';
        }
        else
        {
            for(CDI_Customer_Data_Category__c cdc:customerdatacategories)
            {
                System.debug(cdc.Name);
                customerdatacategoryvalue=customerdatacategoryvalue+','+cdc.Name;
            } 
        }
        
        gusteamvalue='';
        List<CDI_GUS_Team__c>  gusteams=[SELECT Name From CDI_GUS_Team__c];
        if(gusteams.size()==0)
        {
            gusteamvalue=',';
        }
        else
        {
            for(CDI_GUS_Team__c gt:gusteams)
            {
                System.debug(gt.Name);
                gusteamvalue=gusteamvalue+','+gt.Name;
            } 
        }
        for(CDI_Data_Asset__c asset :dataassets){
            wrapper w = new wrapper();
            w.name = asset.Name ;
            w.description=asset.Data_Asset_Description__c==null?'':asset.Data_Asset_Description__c.contains('"')?asset.Data_Asset_Description__c.replaceAll('"',''):asset.Data_Asset_Description__c;
            w.dataAssetType=asset.Data_Asset_Type__c;
            w.parentDataAsset = asset.Parent_Data_Asset__r.Name;
            w.sourcesystemname=asset.Source_System_Name__c==null?'':asset.Source_System_Name__c.contains('"')?asset.Source_System_Name__c.replaceAll('"',''):asset.Source_System_Name__c;
            w.sourcedataassetcode=asset.Source_Data_Asset_Code__c==null?'':asset.Source_Data_Asset_Code__c.contains('"')?asset.Source_Data_Asset_Code__c.replaceAll('"',''):asset.Source_Data_Asset_Code__c;
            w.linktobalancingarguments=asset.Link_To_Balancing_Argument_Document__c==null?'':asset.Link_To_Balancing_Argument_Document__c.contains('"')?asset.Link_To_Balancing_Argument_Document__c.replaceAll('"',''):asset.Link_To_Balancing_Argument_Document__c;
            w.linktoexcpnforbasisforprocessing=asset.Link_To_Excepn_For_Basis_For_Processing__c==null?'':asset.Link_To_Excepn_For_Basis_For_Processing__c.contains('"')?asset.Link_To_Excepn_For_Basis_For_Processing__c.replaceAll('"',''):asset.Link_To_Excepn_For_Basis_For_Processing__c; 
            w.isEncrypted=asset.IsEncrypted__c;
            w.dataRetentionPeriod=asset.Data_Retention_Period__c;
            w.canconsumeraccessdata=asset.Can_Consumers_Access_Data__c;
            w.dataClassification=asset.Data_Classification__r.Name;
            w.dataCategory=asset.Data_Category__r.Name;
            w.dataStore=asset.Data_Store__r.Name;
            w.dataIdentificationCategory=asset.Data_Identification_Category__r.Name;
            w.customerDataCategory=asset.Customer_Data_Category__r.Name;
            w.cryptoAlgorithm=asset.Crypto_Algorithm__r.Name;
            w.isPersonalDataLimitedToPurpose=asset.Is_Personal_Data_Limited_To_Purpose__c;
            w.isDeleted=asset.IsDeleted__c;      
            w.basisForProcessing=asset.Basis_For_Processing__r.Name;
            w.hasDPIABeenPerformed=asset.Has_DPIA_Been_Performed__c;
            w.timeUnit=asset.Time_Unit__r.Name;
            w.retentionProcess=asset.Retention_Process__r.Name;
            w.gusTeam=asset.GUS_Team__r.Name;
            w.status=asset.Status__c;
            w.comments=asset.Comments__c==null?'':asset.Comments__c.contains('"')?asset.Comments__c.replaceAll('"',''):asset.Comments__c;
            lstwrapper.add(w);               
        }        
        
    }
}
public class UnupdatedInstanceAlertJob implements Schedulable {

    /* DESIGN NOTE:
      we only expect there to ever be 1 PreviousInstanceList at once. However, if something was to go wrong with this job that
      updates it, we might not notice for a long time, as we don't often expect new instances to come online. It's safer to
      handle a variable number of previousinstancelists just in case ;) (Also, before this job is ever run, we expect there to be 0!)
    */
    
    private static final String ALERTMSG = 'New instances have come online - consider pushing updated malware rules to them. The following instances are new:';
    
    public static void registerDailyJob(Integer hour, Integer minute) {
        String jobName = 'UnupdatedInstanceAlertJob';
        Type t = Type.forName(jobName);
        //Seconds Minutes Hours DayOfMonth Month DayOfWeek [Year]
        String cronFormat = '0 ' + minute + ' ' + hour + ' * * ? *';
        System.schedule(jobName + '-' + hour + ':' + minute,
                       cronFormat,
                       (Schedulable) t.newInstance());
    }
    
    public void execute(SchedulableContext sc) {
        executeInternal();
    }
    
    private void executeInternal() {    
        List<String> previousInstances = getPreviousInstances();
        Map<String, boolean> previousInstanceMap = new Map<String, boolean>();
        for(String inst : previousInstances) {
            previousInstanceMap.put(inst, true); //value is inconsequential, truthfully, as long as it's non-null
        }
        
        List<String> currentInstances = getCurrentInstances();
        List<String> newInstances = new List<String>();
        
        for(String inst : currentInstances) {
            if(previousInstanceMap.get(inst) == null) {
                newInstances.add(inst);
            }
        }
        
        alertNewInstances(newInstances);
        setPreviousInstances(currentInstances, newInstances);
    }
    
    private void alertNewInstances(List<String> newInstances) {
        if(newInstances.size() == 0) { 
            System.debug('No new instances to update.');
            return;
        }
        
        String msg = '';
        String prefix = '';
        
        //EMAIL ALERTS!!
        
        List<String> recipients = getEmailsToAlert();
        if(recipients.size() == 0) { return; }        
        
        Messaging.reserveSingleEmailCapacity(1);
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(recipients);
        mail.setSenderDisplayName('Security Org Support');
        mail.setSubject('New Instances Detected, Push Malware Rules');
        mail.setBccSender(false);
        mail.setPlainTextBody(ALERTMSG + '\n' + msg);
        mail.setHtmlBody(ALERTMSG + '<p>' + msg);
        
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail});
    }
    
    private List<String> getCurrentInstances() {
        return malwaredefense.MalwareRuleUtil.getAllInstances();
    }
    
    private List<String> getPreviousInstances() {
        List<PreviousInstanceList__c> pinstlists = [select Instances__c from PreviousInstanceList__c];
        List<String> insts = new List<String>();
        
        for(PreviousInstanceList__c p : pinstlists) {
            if(p.Instances__c == null) { continue; }
            List<String> pinstlist = p.Instances__c.split(',');
            for(String i : pinstlist) {
                insts.add(i);
            }
        }
        
        return insts;
    }
    
    private void setPreviousInstances(List<String> instanceList, List<String> newInstances) {        
        String instList = String.join(instanceList, ',');
        String message = '';
        if(newInstances.size() > 0) { message = ALERTMSG + ' \n' + String.join(newInstances, ', '); }
        List<PreviousInstanceList__c> pinstlists = [select id, newInstanceAlert__c from PreviousInstanceList__c];
        
        if(pinstlists.size() == 1) {
            PreviousInstanceList__c p = pinstlists.get(0);
            p.Instances__c = instList;
            if(p.newInstanceAlert__c != null && p.newInstanceAlert__c.length() != 0) { 
                message = p.newInstanceAlert__c + ' \n' + message;
            }
            p.newInstanceAlert__c = message;
            
            update p;
        } else { //maintain that we have only 1 previous instance list
            for(PreviousInstanceList__c q : pinstlists) {
                if(q.newInstanceAlert__c != null && q.newInstanceAlert__c.length() != 0) { 
                	message = q.newInstanceAlert__c + ' \n' + message;
            	}
            }
            
            if(pinstlists.size() > 0) { delete pinstlists; }            
            
            PreviousInstanceList__c p = new PreviousInstanceList__c(Instances__c = instList, newInstanceAlert__c = message);
            insert p;
        }       
    }
    
    private List<String> getEmailsToAlert() {
        List<String> emails = new List<String>();
        
		PermissionSet malwareSignaturesPSet = [Select Id, Label from PermissionSet where Label = 'malwareSignatures'].get(0);
        List<PermissionSetAssignment> psetassigns = [select Assignee.Email from PermissionSetAssignment where PermissionSetId = :malwareSignaturesPSet.id];    
        for(PermissionSetAssignment psa : psetassigns) { emails.add(psa.Assignee.Email); }
        
        return emails;
    }
}
global with sharing class DeleteCustEntities implements Database.Batchable<sObject>, Database.Stateful{
    public String query;
    global Database.QueryLocator start(Database.BatchableContext bc){
        //return Database.getQueryLocator('select id,organization_id__c from guests_public_groups__c');
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext bc, List<SObject> scope){
        /*List<ID> ids = new List<ID>();
        for(SObject so : scope){
            ids.add((ID)so.get('id'));
        }
        system.debug('size: '+ids.size());
        database.delete(ids,false);*/
        Database.DeleteResult[] drList = Database.delete(scope, false);
        system.debug(drList);
    }
    global void finish(Database.BatchableContext bc){}
}
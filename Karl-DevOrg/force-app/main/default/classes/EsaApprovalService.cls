/**
 * Class for Bulk Approval Provision for managers
 */
public class EsaApprovalService {

    public EsaApprovalService(){

    }

    public List<clsApprovals> getApprovalRequests(String status){

        if(status == null) return null;

        List<ProcessInstanceWorkitem> lstProcessWorkItems = new List<ProcessInstanceWorkitem>();
        Id userId = UserInfo.getUserId();
        String targetObjectName = 'ESA';
        String queryStr = 'SELECT Id, Actor.Name, OriginalActor.Name, ProcessInstanceId, ProcessInstance.Status, ';
        queryStr = queryStr + 'ProcessInstance.LastActor.Name, ProcessInstance.TargetObjectId ';
        queryStr = queryStr + 'FROM ProcessInstanceWorkitem ';
        queryStr = queryStr + 'WHERE ';//ProcessInstance.TargetObject.Name  LIKE  \'' + targetObjectName + '%\' AND ';
        queryStr = queryStr + 'ProcessInstance.Status =  \'' + status + '\' AND ';
        queryStr = queryStr + 'ActorId =  \'' + userId + '\'';

        lstProcessWorkItems = Database.Query(queryStr);

        set<Id> inTakeRequestIds = new Set<Id>();
        for(ProcessInstanceWorkitem pItem : lstProcessWorkItems) {
           inTakeRequestIds.add(pItem.ProcessInstance.TargetObjectId);
        }

        List<Esa_Security_Request__c> lstInTakeRequests = New List<Esa_Security_Request__c>();
        lstInTakeRequests = [SELECT Service_Item_Name__c,
                                     Tracked_Number_01__c,
                                     Tracked_String_01__c,
                                     Name__c,
                                     Approver_Info__c,
                                     Approval_Status__c
                                     FROM Esa_Security_Request__c
                                     WHERE Id IN : inTakeRequestIds
                                     ORDER BY Name__c];

        Map<Id,Esa_Security_Request__c> mapIntakeRequets = new Map<Id,Esa_Security_Request__c>();
        Set<String> beHalfOfEmails = new Set<String>();

        for(Esa_Security_Request__c inTakeRequest : lstInTakeRequests) {
            mapIntakeRequets.put(inTakeRequest.Id, inTakeRequest);
            if(inTakeRequest.Tracked_String_01__c != null) {
                beHalfOfEmails.add(inTakeRequest.Tracked_String_01__c); 
            }
        }

        //Get list of Behalf of user names.
        List<User> lstUsers = [SELECT Name,
                                      Email 
                               FROM User 
                               WHERE Email IN :beHalfOfEmails];

        Map<String,String> mapBehalfOfNames = new Map<String,String>();
        for(User usr : lstUsers){
            mapBehalfOfNames.put(usr.Email, usr.Name);
        }

        List<clsApprovals> lstApprovals = new List<clsApprovals>();
        clsApprovals clsApproval;
        
        Esa_Security_Request__c esaRequest;
        String beHalfOfName;

        for(ProcessInstanceWorkitem pItem : lstProcessWorkItems) {
            esaRequest = mapIntakeRequets.get(pItem.ProcessInstance.TargetObjectId);
            if(esaRequest.Tracked_String_01__c != null) {
                beHalfOfName =  mapBehalfOfNames.get(esaRequest.Tracked_String_01__c);  
            }else{
                beHalfOfName = null;
            }

            
           lstApprovals.add(new clsApprovals(pItem, esaRequest, beHalfOfName));
        }
        
        lstApprovals.sort();

        return lstApprovals;
    }

    public List<clsApprovals> getPendingApprovalRequests(){
        return null;
    }

    public List<clsApprovals> getRejectedApprovalRequests(){
        return null;
    }

    public List<clsApprovals> getApprovedRequests(){
        return null;
    }

    public void ApproveOrReject(String action, String comments, String processWorkItemId) {
        Approval.ProcessWorkitemRequest approvalRequest = New Approval.ProcessWorkitemRequest();
        approvalRequest.setAction(action);
        approvalRequest.setComments(comments);
        approvalRequest.setWorkitemId(processWorkItemId);
        System.debug('#############'+approvalRequest);
        Approval.ProcessResult result = Approval.Process(approvalRequest);
        System.debug('^^^^^^^^^^^^^^'+processWorkItemId);
        ProcessInstanceWorkitem pWorkItem;
        /*pWorkItem = [SELECT Actor.Name,
                            OriginalActor.Name,
                            ProcessInstanceId,
                            ProcessInstance.Status,
                            ProcessInstance.LastActor.Name,
                            ProcessInstance.TargetObjectId
                    FROM ProcessInstanceWorkitem
                    WHERE Id = :processWorkItemId];
        
        System.debug('%%%%%%%%%%%%%'+pWorkItem);
        Esa_Security_Request__c securityRequest = [SELECT Service_Item_Name__c,
                                                          Tracked_Number_01__c,
                                                          Name__c,
                                                          Approver_Info__c,
                                                          Approval_Status__c
                                                   FROM Esa_Security_Request__c
                                                   WHERE Id = :pWorkItem.ProcessInstance.TargetObjectId]; */

        //clsApprovals approval =  new clsApprovals(pWorkItem, securityRequest);

        //return approval;
    }

  public class clsApprovals implements Comparable{
    @AuraEnabled
    public Esa_Security_Request__c securityRequest;
    @AuraEnabled
    public ProcessInstanceWorkItem processWorkItem;
    @AuraEnabled
    public String requestorName;
    @AuraEnabled
    public String beHalfOfName;

    public clsApprovals(){

    }

    public clsApprovals(ProcessInstanceWorkitem pItem, Esa_Security_Request__c inTakeRequest, String behalfOf){
        securityRequest = inTakeRequest;
        processWorkItem = pItem;
        requestorName = inTakeRequest.Name__c;
        beHalfOfName = behalfOf;
    }

    public Integer compareTo(Object compareTo) {
        clsApprovals compareToName = (clsApprovals)compareTo;
        if (requestorName == compareToName.requestorName) return 0;
        if (requestorName > compareToName.requestorName) return 1;
        return -1;
    }

  }

}
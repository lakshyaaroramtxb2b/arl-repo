public without sharing class CharterCounts {
    /* Very similar to ChartMeanTimeOverTime. We need a separate class since we often throw this into
     * execute anonymous and exec anonymous has size limitations
     */
     
    // just what it says
    public String extraWhere;
    
    public String objectName;
    public String fieldName1;
    public String fieldName2;
    
    // Bucket the events into these groups (hourly, daily, etc)
    public String period;
    
    // don't pull events before this day
    public Datetime startDate;
    
    // If we run this code via executeanonymous (as source), we can return a value so
    // we raise it as a JSON string in an exception. Horrible hack, I know
    public Boolean resultsAsException = false;

    // when manually providing records
    public SObject[] records;
    
    public void setRecords(SObject[] recs) {
        this.records = recs;
    }
    
    public Map<Datetime,Map<String,Integer>> countByDateSpanValueOverTime(String groupByFieldName) {
        /*
         * Group by month-within-date-span. SELECT count(id) FROM obj WHERE targetMonth WITHIN [DATETIME(field1), DATETIME(field2)] GROUP BY groupByFieldName
         *
         * Required parameters:
         *   objectName - name of the object to query
         *   fieldName1 - name of the start date field for the span
         *   fieldName2 - name of the end date field for the span
         *   groupByFieldName - name of the field whose values we're grouping by
         *   
         * Optional:
         *   startDate - don't query objects before this date. GMT
         *   period - compute mean after bucketing records into these groups. Hourly, daily, weekly, monthly, yearly. Default weekly
         *
         */
        if ((objectName == null) && (records == null)) {
            throw new CharterCountsException('Must supply object parameter');
        }
        if (fieldName1 == null) {
            throw new CharterCountsException('Must supply field1 parameter');
        }
        if (fieldName2 == null) {
            throw new CharterCountsException('Must supply field2 parameter');
        }

        // CRUD/FLS checks on the request. Will throw an exception if it 
        // doesn't work out
        checkAccess(objectName,new String[]{fieldName1,fieldName2});
        
        if (period == null) {
            period = 'monthly';
        }
        final Set<String> ALLOWED_PERIODS = new Set<String>{'hourly','daily','weekly','monthly','yearly'};
        if (!ALLOWED_PERIODS.contains(period)) {
            throw new CharterCountsException('Invalid period '+period);
        }
        
        // fieldname1 (e.g. CreatedDate) needs to not be null, otherwise we can't know the beginning of the date span.
        String sql = 'SELECT Id, '+fieldName1+','+fieldName2+','+groupByFieldName+' FROM '+objectName+' WHERE isDeleted=false AND '+fieldName1+' != null';
        
        if (startDate != null) {
            sql += ' AND CreatedDate >: startDate';
        }
        
        if (extraWhere != null) {
            sql += ' AND '+extraWhere;
        }
        
        sql += ' ORDER BY '+fieldName1;
        System.debug(sql);
        
        Set<String> uniqueGroupByValues = new Set<String>();
        // DateBucket => {groupbyfieldnamevalue => 123}
        Map<Datetime,Map<String,Integer>> res = new Map<Datetime,Map<String,Integer>>();
        
        if (records == null) {
            for (List<SObject> cl : Database.query(sql)) { 
                Map<Datetime,List<SObject>> bukkets = new Map<Datetime,List<SObject>>();
                // modify in place. Ugly but I don't want to do any copying                  
                dateSpanBucketize(bukkets,(List<SObject>)cl, fieldName1, fieldName2, period);
                
                Datetime now = Datetime.now();
                for (Datetime bucket : bukkets.keySet()) {
                    if (!res.containsKey(bucket)) {
                        // Sev1,Sev2,Sev3,Total
                        res.put(bucket,new Map<String,Integer>());
                    }
                    Map<String,Integer> workingBucket = res.get(bucket);
                    for (SObject c : bukkets.get(bucket)) {
                        String groupByFieldValue = String.valueOf(c.get(groupByFieldName));
                        uniqueGroupByValues.add(groupByFieldValue);
                        if (!workingBucket.containsKey(groupByFieldValue)) {
                            workingBucket.put(groupByFieldValue,1);
                        } else {
                            workingBucket.put(groupByFieldValue,workingBucket.get(groupByFieldValue)+1);
                        }
                    }
                }
            }
        } else {
            Set<SObject> toRemove = new Set<SObject>();
            Set<SObject> acceptable = new Set<SObject>(records);
            for (SObject o : records) {
                if (o.get(fieldName1) == null) {
                    // can't do anything with a record with no start date
                    // The soql version cleans this up in the query
                    toRemove.add(o);
                }
            }
            if (toRemove.size() > 0) {
                acceptable.removeAll(toRemove);
                records.clear();
                records.addAll(acceptable);
            }
            Map<Datetime,List<SObject>> bukkets = new Map<Datetime,List<SObject>>();
            // modify in place. Ugly but I don't want to do any copying                  
            dateSpanBucketize(bukkets,records, fieldName1, fieldName2, period);
            
            Datetime now = Datetime.now();
            for (Datetime bucket : bukkets.keySet()) {
                if (!res.containsKey(bucket)) {
                    // Sev1,Sev2,Sev3,Total
                    res.put(bucket,new Map<String,Integer>());
                }
                Map<String,Integer> workingBucket = res.get(bucket);
                for (SObject c : bukkets.get(bucket)) {
                    String groupByFieldValue = String.valueOf(c.get(groupByFieldName));
                    uniqueGroupByValues.add(groupByFieldValue);
                    if (!workingBucket.containsKey(groupByFieldValue)) {
                        workingBucket.put(groupByFieldValue,1);
                    } else {
                        workingBucket.put(groupByFieldValue,workingBucket.get(groupByFieldValue)+1);
                    }
                }
            }
        }
        for (Datetime bucket : res.keySet()) {
            for (String uniqueGroupingValue : uniqueGroupByValues) {
                if (!res.get(bucket).containsKey(uniqueGroupingValue)) {
                    res.get(bucket).put(uniqueGroupingValue,0);
                }
            }
        }
        if (resultsAsException) {
            throw new CharterCountsException(JSON.serialize(res));
        }
        return res;
    }
    
    public Map<Long,Map<String,Integer>> countByDateSpanValueOverTimeLong(String groupByFieldName) {
        boolean resAsExc = this.resultsAsException;
        this.resultsAsException = false;
        Map<Datetime,Map<String,Integer>> res = countByDateSpanValueOverTime(groupByFieldName);
        Map<Long,Map<String,Integer>> r = new Map<Long,Map<String,Integer>> ();
        for (Datetime k: res.keySet()) {
            r.put(k.getTime(),res.get(k));
        }
        if (resAsExc) {
            throw new CharterCountsException(JSON.serialize(r));
        }
        return r;
    }
    
    public void checkAccess(String objectName, String[] fieldNames) {
        Schema.SObjectType objType = Schema.getGlobalDescribe().get(objectName);
        if (objType == null) {
            // we now support non-SObjects
            return;
            //throw new CharterCountsException('Unknown object type '+objType);
        } else if (objType.getDescribe().isAccessible() == false) {
            throw new CharterCountsException('Unknown object type '+objType);
        }
        
        for (String f : fieldNames ) {
            Schema.SObjectField fieldObj = objType.getDescribe().fields.getMap().get(f);
            if (fieldObj == null) {
                throw new CharterCountsException('Unknown field '+f);
            }
            if (fieldObj.getDescribe().isAccessible() == false) {
                throw new CharterCountsException('Unknown field '+f);
            }
        }
    }
    
    
    
    public Map<Datetime,List<SObject>> dateSpanBucketize(Map<Datetime,List<SObject>> results,SObject[] objects, String startFieldName, String endFieldName, String period) {
        
        Datetime defaultStopDate;
        if (period == 'weekly') {
            defaultStopDate = closestSaturday(Datetime.now());
        } else if (period == 'monthly') {
            defaultStopDate = lastDateOfMonth(Datetime.now());
        } else if (period == 'yearly') {
            defaultStopDate = lastDateOfYear(Datetime.now());
        } else if (period == 'daily') {
            defaultStopDate = sameDay(Datetime.now());
        } else if (period == 'hourly') {
            defaultStopDate = sameHour(Datetime.now());
        }

        for (SObject o : objects) {
            Datetime startDate = (Datetime) o.get(startFieldName);
            Datetime bucket,stopDate;
            
            // Either the closed date or the end of the last period we're going to report (e.g. last day of this month)
            if (o.get(endFieldName) != null) { 
                stopDate = (Datetime) o.get(endFieldName);
            } else {
                stopDate = defaultStopDate;
            }
            
            // pretty sure all of this won't be accurate for leap-X's
            if (period == 'weekly') {
                bucket = closestSaturday(startDate);
                while (bucket <= stopDate) {
                    if (!results.containsKey(bucket)) {
                        results.put(bucket,new List<Sobject>());
                    }
                    results.get(bucket).add(o);
                    bucket = bucket.addDays(7);
                }
            } else if (period == 'monthly') {
                bucket = lastDateOfMonth(startDate);
                while (bucket <= stopDate) {
                    if (!results.containsKey(bucket)) {
                        results.put(bucket,new List<Sobject>());
                    }
                    results.get(bucket).add(o);
                    bucket = lastDateOfMonth(bucket.addDays(1));
                }
            } else if (period == 'yearly') {
                bucket = lastDateOfYear(startDate);
                while (bucket <= stopDate) {
                    if (!results.containsKey(bucket)) {
                        results.put(bucket,new List<Sobject>());
                    }
                    results.get(bucket).add(o);
                    bucket = lastDateOfYear(bucket.addDays(10));
                }
            } else if (period == 'daily') {
                bucket = sameDay(startDate);
                while (bucket <= stopDate) {
                    if (!results.containsKey(bucket)) {
                        results.put(bucket,new List<Sobject>());
                    }
                    results.get(bucket).add(o);
                    bucket = bucket.addDays(1);
                }
            } else if (period == 'hourly') {
                bucket = sameHour(startDate);
                while (bucket <= stopDate) {
                    if (!results.containsKey(bucket)) {
                        results.put(bucket,new List<Sobject>());
                    }
                    results.get(bucket).add(o);
                    bucket = bucket.addHours(1);
                }
            } else {
                throw new CharterCountsException('Unknown period '+period);
            }
        }
        return results;
    }
    public Datetime closestSaturday(Datetime subj) {
        Datetime sat;
        String dayOfWeekStr = subj.formatGMT('E');
        if (dayOfWeekStr == 'Sun') {
            sat = subj.addDays(6);
        } else if (dayOfWeekStr == 'Mon') {
            sat = subj.addDays(5);
        } else if (dayOfWeekStr == 'Tue') {
            sat = subj.addDays(4);
        } else if (dayOfWeekStr == 'Wed') {
            sat = subj.addDays(3);
        } else if (dayOfWeekStr == 'Thu') {
            sat = subj.addDays(2);
        } else if (dayOfWeekStr == 'Fri') {
            sat = subj.addDays(1);
        } else {
            sat = subj;
        }
        
        return datetime.newInstanceGmt(sat.yearGmt(),sat.monthGmt(),sat.dayGmt(),23,59,59);
    }
    public Datetime lastDateOfMonth(Datetime subj) {
        if (subj.dayGmt() == 31) {
            System.debug('lastDateOfMonthAtNoon: subject day of month is already the 31st. Light clone');
            // This is the last day. Make a light clone
            return Datetime.newInstanceGmt(subj.yearGmt(),subj.monthGmt(),31,23,59,59);
        }
        
        // This is stupid. I would think that asking for an invalid date (e.g. Feb 31) would
        // throw an exception, but it just overflows into the next month
        Datetime attempt;
        for (Integer i : new Integer[]{31,30,29,28}) {
            attempt = Datetime.newInstanceGmt(subj.yearGmt(),subj.monthGmt(),i,23,59,59);
            System.debug('lastDateOfMonthAtNoon: Trying: ');System.debug(attempt);
            if (attempt.monthGmt() == subj.monthGmt()) {
                return attempt;
            }
        }
        throw new CharterCountsException('Can\'t figure out how many days are in '+subj.formatGmt('M'));
    }
    public Datetime lastDateOfYear(Datetime subj) {
        return Datetime.newInstanceGmt(subj.yearGmt(),12,31,23,59,59);
    }
    public Datetime sameDay(Datetime subj) {
        return Datetime.newInstanceGmt(subj.yearGmt(),subj.monthGmt(),subj.dayGmt(),23,59,59);
    }
    public Datetime sameHour(Datetime subj) {
        return Datetime.newInstanceGmt(subj.yearGmt(),subj.monthGmt(),subj.dayGmt(),subj.hourGmt(),59,59);
    }
}
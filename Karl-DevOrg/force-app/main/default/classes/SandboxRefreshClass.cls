global class SandboxRefreshClass implements SandboxPostCopy {
  global void runApexClass(SandboxContext context) {
      System.debug('Hello Tester Pester ' + context.organizationId()
           + ' ' + context.sandboxId() + context.sandboxName());
           
  try {
      //Delete Production Credentials from Named Credential and External Data Source objects
      system.debug('****MetadataCleaner.wipeNamedCredentialAndExternalDataSourceLoginCredentials*****START');
      MetadataCleaner.wipeNamedCredentialAndExternalDataSourceLoginCredentials();
      system.debug('****MetadataCleaner.wipeNamedCredentialAndExternalDataSourceLoginCredentials*****END');
  } catch (Exception ex) { 
    system.debug('######'+ex.getMessage());
  }

  try {
      EmailScrambler.run('Contact', '*'); 
  } catch (Exception ex) { 
  system.debug('######'+ex.getMessage());
  }    

      
        // Delete Production Credentials from Managed Package.
      sfutil.ESASupportForceUtil.deleteSupportForceCred('SupportForce_Contact_Integ');
      sfutil.ESASupportForceUtil.deleteSupportForceCred('Org62-EmployeeSync');
      sfutil.ESASupportForceUtil.deleteSupportForceCred('SupportForce');
      sfutil.ESASupportForceUtil.deleteSupportForceCred('GUS');

      // Update Production Information with Custom Setting Info.
      
      delete [SELECT Id FROM ESA_Appointment_Override__c];
      
      List<ESA_Entity__c> lstEsaEntity = [SELECT Id, Google_Calendar_Name__c FROM ESA_Entity__c];
      for(ESA_Entity__c esa : lstEsaEntity) {
        esa.Google_Calendar_Name__c = 'salesforce.com_jj6lrn8e1gku6prvm97ch53m58@group.calendar.google.com';
      }
      update lstEsaEntity;

      List<ESA_External_Assignment__c> lstAssigns = [SELECT Id,
                                                          Assignment_Id__c ,
                                                          Assignment_RecordType_Id__c 
                                                     FROM ESA_External_Assignment__c
                                                     WHERE Assignment_Type__c = 'Supportforce Case Queue'];


      for(ESA_External_Assignment__c assign : lstAssigns) {
          assign.Assignment_RecordType_Id__c  = '0120000000001TgAAI';
          assign.Assignment_Id__c ='00G70000001OtrU';
      }

      update lstAssigns;

      lstAssigns = [SELECT Id,
                           Assignment_Id__c ,
                           Assignment_RecordType_Id__c 
                    FROM ESA_External_Assignment__c
                    WHERE Assignment_Type__c != 'Supportforce Case Queue'];


      for(ESA_External_Assignment__c assign : lstAssigns) {
          assign.Assignment_RecordType_Id__c  = '012B0000000JEojIAG';
          assign.Assignment_Id__c ='a0dB0000000XajK';
      }

      update lstAssigns;
      
      
    }
  }
/*
* controller to ask for a simple email and password before showing the CSAC-related content
*/

public with sharing class CSACContent {
    public String email		{get;set;}
    public String password	{get;set;}
    public boolean show		{get;set;}
    
    private List<csacmember__c> memberList;
    
    public CSACContent() {
    	
    }
/*
	public boolean getShow() {
		memberList = [select email__c, password__c from csacmember__c where email__c = :email and password__c = :password];
		if (memberList == null || memberList.size() < 1) {
			//system.debug('FALSE');
			password='';
			return false;
		}
		//system.debug('TRUE');
		//logme(memberList.get(0));
		return true;
	}
*/	
	public void logme(csacmember__c member) {
		csaclogin__c l = new csaclogin__c();
		l.csacmember__c = member.Id;
		insert l;
	}
	
	public PageReference go() {
		memberList = [select email__c, password__c from csacmember__c where email__c = :email and password__c = :password];
		if (memberList == null || memberList.size() < 1) {
			password='';
			show = false;
			return null;
		}
		//system.debug('TRUE');
		logme(memberList.get(0));
		show = true;
		return null;		
	}
}
public with sharing class TD_PopulateStakeholderName {
	
	public static void populateName (List<Trust_Deliverable_Stakeholder__c> TD_stakeholders) {
        Set<Id> stakeholderIds = new Set<Id>();
        for (Trust_Deliverable_Stakeholder__c tds : TD_stakeholders) stakeholderIds.add(tds.Stakeholder__c);
        
        Map<Id, User> stakeholders = new Map<Id, User>([select Id, Name from user where Id IN:stakeholderIds]);
        for (Trust_Deliverable_Stakeholder__c tds : TD_stakeholders) {
        	if (stakeholders.containsKey((tds.Stakeholder__c))) {
                tds.Name = stakeholders.get(tds.Stakeholder__c).Name;
        	} else {
                tds.Name = string.valueOf(stakeholders.get(tds.Stakeholder__c).Id);        		
        	}
        }		
	}

}
/*
* Created by - Himanshu Shrimali (MTX Group Inc.)
* Utility class for Supportforce Console
*/
global class SPT_Utility {

    // Data Members
    public static Map<String, String> statusValuesMap = new Map<String, String>();
    public static List<SPT_Case_Fields_Mapping__mdt> caseFieldMappingList = new List<SPT_Case_Fields_Mapping__mdt>([SELECT Id, MasterLabel, External_Object_Field_API__c
                                                                                                                    FROM SPT_Case_Fields_Mapping__mdt]);
    public static List<SPT_Status_Field_Mapping__mdt> statusPicklistMappingList = new List<SPT_Status_Field_Mapping__mdt>([SELECT MasterLabel, Security_Field_Value__c
                                                                                                                            FROM SPT_Status_Field_Mapping__mdt]);                                                                                                                    
    public static List<SPT_Case_Fields_Mapping__mdt> checkChangeList = new List<SPT_Case_Fields_Mapping__mdt>([SELECT Id, MasterLabel, External_Object_Field_API__c, Check_Change__c
                                                                                                                    FROM SPT_Case_Fields_Mapping__mdt
                                                                                                                    WHERE Check_Change__c = true]);
    
    /*
    * Created by - Himanshu Shrimali
    * Date - 25-11-2019
    * This method returns the maximum last modified date/time.
    */
    public static Datetime getMaxModifiedDateTimeValue() {
        Datetime recentlyModifiedDateTimeValue;
        List<Case> caseList = new List<Case>();
        
        caseList = [SELECT Id, LastModifiedDate
                    FROM Case
                    ORDER BY LastModifiedDate DESC LIMIT 1];
        if(caseList.size() > 0) {
            recentlyModifiedDateTimeValue = caseList.get(0).LastModifiedDate;
            return recentlyModifiedDateTimeValue;
        }
        return null;        
    }

    /*
    * Created by - Himanshu Shrimali
    * Date - 30-12-2019
    * This method returns the query to get external case records
    */
    public static String getExternalCaseRecords(String extraFields, String whereClause) {
        String fieldsToRetrieve = '';
        System.debug('CaseFieldMapping: '+caseFieldMappingList);
        for(SPT_Case_Fields_Mapping__mdt caseFieldMapping : caseFieldMappingList) {
            String fieldAPI = caseFieldMapping.External_Object_Field_API__c;
            fieldsToRetrieve += fieldAPI+', ';
        }
        System.debug('Fields To Retrieve: '+fieldsToRetrieve);
        fieldsToRetrieve = fieldsToRetrieve.removeEnd(', ');
        System.debug('Fields To Retrieve: '+fieldsToRetrieve);
        if(extraFields != null && extraFields != '') {
            extraFields = ', '+extraFields;
        }
        else {
            extraFields = '';
        }
        String query = 'SELECT ' + fieldsToRetrieve + extraFields + ' FROM Case__x '+whereClause;
        System.debug('SOQL Query: '+query);
        return query;
    } 

    /*
    * Created by - Himanshu Shrimali
    * Date - 30-12-2019
    * This method returns the query to get external case comment records
    */
    public static String getExternalCaseCommentRecords(String extraFields, String whereClause) {
        if(extraFields != null && extraFields != '') {
            extraFields = ', '+extraFields;
        }
        else {
            extraFields = '';
        }
        String query = 'SELECT Id, ExternalId, ParentId__c, CommentBody__c, CreatedById__c, CreatedDate__c, IsPublished__c, LastModifiedDate__c'+extraFields+' FROM CaseComment__x '+whereClause;
        return query;
    }

    /*
    * Created by - Himanshu Shrimali
    * Date - 09-01-2020
    * This method returns whether the query to get internal case records
    */
    public static String getInternalCaseRecords(String extraFields, String whereClause) {
        String fieldsToRetrieve = '';
        for(SPT_Case_Fields_Mapping__mdt caseFieldMapping : caseFieldMappingList) {
            String fieldAPI = caseFieldMapping.MasterLabel;
            fieldsToRetrieve += fieldAPI+', ';
        }
        fieldsToRetrieve = fieldsToRetrieve.removeEnd(', ');
        if(extraFields != null && extraFields != '') {
            extraFields = ', '+extraFields;
        }
        else {
            extraFields = '';
        }
        String query = 'SELECT ' + fieldsToRetrieve + extraFields + ' FROM Case '+whereClause;
        System.debug('QUErY: '+query);
        return query;
    }

    /*
    * Created by - Himanshu Shrimali
    * Date - 06-01-2020
    * This method returns whether the field value change should be checked or not
    */
    public static Boolean isFieldValueChanged(Case internalCaseRecord, Case__x externalCaseRecord) {
        for(SPT_Case_Fields_Mapping__mdt caseField : checkChangeList) {
            if(internalCaseRecord.get(caseField.MasterLabel) != externalCaseRecord.get(caseField.External_Object_Field_API__c)) 
                return true;
        }
        return false;
    }

    /*
    * Created by - Himanshu Shrimali
    * Date - 06-01-2020
    * This method sync the values of the internal and external case records.
    */
    public static void syncInternalAndExternalCaseRecords(Case internalCaseRecord, Case__x externalCaseRecord, Boolean internalFirst) {
        System.debug('Internal Record: '+internalCaseRecord);
        System.debug('External Record: '+externalCaseRecord);
        for(SPT_Status_Field_Mapping__mdt statusValue : statusPicklistMappingList) {
            statusValuesMap.put(statusValue.MasterLabel, statusValue.Security_Field_Value__c);
        }
        if(internalFirst) {
            for(SPT_Case_Fields_Mapping__mdt caseFields : caseFieldMappingList) {
                if(caseFields.MasterLabel == 'Status') {
                    internalCaseRecord.put(caseFields.MasterLabel, statusValuesMap.get(String.valueOf(externalCaseRecord.get(caseFields.External_Object_Field_API__c))));
                }
                else {
                    internalCaseRecord.put(caseFields.MasterLabel, externalCaseRecord.get(caseFields.External_Object_Field_API__c));
                }   
            }
        }
        else {
            for(SPT_Case_Fields_Mapping__mdt caseFields : caseFieldMappingList) {
                if(caseFields.MasterLabel == 'Status') {
                    externalCaseRecord.put(caseFields.External_Object_Field_API__c, statusValuesMap.get(String.valueOf(internalCaseRecord.get(caseFields.MasterLabel))));
                }
                // Skip the fields SPT_Contact_Cost_Center__c and Enterprise_Security_Case_Number__c
                else if(caseFields.MasterLabel != 'Description' && 
                        caseFields.MasterLabel != 'SPT_Contact_Cost_Center__c' && 
                        caseFields.MasterLabel != 'Enterprise_Security_Case_Number__c' &&
                        caseFields.MasterLabel != 'SPT_Communication_Flag__c'
                        ){
                    externalCaseRecord.put(caseFields.External_Object_Field_API__c, internalCaseRecord.get(caseFields.MasterLabel));
                }   
            }
        }
        System.debug('Internal Record Exit: '+internalCaseRecord);
        System.debug('External Record Exit: '+externalCaseRecord);
    }
    
    @InvocableMethod(Label='SPT_getPicklistValues')
    global static List<String> getPicklistValues(){
        List<String> picklistList = new List<String>();
        Schema.SObjectType s = Schema.getGlobalDescribe().get('Case') ;
        Schema.DescribeSObjectResult r = s.getDescribe() ;
        Map<String,Schema.SObjectField> fields = r.fields.getMap() ;
        Schema.DescribeFieldResult fieldResult = fields.get('Status').getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : ple){
            picklistList.add(pickListVal.getLabel());
        }    
        return picklistList;
    }
}
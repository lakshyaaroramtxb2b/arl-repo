/**
 * Class: TIAlertUtils
 *
 * Copyright (C) 2017 Salesforce
 *
 * Purpose: Set Task SLA Dates for associated Threat Incident Cases
 * 
 * Where Referenced: 
 *   Process Builder Flows TI - New Alert, TI - New FIT, TI - New Incident Escalation, TI - New RFI
 *
 * Change History:
 *
 * Developer                     Date        Flag                             Description
 * -----------------------------------------------------------------------------------------------------------------------------------------------------
 * Brian Clift                  09/11/2017   CSIR-8, CSIR-9, CSIR-10, CSIR-11 Set Task SLA Dates for associated Threat Incident Cases
 *
 * Incendiant, Inc
 * Fort Collins, CO
 * +1 970.825.2460
 *
 */

public with sharing class TIAlertUtils {
    // Constants
    public static final String THREAT_INTEL_BUSINESS_HOURS_LABEL = 'Threat Intel Standard Hours';
    
    @InvocableMethod (label='Set Task SLA Dates' description='Update Task SLA Notification Dates')    
    public static void setTaskSLADates(List<Id> parentIds){
    	List<Task> slaTasks = [ select SLA_1_Notification_Offset__c,
    	                               SLA_2_Notification_Offset__c,
    	                               SLA_Offset__c from
    	                               Task where  
    	                               whatId = :parentIds[0]  and
    	                               SLA_Offset__c != null ]; 
    	                               
        
        if (!slaTasks.isEmpty()) {
            List<BusinessHours> tiBusinessHours = [ select id from
                                                         BusinessHours where 
                                                         Name = :THREAT_INTEL_BUSINESS_HOURS_LABEL ];
        
            if (!tiBusinessHours.isEmpty()) {
            	for (Task theTask : slaTasks) {
            		theTask.SLA_Due_Date_Time__c = BusinessHours.add(tiBusinessHours[0].id, System.now(), (Long) theTask.SLA_Offset__c * (60  * 60000));
            		
            		if (theTask.SLA_1_Notification_Offset__c != null) {
            			theTask.SLA_1_Notification_Date__c = BusinessHours.add(tiBusinessHours[0].id, theTask.SLA_Due_Date_Time__c, (Long) theTask.SLA_1_Notification_Offset__c * (-60  * 60000));
            		}
            		
            		if (theTask.SLA_2_Notification_Offset__c != null) {
            			theTask.SLA_2_Notification_Date__c = BusinessHours.add(tiBusinessHours[0].id, theTask.SLA_Due_Date_Time__c, (Long) theTask.SLA_2_Notification_Offset__c * (-60  * 60000));
            		}            		
            	}
            	
            	Database.update(slaTasks);
            } else {
            	// Log an error that threat intel business hours are not set
            	System.debug('*** TI_Alert_Utils.setTaskSLADates: Appears that the business hours for "' + THREAT_INTEL_BUSINESS_HOURS_LABEL + '" is not described in the company profile');
            }
        }
                                                                                    
    }
}
/*
Developer: Ralph Callaway <ralph@callawaycloudconsulting.com>
Description:
    Utility methods for dynamic SOQL
*/
public class DynamicSOQLHelper {

    /* Constants */
    
    public static final String NULL_LIST = '(\'\')';
    public static final String NULL_STRING = '\'\'';

    /* Primary Formatting Helpers */
    
    // escapes single quotes and wraps with quotes
    public static String format(String input) {
        if(input == null)
            return NULL_STRING;
        return '\'' + String.escapeSingleQuotes(input) + '\''; 
    }
    
    // returns YYYY-MM-DD
    public static String format(Date input) {
        return input.year() 
            + '-' + (input.month() < 10 ? '0' : '') + input.month() 
            + '-' + (input.day() < 10 ? '0' : '') + input.day();
    }
    
    public static String format(DateTime input) {
        return input.format('yyyy-MM-dd\'T\'hh:mm:ss\'z\'');
    }

    // returns ('value1','value2')
    // filters out duplicate values (case sensitive)
    public static String format(List<String> input) {
        Set<String> inputSet = new Set<String>();
        inputSet.addAll(input);
        return format(inputSet);    
    }
    
    // returns ('value1','value2')
    public static String format(Set<String> input) {
        if(input == null || input.isEmpty())
            return '(\'\')';
        
        String temp = '';
        for(Object value : input) {
            temp += ',\'' + String.valueOf(value) + '\'';
        }
        return '(' + temp.subString(1) + ')';
    }
    
    /* Secondary Formatting Helpers */
    // functions that just cast argument into string type and call primary formatting function
    
    public static String format(Id input) {
        String temp = input;
        return format(temp);
    }
    
    public static String format(List<Id> input) {
        List<String> temp = new List<String>();
        for(Id value : input) {
            temp.add(value);
        }
        return format(temp); 
    }
    
    public static String format(Set<Id> input) {
        Set<String> temp = new Set<String>();
        for(Id value : input) {
            temp.add(value);
        }
        return format(temp);
    }
    
    /* Deprecated Methods */
    
    // Retained for backwards compatibility with older versions of this helper.  Delete
    // this section if deploying to a new org
    public static String formatDateForSOQL(Date input) { return format(input); }
    public static String formatListForSOQL(List<String> input) { return format(input); }
    public static String formatSetForSOQL(Set<String> input) { return format(input); }
    
}
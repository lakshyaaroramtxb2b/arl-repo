/**
 * @author Mahima Aggarwal
 * @email mahima.aggarwal@mtxb2b.com
 * @desc Test class for ARL_GUSCycleRequestItemSyncBatch
*/
@isTest
public class ARL_GUSCycleRequestItemSyncBatchTest {
    
    /**
     * @author Mahima Aggarwal
     * @email mahima.aggarwal@mtxb2b.com
     * @desc Test data setup.
    */
    @testSetup
    private static void testSetup(){
         KARL_Gus_Product_Tag__c defaultProductTag = ARL_TestDataFactory.createKarlGusProductTag(true,true);
        insert defaultProductTag;
        
        List<KARL_Auto_Assignment_Configuration__c> autoAssignmentList = ARL_TestDataFactory.createAutoAssignmentConfig(defaultProductTag.id,1);
        autoAssignmentList[0].Enable_Chatter_Posts__c = true;
        insert autoAssignmentList;
        
          Audit_Cycle__c cycleAudit = ARL_TestDataFactory.createAuditCycle();
        insert cycleAudit;
        
        Audit_Cycle_Coverage__c cycleCoverage = ARL_TestDataFactory.createAuditCycleCoverage(cycleAudit.id);
        cycleCoverage.Auto_Assignment_Configuration__c = autoAssignmentList[0].id;
        insert cycleCoverage;
        
        
        Cycle_Request_Item__c cycleReqItem = ARL_TestDataFactory.createCycleRequestItem(null,null);
        cycleReqItem.Team__c = 'test';
        cycleReqItem.Cycle_Request_Status__c = 'New';
        cycleReqItem.Request_Tech_Details__c = 'testTechDetail';
        cycleReqItem.Request_GUS__c = 'test';
        cycleReqItem.GUS_Status__c = 'New';
        cycleReqItem.Audit_Cycle_Coverage__c = cycleCoverage.id;
        insert cycleReqItem;
        
        KARL_Evidence_Request__c evidenceReq = new KARL_Evidence_Request__c();
        evidenceReq.KARL_Submitted__c = false;
        evidenceReq.KARL_Cycle_ARL_Item__c = cycleReqItem.Id;
        insert evidenceReq;
        
      
       
    }
    
    /**
     * @author Mahima Aggarwal
     * @email mahima.aggarwal@mtxb2b.com
     * @desc This method testing for updating Cycle Request Item from external GUS work record.
    */
	@isTest
    public static void testBatch(){
        Cycle_Request_Item__c critem1 = [Select id,GUS_Status__c,Team__c from Cycle_Request_Item__c limit 1];
        String CRON_EXP = '0 0 0 15 3 ? *';
        
        ADM_Work_c__x admWork = new ADM_Work_c__x();
        admWork.ExternalId = 'test';
        admWork.Status_c__c = 'In-Progress';
        admWork.Team__c = 'test';
        ARL_GUSCycleRequestItemSyncBatch.admWorkList.add(admWork);
        
        ADM_Scrum_Team_c__x scrumTeam = new ADM_Scrum_Team_c__x();
        scrumTeam.ExternalId = critem1.Team__c;
        scrumTeam.Product_Owner_c__c = '005B0000003SUUDIA0';
        scrumTeam.Scrum_Master_c__c = '005B0000003SUUDIA0';
        ARL_GUSCycleRequestItemSyncBatch.admscrunTeamList.add(scrumTeam);
        
        GusUser__x gusRec = new GusUser__x();
        gusRec.ExternalId = '005B0000003SUUDIA0';
        ARL_GUSCycleRequestItemSyncBatch.gusUserList.add(gusRec);
        
        
        Test.startTest();
            ARL_GUSCycleRequestItemSyncBatch obj = new ARL_GUSCycleRequestItemSyncBatch();
            DataBase.executeBatch(obj,1);
        	System.schedule('ARL_GUSCycleRequestItemSyncBatchTest',  CRON_EXP, new ARL_GUSCycleRequestItemSyncBatch()); 
        Test.stopTest();
        
        Cycle_Request_Item__c critem = [Select id,GUS_Status__c from Cycle_Request_Item__c limit 1];
        
        //Expecting GUS_Status__c as In-Progress because external object record has been updated.
        system.assertEquals('In-Progress', critem.GUS_Status__c);
    }
}
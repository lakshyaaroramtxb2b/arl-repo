/**
* @description Test class for PRT_ProjectRestResource
* @author virendra.yadav  | 1/8/2020 
**/
@isTest
public with sharing class PRT_ProjectRestResourceTest {
    @testSetup
    static void makeData(){
        List<User> userList = new List<User>();
        userList = PRT_TestDataFactory.createUsers(3,'System Administrator',false);
        for(integer i=0; i<2; i++){
            userList[i].EmployeeNumber = '77456';
            userList[i].FirstName = 'Test' + i;
            userList[i].LastName = 'Name' + i;
        }
        insert userList;
    }
    @isTest
	static void testDoGet(){        
		List<User> userRecord = new List<User>([SELECT Id FROM USER WHERE Email Like 'PRTUser%']);
        String userId = userRecord[0].id;
        String userId1 =userInfo.getUserId();
        String publishDate = String.valueOf(date.today());
        
        String v2MomJSON = '{"attributes":{"type":"V2MOM_c__x","url":"/services/data/v48.0/sobjects/V2MOM_c__x/0054D000001WzcyQAC"},"V2MOM_User_c__c":"'+ userId+'","ExternalId":"'+userId1+'","Name__c":"Test 123","Published_on_c__c":"'+publishDate+'"}';
        String measureJSON = '{"attributes":{"type":"Measure_c__x","url":"/services/data/v48.0/sobjects/Measure_c__x/0054D000001VnAMQA0"},"MeasureName_c__c":"test measure1","ExternalId":"'+userId1+'","V2MOM_c__c":"'+userId1+'"}';
        String methodJSON = '{"attributes":{"type":"V2MOM_Method_c__x","url":"/services/data/v48.0/sobjects/V2MOM_Method_c__x/0054D000001WzcyQAC"},"Name__c":"test method1","Description_c__c":"test desc","V2MOM_c__c":"'+userId1+'","ExternalId":"'+userId1+'"}';
        
       
        PRT_ProjectRestResource.mockallV2MOMList.add((V2MOM_c__x)JSON.deserialize(v2MomJSON,V2MOM_c__x.class ));
        PRT_ProjectRestResource.mockAllMeasureList.add((Measure_c__x)JSON.deserialize(measureJSON,Measure_c__x.class ));
        PRT_ProjectRestResource.mockAllMethodList.add((V2MOM_Method_c__x)JSON.deserialize(methodJSON,V2MOM_Method_c__x.class ));
        
        List<Project__c> projectList = new List<Project__c>();
        Id devRecordTypeId = Schema.SObjectType.Project__c.getRecordTypeInfosByName().get('Project').getRecordTypeId();
        Project__c noMeasureMethodproject = new Project__c();
		noMeasureMethodproject.Name ='Test Project';
        noMeasureMethodproject.recordtypeId = devRecordTypeId;
		projectList.add(noMeasureMethodproject);

        Project__c noV2MOMproject = new Project__c();
		noV2MOMproject.Name ='No v2mom Project';
        noV2MOMproject.V2MOM_Method__c = 'TEST';
        noV2MOMproject.recordtypeId = devRecordTypeId;
		projectList.add(noV2MOMproject);
        
        Project__c measureProject = new Project__c();
		measureProject.Name ='No v2mom measure Project';
        measureProject.V2MOM_Measure__c = 'TEST';
        measureProject.recordtypeId = devRecordTypeId;
		projectList.add(measureProject);
        insert projectList;

        RestRequest request = new RestRequest();
        request.requestUri =URL.getSalesforceBaseUrl().toExternalForm()+'/services/apexrest/ProjectValidation/'+noMeasureMethodproject.id;
        request.httpMethod = 'GET';		
        RestContext.request = request;
		String message = PRT_ProjectRestResource.doGet();
        
        request = new RestRequest();
        request.requestUri =URL.getSalesforceBaseUrl().toExternalForm()+'/services/apexrest/ProjectValidation/'+noV2MOMproject.id;
        request.httpMethod = 'GET';		
        RestContext.request = request;
		message = PRT_ProjectRestResource.doGet();
        
        request = new RestRequest();
        request.requestUri =URL.getSalesforceBaseUrl().toExternalForm()+'/services/apexrest/ProjectValidation/'+measureProject.id;
        request.httpMethod = 'GET';		
        RestContext.request = request;
		message = PRT_ProjectRestResource.doGet();
	}
}
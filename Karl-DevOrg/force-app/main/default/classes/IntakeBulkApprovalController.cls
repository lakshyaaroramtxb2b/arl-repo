/**
* Bulk Approval related API calls
*/
public with sharing class IntakeBulkApprovalController {
    private static EsaApprovalService approvalSvc = new EsaApprovalService();

    @AuraEnabled(cacheable=true)
    public static List<EsaApprovalService.clsApprovals> getAllApprovalRequests(String status) {
        switch on status {
            when 'Pending' {
                status = ESA_AppConstants.APPROVAL_PENDING_STATUS;
            }
            when else {
                status = ESA_AppConstants.APPROVAL_PENDING_STATUS; 
            }
        }  
        return approvalSvc.getApprovalRequests(status);
    }

    @AuraEnabled
    public static void approveRequest(String comment, String id) {
        approvalSvc.ApproveOrReject(ESA_AppConstants.APPROVAL_APPROVED_STATUS, comment, id);
    }

    @AuraEnabled
    public static void rejectRequest(String comment, String id) {
        approvalSvc.ApproveOrReject(ESA_AppConstants.APPROVAL_REJECTED_STATUS, comment, id);
    }
}
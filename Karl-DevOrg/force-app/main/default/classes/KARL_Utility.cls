/**
 * @author Mahima Aggarwal
 * @email mahima.aggarwal@mtxb2b.com
 * @description This class contains utility methods
 */
public with sharing class KARL_Utility {
    /**
     * @author Mahima Aggarwal
     * @email mahima.aggarwal@mtxb2b.com
     * @description To fetch picklist values
     * @param objectName - sObject name
     * @param fieldName - fieldname on the sObject
     * @return picklist options
     */
    @AuraEnabled
    public static List<SelectOptionWrapper> fetchPicklist(String objectName, String fieldName){
        List<KARL_Object__mdt> karlObjectList = new List<KARL_Object__mdt>();
        List<SelectOptionWrapper> opts = new List<SelectOptionWrapper>();
        List<Schema.PicklistEntry> ple = new List<Schema.PicklistEntry>();
        if(objectName != null || objectName != ''){
           
            for(KARL_Object__mdt karObjectRec: [SELECT Id, KARL_Object_Name__c 
                                                FROM KARL_Object__mdt WHERE KARL_Object_Name__c =:objectName ]){
                                 karlObjectList.add(karObjectRec);                   
                
            }
        }
        if(!karlObjectList.isEmpty()){
            
            
            Schema.SObjectType s = Schema.getGlobalDescribe().get(objectName) ;
            Schema.DescribeSObjectResult r = s.getDescribe() ;
            Map<String,Schema.SObjectField> fields = r.fields.getMap() ;
            Schema.DescribeFieldResult fieldResult = fields.get(fieldName).getDescribe();
             ple = fieldResult.getPicklistValues();  
        }
            
        
        for( Schema.PicklistEntry pickListVal : ple){
            opts.add( new SelectOptionWrapper( pickListVal.getValue(), pickListVal.getLabel())  );
        }
             return opts;
    }

    /**
     * @author Mahima Aggarwal
     * @email mahima.aggarwal@mtxb2b.com
     * @description update selected option
     */
    public class SelectOptionWrapper{
        @AuraEnabled public string value;
        @AuraEnabled public string label;
        
        /**
         * @author Mahima Aggarwal
         * @email mahima.aggarwal@mtxb2b.com
         * @description set values
         * @param value - value of picklist selection
         * @param label - label of picklist selection
         */
        public SelectOptionWrapper(string value, string label){
            this.value = value;
            this.label = label;
        }
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Return a list of the active Audit Teams
     * @return all audit teams
     */
    @AuraEnabled(cacheable=true)
    public static KARL_Audit_Team__c[] getAuditTeams(){
        return [SELECT Id, Name, KARL_Audit_Firm__r.Name, KARL_Active__c
        FROM KARL_Audit_Team__c 
        WHERE KARL_Active__c = true
        WITH SECURITY_ENFORCED
        ORDER BY Name ASC];
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description return a list of the Audit Cycles
     * @return all audit cycles
     */
    @AuraEnabled(cacheable=true)
    public static Audit_Cycle__c[] getAuditCycles(){
        return [SELECT Id, Name, Active__c
        FROM Audit_Cycle__c 
        WHERE Hide_from_Cycle_Picklists__c = false
        WITH SECURITY_ENFORCED
        ORDER BY Name DESC];
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Return a list of Scopes that exist (Global Value Set)
     * @return all scopes options in the global picklist set
     */
    @AuraEnabled(cacheable=true)
    public static Map<String, String> getAuditCycleScopes(){
        // Note that the LWC JS has post-processing to expand the Map into picklist values.
        Map<String, String> pickListValuesList = new Map<String, String>();

        Schema.DescribeFieldResult fieldResult = Request_Item__c.Primary_Scope__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : ple){
            pickListValuesList.put(pickListVal.getLabel(), pickListVal.getValue());
        }  
        return pickListValuesList;
    }

    /**
    * @author Ben Harvie
    * @email ben.harvie@salesforce.com
    * @description Return a list of NIST Control Families that exist (Global Value Set)
    * @return all control families in the global picklist set
    */
    @AuraEnabled(cacheable=true)
    public static Map<String, String> getControlFamilies(){
        // Note that the LWC JS has post-processing to expand the Map into picklist values.
        Map<String, String> pickListValuesList = new Map<String, String>();

        Schema.DescribeFieldResult fieldResult = Control__c.Family_Name__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : ple){
            pickListValuesList.put(pickListVal.getLabel(), pickListVal.getValue());
        }  
        return pickListValuesList;
    }
    
    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Get the Cycle Request Item records (limited to 5) for a particular REQ
     * @param recordId - the record Id (REQ) to look up
     * @return object including max 5 cycle request records
     */
    @AuraEnabled(cacheable=true)
    public static Cycle_Request_Item__c[] getCycleReqItemRecords(String recordId){
        // Conduct a SOQL query to return all Cycle Requests associated with the parent record
        // NOTE: Limit is set to 5 for page design considerations; we will still need to have the
        // related list available on the page layout for further historical records.
        return [SELECT Id, Name, Request__c, Audit_Cycle__r.Name, Cycle_Request_Status__c
        FROM Cycle_Request_Item__c 
        WHERE Request__c =: recordId AND ( Audit_Cycle__c != null OR Audit_Cycle__c != '' )
        WITH SECURITY_ENFORCED
        ORDER BY Name DESC LIMIT 5];
    }
    
    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Get the Evidence Requests for a particular Cycle Request
     * @param recordId - the record Id (CREQ) to look up
     * @return object including max 3 cycle request records
     */
    @AuraEnabled(cacheable=true)
    public static KARL_Evidence_Request__c[] getCreqEvidenceRecords(String recordId){
        return [SELECT Id, Name, KARL_Cycle_ARL_Item__c, KARL_Direct_Link__c, toLabel(Evidence_Status__c)
        FROM KARL_Evidence_Request__c 
        WHERE KARL_Cycle_ARL_Item__c =: recordId
        WITH SECURITY_ENFORCED
        ORDER BY Name DESC LIMIT 3];
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Get the Auditor Request Item records for a particular CREQ
     * @param recordId - the record Id (CREQ) to look up
     * @return object including all Auditor Request Items for a particular CREQ
     */
    @AuraEnabled(cacheable=true)
    public static KARL_Auditor_Request_Item__c[] getAuditorReqItemRecords(String recordId){
        // Conduct a SOQL query to return all Auditor Requests associated with the parent record
        return [SELECT Id, Name, KARL_Name__c, KARL_Cycle_Request_Item__c, KARL_Audit_Team__r.Name, KARL_Status__c
        FROM KARL_Auditor_Request_Item__c 
        WHERE KARL_Cycle_Request_Item__c =: recordId AND ( KARL_Audit_Team__c != null OR KARL_Audit_Team__c != '' )
        WITH SECURITY_ENFORCED
        ORDER BY Name DESC];
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Get the Controls Mapping of a particular request (REQ)
     * @param objectApi - the object used for searching, for object aware context
     * @param recordId - the record Id of the object to lookup, to retrieve the parent REQ
     * @return object of controls that the parent REQ is mapped to
     */
    @AuraEnabled(cacheable=true)
    public static Request_Item_Control__c[] getControlsMapping(String objectApi, String recordId){
        String targetRecord = getParentREQ(objectApi, recordId);

        return [SELECT Id, Name, Request__c, Control_Test_Name__c, Control_Test_Group__c, KARL_Control_Scope_Label__c, 
                Control_Scope__c, Full_Control_Scope__c, Test__c, KARL_Control_Name__c, Test__r.Control_Scope__r.KARL_Inactive__c
                FROM Request_Item_Control__c 
                WHERE Request__c =: targetRecord
                WITH SECURITY_ENFORCED
                ORDER BY KARL_Control_Name__c DESC];
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Get the Requirements Mapping of a particular request (REQ)
     * @param objectApi - the object used for searching, for object aware context
     * @param recordId - the record Id of the object to lookup, to retrieve the parent REQ
     * @return object of requirements that the parent REQ is mapped to
     */
    @AuraEnabled(cacheable=true)
    public static Request_Item_Area__c[] getRequirementsMapping(String objectApi, String recordId){
        String targetRecord = getParentREQ(objectApi, recordId);

        //System.debug(LoggingLevel.DEBUG, 'Target Record >> ' + targetRecord);
        return [SELECT Id, Name, Request_Master_Data__c, Requirement_ID__c, Area_Requirement__c, 
        Area_of_Compliance__c, KARL_Area_Title__c, Area_Requirement__r.Description__c
        FROM Request_Item_Area__c 
        WHERE Request_Master_Data__c =: targetRecord
        WITH SECURITY_ENFORCED
        ORDER BY Area_of_Compliance__c DESC];
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Get the parent request record id (i.e. the REQ) for mapping purposes
     * @param objectApi - the object used for searching, for object aware context
     * @param recordId - the record Id of the object to lookup, to retrieve the parent REQ
     * @return string of the parent Request (REQ)
     */
    @AuraEnabled(cacheable=true)
    public static string getParentREQ(String objectApi, String recordId){
       
        if(objectApi == 'Cycle_Request_Item__c'){
            return [SELECT Request__c FROM Cycle_Request_Item__c WHERE Id =: recordId].Request__c;
        }else if(objectApi == 'KARL_Auditor_Request_Item__c'){
            return [SELECT Request_Master_Data__c FROM KARL_Auditor_Request_Item__c WHERE Id =: recordId].Request_Master_Data__c;
        }else{
            // Assume coming from REQ
            return recordId;
        }
    }
    
    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Return the users contactId from the user table, as this doesn't exist
     * as an native component in LWC.
     * @return contactId
     */
    @AuraEnabled(cacheable=true)
    public static string getContactId(){
        if( FeatureManagement.checkPermission('ARL_Is_External_Auditor') ){
            // Can reliably get contactId for community users
            User karlUser = [SELECT ContactId FROM User 
                                    WHERE Id =: UserInfo.getUserId()
                                    WITH SECURITY_ENFORCED LIMIT 1];
            
            return karlUser.ContactId;
        }else{
            // Need to use filter logic to get contact ID for general usersW
            Contact karlUser = [SELECT Id FROM Contact 
                                    WHERE Email =: UserInfo.getUserEmail()
                                    AND Account.Name = 'salesforce.com'
                                    AND RecordType.DeveloperName = 'Salesforce_Employee'
                                    WITH SECURITY_ENFORCED LIMIT 1];

            return karlUser.Id;
        }
    }
    
    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Get the primary Certification Reports
     * @param recordId - the record Id (Audit Scope Master Data) to look up
     * @return KARL_Audit_Scope_Reports__c
     */
    @AuraEnabled(cacheable=true)
    public static KARL_Audit_Scope_Reports__c[] getPrimaryReports(String recordId){
        return [SELECT Id, Name, toLabel(KARL_Cycle_Alignment__c),KARL_Audit_Frequency__c,
                KARL_Fee_Amount__c, toLabel(KARL_Report_Type__c), KARL_Type_I__c, KARL_Expiry_Date__c,
                KARL_Comments__c, KARL_Audit_Firm__r.Name, 
                KARL_Audit_Scope__r.Name
                FROM KARL_Audit_Scope_Reports__c 
                WHERE KARL_Audit_Scope__c =: recordId
                WITH SECURITY_ENFORCED
                ORDER BY KARL_Report_Type__c ASC];
    }
    
    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Get the Related Certification Reports
     * @param recordId - the record Id (Audit Scope Master Data) to look up
     * @return parent or child reports from KARL_Audit_Scope_In_Report__c
     */
    @AuraEnabled(cacheable=true)
    public static KARL_Audit_Scope_In_Report__c[] getRelatedReports(String recordId, String associationType, String objectApi){
        if(associationType == 'Parent'){
                return [SELECT Id, Name, KARL_Audit_Scope_Report__c, KARL_Report_Scope__c,
                    KARL_Report_Type__c, KARL_Audit_Scope_Report__r.KARL_Triggered_Report_Name__c,
                    KARL_Audit_Scope_Report__r.KARL_Audit_Firm__r.Name, toLabel(KARL_Audit_Scope_Report__r.KARL_Cycle_Alignment__c)
                    FROM KARL_Audit_Scope_In_Report__c 
                    WHERE KARL_Parent_Audit_Scope__c =: recordId
                    WITH SECURITY_ENFORCED
                    ORDER BY KARL_Report_Type__c ASC];
        }else{
            if(objectApi == 'KARL_Audit_Scope_Master_Data__c'){
                return [SELECT Id, Name, KARL_Audit_Scope_Report__c, KARL_Report_Scope__c,
                        KARL_Report_Type__c, KARL_Parent_Audit_Scope__c, KARL_Parent_Audit_Scope__r.Name
                        FROM KARL_Audit_Scope_In_Report__c 
                        WHERE KARL_Audit_Scope_Report__r.KARL_Audit_Scope__c =: recordId
                        AND KARL_Parent_Audit_Scope__c !=: recordId
                        WITH SECURITY_ENFORCED
                        ORDER BY KARL_Report_Type__c ASC];
            }else{
                return [SELECT Id, Name, KARL_Audit_Scope_Report__c, KARL_Report_Scope__c,
                        KARL_Report_Type__c, KARL_Parent_Audit_Scope__c, KARL_Parent_Audit_Scope__r.Name
                        FROM KARL_Audit_Scope_In_Report__c 
                        WHERE KARL_Audit_Scope_Report__c =: recordId
                        AND KARL_Parent_Audit_Scope__c !=: recordId
                        WITH SECURITY_ENFORCED
                        ORDER BY KARL_Report_Type__c ASC];
            }
        }
    }
    
       /**
     * @author Lakshya Arora
     * @email lakshya.arora@mtxb2b.com
     * @description Get the Weekday of given date
     * @param recordId - date
     * @return Weekday of input date
     */
    @AuraEnabled(cacheable=true)
    public static String getweekdayOfDate(Date testingDate){
        if(testingDate != null){
            String []dayArray = new String[]{'Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'};
            Date startOfWeekDate = testingDate.toStartOfWeek();
            Integer differenceBetweenDates = Math.abs(startOfWeekDate.daysBetween(testingDate));
            if(differenceBetweenDates < 7 && differenceBetweenDates >=0){
                return dayArray[differenceBetweenDates];
            }
        }
        return '';
    }

      /**
     * @author Lakshya Arora
     * @email lakshya.arora@mtxb2b.com
     * @description Get the date after n business days
     * @param  dueDaysLimit - number of days
     * @param comparisonDate - input days in which we need add business days 
     * @return date after n business day
     */
    @AuraEnabled(cacheable=true)
    public static Date calculateWorkingDate(Integer dueDaysLimit,Date comparisonDate){
        for(Integer i=1;i<=dueDaysLimit;){
            String weekday = KARL_Utility.getweekdayOfDate(comparisonDate);
            if(weekday != 'Saturday' && weekday != 'Sunday'){
                i++;
            }
            comparisonDate = comparisonDate.addDays(1);
        }
        return comparisonDate;
    }

     /**
     * @author Lakshya Arora
     * @email lakshya.arora@mtxb2b.com
     * @description Get Base url depends upon community or lightning experience
     * @return Return Map of base url prefix and suffix values
     */
    @AuraEnabled(cacheable=true)
    public static Map<String,String> getBaseURL(){
        Map<String,String> baseUrlMap= new Map<String,String>();
        Network karlNetwork;
        String auditorloginurl = '';
        String auditorcommunityHomeUrl = '';
        Integer index;
        if(!Test.isRunningTest()){
            karlNetwork = [SELECT Id FROM Network WHERE Name ='KARL Community' WITH SECURITY_ENFORCED];
            auditorloginurl = Network.getLoginUrl(karlNetwork.Id);
            index = auditorloginurl.lastIndexOf('/login');
            auditorcommunityHomeUrl = auditorloginurl.substring(0, index + 1);
        }

        String baseUrl = '';
        String baseRecordURL = '';
        String baseUrlSuffix = '';
        // Check if we're on the KARL Community or not
        if (Site.getSiteId() != null) {
            baseUrl = auditorcommunityHomeUrl; // we're on the community
            baseRecordURL = baseUrl + 'detail/';
        } else {
            baseUrl = URL.getSalesforceBaseUrl().toExternalForm();
            baseRecordURL = baseUrl + '/one/one.app?#/sObject/'; // force rendering in Lightning instead of Classic
            baseUrlSuffix = '/view'; // Required for Lightning record view
        }
        baseUrlMap.put('baseRecordURL',baseRecordURL);
        baseUrlMap.put('baseUrlSuffix',baseUrlSuffix);
        return baseUrlMap;
    }

     /**
     * @author Lakshya Arora
     * @email lakshya.arora@mtxb2b.com
     * @description Get Org Wide Email Address Id by display name
     * @return Return Id of org wide email address
     */
    @AuraEnabled(cacheable=true)
    public static List<OrgWideEmailAddress> getOrgwideEmailAddressIdByDisplayName(String displayName){
        return [SELECT Id,DisplayName,Address  FROM OrgWideEmailAddress WHERE DisplayName=:displayName];
    }

    /**
     * @author Lakshya Arora
     * @email lakshya.arora@mtxb2b.com
     * @description Get HTML Email Template by developer name
     * @param list of email template developer name
     * @return Return Map of Email Template Developer Name to EmailTemplate
     */
    public static Map<String,EmailTemplate> getEmailTemplateBydevelopernameMap(List<String> emailTemplateDeveloperNameList){
        Map<String,EmailTemplate> nameToEmailTemplateMap = new Map<String,EmailTemplate>();
        for(EmailTemplate emailTemplateObj : [SELECT id,name,body,developername,htmlvalue ,Subject
                                            FROM EmailTemplate 
                                            WHERE developername IN: emailTemplateDeveloperNameList]){
            nameToEmailTemplateMap.put(emailTemplateObj.developername,emailTemplateObj);
        }
        return nameToEmailTemplateMap;
    }

     /**
    * @author Lakshya Arora
    * @email lakshya.arora@mtxb2b.com
    * @desc Send email and log errors in debug log
    * @param list of Messaging.SingleEmailMessage
    */
    public static void sendEmail(List<Messaging.SingleEmailMessage> mailList){
        Messaging.SendEmailResult[] emailResults =  Messaging.sendEmail( mailList, false);
        for(Messaging.SendEmailResult result : emailResults){
            if(!result.isSuccess()){
                System.debug(LoggingLevel.DEBUG,'Error Received');
                for(Database.Error error : result.getErrors()){
                    System.debug(LoggingLevel.DEBUG,error.getMessage());
                }
            }
        }
    }

     /**
    * @author Lakshya Arora
    * @email lakshya.arora@mtxb2b.com
    * @desc Send email and log errors in debug log
    * @param1 Messaging.SingleEmailMessage
    * @param2 EmailTemplate object
    * @param3 html body
    * @param4 List of recipients
    * @param5 Id of Org Wide Email Address 
    * @return object of Messaging.SingleEmailMessage
    */
    public static Messaging.SingleEmailMessage createMessage(Messaging.SingleEmailMessage message,EmailTemplate emailTemplateObj,String htmlBody,
                                                                List<String> toAddressIdList,List<String> ccAddressList,String orgwideEmailAddressId){
        message.setSaveAsActivity(false); 
        message.setTemplateId(emailTemplateObj.id);
        message.setSubject(emailTemplateObj.Subject);
        message.setHtmlBody(htmlBody);
        message.setToAddresses(toAddressIdList); 
        message.setCcAddresses(ccAddressList);
        message.setOrgWideEmailAddressId(orgwideEmailAddressId);
        return message;
    }
}
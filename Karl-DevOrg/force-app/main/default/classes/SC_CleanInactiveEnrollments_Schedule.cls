/**
 * @author: ralph@callaway.cloud
 */
public class SC_CleanInactiveEnrollments_Schedule implements Schedulable {
    
    public void execute(SchedulableContext sc) {
        SC_CleanInactiveEnrollments inactiveUnenroller = new SC_CleanInactiveEnrollments();
        inactiveUnenroller.run();
    }
}
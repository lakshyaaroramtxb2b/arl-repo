@isTest
global with sharing class SC_TrainingTestUtil {
    public static RecordType voluntaryType;
    public static RecordType mandatoryType;
    
    static {
        voluntaryType = [SELECT Id, Name, DeveloperName FROM RecordType Where SObjectType = 'Training_Course_Taken__c' AND DeveloperName='Voluntary_Enrollment'];
        mandatoryType = [SELECT Id, Name, DeveloperName FROM RecordType Where SObjectType = 'Training_Course_Taken__c' AND DeveloperName='Mandatory_Enrollment'];
    }

    public static Training_Course__c createCourseForProvider(SC_Training_Provider__c provider) {
        Training_Course__c course = new Training_Course__c();
        course.Provider__c = provider.Type__c;
        course.Provider_ID__c = '1234';
        course.Is_Active__c = true;
        course.name = 'some course';
        course.URL__c = 'http://blah.com';
        
        insert course;
        return course;
    }
    
    public static Training_Course_Taken__c createCourseTakenForCourse(Id contactId, Training_Course__c course) {
        System.assert(contactId != null);
        Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
        
        Training_Course_Taken__c taken = new Training_Course_Taken__c();
        taken.Contact__c = contactId;
        taken.Training_Course__c = course.Id;
        taken.Attendee_Email__c = randomInt+'abc@salesforce.com';
        taken.Status__c = TrainingCourseTaken_Constants.STATUS_COURSE_NOT_STARTED;
        taken.RecordTypeId = mandatoryType.Id;
        insert taken;
        return taken;
    }
    
    global class MockHttpResponseGenerator implements HttpCalloutMock {
        String responseBody;
        Integer responseCode;
        global MockHttpResponseGenerator(String body) {
            responseBody = body;
        }
        global MockHttpResponseGenerator(Integer code, String body) {
            responseBody = body;
            responseCode = code;
        }
        global MockHttpResponseGenerator(Integer code) {
            responseCode = code;
        }
        global MockHttpResponseGenerator() {}
        
        global Integer callCount = 0;
        global HTTPRequest lastRequest;
        global HTTPResponse respond(HTTPRequest req) {
            callCount += 1;
            lastRequest = req;
            HttpResponse res = new HttpResponse();
            if (responseCode != null) {
                res.setStatusCode(responseCode);
            } else {
                res.setStatusCode(200);
            }
            if (responseBody != null) {
                res.setBody(responseBody);
            }
            return res;
        }
    }
}
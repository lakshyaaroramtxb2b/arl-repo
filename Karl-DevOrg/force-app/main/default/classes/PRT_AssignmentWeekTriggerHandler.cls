/***********************************************************
 * Handler class for PRT_AssignmentTrigger
 * Created by 	- Prashant Gupta
 * Date 		- 18th Dec 2019
 ************************************************************/
public class PRT_AssignmentWeekTriggerHandler {
    /***********************************************************
     * before insert Method to be called from trigger
     * Created by 	- Prashant Gupta
	 * Date 		- 18th Dec 2019
     ************************************************************/ 
	public static void beforeInsert(List<Assignment_Week__c> assignmentList){
    }
    public static void afterInsert(List<Assignment_Week__c> assignmentList){
        PRT_AssignmentWeekTriggerHelper.rollupAssignmentWeekCapacity(assignmentList, null);
    }
    public static void beforeUpdate(List<Assignment_Week__c> assignmentList, Map<Id,Assignment_Week__c> oldMap){
    }
     public static void afterUpdate(List<Assignment_Week__c> assignmentList, Map<Id,Assignment_Week__c> oldMap){
        PRT_AssignmentWeekTriggerHelper.rollupAssignmentWeekCapacity(assignmentList, oldMap);
    }
}
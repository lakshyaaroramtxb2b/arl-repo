global class SyncExternalCaseAndWorkStatusUpdate1 implements Schedulable {

    public String SOURCE_FILE = 'SyncExternalCaseAndWorkStatusUpdate';
    public Integer numberOfRecordsForSync = 0,
                   numberOfInsertedEmployees = 0;
    public Set<Id> caseIds = new Set<Id>();
    public Set<Id> workIds = new Set<Id>();
    public Map<Id,Case__x> mapCases = new Map<Id,Case__x>();
    public Map<Id,ADM_Work_c__x> mapWork = new Map<Id,ADM_Work_c__x>();
    public List<ESA_Security_Request__c> lstSecuirityRequests;
    public List<Case__x> lstCases = new List<Case__x>();
    public List<ADM_Work_c__x> lstWork = new List<ADM_Work_c__x>();

    global void execute(SchedulableContext ctx) {

        lstSecuirityRequests = new List<ESA_Security_Request__c>();

        lstSecuirityRequests = [SELECT Id, 
                                       Case_Work_Closed_Date__c, 
                                       Case_Work_Status__c,
                                       Status__c,
                                       Supportforce_CaseId__c,
                                       Case_Work_Last_Modified_Date__c
                                FROM ESA_Security_Request__c
                                WHERE Supportforce_CaseId__c!=null AND
                                (Case_Work_Closed_Date__c = null OR Case_Work_Closed_Date__c >= LAST_WEEK)
                                ORDER BY Case_Work_Status__c ASC NULLS FIRST];

        String caseId;

        Integer repeatCount = (lstSecuirityRequests.size()/300)+1;
        Integer x = 0,y = 0;
        
      
        caseIds = new Set<Id>();
        workIds = new Set<Id>();
        for(y=0; x<lstSecuirityRequests.size(); x++){
            caseId = lstSecuirityRequests[x].Supportforce_CaseId__c;
            if(caseId != null) {
                if(caseId.startsWith('500')) {
                            caseIds.add(caseId);
                }else {
                            workIds.add(caseId);
                }
            }

            if(y == 299) {
                updateRecords();
                caseIds = new Set<Id>();
                workIds = new Set<Id>();
                y = 0;
            } 
            y++;
        }
        
        updateRecords();
        
        List<Database.SaveResult> saveResults = Database.update(lstSecuirityRequests,false);

        String finalErrorText = '';
        for (Database.SaveResult sr : saveResults) {
            if (sr.isSuccess()) {
                        numberOfInsertedEmployees++;
                    } else {
                        for(Database.Error err : sr.getErrors()) {
                            finalErrorText += err.getStatusCode() + ': ' + err.getMessage() + '\n';
                        }
            }
        }
        Esa_DebugService.writeMessage('SyncExternalCaseWorkStatus Completed:');
        if(string.isNotBlank(finalErrorText)) {
            Esa_DebugService.writeMessage('SyncExternal Case And Work Status Update.finish: Failed with errors');
            Exception ex;
            Esa_DebugService.writeException(ex, 1, SOURCE_FILE, finalErrorText);
        }

    }

    private void updateRecords() {
        if(caseIds.size() > 0) {
            lstCases = [SELECT ExternalId,Status__c,
                               ClosedDate__c,
                               IsClosed__c,
                               LastModifiedDate__c
                        FROM Case__x
                        WHERE ExternalId IN : caseIds];
            for(Case__x ca : lstCases) {
                    mapCases.put(ca.ExternalId,ca);
            }
        }

        if(workIds.size() > 0) {
            lstWork = [SELECT ExternalId,Status_c__c,
                              Closed_On_c__c,
                              Closed_c__c,
                              LastModifiedDate__c 
                       FROM ADM_Work_c__x
                       WHERE ExternalId IN : workIds];
            for(ADM_Work_c__x ca : lstWork) {
                    mapWork.put(ca.ExternalId,ca);
            }
        }

        for(ESA_Security_Request__c eRequest : lstSecuirityRequests) {
            if(mapCases.containsKey(eRequest.Supportforce_CaseId__c)) {

                Case__x ca = mapCases.get(eRequest.Supportforce_CaseId__c);
                eRequest.Case_Work_Closed_Date__c = ca.ClosedDate__c;
                eRequest.Case_Work_Status__c      = ca.Status__c;
                eRequest.Case_Work_Last_Modified_Date__c = ca.LastModifiedDate__c;
                if(ca.IsClosed__c == true) {
                    eRequest.Status__c = 'Closed';
                } else if(ca.Status__c != 'New') {
                    eRequest.Status__c = 'In Progress';
                }

            }else if(mapWork.containsKey(eRequest.Supportforce_CaseId__c)) {

                ADM_Work_c__x work = mapWork.get(eRequest.Supportforce_CaseId__c);
                eRequest.Case_Work_Closed_Date__c = work.Closed_On_c__c;
                eRequest.Case_Work_Status__c      = work.Status_c__c;
                eRequest.Case_Work_Last_Modified_Date__c = work.LastModifiedDate__c;
                if(work.Closed_c__c == 1) {
                    eRequest.Status__c = 'Closed';
                }else if(work.Status_c__c != 'New') {
                    eRequest.Status__c = 'In Progress';
                }
            }
        }
        
    }    

}
public without sharing class AppXDashboardController {
    /*
     * Without sharing is required to get numbers for the queues since the user is a guest user.
     * The Logic is so small that I included it all in this class instead of breaking it up to a vith sharing class.  
    */
    
    private final static string SIXTYTWO_API_USER = AppX62.getUsername();
    private final static string SIXTYTWO_API_PASS = AppX62.getPassword();
    
    private final static integer SIXTYTWO_ERROR_LOGGING_IN = 9999;
    private final static integer SIXTYTWO_ERROR_RUNNING_QUERY = 9998;
    
    public static string getClassForThreshold(double percentage){
        if (percentage <= 25){
            return 'success';
        } else if (percentage <= 50){
            return 'info';
        } else if (percentage <= 75){
            return 'warning';
        } else {
            return 'danger';
        }
    }
    public static integer getOHQueueHealth(){
        double MAX_CONSIDERED_BAD = 4.0;
        return (integer)(((double)getOHQueueDepth() / MAX_CONSIDERED_BAD)*100.0);
    }

    public static integer getOHQueueDepth(){
        return [SELECT Count() FROM Partner_Appointment__c WHERE Date__c < TOMORROW AND Date__c >= TODAY];
    }
    
    public static integer getAEQueueHealth() {
        double MAX_CONSIDERED_BAD = 60.0;
        return (integer)(((double)getAEQueueDepth() / MAX_CONSIDERED_BAD)*100.0);
    }
    
    /*
     * Logs into salesforce's 62 org and gets the queue length.  
     * Note: login.salesforce.com and https://na1-api.salesforce.com must be in the remotesitesetting for this to work
     */
     
    public static integer getAEQueueDepth() {
        integer res = SIXTYTWO_ERROR_LOGGING_IN;
        for (integer i = 0; i < 5; i++){
            res = getAEQueueDepth_api();
            if (res != SIXTYTWO_ERROR_RUNNING_QUERY){
                return res;
            }
        }
        return res;
    }
    
    public static integer getAEQueueDepth_api() {
        //string apps_in_new = 'SELECT count() FROM Program__c WHERE Security_Review_Stage__c IN (\'2 - ProdSec\',\'3 - Prod Sec - Retest\') AND AppX_Directory__c != \'Japan\' AND Stage__c != \'SR-Outsourcer Eval In-Progress\' AND Security_Review_Sub_Stage__c IN (\'New\') AND Review_Cycle__c = \'Initial\'';
        string apps_in_new = 'SELECT count() FROM Security_Review__c WHERE AppX_Directory__c != \'Japan\' AND Stage__c != \'SR-Outsourcer Eval In-Progress\' AND Security_Review_Sub_Stage__c = \'New\' AND Review_Cycle__c = \'Initial\' AND Security_Review_Stage__c IN (\'2 - ProdSec\',\'3 - Prod Sec - Retest\')';
        
        
        try {
            partnerSoapSforceCom.Soap conn = new partnerSoapSforceCom.Soap();
            partnerSoapSforceCom.LoginResult lr = conn.login(SIXTYTWO_API_USER, SIXTYTWO_API_PASS);
            if (lr.sessionId != null){

                //we have logged in
                partnerSoapSforceCom.SessionHeader_element sessionheader = new partnerSoapSforceCom.SessionHeader_element();
                sessionheader.sessionId = lr.sessionId;
                conn.endpoint_x = lr.serverUrl;
                conn.SessionHeader = sessionheader;
                
                partnerSoapSforceCom.QueryResult q = conn.query(apps_in_new);
                return q.size;

            } else {
                return SIXTYTWO_ERROR_LOGGING_IN;
            }
        } catch (Exception e){
            System.debug('Exception: ' + e);
            return SIXTYTWO_ERROR_RUNNING_QUERY;
        }
    }

    public static integer getCXQueueHealth(){
        double MAX_CONSIDERED_BAD = 50.0;
        return (integer)(((double)getCXQueueDepth() / MAX_CONSIDERED_BAD)*100.0);
    }
    
    public static integer getCXQueueDepth(){
        //query from this view https://security.my.salesforce.com/ui/list/FilterEditPage?id=00B30000007JSmn 
        return [SELECT COUNT() FROM Scan_Queue__c WHERE Status__c IN ('Waiting to scan','In queue','Downloading code','Scanning')];
    }
    
    public static integer getBurpQueueDepth(){
        return [SELECT COUNT() FROM WebAppScanner__c WHERE Status__c = 'New'];
    }
    
    public static integer getBurpQueueHealth(){
        double MAX_CONSIDERED_BAD = 8.0;
        return (integer)(((double)getBurpQueueDepth() / MAX_CONSIDERED_BAD)*100.0);
    }
 
    @RemoteAction
    public static string[] getStatus(string module){
        if ('oh'.equals(module.toLowerCase())){
            return new String[]{''+getOHQueueDepth(),''+getOHQueueHealth(),getClassForThreshold(getOHQueueHealth())};
        } else if ('cx'.equals(module.toLowerCase())){
            return new String[]{''+getCXQueueDepth(),''+getCXQueueHealth(),getClassForThreshold(getCXQueueHealth())};
        } else if ('ae'.equals(module.toLowerCase())){
            return new String[]{''+getAEQueueDepth(),''+getAEQueueHealth(),getClassForThreshold(getAEQueueHealth())};
        } else if ('bp'.equals(module.toLowerCase())){
            return new String[]{''+getBurpQueueDepth(),''+getBurpQueueHealth(),getClassForThreshold(getBurpQueueHealth())};
        }
        
        return null;
    }
    @RemoteAction
    public static Partner_Appointment__c[] getUpcomingAppointments(){
        return [SELECT Id,Company__c,Name__c,ContactInfo__c,Description__c,Email__c,Appointment_Owner__r.Name ,Date__c FROM Partner_Appointment__c WHERE Date__c >= TODAY ORDER BY Date__c ASC NULLS LAST LIMIT 20];
    }
}
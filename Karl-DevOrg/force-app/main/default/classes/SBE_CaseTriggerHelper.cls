/*
* Created by - Banshi (MTX Group Inc.)
* Helper class for SBE CASE TRIGGER, 
* Most of the functional code for the trigger exists here and is directly called from Handler Class
*/
public class SBE_CaseTriggerHelper {
    @TestVisible private static List<TM_Security_Work_SLA_Extension_c__x> mockedRequests = new List<TM_Security_Work_SLA_Extension_c__x>(); //For the test class
    
    /*
* Created by - Banshi
* Date - 12-23-2019
* If the fields are updated on case then update corresponding SBE reocrd fields.
*/
    public static void updateSBERecords(List<Case> newList, Map<id,Case> oldMap){
        Set<String> sbeIds = new Set<String>();
        Set<Id> caseIds = new Set<Id>();
        List<String> statusList = new List<String>();
        statusList.add(IEM_Constants.IEM_CASE_STATUS_INTERNAL_REVIEW);
        statusList.add(IEM_Constants.IEM_CASE_STATUS_SECURITY_DECISION);
        statusList.add(IEM_Constants.IEM_CASE_STATUS_BUSINESS_DECISION);
        statusList.add(IEM_Constants.IEM_CASE_STATUS_MONITORING);
        
        
        for(Case selectedCase : newList){
            if( selectedCase.Status != oldMap.get(selectedCase.Id).Status 
               && (statusList.contains(selectedCase.Status)) 
               && (selectedCase.SLA_Extension_Id__c !=null || Test.isRunningTest()) ){
                   caseIds.add(selectedCase.Id);
               }
            //If Business Decision or Security Decision date has been updated via approval process
            if( (selectedCase.Business_Decision_Date__c !=  oldMap.get(selectedCase.Id).Business_Decision_Date__c) || (selectedCase.Security_Decision_Date__c !=  oldMap.get(selectedCase.Id).Security_Decision_Date__c) || (selectedCase.Internally_Reviewed_Date__c !=  oldMap.get(selectedCase.Id).Internally_Reviewed_Date__c) ){
                caseIds.add(selectedCase.Id);
            }
        }
        if(!caseIds.isEmpty() && !System.isFuture() && !System.isBatch()){
            updateSBERecordsFuture(caseIds);
        }
        
    }
    @future
    public static void updateSBERecordsFuture(Set<Id> caseIds){
        List<Case> caseList = new List<Case>();
        Map<String, Case> caseMap = new Map<String, Case>();
        
        for(Case c : [Select Id,Internally_Reviewed_Date__c,Business_Decision_Date__c,Security_Decision_Date__c,Status,SLA_Extension_Id__c From Case Where Id IN :caseIds]){
            caseMap.put(c.SLA_Extension_Id__c, c);
        }
        List<TM_Security_Work_SLA_Extension_c__x> gusSBEs = new List<TM_Security_Work_SLA_Extension_c__x>();
        List<TM_Security_Work_SLA_Extension_c__x> sbeList = new List<TM_Security_Work_SLA_Extension_c__x>();
        
        if(Test.isRunningTest()) {
            gusSBEs = mockedRequests;
        }else{
            gusSBEs = [Select Id,ExternalId,TM_Submitted_for_Business_Approval_c__c ,TM_SLA_Extension_Status_c__c,TM_Approved_by_Business_c__c, TM_Date_Approved_c__c    From TM_Security_Work_SLA_Extension_c__x Where Id IN :caseMap.keySet()];
        }
        for(TM_Security_Work_SLA_Extension_c__x gusSbe : gusSBEs){
            if(caseMap.containsKey(gusSbe.ExternalId))
            {
                Case currentCaseRecord = caseMap.get(gusSbe.ExternalId);
                String caseStatus = currentCaseRecord.Status;
                String gusSBEStatusToUpdate;
                if(caseStatus == IEM_Constants.IEM_CASE_STATUS_INTERNAL_REVIEW){
                    gusSBEStatusToUpdate = IEM_Constants.SBE_STATUS_IEM_ANALYST;
                }
                else if(caseStatus == IEM_Constants.IEM_CASE_STATUS_SECURITY_DECISION){
                    gusSBEStatusToUpdate  = IEM_Constants.SBE_STATUS_SECURITY_DECISION;
                }
                else if(caseStatus == IEM_Constants.IEM_CASE_STATUS_BUSINESS_DECISION){
                    gusSBEStatusToUpdate = IEM_Constants.SBE_STATUS_BUSINESS_DECISION;
                }
                else if(caseStatus == IEM_Constants.IEM_CASE_STATUS_MONITORING){
                    gusSBEStatusToUpdate = IEM_Constants.SBE_STATUS_APPROVED;
                }else if(caseStatus == IEM_Constants.IEM_CASE_STATUS_VALIDATING_RESOLUTION){
                    gusSBEStatusToUpdate = IEM_Constants.SBE_STATUS_APPROVED;
                }
                if(String.isNotEmpty(gusSBEStatusToUpdate) && gusSbe.TM_SLA_Extension_Status_c__c != gusSBEStatusToUpdate){
                    gusSbe.TM_SLA_Extension_Status_c__c = gusSBEStatusToUpdate;
                }
                
                //Update Approved by Business
                if(currentCaseRecord.Business_Decision_Date__c != null){
                    if( currentCaseRecord.Business_Decision_Date__c.date() != gusSbe.TM_Approved_by_Business_c__c){
                        gusSbe.TM_Approved_by_Business_c__c =  currentCaseRecord.Business_Decision_Date__c.date();
                    }
                }
                //Update Approved by Security
                if( currentCaseRecord.Security_Decision_Date__c != null ){
                    if( currentCaseRecord.Security_Decision_Date__c.date() != gusSbe.TM_Date_Approved_c__c){
                        gusSbe.TM_Date_Approved_c__c =  currentCaseRecord.Security_Decision_Date__c.date();
                    }
                }
                //Update Review by I&EM Analyst Complete
                if( currentCaseRecord.Internally_Reviewed_Date__c  != null ){
                    if( currentCaseRecord.Internally_Reviewed_Date__c.date() != gusSbe.TM_Submitted_for_Business_Approval_c__c ){
                        gusSbe.TM_Submitted_for_Business_Approval_c__c  =  currentCaseRecord.Internally_Reviewed_Date__c.date();
                    }
                }
                sbeList.add(gusSbe);
            }
        }
        if(!sbeList.IsEmpty()){
            if(!Test.isRunningTest()){
                List<Database.SaveResult> result = Database.updateImmediate(sbeList);
            }
        }
    }
    
    /*
* Created by - Banshi
* Date - 31-12-2019
* This functon is used to update Extension status based on IEM_Case_Type__c, Business_Decision__c, and Security_Decision__c
*If IEM Case Type == Exception, The IEM Case field 'Extension Status' should be populated as follows:
*If Business Approval is Approved, then Extension Status = Approved.
*If Business or Security Approval rejects, then Extension Status = Rejected.
*/
    public static void updateExtensionStatus(List<Case> newList, Map<id,Case> oldMap){
        for(Case newCase: newList ){
            if(newCase.IEM_Case_Type__c == IEM_Constants.IEM_EXCEPTION && 
               (newCase.IEM_Case_Type__c != oldMap.get(newCase.Id).IEM_Case_Type__c || 
                newCase.Business_Decision__c !=oldMap.get(newCase.Id).Business_Decision__c ||
                newCase.Security_Decision__c !=oldMap.get(newCase.Id).Security_Decision__c
               )){
                   if(newCase.Business_Decision__c == IEM_Constants.IEM_BUSINESS_DECISION_APPROVED){
                       newCase.Extension_Status__c = IEM_Constants.SBE_STATUS_EXTENSION_APPROVED;
                   }
                   if(newCase.Business_Decision__c == IEM_Constants.IEM_BUSINESS_DECISION_REJECTED || newCase.Security_Decision__c == IEM_Constants.IEM_SECURITY_DECISION_REJECTED){
                       newCase.Extension_Status__c = IEM_Constants.SBE_STATUS_EXTENSION_REJECTED;
                   }
               }
        }
    }
    
    /*
* Created by - Banshi
* Date - 31-12-2019
* Method for- When the approval of the case happens within the Security Org we need to sync the approval date/time and comments from the Security org over to the GUS record in GUS so that leadership can see the following items:
*Date/Time of approval
*Who approved it
*Comments from the approval
*/
    public static void syncApprovalToSBE(List<Case> newList, Map<id,Case> oldMap){
        // Create a map that stores all the objects that require editing 
        Set<Id> caseIds = new Set<Id>{};
            for(Case newCase: newList ){
                if(newCase.SLA_Extension_Id__c != null && newCase.Business_Decision__c == IEM_Constants.SBE_STATUS_EXTENSION_APPROVED && (newCase.Action_Plan_Status__c == 'Submitted' && newCase.Action_Plan_Status__c !=oldMap.get(newCase.Id).Action_Plan_Status__c)){
                    caseIds.add(newCase.Id); 
                }
            }
        SYstem.debug('caseIds--> 1' + caseIds);
        if(!caseIds.isEmpty() && !System.isFuture() && !System.isBatch()){
            syncApprovalToSBEFuture(caseIds);
        }
        
    }
    
    /*
* Created by - Banshi
* Date - 31-12-2019
* Method for- When the approval of the case happens within the Security Org we need to sync the approval date/time and comments from the Security org over to the GUS record in GUS so that leadership can see the following items:
*Date/Time of approval
* Modified By - Swarnima on Line 169,208 regarding T-09475
*Who approved it
*Comments from the approval
*/
    @future
    public static void syncApprovalToSBEFuture(Set<Id> caseIds){
        // Create a map that stores all the objects that require editing 
        //Map<Id, String> caseRecordsToSBERecordsMap = new Map<Id, String>{};
       	Map<Id, Case> caseRecordsToSBERecordsMap = new Map<Id, Case>{};
            for(Case newCase: [Select id,SLA_Extension_Id__c,Estimated_Remediation_Date__c 
                               from case 
                               where id IN :caseIds]){
                                   if(newCase.SLA_Extension_Id__c != null){
                                       caseRecordsToSBERecordsMap.put(newCase.Id, newCase);  
                                   }
                                   //System.debug('newCase --> 2' + newCase);
                                 
            }
        List<TM_Security_Work_SLA_Extension_c__x>  sbeRecordsList = new List<TM_Security_Work_SLA_Extension_c__x>();
        if(!caseRecordsToSBERecordsMap.isEmpty()){
            List<Id> processInstanceIds = new List<Id>{};
                //List<Date> estimatedRediationDateList = new List<Date>();
                for (Case c : [SELECT Estimated_Remediation_Date__c, (SELECT ID FROM ProcessInstances ORDER BY CreatedDate DESC LIMIT 1)
                               FROM Case 
                               WHERE ID IN :caseRecordsToSBERecordsMap.keySet()])
            {
                //System.debug('c --> 3' + c);
                processInstanceIds.add(c.ProcessInstances[0].Id);
            }
            
            // Now that we have the most recent process instances, we can check
            // the most recent process steps for comments.  
            for (ProcessInstance pi : [SELECT TargetObjectId,CompletedDate,LastActorId,LastActor.Name,
                                       (SELECT Id, StepStatus, Comments 
                                        FROM Steps Where StepStatus = 'Approved'
                                        ORDER BY CreatedDate DESC
                                        LIMIT 1 )
                                       FROM ProcessInstance
                                       WHERE Id IN :processInstanceIds
                                       ORDER BY CreatedDate DESC])
            {
                //System.debug('pi --> 4' + pi);
                if(!(pi.Steps).isEmpty() || Test.isRunningTest()){
                    if(caseRecordsToSBERecordsMap.containsKey(pi.TargetObjectId)){
                        TM_Security_Work_SLA_Extension_c__x sbeRecord = new TM_Security_Work_SLA_Extension_c__x();
                        sbeRecord.ExternalId =caseRecordsToSBERecordsMap.get(pi.TargetObjectId).SLA_Extension_Id__c;
                        if(caseRecordsToSBERecordsMap.get(pi.TargetObjectId).Estimated_Remediation_Date__c!=null){
                            sbeRecord.TM_Date_Approved_c__c = caseRecordsToSBERecordsMap.get(pi.TargetObjectId).Estimated_Remediation_Date__c;
                        }
                        String msg = '';
                        msg+= 'Approver: '+pi.LastActor.Name+ '\n';
                        msg+= 'Approved Date: '+pi.CompletedDate+ '\n';
                        if(!(pi.Steps).isEmpty()){
                            msg+= 'Comments: '+pi.Steps[0].Comments;
                        }
                        sbeRecord.TM_Remediation_Progress_and_Status_c__c = msg;
                        //System.debug('sbeRecord to check --> 5' + sbeRecord);
                        sbeRecordsList.add(sbeRecord);
                        
                    }
                }
                
            }
            if(!sbeRecordsList.isEmpty() && !Test.isRunningTest()){
                List<Database.SaveResult> result = Database.updateImmediate(sbeRecordsList);
            }
            
        }
        
        
    }
}
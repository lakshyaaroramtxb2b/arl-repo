@isTest
public class CSIRT_chatterFirstPostSetsInProgressTest {
    @isTest
    public static void test_unrelated() {
        RecordType rt = [SELECT Id FROM RecordType WHERE DeveloperName='CSIRT_Security_Incident' and SobjectType='Case'];
        Case c= new Case(Subject='blah',RecordTypeId=rt.id,Status='Escalated');
        insert c;
        FeedItem f = new FeedItem(parentId=c.Id,Type='TextPost',body='blah!');
        insert f;
        System.assert([SELECT Status FROM Case WHERE Id=:c.Id].Status == 'Escalated');
    }
    
    @isTest
    public static void test_related() {
        RecordType rt = [SELECT Id FROM RecordType WHERE DeveloperName='CSIRT_Security_Incident' and SobjectType='Case'];
        Case c= new Case(Subject='blah',RecordTypeId=rt.id,Status='New');
        insert c;
        FeedItem f = new FeedItem(parentId=c.Id,Type='TextPost',body='blah!');
        insert f;
        System.assert([SELECT Status FROM Case WHERE Id=:c.Id].Status == 'In-Progress');
    }
}
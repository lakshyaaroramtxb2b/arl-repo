public class DCGusConnector {
    
	static String kLoginUrl = 'https://login.salesforce.com';
    static String kBaseUrl = 'https://gus.my.salesforce.com';
    
    static String kUN = 'detectioncloud@gus.com';
    static String kPw = 'PNQ-~g\'@3y*s]sj';
    
    static String sessionId;
    static String sessionUrl;
    
    public class OrgResp
    {
        public string id;
        public string success;
        public string [] errors;
    }
    
    public class ADM_Work {
        
        public string SubjectCUSTOMOBJ;
        public string DetailsCUSTOMOBJ;
        public string Product_OwnerCUSTOMOBJ; //005B0000000hfHfIAI adam mnceilly
        //public string Product_Tag_NameCUSTOMOBJ; //Detection Cloud - Operations
        //public string Found_in_BuildCUSTOMOBJ;
        //public string Found_In_Build_CopyCUSTOMOBJ;
        //public string Found_In_Build_NameCUSTOMOBJ;
        //public boolean Apex_Hammer_Compile_FailureCUSTOMOBJ;
        //public string PriorityCUSTOMOBJ;
        //public string Priority_DefaultCUSTOMOBJ;
        //public string Priority_Override_ExplanationCUSTOMOBJ;
        //public string Priority_RankCUSTOMOBJ; 
        public string Product_TagCUSTOMOBJ; //a1aB0000000LDkSIAW
        //public string StatusCUSTOMOBJ; //In Progress
        public double Story_PointsCUSTOMOBJ; //10.0
        
        //public string ClosedCUSTOMOBJ;
       	
        public string AssigneeCUSTOMOBJ;
        
        //public boolean Capex_EnabledCUSTOMOBJ;
        //public string CloudCUSTOMOBJ;
        public string ColumnCUSTOMOBJ;
        //public boolean Critical_CRM_FeatureCUSTOMOBJ;
        //public boolean Data_Silo_Test_AffectedCUSTOMOBJ;    
        public boolean DetailedCUSTOMOBJ;
        public boolean Executive_InvolvedCUSTOMOBJ;
        //public string Has_Story_PointsCUSTOMOBJ;
        public boolean HighlightCUSTOMOBJ;
        //public string Known_Issue_LinkCUSTOMOBJ;
		//public string Log_Bug_From_TemplateCUSTOMOBJ;
        public string RecordTypeId;
        public boolean Red_AccountCUSTOMOBJ;
        //public string Related_URL_LinkCUSTOMOBJ;
        public boolean Request_RD_Mgr_ReviewCUSTOMOBJ;
        public boolean SchemaCUSTOMOBJ;
        //public string Scrum_Team_NameCUSTOMOBJ;
        public string Scrum_TeamCUSTOMOBJ;
        public string TypeCUSTOMOBJ;
        //public string User_Profile_of_the_CreatorCUSTOMOBJ;
        public boolean Use_PrioritizerCUSTOMOBJ;
        //public string visual_link_num_of_Test_FailuresCUSTOMOBJ;
        public string FrequencyCUSTOMOBJ;
        public string ImpactCUSTOMOBJ;
        //public string Product_OwnerCUSTOMOBJ;
        
        //CreatedById 
    }
    
      static void Login() {
        
        HttpRequest request = new HttpRequest();
        
        request.setEndpoint(kLoginUrl + '/services/Soap/u/30.0');
        request.setMethod('POST');
        request.setTimeout(120000);
        request.setHeader('Content-Type', 'text/xml; charset=UTF-8');
        request.setHeader('SOAPAction', 'login');
        request.setBody('<?xml version="1.0" encoding="utf-8" ?> <env:Envelope xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:env="http://schemas.xmlsoap.org/soap/envelope/"><env:Body><n1:login xmlns:n1="urn:partner.soap.sforce.com"><n1:username>' + kUN+ '</n1:username><n1:password>' + kPW + '</n1:password></n1:login></env:Body> </env:Envelope>');
          
        Dom.XmlNode resultElmt = (new Http()).send(request).getBodyDocument().getRootElement()
              .getChildElement('Body','http://schemas.xmlsoap.org/soap/envelope/')
              .getChildElement('loginResponse','urn:partner.soap.sforce.com')
              .getChildElement('result','urn:partner.soap.sforce.com');

        String SERVER_URL = resultElmt.getChildElement('serverUrl','urn:partner.soap.sforce.com').getText().split('/services')[0];
        String GSERVER_URL = SERVER_URL;
        sessionId = resultElmt.getChildElement('sessionId','urn:partner.soap.sforce.com').getText();        
    
    	//String body = QueryInstance (SERVER_URL, 'SELECT Id, Name FROM ADM_Work__c WHERE Id = \'a07B0000003WIQD\'');
        //system.debug('Test = ' + body);
        
        //TestCreate();
    }

    @future(callout=true)
	//public static void CreateGUSWhiteLsitWorkItem(Whitelist_Request__c wlRequest) {
    public static void CreateGUSWhiteLsitWorkItem(string wlName, string wlId, string wlReason, id csirtCase) {
        
        Login();
        
        ADM_Work w = new ADM_Work();
        
        //w.SubjectCUSTOMOBJ = 'Whitelist Request - ' + wlRequest.Detection_Alert_Criteria__r.Name;
        //w.DetailsCUSTOMOBJ =  'Detection Whitelist Request https://security.my.salesforce.com/' + wlRequest.Id + '\n\n\n' + wlRequest.Reason_for_request__c;
        
        w.SubjectCUSTOMOBJ = 'Whitelist Request - ' + wlName;
        w.DetailsCUSTOMOBJ =  'Detection Whitelist Request https://security.my.salesforce.com/' + wlId + '\n\n\n' + wlReason;
        
        
        w.Product_TagCUSTOMOBJ = 'a1aB0000000LDkSIAW';
        w.DetailedCUSTOMOBJ = false;
        w.Executive_InvolvedCUSTOMOBJ = false;
        w.HighlightCUSTOMOBJ = false;
		w.RecordTypeId = '0129000000006gDAAQ';
        w.Red_AccountCUSTOMOBJ = false;
        w.Request_RD_Mgr_ReviewCUSTOMOBJ = false;
        w.SchemaCUSTOMOBJ = false;
        w.Scrum_TeamCUSTOMOBJ = 'a00B0000003NGt6IAG';
        w.TypeCUSTOMOBJ = 'User Story';
        w.Use_PrioritizerCUSTOMOBJ = true;
        
        Id newRec = CreateWorkItem(w);
        
        System.debug('New ADM_Work__c object created ' + newRec);
        
        FeedItem post = new FeedItem();
        post.ParentId = wlId;
        post.Body = 'GUS Ticket Created ' + newRec;
        post.LinkUrl  = 'https://gus.my.salesforce.com/' + newRec;
        insert post;
        
        post = new FeedItem();
        post.ParentId = csirtCase;
        post.Body = 'Whitelist Request Created https://security.salesforce.com/' + wlId;
        post.LinkUrl  = 'https://security.my.salesforce.com/' + wlId;
        insert post;
    }
    
    @future(callout=true)
    public static void TestCreate() {
        
        Login();
        
        ADM_Work w = new ADM_Work();
        
        w.SubjectCUSTOMOBJ = 'Test item API';
        w.DetailsCUSTOMOBJ = 'Test Item Please Ignore';
        //w.Product_OwnerCUSTOMOBJ = '005B0000000hfHfIAI';
        w.Product_TagCUSTOMOBJ = 'a1aB0000000LDkSIAW';
        //w.StatusCUSTOMOBJ = 'Acknowledged';
        //w.PriorityCUSTOMOBJ = 'Low';
        //w.Apex_Hammer_Compile_FailureCUSTOMOBJ = false;
        
        //new testting
        //w.AssigneeCUSTOMOBJ = '005B0000000hfHfIAI';
        //w.FrequencyCUSTOMOBJ = 'a0L9000000000utEAA';
        //w.ImpactCUSTOMOBJ = 'a0O900000004EF2EAM';
        //w.Product_OwnerCUSTOMOBJ = '005B0000000hfHfIAI';
        //
        
        //w.Capex_EnabledCUSTOMOBJ = false;
        //w.ColumnCUSTOMOBJ = 'a30B00000004xeyIAA';
        //w.Critical_CRM_FeatureCUSTOMOBJ = false;
        //w.Data_Silo_Test_AffectedCUSTOMOBJ = false;    
        w.DetailedCUSTOMOBJ = false;
        w.Executive_InvolvedCUSTOMOBJ = false;
        w.HighlightCUSTOMOBJ = false;
		w.RecordTypeId = '0129000000006gDAAQ';
        w.Red_AccountCUSTOMOBJ = false;
        w.Request_RD_Mgr_ReviewCUSTOMOBJ = false;
        w.SchemaCUSTOMOBJ = false;
        w.Scrum_TeamCUSTOMOBJ = 'a00B0000003NGt6IAG';
        w.TypeCUSTOMOBJ = 'User Story';
        w.Use_PrioritizerCUSTOMOBJ = true;

        //w.Found_in_BuildCUSTOMOBJ = '';
        //w.Priority_DefaultCUSTOMOBJ = '';
        //w.Priority_Override_ExplanationCUSTOMOBJ = '';
        //w.Priority_RankCUSTOMOBJ = '';
        //w.Story_PointsCUSTOMOBJ = 10.0;
        
        Id newRec = CreateWorkItem(w);
        
        System.debug('New ADM_Work__c object created ' + newRec);
        
        //String SUPPORT_URL_CREATE = 'https://na1.salesforce.com/services/data/v29.0/sobjects/Case/';
        //Id newRec = postTaskToSupportforce(c, SUPPORT_URL_CREATE, false); //(3rd value
        
    }
    
    public static Id CreateWorkItem(Object c)
    {   
        System.debug('CreateWorkItem()');
    
        String caseJSON = JSON.serializePretty(c);
        
        caseJSON = caseJSON.replaceAll('CUSTOMOBJ', '__c');

        System.debug('caseJSON: ' + caseJSON );

        String CmdUrl = 'https://gus.my.salesforce.com/services/data/v29.0/sobjects/ADM_Work__c/';
        
        String JSONString = PostToInstance(CmdUrl, caseJSON, false);

        system.debug(JSONString);

        if(String.isNotBlank(JSONString))
        {
            OrgResp resp;
            
            try
            {
               resp = (OrgResp)JSON.deserialize(JSONString, OrgResp.class);
            }
            catch(Exception e)
            {
                System.debug('Result not expected from Org instance. Object not created');
                System.debug('reason : ' + JSONString);
                
                return null;
            }
            
            system.debug(resp);
        
            if(resp.success == 'true')
                return resp.id;
        }

        return null;
    }
    
    public static String QueryInstance(string SERVER_URL, string query)
    {
        return RESTQueryInstance(SERVER_URL, query, false);
    }
    
    public static String QueryInstanceJOSN(string SERVER_URL, string query)
    {
        return RESTQueryInstance(SERVER_URL, query, true);
    }
    
    public static String RESTQueryInstance(string SERVER_URL, string query, boolean useJSON)
    {
        final PageReference theUrl = new PageReference(SERVER_URL + '/services/data/v28.0/query/');
        theUrl.getParameters().put('q',query);
        HttpRequest request = new HttpRequest();
        request.setEndpoint(theUrl.getUrl());
        
        System.debug('Query server: ' + theUrl.getUrl());
        
        request.setMethod('GET');

        if(useJSON) 
            request.setHeader('Content-type', 'application/json');
        
        request.setHeader('Authorization', 'OAuth ' + sessionId);

        String body = (new Http()).send(request).getBody();
        
        System.debug('Query result: ' + body);
        
        return body;
    }
    
    public static String PostChatterMessage(Id recordId, String msg) {
        
        ChatterItem newMsg = new ChatterItem();
        newMsg.body = new Body();
        newMsg.body.messageSegments = new List<MessageSegment>();
        
        newMsg.subjectId = recordId;
        newMsg.feedElementType = 'FeedItem';
        
        ChatterText mainMsg = new ChatterText();
        mainMsg.type = 'Text';
        mainMsg.text = msg;

        newMsg.body.messageSegments.add(mainMsg);
        
        String JOSNpostData = JSON.serialize(newMsg);
 
        return PostToInstance(kBaseUrl + '/services/data/v31.0/chatter/feed-elements', JOSNpostData, true);
    }
    
    public class Body
    {
        public List<MessageSegment> messageSegments { get; set; }
    }
    
    public class ChatterItem
    {
        public Body body { get; set; }
        public string feedElementType { get; set; }
        public string subjectId { get; set; }
    }
    
    public class ChatterText extends MessageSegment
    {
        public string text { get; set; }
    }
    
    private static String ID15to18(String initialVal) {
        if (initialVal.length() != 15) return initialVal;
        ID convertToLong = initialVal;
        String longId = convertToLong;
        return longId;
    }

    public virtual class MessageSegment
    {
        public string type { get; set; }
    }
    
    static String PostToInstance(string PostUrl, string JsonBody, Boolean useAssingmentRules)
    {
        String url =  PostUrl;
        String ResBodyIFBatchTest = '';
        
        
        HttpRequest req = new HttpRequest();
        req.setMethod('POST');
        req.setTimeout(120000);
        req.setEndpoint(url);

        if(useAssingmentRules == false)
            req.setHeader('SForce-Auto-Assign', 'FALSE');

        req.setHeader('Content-type', 'application/json');
        req.setHeader('Authorization', 'OAuth ' + sessionId);  
        req.setBody(JsonBody);
        Http http = new Http();
        
        if(ResBodyIFBatchTest=='')
        {
            HTTPResponse res = http.send(req);
            
            system.debug(req);
            system.debug(res.getBody());
            
            return res.getBody();
        }
        else
        {
            return ResBodyIFBatchTest;
        }
    }
    
    public static Dom.XmlNode LoginModule(string domain, string username, string password)
    {
        //----------------------------------------------------------------------
        // Login via SOAP/XML web service api to establish session
        //----------------------------------------------------------------------
        HttpRequest request = new HttpRequest();
        //request.setEndpoint('https://' + domain + '.salesforce.com/services/Soap/u/22.0');
        request.setEndpoint(domain+'/services/Soap/u/28.0');
        request.setMethod('POST');
        request.setTimeout(120000);
        request.setHeader('Content-Type', 'text/xml;charset=UTF-8');
        request.setHeader('SOAPAction', '""');
        //not escaping username and password because we're setting those variables above
        //in other words, this line "trusts" the lines above
        //if username and password were sourced elsewhere, they'd need to be escaped below
        request.setBody('<Envelope xmlns="http://schemas.xmlsoap.org/soap/envelope/"><Header/><Body><login xmlns="urn:partner.soap.sforce.com"><username>' + username + '</username><password>' + password + '</password></login></Body></Envelope>');

        if(!test.isRunningTest())
        {
            Dom.XmlNode resultElmt = (new Http()).send(request).getBodyDocument().getRootElement()
              .getChildElement('Body','http://schemas.xmlsoap.org/soap/envelope/')
              .getChildElement('loginResponse','urn:partner.soap.sforce.com')
              .getChildElement('result','urn:partner.soap.sforce.com');
             //system.debug('resultElmt:'+resultElmt);
             return resultElmt;
        }
        else
            return null;
    }
}
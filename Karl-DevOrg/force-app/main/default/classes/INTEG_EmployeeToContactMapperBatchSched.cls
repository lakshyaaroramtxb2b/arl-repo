global class INTEG_EmployeeToContactMapperBatchSched implements Schedulable {
    /* 
     * This must be scheduled in apex since it takes a parameter to its constructor
     * Run this in a debug console to schedule every 4 hours:
     * System.schedule('EmployeeToContactMapperBatch', '0 0 0,4,8,12,16,20 ? * *', new INTEG_EmployeeToContactMapperBatchSched(4));
     */
    global Integer frequencyInHours;
    
    global INTEG_EmployeeToContactMapperBatchSched(Integer freqHours) {
        frequencyInHours = freqHours;
    }
    
    global void execute(SchedulableContext sc) {
        DateTime lastExecution = Datetime.now().addHours(0-frequencyInHours);
        INTEG_EmployeeToContactMapperBatch batcher = new INTEG_EmployeeToContactMapperBatch(lastExecution);
        Database.executeBatch(batcher);
    }
}
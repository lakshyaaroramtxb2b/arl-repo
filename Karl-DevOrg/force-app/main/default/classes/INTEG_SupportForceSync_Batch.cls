/**
 * @author: ralph@callaway.cloud
 * @description: Sync SupportForce Contact data to Security INTEG_Employee__c object
 */
public class INTEG_SupportForceSync_Batch implements Database.Batchable<sObject>, Database.Stateful {

    private static final String NOT_FOUND_ERR_MSG = 'No Contact__x record found';
    private static final String DUPLICATE_ERR_MSG = 'Duplicate Contact__x records found';

    Map<String, List<String>> errorsWithIds = new Map<String, List<String>>();

    public Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([
            SELECT Name,
                   INTEG_Active__c,
                   INTEG_Division__c,
                   INTEG_Is_On_Leave__c,
                   INTEG_Employee_Id__c,
                   INTEG_Email__c,
                   INTEG_Manager_Email__c,
                   INTEG_Manager_Title__c,
                   INTEG_Hire_Date__c,
                   INTEG_GIA2__c,
                   INTEG_Business_Unit__c,
                   INTEG_Gov_Cloud_SPX_Production_Access__c,
                   Supportforce_Contact__c,
                   SForceContactLastModifiedDate__c,
                   Company_Agency_Name__c,
                   INTEG_First_Name__c,
                   INTEG_Last_Name__c,
                   Federation_Identifier__c,
                   INTEG_Manager__c,
                   INTEG_Manager__r.Federation_Identifier__c
            FROM INTEG_Employee__c
            WHERE Duplicate__c = false 
            AND (INTEG_Active__c = true
            OR (INTEG_Active__c = false AND LastModifiedDate = LAST_N_MONTHS:2))]);
    }

    public void execute(Database.BatchableContext BC, List<INTEG_Employee__c> integEmps) {
        // retrieve supportforce contact records
        // key'd by both email and unique employee id
        Map<String, Contact__x[]> supportForceContacts = getSupportForceContacts(integEmps);
        Set<String> setNotFoundContactIds = new Set<String>();
        List<INTEG_Employee__c> lstNotFoundEmployees = new List<INTEG_Employee__c>();

        // check for records that need updates
        INTEG_Employee__c[] updatedIntegEmps = new INTEG_Employee__c[0];
        for (INTEG_Employee__c integEmp : integEmps) {
            Contact__x[] matchedContacts = findEmployee(supportForceContacts, integEmp);
            if (matchedContacts.isEmpty() && integEmp.INTEG_Active__c == true) {
                if(!setNotFoundContactIds.contains(integEmp.Id)){
                    setNotFoundContactIds.add(integEmp.id);
                    INTEG_Employee__c employee = new INTEG_Employee__c(INTEG_Active__c=false,
                                                                       Id=integEmp.Id);
                    lstNotFoundEmployees.add(employee);
                }
                logError(NOT_FOUND_ERR_MSG, integEmp.Id);
            } else if (matchedContacts.size() > 1) {
                logError(DUPLICATE_ERR_MSG, integEmp.Id);
            } else {
                if(matchedContacts.size() > 0){
                    INTEG_Employee__c updatedIntegEmp = mapSupportContactToInteg(integEmp, matchedContacts[0]);

                    Date weekend = DateTime.now().date().toStartOfWeek().addDays(-1);
                    if(DateTime.now().date() == weekend) {
                        updatedIntegEmps.add(updatedIntegEmp);
                    } else {
                        if (hasChanged(integEmp, updatedIntegEmp)) {
                            updatedIntegEmps.add(updatedIntegEmp);
                        }           
                    }
                }
            }
        }

        if(updatedIntegEmps.size() > 0) {
            updateManagersInfo(updatedIntegEmps);
        }
        // save updates and track errors
        Boolean allOrNone = false;
        Database.SaveResult[] saveResults = Database.update(updatedIntegEmps, allOrNone);
        errorsWithIds = ErrorHelper.appendErrorMap(saveResults, updatedIntegEmps, errorsWithIds);

        if(lstNotFoundEmployees.size() > 0){
           saveResults = Database.update(lstNotFoundEmployees, allOrNone);
           errorsWithIds = ErrorHelper.appendErrorMap(saveResults, lstNotFoundEmployees, errorsWithIds);
        }
    }

    /**
     * Update Employee Managers Org62 Id's for Management ladder calculation
     * @param lstEmployees [List Of Employees]
     */
    private void updateManagersInfo(List<INTEG_Employee__c> lstEmployees){
        Set<String> setManagerEmails = new Set<String>();
        Set<String> setManagerFedIds = new Set<String>();
        for(INTEG_Employee__c employee : lstEmployees){
            if(String.isNotBlank(employee.INTEG_Manager_Email__c)){
                setManagerEmails.add(employee.INTEG_Manager_Email__c);    
            }
            if(String.isNotBlank(employee.INTEG_Manager__c)){
               setManagerFedIds.add(employee.INTEG_Manager__r.Federation_Identifier__c);
            }
        }

        List<INTEG_Employee__c> lstManagerEmps = [SELECT INTEG_Org62UserId__c,
                                                         INTEG_Email__c,
                                                         Federation_Identifier__c
                                                  FROM INTEG_Employee__c
                                                  WHERE INTEG_Email__c IN : setManagerEmails
                                                  OR Federation_Identifier__c IN : setManagerFedIds];

        Map<String,Id> mapManagerIds = new Map<String,Id>();
        Map<String,Id> mapManagerFedIds = new Map<String,Id>();

        for(INTEG_Employee__c empl : lstManagerEmps) {
            mapManagerIds.put(empl.INTEG_Email__c,empl.INTEG_Org62UserId__c);
            mapManagerFedIds.put(empl.Federation_Identifier__c, empl.INTEG_Org62UserId__c);
        }

        for(INTEG_Employee__c emp : lstEmployees) {
           if(mapManagerIds.get(emp.INTEG_Manager_Email__c) != null) {
              emp.INTEG_Manager_ID__c = mapManagerIds.get(emp.INTEG_Manager_Email__c);
           }else if(mapManagerFedIds.get(emp.Federation_Identifier__c) != null){
              emp.INTEG_Manager_ID__c = mapManagerFedIds.get(emp.Federation_Identifier__c);
           }
        }
    }

    public void finish(Database.BatchableContext BC) {
        if (!errorsWithIds.isEmpty()) {
            sendErrorEmail(errorsWithIds);
        }
    }

    /**
     * Locate the Contact__x[] matches for an INTEG_Employee__c record
     *  1. Looks for employee id (unique ids only)
     *  2. Looks by email
     * @param  supportForceContacts retrieved support force Contact__x records
     * @param  integEmp             INTEG_Employee__c record to look for a match
     * @return                      Array of possible matches, empty array if not found
     */
    private Contact__x[] findEmployee(Map<String, Contact__x[]> supportForceContacts, INTEG_Employee__c integEmp) {
        String eid = integEmp.INTEG_Employee_Id__c;
        String email = integEmp.INTEG_Email__c;
        String fedId = integEmp.Federation_Identifier__c;

        // NB: this step takes advantage of the fact we don't put non-unique employee id values in the retrieve Contact__x map
        if (String.isNotBlank(eid) && supportForceContacts.containsKey(eid)) {
            return supportForceContacts.get(eid);
        } else if (String.isNotBlank(email) && supportForceContacts.containsKey(email)) {
            return supportForceContacts.get(email);
        } else if (String.isNotBlank(fedId) && supportForceContacts.containsKey(fedId)) {
            return supportForceContacts.get(fedId);
        }else {
            return new Contact__x[0];
        }
    }

    /**
     * Generates an email with a summary of errors during the sync
     * @param errorsWithIds List of errors we got
     */
    private void sendErrorEmail(Map<String, List<String>> errorsWithIds) {
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        email.setToAddresses(Label.INTEG_SupportForce_SyncError_Recipients.split('\\s*,\\s*'));
        email.setSubject('INTEG_SupportForceSync_Batch ' + DateTime.now());
        email.setPlainTextBody(ErrorHelper.getErrorSummary(errorsWithIds));
        if (!Test.isRunningTest()) {
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { email });
        }
    }

    /**
     * Adds an error for a particular record id to the error logs
     * @param errMsg     error message
     * @param integEmpId id of INTEG_Employee__c record
     */
    private void logError(String errMsg, Id integEmpId) {
        if (!errorsWithIds.containsKey(errMsg)) {
            errorsWithIds.put(errMsg, new String[0]);
        }
        errorsWithIds.get(errMsg).add(integEmpId);
    }

    /**
     * Maps a Contact__x record to a INTEG_Employee__c record
     * @param  oldIntegEmp         Current INTEG_Employee__c record
     * @param  supportForceContact Contact__x source
     * @return                     new INTEG_Employee__c record
     */
    private INTEG_Employee__c mapSupportContactToInteg(INTEG_Employee__c oldIntegEmp, Contact__x supportForceContact) {

        INTEG_Employee__c newIntegEmp = new INTEG_Employee__c(
            INTEG_Active__c = oldIntegEmp.INTEG_Active__c,
            INTEG_Division__c = supportForceContact.Division_c__c,
            INTEG_Is_On_Leave__c = supportForceContact.On_Leave_c__c,
            INTEG_Manager_Email__c = supportForceContact.ReportsToId__r.Email__c,
            INTEG_Manager_Title__c = supportForceContact.ReportsToId__r.Title__c,
            INTEG_Hire_Date__c = supportForceContact.Start_Date_c__c,
            INTEG_Employee_ID__c = supportForceContact.Employee_ID_c__c,
            INTEG_First_Name__c = supportForceContact.FirstName__c,
            INTEG_Last_Name__c = supportForceContact.LastName__c,
            INTEG_Email__c = supportForceContact.Email__c,
            INTEG_GIA2__c = supportForceContact.GIA2_c__c,
            INTEG_Gov_Cloud_SPX_Production_Access__c = supportForceContact.Gov_Cloud_SPX_Production_Access_c__c,
            INTEG_Business_Unit__c = supportForceContact.Business_Unit_c__c,
            Federation_Identifier__c = supportForceContact.Federation_ID_c__c,
            Name = supportForceContact.Name__c,
            SForceContactLastModifiedDate__c = supportForceContact.LastModifiedDate__c,
            Company_Agency_Name__c = supportForceContact.Company_Agency_Name_c__c,
            Supportforce_Contact__c = supportForceContact.ExternalId,
            Id = oldIntegEmp.Id);

        // Only change active flag based on support for status if the employee is currently active in security org
        if (oldIntegEmp.INTEG_Active__c && supportForceContact.Status_c__c == ContactX_Constants.STATUS_DEACTIVE) {
            newIntegEmp.INTEG_Active__c = false;
        }else if (!oldIntegEmp.INTEG_Active__c && supportForceContact.Status_c__c == ContactX_Constants.STATUS_ACTIVE) {
            newIntegEmp.INTEG_Active__c = true;
        }

        return newIntegEmp;
    }

    /**
     * Returns true if new INTEG_Employee__c has changes that need to be committed
     * @param  oldIntegEmp current INTEG_Employee__c record in security org
     * @param  newIntegEmp update INTEG_Employee__c record from support force Contact__x record
     * @return             Boolean, true if changed
     */
    private Boolean hasChanged(INTEG_Employee__c oldIntegEmp, INTEG_Employee__c newIntegEmp) {
        return newIntegEmp.INTEG_Active__c != oldIntegEmp.INTEG_Active__c ||
               newIntegEmp.INTEG_Is_On_Leave__c != oldIntegEmp.INTEG_Is_On_Leave__c ||
               newIntegEmp.INTEG_Division__c != oldIntegEmp.INTEG_Division__c ||
               newIntegEmp.INTEG_Manager_Email__c != oldIntegEmp.INTEG_Manager_Email__c ||
               newIntegEmp.INTEG_Hire_Date__c != oldIntegEmp.INTEG_Hire_Date__c ||
               newIntegEmp.INTEG_Employee_Id__c != oldIntegEmp.INTEG_Employee_Id__c ||
               newIntegEmp.Company_Agency_Name__c != oldIntegEmp.Company_Agency_Name__c ||
               newIntegEmp.Supportforce_Contact__c != oldIntegEmp.Supportforce_Contact__c ||
               newIntegEmp.INTEG_First_Name__c != oldIntegEmp.INTEG_First_Name__c ||
               newIntegEmp.INTEG_Last_Name__c != oldIntegEmp.INTEG_Last_Name__c ||
               newIntegEmp.INTEG_Email__c != oldIntegEmp.INTEG_Email__c ||
               newIntegEmp.Federation_Identifier__c != oldIntegEmp.Federation_Identifier__c ||
               newIntegEmp.INTEG_GIA2__c != oldIntegEmp.INTEG_GIA2__c ||
               newIntegEmp.INTEG_Business_Unit__c != oldIntegEmp.INTEG_Business_Unit__c ||
               newIntegEmp.INTEG_Gov_Cloud_SPX_Production_Access__c != oldIntegEmp.INTEG_Gov_Cloud_SPX_Production_Access__c ||
               newIntegEmp.INTEG_Manager_Title__c != oldIntegEmp.INTEG_Manager_Title__c;
    }

    /**
     * Retrieves all Contact__x records from SupportForce with employee/contractor/inactive
     * record types that match the email or unique employee ids.
     * @param  integEmps INTEG_Employee__c records to look for matches
     * @return           List of matches by email/employee id key
     */
    private Map<String, Contact__x[]> getSupportForceContacts(List<INTEG_Employee__c> integEmps) {
        Set<String> eids = collectEmployeeIds(integEmps);
        Set<String> emails = collectEmails(integEmps);
        Set<String> fedIds = collectFedIds(integEmps);

        Map<String, Contact__x[]> supportForceContacts = new Map<String, Contact__x[]>();
        for (Contact__x supportForceContact : [
            SELECT
                ReportsToId__r.Employee_ID_c__c,
                ReportsToId__r.Email__c,
                ReportsToId__r.Title__c,
                Company_Agency_Name_c__c,
                LastModifiedDate__c,
                Start_Date_c__c,
                Employee_ID_c__c,
                Division_c__c,
                Status_c__c,
                On_Leave_c__c,
                Email__c,
                Id,
                ExternalId,
                FirstName__c,
                LastName__c,
                Federation_ID_c__c,
                Name__c,
                GIA2_c__c,
                Business_Unit_c__c,
                Gov_Cloud_SPX_Production_Access_c__c
            FROM Contact__x
            WHERE RecordTypeId__c IN :(new String[] {
                ContactX_Constants.RT_ID_DEACTIVE,
                ContactX_Constants.RT_ID_INTERNAL,
                ContactX_Constants.RT_ID_INTERNAL_CONTRACTOR })
            AND (Employee_ID_c__c IN :eids OR Email__c IN :emails OR Federation_ID_c__c IN :fedIds)
        ]) {
            String eid = supportForceContact.Employee_ID_c__c;
            // Foundation employee ids start with 'F' in support force, but not
            // in the security org. Strip the 'F' so things match up
            if (String.isNotBlank(eid) && eid.startsWithIgnoreCase('F')) {
                eid = eid.substring(1);
            }
            if (String.isNotBlank(eid) && !ContactX_Constants.EMPLOYEE_ID_NON_UNIQUE_VALS.contains(eid)) {
                if (!supportForceContacts.containsKey(eid)) {
                    supportForceContacts.put(eid, new Contact__x[0]);
                }
                supportForceContacts.get(eid).add(supportForceContact);
            }

            String email = supportForceContact.Email__c;
            if (String.isNotBlank(email)) {
                if (!supportForceContacts.containsKey(email)) {
                    supportForceContacts.put(email, new Contact__x[0]);
                }
                supportForceContacts.get(email).add(supportForceContact);
            }

            String fedId = supportForceContact.Federation_ID_c__c;
            if (String.isNotBlank(fedId) && fedId != email) {
                if (!supportForceContacts.containsKey(fedId)) {
                    supportForceContacts.put(fedId, new Contact__x[0]);
                }
                supportForceContacts.get(fedId).add(supportForceContact);
            }
        }
        return supportForceContacts;
    }

    /**
     * Collect unique employee ids plus "Foundation" style employee ids
     * @param  integEmps Array of INTEG_Employee__c to collect from
     * @return           Set of unique employee ids
     */
    private Set<String> collectEmployeeIds(INTEG_Employee__c[] integEmps) {
        Set<String> eids = new Set<String>();
        for (INTEG_Employee__c integEmp : integEmps) {
            String eid = integEmp.INTEG_Employee_Id__c;
            if (String.isNotBlank(eid) && !ContactX_Constants.EMPLOYEE_ID_NON_UNIQUE_VALS.contains(eid)) {
                eids.add(eid);
                // Foundation employee ids start with 'F' in support force, but not
                // in the security org. Add an 'F' so we retrieve foundation Contact__x records
                eids.add('F' + eid);
            }
        }
        return eids;
    }

    /**
     * Collect unique emails
     * @param  integEmps Array of INTEG_Employee__c to collect from
     * @return           Set of unique emails
     */
    private Set<String> collectEmails(INTEG_Employee__c[] integEmps) {
        Set<String> emails = new Set<String>();
        for (INTEG_Employee__c integEmp : integEmps) {
            if (String.isNotBlank(integEmp.INTEG_Email__c)) {
                emails.add(integEmp.INTEG_Email__c);
            }
        }
        return emails;
    }

    /**
     * Collect unique emails
     * @param  integEmps Array of INTEG_Employee__c to collect from
     * @return           Set of unique federation Ids
     */
    private Set<String> collectFedIds(INTEG_Employee__c[] integEmps) {
        Set<String> fedIds = new Set<String>();
        for (INTEG_Employee__c integEmp : integEmps) {
            if (String.isNotBlank(integEmp.Federation_Identifier__c)) {
                fedIds.add(integEmp.Federation_Identifier__c);
            }
        }
        return fedIds;
    }
}
/**
* @author Lakshya Arora
* @email lakshya.arora@mtxb2b.com
* @desc Test class for KARL_FinalizationReadinessController
*/  
@isTest
public class KARL_FinalizationReadinessControllerTest {
    /**
* @author Lakshya Arora
* @email lakshya.arora@mtxb2b.com
* @desc method to create the test data.
*/   
    @TestSetup
    private static void makeData(){
        User opsAdminUser = ARL_TestDataFactory.createOpsAdmin();
        System.runAs(opsAdminUser){
            Audit_Cycle__c auditCycle = ARL_TestDataFactory.createAuditCycle();
            insert auditCycle;
            KARL_Audit_Firm__c auditFirm = ARL_TestDataFactory.createAuditFirm('testAuditFirm');
            insert auditFirm;
            KARL_Audit_Scope_Master_Data__c auditScope = ARL_TestDataFactory.createAuditScope();
            insert auditScope;
            KARL_Audit_Scope_Reports__c auditScopeReport = ARL_TestDataFactory.createAuditScopeReport(auditScope.Id,auditFirm.Id);
            insert auditScopeReport;
            KARL_Cycle_Audit_Report_Items__c  cycleAuditReportItemObj = ARL_TestDataFactory.createCycleAuditScopeReport(auditScopeReport.Id, auditCycle.Id);
            insert cycleAuditReportItemObj;
        }
    }
    
    /**
* @author Lakshya Arora
* @email lakshya.arora@mtxb2b.com
* @desc method to test the getFinalizationReadinessInfo functionality.
*/
    @isTest
    private static void getFinalizationReadinessInfoTest(){
        List<User> opsAdminUserList = ARL_TestDataFactory.fetchUser('abc@b.com.d');
        if(!opsAdminUserList.isEmpty()){
            System.runAs(opsAdminUserList[0]){
                List<KARL_Cycle_Audit_Report_Items__c> cycleAuditReportList = new List<KARL_Cycle_Audit_Report_Items__c>([SELECT Id FROM KARL_Cycle_Audit_Report_Items__c]);
                if(!cycleAuditReportList.isEmpty()){
                    KARL_FinalizationReadinessController.FinalizationReadinessWrapper finalizationReadinessWrapObj = KARL_FinalizationReadinessController.getFinalizationReadinessInfo(cycleAuditReportList[0].Id);
                    System.assert(!finalizationReadinessWrapObj.auditorReadinessConfirmation);
                    System.assert(!finalizationReadinessWrapObj.managementResponseApproved);
                    System.assert(!finalizationReadinessWrapObj.auditReportFinalDrafConfirmed);
                    System.assert(!finalizationReadinessWrapObj.approvedByLegal);
                    System.assert(!finalizationReadinessWrapObj.auditLetterTempsFormatted);
                    System.assert(!finalizationReadinessWrapObj.basisOfAssertionBOA);
                }
            }
        }
    }
    
    /**
* @author Lakshya Arora
* @email lakshya.arora@mtxb2b.com
* @desc method to test the updateCycleAuditReport functionality.
*/
    @isTest
    private static void updateCycleAuditReportTest(){
        List<User> opsAdminUserList = ARL_TestDataFactory.fetchUser('abc@b.com.d');
        if(!opsAdminUserList.isEmpty()){
            System.runAs(opsAdminUserList[0]){
                List<KARL_Cycle_Audit_Report_Items__c> cycleAuditReportList = new List<KARL_Cycle_Audit_Report_Items__c>([SELECT Id FROM KARL_Cycle_Audit_Report_Items__c]);
                if(!cycleAuditReportList.isEmpty()){
                    Id cycleAuditReportRecordId = cycleAuditReportList[0].Id;
                    KARL_FinalizationReadinessController.updateCycleAuditReport('Auditor_Readiness_Confirmation__c',true,cycleAuditReportRecordId);
                    List<KARL_Cycle_Audit_Report_Items__c> updatedCycleAuditReportList = [SELECT Id,Auditor_Readiness_Confirmation__c,Management_Responses_Approved__c,Audit_Report_Final_Draft_Confirmed__c,
                                                                                          Approved_by_Legal__c,Audit_Letter_Temps_Formatted__c,Basis_of_Assertion_BOA__c
                                                                                          FROM KARL_Cycle_Audit_Report_Items__c
                                                                                          WHERE Id =: cycleAuditReportRecordId];
                    if(!updatedCycleAuditReportList.isEmpty()){
                        KARL_Cycle_Audit_Report_Items__c obj = updatedCycleAuditReportList[0];
                        System.assert(obj.Auditor_Readiness_Confirmation__c);
                        System.assert(!obj.Management_Responses_Approved__c);
                        System.assert(!obj.Audit_Report_Final_Draft_Confirmed__c);
                        System.assert(!obj.Approved_by_Legal__c);
                        System.assert(!obj.Audit_Letter_Temps_Formatted__c);
                        System.assert(!obj.Basis_of_Assertion_BOA__c);
                    }
                }
            }
        }
    }
}
/***********************************************************
* Test class for PRT_ProjectStatusReportAndQuipBatch
* Created by 	- Prashant Gupta
************************************************************/
@isTest
public class PRT_ProjectStatusReportAndQuipBatchTest {
    
    @isTest
    public static void testSchedule(){
        test.startTest();
		PRT_ProjectStatusReportAndQuipBatch testsche = new PRT_ProjectStatusReportAndQuipBatch();
		String sch = '0 0 23 * * ?';
		system.schedule('Test status Check', sch, testsche );
        test.stopTest();
    }
    
    @testSetup
    public static void testSetup(){
       List<Project__c>  projectList = new List<Project__c>();
        
        List<Program__c>  programList = new List<Program__c>();
        programList = PRT_TestDataFactory.createPrograms(5,true);
        projectList = PRT_TestDataFactory.createProjects(5,false);
        for(Integer i=0;i<5;i++){
            projectList.get(i).Status__c = PRT_Constants.PRT_CR_STATUS_PROJECTKICKOFF;
            projectList.get(i).Program__c = programList.get(i).Id ;
            projectList.get(i).Projected_Date_of_Completion__c = Date.today()+5;
            projectList.get(i).Project_Start_Date__c = Date.today();
        }
		insert projectList;
    }
    @isTest
    public static void checkProjectStatusReportNotExist(){
        
        test.startTest();
        Id batchJobId = Database.executeBatch(new PRT_ProjectStatusReportAndQuipBatch(), 200);
        test.stopTest();
        List<Project_Status_Report__c> projectStatusReport=[SELECT id,Name,Overall_Project_Status__c,Budget_Status__c,
                                                            Schedule_Status__c,Latest_Overall_Project_Status__c,Project__c,Program__c
                                                            FROM Project_Status_Report__c];
        System.assertEquals(String.valueOf(projectStatusReport.size()), '5');
        for(Project_Status_Report__c psr : projectStatusReport){
            System.assert(psr.name.contains('Week of'));
            System.assert((psr.Project__c != null));
            System.assert((psr.Program__c != null));
            System.assertEquals(psr.Overall_Project_Status__c,null );
            System.assertEquals(psr.Schedule_Status__c,null);
            System.assertEquals(psr.Budget_Status__c,null );
            System.assertEquals(psr.Latest_Overall_Project_Status__c,null );
        }
       
    
    }
    @isTest
    public static void checkProjectStatusReportExist(){
       List<Project__c>  projectList = new List<Project__c>([SELECT Id,Name FROM Project__c]);
       List<Program__c>  programList = new List<Program__c>([SELECT Id,Name FROM Program__c]);
       List<Project_Status_Report__c> projectStatusReportList = new List<Project_Status_Report__c>();
       projectStatusReportList = PRT_TestDataFactory.createProjectStatusReport(5,false,'On Track','Green','Green');
        for(Integer i=0;i<5;i++){
            projectStatusReportList.get(i).project__c = projectList.get(i).Id;
            projectStatusReportList.get(i).Program__c = programList.get(i).Id;
        }
        insert projectStatusReportList;
        test.startTest();
        Id batchJobId = Database.executeBatch(new PRT_ProjectStatusReportAndQuipBatch(), 200);
        test.stopTest();
        Map<Id,List<Project_Status_Report__c>> projectIdToListMap = new Map<Id,List<Project_Status_Report__c>>();
      	
        for(Project_Status_Report__c psr :[SELECT id,Overall_Project_Status__c,Budget_Status__c,
                                           Schedule_Status__c,Latest_Overall_Project_Status__c,Project__c FROM Project_Status_Report__c]){
                                               
                                               if(!projectIdToListMap.containsKey(psr.Project__c)){
                                                   projectIdToListMap.put(psr.Project__c,new List<Project_Status_Report__c>());
                                               }       
                                               projectIdToListMap.get(psr.Project__c).add(psr);
        }
        
        for(Id projectId : projectIdToListMap.keySet()){
            List<Project_Status_Report__c> psrList = projectIdToListMap.get(projectId);
            System.assertEquals(psrList.get(0).Overall_Project_Status__c, psrList.get(1).Overall_Project_Status__c);
            System.assertEquals(psrList.get(0).Schedule_Status__c, psrList.get(1).Schedule_Status__c);
            System.assertEquals(psrList.get(0).Budget_Status__c, psrList.get(1).Budget_Status__c);
            System.assertEquals(psrList.get(0).Latest_Overall_Project_Status__c, psrList.get(1).Latest_Overall_Project_Status__c);
        }
    }
   
}
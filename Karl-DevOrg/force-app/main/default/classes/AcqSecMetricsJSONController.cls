global class AcqSecMetricsJSONController {
    @remoteAction
	global static String getJSON(String filename) {
		List<AcqSec_Metrics_JSON__c> results = [select data__c from AcqSec_Metrics_JSON__c Where Name=:filename AND acquisition__c=NULL];
        if (results.size() == 0) {
            return NULL;
        }
        String s = results[0].data__c;
        return s;
    }
	
	@remoteAction    
    public static String getJSONForAllAcquisitionsAsArray(String filename) {
        List<String> datas = new List<String>();
		List<AcqSec_Metrics_JSON__c> results = [select data__c from AcqSec_Metrics_JSON__c Where Name=:filename AND acquisition__c!=NULL];
        if (results.size() == 0) {
            return NULL;
        }
        for (AcqSec_Metrics_JSON__c am : results) {
            datas.add(am.data__c);
        }
        return '[' + String.join(datas, ',') + ']';
    }

	@remoteAction    
    global static String getJSONForAcquisition(String filename, String acquisition) {
		List<AcqSec_Metrics_JSON__c> results = [select data__c from AcqSec_Metrics_JSON__c Where Name=:filename AND acquisition__c=:acquisition];
        if (results.size() == 0) {
            return NULL;
        }
        String s = results[0].data__c;
        return s;        
    }
}
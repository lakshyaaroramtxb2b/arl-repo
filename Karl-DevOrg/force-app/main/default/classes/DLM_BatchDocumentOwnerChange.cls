/*
* Class Name: DLM_BatchDocumentOwnerChange
* Description:Batch Utility to update document owner when current owner is inactive / Used to capture role change of an owner.
* Author/Date: Hitesh Yadav / 01.28.2020
* Date New/Modified: 01.28.2020 // Swarnima - 03/31/2020 - T-09359
*/

public class DLM_BatchDocumentOwnerChange implements Database.Batchable<sObject>,Database.Stateful, schedulable{
    String query = 'SELECT Id, Name,Owner.Email,Owner.IsActive,OwnerId,Content_Business_Unit__c,Previous_Content_Owner_Is_Active__c, Custodian_Business_Unit__c,'+
                    'Content_Owner_Business_Unit_Change__c,Previous_Custodian_Owner_Is_Active__c ,Custodian_Owner_Business_Unit_Change__c , Content_Owner__c,Content_Owner__r.Email,'+
                    'Content_Owner__r.Manager__c,Content_Owner__r.Is_Active__c,Content_Owner__r.Business_Unit__c,'+
                    'Content_Custodian__c,Content_Custodian__r.Email,Content_Custodian__r.Manager__c,Content_Custodian__r.Is_Active__c,'+
                    'Content_Custodian__r.Business_Unit__c '+
                    'FROM IS_Document__c Where Content_Owner__c != null OR Content_Custodian__c != null';
                   
    List<IS_Document__c> documentList = new List<IS_Document__c>();

    public Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }
   
    public void execute(Database.BatchableContext bc, List<IS_Document__c> scope){
       
        Boolean roleChanged = false;
        Map<Id,Id> contentOwnerMap = new Map<Id,Id>();                      //Map to store inactive content owner's  
        Map<Id,Id> custodianOwnerMap = new Map<Id,Id>();                    //Map to store inactive custodian owner's
        Map<Id,String> contentRoleChangeMap = new Map<Id,String>();         //Map to store content owner's who's role got changed
        Map<Id,String> custodianRoleChangeMap = new Map<Id,String>();       //Map to store custodian owner's who's role got changed
        Map<Id,Boolean> ownerNeedChangeMap = new Map<Id,Boolean>();         //Map to differentiate between inactive / roles change of owner's

        // Loop through data and seperate out inactive Content owner / Content Custodian
        for(sObject sObj : scope) {
            IS_Document__c document = (IS_Document__c)sObj;
            if(document.Content_Owner__c != null){                              
                if(document.Content_Owner__r.Is_Active__c == false){                                    //Execute when Content owner is Inactive
                   
                    contentOwnerMap.put(document.Id, document.Content_Owner__c);
                    ownerNeedChangeMap.put(document.Id,true);
                }
                if(document.Content_Owner__r.Business_Unit__c != document.Content_Business_Unit__c){    //Execute when Content owner role change
                    contentRoleChangeMap.put(document.Id,document.Content_Owner__r.Business_Unit__c);
                }
            }
           
            if(document.Content_Custodian__c != null){                      
                if(document.Content_Custodian__r.Is_Active__c == false){                                //Execute when Custodian owner is Inactive
                   
                    custodianOwnerMap.put(document.Id, document.Content_Custodian__c);
                    ownerNeedChangeMap.put(document.Id,true);
                }
                if(document.Content_Custodian__r.Business_Unit__c != document.Custodian_Business_Unit__c){  //Execute when Custodian owner role change
                    custodianRoleChangeMap.put(document.Id,document.Content_Custodian__r.Business_Unit__c);
                }
            }    
        }      

        Map<Id,Id> contactContentMap = new Map<Id,Id>();                        //Map to store content owner and there manager id's.  
        Map<Id,Contact> managerContentMap = new Map<Id,Contact>();              //Map to store content owner manager detail.
        Map<Id,Id> contactCustodianMap = new Map<Id,Id>();                      //Map to store custodian owner and there manager id's.
        Map<Id,Contact> managerCustodianMap = new Map<Id,Contact>();            //Map to store custodian owner manager detail.
       
        // Create a map of of current Content Owner as an key and manager as a value
        if(contentOwnerMap.values().size() > 0)
        {
            for(Contact con : [Select ID,Manager_ID__c from Contact where Id IN: contentOwnerMap.values() AND Manager_ID__c != null] ){
                contactContentMap.put(con.Manager_ID__c,con.Id);
            }
            for(Contact con : [Select ID,Org62_User_ID__c,Business_Unit__c from Contact where Org62_User_ID__c IN: contactContentMap.keyset()] ){
                for (Id key : contactContentMap.keySet()) {
                    if(key == con.Org62_User_ID__c){
                        managerContentMap.put(contactContentMap.get(key),con);
                    }
                }
            }
            System.debug('managerContentMap--->'+managerContentMap);
        }
       
        // Create a map of of current content custodian as an key and manager as a value
        if(custodianOwnerMap.values().size() > 0)
        {
            for(Contact con : [Select ID,Manager_ID__c from Contact where Id IN: custodianOwnerMap.values() AND Manager_ID__c != null ] ){
                contactCustodianMap.put(con.Manager_ID__c,con.Id);
            }
            for(Contact con : [Select ID,Org62_User_ID__c,Business_Unit__c from Contact where Org62_User_ID__c IN: contactCustodianMap.keyset()] ){
                for (Id key : contactCustodianMap.keySet()) {
                    if(key == con.Org62_User_ID__c){
                        managerCustodianMap.put(contactCustodianMap.get(key),con);
                    }
                }
            }
        System.debug('managerCustodianMap--->'+managerCustodianMap);
        }    

       
        Map <Id,Contact> documentContentMap = new Map <Id,Contact>();                   //Map to align content owner manger with document.
        Map <Id,Contact> documentCustodianMap = new Map <Id,Contact>();                 //Map to align custodian owner manager with document.
        Map<Id,IS_Document__c> finaldocumentMap = new Map<Id,IS_Document__c>();         //Map to insert data back to document according to changes.
           
        // Create a map of Document Id as a key and Content owner manager as a value.
        for(Id key : contentOwnerMap.keySet()){
            for(Id innnerKey : managerContentMap.keySet()){
                if(contentOwnerMap.get(key) == innnerKey){
                    documentContentMap.put(key,managerContentMap.get(innnerKey));
                }
            }
        }
        System.debug('documentContentMap--->'+documentContentMap);

        // Create a map of Document Id as a key and Content Custodian manager as a value.
        for(Id key : custodianOwnerMap.keySet()){
            for(Id innnerKey : managerCustodianMap.keySet()){
                if(custodianOwnerMap.get(key) == innnerKey){
                    documentCustodianMap.put(key,managerCustodianMap.get(innnerKey));
                }
            }
        }
        System.debug('documentCustodianMap--->'+documentCustodianMap);

               
        // Iterate through content document map and update content document manger as an owner and add it to Map.
        for(Id Key : documentContentMap.keySet()){
           
            if(!finaldocumentMap.containsKey(Key)) {
               finaldocumentMap.put(Key, new IS_Document__c(Id = Key));
            }
            IS_Document__c document = finaldocumentMap.get(Key);
           
            if(contentRoleChangeMap.containsKey(key)){
                if(!ownerNeedChangeMap.containsKey(key)){
                document.Content_Owner_Business_Unit_Change__c = true;
                }
            }
            if(ownerNeedChangeMap.containsKey(key)){
                  document.Content_Owner__c = documentContentMap.get(Key).Id;
                  document.Content_Business_Unit__c = documentContentMap.get(Key).Business_Unit__c;
                  document.Previous_Content_Owner_Is_Active__c = true;
            }
        }

        // Iterate through custodian document map and update custodian document manger as an owner and add it to Map.
        for(Id Key : documentCustodianMap.keySet()){
           
            if(!finaldocumentMap.containsKey(Key)) {
               finaldocumentMap.put(Key, new IS_Document__c(Id = Key));
            }
           
            IS_Document__c document = finaldocumentMap.get(Key);
            if(custodianRoleChangeMap.containsKey(key)){
                 if(!ownerNeedChangeMap.containsKey(key)){
                  document.Custodian_Owner_Business_Unit_Change__c = true;
                 }
            }
            if(ownerNeedChangeMap.containsKey(key)){
                  document.Content_Custodian__c = documentCustodianMap.get(Key).Id;
                  document.Custodian_Business_Unit__c = documentCustodianMap.get(Key).Business_Unit__c;
                  document.Previous_Custodian_Owner_Is_Active__c =true;
            }
        }

        //update Documents on basis of finaldocumentMap if map size is greater than 0 else check for role change(only).
        if(finaldocumentMap.size() > 0) {
            Database.update(finaldocumentMap.values(), false);

        }else{                          
          // Insert in else statement if there is no change of owner and only change in role of owner.           
          if(contentRoleChangeMap.size() > 0){                  
                 for(Id Key : contentRoleChangeMap.keySet()){       //Iterate through documents and update business unit of content owner.
                    if(!finaldocumentMap.containsKey(Key)) {
                       finaldocumentMap.put(Key, new IS_Document__c(Id = Key));
                    }
                    IS_Document__c document = finaldocumentMap.get(Key);

                    if(contentRoleChangeMap.containsKey(key)){
                        document.Content_Owner_Business_Unit_Change__c = true;
                        document.Content_Business_Unit__c = contentRoleChangeMap.get(key);
                    }
                 }
            }
            if(custodianRoleChangeMap.size() > 0){
                for(Id key : custodianRoleChangeMap.keySet()){      //Iterate through documents and update business unit of custodian owner.
                    if(!finaldocumentMap.containsKey(key)) {
                       finaldocumentMap.put(Key, new IS_Document__c(Id = key));
                    }
                    IS_Document__c document = finaldocumentMap.get(key);

                    if(custodianRoleChangeMap.containsKey(key)){
                          document.Custodian_Owner_Business_Unit_Change__c = true;
                          document.Custodian_Business_Unit__c = custodianRoleChangeMap.get(key);
                    }
                 }
            }
            if(finaldocumentMap.size() > 0) {
                Database.update(finaldocumentMap.values(), false);
            }
        }
       
    }        
   
    public void finish(Database.BatchableContext bc){
       
    }    

    public void execute(SchedulableContext sc) {
       
        DLM_BatchDocumentOwnerChange batch = new DLM_BatchDocumentOwnerChange();
        Database.executeBatch(batch,200);
     
   }
}
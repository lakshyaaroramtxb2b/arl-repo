/**
 * Global constants for this app (Officehours app)
 * Use it only for global constants. If your constant's scope is limited to a feature
 * or a sub-set of the app, this is probably not a good place for that.
 *
 */
public with sharing class ESA_AppConstants {
    public static final String ENTITY_CODE_SUPPORTFORCE = 'SupportForce';
    public static final String ENTITY_CODE_GUS = 'GUS';
    public static final Integer ADV_NOTICE_DAYS = 1;
    public static final Integer MAX_ADV_BOOKING_DAYS = 16;
    public static final String SF_SEC_RECORDTYPEID = '0120000000001TgAAI';
    public static final String ORG_WIDE_EMAIL_ADDRESS = 'Intake App';
    public static final String ESA_DOCUMENTS_FOLDER = 'ESA_Documents';
    public static final String GUEST_USER_TYPE = 'Guest';
    public static final String COMMUNITY_SITE_TYPE = 'ChatterNetwork';
    public static final String ENTITY_CODE = 'entityCode';

    public static final String TYPE_CHECKBOX = 'Checkbox';
    public static final String TYPE_DATE = 'Date';
    public static final String TYPE_EMAIL = 'Email';
    public static final String TYPE_PHONE =  'Phone';
    public static final String TYPE_HEADER_SECTION =  'Header Section';
    public static final String TYPE_PICKLIST = 'Picklist';
    public static final String TYPE_MULTI_PICKLIST  = 'Picklist (Multi-Select)';
    public static final String TYPE_TEXT = 'Text (255)';
    public static final String TYPE_LONGTEXT = 'Text Area (Long)';
    public static final String TYPE_NUMBER = 'Number';
    public static final String TYPE_ATTACHMENT = 'Attachment';
    public static final String TYPE_LOOKUP = 'Lookup';

    public static final String INPUT_TYPE_HIDDEN = 'Hidden';
    public static final String INPUT_TYPE_REQUIRED = 'Required';
    public static final String INPUT_TYPE_READONLY = 'Readonly';

    public static final String SUPPORT_FORCE_URL = 'https://supportforce.my.salesforce.com/';
    public static final String CONCIERGE_URL = 'https://concierge.it.salesforce.com/tickets/SupportForce';

    public static final String PICKLIST_DEFAULT ='- select -';

    public static final String IN_PROGRESS_STATUS_KEY = 'IN_PROGRESS';
    public static final String CLOSED_STATUS_KEY = 'CLOSED';
    public static final String INPROGRESS_STATUS_KEY = 'In Progress';
    public static final String SUBMITTED_STATUS_KEY = 'Submitted';

    public static final String ACTION_ALERT_NAME = 'Dialog Alert';
    public static final String ACTION_CONFIRM_NAME = 'Dialog Confirm';
    public static final String ACTION_VALIDATION_NAME = 'Validation Error';
    public static final String ACTION_EVENT_NAME = 'Publish Platform Event';
    public static final String ACTION_OPERATION_NAME = 'Perform Operation';
    public static final String ACTION_SCORE_NAME = 'Set Score';
    public static final String ACTION_CLOSE_REQUEST_NAME = 'Close Request';
    public static final String ACTION_REST_API_NAME = 'REST API Call';

    public static final String APPROVAL_APPROVED_STATUS = 'Approve';
    public static final String APPROVAL_PENDING_STATUS = 'Pending';
    public static final String APPROVAL_REJECTED_STATUS = 'Reject';

    public static final Map<String, String> SF_PRIORITY = new Map<String, String>{'Low' => '3 - Low', 'Normal' => '3 - Low', 'Urgent' => '2 - Medium', 'Critical' => '1 - High'};
    public static final Map<String, String> GUS_PRIORITY = new Map<String, String>{'Low' => '4', 'Normal' => '3', 'Urgent' => '2', 'Critical' => '1'};
    public static final Map<String, String> DAY_SHORT_NAME = new Map<String, String>{'Monday' => 'Mon', 'Tuesday' => 'Tue', 'Wednesday' => 'Wed', 'Thursday' => 'Thu', 'Friday' => 'Fri'};
    public static final String INTERNAL_SUPPORT_CATEGORY_ESA = 'Security: Enterprise Security: Security Architecture/Design Review';
    public static final String INTERNAL_SUPPORT_CATEGORY_ASA = 'Security: Enterprise Security: Application Threat Review';
    public static final String INTERNAL_SUPPORT_CATEGORY_ACQ = 'Security: Enterprise Security: Acquisition Security Review';

    public static final List<String> TRACK_NUMBER_FIELDS_SET = new List<String>{'Tracked_Number_01__c','Tracked_Number_02__c',
                                                                                'Tracked_Number_03__c','Tracked_Number_04__c',
                                                                                'Tracked_Number_05__c','Tracked_Number_06__c',
                                                                                'Tracked_Number_07__c','Tracked_Number_08__c',
                                                                                'Tracked_Number_09__c','Tracked_Number_10__c',
                                                                                'Tracked_Number_11__c','Tracked_Number_12__c',
                                                                                'Tracked_Number_13__c','Tracked_Number_14__c',
                                                                                'Tracked_Number_15__c'};

    public static final List<String> TRACK_DATE_FIELDS_SET = new List<String>{'Tracked_Date_01__c','Tracked_Date_02__c',
                                                                              'Tracked_Date_03__c','Tracked_Date_04__c',
                                                                              'Tracked_Date_05__c','Tracked_Date_06__c',
                                                                              'Tracked_Date_07__c','Tracked_Date_08__c',
                                                                              'Tracked_Date_09__c','Tracked_Date_10__c'};

    public static final List<String> TRACK_STRING_FIELDS_SET = new List<String>{'Tracked_String_01__c','Tracked_String_02__c',
                                                                                'Tracked_String_03__c','Tracked_String_04__c',
                                                                                'Tracked_String_05__c','Tracked_String_06__c',
                                                                                'Tracked_String_07__c','Tracked_String_08__c',
                                                                                'Tracked_String_09__c','Tracked_String_10__c',
                                                                                'Tracked_String_11__c','Tracked_String_12__c',
                                                                                'Tracked_String_13__c','Tracked_String_14__c',
                                                                                'Tracked_String_15__c','Tracked_String_16__c',
                                                                                'Tracked_String_17__c','Tracked_String_18__c',
                                                                                'Tracked_String_19__c','Tracked_String_20__c',
                                                                                'Tracked_String_21__c','Tracked_String_22__c',
                                                                                'Tracked_String_23__c','Tracked_String_24__c',
                                                                                'Tracked_String_25__c'};

    public static Id ESA_REQUEST_QUEUE_ID {private set;
        get {
            if (ESA_REQUEST_QUEUE_ID == null) {
                ESA_REQUEST_QUEUE_ID = [SELECT Id FROM Group WHERE DeveloperName = 'ESA_OH_Request_Queue'].Id;
                return ESA_REQUEST_QUEUE_ID;
            } else {
                return ESA_REQUEST_QUEUE_ID;
            }
        }


    }

    /**
     * Constants for Security Business Operations (BUS)
     */

    public static final String BUS_USER_NAME_SANDBOX ='jalkove@salesforce.com.security.entsecqa';

    // Gus record type and Product tag information - Create Case to GUS work record
    public static final String GUS_WORK_RECORD_TYPE_ID = '0129000000006gDAAQ';
    
    public static final String GUS_FOUND_IN_BUILD_NA_ID = 'a06T0000001Vew1IAC';

    //public static final String GUS_FOUND_IN_BUILD_NA_ID = 'a06B0000000uziqIAA';  //For sandbox testing.

    public static final String SFORCE_SECURITY_QUEUE_ID = '00G70000001OtrU';

    public static final String SFORCE_CASE_STATUS_NEW = 'New';

    public static final String GUS_WORK_OBJECT_API_NAME = 'ADM_Work_c__x';
    public static final String SUPPORTFORCE_CASE_OBJECT_API_NAME = 'Case__x';

    public static final String GUS_BUG = 'Gus Bug';
    public static final String GUS_INVESTIGATION = 'Gus Investigation';
    public static final String GUS_USER_STORY = 'Gus User Story';
    public static final String SUPPORTFORCE_CASE = 'Supportforce Case Queue';

    public static final String APPOINTMENT_OVERRIDE_SELECTION_RTYPE='0123A000000Ab3tQAC';

    public static final String SEC_CHAMPS_ENTITY_CODE = 'SecChamps';

    public static final String PUB_GROUP_ENT_SEC_APPS = 'EntSecApps';
    public static final String QUEUE_SEC_ASSURANCE = 'Trust_Application_Security_Assurance';

    //Status valeus for Travel Requests.

    public static final String INTERNAL_STATUS_APPROVED = 'Approved';
    public static final String INTERNAL_STATUS_REJECTED = 'Rejected';
    public static final String INTERNAL_STATUS_CLOSED = 'Closed';
    public static final String TRAVEL_TYPE = 'Travel';

    public static final String ESA_INTAKE_PROFILE = 'ESA CCP';
    
    //Email templates
    public static final String DEFAULT_SUBMITTED_TEMPLATE = 'Submitted Request team email';

}
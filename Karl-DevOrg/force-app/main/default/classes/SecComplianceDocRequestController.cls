public without sharing class SecComplianceDocRequestController {
    public String name {get;set;}
    public String company {get;set;}
    public String email {get;set;}
    public String docs {get;set;}
    
    public Boolean showForm {get;set;}
    

    public SecComplianceDocRequestController() {
        showForm = True;
    }
    public List<Document> getAvailableDocs() {
        return [SELECT Name,Id,Description FROM Document WHERE isDeleted=False AND FolderId IN (Select Id From Folder WHERE DeveloperName = 'Released_Compliance_Docs')];
    }
    
    public PageReference submitRequest() {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, Label.DOC_LINK_SENT_TO_EMAIL));
        showForm = False;
        return null;
    }
    
}
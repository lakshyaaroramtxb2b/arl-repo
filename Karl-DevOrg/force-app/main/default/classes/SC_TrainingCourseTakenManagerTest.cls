/*
 * Apex test class to support Trail enrollment tests
 */
@isTest
private class SC_TrainingCourseTakenManagerTest {

    static testMethod void testTakenRelationships() {
        Contact con = new Contact(
            LastName = 'McTester'
        );
        insert con;

        Training_Course__c trail = new Training_Course__c(
            Name = 'Trail Training Course',
            Provider_Id__c = 'training_course',
            Course_Level__c = TrainingCourse_Constants.COURSE_LEVEL_TRAIL,
            Sync_Id__c = 'trail_training_course'
        );
        insert trail;

        List<Training_Course__c> modules = new List<Training_Course__c>();
        Training_Course__c module1 = new Training_Course__c(
            Name = 'Module Training Course 1',
            Provider_Id__c = 'training_course_1',
            Course_Level__c = 'Module',
            Sync_Id__c = 'module_training_course_1'
        );
        modules.add(module1);

        Training_Course__c module2 = new Training_Course__c(
            Name = 'Module Training Course 2',
            Provider_Id__c = 'training_course_2',
            Course_Level__c = 'Module',
            Sync_Id__c = 'module_training_course_2'
        );
        modules.add(module2);
        insert modules;

        Integer counting = 0;
        List<Training_Course_Relationship__c> tcRels = new List<Training_Course_Relationship__c>();
        for (Training_Course__c module :modules) {
            counting += 1;
            tcRels.add(new Training_Course_Relationship__c(
                Related_Path__c = trail.Id,
                Related_Module__c = module.Id,
                Sync_Id__c = 'rel_' + 'trail_training_course' + '_module_training_course_' + String.valueOf(counting)
            ));
        }
        insert tcRels;

        Training_Course_Taken__c moduleTaken = new Training_Course_Taken__c(
            Contact__c = con.Id,
            Training_Course__c = modules[0].Id,
            Attendee_Email__c = 'test.tester@test.com',
            Progress__c = 0
        );
        insert moduleTaken;
        Training_Course_Taken__c trailTaken = new Training_Course_Taken__c(
            Contact__c = con.Id,
            Training_Course__c = trail.Id,
            Attendee_Email__c = 'test.tester@test.com',
            Progress__c = 0
        );
        insert trailTaken;

        List<Training_Course_Taken_Relationship__c> takenRelationships =
            [select Id from Training_Course_Taken_Relationship__c];
        System.assertEquals(
            2,
            takenRelationships.size(),
            'Number of Taken Relationship error'
        );

        // update taken module to completed
        moduleTaken.Date_Completed__c = System.today();
        moduleTaken.Date_Started__c = System.today();
        moduleTaken.Progress__c = 100;
        update moduleTaken;

        trailTaken = [
            select
                Progress__c,
                Status__c,
                Date_Started__c,
                Last_Import_Date__c
            from Training_Course_Taken__c
            where Id = :trailTaken.Id
        ];
        System.assertEquals(
            50.0,
            trailTaken.Progress__c,
            'Failed to update progress'
        );
        System.assertEquals(
            'Course Started',
            trailTaken.Status__c,
            'Failed Status Test'
        );
        System.assertEquals(
            System.today(),
            trailTaken.Date_Started__c,
            'Failed Date Started Test'
        );
        System.assertEquals(
            System.today(),
            trailTaken.Last_Import_Date__c,
            'Failed Last import test'
        );

        // update the other module for 100% test
        moduleTaken = [
            select Id
            from Training_Course_Taken__c
            where Id != :moduleTaken.Id
            and Course_Level__c = 'Module'
        ];
        moduleTaken.Date_Completed__c = System.today();
        moduleTaken.Date_Started__c = System.today();
        moduleTaken.Progress__c = 100;
        update moduleTaken;

        trailTaken = [
            select
                Progress__c,
                Status__c,
                Date_Started__c,
                Last_Import_Date__c
            from Training_Course_Taken__c
            where Id = :trailTaken.Id
        ];
        System.assertEquals(
            100.0,
            trailTaken.Progress__c,
            'Failed 100% update progress'
        );
        System.assertEquals(
            'Course Completed',
            trailTaken.Status__c,
            'Failed 100% Status Test'
        );
        System.assertEquals(
            System.today(),
            trailTaken.Date_Started__c,
            'Failed 100% Date Started Test'
        );
        System.assertEquals(
            System.today(),
            trailTaken.Last_Import_Date__c,
            'Failed 100% Last import test'
        );
    }
}
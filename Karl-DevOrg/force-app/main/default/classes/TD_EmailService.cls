/**
 * Email services are automated processes that use Apex classes
 * to process the contents, headers, and attachments of inbound
 * email.
 */
 
global class TD_EmailService implements Messaging.InboundEmailHandler {
    
    public static final String ORG_WIDE_EMAIL_ADDRESS = 'Trust Deliverables';    
    public static final String ORG_WIDE_REPLYTO_EMAIL_ADDRESS = 'trustdeliverables@salesforce.com';    
    public static final String ORG_WIDE_REPLYTO_DISPLAY_NAME = 'Trust Deliverables';    
    private static final String DONE_STATUS = 'Done';    
    private static final String DELIVERABLE_ID_MARKER = '{trustdeliverablereferenceid=';    
    private static final String FROM_ADDRESS_MARKER = 'stakeholderemailaddress=';
    private static final String ALPHANUMERIC = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' 
                                             + 'abcdefghijklmnopqrstuvwxyz'
                                             + '0123456789';  
                                             
                                         
     
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
        
        
        system.debug('email htmlbody -> ' + email.htmlBody);
        system.debug('email textbody -> ' + email.plainTextBody);
        system.debug('email envelope -> ' + envelope);
        
        
        // scan email body and determine if this is a "done" email
        string emailBody = email.plainTextBody;
        if (string.isBlank(emailBody)) emailBody = email.htmlBody;
        
        if (emailBody.contains(DELIVERABLE_ID_MARKER) && emailBody.containsIgnoreCase(DONE_STATUS)) {
            // if it is then scan body and find deliverable id
            Integer id_index_begin = emailBody.indexOf(DELIVERABLE_ID_MARKER);
            String deliverable_id;
            if (id_index_begin > 0) {
                String temp = emailBody.substring(id_index_begin);
                deliverable_id = temp.substringBetween(DELIVERABLE_ID_MARKER, '}'); 
                if (deliverable_id != null) {
                    String stakeholderEmail = getFromAddress(emailBody);
                    if (!string.isBlank(stakeholderEmail)) markComplete(deliverable_id, stakeholderEmail);
                }
            }
        }        
        
        return null;        
    }
    
    private static String getFromAddress(String emailBody) {
        String fromAddress = '';
        Integer from_index_begin = emailBody.indexOf(FROM_ADDRESS_MARKER);
        if (from_index_begin > 0) {
            // extract actual email address from this index
            fromAddress = emailBody.substring(from_index_begin);
            // some emails salesforce creates a link out of email addresses some dont so this covers both 
            fromAddress = fromAddress.substringBetween(FROM_ADDRESS_MARKER, '}'); 
            if (String.isBlank(fromAddress)) {
                fromAddress = fromAddress.substringBetween('stakeholderemailaddress=', '"'); 
            }          
            if (fromAddress.contains('<a href')) {
                fromAddress = fromAddress.substringBetween('mailto:', '"');
            }
            if (String.isBlank(fromAddress)) {
                fromAddress = fromAddress.substringBetween('stakeholderemailaddress=', '<');               
            }
            if (fromAddress.contains('>')) fromAddress = fromAddress.replaceAll('>', '');       
            if (fromAddress.contains(' ')) fromAddress = fromAddress.replaceAll(' ', '');       
        }
        return fromAddress;
    }
    
    public static void markComplete(String deliverable_id, String fromAddress) {
        
        system.debug('deliverable_id: ' + deliverable_id);
        system.debug('fromAddress: ' + fromAddress);
        
        try {
            
           // get deliverable which must match the sender email address as well as the deliverable Id
           Trust_Deliverable__c deliverable = [select Id, (select Stakeholder__r.Email, RACI_Level__c from Deliverable_Stakeholders__r), Name 
                                               from Trust_Deliverable__c 
                                               where Id = :deliverable_id.trim()];
                                              
           // mark status as completed if email is from one of the responsible stakeholder 
           for (Trust_Deliverable_Stakeholder__c tds : deliverable.Deliverable_Stakeholders__r) {
                system.debug('tds = ' + tds);
                system.debug('tds stakeholder email = ' + tds.Stakeholder__r.Email);
                if (tds.Stakeholder__r.Email == fromAddress.trim()
                && (tds.RACI_Level__c.contains(TD_Deliverable_Form.ACCOUNTABLE)
                || tds.RACI_Level__c.contains(TD_Deliverable_Form.RESPONSIBLE))                
                ) {
                    deliverable.Status__c = 'Completed'; 
                    update deliverable;  
                    break;
                }
           }
            
           // post to chatter feed             
           string stakeholderName = '';
           try {
               stakeholderName += [Select Stakeholder__r.Name from Trust_Valid_Stakeholder__c
                            where Stakeholder__r.Email = :string.escapeSingleQuotes(fromAddress.trim())].Stakeholder__r.Name;
               insert new FeedItem (
                   Body = TD_StakeholderChatterHelper.STAKEHOLDER_IS_DONE_TITLE + ': ' + stakeholderName,
                   ParentId = deliverable.Id,
                   Visibility = 'AllUsers',
                   Title = TD_StakeholderChatterHelper.STAKEHOLDER_IS_DONE_TITLE
               );

           } catch (exception e) {
               system.debug('exception trying to post chatter feed for stakeholder email: ' + fromAddress + ' exception: ' + e );
           }
                                       
       } catch (exception e) {system.debug('exception during trust deliverable complete action: ' + e);}    
            
    }
}
global with sharing class BacklogMonitorJob implements Schedulable {

    public static void setSchedule() {
        // this function will set this class to run as a scheduled apex job every hour in the 5th minute.
        // null update to change email notifications
        BacklogMonitorJob a = new BacklogMonitorJob();
        String schStr = '0 30 * * * ?'; // this defines the times it should run at (every hour on the 5th minute forever)
        id cronid = System.schedule('Backlog Monitor Job', schStr, a);
        PopcrabStatus__c ps = getPs();
        ps.textvalue__c = String.valueOf(cronid);
        update(ps); //store job id here
    }
    
    @future(callout=true)
    public static void runAll() {
        BatchNotifier bn = new BatchNotifier('PopcrabMonitor');
        CodeScanBacklogChecker.run(bn);
        ZipUploadBacklogChecker.run(bn);
        TZBacklogChecker.run(bn);
        ScanQueueBacklogChecker.run(bn);
        ScanInfoBacklogProcessor.run(bn);
        bn.processBatch();
    }
    
    private static PopcrabStatus__c getPs() {
       PopcrabStatus__c ps = [SELECT Name, textvalue__c FROM PopcrabStatus__c 
                               WHERE Name='ScheduledJobId' LIMIT 1][0];
        return ps;
    }
    
    public static void killJob() {
        PopcrabStatus__c ps = getPs();
        if (ps.textvalue__c == 'NONE') {
            //cannot abort
            SObjectException e = new SObjectException();
            e.setMessage('Cannot abort as job id not found');                
            throw e;
        } else {
            System.abortJob(ps.textvalue__c);
            ps.textvalue__c = 'NONE';
            update(ps);
        }
            
    }

    global void execute(SchedulableContext ctx)     {
        runAll();
    }
}
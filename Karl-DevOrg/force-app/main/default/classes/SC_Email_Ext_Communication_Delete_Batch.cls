/**
* @author: Hardik
* @description: Batch Class to delete all records for SC_CISO_CampaignMember__c object
*/
public with sharing class SC_Email_Ext_Communication_Delete_Batch implements Database.Batchable<sObject>, Database.Stateful {
    Static Id currentUserId;
    
    // constructor
    public SC_Email_Ext_Communication_Delete_Batch() {
        
    }
    // Query to be passed to start method
    public static final String BASE_SELECT =
        'SELECT Id, Name, CompanyOrAccount__c, Email__c, FirstName__c, LastName__c, Status__c, Account_Emails__c , SuccessOrError__c ' +
        ' FROM SC_CISO_CampaignMember__c';
    
    
    // start method for batch class
    public Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(BASE_SELECT);
    }
    
    
    // execute method for batch class
    public void execute(Database.BatchableContext BC, List<SC_CISO_CampaignMember__c> listOfCM) {
        
        Database.delete(listOfCM, false);
        
    }
    
    
    // finish method for batch class
    public void finish(Database.BatchableContext BC) {
        
    }    
    
}
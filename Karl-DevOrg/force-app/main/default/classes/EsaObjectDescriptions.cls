/**
 * Object Description - Label, API Names, Data types etc...
 */

public class EsaObjectDescriptions {
	@AuraEnabled
	public static Map<String, Object> getObjectFields(String serviceItemquestionId){

        String objectName;
        Map<String, Object> mapObjectValues = new Map<String, Object>();

        ESA_Service_Item_Question__c eSitemQuestion = [SELECT Id,
                                                                  ESA_Security_Question__r.Entity__r.Work_Items_Org__c,
                                                                  ESA_Security_Question__r.Entity__r.External_Interface_Not_Required__c
                                                           FROM ESA_Service_Item_Question__c
                                                           WHERE Id=:serviceItemquestionId];

        if(eSitemQuestion.ESA_Security_Question__r.Entity__r.External_Interface_Not_Required__c){
             return null;
        }

        if(eSitemQuestion.ESA_Security_Question__r.Entity__r.Work_Items_Org__c == 'GUS'){
            objectName = ESA_AppConstants.GUS_WORK_OBJECT_API_NAME;
            mapObjectValues.put('ObjectLabel','GUS Work Object');
        }else{
            objectName = ESA_AppConstants.SUPPORTFORCE_CASE_OBJECT_API_NAME;
            mapObjectValues.put('ObjectLabel','Supportforce Case Object');
        }

		Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
		Schema.SObjectType leadSchema = schemaMap.get(objectName);
		Map<String, Schema.SObjectField> fieldMap = leadSchema.getDescribe().fields.getMap();

		List<EsaObjectDescription> lstEsaObjectDescription = new List<EsaObjectDescription>();
		EsaObjectDescription esaObjDescription;
		Integer accessCount = 0, denyCount = 0, count = 0;
		for (String fieldName: fieldMap.keySet()) {
    		    Schema.DescribeFieldResult fieldInfo = fieldMap.get(fieldName).getDescribe();
                //if(fieldInfo.isUpdateable() == true) continue;
                esaObjDescription = new EsaObjectDescription();
                esaObjDescription.isUpdateable = fieldInfo.isUpdateable();
    		    esaObjDescription.fieldLabel = fieldInfo.getLabel();
    		    esaObjDescription.fieldApiName = fieldName;
    		    esaObjDescription.fieldDataType = String.ValueOf(fieldInfo.getType());
    		  	esaObjDescription.fieldLength = String.ValueOf(fieldInfo.getLength());
    			esaObjDescription.fieldType = String.ValueOf(fieldInfo.getType()) + ' (' +
    			   								 String.ValueOf(fieldInfo.getLength()) + ')';
    		   	lstEsaObjectDescription.add(esaObjDescription);

		}

        mapObjectValues.put('ObjectFields',lstEsaObjectDescription);
        return mapObjectValues;
		//return lstEsaObjectDescription;
	}

	public class EsaObjectDescription {
        @AuraEnabled
        public String fieldLabel {get;set;}
        @AuraEnabled
        public String fieldApiName {get;set;}
        @AuraEnabled
        public String fieldDataType {get; set;}
        @AuraEnabled
        public String fieldLength {get; set;}
        @AuraEnabled
        public String fieldType {get; set;}
        @AuraEnabled
        public Boolean isUpdateable {get; set;}

        public EsaObjectDescription(){

        }

    }

}
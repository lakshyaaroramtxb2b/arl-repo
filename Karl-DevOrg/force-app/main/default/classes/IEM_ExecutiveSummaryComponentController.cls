/***************IEM_CaseListController****************
@Author Prashant
@Date 08/20/2019
@Description Apex class to get case and milestone data for IEM_ExecutiveSummaryComponent Cmp.
**********************************************/
public with sharing class IEM_ExecutiveSummaryComponentController {
    /*
    * Created by - Prashant
    * This functon is used to get case record data by id.
    */
    @AuraEnabled
    public static Case getRecord(ID recordId){        
        return [SELECT ID,  Subject,Description,IEM_Case_Type__c,Business_Justification__c,
                Definition_of_Done__c,Action_Plan__c,Cloud__c,Cloud__r.Name__c,Team_Responsible__c,
                Team_Responsible__r.Name__c,Owner.Name,Status,Vendor_Name__c,
                OwnerId,Highest_Data_Classification__c,
                IEM_Analyst_Key_Notes__c,IEM_Analyst_Recommendation__c,
                Estimated_Remediation_Date__c,
                Business_Approver__c,Business_Approver__r.Name,
                Security_Reviewer__r.Name,Business_Reviewer__r.Name,Residual_Severity__c,
                External_Action_Plan_Link__c,Security_Assurance_SME_User__r.Name,
                Issue_Owner__c,Issue_Owner__r.Name,Action_Plan_Owner__r.Name FROM CASE WHERE ID =: recordId WITH SECURITY_ENFORCED];
                /* IRCloud__Due_Date__c FROM CASE WHERE ID =: recordId WITH SECURITY_ENFORCED]; */
    }
    
    /*
    * Created by - Prashant
    * This functon is used to get milestone record data by id.
    */
    @AuraEnabled
    public static MilestoneWrapper getMileStones(ID recordId){ 
        id securityReviewerUserId;
        for(Case caseRecord :[SELECT id, Subject, Security_Reviewer__c FROM case WHERE id =: recordId LIMIT 1]){
            securityReviewerUserId = caseRecord.Security_Reviewer__c;
        }
        MilestoneWrapper mWrapper = new MilestoneWrapper();
        mWrapper.isStandardUser = (UserInfo.getUserType() == 'Standard' ? true : false);
        mWrapper.isSecurityReviewer = (UserInfo.getUserId() == securityReviewerUserId ? true : false); 
        mWrapper.milestoneList = [SELECT ID,  Name,Status__c,Description__c, Due_Date__c FROM Milestone__c WHERE Case__c =: recordId];
        return mWrapper;
    }
    
    /*
    * Created by - Prashant
    * Wrapper to return milestone data.
    */
    public class MilestoneWrapper {
        @AuraEnabled 
        public Boolean isStandardUser {get;set;}
        @AuraEnabled
        public Boolean isSecurityReviewer {get;set;}
        @AuraEnabled
        public List<Milestone__c> milestoneList {get;set;}
        public MilestoneWrapper(){
            this.milestoneList = new List<Milestone__c>();
            this.isStandardUser = true;
            this.isSecurityReviewer = true;
        }
    }
}
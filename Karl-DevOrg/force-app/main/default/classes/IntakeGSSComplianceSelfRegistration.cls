global class IntakeGSSComplianceSelfRegistration extends IntakeWhiteListedSelfRegistration {
    global override String getEntityCode() {
        return 'GBSExternal';
    }
        
    global override String[] getPermissionSetNames() {
        return new String[]{'ESA_Security_Request_Customer'};
    }
}
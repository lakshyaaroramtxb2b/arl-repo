public with sharing class PRT_V2MOMChangeReqTriggerHelper {
    
    public static List<ADM_Product_Tag_c__x> mockedProductTagList = new List<ADM_Product_Tag_c__x>();
    public static List<V2MOM_c__x> mockedV2momList  = new List<V2MOM_c__x>();
    public static List<ADM_Scrum_Team_c__x> mockedGrcPmoTeam = new List<ADM_Scrum_Team_c__x>();
    public static List<Org62User__x> mockedOwnerList = new List<Org62User__x>();
    public static List<String> mockedOwnerIds = new List<String>();

    public static void createUserStoryWorkRecord(List<V2MOM_Change_Request__c> changeReqList, Map<Id,V2MOM_Change_Request__c> idToChangeReqMap) {
        Set<Id> changeRequestIdSet = new Set<Id>();

        for(V2MOM_Change_Request__c changeReq : changeReqList) {
            if(idToChangeReqMap!=null && changeReq.Status__c != idToChangeReqMap.get(changeReq.Id).Status__c) {
                changeRequestIdSet.add(changeReq.Id);
            }
        }
        createRecord(changeRequestIdSet);
    } 

    @future (callout = true) 
    public static void createRecord(Set<Id> changeReqIdSet) { 
        List<V2MOM_Change_Request__c> changeReqList = new List<V2MOM_Change_Request__c>([SELECT Id, V2MOM__c, V2MOM_Owner__c, V2MOM_Owner__r.ManagerId 
                                                                                         FROM V2MOM_Change_Request__c 
                                                                                         WHERE Id IN: changeReqIdSet]);
        List<ADM_Product_Tag_c__x> productTagList = new List<ADM_Product_Tag_c__x>();
        if(!Test.isRunningTest()) {
            productTagList = [SELECT Id, Name__c, ExternalId FROM ADM_Product_Tag_c__x WHERE Name__c = 'Pending V2MOM Update'];
        }
        else {
            productTagList = mockedProductTagList;
        }
        Method_and_Measure_Change__c methodAndMeasureChange = new Method_and_Measure_Change__c();
        Set<Id> v2momIdSet = new Set<Id>();
        Set<Id> v2momOwnerManagerIdSet = new Set<Id>();
        Map<Id, String> managerIdToEmpNumMap = new Map<Id, String>();
        Map<String, Id> managerEmpNumToExtIdMap = new Map<String, Id>();
        Map<Id, String> v2momIdToNameMap = new Map<Id, String>();
        Map<Id, Id> v2momIdToOwnerIdMap = new Map<Id, Id>();
        Map<Id, String> ownerIdToEmpNumMap = new Map<Id, String>();
        Map<String, Id> empNumToGusUserIdMap = new Map<String, Id>(); 
        Map<Id, Id> ownerToManagerIdMap = new Map<Id, Id>();
        List<Org62User__x> v2momOwnerManagerList = new List<Org62User__x>();
        
        System.debug('productTagList: '+productTagList);
        
        if(changeReqIdSet!=null && !changeReqIdSet.isEmpty()) {
            methodAndMeasureChange = [SELECT Id, Target_Date_of_Change__c, Change_Request__c FROM Method_and_Measure_Change__c WHERE Change_Request__c IN: changeReqIdSet ORDER BY Target_Date_of_Change__c ASC LIMIT 1];
        }
        for(V2MOM_Change_Request__c crRecord : changeReqList) {
            v2momIdSet.add(crRecord.V2MOM__c);
            System.debug('Manager Id: '+crRecord.V2MOM_Owner__r.ManagerId);
            v2momOwnerManagerIdSet.add(crRecord.V2MOM_Owner__r.ManagerId);
        }    

        List<V2MOM_c__x> v2momList  = new List<V2MOM_c__x>();
        if(!Test.isRunningTest()) {
            v2momList = [SELECT Id, ExternalId, Name__c, OwnerId__c FROM V2MOM_c__x WHERE ExternalId =: v2momIdSet];
        }
        else {
            v2momList = mockedV2momList;
        }
        
        for(V2MOM_c__x v2mom : v2momList) {
            v2momIdToNameMap.put(v2mom.ExternalId, v2mom.Name__c);
            v2momIdToOwnerIdMap.put(v2mom.ExternalId, v2mom.OwnerId__c);
        }
        System.debug('V2MOM Id to Owner Id Map: '+v2momIdToOwnerIdMap);

        List<ADM_Scrum_Team_c__x> grcPmoTeam = new List<ADM_Scrum_Team_c__x>();
        if(!Test.isRunningTest()) {
            grcPmoTeam = [SELECT Id, ExternalId, Name__c FROM ADM_Scrum_Team_c__x WHERE Name__c = 'Security GRC PMO'];
        }
        else {
            grcPmoTeam = mockedGrcPmoTeam;
        }
        System.debug('grcPmoTeam: '+grcPmoTeam);
        List<Org62User__x> ownerList = new List<Org62User__x>();
        if(!Test.isRunningTest()) {
            ownerList = [SELECT Id, ExternalId, EmployeeNumber__c, ManagerId__c FROM Org62User__x WHERE Id IN: v2momIdToOwnerIdMap.values()];
        }
        else {
            ownerList = mockedOwnerList;
        }
        for(Org62User__x org62User : ownerList) {
            ownerIdToEmpNumMap.put(org62User.ExternalId, org62User.EmployeeNumber__c);
            ownerToManagerIdMap.put(org62User.ExternalId, org62User.ManagerId__c);
        }
        System.debug('Owner Id to External Id Map: '+ownerIdToEmpNumMap);
        List<User__x> gusUserList = new List<User__x>();
        if(!Test.isRunningTest() && ownerIdToEmpNumMap!=null && !ownerIdToEmpNumMap.isEmpty()) {
            gusUserList = [SELECT Id, ExternalId, EmployeeNumber__c FROM User__x WHERE EmployeeNumber__c IN: ownerIdToEmpNumMap.values()];
        }
        if(gusUserList!=null && !gusUserList.isEmpty()) {
            for(User__x gusUser : gusUserList) {
                empNumToGusUserIdMap.put(gusUser.EmployeeNumber__c, gusUser.ExternalId);
            }    
        }
        System.debug('empNumToGusUserIdMap: '+empNumToGusUserIdMap);
        
        if(ownerToManagerIdMap!=null && !ownerToManagerIdMap.isEmpty()) {
            v2momOwnerManagerList = [SELECT Id, ExternalId, EmployeeNumber__c FROM Org62User__x WHERE ExternalId IN: ownerToManagerIdMap.values()];
            for(Org62User__x org62User : v2momOwnerManagerList) {
                managerIdToEmpNumMap.put(org62User.ExternalId, org62User.EmployeeNumber__c);
            }   
            System.debug('managerIdToEmpNumMap: '+managerIdToEmpNumMap);
            for(User__x gusUser : [SELECT Id, ExternalId, EmployeeNumber__c FROM User__x WHERE EmployeeNumber__c IN: managerIdToEmpNumMap.values()]) {
                managerEmpNumToExtIdMap.put(gusUser.EmployeeNumber__c, gusUser.ExternalId);
            }
            System.debug('Emp Num to External Id Map: '+managerEmpNumToExtIdMap);
        }
        
        
        List<SObject> workList = new List<ADM_Work_c__x>();
        for(V2MOM_Change_Request__c crRecord : changeReqList) {
            ADM_Work_c__x workRecord = new ADM_Work_c__x();
            workRecord.Subject_c__c = 'Approved V2MOM Change Request - '+v2momIdToNameMap.get(crRecord.V2MOM__c);
            if(productTagList!=null && !productTagList.isEmpty()) {
                workRecord.Product_Tag_c__c = productTagList.get(0).ExternalId;   
            }  
            if(ownerIdToEmpNumMap!=null && ownerIdToEmpNumMap.containsKey(v2momIdToOwnerIdMap.get(crRecord.V2MOM__c)) &&
               empNumToGusUserIdMap!=null && empNumToGusUserIdMap.containsKey(ownerIdToEmpNumMap.get(v2momIdToOwnerIdMap.get(crRecord.V2MOM__c)))) {
                workRecord.Assignee_c__c = empNumToGusUserIdMap.get(ownerIdToEmpNumMap.get(v2momIdToOwnerIdMap.get(crRecord.V2MOM__c)));      
            }
            if(ownerToManagerIdMap!=null && ownerToManagerIdMap.containsKey(v2momIdToOwnerIdMap.get(crRecord.V2MOM__c)) &&
               managerIdToEmpNumMap!=null && managerIdToEmpNumMap.containsKey(ownerToManagerIdMap.get(v2momIdToOwnerIdMap.get(crRecord.V2MOM__c))) &&
               managerEmpNumToExtIdMap!=null && managerEmpNumToExtIdMap.containsKey(managerIdToEmpNumMap.get(ownerToManagerIdMap.get(v2momIdToOwnerIdMap.get(crRecord.V2MOM__c))))) {
                workRecord.QA_Engineer_c__c = managerEmpNumToExtIdMap.get(managerIdToEmpNumMap.get(ownerToManagerIdMap.get(v2momIdToOwnerIdMap.get(crRecord.V2MOM__c))));      
            }
            workRecord.RecordTypeId__c = System.Label.Org62_User_Story_RecType_Id;
            if(grcPmoTeam!=null && !grcPmoTeam.isEmpty()) {
                workRecord.Scrum_Team_c__c = grcPmoTeam.get(0).ExternalId;   
            } 
            if(methodAndMeasureChange!=null) {
                Date dt = methodAndMeasureChange.Target_Date_of_Change__c;
                Datetime dueDateTime = Datetime.newInstance(dt.year(), dt.month(), dt.day(), 0, 0, 0);
                workRecord.Due_Date_c__c = Datetime.valueOf(dueDateTime.formatGmt('yyyy-MM-dd hh:mm:ss'));
            } 
            workList.add(workRecord);
            System.debug('Work Record: '+workRecord);
        }

        if(!Test.isRunningTest() && workList!=null && !workList.isEmpty()) {
            List<Database.SaveResult> sr = Database.insertImmediate(workList);
            System.debug('INSERTED'+sr);
        }
    }
   
    public static void notifyUserOnApproval(List<V2MOM_Change_Request__c> changeReqList, Map<Id,V2MOM_Change_Request__c> idToChangeReqMap) {
        List<Id> v2momOwnersList = new List<Id>();
        Set<Id> v2momIds = new Set<Id>();
        Map<Id, String> v2momToNameMap = new Map<Id, String>();

        for(V2MOM_Change_Request__c crRecord : changeReqList) {
            if(idToChangeReqMap!=null && crRecord.Status__c != idToChangeReqMap.get(crRecord.Id).Status__c) {
                Id v2momOwner = crRecord.V2MOM_Owner__c;
                String msg = crRecord.V2MOM_Name__c+' has been updated as per the Change Request. Please review the change request and make any necessary changes to your V2MOM and/or ongoing projects that are impacted by the change.';
                List<String> ownerIds = new List<String>();
                ownerIds.add(String.valueOf(v2momOwner));
                if(!Test.isRunningTest() && v2momOwner!=null && !String.isBlank(v2momOwner)) {
                    chatterUser(msg, ownerIds, String.valueOf(crRecord.Id));
                    notifyUser(msg, ownerIds, String.valueOf(crRecord.Id));
                }
                else if(Test.isRunningTest()) {
                    List<String> userIds = new List<String>();
                    userIds = mockedOwnerIds;
                    chatterUser(msg, userIds, String.valueOf(crRecord.Id));
                    notifyUser(msg, userIds, String.valueOf(crRecord.Id));
                }
            }
        }
    }

    public static void chatterUser(String message, List<String> v2momOwnerId, String recordId) {
        ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
        ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
        ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
        ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();

        messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();

        mentionSegmentInput.id = v2momOwnerId.get(0);
        messageBodyInput.messageSegments.add(mentionSegmentInput);

        textSegmentInput.text = '\n'+message;
        messageBodyInput.messageSegments.add(textSegmentInput);

        feedItemInput.body = messageBodyInput;
        feedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
        feedItemInput.subjectId = recordId;
		
        if(!Test.isRunningTest()) {
         	ConnectApi.FeedElement feedElement = ConnectApi.ChatterFeeds.postFeedElement(Network.getNetworkId(), feedItemInput);   
        }
    }

    @future (callout = true)
    public static void notifyUser(String message, List<String> v2momOwnerId, String recordId) {
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(Url.getOrgDomainUrl().toExternalForm()
            + '/services/data/v46.0/actions/standard/customNotificationAction');
        req.setMethod('POST');
        req.setHeader('Authorization', 'Bearer ' + UserInfo.getSessionId());
        req.setHeader('Content-Type', 'application/json');
        List<CustomNotificationType> approvalNotification = new List<CustomNotificationType>([Select Id,CustomNotifTypeName from CustomNotificationType WHERE CustomNotifTypeName = 'Approval of V2MOM Change Request']);
        CustomNotificationActionInput input = new CustomNotificationActionInput();
        input.customNotifTypeId = approvalNotification.get(0).Id;
        input.recipientIds = v2momOwnerId;
        input.title = 'Changes Approved for Updates';
        input.body = message;
        input.targetId = recordId;
        CustomNotificationAction action = new CustomNotificationAction();
        action.inputs = new List<CustomNotificationActionInput>{input};
        req.setBody(JSON.serialize(action));
        if(!Test.isRunningTest()) {
         	HttpResponse res = h.send(req);
            System.debug(res.getBody());
        }
    }
    
    public class CustomNotificationAction
    {
        public List<CustomNotificationActionInput> inputs { get; set; }
    }

    public class CustomNotificationActionInput
    {
        public String customNotifTypeId { get; set; }
        public List<String> recipientIds { get; set; }
        public String title { get; set; }
        public String body { get; set; }
        public String targetId { get; set; }
    }
}
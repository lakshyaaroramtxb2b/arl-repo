/**
 * @author Mahima Aggarwal
 * @email mahima.aggarwal@mtxb2b.com
 * @desc This class is used to all the auditor request items to their respective audit firms
 */
public with sharing class KARL_ShareAuditorReqItemToAuditFirmBatch implements Database.Batchable<sObject>,Schedulable {
    private String context;
    private Set<Id> firmIdSet;
    private String action;
    private Map<Id,Set<Id>> publicAuditFirmIdToContactIdMap;
    private Integer timeLength;
    private Boolean isAllTime;

    public KARL_ShareAuditorReqItemToAuditFirmBatch(){
        context = 'ScheduledBatch';
        firmIdSet = new Set<Id>();
        publicAuditFirmIdToContactIdMap = new Map<Id,Set<Id>>();
        action = 'Add';
    }

    public KARL_ShareAuditorReqItemToAuditFirmBatch(String runningContext,Set<Id> firmSet,Map<Id,Set<Id>> audFirmIdToContactIdMap,String actionToTake) {
        context = runningContext;
        firmIdSet = firmSet;
        publicAuditFirmIdToContactIdMap = audFirmIdToContactIdMap;
        action = actionToTake;
    }

    public KARL_ShareAuditorReqItemToAuditFirmBatch(Integer daysLength, Boolean isAll){
        context = 'OldAuditorARLItemsSharing';
        firmIdSet = new Set<Id>();
        publicAuditFirmIdToContactIdMap = new Map<Id,Set<Id>>();
        action = 'Add';
        timeLength = daysLength;
        isAllTime = isAll;
    }

    public KARL_ShareAuditorReqItemToAuditFirmBatch(Integer minutesLength){
        context = 'QuickAction';
        firmIdSet = new Set<Id>();
        publicAuditFirmIdToContactIdMap = new Map<Id,Set<Id>>();
        action = 'Add';
        timeLength = minutesLength;
    }

    public Database.QueryLocator start(Database.BatchableContext BC) {
        String query,filter = '';
        if(context == 'ScheduledBatch'){
            query = 'SELECT Id,KARL_Audit_Team__c, ' +
                    'KARL_Audit_Team__r.KARL_Audit_Firm__c, ' +
                    'KARL_Cycle_Request_Item__c ' +
                    'FROM KARL_Auditor_Request_Item__c '+
                    'WHERE CreatedDate = YESTERDAY '; 
        } 
        else if( context == 'Trigger'){
            if(firmIdSet != null && !firmIdSet.isEmpty()){
                query = 'SELECT Id,KARL_Audit_Team__c, ' +
                    'KARL_Audit_Team__r.KARL_Audit_Firm__c, ' +
                    'KARL_Cycle_Request_Item__c ' +
                    'FROM KARL_Auditor_Request_Item__c '+
                    'WHERE KARL_Audit_Team__r.KARL_Audit_Firm__c IN :firmIdSet '; //+ firmIds;
            }
        } else if( context == 'OldAuditorARLItemsSharing'){
            query = 'SELECT Id,KARL_Audit_Team__c, ' +
                    'KARL_Audit_Team__r.KARL_Audit_Firm__c, ' +
                    'KARL_Cycle_Request_Item__c ' +
                    'FROM KARL_Auditor_Request_Item__c ';
            if(isAllTime){
                filter = '';
            } else{
                filter = ' WHERE CreatedDate = LAST_N_DAYS:'+timeLength;
            }
            query += filter;
        } else if( context == 'QuickAction'){
            Datetime nMinutesBack = Datetime.now().addMinutes(-1 * timeLength);
            query = 'SELECT Id,KARL_Audit_Team__c, ' +
                    'KARL_Audit_Team__r.KARL_Audit_Firm__c, ' +
                    'KARL_Cycle_Request_Item__c ' +
                    'FROM KARL_Auditor_Request_Item__c ' +
                    ' WHERE CreatedDate >= :nMinutesBack';
            
        }
        System.debug('query>>'+query);
        return Database.getQueryLocator(query);
    }
     
    public void execute(Database.BatchableContext BC, List<KARL_Auditor_Request_Item__c> auditorReqItemList) {
        if(context == 'ScheduledBatch' || context == 'OldAuditorARLItemsSharing' || context == 'QuickAction'){
            Set<Id> auditFirmIdSet = new Set<Id>();
            Map<Id,Set<Id>> auditFirmIdToContactIdMap = new Map<Id,Set<Id>>();
            
            for(KARL_Auditor_Request_Item__c auditorReqItem : auditorReqItemList){
                auditFirmIdSet.add(auditorReqItem.KARL_Audit_Team__r.KARL_Audit_Firm__c);
            }

            for(KARL_Audit_Team_Contacts__c auditTeamCon : [SELECT Id,KARL_Audit_Team__c,KARL_Contact__c,
                                                            KARL_Audit_Team__r.KARL_Audit_Firm__c
                                                            FROM KARL_Audit_Team_Contacts__c 
                                                            WHERE KARL_Audit_Team__r.KARL_Audit_Firm__c IN : auditFirmIdSet]){ 
                if(!auditFirmIdToContactIdMap.containsKey(auditTeamCon.KARL_Audit_Team__r.KARL_Audit_Firm__c)){
                    auditFirmIdToContactIdMap.put(auditTeamCon.KARL_Audit_Team__r.KARL_Audit_Firm__c,new Set<Id>());
                }
                auditFirmIdToContactIdMap.get(auditTeamCon.KARL_Audit_Team__r.KARL_Audit_Firm__c).add(auditTeamCon.KARL_Contact__c);  
            }

            KARL_AuditorReqItemSharingUtility.shareAuditorReqItem(auditorReqItemList,auditFirmIdToContactIdMap);

        } else if(context == 'Trigger'){
            if(action == 'Add'){
                KARL_AuditorReqItemSharingUtility.shareAuditorReqItem(auditorReqItemList,publicAuditFirmIdToContactIdMap);
            } else if(action == 'Remove'){
                KARL_AuditorReqItemSharingUtility.unShareAuditorReqItem(auditorReqItemList,publicAuditFirmIdToContactIdMap);
            }
        }
        
    }   
     
    public void finish(Database.BatchableContext BC) {
        // execute any post-processing operations like sending email
    }

    public void execute(SchedulableContext sc) {
        KARL_ShareAuditorReqItemToAuditFirmBatch b = new KARL_ShareAuditorReqItemToAuditFirmBatch(); 
        Database.executeBatch(b);
    }
}
/**
 * @author: ralph@callaway.cloud
 * @description: Utility class for working with Ids
 */
public class Id_Util {
    
    /**
     * Check if input is a valid sfdc id
     * @param  input 15 or 18 character salesforce id
     * @return       true if valid sfdc id
     */
    public static Boolean isValid(String input) {
        if (String.isBlank(input)) return false;
        try {
            Id.valueOf(input);
        } catch(System.StringException e) {
            return false;
        }
        return true;
    }

    /**
     * Check if input is a valid sfdc id for specific object type
     * @param  input    15 or 18 character salesforce id
     * @param  sObjType SObjectType to compare to
     * @return          true if valid sfdc id for sobject type
     */
    public static Boolean isValid(String input, Schema.SObjectType sObjType) {
        if (!isValid(input)) return false;
        return sObjType == Id.valueOf(input).getSobjectType();
    }

}
/*

    Developer Name: Jorge L Caceres - jcaceres@salesforce.com | September 2015
    
    Description: Controller for TM_Objective_SCS page.
                 Given an objective ID the page will display the standard layout for the corresponding SCS

*/

public with sharing class TM_Objective_SCS {

    public Id scsId {get; private set;}

    public TM_Objective_SCS(ApexPages.StandardController controller) {
    
        Id objectiveId = controller.getId();
        
        scsId = TM_GenerateConcretes.getSecurityControlStatusId(objectiveId);
 
    }

}
/*
* @Test Class for SC_Email_External_Communication class
* 
*/ 
@isTest
public class SC_Email_External_CommunicationTest {
    
    public static String test_email_id = 'abcd.xy12@yopmail.com'; 
    
    
    @TestSetup
    public static void setupTestData(){
        User usr = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        
        Contact cnt = new Contact(FirstName = 'Test', LastName = 'Name', Email = test_email_id);
        insert cnt;
        
        System.runAs(usr) {
            SC_Email_External_CommunicationTUtils.createEmailTemplates(true);
            SC_Email_External_CommunicationTUtils.createCampaignMemberData();
        }
    }
    
    
    public testmethod static void testFetchEmailTemplatesAndOWD() {
        Test.startTest();
        User usr = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        
        System.runAs(usr) {
            List<EmailTemplate> templates =  SC_Email_External_Communication.fetchEmailTemplates();
            List<OrgWideEmailAddress> owds = SC_Email_External_Communication.getOrgWideEmailAddresses();
        }
        
        Test.stopTest();
    }
    
   
    public testmethod static void testGetCampaignMemberData() {
        Test.startTest();
        User usr = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        
        System.runAs(usr) {
            System.assertEquals(SC_Email_External_Communication.getAllCampaignMemberData().size(), 1);
            SC_Email_External_Communication.getCampaignMemberDataTemplate();
            SC_Email_External_Communication.updateCampaignMemberStatusAsPending();
        } 
        
        Test.stopTest();
    }  
    
    
    public testmethod static void testFetchData() {
        Test.startTest();
        
        User usr = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        
         System.runAs(usr) {
             SC_Email_External_Communication.EmailConfirmation emailC = SC_Email_External_Communication.fetchData();
             
             System.assertEquals(emailC.listOfCampaignMembers.size() , 1);   
             System.assertNotEquals(emailC.listOfEmailTemplates.size() , 0);   
             System.assertNotEquals(emailC.listOfOrgWideDefault.size() , 0);   
        }
        
        Test.stopTest();
    }
    
    
    public testmethod static void testDeleteData() {
        Test.startTest();
        
        User usr = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        
         System.runAs(usr) {
             SC_Email_External_Communication.deleteData();
             //System.assertEquals(SC_Email_External_Communication.getCampaignMemberData().size(), 0);
        }
        
        Test.stopTest(); 
    }
    
    
    public testmethod static void testSendEmails() {
        Test.startTest();
        
        User usr = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        List<EmailTemplate> templates =  SC_Email_External_Communication.fetchEmailTemplates();
        List<OrgWideEmailAddress> owds = [SELECT Id, IsAllowAllProfiles, DisplayName, Address FROM OrgWideEmailAddress WHERE IsAllowAllProfiles= TRUE];
        String ccAdd = 'abcd.xyz189@gmail.com,xyz.abcd12@gmail.com';
         
        List<SC_Email_External_Communication.StatusRecords> staticRecList = new List<SC_Email_External_Communication.StatusRecords>();
        List<SC_Email_External_Communication.AccountEmails> accountEmailList = new List<SC_Email_External_Communication.AccountEmails>();
        
        SC_Email_External_Communication.StatusRecords staticRec = new SC_Email_External_Communication.StatusRecords();
        staticRec.status = 'abc';
        staticRec.numberOfRec = 2;
        staticRec.selectedEmailTemplate = SC_Email_External_Communication.fetchEmailTemplates()[0].Name;
        staticRec.selectedFromAddress = owds[0].Address;
        staticRec.selectedCCAddresses = ccAdd;
        staticRecList.add(staticRec);
        
        SC_Email_External_Communication.AccountEmails accEmails = new SC_Email_External_Communication.AccountEmails();
        accEmails.id = [SELECT Id, Name, Email__c, FirstName__c, LastName__c FROM SC_CISO_CampaignMember__c LIMIT 1].Id;
        accEmails.aeEmail = test_email_id;
        accountEmailList.add(accEmails);
        
            System.runAs(usr) { 
                SC_Email_External_Communication.EmailConfirmation emailC = SC_Email_External_Communication.sendEmails(JSON.serialize(staticRecList), accountEmailList);
                 //System.assertEquals('Pending', listOfSCCamp[0].SuccessOrError__c);

            }
        
        Test.stopTest(); 
    }   
    
    
}
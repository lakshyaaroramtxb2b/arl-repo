/**
 * @author Ben Harvie
 * @email ben.harvie@salesforce.com
 * @description Test class for AuditScopeReportTrigger
*/
@isTest
public with sharing class KARL_AuditScopeReportTriggerHelperTest {
    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Test data setup.
    */
    @testSetup
    private static void testSetup(){
        KARL_Audit_Scope_Master_Data__c auditScope = ARL_TestDataFactory.createAuditScope();
        insert auditScope;

        KARL_Audit_Firm__c auditFirm = ARL_TestDataFactory.createAuditFirm('Test Firm');
        insert auditFirm;

        KARL_Audit_Scope_Reports__c auditScopeReport = ARL_TestDataFactory.createAuditScopeReport(auditScope.Id, auditFirm.Id);
        insert auditScopeReport;
    }
    
    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description In this test, the test is updating the description and asserting on whether the descriptions are equal
    */
	@istest
    public static void unitTest(){ 
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias='standt', Email='standarduser@testorg.com', EmailEncodingKey='UTF-8', LastName='ARL Test User',
                         LanguageLocaleKey='en_US', LocaleSidKey='en_US', ProfileId = p.Id, TimeZoneSidKey='America/Los_Angeles', UserName='stan123@test.com');
        INSERT u;
        PermissionSet pSet = [SELECT Id, Label FROM PermissionSet WHERE Label LIKE '%SCEA Ops Admin%'];
        PermissionSetAssignment psa = new PermissionSetAssignment(PermissionSetId = pSet.Id, AssigneeId = u.Id);
        INSERT psa;
        
        System.runAs(u) {
            KARL_Audit_Scope_Reports__c asrItem = [SELECT Id,KARL_Audit_Scope__r.Name,toLabel(KARL_Report_Type__c), KARL_Triggered_Report_Name__c 
                                                    FROM KARL_Audit_Scope_Reports__c LIMIT 1];
  		
            // Indexed value should be a concatenation of KARL_Audit_Scope__r.Name and Report Type
            String expectedName = asrItem.KARL_Audit_Scope__r.Name + ' - ' + asrItem.KARL_Report_Type__c;
            
            // Expecting a match on the concatenated name and the saved trigger name
            System.assertEquals( asrItem.KARL_Triggered_Report_Name__c, expectedName, 'Names are not matching properly' );   
        }
    }
}
public class ESA_ReminderScheduler implements Schedulable, Database.Batchable<sObject> {
    
    //Lists used to update cases, create comments and send emails (with merge fields)
    static List <Case> cases = new List <Case>();
    static List <Messaging.SingleEmailMessage> allmsg = new List <Messaging.SingleEmailMessage>(); 
    static List <CaseComment> comments = new List <CaseComment>();
    //Map for getting the email templates in one single query
    static Map <String, EmailTemplate> templateMap = new Map <String, EmailTemplate> ();
    
    
    public Database.QueryLocator start(Database.BatchableContext BC) {
        //Get the record from metadata
        Case_Reminder_Config__mdt[] caseReminderMDT = [SELECT Reminder_1_Email_Template__c, Reminder_2_Email_Template__c,Reminder_3_Email_Template__c,
                                                    Reminder_4_Email_Template__c, Closing_Reminder__c, Reminder_1_Days__c, Reminder_2_Days__c,
                                                    Reminder_3_Days__c,Reminder_4_Days__c,Closing_Days__c,Team_Record_Types__c, Status__c 
                                                     FROM Case_Reminder_Config__mdt];
        
        
        //Lists for getting the status and ids for getting the cases
        List <String> caseStatus = new List <String> ();
        List <String> caseRecordId = new List <String> ();
        
        for (Case_Reminder_Config__mdt cr: caseReminderMDT){
            
            caseStatus.add(cr.Status__c);
            caseRecordId.add(cr.Team_Record_Types__c);
        }
        
        //Get only the cases that may need reminders
        return Database.getQueryLocator([
            
            
            SELECT Id, Reminder1__c, Reminder2__c, Reminder3__c, Reminder4__c,
                                      Closing_Date__c, Closing_template__c, Reminder1_Email_Template__c, 
                                      Reminder2_Email_Template__c, Reminder3_Email_Template__c,
                                      Reminder4_Email_Template__c, Last_Reminder__c, Prepare_close__c,
                                      Send_Close__c, Status, RecordTypeId, ContactId, ContactEmail, CaseNumber  
                                FROM Case
                       WHERE Status IN: caseStatus AND RecordTypeId IN: caseRecordId
            
        ]);
        
    }
    
    public void execute(SchedulableContext ctx) {
        Database.executebatch(new ESA_ReminderScheduler(),100);
    }
    
    public void execute (Database.BatchableContext BC, List<Case> lstCases) {
        //We get the 5 possible templates from all the cases
        Set <String> templateNames = new Set <String> ();
        
        for (Case cs: lstCases){
            
            templateNames.add(cs.Reminder1_Email_Template__c);
            templateNames.add(cs.Reminder2_Email_Template__c);
            templateNames.add(cs.Reminder3_Email_Template__c);
            templateNames.add(cs.Reminder4_Email_Template__c);
            templateNames.add(cs.Closing_template__c);
        }
        
        
        //SOQL query for getting only the templates that we are going to use
        List <EmailTemplate> lstTemplate = [SELECT Id, Body, Name FROM EmailTemplate WHERE Name IN: templateNames];
        
        
        //Get the templates in the map
        Map <String, EmailTemplate> mapTemplate = new Map <String, EmailTemplate> ();
        
        for (EmailTemplate et: lstTemplate){
            
            mapTemplate.put(et.Name, et);
            
        }
        
       
        //set today value
        date todayDate = system.today();
        datetime todayDateTime = datetime.now();
        
        //Loop through the cases and do the logic
        if (lstCases.size() > 0){
            for (Case cs: lstCases){
                 system.debug('hello' + cs);          
                //Cases for reminder 1
                if (cs.Reminder1__c != null && cs.Reminder2__c == null && cs.Send_Close__c == false
               && cs.Reminder1__c != cs.Last_Reminder__c && cs.Reminder1__c <= todayDate){
                    system.debug('hello 2222222' + cs); 
                    //Get the template, reminder 1 and update last reminder. If it is the last reminder, prepare closing
                    EmailTemplate template = mapTemplate.get(cs.Reminder1_Email_Template__c);
                    Date reminder = cs.Reminder1__c;
                    cs.Last_Reminder__c = todayDate;
                    if (cs.Prepare_close__c == true){
                       cs.Send_Close__c = true;
                    }
                   //Add the case to update and send email
                    cases.add(cs);
                    sendEmail(cs, template, reminder);           
        }
                //Cases for reminder 2
                else if (cs.Reminder1__c != null && cs.Reminder2__c != null && cs.Reminder3__c == null
                     && cs.Send_Close__c == false 
                    && cs.Reminder2__c != cs.Last_Reminder__c && cs.Reminder2__c <= todayDate){
                        
                        EmailTemplate template = mapTemplate.get(cs.Reminder2_Email_Template__c);
                        Date reminder = cs.Reminder2__c;
                        cs.Last_Reminder__c = todayDate;
                        if (cs.Prepare_close__c == true){
                            cs.Send_Close__c = true;
                        }
                        cases.add(cs);
                        sendEmail(cs, template, reminder);  
                   
                    }
                //Cases for reminder 3
                else if (cs.Reminder1__c != null && cs.Reminder2__c != null && cs.Reminder3__c != null 
                    && cs.Reminder4__c == null && cs.Send_Close__c == false
                    && cs.Reminder3__c != cs.Last_Reminder__c && cs.Reminder3__c <= todayDate){
                        
                        EmailTemplate template = mapTemplate.get(cs.Reminder3_Email_Template__c);
                        Date reminder = cs.Reminder3__c;
                        cs.Last_Reminder__c = todayDate;
                        if (cs.Prepare_close__c == true){
                            cs.Send_Close__c = true;
                        }
                        cases.add(cs);
                        sendEmail(cs, template, reminder); 
                        
                        
                        
                    }
                //Cases for reminder 4
                else if (cs.Reminder1__c != null && cs.Reminder2__c != null && cs.Reminder3__c != null 
                     && cs.Reminder4__c != null && cs.Send_Close__c == false
                    && cs.Reminder4__c != cs.Last_Reminder__c && cs.Reminder4__c <= todayDate){
                        
                        
                        EmailTemplate template = mapTemplate.get(cs.Reminder4_Email_Template__c);
                        Date reminder = cs.Reminder4__c;
                        cs.Last_Reminder__c = todayDate;
                        if (cs.Prepare_close__c == true){
                            cs.Send_Close__c = true;
                        }
                        cases.add(cs);
                        sendEmail(cs, template, reminder); 
                        
                        
                    }
                
            //Cases for last reminder  
            else if (cs.Send_Close__c == true && cs.Closing_Date__c != null
                    && cs.Closing_Date__c != cs.Last_Reminder__c && cs.Closing_Date__c <= todayDate){
                        
                        EmailTemplate template = mapTemplate.get(cs.Closing_template__c);
                        Date reminder = cs.Closing_Date__c;
                        cs.Last_Reminder__c = todayDate;
                        //Close the case
                        cs.Status = 'Withdrawn';
                        cases.add(cs);
                        sendEmail(cs, template, reminder); 
           
                    }
                
                
                
                       }
        
        
        }
        
        try {
            //if(!Test.isRunningTest()) {
                //Send the emails
                Messaging.sendEmail(allmsg);
                //For each email, create the case
                for (Messaging.SingleEmailMessage ml: allmsg){
            
                    
                    createComment(ml.getWhatId(), ml.getPlainTextBody());
                    
            
            
        }
              //Insert comments created
              insert comments;
              system.debug('lat stop');
             
              //Update cases 
              update cases;             
            //}
          
            return;
    } catch (Exception e) {
        System.debug(e.getMessage());
}
        
    }
    
    
    public static void sendEmail (Case caseForEmail, EmailTemplate template, date reminder){
                   
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setUseSignature(false);
        
        OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'entsecapps@salesforce.com'];
        if ( owea.size() > 0 ) {
            mail.setOrgWideEmailAddressId(owea.get(0).Id); 
        }
        
        
        string [] toaddress;
        
        
        toaddress = new String []{caseForEmail.ContactEmail};
            
        mail.setToAddresses(toaddress);
        mail.setTemplateId(template.id);
        mail.setTargetObjectId(caseForEmail.contactId); 
        mail.setWhatId(caseForEmail.id);
        mail.setSaveAsActivity(true);
        allmsg.add(mail);
    }
    
    
    public static void createComment (id caseId, string tBody)    {
        
        CaseComment cc = new CaseComment();
        cc.CommentBody = tBody;
        cc.ParentId = caseId;
        cc.IsPublished = true;
        comments.add(cc);
        
        
    }
    
    public void finish(Database.BatchableContext BC) {
         if(!Test.isRunningTest()) {
           //ESA_ReminderScheduler batch = new ESA_ReminderScheduler();
           //System.scheduleBatch(batch, 'ESA_ReminderScheduler', 10, 100);
        }


}
    
}
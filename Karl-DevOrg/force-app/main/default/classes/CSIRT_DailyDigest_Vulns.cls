public class CSIRT_DailyDigest_Vulns {
	List<Case> vulns;    
    public List<Case> getVulns(){
        if(vulns == null)
        	vulns = [SELECT IRCloud__Vulnerability_Priority__c, Days_Open__c, Subject, IRCloud__Email_Case_Reference_ID__c, ResponseForce__Incident_Name_New__c, IRCloud__Case_Summary__c FROM Case WHERE IsClosed = False AND Include_in_Daily_Digest__c = True AND RecordTypeId = '0123A000001dxqj' ORDER BY IRCloud__Vulnerability_Priority__c, Days_Open__c];
        return vulns;
    }

}
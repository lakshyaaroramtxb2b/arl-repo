@isTest
public class PRT_AssignmentWeekTriggerHelperTest {
    @testSetup
    public static void testSetup(){
        List<Portfolio__c> portfolioDataList = PRT_TestDataFactory.createPortfolio(1,false);
        List<Contact> contactListRecord = PRT_TestDataFactory.createContact(1,true);
        List<project__c> projectRecordList= PRT_TestDataFactory.createProjects(1,false);
        projectRecordList[0].Portfolio__c = portfolioDataList[0].id;      
        insert projectRecordList;
        List<Milestone_PRT__c> milestoneListRecord = PRT_TestDataFactory.createMilestone(1, projectRecordList[0].id, true);
        List<Assignment__c> assignmentListRecord = PRT_TestDataFactory.createAssignment(1, projectRecordList[0].id, milestoneListRecord[0].id, true);
        
    }
    @isTest
    public static void testRollupAssignmentWeekCapacity(){
        List<Contact> contactListRecord= [SELECT ID FROM Contact LIMIT 1];
        List<Assignment__c> assignmentListRecord = [SELECT ID FROM Assignment__c LIMIT 1];
        List<Assignment_Week__c> assignmentWeekListRecord = PRT_TestDataFactory.createAssignmentWeek(1,assignmentListRecord[0].id,contactListRecord[0].id,false);
        assignmentWeekListRecord[0].Number_of_Hours__c = 5;
        insert assignmentWeekListRecord;
        test.startTest();
        assignmentWeekListRecord[0].Number_of_Hours__c = 7;
        update assignmentWeekListRecord;
        test.stopTest();
        
    }

}
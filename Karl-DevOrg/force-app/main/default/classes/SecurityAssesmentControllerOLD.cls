public with sharing class SecurityAssesmentControllerOLD {
    @TestVisible private enum Stages { AUTHORIZE, ACCEPT_SAA, PENTEST_INFO, REQUEST_COMPLETE, REQUEST_ERROR, EDIT_EXISTING, CHOOSE_ENV}
    @TestVisible public Stages stage {get;private set;}
    public boolean show_authorize {get;private set;}
    public boolean show_saa {get;private set;}
    public boolean show_form {get;private set;}
    public boolean show_edit {get;private set;}
    public boolean show_env {get;private set;}

    public string target_environment {get;set;}
    public boolean requires_additional_target_info {get;set;}
    private Customer_Assessment_Areas__mdt[] assessmentAreas;
    
    public static Boolean ISSANDBOX {get
        {
            if (ISSANDBOX == null) {
                ISSANDBOX = [SELECT isSandbox FROM Organization limit 1].isSandbox;
            }
            return ISSANDBOX;  
        } 
        private set;
    }

    public static final string SAA_DOC_NAME = 'Salesforce_Security_Assesment_Agreement';
    public static final string OAUTH_AUTHORIZE_ENDPOINT = 'https://login.salesforce.com/services/oauth2/authorize';
    public static final string OAUTH_TOKEN_ENDPOINT = 'https://login.salesforce.com/services/oauth2/token';
    public static final string OAUTH_CALLBACK_URL = ISSANDBOX? URL.getSalesforceBaseUrl().toExternalForm() + '/securityassessments' :
                                                    'https://security.secure.force.com/securityassesments';
    public static final string OAUTH_CLIENT_ID = '3MVG99OxTyEMCQ3i.Cy5vQYJe7mVOVkqCcjnjh4UxgTbP1hFlYpkEPGq67yMpb5o4ft1ienxiQ1RMjAdd32ZV';
    private static final string OAUTH_CLIENT_SECRET = '916510964017196769';

    // this google client id is registered under project "Security Assessments" for "esa.coordinator@salesforce.com" service google account
    public static final string GOOGLE_CLIENT_ID = '619385653197-4vjr49mgbmnhls0msh25u86q7tl5to4k.apps.googleusercontent.com';
            
    private static final String MARKETING_CLOUD = 'Marketing_Cloud_Services';

    public string gclientId {get {return GOOGLE_CLIENT_ID;}}

    public boolean SAA_Accepted {get;set;}
    public string company_name {get;set;}
    public string member_id {get;set;}
    public string company_org_id {get;set;}
    public string pm_name {get;set;}
    public string pm_workphone {get;set;}
    public string pm_workphone_helpText {get; set;}
    public string pm_email {get;set;}
    public string pm_cellphone {get;set;}
    public string tester_name {get;set;}
    public string tester_email {get;set;}
    public string tester_workphone {get;set;}
    public string tester_cellphone {get;set;}
    public string overview_tests {get;set;}
    public dateTime start_time {get;set;}
    public dateTime end_time {get;set;}
    public string testing_orgs {get;set;}
    public string testing_users {get;set;}
    public string server_load {get;set;}
    public string tools_used {get;set;}
    public string source_ip {get;set;}
    public string test_environment {get; set;}

    public string additional_Email1 {get;set;}
    public string additional_Email2 {get;set;}
    public string additional_Email3 {get;set;}
    public string additional_Email4 {get;set;}
    public string additional_Email5 {get;set;}
    
    public string publicIpStartRange1 {get;set;}
    public string publicIpStartRange2 {get;set;}
    public string publicIpStartRange3 {get;set;}
    public string publicIpStartRange4 {get;set;}
    public string publicIpStartRange5 {get;set;}
    
    public string publicIpEndRange1 {get;set;}
    public string publicIpEndRange2 {get;set;}
    public string publicIpEndRange3 {get;set;}
    public string publicIpEndRange4 {get;set;}
    public string publicIpEndRange5 {get;set;}
    
    public boolean ipValidationFlag {get;set;}

    public string additional_target_info {get;set;}


    // FB, linkedin login
    public string social_token {get;set;}

    // VF hints. For revisit/edit, only show the auth provider used
    public Boolean showFB {get;set;}
    public Boolean showLI {get;set;}
    public Boolean showGoogle {get;set;}
    public Boolean showSFDC {get;set;}

    public Boolean isMarketingCloud {get; set;}

    public Map<String,Boolean> mapSubEnvironments {get; set;}

    // opaque oauth state. Used to hold record ID through the SFDC oauth flow
    // when the user is coming back to view or edit a record
    private ID oauth_state;

    // Used to hold customer assessment for re-visit/edit
    public Customer_Assessment__c record {get;private set;}

    // Users may only edit record when it's not approved
    public boolean record_editable {get; private set;}

    // Store the oauth userInfo the user authenticated with
    @TestVisible
    private Stringable userInfo;

    public String now {get;set;}
    public final string saaDocId {get; private set;}

    public List<SelectOption> server_load_entries {get; private set;}

    // This is stupid but we keep hitting problems where it's complaining
    // about tests doing having uncommitted work and doing DML before callouts.
    // You're supposed to be able to do Test.start()/end() but that doesn't seem
    // to actually work
    public HttpResponse testResponse;
    
    private class AssessmentException extends Exception {}

    public SecurityAssesmentControllerOLD() {
        
        // show all social auth providers by default
        showFB = showGoogle = showLI = showSFDC = True;
        record_editable = False;

        SAA_Accepted = FALSE;

        now = getFirstAvailableDate().formatGmt('yyyy-MM-dd\'T\'HH:mm:00\'Z\'');
        List<Document> docs = [SELECT Id FROM Document WHERE DeveloperName = :SAA_DOC_NAME LIMIT 1];
        if (docs.size() == 1){
           saaDocId = docs[0].id;
        }else{
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Unable to load page. Please contact support. Error Code : 1',''));
            saaDocId = NULL;
        }

        server_load_entries = new List<SelectOption>();
        Schema.DescribeFieldResult dfr = Customer_Assessment__c.Server_Load__c.getDescribe();

        for(Schema.PicklistEntry entry : dfr.getPicklistValues()){
            string value = entry.getValue();
            server_load_entries.add(new SelectOption(value,value));
        }

        try {
            record = loadRecord(ApexPages.currentPage().getParameters().get('assessmentId'));
        } catch (Exception e) {
        }


        if (record != null) {
            // checkAndProcessSFDCOauthCode() will need to use this state to keep track of the original re-visit/edit request
            oauth_state = record.id;
            SAA_Accepted = True;
        } else {
            // They're not loading a record, give them the environment selector by default
            changeStage(stages.CHOOSE_ENV);
        }

        // This may also load "record" if we're being invoked as an oauth callback
        Boolean wasSFDCOauthCallback = checkAndProcessSFDCOauthCode();

        if (record != null) {
          hideUnusedAuthProviders(false);
          changeStage(Stages.AUTHORIZE);
        } else {
            getEnvironments();
        }

        if(string.isNotBlank(target_environment)) {
            String subEnvironments;
            try {
                subEnvironments = [SELECT Sub_Environments__c FROM Customer_Assessment_Areas__mdt
                                   WHERE DeveloperName =:target_environment].Sub_Environments__c;
            }catch(exception ex) {
                subEnvironments = null;
            }

            if(subEnvironments !=null) {
                Set<String> subEnvsSet = new Set<String>();
                subEnvsSet.addAll(subEnvironments.split(','));

                mapSubEnvironments = new Map<String,Boolean>();
                for(String st : subEnvsSet) {
                    mapSubEnvironments.put(st,false);
                }
            }
        }

        if(target_environment == MARKETING_CLOUD) {
            isMarketingCloud = true;
        } else {
            isMarketingCloud = false;
        }

        //pm_workphone_helpText = Schema.SObjectType.Customer_Assessment__c.fields.PM_Work_Phone__c.inlineHelpText;

    }

    @TestVisible
    private void changeStage(Stages newStage) {
        stage = newStage;
        show_authorize = show_form = show_saa = show_edit = show_env = False;

        if (stage == Stages.ACCEPT_SAA) {
            show_saa = True;
        } else if (stage == Stages.PENTEST_INFO) {
            show_form = True;
        } else if (stage == Stages.EDIT_EXISTING) {
            show_form = True;
            show_edit = True;
        } else if (stage == Stages.CHOOSE_ENV) {
            show_env = True;
        } else if (stage == Stages.REQUEST_COMPLETE) {
            // pass
        } else if (stage == Stages.AUTHORIZE) {
            show_authorize=True;
        }
    }

    private boolean checkAndProcessSFDCOauthCode(){
        /*
           This page is also used as the callback for the SFDC oauth login. Check for the code, query for
           user details, and populate user information if the code is available
        */

        string code = ApexPages.currentPage().getParameters().get('code');
        if (code == NULL || code == '') return False;

        if (ApexPages.currentPage().getParameters().get('state') != null) {
            // state parm is also use to pass the target environment during call back for new request
            if (!ApexPages.currentPage().getParameters().get('state').startsWith('TARGETENV')) {
                // This is a callback to a revisit/edit request. We previously passed the record ID as the state
                record = loadRecord(ApexPages.currentPage().getParameters().get('state'));
            } else {
                target_environment = ApexPages.currentPage().getParameters().get('state').replace('TARGETENV', '');
            }
        }

        HttpRequest req = new HttpRequest();
        req.setMethod('POST');
        req.setHeader('Accept', 'application/json');
        req.setEndpoint(OAUTH_TOKEN_ENDPOINT);

        string body = 'grant_type=authorization_code';
        body += '&code=' + code;
        body += '&client_id=' + OAUTH_CLIENT_ID;
        body += '&client_secret=' + OAUTH_CLIENT_SECRET;
        body += '&redirect_uri=' + OAUTH_CALLBACK_URL;
        req.setBody(body);

        Http http = new Http();
        HttpResponse res;
        try{
            res = http.send(req);
        }catch(exception e){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Unable to process request. Please contact support. Error Code : 2',''));
        }

        if (res.getStatusCode() == 200){

            OauthTokenResponse oauthToken = (OauthTokenResponse) JSON.deserialize(res.getBody(), OauthTokenResponse.class);

            req = new HttpRequest();
            req.setMethod('GET');
            req.setHeader('Accept', 'application/json');
            req.setHeader('Authorization', 'Bearer ' + oauthToken.access_token);
            req.setEndpoint(oauthToken.id);

            http = new Http();

            try{
                res = http.send(req);

            }catch(exception e){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Unable to process request. Please contact support. Error Code : 3',''));
            }

            if (res.getStatusCode() == 200){
                IdentityResponse userData = (IdentityResponse) JSON.deserialize(res.getBody(), IdentityResponse.class);
                pm_name = userData.display_name;
                pm_email = userData.email;
                pm_cellphone = userData.mobile_phone;
                company_org_id = userData.organization_id;

                userInfo = userData;
                postLogin();

                return True;

            }else{
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Unable to process request. Please contact support. Error Code : 4',''));
            }
        }else{
              ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Unable to process request. Please contact support. Error Code : 5',''));
              System.debug('received bad status code from OAUTH response: ' + res.toString());
        }
        return False;
    }

    public void toggleSaa(){
        changeStage( SAA_Accepted ? Stages.PENTEST_INFO : Stages.ACCEPT_SAA);
    }

    public string getOauthAuthorizeURL(){
        /* SFDC Oauth */
        String url = OAUTH_AUTHORIZE_ENDPOINT + '?response_type=code&client_id=' + OAUTH_CLIENT_ID + '&redirect_uri=' + OAUTH_CALLBACK_URL;
            if (oauth_state != null) {
                // The user came for re-visit/edit. Pass the record ID as the state so it
                // gets sent back on the callback
                url += '&state='+ oauth_state;
            } else {
                // new request so need to pass the selected environment
                url += '&state=TARGETENV' + target_environment;
            }

        return url;
    }

    public void handleFBLogin() {
        // actionFunction should have just populated social_token. Call the FB API to get user information
        String url = 'https://graph.facebook.com/me?fields=name,email&access_token='+social_token;
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(url);
        req.setMethod('GET');
        req.setTimeout(60*1000);
        HttpResponse res = h.send(req);
        String response = res.getBody();
        if (res.getStatusCode() != 200) {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Unable to process request. Please contact support. Error Code : 6',''));
            return;
        }

        FBUserInfo u = new FBUserInfo().parse(response);
        pm_name = u.name;
        pm_email = u.email;

        userInfo = u;
        postLogin();
    }

    public void handleLinkedInLogin() {
        // actionFunction should have just populated social_token. Call the LinkedIn API to get user information
        String url = 'https://api.linkedin.com/v1/people/~:(id,firstName,lastName,emailAddress,positions,phone-numbers)?format=json&oauth_token='+social_token;
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(url);
        req.setMethod('GET');
        req.setTimeout(60*1000);
        HttpResponse res = h.send(req);
        String response = res.getBody();
        if (res.getStatusCode() != 200) {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Unable to process request. Please contact support. Error Code : 7',''));
            return;
        }

        LinkedInUserInfo u = new LinkedInUserInfo().parse(response);
        pm_name = u.name;
        pm_email = u.email;
        company_name = u.companyName;

        userInfo = u;
        postLogin();
    }

    public void handleGoogleLogin() {

        // actionFunction should have just populated social_token. Call the LinkedIn API to get user information
        String url = 'https://www.googleapis.com/oauth2/v3/tokeninfo?id_token='+social_token;
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(url);
        req.setMethod('GET');
        req.setTimeout(60*1000);
        HttpResponse res;
        if ((Test.isRunningTest()) && (testResponse != null)) {
            res = testResponse;
        } else {
            res = h.send(req);
        }
        String response = res.getBody();
        if (res.getStatusCode() != 200) {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Unable to process request. Please contact support. Error Code : 9',''));
            return;
        }

        GoogleUserInfo u = new GoogleUserInfo().parse(response);
        if (u == null) {
            // They tried to pass of a JWT with a different target audience
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Unable to process request. Please contact support. Error Code : 10',''));
            return;
        }
        pm_name = u.name;
        pm_email = u.email;

        userInfo = u;
        postLogin();
    }

    public List<SelectOption> getTestEnvironments() {
        List<SelectOption> te = new List<SelectOption>();
        te.add(new SelectOption('Requesting a test Account','Requesting a test Account'));
        te.add(new SelectOption('Using my own account','Using my own account'));
        return te;
    }

    public pageReference save(){

        if (record == null) {
            record = new Customer_Assessment__c();
        }
        
        record.SAA_Accepted__c = SAA_Accepted;
        record.Name =  company_name;
        record.Company_Org_Id__c =  company_org_id;
        record.MemberID__c = member_id;
        record.PM_Name__c =  pm_name ;
        record.PM_Work_Phone__c =  pm_workphone ;
        record.PM_Email__c =  pm_email;
        record.PM_Cell_Phone__c =  pm_cellphone;
        record.Tester_Name__c =  tester_name;
        record.Tester_Email__c =  tester_email;
        record.Tester_Work_Phone__c =  tester_workphone;
        record.Tester_Cell_Phone__c =  tester_cellphone;
        record.Tests_Performed__c = overview_tests;
        record.Start_Time__c = start_time;
        record.End_Time__c =  end_time;
        record.Org_IDs__c =  testing_orgs;
        record.Usernames__c =  testing_users;
        record.Server_Load__c = server_load;
        record.Tools_Used__c =  tools_used;
        record.Source_IPs__c = source_ip;
        record.Target_Environment__c = target_environment;
        record.Auth_Info__c = userInfo.toString();
        record.Additional_Target_Information__c = additional_target_info;
        record.Target_Environment__c = target_environment;
        record.Test_Environment__c = test_environment;
        record.Additional_Email1__c = additional_Email1;
        record.Additional_Email2__c = additional_Email2;
        record.Additional_Email3__c = additional_Email3;
        record.Additional_Email4__c = additional_Email4;
        record.Additional_Email5__c = additional_Email5;
        record.Public_IP_Statring_Range_1__c = publicIpStartRange1;
        record.Public_IP_Statring_Range_2__c = publicIpStartRange2;
        record.Public_IP_Statring_Range_3__c = publicIpStartRange3;
        record.Public_IP_Statring_Range_4__c = publicIpStartRange4;
        record.Public_IP_Statring_Range_5__c = publicIpStartRange5;
        record.Public_IP_End_Range_1__c = publicIpEndRange1;
        record.Public_IP_End_Range_2__c = publicIpEndRange2;
        record.Public_IP_End_Range_3__c = publicIpEndRange3;
        record.Public_IP_End_Range_4__c = publicIpEndRange4;
        record.Public_IP_End_Range_5__c = publicIpEndRange5;
        
        String subEnvs = null;
        if(mapSubEnvironments != null) {
            for (String keyVal : mapSubEnvironments.keySet()){
               if(mapSubEnvironments.get(keyVal) == true) {
                  if(String.isNotBlank(subEnvs)) {
                     subEnvs +=', ';
                     subEnvs += keyVal;
                  } else {
                     subEnvs = keyVal;
                  }

               }
            }
        }
        /*
        if(mapSubEnvironments != null) {
            for (String keyVal : mapSubEnvironments.keySet()){
               if(mapSubEnvironments.get(keyVal) == true) {
                  if(String.isNotBlank(subEnvs)) {
                     subEnvs +=',';
                     subEnvs += keyVal + ' : ' + mapSubEnvironments.get(keyVal);
                  } else {
                     subEnvs = keyVal + ' : ' + mapSubEnvironments.get(keyVal);
                  }

               }
            }
        } */


        record.Sub_Environments__c = subEnvs;

        try {
            validateRecord(record);
            upsert record;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Request processed. You will receive a confirmation email once we have reviewed your submission.',''));
            changeStage(Stages.REQUEST_COMPLETE);

        } catch (DmlException e) {           
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, trimExceptionMessage(e.getMessage()),''));
            changeStage(Stages.EDIT_EXISTING);
            record = null;

        } catch (AssessmentException e) {           
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL, e.getMessage(),''));
            changeStage(Stages.EDIT_EXISTING);
            record = null;

        } catch (exception e) {           
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Unable to process request. Please contact support. Error Code : 6'+e.getMessage(),''));
        }
        
        return null;

    }
    
    // Validate IP values
    private void validateIpValues() {
        validateIpPattern(publicIpStartRange1,publicIpEndRange1,1);
        validateIpPattern(publicIpStartRange2,publicIpEndRange2,2);
        validateIpPattern(publicIpStartRange3,publicIpEndRange3,3);
        validateIpPattern(publicIpStartRange4,publicIpEndRange4,4);
        validateIpPattern(publicIpStartRange5,publicIpEndRange5,5);
    }
    
    private void validateIpPattern(String startValue, String endValue, Integer rowVal) {
       String regex = '^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$';  
       Pattern p = Pattern.compile(regex);
       Matcher MyMatcher;
       
       if((String.isNotBlank(startValue) && String.isBlank(endValue)) || (String.isBlank(startValue) && String.isNotBlank(endValue))) {
         ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Please Enter Both Values Start & End Ranges #'+rowVal));
       }
       
       if(String.isNotBlank(startValue)) {
         MyMatcher = p.matcher(startValue);
         if (!MyMatcher.matches()) {
           ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Invalid Ip Value : '+startValue));
           ipValidationFlag = false;
         }else {
           validatePrivateIps(startValue);
         } 
       }
       
       if(String.isNotBlank(endValue)) {
         MyMatcher = p.matcher(endValue);
         if (!MyMatcher.matches()) {
           ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Invalid Ip Value : '+endValue));
           ipValidationFlag = false;
         }else {
           validatePrivateIps(endValue);
         } 
       }
    }
    
    
    // Validation to restrict Private IP ranges
    //172.16.0.0 – 172.31.255.255
    //192.168.0.0 – 192.168.255.255
    //10.0.0.0 – 10.255.255.255
    
    private void validatePrivateIps(String ipValue) {
          String[] ipRanges = ipValue.split('\\.');
          String errMsg = 'Do not enter private ip values (172.16.0.0 – 172.31.255.255, 192.168.0.0 – 192.168.255.255, 10.0.0.0 – 10.255.255.255)';
          if(Integer.ValueOf(ipRanges[0]) == 10) {
              ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, errMsg));
              ipValidationFlag = false;
              return;
          }
          
          if(Integer.ValueOf(ipRanges[0]) == 172) {
            If(Integer.ValueOf(ipRanges[1]) >=16 && Integer.ValueOf(ipRanges[1]) <=31) {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, errMsg));
                ipValidationFlag = false;
                return;
            }
          }
          
          if(Integer.ValueOf(ipRanges[0]) == 192) {
            If(Integer.ValueOf(ipRanges[1]) == 168) {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, errMsg));
                ipValidationFlag = false;
                return;
            }
          }
    }
    
    
    
    
    private String trimExceptionMessage(String eMessage) {
        if (eMessage.contains('FIELD_CUSTOM_VALIDATION_EXCEPTION')) {
            return eMessage.substringBetween('FIELD_CUSTOM_VALIDATION_EXCEPTION,', ':').trim();
        } else {
            return eMessage;        
        }
    }


    private void validateRecord(Customer_Assessment__c record) {
        // put all record validations that cannot be done with validation rules here
        // throw AssessmentException and that will be displayed to the user
        
        // validate start date
        if (record.Start_Time__c.date() < getFirstAvailableDate().date()) {
            throw new AssessmentException('Start Date can not be earlier than ' + getFirstAvailableDate().date().format());
        }
        
        // validate end date
        if (record.End_Time__c.date() > record.Start_Time__c.addDays(30).date()) {
            throw new AssessmentException('End Date can not be more than 30 days after the Start Date ');
        }
        if (record.End_Time__c < record.Start_Time__c) {
            throw new AssessmentException('End Date must be greater than the Start Date');
        }
        
        ipValidationFlag = true;
        validateIpValues();
        if(ipValidationFlag == false) 
           throw new AssessmentException('Please review IP Exceptions');
    }

    @TestVisible
    private DateTime getFirstAvailableDate() {
        Datetime today = Datetime.now();
        // 5 business days
        Integer addDays = 5;
        String day = today.format('E');
        if ((day == 'Tue') || (day == 'Wed') || (day == 'Thu') || (day == 'Fri')) {
            // add the weekend
            addDays += 2;
        } else if (day == 'Sat') {
            // just sunday
            addDays += 1;
        }

        return today.addDays(addDays);
    }

    @TestVisible
    private void postLogin() {
        /*
           After the user logs in, we need to set some flags based on what they're doing. This
           method is also responsible for the access control check during revisit/edit
        */
        if (record == null) {
            // They're creating a record. Next stage is SAA
            changeStage(Stages.ACCEPT_SAA);
        } else {
            // Revisit/edit. Check the auth against the record's auth_info__c
            if (!isCurrentAuthSameAsRecordAuth()) {
                record = null;
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Unable to process request. Please contact support. Error Code : 8',''));
                changeStage(Stages.REQUEST_ERROR);
            } else {
                changeStage(Stages.EDIT_EXISTING);
                if (record.Approved__c == False) {
                    // The record is only editable if not approved
                    record_editable = True;
                }
            }

        }
    }

    private Customer_Assessment__c loadRecord(Id oid) {
       // Load existing customer assessment by Id. Used for re-visit/edit
       List<Customer_Assessment__c> res = [SELECT Approved__c,SAA_Accepted__c,Name,Company_Org_Id__c,PM_Name__c,PM_Work_Phone__c,
                                                  PM_Email__c,PM_Cell_Phone__c,Tester_Name__c,Tester_Email__c,Tester_Work_Phone__c,
                                                  Tester_Cell_Phone__c,Tests_Performed__c,Start_Time__c,End_Time__c,Org_IDs__c,
                                                  Usernames__c,Server_Load__c,Tools_Used__c,Source_IPs__c,Target_Environment__c,
                                                  Auth_Info__c,Additional_Target_Information__c,Additional_Email1__c,
                                                  Additional_Email2__c,Additional_Email3__c,Additional_Email4__c,Additional_Email5__c,
                                                  Public_IP_Statring_Range_1__c,Public_IP_Statring_Range_2__c,Public_IP_Statring_Range_3__c,
                                                  Public_IP_Statring_Range_4__c,Public_IP_Statring_Range_5__c,Public_IP_End_Range_1__c,
                                                  Public_IP_End_Range_2__c,Public_IP_End_Range_3__c,Public_IP_End_Range_4__c,
                                                  Public_IP_End_Range_5__c
                                           FROM Customer_Assessment__c WHERE Id=:oid];
       if (res.size() == 0) {
           return null;
       }

       Customer_Assessment__c request = res.get(0);

       SAA_Accepted = request.SAA_Accepted__c;
       company_name = request.Name;
       company_org_id = request.Company_Org_Id__c;
       pm_name = request.PM_Name__c;
       pm_workphone = request.PM_Work_Phone__c;
       pm_email = request.PM_Email__c;
       pm_cellphone = request.PM_Cell_Phone__c;
       tester_name = request.Tester_Name__c;
       tester_email = request.Tester_Email__c;
       tester_workphone = request.Tester_Work_Phone__c;
       tester_cellphone = request.Tester_Cell_Phone__c;
       overview_tests = request.Tests_Performed__c;
       start_time = request.Start_Time__c;
       end_time = request.End_Time__c;
       testing_orgs = request.Org_IDs__c;
       testing_users = request.Usernames__c;
       server_load = request.Server_Load__c;
       tools_used = request.Tools_Used__c;
       source_ip = request.Source_IPs__c;
       target_environment = request.Target_Environment__c;
       additional_target_info = request.Additional_Target_Information__c;
       additional_Email1 = request.Additional_Email1__c;
       additional_Email2 = request.Additional_Email2__c;
       additional_Email3 = request.Additional_Email3__c;
       additional_Email4 = request.Additional_Email4__c;
       additional_Email5 = request.Additional_Email5__c;
       publicIpStartRange1 = request.Public_IP_Statring_Range_1__c;
       publicIpStartRange2 = request.Public_IP_Statring_Range_2__c;
       publicIpStartRange3 = request.Public_IP_Statring_Range_3__c;
       publicIpStartRange4 = request.Public_IP_Statring_Range_4__c;
       publicIpStartRange5 = request.Public_IP_Statring_Range_5__c;
       publicIpEndRange1 = request.Public_IP_End_Range_1__c;
       publicIpEndRange2 = request.Public_IP_End_Range_2__c;
       publicIpEndRange3 = request.Public_IP_End_Range_3__c;
       publicIpEndRange4 = request.Public_IP_End_Range_4__c;
       publicIpEndRange5 = request.Public_IP_End_Range_5__c; 

       return request;
    }

    private Boolean isCurrentAuthSameAsRecordAuth() {
        // Take a userinfo class that implements Stringable and compare it to the saved Auth_Info__c field
        // of a record. This is used to verify that the user attempting to load the record is the same
        // user that created the record
        if ((record == null) || (record.Auth_Info__c == null)) {
            return False;
        }
        return userInfo.toString() == record.Auth_Info__c;
    }

    private void hideUnusedAuthProviders(boolean SFDCOnly) {
       if ((!sfdcOnly) && ((record == null) || (record.Auth_Info__c == null))) {
           return;
       }

       if (sfdcOnly || (record.Auth_Info__c.startsWith('SFDC'))) {
           showFB = showLI = showGoogle = False;
       } else if (record.Auth_Info__c.startsWith('FB')) {
           showSFDC = showLI = showGoogle = False;
       } else if (record.Auth_Info__c.startsWith('LI')) {
           showSFDC = showFB = showGoogle = False;
       } else if (record.Auth_Info__c.startsWith('GOOGLE')) {
           showSFDC = showFB = showLI = False;
       }

    }

    @TestVisible
    public PageReference setTargetEnv() {
        if (target_environment == 'Other') {
            requires_additional_target_info = True;
        }

        Boolean found = False;
        for (Customer_Assessment_Areas__mdt area : assessmentAreas) {
            if (target_environment == area.DeveloperName) {
                found = True;

                if(target_environment.startsWith('Heroku')) {
                    PageReference p = new PageReference('https://help.heroku.com/pentest-requests/new');
                    p.setredirect(true);
                    return p;
                }

                if (area.Is_SFDC_Platform__c) {
                    // only show SFDC login per requirements
                    hideUnusedAuthProviders(true);
                }
                break;
            }
        }
        if (found) {
            changeStage(Stages.AUTHORIZE);
        }
        return null;
    }

    public List<SelectOption> getEnvironments() {
       if (assessmentAreas == null) {
           assessmentAreas = [SELECT Label,DeveloperName,Is_SFDC_Platform__c FROM Customer_Assessment_Areas__mdt ORDER BY Label];
       }

       List<SelectOption> res = new List<SelectOption>();
       res.add(new SelectOption('', '--Select--'));

       for (Customer_Assessment_Areas__mdt area : assessmentAreas) {
         res.add(new SelectOption(area.DeveloperName, area.Label));
       }
       return res;
    }

    // All userinfo classes must support toString so we can compare them to stored record auth
    public interface Stringable { String toString(); }

    /* Begin SFDC Oauth classes */
    private class OauthTokenResponse{

        public string id;
        public string access_token;

    }
    private class IdentityResponse implements Stringable{

        public string user_id;
        public string organization_id;
        public string email;
        public string display_name;
        public string mobile_phone;
        public string sobjects;

        public Map<string,string> urls;

        public override String toString() {
            return 'SFDC|'+user_id+':'+organization_id;
        }

    }
    /* End SFDC Oauth classes */

    /* Begin Facebook oauth classes */
    public class FBUserInfo implements Stringable{

        public String name;
        public String email;
        public String id;

        public FBUserInfo parse(String json) {
            return (FBUserInfo) System.JSON.deserialize(json, FBUserInfo.class);
        }

        public override String toString() {
            return 'FB|'+id;
        }
    }
    /* End Facebook oauth classes */

    /* Begin Google oauth classes */
    public class GoogleUserInfo implements Stringable{

        public String name;
        public String email;
        public String sub;
        public String aud; // audience, must be checked that it matches client ID

        public GoogleUserInfo parse(String json) {
            GoogleUserInfo usr = (GoogleUserInfo) System.JSON.deserialize(json, GoogleUserInfo.class);
            if (usr.aud != GOOGLE_CLIENT_ID) {
                return null;
            }
            return usr;
        }

        public override String toString() {
            return 'GOOGLE|'+sub;
        }
    }
    /* End Google oauth classes */

    /* Begin LinkedIn oauth classes */
    public class Company {
            public String name;
        }

    public class Values {
        public Company company;
        public Boolean isCurrent;
        public Company location;
        public String title;
    }

    public class Positions {
        public List<Values> values;
    }

    public class LinkedInUserInfo implements Stringable {

        public String emailAddress;
        public String firstName;
        public String lastName;
        public String id;
        public Positions positions;

        public override String toString() {
            return 'LI|'+id;
        }

        public string companyName {
            get {
                if ((this.positions != null) && (this.positions.values.size() > 0)) {
                    return this.positions.values.get(0).company.name;
                }
                return null;
            }
        }
        public string email {
            get {
                return this.emailAddress;
            }
        }

        public string name {
            get {
                String f = firstName;
                if (String.isBlank(firstName)) { firstName = '';}
                String l = lastName;
                if (String.isBlank(lastName)) { lastName = '';}
                return f+' '+l;
            }
        }

        public LinkedInUserInfo parse(String json) {
            return (LinkedInUserInfo) System.JSON.deserialize(json, LinkedInUserInfo.class);
        }

    }
    /* End LinkedIn oauth classes */
}
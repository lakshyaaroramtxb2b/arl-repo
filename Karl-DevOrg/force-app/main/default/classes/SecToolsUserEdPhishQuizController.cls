public class SecToolsUserEdPhishQuizController {

    public Boolean is_started {get;set;}
    public Boolean is_done {get;set;}
    public Integer questionCount {get;private set;}

    public Id qId {get;set;}
    public String sId;
    public PhishingQuizQuestion__c question;
    public List<PhishingQuizAnswer__c> missedAnswers {get;private set;}
    public List<PhishingQuizAnswer__c> allAnswers {get;private set;}
    public Integer correctCount {get;set;}
    private Set<String> incorrectTopicSet = new Set<String>();
    public List<String> incorrectTopics {get;set;}
    public PhishingQuizUser__c user;
    public String answer {get;set;}
    public Integer answeredCount {get;set;}
    
    public class infowrapper {
        public String altlink {get;set;}  
        public String link {get;set;}
        public String correct {get;set;}
    }
    
    public SecToolsUserEdPhishQuizController() {
        is_started = is_done = False;
        questionCount = 10;
        answeredCount = 0;
    }
    
    public PageReference start() {
        is_started=True;

        String userId = String.valueOf(Crypto.getRandomLong());
        user = new PhishingQuizUser__c(Name=userid,
                               Email__c='random@salesforce.com',
                               Company__c=userid,
                               Key__c=userid,
                               Created__c=System.now());
        insert user;
        getNextQuestion();
        return null;}
    
    public PhishingQuizQuestion__c getNextQuestion() {
        // Get the questions that the QuizUser hasn't answered
        List<PhishingQuizQuestion__c> all = [Select Id,link__c,answer__c,Name from PhishingQuizQuestion__c where active__c = 1
                                           and Id not in 
                                           (select PhishingQuizQuestion__c from PhishingQuizAnswer__c where 
                                               PhishingQuizUser__c =: user.Id)
                                        ];
        // It looks like they're done, there are no more questions or they've answered enough
        if ((all.size() == 0) || (answeredCount >= questionCount)) {
            is_done = True;
            score();
            return null;
        } 
        
        Integer rint = math.mod(math.abs(Crypto.getRandomInteger()),all.size());
        question = all.get(rint);
        
        // might as well be a void(). This isn't really used
        return question;
    }
    
    public String getLink() {        return question.link__c;    }
    
    public PageReference submitAnswer() {
        PhishingQuizAnswer__c a = new PhishingQuizAnswer__c(Name=question.Name+':'+user.Name,
                                            PhishingQuizUser__c=user.Id,
                                            PhishingQuizQuestion__c=question.Id);
        
        if (this.answer == question.answer__c) {
            a.isCorrect__c=1;
        } else {
            a.isCorrect__c=0;
        }
        insert a;
        answeredCount +=1;
        getNextQuestion();

        return null;
    }
    
    public PageReference score() {
        System.debug('**score');
        if (is_done == False) {
            return null;
        }
        
        missedAnswers = new List<PhishingQuizAnswer__c>();
        
        allAnswers = [select Name,Id,isCorrect__c 
                          from PhishingQuizAnswer__c where PhishingQuizUser__c =: user.Id];
        correctCount = 0;
        for (PhishingQuizAnswer__c ans: allAnswers) {
            if (ans.isCorrect__c != 1){
                missedAnswers.add(ans);
            }
            else {
                correctCount +=1; //hey, they got it!
            }
        }
        incorrectTopics = new List<String>();
        for (String s: incorrectTopicSet) {
          //lame that we can't pass a Set to apex:repeat
          incorrectTopics.add(s);
        }
        
        if (user.quiz_completion__c == null) {
            user.quiz_completion__c = datetime.now();
        }
        user.quiz_score__c = numPercentCorrect();
        update user;
        return null;
        
    }
    
    public Long numPercentCorrect() {
        return Math.roundToLong(((Double)correctCount/questionCount)*100);
    }
    
    public String getPercentCorrect() {
        return String.valueOf(numPercentCorrect())+'%';
    }
    
    public List<infowrapper> getTheInfo() {
        System.debug('**getTheInfo');
        if (is_done == False) {
            return null;
        }
        
        List<PhishingQuizAnswer__c> phish= [select isCorrect__c, PhishingQuizQuestion__r.altlink__c, PhishingQuizQuestion__r.link__c from PhishingQuizAnswer__c
                        where PhishingQuizUser__c =: user.Id];
        List<infowrapper> tmp = new List<infowrapper>();
        infowrapper x;
        for (PhishingQuizAnswer__c p: phish) {
            x = new infowrapper();
            x.altlink = p.PhishingQuizQuestion__r.altlink__c;
            x.link = p.PhishingQuizQuestion__r.link__c;
            if(p.isCorrect__c == 1)
            {
                x.correct = 'Correct';
            } else {
                x.correct = 'Incorrect';
            }
            tmp.add(x); //I think the call is push(), it may be append() or something
        }
        return tmp;
    }
    
    public String getRowDiv() {
      // dumb
      return '<div class="row">';
    }
    public String getEndRowDiv() {
      // also dumb
      return '</div>';
    }
}
public class Esa_Script_UpdateHireDate_From_Sforce{

/**
*  Execute following in workbench
*  Esa_Script_UpdateHireDate_From_Sforce.updateEmployeesInfo();
*  Esa_Script_UpdateHireDate_From_Sforce.updateContactInfo();
**/

  //copy the values from supportforce to Employee object
  public static void updateEmployeesInfo(){
       List<INTEG_Employee__c> lstEmployees = [SELECT Id,
                                               INTEG_Hire_Date__c,
                                               INTEG_Email__c,
                                               Federation_Identifier__c
                                        FROM INTEG_Employee__c
                                        WHERE INTEG_Hire_Date__c = null 
                                        AND Federation_Identifier__c LIKE '%salesforce.com' 
                                        LIMIT 100];

        System.debug('#######'+lstEmployees.size());

        Set<String> setEmployeeFedIds = new Set<String>();
        for(INTEG_Employee__c employee : lstEmployees){
          setEmployeeFedIds.add(employee.Federation_Identifier__c);
        }
        
        List<Contact__x> lstSupportForceContacts = [SELECT Federation_ID_c__c,
          Start_Date_c__c
        FROM Contact__x
        WHERE Federation_ID_c__c IN :setEmployeeFedIds];
        
        Map<String,Date> mapEmployeeStartDate = new Map<String,Date>();
        
        for(Contact__x contactX : lstSupportForceContacts){
        mapEmployeeStartDate.put(contactX.Federation_ID_c__c, contactX.Start_Date_c__c);
        }
        
        for(INTEG_Employee__c employee : lstEmployees){
          employee.INTEG_Hire_Date__c = mapEmployeeStartDate.get(employee.Federation_Identifier__c);
          System.debug('######'+employee.INTEG_Hire_Date__c);
        }
        
        update lstEmployees;
   }

    // Copy Contacts info from Employees object.
    
    public static void updateContactInfo(){
        List<Contact> lstContacts = [SELECT Id,
                                            Hire_Date__c,
                                            Federation_Identifier__c
                                     FROM Contact
                                     WHERE Hire_Date__c=null
                                     AND Federation_Identifier__c Like '%salesforce.com'
                                     LIMIT 1000];
            system.debug('##########'+lstContacts.size());
            Set<String> setMissingContacts = new Set<String>();
            for(Contact cont : lstContacts){
            setMissingContacts.add(cont.Federation_Identifier__c);
            }
            
            List<INTEG_Employee__c> lstEmployees = [SELECT Id,
                                                           INTEG_Hire_Date__c,
                                                           Federation_Identifier__c
                                                     FROM INTEG_Employee__c
                                                     WHERE Federation_Identifier__c IN :setMissingContacts];
            
            Map<String,Date> mapEmployeeStartDate = new Map<String,Date>();
            for(INTEG_Employee__c employee : lstEmployees){
            mapEmployeeStartDate.put(employee.Federation_Identifier__c, employee.INTEG_Hire_Date__c);
            }
            
            for(Contact cont : lstContacts){
                cont.Hire_Date__c = mapEmployeeStartDate.get(cont.Federation_Identifier__c);
                System.debug('$$$$$$$$'+cont.Hire_Date__c);
            }
            
            update lstContacts;
                
    } 

}
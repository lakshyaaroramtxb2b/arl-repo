public class SC_AutoenrollUserscheduler implements Schedulable {
    public void execute(SchedulableContext sc) {
        for (Training_Course__c course : [
            SELECT 
                Auto_Enroll_Where_Clause_10K__c,
                Auto_Enrollment_Enabled__c,
                Default_Due_Date__c,
                Default_Due_Days__c,
                Default_Enrollment_Template_DevelopName__c,
                Tracked__c,
                Org_Wide_Email_Id__c,
                Id
            FROM Training_Course__c 
            WHERE Tracked__c = true
            AND Auto_Enrollment_Enabled__c = true
        ]) {
            SC_AutoEnroll enroller = new SC_AutoEnroll(course);
            enroller.run();
            SC_AutoUnenroll unenroller = new SC_AutoUnenroll(course);
            unenroller.run();
        }
    }
}
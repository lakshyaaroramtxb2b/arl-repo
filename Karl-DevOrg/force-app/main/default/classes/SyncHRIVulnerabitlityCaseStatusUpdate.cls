/*
* Sync External Case/Bug status back to HRI_Vulnerability Record
* Id batchJobId = Database.executeBatch(new SyncHRIVulnerabitlityCaseStatusUpdate(), 100);
 */

public class SyncHRIVulnerabitlityCaseStatusUpdate implements Schedulable, Database.Batchable<sObject> {

    public String SOURCE_FILE = 'SyncHRIVulnerabitlityCaseStatusUpdate';
    
    public void execute(SchedulableContext ctx) {
      Database.executebatch(new SyncHRIVulnerabitlityCaseStatusUpdate(), 100);
    }

    public Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([
                              SELECT Id, 
                                     External_Case_Closed__c,
                                     Supportforce_Case__c,
                                     Supportforce_Case_Closed_Date__c, 
                                     Supportforce_Case_Status__c,
                                     Supportforce_Case_Last_Modified_Date__c,
                                     Supportforce_Case_Due_By_Date__c,
                                     Gus_Bug__c,
                                     Vuln_Resolved_Within_SLA__c,
                                     Supportforce_Case__r.Status__c,
                                     Supportforce_Case__r.ClosedDate__c,
                                     Supportforce_Case__r.LastModifiedDate__c,
                                     Gus_Bug__r.Status_c__c,
                                     Gus_Bug__r.Closed_On_c__c,
                                     Gus_Bug__r.LastModifiedDate__c
                              FROM HRI_Vulnerability__c
                              WHERE (Supportforce_Case__c != null OR Gus_Bug__c != null)
                              AND (External_Case_Closed__c = false 
                              OR Supportforce_Case_Closed_Date__c >= LAST_WEEK)]);
    }
    
    public void execute(Database.BatchableContext BC, List<HRI_Vulnerability__c> lstVulnerabilities) {  

        // Update HRI Vulnerability Records with External Case/Bug Status
        Set<String> caseIds = new Set<String>();
                              
        if(lstVulnerabilities.size() > 0) {

            for(HRI_Vulnerability__c hriVuln : lstVulnerabilities){
                if(hriVuln.Supportforce_Case__c != null) {
                  hriVuln.Supportforce_Case_Closed_Date__c = hriVuln.Supportforce_Case__r.ClosedDate__c;
                  hriVuln.Supportforce_Case_Status__c = hriVuln.Supportforce_Case__r.Status__c;
                  hriVuln.Supportforce_Case_Last_Modified_Date__c = hriVuln.Supportforce_Case__r.LastModifiedDate__c;
                  
                  if(hriVuln.Supportforce_Case_Closed_Date__c == null){
                     if(hriVuln.Supportforce_Case_Due_By_Date__c < Date.today()){
                        hriVuln.Vuln_Resolved_Within_SLA__c = 'False';
                     }
                  }
                }

                if(hriVuln.Gus_Bug__c != null) {
                  hriVuln.Supportforce_Case_Closed_Date__c = hriVuln.Gus_Bug__r.Closed_On_c__c;
                  hriVuln.Supportforce_Case_Status__c = hriVuln.Gus_Bug__r.Status_c__c;
                  hriVuln.Supportforce_Case_Last_Modified_Date__c = hriVuln.Gus_Bug__r.LastModifiedDate__c;
                  
                  if(hriVuln.Supportforce_Case_Closed_Date__c == null){
                     if(hriVuln.Supportforce_Case_Due_By_Date__c < Date.today()){
                        hriVuln.Vuln_Resolved_Within_SLA__c = 'False';
                     }
                  }
                }

                if(hriVuln.Supportforce_Case_Closed_Date__c != null) {
                  hriVuln.External_Case_Closed__c = true;
                }

            }
            
            List<Database.SaveResult> saveResults = Database.update(lstVulnerabilities,false);

            String finalErrorText = '';
            Integer numberCasesUpdated = 0;
            for (Database.SaveResult sr : saveResults) {
                if (sr.isSuccess()) {
                            numberCasesUpdated++;
                        } else {
                            for(Database.Error err : sr.getErrors()) {
                                finalErrorText += err.getStatusCode() + ': ' + err.getMessage() + '\n';
                            }
                }
            }
            
            Esa_DebugService.writeMessage('SyncExternalCaseStatus HRI_Vulnerability Completed:');
            if(string.isNotBlank(finalErrorText)) {
                Esa_DebugService.writeMessage('Sync External Case Status Update - HRI Vulnerability. finish: Failed with errors');
                Exception ex;
                Esa_DebugService.writeException(ex, 1, SOURCE_FILE, finalErrorText);
            }

        }
    }

    public void finish(Database.BatchableContext BC) {
        if(!Test.isRunningTest()) {
           SyncHRIVulnerabitlityCaseStatusUpdate batch = new SyncHRIVulnerabitlityCaseStatusUpdate();
           System.scheduleBatch(batch, 'SyncHRIVulnerabitlityCaseStatusUpdate', 30, 100);
        }
    }
}
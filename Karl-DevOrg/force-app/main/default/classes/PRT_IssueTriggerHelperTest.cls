@isTest
public class PRT_IssueTriggerHelperTest {
    static testMethod void updateSlippageCommentOfProjectTest() {
        List<Project__c> projectList = new List<Project__c>(PRT_TestDataFactory.createProjects(1,false));
        projectList.get(0).RecordTypeId = PRT_Constants.PRT_PROJECT_PROJECT_RECORDTYPEID;
        INSERT projectList;
        Issues__c issueRec = new Issues__c();
        issueRec.Project__c = projectList.get(0).Id;
        issueRec.Status__c = 'In Progress';
        issueRec.Description__c = 'Test 101 Desc';
        INSERT issueRec;
        List<Project__c> updatedProjectList = new List<Project__c>([SELECT Slippage_Comments__c FROM Project__c]);
        System.assertEquals('- Test 101 Desc', updatedProjectList.get(0).Slippage_Comments__c);
        List<Issues__c> updateIssueList = new List<Issues__c>([SELECT Id, Description__c FROM Issues__c]);
        updateIssueList.get(0).Status__c = 'Closed';
        UPDATE updateIssueList;
        List<Project__c> updatedProject2List = new List<Project__c>([SELECT Slippage_Comments__c FROM Project__c]);
        System.assertEquals(null, updatedProject2List.get(0).Slippage_Comments__c);
    } 
}
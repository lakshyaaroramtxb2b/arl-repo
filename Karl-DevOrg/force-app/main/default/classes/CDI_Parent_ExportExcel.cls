public with sharing class CDI_Parent_ExportExcel {

public String Recordid { get; set; }
  
public PageReference redirect()
{
      
   PageReference pr = new PageReference('/apex/CDI_ExportExcel');
      pr.getParameters().put('datastoreId', ApexPages.CurrentPage().getparameters().get('id'));
        pr.setRedirect(true);
        return pr;
 
} 
}
global with sharing class HW_OspreyNotificationExec{
    public HW_OspreyNotificationExec(){}
    public static void removeInactiveUserAccounts(){
        HW_OspreyNotificationHandler handler = new HW_OspreyNotificationHandler();
        handler.teams = new Set<String>{'sfdc-aloha','sfdc-platform-dreamhouse'};
        handler.alertType = 'removeInactiveUsers';
        handler.query = 'SELECT email__c, Heroku_Warden_organizations__r.name__c FROM Heroku_Warden_users__c WHERE Employee__c != null AND Employee__r.INTEG_Active__c = false AND Heroku_Warden_organizations__r.name__c=:teams AND LastModifiedDate >= LAST_N_DAYS:1';
        //split in to chunks of size 50 each which provides good treshhold to avoid 'Exceeded maximum time allotted for call out (120000 ms)' error
        Database.executeBatch(handler,50);
    }
}
/*
 * Apex class to support Trail enrollment
 */

public class SC_TrainingCourseTakenModuleManager {

    public static Training_Course_Taken__c[] rollupModuleProgress(Training_Course_Taken__c[] courseTakenRecs) {
        Training_Course_Taken__c[] pathTakenRecs = getRelatedPathTakenStatus(courseTakenRecs);
        if (!pathTakenRecs.isEmpty()) {
            update pathTakenRecs;
        }
        return pathTakenRecs;
    }

    public static Training_Course_Taken__c[] getRelatedPathTakenStatus(Training_Course_Taken__c[] courseTakenRecs) {

        Training_Course_Taken__c[] pathTakenRecs = new Training_Course_Taken__c[0];
        for (AggregateResult pathSummary : [
            SELECT 
                MIN(moduleTaken.Date_Started__c) pathDateStarted,
                MAX(moduleTaken.Date_Completed__c) pathDateCompleted,
                AVG(moduleTaken.Progress__c) pathProgress,
                rel.Related_Path_Taken__c pathTakenId
            FROM Training_Course_Taken_Relationship__c rel,
                rel.Related_Module_Taken__r moduleTaken
            WHERE Related_Path_Taken__c IN :getPathTakenIds(courseTakenRecs)
                AND Related_Path_Taken__c != null
                AND moduleTaken.Training_Course__r.Is_Active__c = true 
            GROUP BY rel.Related_Path_Taken__c
        ]) {
            Training_Course_Taken__c pathTaken = new Training_Course_Taken__c(Id = (Id) pathSummary.get('pathTakenId'));
            pathTaken.Progress__c = ((Decimal) pathSummary.get('pathProgress')).intValue();
            pathTaken.Date_Started__c = (Date) pathSummary.get('pathDateStarted');
            pathTaken.Last_Import_Date__c = Date.today();
            if (pathTaken.Progress__c == 100) {
                pathTaken.Date_Completed__c = (Date) pathSummary.get('pathDateCompleted');
                pathTaken.Status__c = TrainingCourseTaken_Constants.STATUS_COURSE_COMPLETED;
            } else if (pathTaken.Progress__c > 0) {
                pathTaken.Status__c = TrainingCourseTaken_Constants.STATUS_COURSE_STARTED;
            } else {
                pathTaken.Status__c = TrainingCourseTaken_Constants.STATUS_COURSE_NOT_STARTED;
            }
            pathTakenRecs.add(pathTaken);
        }
        return pathTakenRecs;
    }

    // takes a list of course taken records and returns a list of related paths for 
    // any module taken records
    private static Set<Id> getPathTakenIds(Training_Course_Taken__c[] courseTakenRecs) {
        Set<Id> pathTakenIds = new Set<Id>();
        for (Training_Course_Taken__c moduleTaken : [
            SELECT (SELECT Related_Path_Taken__c FROM Related_Paths_Taken__r) 
            FROM Training_Course_Taken__c
            WHERE Id IN :courseTakenRecs
        ]) {
            for (Training_Course_Taken_Relationship__c pathTakenRelation : moduleTaken.Related_Paths_Taken__r) {
                pathTakenIds.add(pathTakenRelation.Related_Path_Taken__c);
            }
        }
        return pathTakenIds;
    }
}
public with sharing class SPT_LookupController {

    @AuraEnabled
    public static List<SObJectResult> getResults(String ObjectName, String fieldName, String value) {
        Set<String> wl = new Set<String>{'Case', 'CaseComment', 'Case__x', 'CaseComment__x'};
        if (!wl.contains(ObjectName)) return null;    
        List<SObJectResult> sObjectResultList = new List<SObJectResult>();
        for(sObject so : Database.Query('Select Id,'+fieldName+' FROM '+ObjectName+' WHERE '+fieldName+' LIKE \'%' + value + '%\''+' WITH SECURITY_ENFORCED LIMIT 5 ')) {
            String fieldvalue = (String)so.get(fieldName);
            sObjectResultList.add(new SObjectResult(fieldvalue, so.Id));
        }
        //System.debug('Output: '+Database.Query('Select Id,'+fieldName+' FROM '+ObjectName+' WHERE '+fieldName+' LIKE \'%' + value + '%\''));
        
        return sObjectResultList;
    }

    @AuraEnabled
    public static List<ParentCaseWrapper> getCaseNumber(Id recordId){
        List<ParentCaseWrapper> parentCaseList = new List<ParentCaseWrapper>();
        List<Case> caseList = new List<Case>([SELECT Id, CaseNumber, Parent.CaseNumber, Enterprise_Security_Case_Number__c, Parent.Enterprise_Security_Case_Number__c
                                              FROM Case
                                              WHERE Id =: recordId]);
        if(caseList!=null || !caseList.isEmpty()) {
            if(caseList.get(0).Enterprise_Security_Case_Number__c != null)
                parentCaseList.add(new ParentCaseWrapper(caseList.get(0).ParentId, caseList.get(0).Parent.Enterprise_Security_Case_Number__c));
            return parentCaseList;
        }        
        return parentCaseList;
    }

    @AuraEnabled
    public static Boolean updateParentCase(Id recordId, String parentCaseId){
        List<Case> caseList = new List<Case>([SELECT Id, ParentId FROM Case WHERE Id =: recordId]);

        if(caseList!=null || !caseList.isEmpty()) {
            if(parentCaseId == '' && String.isEmpty(parentCaseId))
                caseList.get(0).ParentId = null;
            else 
                caseList.get(0).ParentId = parentCaseId;
            UPDATE caseList;
            System.debug('Parent Case Updated');
            return true;
        }
        return false;
    }
    
    @AuraEnabled
    public static CaseDetailWrapper getCaseDetail(Id recordId){ 
        CaseDetailWrapper caseDetail = new CaseDetailWrapper();
        List<Case> caseList = new List<Case>([SELECT Id,RecordTypeId, Owner.Name, CaseNumber, Parent.CaseNumber, Enterprise_Security_Case_Number__c, Parent.Enterprise_Security_Case_Number__c
                                              FROM Case
                                              WHERE Id =: recordId]);
        if(caseList!=null || !caseList.isEmpty()) {
            List<String> restrictedQueueGroupNamesList = new List<String>();
            List<String> restrictedQueueGroupNamesMasterList = new List<String>();
            
            //Fetch list of restricted queue or group names metadata.
            SPT_Case_Owner_Queue_Group_Name__mdt [] queueOrGroupNames = [SELECT MasterLabel,QualifiedApiName FROM SPT_Case_Owner_Queue_Group_Name__mdt];
            if(!queueOrGroupNames.isEmpty()){
                for(SPT_Case_Owner_Queue_Group_Name__mdt item : queueOrGroupNames){
                    restrictedQueueGroupNamesList.add(item.QualifiedApiName); //List of names
                    restrictedQueueGroupNamesMasterList.add(item.MasterLabel); //List of names
                    
                }
                List<GroupMember> gm  = [SELECT UserOrGroupId
                                         FROM 
                                         GroupMember 
                                         WHERE Group.DeveloperName IN : restrictedQueueGroupNamesList AND UserOrGroupId = :caseList[0].OwnerId
                                        ];
                //If true, it means owner is part of restricted queue or group.
                if(!gm.isEmpty() || restrictedQueueGroupNamesMasterList.contains(caseList[0].Owner.Name)){
                    caseDetail.isOwnerNotQueueOrGroup = false;
                }
                
            }
            //commented until get confirmation.
            /*if( caseList[0].RecordTypeId == SPT_Constants.SEC_RECORDTYPE_ID){
                caseDetail.isValidRecordType = false;
            }*/
            if(caseList[0].Enterprise_Security_Case_Number__c != null){
                caseDetail.parentId = caseList[0].ParentId;
                caseDetail.caseNumber = caseList[0].Parent.Enterprise_Security_Case_Number__c;
                
            }
        }        
        return caseDetail;
    }
    
    @AuraEnabled
    public static void deleteCase(Id recordId){
        try {
            delete [Select Id From Case Where Id =:recordId];
        } catch(DmlException e) {
            // "Convert" the exception into an AuraHandledException
            throw new AuraHandledException('Something went wrong: '
                                           + e.getMessage());  
        }
        
    }
    public class CaseDetailWrapper {
        @AuraEnabled
        public Boolean isValidRecordType {get;set;}
        @AuraEnabled
        public Boolean isOwnerNotQueueOrGroup {get;set;}
        @AuraEnabled
        public Id parentId {get;set;}
        @AuraEnabled
        public String caseNumber {get;set;}
        public CaseDetailWrapper(){
            this.isValidRecordType = true;
            this.isOwnerNotQueueOrGroup = true;
            this.parentId = null;
            this.caseNumber = '';
            
        }
		   
        
    }
    
    public class SObJectResult {
        @AuraEnabled
        public String recName;
        @AuraEnabled
        public Id recId;
        
        public SObJectResult(String recNameTemp, Id recIdTemp) {
            recName = recNameTemp;
            recId = recIdTemp;
        }
    }

    public class ParentCaseWrapper {
        @AuraEnabled
        public Id parentId;
        @AuraEnabled
        public String caseNumber;

        public ParentCaseWrapper(Id parentIdTemp, String caseNumberTemp) {
            parentId = parentIdTemp;
            caseNumber = caseNumberTemp;
        }
    }
}
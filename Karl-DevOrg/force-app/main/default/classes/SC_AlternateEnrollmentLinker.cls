/**
 * @author: ralph@callaway.cloud
 * @description: Responsible for creating/linking alternate enrollments for enrollments
 *     tied to courses with alternates.
 */
public class SC_AlternateEnrollmentLinker implements Queueable {
    
    // ensure people don't enqueue directly and end up with multiple 
    // batches running at the same time 
    private SC_AlternateEnrollmentLinker() { }

    /**
     * Link alternate enrollments for up to 1k enrollments for a single course
     * @param context QueableContext
     */
    public void execute(QueueableContext context) {

        // load batch for one course and existing alternates
        Training_Course_Taken__c[] enrollments = getEnrollmentBatch();
        if (enrollments.isEmpty()) return;
        Id alternateCourseId = enrollments[0].Training_Course__r.Alternate_Course__c;
        
        // clear alternate enrollments if parent doesn't have alternate
        if (alternateCourseId == null) {
            for (Training_Course_Taken__c enrollment : enrollments) {
                enrollment.Alternate_Enrollment__c = null;
            }
            update enrollments;

        // otherwise, generate/link alternate enrollments
        } else {
            
            // load / generate altnerate enrollments
            Map<Id, Training_Course_Taken__c> alternates = getExistingAlternates(enrollments);
            Training_Course_Taken__c[] newAlternates = new Training_Course_Taken__c[0];
            for (Training_Course_Taken__c enrollment : enrollments) {
                if (!alternates.containsKey(enrollment.Contact__c)) {
                    Training_Course_Taken__c alternate = generateAlternate(enrollment);
                    newAlternates.add(alternate);
                    alternates.put(alternate.Contact__c, alternate);
                }
            }
            insert newAlternates;

            // link enrollments to their alternates
            for (Training_Course_Taken__c enrollment : enrollments) {
                enrollment.Alternate_Enrollment__c = alternates.get(enrollment.Contact__c).Id;
            }
            update enrollments;
        }

        // continue with next batch if needed
        if (hasWork()) {
            System.enqueueJob(new SC_AlternateEnrollmentLinker());
        }
    }

    /**
     * Queues enrollment linker if not already running (not checking to see if we have work)
     */
    public static void run() {
        if (!isRunning()) {
            System.enqueueJob(new SC_AlternateEnrollmentLinker());
        }
    }

    /**
     * Checks if any of the enrollments have 
     * @param  enrollments list of enrollments to check
     * @return             whether alternate_course_mismatch__c is true for any records
     */
    public static Boolean hasAlternateMismatch(Training_Course_Taken__c[] enrollments) {
        for (Training_Course_Taken__c enrollment : enrollments) {
            if (enrollment.Alternate_Enrollment_Mismatch__c) {
                return true;
            }
        }
        return false;
    }

    private static Boolean isRunning() {
        return ![
            SELECT Id
            FROM AsyncApexJob
            WHERE ApexClass.Name = 'SC_AlternateEnrollmentLinker'
                AND JobType = 'Queueable'
                AND Status IN ('Holding', 'Queued', 'Preparing', 'Processing')
            LIMIT 1
        ].isEmpty();
    }

    /**
     * Create an enrollment for the alternate course
     * @param  enrollment               existing enrollment
     * @return Training_Course_Taken__c alternate enrollment
     */
    private static Training_Course_Taken__c generateAlternate(Training_Course_Taken__c enrollment) {
        Training_Course_Taken__c alternate = (Training_Course_Taken__c) Training_Course_Taken__c.sObjectType
            .newSObject(TrainingCourseTaken_Constants.RT_ID_VOLUNTARY, true);
        alternate.Training_Course__c = enrollment.Training_Course__r.Alternate_Course__c;
        alternate.Contact__c = enrollment.Contact__c;
        alternate.Attendee_Email__c = enrollment.Attendee_Email__c;
        alternate.Status__c = TrainingCourseTaken_Constants.STATUS_COURSE_NOT_STARTED;
        return alternate;
    }

    /**
     * Returns true if there are additional batches that can be processed
     * @return Boolean
     */
    private static Boolean hasWork() {
        // query is slightly optimized to avoid query selectivity issues
        // by only returning records related to courses with alternates
        // or enrollments with alternates related to a course 
        return ![
            SELECT Id 
            FROM Training_Course_Taken__c 
            WHERE Alternate_Enrollment_Mismatch__c = true 
                AND (Training_Course__r.Alternate_Course__c != null 
                    OR (Training_Course__r.Alternate_Course__c = null AND Alternate_Enrollment__c != null))
                LIMIT 1].isEmpty();
    }

    /**
     * Loads any existing alternate enrollments for supplied batch
     * @param  enrollments enrollments to check for alternates
     * @return             existing alterates mapped by contact id
     */
    private static Map<Id, Training_Course_Taken__c> getExistingAlternates(Training_Course_Taken__c[] enrollments) {
        Set<Id> contacts = new Set<Id>();
        for (Training_Course_Taken__c enrollment : enrollments) {
            contacts.add(enrollment.Contact__c);
        }
        Map<Id, Training_Course_Taken__c> existingAlternates = new Map<Id, Training_Course_Taken__c>();
        for (Training_Course_Taken__c alternate : [
            SELECT Contact__c
            FROM Training_Course_Taken__c
            WHERE Training_Course__c = :enrollments[0].Training_Course__r.Alternate_Course__c
                AND Contact__c IN :contacts
        ]) {
            existingAlternates.put(alternate.Contact__c, alternate);
        }
        return existingAlternates;
    }

    /**
     * Returns a list of enrollments that need updates to their alternate enrollment
     * for just one course
     * @return Training_Course_Taken__c[] enrollmentBatch
     */
    private Training_Course_Taken__c[] getEnrollmentBatch() {
        Training_Course_Taken__c[] enrollmentBatch = new Training_Course_Taken__c[0];
        for (Training_Course_Taken__c enrollment : [
            SELECT 
                Alternate_Enrollment__c,
                Attendee_Email__c,
                Contact__c,
                Training_Course__r.Alternate_Course__c, 
                Id
            FROM Training_Course_Taken__c 
            WHERE Alternate_Enrollment_Mismatch__c = true
                AND (Training_Course__r.Alternate_Course__c != null 
                    OR (Training_Course__r.Alternate_Course__c = null AND Alternate_Enrollment__c != null))
            ORDER BY Training_Course__c
            LIMIT 1000
        ]) {
            if (enrollmentBatch.isEmpty() || enrollmentBatch[0].Training_Course__c == enrollment.Training_Course__c) {
                enrollmentBatch.add(enrollment);
            }    
        }
        return enrollmentBatch;
    }

}
/*
* Created by - Himanshu Shrimali (MTX Group Inc.)
* Class to store constants for Case object work.
*/
public class SPT_Constants {
    // Data memebers
    //public final static List<Schema.RecordTypeInfo> CASE_RECORDTYPE_LIST = Case.SObjectType.getDescribe().getRecordTypeInfos();
    public final static String SEC_RECORDTYPE_NAME = 'Enterprise Security';
    public final static Id SEC_RECORDTYPE_ID = Schema.SObjectType.Case.getRecordTypeInfosByName().get(SEC_RECORDTYPE_NAME).getRecordTypeId();
    public final static Integer MINS_BETWEEN_RUNS = Integer.valueOf(System.Label.MINS_BETWEEN_RUNS);
    public final static String CURRENT_USER_ID = UserInfo.getUserId();
    public final static Id SF_RECORDTYPE_ID = System.Label.SupportForce_RecordType_Id;
    public final static Id INBOUND_USER_ID = System.Label.SPT_InboundUserId;
    public final static Id OUTBOUND_USER_ID = System.Label.SPT_OutboundUserId;
    public final static List<String> SF_CONTACTS_RECORD_TYPE_ID = System.Label.SF_ContactsRecordTypeId.split(',');
    public final static List<String> SPT_EMPLOYEE_RECORD_TYPE_ID = System.Label.SPT_EmployeeRecordTypeId.split(',');
    public final static String SF_TRUST_APPLICATION_QUEUE_ID = String.valueOf(System.Label.SF_TrustApplicationQueueId);
    public final static String SF_TRUST_ENTERPRISE_QUEUE_ID = String.valueOf(System.Label.SF_TrustEnterpriseQueueId);
    public final static String SF_TRUST_MERGERS_QUEUE_ID = String.valueOf(System.Label.SF_TrustMergersQueueId);
    public final static String TRUST_APPLICATION_SECURITY_ASSURANCE ='00G3A000004eDCp';
}
public with sharing class CSIRT_caseCache {
    
        private static boolean recordTypeCache = false;
        private static boolean incidentRT = false;
        private static boolean investigationRT = false;
        private static List<RecordType> EMailRT = new List<RecordType>(); 
        private static List<RecordType> incidentRtId = new List<RecordType>(); 
        private static List<RecordType> investigationRtId = new List<RecordType>(); 
        
        public static List<RecordType> EmailRtId()
        {
                if(recordTypeCache) return EmailRT;
                CacheCaseInfo();
                return EmailRT;
        }
        
        private static void CacheCaseInfo()
        {
                if(recordTypeCache) return;
                List<RecordType> rts = [
                SELECT Id, developerName 
                FROM RecordType 
                WHERE DeveloperName='CSIRT_Email' and SobjectType='Case'];
                EmailRT = rts;
                recordTypeCache = true;
        }
        
        public static List<RecordType> Incident()
        {
                if(incidentRt) return incidentRtId;
                incidentCache();
                return incidentRtId;
        }
        
                public static List<RecordType> Investigation()
        {
                if(incidentRt) return investigationRtId;
                investigationCache();
                return investigationRtId;
        }
        
        private static void incidentCache()
        {
                if(incidentRT) return;
                List<RecordType> inc_rts = [
                SELECT Id, developerName 
                FROM RecordType 
                WHERE DeveloperName='CSIRT_Internal_Security_Incident' and SobjectType='Case'];
                incidentRtId = inc_rts;
                incidentRT = true;
        }
        
                private static void investigationCache()
        {
                if(investigationRT) return;
                List<RecordType> inv_rts = [
                SELECT Id, developerName 
                FROM RecordType 
                WHERE DeveloperName='D_R_Investigation' and SobjectType='Case'];
                investigationRtId = inv_rts;
                investigationRT = true;
        }
}
public with sharing class HRI_AppConstants{

	public static final Map<String, String> SF_PRIORITY = new Map<String, String>{'P0' => '1 - Business Stopping', 
                                                                                  'P1' => '1 - Business Stopping', 
                                                                                  'P2' => '2 - Urgent',
                                                                                  'P3' => '3 - Standard',  
                                                                                  'P4' => '3 - Standard'};

    public static final Map<String, String> GUS_PRIORITY = new Map<String, String>{'P0' => '1', 
                                                                                   'P1' => '1',
                                                                                   'P2' => '2',
                                                                                   'P3' => '3', 
                                                                                   'P4' => '4'};

    public static final String GUS_FOUND_IN_BUILD_NA_ID = 'a06T0000001Vew1IAC';

    public static final String SUPPORTFORCE_CASE = 'Supportforce Case';

    public static final String GUS_BUG = 'Gus Bug';

}
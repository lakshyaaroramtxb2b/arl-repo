public class CDI_SyncExternalGusUser implements Database.Batchable<sObject>,Database.Stateful {
	public DateTime starttime=DateTime.now();
    public DateTime lastmodifieddate;
    public Integer successRowCount = 0;
    public Integer errorRowCount = 0;
    public String errors='';
 
       public Database.QueryLocator start(Database.BatchableContext bc) {
        List<GusUser__x> gususers=[Select LastModifiedDate__c From GusUser__x  ORDER BY LastModifiedDate__c DESC limit 1];
        lastmodifieddate=gususers[0].LastModifiedDate__c;
          System.debug(lastmodifieddate);
        List<CDI_Batch_Import__c>  batchimports=[Select ExternalUserLastModifiedTime__c from CDI_Batch_Import__c Where Object_Type__c='GUS User' ORDER BY ExternalUserLastModifiedTime__c DESC limit 1];
         String query;
           if(batchimports.size()>0 && batchimports[0].ExternalUserLastModifiedTime__c !=null  && lastmodifieddate>batchimports[0].ExternalUserLastModifiedTime__c)
         {
             DateTime externaldatetime=batchimports[0].ExternalUserLastModifiedTime__c;
             
             query='Select ExternalId,Name__c,IsActive__c,LastModifiedDate__c,Email__c From GusUser__x where ' + 'LastModifiedDate__c > '+externaldatetime.formatGmt('yyyy-MM-dd\'T\'HH:mm:ss.SSSZ')+'';
                 
         }
           else
           {
               query='Select ExternalId,Name__c,IsActive__c,LastModifiedDate__c,Email__c From GusUser__x';
           }
           
        return Database.getQueryLocator(query);
       }
            public void execute(Database.BatchableContext bc, List<GusUser__x> externalgususers){
           List<CDI_GUS_User__c> internalgususers=new List<CDI_GUS_User__c>();
                
          	for(GusUser__x externalgususer:externalgususers)
        	{
            CDI_GUS_User__c internalgususer= new CDI_GUS_User__c();
            internalgususer.Name=externalgususer.Name__c;
            internalgususer.Email_Id__c=externalgususer.Email__c;
            internalgususer.External_Id__c=externalgususer.Email__c;
            If(externalgususer.IsActive__c== True)
            {
                internalgususer.IsDeleted__c=False;
            }
            else
            {
                internalgususer.IsDeleted__c=True;
            }
            internalgususer.Source_Gus_User_Code__c=externalgususer.ExternalId;
            internalgususer.Source_System_Name__c='Gus';
            Internalgususers.add(internalgususer);
           
        }
                 
            Schema.SObjectField externalIddata=CDI_GUS_User__c.Fields.External_Id__c;
            Database.UpsertResult[] results= Database.upsert(Internalgususers,externalIddata,false);
                 for (Integer j = 0; j < results.size(); j++) {
                    if (!results[j].isSuccess())
                    {
                        errorRowCount=errorRowCount + 1;
                        Database.Error error=results.get(j).getErrors().get(0);
                        errors=error.getMessage();
                
                        
                    } 
                    else
                    {
                        successRowCount=successRowCount + 1;    
                    }
                }
                System.debug('Sucess Row Count'+successRowCount);
                System.debug(errors);
            }
            public void finish(Database.BatchableContext bc){
                System.debug('Sucess Row Count Inside Finish Method '+successRowCount);
            List<GusUser__x> gususersdata=[Select LastModifiedDate__c From GusUser__x  ORDER BY LastModifiedDate__c DESC limit 1];
            CDI_Batch_Import__c batchImport = new CDI_Batch_Import__c();
           batchImport.Start_Time__c=starttime;
           batchImport.End_Time__c=DateTime.now();
           batchImport.Object_Type__c='GUS User';
           batchImport.Rows_Failed__c=errorRowCount;
           batchImport.Rows_Succeeded__c=successRowCount;
           batchImport.Errors__c=errors;
           batchImport.ExternalUserLastModifiedTime__c=gususersdata[0].LastModifiedDate__c;
           System.debug(gususersdata[0].LastModifiedDate__c);
           System.debug('Finish '+batchImport.ExternalUserLastModifiedTime__c);
            insert batchImport;
            }
    		
    }
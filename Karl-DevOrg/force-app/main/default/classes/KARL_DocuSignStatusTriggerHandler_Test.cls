@isTest
public with sharing class KARL_DocuSignStatusTriggerHandler_Test {
    /**
* @author Swarnima S Mandhata
* @email swarnima.singh@mtxb2b.com
* @description On Insert Update call.
*/
    @testSetup
    private static void testSetup(){
        User opsAdminUser = ARL_TestDataFactory.createOpsAdmin();
        System.runAs(opsAdminUser){
            /**
* Audit Cycle Setup
*/
            // Create shell audit cycle
            Audit_Cycle__c auditCycle = ARL_TestDataFactory.createAuditCycle();
            insert auditCycle;
            
            /**
* Issue Creation
*/
            KARL_Issue_Tracker__c issue = ARL_TestDataFactory.createAuditIssue(auditCycle.Id);
            insert issue;
            
            KARL_Audit_Scope_Master_Data__c auditScope = ARL_TestDataFactory.createAuditScope();
            insert auditScope;
            KARL_Audit_Firm__c auditFirm = ARL_TestDataFactory.createAuditFirm('Test Audit Firm');
            insert auditFirm;
            KARL_Audit_Scope_Reports__c auditScopeReport = ARL_TestDataFactory.createAuditScopeReport(auditScope.Id,auditFirm.Id);
            insert auditScopeReport;
            KARL_Cycle_Audit_Report_Items__c cycleAuditReporItem = ARL_TestDataFactory.createCycleAuditScopeReport(auditScopeReport.Id,auditCycle.Id);
            cycleAuditReporItem.Scope__c = 'SS';
            cycleAuditReporItem.Status__c = 'Pending';
            cycleAuditReporItem.Auditor_Readiness_Confirmation__c = true;
            cycleAuditReporItem.Management_Responses_Approved__c = true;
            cycleAuditReporItem.Audit_Report_Final_Draft_Confirmed__c = true;
            cycleAuditReporItem.Approved_by_Legal__c = true;
            cycleAuditReporItem.Audit_Letter_Temps_Formatted__c = true;
            cycleAuditReporItem.Basis_of_Assertion_BOA__c = true;
            insert cycleAuditReporItem;            
        }
    }
    /**
* @author Swarnima S Mandhata
* @email swarnima.singh@mtxb2b.com
* @description On Insert call.
*/
    @isTest
    private static void statusChangeTest1(){
        List<User> opsAdminUserList = ARL_TestDataFactory.fetchUser('abc@b.com.d');
        if(!opsAdminUserList.isEmpty()){
            System.runAs(opsAdminUserList[0]){
                KARL_Cycle_Audit_Report_Items__c reportItem = [SELECT Id,Status__c FROM KARL_Cycle_Audit_Report_Items__c LIMIT 1];
                dfsle__EnvelopeStatus__c envelopeRec = new dfsle__EnvelopeStatus__c();
                envelopeRec.dfsle__Status__c = 'Sent';
                envelopeRec.dfsle__SourceId__c = reportItem.id;
                test.startTest();
                insert envelopeRec;
                test.stopTest();
                
                KARL_Cycle_Audit_Report_Items__c reportItem1 = [SELECT Id,Status__c FROM KARL_Cycle_Audit_Report_Items__c LIMIT 1];
                System.assertEquals('In Progress with Docusign', reportItem1.Status__c);
            }
        }
        
    }
        /**
* @author Swarnima S Mandhata
* @email swarnima.singh@mtxb2b.com
* @description On update call.
*/
    @isTest
    private static void statusChangeTest2(){
        List<User> opsAdminUserList = ARL_TestDataFactory.fetchUser('abc@b.com.d');
        if(!opsAdminUserList.isEmpty()){
            System.runAs(opsAdminUserList[0]){
                KARL_Cycle_Audit_Report_Items__c reportItem = [SELECT Id,Status__c FROM KARL_Cycle_Audit_Report_Items__c LIMIT 1];
                dfsle__EnvelopeStatus__c envelopeRec = new dfsle__EnvelopeStatus__c();
                envelopeRec.dfsle__Status__c = 'Sent';
                envelopeRec.dfsle__SourceId__c = reportItem.id;
                insert envelopeRec;
                
                test.startTest();
                envelopeRec.dfsle__Status__c = 'Completed';
                update envelopeRec;
                Test.stopTest();
                
                KARL_Cycle_Audit_Report_Items__c reportItem1 = [SELECT Id,Status__c FROM KARL_Cycle_Audit_Report_Items__c LIMIT 1];
                System.assertEquals('DocuSign Completed', reportItem1.Status__c);
            }
        }
        
    }
}
public class Squawker_ViewRuleController {
    public SquawkerRule__c SquawkerRule {get; set;}
    
    public List<SelectOption> logicFeildsOptions = new List<SelectOption>();
    public String objectToFollow {get;set;}
    
    //logic rules
    public String ruleOne_Feild {get;set;}
    public String ruleOne_Operation {get;set;}
    public String ruleOne_Value {get;set;}
    public String ruleTwo_Feild {get;set;}
    public String ruleTwo_Operation {get;set;}
    public String ruleTwo_Value {get;set;}
    public String ruleThree_Feild {get;set;}
    public String ruleThree_Operation {get;set;}
    public String ruleThree_Value {get;set;}
    
    //Our newly created rule object to be inserted into the DB
    private Map<String, Schema.SObjectType> schemaMap;

    //Groups that will get notfications. SFDC public groups
    public SelectOption[] selectedGroups { get; set; }
    public SelectOption[] allGroups { get; set; }
    
    public Squawker_ViewRuleController() {
        SquawkerRule = [Select Id, Name, Description__c, ObjectToWatch__c, ObjectToWatchFriendly__c, 
                	AlertLogic__c, GroupsToNotify__c, Last_Edit_Date__c, LastModifiedBy.Name, Groups_To_Notify_Friendly__c 
            		FROM SquawkerRule__c
                    WHERE Id = :ApexPages.currentPage().getParameters().get('id')];
    }

    public SquawkerRule__c getSquawkerRule() {
        return SquawkerRule;
    }

    public PageReference save() {
        update SquawkerRule;
        
        return null;
    }
}
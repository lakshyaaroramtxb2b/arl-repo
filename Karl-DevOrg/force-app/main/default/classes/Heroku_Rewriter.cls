/*

    Developer Name: Jorge L Caceres - jcaceres@salesforce.com | February 2019
    
    Description: Heroku Abuse Site UrlRewriter class specifically written to handle paths in the url
                 to pass them through to the processing controller otherwise salesforce cannot route 
                 any paths after the community name /CASE and the page name /CcaseId

*/

global class Heroku_Rewriter implements Site.UrlRewriter {
            
    private final String CASE_PAGE = '/C';

    global PageReference mapRequestUrl(PageReference myFriendlyUrl){
        
        String friendlyUrl = myFriendlyUrl.getUrl();

        if(friendlyUrl.contains(CASE_PAGE)){
            pageReference newPage = Page.HerokuCase;
            newpage.getParameters().put('id', friendlyUrl.substringAfter(CASE_PAGE));
            newPage.setRedirect(true);
            
            return newPage;
        
        } else return null;
   
    }
    
    global PageReference[] generateUrlFor(PageReference[]
         yourSalesforceUrls) {return null;}

                   
}
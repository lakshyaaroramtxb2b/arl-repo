//YOU MUST READ
//Need to start service - only once
//NB cannot edit calss while running so you need to kill service from setup / monitor / schedualed jobs
//
//Code to run from intermendiate window develoepr console
//
////TO test functionaility e.g. run service 
//OLD DO NOT USE - new SquawkService().ScanService();
//SquawkService.ScanService();
//
////TO setup service i.e. start it
//new SquawkService().schedualService();
//SquawkService.schedualService();
//
//NB in the future might need to convert code to batch apex if we hit govner limits etc

global class SquawkService implements Schedulable
{  
    static List<SquawkSettings__c> squawkSettings;
    static DateTime lastScanTime;
    static Map<Id, String[]> notifyAddresses = new Map<Id, String[]>();
    static OrgWideEmailAddress owea;
    
    global void execute(SchedulableContext SC)
    {
      //DO Work
      ScanService();
      
      // This section of code will schedule the next execution 1 minute from now
      schedualService();
      
      //remove finished job
      system.abortJob(sc.getTriggerId());
    }
    
    global void schedualService()
    {
      datetime nextScheduleTime = system.now().addMinutes(1);
      string minute = string.valueof(nextScheduleTime.minute());
      string second = string.valueof(nextScheduleTime.second ());
      string cronvalue = second+' '+minute+' * * * ?' ;
      string jobName = 'squawkerService ' +
      nextScheduleTime.format('hh:mm');
     
      SquawkService p = new SquawkService();
      system.schedule(jobName, cronvalue , p);
     
      // this section of code will abort the current schedule job
      //system.abortJob(sc.getTriggerId());
    }
    
    global void TestRun()
    {
        System.debug('TestRun()');
    }
    
    @future
    global static void ScanService()
    {
        System.debug('ScanService()');

        notifyAddresses.clear();
        
        lastScanTime = getLastScanTime();
        
        List<SquawkerRule__c> squawkerRules = [SELECT Id, ObjectToWatch__c, ObjectToWatchFriendly__c, RuleLogic__c, GroupsToNotify__c FROM SquawkerRule__c WHERE active__c = true];

 		//Id is of the Object that FeelItems are for
 		//MessageOptions include the contact list and the Feeditem data
		Map<Id, MessageOptions> results = GetMatchedObjects(squawkerRules);
        
        List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
        
        for (Id key : results.keySet())
        {
            emails.addAll( generateEmailNotfications(results.get(key).objectMessages, 
                                                     results.get(key).emailContacts,
                                                     results.get(key).ruleIds));
        }
        
        Messaging.sendEmail(emails, false);
        
        upDateLastScanTime();
        
        System.debug('ScanService Complete');
    }
    
    global static DateTime getLastScanTime()
    {
        squawkSettings = [SELECT lastScanTime__c FROM SquawkSettings__c WHERE Name = 'production'];
        
        if(squawkSettings.size() == 0)
        {
            SquawkSettings__c newSetting = new SquawkSettings__c();
            
            newSetting.lastScanTime__c = DateTime.now().addMinutes(-5);
            newSetting.Name = 'production';
            
            insert newSetting;
            
            return newSetting.lastScanTime__c;
        }
        
        return squawkSettings[0].lastScanTime__c;
    }
    
    global static void upDateLastScanTime()
    {
        squawkSettings = [SELECT lastScanTime__c FROM SquawkSettings__c WHERE Name = 'production'];
        
        if(squawkSettings.size() == 0)
        {
            SquawkSettings__c newSetting = new SquawkSettings__c();
            
            newSetting.lastScanTime__c = DateTime.now();
            newSetting.Name = 'production';
            
            update newSetting;
        }
        
        squawkSettings[0].lastScanTime__c = DateTime.now();
        
        update squawkSettings[0];
    }
    
    global static List<Id> GetMatchedWithRecentChatter(List<Id> matchedObjects)
    {
        List<Id> chatterMatchedObjs = new List<Id>();
        
        List<FeedItem> items = [SELECT Id From FeedItem WHERE ParentID IN : matchedObjects AND CreatedDate >: lastScanTime];
        
        for(FeedItem item : items)
            chatterMatchedObjs.add(item.Id);
        
        return chatterMatchedObjs;
    }
    
    global class MatchedResult
    {
        public FeedItem msg;
        //to save on memory usage we might be able to remove
        public SquawkerRule__c rule;
    }
    
    global class MessageOptions
    {
        public Set<FeedItem> objectMessages;
        public Set<String> emailContacts;
        public Set<Id> ruleIds; //rules that caused trigger
    }
    
    global static Set<String> getRuleUniqueContacts(SquawkerRule__c rule)
    {
        Set<String> uniqueContacts = new Set<String>();
        
        for(String groupId : rule.GroupsToNotify__c.split(','))
        {
            uniqueContacts.addAll(getEmailAddresses(groupId));
        }
        
        return uniqueContacts;
    }
    
    //Id is of the Object that FeelItems are for
 	//MessageOptions include the contact list and the Feeditem data
    global static Map<Id, MessageOptions> GetMatchedObjects(List<SquawkerRule__c> squawkerRules)
    {
        Map<Id, MessageOptions> results = new Map<Id, MessageOptions>();
        
        for(SquawkerRule__c rule : squawkerRules)
        {
            System.debug('Processing rule: ' + rule.Id);
            
            Set<String> ruleContactList = getRuleUniqueContacts(rule);
            
            if(ruleContactList.size() > 0)
            {
                List<string> ruleNotifyGroups = rule.GroupsToNotify__c.split(',');
                List<SquawkRuleLogic> ruleLogic = (List<SquawkRuleLogic>)JSON.deserialize(rule.RuleLogic__c, List<SquawkRuleLogic>.class);
                
                String queryString = new Squawker_QueryGenerator().createQueryString(ruleLogic, rule, lastScanTime);
       
                sobject [] records = Database.query(queryString);
                
                for( sobject obj : records)
                {
                    FeedItem feedItem = (FeedItem)obj;
                    
                    if(results.containsKey(feedItem.ParentId))
                    {
                        results.get(feedItem.ParentId).emailContacts.addAll(ruleContactList);
                        results.get(feedItem.ParentId).objectMessages.add(feedItem);
                        results.get(feedItem.ParentId).ruleIds.add(rule.Id);
                    }
                    else
                    {
                        MessageOptions msgOpt = new MessageOptions();
                        
                    	msgOpt.emailContacts = new Set<String>();
                        msgOpt.emailContacts.addAll(ruleContactList);
                        
                        msgOpt.objectMessages = new Set<FeedItem>();
                        msgOpt.objectMessages.add(feedItem);
                        
                        msgOpt.ruleIds = new Set<Id>();
                        msgOpt.ruleIds.add(rule.Id);
                              
                        results.put(feedItem.ParentId, msgOpt);
                    }
                }
        	}
            else
            {
                System.debug('Rule has no contacts to notify. No need to process rule : ' + rule.Id);
            }
        }
        
        System.debug('GetMatchedObjects()');
        System.debug(results);
        
        return results;
    }
    
    private static List<String> getEmailAddresses(Id GroupId) {
        
        List<String> idList = new List<String>();
        Set<String> mailToAddresses = new Set<String>();
        
        Group g = [SELECT (select userOrGroupId from groupMembers) FROM group WHERE Id =: GroupId];
        
        for (GroupMember gm : g.groupMembers) {
        	idList.add(gm.userOrGroupId);
        }
        
        User[] usr = [SELECT email FROM user WHERE id IN :idList];
        
        for(User u : usr) {
            if(!mailToAddresses.contains(u.email))
        		mailToAddresses.add(u.email);
        }
        
        return convertToList(mailToAddresses);
    }
    
    public static list<string> convertToList(set<string> setToConvert) {
        list<string> lstString = new list<string>();
        lstString.addAll(setToConvert);
        return lstString;
    }
    
    global static List<Messaging.SingleEmailMessage> generateEmailNotfications(Set<FeedItem> feedItems, Set<String> toAddresses, Set<Id> ruleIds)
    {
        //The current arcutecture would support sending per object email update
        //However to work with current CloudSwarm per FeedItem we use for loop
        //To send as one email then for loop on email body not the email message
        
        List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
        
        for(FeedItem feedItem : feedItems)
        {
        	Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
            
            String baseUrl = System.URL.getSalesforceBaseUrl().toExternalForm();
      
            String plainText = feedItem.CreatedBy.Name + ' wrote: ' + feedItem.Body;
    		
            Case c = [select Id, Subject, CaseNumber, RecordType.Name from Case WHERE Id = :feedItem.ParentId LIMIT 1]; // added to get Subject,CaseNumber,RecordType.Name - AndyW

            String htmlText = '';      
            // htmlText += '<img src="http://oi59.tinypic.com/24l8nxk.jpg"/>'; // removed icon - AndyW
            htmlText += '<br/>';
            htmlText += '<h2 style=""> #' +  c.CaseNumber + ' : ' + c.Subject + '</h2>';
            htmlText += '<br/>';
            htmlText += '<h3 style="">' + feedItem.CreatedBy.Name  + ' wrote : </h3> <p style=""><i> ' + feedItem.Body + ' </i></p>';
            htmlText += '<br/>';
            htmlText += '<br/>';
            htmlText += '<a href="' + baseUrl + '/' + feedItem.ParentId + '">view comments</a>';
  
            // Assign the addresses for the To and CC lists to the mail object.
            email.setBccAddresses(convertToList(toAddresses));
            
            //email.setSenderDisplayName('Salesforce');
            // email.setSubject(feedItem.CreatedBy.Name + ' posted on ' + feedItem.ParentId.getSObjectType().getDescribe().getName() + ' ' + feedItem.ParentId); 
            email.setSubject(feedItem.CreatedBy.Name + ' posted on ' + c.RecordType.Name + ' #' + c.CaseNumber); // changed to use c.RecordType.Name and c.CaseNumber - AndyW
            email.setPlainTextBody(plainText);
            email.setHtmlBody(htmlText);
            
            if(owea == null)
            	owea = [select Id from OrgWideEmailAddress where Address = 'security@salesforce.com'];
            
            email.setOrgWideEmailAddressId(owea.Id);

            //string senderAddress = 'security@salesforce.com';
            //email.setReplyTo(senderAddress);
			//email.setSenderDisplayName('Security Org');

            emails.add(email);
        }    
        
        return emails;
    }
}
@isTest
public class ESA_ReminderScheduler_Test {
    
    @testSetup
    static void testSetup(){
        
        
        date today = system.today();
        //create account
        Account account1 = new Account (Name = 'Test account');
        insert account1;
        //create contact
        Contact contact1 = new Contact (Lastname = 'test contact',  Email = 'matias.granata@salesforce.com', AccountId = account1.Id);
        insert contact1;
        //create case r1
        Case case1 = new Case (ContactId = contact1.Id, Subject  = 'Test to test 1', RecordTypeId  = '0123A000000Ac6AQAS');
        insert case1;
        //create case r2
        Case case2 = new Case (ContactId = contact1.Id, Subject  = 'Test to test 2', RecordTypeId  = '0123A000000Ac6AQAS');
        insert case2;
        //create case r3
        Case case3 = new Case (ContactId = contact1.Id, Subject  = 'Test to test 3', RecordTypeId  = '0123A000000Ac6AQAS');
        insert case3;
        //create case r4
        Case case4 = new Case (ContactId = contact1.Id, Subject  = 'Test to test 4', RecordTypeId  = '0123A000000Ac6AQAS');
        insert case4;
        //create case r5
        Case case5 = new Case (ContactId = contact1.Id, Subject  = 'Test to test 5', RecordTypeId  = '0123A000000Ac6AQAS');
        insert case5;
        //create case r6
        Case case6 = new Case (ContactId = contact1.Id, Subject  = 'Test to test 6', RecordTypeId  = '0123A000000Ac6AQAS');
        insert case6;
        //create case r7
        Case case7 = new Case (ContactId = contact1.Id, Subject  = 'Test to test 7', RecordTypeId  = '0123A000000Ac6AQAS');
        insert case7;
        
    }
    
    @isTest
    static void ReminderSchedulerTest(){
        date today = system.today();
        List <Case> caseList = [SELECT id, Last_Reminder__c, Reminder1__c, subject, status FROM Case WHERE Subject  LIKE 'Test to test%'];
        Test.startTest();
        for (Case cs: caseList){
            cs.status = 'Waiting for Customer Response';   
        }
        update caseList;
        for (Case cs: caseList){
            
            if (cs.Subject == 'Test to test 1'){
                
                cs.Reminder1__c = today;
            }
            else if (cs.Subject == 'Test to test 2'){
                
                cs.Reminder1__c = today -1;
                cs.Last_Reminder__c = today - 1;
                cs.Reminder2__c = today;
                cs.Reminder2_Email_Template__c = '2nd EntSec Reminder';
            }
            else if (cs.Subject == 'Test to test 3'){
                
                cs.Reminder1__c = today -2;
                cs.Reminder2__c = today - 1;
                cs.Last_Reminder__c = today - 1;
                cs.Reminder3__c = today;
                cs.Reminder3_Email_Template__c = '3rd EntSec Reminder';
            }
            else if (cs.Subject == 'Test to test 4'){
                
                cs.Reminder1__c = today -3;
                cs.Reminder2__c = today - 2;
                cs.Reminder3__c = today - 1;
                cs.Last_Reminder__c = today - 1;
                cs.Reminder4__c = today;
                cs.Reminder4_Email_Template__c = 'Final EntSec Reminder';
            }
            else if (cs.Subject == 'Test to test 5'){
                
                cs.Send_Close__c = true;
                cs.Closing_Date__c = today;
                cs.Last_Reminder__c = today - 1;
                cs.Closing_template__c = 'Closing Reminder EntSec';
            }
            else if (cs.Subject == 'Test to test 6'){
                
                cs.Reminder1__c = today -4;
                cs.Reminder2__c = today - 3;
                cs.Reminder3__c = today - 2;
                cs.Last_Reminder__c = today - 2;
                cs.Reminder4__c = today - 1;
                cs.Reminder4_Email_Template__c = 'Final EntSec Reminder';

            }
            else if (cs.Subject == 'Test to test 7'){
                
                cs.Reminder1__c = today -3;
                cs.Reminder2__c = today - 2;
                cs.Reminder3__c = today - 1;
                cs.Last_Reminder__c = today - 1;
                cs.Reminder4__c = today + 1;
                cs.Reminder4_Email_Template__c = 'Final EntSec Reminder';

            }
            
        }
        update caseList;
        
        
		ESA_ReminderScheduler rs = new ESA_ReminderScheduler();
		Database.executeBatch(rs); 
        //Integer invocations = Limits.getEmailInvocations();
        //System.assertequals(5, invocations);
        Test.stopTest();
        
        Case cs1 = [SELECT id, Last_Reminder__c, status FROM Case WHERE Subject  ='Test to test 1' LIMIT 1];
        system.assertequals(system.today(),cs1.Last_reminder__c);
        Case cs2 = [SELECT id, Last_Reminder__c, status FROM Case WHERE Subject  ='Test to test 2' LIMIT 1];
        system.assertequals(system.today(),cs2.Last_reminder__c);
        Case cs3 = [SELECT id, Last_Reminder__c, status FROM Case WHERE Subject  ='Test to test 3' LIMIT 1];
        system.assertequals(system.today(),cs3.Last_reminder__c);
        Case cs4 = [SELECT id, Last_Reminder__c, status FROM Case WHERE Subject  ='Test to test 4' LIMIT 1];
        system.assertequals(system.today(),cs4.Last_reminder__c);
        Case cs5 = [SELECT id, Last_Reminder__c, status FROM Case WHERE Subject  ='Test to test 5' LIMIT 1];
        system.assertequals(system.today(),cs5.Last_reminder__c);
        Case cs6 = [SELECT id, Last_Reminder__c, status FROM Case WHERE Subject  ='Test to test 6' LIMIT 1];
        system.assertequals(system.today(),cs6.Last_reminder__c);
        Case cs7 = [SELECT id, Last_Reminder__c, status FROM Case WHERE Subject  ='Test to test 7' LIMIT 1];
        system.assertequals(system.today() - 1,cs7.Last_reminder__c);
        Integer email =[SELECT count() FROM EmailMessage ];
        
        system.assertequals(6, email);
        
        
    }
    
    //add another method to check the trigger
    

}
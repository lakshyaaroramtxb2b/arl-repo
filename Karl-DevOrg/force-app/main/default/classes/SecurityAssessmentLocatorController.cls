public class SecurityAssessmentLocatorController {
	private String SANumber;
    private String message;
   	
    public SecurityAssessmentLocatorController() {
        SANumber = ApexPages.currentPage().getParameters().get('SAnumber');
    }
    	
    public PageReference locateSAandRedirect() {
        if (String.isBlank(SANumber)){
            setMessage('SANumber parameter required');
            return null;
        }
        if (!SANumber.startsWith('SA-') && SANumber.length() != 6){
            setMessage('Check the SA number format. Should be SA-0XXXXX');
            return null;
        }
        SANumber = formatSANumber(SANumber);
        List<ADM_Security_Assessment_c__x> sa = [SELECT ExternalId FROM ADM_Security_Assessment_c__x WHERE Name__c =:SANumber LIMIT 1];
        if (sa.isEmpty()){
            setMessage('Could not find Security Assessment record with this number: ' + SANumber);
            return null;
        }
     	
        return new PageReference('https://gus.my.salesforce.com/' + sa.get(0).ExternalId);
    }
    
    private String formatSANumber (String num){
        if (!num.startsWith('SA-')){
            num = 'SA-' + num;
        }
        return num;
    }

    public String getMessage() {
        return message;
    }
    
    public void setMessage(String newMessage) {
        message = newMessage;
    }
}
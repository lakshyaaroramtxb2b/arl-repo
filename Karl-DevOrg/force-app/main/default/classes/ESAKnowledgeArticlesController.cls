public class ESAKnowledgeArticlesController {
    private final ESA_Entity__c esaEntity;
    
    public ESAKnowledgeArticlesController() {
        String entityCode = ApexPages.currentPage().getParameters().get('entityCode');
        if (!String.isBlank(entityCode) && ESAController.checkCommunityHasAccessToEntity(entityCode)) {
            esaEntity = [SELECT Knowledge_Data_Category__c FROM ESA_Entity__c WHERE EntityCode__c = :entityCode];
        }
    }
    
    public String getKnowledgeDataCategory() {
        if (esaEntity == null) {
            return null;
        }
        return esaEntity.Knowledge_Data_Category__c;
    }

}
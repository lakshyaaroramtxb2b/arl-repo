/*created by Swarnima singh Mandhata*/
@isTest
public class PRT_PortfolioIntegrationBatchTest {
    @testSetup
    public static void testSetup(){
        List<User> userList = new List<User>();
        userList = PRT_TestDataFactory.createUsers(3,'System Administrator',true);
    }

    static testMethod void testExecuteMethod(){
        List<User> userList = new List<User>([SELECT Id FROM USER WHERE Email Like 'PRTUser%']);
        List<Portfolio__c> portfolioDataList = new List<Portfolio__c>();
        
        Portfolio__c portfolioRecord = new Portfolio__c();
        portfolioRecord.Portfolio_Active__c = true;
        portfolioRecord.Portfolio_Goals__c = 'Test security Portfolio';
        portfolioRecord.Portfolio_Health__c = 'On Track';
        portfolioRecord.Portfolio_Manager__c = userList[0].id;
        portfolioRecord.Portfolio_Owner__c = userList[0].id;
        portfolioRecord.GUS_Portfolio__c = '';
        portfolioDataList.add(portfolioRecord);
        insert portfolioDataList;
        
        List<PPM_Portfolio_c__x> portfolioList = new List<PPM_Portfolio_c__x>();
        PPM_Portfolio_c__x gusPortfolio = new PPM_Portfolio_c__x();
        gusPortfolio.Portfolio_Goals_c__c = 'Test portFolio goALS';
        gusPortfolio.Portfolio_Health_c__c = 'On Track';
        gusPortfolio.Name__c = 'Test Portgolio Name1';
        gusPortfolio.Portfolio_Manager_c__c = userList[0].id;
        gusPortfolio.Portfolio_Owner_c__c = userList[0].id;
        gusPortfolio.Portfolio_Summary_c__c = 'test summary portfolio';
        portfolioList.add(gusPortfolio);
        
         PRT_PortfolioIntegrationBatch.mockallGusPortfolioList.addAll(portfolioList);
        
        Test.startTest();
            PRT_PortfolioIntegrationBatch pib = new PRT_PortfolioIntegrationBatch();
            Id batchId = Database.executeBatch(pib);
        Test.stopTest();

    }
    
    static testMethod void testScheduled() {        
        List<User> userList = new List<User>([SELECT Id FROM USER WHERE Email Like 'PRTUser%']);
        List<Portfolio__c> portfolioDataList = new List<Portfolio__c>();
        
        Portfolio__c portfolioRecord = new Portfolio__c();
        portfolioRecord.Portfolio_Active__c = true;
        portfolioRecord.Portfolio_Goals__c = 'Test security Portfolio';
        portfolioRecord.Portfolio_Health__c = 'On Track';
        portfolioRecord.Portfolio_Manager__c = userList[0].id;
        portfolioRecord.Portfolio_Owner__c = userList[0].id;
        portfolioRecord.GUS_Portfolio__c = '';
        portfolioDataList.add(portfolioRecord);
        insert portfolioDataList;
        
        List<PPM_Portfolio_c__x> portfolioList = new List<PPM_Portfolio_c__x>();
        PPM_Portfolio_c__x gusPortfolio = new PPM_Portfolio_c__x();
        gusPortfolio.Portfolio_Goals_c__c = 'Test portFolio goALS';
        gusPortfolio.Portfolio_Health_c__c = 'On Track';
        gusPortfolio.Name__c = 'Test Portgolio Name1';
        gusPortfolio.Portfolio_Manager_c__c = userList[0].id;
        gusPortfolio.Portfolio_Owner_c__c = userList[0].id;
        gusPortfolio.Portfolio_Summary_c__c = 'test summary portfolio';
        portfolioList.add(gusPortfolio);
        
         PRT_PortfolioIntegrationBatch.mockallGusPortfolioList.addAll(portfolioList);
        
        test.starttest();
         PRT_PortfolioIntegrationBatch pib = new PRT_PortfolioIntegrationBatch();   
         String chron = '0 0 23 * * ?';        
         system.schedule('Portfolio Integration', chron, pib);
        test.stopTest();
    }
                                                 
}
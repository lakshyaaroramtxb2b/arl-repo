@isTest
private class NodeContainerTests {
     static testMethod void genericTests() {
        Flow__c flow = new Flow__c();
        insert flow;
        Node__c yesnode = new Node__c(Flow__c=flow.Id,
                         X__c=1,
                         Y__c=2);
        Node__c nonode  = new Node__c(Flow__c=flow.Id,
                         X__c=1,
                         Y__c=2);
        Node__c allnode = new Node__c(Flow__c=flow.Id,
                         X__c=1,
                         Y__c=2);
        insert yesnode;
        insert nonode;
        insert allnode;
        Id next;
        
        Map<String,Boolean> tfMap = new Map<String,Boolean>();
        Map<String,String> strMap = new Map<String,String>();
        tfMap.put('stuff',False);
        tfMap.put('blah',True);
        strMap.put('sstuff','afds');
        strMap.put('sblah','afds');
        Node__c a= new Node__c(
                         X__c=1,
                         Y__c=2);
        a.Type__c = 'Decision';
        a.DecisionAndOr__c = 'AND';
        a.YesNext__c = yesnode.Id;
        a.NoNext__c  = nonode.Id;
        a.Next__c    = allnode.Id;
        NodeContainer nc = new NodeContainer(a);
        
        System.assert(nc.isQuestion() == False);
        nc.n.Type__C = 'Question';
        System.assert(nc.isQuestion() == True);
        System.assert(nc.isFinal() == False);
        
        System.assert(nc.isDecision() == False);
        nc.n.Type__C = 'Decision';
        System.assert(nc.isDecision() == True);
        System.assert(nc.isFinal() == False);
        
        System.assert(nc.isUrl() == False);
        nc.n.Type__C = 'URL';
        System.assert(nc.isUrl() == True);
        System.assert(nc.isFinal() == True);
        
        System.assert(nc.isVariable() == False);
        nc.n.Type__C = 'Variable';
        System.assert(nc.isVariable() == True);
        System.assert(nc.isFinal() == False);
    }
    
    static testMethod void questionTest() {
        Flow__c flow = new Flow__c();
        insert flow;
        Node__c yesnode = new Node__c(Flow__c=flow.Id,
                         X__c=1,
                         Y__c=2);
        Node__c nonode  = new Node__c(Flow__c=flow.Id,
                         X__c=1,
                         Y__c=2);
        Node__c allnode = new Node__c(Flow__c=flow.Id,
                         X__c=1,
                         Y__c=2);
        insert yesnode;
        insert nonode;
        insert allnode;
        
        Map<String,Boolean> tfMap = new Map<String,Boolean>();
        Map<String,String> strMap = new Map<String,String>();
        tfMap.put('stuff',False);
        tfMap.put('blah',True);
        strMap.put('sstuff','afds');
        strMap.put('sblah','afds');
        Node__c a= new Node__c();
        a.Type__c = 'Question';
        a.Text__c = 'What is your name?';
        a.YesNext__c = yesnode.Id;
        a.NoNext__c  = nonode.Id;
        a.isYesNo__c = True;
        
        NodeContainer nc = new NodeContainer(a);
        System.assert(nc.getQuestion() == 'What is your name?');
        System.assert(nc.getAnswerOptions().size() == 2);
        System.assert(nc.getQuestionNext('Yes') == yesnode.Id);
        System.assert(nc.getQuestionNext('No') == nonode.Id);
    }
    static testMethod void decisionTest() {
        Flow__c flow = new Flow__c();
        insert flow;
        Node__c yesnode = new Node__c(Flow__c=flow.Id,
                         X__c=1,
                         Y__c=2);
        Node__c nonode  = new Node__c(Flow__c=flow.Id,
                         X__c=1,
                         Y__c=2);
        Node__c allnode = new Node__c(Flow__c=flow.Id,
                         X__c=1,
                         Y__c=2);
        insert yesnode;
        insert nonode;
        insert allnode;
        Id next;
        
        Map<String,Boolean> tfMap = new Map<String,Boolean>();
        Map<String,String> strMap = new Map<String,String>();
        tfMap.put('stuff',False);
        tfMap.put('blah',True);
        strMap.put('sstuff','afds');
        strMap.put('sblah','afds');
        strMap.put('istuff1','1');
        strMap.put('istuffnull',null);
        strMap.put('istuff3','3');
        Node__c a= new Node__c(
                         X__c=1,
                         Y__c=2);
        a.Type__c = 'Decision';
        a.DecisionAndOr__c = 'AND';
        a.YesNext__c = yesnode.Id;
        a.NoNext__c  = nonode.Id;
        a.Next__c    = allnode.Id;
        
        a.DecisionRules__c = 'sstuff =BAD "afds"\n';
        NodeContainer nc = new NodeContainer(a);
        try {
            nc.makeDecision(tfMap,strMap);
        }
        catch (FlowException e) {
            //correct
        }
        
        nc.n.DecisionRules__c = 'sstuff == "afds"\n';
        next = nc.makeDecision(tfMap,strMap);
        System.assert(next == yesnode.Id);
        
        nc.n.DecisionRules__c = 'sstuff == "aXXXfds"\n';
        next = nc.makeDecision(tfMap,strMap);
        System.assert(next == nonode.Id);
        
        nc.n.DecisionRules__c = 'sstuff ==   "afds"\n';
        next = nc.makeDecision(tfMap,strMap);
        System.assert(next == yesnode.Id);
        
        nc.n.DecisionRules__c = 'sstuff    == "afds"\n';
        next = nc.makeDecision(tfMap,strMap);
        System.assert(next == yesnode.Id);
        
        nc.n.DecisionRules__c = 'sstuff    == \'afds\'\n';
        next = nc.makeDecision(tfMap,strMap);
        System.assert(next == yesnode.Id);
        
        nc.n.DecisionRules__c = 'sstuff    == "afds"   \n';
        next = nc.makeDecision(tfMap,strMap);
        System.assert(next == yesnode.Id);
        
        a.DecisionAndOr__c = 'OR';
        nc.n.DecisionRules__c = 'sstuff    == "afds"';
        nc.n.DecisionRules__c += '\nasdflkj99 == 243k';
        next = nc.makeDecision(tfMap,strMap);
        System.assert(next == yesnode.Id);
        
        a.DecisionAndOr__c = 'OR';
        nc.n.DecisionRules__c = 'sstuff    == "afds"';
        nc.n.DecisionRules__c += '\nasdflkj1 == "243k"';
        next = nc.makeDecision(tfMap,strMap);
        System.assert(next == yesnode.Id);
        
        a.DecisionAndOr__c = 'OR';
        nc.n.DecisionRules__c = 'sstuff    == "afds"';
        nc.n.DecisionRules__c += '\nasdflkj2 ==   \'243k\'';
        next = nc.makeDecision(tfMap,strMap);
        System.assert(next == yesnode.Id);
        
        a.DecisionAndOr__c = 'OR';
        nc.n.DecisionRules__c = 'sstuff    == "XXXafds"';
        nc.n.DecisionRules__c += '\nasdflkj3 ==   \'243k\'';
        next = nc.makeDecision(tfMap,strMap);
        System.assert(next == nonode.Id);
        
        a.DecisionAndOr__c = 'OR';
        nc.n.DecisionRules__c = 'sstXXXuff    == "afds"';
        nc.n.DecisionRules__c += '\nasdflkj4 ==   \'243k\'';
        next = nc.makeDecision(tfMap,strMap);
        System.assert(next == nonode.Id);
        
        a.DecisionAndOr__c = 'OR';
        nc.n.DecisionRules__c = 'blah    == "afds"';
        nc.n.DecisionRules__c += '\nasdflkj5 ==   \'243k\'';
        next = nc.makeDecision(tfMap,strMap);
        System.assert(next == nonode.Id);
        
        a.DecisionAndOr__c = 'OR';
        nc.n.DecisionRules__c = 'istuff1    >= "1"';
        nc.n.DecisionRules__c += '\nasdflkj6 ==   \'243k\'';
        next = nc.makeDecision(tfMap,strMap);
        System.assert(next == yesnode.Id);
        
        a.DecisionAndOr__c = 'OR';
        nc.n.DecisionRules__c = 'istuff1    <= "1"';
        nc.n.DecisionRules__c += '\nasdflkj7 ==   \'243k\'';
        next = nc.makeDecision(tfMap,strMap);
        System.assert(next == yesnode.Id);
        
        a.DecisionAndOr__c = 'OR';
        nc.n.DecisionRules__c = 'istuff1    != "3"';
        nc.n.DecisionRules__c += '\nasdflkj8 ==   \'243k\'';
        next = nc.makeDecision(tfMap,strMap);
        System.assert(next == yesnode.Id);
        
        a.DecisionAndOr__c = 'OR';
        nc.n.DecisionRules__c = 'istuff1    >= "1"';
        next = nc.makeDecision(tfMap,strMap);
        System.assert(next == yesnode.Id);
        
        a.DecisionAndOr__c = 'AND';
        nc.n.DecisionRules__c = 'sstuff    == "afds"';
        nc.n.DecisionRules__c += '\nblah == TruE';
        next = nc.makeDecision(tfMap,strMap);
        System.assert(next == yesnode.Id);
        
        a.DecisionAndOr__c = 'AND';
        nc.n.DecisionRules__c = 'sstuff    == "afds"';
        nc.n.DecisionRules__c += '\nasdflkj9 == "243k"';
        next = nc.makeDecision(tfMap,strMap);
        System.assert(next == nonode.Id);
        
        a.DecisionAndOr__c = 'AND';
        nc.n.DecisionRules__c = 'sstuff    == "afds"';
        next = nc.makeDecision(tfMap,strMap);
        System.assert(next == yesnode.Id);
        
        a.DecisionAndOr__c = 'AND';
        nc.n.DecisionRules__c = 'sstuff    == "XXXafds"';
        nc.n.DecisionRules__c += '\nasdflkj11 ==   \'243k\'';
        next = nc.makeDecision(tfMap,strMap);
        System.assert(next == nonode.Id);
        
        a.DecisionAndOr__c = 'AND';
        nc.n.DecisionRules__c = 'sstuff    == "afds"';
        nc.n.DecisionRules__c += '\nblah == TruE';
        nc.n.DecisionRules__c += '\nstuff = afds';
        next = nc.makeDecision(tfMap,strMap);
        System.assert(next == yesnode.Id);
        
        a.DecisionAndOr__c = 'AND';
        nc.n.DecisionRules__c = 'istuff1    >= "1"';
        nc.n.DecisionRules__c += '\nasdflkj12 ==   \'243k\'';
        next = nc.makeDecision(tfMap,strMap);
        System.assert(next == nonode.Id);
        
        a.DecisionAndOr__c = 'AND';
        nc.n.DecisionRules__c = 'istuff1    <= "1"';
        nc.n.DecisionRules__c += '\nasdflkj13 ==   \'243k\'';
        next = nc.makeDecision(tfMap,strMap);
        System.assert(next == nonode.Id);
        
        a.DecisionAndOr__c = 'AND';
        nc.n.DecisionRules__c = 'istuff1    != "3"';
        nc.n.DecisionRules__c += '\nasdflkj14 ==   \'243k\'';
        next = nc.makeDecision(tfMap,strMap);
        System.assert(next == nonode.Id);
        
        a.DecisionAndOr__c = 'AND';
        nc.n.DecisionRules__c = 'istuff1    >= "1"';
        nc.n.DecisionRules__c = '\nistuff3    != "2"';
        next = nc.makeDecision(tfMap,strMap);
        System.assert(next == yesnode.Id);
        
    }
    static testMethod void variableTest() {
        Flow__c flow = new Flow__c();
        insert flow;
        Node__c allnode = new Node__c(Flow__c=flow.Id,
                         X__c=1,
                         Y__c=2);
        insert allnode;
        Id next;
        
        Map<String,Boolean> tfMap = new Map<String,Boolean>();
        Map<String,String> strMap = new Map<String,String>();
        tfMap.put('stuff',False);
        tfMap.put('blah',True);
        strMap.put('sstuff','afds');
        strMap.put('sblah','afds');
        Node__c a= new Node__c(
                         X__c=1,
                         Y__c=2);
        a.Type__c = 'Variable';
        a.Next__c    = allnode.Id;
        
        NodeContainer nc = new NodeContainer(a);
        
        nc.n.SetTrue__c = 'stuff';
        tfMap = nc.updateTfMap(tfMap);
        System.assert(tfMap.get('stuff') == True);
        
        nc.n.SetTrue__c = 'stuffXXX';
        tfMap = nc.updateTfMap(tfMap);
        System.assert(tfMap.get('stuffXXX') == True);
        
        nc.n.SetFalse__c = 'stuff';
        tfMap = nc.updateTfMap(tfMap);
        System.assert(tfMap.get('stuff') == False);
        
        nc.n.SetFalse__c = 'stuffXYZ';
        tfMap = nc.updateTfMap(tfMap);
        System.assert(tfMap.get('stuffXYZ') == False);
        
        nc.n.AppendTo__c = 'sstuff';
        nc.n.AppendTo__c += '\nqwerty';
        nc.n.AppendVal__c = 'blah';
        strMap = nc.updateStrMap(strMap);
        System.assert(strMap.get('sstuff') == 'afdsblah');
        System.assert(strMap.get('qwerty') == 'blah');
        
        nc.n.AppendTo__c = 'sstuffXYZ';
        nc.n.AppendVal__c = 'blah';
        strMap = nc.updateStrMap(strMap);
        System.assert(strMap.get('sstuffXYZ') == 'blah');
        
        next = nc.getVariableNext();
        System.assert(next == allnode.Id);
    }
    
    static testMethod void urlTest() {
        Flow__c flow = new Flow__c();
        insert flow;
        Node__c allnode = new Node__c(Flow__c=flow.Id,
                         X__c=1,
                         Y__c=2);
        insert allnode;
        Id next;
        
        Map<String,Boolean> tfMap = new Map<String,Boolean>();
        Map<String,String> strMap = new Map<String,String>();
        tfMap.put('stuff',False);
        tfMap.put('blah',True);
        strMap.put('sstuff','afds');
        strMap.put('sblah','afds');
        Node__c a= new Node__c(
                         X__c=1,
                         Y__c=2);
        a.Type__c = 'URL';
        a.URL__c = 'http://www.blah.com/?stuff=[stuff]&asdf=[  blah]&sblah=[ sblah ]';
        NodeContainer nc = new NodeContainer(a);
        String proper = 'http://www.blah.com/?stuff=False&asdf=True&sblah=afds';
        System.assert(nc.getUrl(tfMap,strMap) == proper);
    }
        
}
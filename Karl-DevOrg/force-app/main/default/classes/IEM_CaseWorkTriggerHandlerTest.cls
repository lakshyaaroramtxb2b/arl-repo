@isTest
public class IEM_CaseWorkTriggerHandlerTest {
    @isTest
    static void testPopulateExternalObjectFields(){
        Account acc = new Account();
        acc.Name  = 'Test Account';
        INSERT acc;
        
        Contact con = new Contact();
        con.LastName = 'Test Name';
        con.AccountId = acc.Id;
        
        Case caseRecord = new Case();
        caseRecord.AccountId = acc.Id;
        caseRecord.ContactId = con.Id;
        INSERT caseRecord;
        
        ADM_Work_c__x work = new ADM_Work_c__x();
        work.Subject_c__c = 'Test Subject';
        work.ExternalId = 'E-101';
        Test.startTest();
        Case_Work__c caseWork = new Case_Work__c();
        caseWork.Case__c = caseRecord.Id;
        caseWork.Work__c = work.ExternalId;
        INSERT caseWork;
        Test.stopTest();
        Case_Work__c updateRecord = [Select Id,Work_Subject__c From Case_Work__c Where Id=:caseWork.Id];
         System.assertEquals(null, updateRecord.Work_Subject__c);
    }
}
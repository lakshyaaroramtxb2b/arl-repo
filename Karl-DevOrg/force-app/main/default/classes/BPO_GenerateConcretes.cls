/*

    Developer Name: Jorge L Caceres - jcaceres@salesforce.com | December 2016
    
    Description: Instantiate concrete tasks for an assessment

*/

global without sharing class BPO_GenerateConcretes {
    
    private static final Map<String, String> TASKITEM_RECORDTYPE_IDS = new Map<String, String>{
        'Attachment' => '0123A000001aORV',
        'Boolean' => '0123A000001aORL',
        'Long Text' => '0123A000001aORQ',
        'Date' => '0123A000001aOb1'
    };

    private static final String SUCCESS_MSG = 'Assessment is being instantiated. Please wait a moment and refresh.';  
    private static final String NOTAUTHORIZED_MSG = 'You are not authorized to use this function please ask a BPO app admin.'; 
    private static final String ALREADYINSTANTIATED_MSG = 'Assessment has already been instantiated. You can only use this button once.';
    
    private static final String DATACENTER_ASSESSMENT_RECTYPEID = '0123A000001DUo1QAG';
    private static final String DATACENTER_ABSTASK_RECTYPEID = '0123A000001DUoBQAW';

    public BPO_Assessment__c assessment {get; set;}
    private String assessmentId;

    public BPO_GenerateConcretes(ApexPages.StandardController stdController) {
        this.assessmentId = stdController.getId();
    }
    
    public void instantiateButton () {        
        String returnMessage = generateConcretesForAssessment(this.assessmentId); 
        if (returnMessage == SUCCESS_MSG) {
            ApexPages.addMessage(New ApexPages.Message(ApexPages.Severity.INFO, returnMessage));        
        } else {
            ApexPages.addMessage(New ApexPages.Message(ApexPages.Severity.ERROR, returnMessage));                    
        }     
    } 
        
    webservice static String generateConcretesForAssessment(Id assessmentId) {
        // validate that request is from an admin
        Set<String> allowedUsers = new Set<String>{'BPO_Admin', 'Data_Center_App_Admin', 'Data_Center_App_User'};
        If (0 == [SELECT count() FROM PermissionSetAssignment WHERE PermissionSet.Name IN :allowedUsers
                  AND AssigneeId = :UserInfo.getUserId()]) {
            return NOTAUTHORIZED_MSG;    
        }

        // validate that not already instantiated
        If (0 < [SELECT count() FROM BPO_Task__c WHERE BPO_Assessment__c = :assessmentId]) {
            return ALREADYINSTANTIATED_MSG;    
        }
        
        // make sure instantiation is not currently active, lock record
        BPO_Assessment__c assessment = [SELECT Id, IsInstantiating__c FROM BPO_Assessment__c 
                                        WHERE Id = :assessmentId FOR UPDATE];
        if (!assessment.IsInstantiating__c) {
            assessment.IsInstantiating__c = true;
            update assessment;
            generateConcretesForAssessmentFuture(assessmentId);
        }
        return SUCCESS_MSG;  
    }
        
    @future
    public static void generateConcretesForAssessmentFuture(Id assessmentId) {
    
        // determine Assessment Record Type
        Id assRecTypeId = [SELECT RecordTypeId FROM BPO_Assessment__c WHERE Id = :assessmentId].RecordTypeId;
        
        // build queries
        String queryItems = 'SELECT Id, Description__c, BPO_Abstract_Task__c,' +
                       'Response_Type__c, Required__c ' +
                       'FROM BPO_Abstract_Task_Item__c ';
        String queryTasks = 'SELECT Id, Description__c, Required__c FROM BPO_Abstract_Task__c ';
        
        if (assRecTypeId == DATACENTER_ASSESSMENT_RECTYPEID) {
            queryItems += 'WHERE BPO_Abstract_Task__r.RecordTypeId = :DATACENTER_ABSTASK_RECTYPEID';
            queryTasks += 'WHERE RecordTypeId = :DATACENTER_ABSTASK_RECTYPEID';
        } else {
            queryItems += 'WHERE BPO_Abstract_Task__r.RecordTypeId != :DATACENTER_ABSTASK_RECTYPEID';
            queryTasks += 'WHERE RecordTypeId != :DATACENTER_ABSTASK_RECTYPEID';
        }
     
        // initialize map of abstract task items
        Map<Id, List<BPO_Abstract_Task_Item__c>> abstractTaskItemsMap = new Map<Id, List<BPO_Abstract_Task_Item__c>>();
        for (BPO_Abstract_Task_Item__c abstractTaskItem : database.query(queryItems)) {
            if (abstractTaskItemsMap.containsKey(abstractTaskItem.BPO_Abstract_Task__c)) {
                List<BPO_Abstract_Task_Item__c> taskItems = abstractTaskItemsMap.get(abstractTaskItem.BPO_Abstract_Task__c);
                taskItems.add(abstractTaskItem);    
            } else {
                abstractTaskItemsMap.put(abstractTaskItem.BPO_Abstract_Task__c, new List<BPO_Abstract_Task_Item__c>{abstractTaskItem}); 
            }                                                         
        }        
        
        // generate task and taskItems
        List<BPO_Task__c> concreteTasks = new List<BPO_Task__c>();
        List<BPO_Task_Item__c> concreteTaskItems = new List<BPO_Task_Item__c>();
        
        // loop through abstract task and generate 1 per 1
        for (BPO_Abstract_Task__c abstractTask : database.query(queryTasks)) {
            // generate concrete task
            concreteTasks.add(new BPO_Task__c(
                              BPO_Abstract_Task__c = abstractTask.Id,
                              BPO_Assessment__c = assessmentId,
                              Description__c = abstractTask.Description__c,
                              Required__c = abstractTask.Required__c)); 

        }
        
        if (!concreteTasks.isEmpty()) {
            // store generated task (need to store them at this point to get ids to link to taskitems)
            insert concreteTasks;
        
            // generate concrete task items for the newly generated tasks
            for (BPO_Task__c concreteTask : concreteTasks) {
                if (abstractTaskItemsMap.containsKey(concreteTask.BPO_Abstract_Task__c)) {                  
                    for (BPO_Abstract_Task_Item__c abstractTaskItem : abstractTaskItemsMap.get(concreteTask.BPO_Abstract_Task__c)) {
                        concreteTaskItems.add(new BPO_Task_Item__c(
                                          RecordTypeId = TASKITEM_RECORDTYPE_IDS.containsKey(abstractTaskItem.Response_Type__c)? 
                                                         TASKITEM_RECORDTYPE_IDS.get(abstractTaskItem.Response_Type__c) : null,
                                          BPO_Abstract_Task_Item__c = abstractTaskItem.Id,
                                          BPO_Task__c = concreteTask.Id,
                                          BPO_Assessment__c = concreteTask.BPO_Assessment__c,
                                          Task_Item_Description__c = abstractTaskItem.Description__c,
                                          Required__c = abstractTaskItem.Required__c));                                                 
                    }
                }
            }
            
            // insert concrete task items
            if (!concreteTaskItems.isEmpty()) insert concreteTaskItems;                                              
        }
        
        // clear instantiation flag
        update new BPO_Assessment__c(Id = assessmentId, IsInstantiating__c = false);
        
    }
}
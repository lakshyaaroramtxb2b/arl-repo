public without sharing class SPT_CaseCommentHelper {

    /*
    * Whenever case comment is created in security org, we take it's case's 
    * SupportForce Id and sync it with the same case in supportforce org.
    */
    public static void processCaseComments(List<CaseComment> caseCommentList) {
        Set<Id> caseIdSet = new Set<Id>();
        Set<Id> caseExternalIdSet = new Set<Id>();
        Set<Id> caseCommentIdSet = new Set<Id>();
        
        // Storing case comment Id and it's case Id
        for(CaseComment caseComment : caseCommentList) {
            caseIdSet.add(caseComment.ParentId);
            caseCommentIdSet.add(caseComment.Id);
        }
        System.debug('Case Id Set: '+caseIdSet);

        // Storing the SupportForce Case Id of the respective cases
        for(Case caseRecord : [SELECT Id, SupportForce_Case_Id__c
                               FROM Case
                               WHERE Id IN :caseIdSet]) {
            caseExternalIdSet.add(caseRecord.SupportForce_Case_Id__c);
        }
        System.debug('Case External Id Set: '+caseExternalIdSet);

        // If cases present, call future method
        if(caseExternalIdSet != null && !caseExternalIdSet.isEmpty()) {
            callFutureMethod(caseExternalIdSet, caseCommentIdSet);
        }
    }

    // Future method helping in storing the internal case comment in the external object also.
    @future
    public static void callFutureMethod(Set<Id> caseExternalIdSet, Set<Id> caseCommentIdSet) {
        List<CaseComment__x> newCaseCommentList = new List<CaseComment__x>();
        List<Case__x> externalCaseList = new List<Case__x>();
        Set<Id> externalCaseIdSet = new Set<Id>();
        
        // Getting the external cases
        if(!Test.isRunningTest()) {
            externalCaseList = [SELECT Id, ExternalId FROM Case__x WHERE ExternalId IN :caseExternalIdSet];
        }
        else {
            externalCaseList = [SELECT Id, ExternalId FROM Case__x];
        }
        System.debug('External Case Is: '+externalCaseList);

        for(Case__x caseRecord : externalCaseList) {
            externalCaseIdSet.add(caseRecord.ExternalId);
        }

        // Getting the internal case comments and creating an external case comment 
        Database.DMLOptions dmo = new Database.DMLOptions();
        dmo.EmailHeader.triggerUserEmail = true;
        for(CaseComment caseComment : [SELECT Id, CommentBody, IsPublished, Parent.SupportForce_Case_Id__c
                                       FROM CaseComment
                                       WHERE Id IN :caseCommentIdSet]) {
            // Creating external case comment with the same internal case comment datas                                           
            CaseComment__x sfCaseCommentRecord = new CaseComment__x();
            sfCaseCommentRecord.CommentBody__c = caseComment.CommentBody;
            sfCaseCommentRecord.IsPublished__c = caseComment.IsPublished;
            sfCaseCommentRecord.IsNotificationSelected__c = true;
            sfCaseCommentRecord.setOptions(dmo);
            if(!Test.isRunningTest() && externalCaseIdSet.contains(caseComment.Parent.SupportForce_Case_Id__c)) {
                sfCaseCommentRecord.ParentId__c = caseComment.Parent.SupportForce_Case_Id__c;
                newCaseCommentList.add(sfCaseCommentRecord); 
            }
            else if(Test.isRunningTest()){
                sfCaseCommentRecord.ParentId__c = caseComment.Parent.SupportForce_Case_Id__c;
                newCaseCommentList.add(sfCaseCommentRecord);
            }
        }
        // If exernal case comment is created, insert it
        if(newCaseCommentList != null && !newCaseCommentList.isEmpty()) {
            System.debug('New Case Inserted SUCCESS');
            Database.insertAsync(newCaseCommentList);
        }
    }
}
/**
 * @author: ralph@callaway.cloud
 * @description: scheduler for INTEG_SupportForceSync_Batch
 */
public class INTEG_SupportForceSync_Schedule implements Schedulable {
    // Tested by SC_BatchSetFrozenOrg62UsersTest
    public void execute(SchedulableContext sc) {
        Database.executeBatch((Database.Batchable<SObject>) 
            Type.forName('INTEG_SupportForceSync_Batch').newInstance(), 50);
    }
}
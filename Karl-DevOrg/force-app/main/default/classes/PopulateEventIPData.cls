global class PopulateEventIPData implements Schedulable {

   public static String CRON_EXP = '0 15 * * * ?';
   
   global void execute(SchedulableContext ctx) {
   
        DateTime dt = System.Now().addMinutes(-17);
        List<Account> accLst = [SELECT Id, Name FROM Account WHERE LastModifiedDate>=:dt];
        
        List<Detection_Security_Event__c> evts = [SELECT Id, IP_Destination__c, IP_Source__c, FieldMapRaw__c 
                                               FROM Detection_Security_Event__c 
                                               WHERE LastModifiedDate >=:dt
                                                 AND (IP_Destination__c = NULL OR IP_Source__c = NULL)];
        
        List<Detection_Security_Event__c> alertsToUpdate = new List<Detection_Security_Event__c>();
                
        for(Detection_Security_Event__c e : evts)
        {
            Map<String, Object> unraw = (Map<String, Object>) JSON.deserializeUntyped(e.fieldmapraw__c);
            String IPDestination = (String) unraw.get('IPDestination');
            String IPSource = (String) unraw.get('IPSource');
            
            if(String.isNotBlank(IPDestination) && IPDestination.length() < 20)
            {
                e.IP_Destination__c = IPDestination;
            }
            
            if(String.isNotBlank(IPSource) && IPSource.length() < 20)
            {
                e.IP_Source__c = IPSource;
            }
            
            if(e.IP_Source__c != null || e.IP_Destination__c != null)
            {
                e.IP_Destination__c = IPDestination;
                e.IP_Source__c = IPSource;
                    
                //System.debug(e.id);
                    
                alertsToUpdate.add(e);
            }
        }
        
        update alertsToUpdate;
   }   
}
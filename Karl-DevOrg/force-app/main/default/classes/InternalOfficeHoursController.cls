public class InternalOfficeHoursController {


    private static final Integer ADV_NOTICE_DAYS = 1;
    private static final Integer MAX_ADV_BOOKING_DAYS = 28;
    private static final Integer APPT_MEETING_MINUTES = 30;
    private static final Set<String> APPT_DAYS_OF_WEEK = new Set<String>{'Tue','Thu'};
    private static final Set<Integer> APPT_HOURS_OF_DAY = new Set<Integer>{10,15};
    private static final Set<Integer> APPT_MINUTES_OF_DAY = new Set<Integer>{0, 30};
    private static final Id ORG_WIDE_EMAIL_ADDRESS_ID = '0D2300000008OqS';
    private static final String OFFICE_HOURS_EMAIL = 'internalsecohour@249pdhp27owcudp258b24vuffdprqsj91drji90pj5bg99vpkc.3-jsbieac.3.apex.salesforce.com';
    
    public Integer statusCode { get; set; }
    public String statusmsg { get; set; }
    public String uName {get;set;}
    public String uCloud { get; set; }
    public String uEmail {get;set;}
    public String uTeam {get;set;}
    public String uContactInfo {get;set;}
    public String uDescription {get;set;}
    public String uDate {get;set;}
    public List<AppointmentDay> appts {get;private set;}
    public String errorMessages { get; set; }

    public String cube{get; private set;}
    
    public InternalOfficeHoursController() {
      
      // prepare calendar for each cloud
      uCloud = ApexPages.currentPage().getParameters().get('uCloud');
      if (uCloud == null) return;
      if (isCloudValid() == false) {
          uCloud = null;
          return;
      }
      
      CloudOwners__c cos = CloudOwners__c.getAll().get(uCloud);
      cube = cos.Location__c;
             
      Datetime earliest = datetime.now().addDays(ADV_NOTICE_DAYS);
      Datetime latest = datetime.now().addDays(MAX_ADV_BOOKING_DAYS);
      
      List<Datetime> possible = makeDtList();
      Map<String,AppointmentDay> admap = new Map<String,AppointmentDay>();
      appts = new List<AppointmentDay>();
           
      for (Datetime dt : possible) {
        String fmt = dt.format('yyyy-MM-dd');
        if (!admap.containsKey(fmt)) {
          AppointmentDay ad = new AppointmentDay(dt,new List<Appointment>());
          admap.put(fmt,ad); // for easy referencing
          appts.add(ad);  // for ordering
        }
        admap.get(fmt).appointments.add(new Appointment(dt));
      }
      
      
      Map<String,List<Internal_Appointment__c>> apmap = new Map<String,List<Internal_Appointment__c>>();
      for (Internal_Appointment__c pa : [SELECT Date__c FROM Internal_Appointment__c
                                             WHERE Date__c >: earliest 
                                             AND Date__c <: latest
                                             AND Cloud__c =: uCloud]) {
        String fmt = pa.Date__c.format('yyyy-MM-dd');
        //throw new tmpException('bang');                                      
        if (admap.containsKey(fmt)) {
          // duh, i would hope this is a valid day to book
          admap.get(fmt).makeUnavailable(pa.Date__c);
        }
      }
    }
    
    public PageReference checkCloudSelection() {
        if (uCloud == null) {
            return new PageReference('/apex/select_cloud');
        } 
        
        return null;
    }
    
    private boolean isCloudValid() {
        // check to see whether the cloud is one of the 8 valid clouds
        if (uCloud.equals('service') ||
        uCloud.equals('sales') ||
        uCloud.equals('collaboration') ||
        uCloud.equals('marketing') ||
        uCloud.equals('platform') ||
        uCloud.equals('core') ||
        uCloud.equals('mobile') ||
        uCloud.equals('techops')
        ) return true;
        
        return false;
    }
       
    public PageReference doBooking() {
      try {
        Datetime reqDate = datetime.valueOf(uDate);
        String day = reqDate.format('yyyy-MM-dd');
        String hour = String.valueOf(reqDate.hour());
        String minute = String.valueOf(reqDate.minute());
        
        
        // ghetto workaround
        Datetime past = reqDate.addDays(-1);
        for (Internal_Appointment__c ghettoworkaround: [SELECT Id,Date__c FROM Internal_Appointment__c WHERE Date__c >:past AND Cloud__c =: uCloud]) {
          if (ghettoworkaround.Date__c.format('yyyy-MM-dd-HH-mm') == reqDate.format('yyyy-MM-dd-HH-mm')) {
            statusmsg = 'Booking no longer available.  Sorry, please select another time';
            statusCode = 1;
            return null;
          }
        }
        
        
        Boolean valid = False;
        for (Datetime p : makeDtList()) {
          if ((p.year() == reqDate.year()) && (p.month() == reqDate.month()) && (p.day() == reqDate.day()) &&
              (p.hour() == reqDate.hour()) && (p.minute() == reqDate.minute())) {
            valid = True;
          }
        }
        if (!valid) {
          statusmsg = 'Sorry, that date is not available. Please select another time';
          statusCode = 1;
          return null;
        }
        if (!uEmail.endswith('@salesforce.com')) {
          statusmsg = 'Sorry, only salesforce email address is accepted. Please use your salesforce email address to book.';
          statusCode = 1;
          return null;
        }
        
        // get the cloud owners' emails and cube location
        CloudOwners__c cos = CloudOwners__c.getAll().get(uCloud);
        if (uContactInfo == null || uContactInfo == '') uContactInfo = cos.Location__c;
        
        
        Internal_Appointment__c pa = new Internal_Appointment__c();
        pa.Name = uDate+' - '+uName;
        pa.Email__c = uEmail;
        pa.Name__c = uName;
        pa.Team__c = uTeam;
        pa.Description__c = uDescription;
        pa.ContactInfo__c = uContactInfo;
        pa.Date__c = reqDate;
        pa.Cloud__c = uCloud;
        insert pa;
        Internal_Appointment__c pa2 = [Select Name, Email__c, Name__c, Cloud__c, Team__c, Description__c, ContactInfo__c,
        Date__c, RefId__c FROM Internal_Appointment__c WHERE Id = :pa.Id LIMIT 1];
        pa2.Date__c = pa2.Date__c.addHours(8);
        sendInvitation(pa2);
      }
      catch (Exception e) {
        //statusmsg = 'Booking error.  Sorry, please try again';
        statusmsg = e.getTypeName()+e.getMessage();
        statusCode = 1;
        throw e;
        return null;
      }
      statusmsg = 'Booking complete.  A meeting invitation will be sent to "'+uEmail+'".';
      statusCode = 0;
      return null;
    }
    
    private List<Datetime> makeDtList() {
      List<datetime> all = new List<datetime>();

      
      // Non-BusinessHours version
      Datetime now = datetime.now().addDays(ADV_NOTICE_DAYS);
      Integer daysAhead = 0+ADV_NOTICE_DAYS;
      while (daysAhead < MAX_ADV_BOOKING_DAYS) {
        if (APPT_DAYS_OF_WEEK.contains(now.format('EEE'))) {
          for (Integer hr : APPT_HOURS_OF_DAY) {
            for (Integer min : APPT_MINUTES_OF_DAY) {
                all.add(datetime.newInstance(now.year(),
                                             now.month(),
                                             now.day(),
                                             hr,
                                             min,
                                             0));
            }
          }
        }
        daysAhead++;
        now = now.addDays(1);
      } 
                                        
      return all;
    }
        
    private static String[] getCloudOwnerEmails(Internal_Appointment__c app) {
        CloudOwners__c cos = CloudOwners__c.getAll().get(app.Cloud__c);
        String[] cos_emails = cos.Emails__c.split(',');
        return cos_emails;
    }
    
    private static String[] getCloudOwnerNames(Internal_Appointment__c app) {
        CloudOwners__c cos = CloudOwners__c.getAll().get(app.Cloud__c);
        String[] cos_names = cos.Names__c.split(',');
        return cos_names;
    }
    
    public void sendInvitation(Internal_Appointment__c app) {
      
      Messaging.SingleEmailMessage mail= new Messaging.SingleEmailMessage();

      //get cloud owner names and emails
      String[] cos_emails = getCloudOwnerEmails(app);
      String[] cos_names = getCloudOwnerNames(app);
      
      //combine the emails
      String[] emails = new List<String>();
      for (String s: cos_emails) emails.add(s);
      emails.add(app.Email__c);

      mail.setOrgWideEmailAddressId(ORG_WIDE_EMAIL_ADDRESS_ID);
      mail.setToAddresses(emails);   
      mail.setReplyTo(OFFICE_HOURS_EMAIL);
      mail.setSubject('['+app.RefId__c+'] Internal Security Office Hours | '+ app.Team__c  + ' | Ref:' + String.valueOf(app.Id)); //cheating, we split on '|' later for handling responses 
      mail.setPlainTextBody('Description: '+app.Description__c+'\n\n\nLocation: '+app.ContactInfo__c + '\n\n\nIf you wish to cancel or update the contact information for an appointment, please send an email to: ' + OFFICE_HOURS_EMAIL  + '.');
      
      Messaging.EmailFileAttachment atta = new Messaging.EmailFileAttachment();
      atta.setBody(Blob.valueOf(getVCALENDAR(app,APPT_MEETING_MINUTES, cos_emails, cos_names)));
      atta.setFileName('appointment_request.ics');
      atta.setContentType('text/calendar');
      atta.setInline(true);
      
      mail.setFileAttachments(new Messaging.EmailFileAttachment[]{atta});
      Messaging.sendEmail(new Messaging.Email[] {mail});
    }
    
    public static void sendCancellation(Internal_Appointment__c app) {
      Messaging.SingleEmailMessage mail= new Messaging.SingleEmailMessage();
      
      //get cloud owner names and emails
      String[] cos_emails = getCloudOwnerEmails(app);
      String[] cos_names = getCloudOwnerNames(app);
      
      //combine the emails
      String[] emails = new List<String>();
      for (String s: cos_emails) emails.add(s);
      emails.add(app.Email__c);

      
      mail.setToAddresses(emails);
      mail.setReplyTo(OFFICE_HOURS_EMAIL);
      mail.setSubject('['+app.RefId__c+'] Internal Security Office Hours | '+ app.Team__c  + ' | Ref:' + String.valueOf(app.Id)); //cheating, we split on '|' later for handling responses 
      mail.setPlainTextBody('Description: '+app.Description__c+'\n\n\nLocation: '+app.ContactInfo__c);
      
      Messaging.EmailFileAttachment atta = new Messaging.EmailFileAttachment();
      atta.setBody(Blob.valueOf(getVcalCancellation(app,APPT_MEETING_MINUTES, cos_emails, cos_names)));
      atta.setFileName('appointment_request.ics');
      atta.setContentType('text/calendar');
      atta.setInline(true);
      
      mail.setFileAttachments(new Messaging.EmailFileAttachment[]{atta});
      Messaging.sendEmail(new Messaging.Email[] {mail});
    }
    
    private static String Schedule(Boolean IsAllDayEvent, Datetime StartDateTime, Datetime EndDateTime)
    {
    String result;
    if (IsAllDayEvent)
    {
      result = 'DTSTART;VALUE=DATE:' + StartDateTime.formatGmt('yyyyMMdd') + CRLF;
      result += 'DTEND;VALUE=DATE:' + EndDateTime.AddDays(1).formatGmt('yyyyMMdd') + CRLF;
    }
    else
    {
      result = 'DTSTART:' + StartDateTime.formatGmt('yyyyMMdd\'T\'HHmmss\'Z\'') + CRLF;
      result += 'DTEND:' + EndDateTime.formatGmt('yyyyMMdd\'T\'HHmmss\'Z\'') + CRLF;
    }
    return result;
  }
  
  static String CRLF = '\r\n';
  private static String SetField(String fieldName, String fieldValue)
  {
    if (fieldValue != null && fieldValue != '')
    {
      return fieldName + Escape(fieldValue) + CRLF;
    }
    return '';
  }
  
  private static String SetDescriptionField(String fieldName, String fieldValue)
  {
    if (fieldValue != null && fieldValue != '')
    {
      return fieldName + Escape(fieldValue) + '\\n';
    }
    return '';
  }
  
  public String getVCALENDAR(Internal_Appointment__c pa,Integer minutes, String[] emails, String[] names)
  {
    String dtstamp = 'DTSTAMP:' + Datetime.Now().formatGmt('yyyyMMdd\'T\'HHmmss\'Z\'') + CRLF;
    String result = 'BEGIN:VCALENDAR' + CRLF + 'PRODID:-//Force.com Labs Prodsec//iCalendar Export//EN' + CRLF + 'VERSION:2.0' + CRLF;
    result += 'CALSCALE:GREGORIAN' + CRLF;
    result += 'METHOD:REQUEST' + CRLF;
    result += 'X-MS-OLK-FORCEINSPECTOROPEN:TRUE' + CRLF;   
    result += 'X-WR-RELCALID:' + String.valueOf(Crypto.GetRandomLong()) + CRLF;
    result += 'BEGIN:VEVENT' + CRLF;
    result += 'UID:'+pa.Name__c + CRLF;
    result += Schedule(false, pa.Date__c, pa.Date__c.addMinutes(minutes));
    result += dtstamp;
    result += SetField('SUMMARY:', '['+pa.RefId__c+'] '+ pa.Description__c);
    result += SetField('LOCATION:', pa.ContactInfo__c);
    result += 'CLASS:PUBLIC' + CRLF;
    result += 'TRANSP:OPAQUE' + CRLF + 'X-MICROSOFT-CDO-BUSYSTATUS:BUSY' + CRLF;
    result += 'X-MICROSOFT-CDO-IMPORTANCE:1' + CRLF;
    result += 'X-MICROSOFT-DISALLOW-COUNTER:FALSE' + CRLF;
    result += 'X-MS-OLK-ALLOWEXTERNCHECK:TRUE' + CRLF;
    result += 'X-MS-OLK-AUTOFILLLOCATION:FALSE' + CRLF;
    result += 'X-MS-OLK-CONFTYPE:0' + CRLF;
    
    String addInfo = SetDescriptionField('Contact: ', pa.Name__c);
    addInfo += SetDescriptionField('Email: ', pa.Email__c);
    addInfo += '\\n';
    result += SetField('DESCRIPTION:', addInfo+pa.Description__c);
    result += 'ATTENDEE;ROLE=REQ-PARTICIPANT;PARTSTAT=NEEDS-ACTION;CN='+pa.Name__c+':MAILTO:'+pa.Email__c + CRLF; // partner
    Integer i = 0;
    for (String email: emails) {
       result += 'ATTENDEE;ROLE=REQ-PARTICIPANT;PARTSTAT=NEEDS-ACTION;CN=' + names[i++] + ':MAILTO:'+ email + CRLF; // cloud owners
    }
    result += 'ORGANIZER;CN=Force.com Secure Cloud:MAILTO:' + OFFICE_HOURS_EMAIL  + CRLF; // make us the organizer so outlook replies work
    result += 'BEGIN:VALARM' + CRLF;
    result += 'TRIGGER:-PT15M' + CRLF;
    result += 'ACTION:DISPLAY' + CRLF;
    result += 'DESCRIPTION:Reminder' + CRLF;
    result += 'END:VALARM' + CRLF;
    result += 'END:VEVENT' + CRLF;
    result += 'END:VCALENDAR' + CRLF;
    return result;
  }
  
  static String getVcalCancellation(Internal_Appointment__c pa,Integer minutes, String[] emails, String[] names){
    String dtstamp = 'DTSTAMP:' + Datetime.Now().formatGmt('yyyyMMdd\'T\'HHmmss\'Z\'') + CRLF;
    String result = 'BEGIN:VCALENDAR' + CRLF + 'PRODID:-//Force.com Labs Prodsec//iCalendar Export//EN' + CRLF + 'VERSION:2.0' + CRLF;
    result += 'CALSCALE:GREGORIAN' + CRLF;
    result += 'METHOD:CANCEL' + CRLF;
    result += 'X-MS-OLK-FORCEINSPECTOROPEN:TRUE' + CRLF;   
    result += 'X-WR-RELCALID:' + String.valueOf(Crypto.GetRandomLong()) + CRLF;
    result += 'BEGIN:VEVENT' + CRLF;
    result += 'UID:'+pa.Name__c + CRLF;
    result += Schedule(false, pa.Date__c, pa.Date__c.addMinutes(minutes));
    result += dtstamp;
    result += SetField('SUMMARY:', '['+pa.RefId__c+'] ' + pa.Description__c);
    result += SetField('LOCATION:', pa.ContactInfo__c);
    result += 'CLASS:PUBLIC' + CRLF;
    result += 'TRANSP:OPAQUE' + CRLF + 'X-MICROSOFT-CDO-BUSYSTATUS:BUSY' + CRLF;
    result += 'X-MICROSOFT-CDO-IMPORTANCE:1' + CRLF;
    result += 'X-MICROSOFT-DISALLOW-COUNTER:FALSE' + CRLF;
    result += 'X-MS-OLK-ALLOWEXTERNCHECK:TRUE' + CRLF;
    result += 'X-MS-OLK-AUTOFILLLOCATION:FALSE' + CRLF;
    result += 'X-MS-OLK-CONFTYPE:0' + CRLF;
    
    String addInfo = SetDescriptionField('Contact: ', pa.Name__c);
    addInfo += SetDescriptionField('Email: ', pa.Email__c);
    addInfo += '\\n';
    result += SetField('DESCRIPTION:', addInfo+pa.Description__c);
    result += 'ATTENDEE;ROLE=REQ-PARTICIPANT;PARTSTAT=NEEDS-ACTION;CN='+pa.Name__c+':MAILTO:'+pa.Email__c + CRLF; // internal cloud team member
    Integer i = 0;
    for (String email: emails) {
       result += 'ATTENDEE;ROLE=REQ-PARTICIPANT;PARTSTAT=NEEDS-ACTION;CN=' + names[i++] + ':MAILTO:'+ email + CRLF; // cloud owners
    }
    result += 'ORGANIZER;CN=Force.com Secure Cloud:MAILTO:' + OFFICE_HOURS_EMAIL  + CRLF; // make us the organizer so outlook replies work
    result += 'BEGIN:VALARM' + CRLF;
    result += 'TRIGGER:-PT15M' + CRLF;
    result += 'ACTION:DISPLAY' + CRLF;
    result += 'DESCRIPTION:Reminder' + CRLF;
    result += 'END:VALARM' + CRLF;
    result += 'END:VEVENT' + CRLF;
    result += 'END:VCALENDAR' + CRLF;
    return result;
  }
  
  private static String Escape(String original)
  {
    return original.replace('\n','\\n').replace('\r','');
  }
    public class AppointmentDay {
      
      public String dayMonth {get;private set;}
      public List<Appointment> appointments {get;private set;}
      
      public AppointmentDay(datetime whentime, List<Appointment> apps) {
        dayMonth = whentime.format('MMM dd');
        appointments = apps;
      }
      public void makeUnavailable(datetime dt) {
        for (Appointment a : appointments) {
          if ((a.rt.year() == dt.year()) && (a.rt.month() == dt.month()) && (a.rt.day() == dt.day()) &&
              (a.rt.hour() == dt.hour()) && (a.rt.minute() == dt.minute())) {
              a.available = False;
          }
        }
      }
    }
    
    public class Appointment {
      public Datetime rt;
      public Boolean available {get;private set;}
      public Appointment(datetime dt) {
        rt = dt;
        available = True;

                                            
      }
      public String getprettyTime() {
        Datetime endtime = rt.addMinutes(APPT_MEETING_MINUTES);
        //return String.valueOf(Math.mod(rt.hour(),12))+'-'+String.valueOf(Math.mod(endtime.hour(),12))+endtime.format(' a (z)');
        return String.valueOf(rt.format('h:mm a (z)'));
      }
      public String getuglyTime() {
        return rt.format('yyyy-MM-dd HH:mm:ss');
      }
    }

}
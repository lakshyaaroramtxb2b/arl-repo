/**
 * @author: ralph@callaway.cloud
 * @description: core training enrollment logic
 */
public class SC_Enroller {

    private Options opts;

    public SC_Enroller(Options opts) {
        this.opts = opts;
        System.assertNotEquals(null, opts, 'opts required');
        System.assert(opts.isValid(), 'invalid opts, errors: ' + String.join(opts.getErrors(), ', '));
    }

    public Enrollment[] enroll(Contact[] contacts) {
        // load related records 
        Map<Id, Training_Course_Taken__c> existingEnrollments = getExistingEnrollments(contacts, opts.courseId);

        // prepare enrollments
        Enrollment[] enrollments = new Enrollment[0];
        for (Contact contact : contacts) {
            enrollments.add(new Enrollment(contact, existingEnrollments.get(contact.Id), opts));
        }

        // save enrollments
        createEnrollmentRecords(enrollments);
        
        // send notifications if required 
        if (opts.sendNotification) {
            sendNotifications(enrollments);
        }

        return enrollments;
    }

    public Enrollment enroll(Contact contact) {
        return enroll(new Contact[] { contact })[0];
    }

    private void createEnrollmentRecords(Enrollment[] enrollments) {
        Enrollment[] updateNeeded = new Enrollment[0];
        Training_Course_Taken__c[] tcts = new Training_Course_Taken__c[0];
        for (Enrollment enrollment : enrollments) {
            if (enrollment.result == Result.ENROLLED) {
                updateNeeded.add(enrollment);
                tcts.add(enrollment.newRec);
            }
        }
        Database.UpsertResult[] saveResults = Database.upsert(tcts, false);
        for (Integer i = 0; i < saveResults.size(); i++) {
            updateNeeded[i].setSaveResult(saveResults[i]);
        }
    }

    private void sendNotifications(Enrollment[] enrollments) {
        Enrollment[] emailNeeded = new Enrollment[0];
        Messaging.SingleEmailMessage[] emails = new Messaging.SingleEmailMessage[0];
        for (Enrollment enrollment : enrollments) {
            if (enrollment.result != Result.ERROR && enrollment.email != null) {
                emailNeeded.add(enrollment);
                emails.add(enrollment.email);
            }
        }
        Messaging.SendEmailResult[] emailResults = Messaging.sendEmail(emails, false);
        for (Integer i = 0; i < emailResults.size(); i++) {
            emailNeeded[i].setEmailResult(emailResults[i]);
        }
    }

    private Map<Id, Training_Course_Taken__c> getExistingEnrollments(Contact[] contacts, Id courseId) {
        Map<Id, Training_Course_Taken__c> existingEnrollments = new Map<Id, Training_Course_Taken__c>();
        for (Training_Course_Taken__c enrollment : [
            SELECT
                Block_Enrollment__c,
                Contact__c,
                Due_Date__c,
                Enrollment_Notification_Date__c,
                Is_Complete_Combined__c,
                RecordTypeId,
                Id
            FROM Training_Course_Taken__c
            WHERE Training_Course__c = :courseId
                AND Contact__c IN :contacts
        ]) {
            existingEnrollments.put(enrollment.Contact__c, enrollment);
        }
        return existingEnrollments;
    }

    public Enum Result {
        INVALID,
        SKIPPED,
        NO_CHANGE,
        ENROLLED,
        ERROR
    }

    public class Enrollment {
        public Contact contact;
        public Messaging.SingleEmailMessage email;
        public Training_Course_Taken__c newRec;
        public SC_Enroller.Result result;
        public String[] errors = new String[0];
        public Boolean notified = false;
        
        private Options opts;
        
        private Messaging.SendEmailResult emailResult;
        private Training_Course_Taken__c oldRec;

        public Enrollment(Contact contact, Training_Course_Taken__c oldRec, Options opts) {
            this.contact = contact;
            this.oldRec = oldRec;
            this.opts = opts;
            if (!isValid()) {
                this.result = SC_Enroller.Result.INVALID;
            } else if (!isEnrollable()) {
                this.result = SC_Enroller.Result.SKIPPED;
            } else {
                init();
                if (hasChanges()) {
                    this.result = SC_Enroller.Result.ENROLLED;
                } else {
                    this.result = SC_Enroller.Result.NO_CHANGE;
                }
            }
        }

        public void setSaveResult(Database.UpsertResult saveResult) {
            if (!saveResult.isSuccess()) {
                this.result = SC_Enroller.Result.ERROR;
                for (Database.Error err : saveResult.getErrors()) {
                    this.errors.add(String.valueOf(err));
                }
            } else if (email != null) {
                email.setWhatId(newRec.Id);
            }
        }

        public void setEmailResult(Messaging.SendEmailResult emailResult) {
            if (!emailResult.isSuccess()) {
                this.result = SC_Enroller.Result.ERROR;
                for (Messaging.SendEmailError err : emailResult.getErrors()) {
                    this.errors.add(String.valueOf(err));
                }
            } else {
                notified = true;
            }
        }

        private Boolean hasChanges() {
            return newRec != null && (
                oldRec == null ||
                newRec.RecordTypeId != oldRec.RecordTypeId ||
                newRec.Enrollment_Notification_Date__c != oldRec.Enrollment_Notification_Date__c ||
                newRec.Block_Enrollment__c != oldRec.Block_Enrollment__c ||
                newRec.Due_Date__c != oldRec.Due_Date__c);
        }

        private void init() {
            if (oldRec == null) {
                newRec = (Training_Course_Taken__c) 
                    Training_Course_Taken__c.sObjectType.newSObject(null, true);
                newRec.Contact__c = contact.Id;
                newRec.Training_Course__c = opts.courseId;    
            } else {
                newRec = oldRec.clone(true);
            }
            newRec.Block_Enrollment__c = false;
            newRec.RecordTypeId = (opts.isMandatory) ? 
                TrainingCourseTaken_Constants.RT_ID_MANDATORY : 
                TrainingCourseTaken_Constants.RT_ID_VOLUNTARY;
            if (oldRec == null || !oldRec.Is_Complete_Combined__c) {
                if (opts.isMandatory && newRec.Due_Date__c == null) {
                    newRec.Due_Date__c = opts.dueDate;
                }
                if (opts.sendNotification && newRec.Enrollment_Notification_Date__c == null) {
                    newRec.Enrollment_Notification_Date__c = DateTime.now();
                    email = new Messaging.SingleEmailMessage();
                    email.setOrgWideEmailAddressId(opts.orgWideEmailId);
                    email.setTargetObjectId(contact.Id);
                    email.setSaveAsActivity(true);
                    email.setCharset('UTF-8');
                    email.setTemplateId(opts.templateId);
                }
            }
        }

        private Boolean isEnrollable() {
            if (!errors.isEmpty()) return false;
            if (!contact.Is_Active__c) {
                errors.add('Contact is inactive');
            }
            if (contact.Is_Frozen__c) {
                errors.add('Contact is frozen');
            }
            if (contact.Is_On_Leave_Of_Absence__c) {
                errors.add('Contact is on leave of absence');
            }
            if (oldRec != null && oldRec.Block_Enrollment__c) {
                errors.add('Enrollment blocked from future enrollment');
            }
            return errors.isEmpty();
        }

        private Boolean isValid() {
            if (!errors.isEmpty()) return false;
            if (contact.RecordTypeId != Contact_Constants.RT_ID_SALESFORCE_EMPLOYEE) {
                errors.add('Contact not Salesforce Employee record type');
            }
            if (String.isBlank(contact.Email)) {
                errors.add('Contact missing email');
            }
            if (String.isBlank(contact.Org62_User_ID__c)) {
                errors.add('Contact has blank org62 user id');
            }
            /* this id_util is not 100% accurate and preventing enrollments
            if (!Id_Util.isValid(contact.Org62_User_Id__c, User.sObjectType)) {
                errors.add('Contact has invalid org62 user id');
            }
            */
            return errors.isEmpty();
        }   
    }

    public class Options {
        public Id courseId { get; set; }
        public Id templateId { get; set; }
        public Id orgWideEmailId { get; set; }
        public Boolean isMandatory { get; set; }
        public Boolean sendNotification { get; set; }
        public Date dueDate { get; set; }
        private String[] errors = new String[0];

        public Options() {
            isMandatory = false;
            sendNotification = false;
        }

        public String[] getErrors() { return errors.clone(); }
        
        /**
         * Check if options is valid. Use getErrors() to get issues
         * @return true if valid
         */
        public Boolean isValid() {
            validate();
            return errors.isEmpty();
        }

        /**
         * Populates errors array with any validation issues
         */
        private void validate() {
            errors = new String[0];
            if (courseId == null) errors.add('courseId is required');
            if (!Id_Util.isValid(courseId, Training_Course__c.sObjectType))
                errors.add('courseId is not a valid Training_Course__c id');
            if (isMandatory) {
                if (dueDate == null) errors.add('dueDate required if isMandatory = true');
            } else {
                if (dueDate != null) errors.add('dueDate must be blank if isMandatory = false');
            }
            if (sendNotification) {
                if (templateId == null) errors.add('templateId required if sendNotification = true');    
                if (orgWideEmailId == null) errors.add('orgWideEmailId required if sendNotification = true');
                if (!Id_Util.isValid(templateId, EmailTemplate.sObjectType))
                    errors.add('templateId is not a valid email template id');
                if (!Id_Util.isValid(orgWideEmailId, OrgWideEmailAddress.sObjectType))
                    errors.add('orgWideEmailId is not a valid org wide email address id');
            } else {
                if (templateId != null) errors.add('templateId must be blank if sendNotification = false');
                if (orgWideEmailId != null) errors.add('orgWideEmailId must be blank if sendNotification = false');
            }
        }
    }

}
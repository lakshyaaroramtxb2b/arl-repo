public with sharing class CDI_Parent_ExportCSV {
public PageReference redirect()
{
   PageReference pr = new PageReference('/apex/CDI_ExportCsv');
      pr.getParameters().put('datastoreId', ApexPages.CurrentPage().getparameters().get('id'));
        pr.setRedirect(true);
        return pr;
   
}
}
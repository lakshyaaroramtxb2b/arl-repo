/**
 * @author Mahima Aggarwal
 * @email mahima.aggarwal@mtxb2b.com
 * @desc Test class for KARL_AuditorRequestItemTriggerHelper
*/
@isTest
public class KARL_AuditorRequestItemTriggerHelperTest {
	/**
     * @author Mahima Aggarwal
     * @email mahima.aggarwal@mtxb2b.com
     * @desc Test data setup.
    */
    @testSetup
    private static void testSetup(){
        User testUser = ARL_TestDataFactory.createExternalAuditor();
    }
        
    @istest
    public static void unitTest(){ 
        Account acc = new Account(Name = 'salesforce.com - ESA Office Hours');
        insert acc;
        
        Contact con = new Contact(LastName = 'test', AccountId = acc.Id,Email = 'test@gmail.com');
        insert con;
        
        Request_Item__c reqItem = ARL_TestDataFactory.createRequestItem();
        reqItem.Primary_Scope__c = 'SS';
        insert reqItem;
        
        Cycle_Request_Item__c cycleReqItem = ARL_TestDataFactory.createCycleRequestItem(reqItem.id,null);
        cycleReqItem.Cycle_Request_Status__c = 'New';
        insert cycleReqItem;
        
        KARL_Audit_Team__c auditTeam = ARL_TestDataFactory.createAuditTeam('test');
        insert auditTeam;
        
        KARL_Audit_Team_Contacts__c auditTeamCon = ARL_TestDataFactory.createAuditTeamContact(auditTeam.Id,con.Id);
        insert auditTeamCon;
        
        
        KARL_Auditor_Request_Item__c auditorReqItem = ARL_TestDataFactory.createAuditorRequestItem(cycleReqItem.Id,auditTeam.Id);
        insert auditorReqItem;
        
        cycleReqItem.Cycle_Request_Status__c = 'Provided to Auditor';
        update cycleReqItem;
        
        auditorReqItem.KARL_Status__c = 'Testing Completed';
        update auditorReqItem;
    }
    
    @istest
    public static void unitTest2(){
    	Request_Item__c reqItem = ARL_TestDataFactory.createRequestItem();
        reqItem.Primary_Scope__c = 'SS';
        insert reqItem;
        
        Cycle_Request_Item__c cycleReqItem = ARL_TestDataFactory.createCycleRequestItem(reqItem.id,null);
        cycleReqItem.Cycle_Request_Status__c = 'New';
        insert cycleReqItem;
        
        KARL_Audit_Team__c auditTeam = ARL_TestDataFactory.createAuditTeam('test');
        insert auditTeam;
        
        KARL_Auditor_Request_Item__c auditorReqItem = ARL_TestDataFactory.createAuditorRequestItem(cycleReqItem.Id,auditTeam.Id);
        insert auditorReqItem;
        
        User usr = [Select id from User where FirstName = 'External_Auditor'];
        System.runAs(usr){
            auditorReqItem.KARL_Status__c = 'Testing in Progress';
            try{
                update auditorReqItem;
            } catch(Exception e){}
            
        }
    }
}
global class Security_URLRewriter implements Site.UrlRewriter {

  global PageReference mapRequestUrl(PageReference externalUrl) {
      if ((externalUrl.getUrl() == '/') || (externalUrl.getUrl().startsWith('/?'))) {
          return Page.Security_Home;
      }
      List<String> url_pieces = externalUrl.getUrl().split('\\?');
      String friendly_path = url_pieces.get(0).toLowerCase();
      String params = '';
      if (url_pieces.size() > 1) {
          params = url_pieces.get(1);
      }
      
      String internal_path = friendly_path.substring(1).replace('/','_');
      String internal_url = internal_path;
      if (params != '') {
          internal_url += '?'+params;
      }
      System.debug('External: '+externalUrl.getUrl()+'  Internal: '+internal_url);
      return new PageReference('/apex/'+internal_url);
  }

  global List<PageReference> generateUrlFor(List<PageReference> externalUrls) {
      List<PageReference> myFriendlyUrls = new List<PageReference>();
      for (PageReference p : externalUrls) {
          List<String> pieces = p.getUrl().split('\\?');
          String path = pieces.get(0).toLowerCase();
          String params = '';
          String friendly_path = '';
          String friendly_url = '';
          if (pieces.size() > 1) {
              params = pieces.get(1);
          }
          if (path.startsWith('/apex')) {
              friendly_path = path.substring('/apex'.length()).replace('_','/');
          } else {
              friendly_path = path.replace('_','/');
          }
          friendly_url = friendly_path;
          if (params != '') {
              friendly_url += '?'+params;
          }
          System.debug('Internal: '+p.getUrl()+'  Friendly: '+friendly_url);
          myFriendlyUrls.add(new PageReference(friendly_url));
      }
      return myFriendlyUrls;
  }

  @isTest
  static void testExternalToInternal() {
      Security_URLRewriter rewriter = new Security_URLRewriter();
      PageReference p = rewriter.mapRequestUrl(new PageReference('/security/tools/forcecom/home'));
      System.assert(p.getUrl() == '/apex/security_tools_forcecom_home');
  }
  
  @isTest
  static void testInternalToExternal() {
      Security_URLRewriter rewriter = new Security_URLRewriter();
      List<PageReference> p = rewriter.generateUrlFor(new List<PageReference> {Page.Security_Tools_Forcecom_Home} );
      System.assert(p.get(0).getUrl() == '/security/tools/forcecom/home');
  }
  
}
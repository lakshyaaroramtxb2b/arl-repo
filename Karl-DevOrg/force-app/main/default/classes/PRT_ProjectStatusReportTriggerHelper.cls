/*
* Created by - Prashant Gupta
* Date - 19-09-2019
* Helper for PRT_ProjectStatusReportTrigger
*/
public class PRT_ProjectStatusReportTriggerHelper {
    /*
     * Created by - Prashant Gupta
     * Date - 19-09-2019
     * This method populates V2MOM_and_Measure__c from project to newly created PSR
     * Removed - Prashnat Gupta
     * V2MOM_and_Measure__c field not needed anymore.
     */
    /*public static void populateV2MOMandMeasure(List<Project_Status_Report__c> newList){
        Set<Id> projectIDSet = new Set<ID>();
        List<Project_Status_Report__c> psrToBeUpdatedList = new List<Project_Status_Report__c>();
        for(Project_Status_Report__c projectSR : newList){
            if(projectSR.Project__c != null){
                    projectIDSet.add(projectSR.Project__c);
                    psrToBeUpdatedList.add(projectSR);
              }
        }
        if(projectIDSet!=null && !projectIDSet.isEmpty()){
            Map<id,Project__c> projectMap = new Map<id,Project__c>([SELECT id, V2MOM_and_Measure__c 
                                                                    FROM Project__c 
                                                                    WHERE id IN :projectIDSet]);
            if(projectMap!=null && !projectMap.isEmpty()){
                for(Project_Status_Report__C psr : psrToBeUpdatedList){
                    if(projectMap.containsKey(psr.Project__c)){
                        psr.V2MOM_and_Measure__c = projectMap.get(psr.Project__c).V2MOM_and_Measure__c;
                    }
                }
            }
        }
    }*/
    /*
     * Created by - Prashant Gupta
     * Date - 19-09-2019
     * This method checks if all required fields are populated before approval
     */
    public static void checkRequiredFields(List<Project_Status_Report__c> newList, Map<id,Project_Status_Report__c> oldMap){
        List<Project_Status_Report__c> projectSR_List = new List<Project_Status_Report__c>();
        for(Project_Status_Report__c pro : newList){
            if(pro.Submitted__c
              && (oldMap==null || pro.Submitted__c !=oldMap.get(pro.id).Submitted__c)){
                projectSR_List.add(pro);
            }
        }
        if(!projectSR_List.isEmpty()){      
            PRT_Utility.checkRequiredFields(projectSR_List,Schema.SObjectType.Project_Status_Report__c.fieldSets.getMap().get('PRT_PSR_Required_fields'));
        }
    }
    /*
     * Created by - Prashant Gupta
     * Date - 19-09-2019
     * This method updates Budget and scedule status from PRT to project
     */
    public static void updatePRTFieldsOnProject(List<Project_Status_Report__c> newList, Map<id,Project_Status_Report__c> oldMap){
        Set<Id> psrProjectIds = new Set<ID>();
        Map<id, Project__c> updatedProjects = new Map<id, Project__c>();
        for(Project_Status_Report__c pro : newList){
            system.debug('test data --> '+ pro.Approved__c);
            /*if((oldMap==null && pro.Budget_Status__c!=null && pro.Schedule_Status__c!=null) || (oldMap!=null && 
              ((pro.Budget_Status__c!=null && pro.Budget_Status__c !=oldMap.get(pro.id).Budget_Status__c)
              || (pro.Schedule_Status__c!=null && pro.Schedule_Status__c !=oldMap.get(pro.id).Schedule_Status__c)
              || (pro.Overall_Project_Status__c!=null && pro.Overall_Project_Status__c != oldMap.get(pro.id).Overall_Project_Status__c)
              || (pro.Approved__c == true && pro.Approved__c !=oldMap.get(pro.id).Approved__c)))){*/
              if(pro.Approved__c == true)    {
                  psrProjectIds.add(pro.Project__c);
            }
        }
        if(!psrProjectIds.isEmpty()){      
            for(Project_Status_Report__c pro : [SELECT ID,Project__c ,Overall_Project_Status__c,Budget_Status__c,Schedule_Status__c, Weekly_Status_Summary__c FROM Project_Status_Report__c WHERE Project__c IN: psrProjectIds
                                               ORDER BY CreatedDate DESC LIMIT 1]){
                if(!updatedProjects.containsKey(pro.Project__c)){
                    Project__c project = new Project__c();
                    project.id = pro.Project__c;
                    project.Budget_Status__c = Pro.Budget_Status__c;
                    project.Schedule_Status__c = Pro.Schedule_Status__c; 
                    project.Overall_Project_Health__c = pro.Overall_Project_Status__c;
                    //Remove HTML Markup
                    if(pro.Weekly_Status_Summary__c != null)    {
                    String weeklyStatusSummary = pro.Weekly_Status_Summary__c.stripHtmlTags();
                    project.Project_Health_Comments__c = weeklyStatusSummary;
                    }
                    updatedProjects.put(pro.Project__c, project);
                }
            }
        }

        if(!updatedProjects.isEmpty()){
            update updatedProjects.values();
        }
    }
    /*
     * Created by - Prashant Gupta
     * Date - 19-09-2019
     * This method updates the name as name+weekending
     */
    public static void updateReportName(List<Project_Status_Report__c> newList, Map<id,Project_Status_Report__c> oldMap){
        for(Project_Status_Report__c pro : newList){
            if((oldMap!=null && oldMap.get(pro.Id).Week_ending__c != pro.Week_ending__c)|| pro.Week_ending__c!=null){
                if(!pro.name.contains('Week of:')){
                    pro.Name = pro.Name +' - Week of: '+pro.Week_ending__c.format(); 
                    if(pro.Name.length()>80){
                        pro.Name = pro.Name.substring(0, 80);
                    }
                }
                else if(pro.Name!=null && oldMap!=null){
                    String name = pro.Name;
                    pro.Name = name.replace(oldMap.get(pro.Id).Week_ending__c.format(),pro.Week_ending__c.format());
                    if(pro.Name.length()>80){
                        pro.Name = pro.Name.substring(0, 80);
                    }
                    System.debug('>>>> replaced date>>>' + pro.Name);
                    System.debug('>>>> replaced date>>>' + pro.Week_ending__c.format());
                    System.debug('>>>> replaced date>>>' + oldMap.get(pro.Id).Week_ending__c.format());
                }
            }
        }
    }

    /*
     * Created by - Himanshu Shrimali
     * Date - 29-05-2020
     * This method populates Portfolio Manager on this record
     */
    public static void populatePortfolioManager(List<Project_Status_Report__c> newList, Map<Id, Project_Status_Report__c> oldMap) {
        Set<Id> portfolioIdSet = new Set<Id>();
        Map<Id, Id> portfolioToManagerIdMap = new Map<Id, Id>();
        for(Project_Status_Report__c projectStatusReport : newList) {
            if(projectStatusReport.Project__r.Portfolio__c!=null) {
                System.debug('Portfolio: '+projectStatusReport.Project__r.Portfolio__c);
                portfolioIdSet.add(projectStatusReport.Project__r.Portfolio__c);
            }
        }

        List<Portfolio__c> portfolioList = new List<Portfolio__c>([SELECT Id, Portfolio_Manager__c FROM Portfolio__c WHERE Id IN: portfolioIdSet]);
        for(Portfolio__c portfolio : portfolioList) {
            if(portfolio.Portfolio_Owner__c!=null) {
                portfolioToManagerIdMap.put(portfolio.Id, portfolio.Portfolio_Manager__c);
            }
        }

        System.debug('Portfolio: '+portfolioToManagerIdMap);

        for(Project_Status_Report__c projectStatusReport : newList) {
            if(projectStatusReport.Project__r.Portfolio__c!=null) {
                projectStatusReport.Portfolio_Manager__c = portfolioToManagerIdMap.get(projectStatusReport.Project__r.Portfolio__c);
            }
        }
    }

	/*
     * Created by - Himanshu Shrimali
     * Date - 24-06-2020
     * This method calls future method to sync Path to Green fields on PSR and GUS Project
     */
    public static void updatePathToGreen(List<Project_Status_Report__c> newList, Map<Id, Project_Status_Report__c> oldMap) {
        Set<Id> psrIdSet = new Set<Id>();
        for(Project_Status_Report__c psrRecord : newList) {
            if(!psrRecord.Approved__c && (oldMap == null || (oldMap !=null && (psrRecord.Budget_Path_to_Green__c != oldMap.get(psrRecord.Id).Budget_Path_to_Green__c || psrRecord.Schedule_Path_to_Green__c != oldMap.get(psrRecord.Id).Schedule_Path_to_Green__c)))) {
                psrIdSet.add(psrRecord.Id);
            }
        }
        
        if(psrIdSet!=null && !psrIdSet.isEmpty()) {
            PRT_ProjectStatusReportTriggerHelper.syncPathToGreenOnProject(psrIdSet);
        }      
    }
    
    public static void syncPathToGreenOnProject(Set<Id> psrIdSet) {
        List<Project_Status_Report__c> psrList = new List<Project_Status_Report__c>([SELECT Id, Budget_Path_to_Green__c, Schedule_Path_to_Green__c, Project__c, Project__r.GUS_Project__c
                                                                                     FROM Project_Status_Report__c
                                                                                     WHERE Id IN: psrIdSet]);
        Map<Id, String> projIdToPathGreenMap = new Map<Id, String>();
        for(Project_Status_Report__c psrRec : psrList) {
            String budgetPathToGreen = '';
            String schedulePathToGreen = '';
            if(psrRec.Budget_Path_to_Green__c != null) {
                budgetPathToGreen = psrRec.Budget_Path_to_Green__c; 
            }
            if(psrRec.Schedule_Path_to_Green__c != null) {
                schedulePathToGreen = psrRec.Schedule_Path_to_Green__c; 
            }
            if(psrRec.Project__r != null) {
                projIdToPathGreenMap.put(psrRec.Project__c, budgetPathToGreen+' '+schedulePathToGreen);
            }
        }
        
        List<Project__c> updateProjList = new List<Project__c>();
        List<Project__c> projectList = new List<Project__c>([SELECT Id, Path_to_Green__c
                                                             FROM Project__c
                                                             WHERE Id IN: projIdToPathGreenMap.keySet()]);
        for(Project__c project : projectList) {
            project.Path_to_Green__c = projIdToPathGreenMap.get(project.Id);
            updateProjList.add(project);
        }          
        
        if(updateProjList!=null && !updateProjList.isEmpty()) {
            UPDATE updateProjList;
        }
    }
}
public with sharing class PopcrabPortal_ScannerNewsController {
	public boolean ShowScannerNews {get;set;}
	public string ScannerNewsText {get;set;}
	
	public PopcrabPortal_ScannerNewsController(){
		ShowScannerNews=false;
		ScannerNewsText='';
		if(chimera__GeneralPortalSettings__c.getInstance('ScannerNewsDisabled')==null || chimera__GeneralPortalSettings__c.getInstance('ScannerNewsDisabled').chimera__Value__c=='Yes')
        {
        	ShowScannerNews=false;
        }
        else if(chimera__GeneralPortalSettings__c.getInstance('ScannerNewsDisabled').chimera__Value__c!='Yes')
        	ShowScannerNews=true;
        	
       	ScannerNewsText=chimera__GeneralPortalSettings__c.getInstance('ScannerNewsText')!=null?chimera__GeneralPortalSettings__c.getInstance('ScannerNewsText').chimera__Value__c:'';
       	
	}
	public PageReference save(){
		if(chimera__GeneralPortalSettings__c.getInstance('ScannerNewsDisabled')!=null)
		{
			chimera__GeneralPortalSettings__c gps=new chimera__GeneralPortalSettings__c(id=chimera__GeneralPortalSettings__c.getInstance('ScannerNewsDisabled').id);
			gps.chimera__Value__c=ShowScannerNews==true?'No':'Yes';
			update gps;
		}
		else
		{
			chimera__GeneralPortalSettings__c gps=new chimera__GeneralPortalSettings__c(name='ScannerNewsDisabled');
			gps.chimera__Value__c=ShowScannerNews==true?'No':'Yes';
			insert gps;
		}
		
		if(chimera__GeneralPortalSettings__c.getInstance('ScannerNewsText')!=null)
		{
			chimera__GeneralPortalSettings__c gpsText=new chimera__GeneralPortalSettings__c(id=chimera__GeneralPortalSettings__c.getInstance('ScannerNewsText').id);
			gpsText.chimera__Value__c=ScannerNewsText;
			update gpsText;
		}
		else
		{
			chimera__GeneralPortalSettings__c gpsText=new chimera__GeneralPortalSettings__c(name='ScannerNewsText');
			gpsText.chimera__Value__c=ScannerNewsText;
			insert gpsText;
		}
		
		return null;
	}
}
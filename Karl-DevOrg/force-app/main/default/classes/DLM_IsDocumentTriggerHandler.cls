/***********************************************************
* Handler class for DLM_IsDocumentTrigger
* Created by    - Swarnima Singh Mandhata
* Date      - 02 July 2020
************************************************************/
public with sharing class DLM_IsDocumentTriggerHandler {
    public static void beforeInsert(List<IS_Document__c> newList){
       
    }
    public static void beforeUpdate(List<IS_Document__c> newList, Map<id,IS_Document__c> oldMap){
        
    }
    public static void afterInsert(List<IS_Document__c> newList){
       
        if(!(System.isBatch() || System.isFuture())){
            DLM_IsDocumentTriggerHelper.setProductTag(newList, null);
            DLM_IsDocumentTriggerHelper.createWorkRecord_OnWorkflowRule(newList, null);
        }   
    }
    
    public static void afterUpdate(List<IS_Document__c> newList, Map<id,IS_Document__c> oldMap){
        if(!(System.isBatch() || System.isFuture())){
            DLM_IsDocumentTriggerHelper.createWorkRecord_OnWorkflowRule(newList, oldMap);
            DLM_IsDocumentTriggerHelper.setProductTag(newList, oldMap); 
        }
      
    }
}
public class CSIRT_TenYearDelete implements Schedulable {
	public void execute(SchedulableContext sc){
        String[] RTFilter = New String[]{'CSIRT%', 'STAR%', 'eDiscovery%'};
        list<RecordType> csirtRTlist = [SELECT ID FROM RecordType WHERE DeveloperName LIKE :RTFilter AND SobjectType='Case']; 
        integer year = Date.today().year()-10;
        integer month = Date.today().month();
        list<Case> caseList = [SELECT LastModifiedDate FROM Case WHERE RecordTypeID IN :csirtRTlist AND CALENDAR_YEAR(LastModifiedDate)= :year AND CALENDAR_MONTH(LastModifiedDate) = :month];
        Delete caseList;
    }

}
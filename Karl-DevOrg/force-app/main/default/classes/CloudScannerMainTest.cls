/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class CloudScannerMainTest {

    static testMethod void CloudScannerMainTest1() {
        User PortalUser=CreatePortalUser('TestCloudScannerAccount');
        User SysAdmin=[select id from User where id=:Userinfo.getUserId()];
        Id MainAccountid=[select id from Account where name='TestCloudScannerAccount'].id;
        System.RunAs(SysAdmin)
        {
            Contributing_Org__c co=new Contributing_Org__c();
            co.Account__c=MainAccountid;
            co.Org_Id__c='00DT0000000DpvcMAC';
            co.Publisher_Org__c=true;
            insert co;
            co=new Contributing_Org__c();
            co.Account__c=MainAccountid;
            co.Org_Id__c='00DT0000000Dpv1MAC';
            insert co;
            co=new Contributing_Org__c();
            co.Account__c=MainAccountid;
            co.Org_Id__c='00DT0000000rpv1MAA';
            insert co;
            CodeScan__c cs=new CodeScan__c();
            cs.Name='CloudScan';
            cs.X62OrgUsername__c= UserInfo.getUserEmail();
            cs.OrgID__c='00DT0000000Dpvc';
            cs.Username__c='test@test.com';
            cs.Account_Type__c='SecurityReview';
            insert cs;
            cs=new CodeScan__c();
            cs.Name='CloudScan';
            cs.X62OrgUsername__c= UserInfo.getUserEmail();
            cs.OrgID__c='00DT0000000rpv2';
            cs.Username__c='test@test2.com';
            cs.Account_Type__c='SecurityReview';
            insert cs;
            Scan_Info__c si=new Scan_Info__c();
            si.Name='test4';
            insert si;
            system.debug('si:'+si.id);
            Attachment newAtt=new Attachment();
            newAtt.Name='hahah.html.zip';
            newAtt.parentid=si.id;
            newAtt.body=blob.valueof('test');
            insert newAtt;
            Scan_Queue__c sq=new Scan_Queue__c();
            sq.CodeScan__c=cs.id;
            sq.Scan_Info__c=si.id;
            insert sq;
            
            
        }
        CloudScannerMainController csmc;
        string attachid=[select id from Attachment where name='hahah.html.zip'].id;
        System.RunAs(PortalUser)
        {
            csmc=new CloudScannerMainController();
            csmc.Scanorgid='00DT0000000Dpvc';
            csmc.username='test@test3.com';
            csmc.SubmitScan();
            system.assertEquals(3,[select credits__c from Contributing_Org__c where Org_Id__c='00DT0000000DpvcMAC'].credits__c);
            csmc.searchterm='test@test.com';
            csmc.Search();
            system.assertEquals(1, csmc.Scans.size());
            csmc.reset();
            csmc=new CloudScannerMainController();
            csmc.Scanorgid='00DT0000000rpv1';
            csmc.username='test@test3.com';
            csmc.SubmitScan();
            system.assertEquals(3,[select credits__c from Contributing_Org__c where Org_Id__c='00DT0000000rpv1MAA'].credits__c);
            
            csmc=new CloudScannerMainController();
            csmc.Scanorgid='00DT0000000rpv1';
            csmc.username='test@test3.com';
            csmc.SubmitScan();
            
            system.assertEquals(3,[select credits__c from Contributing_Org__c where Org_Id__c='00DT0000000rpv1MAA'].credits__c);
            
            csmc=new CloudScannerMainController();
            csmc.Scanorgid='00DT0000000rpv1';
            csmc.username='test@test3.com';
            csmc.SubmitScan();
            
            system.assertEquals(3,[select credits__c from Contributing_Org__c where Org_Id__c='00DT0000000rpv1MAA'].credits__c);
            
            
            string csrf=CloudScannerMainController.genCSRF(attachid);
            
            PageReference pageRef = Page.ScannerReportDownload;
            pageRef.getParameters().put('id', attachid);
            pageRef.getParameters().put('csrf', csrf);
            Test.setCurrentPage(pageRef);
            ScannerReportDownloadController srdc=new ScannerReportDownloadController();
            System.AssertNotEquals(null,srdc.init());
            srdc.init();
            system.debug(srdc.bodyresponse);
            
            Boolean AttachmentAlreadyCreated=false;
            WrapperAttachment__c Wa=new WrapperAttachment__c();
            for(WrapperAttachment__c waitem:[select id from WrapperAttachment__c where Original_Attachment_Id__c = :attachid and OwnerId = :Userinfo.getUserId() ])
            {
                Wa=waitem;
                AttachmentAlreadyCreated=true;
            }
            system.assertEquals(true,AttachmentAlreadyCreated);
            
            pageRef = Page.ScannerReportDownload;
            pageRef.getParameters().put('id', attachid);
            pageRef.getParameters().put('csrf', 'FakeCSRF');
            Test.setCurrentPage(pageRef);
            
            srdc=new ScannerReportDownloadController();
            System.AssertEquals(null,srdc.init());
        }
    }
    public static User CreatePortalUser(string AccountName){
        Account a = new Account(Name=AccountName);
        insert a;
        Contact c = new Contact(AccountId=a.Id, LastName='Test111',email='test-user@fakeemail.com');
        insert c;
        String userProfile = 'Chimera HVCP';

        List<Profile> p = [SELECT Id FROM profile WHERE name =: userProfile]; 
        User u = new User( email='test-user@fakeemail.com', contactid = c.Id, profileid = p[0].id, 
                UserName='test-user@fakeemail.com', alias='tuser1', CommunityNickName='tuser1', 
        TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', 
        LanguageLocaleKey='en_US', FirstName = 'Test', LastName = 'User' );
        insert u;
        return u;
    }
}
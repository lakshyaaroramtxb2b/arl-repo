@isTest
public class PRT_GUSSyncHelperTest {
    @testSetup
    public static void testSetup(){
        List<User> userList = new List<User>();
        userList = PRT_TestDataFactory.createUsers(1,'System Administrator',true);
        System.runAs(userList[0]){
            PRT_Integration_Settings__c integrationSetting = new PRT_Integration_Settings__c();
            integrationSetting.GUS_Program_Integration__c = true;
            integrationSetting.Disable_Program_Trigger__c = false;
            insert integrationSetting; 
            List<Portfolio__c> portfolioDataList = PRT_TestDataFactory.createPortfolio(1,true);
        } 
    }
    @isTest
    public static void createUpdatePortfolioInGUSTest(){
        Set<Id> portfolioIdSet = new Set<Id>();
        Set<Id> userIdSet = new Set<Id>();
        Map<Id,String> idVsStringMap = new Map<Id,String>();
         List<User> userList = new List<User>([SELECT Id,EmployeeNumber FROM USER WHERE Email Like 'PRTUser%']);
        for(user u:userList){
            userIdSet.add(u.id);
        }
        List<Portfolio__c> portfolioDataList = PRT_TestDataFactory.createPortfolio(1,false);
        portfolioDataList[0].Portfolio_Active__c =true;
        portfolioDataList[0].Portfolio_Goals__c = 'test Goals';
        portfolioDataList[0].Portfolio_Manager__c =userList[0].id;
        portfolioDataList[0].Portfolio_Owner__c = userList[0].id;
        portfolioDataList[0].Portfolio_Health__c = 'On Track';
        portfolioDataList[0].Portfolio_Summary__c = 'test Summary';
        portfolioDataList[0].GUS_Portfolio__c = '';
        for(Portfolio__c port: portfolioDataList){
            portfolioIdSet.add(port.id);
        }
        insert portfolioDataList;
        List<User__x> userRecordList = new List<User__x>();
        User__x user = new User__x();
        user.ExternalId = 'ex1000000000000001';
        user.EmployeeNumber__c = userList[0].EmployeeNumber;
        userRecordList.add(user);
        PRT_GUSSyncHelper.mockallGusUserList.addAll(userRecordList);
        
        List<PPM_Portfolio_c__x> portfolioList = new List<PPM_Portfolio_c__x>();
        PPM_Portfolio_c__x gusPortfolio = new PPM_Portfolio_c__x();
        gusPortfolio.Portfolio_Goals_c__c = String.valueOf(portfolioDataList[0].id);
        gusPortfolio.Portfolio_Health_c__c = 'On Track';
        gusPortfolio.Name__c = 'Test Portgolio Name1';
        gusPortfolio.Portfolio_Manager_c__c = userList[0].id;
        gusPortfolio.Portfolio_Owner_c__c = userList[0].id;
        gusPortfolio.Portfolio_Summary_c__c = 'test summary portfolio';
        portfolioList.add(gusPortfolio);
        PRT_GUSSyncHelper.mockallGusPortfolioList.addAll(portfolioList);
        
        idVsStringMap.put(portfolioDataList[0].id,String.valueOf(portfolioDataList[0].id));
        test.startTest();
        PRT_GUSSyncHelper.createUpdatePortfolioInGUS(portfolioIdSet,userIdSet);
        PRT_GUSSyncHelper.reupdatePortfolioRecords(portfolioIdSet,idVsStringMap);
        test.stopTest();
    }
    
    @isTest
    public static void createUpdateProgramInGUSTest(){
        Set<Id> programIdSet = new Set<Id>();
        Set<Id> userIdSet = new Set<Id>();
        Map<Id,String> idVsStringMap = new Map<Id,String>();
         List<User> userList = new List<User>([SELECT Id,EmployeeNumber FROM USER WHERE Email Like 'PRTUser%']);
        for(user u:userList){
            userIdSet.add(u.id);
        }
        List<Portfolio__c> portfolioDataList = [Select Id FROM Portfolio__c LIMIT 1];
        program__c prog = new program__c();
        prog.Name = 'testProgramforupdate';
        prog.Program_Status__c = 'Active';
        prog.Program_Owner__c = userList[0].id;
        prog.Program_Manager__c = userList[0].id;
        prog.Executive_Sponsor__c = userList[0].id;
        prog.Portfolio__c = portfolioDataList[0].Id;
        //prog.Parent_Program__c = progRec.Id;
        prog.Budget_Allocated__c = 23.00;
        prog.Program_Summary__c ='test text Summary';
        insert prog;
        programIdSet.add(prog.id);
        
        List<User__x> userRecordList = new List<User__x>();
        User__x user = new User__x();
        user.ExternalId = 'ex1000000000000001';
        user.EmployeeNumber__c = userList[0].EmployeeNumber;
        userRecordList.add(user);
        PRT_GUSSyncHelper.mockallGusUserList.addAll(userRecordList);
             
        List<PPM_Program_c__x> progamList = new List<PPM_Program_c__x>();
        PPM_Program_c__x program = new PPM_Program_c__x();
        program.Name__c = 'Test Name';
        //program.ExternalId = prog.id;
        program.Program_Goals_c__c = prog.id ;
        program.Program_Health_c__c = 'Completed';
        program.Program_Health_Comments_c__c = '';
        program.Program_Manager_c__c = userList[0].id;
        program.Program_Owner_c__c = userList[0].id;
        program.Program_Summary_c__c = 'TEst';
        program.Executive_Sponsor_c__c = userList[0].id;
        program.Portfolio_c__c = portfolioDataList[0].Id;
        progamList.add(program);
        PRT_GUSSyncHelper.mockallGusProgramList.addAll(progamList);
        
        idVsStringMap.put(prog.id,String.valueOf(prog.id));
        test.startTest();
        PRT_GUSSyncHelper.createUpdateProgramInGUS(programIdSet,userIdSet);
        PRT_GUSSyncHelper.reupdateProgramRecords(programIdSet,idVsStringMap);
        test.stopTest();
        
    }
    
    @isTest
    public static void createUpdateProjectInGUSTest(){
        Set<Id> porfolioIdSet = new Set<Id>();
        Set<Id> programIdSet = new Set<Id>();
        Set<Id> projectIdSet = new Set<Id>();
        Set<Id> userIdSet = new Set<Id>();
        List<Portfolio__c> portfolioDataList = [Select Id FROM Portfolio__c LIMIT 1];
        porfolioIdSet.add(portfolioDataList[0].id);
        Map<Id,String> idVsStringMap = new Map<Id,String>();
         List<User> userList = new List<User>([SELECT Id,EmployeeNumber FROM USER WHERE Email Like 'PRTUser%']);
        for(user u:userList){
            userIdSet.add(u.id);
        }
        List<program__c> programList = new List<program__c>();
        program__c prog = new program__c();
        prog.Name = 'testProgramforupdate';
        programList.add(prog);
        insert programList;
        programIdSet.add(programList[0].id);
          List<Project__c> projectIList = new List<Project__c>(PRT_TestDataFactory.createProjects(2,false));
        for(integer i=0; i<2; i++){
            projectIList[i].Program__c = programList[0].id;
            projectIList[i].Project_Manager__c = userList[0].id;
            projectIList[i].Executive_Sponsor_Internal__c = userList[0].id;
            projectIList[i].Sponsor_Internal__c = userList[0].id;
            projectIList[i].Overall_Project_Health__c = 'On Track';
            
        }
        insert projectIList; 
        projectIdSet.add( projectIList[0].id);
        System.debug('projectIdSet' + projectIdSet);
        
        List<User__x> userRecordList = new List<User__x>();
        User__x user = new User__x();
        user.ExternalId = 'ex1000000000000001';
        user.EmployeeNumber__c = userList[0].EmployeeNumber;
        userRecordList.add(user);
        PRT_GUSSyncHelper.mockallGusUserList.addAll(userRecordList);
        
        List<PPM_Project_c__x> projectList = new List<PPM_Project_c__x>();
        PPM_Project_c__x project = new PPM_Project_c__x();
        //project.ExternalId = projectIList[0].Id;
        project.Name__c = 'Test';
        project.Actual_End_Date_c__c = Date.today()+5;
        project.Actual_Start_Date_c__c = Date.today();
        project.Description_c__c = 'Test';
        project.Project_Health_c__c = 'Completed';
        project.Build_Planned_End_Date_c__c = Date.today()+5;
        project.Business_Case_c__c = 'Test Data';
        project.Customer_POC_c__c = 'Test';
        project.Delivery_Team_Priority_c__c = 23;
        project.Project_Deliverables_c__c = 'TestData';
        project.Project_Health_Comments_c__c = 'TEst Data';
        projectList.add(project);
        
        PRT_GUSSyncHelper.mockallGusProjectList.addAll(projectList);
        test.startTest();
        PRT_GUSSyncHelper.createUpdateProjectInGUS(projectIdSet,userIdSet,programIdSet,porfolioIdSet);
        try{
            PRT_GUSSyncHelper.reupdateProjectRecords(projectIdSet);
        }catch(exception e){
            System.debug('Required External Id');
        }
        PRT_GUSSyncHelper.syncProgramName(programIdSet);
        test.stopTest();
    }
    
    @isTest
    public static void createEpicRecordsOnGUSTest(){
         List<User> userList = new List<User>([SELECT Id,EmployeeNumber FROM USER WHERE Email Like 'PRTUser%']);
        Set<id> projectIDSet = new Set<Id>();
        List<Project__c> projectList = new List<Project__c>(PRT_TestDataFactory.createProjects(1, false));
        projectList[0].Status__c =  PRT_Constants.PRT_CR_STATUS_PROJECTKICKOFF;
        projectList[0].Projected_Date_of_Completion__c = date.today()+365;
        projectList[0].Project_Start_Date__c = date.today();
        projectIDSet.add(projectList[0].id);
        insert projectList;
        
        List<Milestone_PRT__c> milestoneList = new List<Milestone_PRT__c>(PRT_TestDataFactory.createMilestone(5,projectList[0].id,false));
        milestoneList[0].description__c = 'test description';
        //milestoneList[0].GUS_Epic__c = epicRec.Id;
        milestoneList[0].Start_Date__c = date.today();
        milestoneList[0].Due_Date__c = date.today()+20;
        insert milestoneList; 
        
        List<ADM_Epic_c__x> epicGusList = new List<ADM_Epic_c__x>();
        ADM_Epic_c__x epicRec = new ADM_Epic_c__x();
        epicRec.Actual_End_Date_c__c = date.today()+1;
        epicRec.Actual_Start_Date_c__c = date.today();
        epicRec.End_Date_c__c = date.today()+1;
        epicRec.Start_Date_c__c = date.today();
        epicRec.Description_c__c = 'test description';
        epicRec.Name__c = 'test Epic Name';
        epicRec.Health_c__c = 'On Track';
        epicRec.Success_Criteria_c__c = milestoneList[0].id;
        epicRec.Planned_End_Date_c__c=date.today()+23;
        
        epicGusList.add(epicRec);
        PRT_GUSSyncHelper.mockallGusEpicGusList.addAll(epicGusList);
        
         
        test.startTest();
        PRT_GUSSyncHelper.createEpicRecordsOnGUS(projectIDSet);
        test.stopTest();
        
    }
}
/***********************************************************
* Helper class for PRT_RiskTrigger
* Created by 	- Prashant Gupta
************************************************************/
public class PRT_RiskTriggerHelper {
    /***********************************************************
    * This method updates Current_State__c based on 
    * risk's Became_Issue_from_Risk__c field
    * Created by 	- Prashant Gupta
    * Updated by - Himanshu Shrimali (Removed this method Task-09266)
    ************************************************************/
    // public static void updateCurrentState(List<Risk__c> newList, Map<id,Risk__c> oldMap){
    //     for(Risk__c risk : newList){
    //         if(risk.Became_Issue_from_Risk__c 
    //            &&(oldMap==null 
    //               || risk.Became_Issue_from_Risk__c!=oldMap.get(risk.id).Became_Issue_from_Risk__c))
    //             risk.Current_State__c = PRT_Constants.PRT_RISK_CURRENT_STATE_ISSUE;
    //     }
    // }

    // This method updates the Slippage Comment of Project associated to Risk
    public static void updateSlippageCommentOfProject(List<Risk__c> newList, Map<Id, Risk__c> oldMap) {
        List<Project__c> updateProjectList = new List<Project__c>();
        Map<Id, List<String>> projIdToOldDescListMap = new Map<Id, List<String>>();
        Map<Id, List<String>> projIdToDescListMap = new Map<Id, List<String>>();
        Map<Id, Id> riskIdToProjectIdMap = new Map<Id, Id>();
        Set<Id> projectIdSet = new Set<Id>();
        for(Risk__c risk : newList) {
            if(risk.Project__c != null) {
                if(!riskIdToProjectIdMap.values().contains(risk.Project__c)) {
                    riskIdToProjectIdMap.put(risk.Id, risk.Project__c);
                }
                if((oldMap==null && risk.Status__c != 'Closed') || (oldMap!=null && risk.Status__c != 'Closed' && (oldMap.get(risk.Id).Description__c != risk.Description__c || oldMap.get(risk.Id).Status__c == 'Closed'))) {
                    if(!projIdToDescListMap.containsKey(risk.Project__c)) {
                        projIdToDescListMap.put(risk.Project__c, new List<String>());
                    }
                    projIdToDescListMap.get(risk.Project__c).add(risk.Description__c);
                }
                if(oldMap != null) {
                    if(oldMap.get(risk.Id).Description__c != risk.Description__c || (oldMap.get(risk.Id).Status__c != risk.Status__c && risk.Status__c == 'Closed')) {
                        if(!projIdToOldDescListMap.containsKey(risk.Project__c)) {
                            projIdToOldDescListMap.put(risk.Project__c, new List<String>());
                        }
                        projIdToOldDescListMap.get(risk.Project__c).add(oldMap.get(risk.Id).Description__c);
                    }
                }
            }
        }
        System.debug('projIdToOldDescListMap: '+projIdToOldDescListMap);
        System.debug('projIdToDescListMap: '+projIdToDescListMap);

        List<Project__c> projectList = new List<Project__c>([SELECT Id, Slippage_Comments__c FROM Project__c 
                                                             WHERE Id IN: riskIdToProjectIdMap.values()]);
        System.debug('project: '+projectList);
        if(projectList!=null && !projectList.isEmpty()) {
            for(Project__c project : projectList) {
                String finalDescription = '';
                Project__c updateProject = new Project__c();
                updateProject.Id = project.Id;
                finalDescription = project.Slippage_Comments__c == null ? '' : project.Slippage_Comments__c;

                System.debug('Actual Desc: '+finalDescription);
                
                if(oldMap!=null && projIdToOldDescListMap!=null && !projIdToOldDescListMap.isEmpty()) {
                    for(String oldDesc : projIdToOldDescListMap.get(project.Id)) {
                        if(oldDesc != null && finalDescription != '') {
                            String removeDesc = oldDesc.replaceAll('<[/a-zAZ0-9]*>','');
                            finalDescription = String.valueOf(finalDescription).remove('- '+removeDesc);
                        }
                    }
                }  
                System.debug('After Deleted Desc: '+finalDescription);
                if(projIdToDescListMap!=null && !projIdToDescListMap.isEmpty()) {
                    for(String description : projIdToDescListMap.get(project.Id)) {
                        if(description != null) {
                         	finalDescription += '- '+String.valueOf(description)+' ';   
                        }
                    }
                }     

                finalDescription = finalDescription.replaceAll('<[/a-zAZ0-9]*>','');
                System.debug('Updated Desc: '+finalDescription);

                updateProject.Slippage_Comments__c = finalDescription;
                updateProjectList.add(updateProject);
            }
        }
        
        if(updateProjectList != null && !updateProjectList.isEmpty()) {
            UPDATE updateProjectList;
        }
    }

    /***********************************************************
    * This method updates sponsors on the risk from Project Record
    * Created by 	- Prashant Gupta
    ************************************************************/
    public static void updateSponsorFromProject(List<Risk__c> newList, Map<id,Risk__c> oldMap){
        List<Risk__c> riskList = new List<Risk__c>();
        Set<id> projectIdSet = New Set<id>();
        for(Risk__c risk : newList){
            if(oldMap==null && risk.Project__c!=null){
                riskList.add(risk);
                projectIdSet.add(risk.Project__c);
            }
        }
        Map<id,Project__c> projectMap = new Map<id,Project__c>([SELECT id, Sponsor_Internal__c FROM Project__c WHERE id in: projectIdSet]);
        for(Risk__c risk : riskList){
            if(projectMap.containsKey(risk.Project__c)){
                risk.Project_Sponsor__c = projectMap.get(risk.Project__c).Sponsor_Internal__c;
            }
        }
    }

    public static void checkRequiredFields(List<Risk__c> newList, Map<id,Risk__c> oldMap){
        Map<String,Integer> statusVsNumber = new Map<String,Integer>();
        statusVsNumber.put(PRT_Constants.PRT_RISK_STATUS_New,1);
        statusVsNumber.put(PRT_Constants.PRT_RISK_STATUS_ASSIGNED,2);
        statusVsNumber.put(PRT_Constants.PRT_RISK_STATUS_IN_PROGRESS,3);
        statusVsNumber.put(PRT_Constants.PRT_RISK_STATUS_CLOSED,4);
        statusVsNumber.put(PRT_Constants.PRT_RISK_STATUS_BLOCKED,5);
        System.debug('Map Of Risk Status' + statusVsNumber);

        String type='Risk__c';
        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Schema.SObjectType leadSchema = schemaMap.get(type);
        Map<String, Schema.SObjectField> fieldMap = leadSchema.getDescribe().fields.getMap();
        boolean hasError = False;

        for(Risk__c riskRecord:newList){
            String error= 'Please populate these fields : ';
            if(oldmap!=null && riskRecord.Status__c !=oldMap.get(riskRecord.id).Status__c){
                if(statusVsNumber.get(riskRecord.Status__c) >= 2){
                    if(riskRecord.Name==null || riskRecord.Name == ''){
                        error += fieldMap.get('Name').getDescribe().getLabel() + ', ';
                        hasError = True;
                    }
                    if(riskRecord.Project__c == null){
                        error += fieldMap.get('Project__c').getDescribe().getLabel() + ', ';
                        hasError = True;
                    }
                    if(riskRecord.Description__c==null || riskRecord.Description__c == ''){
                        error += fieldMap.get('Description__c').getDescribe().getLabel() + ', ';
                        hasError = True;
                    }
                    if(riskRecord.Type__c==null || riskRecord.Type__c == ''){
                        error += fieldMap.get('Type__c').getDescribe().getLabel() + ', ';
                        hasError = True;
                    }
                }
                if(statusVsNumber.get(riskRecord.Status__c) >= 3){
                    if(riskRecord.Due_Date__c==null){
                        error += fieldMap.get('Due_Date__c').getDescribe().getLabel() + ', ';
                        hasError = True;
                    }
                    if(riskRecord.Probability__c==null){
                        error += fieldMap.get('Probability__c').getDescribe().getLabel() + ', ';
                        hasError = True;
                    }
                    if(riskRecord.Impact__c==null || riskRecord.Impact__c == ''){
                        error += fieldMap.get('Impact__c').getDescribe().getLabel() + ', ';
                        hasError = True;
                    }
                }
                if(statusVsNumber.get(riskRecord.Status__c) >= 4){
                    if(riskRecord.Response_Strategy__c==null){
                        error += fieldMap.get('Response_Strategy__c').getDescribe().getLabel() + ', ';
                        hasError = True;
                    }
                    if(riskRecord.Action__c==null){
                        error += fieldMap.get('Action__c').getDescribe().getLabel() + ', ';
                        hasError = True;
                    }
                }
                if(hasError){
                    if(error.endsWith(', ')){
                        error= error.substringBeforeLast(', ');
                    }
                    riskRecord.addError(error);
                }  
            }
        }
    }    
}
/***********************************************************
 * Test Class for PRT_MilestoneTriggerHelper
* Created by 	- Prashant Gupta
* Date 		- 09-09-2019
************************************************************/
@isTest
public class PRT_MilestoneTriggerHelperTest {
    @testSetup
    public static void testSetup(){
        List<User> userList = new List<User>();
        userList = PRT_TestDataFactory.createUsers(1,'System Administrator',true);
        System.runAs(userList[0]){
            PRT_Integration_Settings__c integrationSetting = new PRT_Integration_Settings__c();
            integrationSetting.GUS_Program_Integration__c = true;
            integrationSetting.Disable_Program_Trigger__c = false;
            insert integrationSetting; 
            List<Portfolio__c> portfolioDataList = PRT_TestDataFactory.createPortfolio(1,true);
        } 
    }
    static testMethod void milestoneDateUpdateTest(){
        List<Project__c> projectList = new List<Project__c>(PRT_TestDataFactory.createProjects(1, false));
        projectList[0].Status__c =  PRT_Constants.PRT_CR_STATUS_PROJECTKICKOFF;
        projectList[0].Projected_Date_of_Completion__c = date.today()+1;
        projectList[0].Project_Start_Date__c = date.today();
        insert projectList;
        
        List<Milestone_PRT__c> childMilestoneList = new List<Milestone_PRT__c>(PRT_TestDataFactory.createMilestone(5,projectList[0].id,true)); 
        List<Milestone_PRT__c> parentMilestoneList = new List<Milestone_PRT__c>(PRT_TestDataFactory.createMilestone(1,projectList[0].id,true));
        List<Milestone_Dependency__c> milestoneDependencyList= new List<Milestone_Dependency__c>(PRT_TestDataFactory.createMilestoneDependency(5,false));
        
        for(Integer i=0;i<5;i++){
          
            milestoneDependencyList[i].Dependent_Milestone__c = childMilestoneList[i].id;
            milestoneDependencyList[i].Milestone__c = parentMilestoneList[0].id;
        }
        insert milestoneDependencyList;
        
        test.startTest();
        try{
        Integer day = 1;
        for(Milestone_PRT__c mile : childMilestoneList){
            mile.Due_Date__c = Date.newInstance(2019, 9, day++) ;
        }
        
        for(Milestone_PRT__c mile : parentMileStoneList){
            mile.Start_Date__c = Date.newInstance(2019, 8, 25);
        }
        update childMileStoneList;
        update parentMileStoneList;
        }
        catch(Exception e){
            System.assert(e.getMessage().contains('Parent Milestone Start Date must be on or after dependent'));
            
        }
        
        test.stopTest();
    }
    
    static testMethod void testCheckRequiredFields(){
         List<Project__c> projectList = new List<Project__c>(PRT_TestDataFactory.createProjects(1, false));
        projectList[0].Status__c =  PRT_Constants.PRT_CR_STATUS_PROJECTKICKOFF;
        projectList[0].Projected_Date_of_Completion__c = date.today()+1;
        projectList[0].Project_Start_Date__c = date.today();
        insert projectList;
        List<Milestone_PRT__c> childMilestoneList = new List<Milestone_PRT__c>(PRT_TestDataFactory.createMilestone(1,projectList[0].id,true));
        childMilestoneList[0].Name = 'update Name';
        childMilestoneList[0].status__c = PRT_Constants.PRT_MILE_STATUS_COMPLETE;
        test.startTest();
        try{
            update childMilestoneList;
        }
        catch(exception e){}
        
        System.assertEquals(childMilestoneList[0].due_date__c==null,true);
        test.stopTest();
        
        
    }
    
    public static testMethod List<ADM_Epic_c__x> testUpdateExternalGusEpicRecords(){
        List<Project__c> projectList = new List<Project__c>(PRT_TestDataFactory.createProjects(1, false));
        projectList[0].Status__c =  PRT_Constants.PRT_CR_STATUS_PROJECTKICKOFF;
        projectList[0].Projected_Date_of_Completion__c = date.today()+365;
        projectList[0].Project_Start_Date__c = date.today();
        insert projectList;
        
        List<ADM_Epic_c__x> epicGusList = new List<ADM_Epic_c__x>();
        ADM_Epic_c__x epicRec = new ADM_Epic_c__x();
        epicRec.Actual_End_Date_c__c = date.today()+1;
        epicRec.Actual_Start_Date_c__c = date.today();
        epicRec.End_Date_c__c = date.today()+1;
        epicRec.Start_Date_c__c = date.today();
        epicRec.Description_c__c = 'test description';
        epicRec.Name__c = 'test Epic Name';
        epicRec.Health_c__c = 'On Track';
        epicRec.Success_Criteria_c__c = 'test area test';
        epicRec.Planned_End_Date_c__c=date.today()+23;
        
        epicGusList.add(epicRec);
        
        List<Milestone_PRT__c> milestoneList = new List<Milestone_PRT__c>(PRT_TestDataFactory.createMilestone(5,projectList[0].id,false));
        milestoneList[0].description__c = 'test description';
        milestoneList[0].GUS_Epic__c = epicRec.Id;
        
        milestoneList[0].Start_Date__c = date.today();
        milestoneList[0].Due_Date__c = date.today()+20;
        test.startTest();
        insert milestoneList;
        System.debug('gus id in milestone' + milestoneList[0].GUS_Epic__c);
        test.stopTest();
        
        return epicGusList;
        
        
    }
}
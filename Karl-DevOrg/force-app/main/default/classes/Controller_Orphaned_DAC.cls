public class Controller_Orphaned_DAC {

    public List<String> getOrphan()
    {
        
        List<String> rets = new List<String>();
        
       //Step1 build list of DACS with an associated DSECS. this relationship exists in the Detection_comparision records that stores DACS/DAC relationship
        //Step2 query all DSECS and build a list of DSECS are not in the list created in step1 i.e. create a list of DSECS with NO DACS
        
        //Detection_Comparison__c uses a string ID 'ValueToCompare__c' value for the ID to reffer to DSEC objects
        //Get Detection_Comparison__c objects that are looking for a specfic DSEC
        
        List <Detection_Comparison__c> comparisonRecords = [SELECT Detection_Security_Event_Criteria__r.id, ValueToCompare__c, DetectionStage__c FROM Detection_Comparison__c WHERE FieldToCompare__c = 'a373A000000PZ3O'];
        
        //list a
        List<Detection_Security_Event_Criteria__c> DSECsWithDacs = new List<Detection_Security_Event_Criteria__c>();
        //list b
        List<Detection_Security_Event_Criteria__c> DSECsWith_NO_Dacs = new List<Detection_Security_Event_Criteria__c>();
        
        //build a list of DACS with associated DSECS. we do this by extracting them form the Detection_comparision record that stores this relationship
        for( Detection_Comparison__c record : comparisonRecords) {
            
            //record.ValueToCompare__c // is the ID of the DSEC the comparision record is searching for
            //record.DetectionStage__r.DetectionAlertCriteria__c // is the ID of the DAC
        
            //If both values are not Null then ... the DSEC has a DAC
            //do check here and then add to master list
            DSECsWithDacs.add(record.Detection_Security_Event_Criteria__r);
        }
        
        //loop over all DSECS
        for( Detection_Security_Event_Criteria__c dsec : [SELECT Id,Name, Rule_Name__c, Owner.Name, status__c, CreatedDate 
                        FROM Detection_Security_Event_Criteria__c 
                        WHERE status__c != 'inactive'
                        ORDER BY Owner.Name])
        {
            boolean found = false;
        
            //loop over each DSEC record in DSECsWithDacs list
            for(Detection_Security_Event_Criteria__c record : DSECsWithDacs)
            {
                //The DSEC has a DAC
                if (record != NULL)
                {
                    if(dsec.Id == record.Id)
                    {
                        found = true;
                        break; //exit for loop                  
                    }
                }
            }

            //DSEC does not have a DAC as it was not in the list DSECsWithDacs
            if(found != true)
            {
                DSECsWith_NO_Dacs.add(dsec);
                                string link = 'https://security.my.salesforce.com/' + dsec.id; 
                rets.add(dsec.Name + ', ' + dsec.Owner.Name + ', ' + dsec.Rule_Name__c + ', ' + dsec.status__c + ', ' + dsec.CreatedDate + ', ' + link);
            }
        }
        
        //Finished we have the list DSECsWith_NO_Dacs
        //
        
        return rets;
    }
}
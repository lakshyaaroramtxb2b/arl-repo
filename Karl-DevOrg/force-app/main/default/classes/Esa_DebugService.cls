/**
 * Use this to log errors/warnings in the app.
 * This is an app-managed functionality, hence the need for "without sharing".
 */
public without sharing class Esa_DebugService {
    public static final string LEVEL_ERROR = 'Error';
    public static final string LEVEL_WARNING = 'Warning';
    public static final string LEVEL_INFO = 'Info';
    public static final string LEVEL_DEBUG = 'Debug';

    /**
     * Used to store queued messages which will be processed later in the request lifecycle.
     * Helpful for writing debug logs before a callout.  It's the caller's responsibility to
     * call processQueuedMessages method at the end of request lifecycle in Apex, preferably in a
     * finally block.
     * Not a great way to do this, but can't find anything better with the current platform limitations. #platform-gap
     */
    private static List<Esa_Debug_Log__c> queuedMessages = new List<Esa_Debug_Log__c>();

    /**
     * Determines if debug records should, by default, be immediately written
     * to database (false) or be queued for later bulk insert (true);
     */
    private static Boolean queueMessagesForWrite = false;

    public static void processQueuedMessages() {
        processQueuedMessages(true, false);
    }

    public static void processQueuedMessages(Boolean writeQueueNow, Boolean queueFutureMessages) {
        if (writeQueueNow) {
            if (queuedMessages.size() > 0) {
                try {
                    if (Test.isRunningTest()) {
                        System.debug('writing queued debug service log(s): ' + queuedMessages);
                    }
                    insert queuedMessages;
                    queuedMessages.clear();
                }
                catch (Exception e) {
                    System.debug('Exception in Esa_DebugService.write():  ' + e);
                    System.debug('Stack Trace:  ' + e.getStackTraceString());
                }
            }
        }
        queueMessagesForWrite = queueFutureMessages;
    }

    private static void write(String level, Integer severity, String sourceFile, String userId, String message, Exception ex) {
        write(level, severity, sourceFile, userId, message, ex, false);
    }

    public static void write(String Level, Integer severity, String sourceFile, String userId, String message, Exception ex, boolean queueMessageForWrite) {
        try {
            if (ex != null && ex instanceof Esa_Exceptions.Esa_NoDebugAppException) {
                // This exception does not contain any useful info, just return.
                System.debug('Exception doesn\'t contain any useful info');
                return;
            }
            Esa_Debug_Log__c log = new Esa_Debug_Log__c();
            log.Message__c = message;
            log.Source_File__c = sourceFile;
            log.User__c = userId;
            log.Level__c = level;
            if (ex != null) {
                log.Exception_Details__c = ex.getMessage();
                log.StackTrace__c = ex.getStackTraceString();
                log.TypeName__c = ex.getTypeName();
                system.debug(
                    '\n### ex: ' + ex +
                    '\n### stack trace:\n' + ex.getStackTraceString());
            }
            //log.OwnerId = Esa_EventContext.getDefaultRecordOwnerId();

            if (queueMessageForWrite || queueMessagesForWrite) {
                queuedMessages.add(log);
            }
            else {
                if (Test.isRunningTest()) {
                    System.debug('writing debug service log: ' + log);
                }
                insert log;
            }
        }
        catch (Exception e) {
            System.debug('Exception in Esa_DebugService.write():  ' + e);
            System.debug('Stack Trace:  ' + e.getStackTraceString());
        }
    }



    public static void writeException(Integer severity, String sourceFile, String userId, String message, String details, String stackTrace, String typeName) {
        try {
            Esa_Debug_Log__c log = new Esa_Debug_Log__c();
            log.Message__c = message;
            log.Source_File__c = sourceFile;
            log.User__c = userId;
            log.Level__c = LEVEL_ERROR;
            log.Exception_Details__c = details;
            log.StackTrace__c = stackTrace;
            log.TypeName__c = typeName;
            //log.OwnerId = Esa_EventContext.getDefaultRecordOwnerId();

            if (queueMessagesForWrite) {
                queuedMessages.add(log);
            }
            else {
                if (Test.isRunningTest()) {
                    System.debug('writing debug service log: ' + log);
                }
                insert log;
            }

            system.debug('### details: ' + details);
            system.debug('### stack trace: ' + stackTrace);
        }
        catch (Exception e) { }
    }

    public static void writeException(Exception ex, string message) {
        writeException(ex, null, null, UserInfo.getUserId(), message);
    }

    public static void writeException(Exception ex, string sourceFile, string message) {
        writeException(ex, null, sourceFile, UserInfo.getUserId(), message);
    }

    public static void writeException(Exception ex, Integer severity, string sourceFile, string message) {
        writeException(ex, severity, sourceFile, UserInfo.getUserId(), message);
    }

    public static void writeException(Exception ex, Integer severity, string sourceFile, string userId, string message) {
        write(LEVEL_ERROR, severity, sourceFile, userId, message, ex);
    }

    public static void writeWarning(String sourceFile, string userId, string message) {
        write(LEVEL_WARNING, null, sourceFile, userId, message, null);
    }

    public static void writeMessage(string message) {
        writeMessage(message, null, UserInfo.getUserId());
    }

    public static void writeErrorMessage(String message, String level) {
        write(level, null, null, UserInfo.getUserId(), message, null);
    }

    public static void writeMessage(string message, string sourceFile) {
        writeMessage(message, sourceFile, UserInfo.getUserId());
    }

    public static void writeMessage(string message, string sourceFile, string userId) {
        write(LEVEL_INFO, null, sourceFile, userid, message, null);
    }

    // If you invoke this method, it's the caller's responsibility to invoke processQueuedMessages later in the request.
    // See the documentation for queuedMessages above for more details.
    public static void queueWriteException(String errorMessage, Integer severity, string sourceFile, string userId, string message) {
        write(LEVEL_ERROR, severity, sourceFile, userId, message, null, true);
    }

    public static void queueWriteException(Exception ex, Integer severity, string sourceFile, string userId, string message) {
        write(LEVEL_ERROR, severity, sourceFile, userId, message, ex, true);
    }

    // If you invoke this method, it's the caller's responsibility to invoke processQueuedMessages later in the request.
    // See the documentation for queuedMessages above for more details.
    public static void queueWriteWarning(String sourceFile, string userId, string message) {
        write(LEVEL_WARNING, null, sourceFile, userId, message, null, true);
    }

    public static void processDatabaseSaveErrors(Database.SaveResult[] results) {
        processDatabaseSaveErrors(results, null, null);
    }

    public static void processDatabaseDeleteErrors(Database.DeleteResult[] results) {
        processDatabaseDeleteErrors(results, null, null);
    }

    public static void processDatabaseUpsertErrors(Database.UpsertResult[] results) {
        processDatabaseUpsertErrors(results, null, null);
    }

    public static void processDatabaseSaveErrors(Database.SaveResult[] results, String sourceFile) {
        processDatabaseSaveErrors(results, sourceFile, null);
    }

    public static void processDatabaseDeleteErrors(Database.DeleteResult[] results, String sourceFile) {
        processDatabaseDeleteErrors(results, sourceFile, null);
    }

    public static void processDatabaseUpsertErrors(Database.UpsertResult[] results, String sourceFile) {
        processDatabaseUpsertErrors(results, sourceFile, null);
    }

    public static void processDatabaseSaveErrors(Database.SaveResult[] results, String sourceFile, String msg) {
            processDatabaseErrors(buildProxyArray(results), sourceFile, msg);
    }

    public static void processDatabaseDeleteErrors(Database.DeleteResult[] results, String sourceFile, String msg) {
            processDatabaseErrors(buildProxyArray(results), sourceFile, msg);
    }

    public static void processDatabaseUpsertErrors(Database.UpsertResult[] results, String sourceFile, String msg) {
        System.debug('upsert results: ' + results);
        processDatabaseErrors(buildProxyArray(results), sourceFile, msg);
    }

    private static void processDatabaseErrors(Esa_DbResultProxy[] results, String sourceFile, String msg) {

        System.debug('processing ' +  results.size() + ' db results: ' + msg + ':' + sourceFile + ' - ' + results);
        Id userId = UserInfo.getUserId();

        for (Esa_DbResultProxy result : results) {
            if (result == null) {
                System.debug('result is null?');
                continue;
            }
            if (!result.isSuccess()) {
                // Operation failed, so get all errors
                Id recordId = result.getId();
                for(Database.Error err : result.getErrors()) {
                    String errmsg = (msg == null ? '' : msg + ': ')
                                  + '(' + recordId + ') '
                                  + err.getStatusCode() + ': ' + err.getMessage();
                    Esa_DebugService.queuewriteWarning(sourceFile, userId, errmsg);
                }
            }
        }

        Esa_DebugService.processQueuedMessages();
    }

    private static Esa_DbResultProxy[] buildProxyArray(Database.SaveResult[] results) {
        Esa_DbResultProxy[] rtn = new Esa_DbResultProxy[results.size()];
        for (Database.SaveResult r : results ) {
            if (r != null) {
              rtn.add(new Esa_DbResultProxy(r));
            }
        }

        return rtn;
    }

    private static Esa_DbResultProxy[] buildProxyArray(Database.DeleteResult[] results) {
        List<Esa_DbResultProxy> rtn = new List<Esa_DbResultProxy>();
        for (Database.DeleteResult r : results ) {
            if (r != null) {
              rtn.add(new Esa_DbResultProxy(r));
            }
        }

        return rtn;
    }

    private static Esa_DbResultProxy[] buildProxyArray(Database.UpsertResult[] results) {
        Esa_DbResultProxy[] rtn = new Esa_DbResultProxy[results.size()];
        for (Database.UpsertResult r : results ) {
            if (r != null) {
              rtn.add(new Esa_DbResultProxy(r));
            }
        }

        return rtn;
    }


    //the platform doesnt have a base class Esa_for (Save|Delete|Upsert)Result, so we need this proxy object
    public class Esa_DbResultProxy {
        private final Database.Error[] errors;
        private final Id id;
        private final Boolean success;

        public Database.Error[] getErrors() {
            return errors;
        }

        public Boolean isSuccess() {
            return success;
        }

        public Id getId() {
            return id;
        }

        public Esa_DbResultProxy(Database.UpsertResult result) {
            this.errors = result.getErrors();
            this.id = result.getId();
            this.success = result.isSuccess();
        }

        public Esa_DbResultProxy(Database.SaveResult result) {
            this.errors = result.getErrors();
            this.id = result.getId();
            this.success = result.isSuccess();
        }

        public Esa_DbResultProxy(Database.DeleteResult result) {
            this.errors = result.getErrors();
            this.id = result.getId();
            this.success = result.isSuccess();
        }
    }
}
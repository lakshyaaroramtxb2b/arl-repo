public with sharing class KARL_AuditorReqItemSharingUtility {
    /**
     * @author Mahima Aggarwal
     * @email mahima.aggarwal@mtxb2b.com
     * @desc This method share of all the Auditor Request Items to the contacts which belong to same firm
     * Here we pass the contacts to which we want to share the records in form of a Map of audit firm Id to
     * Contacts
    */
    public static void shareAuditorReqItem(List<KARL_Auditor_Request_Item__c> auditorReqItemList,Map<Id,Set<Id>> auditFirmIdToContactIdMap){
        Map<Id,Id> auditorReqItemIdToAuditFirmIdMap = new Map<Id,Id>();
        Set<Id> allContactIdSet = new Set<Id>();
        Map<Id,Id> contactIdToUserIdMap = new Map<Id,Id>();
        List<KARL_Auditor_Request_Item__Share> auditorReqItemShareList = new List<KARL_Auditor_Request_Item__Share>();
        List<Cycle_Request_Item__Share> cycleReqItemShareList = new List<Cycle_Request_Item__Share>();

        for(KARL_Auditor_Request_Item__c auditorReqItem : auditorReqItemList){
            auditorReqItemIdToAuditFirmIdMap.put(auditorReqItem.Id,auditorReqItem.KARL_Audit_Team__r.KARL_Audit_Firm__c);
        }

        for(Set<Id> contactIdSet : auditFirmIdToContactIdMap.values()){
            allContactIdSet.addAll(contactIdSet);
        }
        
        for(User usr : [SELECT Id, ContactId, Contact.AccountId
                        FROM User 
                        WHERE ContactId IN :allContactIdSet]){
            contactIdToUserIdMap.put(usr.ContactId,usr.Id);
        }

        for(KARL_Auditor_Request_Item__c auditorReqItem : auditorReqItemList){
            Id auditFirmId = auditorReqItemIdToAuditFirmIdMap.get(auditorReqItem.Id);
            if(auditFirmId != null && auditFirmIdToContactIdMap.get(auditFirmId) != null &&
                !auditFirmIdToContactIdMap.get(auditFirmId).isEmpty()){
                    for(Id conId : auditFirmIdToContactIdMap.get(auditFirmId)){
                        Id userId = contactIdToUserIdMap.get(conId);
                        if(userId != null){
                            KARL_Auditor_Request_Item__Share auditorReqItemShare = new KARL_Auditor_Request_Item__Share();
                            auditorReqItemShare.ParentId = auditorReqItem.Id; 
                            auditorReqItemShare.UserOrGroupId = userId;
                            auditorReqItemShare.AccessLevel = 'Edit';
                            auditorReqItemShare.RowCause = Schema.KARL_Auditor_Request_Item__Share.RowCause.Manual;

                            Cycle_Request_Item__Share cycleReqItemShare = new Cycle_Request_Item__Share();
                            cycleReqItemShare.ParentId = auditorReqItem.KARL_Cycle_Request_Item__c; 
                            cycleReqItemShare.UserOrGroupId = userId;
                            cycleReqItemShare.AccessLevel = 'Edit';
                            cycleReqItemShare.RowCause = Schema.Cycle_Request_Item__Share.RowCause.Manual;

                            auditorReqItemShareList.add(auditorReqItemShare);
                            cycleReqItemShareList.add(cycleReqItemShare);
                        }
                    }
            }
        }

        System.debug('insert auditorReqItemShareList>>'+auditorReqItemShareList);
        System.debug('delete cycleReqItemShareList>>'+cycleReqItemShareList);

        List<Database.SaveResult> srAuditorReqItemList = Database.insert(auditorReqItemShareList,false);
        List<Database.SaveResult> srCycleReqItemList = Database.insert(cycleReqItemShareList,false);

        for(Database.SaveResult sr : srAuditorReqItemList){
            if(!sr.isSuccess()){
                System.debug('Auditor Request Item sharing Failed due to following Reasons:');
                for(Database.Error dr : sr.getErrors()){
                    System.debug('Error Code: '+dr.getStatusCode());
                    System.debug('Error Description: '+dr.getMessage());
                    System.debug('Fields affected: '+dr.getFields());
                }
            }
        }

        for(Database.SaveResult sr : srCycleReqItemList){
            if(!sr.isSuccess()){
                System.debug('Cycle Request Item sharing Failed due to following Reasons:');
                for(Database.Error dr : sr.getErrors()){
                    System.debug('Error Code: '+dr.getStatusCode());
                    System.debug('Error Description: '+dr.getMessage());
                    System.debug('Fields affected: '+dr.getFields());
                }
            }
        }
    }

    /**
     * @author Mahima Aggarwal
     * @email mahima.aggarwal@mtxb2b.com
     * @desc This method unShare the Auditor Request Items which are shared to auditFirmIdToContactIdMap's contacts
    */
    public static void unShareAuditorReqItem(List<KARL_Auditor_Request_Item__c> auditorReqItemList,Map<Id,Set<Id>> auditFirmIdToContactIdMap){
        System.debug('delete execution start>>');
        Set<Id> allContactIdSet = new Set<Id>();
        Set<Id> userIdSet = new Set<Id>();
        Set<Id> auditorReqItemIdSet = new Set<Id>();
        String rowCauseTypeAuditorReqItem = Schema.KARL_Auditor_Request_Item__Share.RowCause.Manual;
        String rowCauseTypeCycleReItem = Schema.Cycle_Request_Item__Share.RowCause.Manual;

        Set<Id> cycleReqItemIdToConsiderSet = new Set<Id>();
        Set<Id> allAuditorReqItemIdSet = new Set<Id>();
        Map<Id,Id> auditorReqIdToCycleReqIdMap = new Map<Id,Id>();

        for(Set<Id> contactIdSet : auditFirmIdToContactIdMap.values()){
            allContactIdSet.addAll(contactIdSet);
        }

        for(User usr : [SELECT Id, ContactId, Contact.AccountId
                        FROM User 
                        WHERE ContactId IN :allContactIdSet]){
            userIdSet.add(usr.Id);
        }

        for(KARL_Auditor_Request_Item__c auditorReqItem : auditorReqItemList){
            cycleReqItemIdToConsiderSet.add(auditorReqItem.KARL_Cycle_Request_Item__c);
            auditorReqItemIdSet.add(auditorReqItem.Id);
        }

        List<KARL_Auditor_Request_Item__Share> auditorReqItemShareList = [SELECT Id 
                                                                FROM KARL_Auditor_Request_Item__Share
                                                                WHERE UserOrGroupId IN : userIdSet
                                                                AND ParentId IN : auditorReqItemIdSet
                                                                AND RowCause = :rowCauseTypeAuditorReqItem
                                                                ];
        
        System.debug('delete auditorReqItemShareList>>'+auditorReqItemShareList);
        Database.DeleteResult[] drAuditorReqItemList = Database.delete(auditorReqItemShareList, false);

        for(Database.DeleteResult dr : drAuditorReqItemList) {
            if (dr.isSuccess()) {
                // Operation was successful, so get the ID of the record that was processed
                System.debug('Successfully deleted with ID: ' + dr.getId());
            }
            else {
                System.debug('Failed due to following Reasons:');                
                for(Database.Error err : dr.getErrors()) {
                    System.debug('The following error has occurred.');                    
                    System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    System.debug('fields that affected this error: ' + err.getFields());
                }
            }
        }

        for(KARL_Auditor_Request_Item__c auditorReqItem : [SELECT Id,KARL_Audit_Team__c,KARL_Cycle_Request_Item__c
                                                            FROM KARL_Auditor_Request_Item__c
                                                            WHERE KARL_Cycle_Request_Item__c IN :cycleReqItemIdToConsiderSet]){
            allAuditorReqItemIdSet.add(auditorReqItem.Id);
            auditorReqIdToCycleReqIdMap.put(auditorReqItem.Id,auditorReqItem.KARL_Cycle_Request_Item__c);
        }

        for(KARL_Auditor_Request_Item__Share auditorReqItemShare : [SELECT Id,ParentId,UserOrGroupId
                                                                FROM KARL_Auditor_Request_Item__Share
                                                                WHERE UserOrGroupId IN : userIdSet
                                                                AND ParentId IN : allAuditorReqItemIdSet
                                                                AND RowCause = :rowCauseTypeAuditorReqItem
                                                                ]){
            cycleReqItemIdToConsiderSet.remove(auditorReqIdToCycleReqIdMap.get(auditorReqItemShare.ParentId));
        }

        List<Cycle_Request_Item__Share> cycleReqItemShareList = [SELECT Id 
                                                                FROM Cycle_Request_Item__Share
                                                                WHERE UserOrGroupId IN : userIdSet
                                                                AND ParentId IN : cycleReqItemIdToConsiderSet
                                                                AND RowCause = :rowCauseTypeCycleReItem
                                                                ];

        System.debug('delete cycleReqItemShareList>>'+cycleReqItemShareList);
        Database.DeleteResult[] drCycleReqItemList = Database.delete(cycleReqItemShareList, false);

        for(Database.DeleteResult dr : drCycleReqItemList) {
            if (dr.isSuccess()) {
                // Operation was successful, so get the ID of the record that was processed
                System.debug('Successfully deleted with ID: ' + dr.getId());
            }
            else {
                System.debug('Failed due to following Reasons:');                
                for(Database.Error err : dr.getErrors()) {
                    System.debug('The following error has occurred.');                    
                    System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    System.debug('fields that affected this error: ' + err.getFields());
                }
            }
        }
    }
}
@isTest
private class SC_BulkAttendanceControllerTest {
    @testSetup static void setup(){
        SC_Training_Provider__c provider = new SC_Training_Provider__c();
        provider.Name = 'Trailhead';
        provider.Type__c = 'Trailhead';
        insert provider;

        Account acc = new Account(
                Name = 'TEST BULK ACCOUNT'
        );
        insert acc;

        Contact con = new Contact(
                AccountId = acc.Id,
                Company__c = 'Company',
                Copy_of_Original_Email__c = 'test.bulk.con.email@test.com',
                Email = 'test.bulk.con.email@test.com',
                Employee_Id__c = 'EID-1',
                Employee_Type__c = 'Contract',
                FirstName = 'TEST',
                LastName = 'TEST BULK CON',
                Manager_Email__c = 'test.test@test.com',
                Manager_ID__c = 'MID',
                JobCode__c = 'JC',
                Org62_User_Id__c = '01r3ATEST01OWMPQA4',
                RecordTypeId = [
                        select Id
                        from RecordType
                        where Name = 'Salesforce Employee'
                        and SobjectType = 'Contact' limit 1
                ].Id
        );
        insert con;

        Profile pro = [
                select Id
                from Profile
                where Name LIKE '%Portal User%'
                limit 1
        ];

        User testUser = new User(
                Alias = 'bulk',
                CommunityNickName = 'BulkTestUserForBulkEnroll',
                ContactId = con.Id,
                Email = 'test.bulk.con.email@test.com',
                EmailEncodingKey = 'ISO-8859-1',
                FirstName = 'BULK TEST',
                LastName = 'CONTACT',
                LanguageLocaleKey = 'en_US',
                LocaleSidKey = 'en_US',
                ProfileId = pro.Id,
                UserName = 'test.bulk.con.email@test.com',
                TimeZoneSidKey = 'America/New_York'
        );
        insert testUser;

        Training_Course__c tc = new Training_Course__c(
                Default_Due_Days__c = 30.0,
                Is_Active__c = true,
                Name = 'TEST_ONE_ON_ONE',
                Provider__c = 'Trailhead',
                Provider_Id__c = '1996',
                Tracked__c = true,
                URL__c = 'https://developer.salesforce.com/trailhead/module/one-on-one-meetings/'
        );
        insert tc;

        Training_Course_Taken__c tct = new Training_Course_Taken__c(
                Attendee_Email__c = 'test.bulk.con.email@test.com',
                Contact__c = con.Id,
                Training_Course__c = tc.Id
        );
        insert tct;
    }

    @isTest static void simpleHappyPath() {
        Training_Course__c tc = [select id, Name, Org_Wide_Email_Id__c from Training_Course__c limit 1];

        // Load page
        PageReference pageRef = Page.SC_BulkAttendance;
        ApexPages.StandardController stdController =  new ApexPages.StandardController(tc);
        SC_BulkAttendanceController scBulk = new SC_BulkAttendanceController(stdController);
        Test.setCurrentPage(pageRef);

        System.Assert(scBulk.getUserHasPermissionToUpload(),'Test user should be able to upload');

        // Process basic file
        Attachment testFile = new Attachment();
        string fileString = 'Email,Completion Date\ntest.bulk.con.email@test.com,2018-07-22\ntest@test.com,2017-08-22\ntest2@test.com,5/54/4543';
        testFile.Body = Blob.valueOf(fileString);
        scBulk.attendanceFile = testFile;
        scBulk.submit();

        // Assert that training course taken was updated
        Training_Course_Taken__c attendee = [SELECT Status__c, Date_Completed__c
            FROM Training_Course_Taken__c limit 1];
        System.AssertEquals('Course Completed',attendee.Status__c,'Status should be updated to "Course Completed"');
        System.AssertEquals(Date.valueOf('2018-07-22'),attendee.Date_Completed__c,'Date completed should be updated to match');
    }


}
/**
 * @author Ben Harvie
 * @email ben.harvie@salesforce.com
 * @desc Test class for ControlTrigger
*/
@isTest
public class ARL_ControlTriggerHandlerTest {
    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @desc Test data setup.
    */
    @testSetup
    private static void testSetup(){
        User ospAdmin = ARL_TestDataFactory.createNamedOpsAdmin('TestSetup', 'User', 'testsetup@example.com');
        
        System.runAs(ospAdmin){
            Control__c controlItem = ARL_TestDataFactory.createControl();
            insert controlItem;

            KARL_GRC_Controls__c ccfControl = ARL_TestDataFactory.createCcfControl();
            insert ccfControl;

            Control__c ctrlItem = [SELECT id FROM Control__c LIMIT 1];
            ctrlItem.Control_Description__c	 = 'test12345';
            update ctrlItem; 
        }
    }
    
    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @desc In this test, the test makes no change to the description so we expect no new version
    */
	@isTest
    public static void controlVersions1(){ 
        // Setup Ops Admin
        User u = ARL_TestDataFactory.createOpsAdmin();

        // Run the test as the SCEA Ops Admin
        System.runAs(u) {

            Test.startTest();
                Control__c ctrlItem = [SELECT id FROM Control__c LIMIT 1];
                KARL_GRC_Controls__c ccfItem = [SELECT id FROM KARL_GRC_Controls__c LIMIT 1];

                //Updating request item will create new Control Version
                ctrlItem.Control_Description__c	 = 'test12345';
                update ctrlItem; 

            Test.stopTest();
            List<Control_Version__c> ctrlVersion = [SELECT id FROM Control_Version__c];        
            System.debug('Testing >> ' + ctrlVersion);
            system.assertEquals(1, ctrlVersion.size(), 'Incorrect number of Control Versions');
        }

    }
    
    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @desc In this test, the test is updating the description and asserting on the versions
    */
	@isTest
    public static void controlVersions2(){ 
        // Setup Ops Admin
        User u = ARL_TestDataFactory.createOpsAdmin();

        // Run the test as the SCEA Ops Admin
        System.runAs(u) {

            Test.startTest();
                Control__c ctrlItem = [SELECT id FROM Control__c LIMIT 1];
                KARL_GRC_Controls__c ccfItem = [SELECT id FROM KARL_GRC_Controls__c LIMIT 1];

                //Updating request item will create new Control Version
                ctrlItem.Control_Description__c	 = 'test123456789';
                update ctrlItem; 

            Test.stopTest();
            List<Control_Version__c> ctrlVersion = [SELECT id FROM Control_Version__c];        
            System.debug('Testing >> ' + ctrlVersion);
            system.assertEquals(2, ctrlVersion.size(), 'Incorrect number of Control Versions');
        }

    }
    
    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @desc In this test, the test is updating the CCF alignment and asserting matching descriptions
    */
    @isTest
    public static void ccfAlignment(){ 
        // Setup Ops Admin
        User u = ARL_TestDataFactory.createOpsAdmin();

        // Run the test as the SCEA Ops Admin
        System.runAs(u) {

            Test.startTest();
                Control__c ctrlItem = [SELECT Id FROM Control__c LIMIT 1];
                KARL_GRC_Controls__c ccfItem = [SELECT Id, Description__c FROM KARL_GRC_Controls__c LIMIT 1];

                // Updating request item will create new Control Version + align with CCF (description should be overwritten)
                ctrlItem.Control_Description__c	 = 'test12345';
                ctrlItem.CCF_Control__c = ccfItem.Id;
                ctrlItem.KARL_CCF_Alignment__c = true;
                update ctrlItem; 

                Control__c ctrlItemUpdated = [SELECT Id, Control_Description__c FROM Control__c LIMIT 1];

                system.assertEquals(ctrlItemUpdated.Control_Description__c, ccfItem.Description__c, 'Control Descriptions not matching');

            Test.stopTest();
        }
    }

}
/**
 * @author Mahima Aggarwal
 * @email mahima.aggarwal@mtxb2b.com
 * @desc Test class for KARL_EvidenceRequestTriggerHelper
*/
@isTest
public without sharing class KARL_EvidenceRequestTriggerHelperTest {
    /**
     * @author Mahima Aggarwal
     * @email mahima.aggarwal@mtxb2b.com
     * @desc Test data setup.
    */
    @testSetup
    private static void testSetup(){
        User testUser = ARL_TestDataFactory.createOpsAdmin();
    }

    @istest
    public static void unitTest(){ 
        User usr = [Select id from User where FirstName = 'Ops_Admin'];
        System.runAs(usr){
            Request_Item__c reqItem = ARL_TestDataFactory.createRequestItem();
            reqItem.Primary_Scope__c = 'SS';
            reqItem.KARL_Evidence_Submission_Workflow__c = true;
            reqItem.Create_GUS_Case__c = true;
            insert reqItem;
            
            Cycle_Request_Item__c cycleReqItem = ARL_TestDataFactory.createCycleRequestItem(reqItem.id,null);
            cycleReqItem.Cycle_Request_Status__c = 'New';
            cycleReqItem.Request_GUS__c = 'testWorkId';
            cycleReqItem.KARL_Update_GUS__c = true;
            cycleReqItem.Request_Tech_Details__c = 'tech details old';
            insert cycleReqItem;

            KARL_Evidence_Request__c evidenceReq = new KARL_Evidence_Request__c();
            evidenceReq.KARL_Submitted__c = false;
            evidenceReq.KARL_Cycle_ARL_Item__c = cycleReqItem.Id;
            insert evidenceReq;
        }
    }
    @isTest
    public static void changeGusCaseStatusTest(){
         User usr = [Select id from User where FirstName = 'Ops_Admin'];
        System.runAs(usr){
            Request_Item__c reqItem = ARL_TestDataFactory.createRequestItem();
            reqItem.Primary_Scope__c = 'SS';
            reqItem.KARL_Evidence_Submission_Workflow__c = true;
            reqItem.Create_GUS_Case__c = true;
            insert reqItem;
            
            Cycle_Request_Item__c cycleReqItem = ARL_TestDataFactory.createCycleRequestItem(reqItem.id,null);
            cycleReqItem.Cycle_Request_Status__c = 'New';
            cycleReqItem.Request_GUS__c = 'testWorkId';
            cycleReqItem.KARL_Update_GUS__c = true;
            cycleReqItem.Request_Tech_Details__c = 'tech details old';
            insert cycleReqItem;

            KARL_Evidence_Request__c evidenceReq = new KARL_Evidence_Request__c();
            evidenceReq.KARL_Submitted__c = false;
            evidenceReq.KARL_Cycle_ARL_Item__c = cycleReqItem.Id;
            evidenceReq.Evidence_Status__c = KARL_Constants.EVIDENCE_STATUS_Pending_Engineer;
            insert evidenceReq;
            
            test.startTest();
            evidenceReq.Evidence_Status__c = KARL_Constants.EVIDENCE_STATUS_ReturnToEngineer;
            update evidenceReq;
            test.stopTest();
        }
        
    }
}
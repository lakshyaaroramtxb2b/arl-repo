/**
 * @author: ralph@callaway.cloud
 * @description: Handles keeping bi-directional alternate course lookups mirrored
 */
public class SC_AlternateCourseLinker {

    /**
     * If a courses alternate course is set/cleared/changed, update it's current and former and alternate
     * course to link back to the training course so we get a one-to-one relationship via mirrored lookups.
     * @param newList Trigger.new
     * @param oldMap  Trigger.oldMap (or null)
     */
    private static Set<Id> alreadyLinked = new Set<Id>();
    public static void process(Training_Course__c[] newList, Map<Id, Training_Course__c> oldMap) {
        Training_Course__c[] alternates = new Training_Course__c[0];
        for (Training_Course__c newCourse : newList) {
            Training_Course__c oldCourse = (oldMap != null) ? oldMap.get(newCourse.Id) : new Training_Course__c();
            if (newCourse.Alternate_Course__c != oldCourse.Alternate_Course__c) {
                if (newCourse.Alternate_Course__c != null && !alreadyLinked.contains(newCourse.Alternate_Course__c)) {
                    alreadyLinked.add(newCourse.Alternate_Course__c);
                    alternates.add(new Training_Course__c(Id = newCourse.Alternate_Course__c, Alternate_Course__c = newCourse.Id));
                }
                if (oldCourse.Alternate_Course__c != null && !alreadyLinked.contains(oldCourse.Alternate_Course__c)) {
                    alreadyLinked.add(oldCourse.Alternate_Course__c);
                    alternates.add(new Training_Course__c(Id = oldCourse.Alternate_Course__c, Alternate_Course__c = null));
                }
                alreadyLinked.add(newCourse.Id);
            }
        }
        if (!alternates.isEmpty()) {
            update alternates;
            SC_AlternateEnrollmentLinker.run();       
        }
    }

}
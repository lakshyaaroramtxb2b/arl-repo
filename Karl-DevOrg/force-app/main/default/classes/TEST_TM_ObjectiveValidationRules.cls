/**
 * Trust Maturity Model v2
 * Dec 2015
 * Written By: Jorge L Caceres (J-team member J3)
 * 
 * Test class for the following functionality living in TM_ObjectiveValidationRules
 * 
**/

@isTest
private class TEST_TM_ObjectiveValidationRules {
    
    // create test common data
    private static Account buzUnit1;
    private static Account buzUnit2;
    
    private static IRCloud__Environment__c env1;
    private static IRCloud__Environment__c env2;
    private static IRCloud__Environment__c env3;
    
    private static User user1;
    private static User user2;
    private static User adminUser;
    
    private static TM_AbstractObjective__c absObjective1;
    private static TM_AbstractObjective__c absObjective2;
    private static TM_AbstractObjective__c absObjective3;
    private static TM_AbstractObjective__c absObjective4;
    private static TM_AbstractObjective__c absObjective5;

    private static TM_Placement__c placement1;
    private static TM_Placement__c placement2;
    private static TM_Placement__c placement3;
    private static TM_Placement__c placement4;
    private static TM_Placement__c placement5;

    private static TM_Objective__c objective1;
    private static TM_Objective__c objective2;
     
    static {
        buzUnit1 = TEST_TM_Util.generateAccount(TM_Constants.BU_ACCOUNT_REC_TYPE_ID);
        buzUnit2 = TEST_TM_Util.generateAccount(TM_Constants.BU_ACCOUNT_REC_TYPE_ID);
        insert new Account[] { buzUnit1, buzUnit2 };
        
        env1 = TEST_TM_Util.generateEnvironment(TM_Constants.PARENT_INFRA_IT_ENVIRONMENT_REC_TYPE_ID, buzUnit1.Id);
        env2 = TEST_TM_Util.generateEnvironment(TM_Constants.PARENT_INFRA_PROD_ENVIRONMENT_REC_TYPE_ID, buzUnit1.Id);
        env3 = TEST_TM_Util.generateEnvironment(TM_Constants.OTHER_ENVIRONMENT_REC_TYPE_ID, buzUnit2.Id);
        insert new IRCloud__Environment__c[] { env1, env2, env3 };        

        user1 = TEST_TM_Util.generateUser();
        user2 = TEST_TM_Util.generateUser();
        adminUser = TEST_TM_Util.generateUser();
        // avoid mixed DML errors
        System.runAs(new User(Id = UserInfo.getUserId())) {
            insert new User[] { user1, user2, adminUser };
            insert new PermissionSetAssignment(AssigneeId = adminUser.Id, PermissionSetId = TM_Constants.PERM_SET_ADMIN_ID);
       } 
        
        absObjective1 = TEST_TM_Util.generateTMAbstractObjective();
        absObjective2 = TEST_TM_Util.generateTMAbstractObjective();
        absObjective3 = TEST_TM_Util.generateTMAbstractObjective();
        absObjective4 = TEST_TM_Util.generateTMAbstractObjective();
        absObjective5 = TEST_TM_Util.generateTMAbstractObjective();
        insert new TM_AbstractObjective__c[] { absObjective1, absObjective2, absObjective3, absObjective4, absObjective5 };        
        
        placement1 = TEST_TM_Util.generateTMPlacement(absObjective1.Id, 'SIR', '1', 1, TM_Constants.BU_ENVIRONMENT_TYPE); 
        placement2 = TEST_TM_Util.generateTMPlacement(absObjective2.Id, 'SIR', '1', 1, TM_Constants.BU_ENVIRONMENT_TYPE); 
        placement3 = TEST_TM_Util.generateTMPlacement(absObjective3.Id, 'SIR', '1', 1, TM_Constants.PARENT_INFRA_IT_ENVIRONMENT_NAME); 
        placement4 = TEST_TM_Util.generateTMPlacement(absObjective4.Id, 'SIR', '1', 1, TM_Constants.PARENT_INFRA_PROD_ENVIRONMENT_NAME); 
        placement5 = TEST_TM_Util.generateTMPlacement(absObjective5.Id, 'SIR', '1', 1, TM_Constants.OTHER_ENVIRONMENT_NAME); 
        insert new TM_Placement__c[] { placement1, placement2, placement3, placement4, placement5 };  
        
        // objective with designated roles
        objective1 = TEST_TM_Util.generateTMObjectiveEnv(placement1.Id, env1.Id);
        objective1.ImplementationOwner__c = user1.Id;
        objective1.ValidationOwner__c = user2.Id;
        objective1.ImplementBefore__c = Date.today().addDays(7);
        objective1.ValidateBefore__c = Date.today().addDays(7);
        insert objective1;     
        
        // objective without designated roles
        objective2 = TEST_TM_Util.generateTMObjectiveEnv(placement2.Id, env2.Id);
        objective2.ImplementBefore__c = Date.today().addDays(7);
        objective2.ValidateBefore__c = Date.today().addDays(7);
        insert objective2;  
        
        // reload to get default values
        for (TM_Objective__c objective : [select Id, Status__c, ImplementationOwner__c, ValidationOwner__c, ImplementBefore__c, ValidateBefore__c
                                          from TM_Objective__c where Id IN :new List<Id>{objective1.Id, objective2.Id}]) {
            if (objective.Id == objective1.Id) objective1 = objective;                                      
            if (objective.Id == objective2.Id) objective2 = objective;                                                                                  
        }
              
    }
    
    static testMethod void testImplementerRole() {
        // validate implementer role status update
        System.runAs(user1) {
            // all valid status do not fail
            for (String s : TM_Constants.VALID_IMPLEMENTER_STATUS) {
                objective1.Status__c = s;
                try {
                    update objective1;
                } catch (exception e) {
                    System.assertEquals(null, e); 
                }           
            }

            // all other status fail
            Set<String> invalidStatus = TM_Constants.VALID_OBJECTIVE_STATUS;
            invalidStatus.removeAll(new List<String>(TM_Constants.VALID_IMPLEMENTER_STATUS));
            for (String s : invalidStatus) {
                if (objective2.Status__c == s) continue;
                objective1.Status__c = s;
                try {
                    update objective1;
                    System.assertNotEquals(s, objective1.Status__c, 'Able to update status when it should not be');                     
                } catch (exception e) {}           
            }
        
        }
    }

    static testMethod void testValidatorRole() {
        // validate validator role status update
        System.runAs(user2) {
            // all valid status do not fail
            for (String s : TM_Constants.VALID_VALIDATOR_STATUS) {
                objective1.Status__c = s;
                try {
                    update objective1;
                } catch (exception e) {
                    System.assertEquals(null, e); 
                }           
            }

            // all other status fail
            Set<String> invalidStatus = TM_Constants.VALID_OBJECTIVE_STATUS;
            invalidStatus.removeAll(new List<String>(TM_Constants.VALID_VALIDATOR_STATUS));
            for (String s : invalidStatus) {
                if (objective2.Status__c == s) continue;
                objective1.Status__c = s;
                try {
                    update objective1;
                    System.assertNotEquals(s, objective1.Status__c, 'Able to update status when it should not be');                     
                } catch (exception e) {}           
            }
        
        }
    }
    
    static testMethod void testAdminRole() {
        // validate admin role status update
        System.runAs(adminUser) {
            // all valid status do not fail
            for (String s : TM_Constants.VALID_OBJECTIVE_STATUS) {
                if (objective2.Status__c == s) continue;
                objective1.Status__c = s;
                try {
                    update objective1;
                } catch (exception e) {
                    System.assertEquals(null, e); 
                }           
            }        
        }
        
    }
   
    static testMethod void testNotDesignatedRole() {
        // validate that no status changes allowed if there are no designated roles in the objective
        System.runAs(user2) {
            // all valid status fail
            for (String s : TM_Constants.VALID_OBJECTIVE_STATUS) {
                if (objective2.Status__c == s) continue;
                objective2.Status__c = s;
                try {
                    // this should fail and throw exception
                    update objective2;
                    System.assertNotEquals(s, objective2.Status__c, 'Able to update status when it should not be');                     
                } catch (exception e) {}           
            }        
        }
        
    }
}
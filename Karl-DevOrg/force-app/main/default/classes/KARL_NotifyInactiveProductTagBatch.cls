/**
 * @author Lakshya Arora
 * @email lakshya.arora@mtxb2b.com
 * @desc This is schedulable and batch class to chatter on Gus Work Record n days to notify before due date .
 */

public with sharing class KARL_NotifyInactiveProductTagBatch implements Database.Batchable<sObject>, Schedulable{
    
    /**
    * @author Lakshya Arora
    * @email lakshya.arora@mtxb2b.com
    * @desc This method is for schedulable interface to execute.
    */
    public void execute(SchedulableContext sc) {
        database.executebatch( new KARL_NotifyInactiveProductTagBatch(),200);
    }

    public Database.QueryLocator start(Database.BatchableContext BC){
        String query = 'SELECT Id,Suggested_Product_Tag__c,KARL_GRC_Assignee__c,Name,KARL_GRC_Assignee__r.Name  '
                        + ' FROM Request_Item__c '
                        + ' WHERE  Suggested_Product_Tag__c != null AND Suggested_Product_Tag__r.Active__c = FALSE '
                        + ' WITH SECURITY_ENFORCED';
        return Database.getQueryLocator(query);
    }

   
    public void execute(Database.BatchableContext BC, List<Request_Item__c> requestItemList){
        System.debug(Logginglevel.DEBUG,'requestItemList = > '+requestItemList);
        String externalId = System.label.ARL_GUS_Work_Product_Tag_ID;
        if(Test.isRunningTest()){
            externalId = 'ACT123';
        }
        List<KARL_GUS_Product_Tag__c> defaultProductTagId = [SELECT id,Name,Product_Tag_External_Id__c from KARL_GUS_Product_Tag__c WHERE Product_Tag_External_Id__c =: externalId];
        if(defaultProductTagId.isEmpty()){
            System.debug(Logginglevel.DEBUG,'Invalid Product Id in ARL_GUS_Work_Product_Tag_ID Label.');
            return;
        }
        
        String orgWideDisplayName = System.label.ARL_Org_Wide_Email_Address_Display_Name;
        Id orgwideEmailAddressId = [SELECT Id,DisplayName FROM OrgWideEmailAddress WHERE DisplayName=:orgWideDisplayName].Id;
        EmailTemplate inactiveProductTagEmailTemplate = [SELECT Id,htmlvalue,Subject FROM EmailTemplate WHERE developerName = 'KARL_Product_Tag_Inactive_Notification' LIMIT 1];
        if(orgwideEmailAddressId == null || inactiveProductTagEmailTemplate == null){
            System.debug(Logginglevel.DEBUG,'Org Wide Address not found.');
            return;
        }
        List<Messaging.SingleEmailMessage> mailList = new List<Messaging.SingleEmailMessage>();
        List<Request_Item__c> updateRequestItemList = new List<Request_Item__c>();
        Set<Id> contactIdSet = new Set<Id>();
        Map<Id,Request_Item__c> requestItemIdToInfoMap = new Map<Id,Request_Item__c>();
        for(Request_Item__c requestItemObj : requestItemList){
            requestItemIdToInfoMap.put(requestItemObj.Id,requestItemObj);
            requestItemObj.Inactive_Product_Tag__c = true;
            requestItemObj.Suggested_Product_Tag__c = defaultProductTagId[0].Id;
            updateRequestItemList.add(requestItemObj);
            //Get all Message Item
            if(requestItemObj.KARL_GRC_Assignee__c != null){
                Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
                String htmlBody = inactiveProductTagEmailTemplate.htmlvalue;
                htmlBody = htmlBody.replace('{!gusUserName}',requestItemObj.KARL_GRC_Assignee__r.Name);
                htmlBody = htmlBody.replace('{!requestMasterData}',requestItemObj.Name);
                    message.setSaveAsActivity(false); 
                    message.setTemplateId(inactiveProductTagEmailTemplate.id);
                    message.setHtmlBody(htmlBody);
                    message.setSubject(inactiveProductTagEmailTemplate.Subject);
                    message.setToAddresses(new List<String>{requestItemObj.KARL_GRC_Assignee__c}); 
                    message.setOrgWideEmailAddressId(orgwideEmailAddressId);
                    mailList.add(message);
                
            }
        }
        Messaging.SendEmailResult[] emailResults =  Messaging.sendEmail( mailList, false);
        for(Messaging.SendEmailResult result : emailResults){
            if(!result.isSuccess()){
                System.debug(Logginglevel.DEBUG,'Error Received');
                for(Database.Error error : result.getErrors()){
                    System.debug(Logginglevel.DEBUG,error.getMessage());
                }
            }
        }
        if(!updateRequestItemList.isEmpty()){
            update updateRequestItemList;
            System.debug(Logginglevel.DEBUG,'updateRequestItemList = '+updateRequestItemList);
        }
        
    }

    public void finish(Database.BatchableContext BC){}

    
    public static void start(){
        System.schedule('Send Inactive Notification for Product tag', '0 0 7 * * ? *', new KARL_NotifyInactiveProductTagBatch());
    }
}
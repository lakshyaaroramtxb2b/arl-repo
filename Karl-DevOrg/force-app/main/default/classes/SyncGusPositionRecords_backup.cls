global class SyncGusPositionRecords_backup implements Database.Batchable<sObject>, Database.Stateful {

    public String SOURCE_FILE = 'SyncGusPositionRecords';
    public List<Headcount_c__x> lstGusHeadCounts;
    public List<Headcount__c> lstSecOrgHeadCounts = new List<Headcount__c>();
    
    public Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([
                             SELECT ExternalId,
                                    Name__c,
                                    Team_c__c,
                                    Role_c__c,
                                    Parent_Cloud_c__c,
                                    Parent_Cloud_c__r.Name__c,
                                    Effective_Date_c__c,
                                    Status_c__c,
                                    Recruiter_Status_c__c,
                                    Candidate_Name_c__c,
                                    Target_Start_Date_c__c,
                                    Backfill_Name_c__c,
                                    Backfill_Date_c__c,
                                    Headcount_Name_c__c,
                                    Employee_Name_Text_c__c,
                                    Hiring_Manager_c__c,
                                    Hiring_Manager_c__r.Name__c,
                                    Talentforce_Offer_c__c,
                                    Initial_Status_Value_c__c,
                                    Original_Effective_Date_c__c,
                                    Recruiter_c__c,
                                    Division_c__c,
                                    Description_c__c,
                                    Recruiter_Update_c__c,
                                    LastModifiedById__c,
                                    LastModifiedDate__c,
                                    Country_c__c,
                                    City_c__c,
                                    State_c__c,
                                    Employee_Number_Text_c__c,
                                    Cost_Center_c__c,
                                    Business_Unit_c__c,
                                    Cost_Center_LU_c__c,
                                    Business_Unit_LU_c__c,
                                    Cost_Center_LU_c__r.Name__c,
                                    Business_Unit_LU_c__r.Name__c,
                                    Job_Status_Probability_of_Close_c__c 
                            FROM Headcount_c__x]);
    }

    public void execute(Database.BatchableContext bc, List<Headcount_c__x> lstGusHeadCounts) {
        List<Headcount__c> lstSecOrgHeadCounts = new List<Headcount__c>();
        Headcount__c headCount;
        String latestId;
        Set<Id> setGusPositionIds = new Set<Id>();
        
        for(Headcount_c__x gusHeadCount : lstGusHeadCounts){
          setGusPositionIds.add(gusHeadCount.ExternalId);
        }

        for(Headcount_c__x gusHeadCount : lstGusHeadCounts) {
            headCount = new Headcount__c();
            headCount.Gus_Position_Id__c = gusHeadCount.ExternalId;
            headCount.Name = gusHeadCount.Name__c;
            headCount.Team__c = gusHeadCount.Team_c__c;
            headCount.Role__c = gusHeadCount.Role_c__c;
            headCount.Parent_Cloud__c = gusHeadCount.Parent_Cloud_c__c;
            headCount.Parent_Cloud_Text__c = gusHeadCount.Parent_Cloud_c__r.Name__c;
            headCount.Effective_Date__c = gusHeadCount.Effective_Date_c__c;
            headCount.Status__c = gusHeadCount.Status_c__c;
            headCount.Recruiter_Status__c = gusHeadCount.Recruiter_Status_c__c;
            headCount.Candidate_Name__c = gusHeadCount.Candidate_Name_c__c;
            headCount.Target_Start_Date__c = gusHeadCount.Target_Start_Date_c__c;
            headCount.Backfill_Name__c = gusHeadCount.Backfill_Name_c__c;
            headCount.Backfill_Date__c = gusHeadCount.Backfill_Date_c__c;
            headCount.Headcount_Name__c = gusHeadCount.Headcount_Name_c__c;
            headCount.Employee_Number_Text__c = gusHeadCount.Employee_Number_Text_c__c;
            if(headCount.Headcount_Name__c == null) {
              headCount.Employee_Name_Text__c = null;
              headCount.Employee_Number_Text__c = null;
            }
            //if(gusHeadCount.Headcount_Name_c__c != null) {
            //  headCount.Employee_Name_Text__c = gusHeadCount.Headcount_Name_c__r.Name__c;
            //}
            headCount.Hiring_Manager__c = gusHeadCount.Hiring_Manager_c__c;
            headCount.Hiring_Manager_Text__c = gusHeadCount.Hiring_Manager_c__r.Name__c;
            headCount.Talentforce_Offer__c = gusHeadCount.Talentforce_Offer_c__c;
            headCount.Initial_Status_Value__c = gusHeadCount.Initial_Status_Value_c__c;
            headCount.Original_Effective_Date__c = gusHeadCount.Original_Effective_Date_c__c;
            headCount.Recruiter__c = gusHeadCount.Recruiter_c__c;
            headCount.Description__c = gusHeadCount.Description_c__c;
            headCount.Recruiter_Update__c = gusHeadCount.Recruiter_Update_c__c;
            headCount.LastModifiedBy__c = gusHeadCount.LastModifiedById__c;
            headCount.LastModifiedDate__c = gusHeadCount.LastModifiedDate__c;
            headCount.Country__c = gusHeadCount.Country_c__c;
            headCount.City__c = gusHeadCount.City_c__c;
            headCount.State__c = gusHeadCount.State_c__c;
            headCount.Cost_Center_LU__c = gusHeadCount.Cost_Center_LU_c__c;
            headCount.Cost_Center__c = gusHeadCount.Cost_Center_LU_c__r.Name__c;
            headCount.Business_Unit_LU__c = gusHeadCount.Business_Unit_LU_c__c;
            headCount.Business_Unit__c = gusHeadCount.Business_Unit_LU_c__r.Name__c; 
            headCount.Job_Status_Probability_of_Close__c = gusHeadCount.Job_Status_Probability_of_Close_c__c;
            lstSecOrgHeadCounts.add(headCount);
        }

        Schema.SObjectField upsertField = Headcount__c.Gus_Position_Id__c; 
        List<Database.UpsertResult> saveResults = Database.upsert(lstSecOrgHeadCounts, upsertField, false);
        
        system.debug('*********'+lstSecOrgHeadCounts.size());
        system.debug('$$$$$$$$$$'+saveResults.size());
        Integer numberOfInsertedPositions = 0;
        Integer numberOfUpdatedPositions = 0;
        String finalErrorText = '';
        for (Database.UpsertResult sr : saveResults) {
            if (sr.isSuccess()) {
               if(sr.isCreated()) {
                 numberOfInsertedPositions++;
               }else {
                 numberOfUpdatedPositions++; 
               }        
            } else {
                        for(Database.Error err : sr.getErrors()) {
                            finalErrorText += err.getStatusCode() + ': ' + err.getMessage() + '\n';
                        }
            }
        }

        Esa_DebugService.writeMessage('SyncGusPositionRecords Completed:');
        if(string.isNotBlank(finalErrorText)) {
            system.debug('##########'+finalErrorText);
            Esa_DebugService.writeMessage('SyncGusPositionRecords.finish: Failed with errors');
            Exception ex;
            Esa_DebugService.writeException(ex, 1, SOURCE_FILE, finalErrorText);
        }
    }
    
    

    public void finish(Database.BatchableContext BC) {
         Esa_Script_Gus_PositionsObject.updateEmployeeName();
        //Esa_Script_Gus_PositionsObject.updateParentCloudInfo();
        //Esa_Script_Gus_PositionsObject.updateHiringManagersInfo();
        //Esa_Script_Gus_PositionsObject.updateLastModifiedByInfo();
        //Esa_Script_Gus_PositionsObject.updateRecruiterInfo();
    }

}
/**
* @File Name          : IEM_GroupsAndPermissionSetControllerTest.cls
* @Description        : 
* @Author             : Banshi
* @Group              : 
* @Last Modified By   : Banshi
* @Last Modified On   : 11/21/2019, 2:35:58 PM
* @Modification Log   : 
* Ver       Date            Author      		    Modification
* 1.0    11/21/2019   Banshi     Initial Version
**/
@isTest
public class IEM_GroupsAndPermissionSetControllerTest {
    @isTest
    static void getPublicGroupsTest(){
        Integer totalExistingGroups = IEM_GroupsAndPermissionSetController.getPublicGroups().size();
        Group testGroup = new Group();
        testGroup.Name = 'test';
        insert testGroup;
        System.assertEquals(totalExistingGroups, IEM_GroupsAndPermissionSetController.getPublicGroups().size());
    }
    @isTest
    static void getPermissionSetsTests(){
        Integer totalExistingPermissionSets = IEM_GroupsAndPermissionSetController.getPermissionSets().size();
        PermissionSet permissionSet = new PermissionSet();
        permissionSet.Name = 'permissionset1';
        permissionSet.Label = 'permissionset1';
        insert permissionSet;
        System.assertEquals(totalExistingPermissionSets, IEM_GroupsAndPermissionSetController.getPermissionSets().size());
    }
    @isTest
    static void getMembersTest(){
        Group testGroup = new Group();
        testGroup.Name = 'test';
        insert testGroup;
        
        GroupMember gm = new GroupMember();
        gm.GroupId = testGroup.Id;
        gm.UserOrGroupId = Userinfo.getUserId();
        insert gm;
        List<IEM_GroupsAndPermissionSetController.GetMembersWrapper> membersList = IEM_GroupsAndPermissionSetController.getMembers('Group',testGroup.Id);
        System.assertEquals(1, membersList.size());
        
        PermissionSet permissionSet = new PermissionSet();
        permissionSet.Name = 'permissionset1';
        permissionSet.Label = 'permissionset1';
        insert permissionSet;
        
        
        PermissionSetAssignment psa = new PermissionSetAssignment();
        psa.PermissionSetId = permissionSet.Id;
        psa.AssigneeId = Userinfo.getUserId();
        insert psa;
        
        membersList = IEM_GroupsAndPermissionSetController.getMembers('PermissionSet',permissionSet.Id);
        System.assertEquals(1, membersList.size());
    }
    @isTest
    static void getUsersTest(){
        System.assert(IEM_GroupsAndPermissionSetController.getUsers().size() > 1);
    }
    @isTest
    static void updateMembersTest(){
        Profile profile = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User userRec = new User(Alias = 'standt', Email='standarduser'+System.now().millisecond()+'@testorg.com', 
                                EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                                LocaleSidKey='en_US', ProfileId = profile.Id, 
                                TimeZoneSidKey='America/Los_Angeles', UserName='standarduser'+System.now().millisecond()+'@testorg.com');
        insert  userRec;       
        Group testGroup = new Group();
        testGroup.Name = 'test';
        insert testGroup;
        
        GroupMember gm = new GroupMember();
        gm.GroupId = testGroup.Id;
        gm.UserOrGroupId = userRec.Id;
        insert gm;
        
        List<Id> existingMemberIds = new List<Id>();
        existingMemberIds.add(userRec.Id);
        
        List<Id> newMemberIds = new List<Id>();
        newMemberIds.add(Userinfo.getUserId());
        newMemberIds.add(userRec.Id);
        
        List<IEM_GroupsAndPermissionSetController.GetMembersWrapper> wrapperList = new List<IEM_GroupsAndPermissionSetController.GetMembersWrapper>();
        IEM_GroupsAndPermissionSetController.GetMembersWrapper memberDataWrapper = new IEM_GroupsAndPermissionSetController.GetMembersWrapper();
        memberDataWrapper.MemberId = Userinfo.getUserId();
        memberDataWrapper.Name = userInfo.getUserName();
        wrapperList.add(memberDataWrapper);
        
        String memberListString = JSON.serialize(wrapperList);
        SYstem.debug('memberListString-> '+ memberListString);
        //String memberListString = '[{"MemberId":"'+Userinfo.getUserId()+'","Name":"'+userInfo.getUserName()+'"}]';
        String result = IEM_GroupsAndPermissionSetController.updateMembers('Group',testGroup.Id, memberListString, existingMemberIds);
        List<GroupMember> groupMemberList = [Select Id From GroupMember Where GroupId= :testGroup.Id];
        // System.assertEquals(1,groupMemberList.size());
        
        result = IEM_GroupsAndPermissionSetController.updateMembers('Group',testGroup.Id, memberListString, existingMemberIds);
        groupMemberList = [Select Id From GroupMember Where GroupId= :testGroup.Id];
        //System.assertEquals(1,groupMemberList.size());
        
        String emptyList = null;
        //result = IEM_GroupsAndPermissionSetController.updateMembers('Group',testGroup.Id,emptyList , existingMemberIds);
        //groupMemberList = [Select Id From GroupMember Where GroupId= :testGroup.Id];
        //System.assertEquals(1,groupMemberList.size());
        
    }
    
    @isTest
    static void updatePermissionSetMembersTest(){
        Profile profile = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User userRec = new User(Alias = 'standt', Email='standarduser'+System.now().millisecond()+'@testorg.com', 
                                EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                                LocaleSidKey='en_US', ProfileId = profile.Id, 
                                TimeZoneSidKey='America/Los_Angeles', UserName='standarduser'+System.now().millisecond()+'@testorg.com');
        insert  userRec;       
        PermissionSet permissionSet = new PermissionSet();
        permissionSet.Name = 'permissionset1';
        permissionSet.Label = 'permissionset1';
        insert permissionSet;
        
        
        PermissionSetAssignment psa = new PermissionSetAssignment();
        psa.PermissionSetId = permissionSet.Id;
        psa.AssigneeId = userRec.Id;
        insert psa;
        List<Id> existingMemberIds = new List<Id>();
        existingMemberIds.add(userRec.Id);
        
        List<Id> newMemberIds = new List<Id>();
        newMemberIds.add(Userinfo.getUserId());
        newMemberIds.add(userRec.Id);
        
        
        List<IEM_GroupsAndPermissionSetController.GetMembersWrapper> wrapperList = new List<IEM_GroupsAndPermissionSetController.GetMembersWrapper>();
        IEM_GroupsAndPermissionSetController.GetMembersWrapper memberDataWrapper = new IEM_GroupsAndPermissionSetController.GetMembersWrapper();
        memberDataWrapper.MemberId = Userinfo.getUserId();
        memberDataWrapper.Name = userInfo.getUserName();
        wrapperList.add(memberDataWrapper);
        
        String getMemberSTring = JSON.serialize(wrapperList);
        String result = IEM_GroupsAndPermissionSetController.updateMembers('PermissionSet',permissionSet.Id, getMemberSTring, existingMemberIds);
        List<PermissionSetAssignment> permissionSetAssignmentList = [Select Id From PermissionSetAssignment Where PermissionSetId= :permissionSet.Id];
        //System.assertEquals(0,permissionSetAssignmentList.size());
        
        existingMemberIds.add(Userinfo.getUserId());
        result = IEM_GroupsAndPermissionSetController.updateMembers('PermissionSet',permissionSet.Id, getMemberSTring, existingMemberIds);
        permissionSetAssignmentList = [Select Id From PermissionSetAssignment Where PermissionSetId= :permissionSet.Id];
        //System.assertEquals(0,permissionSetAssignmentList.size());
        
        /*result = IEM_GroupsAndPermissionSetController.updateMembers('PermissionSet',permissionSet.Id, new List<Id>{}, existingMemberIds);
permissionSetAssignmentList = [Select Id From PermissionSetAssignment Where PermissionSetId= :permissionSet.Id];
System.assertEquals(1,permissionSetAssignmentList.size()); */
        
    }
    
    @isTest
    public static void retriveUserTest(){
        Profile profile = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User userRec = new User(Alias = 'standt', Email='standarduser'+System.now().millisecond()+'@testorg.com', 
                                EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                                LocaleSidKey='en_US', ProfileId = profile.Id, 
                                TimeZoneSidKey='America/Los_Angeles', UserName='standarduser'+System.now().millisecond()+'@testorg.com');
        insert  userRec;
        
        List<IEM_GroupsAndPermissionSetController.GetMembersWrapper> wrapperList = new List<IEM_GroupsAndPermissionSetController.GetMembersWrapper>();
        IEM_GroupsAndPermissionSetController.GetMembersWrapper memberDataWrapper = new IEM_GroupsAndPermissionSetController.GetMembersWrapper();
        memberDataWrapper.MemberId = Userinfo.getUserId();
        memberDataWrapper.Name = userInfo.getUserName();
        wrapperList.add(memberDataWrapper);        
        String getMemberSTring = JSON.serialize(wrapperList);
        
        IEM_GroupsAndPermissionSetController.retriveUser('s', getMemberSTring);
    }
}
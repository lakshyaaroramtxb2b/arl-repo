/**
* @author Vishnu Kumar
* @email vishnu.kumar@mtxb2b.com
* @desc This is test class for ARL_AuditCycleReportController.
*/
@isTest
public class ARL_AuditCycleReportControllerTest {

    /**
    * @author Vishnu Kumar
    * @email vishnu.kumar@mtxb2b.com
    * @desc Test data setup.
    */
	@testSetup
    private static void testSetup(){
        Request_Item__c reqItem = ARL_TestDataFactory.createRequestItem();
        reqItem.Areas_of_Compliance__c  = 'HITRUST;FedRAMP Moderate';
        insert reqItem;
        
        Audit_Cycle__c auditCycle = ARL_TestDataFactory.createAuditCycle();
        insert auditCycle;
        
        Audit_Cycle_Coverage__c acc = ARL_TestDataFactory.createAuditCycleCoverage(auditCycle.Id);
        acc.Area__c = 'HITRUST';
        acc.Scope__c  = 'HK';
        insert acc;
        
        Audit_Cycle_Coverage__c acc2 = ARL_TestDataFactory.createAuditCycleCoverage(auditCycle.Id);
        acc2.Area__c = 'FedRAMP Moderate';
        acc2.Scope__c  = 'HK';
        insert acc2;
        
        Control_Scope__c cs = ARL_TestDataFactory.createControlScope();
        cs.Scope_Name__c  = 'HK';
        insert cs;
        
        Control_Test__c ct = ARL_TestDataFactory.createControlTest(cs.id);
        insert ct;
        
        Request_Item_Control__c ric = ARL_TestDataFactory.createRequestItemControl(reqItem.id,ct.Id);
        insert ric;
        
        Request_Item_Control__c ric2 = ARL_TestDataFactory.createRequestItemControl(reqItem.id,ct.Id);
        insert ric2;
        
        Cycle_Request_Item__c cri = ARL_TestDataFactory.createCycleRequestItem(reqItem.id,auditCycle.Id);
        insert cri;
        
        Audit_Cycle_Coverage_Item__c acci1 = ARL_TestDataFactory.createAuditCycleCoverageItem(acc.Id,cri.Id);
        insert acci1;
    }
    
    /**
    * @author Vishnu Kumar
    * @email vishnu.kumar@mtxb2b.com
    * @desc This method getting report for audit cycle and expecting a report of cycle request items.
    */
    @isTest
    private static void unitTest(){
        Audit_Cycle__c auditCycle = [select id from Audit_Cycle__c limit 1];
        List<ARL_AuditCycleReportController.ReportWrapper> reportItems = ARL_AuditCycleReportController.getReport(auditCycle.Id);
        
        //Assert: This should generate onlt one report item as Audit Cycle have only one Cycle Request Item
        system.assertEquals(1, reportItems.size() );
        
        ARL_AuditCycleReportController.isCommunity();
    }
}
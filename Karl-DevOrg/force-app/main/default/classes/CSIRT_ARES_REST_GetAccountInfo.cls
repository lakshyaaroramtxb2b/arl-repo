@RestResource(urlMapping='/ARES/Org62/GetAccountInfo/*')
global with sharing class CSIRT_ARES_REST_GetAccountInfo {
	@HttpPost 
	global static String GetAccountInfo(String customerOrgId) {
        
        CSIRT_ARES_InternalOrgIntegration Org62 = new CSIRT_ARES_InternalOrgIntegration('ORG62');
        return Org62.GetAccountInfo(customerOrgId);
        
        
   }
}
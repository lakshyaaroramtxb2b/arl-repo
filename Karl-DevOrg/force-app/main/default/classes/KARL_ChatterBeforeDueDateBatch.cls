/**
 * @author Lakshya Arora
 * @email lakshya.arora@mtxb2b.com
 * @desc This is schedulable and batch class to chatter on Gus Work Record due date for notify days before due date .
 */

public with sharing class KARL_ChatterBeforeDueDateBatch implements Database.Batchable<sObject>, Schedulable{
    /**
    * @author Lakshya Arora
    * @email lakshya.arora@mtxb2b.com
    * @desc This method is for schedulable interface to execute.
    */
    public void execute(SchedulableContext sc) {
        database.executebatch( new KARL_ChatterBeforeDueDateBatch(),200);
    }

    /**
    * @author Lakshya Arora
    * @email lakshya.arora@mtxb2b.com
    * @desc This method is to start the batch which will fetch all the cycle request items that are not closed and Ready for review
    */
    public Database.QueryLocator start(Database.BatchableContext BC){
        String cycleRequestItemStatusClosed = 'Closed';
        String cycleRequestItemStatusRFR = 'Ready For Review';
        String query ='';
        if(!Test.isRunningTest()){
             query = 'SELECT Id,Audit_Cycle__c,Request_Assignee__c,Period_Of_Days__c,Request_Gus__c,Cycle_Request_Status__c,Name,Audit_Cycle_Coverage__r.Auto_Assignment_Configuration__r.Days_Before_Due_Date_to_Notify__c,Request_Gus__r.Due_Date_c__c,Request_Assignee__r.Name '
                        + ' FROM Cycle_Request_Item__c '
                        + ' WHERE Cycle_Request_Status__c  != :cycleRequestItemStatusClosed AND Cycle_Request_Status__c  != :cycleRequestItemStatusRFR' 
                        + ' AND Request_Gus__c != null AND Request_Assignee__c != null AND Audit_Cycle_Coverage__r.Auto_Assignment_Configuration__r.Days_Before_Due_Date_to_Notify__c != null '
                        + ' WITH SECURITY_ENFORCED';
        }
        else{
            // as it is referncing an external object dont ned to use security enforced -> used in test class
            query = 'SELECT Id,Audit_Cycle__c,Request_Assignee__c,Period_Of_Days__c,Request_Gus__c,Cycle_Request_Status__c,Name,Audit_Cycle_Coverage__r.Auto_Assignment_Configuration__r.Days_Before_Due_Date_to_Notify__c,Request_Gus__r.Due_Date_c__c,Request_Assignee__r.Name '
                        + ' FROM Cycle_Request_Item__c '
                        + ' WHERE Cycle_Request_Status__c  != :cycleRequestItemStatusClosed AND Cycle_Request_Status__c  != :cycleRequestItemStatusRFR';
        }
        return Database.getQueryLocator(query);
    }

    /**
    * @author Lakshya Arora
    * @email lakshya.arora@mtxb2b.com
    * @desc This method will calculate the due date and post chatter accordingly
    */
    public void execute(Database.BatchableContext BC, List<Cycle_Request_Item__c> cycleRequestItemList){
        Date todayDate = System.today();
        Set<Id> contactIdSet = new Set<Id>();
        List<Cycle_Request_Item__c> elgibleCRIList = new List<Cycle_Request_Item__c>();
        if(!Test.isRunningTest()){
            for(Cycle_Request_Item__c criObj : cycleRequestItemList){
                if(criObj.Request_Gus__r.Due_Date_c__c != null){
                    Datetime dueDate = criObj.Request_Gus__r.Due_Date_c__c;
                    Date compareDueDate = Date.newInstance(dueDate.yearGMT(), dueDate.monthGMT(), dueDate.dayGMT());
                    if(compareDueDate >= todayDate){
                        Date comparisonDate = KARL_Utility.calculateWorkingDate(Integer.valueOf(criObj.Audit_Cycle_Coverage__r.Auto_Assignment_Configuration__r.Days_Before_Due_Date_to_Notify__c), todayDate);
                        System.debug(LoggingLevel.DEBUG,'compareDueDate = '+compareDueDate);
                        System.debug(LoggingLevel.DEBUG,'comparisonDate = '+comparisonDate);
                        if(comparisonDate == compareDueDate){
                            contactIdSet.add(criObj.Request_Assignee__c);
                            elgibleCRIList.add(criObj);
                        }
                    }
                }
            }
        }
        else{
            for(Cycle_Request_Item__c criObj : cycleRequestItemList){
                Date comparisonDate = KARL_Utility.calculateWorkingDate(5, todayDate);
                    contactIdSet.add(criObj.Request_Assignee__c);
                    elgibleCRIList.add(criObj);
            }
        }
       
            List<FeedItem__x> feedItemList = new List<FeedItem__x>();
            for(Cycle_Request_Item__c criObj : elgibleCRIList){
                feedItemList.add(postChatterOnGusWork(
                    criObj.Request_Gus__c,
                    criObj.Request_Assignee__r.Name,
                    criObj.Cycle_Request_Status__c,
                    criObj.Name,
                    Integer.valueOf(criObj.Audit_Cycle_Coverage__r.Auto_Assignment_Configuration__r.Days_Before_Due_Date_to_Notify__c)
                ));
            }
            if(!feedItemList.isEmpty() && !Test.isRunningTest()){
                Database.insertImmediate(feedItemList);
                System.debug(LoggingLevel.DEBUG,'feedItemList= '+feedItemList);
            }
        
        
    }

    /**
    * @author Lakshya Arora
    * @email lakshya.arora@mtxb2b.com
    * @desc This method will create external FeedItem records
    * @return Feed item Record
    */
    private static FeedItem__x postChatterOnGusWork(String gusWorkRecordId,String requestAssigneeName, String cycleRequestItemStatus,String cycleRequestItemName,Integer dueDays){
        System.debug(LoggingLevel.DEBUG,'postChatterOnGusWork');
        FeedItem__x workFeedItem = new FeedItem__x();
        workFeedItem.Body__c = cycleRequestItemName+' is in '+cycleRequestItemStatus+'\n'+'@'+requestAssigneeName+' needs to ready for review/close the case in '+dueDays+' days.';
        workFeedItem.ParentId__c = gusWorkRecordId;
        workFeedItem.Status__c = 'Published';
        return workFeedItem;
    }

    /**
    * @author Lakshya Arora
    * @email lakshya.arora@mtxb2b.com
    * @desc This method will call after all the operations get completed
    */
    public void finish(Database.BatchableContext BC){}

    
    /**
    * @author Lakshya Arora
    * @email lakshya.arora@mtxb2b.com
    * @desc This method will schedule the batch daily at 7am
    */
    public static void start(){
        System.schedule('Chatter on Work', '0 0 7 * * ? *', new KARL_ChatterBeforeDueDateBatch());
    }
}
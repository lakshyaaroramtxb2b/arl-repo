/***********************************************************
* Helper for ProgramTrigger
* Created by 	- Prashant Gupta
* Date - 01-12-2019
************************************************************/
public class PRT_ProgramTriggerHelper {
    
/***********************************************************
* Handler class for PRT_ProjectTrigger
* Created by 	- Prashant Gupta
************************************************************/
    public static void updateInsertGUSRecords(List<Program__c> newList, Map<id,Program__c> oldMap){
        Set<ID> portfolioToCreateIDSet = new Set<ID>();
        Set<ID> userIDSet = new Set<ID>();
        for(Program__c programRec : newList){
            if(programRec.Program_Status__c=='Active' ){
                if(oldMap==null || (oldMap!=null && programRec.Parent_Program__c==oldMap.get(programRec.id).Parent_Program__c)){
                    portfolioToCreateIDSet.add(programRec.ID);
                    if(programRec.Program_Owner__c!=null)
                        userIDSet.add(programRec.Program_Owner__c);
                    if(programRec.Program_Manager__c!=null)
                        userIDSet.add(programRec.Program_Manager__c);
                    if(programRec.Executive_Sponsor__c!=null)
                        userIDSet.add(programRec.Executive_Sponsor__c);
                }
            }
        }        
        if(portfolioToCreateIDSet!=null && !portfolioToCreateIDSet.isEmpty() && !test.isRunningTest()){
            PRT_GUSSyncHelper.createUpdateProgramInGUS(portfolioToCreateIDSet,userIDSet);
        }        
    }   
    
    
     /*
     * Created by - Virendra Yadav
     * Date - 12-12-2019
     * This function is use to make budget allocation field total for  Project -> Program 
     */
    public static void setBudgetAllocationToPortfolio(List<Program__c> newList, Map<id,Program__c> oldMap){
        Set<Id> portfolioIds = new Set<Id>();
        Map<Id, Decimal> portfolioBudgetMap = new Map<Id, Decimal>();
        Set<Id> programIds = new Set<Id>();
        Map<Id, Decimal> programBudgetMap = new Map<Id, Decimal>();
        System.debug('??'+newList);
        for (Program__c program: newList) {
            System.debug('>>> oldMap>>' + oldMap);
            if((oldMap != null && oldMap.get(program.Id).Budget_Allocated__c != program.Budget_Allocated__c)) {
                if(program.Portfolio__c != null) {
                    portfolioBudgetMap.put(program.Portfolio__c, 0);
                    portfolioIds.add(program.Portfolio__c);
                }
                if(program.Parent_Program__c != null) {
                    programBudgetMap.put(program.Parent_Program__c, 0);
                    programIds.add(program.Parent_Program__c);
                }
            }
        }
System.debug('??'+portfolioIds);
        // Updating the portfolios
        List<Portfolio__c> updatedPortfolio = new List<Portfolio__c>();
        for(Program__c program : [SELECT Id, Budget_Allocated__c, Portfolio__c FROM Program__c WHERE Portfolio__c IN :portfolioIds]){
            if(program.Budget_Allocated__c != null) {
                portfolioBudgetMap.put(program.Portfolio__c, portfolioBudgetMap.get(program.Portfolio__c) + program.Budget_Allocated__c);
            }
        }

System.debug('??'+portfolioBudgetMap);
        for (Id portfolioId: portfolioIds) {
            if(portfolioId != null) {
                Portfolio__c portfolio = new Portfolio__c(id=portfolioId);
                portfolio.Budget_Allocated__c = portfolioBudgetMap.get(portfolioId);
                updatedPortfolio.add(portfolio);
            }
        }

        // Updating the programs
        List<Program__c> updatedProgram = new List<Program__c>();
            // SUM of sub projects
        for(Project__c project : [SELECT Id, Budget_Allocated__c, Program__c FROM Project__c WHERE Program__c IN :programIds]){
            if(project.Budget_Allocated__c != null) {
                programBudgetMap.put(project.Program__c, programBudgetMap.get(project.Program__c) + project.Budget_Allocated__c);
            }
        }

            // SUM of sub programs
        for(Program__c program : [SELECT Id, Budget_Allocated__c, Parent_Program__c FROM Program__c WHERE Parent_Program__c IN :programIds]){
            if(program.Budget_Allocated__c != null) {
                programBudgetMap.put(program.Parent_Program__c, programBudgetMap.get(program.Parent_Program__c) + program.Budget_Allocated__c);
            }
        }
        for (Id programId: programIds) {
            Program__c program = new Program__c(id=programId);
            program.Budget_Allocated__c = programBudgetMap.get(programId);
            updatedProgram.add(program);
        }

        if(updatedPortfolio!=null && !updatedPortfolio.isEmpty()){
            update updatedPortfolio;
        }
        if(updatedProgram!=null && !updatedProgram.isEmpty()){
            update updatedProgram;
        }
    }  
    
    /*
     * Created by - Prashant Gupta
     * Date - 13-12-2019
     */
    public static void setSubProgramName(List<Program__c> newList, Map<id,Program__c> oldMap){
        Map<id,String> parentProgramVsName = new Map<id,String>();
        Set<id> programIDSet = new Set<ID>();
        for(Program__c prog : newList){
            if(prog.Parent_Program__c!=null && (oldMap==null || prog.Parent_Program__c != oldMap.get(prog.id).Parent_Program__c || prog.Program_Status__c != oldMap.get(prog.id).Program_Status__c) ){
                programIDSet.add(prog.Id);
            }
        }
        
        if(programIDSet!=null && !programIDSet.isEmpty()){
            PRT_GUSSyncHelper.syncProgramName(programIDSet);
        } 
    }
}
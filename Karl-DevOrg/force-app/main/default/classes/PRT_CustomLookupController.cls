/**
 * @File Name          : PRT_CustomLookupController.cls
 * @Description        : 
 * @Author             : Prashant
 * @Group              : 
 * @Last Modified By   : Banshi
**/
public with sharing class PRT_CustomLookupController {
    public static V2MOM_c__x v2MOMRecord;

    @AuraEnabled
    public static List<RecordsData> fetchRecords(String objectName, String filterField, String searchString,
                                                 String value,String userId) {
        Set<String> objectNameList = new Set<String>{'Measure_c__x','User'};
        System.debug(objectName+' '+filterField+ ' '+searchString+ ''+value+' '+ userId);
        if(!objectNameList.contains(objectName)) return null;
        if(objectName=='User'){
            return searchInternalObject(objectName, filterField, searchString, value);
        }else{
            return searchExternalObject(objectName, filterField, searchString, value, 
                                        userId, null);
        }
        
    }
   
    public class RecordsData {
        @AuraEnabled public String label;
        @AuraEnabled public String value; 
        @AuraEnabled public User userRec;
        public RecordsData(String label, String value) {
            this.label = label;
            this.value = value;
        }
    }
    
    @AuraEnabled
    public static void deleteV2Mapping(String dependentMethodId, String controllingMeasureId){
        List<V2MomMapping__c> v2MomMappingList = new List<V2MomMapping__c>([SELECT ID 
                                                                            FROM V2MomMapping__c 
                                                                            WHERE Dependent_Method__c =:dependentMethodId 
                                                                            AND Controlling_Measure__c =:controllingMeasureId]);
        
        if(v2MomMappingList!= null && !v2MomMappingList.isEmpty()){
            delete v2MomMappingList;
        }
        
    }
    
    private static List<RecordsData> searchExternalObject(String objectName, String filterField, String searchString, String value,String userId, String employeeNo){
        System.debug('???' + employeeNo);
        List<RecordsData> recordsDataList = new List<RecordsData>();
            String employeenumber = null;
       if(userId!=null && userId!=''){
           for(User userRec : [SELECT id, ContactId, EmployeeNumber FROM User 
                               Where id = :userId]){
               System.debug('User Record in Search External Object ' + userRec);
            employeenumber = userRec.EmployeeNumber;
       }        
        }
            ID externalUserID = null;
            if(employeenumber!=null && employeenumber!=''){
                for(Org62User__x userRec: [SELECT ID,ExternalID, EmployeeNumber__c FROM Org62User__x WHERE EmployeeNumber__c = :employeenumber ] ){
                externalUserID = userRec.ExternalID;
                    System.debug('Org62 record ' +userRec );

            }
             System.debug('>>' +externalUserID);
                if(test.isRunningTest()){
                    String mockId = 'EX101';
                    String publishDate = String.valueOf(date.today());
                    String v2MomJSON = '{"attributes":{"type":"V2MOM_c__x","url":"/services/data/v48.0/sobjects/V2MOM_c__x/0054D000001WzcyQAC"},"V2MOM_User_c__c":"'+ mockId+'","ExternalId":"'+mockId+'","Name__c":"Test 123","Published_on_c__c":"'+publishDate+'"}';
                    V2MOM_c__x v2MOMRecord = (V2MOM_c__x)JSON.deserialize(v2MomJSON, V2MOM_c__x.class);  
                    
                }else{
                    v2MOMRecord = [SELECT ID,ExternalId,V2MOM_User_c__c FROM V2MOM_c__x WHERE V2MOM_User_c__c=:externalUserID Order by CreatedDate__c DESC LIMIT 1];
                System.debug('V2mom Record ' + v2MOMRecord);
                }
            }
            System.debug('External record value' + value);
            String query = 'SELECT Id,ExternalId, ' + filterField + ' FROM '+objectName;
            if(String.isNotBlank(value)) {
                query += ' WHERE ExternalId = \''+ value + '\' LIMIT 100';
            } else if(!test.isRunningTest()){
                query += ' WHERE '+filterField+
                    ' LIKE ' + '\'%' + String.escapeSingleQuotes(searchString.trim()) + '%\' AND V2MOM_c__c =\''+v2MOMRecord.ExternalId+'\' LIMIT 49999';
            }else{
                query += ' WHERE '+filterField+
                    ' LIKE ' + '\'%' + String.escapeSingleQuotes(searchString.trim()) + '%\' AND V2MOM_c__c =\''+'EX010'+'\' LIMIT 49999';
            }
            System.debug('Query' + query);
            for(SObject s : Database.query(query)) {
                
                System.debug('Query' + s);
                recordsDataList.add( new RecordsData((String)s.get(filterField), (String)s.get('ExternalId')) );
            }
        
        
        return recordsDataList;
    }
    private static List<RecordsData> searchInternalObject(String objectName, String filterField, String searchString, String value){
        System.debug('???' + value);
       
            
            List<RecordsData> recordsDataList = new List<RecordsData>();
            String query = 'SELECT Id, ' + filterField + ' FROM '+objectName;
            if(String.isNotBlank(value)) {
                query += ' WHERE id= \''+ value + '\' AND contactId =null';
            } else {
                query += ' WHERE '+filterField+
                        ' LIKE ' + '\'%' + String.escapeSingleQuotes(searchString.trim()) + '%\'  AND contactId =null';
            }
            if(objectName == 'User'){
                query+=' AND isactive  = true';
            }
            query+=' Limit 100';
            System.debug('Query' + query);
            for(SObject s : Database.query(query)) {
                
                System.debug('Query' + s);
                recordsDataList.add( new RecordsData((String)s.get(filterField), (String)s.get('Id')) );
            }
            return recordsDataList;
    }
}
global class HoneypotEmailHandler implements Messaging.InboundEmailHandler {
  private final Pattern EMAILPATTERN = pattern.compile('([a-zA-Z0-9\\-_\\.]+@[a-zA-Z0-9\\-_\\.]+)');
  private Notifier.NamedNotifier logger;
  
  global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email,
    Messaging.InboundEnvelope envelope) {
 
    logger = Notifier.getNotifier('EmailHoneyPot');
    
    Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();
    result.success = True;
 
    String [] targetEmail = getTargetEmail(email);
    
    List<Email_Honeypot__c> entryL = [SELECT Contact_Name__c,Contact_Email__c,Vendor__c,Description__c,
                                        Authorized_Source_Email__c
                                      FROM Email_Honeypot__c
                                      WHERE Email__c IN: targetEmail];
    System.debug(targetEmail);
    if (entryL.size() < 1) {
        logger.log(Notifier.Severity.WARNING,'Random SPAM on InboundEmailHandler:\n\n',new String[] {msgToString(email)});
        return result;
    }
    
    Email_Honeypot__c entry = entryL.get(0);
    String sourceEmail = getSourceEmail(email);
    if (entry.Authorized_Source_Email__c != sourceEmail) {
        logger.log(Notifier.Severity.CRITICAL,'Unauthorized message to '+entry.Vendor__c+' honeypot! :\n\n',
                                                new String[] {entryToString(entry,sourceEmail),msgToString(email)}
                    );
        return result;
    } else {
        logger.log(Notifier.Severity.INFO,'Authorized email on address :\n\n',
                                            new String[] {entryToString(entry,sourceEmail),msgToString(email)}
                    );
        return result;
    }
    
    /*
    if (email.binaryAttachments != null && email.binaryAttachments.size() > 0) {
      for (integer i = 0 ; i < email.binaryAttachments.size() ; i++) {
        Attachment attachment = new Attachment();
        // attach to the newly created contact record
        attachment.ParentId = contact.Id;
        attachment.Name = email.binaryAttachments[i].filename;
        attachment.Body = email.binaryAttachments[i].body;
        insert attachment;
      }
    }
    */
    return result;
 
  }
  
  private String [] getTargetEmail(Messaging.InboundEmail email){
    Set<String> addresses = new Set<String>();
    
    for (String textString: email.toAddresses) {
      System.debug(textString);
      Matcher m = EMAILPATTERN.matcher(textString);
      while (m.find()) { 
          addresses.add(m.group());
      }
    }
    
    // lame.  Need List.valueOf(set)
    String[] tmp = new String[]{};
    for (String s: addresses) {
      tmp.add(s);
    }
    return tmp;
  }
  
  private String getSourceEmail(Messaging.InboundEmail email) {
    Matcher m = EMAILPATTERN.matcher(email.fromAddress);
    if (m.find()) {
      return m.group();
    }
    // This should really throw an exception.
    logger.log(Notifier.Severity.WARNING,'Parse error on source address :\n\n'+email.fromAddress);  
    return 'PARSE ERROR';
  }
  
  private String msgToString(Messaging.InboundEmail email) {
    String msg = '';
    for (Messaging.InboundEmail.Header h : email.headers) {
        msg += h.name+':'+h.value+'\n\n';
    }
    msg += email.plainTextBody+'\n\n\n\n';
    msg += email.htmlBody;
    return msg;
  }
  
  private String entryToString(Email_Honeypot__c pot,String actualSource) {
    String msg = 'Vendor: '+pot.Vendor__c+'\n';
    msg += 'Authorized Source: '+pot.Authorized_Source_Email__c+'\n';
    msg += 'Actual Source: '+actualSource+'\n';
    msg += 'Contact Name: '+pot.Contact_Name__c+'\n';
    msg += 'Contact Email: '+pot.Contact_Email__c+'\n';
    msg += 'Description: '+pot.Description__c+'\n';
    return msg;
  }
    
    
 
}
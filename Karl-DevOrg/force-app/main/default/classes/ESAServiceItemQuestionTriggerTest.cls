@isTest
public class ESAServiceItemQuestionTriggerTest {
   @isTest
   static void EsaSItemTriggerTest() {
   		ESA_Entity__c esEntity = new ESA_Entity__c(Name='TestEntity',EntityCode__c='XYZ');
   		insert esEntity;

   		ESA_Service_Item_Category__c esCategory = new ESA_Service_Item_Category__c(Name='esCategory',ESA_Entity__c=esEntity.Id);
   		insert esCategory;

   		ESA_Service_Item__c esItem = new ESA_Service_Item__c(Name='Test SItem',Case_Queue__c='ESA',Order_Sequence__c=100,ItemCategory__c=esCategory.Id);
   		insert esItem;

   		ESA_Security_Question__c eQuestion1 = new ESA_Security_Question__c();
   		eQuestion1.Question__c = 'Question 1';
   		eQuestion1.Data_Type__c= 'Text (255)';
   		insert eQuestion1;

   		ESA_Security_Question__c eQuestion2 = new ESA_Security_Question__c();
   		eQuestion2.Question__c = 'Question 2';
   		eQuestion2.Data_Type__c= 'Text (255)';
   		eQuestion2.Controlling_Question__c = eQuestion1.Id;
   		insert eQuestion2;

   		List<ESA_Service_Item_Question__c> lstEsQuestion = new List<ESA_Service_Item_Question__c>();
   		ESA_Service_Item_Question__c esQueston = new ESA_Service_Item_Question__c();
   		esQueston.ESA_Security_Question__c = eQuestion1.Id;
   		esQueston.ESA_Service_Item__c = esItem.Id;
   		esQueston.Required__c = true; 
   		lstEsQuestion.add(esQueston);
   		esQueston = new ESA_Service_Item_Question__c();
   		esQueston.ESA_Security_Question__c = eQuestion2.Id;
   		esQueston.ESA_Service_Item__c = esItem.Id;
   		lstEsQuestion.add(esQueston);
   		insert lstEsQuestion;

   		lstEsQuestion[1].Required__c = true;
   		try {
   				update lstEsQuestion[1];
   				throw new applicationException('Excpeting a Exception Here for Dependent questions');
   			}catch(exception ex) {
   				Boolean expectedExceptionThrown =  ex.getMessage().contains('Dependent questions can\'t be required...') ? true : false;
				System.AssertEquals(expectedExceptionThrown, true);
   			}

   }

   public class applicationException extends Exception {}
}
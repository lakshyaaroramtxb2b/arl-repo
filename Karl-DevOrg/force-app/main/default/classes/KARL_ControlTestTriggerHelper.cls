/**
* @author Ben Harvie
* @email ben.harvie@salesforce.com
* @description This is trigger handler class for Control_Test__c.
*/
public with sharing class KARL_ControlTestTriggerHelper {
    public static Set<Id> recordIndexId = new Set<Id>();
    
    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description This method gets triggered from Control Test Trigger
    */
    public static void run(){
        // Before Update
        if( Trigger.isBefore && Trigger.isUpdate ){
            populateName();
        }
        // Before Insert
        else if( Trigger.isBefore && Trigger.isInsert ){
            populateName();
        }
        // After Update
        if( Trigger.isAfter && Trigger.isUpdate ){
            updateIndexFields();
        }
        // After Insert
        else if( Trigger.isAfter && Trigger.isInsert ){
            updateIndexFields();
        }
    }
    
    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Populate the Name field with appropriate information
    */
    public static void populateName(){
        System.debug(LoggingLevel.DEBUG, '--- START Updating Control Test Name ---');

        // Build list of controls
        Map<Id, Control_Scope__c> controlScopeItems = new Map <Id, Control_Scope__c>([SELECT Id,Triggered_Control_Identifier__c FROM Control_Scope__c 
                                                                        WITH SECURITY_ENFORCED]);

        // Update the name of the records
        Map<Id, Control_Test__c> oldMap = (Map<Id, Control_Test__c>)Trigger.oldMap;
        for(Control_Test__c controlTest : (List<Control_Test__c>)Trigger.New ){
            // Only process if Control Name is set
            if(!String.isBlank(controlTest.Control_Scope__c)){  
                String newName = controlScopeItems.get(controlTest.Control_Scope__c).Triggered_Control_Identifier__c + ' - ' + controlTest.Test_Name__c;
                Integer maxLength = 80;
                if(newName.length() > maxLength){
                    newName = newName.substring(0, maxLength);
                }
                
                if( Trigger.isInsert ||
                    ( Trigger.isUpdate && (controlTest.Name <> newName) ) ){
                        controlTest.Name = newName;
                }
            }
        }
        System.debug(LoggingLevel.DEBUG, '--- END Updating Test Scope Name ---');
    }
    
    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description This method updates Index fields for search capabilities
    */
    private static void updateIndexFields(){
        /**
         * @author Ben Harvie
         * @email ben.harvie@salesforce.com
         * @description Recursion logic based on set of Ids
         */
        for(Id recursionCheck : Trigger.newMap.keySet()){
            if(recordIndexId.contains(recursionCheck)){
                System.debug(LoggingLevel.DEBUG, 'Already processed this record >> ' + recursionCheck);
                return; // Kill the run - we've already processed this Id
            }else{
                System.debug(LoggingLevel.DEBUG, 'New record to process >> ' + recursionCheck);
                recordIndexId.add(recursionCheck); // add Id to set so we don't run it again
            }
        }

        List<Control_Test__c> controlTests = new List<Control_Test__c>();
        Integer maxLength = 80;

        for( Control_Test__c  ctItems : [SELECT Id,Control_Scope__r.KARL_Triggered_Scope_Name__c,KARL_Control_Scope_Number__c,
                                        Test_Name__c,KARL_Control_Scope_Name_Indexed__c, KARL_Control_Number_Indexed__c,
                                        KARL_CT_Scope__c, Control_Scope__r.Control_Scope_Control_Number__c,
                                        KARL_Control_Scope_Number_Indexed__c, Control_Scope__r.Scope_Name__c,
                                        Control_Test_Control_Number__c, Control_Scope__r.Modified_Control_Identifier__c
                                        FROM Control_Test__c WHERE Id in: (List<Control_Test__c >)Trigger.New 
                                        WITH SECURITY_ENFORCED]){
            
            String controlIdentifier = '';   

            // Update only if different
            if( ctItems.KARL_Control_Scope_Name_Indexed__c != ctItems.KARL_Control_Scope_Number__c + ' - ' + ctItems.Test_Name__c
                || ctItems.KARL_Control_Number_Indexed__c != ctItems.Control_Scope__r.Control_Scope_Control_Number__c
                || (ctItems.KARL_Control_Scope_Number_Indexed__c != ctItems.Control_Test_Control_Number__c + '-' + ctItems.Control_Scope__r.Scope_Name__c
                    || ctItems.KARL_Control_Scope_Number_Indexed__c != ctItems.Control_Scope__r.Modified_Control_Identifier__c)){
                // Set the Control Description in the Control Scope record
                Control_Test__c ctItem = new Control_Test__c();
                ctItem.Id = ctItems.Id;
                ctItem.KARL_Control_Number_Indexed__c = ctItems.Control_Scope__r.Control_Scope_Control_Number__c;
                
                // Check if the modified control identifer is in use, and set to that - otherwise establish value
                if(!String.isBlank(ctItems.Control_Scope__r.Modified_Control_Identifier__c)){
                    controlIdentifier = ctItems.Control_Scope__r.Modified_Control_Identifier__c;
                }else{
                    controlIdentifier = ctItems.Control_Test_Control_Number__c + '-' + ctItems.Control_Scope__r.Scope_Name__c;
                }

                ctItem.KARL_Control_Scope_Number_Indexed__c = controlIdentifier;
                ctItem.KARL_Control_Scope_Name_Indexed__c   = controlIdentifier + ' - ' + ctItems.Test_Name__c;
                controlTests.add(ctItem);
            }
        }
        
        // Make the update accounting for sharing and permission sets
		if( !controlTests.isEmpty() && Schema.sObjectType.Control_Test__c.isUpdateable() 
          	&& Control_Test__c.KARL_Control_Number_Indexed__c.getDescribe().isUpdateable() 
          	&& Control_Test__c.KARL_Control_Scope_Number_Indexed__c.getDescribe().isUpdateable() 
          	&& Control_Test__c.KARL_Control_Scope_Name_Indexed__c.getDescribe().isUpdateable() ){
            update controlTests;
        }
    }
}
/**
* @author Ben Harvie
* @email ben.harvie@salesforce.com
* @description This is trigger handler class for Control__c.
*/
public with sharing class ARL_ControlTriggerHandler {
    public static Set<Id> ctrlVersionId = new Set<Id>();
    
    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description This method gets triggered from Control Trigger
    */
    public static void run(){
        // Before Update
        if( Trigger.isBefore && Trigger.isUpdate ){
            captureCcfDescription();
        }
        // Before Insert
        else if( Trigger.isBefore && Trigger.isInsert ){
            captureCcfDescription();
        }
        //After Update
        if( Trigger.isAfter && Trigger.isUpdate ){
            captureControlVersion();
        }
        //After Insert
        else if( Trigger.isAfter && Trigger.isInsert ){
            captureControlVersion();
        }
    }
    
    
    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description This method creates a Control version when the control description changes.
    */
    private static void captureControlVersion(){
        /**
         * @author Ben Harvie
         * @email ben.harvie@salesforce.com
         * @description Recursion logic based on set of Ids
         */
        for(Id recursionCheck : Trigger.newMap.keySet()){
            if(ctrlVersionId.contains(recursionCheck)){
                System.debug(LoggingLevel.DEBUG, 'Already processed this record >> ' + recursionCheck);
                return; // Kill the run - we've already processed this Id
            }else{
                System.debug(LoggingLevel.DEBUG, 'New record to process >> ' + recursionCheck);
                ctrlVersionId.add(recursionCheck); // add Id to set so we don't run it again
            }
        }
        
        // Variable Declarations
        List<Control__c> controls = new List<Control__c>();
        List<Control_Version__c> controlVersions = new List<Control_Version__c>();
        Map<Id, Control__c> oldMap = (Map<Id, Control__c>)Trigger.oldMap;
        Map<Id, Control__c> newMap = (Map<Id, Control__c>)Trigger.newMap;

        // Filling map of Controls and thier Control Scopes
        Map<Id, List<Control_Scope__c > > controlAndControlScopes = new Map<Id, List<Control_Scope__c> >();
        for( Control_Scope__c csItem : [SELECT id,Control_Name__c from Control_Scope__c WHERE Control_Name__c  in: (List<Control__c>)Trigger.New ]){
            List<Control_Scope__c> csItems = new List<Control_Scope__c>();
            if( controlAndControlScopes.containsKey(csItem.Control_Name__c) ){
                csItems = controlAndControlScopes.get(csItem.Control_Name__c);
            }
            csItems.add(csItem);
            
            controlAndControlScopes.put(csItem.Control_Name__c , csItems);
        }
        
        // Creating Control Vesrions
        for(Control__c controlItem : (List<Control__c>)Trigger.New ){
            Control__c oldC = Trigger.isUpdate ? oldMap.get(controlItem.Id) : null;
			
            //Check
            //1. If is for insert then create version record
            //2. If is for update then check certain fields and if value changed then create version record
            if( Trigger.isInsert ||
              ( Trigger.isUpdate && (controlItem.Name <> oldC.Name  
              || controlItem.Control_Description__c <> oldC.Control_Description__c
              || controlItem.Control_Number__c <> oldC.Control_Number__c) )
              ){
                  Decimal oldVersion;
                  if(controlItem.Version__c == null){
                      oldVersion = 1;
                  }else{
                      oldVersion = controlItem.Version__c;
                  }
               	
                  Decimal version = Trigger.isUpdate ? (oldVersion + 1) : 1;
                                  
                controlVersions.add(new Control_Version__c(
                    Control__c = controlItem.Id,
                    Name = controlItem.Name,
                    Control_Description__c  = controlItem.Control_Description__c ,
                    Control_Identifier__c   = controlItem.Control_Number__c,
                    Version__c = version
                ));
             	
                //Update control item with new version
                Control__c cItem = new Control__c();
                cItem.Id = controlItem.Id;
                cItem.Version__c = version;
                cItem.KARL_Indexed_Control_Number__c = controlItem.Family_Name__c + '-' + controlItem.Control_Identifier__c;
                controls.add(cItem);
        	}else{
                Decimal setVersion;
                if( controlItem.Version__c == null){
                    setVersion = 1;
                }else{
                    // Maintain current version due to no change
                    setVersion = controlItem.Version__c;
                }

                Decimal version = setVersion;

                if(controlItem.Version__c == null){
                    controlVersions.add(new Control_Version__c(
                        Control__c = controlItem.Id,
                        Name = controlItem.Name,
                        Control_Description__c  = controlItem.Control_Description__c ,
                        Version__c = version
                    ));
                }
             	
                //Update control item with new version
                Control__c cItem = new Control__c();
                cItem.Id = controlItem.Id;
                cItem.Version__c = version;
                cItem.KARL_Indexed_Control_Number__c = controlItem.Family_Name__c + '-' + controlItem.Control_Identifier__c;
                controls.add(cItem);
            }
        }

		if( !controlVersions.isEmpty() && Schema.sObjectType.Control_Version__c.isCreateable() 
          	&& Control_Version__c.Name.getDescribe().isCreateable() 
          	&& Control_Version__c.Control_Description__c.getDescribe().isCreateable() 
          	&& Control_Version__c.Control_Identifier__c	.getDescribe().isCreateable() 
          	&& Control_Version__c.Version__c.getDescribe().isCreateable() ){
            insert controlVersions; 
        }
        
        if( !controls.isEmpty() && Schema.sObjectType.Control__c.isUpdateable() 
           && Control__c.Version__c.getDescribe().isUpdateable()){ 
            update controls; 
        }
                        
		// Update Related Control Scopes
        List<Control_Scope__c > CTScopeItemsToUpdate = new List<Control_Scope__c>();
        for( Control_Version__c controlV : controlVersions ){
            // Update Control Scopes with revised Control Versions
            if( controlAndControlScopes.containsKey(controlV.Control__c) ){
                
                   for( Control_Scope__c csItem : controlAndControlScopes.get(controlV.Control__c) ){
                       csItem.Control_Description__c  = controlV.Control_Description__c;
                       csItem.KARL_Triggered_Control_Number__c  = newMap.get(controlV.Control__c).KARL_Indexed_Control_Number__c;
                       csItem.KARL_Triggered_Control_Name__c	= controlV.Name;
                       CTScopeItemsToUpdate.add(csItem);
                   }
            }
        }
        
        if( !CTScopeItemsToUpdate.isEmpty() && Schema.sObjectType.Control_Scope__c.isUpdateable() 
          	&& Control_Scope__c.Control_Description__c.getDescribe().isUpdateable()){ 
            update CTScopeItemsToUpdate; 
        }
    }
    
    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description This method captures the CCF Control Description when aligned
    */
    private static void captureCcfDescription(){
        /**
         * @author Ben Harvie
         * @email ben.harvie@salesforce.com
         * @description Recursion logic based on set of Ids
         */

        // Build map of the CCF Controls
        //List<KARL_GRC_Controls__c> relevantControl;
        Map<Id, List<KARL_GRC_Controls__c>> ccfControls = new Map<Id, List<KARL_GRC_Controls__c>>();
        for ( KARL_GRC_Controls__c  ccfControlItems : [SELECT Id,Description__c,Short_Description__c
                                                        FROM KARL_GRC_Controls__c
                                                        WITH SECURITY_ENFORCED] ){
            if(!ccfControls.containsKey(ccfControlItems.Id)){
                ccfControls.put(ccfControlItems.Id,new List<KARL_GRC_Controls__c>());
            }
            ccfControls.get(ccfControlItems.Id).add(ccfControlItems);
        }
        
        for(Control__c controlItem : (List<Control__c>)Trigger.New ){
            if(String.isNotBlank(controlItem.CCF_Control__c) && controlItem.KARL_CCF_Alignment__c){
                System.debug(LoggingLevel.DEBUG, 'Aligning Control Descriptions with CCF : ' + controlItem.Id);
                for(KARL_GRC_Controls__c ccfControlValues : ccfControls.get(controlItem.CCF_Control__c)){
                    System.debug(LoggingLevel.DEBUG, 'CCF Control Values : ' + ccfControlValues);
                    System.debug(LoggingLevel.DEBUG, 'Current Control Values : ' + controlItem);
                     // Update the Name
                    String curName      = controlItem.Name;
                    String newName      = ccfControlValues.Short_Description__c;
                    Integer maxLength   = 80;
                    if(newName.length() > maxLength){
                        newName = newName.substring(0, maxLength);
                    }

                    // Update the Control Description
                    String controlDescription       = controlItem.Control_Description__c;
                    String ccfControlDescription    = ccfControlValues.Description__c;

                    if( Trigger.isInsert ||
                    ( Trigger.isUpdate && (controlDescription <> ccfControlDescription || curName <> newName) ) ){
                        controlItem.Name                    = newName;
                        controlItem.Control_Description__c  = ccfControlDescription;
                    }
                }
            }else{
                System.debug(LoggingLevel.DEBUG, 'Control is not aligned with CCF : ' + controlItem.Id);
            }
            
        }

    }
}
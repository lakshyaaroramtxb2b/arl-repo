/*

    Developer Name: Jorge L Caceres - jcaceres@salesforce.com | September 2015
    
    Description: Controller for TM_Matrix page.

*/

public with sharing class TM_Matrix_Beta {

    public static String baseURL { get; private set; } 
                                 {baseURL = 'https://' +ApexPages.currentPage().getHeaders().get('X-Salesforce-Forwarded-To');}
    
    public static boolean isAuthenticated { get; private set; } {
        isAuthenticated = (UserInfo.getUserType() != TM_Constants.GUEST_USER_TYPE);
    }
    
    public static String visibleBizUnits { get; private set; } {
        if (isAuthenticated) {
            visibleBizUnits = JSON.serialize(
            [SELECT Id, Name FROM Account WHERE RecordTypeId = :TM_Constants.BU_ACCOUNT_REC_TYPE_ID and TMM_Tracking__c = true]
            );
        } else {
            visibleBizUnits =  JSON.serialize(new List<Account>());
        }

    }
    
    @RemoteAction
    public static List<TM_AbstractObjective__c> getAllAbstracts() {
    
        If (Schema.sObjectType.TM_AbstractObjective__c.isAccessible()) {
            return [SELECT Name,Id,UUID__c,Description__c,Why__c,Requirements__c,ImplementationAdvice__c,
                ValidationAdvice__c,WhoCanHelp__c,SeeAlso__c,Deprecates__c,Dependencies__c,Tags__c,
                (SELECT Level__c,Category__c,Priority__c,Environment_Type__c FROM TM_Placements__r) 
                FROM TM_AbstractObjective__c];
        } else {
            return null;
        }
    }
    
    @RemoteAction
    public static List<TM_Objective__c> getAllConcretes(Id bizUnitId) {
        return selectStar(SObjectType.TM_Objective__c.getName(),
                         'WHERE Business_Unit__c = \'' + String.escapeSingleQuotes(String.valueOf(bizUnitId)) + '\'');
    }
    
    @RemoteAction
    public static List<Environment__c> getAllEnvironments(Id bizUnitId) {
       return selectStar(SObjectType.Environment__c.getName(),
                         'WHERE Account__c = \'' + String.escapeSingleQuotes(String.valueOf(bizUnitId)) + '\'');
    }

    @RemoteAction
    public static TM_AbstractObjective__c getAbstractByUuid(String uuid) {
    
        If (Schema.sObjectType.TM_AbstractObjective__c.isAccessible()) {
            return [SELECT Name,Id,UUID__c,Description__c,Why__c,Requirements__c,ImplementationAdvice__c,
                ValidationAdvice__c,WhoCanHelp__c,SeeAlso__c,Deprecates__c,Dependencies__c,Tags__c,
                (SELECT Level__c,Category__c,Priority__c,Environment_Type__c FROM TM_Placements__r) 
                FROM TM_AbstractObjective__c
                WHERE UUID__c = :uuid];
        } else {
            return null;
        }
    }
    
    @RemoteAction
    public static List<TM_Objective__c> getConcretesByUuid(Id bizUnitId, String uuid) {
        return selectStar(SObjectType.TM_Objective__c.getName(),
                         'WHERE Business_Unit__c = \'' + 
                         String.escapeSingleQuotes(String.valueOf(bizUnitId)) +
                         '\' AND TM_Placement__r.TM_AbstractObjective__r.UUID__c = \'' +
                         String.escapeSingleQuotes(String.valueOf(uuid)) + '\''
                         );
    }
    
    private static List<SObject> selectStar(String sobjectName, String whereClause) {
        system.debug('where: ' + whereClause);
        Set<String> fieldNames = schema.describeSObjects(new List<String> {sobjectName})[0].fields.getMap().keyset();
        List<String> iterableFields = new List<String>(fieldNames);
        return Database.query(String.format('SELECT {0} FROM {1} {2}', 
                              new List<String> {String.join(iterableFields, ','), 
                              sobjectName, 
                              (whereClause == null? '' : whereClause)})); 
    }

}
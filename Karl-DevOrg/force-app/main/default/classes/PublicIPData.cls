public with sharing class PublicIPData 
{
    public String jsonString {get;set;}

    //Constructor
    public PublicIPData()
    {
        jsonString = prepareData();
    }

    //Temp Method to prepare the Data
    private String prepareData()
    {
        String strIP = ApexPages.currentPage().getParameters().get('ip');

        if(strIP != null)
        { 
         HttpRequest req = new HttpRequest();
         req.setEndpoint('http://freegeoip.net/json/' + strIP);
         req.setMethod('GET');
        
         Http http = new Http();
         HTTPResponse res = http.send(req);
         
         return res.getBody();
        }
        
        return '[]';
    }
}
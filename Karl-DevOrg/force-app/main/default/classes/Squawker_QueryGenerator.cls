public class Squawker_QueryGenerator {

    public String createQueryString(List<SquawkRuleLogic> ruleLogic, SquawkerRule__c rule, DateTime lastScanTime)
    {
		string queryInner = 'SELECT Id FROM ' + rule.ObjectToWatch__c + ' WHERE ';
		
        Integer ruleCount = ruleLogic.size();
        Integer currentRuleIndex = 0;
        
        //Get list of feilds as we need to do some processing for each feild depending on type
        SObjectType objType = Schema.getGlobalDescribe().get(rule.ObjectToWatch__c);
		Map<String,Schema.SObjectField> mfields = objType.getDescribe().fields.getMap();
                
        for(SquawkRuleLogic logic : ruleLogic)
        {
			currentRuleIndex++;
            
            string objectfullname = rule.ObjectToWatch__c + '.' + logic.field;
			Schema.SObjectType targetType = Schema.getGlobalDescribe().get(objectfullname);
                
           	System.debug('objectfullname = ' + objectfullname);
            System.debug('targetType = ' + targetType);
                
            //get the field type using the map form before
            Schema.DisplayType fielddataType = mfields.get(logic.field).getDescribe().getType();
           	System.debug('logic.field is a ' + fielddataType);
                
            string parsedValue;
                    
            if(fielddataType == Schema.DisplayType.STRING || fielddataType == Schema.DisplayType.ID || fielddataType == Schema.DisplayType.REFERENCE || fielddataType == Schema.DisplayType.TEXTAREA)
            	parsedValue = '\'' + logic.value + '\'';
            else
                parsedValue = logic.value;
                
            queryInner += logic.field + ' ' + logic.op + ' ' + parsedValue;  
                
            if(currentRuleIndex < ruleCount)
                queryInner += ' AND ';
                
            System.debug('queryInner = ' + queryInner);
		}
           
        //Current version only looks at FeedItms of type TextPost
        string queryInnerLeft = 'SELECT Id, Title, Body, ParentId, CreatedBy.Name FROM FeedItem WHERE '
                    + ' ParentId in (' + queryInner + ') AND CreatedDate > ' + lastScanTime.format('yyyy-MM-dd\'T\'HH:mm:ssZ') + ' AND Type = \'TextPost\'';
                
        System.debug('Squawker_QueryGenerator = ' + queryInnerLeft);  
        
        return queryInnerLeft;
    }
}
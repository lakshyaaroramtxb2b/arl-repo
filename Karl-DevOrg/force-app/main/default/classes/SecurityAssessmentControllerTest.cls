@isTest
global class SecurityAssessmentControllerTest {
    @isTest
    public static void testToggleSaa() {
        SecurityAssesmentController ctr = new SecurityAssesmentController();
        System.assert(ctr.stage == SecurityAssesmentController.Stages.CHOOSE_ENV);
        ctr.target_environment = 'Salesforce_Services';
        ctr.setTargetEnv();
        System.assert(ctr.stage == SecurityAssesmentController.Stages.AUTHORIZE);
        ctr.postLogin();
        System.assert(ctr.stage == SecurityAssesmentController.Stages.ACCEPT_SAA);
        ctr.toggleSaa();
        System.assert(ctr.stage == SecurityAssesmentController.Stages.ACCEPT_SAA);
        ctr.SAA_Accepted = true;
        ctr.toggleSaa();
        System.assert(ctr.stage == SecurityAssesmentController.Stages.PENTEST_INFO);        
    }

    @isTest
    public static void testGetOauthAuthorizeURL() {
        SecurityAssesmentController ctr = new SecurityAssesmentController();
        String url = ctr.getOauthAuthorizeUrl();
        System.assert(url.contains(SecurityAssesmentController.OAUTH_CLIENT_ID));
    }

    @isTest
    public static void testHandleFBLogin() {
        String data = '{"name":"blah","id":"someid","email":"someemail"}';
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator(data));
        SecurityAssesmentController ctr = new SecurityAssesmentController();
        ctr.handleFBLogin();
        
        System.assert(ctr.userInfo != null);
        System.assert(ctr.userInfo.toString()=='FB|someid');
        System.assert(ctr.pm_name == 'blah');
        System.assert(ctr.stage == SecurityAssesmentController.Stages.ACCEPT_SAA);
    }

    @isTest
    public static void testHandleLinkedInLogin() {
        String data = '{"firstName":"blah","lastName":"last","id":"someid","emailAddress":"someemail"}';
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator(data));
        SecurityAssesmentController ctr = new SecurityAssesmentController();
        ctr.handleLinkedInLogin();
        
        System.assert(ctr.userInfo != null);
        System.assert(ctr.userInfo.toString()=='LI|someid');
        System.assert(ctr.pm_name == 'blah last');
        System.assert(ctr.stage == SecurityAssesmentController.Stages.ACCEPT_SAA);
    }

    @isTest
    public static void testHandleGoogleLogin() {
        String data = '{"name":"blah","sub":"someid","email":"someemail","aud":"'+SecurityAssesmentController.GOOGLE_CLIENT_ID+'"}';
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator(data));
        SecurityAssesmentController ctr = new SecurityAssesmentController();
        ctr.handleGoogleLogin();
        
        System.assert(ctr.userInfo != null);
        System.assert(ctr.userInfo.toString()=='GOOGLE|someid');
        System.assert(ctr.pm_name == 'blah');
        System.assert(ctr.stage == SecurityAssesmentController.Stages.ACCEPT_SAA);
    }

    @isTest
    public static void testSave() {
        SecurityAssesmentController ctr = new SecurityAssesmentController();
        ctr.pm_name = 'blah';
        ctr.publicIpStartRange1 = '192.2.2.1';
        ctr.publicIpStartRange2 = '192.2.2.1';
        ctr.publicIpStartRange3 = '192.2.2.1';
        ctr.publicIpStartRange4 = '192.2.2.1';
        ctr.publicIpStartRange5 = '192.2.2.1';
        
        ctr.publicIpEndRange1 = '192.168.1.1';
        ctr.publicIpEndRange1 = 'Test';
        ctr.publicIpEndRange1 = '192.162.1.1';
        ctr.publicIpEndRange1 = '192.162.1.1';
        
        ctr.userInfo = new SecurityAssesmentController.FBUserInfo();
        ctr.save();
        System.assert(ctr.record != null);
        System.assert(ctr.record.PM_Name__c == 'blah');
    }

    @isTest
    public static void testGetEnvironments() {
        SecurityAssesmentController ctr = new SecurityAssesmentController();
        System.assert(ctr.getEnvironments().size() != 0);
    }

    @isTest
    public static void testSFDCCode() {
        Test.setCurrentPageReference(new PageReference('/'));
        System.currentPageReference().getParameters().put('code', '1234');
        
        String[] data = new String[] {'{"id":"anid","access_token":"1234"}','{"user_id":"blah","organization_id":"someid","email":"someemail"}'};
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator(data));
        SecurityAssesmentController ctr = new SecurityAssesmentController();
        System.assert(ctr.userInfo.toString() == 'SFDC|blah:someid');
    }
    
    @isTest
    public static void testRecordLoading() {
        Test.setCurrentPageReference(new PageReference('/'));
        System.currentPageReference().getParameters().put('assessmenttId', 'asdf');
        SecurityAssesmentController ctr = new SecurityAssesmentController();
        
        System.assert(ctr.record == null);
        
        Customer_Assessment__c assessment = new Customer_Assessment__c(PM_Name__c = 'blah');
        insert assessment;
        
        System.currentPageReference().getParameters().put('assessmentId', assessment.Id);
        ctr = new SecurityAssesmentController();
        System.assert(ctr.record != null);
        System.assert(ctr.record.id == assessment.Id);
        System.assert(ctr.pm_name == 'blah');
        
        String data = '{"name":"blah","sub":"someid","email":"someemail","aud":"'+SecurityAssesmentController.GOOGLE_CLIENT_ID+'"}';
        assessment.Auth_Info__c = 'GOOGLE|someid';
        update assessment;
        
        Test.startTest();
        ctr = new SecurityAssesmentController();
        // Test.startTest doesn't seem to work to allow DML before callouts
        // Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator(data));
        MockHttpResponseGenerator mock = new MockHttpResponseGenerator(data);
        ctr.testResponse = mock.respond(null);
        System.assert(ctr.testResponse != null);
        ctr.handleGoogleLogin();
        System.assert(ctr.record != null);
        System.assert(ctr.record.id == assessment.Id);
        System.assert(ctr.pm_name == 'blah');
        Test.stopTest();
        
    }
    
    global class MockHttpResponseGenerator implements HttpCalloutMock {
        global String[] responseStrings;
        global Integer offset = 0;
        global Integer responseStatus = 200;
        global String contentType = 'application/json';
        
        global MockHttpResponseGenerator(String responseString) {
            responseStrings = new String[]{};
            responseStrings.add(responseString);
        }
        
        global MockHttpResponseGenerator(String[] responseString) {
            responseStrings = responseString;
        }
        
        global HTTPResponse respond(HTTPRequest req) {
            // Create a fake response
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', contentType);
            res.setBody(responseStrings.get(offset));
            offset += 1;
            res.setStatusCode(responseStatus);
            return res;
        }
    }
    
}
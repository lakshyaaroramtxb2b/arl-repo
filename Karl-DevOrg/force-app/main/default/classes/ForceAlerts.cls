public class ForceAlerts {

    public void ForceEvent()
    {
        try
        {
            List<Detection_AIQ_Filters__c> aiqFilters = [SELECT Id, KeyText__c, Value__c 
                                                         FROM Detection_AIQ_Filters__c ]; 
            
            List<Suppression_Rules__c> supressionRules = [SELECT Id, Hours__c, CreatedDate, Filter__c, RegexRule__c, Disable__c
                                                           FROM Suppression_Rules__c
                                                           WHERE
                                                            Peer_reviewed__c = true
                                                            AND Peer_reviewed_by__c != NULL
                                                            AND Disable__c = false
                                                            AND CreatedDate = LAST_N_DAYS:7]; //max time is 8 hours, 3 days to reduce set
            
            List<Detection_Security_Event__c> events = [SELECT Id, EventTimestamp__c, DetectionSecurityEventCriteria__r.Last_Validated__c, DetectionSecurityEventCriteria__r.IncidentLibrary__c, AttackIQ_Event__c, FieldMapRaw__c, CreatedDate, UniqueHash_Time_Independent__c 
                                                        FROM Detection_Security_Event__c 
                                                        WHERE Id = 'a2c3A00000QOwucQAD'];
            
            Set<Detection_Security_Event__c> aiqEvents = new Set<Detection_Security_Event__c>();
            Set<Events_Matching_Suppression_Rule__c> emsrs = new Set<Events_Matching_Suppression_Rule__c>();
            
            List<Detection_Security_Event__c> allEvents = new List<Detection_Security_Event__c>();
            
            List<Detection_Security_Event_Criteria__c> dsecs = new List<Detection_Security_Event_Criteria__c>();
            
            List<FeedItem> feedItems = new List<FeedItem>();
            
            for (Detection_Security_Event__c event : events) {

                Map<String, Object> unraw = (Map<String, Object>) JSON.deserializeUntyped(event.fieldmapraw__c);
    
                    //Calculate event hash
                    String eventStringRaw;

                    for (String key : unraw.keySet())
                    {
                        //Ignore dynamic values that will bias simular events i.e. time
                        if(!key.contains('time') 
                           && !key.contains('Time') 
                           && !key.contains('UUID')
                           && !key.contains('RequestId')
                           && !key.contains('EventId')
                           && !key.contains('AccessKey')
                           && !key.contains('RawMsg'))
                            eventStringRaw += unraw.get(key);
                    }
    
                    //add time independtent hash
                    Blob eventStringBlob = Blob.valueOf(eventStringRaw);
                    event.UniqueHash_Time_Independent__c  =  EncodingUtil.convertToHex(Crypto.generateDigest('MD5', eventStringBlob));
                    allEvents.add(event);
                    
                    
                    
                    //AIQ Check
                    for(Detection_AIQ_Filters__c aiqf : aiqFilters)
                    {
                        String eventKeyVal = (String) unraw.get(aiqf.KeyText__c);
                            
                        if(String.isNotBlank(eventKeyVal))
                        {
                            if(eventKeyVal == aiqf.Value__c)
                            {
                                //attackIQ related as matched filter key / value pair
                                Boolean found = false;
                                
                                for(Detection_Security_Event__c evt : aiqEvents)
                                {
                                    if(event.Id == evt.Id)
                                    {
                                        found = true;
                                        break;
                                    }
                                }
                                
                                if(found == false){
                                    aiqEvents.add(event);
                                    System.debug('Here');
                                }
                                
                                event.DetectionSecurityEventCriteria__r.Last_Validated__c = DateTime.now();
                                
                                if(event.DetectionSecurityEventCriteria__r.IncidentLibrary__c == null)
                                   event.DetectionSecurityEventCriteria__r.IncidentLibrary__c = 'a363A000000HxOWQA0';
                                
                                if(!dsecs.contains(event.DetectionSecurityEventCriteria__r))
                                    dsecs.add(event.DetectionSecurityEventCriteria__r);
                            }
                        }
                    }
                    
                    //Supression Check
                    for(Suppression_Rules__c sRule : supressionRules)
                    {
                        //is active
                        //TODO NOT WORKING
                        //
                        DateTime expireDt = sRule.CreatedDate.addHours(Integer.valueOf(sRule.Hours__c));                    
                                         
                        if(event.CreatedDate < expireDt)
                        {
                            Integer filterRules = sRule.Filter__c.split('\n').size();
                            Integer rulesMatching = 0;
    
                            for(String filter : sRule.Filter__c.split('\n'))
                            {
                                String key = filter.split('=')[0].trim();
                                String value = filter.split('=')[1].trim();
                                String eventVal = (String) unraw.get(key);
                                
                                if(sRule.RegexRule__c)
                                {
                                    FeedItem post = new FeedItem();
                                    post.ParentId = event.Id;                   
                                    post.Body = 'Suppression check Regex Rule';
                                    
                                    if(String.isNotBlank(eventVal))
                                    {
                                        //regex match
                                        Pattern p = Pattern.compile(value);
                                        Matcher m = p.matcher(eventVal);
                                        
                                        post.Body += 'Regex Rule value ' + value;
                                        
                                        if(m.matches())
                                        {
                                            rulesMatching++;
                                            
                                            post.Body += 'Matches';
                                        }
                                    }
                                    
                                    feedItems.add(post);
                                }
                                else
                                {
                                    //post.Body += 'String Rule ';
                                    
                                    //basic string match
                                    if(String.isNotBlank(eventVal))
                                    {
                                        if(eventVal.equals(value))
                                        {
                                            rulesMatching++;
                                            
                                            //post.Body += 'Matches';
                                        }
                                    }
                                }
                            }       
                            
                            if(rulesMatching >= filterRules)
                            {
                                Events_Matching_Suppression_Rule__c emsr = new Events_Matching_Suppression_Rule__c();
                                emsr.Detection_Security_Event__c = event.id;
                                emsr.Suppression_Rules__c = sRule.id;
    
                                if(!emsrs.contains(emsr))
                                    emsrs.add(emsr);
    
                                //only care about one supression rule
                                break;
                            }
                        }
                    }
            }
    
            //hash
            UPDATE allEvents;
            
            //update events
            for(Detection_Security_Event__c aiqEvent : aiqEvents)
            {
                aiqEvent.AttackIQ_Event__c = true;
            }
            
            List<Detection_Security_Event__c> aiqEventsUpdate = new List<Detection_Security_Event__c>();
            aiqEventsUpdate.addAll(aiqEvents); 
                
            UPDATE aiqEventsUpdate;
            
            List<Events_Matching_Suppression_Rule__c> evtSupressMatches = new List<Events_Matching_Suppression_Rule__c>();
            evtSupressMatches.addAll(emsrs);
            
            //INSERT evtSupressMatches;
            
            UPDATE dsecs;
            
            //Debugging
            //INSERT feedItems;
            //
            }
        catch (Exception ex)
        {
            
            Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
            message.toAddresses = new String[] { 'amcneilly@salesforce.com' };
            message.subject = 'Trigger Alert';
            message.plainTextBody = ex.getMessage() + ',' + ex.getStackTraceString();
            Messaging.SingleEmailMessage[] messages =   new List<Messaging.SingleEmailMessage> {message};
            Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
        }
    }
    
    public void ForceAlertsNow(List<String> daids)
    {
        try
        {    
            List<CSIRT_HANDLEDETECTIONALERT.IntakeAlert> newAlerts = new List<CSIRT_HANDLEDETECTIONALERT.IntakeAlert>();

            List<Detection_Alert__c> alerts = [SELECT Id, OwnerId, DetectionAlertCriteria__r.OwnerId, CSIRT_Adjudication__c, AlertVersion__c, json_data__c, DetectionAlertCriteria__r.Requires_User_Verification__c, DetectionAlertCriteria__r.Last_Validated__c, DetectionAlertCriteria__r.Automated_Suppression__c, DetectionAlertCriteria__r.Suppression_Threshold__c, HiddenDataMap__c, HiddenEventMap__c, AlertInformation__c, Environment__c, Subject__c, Severity__c, Description__c, RecipientTeam__c, CreatedDate, AttackIQ_Alert__c, Suppressed__c, RecipientTeamFormat__c
                                               FROM Detection_Alert__c 
                                               WHERE Id IN : daids];
            
            for( Detection_Alert__c detectionAlert : alerts ) {
    
                 
                                CSIRT_HANDLEDETECTIONALERT.IntakeAlert newAlert = new  CSIRT_HANDLEDETECTIONALERT.IntakeAlert();
                                
                                newAlert.DetectionAlert = detectionAlert.Id;
                                newAlert.Environment = detectionAlert.Environment__c;
                                newAlert.Subject = detectionAlert.Subject__c;
                                newAlert.Description = detectionAlert.AlertInformation__c;
                                newAlert.EventTimestamp = detectionAlert.CreatedDate;
                                
                                newAlert.RecipientTeam = detectionAlert.RecipientTeam__c;
                    
                                newAlert.Severity = detectionAlert.Severity__c;
                                    
                                newAlerts.add(newAlert);
            }
            
            //Alert CSIRT after evtList is populated - dgatchell change July 9, 2018 to prevent a race where Alerts are created in ResponseOrg and Detection Events are fetched from SecOrg before insert is completed
            CSIRT_HANDLEDETECTIONALERT.ProcessAlert(newAlerts);
        }
        catch (Exception ex)
        {
            
            Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
            message.toAddresses = new String[] { 'amcneilly@salesforce.com' };
            message.subject = 'Trigger Alert';
            message.plainTextBody = ex.getMessage() + ',' + ex.getStackTraceString();
            Messaging.SingleEmailMessage[] messages =   new List<Messaging.SingleEmailMessage> {message};
            Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
        }
    }
}
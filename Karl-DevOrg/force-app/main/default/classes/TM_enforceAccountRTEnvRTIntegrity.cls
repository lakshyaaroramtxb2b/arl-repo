/**
 * Trust Maturity Model v2
 * Oct 2015
 * Written By: Joanna Chen
 * 
 * This class is called by a Before Update trigger on Account.
 * 
 * Enforce the Trust Maturity Model constraint that Environments of record type 'Parent...' must look up to an Account of record type 'Salesforce Business Unit'.
 * Specifically this class prevents dirty data by keeping a user from changing the Account RT to something other than 'Salesforce Business Unit' when it has child Environments of RT = 'Parent...'
 * Note that there is a validation rule set on the Environment object to prevent Environments of RT='Parent...' being parented to Accounts that don't have RT='Salesforce Business Unit'
 **/

public class TM_enforceAccountRTEnvRTIntegrity {
    
    // the Id of the Account RT = Salesforce Business Unit 
    private static final String BU_ACCOUNT_REC_TYPE_ID = TM_Constants.BU_ACCOUNT_REC_TYPE_ID;
    private static final String PARENT_INFRA_DEV_QE_ENVIRONMENT_REC_TYPE_ID = TM_Constants.PARENT_INFRA_DEV_QE_ENVIRONMENT_REC_TYPE_ID;
    private static final String PARENT_INFRA_IT_ENVIRONMENT_REC_TYPE_ID = TM_Constants.PARENT_INFRA_IT_ENVIRONMENT_REC_TYPE_ID;
    private static final String PARENT_INFRA_PROD_ENVIRONMENT_REC_TYPE_ID = TM_Constants.PARENT_INFRA_PROD_ENVIRONMENT_REC_TYPE_ID;
    private static final String PARENT_SERVICE_ENVIRONMENT_REC_TYPE_ID = TM_Constants.PARENT_SERVICE_ENVIRONMENT_REC_TYPE_ID;


	// called in the Before Update trigger on Account    
    public static void enforceAccountRTEnvRTIntegrity (Map<Id,Account> newAccounts, Map<Id,Account> oldAccounts) {
        
        List <String> accountsIdsToCheck = new List<String>();        
                
        // if Account Record Type is being changed and the old Record Type = Business Unit, keep track of the account
        for (Account a: newAccounts.values()) {
            Account oldA = oldAccounts.get(a.Id);
            if ((a.RecordTypeId != oldA.RecordTypeId) && (oldA.RecordTypeId == BU_ACCOUNT_REC_TYPE_ID)) {            
                accountsIdsToCheck.add(a.Id);
            }
        }
            
        // check if any of its child Environments has a record type of 'Parent ...'. If they do then throw an error
        List<Account> accountEnvs = [SELECT Id, 
                                         (SELECT Id, Name, RecordType.Id FROM Environments__r 
                                          WHERE RecordType.Id = :PARENT_INFRA_DEV_QE_ENVIRONMENT_REC_TYPE_ID OR
                                          RecordType.Id = :PARENT_INFRA_IT_ENVIRONMENT_REC_TYPE_ID OR
                                          RecordType.Id = :PARENT_INFRA_PROD_ENVIRONMENT_REC_TYPE_ID OR
                                          RecordType.Id = :PARENT_SERVICE_ENVIRONMENT_REC_TYPE_ID) 
                                         FROM Account 
                                         WHERE Id IN :accountsIdsToCheck];
        
        for (Account acc: accountEnvs) {
            if (acc.Environments__r.size() > 0) {
                newAccounts.get(acc.Id).addError('Cannot update record type. Account has '
                           + acc.Environments__r.size() + 
                           ' Environment(s) of record type \'Parent... \' that must be parented to an Account of record type \'Salesforce Business Unit.\'');
            }
        }

    }
}
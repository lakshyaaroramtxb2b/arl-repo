/*
    Developer Name: Jorge L Caceres - jcaceres@salesforce.com | February 2018
    Description:
        generic batch job for scrambling email addresses
        
        
        I.e., run from dev console or in sandbox init calss
        EmailScrambler.run('Contact', 'Manager_Email__c, Another__c');  scramble 2 email fields
        EmailScrambler.run('Contact', '*');  scramble all email type fields
        
*/

public class EmailScrambler implements Database.Batchable<sObject>, Database.Stateful {

    public String objectName;
    private List<String> emailFields;

    public EmailScrambler (String objectName, String emailFieldNames) {
        // retrieve email scrambler definitions from constructor parameters 
        this.objectName = objectName; 
        // retrieve list of email fields to be scrambled
        if (emailFieldNames == '*') {
            this.emailFields = getAllEmailFieldNames(objectName);
            } else {
            this.emailFields = getEmailFieldNames(emailFieldNames);
        }
    }

    public Database.QueryLocator start(Database.BatchableContext BC) {
    // return query locator for retrieving all the given fields 
        return Database.getQueryLocator(getObjectQuery(objectName, emailFields));
    }

    public void execute(Database.BatchableContext BC, List<sObject> scope) {
        // scramble email fields for object in scope
        for (sObject s : scope) {
            // scramble all email fields
            for (String emailField : emailFields) {
                String email = (String) s.get(emailField);
                if (String.isNotBlank(email)) {
                s.put(emailField, scrambleEmail(email));
            }
            }
        }
        Database.update(scope, false);
    }

    public void finish(Database.BatchableContext BC) {
    } 

    private List<String> getAllEmailFieldNames(String objectName) {
        // given an object name return a list with all field names of type email
        Schema.SObjectType objectToken = Schema.getGlobalDescribe().get(objectName);
        List<String> emailFields = new List<String>();
        Map<String, Schema.SObjectField> fieldsMap = schema.describeSObjects(new List<String> {objectToken.getDescribe().getName()})[0].fields.getMap();
        for (Schema.SObjectField f : fieldsMap.values()) {
            if (f.getDescribe().getType() == Schema.DisplayType.Email) {
                emailFields.add(f.getDescribe().getName());
            }
        } 
        return emailFields;
    }

    private List<String> getEmailFieldNames(String emailFieldNames) {
        // given a string with comma separated field names return a list of field names
        List<String> emailFields = new List<String>();
        if (emailFieldNames != null) {
            emailFields.addAll(emailFieldNames.remove(' ').split(','));
        } 
        return emailFields;
    }

    private String getObjectQuery(String objectName, List<String> emailFields) {
        return String.format('SELECT {0} FROM {1}',
        new List<String> {String.join(emailFields, ',')
        , objectName}); 
    }

    @TestVisible
    private static String scrambleEmail(String email) {
        return email.replace('@', '=') + '@example.com';
    }

    public static void run(String objectName) {
        Database.executeBatch(new EmailScrambler(objectName, '*'));
    }

    public static void run(String objectName, String emailFieldNames) {
        Database.executeBatch(new EmailScrambler(objectName, emailFieldNames));
    }

}
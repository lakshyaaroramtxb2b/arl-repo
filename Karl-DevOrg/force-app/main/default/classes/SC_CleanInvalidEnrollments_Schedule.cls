/**
 * @author: ralph@callaway.cloud
 */
public class SC_CleanInvalidEnrollments_Schedule implements Schedulable {
    
    public void execute(SchedulableContext sc) {
        Database.executeBatch((Database.Batchable<SObject>) 
            Type.forName('SC_CleanInvalidEnrollments_Batch').newInstance());
    }   
}
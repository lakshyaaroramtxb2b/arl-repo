public with sharing class addContactToCase 
{

@InvocableMethod

    public static void selectContact(List<Case> Cases) 
    {
    	List<Id> caseIds = new List<Id>();
    	
    	for(Case c : Cases)
			caseIds.add(c.Id);
    		         
        Map<String, Case> mapCaseEmails = new Map<String, Case>();
        List<case> upcase = new List<case>();    
		List<Case> casedeets = new List<Case>();
        casedeets = [SELECT Id, alert_Name__c, alert_resource__c, alert_destination_user__c, Description, ContactId from Case where Id IN : caseIds];
        
        
        List<string> emails = new List<string>();
            
        for(case c : casedeets)
        {
            
            //String AlertName1 = 'Bro Radius - Login failures by username';
            //String AlertName2 = 'Cisco ASA - VPN posture assessment failure';
            //if(c.alert_name__c.equals(AlertName1))
            //{ 
                system.debug('The case id is:' + c.id);
                system.debug('The case resource(username) is:' + c.alert_destination_user__c);    
        
                emails.add(c.alert_destination_user__c + '@salesforce.com');
        
                mapCaseEmails.put(c.alert_destination_user__c + '@salesforce.com', c);
            }
        
            //else if(c.alert_name__c.equals(AlertName2))
            //{
            // 	emails.add(c.alert_destination_user__c + '@salesforce.com');
            //    mapCaseEmails.put(c.alert_destination_user__c + '@salesforce.com', c);  
            //}
        //}
        //also choose the correct contact recordtypes
        List<RecordType> ContactRecordType = [SELECT Id from RecordType where DeveloperName = 'Salesforce_Employee'];
        id CRTId = ContactRecordType[0].id;
        List<Contact> contacts=[select id, email from Contact where recordtypeId = :ContactRecordType AND
                                email in :emails];
        //assign contact to case
        system.debug('The size of the contact list is (should not be null:' + contacts.size());
        if(contacts.size() > 0)
        {        
            for(contact c : contacts)
            {       
                Case caseRec = mapCaseEmails.get(c.email);
                system.debug('The caseRed id is:' + caseRec.id);
                //Adams TIP what if email is not found 
                system.debug('The contact id is:' + c.id);
                caseRec.contactId = c.Id;
                
                upcase.add(caseRec);                
            }
        }
        update upcase; 
    }
}
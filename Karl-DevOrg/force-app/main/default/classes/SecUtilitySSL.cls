public with sharing class SecUtilitySSL {

    public SecUtilitySSL(SecContactEmailController controller) {

    }
    
    public SecUtilitySSL() {

    }
    public static PageReference ForceSSLAndRedirectIfNot(){
      if (URL.getCurrentRequestUrl().getProtocol() != 'https' && URL.getCurrentRequestUrl().toExternalForm().startsWith('http://security.force.com')){
          //we aren't on a secure.force.com domain
          return new PageReference(URL.getCurrentRequestUrl().toExternalForm().replaceFirst('(?im)https?://(.+?)\\.force\\.com(.+?)$', 'https://$1.secure.force.com$2'));
      } else {
          //no redirect
          return null;
      }
    }
    
    public boolean isSSL {get {return URL.getCurrentRequestUrl().getProtocol()  == 'https';} 
                             private set;}
}
/*
* Created by - Prashant Gupta (MTX Group Inc.)
* Handler class for IEM CASE TRIGGER, 
*/
public class IEM_CaseTriggerHandler {
    /*
     *  Method is used to track all method calls on beore update
     *  This method takes input of List of Case
     */ 
    public static void beforeUpdate(List<Case> caseList, Map<id,Case> oldMap, Map<id,Case> newMap){
        IEM_CaseTriggerHelper.statusAutomation(caseList, oldMap);
        IEM_CaseTriggerHelper.checkRejectionReason(caseList, oldMap, newMap);
        IEM_CaseTriggerHelper.setCaseStatusDates(caseList, oldMap);
        IEM_CaseTriggerHelper.setResidualRisk(caseList, oldMap);
        IEM_CaseTriggerHelper.parseBusinessJustification(caseList, oldMap);
        IEM_CaseTriggerHelper.setCaseApprovers(caseList, oldMap);
        IEM_CaseTriggerHelper.updateApprovers(caseList, oldMap);
        IEM_CaseTriggerHelper.updateCaseEmailFields(caseList,oldMap);
        IEM_CaseTriggerHelper.clearlStatusFields(caseList, oldMap);
        IEM_CaseTriggerHelper.populateReportingBusinessApprover(caseList, oldMap);
        /* IEM_CaseTriggerHelper.filterOwner(caseList, oldMap); */
        
    }
    
    /*
     *  Method is used to track all method calls on after update
     *  This method takes input of List of Case and oldMap
     */
    public static void afterUpdate(List<Case> caseList, Map<Id, Case> oldMap){
        IEM_CaseTriggerHelper.createGUSWorkRecords(caseList, oldMap);
        IEM_CaseTriggerHelper.setCaseRiskAssessor(caseList, oldMap);
        IEM_CaseTriggerHelper.unlockApprovalRecords(caseList, oldMap);
        IEM_CaseTriggerHelper.removeCaseAccess(caseList, oldMap);
        IEM_CaseTriggerHelper.createShareRecordsForInternalUsers(caseList, oldMap);//for internal users
        IEM_CaseTriggerHelper.createShareRecordsForExternalUsers(caseList, oldMap); //for External users.
        IEM_CaseTriggerHelper.updateMilestoneCaseFields(caseList, oldMap);
        IEM_CaseTriggerHelper.updateParentCloud(caseList, oldMap);
        IEM_CaseTriggerHelper.updateMilestoneStatus(caseList, oldMap);
        IEM_CaseTriggerHelper.populateExternalObjectsFields(caseList, oldMap);
        IEM_CaseTriggerHelper.closeAllOpenActivity(caseList, oldMap);
        
    } 
    
    /*
     *  Method is used to track all method calls on after insert
     *  This method takes input of List of Case
     */
    public static void afterInsert(List<Case> caseList){       
        IEM_CaseTriggerHelper.createGUSWorkRecords(caseList, null);
        IEM_CaseTriggerHelper.setCaseRiskAssessor(caseList, null);
        IEM_CaseTriggerHelper.createShareRecordsForInternalUsers(caseList, null); //for internal users
        IEM_CaseTriggerHelper.createShareRecordsForExternalUsers(caseList, null); //for External Users
        IEM_CaseTriggerHelper.updateParentCloud(caseList, null); 
        IEM_CaseTriggerHelper.setCaseOwnerAfterInsert(caseList); 
        IEM_CaseTriggerHelper.populateExternalObjectsFields(caseList, null);
        
    }
    /*
     *  Method is used to track all method calls on beore update
     *  This method takes input of List of Case
     */
    public static void beforeInsert(List<Case> caseList){
        IEM_CaseTriggerHelper.updateRequestor(caseList); 
        IEM_CaseTriggerHelper.setCaseStatusDatesBeforeInsert(caseList);
        IEM_CaseTriggerHelper.updateCaseEmailFields(caseList,null);
    }
}
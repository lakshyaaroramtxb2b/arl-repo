global class DetectionStatsHealthCheck implements Schedulable {
    
    global Map<Id, Integer> dseMap;
    
    global void execute(SchedulableContext ctx) {
        
        //Database.executeBatch(new DCStatsBatchService());

        List<Detection_Security_Event_Criteria__c> dsecs = [SELECT Id FROM Detection_Security_Event_Criteria__c];
        List<id> dsecIds = new List<id>();
        
        List<DC_Stat__c> dcStats = new List<DC_Stat__c>();
        
        for(Detection_Security_Event_Criteria__c dsec : dsecs) {
            dsecIds.add(dsec.id);
        }

        AggregateResult [] ars = [SELECT Count(Id) cnt, DetectionSecurityEventCriteria__c 
         							FROM Detection_Security_Event__c 
         							WHERE DetectionSecurityEventCriteria__c IN: dsecIds
         								AND CreatedDate = YESTERDAY
                                    GROUP BY DetectionSecurityEventCriteria__c];
        
        return;
        
        for(AggregateResult ar : ars) {
            
            Integer v = Integer.valueOf( ar.get('cnt') );
            dseMap.put(((Id)ar.get('DetectionSecurityEventCriteria__c')), v);
        }
        
        for(Detection_Security_Event_Criteria__c dsec : dsecs) {
            
            Integer val = 0;
            
            if(dseMap.containsKey(dsec.Id)) {
            	val = (Integer)dseMap.get(dsec.Id);
            }
            
            DC_Stat__c  s = new DC_Stat__c ();
            
			s.Detection_Security_Event_Criteria__c = dsec.id;
			s.Events_Per_Day__c = val;
			s.date__c = date.today().addDays(-1);
            
            dcStats.add(s);
        }
        
        insert dcStats;
	}
}
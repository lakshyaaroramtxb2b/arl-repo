@IsTest
public class PERMCHECK_SearchNotificationCheckerTest {
    static testMethod void testNotifyAll() {
        PERMCHECK_Search_Notification__c[] nts = PERMCHECK_SearchNotifCheckerTestUtil.createSearchNotifications();
        PERMCHECK_Search_Hit__c[] hits = PERMCHECK_SearchNotifCheckerTestUtil.createHitsForNotification(nts.get(0));
        PERMCHECK_SearchNotificationChecker checker = new PERMCHECK_SearchNotificationChecker();
        System.assert(checker.notifyAll() == null);
    }
    
    static testMethod void testFindNewAndNotify() {
        PERMCHECK_Search_Notification__c[] nts = PERMCHECK_SearchNotifCheckerTestUtil.createSearchNotifications();
        PERMCHECK_Search_Hit__c[] hits = PERMCHECK_SearchNotifCheckerTestUtil.createHitsForNotification(nts.get(0));
        PERMCHECK_SearchNotificationChecker checker = new PERMCHECK_SearchNotificationChecker();
        System.assert(checker.findNewAndNotify(nts.get(0)) != null);
    }
    
    static testMethod void testGetNotificationTargets() {
        PERMCHECK_SearchNotifCheckerTestUtil.createSearchNotifications();
        PERMCHECK_SearchNotificationChecker checker = new PERMCHECK_SearchNotificationChecker();
        System.assert(checker.getNotificationTargets().size() == 2);
    }
    
    static testMethod void testGetNewHitsForNotification() {
        PERMCHECK_Search_Notification__c[] nts = PERMCHECK_SearchNotifCheckerTestUtil.createSearchNotifications();
        PERMCHECK_Search_Hit__c[] hits = PERMCHECK_SearchNotifCheckerTestUtil.createHitsForNotification(nts.get(0));
        PERMCHECK_SearchNotificationChecker checker = new PERMCHECK_SearchNotificationChecker();
        System.assert(checker.getNewHitsForNotification(nts.get(0)).size() == 1);
        
    
    }
}
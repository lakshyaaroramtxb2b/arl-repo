@isTest
public class SPT_UserLookupController_Test {
    
    @isTest
    public static void getResultsTest1(){
        Account acc = new Account();
        acc.Name = 'Test Account';
        insert acc;
        List<Profile> p = [SELECT Id FROM Profile WHERE Name = 'System Administrator']; 
        User u = new User(Email='test-user@email.com', ProfileId = p[0].Id, 
                          UserName='test-user@email.com', alias='tuser1', CommunityNickName='tuser1', 
                          TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', 
                          LanguageLocaleKey='en_US', FirstName = 'Test', LastName = 'User',
                          EmployeeNumber = '123456', isActive = true );
        INSERT u;
       
        Id [] fixedSearchResults= new Id[1];
        fixedSearchResults[0] = u.Id;
        Test.setFixedSearchResults(fixedSearchResults);
        //SOSLController.findMatchingData('Test');
        SPT_UserLookupController.getResults('Test');
    }
    /*@isTest
    public static void getResultsTest() {
        List<Account> accountList = SPT_TestUtility.createAccountRecords('SPT Test Account', 1, true);
        List<Contact> contactList = SPT_TestUtility.createContactRecords('SPT Test Contact', 1, false);
        List<Profile> p = [SELECT Id FROM Profile WHERE Name = 'System Administrator']; 
        User userC = [SELECT ID, Name FROM User WHERE ID =: userInfo.getUserId()];
        
        System.runAs(userC){
             User u = new User(Email='test-user@email.com', ProfileId = p[0].Id, 
                          UserName='test-user@email.com', alias='tuser1', CommunityNickName='tuser1', 
                          TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', 
                          LanguageLocaleKey='en_US', FirstName = 'Test', LastName = 'User',
                          EmployeeNumber = '123456', isActive = true );
        INSERT u;
        Group testGroup = new Group();
        testGroup.Name = 'Enterprise Security';
        testGroup.Type = 'Queue';
        testGroup.DeveloperName = 'Enterprise_Security1';
        insert testGroup;
        
        GroupMember gm = new GroupMember();
        gm.GroupId = testGroup.Id;
        gm.UserOrGroupId = Userinfo.getUserId();
        insert gm;
            
        }
       
        
        String uName = userC.Name;
        List<SPT_UserLookupController.SObJectResult> resultList = new List<SPT_UserLookupController.SObJectResult>();
        //SPT_UserLookupController.SObJectResult('Profile','System Administrator', p[0].Id);
        System.runAs(userC){
            resultList = SPT_UserLookupController.getResults('Enterprise Security');
        }
        
        //System.assert(resultList.size() > 0) ;   
    }*/
    @isTest
    public static void isEnterpriseSecurityGroupMemberTest(){
         Group testGroup1 = new Group();
        testGroup1.Name = 'Trust Application Security Assurance';
        insert testGroup1;
        
         Group testGroup = new Group();
        testGroup.Name = 'Enterprise Security';
        testGroup.DeveloperName = 'Enterprise_Security1';
        insert testGroup;
        
        GroupMember gm = new GroupMember();
        gm.GroupId = testGroup.Id;
        gm.UserOrGroupId = Userinfo.getUserId();
        insert gm;
        
        Id userId = userInfo.getUserId();
        String result = SPT_UserLookupController.isEnterpriseSecurityGroupMember(userId);
        
        System.assertEquals(result!=null, true);
    }
}
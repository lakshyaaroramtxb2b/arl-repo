global class SC_PhishCampaignContactPopBatchSched implements Schedulable {
    /* 
     * SC_PhishCampaignContactPopBatchSched s = new SC_PhishCampaignContactPopBatchSched();
     * String sch = '0 0 1,5,9,13,17,21 * * ?';
     * System.schedule('SC Phish Campaign Contact Populator', sch, s);
     *
     */
    global void execute(SchedulableContext sc) {
        SC_PhishCampaignContactPopulatorBatch b = new SC_PhishCampaignContactPopulatorBatch();
        Database.executeBatch(b, 1000);
    }
}
global with sharing class GuestRollupSummary implements Database.Batchable<sObject>, Database.Stateful{
    public String query;
    public Map<String, Integer> getRollupCount(String entityName, Set<String> orgsSet){
        Map<String, Integer> rollupMap = new Map<String, Integer>();
        for(SObject so: database.query('select count(id) total, organization_id__c from '+entityName+' where organization_id__c in :orgsSet group by organization_id__c')){
            rollupMap.put(((String)so.get('organization_id__c')),(Integer)so.get('total'));
        }
        return rollupMap;
    }
     global Database.QueryLocator start(Database.BatchableContext bc){
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext bc, List<SObject> scope){
        Set<String> orgsset = new Set<String>();
        for(SObject so: scope){
            orgsset.add(((String)so.get('organization_id__c')));
        }
        
        Map<String, Integer> gpgRollupMap = getRollupCount('guests_public_groups__c',orgsset);
        Map<String, Integer> coRollupMap = getRollupCount('guests_cust_objs__c',orgsset);
        Map<String, Integer> soRollupMap = getRollupCount('guests_standard_objs__c',orgsset);
        Map<String, Integer> owdRollupMap = getRollupCount('guests_owd_entityperms__c',orgsset);
        Map<String, Integer> adminRollupMap = getRollupCount('guests_sfdc_admins__c',orgsset);
        Map<String, Integer> siteRollupMap = getRollupCount('guests_sfdc_sites__c',orgsset);
        List<SObject> toupdatelist = new List<SObject>();
        for(SObject so : scope){
            so.put('public_group_count__c', gpgRollupMap.get(((String)so.get('organization_id__c'))));
            so.put('cust_obj_count__c', coRollupMap.get(((String)so.get('organization_id__c'))));
            so.put('owd_entityperms_count__c', owdRollupMap.get(((String)so.get('organization_id__c'))));
            so.put('standard_obj_count__c', soRollupMap.get(((String)so.get('organization_id__c'))));
            so.put('admins_count__c', adminRollupMap.get(((String)so.get('organization_id__c'))));
            so.put('sites_count__c', siteRollupMap.get(((String)so.get('organization_id__c'))));
            toupdatelist.add(so);
        }
        List<Database.SaveResult> result = Database.update(toupdatelist, false);
    }
    global void finish(Database.BatchableContext bc){
    }
}
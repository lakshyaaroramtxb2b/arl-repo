public class EsaCreateGusRecord {
    public Case caseRecord {get; set;}
    public String errorMessage {get; set;}
    
    public EsaCreateGusRecord(ApexPages.StandardController stdController){
        caseRecord = (Case)stdController.getRecord();
        caseRecord = [SELECT Id,CaseNumber,
                             Priority,   
                             Subject,
                             Description,
                             Gus_Work_Number__c,
                             Abuse_type__c,
                             Action_Taken_Decision_Capture__c,
                             Comments,
                             SuppliedEmail,
                             SuppliedName,
                             Origin,
                             Contact.Name
                      FROM Case
                      WHERE Id =:caseRecord.Id];
    }

    public PageReference createGusRecord(){
        errorMessage = null;
        PageReference p = new PageReference('/lightning/r/Case/'+caseRecord.Id+'/view');
        if(caseRecord.Gus_Work_Number__c != null) {
            errorMessage = Label.Esa_Gus_Record_Already_Exists;
            return null;
        }
        ADM_Work_c__x work = new ADM_Work_c__x();
        work.RecordTypeId__c = ESA_AppConstants.GUS_WORK_RECORD_TYPE_ID;
        work.Product_Tag_c__c = Label.Esa_Heroku_Abuse_Product_Tag;
        work.Subject_c__c = caseRecord.Subject;
        String workDescription;
        workDescription = 'Case Priority : '+ caseRecord.Priority;
        workDescription += '\nCase Contact : '+ caseRecord.Contact.Name;
        workDescription += '\nCase Origin : '+ caseRecord.Origin;
        workDescription += '\nAbuse Type : '+ caseRecord.Abuse_type__c;
        workDescription += '\nAction Taken / Decision Capture : '+ caseRecord.Action_Taken_Decision_Capture__c;
        workDescription += '\nWeb Email : '+ caseRecord.SuppliedEmail;
        workDescription += '\nWeb Name  : '+ caseRecord.SuppliedName;
        workDescription += '\n\nDescription : ' + caseRecord.Description;

        work.Details_c__c =  workDescription.abbreviate(31000);
        work.Column_c__c = Label.ESA_Heroku_Abuse_Kanban_Column;
        Database.SaveResult sr = Database.updateImmediate(work);
        if (!sr.isSuccess()) {
            List<Database.Error> err = sr.getErrors();
            errorMessage  = err[0].getMessage();
            return null;
        }        
        work = [SELECT ExternalId,Name__c FROM ADM_Work_c__x WHERE Id=:sr.getID()];
        caseRecord.Gus_Work_Number__c = work.Name__c;
        caseRecord.Gus_Work_Id__c = work.ExternalId;
        update caseRecord;
        return p;
    }

    public PageReference backtoCase(){
        PageReference p = new PageReference('/lightning/r/Case/'+caseRecord.Id+'/view');
        return p;
    }
}
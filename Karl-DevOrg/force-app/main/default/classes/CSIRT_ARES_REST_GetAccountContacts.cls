@RestResource(urlMapping='/ARES/Org62/GetAccountContacts/*')
global with sharing class CSIRT_ARES_REST_GetAccountContacts {
	@HttpPost 
	global static String GetAccountContacts(Id accountId) {
        
        CSIRT_ARES_InternalOrgIntegration Org62 = new CSIRT_ARES_InternalOrgIntegration('ORG62');
        return Org62.GetAccountAdminContacts(accountId);
        
        
   }
}
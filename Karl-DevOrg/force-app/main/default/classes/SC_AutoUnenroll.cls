/**
 * @author: ralph@callaway.cloud
 */
public class SC_AutoUnenroll {
    
    private Training_Course__c course;

    public SC_AutoUnenroll(Id courseId) {
        this(getCourse(courseId));
    }

    public SC_AutoUnenroll(Training_Course__c course) {
        this.course = course;
    }

    /**
     * Run auto unenrollment. Only runs if number of candidates is greater than zero
     * @return Number of unenrolment candidates
     */
    public Integer run() {
        System.assert(course.Tracked__c, 'Course not tracked');
        System.assert(course.Auto_Enrollment_Enabled__c, 'Auto enrollment not enabled');
        System.assertNotEquals(null, course.Auto_Enroll_Where_Clause_10K__c, 'Blank Auto Enroll Where Clause');
        Integer candidates = getCandidateCount(course.Id, course.Auto_Enroll_Where_Clause_10K__c);
        if (candidates > 0) {
            String filter = 'Training_Course__c = ' + DynamicSOQLHelper.format(course.Id) + ' ' +
                'AND RecordTypeId = ' + 
                    DynamicSOQLHelper.format(TrainingCourseTaken_Constants.RT_ID_MANDATORY) + ' ' +
                'AND Contact__c NOT IN (SELECT Id FROM Contact WHERE ' + 
                course.Auto_Enroll_Where_Clause_10K__c + ')';
            SC_Unenroll_Batch.Options opts = new SC_Unenroll_Batch.Options(filter);
            Database.executeBatch(new SC_Unenroll_Batch(opts));
        }
        return candidates;
    }

    private static Training_Course__c getCourse(Id courseId) {
        return [
            SELECT 
                Auto_Enroll_Where_Clause_10K__c,
                Auto_Enrollment_Enabled__c,
                Tracked__c,
                Id
            FROM Training_Course__c
            WHERE Id = :courseId
        ];
    }

    /**
     * Returns number of candidates for course who DONT match autoEnrollFilter that
     * have a mandatory enrollment
     * @param  courseId         Course Id
     * @param  autoEnrollFilter Filter for contacts who should be enrolled in course
     * @return                  Number of contacts who don't match filter
     */
    private Integer getCandidateCount(Id courseId, String autoEnrollFilter) {
        String query = 'SELECT COUNT(Id) cnt FROM Training_Course_Taken__c ' +
            'WHERE Training_Course__c = :courseId ' +
            'AND RecordTypeId = ' + 
                DynamicSOQLHelper.format(TrainingCourseTaken_Constants.RT_ID_MANDATORY) + ' ' +
            'AND Contact__c NOT IN (SELECT Id FROM Contact WHERE ' + autoEnrollFilter + ')';
        AggregateResult agg = Database.query(query);
        return (Integer) agg.get('cnt'); 
    }
}
global class Notifier {
    global enum Severity { DEBUG, INFO, WARNING, ERROR, CRITICAL }
    private Notification_Config__c config;
    private Severity sev;
    private String msg;
    private String [] extr;
    
    /* Returns a named instance of a notifier, pulling the named
       config.  A null name returns the default notifier.  You shouldn't
       need or really even use this
    */
    global static NamedNotifier getNotifier(String notifierName) {
        Notification_Config__c nc;
        if (notifierName == null) {
            nc = [ SELECT Email_Levels__c, 
                                            Debug_Levels__c,
                                            Notification_Email__c,
                                            Source_Org_Wide_Address_ID__c
                                          FROM Notification_Config__c
                                          WHERE Active__c = True 
                                            AND Org_Default_Config__c = True
                                          LIMIT 1];
            notifierName = nc.Name;
        } else {
            nc = [ SELECT Email_Levels__c, 
                                            Debug_Levels__c,
                                            Notification_Email__c,
                                            Source_Org_Wide_Address_ID__c
                                          FROM Notification_Config__c
                                          WHERE Active__c = True 
                                            AND Name =: notifierName
                                          LIMIT 1];
        }
        return new NamedNotifier(notifierName,nc);
    }
    global class NamedNotifier {
        private Notification_Config__c config;
        private String name;
        
        global NamedNotifier(String notifierName, Notification_Config__c nc) {
            name = notifierName;
            config = nc;
        }
        
        global void log(Severity sev, String message, String [] extra) {
            if (config.Email_Levels__c.contains(sev.name())) {
                sendEmail(sev,message,extra);
            }
            if (config.Debug_Levels__c.contains(sev.name())) {
                debug(sev,message,extra);
            }
        }
    
        global void log(Severity s, String message) {
            log(s,message,null);
        }
        
        private void sendEmail(Severity sev, String msg, String [] extr) {
            
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setToAddresses(new String[] {config.Notification_Email__c});
        
            mail.setOrgWideEmailAddressId(config.Source_Org_Wide_Address_ID__c);
            mail.setSubject(name+' - '+sev.name()+' : '+msg);
            mail.setUseSignature(false);
            mail.setPlainTextBody(name+' - '+sev.name()+' : '+msg+'\n\n'+join(extr,'\n'));
            //mail.setHtmlBody(name+' - '+sev.name()+' : '+msg+'<br /><br />'+join(extr,'<br />'));
            Messaging.SendEmailResult [] results = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
            for (Messaging.SendEmailResult res : results) {
                if (!res.isSuccess()) {
                    for (Messaging.SendEmailError err : res.getErrors()) {
                        System.debug('Notifier: EmailError: '+err.getMessage());
                    }
                }
            }
        }
        
        private void debug(Severity sev, String msg, String [] extr) {
            System.debug('['+sev.name()+'] Notification: '+msg+'  Extra: '+join(extr,' : '));
        }
    
        private String join(String [] theList, String joinPattern) {
            String ret = '';
            if ((theList == null) || (theList.size() == 0 )) return ret;
        
            for (String el : theList) {
                ret += el + joinPattern;
            }
            return ret.substring(0,ret.length()-joinPattern.length());
        }
         
    } 
    
    
    
    
    /* DEPRECATED.  Use Notifier.getNotifier(name)
     */
    global Notifier(Severity s, String message) {
        this(s,message, new List<String>() );
    }
    
    
    /* DEPRECATED.  Use Notifier.getNotifier(name)
     */
    global Notifier(Severity s, String message, List<String> extra) {
        sev = s;
        msg = message;
        extr = extra;
        config = [ SELECT Email_Levels__c, 
                          Debug_Levels__c,
                          Notification_Email__c,
                          Source_Org_Wide_Address_ID__c
                       FROM Notification_Config__c
                       WHERE Active__c = True 
                         AND Org_Default_Config__c = True
                       LIMIT 1];
        if (config.Email_Levels__c.contains(sev.name())) {
            sendEmail();
        }
        if (config.Debug_Levels__c.contains(sev.name())) {
            debug();
        }
    }
    
    /* DEPRECATED.  Use Notifier.getNotifier(name)
     */
    private void sendEmail() {
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(new String[] {config.Notification_Email__c});
        
        mail.setOrgWideEmailAddressId(config.Source_Org_Wide_Address_ID__c);
        mail.setSubject('CodeScanner - '+sev.name()+' : '+msg);
        mail.setUseSignature(false);
        mail.setPlainTextBody('CodeScanner - '+sev.name()+' : '+msg+'\n\n'+join(extr,'\n'));
        mail.setHtmlBody('CodeScanner - '+sev.name()+' : '+msg+'<br /><br />'+join(extr,'<br />'));
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
    
    }
    
    /* DEPRECATED.  Use Notifier.getNotifier(name)
     */
    private void debug() {
        System.debug('['+sev.name()+'] Notification: '+msg+'  Extra: '+join(extr,' : '));
    }
    
    /* DEPRECATED.  Use Notifier.getNotifier(name)
     */
    private String join(String [] theList, String joinPattern) {
        String ret = '';
        if ((theList == null) || (theList.size() == 0 )) return ret;
        
        for (String el : theList) {
            ret += el + joinPattern;
        }
        return ret.substring(0,ret.length()-joinPattern.length());
    }
}
global with sharing class BatchNotifier {
    // email notifications for popcrab errors
    
    global with sharing class SingleNotifier {
        public Notifier.Severity sev;
        public String msg;
        public String [] extr;
    
        global SingleNotifier(Notifier.Severity s, String message, List<String> extra) {
            sev = s;
            msg = message;
            extr = extra;
        }
    }
    
    List<SingleNotifier> arr;
    String nName;

    global BatchNotifier(String notifierName) {
        arr = new List<SingleNotifier>();
        nName = notifierName;
    }

    global void addNotifier(Notifier.Severity s, String message) {
        addNotifier(s, message, new List<String>());
    }
    
    global void addNotifier(Notifier.Severity s, String message, List<String> extra) {
        arr.add(new SingleNotifier(s, message, extra));
    }
    
    global void processBatch() {
        Notifier.NamedNotifier nn = Notifier.getNotifier(nName);
        List<String> sn = new List<String>();
        Boolean anythingToDo = False;
        for (SingleNotifier a: arr) {
            if (a.sev != Notifier.Severity.DEBUG) {
                sn.add('=============================================\n' + a.sev.name() + ' - ' + a.msg + '\n');
                sn.addAll(a.extr);
                sn.add('\n\n');
                anythingToDo = True;
            }
        }
        if (anythingToDo == True)
            nn.log(Notifier.Severity.ERROR, 'Batch Email', sn);
    }
    
    global Integer size() {
        return arr.size();
    }
    
    global List<SingleNotifier> getArray() {
        return arr;
    }
}
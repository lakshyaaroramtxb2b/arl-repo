/**
 * Trust Maturity Model v2
 * Utility class for Trust Maturity Model tests
 * 
**/

@isTest
public class TEST_TM_Util {


    // handle TM_AbstractObjective__c
    public static TM_AbstractObjective__c createTMAbstractObjective() {
        TM_AbstractObjective__c ao = generateTMAbstractObjective();
        insert ao;
        return ao;
    }

    public static TM_AbstractObjective__c generateTMAbstractObjective() {
        String fillerText = 'testTMAbstractObjective';
        
        TM_AbstractObjective__c ao = new TM_AbstractObjective__c();
        ao.Name = fillerText;
        ao.Description__c = fillerText;
        ao.Why__c = fillerText;
        ao.Requirements__c = fillerText;
        ao.ImplementationAdvice__c   = fillerText;
        ao.ValidationAdvice__c = fillerText;
        return ao;
    }
        
    // handle TM_Placement__c
    public static TM_Placement__c createTMPlacement(String aoId, String tmCategory, String level, Integer priority, String envType) {
        TM_Placement__c p = generateTMPlacement(aoId, tmCategory, level, priority, envType);
        insert p;
        return p;
    }
    
    public static TM_Placement__c generateTMPlacement(String aoId, String tmCategory, String level, Integer priority, String envType) {
        TM_Placement__c p = new TM_Placement__c();
        p.TM_AbstractObjective__c = aoId;
        p.Category__c = tmCategory;
        p.Level__c = level;
        p.Priority__c = priority;
        p.Environment_Type__c = envType;
        return p;
    }

    // handle TM_Objective__c
    public static TM_Objective__c createTMObjectiveEnv(String placementId, String envId) {
        TM_Objective__c o = generateTMObjectiveEnv(placementId, envId);
        insert o;
        return o;
    }

    public static TM_Objective__c generateTMObjectiveEnv(String placementId, String envId) {
        TM_Objective__c o = new TM_Objective__c();
        o.TM_Placement__c = placementId;
        o.CSIRT_Environment__c = envId;
        return o;
    }

    // handle Account
    public static Account createAccount(String recordTypeId) {
        Account a = generateAccount(recordTypeId); 
        insert a;
        return a;
    }

    public static Account generateAccount(String recordTypeId) {
        String fillerText = 'test Account';
        
        Account a = new Account();
        a.RecordTypeId = Id.valueOf(recordTypeId);
        a.Name = fillerText;
        return a;
    }
    
    // handle IRCloud__Environment__c
    public static IRCloud__Environment__c createEnvironment(String recordTypeId, String accountId) {
        IRCloud__Environment__c e = generateEnvironment(recordTypeId, accountId);
        insert e;
        return e;
    }
    
    public static IRCloud__Environment__c generateEnvironment(String recordTypeId, String accountId) {
        String fillerText = 'testEnvironment';
        
        IRCloud__Environment__c e = new IRCloud__Environment__c();
        e.RecordTypeId = Id.ValueOf(recordTypeId);
        e.Name = fillerText;
        e.IRCloud__Account__c = Id.ValueOf(accountId);
        return e;
    }
    
    // User
    public static User createUser() {
        User u = generateUser(SYSADMIN_PROFILE_ID);
        insert u;
        return u;
    }
    public static User generateUser() {
       return generateUser(SYSADMIN_PROFILE_ID);
    }

    public static User createUser(Id profileId) {
        User u = generateUser(profileId);
        insert u;
        return u;
    }
    public static User generateUser(Id profileId) {
        String testString = generateRandomString(8);
        String testEmail = generateRandomEmail();
        return new User(lastName = testString,
            userName = testEmail,
            profileId = profileId,
            alias = testString,
            email = testEmail,
            emailEncodingKey = 'ISO-8859-1',
            languageLocaleKey = 'en_US',
            localeSidKey = 'en_US',
            timeZoneSidKey = 'America/Los_Angeles'
        );
    }

    public static Id SYSADMIN_PROFILE_ID
    {
        get {
            if(null == SYSADMIN_PROFILE_ID) {
                SYSADMIN_PROFILE_ID = [select id from Profile where name = 'System Administrator'][0].id;
            }
            return SYSADMIN_PROFILE_ID;
        }
        set;
    }

    public static Id TM_TMM_EXTERNAL_PROFILE_ID
    {
        get {
            if(null == TM_TMM_EXTERNAL_PROFILE_ID) {
                TM_TMM_EXTERNAL_PROFILE_ID = [select id from Profile where name = 'Trust Maturity External'][0].id;
            }
            return TM_TMM_EXTERNAL_PROFILE_ID;
        }
        set;
    }

    public static Id TM_VALIDATOR_PERMSET_ID
    {
        get {
            if(null == TM_VALIDATOR_PERMSET_ID) {
                TM_VALIDATOR_PERMSET_ID = [select id from PermissionSet where Label = 'TM Validator'][0].id;
            }
            return TM_VALIDATOR_PERMSET_ID;
        }
        set;
    }

    public static Id TM_IMPLEMENTER_PERMSET_ID
    {
        get {
            if(null == TM_IMPLEMENTER_PERMSET_ID) {
                TM_IMPLEMENTER_PERMSET_ID = [select id from PermissionSet where Label = 'TM Implementer'][0].id;
            }
            return TM_IMPLEMENTER_PERMSET_ID;
        }
        set;
    }

    /* Random Functions */

    private static Set<String> priorRandoms;
    public static String generateRandomString(){return generateRandomString(null);}
    public static String generateRandomString(Integer length){
        if(priorRandoms == null)
            priorRandoms = new Set<String>();

        if(length == null) length = 1+Math.round( Math.random() * 8 );
        String characters = 'abcdefghijklmnopqrstuvwxyz1234567890';
        String returnString = '';
        while(returnString.length() < length){
            Integer charpos = Math.round( Math.random() * (characters.length()-1) );
            returnString += characters.substring( charpos , charpos+1 );
        }
        if(priorRandoms.contains(returnString)) {
            return generateRandomString(length);
        } else {
            priorRandoms.add(returnString);
            return returnString;
        }
    }

    public static String generateRandomEmail(){return generateRandomEmail(null);}
    public static String generateRandomEmail(String domain){
        if(domain == null || domain == '')
            domain = generateRandomString(8) + '.com';
        return generateRandomString(3) + System.now().getTime() + '@' + domain; // use timestamp to ensure usernames are unique
    }

    public static String generateRandomUrl() {
        return 'http://' + generateRandomString() + '.com';
    }
    
}
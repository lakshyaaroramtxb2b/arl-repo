/**
 * @author Swarnima Mandhata
 * @email swarnima.singh@mtxb2b.com
 * @description This is schedulable and batch class for Scope Notifications (on Evidence Requests)
 * It sends email on daily basis for evidence w.r.t to scope configuration.
 */
public with sharing class KARL_ScopeNotificationConfiguration implements Database.Batchable<sObject>,Database.Stateful, schedulable{
    public Database.QueryLocator start(Database.BatchableContext BC){
        
        
        return Database.getQueryLocator([SELECT Id, Name, Scope__c,Group_Email_Alias__c, Mute_Emails__c,OwnerId,Owner.email FROM KARL_Scope_Notifications_Config__c
                                         WHERE Scope__c != null WITH SECURITY_ENFORCED LIMIT 9999]);
        
    }
    public void execute (Database.BatchableContext BC,List<sObject> scopeList){
        Map<String,KARL_Scope_Notifications_Config__c> scopeToGroupEmailMap = new Map<String,KARL_Scope_Notifications_Config__c>();
        Set<String> toAddressSet = new Set<String>();
        List<Messaging.SingleEmailMessage> mailList = new List<Messaging.SingleEmailMessage>();
        if(!scopeList.isEmpty()){
            for(KARL_Scope_Notifications_Config__c notificationCongif: (List<KARL_Scope_Notifications_Config__c>)scopeList){
                if(!scopeToGroupEmailMap.containsKey(notificationCongif.Scope__c)){
                    scopeToGroupEmailMap.put(notificationCongif.Scope__c, notificationCongif);
                }
            }
        }
        if(!scopeToGroupEmailMap.isEmpty()){
            EmailTemplate templateRec = [SELECT id,Subject, HtmlValue FROM EmailTemplate WHERE DeveloperName  = 'KARL_Send_Scope_Notification' LIMIT 1];
            for(KARL_Evidence_Request__c evidenceReq:[SELECT Id, Name, KARL_Cycle_ARL_Item__c,KARL_Cycle_ARL_Item__r.Request_Assignee__c, KARL_Submitted__c, KARL_Direct_Link__c, KARL_Primary_Scope__c,
                                                      KARL_Audit_Cycle__c, KARL_Request_Type__c, KARL_Request_Name__c, Evidence_Status__c, KARL_GUS_Ticket_Details__c, 
                                                      KARL_Request_Assignee_Name__c, KARL_Request_Assignee__c, GUS_Details__c,Owner.Email,Owner.FirstName 
                                                      FROM KARL_Evidence_Request__c WHERE KARL_Primary_Scope__c IN: scopeToGroupEmailMap.keySet()]){
                                                        
                                                          if(scopeToGroupEmailMap.containsKey(evidenceReq.KARL_Primary_Scope__c)){
                                                              if(String.isNotEmpty(scopeToGroupEmailMap.get(evidenceReq.KARL_Primary_Scope__c).Group_Email_Alias__c)){
                                                                  toAddressSet.add(scopeToGroupEmailMap.get(evidenceReq.KARL_Primary_Scope__c).Group_Email_Alias__c);
                                                              }
                                                               if(String.isNotEmpty(evidenceReq.KARL_Cycle_ARL_Item__r.Request_Assignee__c)){
                                                                  toAddressSet.add(evidenceReq.KARL_Cycle_ARL_Item__r.Request_Assignee__c);
                                                              }
                                                          }
                                                          Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
                                                          message.setTemplateId(templateRec.id);
                                                          message.setSubject(templateRec.Subject);
                                                          String htmlBody = templateRec.htmlvalue;
                                                          if(String.isNotEmpty(evidenceReq.KARL_Primary_Scope__c)){
                                                              htmlBody = htmlBody.replace('@Scope', evidenceReq.KARL_Primary_Scope__c);
                                                          }
                                                          htmlBody = htmlBody.replace('@evidenceName',evidenceReq.Name);
                                                          if(String.isNotEmpty(evidenceReq.KARL_Audit_Cycle__c)){
                                                              htmlBody = htmlBody.replace('@auditCycle', evidenceReq.KARL_Audit_Cycle__c);
                                                          }
                                                          if(String.isNotEmpty(evidenceReq.Evidence_Status__c)){
                                                              htmlBody = htmlBody.replace('@evidenceStatus', evidenceReq.Evidence_Status__c);
                                                          }
                                                          if(String.isNotEmpty(evidenceReq.Owner.FirstName))
                                                          htmlBody = htmlBody.replace('@ownerName', evidenceReq.Owner.FirstName);
                                                          String link = '';
                                                            link = System.Label.KARL_org_Link+evidenceReq.Id;
                                                            htmlBody = htmlBody.replace('@link',link);
                                                          
                                                           message.setHtmlBody(htmlBody);
                                                          if(!toAddressSet.isEmpty()){
                                                              message.setToAddresses(new list<String>(toAddressSet));
                                                              mailList.add(message);
                                                          }
                                                      }
        }
        if(!mailList.isEmpty()){
            Messaging.SendEmailResult[] emailResults  = Messaging.sendEmail( mailList, false);
            for(Messaging.SendEmailResult result : emailResults){
                System.debug('result--> '+ result);
                
                if(!result.IsSuccess()){
                    String errorLog ='';
                    for(Database.Error error :  result.getErrors()){
                        System.debug(error.getMessage());
                        errorLog+=error.getMessage();
                    }
                    System.debug(errorLog);
                }
            }
        }
    }
    public void finish(Database.BatchableContext BC){
        
    }
    public void execute(SchedulableContext SC) {
        if(!Test.isRunningTest()){
            database.executebatch(new KARL_ScopeNotificationConfiguration());
        }
    }
}
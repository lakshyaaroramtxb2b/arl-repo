public without sharing class SPT_CaseCommentTriggerHandler {
    // After Insert Method
    public static void afterInsert(List<CaseComment> caseCommentList) {
        // If this method is being called by a batch class or future method, then it doesn't call processCaseComments() method        
        if(!(System.isBatch() || System.isFuture())){
            SPT_CaseCommentHelper.processCaseComments(caseCommentList);
        }
    }
}
/**
* @author Lakshya Arora
* @email lakshya.arora@mtxb2b.com
* @description Test class for KARL_AuditScopeTriggerHelper
*/
@isTest
public with sharing class KARL_AuditScopeTriggerHelperTest {
        /**
    * @author Lakshya Arora
    * @email lakshya.arora@mtxb2b.com
    * @description Test class for KARL_AuditScopeTriggerHelper
    */
    @testSetup
    private static void makeData(){
        User opsAdminUser = ARL_TestDataFactory.createOpsAdmin();
        System.runAs(opsAdminUser){
            KARL_Audit_Scope_Master_Data__c auditScopeObj = ARL_TestDataFactory.createAuditScope();
            auditScopeObj.Karl_Scope__c = KARL_Constants.KARL_SCOPE_ADVERTISING_STUDIO;
            auditScopeObj.Active__c = true;
            insert auditScopeObj;
        }
    }

    /**
    * @author Lakshya Arora
    * @email lakshya.arora@mtxb2b.com
    * @description This is used to test the Auto number functioanlity
    */
    @isTest
    private static void uniqueAuditScopeValidationTest(){
        List<User> opsAdminUserList = ARL_TestDataFactory.fetchUser('abc@b.com.d');
        if(!opsAdminUserList.isEmpty()){
            System.runAs(opsAdminUserList[0]){
                KARL_Audit_Scope_Master_Data__c auditScopeObj = ARL_TestDataFactory.createAuditScope();
                auditScopeObj.Karl_Scope__c = KARL_Constants.KARL_SCOPE_ADVERTISING_STUDIO;
                auditScopeObj.Active__c = true;
                Test.startTest();
                try{
                    insert auditScopeObj;
                }
                catch(Exception exceptionObj){
                    System.assert(exceptionObj.getMessage().contains(KARL_AuditScopeTriggerHelper.activeauditscope),'Unique Validation failed');
                }
                Test.stopTest();
                
            }
        }
    }

}
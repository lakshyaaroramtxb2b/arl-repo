// Lines of code per day/month/total

// Total issues found per day/month/total
// Total_Issues__c

// average scan time


//Scan_Type__c portal or tz

public class TestClass {

    public Decimal getSumTotalIssues() {
        List<Scan_Info__c> scanInfoList =  [select Total_Security_and_Code_Quality_Issues__c from Scan_Info__c];
        Decimal total = 0;
        for (Scan_Info__c si : scanInfoList)
        {
            Decimal totalIssues = si.Total_Security_and_Code_Quality_Issues__c;
            if(totalIssues != null)
            {
                total = total + totalIssues;
            }
        }
        return total;
    }
    
    public Decimal getSumTotalQuality() {
        List<Scan_Info__c> scanInfoList =  [select Total_Quality__c from Scan_Info__c];
        Decimal total = 0;
        for (Scan_Info__c si : scanInfoList)
        {
            Decimal totalIssues = si.Total_Quality__c;
            if(totalIssues != null)
            {
                total = total + totalIssues;
            }
        }
        return total;
    }
    
    public Decimal getSumTotalSecurity() {
        List<Scan_Info__c> scanInfoList =  [select Total_Issues__c from Scan_Info__c];
        Decimal total = 0;
        for (Scan_Info__c si : scanInfoList)
        {
            Decimal totalIssues = si.Total_Issues__c;
            if(totalIssues != null)
            {
                total = total + totalIssues;
            }
        }
        return total;
    }


    
    public String lastMonth;
    public String last24;
    public String totalLoc;
    public String queueSize;
    
    public TestClass () {
    // EMPTY       
    }
    
    
    public String getQueueSize() {
        return queueSize;
    }

    public String getTotalLoc() {
        return totalLoc;
    }

    public String getLastMonth() {
        return lastMonth;
    }

    public String getLastTwentyFour() {
        return last24;
    }
    
    public void last24LoC() {
        // Last 24hrs of scanInfos
        datetime dt = System.now() -1;
        List<Scan_Info__c> scanInfoList = [select Scan_Type__c, Line_Count__c from Scan_Info__c where createdDate > :dt];
    
        last24 = '';
        Decimal total = 0;
        total = sumLinesOfCode(scanInfoList);
        last24 += String.ValueOf(total) + '<br>';
    }
    
    public void lastMonthLoC() {
        // Last 30 days
        datetime dt = System.now() -30;
        List<Scan_Info__c> scanInfoList = [select Scan_Type__c, Line_Count__c from Scan_Info__c where createdDate > :dt];
    
        lastMonth = '';
        Decimal total = 0;
        total = sumLinesOfCode(scanInfoList);
        lastMonth += String.ValueOf(total) + '<br>';
    }
    
     public void pleaseGetTotalLoC() {
        // The getter for the totalLoc variable MUST BE NAMED getTotalLoc so had to use a different name for this function. Stupid. 
         
        List<Scan_Info__c> scanInfoList = [select Scan_Type__c, Line_Count__c from Scan_Info__c ];
    
        totalLoc = '';
        Decimal total = 0;
        total = sumLinesOfCode(scanInfoList);
        totalLoc += String.ValueOf(total) + '<br>';
    }
    
    public void getAverageQueueSize() {

        List<Scan_Info__c> queueSizeList = [select Queue_Size__c from Scan_Info__c ];
        Decimal total = 0;
        for (Scan_Info__c si : queueSizeList)
        {
            Decimal qs = si.Queue_Size__c;
            if (qs != null)
            {
                total = total +  qs;
            }
        }
        
        Integer size = queueSizeList.size();
        Decimal average = total / size;
        queueSize = String.valueOf(average) + '<br>';
        
    }
    
    public Decimal sumLinesOfCode(List<Scan_Info__c> scanInfoList) {

        Decimal total = 0;
        for (Scan_Info__c si : scanInfoList)
        {
            Decimal lineCount =  si.Line_Count__c;
            if (lineCount != null)
            {
                total = total + lineCount;
            }
        }
        return total;
    }
    
   /* public String sumTotalIssues(List<Scan_Info__c> scanInfoList) {
        Decimal total = 0;
        for (Scan_Info__c si : scanInfoList)
        {
            Decimal totalIssues = si.Total_Security_and_Code_Quality_Issues__c;
            if(totalIssues != null)
            {
                total = total + totalIssues;
            }
        }
        return total;
    }*/
}
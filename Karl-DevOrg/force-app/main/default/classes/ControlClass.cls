/**
 * A Fake Apex Class to detect UI Changes that may break CheckMarx.
 * Don't touch this class please.  - Brendan
 */
Public Class ControlClass {


public class SiteLoginController {
    public String username {get; set;}
    public String password {get; set;}

    public PageReference login() {
        String startUrl = System.currentPageReference().getParameters().get('startURL');
        return Site.login(username, password, startUrl);
    }
    
     public SiteLoginController () {}
     
     public string nosuch() {
         // some html here.  </pre>
         // </td></tr></table>
         string s = 'nosuch';
         return s;
     }
}

public static testMethod void testControlClass () {
        // Instantiate a new controller with all parameters in the page
        SiteLoginController controller = new SiteLoginController ();
        controller.username = 'test@salesforce.com';
        controller.password = '123456'; 
                
        System.assertEquals(controller.login(),null);                           
    }         
   
}
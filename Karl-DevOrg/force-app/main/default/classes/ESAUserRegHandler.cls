//This class provides logic for inbound just-in-time provisioning of single sign-on users in your Salesforce organization.
global class ESAUserRegHandler implements Auth.SamlJitHandler {

    private class JitException extends Exception{}

    private void handleUser(boolean create, User ESAuser, Map<String, String> attributes,
        String federationIdentifier, boolean isStandard) {
            
        if(create && attributes.containsKey('User.Username')) {
            ESAuser.Username = attributes.get('User.Username');
        }
        if(attributes.containsKey('User.Phone')) {
            ESAuser.Phone = attributes.get('User.Phone');
        }
        if(create && attributes.containsKey('User.Email')) {
            ESAuser.Email = attributes.get('User.Email');
        }
        if(attributes.containsKey('User.FirstName')) {
            ESAuser.FirstName = attributes.get('User.FirstName');
        }
        if(attributes.containsKey('User.LastName')) {
            ESAuser.LastName = attributes.get('User.LastName');
        }
        if(attributes.containsKey('User.Title')) {
            ESAuser.Title = attributes.get('User.Title');
        }
        if(attributes.containsKey('User.CompanyName')) {
            ESAuser.CompanyName = attributes.get('User.CompanyName');
        }
        if(attributes.containsKey('User.AboutMe')) {
            ESAuser.AboutMe = attributes.get('User.AboutMe');
        }
        if(attributes.containsKey('User.Street')) {
            ESAuser.Street = attributes.get('User.Street');
        }
        if(attributes.containsKey('User.State')) {
            ESAuser.State = attributes.get('User.State');
        }
        if(attributes.containsKey('User.City')) {
            ESAuser.City = attributes.get('User.City');
        }
        if(attributes.containsKey('User.Zip')) {
            ESAuser.PostalCode = attributes.get('User.Zip');
        }
        if(attributes.containsKey('User.Country')) {
            ESAuser.Country = attributes.get('User.Country');
        }
        if(attributes.containsKey('User.CallCenter')) {
            ESAuser.CallCenterId = attributes.get('User.CallCenter');
        }
        if(attributes.containsKey('User.Manager')) {
            ESAuser.ManagerId = attributes.get('User.Manager');
        }
        if(attributes.containsKey('User.MobilePhone')) {
            ESAuser.MobilePhone = attributes.get('User.MobilePhone');
        }
        if(attributes.containsKey('User.DelegatedApproverId')) {
            ESAuser.DelegatedApproverId = attributes.get('User.DelegatedApproverId');
        }
        if(attributes.containsKey('User.Department')) {
            ESAuser.Department = attributes.get('User.Department');
        }
        if(attributes.containsKey('User.Division')) {
            ESAuser.Division = attributes.get('User.Division');
        }
        if(attributes.containsKey('User.EmployeeNumber')) {
            ESAuser.EmployeeNumber = attributes.get('User.EmployeeNumber');
        }
        if(attributes.containsKey('User.Extension')) {
            ESAuser.Extension = attributes.get('User.Extension');
        }
        if(attributes.containsKey('User.Fax')) {
            ESAuser.Fax = attributes.get('User.Fax');
        }
        if(attributes.containsKey('User.CommunityNickname')) {
            ESAuser.CommunityNickname = attributes.get('User.CommunityNickname');
        }
        if(attributes.containsKey('User.ReceivesAdminInfoEmails')) {
            String ReceivesAdminInfoEmailsVal = attributes.get('User.ReceivesAdminInfoEmails');
            ESAuser.ReceivesAdminInfoEmails = '1'.equals(ReceivesAdminInfoEmailsVal) || Boolean.valueOf(ReceivesAdminInfoEmailsVal);
        }
        if(attributes.containsKey('User.ReceivesInfoEmails')) {
            String ReceivesInfoEmailsVal = attributes.get('User.ReceivesInfoEmails');
            ESAuser.ReceivesInfoEmails = '1'.equals(ReceivesInfoEmailsVal) || Boolean.valueOf(ReceivesInfoEmailsVal);
        }
        String uid = UserInfo.getUserId();
        User currentUser = 
            [SELECT LocaleSidKey, LanguageLocaleKey, TimeZoneSidKey, EmailEncodingKey FROM User WHERE Id=:uid];
        if(attributes.containsKey('User.LocaleSidKey')) {
            ESAuser.LocaleSidKey = attributes.get('User.LocaleSidKey');
        } else if(create) {
            ESAuser.LocaleSidKey = currentUser.LocaleSidKey;
        }
        if(attributes.containsKey('User.LanguageLocaleKey')) {
            ESAuser.LanguageLocaleKey = attributes.get('User.LanguageLocaleKey');
        } else if(create) {
            ESAuser.LanguageLocaleKey = currentUser.LanguageLocaleKey;
        }
        if(attributes.containsKey('User.Alias')) {
            ESAuser.Alias = attributes.get('User.Alias');
        } else if(create) {
            String alias = '';
            if(ESAuser.FirstName == null) {
                alias = ESAuser.LastName;
            } else {
                alias = ESAuser.FirstName.charAt(0) + ESAuser.LastName;
            }
            if(alias.length() > 5) {
                alias = alias.substring(0, 5);
            }
            ESAuser.Alias = alias;
        }
        if(attributes.containsKey('User.TimeZoneSidKey')) {
            ESAuser.TimeZoneSidKey = attributes.get('User.TimeZoneSidKey');
        } else if(create) {
            ESAuser.TimeZoneSidKey = currentUser.TimeZoneSidKey;
        }
        if(attributes.containsKey('User.EmailEncodingKey')) {
            ESAuser.EmailEncodingKey = attributes.get('User.EmailEncodingKey');
        } else if(create) {
            ESAuser.EmailEncodingKey = currentUser.EmailEncodingKey;
        }

        /*
         * If you are updating Contact or Account object fields, you cannot update the following User fields at the same time.
         * If your identity provider sends these User fields as attributes along with Contact 
         * or Account fields, you must modify the logic in this class to update either these 
         * User fields or the Contact and Account fields.
         */
        if(attributes.containsKey('User.IsActive')) {
            String IsActiveVal = attributes.get('User.IsActive');
            ESAuser.IsActive = '1'.equals(IsActiveVal) || Boolean.valueOf(IsActiveVal);
        }
        if(attributes.containsKey('User.ForecastEnabled')) {
            String ForecastEnabledVal = attributes.get('User.ForecastEnabled');
            ESAuser.ForecastEnabled = '1'.equals(ForecastEnabledVal) || Boolean.valueOf(ForecastEnabledVal);
        }
system.debug('>>> User.ProfileId: ' + attributes.get('User.ProfileId'));
system.debug('>>> create: ' + create);
        if(create && attributes.containsKey('User.ProfileId')) {
            //String profileId = attributes.get('User.ProfileId');
            //Profile p = [SELECT Id FROM Profile WHERE Name=:profileId];
            //ESAuser.ProfileId = p.Id;
        }
        if(attributes.containsKey('User.UserRoleId')) {
            //String userRole = attributes.get('User.UserRoleId');
            //UserRole r = [SELECT Id FROM UserRole WHERE Id=:userRole];
            //ESAuser.UserRoleId = r.Id;
        }
        if(create) {
            ESAuser.ProfileId = '00e3A000000jbFd'; // hard coded to new community plus license profile
        }

        //Handle custom fields here
system.debug('>>> ESAuser: ' + ESAuser);
        if(!create) {
system.debug('>>> about to update ESAuser: ' + ESAuser);
            update(ESAuser);
        } else {
system.debug('>>> about to insert ESAuser: ' + ESAuser);
            insert(ESAuser);
        }
    }

    private void handleContact(boolean create, String accountId, User ESAuser, Map<String, String> attributes) {
        Contact c;
        boolean newContact = false;
        if(create) {
            if(attributes.containsKey('User.Contact')) {
            String contact = attributes.get('User.Contact');
                c = [SELECT Id, AccountId FROM Contact WHERE Id=:contact];
                ESAuser.ContactId = contact;
            } else {
                c = new Contact();
                newContact = true;
            }
        } else {
            if(attributes.containsKey('User.Contact')) {
                String contact = attributes.get('User.Contact');
                c = [SELECT Id, AccountId FROM Contact WHERE Id=:contact];
                if(ESAuser.ContactId != c.Id) {
                    throw new JitException('Cannot change User.ContactId');
                }
            } else {
                String contact = ESAuser.ContactId;
                c = [SELECT Id, AccountId FROM Contact WHERE Id=:contact];
            }
        }
        if(!newContact && c.AccountId != accountId) {
            // don't think it is necessary to throw this. If a contact exist just update it even if for another account.
            // throw new JitException('Mismatched account: ' + c.AccountId + ', ' + accountId);
        }

        if(create && attributes.containsKey('Contact.Email')) {
            c.Email = attributes.get('Contact.Email');
        }
        if(attributes.containsKey('Contact.FirstName')) {
            c.FirstName = attributes.get('Contact.FirstName');
        }
        if(attributes.containsKey('Contact.LastName')) {
            c.LastName = attributes.get('Contact.LastName');
        }
        if(attributes.containsKey('Contact.Phone')) {
            c.Phone = attributes.get('Contact.Phone');
        }
        if(attributes.containsKey('Contact.MailingStreet')) {
            c.MailingStreet = attributes.get('Contact.MailingStreet');
        }
        if(attributes.containsKey('Contact.MailingCity')) {
            c.MailingCity = attributes.get('Contact.MailingCity');
        }
        if(attributes.containsKey('Contact.MailingState')) {
            c.MailingState = attributes.get('Contact.MailingState');
        }
        if(attributes.containsKey('Contact.MailingCountry')) {
            c.MailingCountry = attributes.get('Contact.MailingCountry');
        }
        if(attributes.containsKey('Contact.MailingPostalCode')) {
            c.MailingPostalCode = attributes.get('Contact.MailingPostalCode');
        }
        if(attributes.containsKey('Contact.OtherStreet')) {
            c.OtherStreet = attributes.get('Contact.OtherStreet');
        }
        if(attributes.containsKey('Contact.OtherCity')) {
            c.OtherCity = attributes.get('Contact.OtherCity');
        }
        if(attributes.containsKey('Contact.OtherState')) {
            c.OtherState = attributes.get('Contact.OtherState');
        }
        if(attributes.containsKey('Contact.OtherCountry')) {
            c.OtherCountry = attributes.get('Contact.OtherCountry');
        }
        if(attributes.containsKey('Contact.OtherPostalCode')) {
            c.OtherPostalCode = attributes.get('Contact.OtherPostalCode');
        }
        if(attributes.containsKey('Contact.AssistantPhone')) {
            c.AssistantPhone = attributes.get('Contact.AssistantPhone');
        }
        if(attributes.containsKey('Contact.Department')) {
            c.Department = attributes.get('Contact.Department');
        }
        if(attributes.containsKey('Contact.Description')) {
            c.Description = attributes.get('Contact.Description');
        }
        if(attributes.containsKey('Contact.Fax')) {
            c.Fax = attributes.get('Contact.Fax');
        }
        if(attributes.containsKey('Contact.HomePhone')) {
            c.HomePhone = attributes.get('Contact.HomePhone');
        }
        if(attributes.containsKey('Contact.MobilePhone')) {
            c.MobilePhone = attributes.get('Contact.MobilePhone');
        }
        if(attributes.containsKey('Contact.OtherPhone')) {
            c.OtherPhone = attributes.get('Contact.OtherPhone');
        }
        if(attributes.containsKey('Contact.Title')) {
            c.Title = attributes.get('Contact.Title');
        }
        if(attributes.containsKey('Contact.Salutation')) {
            c.Salutation = attributes.get('Contact.Salutation');
        }
        if(attributes.containsKey('Contact.LeadSource')) {
            c.LeadSource = attributes.get('Contact.LeadSource');
        }
        if(attributes.containsKey('Contact.CanAllowPortalSelfReg')) {
            String CanAllowPortalSelfRegVal = attributes.get('Contact.CanAllowPortalSelfReg');
            c.CanAllowPortalSelfReg = '1'.equals(CanAllowPortalSelfRegVal) || Boolean.valueOf(CanAllowPortalSelfRegVal);
        }
        if(attributes.containsKey('Contact.DoNotCall')) {
            String DoNotCallVal = attributes.get('Contact.DoNotCall');
            c.DoNotCall = '1'.equals(DoNotCallVal) || Boolean.valueOf(DoNotCallVal);
        }
        if(attributes.containsKey('Contact.HasOptedOutOfEmail')) {
            String HasOptedOutOfEmailVal = attributes.get('Contact.HasOptedOutOfEmail');
            c.HasOptedOutOfEmail = '1'.equals(HasOptedOutOfEmailVal) || Boolean.valueOf(HasOptedOutOfEmailVal);
        }
        if(attributes.containsKey('Contact.HasOptedOutOfFax')) {
            String HasOptedOutOfFaxVal = attributes.get('Contact.HasOptedOutOfFax');
            c.HasOptedOutOfFax = '1'.equals(HasOptedOutOfFaxVal) || Boolean.valueOf(HasOptedOutOfFaxVal);
        }
        if(attributes.containsKey('Contact.Owner')) {
            c.OwnerId = attributes.get('Contact.Owner');
        }
        if(attributes.containsKey('Contact.AssistantName')) {
            c.AssistantName = attributes.get('Contact.AssistantName');
        }
        if(attributes.containsKey('Contact.Birthdate')) {
            c.Birthdate = Date.valueOf(attributes.get('Contact.Birthdate'));
        }
        if(newContact) {
            c.AccountId = accountId;
            insert(c);
            ESAuser.ContactId = c.Id;
        } else {
            update(c);
        }
    }

    private String handleAccount(boolean create, User u, Map<String, String> attributes) {
        Account a;
        
        Try {
            a = [SELECT Id FROM Account WHERE AccountNumber = : attributes.get('Account.AccountNumber')];
        } catch (Exception e) {
            throw new JitException('Community Account Not Found');          
        }

        return a.Id;
    }

    private void handleJit(boolean create, User u, Id samlSsoProviderId, Id communityId, Id portalId,
        String federationIdentifier, Map<String, String> attributes, String assertion) {
        system.debug(communityId);
        system.debug(portalId);
        system.debug(create);
        system.debug(samlSsoProviderId);
        system.debug(federationIdentifier);

        String account = handleAccount(create, u, attributes);
        handleContact(create, account, u, attributes);
        handleUser(create, u, attributes, federationIdentifier, false);

     }

    global User createUser(Id samlSsoProviderId, Id communityId, Id portalId,
        String federationIdentifier, Map<String, String> attributes, String assertion) {
        system.debug('create');            
        /*
        since JIT matches user based on fed Id we need to do our matching based on username
        */
        User ESAuser = getESAuser(attributes.get('User.Username'));
        system.debug('ESAuser: ' + ESAuser);            
        Boolean create = (ESAuser.Id == null);

        handleJit(create, ESAuser, samlSsoProviderId, communityId, portalId,
            federationIdentifier, attributes, assertion);
        return ESAuser;
    }

    global void updateUser(Id userId, Id samlSsoProviderId, Id communityId, Id portalId,
        String federationIdentifier, Map<String, String> attributes, String assertion) {
        system.debug('update: ' + userId);            
            
        /*
        since JIT matches user based on fed Id we need to do our matching based on username
        */
        User ESAuser = getESAuser(attributes.get('User.Username'));
        Boolean create = (ESAuser.Id == null);
        system.debug('ESAuser: ' + ESAuser);            
            
        handleJit(create, ESAuser, samlSsoProviderId, communityId, portalId,
            federationIdentifier, attributes, assertion);
    }
    
    private User getESAuser (String userName) {
        User ESAuser;
        system.debug('userName: ' + userName);            
        Try {
            ESAuser = [SELECT Id, FirstName, ContactId FROM user 
                       WHERE FederationIdentifier = :userName];
        } catch (Exception e) {
            ESAuser = new User(FederationIdentifier = userName);
        }
        system.debug('ESAuser: ' + ESAuser);            
        return ESAuser;
    }
}
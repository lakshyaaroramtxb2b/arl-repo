/**
* This class will delete the Headcount records which are deleted in GUS
*/
public class DeleteGusPositionRecordsbatch implements Database.Batchable<sObject>, Database.Stateful {

    public String SOURCE_FILE = 'DeleteGusPositionRecords';
    
    
    public Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([
                                         SELECT Id,
                                                GUS_Position__r.Name__c
                                         FROM Headcount__c
                                         WHERE GUS_Position__c != null]);
    }

    public void execute(Database.BatchableContext bc, List<Headcount__c> lstHeadCounts) {
        List<Headcount__c> lstSecOrgHeadCounts = new List<Headcount__c>();
        
        for(Headcount__c headCount : lstHeadCounts) {
            if(headcount.GUS_Position__r.Name__c == null){
               headcount.GUS_Position__c = null;
               lstSecOrgHeadCounts.add(headcount);
            }
        } 
        
        if(lstSecOrgHeadCounts.size() > 0) {
            List<Database.SaveResult> saveResults = Database.Update(lstSecOrgHeadCounts, false);
            
            String finalErrorText = '';
            for (Database.SaveResult sr : saveResults) {
                if (!sr.isSuccess()) 
                    for(Database.Error err : sr.getErrors()) {
                                finalErrorText += err.getStatusCode() + ': ' + err.getMessage() + '\n';
                    }
            }
            
            if(string.isNotBlank(finalErrorText)) {
                system.debug('##########'+finalErrorText);
                Esa_DebugService.writeMessage('UpdateGusPositionRecords.finish: Failed with errors');
                Exception ex;
                Esa_DebugService.writeException(ex, 1, SOURCE_FILE, finalErrorText);
            }
        }
    }
               
    public void finish(Database.BatchableContext BC) {
         Esa_Script_Gus_PositionsObject.deleteGusDeletedPositons();
    }

}
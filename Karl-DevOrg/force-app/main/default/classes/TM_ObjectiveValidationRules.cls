/*
 
    Developer Name: Jorge L Caceres - jcaceres@salesforce.com | December 2015    

    Description: Validation rules that are impossible to implement via setup
    
*/

public with sharing class TM_ObjectiveValidationRules {

    public static void validateStatusChange (List<TM_Objective__c> objectives, Map<Id, TM_Objective__c> oldObjectives) {

        for (TM_Objective__c objective : objectives) {
            
            if (objective.Status__c != oldObjectives.get(objective.Id).Status__c && !TM_Constants.Current_User_Is_Admin) {
                
                if (UserInfo.getUserId() == objective.ImplementationOwner__c &&
                   TM_Constants.VALID_IMPLEMENTER_STATUS.contains(objective.Status__c)) {
                       // no problem
                } else if (UserInfo.getUserId() == objective.ValidationOwner__c &&
                   TM_Constants.VALID_VALIDATOR_STATUS.contains(objective.Status__c)) {
                      // no problem
                } else {
                      // not allowed
                      objective.Status__c.addError('Status value not permitted for your current role.'); 
                }
            } 

        }       
    }

}
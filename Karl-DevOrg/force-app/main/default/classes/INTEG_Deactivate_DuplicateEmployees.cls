/**
 * Batch apex class to de-acticate Duplicate Contacts.
 * Run this in Dev Console to setup
 * INTEG_Deactivate_DuplicateEmployees deactivateEmp = new INTEG_Deactivate_DuplicateEmployees();
 * deactivateEmp.execute(null);
 */
global class INTEG_Deactivate_DuplicateEmployees implements Schedulable {
    
    global void execute(SchedulableContext ctx) {
        try {
            deactivateDuplicateEmployees();
        } catch (Exception e) {
            Esa_DebugService.WriteException(e, 'INTEG_Deactivate_DuplicateEmployees', 'Exception in execute method');
        } finally {
            // Runs every 60 minutes
            abortCurrentAndScheduleNextExecution(ctx, 60);
        }
    }
    
    private void abortCurrentAndScheduleNextExecution(SchedulableContext ctx, Integer intervalMins) {
        if (ctx != null) {
           System.abortJob(ctx.getTriggerId());
        }
        
        Datetime nextScheduleTime = System.now().addMinutes(intervalMins);
        String minute = String.valueof(nextScheduleTime.minute());
        String second = String.valueof(nextScheduleTime.second ());
        String cronvalue = second + ' ' + minute + ' * * * ?';
        String jobName = Integ_Deactivate_DuplicateEmployees.class.getName() + nextScheduleTime.format('yyyy-MM-dd-HH:mm:ss');  
   
        Integ_Deactivate_DuplicateEmployees p = new Integ_Deactivate_DuplicateEmployees();   
        System.schedule(jobName, cronvalue , p);
    }

    private void deactivateDuplicateEmployees() {

        //Get Duplicate Employee Records
        AggregateResult[] groupedResults = [SELECT  MIN(Id) minId,
                                            INTEG_Employee_ID__c
                                    FROM INTEG_Employee__c 
                                    WHERE Duplicate__c=false
                                    GROUP BY INTEG_Employee_ID__c HAVING count(CreatedDate) > 1];

        Set<String> setEmployeeIds = new Set<String>();
        system.debug('Total Duplicate Contacts ******'+groupedResults.size());
        String employeeId;
        for (AggregateResult ar : groupedResults)  {
            employeeId = String.ValueOf(ar.get('INTEG_Employee_ID__c'));

            if(String.isBlank(employeeId)) continue;

            if (String.isNotBlank(employeeId) && employeeId.startsWithIgnoreCase('F')) {
                employeeId = employeeId.substring(1);
            }
            if (String.isNotBlank(employeeId) && !ContactX_Constants.EMPLOYEE_ID_NON_UNIQUE_VALS.contains(employeeId)) {
                setEmployeeIds.add(String.ValueOf(ar.get('minId')));
            }
        }

        system.debug('Total Valid - Duplicate Contacts #########'+setEmployeeIds.size());

        List<INTEG_Employee__c> lstDuplicateEmployees = [SELECT Id,
                                                                Duplicate__c,
                                                                INTEG_Active__c 
                                                         FROM INTEG_Employee__c 
                                                         WHERE ID IN :setEmployeeIds];

        for(INTEG_Employee__c employee : lstDuplicateEmployees){
            employee.INTEG_Active__c = false;
            employee.Duplicate__c = true;
        }

        Database.SaveResult[] saveResult = Database.Update(lstDuplicateEmployees, false);

        if(checkAndWriteErrorsForDebug(saveResult)){
            for(Database.SaveResult saveRes : saveResult){
                checkAndWriteErrorsForDebug(saveRes);
            }
        }

        //De-activate Contact Records

        List<Contact> lstDuplicateContacts = [SELECT Id,
                                                     Is_Active__c,
                                                     Duplicate__c
                                              FROM Contact 
                                              WHERE Is_Active__c = true
                                              AND Employee__c IN : setEmployeeIds
                                              AND RecordTypeId = :Contact_Constants.RT_ID_SALESFORCE_EMPLOYEE];

        for(Contact cont : lstDuplicateContacts){
            cont.Is_Active__c = false;
            cont.Duplicate__c = true;
        }

        saveResult = Database.Update(lstDuplicateContacts, false);
        
        if(checkAndWriteErrorsForDebug(saveResult)){
            for(Database.SaveResult saveRes : saveResult){
                checkAndWriteErrorsForDebug(saveRes);
            }
        }    
    }

    private static Boolean checkAndWriteErrorsForDebug(List<Database.SaveResult> saveResults) {
        Boolean foundErrors = false;
        for (Database.SaveResult saveRes : saveResults) {
            if (checkAndWriteErrorsForDebug(saveRes)) {
                foundErrors = true;
            }
        }
        return foundErrors;
    }

    private static Boolean checkAndWriteErrorsForDebug(Database.SaveResult saveRes) {
        Boolean foundErrors = !saveRes.isSuccess();
        if (foundErrors) {
            for (Database.Error error : saveRes.getErrors()) {
                Esa_DebugService.writeErrorMessage(error.getMessage(), Esa_DebugService.LEVEL_ERROR);
            } 
        }
        return foundErrors;
    }
}
@isTest
public class SPT_ChangeCaseLabelController_Test {
        
    @isTest
    public static void getCaseOriginTest(){
        Case caseRec = new Case();
        caseRec.subject = 'test sub';
        caseRec.Enterprise_Security_Case_Number__c = '12345';
        insert caseRec;

        String result = SPT_ChangeCaseLabelController.getCaseOrigin(caseRec.id);
        System.assertEquals(result != null, true);
    }

}
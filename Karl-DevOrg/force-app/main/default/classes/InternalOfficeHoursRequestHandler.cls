/*
 * Email services are automated processes that use Apex classes
 * to process the contents, headers, and attachments of inbound
 * email.
 */
global class InternalOfficeHoursRequestHandler implements Messaging.InboundEmailHandler{
    private Internal_Appointment__c p;
    private static final String ORG_WIDE_EMAIL_ADDRESS_ID = '0D2300022008OqS';



    public integer emailAcceptStatus(Messaging.InboundEmail email){
    
       //todo: change to startwith instead of contains
       String subject = email.subject.toLowerCase();
       if (subject.contains('declined')) return 0;
       else return 1;

    }
    public void debugMyInboundMessage(Messaging.InboundEmail email){
         string debugstr = 'Email from ' + email.fromAddress + '\r\n';
         debugstr += 'Our text attachment size is ';
            if (email.textAttachments == null){
                 debugstr += '0';
            } else { 
                debugstr += ''+email.textAttachments.size();
                debugstr += '\r\n';
                for (Messaging.InboundEmail.TextAttachment b : email.textAttachments){
                    debugstr += 'fileName: ' + b.fileName + '\r\n';
                    debugstr += 'mimeTypeSubType: ' + b.mimeTypeSubType + '\r\n';
                }
            }
            
        debugstr += '\r\nInvite Attendance: ' + emailAcceptStatus(email) + '\r\n';
            
            debugstr += '\r\nMatching for office hours id: \r\n';
            
           
            
            Messaging.SingleEmailMessage reply = new Messaging.SingleEmailMessage();
            reply.setOrgWideEmailAddressId(ORG_WIDE_EMAIL_ADDRESS_ID);
            reply.setToAddresses(new String[] {'xiaoran.wang@salesforce.com'});
            reply.setSubject('Debug ');
            reply.setPlainTextBody(debugstr);
            Messaging.sendEmail(new Messaging.Email[] {reply});


    }

    public string parseOfficeHourRefId(string sub){
        string idgrep = '\\[(IOH-\\d+?)\\]';
        Matcher m = Pattern.compile(idgrep).matcher(sub);
        while (m.find()){
            return m.group(1);
            break;
        }
        return null;   
    }

    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
        // use the 'ref' number in the subject line to find the right appointment
        
   
        String subject = email.subject;
        string refIdMatch = parseOfficeHourRefId(email.subject);
        if (refIdMatch == null){
            //try oldschool way instead
            String[] tokens = subject.split('Ref:');

            p = [SELECT  Id, Name, Email__c, Name__c, Cloud__c, Team__c, Description__c, ContactInfo__c, Date__c, RefId__c
                 FROM Internal_Appointment__c  WHERE Id =: tokens[tokens.size() - 1].trim() ];
            // check to see if someone already has the appointment
        } else {
            p = [SELECT  Id, Name, Email__c, Name__c, Cloud__c, Team__c, Description__c, ContactInfo__c, Date__c, RefId__c
                 FROM Internal_Appointment__c WHERE RefId__c =:refIdMatch LIMIT 1];
        }
        
        integer acceptStatus = emailAcceptStatus(email);

        if (acceptStatus == 0) { //user or us declined
           InternalOfficeHoursController.sendCancellation(p);
        }
               
        return result;
    }
}
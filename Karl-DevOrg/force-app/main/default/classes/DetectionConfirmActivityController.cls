public class DetectionConfirmActivityController {
    
    public String caseId {get;set;}
	public String response {get;set;}
    public String inputResponse{get;set;}
    public boolean donePanel{get;set;}

    
    public DetectionConfirmActivityController()
    {
        caseId = apexpages.currentpage().getparameters().get('caseid');
        
        donePanel = false;
    }
    
    public void saveResponse(){
        
        if(caseId instanceof Id)
        {
            AlertsPendingJudication__c apjCase = [SELECT Id, DA__c, DAC__c, Last_Contact_Time__c, Status__c, Email_Frequency__c, Email__c, Attempts__c   
                                                  FROM AlertsPendingJudication__c
                                                  WHERE Status__c = 'Pending'
                                                    AND Id =: caseId
                                                  LIMIT 1];

            apjCase.Response__c = inputResponse;

            if(response == 'yes')
            {
            	apjCase.Status__c = 'User Confirmed';
                UPDATE apjCase;
            }
            else if(response == 'no')
            {
            	apjCase.Status__c = 'User Uncertain';
                UPDATE apjCase;
            }   
        }
        
        new DetectionContactUsersWorker().execute(null);
        
        donePanel = true;

        //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Thanks for your response. Your case will be reviewed by the security team shortly.'));
    }
}
@isTest
public class DLM_TestDataFactory {
     Public static ID DLM_CONTACTRECORDTYPEID = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Salesforce Employee').getRecordTypeId();
    public static List<Account> createAccounts(Integer num, Boolean doInsert) {
        List<Account> accounts = new List<Account>();
        for(integer i=0; i<num; i++ ){
            accounts.add(new Account(Name = 'salesforce.com', Website = 'http://www.test.com/'));
        }
        if(doInsert){
            insert accounts; //insert the account list
        }
        return accounts;
    }
    public static List<Contact> createContactsManager(Integer num, Id org62Id, Boolean doInsert) {
        List<Account> accountList = createAccounts(1, true);
        List<Contact> contacts = new List<Contact>();
        for(integer i=0; i<num; i++ ){
            contacts.add(new Contact(AccountId = accountList[0].id,FirstName = 'Test Contact' + i, LastName = 'Record', email='test'+i+'@abcTesting.com', Org62_User_ID__c= org62Id)); //new Contact detail
        }
        if(doInsert){
            insert contacts; //insert the account list
        }
        return contacts;
    }
    public static List<Contact> createContacts(Integer num,Id accountId, Boolean doInsert) {
        List<Contact> contacts = new List<Contact>();
        for(integer i=0; i<num; i++ ){
            contacts.add(new Contact(AccountId = accountId, FirstName = 'Test Contact' + i, LastName = 'Record', email='test'+i+'@abcTesting.com')); //new Contact detail
        }
        if(doInsert){
            insert contacts; //insert the account list
        }
        return contacts;
    }
	public static List<IS_Document__c> createIsDocument(Integer num, Boolean doInsert) {
        List<IS_Document__c > docList = new List<IS_Document__c >();
        for(integer i=0; i<num; i++ ){
            docList.add(new IS_Document__c (Name ='Title'+i, Stage__c='Discovery', 
                                  Document_Hyperlink__c = 'www.link.com', Latest_Publication_Status__c = 'Not Published',
                                  Document_Summary__c ='Summary' ));
        }
        if(doInsert){
            insert docList;
        }
        return docList;
    }
    public static List<IS_Document__c> createIsDocumentWithContacts(Integer num, Boolean doInsert) {
        List<Account> accountList = createAccounts(1, true);
        List<Contact> contactList = createContacts(2, accountList[0].Id,false);
        List<Id> contactIdList = new List<Id>();
        List<Contact> contactUpdateList = new List<Contact>();
        
        for(Contact con : contactList){
            contactIdList.add(con.Id);
            
        }
        for(contact con1: [select id,RecordTypeId,Org62_User_ID__c
                           FROM COntact WHERE Id IN: contactIdList]){
                               con1.RecordTypeId = DLM_CONTACTRECORDTYPEID;
                               if(con1.Org62_User_ID__c == null){
                                   con1.Org62_User_ID__c = 'QWERTYUIOPASDFGYUJ';
                                   contactUpdateList.add(con1);
                               }
                           }
        Update contactUpdateList;
        
        List<IS_Document__c > docList = new List<IS_Document__c >();
        for(integer i=0; i<num; i++ ){
            docList.add(new IS_Document__c (Name ='Title'+i, Stage__c='Discovery', 
                                  Document_Hyperlink__c = 'www.link.com', Latest_Publication_Status__c = 'Not Published',
                                  Document_Summary__c ='Summary',
                                            Content_Owner__c = contactList[0].Id,
                                           Content_Custodian__c = contactList[1].Id));
        }
        if(doInsert){
            insert docList;
        }
        return docList;
    }
    public static List<IS_Standard__c> createIsStandard(Integer num, Boolean doInsert) {
        List<Account> accountList = createAccounts(1, true);
        List<Contact> contactList = createContacts(1, accountList[0].Id,true);
        
        
        List<IS_Standard__c> standarList = new List<IS_Standard__c>();
        for(integer i=0; i<num; i++ ){
            standarList.add(new IS_Standard__c(Name ='Standard '+i, 
                                             Standard_Hyperlink__c  = 'www.link.com', SFSS_Content_Owner_1__c = contactList[0].Id
                                            ));
        }
        if(doInsert){
            insert standarList;
        }
        return standarList;
    }
    public static List<IS_Document_Standard__c> createIsDocumentStandard(Integer num, Boolean doInsert) {
        List<IS_Standard__c> isStandardList = createIsStandard(num, true);
        List<IS_Document__c> isDocumentList = createIsDocument(num, true);
        
        
        List<IS_Document_Standard__c> isDocumentStandarList = new List<IS_Document_Standard__c>();
        for(integer i=0; i<num; i++ ){
            isDocumentStandarList.add(new IS_Document_Standard__c(IS_Standard__c  =isStandardList[i].Id,
                                                        IS_Document__c = isDocumentList[i].Id
                                            ));
        }
        if(doInsert){
            insert isDocumentStandarList;
        }
        return isDocumentStandarList;
    }
    
    public static List<IS_Stakeholder__c> createIsStakeholder(Integer num,List<Contact> contactList, id isStandard, Boolean doInsert){
        List<IS_Stakeholder__c> stakeholderList = new List<IS_Stakeholder__c>();
        
         for(integer i=0; i<num; i++ ){
            stakeholderList.add(new IS_Stakeholder__c( Contact__c = contactList[0].Id, Contact_Business_Unit_Change__c = false, Role__c = 'Stakeholder',
                                                    IS_Standard__c = isStandard ));
        }
         if(doInsert){
            insert stakeholderList;
        }
        return stakeholderList;
    }
}
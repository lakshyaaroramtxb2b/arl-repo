@isTest
public class PRT_SLAMetricCreationControllerTest {
    @TestSetup
    static void makeData(){
        Program__c programRec = new Program__c();
        programRec.Name = 'Test Program';
        INSERT programRec;

        Operations__c operationRec = new Operations__c();
        operationRec.Name = 'Test Operation';
        operationRec.Related_Program__c = programRec.Id;
        INSERT operationRec;

        SLA__c slaRec = new SLA__c();
        slaRec.Operation__c = operationRec.Id;
        slaRec.Name = 'Test SLA';
        slaRec.Reporting_Period__c = 'Weekly';
        INSERT slaRec;

        SLA_Metric__c slaMetricRec = new SLA_Metric__c();
        slaMetricRec.Operation__c = operationRec.Id;
        slaMetricRec.SLA__c = slaRec.Id;
        slaMetricRec.Due_Date__c = Date.today().addDays(8);
        INSERT slaMetricRec;
    }

    @isTest 
    public static void findRecordsTest() {
        List<SObject> operationList = PRT_SLAMetricCreationController.findRecords('Operations__c', 'Name', 'Test');
        System.assertEquals(1, operationList.size());
    }

    @isTest
    public static void createSLAMetricRecordTest() {
        List<Operations__c> operationList = new List<Operations__c>([SELECT Id FROM Operations__c]);
        List<SLA_Metric__c> metricList = new List<SLA_Metric__c>([SELECT Id FROM SLA_Metric__c]);
        List<Map<String, String>> metricDataList = new List<Map<String, String>>();
        Map<String, String> metricDataMap = new Map<String, String>();
        metricDataMap.put('Name', 'Test SLA Name');
        metricDataMap.put('Value', '74');
        metricDataMap.put('Comments', 'Test Comment');
        metricDataMap.put('Id', ''+metricList.get(0).Id);
        metricDataList.add(metricDataMap);
        Boolean isInserted = PRT_SLAMetricCreationController.createSLAMetricRecord(operationList.get(0).Id, metricDataList);
        List<SLA_Metric__c> slaMetricList = new List<SLA_Metric__c>([SELECT Id FROM SLA_Metric__c]);
        System.assertEquals(2, slaMetricList.size());
        
        List<SLA_Metric__c> slaMetricList2 = PRT_SLAMetricCreationController.getSLAMetricRecords(operationList.get(0).Id);
        System.assertEquals(1, slaMetricList2.size());
    }
    
    @isTest
    public static void getEmptySlaMetricRecordsTest() {
        List<Operations__c> operationList = new List<Operations__c>([SELECT Id FROM Operations__c]);
        List<SLA_Metric__c> slaMetricList = PRT_SLAMetricCreationController.getEmptySLAMetricRecords(operationList.get(0).Id);
        System.assertEquals(2, slaMetricList.size());
    }
}
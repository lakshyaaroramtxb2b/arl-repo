/**
* @author Ben Harvie
* @email ben.harvie@salesforce.com
* @description This is trigger helper class for KARL_Cycle_Follow_up__c.
*/
public with sharing class KARL_FollowUpTriggerHelper {
    public static set<String> running;
    
    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description This method gets triggered from Follow-Ups Object
    */
    public static void run(){
        // After Insert
        if( Trigger.isAfter && Trigger.isInsert ){
            updateFollowupSharing();
        }
    }
    
    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description This method checks for the execution recursion (sharing model)
     * @param context - current context of job execution
     * @return running context for job execution
    */
    public static boolean isRunning(string context){
        if( running == null ){
            return false;
        }
        return running.contains(context);
    }
    
    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description This method adds in the running method list.
     * @param context - current context of job execution
    */
    public static void addRun(string context){
        if( running == null ){
            running = new set<String>();
        }
        running.add(context);
    }
    
    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description This method updates the sharing rules for the new Follow-Up
    */
    private static void updateFollowupSharing(){
        //Logic to Stop Recursion
        if( isRunning('updateFollowupSharing') ){
            return;
        }
        addRun('updateFollowupSharing');

        Set<Id> auditFirmIdSet = new Set<Id>();
        Map<Id,Set<Id>> auditFirmIdToContactIdMap = new Map<Id,Set<Id>>();
        List<KARL_Cycle_Follow_up__c> followUpItemList = new List<KARL_Cycle_Follow_up__c>();
        
        // Build the list of followups and audit firms that we need to share to (should be a short list since trigger based)
        for( KARL_Cycle_Follow_up__c  followUpItem : [SELECT Id, Name,KARL_Followup_Audit_Team__c,
                                                        KARL_Followup_Audit_Team__r.KARL_Audit_Firm__c
                                                        FROM KARL_Cycle_Follow_up__c 
                                                        WHERE Id in: (List<KARL_Cycle_Follow_up__c >)Trigger.New ]){
                
                
                followUpItemList.add(followUpItem);
                auditFirmIdSet.add(followUpItem.KARL_Followup_Audit_Team__r.KARL_Audit_Firm__c);
        }
        
        // Get the list of Audit Team Contacts that exist for the Audit Firms in the set
        for(KARL_Audit_Team_Contacts__c auditTeamCon : [SELECT Id,KARL_Audit_Team__c,KARL_Contact__c,
                                                        KARL_Audit_Team__r.KARL_Audit_Firm__c
                                                        FROM KARL_Audit_Team_Contacts__c 
                                                        WHERE KARL_Audit_Team__r.KARL_Audit_Firm__c IN : auditFirmIdSet]){ 
            if(!auditFirmIdToContactIdMap.containsKey(auditTeamCon.KARL_Audit_Team__r.KARL_Audit_Firm__c)){
                auditFirmIdToContactIdMap.put(auditTeamCon.KARL_Audit_Team__r.KARL_Audit_Firm__c,new Set<Id>());
            }
            auditFirmIdToContactIdMap.get(auditTeamCon.KARL_Audit_Team__r.KARL_Audit_Firm__c).add(auditTeamCon.KARL_Contact__c);  
        }

        KARL_FollowUpSharingUtility.shareFollowUpItem(followUpItemList,auditFirmIdToContactIdMap);

        //KARL_ShareFollowUpToAuditFirmBatch objBatchAdd = new KARL_ShareFollowUpToAuditFirmBatch('Trigger',auditFirmIdToContactIdMap.keySet(),auditFirmIdToContactIdMap,'Add');
        //Database.executebatch(objBatchAdd,100);
    }
}
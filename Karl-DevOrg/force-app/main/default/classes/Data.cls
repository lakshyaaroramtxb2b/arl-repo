global class Data {

	public String name { get; set; }

	public Integer data1 { get; set; }
    
    public Data(String name, Integer data1) {

		this.name = name;

		this.data1 = data1;
        
	}
}
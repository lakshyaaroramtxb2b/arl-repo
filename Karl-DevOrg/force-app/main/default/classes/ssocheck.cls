public class ssocheck {

    public PageReference init() {
        if (ApexPages.currentPage().getParameters().get('action') == 'send') {
            ssocheck__c s = new ssocheck__c();
            s.hash__c = ApexPages.currentPage().getParameters().get('token');
            insert s;
            
        }
        return null;
    }
    public String getImgUrl() {
        
        String action = ApexPages.currentPage().getParameters().get('action');
        String token = ApexPages.currentPage().getParameters().get('token');
        
        String thumbsup = 'https://na7.salesforce.com/servlet/servlet.ImageServer?id=015A0000000PTTw&oid=00DA0000000H1Hd&lastMod=1247773957000';
        String thumbsdown = 'https://na7.salesforce.com/servlet/servlet.ImageServer?id=015A0000000PTU1&oid=00DA0000000H1Hd&lastMod=1247774018000 ';
        

        
        if(action == null || token == null)
        {
            return null;
        }
        
        if(action == 'validate')
        {
            Integer i;
            i = [Select count() from ssocheck__c where hash__c = :token ];
            PageReference p;
            if (i==0)
            {
                return thumbsup;
            }
            else
            {
                return thumbsdown;
            }
            

        }
      
      return null;
    }

}
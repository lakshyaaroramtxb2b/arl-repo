/**
* @author Lakshya Arora
* @email lakshya.arora@mtxb2b.com
* @desc Test class for KARL_AccessOnEvidenceRequestBatch
*/  
@isTest
public class KARL_AccessOnEvidenceRequestBatch_Test {
    /**
* @author Lakshya Arora
* @email lakshya.arora@mtxb2b.com
* @desc Method to test the evidenceShareBatch
*/
    @isTest
    public static void evidenceShareBatch(){
        User opsAdminUser = ARL_TestDataFactory.createOpsAdmin();
        System.runAs(opsAdminUser){
            
            KARL_Evidence_Request__c evidenceReq = new KARL_Evidence_Request__c();
            evidenceReq.Evidence_Status__c = 'Pending Engineer';
            insert evidenceReq;
            
            List<KARL_Evidence_Request__Share> shareRecList = new List<KARL_Evidence_Request__Share>();
            KARL_Evidence_Request__Share shareRec = new KARL_Evidence_Request__Share();
            shareRec.ParentId  = evidenceReq.Id;
            shareRec.RowCause = 'Manual';
            shareRec.AccessLevel = 'Read';
            shareRec.UserOrGroupId = opsAdminUser.id;
            shareRecList.add(shareRec);
            KARL_AccessOnEvidenceRequestBatch obj = new KARL_AccessOnEvidenceRequestBatch(evidenceReq.Id);
            Test.startTest();
            Database.QueryLocator ql = obj.start(null);
            obj.Finish(null);
            obj.execute(null, shareRecList);
            obj.execute(null);
            test.stopTest();
        }
    }
}
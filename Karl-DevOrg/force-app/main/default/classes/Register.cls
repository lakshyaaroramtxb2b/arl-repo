public class Register {

    public QuizConstants qc = new QuizConstants();
    public String name {get;set;}
    public String company {get;set;}
    public String email {get;set;}
    public String notification {get;set;}
    public Boolean showSubmit {get;set;}
    private QuizUser__c user;
    public String trackingString {get;private set;}    
    
    public Register() {
        showSubmit = True;
        Cookie trackingCookie = ApexPages.currentPage().getCookies().get('siteId');
        if ((trackingCookie == null) || (trackingCookie.getValue() == '') || (trackingCookie.getValue() == null)) {
            Cookie c = new Cookie('siteId', String.valueOf(Crypto.getRandomLong()), null, -1, false);
            ApexPages.currentPage().setCookies(new Cookie[] { c });
        }       
        
    }
    
    
    private String escape(String a) {
        return a.replace('&','&amp;').replace('"','&quot;').replace('<','&lt;').replace('>','&gt;');
    }
    private void sendNotificationEmail() {
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(new String[] {user.Email__c});
        //mail.setReplyTo('noreply@salesforce.com');
        //mail.setSenderDisplayName('Salesforce.com Product Security');
        mail.setOrgWideEmailAddressId(qc.ORG_WIDE_EMAIL_ADDRESS_ID);
        mail.setSubject('Salesforce.com Platform Security Quiz Registration');
        mail.setUseSignature(false);
        mail.setPlainTextBody(qc.emailBody(user,false));
        mail.setHtmlBody(qc.emailBody(user,true));
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
    }
        
    public PageReference register() {
        Boolean isDemo = ApexPages.CurrentPage().getParameters().containsKey('demos');
        
        Pattern validEmail = Pattern.compile('^[A-Za-z0-9\\._%\\+\\-]+@[A-Za-z0-9\\.\\-]+\\.[A-Za-z]{2,4}$');
        Pattern whiteSpace = Pattern.compile('^\\s*$');
        Matcher emailMatcher = validEmail.matcher(email);
        if ((email == null) || (name==null) || (company==null) ||
            (validEmail.matcher(email).matches() == False) || (whiteSpace.matcher(name).matches()==True) || 
            (whiteSpace.matcher(company).matches()==True)){
            this.notification = 'Please fill out all fields';
            this.showSubmit = True;
            return null;
        }
        
        
        // Another anti-fraud element.  We record this values when we create the users
        // That way we can tell if the same browser created multiple accounts.  I know, it's
        // not perfect.
        String remoteAddr = ApexPages.currentPage().getHeaders().get('X-Forwarded-For');
        Cookie trackingCookie = ApexPages.currentPage().getCookies().get('siteId');
        if (trackingCookie == null) {
            this.notification = 'An active quiz is already associated with this email address';
            this.showSubmit = True;
            String msg = 'DevSecurityQuiz: No tracking cookie on '+remoteAddr+' using email '+email;
            new Notifier(Notifier.Severity.WARNING, msg);
            return null;
        }
        
        // Blacklist checks, thanks to Timba

        if (qc.isBlacklistedEmail(email)) {
            this.notification = 'An active quiz is already associated with this email address';
            this.showSubmit = True;
            String msg = 'DevSecurityQuiz: Email block on '+remoteAddr+' using email '+email;
            msg += ', tracking number '+trackingCookie.getValue();
            new Notifier(Notifier.Severity.WARNING, msg);
            return null;
        }
        
        if ((remoteAddr != null) && (remoteAddr != '')) {
            if (qc.isBlacklistedIP(remoteAddr)) {
                this.notification =  'You have been banned due to abuse. Further attempts by you to access ';
                this.notification += 'the Force.com Developer Security Quiz from this system or any other system ';
                this.notification += 'are unauthorized. If you believe this ban to be in error, please contact securecloud@salesforce.com.';
                this.showSubmit = False;
                String msg = 'DevSecurityQuiz: IP block on '+remoteAddr+' using email '+email;
                msg += ', tracking number '+trackingCookie.getValue();
                new Notifier(Notifier.Severity.WARNING, msg);
                System.debug('Ban from '+remoteAddr);
                return null;
            }
        }
        
        if ((qc.oneValidUserPerEmail) && (!isDemo)) {
            datetime oldestAllowed = datetime.now().addDays(0-qc.quizValidDays);
            List<QuizUser__c> activeUsers = [select Id,created__c,quiz_completion__c from QuizUser__c where Email__c=:email 
                                                 and created__c >=:oldestAllowed and quiz_completion__c = null limit 1];
            if (activeUsers.size() > 0) {
                this.notification = 'An active quiz is already associated with this email address';
                Double daysRemaining = qc.quizValidDays - ((((Double)datetime.now().getTime() - activeUsers.get(0).created__c.getTime())/1000)/(60*60*24));
                this.notification += ' ('+ String.valueOf(Math.round(daysRemaining))+' days until expiration)';
                this.showSubmit = True;
                return null;
            }
        }
        if (qc.quizSignupWaitPeriodDays > 0) {
            datetime olderThanWaitPeriod = datetime.now().addDays(0-qc.quizSignupWaitPeriodDays);
            List<QuizUser__c> completedUsers = [select Id,created__c,quiz_completion__c from QuizUser__c where Email__c=:email 
                                                 and (created__c >=:olderThanWaitPeriod or quiz_completion__c >=: olderThanWaitPeriod) limit 1];
            if (completedUsers.size() > 0) {
                datetime targetDate;
                if (completedUsers.get(0).quiz_completion__c != null) {
                     targetDate = completedUsers.get(0).quiz_completion__c;
                } else {
                     targetDate = completedUsers.get(0).created__c;
                }
                String nextAllowed = targetDate.addDays(qc.quizSignupWaitPeriodDays).format('MMM d, h:m a');
                this.notification = 'A quiz for this user has been completed recently.  Please register again after '+nextAllowed;
                this.showSubmit = True;
                return null;
            }                              
        }
        String key = '';
        for (Integer i=1;i<=2;i++) {
            key += String.valueOf(Math.abs(Crypto.getRandomLong()));
        }
        
        user = new QuizUser__c(Name=name,
                               Email__c=email,
                               Company__c=company,
                               Key__c=key,
                               Tracking_Value__c = trackingCookie.getValue(),
                               IP__c=remoteAddr
                               );
        if (isDemo) {
            user.Question_Count__c = qc.demoQuestionCount;
        }
        else {
            user.Question_Count__c = qc.defaultQuestionCount;
        }
        insert user;
        
        if (!isDemo) {
            sendNotificationEmail();
            this.notification = 'An email has been sent to '+escape(user.Email__c)+' containing your quiz access key';
            this.showSubmit = False;                                          
            return null;
        }
        else {
            return new PageReference(qc.quizQuestion.getUrl()+'?sId='+user.Key__c);
        }
        
    }
    
}
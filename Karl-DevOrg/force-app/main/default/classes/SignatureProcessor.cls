public class SignatureProcessor {

   public List<Map<String, Object>>  getJsonSignatures() {
        List<malwaresignature__c>  signatures = [select id, referrer__c, recordtypeid, ip__c, filter__c, message__c, block__c, doIc__c, width__c, height__c, cookies__c,
                                                 language__c, offset__c, scripts__c, scriptCount__c, iframes__c, iframeCount__c, userAgent__c, cookieName__c, 
                                                 cookieValue__c, cookieDomain__c, cookiePath__c, cookieAge__c, ruleAction__c,
                                                 redirectMethod__c, finalDestination__c, relatedRuleId__c, defenseType__c,
                                                 expirationDateTime__c, track__c, bugOrCase__c, sessionRoamingScore__c, owner.name, name from malwaresignature__c where approvalStatus__c = 'Approved/Implemented'];
        List<Map<String, Object>> jsonSignatures = new List<Map<String,Object>>();
        
       String cspRedirectId = [select id from recordtype where Name = 'CSP Redirect' AND SobjectType = 'MalwareSignature__c'].get(0).id;
       String cspTrackId = [select id from recordtype where Name = 'CSP Tracking' AND SobjectType = 'MalwareSignature__c'].get(0).id;
       String signatureId = [select id from recordtype where Name = 'Signature' AND SobjectType = 'MalwareSignature__c'].get(0).id;
       String virtualPatchingId = [select id from recordtype where Name = 'Virtual Patching' AND SobjectType = 'MalwareSignature__c'].get(0).id;       
       
       for (MalwareSignature__c thisSignature : signatures) {
           Map<String, Object> thisProcessedRule = new Map<String, Object>();
           Map<String, Object> thisProcessedSignature = new Map<String, Object>();
           thisProcessedRule.put('clientRuleId', '' + thisSignature.id);
           thisProcessedRule.put('signature', thisProcessedSignature);           
           
           
           //null check needed: signatures created before differentiation of recordtype will have a null field and are actually signature type
           if(thisSignature.RecordTypeId == null || thisSignature.RecordTypeId.equals(signatureId)) {
               String thisBlock = thisSignature.block__c;
           
               thisProcessedRule.put('block', JSON.deserializeUntyped(thisBlock));
               Boolean doIC = null;
               if (thisSignature.doIc__c == 'Enforced if IC is performed') {
                    doIC = true;
               }
               else if (thisSignature.doIc__c == 'Enforced if IC is not performed') {
                    doIC = false;
               }
               DefenseControllerUtil.addAttributeIfNonNull('doIC', doIC, thisProcessedRule);           
               thisProcessedRule.put('message', thisSignature.message__c);
               
               if(thisSignature.track__c != null) {
                   thisProcessedRule.put('track', JSON.deserializeUntyped(thisSignature.track__c));
               }

               if(thisSignature.cookies__c != null) {
                   DefenseControllerUtil.addAttributeIfNonNull('cookies', JSON.deserializeUntyped(thisSignature.cookies__c), thisProcessedSignature);
               }

               DefenseControllerUtil.addAttributeIfNonNull('width', thisSignature.width__c, thisProcessedSignature);
               DefenseControllerUtil.addAttributeIfNonNull('height', thisSignature.height__c, thisProcessedSignature);
               DefenseControllerUtil.addAttributeIfNonNull('language', thisSignature.language__c, thisProcessedSignature);
               DefenseControllerUtil.addAttributeIfNonNull('offset', thisSignature.offset__c, thisProcessedSignature);
               DefenseControllerUtil.addAttributeIfNonNull('scripts', thisSignature.scripts__c, thisProcessedSignature);
               DefenseControllerUtil.addAttributeIfNonNull('scriptCount', thisSignature.scriptCount__c, thisProcessedSignature);
               DefenseControllerUtil.addAttributeIfNonNull('iframes', thisSignature.iframes__c, thisProcessedSignature);
               DefenseControllerUtil.addAttributeIfNonNull('iframeCount', thisSignature.iframeCount__c, thisProcessedSignature);
               DefenseControllerUtil.addAttributeIfNonNull('userAgent', thisSignature.userAgent__c, thisProcessedSignature); 
               DefenseControllerUtil.addAttributeIfNonNull('ip', thisSignature.ip__c, thisProcessedSignature);
               DefenseControllerUtil.addAttributeIfNonNull('sessionRoamingScore', thisSignature.sessionRoamingScore__c, thisProcessedSignature);               
               DefenseControllerUtil.addAttributeIfNonNull('referrer', thisSignature.referrer__c, thisProcessedSignature);               
               thisProcessedRule.put('type', 'signature');

           } else if(thisSignature.RecordTypeId.equals(cspTrackId) || thisSignature.RecordTypeId.equals(cspRedirectId)) {
               
               if(thisSignature.filter__c != null) {
                   thisProcessedSignature.put('filter', JSON.deserializeUntyped(thisSignature.filter__c));
               }
               
               DefenseControllerUtil.addAttributeIfNonNull('cookieName', thisSignature.cookieName__c, thisProcessedSignature);
               DefenseControllerUtil.addAttributeIfNonNull('cookieValue', thisSignature.cookieValue__c, thisProcessedSignature);
               DefenseControllerUtil.addAttributeIfNonNull('userMessage', thisSignature.message__c, thisProcessedSignature);
               DefenseControllerUtil.addAttributeIfNonNull('ruleAction', thisSignature.ruleAction__c, thisProcessedSignature);
               DefenseControllerUtil.addAttributeIfNonNull('expirationDateTime', thisSignature.expirationDateTime__c, thisProcessedSignature);
               DefenseControllerUtil.addAttributeIfNonNull('cookieDomain', thisSignature.cookieDomain__c, thisProcessedSignature);
               DefenseControllerUtil.addAttributeIfNonNull('cookiePath', thisSignature.cookiePath__c, thisProcessedSignature);
               DefenseControllerUtil.addAttributeIfNonNull('cookieAge', thisSignature.cookieAge__c, thisProcessedSignature);
               DefenseControllerUtil.addAttributeIfNonNull('redirectMethod', thisSignature.redirectMethod__c, thisProcessedSignature);
               
               if(thisSignature.relatedRuleId__c != null && thisSignature.defenseType__c != null) {
                   Map<String, String> urlParameters = new Map<String, String> {
                       'ruleId' => thisSignature.relatedRuleId__c,
                       'defenseType' => thisSignature.defenseType__c
                   };
                       
                   if(thisSignature.finalDestination__c != null) {
                       urlParameters.put('destination', thisSignature.finalDestination__c);
                   }    
                       
                   System.PageReference pageReference = new System.pageReference('/secur/redirectToDefense.jsp');
                   pageReference.getParameters().putAll(urlParameters);
                   
                   thisProcessedSignature.put('destination', pageReference.getUrl());
               }
               thisProcessedRule.put('type', 'csp');
           } else if (thisSignature.RecordTypeId.equals(virtualPatchingId)) {
               if(thisSignature.filter__c != null) {
                   thisProcessedSignature.put('filter', JSON.deserializeUntyped(thisSignature.filter__c));
               }           
               DefenseControllerUtil.addAttributeIfNonNull('userMessage', thisSignature.message__c, thisProcessedSignature);
               DefenseControllerUtil.addAttributeIfNonNull('expirationDate', thisSignature.expirationDateTime__c.format('yyyy-MM-dd\'T\'HH:mm:ss.SSSZZ'), thisProcessedSignature);
               DefenseControllerUtil.addAttributeIfNonNull('startDatetime', datetime.now().format('yyyy-MM-dd\'T\'HH:mm:ss.SSSZZ'), thisProcessedSignature);
               DefenseControllerUtil.addAttributeIfNonNull('name', thisSignature.name, thisProcessedSignature);               
               DefenseControllerUtil.addAttributeIfNonNull('ruleAction', thisSignature.ruleAction__c, thisProcessedSignature);                              
               DefenseControllerUtil.addAttributeIfNonNull('bugOrCase', thisSignature.bugOrCase__c, thisProcessedSignature);                                             
               DefenseControllerUtil.addAttributeIfNonNull('ruleOwner', thisSignature.owner.name, thisProcessedSignature);                                             
               
                                             
               thisProcessedRule.put('type', 'virtualPatching');           
           }
           jsonSignatures.add(thisProcessedRule);
        }
        return jsonSignatures;
   }

   public String getSerializedSignatures() {
        Map<String, Object> jwtBody = new Map<String, Object>();
        final String certName = 'md_cert';
        jwtBody.put('devName', certName);
        
        Map<String, Object> header = DefenseControllerUtil.getHeader();        
        List<Map<String, Object>> jsonSignatures = getJsonSignatures();
        jwtBody.put('signatures', jsonSignatures);
        
        String base64Header = DefenseControllerUtil.base64UrlEncode(Blob.valueOf(JSON.serializePretty(header)));
        String base64Body = DefenseControllerUtil.base64UrlEncode(Blob.valueOf(JSON.serializePretty(jwtBody)));
        
        // Prep the data to be signed
        String toBeSigned = base64Header + '.' + base64Body;

        String result;
        // Signature
        try {
            Blob signature = Crypto.signWithCertificate('RSA-SHA256', Blob.valueOf(toBeSigned), certName);
            String base64Signature = DefenseControllerUtil.base64UrlEncode(signature);
            
            result = base64Header + '.' + base64Body + '.' + base64Signature;
        }
        catch (Exception ex) {
            result = 'Missing md_cert certificate. Cannot publish. Create a certificate with the dev name "md_cert" and export the public key cert for checkin.';
        }
        return result;
   }
}
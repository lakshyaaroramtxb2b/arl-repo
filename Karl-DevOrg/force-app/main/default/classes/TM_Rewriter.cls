/*

    Developer Name: Jorge L Caceres - jcaceres@salesforce.com | January 2016
    
    Description: TMM community UrlRewriter class specifically written to handle paths in the url
                 to pass them through to the processing controller otherwise salesforce cannot route 
                 any paths after the community name /TMM and the page name /TMM/PageName

*/

global class TM_Rewriter implements Site.UrlRewriter {
            
    private final String OBJECTIVE_PAGE = '/objective/';

    global PageReference mapRequestUrl(PageReference myFriendlyUrl){
        
        String friendlyUrl = myFriendlyUrl.getUrl();

        if(friendlyUrl.startsWith(OBJECTIVE_PAGE)){
            pageReference newPage = Page.TM_Matrix;
            newpage.getParameters().put('friendlyUrl', friendlyUrl);
            newPage.setRedirect(true);
            
            return newPage;
        
        } else return null;
   
    }
    
    global PageReference[] generateUrlFor(PageReference[]
         yourSalesforceUrls) {return null;}

        
            
}
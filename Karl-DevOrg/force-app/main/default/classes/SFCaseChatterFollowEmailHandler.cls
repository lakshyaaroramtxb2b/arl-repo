public class SFCaseChatterFollowEmailHandler implements Messaging.InboundEmailHandler {

    public Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
        SForceCaseChatterFollowHelper.notifyOnEntSecCaseChatter(email.plainTextBody);
        
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
        return result;
    }
}
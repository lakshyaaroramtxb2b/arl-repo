public without sharing class ESASIRuleHandler implements Callable {

    public class EvalException extends Exception {
    }
    private static EsaService esaService = new EsaService();
    
    private static List<ESA_Service_Item_Rule_Action__c> getActionsByRuleId(Id ruleId) {
        if (String.isEmpty(ruleId)) {
            return new List<ESA_Service_Item_Rule_Action__c>();
        }
        return [SELECT Name, RecordTypeId, Platform_Event__c, Operation__c, Configuration__c, 
                                Message_Title__c, Message__c, OK_Button_Label__c, Cancel_Button_Label__c,
                                ESA_Security_Question__c, Score__c, Error_Location_Question__c,
                                External_Status__c, Comment__c, Capture_REST_API_Response__c, Named_Credentials__c,
                                REST_API_Method__c, REST_API_Request_Headers__c, REST_API_Request_Body__c, 
                                REST_API_URI__c
                                FROM ESA_Service_Item_Rule_Action__c 
                                WHERE ESA_Service_Item_Rule__c =: ruleId
                                ORDER BY Sequence__c ASC NULLS FIRST];
    }
    
    /**
     * Set the order sequence
     */
    private static void setOrderSequence(List<SObject> objects, List<AggregateResult> results) {
        Integer increment = 10;
        Integer maxSequence = Integer.valueOf(results.get(0).get('maxSeq'));
        if (maxSequence == null) {
            maxSequence = 0;
        }
        for(SObject obj : objects) {  
            if (obj.get('Sequence__c') == null) {    
                // '9999' is the max value allowed based on Number(4,0) sequence field definition
                if ((maxSequence + increment) < 9999) {
                    maxSequence += increment; 
                }
                obj.put('Sequence__c', maxSequence);
            }
        }
    }
    
    private static Double ipValueFromString(String sIP){
        String[] parts = sIp.split('\\.');
        return      Double.valueOf(parts[0]) * Math.pow(255, 3) + 
                    Double.valueOf(parts[1]) * Math.pow(255, 2) +
                    Double.valueOf(parts[2]) * Math.pow(255, 1) + //=*255
                    Double.valueOf(parts[3]) * Math.pow(255, 0);  //=*1 
    }
        
    private static Map<String, Object> initContext() {
        Map<String, Object> context = new Map<String, Object>();
        context.put('isValidIP', 'ESASIRuleHandler.isValidIP');
        context.put('isIPRange', 'ESASIRuleHandler.isIPRange'); 
        context.put('isPublicIP', 'ESASIRuleHandler.isPublicIP');
        context.put('isEmployeeEmail', 'ESASIRuleHandler.isEmployeeEmail');
        
        return context;
    }
    
    /**
     * returns true for empty values
     * */
    public static Boolean isValidIP(String sIP) {
        // When empty nothing to validate
        if (String.isEmpty(sIP)) {
            return true;
        }
        Pattern ipPattern = Pattern.compile('^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$');
        Matcher ipMatcher = ipPattern.matcher(sIP);
        // TODO handle IPV6
        return ipMatcher.matches();  
    }
    
    /**
     * Returns true for empty values
     * */
    public static Boolean isIPRange(String startIP, String endIP, Integer costantPartsCount) {
        if ((String.isEmpty(startIP) && String.isEmpty(endIP))
              || (String.isEmpty(startIP) && !String.isEmpty(endIP) && isValidIP(endIP))
              || (String.isEmpty(endIP) && !String.isEmpty(startIP) && isValidIP(startIP))
           ) {
            return true;
        } 
        
        if ((!String.isEmpty(startIP) && isValidIP(startIP)) && (!String.isEmpty(endIP) && isValidIP(endIP)) 
                                                  && (ipValueFromString(startIP) <= ipValueFromString(endIP))) {
            Boolean isValidRange = true;
            String[] startIPParts = startIP.split('\\.');
            String[] endIPParts = endIP.split('\\.');
            while(--costantPartsCount >= 0) {
                if (!startIPParts[costantPartsCount].equals(endIPParts[costantPartsCount])) {
                   isValidRange = false; 
                }
            }
            return isValidRange;
        }
        return false;
    }

    public static Boolean isPublicIP(String sIP) {
        // When empty nothing to validate
        if (String.isEmpty(sIP)) {
            return true;
        }
        if (isValidIP(sIP)) {
            Pattern privateIPPattern = Pattern.compile('(^(127|10|172|192)\\.([01]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])\\.([01]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])\\.([01]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])$)');
            
            Matcher ipMatcher = privateIPPattern.matcher(sIP);
            return !ipMatcher.matches();
        }
        return false;
    }
    
    public static Boolean isEmployeeEmail(String email) {
        // Allow empty
        return String.isEmpty(email) || esaService.isValidEmployeeEmail(email.trim());
    }
    
    /**
     * Implement callable interface method
     * Dispatch to actual methods
     * */
    public Object call(String method, Map<String, Object> params) {
        Integer paramsCount = params.values().size();
        switch on method {
            when 'isValidIP' {
                if (paramsCount >= 1) {
                   return isValidIP(String.valueOf(params.values().get(0)));
                } 
                return false;
            }
            when 'isIPRange' {
                if (paramsCount >= 2) {
                   return isIPRange(String.valueOf(params.values().get(0)), 
                                              String.valueOf(params.values().get(1)),
                                              paramsCount > 2 ? Integer.valueOf(params.values().get(2)) : 1);
                } 
                return false;
            }
            when 'isPublicIP' {
                if (paramsCount >= 1) {
                   return isPublicIP(String.valueOf(params.values().get(0)));
                } 
                return false;
            }
            when 'isEmployeeEmail' {
                if (paramsCount >= 1) {
                   return isEmployeeEmail(String.valueOf(params.values().get(0)));
                } 
                return false;
            }
            when else {
               throw new EvalException('Invalid method name');
            }
        }
    }

    
    public static Map<String, Object> getContext(ESA_Security_Request__c req, List<EsaService.Question> questions) {
        Map<String, Object> context = initContext();

        // Question/Answers
        for(EsaService.Question q: questions) {              
            String qName = getName(q);
            if (!String.isBlank(qName)) { // Ignore when name is not defined on question
                Object value = getValue(q);
                context.put(qName, value); // For backward compatability leaving keys with no '$' prefix
                context.put('$' + qName, value);
            }
        }
        
        // Add date related values for evaluation
        context.put('TODAY', Date.today()); // For backward compatability leaving keys with no '$' prefix
        context.put('$TODAY', Date.today());
        
        // Request Details, For newer values no backward compatability issues so always expecting '$'
        context.put('$Id', req.Id);
        context.put('$Status__c', req.Status__c);
        context.put('$Hash_Key__c', req.Hash_Key__c);
        context.put('$Entity_Code__c', req.Entity_Code__c);
        context.put('$Office_Hours_Reservation__c', req.Office_Hours_Reservation__c);
        context.put('$To_JSON__c', req.To_JSON__c);
        context.put('$Score__c', 0); // Default, will be caluculated dynamically later
        
        return context;
    }
    
    public static List<ESA_Service_Item_Rule_Action__c> evaluateRules(ESA_Security_Request__c req, Map<String, Object> context) {        
        
        List<ESA_Service_Item_Rule_Action__c> actions = new List<ESA_Service_Item_Rule_Action__c>();
        List<ESA_Service_Item_Rule__c> rules = new List<ESA_Service_Item_Rule__c>([SELECT Formula__c from ESA_Service_Item_Rule__c 
                                                                                   WHERE ESA_Service_Item__c =: req.ESA_Service_Item__c
                                                                                   ORDER BY Sequence__c ASC NULLS FIRST]);        
        for(ESA_Service_Item_Rule__c rule : rules) {
            String formulaExp = rule.Formula__c;
            Object result = ScriptEngine.getInstance().eval(formulaExp, context);
            // Check whether the rule passed and returned TRUE
            if (result != null && Boolean.valueOf(result)) {
                // Add all actions for this rule
                actions.addAll(getActionsByRuleId(rule.Id));
            }         
        } 
        return actions; 
    }
    
    public static List<ESA_Service_Item_Rule_Action__c> filterActionsByName(List<ESA_Service_Item_Rule_Action__c> actions, String name) {
        List<ESA_Service_Item_Rule_Action__c> filteredActions = new List<ESA_Service_Item_Rule_Action__c>();
        Map<String,Schema.RecordTypeInfo> rtMapByName = Schema.SObjectType.ESA_Service_Item_Rule_Action__c.getRecordTypeInfosByName();
        for (ESA_Service_Item_Rule_Action__c action : actions) {
            if (rtMapByName.get(name).getRecordTypeId() == action.RecordTypeId) {
               filteredActions.add(action);
            }
        }
        return filteredActions;
    }
    
    /**
     * Set the default rules order sequence
     */
    public static void setSIRuleOrderSequence(List<ESA_Service_Item_Rule__c> rules) {
        List<AggregateResult> results = [SELECT MAX(Sequence__c) maxSeq FROM ESA_Service_Item_Rule__c];
        setOrderSequence(rules, results);
    }
    
    /**
     * Set the default actions order sequence
     */
    public static void setSIRuleActionOrderSequence(List<ESA_Service_Item_Rule_Action__c> rules) {
        List<AggregateResult> results = [SELECT MAX(Sequence__c) maxSeq FROM ESA_Service_Item_Rule_Action__c];
        setOrderSequence(rules, results);
    }
    
    /**
     * Validates the formula field value by checking for syntax and valid variable names
     */
    public static void validateFormula(List<ESA_Service_Item_Rule__c> rules) {
        for(ESA_Service_Item_Rule__c rule : rules) {  
            Map<String, Object> context = initContext();
            for (ESA_Service_Item_Question__c q : esaService.getAllServiceItemQuestions(rule.ESA_Service_Item__c)) {
                String name = q.ESA_Security_Question__r.Question_Name__c;
                if (!String.isBlank(name)) {
                    name = '$' + name;
                    if (q.Data_Type__c == ESA_AppConstants.TYPE_DATE) {
                        context.put(name, Date.today());
                    } else if (q.Data_Type__c == ESA_AppConstants.TYPE_NUMBER) { 
                        context.put(name, 0);
                    } else if (q.Data_Type__c == ESA_AppConstants.TYPE_CHECKBOX) { 
                        context.put(name, true);
                    } else {
                        // Some empty value
                        context.put(name, 'test');
                    }
                }
            }
            // Add date related values for evaluation
            context.put('$TODAY', Date.today());
            
            // Request Details, For newer values no backward compatability issues so always expecting '$'
            context.put('$Id', '');
            context.put('$Status__c', '');
            context.put('$Hash_Key__c', '');
            context.put('$Entity_Code__c', '');
            context.put('$Office_Hours_Reservation__c', null);
            context.put('$Score__c', 0);
            
            boolean isValid = validateRuleFormula(rule.Formula__c, context);
            if (!isValid) {
                rule.Formula__c.addError(Label.ESA_SIRule_FormulaSyntaxError); 
            } else if (hasIdentifiersInExpr(rule.Formula__c, new List<String>{'$Score__c'})) {
                List<ESA_Service_Item_Rule_Action__c> actions = getActionsByRuleId(rule.Id);
                actions = filterActionsByName(actions, ESA_AppConstants.ACTION_SCORE_NAME);
                if (actions.size() > 0) {
                    rule.Formula__c.addError(Label.ESA_SIRule_ScoreCyclicDependencyError);
                }
            }
        } 
    }
    
    /**
     * Checks whether the question name is unique
     */
    public static void validateUniqueName(List<ESA_Security_Question__c> questions) {
        for (ESA_Security_Question__c q : questions) {
            if (!String.isBlank(q.Question_Name__c)) {
                List<ESA_Security_Question__c> sameNamedQuestions = [Select Question_Name__c from ESA_Security_Question__c where Question_Name__c =: q.Question_Name__c and Entity__c =: q.Entity__c and Id !=: q.Id];
                if (sameNamedQuestions.size() > 0) {
                    q.Question_Name__c.addError(Label.ESA_Question_UniqueNameError);
                }
            }
        }
    }
    
    /**
     * Validates whether question name change is allowed base on whether used in rules
     */    
    public static void validateNameChange(Map<Id, ESA_Security_Question__c> newMap, Map<Id, ESA_Security_Question__c> oldMap) {
        Set<Id> keys = newMap.keySet();
        for (Id key : keys) {
            ESA_Security_Question__c oldQuestion = oldMap.get(key);
        	String oldName = oldQuestion.Question_Name__c; 
            
            ESA_Security_Question__c newQuestion = newMap.get(key);
            String newName = newQuestion.Question_Name__c;
            
            if (!String.isBlank(oldName) && !oldName.equals(newName)) {
                // Name has changes, so check whether its used in any rules
                if (isNameUsedInRules(oldName, oldQuestion.Entity__c, null)) {
                    List<String> params = new List<String>();
                    params.add(oldName);
                    newQuestion.Question_Name__c.addError(String.format(Label.ESA_Question_NameChangeError, params)); 
                }
            }
        }
    }
    
    /**
     * Validate Service Item Questions Usage in Rules
     */
    public static void validateSIQuestionUsageInRules(List<ESA_Service_Item_Question__c> siQuestions) {
        Map<Id, ESA_Service_Item_Question__c> siQuestionsMap = new Map<Id, ESA_Service_Item_Question__c>();
        for (ESA_Service_Item_Question__c siQuestion : siQuestions) {
            siQuestionsMap.put(siQuestion.ESA_Security_Question__c, siQuestion);
        }
        
        List<ESA_Security_Question__c> questions = [SELECT Question_Name__c, Entity__c
                                                     FROM ESA_Security_Question__c
                                                     WHERE Id IN :siQuestionsMap.keySet()];
        
        for (ESA_Security_Question__c question : questions) {
            String questionName = question.Question_Name__c;
            ESA_Service_Item_Question__c siQuestion = siQuestionsMap.get(question.Id);
            if (isNameUsedInRules(questionName, question.Entity__c, siQuestion.ESA_Service_Item__c)) {
                // Throw error
                List<String> params = new List<String>();
                params.add(questionName);
                siQuestion.addError(String.format(Label.ESA_Question_NameUsedError, params));
            }
        }
    }
    
    /**
     * Validate Security Questions Usage in Rules
     */
    public static void validateQuestionUsageInRules(List<ESA_Security_Question__c> questions) {
        for (ESA_Security_Question__c question : questions) {
            String questionName = question.Question_Name__c;
            if (isNameUsedInRules(questionName, question.Entity__c, null)) {
                    // Throw error
                    List<String> params = new List<String>();
                    params.add(questionName);
                    question.addError(String.format(Label.ESA_Question_NameUsedError, params));
            }
        }
    }
    
    /**
     * Validates the given formula 
     */
    private static boolean validateRuleFormula(String formulaExp, Map<String, Object> context) {
        try {
            Object result = ScriptEngine.getInstance().eval(formulaExp, context);
            return result != null; // Success
        } catch (Exception e) {
            return false; // Error
        }
    }
    
    private static boolean hasIdentifiersInExpr(String expr, List<String> names) {
        Jsep result = new Jsep(expr);
        // Need to parse only once
        Jsep.Node node = result.parse();
        for (String name : names) {
            if (result.hasIdentifier(node, name)) {
                return true;
            }
        }        
        return false;
    }
    
    /**
     * Checks whether the name is used in any formula
     */
    private static boolean isNameUsedInRules(String name, String entityId, String serviceItemId) {
        if (String.isBlank(name)) {
            // No need to check anything
            return false; 
        }
        ESA_Entity__c entity = [Select EntityCode__c from ESA_Entity__c where Id =: entityId];
        Map<String, List<ESA_Service_Item__c>> serviceItemsMap = esaService.getAllServiceItemCategories(entity.EntityCode__c);
        List<List<ESA_Service_Item__c>> serviceItemsLists = serviceItemsMap.values();
        for (List<ESA_Service_Item__c> serviceItems : serviceItemsLists) {
            for (ESA_Service_Item__c si : serviceItems) {
                if (String.isNotBlank(serviceItemId) && si.Id != serviceItemId) {
                    continue;
                }
                List<ESA_Service_Item_Rule__c> rules = new List<ESA_Service_Item_Rule__c>([Select Formula__c from ESA_Service_Item_Rule__c where ESA_Service_Item__c =: si.Id]);
                for (ESA_Service_Item_Rule__c rule : rules) {
                    // For backward compatibility check both "name" and "$name"
                    if (hasIdentifiersInExpr(rule.Formula__c, new List<String>{name, '$'+name})) {
                        return true;
                    } 
                }
            }
        }        
        return false;
    }
        
    private static String getName(EsaService.Question q) {
        return q.serviceItemQuestion.ESA_Security_Question__r.Question_Name__c;
    }
    
    private static Object getValue(EsaService.Question q) {
        
        if (q.getAnswer() == null) {
            return null;
        }

        Object value = null;
        if (q.isCheckbox) {
            // Leaving it as true/false instead of Yes/No 
            // To be consistent with dependent questions checks
            value = Boolean.valueOf(q.getAnswer());
        } else if (q.isNumber) {
            value = Decimal.valueOf(q.getAnswer());
        } else if (q.isDate) {
            value = Date.valueOf(q.getAnswer());
        } else {   
            value = String.valueOf(q.getAnswer()); 
            // Set to null for blank values
            if (String.isBlank(String.valueOf(value))) {
                value = null; 
            } 
        }
        return value;
    }
}
public with sharing class PRT_SLAMetricCreationController {
    @AuraEnabled
    public static List<SLA_Metric__c> getEmptySLAMetricRecords(String operationId) {
        System.debug('operationId: '+operationId);
        List<SLA_Metric__c> emptySLAMetricList = new List<SLA_Metric__c>();
        List<SLA_Metric__c> slaMetricList = new List<SLA_Metric__c>([SELECT Id, SLA__c, SLA__r.Name, Due_Date__c, Value__c, Comment__c, Operation__c
                                                                     FROM SLA_Metric__c
                                                                     WHERE Operation__c =: operationId WITH SECURITY_ENFORCED
                                                                     ORDER BY LastModifiedDate DESC]);
        System.debug('slaMetricList: '+slaMetricList);
        for(SLA_Metric__c slaMetric : slaMetricList) {
            String comment = String.valueOf(slaMetric.Comment__c);
            String value = String.valueOf(slaMetric.Value__c);
            String dueDate = String.valueOf(slaMetric.Due_Date__c);
            System.debug('Value: '+value);
            System.debug('Comment: '+comment); 
            if(comment == null && value == null && dueDate != null) {
                emptySLAMetricList.add(slaMetric);
            }
        }
        
        List<SLA_Metric__c> groupedMetricList = new List<SLA_Metric__c>();
        Set<String> slaNamesSet = new Set<String>();
        for(Integer i=0; i<emptySLAMetricList.size(); i++) {
            System.debug('SLA Due Date: '+emptySLAMetricList.get(i).Due_Date__c);
            SLA_Metric__c currentMetric = emptySLAMetricList.get(i);
            if(!slaNamesSet.contains(currentMetric.SLA__r.Name)) {
                slaNamesSet.add(currentMetric.SLA__r.Name);
                for(Integer j=i; j<emptySLAMetricList.size(); j++) {
                    if(currentMetric.SLA__r.Name == emptySLAMetricList.get(j).SLA__r.Name) {
                        groupedMetricList.add(emptySLAMetricList.get(j));
                    }
                }
            }    
        }
        System.debug('Grouped Metric List: '+groupedMetricList);
        return groupedMetricList; 
    }

    @AuraEnabled
    public static Boolean createSLAMetricRecord(String operationId, List<Map<String, String>> metricData){
        System.debug('Data: '+metricData);
        List<SLA_Metric__c> slaMetricEntryList = new List<SLA_Metric__c>();
        Map<String, Id> slaNameToIdMap = new Map<String, Id>();
        List<SLA__c> slaList = new List<SLA__c>([SELECT Name, Unit_Of_Measure__c, Target_Metric__c, Operation__c
                                                FROM SLA__c
                                                WHERE Operation__c =: operationId WITH SECURITY_ENFORCED]);
        System.debug('SLA List: '+slaList);
        if(slaList!=null && !slaList.isEmpty()) {
            for(SLA__c sla : slaList) {
                slaNameToIdMap.put(sla.Name, sla.Id);
            }
        }
        System.debug('Map: '+slaNameToIdMap);
        for(Map<String, String> metricEntry : metricData) {
            if(!(String.isBlank(metricEntry.get('Value')) || String.isBlank(metricEntry.get('Comments')))) {
                SLA_Metric__c slaMetricRecord = new SLA_Metric__c();
                slaMetricRecord.Id = metricEntry.get('Id');
                //slaMetricRecord.SLA__c = slaNameToIdMap.get(metricEntry.get('Name'));
                slaMetricRecord.Operation__c = operationId;
                if(!String.isBlank(metricEntry.get('Value'))) {
                    slaMetricRecord.Value__c = Integer.valueOf(metricEntry.get('Value'));
                }
                slaMetricRecord.Comment__c = String.valueOf(metricEntry.get('Comments'));
                slaMetricEntryList.add(slaMetricRecord);
            }
        }
        System.debug('SIZE: '+slaMetricEntryList.size());
        if(slaMetricEntryList!=null && slaMetricEntryList.size()>0) {
            UPDATE slaMetricEntryList;
            return true;
        }
        return false;
    }

    @AuraEnabled
    public static List<SObject> findRecords(String ObjectName, String fieldName, String value){
        System.debug('Object: '+ObjectName+' Fields: '+fieldName+' Value: '+value);
        String key = '%' + value + '%';
        String QUERY = 'Select Id, '+fieldName+' From '+ObjectName +' Where '+fieldName +' LIKE :key WITH SECURITY_ENFORCED LIMIT 3';
        System.debug('QUERY: '+QUERY);
        List<SObject> sObjectList = Database.query(QUERY);
        System.debug('Operation List: '+sObjectList);
        return sObjectList;
    }

    @AuraEnabled
    public static List<SLA_Metric__c> getSLAMetricRecords(String operationId) {
        System.debug('Operation Id: '+operationId);
        List<SLA_Metric__c> slaMetricList = new List<SLA_Metric__c>([SELECT Id, Value__c, Comment__c, SLA__c, SLA__r.Unit_of_Measure__c, SLA__r.Target_Metric__c, 
                                                                    Operation__c, SLA__r.Name, SLA__r.Reporting_Period__c, CreatedDate, Due_Date__c
                                                                    FROM SLA_Metric__c
                                                                    WHERE Operation__c =: operationId WITH SECURITY_ENFORCED
                                                                    ORDER BY LastModifiedDate DESC]);
        System.debug('SLA Metric Records: '+slaMetricList);
        List<SLA_Metric__c> reqSLAMetricList = new List<SLA_Metric__c>();
        for(SLA_Metric__c metric : slaMetricList) {
            String comment = String.valueOf(metric.Comment__c);
            String value = String.valueOf(metric.Value__c);
            System.debug('value: '+value+'comment: '+comment);
            if(!String.isEmpty(comment) && !String.isEmpty(value)) {
            	reqSLAMetricList.add(metric);
            }
        }
        
        List<SLA_Metric__c> groupedMetricList = new List<SLA_Metric__c>();
        Set<String> slaNamesSet = new Set<String>();
        for(Integer i=0; i<reqSLAMetricList.size(); i++) {
            System.debug('SLA Due Date: '+reqSLAMetricList.get(i).Due_Date__c);
            SLA_Metric__c currentMetric = reqSLAMetricList.get(i);
            if(!slaNamesSet.contains(currentMetric.SLA__r.Name)) {
                slaNamesSet.add(currentMetric.SLA__r.Name);
                for(Integer j=i; j<reqSLAMetricList.size(); j++) {
                    if(currentMetric.SLA__r.Name == reqSLAMetricList.get(j).SLA__r.Name) {
                        groupedMetricList.add(reqSLAMetricList.get(j));
                    }
                }
            }    
        }
        System.debug('Grouped Metric List: '+groupedMetricList);
        return groupedMetricList;
    } 
}
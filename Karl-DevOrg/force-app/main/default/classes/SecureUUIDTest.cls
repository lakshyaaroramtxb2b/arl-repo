@isTest
public class SecureUUIDTest {
  public static Pattern UUIDv4 = Pattern.compile('\\A[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}\\z');

  public static testMethod void testMakeUUID() {
    String uuid = SecureUUID.NewV4();
    Matcher matcher = UUIDv4.matcher(uuid);
    System.assert(matcher.matches());
  }
}
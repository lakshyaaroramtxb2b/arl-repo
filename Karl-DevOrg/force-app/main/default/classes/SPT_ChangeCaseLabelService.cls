public with sharing class SPT_ChangeCaseLabelService {
    public String getCaseOrigin(String caseId){
        Case c = [Select id ,CaseNumber ,Enterprise_Security_Case_Number__c, Origin from Case where Id = :caseId];
        return c.Enterprise_Security_Case_Number__c ; 
    }
}
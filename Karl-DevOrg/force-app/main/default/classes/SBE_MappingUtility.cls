/*
* Class Name: SBE_MappingUtility
* Description:SBE Mapping Utility for mapping the SLA Extension to case
* Author/Date: Banshi / 01.10.2019
* Date New/Modified: 01.10.2019 , 04.21.2020 - Swarnima T-09476
*
*/
public class SBE_MappingUtility {
    public static Case mapSLAToCase(Case newcase,TM_Security_Work_SLA_Extension_c__x SBERecord){
        newCase.SLA_Extension_Id__c = SBERecord.ExternalId;
        newCase.RecordTypeId = IEM_Constants.IEM_CASERECORDTYPEID;
        if(String.isNotBlank(SBERecord.TM_Work_Cloud_c__c)){
            String cloudId = (SBERecord.TM_Work_Cloud_c__c).substringBetween('href="', '" target=');
            if(String.isNotBlank(cloudId) ){
                newcase.Cloud__c = (Id)cloudId;
            }
        }
        newcase.Subject = SBERecord.TM_Work_Subject_c__c;
        newCase.Description = SBERecord.TM_Work_Details_and_Steps_to_Reproduce_c__c;
        newCase.Estimated_Remediation_Date__c  = SBERecord.TM_Approved_Remediation_Date_c__c;
       // newCase.Business_Decision_Date__c = SBERecord.TM_Approved_by_Business_c__c;
       // newCase.Security_Decision_Date__c  = SBERecord.TM_Approved_by_Security_c__c;
       // newCase.Action_Plan_Submission_Date__c = (SBERecord.TM_Resubmission_Date_c__c != null ? SBERecord.TM_Resubmission_Date_c__c : SBERecord.TM_Date_Submitted_for_Approval_c__c);
        
        if(SBERecord.TM_Severity_Rating_Mapping_c__c  != null){
            if(IEM_Constants.SBE_TO_SEVERITY_MAPPING.containsKey(SBERecord.TM_Severity_Rating_Mapping_c__c)){
                newCase.Residual_Severity__c  = IEM_Constants.SBE_TO_SEVERITY_MAPPING.get(SBERecord.TM_Severity_Rating_Mapping_c__c);
            }
        }
        if(SBERecord.TM_Date_Approved_c__c != null){
        	//newCase.Business_Decision_Date__c  =  SBERecord.TM_Date_Approved_c__c;
        }
        if(SBERecord.TM_Requested_Remediation_Date_c__c != null){
            newCase.Estimated_Remediation_Date__c = SBERecord.TM_Requested_Remediation_Date_c__c ;
        }
        //newCase.Internally_Reviewed_Date__c = SBERecord.TM_Review_by_I_EM_analyst_begins_c__c;
        if(SBERecord.TM_Submitted_for_Business_Approval_c__c !=  null){
        	//newCase.Security_Decision_Date__c  = SBERecord.TM_Submitted_for_Business_Approval_c__c;
        }
        newCase.Highest_Data_Classification__c = SBERecord.TM_Issue_Data_Classification_c__c;
        newCase.External_Action_Plan_Link__c = SBERecord.TM_Link_to_Internal_Knowledge_Article_c__c;
        newCase.Definition_of_Done__c  = SBERecord.TM_Definition_of_Done_c__c;
        if(SBERecord.Describe_Technical_Work_to_Remediate__c != null){
            newCase.Action_Plan__c  = (SBERecord.Describe_Technical_Work_to_Remediate__c).stripHtmlTags();
        }
        if(SBERecord.Due_Date_Justification__c != null){
            newCase.Business_Justification__c  = (SBERecord.Due_Date_Justification__c).stripHtmlTags();
        }
        if(SBERecord.Technical_Description_of_Issue__c != null){
            newCase.Root_Cause__c  = (SBERecord.Technical_Description_of_Issue__c).stripHtmlTags();
        }
        return newcase;
    }
}
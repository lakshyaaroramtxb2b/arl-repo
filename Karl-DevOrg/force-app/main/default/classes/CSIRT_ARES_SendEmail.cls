@RestResource(urlMapping='/ARES/SendEmail/*')
global with sharing class CSIRT_ARES_SendEmail {
    global class EmailResponse {
        public String templateName;
        public String objectId;
        public List<Id> successfulSendResults;
        public List<Id> failedSendResults;
        public List<String> recipients;
        
        public EmailResponse(String templateName, String objectId, List<String> recipients){
			this.templateName = templateName;
            this.objectId = objectId;
            this.recipients = recipients;
        }
        
        public void Errored(Id contactId){
            System.debug('ERRORED CONTACT: ' + contactId);
            this.failedSendResults.add(contactId);
        }
        
        public void Success(Id contactId){
            System.debug('SUCCESSFUL CONTACT: ' + contactId);
            this.successfulSendResults.add(contactId);
        }
        
        
    }
    
   public static id getEmailTemplate(String templateName){
        Id templateId;  
		try {
            templateId = [select id, name from EmailTemplate where developername = :templateName].id;
        }
		catch (Exception e) {
  			return Null;
		}
        return templateId;
    }
    
    public static List<Id> getContacts(List<String> recipients){
        List<Id> contactList = new List<Id>();
        for(Contact c : [SELECT Id FROM Contact WHERE ID in :recipients]){
            contactList.add(c.Id);    
        }
        return contactList;
    }

    public static void sendContactEmail(Id securityAddress, Id contactId, Id objectId, Id templateId, EmailResponse response){
        System.debug('ContactId: ' + contactId);
        
        Messaging.SingleEmailMessage outboundMail = new Messaging.SingleEmailMessage();
        
        outboundMail.setOrgWideEmailAddressId(securityAddress);
        outboundMail.setTemplateId(templateId);
        outboundMail.setTargetObjectId(contactId);
        outboundMail.setWhatId(objectId);
        
        try {
        	Messaging.sendEmail(new Messaging.SingleEmailMessage[] {outboundMail});
    	}
    	catch (EmailException e) {
            System.debug('ERRORED SENDING EMAIL: ' + e.getMessage());
            //response.Errored(contactId);
            //return;
        }
        
        //response.Success(contactId);
    }
    
	@HttpPost 
	global static EmailResponse sendEmail(String templateName, List<String> recipients, String objectId) {
        List<Id> successfulSendResults = new List<Id>();
        List<Id> failedSendResults = new List<Id>();
		EmailResponse response = new EmailResponse(templateName, objectId, recipients);
        
        List<OrgWideEmailAddress> securityAddress = [select Id from OrgWideEmailAddress where Address = 'security@salesforce.com'];
        Id templateId = getEmailTemplate(templateName);
        if(templateId == Null){
            response.Errored('ERRORED GETTING TEMPLATE');
            return response;
        }
        
        List<Id> contactList = getContacts(recipients);
        if(contactList.size() < 1){
            response.Errored('NO CONTACTS FOUND');
            return response;
        }
        
        for(Id c : contactList){
            sendContactEmail(securityAddress[0].Id, c, Id.valueOf(objectId), templateId, response);
        }
        
        return response;

   }
}
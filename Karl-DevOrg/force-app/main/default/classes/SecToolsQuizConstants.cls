public class SecToolsQuizConstants {
    public PageReference register;
    public PageReference quizDone;
    public PageReference quizQuestion;
    public final Integer quizValidDays = 7;
    public final Integer quizSignupWaitPeriodDays = 1; // wait between signing up again after completing a quiz
    public final Boolean oneValidUserPerEmail = True;
    public final Integer demoQuestionCount = 14;
    public final Integer defaultQuestionCount = 10;
    public String protoPrefix = 'https://';
    public final Id ORG_WIDE_EMAIL_ADDRESS_ID = '0D2300000008OqS';
    public final Pattern [] EmailBlacklist;
    public final Pattern [] IPBlacklist;
    
    {
      EmailBlacklist = new List<Pattern>();
      
      // These are really broad but I can't keep whack-a-mole'ing 
      // all of these throw-aways
      EmailBlacklist.add(Pattern.compile('.*email.net$'));
      EmailBlacklist.add(Pattern.compile('.*email.com$'));
      
      EmailBlacklist.add(Pattern.compile('.*minutemail\\.com$')); // there are several of these
      EmailBlacklist.add(Pattern.compile('.*temporaryemail.net$'));
      EmailBlacklist.add(Pattern.compile('.*mintemail\\.com$'));
      EmailBlacklist.add(Pattern.compile('.*@pjjkp\\.com$'));
      EmailBlacklist.add(Pattern.compile('.*@mailinator\\.com$'));
      EmailBlacklist.add(Pattern.compile('.*@guerrillamailblock\\.com$'));
      EmailBlacklist.add(Pattern.compile('.*@noclickemail\\.com$'));
      EmailBlacklist.add(Pattern.compile('.*@yopmail\\.com$'));
      EmailBlacklist.add(Pattern.compile('.*@tempemail\\.co\\.za$'));
      EmailBlacklist.add(Pattern.compile('.*trashmail\\.com$'));
      EmailBlacklist.add(Pattern.compile('.*@mailmetrash\\.com$'));
      EmailBlacklist.add(Pattern.compile('.*@disposableinbox.com$'));
      
      IPBlacklist = new List<Pattern>();
      IPBlacklist.add(Pattern.compile('^200\\.40\\.185\\..*'));
      
      // 1/31/2011, 200.40.249.138, r200-40-249-138.ae-static.anteldata.net.uy
      IPBlacklist.add(Pattern.compile('^200\\.40\\.249\\..*'));
      
      // 1/31/2011, 200.40.111.26, r200-40-111-26.ae-static.anteldata.net.uy
      IPBlacklist.add(Pattern.compile('^200\\.40\\.111\\..*'));
      IPBlacklist.add(Pattern.compile('89\\.253\\.113\\.228'));
      IPBlacklist.add(Pattern.compile('200\\.40\\.81\\..*'));
      // 1/29/2011 from 190.134.55.152, r190-134-55-152.dialup.adsl.anteldata.net.uy
      IPBlacklist.add(Pattern.compile('190\\.134\\.55\\..*'));
      
      // 02/01/2001, 184.154.132.82, go.vfserver.com (hosting company)
      IPBlacklist.add(Pattern.compile('184\\.154\\.132\\..*'));
      
      // SFDC - test only
      //IPBlacklist.add(Pattern.compile('204.14\\.239\\.217'));
      //IPBlacklist.add(Pattern.compile('204\\.14\\.239\\..*'));
    }
    
    public SecToolsQuizConstants() {
        String protoPrefix = 'https://'; // Damn it! How do you figure out if the req is secure?
        String mysite = Site.getCurrentSiteUrl();
        if ((mysite != null) && (mysite != '')) {
            if (mysite.endsWith('/')) {
                mysite = mysite.substring(0,mysite.length()-1);
            }
            register = new PageReference(mysite+Page.Security_Tools_Forcecom_Quiz_Register.getUrl());
            quizDone = new PageReference(mysite+Page.Security_Tools_Forcecom_Quiz_Scorecard.getUrl());
            quizQuestion = new PageReference(mysite+Page.Security_Tools_Forcecom_Quiz_Question.getUrl());
        }
        else {
            register = new PageReference(protoPrefix+ApexPages.currentPage().getHeaders().get('Host')+Page.Security_Tools_Forcecom_Quiz_Register.getUrl());
            quizDone = new PageReference(protoPrefix+ApexPages.currentPage().getHeaders().get('Host')+Page.Security_Tools_Forcecom_Quiz_Scorecard.getUrl());
            quizQuestion = new PageReference(protoPrefix+ApexPages.currentPage().getHeaders().get('Host')+Page.Security_Tools_Forcecom_Quiz_Question.getUrl());
        }
    }
    private String escape(String a) {
        return a.replace('&','&').replace('"','"').replace('<','<').replace('>','>');
    }
    
    public Boolean isBlacklistedEmail(String email) {
        for (Pattern p : EmailBlacklist) {
            if (p.matcher(email).matches()) {
                return True;
            }
        }
        return False;
    }
    
    public Boolean isBlacklistedIP(String IP) {
        
        // I believe this could also look like 
        // 1.2.3.4, 2.3.4.5, 3.4.5.6
        String [] ips = IP.split(',');
        for (String i : ips) {
            i = i.trim();
            for (Pattern p : IPBlacklist) {
                if (p.matcher(i).matches()) {
                    return True;
                }
            }
        }
        return False;
    }
    
    public String emailBody(QuizUser__c user,Boolean useHtml) {
        

        String s = 'Hello '+escape(user.Name)+',';
        if (useHtml = True) { s+= '<br /><br />'; } else { s+= '\n\n';}
        s += 'Thank you for registering for the salesforce.com Platform Security ';
        s += 'Quiz.  Please use the below link to access the quiz.  Note that the ';
        s += 'quiz and results will only be available for the next ';
        s += String.valueOf(quizValidDays)+' days.';
        if (useHtml = True) { s+= '<br /><br />'; } else { s+= '\n\n';}
        if (useHtml = True) {
            s+= '<a href="'+quizQuestion.getUrl();
            s+= '?sId='+user.Key__c+'">';
            s+= quizQuestion.getUrl();
            s+= '?sId='+user.Key__c+'</a><br /><br />';
        }
        else {
            s+= quizQuestion.getUrl();
            s+= '?sId='+user.Key__c+'\n\n';
        }
        s+= 'Sincerely,';
        if (useHtml = True) { s+= '<br />'; } else { s+= '\n';}
        s+= 'Force.com Cloud Security Team';
        return s;
    }
}
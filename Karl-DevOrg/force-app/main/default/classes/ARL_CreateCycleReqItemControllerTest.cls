/**
* @author Mahima Aggarwal
* @email mahima.aggarwal@mtxb2b.com
* @description Test class for CycleRequestItemTrigger
*/
@isTest
public class ARL_CreateCycleReqItemControllerTest {  
    
    /**
* @author Lakshya Arora
* @email lakshya.arora@mtxb2b.com
* @description Create user for test class
*/
    
    @testSetup
    public static void createUser(){
        User opsAdminUser = ARL_TestDataFactory.createOpsAdmin();
    }
      /**
* @author Lakshya Arora
* @email lakshya.arora@mtxb2b.com
* @description test functionality of errorMessageTest
*/
    @isTest
    private static void errorMessageTest(){
        List<User> opsAdminUserList = ARL_TestDataFactory.fetchUser('abc@b.com.d');
        if(!opsAdminUserList.isEmpty()){
            System.runAs(opsAdminUserList[0]){
                Request_Item__c reqItem_1 = ARL_TestDataFactory.createRequestItem();
                reqItem_1.Request_Name__c = 'Request 1';
                reqItem_1.Areas_of_Compliance__c  = 'HITRUST;FedRAMP Moderate';
                reqItem_1.Primary_Scope__c = 'HK';
                insert reqItem_1;
                
                Request_Item__c reqItem_2 = ARL_TestDataFactory.createRequestItem();
                reqItem_2.Request_Name__c = 'Request 2';
                reqItem_2.Areas_of_Compliance__c  = 'SOC 2';
                reqItem_2.Primary_Scope__c = 'HK';
                insert reqItem_2;
                
                // Create a Control Scope for Heroku
                Control_Scope__c cs = ARL_TestDataFactory.createControlScope();
                cs.Scope_Name__c  = 'HK';
                insert cs;
                
                // Create a Control Test for the new Control Scope
                Control_Test__c ct = ARL_TestDataFactory.createControlTest(cs.id);
                insert ct;
                
                // Map the Control Test to the Request Item via Junction
                Request_Item_Control__c ric_1 = ARL_TestDataFactory.createRequestItemControl(reqItem_1.id,ct.Id);
                insert ric_1;
                
                Request_Item_Control__c ric_2 = ARL_TestDataFactory.createRequestItemControl(reqItem_2.id,ct.Id);
                insert ric_2;
                
                KARL_Audit_Team__c auditTeam = ARL_TestDataFactory.createAuditTeam('test');
                insert auditTeam;
                
                // Create an Audit Cycle
                Audit_Cycle__c auditCycle = ARL_TestDataFactory.createAuditCycle();
                insert auditCycle;
                
                
                
                Audit_Cycle__c auditCycle1 = [SELECT id FROM Audit_Cycle__c LIMIT 1];
                ARL_CreateCycleReqItemController.createCycleRequestItems(auditCycle1.Id);
            }
        }
    }
    
    /**
* @author Mahima Aggarwal
* @email mahima.aggarwal@mtxb2b.com
* @description Test method to check auti generated Cycle Request Items from Audit Cycle.
*/
    @isTest
    public static void testCreateCycleRequestItems(){
        List<User> opsAdminUserList = ARL_TestDataFactory.fetchUser('abc@b.com.d');
        if(!opsAdminUserList.isEmpty()){
            System.runAs(opsAdminUserList[0]){
                // Create two Request Items
                Request_Item__c reqItem_1 = ARL_TestDataFactory.createRequestItem();
                reqItem_1.Request_Name__c = 'Request 1';
                reqItem_1.Areas_of_Compliance__c  = 'HITRUST;FedRAMP Moderate';
                reqItem_1.Primary_Scope__c = 'HK';
                insert reqItem_1;
                
                Request_Item__c reqItem_2 = ARL_TestDataFactory.createRequestItem();
                reqItem_2.Request_Name__c = 'Request 2';
                reqItem_2.Areas_of_Compliance__c  = 'SOC 2';
                reqItem_2.Primary_Scope__c = 'HK';
                insert reqItem_2;
                
                // Create a Control Scope for Heroku
                Control_Scope__c cs = ARL_TestDataFactory.createControlScope();
                cs.Scope_Name__c  = 'HK';
                insert cs;
                
                // Create a Control Test for the new Control Scope
                Control_Test__c ct = ARL_TestDataFactory.createControlTest(cs.id);
                insert ct;
                
                // Map the Control Test to the Request Item via Junction
                Request_Item_Control__c ric_1 = ARL_TestDataFactory.createRequestItemControl(reqItem_1.id,ct.Id);
                insert ric_1;
                
                Request_Item_Control__c ric_2 = ARL_TestDataFactory.createRequestItemControl(reqItem_2.id,ct.Id);
                insert ric_2;
                
                KARL_Audit_Team__c auditTeam = ARL_TestDataFactory.createAuditTeam('test');
                insert auditTeam;
                
                // Create an Audit Cycle
                Audit_Cycle__c auditCycle = ARL_TestDataFactory.createAuditCycle();
                insert auditCycle;
                
                // Create 3 different Scope Coverages for the the new Audit Cycle
                Audit_Cycle_Coverage__c acc = ARL_TestDataFactory.createAuditCycleCoverage(auditCycle.Id);
                acc.Area__c = 'HITRUST';
                acc.Scope__c  = 'HK';
                acc.KARL_Audit_Team__c = auditTeam.Id;
                insert acc;
                
                Audit_Cycle_Coverage__c acc2 = ARL_TestDataFactory.createAuditCycleCoverage(auditCycle.Id);
                acc2.Area__c = 'FedRAMP Moderate';
                acc2.Scope__c  = 'HK';
                acc2.KARL_Audit_Team__c = auditTeam.Id;
                insert acc2;
                
                Audit_Cycle_Coverage__c acc3 = ARL_TestDataFactory.createAuditCycleCoverage(auditCycle.Id);
                acc3.Area__c = 'FedRAMP High';
                acc3.Scope__c  = 'HK';
                acc3.KARL_Audit_Team__c = auditTeam.Id;
                insert acc3;
                
                Audit_Cycle_Coverage__c acc4 = ARL_TestDataFactory.createAuditCycleCoverage(auditCycle.Id);
                acc4.Area__c = 'SOC 2';
                acc4.Scope__c  = 'HK';
                acc4.KARL_Audit_Team__c = auditTeam.Id;
                insert acc4;
                
                
                Audit_Cycle__c auditCycle1 = [SELECT id FROM Audit_Cycle__c LIMIT 1];
                ARL_CreateCycleReqItemController.createCycleRequestItems(auditCycle1.Id);
                
                List<Cycle_Request_Item__c> cycleReqItems = [SELECT id FROM Cycle_Request_Item__c];
                
                //Expecting 2 cycle request items as it has 2 Request Item Control
                system.assertEquals(2, cycleReqItems.size(), 'Incorrect number of cycle request items created');
            }
        }
    }
       /**
* @author Lakshya Arora
* @email lakshya.arora@mtxb2b.com
* @description test functionality of duplicate Items
*/
    @isTest
    public static void duplicateItemChecks(){
        List<User> opsAdminUserList = ARL_TestDataFactory.fetchUser('abc@b.com.d');
        if(!opsAdminUserList.isEmpty()){
            System.runAs(opsAdminUserList[0]){
                // Create two Request Items
                Request_Item__c reqItem_1 = ARL_TestDataFactory.createRequestItem();
                reqItem_1.Request_Name__c = 'Request 1';
                reqItem_1.Areas_of_Compliance__c  = 'HITRUST;FedRAMP Moderate';
                reqItem_1.Primary_Scope__c = 'HK';
                insert reqItem_1;
                
                Request_Item__c reqItem_2 = ARL_TestDataFactory.createRequestItem();
                reqItem_2.Request_Name__c = 'Request 2';
                reqItem_2.Areas_of_Compliance__c  = 'SOC 2';
                reqItem_2.Primary_Scope__c = 'HK';
                insert reqItem_2;
                
                // Create a Control Scope for Heroku
                Control_Scope__c cs = ARL_TestDataFactory.createControlScope();
                cs.Scope_Name__c  = 'HK';
                insert cs;
                
                // Create a Control Test for the new Control Scope
                Control_Test__c ct = ARL_TestDataFactory.createControlTest(cs.id);
                insert ct;
                
                // Map the Control Test to the Request Item via Junction
                Request_Item_Control__c ric_1 = ARL_TestDataFactory.createRequestItemControl(reqItem_1.id,ct.Id);
                insert ric_1;
                
                Request_Item_Control__c ric_2 = ARL_TestDataFactory.createRequestItemControl(reqItem_2.id,ct.Id);
                insert ric_2;
                
                KARL_Audit_Team__c auditTeam = ARL_TestDataFactory.createAuditTeam('test');
                insert auditTeam;
                
                // Create an Audit Cycle
                Audit_Cycle__c auditCycle = ARL_TestDataFactory.createAuditCycle();
                insert auditCycle;
                
                // Create 3 different Scope Coverages for the the new Audit Cycle
                Audit_Cycle_Coverage__c acc = ARL_TestDataFactory.createAuditCycleCoverage(auditCycle.Id);
                acc.Area__c = 'HITRUST';
                acc.Scope__c  = 'HK';
                acc.KARL_Audit_Team__c = auditTeam.Id;
                insert acc;
                
                Audit_Cycle_Coverage__c acc2 = ARL_TestDataFactory.createAuditCycleCoverage(auditCycle.Id);
                acc2.Area__c = 'FedRAMP Moderate';
                acc2.Scope__c  = 'HK';
                acc2.KARL_Audit_Team__c = auditTeam.Id;
                insert acc2;
                
                Audit_Cycle_Coverage__c acc3 = ARL_TestDataFactory.createAuditCycleCoverage(auditCycle.Id);
                acc3.Area__c = 'FedRAMP High';
                acc3.Scope__c  = 'HK';
                acc3.KARL_Audit_Team__c = auditTeam.Id;
                insert acc3;
                
                Audit_Cycle_Coverage__c acc4 = ARL_TestDataFactory.createAuditCycleCoverage(auditCycle.Id);
                acc4.Area__c = 'SOC 2';
                acc4.Scope__c  = 'HK';
                acc4.KARL_Audit_Team__c = auditTeam.Id;
                insert acc4;
                
                
                Audit_Cycle__c auditCycle1 = [SELECT id FROM Audit_Cycle__c LIMIT 1];
                ARL_CreateCycleReqItemController.createCycleRequestItems(auditCycle1.Id);
                
                List<Cycle_Request_Item__c> cycleReqItems = [SELECT id FROM Cycle_Request_Item__c];
                
                //Expecting 2 cycle request items as it has 2 Request Item Control
                system.assertEquals(2, cycleReqItems.size(), 'Incorrect number of cycle request items created');
                
                ARL_CreateCycleReqItemController.createCycleRequestItems(auditCycle1.Id);
            }
        }
    }
}
/*
Developer: Jorge Caceres <jcaceres@salesforce.com>
Description:
    Place validations here and call from trigger
*/
public class SC_TrainingCourseValidations {
    public static void validatePopulateOrgWideEmail(List<Training_Course__c> newList, Map<Id, Training_Course__c> oldMap) {
       // build map of all related org wide
       Map<String, Id> orgWideIdsByNameMap = new Map<String, Id>();
       Set<String> orgWideNames = new Set<String>();
       for (Training_Course__c tc : newList) {
            if (String.isNotBlank(tc.Org_Wide_Email_Name__c)) {
                orgWideNames.add(tc.Org_Wide_Email_Name__c);    
            }
        }
        if (!orgWideNames.isEmpty()) {
            for (OrgWideEmailAddress ow : [SELECT Id, DisplayName FROM OrgWideEmailAddress 
                                           WHERE DisplayName IN :orgWideNames]) {
                orgWideIdsByNameMap.put(ow.DisplayName, ow.ID);                                    
            }
        }
        
       // validate all against map
       for (Training_Course__c tc : newList) {
           if (String.isNotBlank(tc.Org_Wide_Email_Name__c) && !orgWideIdsByNameMap.containsKey(tc.Org_Wide_Email_Name__c)) {
                tc.Org_Wide_Email_Name__c.addError('Name not found in Organization Wide Email Address Settings for the org.');   
           }
           if (orgWideIdsByNameMap.containsKey(tc.Org_Wide_Email_Name__c)) {
                tc.Org_Wide_Email_Id__c = orgWideIdsByNameMap.get(tc.Org_Wide_Email_Name__c);   
           } else {
                tc.Org_Wide_Email_Id__c = null;              
           }           
        }
        
    }
}
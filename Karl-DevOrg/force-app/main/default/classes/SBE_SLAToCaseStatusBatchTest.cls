@isTest
public class SBE_SLAToCaseStatusBatchTest {
    testmethod static void testEnrollment(){
        Account acct = new Account();
        acct.Name = 'salesforce.com - ESA Office Hours';
        insert acct;
        
        Contact contact = new Contact();
        contact.LastName = 'Test'; 
        contact.Employee_ID__c = 'MTX101';
        contact.AccountId = Label.ESA_Office_Hours_Account_ID;
        INSERT contact;
        
        Case newcase = new Case();
        newcase.RecordTypeId = IEM_Constants.IEM_CASERECORDTYPEID;
        newcase.Cloud__c = 'EntSecTools';
        newcase.Description = 'Test';
        newcase.Team_Responsible__c = 'TRUST Enterprise Security Tools Development';
        newcase.Subject = 'Test Subject';
        newCase.SLA_Extension_Id__c = 'EX101';
        newCase.Status = IEM_Constants.IEM_CASE_STATUS_INTERNAL_REVIEW;
        INSERT newcase;
        System.debug('##newCase##'+[Select Id, Status From Case Where id = :newCase.Id]);
       //List<Milestone__c> milestoneList = IEM_TestDataFactory.createMilestoneRecords(1,newcase.id,true);
              
        TM_Security_Work_SLA_Extension_c__x workSLA = new TM_Security_Work_SLA_Extension_c__x();
        //workSLA.TM_Work_Subject_c__c = 'Test Subject';
        workSLA.ExternalId= 'EX101';
        workSLA.TM_Definition_of_Done_c__c= 'Completed';
        workSLA.Describe_Technical_Work_to_Remediate__c= 'Nothing';
        //workSLA.Due_Date_Justification__c= 'No';
        workSLA.Technical_Description_of_Issue__c= 'No';
        workSLA.TM_SLA_Extension_Status_c__c = IEM_Constants.SBE_STATUS_DENIED;
        //workSLA.TM_Work_Cloud_c__c = 'Very High (EVP)';
        
        
        SBE_SLAToCaseStatusBatch.mockallSBEList.add(workSLA);
        SBE_SLAExtensionToCase.mockGUSUserEmpMap.put('MTX101', 'EX101');
      
        Test.startTest();
       	//SchedulableContext sc = null;
        SBE_SLAToCaseStatusBatch IEMGus = new SBE_SLAToCaseStatusBatch();
        Database.executeBatch(IEMGus);
        Test.stopTest();
       List<Case> caseList = [Select Id from Case Where Status = : IEM_Constants.IEM_CASE_STATUS_CLOSED];
        System.assertEquals(1, caseList.size());
    }
}
/**
 * @author Lakshya Arora
 * @email lakshya.arora@mtxb2b.com
 * @description Class for unit testing the KARL_CustomLookupController
 */
@isTest
public with sharing class KARL_CustomLookupControllerTest {
    /**
     * @author Lakshya Arora
     * @email lakshya.arora@mtxb2b.com
     * @description Method will unit test the getRecordsAsPerObjectAndFieldList Fucntionality 
     * by validating the correct number of records
     */
    @isTest
    private static void getRecordsAsPerObjectAndFieldListTest(){
        User opsAdminUser = ARL_TestDataFactory.createOpsAdmin();
        System.runAs(opsAdminUser){
            Request_Item__c reqItemObj = ARL_TestDataFactory.createRequestItem();
            insert reqItemObj;
            List<KARL_CustomLookupController.RecordsData> recordsDataList = KARL_CustomLookupController.getRecordsAsPerObjectAndFieldList('Request_Item__c','Name','REQ','Primary_Scope__c');
            System.assert(!recordsDataList.isEmpty());
            System.assertEquals(recordsDataList[0].secondarylabel,'CS');
        }
    }
}
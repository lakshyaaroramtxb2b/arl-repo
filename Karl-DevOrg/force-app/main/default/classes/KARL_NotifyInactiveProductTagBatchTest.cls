/**
* @author Lakshya Arora
* @email lakshya.arora@mtxb2b.com
* @description Test class for KARL_NotifyInactiveProductTagBatch
*/
@isTest
public class KARL_NotifyInactiveProductTagBatchTest {
    /**
* @author Lakshya Arora
* @email lakshya.arora@mtxb2b.com
* @desc method to test the notifyInactiveProductBatch functionality.
*/
    @istest
    private static void notifyInactiveProductBatchTest(){ 
        final String  SALESFORCEEMPLOYEERECORDTYPEID = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Salesforce_Employee').getRecordTypeId();
        User opsAdminUser = ARL_TestDataFactory.createOpsAdmin();
        System.runAs(opsAdminUser){
            Account acc = new Account(Name = 'salesforce.com');
            insert acc;
            
            Contact con = new Contact(LastName = 'test', AccountId = acc.Id,Email = 'test@gmail.com.test',RecordTypeId = SALESFORCEEMPLOYEERECORDTYPEID);
            insert con;
            
            Contact con2 = new Contact(LastName = 'test2', AccountId = acc.Id,Email = 'test2@gmail.com.test',RecordTypeId = SALESFORCEEMPLOYEERECORDTYPEID);
            insert con2;
            
            Karl_Gus_product_Tag__c karGusProdTagInactive = new Karl_Gus_product_Tag__c();
            karGusProdTagInactive.Product_Tag_External_Id__c = 'INAC123';
            karGusProdTagInactive.Active__c = False;
            karGusProdTagInactive.Active_Scrum_Team__c = true;
            insert karGusProdTagInactive;
            
            Karl_Gus_product_Tag__c karGusProdTagActive= new Karl_Gus_product_Tag__c();
            karGusProdTagActive.Product_Tag_External_Id__c = 'ACT123';
            karGusProdTagActive.Active__c = true;
            karGusProdTagActive.Active_Scrum_Team__c = true;
            insert karGusProdTagActive;
            
            Audit_Cycle__c auditCycle = ARL_TestDataFactory.createAuditCycle();
            insert auditCycle;
            Request_Item__c reqItem = ARL_TestDataFactory.createRequestItem();
            reqItem.Primary_Scope__c = 'SS';
            reqItem.KARL_Evidence_Submission_Workflow__c = true;
            reqItem.Create_GUS_Case__c = false;
            reqItem.Suggested_Product_Tag__c = karGusProdTagInactive.Id;
            reqItem.KARL_GRC_Assignee__c = con.Id;
            insert reqItem;
            Test.startTest();
            Database.executeBatch(new KARL_NotifyInactiveProductTagBatch(),200);
            Test.stopTest();
            System.assert([SELECT id,Inactive_Product_Tag__c FROM Request_Item__c WHERE KARL_GRC_Assignee__c =: con.Id][0].Inactive_Product_Tag__c);
            System.assert([SELECT id,Suggested_Product_Tag__c FROM Request_Item__c WHERE KARL_GRC_Assignee__c =: con.Id][0].Suggested_Product_Tag__c == karGusProdTagActive.Id);
        } 
    }
}
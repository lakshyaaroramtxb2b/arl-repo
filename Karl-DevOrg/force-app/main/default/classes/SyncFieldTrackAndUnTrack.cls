/**
 * Track the Secuirty Answers to InTake Requests
 * @author suresh.uppala
 */
global without sharing class SyncFieldTrackAndUnTrack implements Schedulable,
                                                       Database.Batchable<sObject>,
                                                       Database.AllowsCallouts,
                                                       Database.Stateful {

    private static final String HOURLY = '0 0 0/1 1/1 * ? *'; //every hour
    private static final String SOURCE_FILE = 'SyncFieldTrackAndUnTrack';

    global List<ESA_Security_Request__c> lstInTakeRequests;
    global List<ESA_Service_Item_Question__c> lstSitemQuestions;
    global Map<Id,ESA_Service_Item__c> mapServiceItems;
    global Boolean errorFlag = false;
    
    private String fullErrorText;
                                                                 
    public SyncFieldTrackAndUnTrack() {

    }

    public static String scheduleJob() {
        return scheduleJob(HOURLY);
    }

    public static Id scheduleJob(String cronExpression) {
        return scheduleJob(cronExpression, null);
    }
    
    public static void run() {
        datetime thisTime = system.now().addSeconds(15);
        integer minute = thisTime.minute();
        integer second = thisTime.second();
        integer hour = thisTime.hour();
        integer year = thisTime.year();
        integer month = thisTime.month();
        integer day = thisTime.day();
    
        String cronExpression = second + ' ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ? ' + year; 
        scheduleJob(cronExpression, null); 
         
    }

    public static Id scheduleJob(String cronExpression, String eventName) {
        String jobName = SOURCE_FILE + ' ' + System.now();
        String jobId = System.schedule(jobName, cronExpression, new SyncFieldTrackAndUnTrack());
        return jobId;
    }

    global void execute(SchedulableContext sc) {
        Database.executeBatch(this);
    }

    global List<ESA_Security_Request__c> start(Database.BatchableContext context){
        
        lstSitemQuestions = new List<ESA_Service_Item_Question__c>();
        lstInTakeRequests = new List<ESA_Security_Request__c>();
        Set<Id> setSItems = new Set<Id>();
        
        try{
                lstSitemQuestions = [SELECT Id,
                                            ESA_Service_Item__c
                                     FROM ESA_Service_Item_Question__c
                                     WHERE Tracked_Operation_Pending__c != null];
                
                for(ESA_Service_Item_Question__c sItemQues : lstSitemQuestions) {
                    setSItems.add(sItemQues.ESA_Service_Item__c);
                }

                List<ESA_Service_Item__c> lstSItems = [SELECT Id,
                                                              (SELECT Track_At_Request_Level__c,
                                                                      Tracking_Field__c,
                                                                      ESA_Security_Question__c
                                                               FROM ESA_Service_Item_Questions__r
                                                               WHERE Tracked_Operation_Pending__c != null)  
                                                       FROM ESA_Service_Item__c
                                                       WHERE Id IN : setSItems];

                mapServiceItems = new Map<Id,ESA_Service_Item__c>();

                for(ESA_Service_Item__c sItem : lstSItems) {
                    mapServiceItems.put(sItem.Id,sItem);
                }

                lstInTakeRequests = [SELECT Id,
                                            ESA_Service_Item__c,
                                            (SELECT Answer__c,
                                                    ESA_Security_Question__c,
                                                    ESA_Security_Question__r.Data_Type__c
                                             FROM ESA_Security_Answers__r)
                                     FROM ESA_Security_Request__c
                                     WHERE ESA_Service_Item__c IN : setSItems];

                return lstInTakeRequests;
            }catch(Exception ex){
                errorFlag = true;
                return null;
            }
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        List<ESA_Security_Request__c> listInTakeRequests = new List<ESA_Security_Request__c>();
         
        try {
                Map<Id,ESA_Service_Item_Question__c> mapServiceItemQuestions = new Map<Id,ESA_Service_Item_Question__c>();
                ESA_Service_Item_Question__c esaSIQuestion;
                List<ESA_Service_Item_Question__c> lstEsaSIQuestions = new List<ESA_Service_Item_Question__c>();
                List<ESA_Security_Answer__c> lstAnswers;
                
                for(Sobject sObj : scope) {
                    listInTakeRequests.add((ESA_Security_Request__c)sObj);
                }
                
                if(listInTakeRequests.size() == 0) {
                    errorFlag = true;
                    return;
                }

                for(ESA_Security_Request__c inTakeRequest : listInTakeRequests) {

                    lstSitemQuestions = mapServiceItems.get(inTakeRequest.ESA_Service_Item__c).ESA_Service_Item_Questions__r;

                    if(lstSitemQuestions.size() >= 0){

                        lstAnswers = inTakeRequest.ESA_Security_Answers__r;
                        mapServiceItemQuestions = new Map<Id,ESA_Service_Item_Question__c>();
                        
                        for(ESA_Service_Item_Question__c sIQuestion : lstSitemQuestions) {
                            mapServiceItemQuestions.put(sIQuestion.ESA_Security_Question__c,sIQuestion);
                        }
                        
                        for(ESA_Security_Answer__c eAnswer : lstAnswers) {

                            esaSIQuestion = mapServiceItemQuestions.get(eAnswer.ESA_Security_Question__c);
                            
                            if(esaSIQuestion != null){
                                if(esaSIQuestion.Track_At_Request_Level__c == true) {
                                    if(String.isNotBlank(eAnswer.Answer__c)) {
                                        if(eAnswer.ESA_Security_Question__r.Data_Type__c == ESA_AppConstants.TYPE_NUMBER) {
                                            inTakeRequest.put(esaSIQuestion.Tracking_Field__c,Double.ValueOf(eAnswer.Answer__c));
                                        }else if(eAnswer.ESA_Security_Question__r.Data_Type__c == ESA_AppConstants.TYPE_DATE) {
                                            inTakeRequest.put(esaSIQuestion.Tracking_Field__c,Date.ValueOf(eAnswer.Answer__c));
                                        }else {
                                            inTakeRequest.put(esaSIQuestion.Tracking_Field__c,eAnswer.Answer__c.abbreviate(245));
                                        }
                                    }
                                }else {
                                    if(esaSIQuestion.Tracking_Field__c != null) {
                                        inTakeRequest.put(esaSIQuestion.Tracking_Field__c, null);
                                    }
                                    
                                }
                            }
                        }
                    }
                }
                
                if(listInTakeRequests.size() > 0) {
                    update listInTakeRequests;
                }
                
                
            }catch(Exception exc) {
                errorFlag = true;
                processException(exc);
            }
    }

    global void finish(Database.BatchableContext BC){
        if(errorFlag == false) {
            List<ESA_Service_Item_Question__c> lstEsaSIQuestions;
            lstEsaSIQuestions =  [SELECT Id,
                                             Tracking_Field__c,
                                             Track_At_Request_Level__c,
                                             Tracked_Operation_Pending__c
                                      FROM ESA_Service_Item_Question__c
                                      WHERE Tracked_Operation_Pending__c != null];

                for(ESA_Service_Item_Question__c esaQuestion : lstEsaSIQuestions) {
                    if(esaQuestion.Track_At_Request_Level__c == false) {
                       esaQuestion.Tracking_Field__c = null;
                    }
                    esaQuestion.Tracked_Operation_Pending__c = null;
                }
                if(lstEsaSIQuestions.size() > 0) {
                    update lstEsaSIQuestions;
                }
        }
    }

    private void processException(Exception exc) {
        Esa_DebugService.writeMessage('SyncFieldTrackAndUnTrack.finish: Failed with errors');
        if (exc instanceof DMLException) {
            String errorString = '';
            Integer numErrors = exc.getNumDml();
            fullErrorText = 'There were ' + numErrors + ' DML errors: \n';

            Esa_DebugService.writeException(exc, 1, SOURCE_FILE, fullErrorText);
        } else {
            //we got an unexpected non-dmlexception, write it
            fullErrorText = exc.getMessage();
            Esa_DebugService.writeException(exc, 1, SOURCE_FILE, 'Unhandled exception');
        }
    }                                                         
}
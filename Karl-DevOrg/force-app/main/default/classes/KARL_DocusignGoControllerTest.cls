/**
* @author Lakshya Arora
* @email lakshya.arora@mtxb2b.com
* @description Test the  Docusign Go button functionalities
*/
@isTest
public with sharing class KARL_DocusignGoControllerTest {
    /**
* @author Lakshya Arora
* @email lakshya.arora@mtxb2b.com
* @description Create test data
*/
    @TestSetup
    private static void makeData(){
        User opsAdminUser = ARL_TestDataFactory.createOpsAdmin();
        PermissionSet pSet = [SELECT Name FROM PermissionSet WHERE NAME = 'Docusign_Administrator'];
        insert new PermissionSetAssignment(AssigneeId = opsAdminUser.id, PermissionSetId = pSet.Id);
        System.runAs(opsAdminUser){
            Account acc = ARL_TestDataFactory.createAccount('salesforce.com - ESA Office Hours');
            insert acc;
            Contact con = ARL_TestDataFactory.createContact(acc.Id,'test','test@gmail.com.invalid');
            con.recordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Salesforce Employee').getRecordTypeId();
            insert con;
            Audit_Cycle__c auditCycle = ARL_TestDataFactory.createAuditCycle();
            insert auditCycle;
            KARL_Audit_Scope_Master_Data__c auditScope = ARL_TestDataFactory.createAuditScope();
            insert auditScope;
            KARL_Audit_Firm__c auditFirm = ARL_TestDataFactory.createAuditFirm('Test Audit Firm');
            insert auditFirm;
            KARL_Audit_Scope_Reports__c auditScopeReport = ARL_TestDataFactory.createAuditScopeReport(auditScope.Id,auditFirm.Id);
            insert auditScopeReport;
            KARL_Cycle_Audit_Report_Items__c  cycleAuditReportObj = ARL_TestDataFactory.createCycleAuditScopeReport(auditScopeReport.Id,auditCycle.Id);
            cycleAuditReportObj.Status__c = KARL_Constants.REPORT_STATUS_READY_TO_SIGN;
            cycleAuditReportObj.Auditor_Readiness_Confirmation__c = true;
            cycleAuditReportObj.Management_Responses_Approved__c = true;
            cycleAuditReportObj.Audit_Report_Final_Draft_Confirmed__c = true;
            cycleAuditReportObj.Approved_by_Legal__c = true;
            cycleAuditReportObj.Audit_Letter_Temps_Formatted__c = true;
            cycleAuditReportObj.Basis_of_Assertion_BOA__c = true;
            insert cycleAuditReportObj;
            
            KARL_Audit_Report_Finalization_Config__c finalizationConfigObj = ARL_TestDataFactory.createAuditFinalizationConfiguration(cycleAuditReportObj.Id,auditFirm.Id,null);
            insert finalizationConfigObj;
            KARL_Audit_Signature_Templates__c signatureTemplateObj = ARL_TestDataFactory.createAuditSignatureTemplate(finalizationConfigObj.Id,String.valueOf(dfsle.UUID.randomUUID()),'Sample Template');
            insert signatureTemplateObj;
            KARL_Audit_Finalization_Signatories__c signatoryObj = ARL_TestDataFactory.createAuditFinalizationSignatory(con.Id,cycleAuditReportObj.Id);
            insert signatoryObj;
        }
    }
    
    /**
* @author Lakshya Arora
* @email lakshya.arora@mtxb2b.com
* @description Test send docusign functionality
*/
    @isTest
    private static void sendDocusignDocumentsTest(){
        List<User> opsAdminUserList = ARL_TestDataFactory.fetchUser('abc@b.com.d');
        if(!opsAdminUserList.isEmpty()){
            System.runAs(opsAdminUserList[0]){
                List<KARL_Cycle_Audit_Report_Items__c> cycleAuditReportItemList = [SELECT Id FROM KARL_Cycle_Audit_Report_Items__c];
                Test.startTest();
                if(!cycleAuditReportItemList.isEmpty())
                    KARL_DocuSignGoController.sendDocusignDocuments(cycleAuditReportItemList[0].Id);
                Test.stopTest();
            }
        }
    }
}
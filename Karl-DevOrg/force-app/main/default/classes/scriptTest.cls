public class scriptTest {

public static void empLadderTest() {
List<Integ_employee__c> scope = [SELECT Id,Name,INTEG_Manager__c,INTEG_Manager_ID__c,
                                 INTEG_Org62UserId__c,INTEG_Employee_ID__c,
                                 INTEG_Manager_Level_1__c,INTEG_Manager_Level_10__c,
                                 INTEG_Manager_Level_2__c,INTEG_Manager_Level_3__c,
                                 INTEG_Manager_Level_4__c,INTEG_Manager_Level_5__c,
                                 INTEG_Manager_Level_6__c,INTEG_Manager_Level_7__c,
                                 INTEG_Manager_Level_8__c,INTEG_Manager_Level_9__c 
                                 FROM INTEG_Employee__c WHERE Id='0033A00002GMRpZ'];
try {
          String managerId, eId, Integ_Manager_Id;
          List<String> lstManagers;
          List<INTEG_Employee__c> lstEmployees =  new List<INTEG_Employee__c>();
          

          Map<String,String> mapEmployee1 = new Map<String,String>();
          Map<String,Id> mapEmployee2 = new Map<String,String>();
          Map<String,String> mapEmpNumbers = new Map<String,String>();
          Map<String,String> mapEmployeeNames = new Map<String,String>();

          Set<String> setManagerIds = new Set<String>();

          for(INTEG_Employee__c empl : scope) {
              
              lstEmployees.add(empl);
              eId = null;
              if(String.IsNotBlank(empl.INTEG_Manager_ID__c)) {
                   eId = empl.INTEG_Manager_ID__c;
              }
              mapEmployee1.put(empl.Id,eId);  
              mapEmployee2.put(empl.INTEG_Org62UserId__c,empl.Id);
              mapEmployeeNames.put(empl.INTEG_Org62UserId__c,empl.Name);
              mapEmpNumbers.put(empl.INTEG_Org62UserId__c,empl.INTEG_Employee_ID__c);
              setManagerIds.add(empl.INTEG_Manager_ID__c);   
          }
          
          Integer flag = 1;
          While(setManagerIds.size() > 0) {
            
            if(flag > 10) break;
            List<INTEG_Employee__c> lstEmpl = [SELECT Id,Name,INTEG_Manager_ID__c,
                                               INTEG_Org62UserId__c,INTEG_Employee_ID__c  
                                               FROM INTEG_Employee__c 
                                               WHERE INTEG_Org62UserId__c IN : setManagerIds 
                                               AND INTEG_Org62UserId__c != null];

            setManagerIds = new Set<String>();
            for(INTEG_Employee__c emp : lstEmpl) {
               eId = null;
               if(String.IsNotBlank(emp.INTEG_Manager_ID__c)) {
                   eId = emp.INTEG_Manager_ID__c;
               }
                
                mapEmployee1.put(emp.Id,eId);
                mapEmployee2.put(emp.INTEG_Org62UserId__c,emp.Id);
                mapEmployeeNames.put(emp.INTEG_Org62UserId__c,emp.Name);
                mapEmpNumbers.put(emp.INTEG_Org62UserId__c,emp.INTEG_Employee_ID__c);
                setManagerIds.add(emp.INTEG_Manager_ID__c); 
                if(eId == null) {
                  break;
                }
            }
            flag = flag + 1;
          }
          
          for(INTEG_Employee__c emp : lstEmployees) {
            emp.INTEG_Manager_Level_1__c = null;
            emp.INTEG_Manager_Level_2__c = null;
            emp.INTEG_Manager_Level_3__c = null;
            emp.INTEG_Manager_Level_4__c = null;
            emp.INTEG_Manager_Level_5__c = null;
            emp.INTEG_Manager_Level_6__c = null;
            emp.INTEG_Manager_Level_7__c = null;
            emp.INTEG_Manager_Level_8__c = null;
            emp.INTEG_Manager_Level_9__c = null;
            emp.INTEG_Manager_Level_10__c = null;
                
            lstManagers = new List<String>();
            String managerName = '';
            if(String.isNotBlank(emp.INTEG_Manager_ID__c)) {
                managerId = mapEmployee2.get(emp.INTEG_Manager_ID__c);
                emp.INTEG_Manager__c = managerId;
                managerName = mapEmployeeNames.get(emp.INTEG_Manager_ID__c);
                if(mapEmpNumbers.get(emp.INTEG_Manager_ID__c) != null) {
                  managerName = formatManager(managerName,mapEmpNumbers.get(emp.INTEG_Manager_ID__c));
                  //managerName = managerName + '(' + mapEmpNumbers.get(emp.INTEG_Manager_ID__c) + ')';
                }
                lstManagers.add(managerName);
                Integ_Manager_Id = mapEmployee1.get(managerId);
           
                if(String.isNotBlank(Integ_Manager_Id)) {
                    managerId = mapEmployee2.get(Integ_Manager_Id);
                    managerName = formatManager(mapEmployeeNames.get(Integ_Manager_Id),mapEmpNumbers.get(Integ_Manager_Id));
                    lstManagers.add(managerName);
                    Integ_Manager_Id = mapEmployee1.get(managerId);
                    
                    if(String.isNotBlank(Integ_Manager_Id)) {
                        managerId = mapEmployee2.get(Integ_Manager_Id);
                        managerName = formatManager(mapEmployeeNames.get(Integ_Manager_Id),mapEmpNumbers.get(Integ_Manager_Id));
                        lstManagers.add(managerName);
                        Integ_Manager_Id = mapEmployee1.get(managerId);
                        
                        if(String.isNotBlank(Integ_Manager_Id)) {
                            managerId = mapEmployee2.get(Integ_Manager_Id);
                            managerName = formatManager(mapEmployeeNames.get(Integ_Manager_Id),mapEmpNumbers.get(Integ_Manager_Id));
                            lstManagers.add(managerName);
                            Integ_Manager_Id = mapEmployee1.get(managerId);
                            
                            if(String.isNotBlank(Integ_Manager_Id)) {
                                managerId = mapEmployee2.get(Integ_Manager_Id);
                                managerName = formatManager(mapEmployeeNames.get(Integ_Manager_Id),mapEmpNumbers.get(Integ_Manager_Id));
                                lstManagers.add(managerName);
                                Integ_Manager_Id = mapEmployee1.get(managerId);
                                
                                if(String.isNotBlank(Integ_Manager_Id)) {
                                    managerId = mapEmployee2.get(Integ_Manager_Id);
                                    managerName = formatManager(mapEmployeeNames.get(Integ_Manager_Id),mapEmpNumbers.get(Integ_Manager_Id));
                                    lstManagers.add(managerName);
                                    Integ_Manager_Id = mapEmployee1.get(managerId);
                                    
                                    if(String.isNotBlank(Integ_Manager_Id)) {
                                        managerId = mapEmployee2.get(Integ_Manager_Id);
                                        managerName = formatManager(mapEmployeeNames.get(Integ_Manager_Id),mapEmpNumbers.get(Integ_Manager_Id));
                                        lstManagers.add(managerName);
                                        Integ_Manager_Id = mapEmployee1.get(managerId);
                                        
                                        if(String.isNotBlank(Integ_Manager_Id)) {
                                            managerId = mapEmployee2.get(Integ_Manager_Id);
                                            managerName = formatManager(mapEmployeeNames.get(Integ_Manager_Id),mapEmpNumbers.get(Integ_Manager_Id));
                                            lstManagers.add(managerName);
                                            Integ_Manager_Id = mapEmployee1.get(managerId);
                                            
                                            if(String.isNotBlank(Integ_Manager_Id)) {
                                                managerId = mapEmployee2.get(Integ_Manager_Id);
                                                managerName = formatManager(mapEmployeeNames.get(Integ_Manager_Id),mapEmpNumbers.get(Integ_Manager_Id));
                                                lstManagers.add(managerName);
                                                Integ_Manager_Id = mapEmployee1.get(managerId);
                                                
                                                if(String.isNotBlank(Integ_Manager_Id)) {
                                                    managerId = mapEmployee2.get(Integ_Manager_Id);
                                                    managerName = formatManager(mapEmployeeNames.get(Integ_Manager_Id),mapEmpNumbers.get(Integ_Manager_Id));
                                                    lstManagers.add(managerName);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            
            if(lstManagers.size() > 0) {
               Integer x = lstManagers.size() - 1;
               for(Integer i=0;i<lstManagers.size();i++) {
                  if(i == 0) emp.INTEG_Manager_Level_1__c  = lstManagers[x];
                  if(i == 1) emp.INTEG_Manager_Level_2__c  = lstManagers[x];
                  if(i == 2) emp.INTEG_Manager_Level_3__c  = lstManagers[x];
                  if(i == 3) emp.INTEG_Manager_Level_4__c  = lstManagers[x];
                  if(i == 4) emp.INTEG_Manager_Level_5__c  = lstManagers[x];
                  if(i == 5) emp.INTEG_Manager_Level_6__c  = lstManagers[x];
                  if(i == 6) emp.INTEG_Manager_Level_7__c  = lstManagers[x];
                  if(i == 7) emp.INTEG_Manager_Level_8__c  = lstManagers[x];
                  if(i == 8) emp.INTEG_Manager_Level_9__c  = lstManagers[x];
                  if(i == 9) emp.INTEG_Manager_Level_10__c = lstManagers[x];
                  x--;
               }
            }
        }
        
        for(Integ_employee__c emp : lstEmployees) {
          system.debug('*********'+emp.INTEG_Manager_Level_1__c);
          system.debug('*********'+emp.INTEG_Manager_Level_2__c);
          system.debug('*********'+emp.INTEG_Manager_Level_3__c);
          system.debug('*********'+emp.INTEG_Manager_Level_4__c);
          system.debug('*********'+emp.INTEG_Manager_Level_5__c);
          system.debug('*********'+emp.INTEG_Manager_Level_6__c);
          system.debug('*********'+emp.INTEG_Manager_Level_7__c);
          system.debug('*********'+emp.INTEG_Manager_Level_8__c);
          system.debug('*********'+emp.INTEG_Manager_Level_9__c);
          system.debug('*********'+emp.INTEG_Manager_Level_10__c);
          
        }
        
        //if(lstEmployees.size() > 0)
        //    update lstEmployees;

        }catch(exception ex) {
          //Esa_DebugService.writeException(ex, 1, SOURCE_FILE,'EmployeeManagerLadder.finish: Failed with errors :Unhandled exception');
        }

}

public static String formatManager(String managerName, String managerNumber) {
        string manager = '';
        if (String.isNotBlank(managerName)) {
            manager += managerName;
            if (String.isNotBlank(managerNumber)) {
                manager += ' (' + managerNumber + ')';
            }
        }
        return manager;   
    }

}
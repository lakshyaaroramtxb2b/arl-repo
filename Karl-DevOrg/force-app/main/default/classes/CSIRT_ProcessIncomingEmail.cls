public class CSIRT_ProcessIncomingEmail {

    @InvocableMethod(label='ProcessEmail' description='Handles Incoming CSIRT Emails')
    public static void ProcessEmail(List<Id> EmailMsgIDs) {
                
        List<RecordType> recTypes = [SELECT Id FROM RecordType WHERE SobjectType = 'Case' AND DeveloperName IN ('CSIRT_Email', 'CSIRT_Alert', 'SDC_Email')];
        Set<Id> recTypeIds = new Set<Id>();
        for (RecordType rt : recTypes){
            recTypeIds.add(rt.Id);        
        } 
        EmailMessage eMsg = [SELECT Id, ParentId, BccAddress, CcAddress, FromAddress, ToAddress, FromName FROM EmailMessage WHERE Id = :EmailMsgIDs[0]];
        
        if (eMsg.ToAddress == null){
            return;
        }
        
        Case c = [SELECT Id, AccountId, IRCloud__Email_To__c, ContactId, SuppliedName, SuppliedEmail, RecordTypeId FROM Case WHERE Id = :eMsg.ParentId];
         
        //May want to beef this up in the future to look at the CC and BCC fields as well; as we may be CC'd or BCC'd on emails
        List<IRCloud__Environment__c> envs = [SELECT Id, IRCloud__Account__c FROM IRCloud__Environment__c WHERE IRCloud__Security_Email__c = :eMsg.ToAddress];
        List<IRCloud__Impacted_Environment__c> caseEnvs = [SELECT Id, IRCloud__case__c, IRCloud__Environment__c FROM IRCloud__Impacted_Environment__c WHERE IRCloud__case__c = :c.Id];
        Map<Id, IRCloud__Impacted_Environment__c> caseEnvMap = new Map<Id, IRCloud__Impacted_Environment__c>();
        for (IRCloud__Impacted_Environment__c e : caseEnvs){
            caseEnvMap.put(e.IRCloud__Environment__c, e);
        }
         
        IRCloud__Environment__c env;
        if (envs.size() > 0 && !caseEnvMap.containsKey(envs[0].Id) && recTypeIds.contains(c.RecordTypeId)) {
            env = envs[0];
            IRCloud__Impacted_Environment__c impEnv = new IRCloud__Impacted_Environment__c();
            impEnv.IRCloud__case__c = c.Id;
            impEnv.IRCloud__Environment__c = env.Id;
            insert impEnv;
            
        } else {
            return;
        }
         
        List<Contact> contacts = [SELECT Id FROM Contact WHERE Email =:eMsg.FromAddress];
        if (contacts.size() > 0) {
            if (c.ContactId == Null){
                c.ContactId = contacts[0].Id;
            }
        }
         
        if (string.isblank(c.IRCloud__Email_To__c)){
            c.IRCloud__Email_To__c = eMsg.ToAddress;
            if (c.AccountId == Null){
                c.AccountId = env.IRCloud__Account__c;
            }
            c.SuppliedName = eMsg.FromName;
            c.SuppliedEmail = eMsg.FromAddress;
        }
        update c;
    }
}
public without sharing class ChartMeanTimeOverTime {

    /* Only some of these are enabled for using setRecords() */
    
    /* instance stuff */
    
    // just what it says
    public String extraWhere;
    
    // do we ignore stuff where fieldName1 and fieldName2 where on the same day
    public boolean ignoreSameDate = false;
    
    // do we ignore stuff where fieldName1 and fieldName2 are exactly the same
    public boolean ignoreSameTime = false;
    
    // do we ignore stuff where fieldName2 is less than fieldName1
    public boolean ignoreNegativeDifference = true;
    
    public String objectName;
    public String fieldName1;
    public String fieldName2;
    
    // Bucket the events into these groups (hourly, daily, etc)
    public String period;
    
    // measure mean in these units
    public String precision;
    
    // don't pull events before this day
    public Datetime startDate;
    
    // If we run this code via executeanonymous (as source), we can return a value so
    // we raise it as a JSON string in an exception. Horrible hack, I know
    public Boolean resultsAsException = false;
    
    // If we're grouping ignore or only include these. Drop the record from the dataset if ignored or unincluded
    public Set<String> includeGroupingVals;
    public Set<String> excludeGroupingVals;

    // when manually providing records
    public SObject[] records;
    
    public Map<Datetime,Map<String,Map<String,Double>>> meanTimeByFieldValueOverTime(String groupByFieldName) {
        /* Same as above but also group by unique values within a field
         *
         * Required parameters:
         *   objectName - name of the object to query
         *   fieldName1 - name of the start date field
         *   fieldName2 - name of the end date field
         *   groupByFieldName - name of the field to do grouping by
         *   
         * Optional:
         *   startDate - don't query objects before this date. GMT
         *   period - compute mean after bucketing records into these groups. Hourly, daily, weekly, monthly, yearly. Default weekly
         *   precision - return mean in these terms (milliseconds, seconds, minutes, hours, days, weeks). Default days
         *
         */
        if ((objectName == null) && (records == null)) {
            throw new ChartMeanTimeOverTimeException('Must supply object parameter or records');
        }
        if (fieldName1 == null) {
            throw new ChartMeanTimeOverTimeException('Must supply field1 parameter');
        }
        if (fieldName2 == null) {
            throw new ChartMeanTimeOverTimeException('Must supply field2 parameter');
        }
        // CRUD/FLS checks on the request. Will throw an exception if it 
        // doesn't work out
        if (records != null) {
            checkAccess(objectName,new String[]{fieldName1,fieldName2});
        }
        
        if (period == null) {
            period = 'weekly';
        }
        final Set<String> ALLOWED_PERIODS = new Set<String>{'hourly','daily','weekly','monthly','yearly'};
        if (!ALLOWED_PERIODS.contains(period)) {
            throw new ChartMeanTimeOverTimeException('Invalid period '+period);
        }
        
        if (precision == null) {
            precision = 'days';
        }
        final Set<String> ALLOWED_PRECISION = new Set<String>{'milliseconds', 'seconds', 'minutes', 'hours', 'days', 'weeks'};
        if (!ALLOWED_PRECISION.contains(precision)) {
            throw new ChartMeanTimeOverTimeException('Invalid precision '+precision);
        }
        
        String sql = 'SELECT '+fieldName1+','+fieldName2+','+groupByFieldName+' FROM '+objectName+' WHERE isDeleted=false AND '+fieldName1+' != null AND '+fieldName2+' != null';
        if (startDate != null) {
            sql += ' AND CreatedDate >: startDate';
        }
        
        if (extraWhere != null) {
            sql += ' AND '+extraWhere;
        }
        
        sql += ' ORDER BY '+fieldName2;
        
        System.debug(sql);
        
        Set<String> uniqueGroupByValues = new Set<String>();
        
        Map<Datetime,List<SObject>> buckets = new Map<Datetime,List<SObject>>();
        
        if (records == null) {
            for (List<SObject> objs : Database.query(sql)) {
                Set<SObject> toRemove = new Set<SObject>();
                Set<SObject> acceptable = new Set<SObject>(objs);
                
                for (SObject o : objs) {
                    if (includeGroupingVals != null) {
                        if (includeGroupingVals.contains(String.valueOf(o.get(groupByFieldName)))) {
                            uniqueGroupByValues.add(String.valueOf(o.get(groupByFieldName)));
                        } else {
                            // we're ignoring this record
                            toRemove.add(o);
                        }
                    } else if (excludeGroupingVals != null) {
                        if (!excludeGroupingVals.contains(String.valueOf(o.get(groupByFieldName)))) {
                            uniqueGroupByValues.add(String.valueOf(o.get(groupByFieldName)));
                        } else {
                            // we're ignoring this record
                            toRemove.add(o);
                        }
                    } else {
                        uniqueGroupByValues.add(String.valueOf(o.get(groupByFieldName)));
                    }
                    
                    if (ignoreSameDate) {
                        if ( (((Datetime) o.get(fieldName1)).yearGmt() == ((Datetime) o.get(fieldName2)).yearGmt()) && (((Datetime) o.get(fieldName1)).dayOfYearGmt() == ((Datetime) o.get(fieldName2)).dayOfYearGmt()) ) {
                            toRemove.add(o);
                        }
                    }
                    
                    if (ignoreSameTime) {
                        if ( ((Datetime) o.get(fieldName2)).getTime() == ((Datetime) o.get(fieldName1)).getTime() ) {
                            toRemove.add(o);
                        }
                    }
                    
                    if (ignoreNegativeDifference) {
                        if ( ((Datetime) o.get(fieldName2)) < ((Datetime) o.get(fieldName1)) ) {
                            toRemove.add(o);
                        }
                    }
                
                }
                    
                if (toRemove.size() > 0) {
                    acceptable.removeAll(toRemove);
                    objs.clear();
                    objs.addAll(acceptable);
                }
                dateBucketize(buckets,objs,fieldName2,period);
            }
        } else {
            Set<SObject> toRemove = new Set<SObject>();
            Set<SObject> acceptable = new Set<SObject>(records);
            
            for (SObject o : records ) {
                
                if (includeGroupingVals != null) {
                    if (includeGroupingVals.contains(String.valueOf(o.get(groupByFieldName)))) {
                        uniqueGroupByValues.add(String.valueOf(o.get(groupByFieldName)));
                    } else {
                        // we're ignoring this record
                        toRemove.add(o);
                    }
                } else if (excludeGroupingVals != null) {
                    if (!excludeGroupingVals.contains(String.valueOf(o.get(groupByFieldName)))) {
                        uniqueGroupByValues.add(String.valueOf(o.get(groupByFieldName)));
                    } else {
                        // we're ignoring this record
                        toRemove.add(o);
                    }
                } else {
                    uniqueGroupByValues.add(String.valueOf(o.get(groupByFieldName)));
                }
                
                if (ignoreSameDate) {
                    if ( (((Datetime) o.get(fieldName1)).yearGmt() == ((Datetime) o.get(fieldName2)).yearGmt()) && (((Datetime) o.get(fieldName1)).dayOfYearGmt() == ((Datetime) o.get(fieldName2)).dayOfYearGmt()) ) {
                        toRemove.add(o);
                    }
                }
                
                if (ignoreSameTime) {
                    if ( ((Datetime) o.get(fieldName2)).getTime() == ((Datetime) o.get(fieldName1)).getTime() ) {
                        toRemove.add(o);
                    }
                }
                
                if (ignoreNegativeDifference) {
                    if ( ((Datetime) o.get(fieldName2)) < ((Datetime) o.get(fieldName1)) ) {
                        toRemove.add(o);
                    }
                }
                
                // we obviously can't calculate a mean without these
                if ((o.get(fieldName1) == null) || (o.get(fieldName2) == null)) {
                    toRemove.add(o);
                }
                
            }
            
            if (toRemove.size() > 0) {
                acceptable.removeAll(toRemove);
                records.clear();
                records.addAll(acceptable);
            }
            
            dateBucketize(buckets,records,fieldName2,period);
        }
        
        Map<Datetime,Map<String,Map<String,Double>>> res = new Map<Datetime,Map<String,Map<String,Double>>>();
        for (Datetime bucket : buckets.keySet()) {
            res.put(bucket,new Map<String,Map<String,Double>>());
            
            Map<String,List<Sobject>> grouped = new Map<String,List<SObject>>();
            
            for (String groupByVal : uniqueGroupByValues) {
                grouped.put(groupByVal,new List<SObject>());
            }
            for (SObject o : buckets.get(bucket) ) {
                grouped.get(String.valueOf(o.get(groupByFieldName))).add(o);    
            }
            for (String groupByVal : grouped.keySet()) {
                if (grouped.get(groupByVal).size() == 0) {
                    continue;
                }
                if (precision == 'milliseconds') {
                    res.get(bucket).put(groupByVal,new Map<String,Double>{'records'=>grouped.get(groupByVal).size(),'mean'=>computeMeanTimeDifferenceMilliseconds(grouped.get(groupByVal),fieldName1,fieldName2)});
                } else if (precision == 'seconds') {
                    res.get(bucket).put(groupByVal,new Map<String,Double>{'records'=>grouped.get(groupByVal).size(),'mean'=>computeMeanTimeDifferenceMilliseconds(grouped.get(groupByVal),fieldName1,fieldName2) / 1000});
                } else if (precision == 'minutes') {
                    res.get(bucket).put(groupByVal,new Map<String,Double>{'records'=>grouped.get(groupByVal).size(),'mean'=>computeMeanTimeDifferenceMilliseconds(grouped.get(groupByVal),fieldName1,fieldName2) / (1000*60)});
                } else if (precision == 'hours') {
                    res.get(bucket).put(groupByVal,new Map<String,Double>{'records'=>grouped.get(groupByVal).size(),'mean'=>computeMeanTimeDifferenceMilliseconds(grouped.get(groupByVal),fieldName1,fieldName2) / (1000*60*60)});
                } else if (precision == 'days') {
                    res.get(bucket).put(groupByVal,new Map<String,Double>{'records'=>grouped.get(groupByVal).size(),'mean'=>computeMeanTimeDifferenceMilliseconds(grouped.get(groupByVal),fieldName1,fieldName2) / (1000*60*60*24)});
                } else if (precision == 'weeks') {
                    res.get(bucket).put(groupByVal,new Map<String,Double>{'records'=>grouped.get(groupByVal).size(),'mean'=>computeMeanTimeDifferenceMilliseconds(grouped.get(groupByVal),fieldName1,fieldName2) / (1000*60*60*24*7)});
                } else {
                    throw new ChartMeanTimeOverTimeException('Unknown precision '+precision);
                }
            }
        }
        if (resultsAsException) {
            throw new ChartMeanTimeOverTimeException(JSON.serialize(res));
        } 
        return res;
    }
    
    public Map<Long,Map<String,Map<String,Double>>> meanTimeByFieldValueOverTimeLong(String groupByFieldName) {
        boolean resAsExc = this.resultsAsException;
        this.resultsAsException = false;
        Map<Datetime,Map<String,Map<String,Double>>> res = meanTimeByFieldValueOverTime(groupByFieldName);
        Map<Long,Map<String,Map<String,Double>>> r = new Map<Long,Map<String,Map<String,Double>>> ();
        for (Datetime k: res.keySet()) {
            r.put(k.getTime(),res.get(k));
        }
        if (resAsExc) {
            throw new ChartMeanTimeOverTimeException(JSON.serialize(r));
        }
        return r;
    }
    
    public Map<Datetime,Map<String,Integer>> countByFieldValueOverTime(String groupByFieldName) {
        /*
         * Basically a functioning group by month. SELECT count(id) FROM obj GROUP BY MONTHLY(fieldName1) THEN UNIQUE(groupByFieldName)
         *
         * Required parameters:
         *   objectName - name of the object to query
         *   fieldName1 - name of the start date field
         *   groupByFieldName - name of the field whose values we're grouping by
         *   
         * Optional:
         *   startDate - don't query objects before this date. GMT
         *   period - compute mean after bucketing records into these groups. Hourly, daily, weekly, monthly, yearly. Default weekly
         *   precision - return mean in these terms (milliseconds, seconds, minutes, hours, days, weeks). Default days
         *
         */
        if ((objectName == null) && (records == null)) {
            throw new ChartMeanTimeOverTimeException('Must supply object parameter');
        }
        if (fieldName1 == null) {
            throw new ChartMeanTimeOverTimeException('Must supply field1 parameter');
        }

        // CRUD/FLS checks on the request. Will throw an exception if it 
        // doesn't work out
        checkAccess(objectName,new String[]{fieldName1});
        
        if (period == null) {
            period = 'monthly';
        }
        final Set<String> ALLOWED_PERIODS = new Set<String>{'hourly','daily','weekly','monthly','yearly'};
        if (!ALLOWED_PERIODS.contains(period)) {
            throw new ChartMeanTimeOverTimeException('Invalid period '+period);
        }
        
        if (precision == null) {
            // this means the SLAs given are being measured in minutes
            precision = 'minutes';
        }
        final Set<String> ALLOWED_PRECISION = new Set<String>{'milliseconds', 'seconds', 'minutes', 'hours', 'days', 'weeks'};
        if (!ALLOWED_PRECISION.contains(precision)) {
            throw new ChartMeanTimeOverTimeException('Invalid precision '+precision);
        }       
       
        // fieldname1 (e.g. CreatedDate) needs to not be null, otherwise we can't know if it's in SLA. We could still know that something
        // is out of SLA with fieldName2 being null based on the time that has passed since fieldName1
        String sql = 'SELECT Id, '+fieldName1+','+groupByFieldName+' FROM '+objectName+' WHERE '+fieldName1+' != null';
        
        if (startDate != null) {
            sql += ' AND CreatedDate >: startDate';
        }
        
        if (extraWhere != null) {
            sql += ' AND '+extraWhere;
        }
        
        sql += ' ORDER BY '+fieldName1;
        System.debug(sql);
        
        Set<String> uniqueGroupByValues = new Set<String>();
        
        // DateBucket => {groupbyfieldnamevalue => 123}
        Map<Datetime,Map<String,Integer>> res = new Map<Datetime,Map<String,Integer>>();
        
        if (records == null) {
            for (List<SObject> cl : Database.query(sql)) { 
                Map<Datetime,List<SObject>> bukkets = new Map<Datetime,List<SObject>>();
                // modify in place. Ugly but I don't want to do any copying                  
                dateBucketize(bukkets,(List<SObject>)cl, fieldName1, period);
                
                Datetime now = Datetime.now();
                for (Datetime bucket : bukkets.keySet()) {
                    if (!res.containsKey(bucket)) {
                        // Sev1,Sev2,Sev3,Total
                        res.put(bucket,new Map<String,Integer>());
                    }
                    Map<String,Integer> workingBucket = res.get(bucket);
                    for (SObject c : bukkets.get(bucket)) {
                        String groupByFieldValue = String.valueOf(c.get(groupByFieldName));
                        uniqueGroupByValues.add(groupByFieldValue);
                        if (!workingBucket.containsKey(groupByFieldValue)) {
                            workingBucket.put(groupByFieldValue,1);
                        } else {
                            workingBucket.put(groupByFieldValue,workingBucket.get(groupByFieldValue)+1);
                        }
                    }
                }
            }
        } else {
            Set<SObject> toRemove = new Set<SObject>();
            Set<SObject> acceptable = new Set<SObject>(records);
            for (SObject o : records) {
                // cleanup
                if (o.get(fieldName1) == null) {
                    toRemove.add(o);
                }
            }
            if (toRemove.size() > 0) {
                acceptable.removeAll(toRemove);
                records.clear();
                records.addAll(acceptable);
            }
            Map<Datetime,List<SObject>> bukkets = new Map<Datetime,List<SObject>>();
            // modify in place. Ugly but I don't want to do any copying                  
            dateBucketize(bukkets,records, fieldName1, period);
            
            Datetime now = Datetime.now();
            for (Datetime bucket : bukkets.keySet()) {
                if (!res.containsKey(bucket)) {
                    // Sev1,Sev2,Sev3,Total
                    res.put(bucket,new Map<String,Integer>());
                }
                Map<String,Integer> workingBucket = res.get(bucket);
                for (SObject c : bukkets.get(bucket)) {
                    String groupByFieldValue = String.valueOf(c.get(groupByFieldName));
                    uniqueGroupByValues.add(groupByFieldValue);
                    if (!workingBucket.containsKey(groupByFieldValue)) {
                        workingBucket.put(groupByFieldValue,1);
                    } else {
                        workingBucket.put(groupByFieldValue,workingBucket.get(groupByFieldValue)+1);
                    }
                }
            }
        }
        
        for (Datetime bucket : res.keySet()) {
            for (String uniqueGroupingValue : uniqueGroupByValues) {
                if (!res.get(bucket).containsKey(uniqueGroupingValue)) {
                    res.get(bucket).put(uniqueGroupingValue,0);
                }
            }
        }
        if (resultsAsException) {
            throw new ChartMeanTimeOverTimeException(JSON.serialize(res));
        }
        return res;
    }
    
    public Map<Long,Map<String,Integer>> countByFieldValueOverTimeLong(String groupByFieldName) {
        boolean resAsExc = this.resultsAsException;
        this.resultsAsException = false;
        Map<Datetime,Map<String,Integer>> res = countByFieldValueOverTime(groupByFieldName);
        Map<Long,Map<String,Integer>> r = new Map<Long,Map<String,Integer>> ();
        for (Datetime k: res.keySet()) {
            r.put(k.getTime(),res.get(k));
        }
        if (resAsExc) {
            throw new ChartMeanTimeOverTimeException(JSON.serialize(r));
        }
        return r;
    }
    
    public void setRecords(SObject[] recs) {
        this.records = recs;
    }
    
    public Double computeMeanTimeDifferenceMilliseconds(SObject[] objs, String fieldName1, String fieldName2) {
   
        Long total = 0;
        for (SObject obj : objs) {
            total += ((datetime) obj.get(fieldName2)).getTime() - ((datetime) obj.get(fieldName1)).getTime();
        }
        return total / objs.size();
    }
    public Map<Datetime,List<SObject>> dateBucketize(Map<Datetime,List<SObject>> results,SObject[] objects, String fieldName, String period) {
        for (SObject o : objects) {
            Datetime f = (Datetime) o.get(fieldName);
            Datetime bucket;
            if (period == 'weekly') {
                bucket = closestSaturdayAtNoon(f);
            } else if (period == 'monthly') {
                bucket = lastDateOfMonthAtNoon(f);
            } else if (period == 'yearly') {
                bucket = lastDateOfYearAtNoon(f);
            } else if (period == 'daily') {
                bucket = sameDay(f);
            } else if (period == 'hourly') {
                bucket = sameHour(f);
            } else {
                throw new ChartMeanTimeOverTimeException('Unknown period '+period);
            }
            
            if (!results.containsKey(bucket)) {
                results.put(bucket,new List<Sobject>());
            }
            results.get(bucket).add(o);
        }
        return results;
    }
    public Datetime closestSaturdayAtNoon(Datetime subj) {
        Datetime sat;
        String dayOfWeekStr = subj.formatGMT('E');
        if (dayOfWeekStr == 'Sun') {
            sat = subj.addDays(6);
        } else if (dayOfWeekStr == 'Mon') {
            sat = subj.addDays(5);
        } else if (dayOfWeekStr == 'Tue') {
            sat = subj.addDays(4);
        } else if (dayOfWeekStr == 'Wed') {
            sat = subj.addDays(3);
        } else if (dayOfWeekStr == 'Thu') {
            sat = subj.addDays(2);
        } else if (dayOfWeekStr == 'Fri') {
            sat = subj.addDays(1);
        } else {
            sat = subj;
        }
        
        return datetime.newInstanceGmt(sat.yearGmt(),sat.monthGmt(),sat.dayGmt(),12,0,0);
    }
    public Datetime lastDateOfMonthAtNoon(Datetime subj) {
        if (subj.dayGmt() == 31) {
            System.debug('lastDateOfMonthAtNoon: subject day of month is already the 31st. Light clone');
            // This is the last day. Make a light clone
            return Datetime.newInstanceGmt(subj.yearGmt(),subj.monthGmt(),31,12,0,0);
        }
        
        // This is stupid. I would think that asking for an invalid date (e.g. Feb 31) would
        // throw an exception, but it just overflows into the next month
        Datetime attempt;
        for (Integer i : new Integer[]{31,30,29,28}) {
            attempt = Datetime.newInstanceGmt(subj.yearGmt(),subj.monthGmt(),i,12,0,0);
            System.debug('lastDateOfMonthAtNoon: Trying: ');System.debug(attempt);
            if (attempt.monthGmt() == subj.monthGmt()) {
                return attempt;
            }
        }
        throw new ChartMeanTimeOverTimeException('Can\'t figure out how many days are in '+subj.formatGmt('M'));
    }
    public Datetime lastDateOfYearAtNoon(Datetime subj) {
        return Datetime.newInstanceGmt(subj.yearGmt(),12,31,12,0,0);
    }
    public Datetime sameDay(Datetime subj) {
        return Datetime.newInstanceGmt(subj.yearGmt(),subj.monthGmt(),subj.dayGmt(),12,0,0);
    }
    public Datetime sameHour(Datetime subj) {
        return Datetime.newInstanceGmt(subj.yearGmt(),subj.monthGmt(),subj.dayGmt(),subj.hourGmt(),0,0);
    }
    
    public void checkAccess(String objectName, String[] fieldNames) {
        Schema.SObjectType objType = Schema.getGlobalDescribe().get(objectName);
        if (objType == null) {
            // we now support non-SObjects
            return;
            //throw new ChartMeanTimeOverTimeException('Unknown object type '+objType);
        } else if (objType.getDescribe().isAccessible() == false) {
            throw new ChartMeanTimeOverTimeException('Unknown object type '+objType);
        }
        
        for (String f : fieldNames ) {
            Schema.SObjectField fieldObj = objType.getDescribe().fields.getMap().get(f);
            if (fieldObj == null) {
                throw new ChartMeanTimeOverTimeException('Unknown field '+f);
            }
            if (fieldObj.getDescribe().isAccessible() == false) {
                throw new ChartMeanTimeOverTimeException('Unknown field '+f);
            }
        }
    }
}
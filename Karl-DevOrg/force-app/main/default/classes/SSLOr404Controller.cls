public with sharing class SSLOr404Controller {
    // This doesn't really work
    public String msg {get;private set;}

    public PageReference check() {
        String path = Site.getOriginalUrl();
        String url  = Site.getCurrentSiteUrl();
        if (!url.startsWith('https://')) {
            Pattern domainPieces = pattern.compile('^([a-zA-Z0-9\\-_\\.]+)\\.force.com$');
            Matcher m = domainPieces.matcher(Site.getDomain());
            m.find(); // lame
            String newUrl = 'https://'+m.group(1)+'.secure.force.com'+path;
            System.debug(newUrl);
            return new PageReference(newUrl);
        }
        // I can't find an Apex 404 exception and I don't want to 
        // cause a loop with 401 exceptions -- Soby
        msg = '404 Not Found';
        return null;
    }
        
}
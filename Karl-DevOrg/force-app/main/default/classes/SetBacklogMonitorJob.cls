public with sharing class SetBacklogMonitorJob {

    //transfer ownership
    public String errMsg {get; set;}
    public List<BatchNotifier.SingleNotifier> issueList {get; set;}
    public Boolean haveIssues {get; set;}
    
    public PageReference doTestTZJob() {
        errMsg = 'No errors';
        haveIssues = False;
        try{
            BatchNotifier bn = new BatchNotifier('PopcrabMonitorTests');
            TZBacklogChecker.run(bn);
            bn.processBatch();
            errMsg = 'No errors - ' + bn.size() + ' issues';
            issueList = bn.getArray();
            if (issueList.size() > 0)
                haveIssues = True;
        } catch(Exception e) {
            errMsg = e.getMessage() + ' -- \n\n\n\n -- ' + e.getStackTraceString();
        }
        return null;
    }

    public PageReference doTestPausedCodeScanJob() {
        errMsg = 'No errors';
        haveIssues = False;
        try{
            BatchNotifier bn = new BatchNotifier('PopcrabMonitorTests');
                CodeScanBacklogChecker.run(bn);
                bn.processBatch();
                errMsg = 'No errors - ' + bn.size() + ' issues';
                issueList = bn.getArray();
                if (issueList.size() > 0)
                    haveIssues = True;
            } catch(Exception e) {
            errMsg = e.getMessage() + ' -- \n\n\n\n -- ' + e.getStackTraceString();
        }
        
        return null;        
    }
    
    
    public PageReference doTestScanQueueJob() {
        errMsg = 'No errors';
        haveIssues = False;
        try{
            BatchNotifier bn = new BatchNotifier('PopcrabMonitorTests');
            ScanQueueBacklogChecker.run(bn);
            bn.processBatch();
            errMsg = 'No errors - ' + bn.size() + ' issues';
            issueList = bn.getArray();
            if (issueList.size() > 0)
                haveIssues = True;
        } catch(Exception e) {
            errMsg = e.getMessage() + ' -- \n\n\n\n -- ' + e.getStackTraceString();
        }
        return null;
    }

    public PageReference doTestZipUploadJob() {
        errMsg = 'No errors';
        haveIssues = False;
        try{
            BatchNotifier bn = new BatchNotifier('PopcrabMonitorTests');
            ZipUploadBacklogChecker.run(bn);
            bn.processBatch();
            errMsg = 'No errors - ' + bn.size() + ' issues';
            issueList = bn.getArray();
            if (issueList.size() > 0)
                haveIssues = True;
        } catch(Exception e) {
            errMsg = e.getMessage() + ' -- \n\n\n\n -- ' + e.getStackTraceString();
        }
        return null;
    }

    public PageReference doTestCodeScanJob() {
        errMsg = 'No errors';
        haveIssues = False;
        try{
            BatchNotifier bn = new BatchNotifier('PopcrabMonitorTests');
            CodeScanBacklogChecker.run(bn);
            bn.processBatch();
            errMsg = 'No errors - ' + bn.size() + ' issues';
            issueList = bn.getArray();
            if (issueList.size() > 0)
                haveIssues = True;
        } catch(Exception e) {
            errMsg = e.getMessage() + ' -- \n\n\n\n -- ' + e.getStackTraceString();
        }
        return null;
    }

    public PageReference doTestAllJobs() {
        errMsg = 'No errors';
        haveIssues = False;
        try{
            BatchNotifier bn = new BatchNotifier('PopcrabMonitorTests');
            CodeScanBacklogChecker.run(bn);
            ZipUploadBacklogChecker.run(bn);
            TZBacklogChecker.run(bn);
            ScanQueueBacklogChecker.run(bn);
            bn.processBatch();
            errMsg = 'No errors - ' + bn.size() + ' issues';
            issueList = bn.getArray();
            if (issueList.size() > 0)
                haveIssues = True;
        } catch(Exception e) {
            errMsg = e.getMessage() + ' -- \n\n\n\n -- ' + e.getStackTraceString();
        }
        return null;
    }

    public PageReference doClearJob() {
        BacklogMonitorJob.killJob();
        return null;
    }
    
        public PageReference doSetJob() {
        BacklogMonitorJob.setSchedule();
        return null;
    }
    public string getJobId() {
        PopcrabStatus__c obj = [SELECT Name, textvalue__c FROM PopcrabStatus__c
                               WHERE Name='ScheduledJobId' LIMIT 1][0];
        try {
            Id.valueOf(obj.textvalue__c);
            return obj.textvalue__c;
        } catch(Exception e) {
            return 'NONE';
        }
    }

}
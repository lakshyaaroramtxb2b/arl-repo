/*
Developer: Ralph Callaway <ralph@callawaycloudconsulting.com>
Description:
    Test methods for DynamicSOQLHelper.cls
*/
@isTest
private class DynamicSOQLHelper_Test {

    /* Constants */
    
    public static final String NULL_LIST = '(\'\')';
    public static final String NULL_STRING = '\'\'';

    /* Test Methods */
    
    private static final Id TEST_ID = '001V0000009oBXSIA2';
    
    @isTest
    private static void testListFormat() {
        String order1 = '(\'asdf\',\'qwer\')';
        String order2 = '(\'qwer\',\'asdf\')';
        String output = DynamicSOQLHelper.format(new String[] { 'asdf','asdf','qwer','qwer' });
        system.assert(output == order1 || output == order2, output);
    }
    
    @isTest
    private static void testNullListFormat() {
        system.assertEquals(NULL_LIST, DynamicSOQLHelper.format(new List<String>()));
    }
    
    @isTest
    private static void testSetFormat() {
        Set<String> input = new Set<String>();
        input.add('asdf');
        input.add('qwer');
        String order1 = '(\'asdf\',\'qwer\')';
        String order2 = '(\'qwer\',\'asdf\')';
        String output = DynamicSOQLHelper.format(input);
        system.assert(output == order1 || output == order2, output);
    }
    
    @isTest
    private static void testNullSetFormat() {
        system.assertEquals(NULL_LIST, DynamicSOQLHelper.format(new Set<String>()));
    }
    
    @isTest
    private static void testDateFormat() {
        system.assertEquals(
              '2011-01-01'
            , DynamicSOQLHelper.format(Date.newInstance(2011, 1, 1))
        );
    }
    
    @isTest
    private static void testStringFormat() {
        system.assertEquals(
              '\'a\\\'a\''
            , DynamicSOQLHelper.format('a\'a')
        );
    }
    
    @isTest
    private static void testNullStringFormat() {
        String empty;
        system.assertEquals(NULL_STRING, DynamicSOQLHelper.format(empty));
    }
    
    @isTest
    private static void testIdFormat() {
        system.assertEquals('\'' + TEST_ID + '\'', DynamicSOQLHelper.format(TEST_ID));
    }
    
    @isTest
    private static void testSetIdFormat() {
        system.assertEquals('(\'' + TEST_ID + '\')', DynamicSOQLHelper.format(new Set<Id>{ TEST_ID }));
    }
    
    @isTest
    private static void testListIdFormat() {
        system.assertEquals('(\'' + TEST_ID + '\')', DynamicSOQLHelper.format(new List<Id>{ TEST_ID, TEST_ID }));
    }
}
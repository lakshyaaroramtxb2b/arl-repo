/**
 * Trust Maturity Model v2
 * Oct 2015
 * Written By: Joanna Chen
 * 
 * Enforce the Trust Maturity Model constraint that Environments of record type 'Parent...' must look up to an Account of record type 'Salesforce Business Unit'. * 
 **/

@isTest
public class TEST_TM_enforceAccountRTEnvRTIntegrity {
    
    private static final String BU_ACCOUNT_REC_TYPE_ID = TM_Constants.BU_ACCOUNT_REC_TYPE_ID;
    private static final String CUSTOMER_ACCOUNT_REC_TYPE_ID = TM_Constants.CUSTOMER_ACCOUNT_REC_TYPE_ID;
    private static final String PARENT_INFRA_DEV_QE_ENVIRONMENT_REC_TYPE_ID = TM_Constants.PARENT_INFRA_DEV_QE_ENVIRONMENT_REC_TYPE_ID;
    private static final String OTHER_ENVIRONMENT_REC_TYPE_ID = TM_Constants.OTHER_ENVIRONMENT_REC_TYPE_ID;
    
    
    static testMethod void testCannotChangeBUAcctRtWithEnvParents(){

        // ---------------- CREATE TEST DATA 
        Account a = TEST_TM_Util.createAccount(BU_ACCOUNT_REC_TYPE_ID);
        TEST_TM_Util.createEnvironment(PARENT_INFRA_DEV_QE_ENVIRONMENT_REC_TYPE_ID, a.Id);
            
        // ---------------- RUN TEST            
        
        // update the Record Type
        a.RecordTypeId = Id.valueOf(CUSTOMER_ACCOUNT_REC_TYPE_ID);
        
        test.startTest();
        
        try {
            update a;
        } catch (DmlException e) {
            // show error IRL, since this wouldn't be allowed
        }

        test.stopTest();
        
        
        // ---------------- VERIFY EXPECTED BEHAVIOR
        Account postUpdateAccount = [SELECT Id, RecordTypeId FROM Account WHERE Id = :a.Id];
        System.assertEquals(Id.valueOf(BU_ACCOUNT_REC_TYPE_ID), postUpdateAccount.RecordTypeId, 'Account should not have been updated.');

    }
    
    static testMethod void testCanChangeBUAcctRtWithNoEnvParents(){

        // ---------------- CREATE TEST DATA 
        Account a = TEST_TM_Util.createAccount(BU_ACCOUNT_REC_TYPE_ID);
        TEST_TM_Util.createEnvironment(OTHER_ENVIRONMENT_REC_TYPE_ID, a.Id);
            
        // ---------------- RUN TEST            
        
        // update the Record Type
        a.RecordTypeId = Id.valueOf(CUSTOMER_ACCOUNT_REC_TYPE_ID);
        
        test.startTest();
        update a;
        test.stopTest();
        
        
        // ---------------- VERIFY EXPECTED BEHAVIOR
        Account postUpdateAccount = [SELECT Id, RecordTypeId FROM Account WHERE Id = :a.Id];
        System.assertEquals(Id.valueOf(CUSTOMER_ACCOUNT_REC_TYPE_ID), postUpdateAccount.RecordTypeId, 'Account should have been updated.');

    }
    
    static testMethod void testCannotChangeBUAcctRtWithMixedEnvs(){

        // ---------------- CREATE TEST DATA 
        Account a = TEST_TM_Util.createAccount(BU_ACCOUNT_REC_TYPE_ID);
        TEST_TM_Util.createEnvironment(PARENT_INFRA_DEV_QE_ENVIRONMENT_REC_TYPE_ID, a.Id);
        TEST_TM_Util.createEnvironment(OTHER_ENVIRONMENT_REC_TYPE_ID, a.Id);
            
        // ---------------- RUN TEST            
        
        // update the Record Type
        a.RecordTypeId = Id.valueOf(CUSTOMER_ACCOUNT_REC_TYPE_ID);
        
        test.startTest();
        try {
            update a;
        } catch (DmlException e) {
            // show error IRL, since this wouldn't be allowed
        }
        test.stopTest();
        
        
        // ---------------- VERIFY EXPECTED BEHAVIOR
        Account postUpdateAccount = [SELECT Id, RecordTypeId FROM Account WHERE Id = :a.Id];
        System.assertEquals(Id.valueOf(BU_ACCOUNT_REC_TYPE_ID), postUpdateAccount.RecordTypeId, 'Account should not have been updated.');

    }
    
    static testMethod void testCanChangeCustAcctRtWithNoEnvParents(){

        // ---------------- CREATE TEST DATA 
        Account a = TEST_TM_Util.createAccount(CUSTOMER_ACCOUNT_REC_TYPE_ID);
        TEST_TM_Util.createEnvironment(OTHER_ENVIRONMENT_REC_TYPE_ID, a.Id);
            
        // ---------------- RUN TEST            
        
        // update the Record Type
        a.RecordTypeId = Id.valueOf(BU_ACCOUNT_REC_TYPE_ID);
        
        test.startTest();
        update a;
        test.stopTest();
        
        
        // ---------------- VERIFY EXPECTED BEHAVIOR
        Account postUpdateAccount = [SELECT Id, RecordTypeId FROM Account WHERE Id = :a.Id];
        System.assertEquals(Id.valueOf(BU_ACCOUNT_REC_TYPE_ID), postUpdateAccount.RecordTypeId, 'Account should have been updated.');

    }

}
public with sharing class PRT_SLATriggerHelper {
    public static void setDefaultDueDate(List<SLA__c> slaList, Map<Id,SLA__c> idToSLAMap) {
        for(SLA__c slaRecord : slaList) {
            if(slaRecord.Reporting_Period__c == 'Daily') {
                slaRecord.Due_Date__c = Date.today().addDays(1);
            }
            else if(slaRecord.Reporting_Period__c == 'Weekly') {
                String currentDay = '';
                if(!Test.isRunningTest())    {
                    currentDay = DateTime.now().format('E');
                }
                else    {
                    currentDay = 'Sat';
                }
                
                if(currentDay == 'Mon')
                    slaRecord.Due_Date__c = Date.today().addDays(4);
                else if(currentDay == 'Tue')
                    slaRecord.Due_Date__c = Date.today().addDays(3);
                else if(currentDay == 'Wed')
                    slaRecord.Due_Date__c = Date.today().addDays(2);
                else if(currentDay == 'Thu')
                    slaRecord.Due_Date__c = Date.today().addDays(1);
                else if(currentDay == 'Fri')
                    slaRecord.Due_Date__c = Date.today().addDays(7);
                else if(currentDay == 'Sat')
                    slaRecord.Due_Date__c = Date.today().addDays(6);
                else if(currentDay == 'Sun')
                    slaRecord.Due_Date__c = Date.today().addDays(5);
            }
            else if(slaRecord.Reporting_Period__c == 'Monthly') {
                Date firstDateOfMonth = Date.today().toStartOfMonth(); 
                Date lastDateOfMonth = firstDateOfMonth.addMonths(1).addDays(-1);
                slaRecord.Due_Date__c = lastDateOfMonth;

            }
            else if(slaRecord.Reporting_Period__c == 'Quarterly') {
                Date fiscalQuarterLastDate = [Select EndDate From Period Where type = 'Quarter' and StartDate = THIS_FISCAL_QUARTER].EndDate;
                slaRecord.Due_Date__c = fiscalQuarterLastDate;
            }
            else if(slaRecord.Reporting_Period__c == 'Annually') {
                Date fiscalYearLastDate = [Select EndDate From Period Where type = 'Year' and StartDate = THIS_FISCAL_YEAR].EndDate;
                slaRecord.Due_Date__c = fiscalYearLastDate;
            }
        }
    }

    public static void createSLAMetricRecord(List<SLA__c> newList, Map<Id, SLA__c> oldMap) {
        List<SLA_Metric__c> slaMetricList = new List<SLA_Metric__c>();

        for(SLA__c slaRec : newList) {
            System.debug('Operation: '+slaRec.Operation__c);
            if(oldMap == null || (oldMap!=null && slaRec.Due_Date__c != oldMap.get(slaRec.Id).Due_Date__c)) {
                SLA_Metric__c newSlaMetric = new SLA_Metric__c();
                newSlaMetric.SLA__c = slaRec.Id;
                newSlaMetric.Due_Date__c = slaRec.Due_Date__c;
                newSlaMetric.Operation__c = slaRec.Operation__c;
                if(slaRec.Due_Date__c != null) {
                    slaMetricList.add(newSlaMetric);   
                }
            }
        }
        System.debug('slaMetricList: '+slaMetricList);

        if(slaMetricList!=null && !slaMetricList.isEmpty()) {
            INSERT slaMetricList;
        }
    }
}
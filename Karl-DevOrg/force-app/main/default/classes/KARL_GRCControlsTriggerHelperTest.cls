/**
 * @author Ben Harvie
 * @email ben.harvie@salesforce.com
 * @desc Test class for GRC Controls Trigger Helper (CCF Controls)
*/
@isTest
public with sharing class KARL_GRCControlsTriggerHelperTest {
    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @desc Test data setup.
    */
    @testSetup
    private static void testSetup(){
        User ospAdmin = ARL_TestDataFactory.createNamedOpsAdmin('TestSetup', 'User', 'testsetup@example.com');
        
        System.runAs(ospAdmin){
            KARL_GRC_Controls__c ccfControl = ARL_TestDataFactory.createCcfControl();
            insert ccfControl;

            Control__c controlItem = ARL_TestDataFactory.createControl();
            controlItem.CCF_Control__c = ccfControl.Id;
            controlItem.KARL_CCF_Alignment__c = true;
            insert controlItem;

            Control_Scope__c controlScope = ARL_TestDataFactory.createMappedControlScope(controlItem.Id);
            controlScope.CCF_Control__c = ccfControl.Id;
            controlScope.KARL_CS_CCF_Alignment__c = true;
            insert controlScope;
        }
    }
    
    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @desc In this test, we're checking that the CCF name is created correctly
    */
	@isTest
    public static void checkName(){ 
        // Setup Ops Admin
        User u = ARL_TestDataFactory.createOpsAdmin();

        System.runAs(u) {

            Test.startTest();
                KARL_GRC_Controls__c ccfItem = [SELECT Id,Name,Object_Name__c,Short_Description__c FROM KARL_GRC_Controls__c LIMIT 1];

                String expectedName = ccfItem.Object_Name__c + ' - ' + ccfItem.Short_Description__c;

                system.assertEquals(ccfItem.Name, expectedName, 'Incorrect name of the CCF Record');
            Test.stopTest();
        }
    }
    
    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @desc In this test, we're checking that the CCF name is created correctly
    */
	@isTest
    public static void checkUpdates(){ 
        // Setup Ops Admin
        User u = ARL_TestDataFactory.createOpsAdmin();

        System.runAs(u) {

            Test.startTest();
                KARL_GRC_Controls__c ccfItem = [SELECT Id,Description__c,Short_Description__c FROM KARL_GRC_Controls__c LIMIT 1];

                // Make a change to the description
                ccfItem.Description__c = 'Updated 123';
                ccfItem.Short_Description__c = 'New Name 123';
                update ccfItem;

                Control__c controlItem = [SELECT Id,Control_Description__c FROM Control__c LIMIT 1];
                Control_Scope__c controlScopeItem = [SELECT Id,Modified_Description__c FROM Control_Scope__c LIMIT 1];

                system.assertEquals(ccfItem.Description__c, controlItem.Control_Description__c, 'Control__c descriptions don\'t match');
                system.assertEquals(ccfItem.Description__c, controlScopeItem.Modified_Description__c, 'Control_Scope__c descriptions don\'t match');
            Test.stopTest();
        }
    }
}
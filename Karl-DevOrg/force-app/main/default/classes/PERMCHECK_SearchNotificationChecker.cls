public with sharing class PERMCHECK_SearchNotificationChecker {
    public void notifyAll() {
        Messaging.SingleEmailMessage[] mails = new Messaging.SingleEmailMessage[]{};
        for (PERMCHECK_Search_Notification__c target : this.getNotificationTargets()) {
            Messaging.SingleEmailMessage res = this.findNewAndNotify(target);
            if (res != null) {
                mails.add(res);
            }
        }
        if (mails.size() > 0) {
            Messaging.sendEmail(mails);
        }
    }
    
    public Messaging.SingleEmailMessage findNewAndNotify(PERMCHECK_Search_Notification__c config) {
        if (config.PERMCHECK_Last_Record_Time__c == null) {
            config.PERMCHECK_Last_Record_Time__c = Datetime.now();
            // They've never been notified so we set the baseline as records created after our first run
            update config;
        }
        PERMCHECK_Search_Hit__c[] hits = this.getNewHitsForNotification(config);
        if (hits.size() == 0) {
            return null;
        }
        
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        List<String> sendTo = new List<String>();
        sendTo.add(config.Name);
        mail.setToAddresses(sendTo);
    
        mail.setSubject('Permission checker: '+String.valueOf(hits.size())+' new results');
        String body = '';
        for (PERMCHECK_Search_Hit__c hit : hits) {
            body += String.format('Search Name: {0} <br />' +
                                 'Name: {1} {2} ({3}) <br />' +
                                 'Execution Time: {4}<br /><br /><br />',
                                 new List<String> {hit.PERMCHECK_Search_Name__c,
                                                   hit.PERMCHECK_First_Name__c,
                                                   hit.PERMCHECK_Last_Name__c,
                                                   hit.PERMCHECK_Email__c,
                                                   json.serialize(hit.PERMCHECK_Execution_Time__c)}
                                 );
               
                   
            
            
        }
        mail.setHtmlBody(body);
    
       
        config.PERMCHECK_Last_Record_Time__c = hits.get(hits.size()-1).PERMCHECK_Execution_Time__c;
        update config;
        return mail;
    }
    
    public PERMCHECK_Search_Notification__c[] getNotificationTargets() {
        return [SELECT Name, PERMCHECK_Last_Record_Time__c, PERMCHECK_Active__c FROM PERMCHECK_Search_Notification__c WHERE PERMCHECK_Active__c =: True];
    }
    public PERMCHECK_Search_Hit__c[] getNewHitsForNotification(PERMCHECK_Search_Notification__c notify) {
        return [SELECT PERMCHECK_Email__c, PERMCHECK_First_Name__c, PERMCHECK_Last_Name__c, PERMCHECK_Search_Name__c, PERMCHECK_Execution_Time__c FROM PERMCHECK_Search_Hit__c WHERE
                PERMCHECK_Execution_Time__c >: notify.PERMCHECK_Last_Record_Time__c AND PERMCHECK_New_Result__c = True ORDER BY PERMCHECK_Execution_Time__c ASC];
    }
}
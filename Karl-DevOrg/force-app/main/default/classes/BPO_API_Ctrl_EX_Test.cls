@isTest
private class BPO_API_Ctrl_EX_Test {
    
    private static BPO_API_Ctrl_EX getBPO_API_Ctrl_EX() {
        List<BPO_Assessment__c> assessments = [select name, id from BPO_Assessment__c];        
        return new BPO_API_Ctrl_EX(new ApexPages.StandardController(assessments.get(0)));
    }
    
    @testSetup 
    static void setup() {
       BPO_TestUtil.createBPOAssessments(1, 3, 2);
    }
  
    @isTest
    static void testTaskItemStatusOptions() {
        BPO_API_Ctrl_EX bpoCtrlEx = getBPO_API_Ctrl_EX();
        // Available options are 'Not Started', 'In Progress', 'Completed' and 'Not Applicable'
        System.assert(bpoCtrlEx.taskItemStatusOptions.size() >= 4);
    }
    
    @isTest
    static void testGetAllTasks() {
        BPO_API_Ctrl_EX bpoCtrlEx = getBPO_API_Ctrl_EX();
        List<BPO_Task__c> tasks = bpoCtrlEx.getAllTasksWithItems();
        // Check whether tasks are returned
        System.assertEquals(3, tasks.size());
        for (BPO_Task__c task : tasks) {
            // Check whether task items are returned
            System.assertEquals(2, task.BPO_Task_Items__r.size());
        }
    } 
    
    @isTest
    static void testChangeTaskItemStatus() {
        List<BPO_Task_Item__c> taskItems = [select Id, Name, LastModifiedDate, Response_Boolean__c, Status__c from BPO_Task_Item__c];
        BPO_Task_Item__c taskItem = taskItems.get(0);
        System.assertEquals('In Progress', taskItem.Status__c);        
            
        // Test changing status successfully
        BPO_API_Ctrl_EX bpoCtrlEx = getBPO_API_Ctrl_EX();
        bpoCtrlEx.taskItemId = taskItem.id;
        bpoCtrlEx.taskItemLastModifiedDate = String.valueOfGmt(taskItem.LastModifiedDate);
        bpoCtrlEx.taskItemStatus = BPO_TestUtil.STATUS_COMPLETED;
        bpoCtrlEx.changeTaskItemStatus();        
        // Retrieve the taskItem again to test the status
        taskItem = [select Id, Name, LastModifiedDate, Response_Boolean__c, Status__c from BPO_Task_Item__c where id = :taskItem.id];
        System.assertEquals(BPO_TestUtil.STATUS_COMPLETED, taskItem.Status__c);
        System.assertEquals(BPO_API_Ctrl_EX.SUCCESS, bpoCtrlEx.taskItemUpdateStatus);
        
        // Test changing status failure
        bpoCtrlEx.taskItemId = taskItem.id;
        bpoCtrlEx.taskItemLastModifiedDate = '2017-05-10 10:15:00'; // Some past date
        bpoCtrlEx.taskItemStatus = BPO_TestUtil.STATUS_NA;
        bpoCtrlEx.changeTaskItemStatus();        
        // Retrieve the taskItem again to test the status is not changed
        taskItem = [select Id, Name, LastModifiedDate, Status__c from BPO_Task_Item__c where id = :taskItem.id];
        System.assertNotEquals(BPO_TestUtil.STATUS_NA, taskItem.Status__c);
        System.assertEquals(BPO_API_Ctrl_EX.FAILURE, bpoCtrlEx.taskItemUpdateStatus);       
    }
    
    @isTest
    static void testChangeTaskItemResponseDate() {
        List<BPO_Task_Item__c> taskItems = [select Id, Name, LastModifiedDate, Response_Date__c from BPO_Task_Item__c];
        BPO_Task_Item__c taskItem = taskItems.get(0);
        System.assertEquals('2017-01-01', String.valueOf(taskItem.Response_Date__c)); 
            
        // Test changing date response successfully
        BPO_API_Ctrl_EX bpoCtrlEx = getBPO_API_Ctrl_EX();
        bpoCtrlEx.taskItemId = taskItem.id;
        bpoCtrlEx.taskItemLastModifiedDate = String.valueOfGmt(taskItem.LastModifiedDate);
        bpoCtrlEx.taskItemResponseType = BPO_API_Ctrl_EX.DATE_RESPONSE_TYPE;
        bpoCtrlEx.taskItemResponse = '02/02/2017';
        bpoCtrlEx.changeTaskItemResponse();        
        // Retrieve the taskItem again to test the response date
        taskItem = [select Id, Name, LastModifiedDate, Response_Date__c from BPO_Task_Item__c where id = :taskItem.id];
        System.assertEquals('2017-02-02', String.valueOf(taskItem.Response_Date__c));
        System.assertEquals(BPO_API_Ctrl_EX.SUCCESS, bpoCtrlEx.taskItemUpdateStatus);
        
        // Test changing response date failure
        bpoCtrlEx.taskItemId = taskItem.id;
        bpoCtrlEx.taskItemLastModifiedDate = '2017-05-10 10:15:00'; // Some past date
        bpoCtrlEx.taskItemResponseType = BPO_API_Ctrl_EX.DATE_RESPONSE_TYPE;
        bpoCtrlEx.taskItemResponse = '03/03/2017';
        bpoCtrlEx.changeTaskItemResponse();                
        // Retrieve the taskItem again to test the date response is not changed
        taskItem = [select Id, Name, LastModifiedDate, Response_LongText__c, Response_Date__c, Response_Boolean__c from BPO_Task_Item__c where id = :taskItem.id];
        System.assertNotEquals('2017-03-03', String.valueOf(taskItem.Response_Date__c));
        System.assertEquals(BPO_API_Ctrl_EX.FAILURE, bpoCtrlEx.taskItemUpdateStatus);       
    }
    
    @isTest
    static void testChangeTaskItemResponseText() {
        List<BPO_Task_Item__c> taskItems = [select Id, Name, LastModifiedDate, Response_LongText__c from BPO_Task_Item__c];
        BPO_Task_Item__c taskItem = taskItems.get(0);
        System.assertEquals('Some Text', taskItem.Response_LongText__c);
            
        // Test changing text successfully
        String someNewText = 'Some New Text';
        BPO_API_Ctrl_EX bpoCtrlEx = getBPO_API_Ctrl_EX();
        bpoCtrlEx.taskItemId = taskItem.id;
        bpoCtrlEx.taskItemLastModifiedDate = String.valueOfGmt(taskItem.LastModifiedDate);
        bpoCtrlEx.taskItemResponseType = BPO_API_Ctrl_EX.LONG_TEXT_RESPONSE_TYPE;
        bpoCtrlEx.taskItemResponse = someNewText;
        bpoCtrlEx.changeTaskItemResponse();        
        // Retrieve the taskItem again to test the response text
        taskItem = [select Id, Name, LastModifiedDate, Response_LongText__c from BPO_Task_Item__c where id = :taskItem.id];
        System.assertEquals(someNewText, taskItem.Response_LongText__c);
        System.assertEquals(BPO_API_Ctrl_EX.SUCCESS, bpoCtrlEx.taskItemUpdateStatus);
        
        // Test changing Text failure
        String someOtherText = 'Some Other Text';
        bpoCtrlEx.taskItemId = taskItem.id;
        bpoCtrlEx.taskItemLastModifiedDate = '2017-05-10 10:15:00'; // Some past date
        bpoCtrlEx.taskItemResponseType = BPO_API_Ctrl_EX.LONG_TEXT_RESPONSE_TYPE;
        bpoCtrlEx.taskItemResponse = someOtherText;
        bpoCtrlEx.changeTaskItemResponse();                
        // Retrieve the taskItem again to test the text is not changed
        taskItem = [select Id, Name, LastModifiedDate, Response_LongText__c from BPO_Task_Item__c where id = :taskItem.id];
        System.assertNotEquals(someOtherText, taskItem.Response_LongText__c);
        System.assertEquals(BPO_API_Ctrl_EX.FAILURE, bpoCtrlEx.taskItemUpdateStatus);       
    }
    
    @isTest
    static void testChangeTaskItemResponseBoolean() {
        List<BPO_Task_Item__c> taskItems = [select Id, Name, LastModifiedDate, Response_Boolean__c from BPO_Task_Item__c];
        BPO_Task_Item__c taskItem = taskItems.get(0); 
        System.assertEquals(true, taskItem.Response_Boolean__c);
            
        // Test changing (Yes/No) Boolean successfully
        BPO_API_Ctrl_EX bpoCtrlEx = getBPO_API_Ctrl_EX();
        bpoCtrlEx.taskItemId = taskItem.id;
        bpoCtrlEx.taskItemLastModifiedDate = String.valueOfGmt(taskItem.LastModifiedDate);
        bpoCtrlEx.taskItemResponseType = BPO_API_Ctrl_EX.BOOLEAN_RESPONSE_TYPE;
        bpoCtrlEx.taskItemResponse = 'false';
        bpoCtrlEx.changeTaskItemResponse();        
        // Retrieve the taskItem again to test the response
        taskItem = [select Id, Name, LastModifiedDate, Response_Boolean__c from BPO_Task_Item__c where id = :taskItem.id];
        System.assertEquals(false, taskItem.Response_Boolean__c);
        System.assertEquals(BPO_API_Ctrl_EX.SUCCESS, bpoCtrlEx.taskItemUpdateStatus);
        
        // Test changing (Yes/No) Boolean failure
        bpoCtrlEx.taskItemId = taskItem.id;
        bpoCtrlEx.taskItemLastModifiedDate = '2017-05-10 10:15:00'; // Some past date
        bpoCtrlEx.taskItemResponseType = BPO_API_Ctrl_EX.BOOLEAN_RESPONSE_TYPE;
        bpoCtrlEx.taskItemResponse = 'true';
        bpoCtrlEx.changeTaskItemResponse();                
        // Retrieve the taskItem again to test the response is not changed
        taskItem = [select Id, Name, LastModifiedDate, Response_Boolean__c from BPO_Task_Item__c where id = :taskItem.id];       
        System.assertNotEquals(true, taskItem.Response_Boolean__c);
        System.assertEquals(BPO_API_Ctrl_EX.FAILURE, bpoCtrlEx.taskItemUpdateStatus);       
    }
}
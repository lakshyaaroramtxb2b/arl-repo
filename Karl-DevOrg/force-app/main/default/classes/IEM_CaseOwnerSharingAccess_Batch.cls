/**
 * @File Name          : IEM_CaseOwnerSharingAccess_Batch.cls
 * @Description        : It is used to change the CaseShare Records Of IEM case from Read to Edit. And display the Deatils 
 *                      of case and caseVisibility in form of Table through mail.
 * @Author             : Swarnima Singh Mandhata
 * @Group              : 
 * @Last Modified By   : Swarnima Singh Mandhata
 * @Last Modified On   : 05/07/2020
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    05/07/2020   Swarnima     Initial Version
**/
public class IEM_CaseOwnerSharingAccess_Batch implements Database.Batchable<sObject>,Schedulable,Database.Stateful{
    static String emailAddress = System.label.IEM_Case_Share_Email_Send_To;
    
    
    public List<CaseShare> caseShareGlobalList = new List<CaseShare>();
    public Set<Id> userIdSet = new Set<Id>();

    public Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator([SELECT ID,casenumber,Action_Plan_Owner__c, Issue_Owner__c, Severity_Assessor_User__c, 
                                         Security_Assurance_SME_User__c, Severity_Assessor_User__r.Profile.Name,
                                         Security_Assurance_SME_User__r.Profile.Name, Security_Assurance_SME_User__r.IsActive,
                                         Severity_Assessor_User__r.isActive
                                         FROM Case 
                                         WHERE RecordTypeId =: IEM_Constants.IEM_CASERECORDTYPEID
                                        ]);
    }
    public void execute (Database.BatchableContext BC,List<sObject> caseList){
        Map<String, CaseShare> keyVsCaseShareRecordMap = new Map<String,CaseShare>();
        Set<Id> allContactIdSet = new Set<Id>();
        List<CaseShare> caseShareRecordsList = new List<CaseShare>();
        
        String shareRecKey;
        String actionPlanKey;
        String issueOwnerKey;
        String severityAssessorKey;
        String securitySMEKey;
        Map<Id, Id> contactIdToUserIdMap = new Map<Id,Id>();
         List<Id> profileIdList = new List<Id>();
        
        // fetch all the users with non-portal profiles to get userIds for contacttype field on case.
        for(profile p:[SELECT id, Name,UserLicenseId,UserLicense.name 
                      FROM Profile
                       WHERE (NOT Name Like '%Portal%') AND (NOT UserLicense.name Like '%Portal%')]){
                           profileIdList.add(p.Id);
                       }
        for(User userRec: [SELECt ID, ContactId,userRoleId, userRole.name,IsActive, ProfileId, Profile.Name 
                     FROM user 
                     WHERE ContactId!=null
                     AND isActive = true AND ProfileId IN: profileIdList]){
                                if(!contactIdToUserIdMap.containsKey(userRec.ContactId)){
                                    contactIdToUserIdMap.put(userRec.ContactId,userRec.id);
                                }
                         userIdSet.add(userRec.Id);
                                //contactIdToUserIdMap.get(userRec.ContactId).add(String.valueOf(userRec.id));
                            }
        //case Share 
        if(caseList!=null && !caseList.isEmpty()){
            //featch caseShare records realated to caseIds. And create a Share Unique Key(userId+caseSHare Record).
            for(CaseShare caseShareRec: [SELECT ID,UserOrGroupId, CaseAccessLevel, IsDeleted, CaseId, Case.casenumber, UserOrGroup.Name 
                                         FROM CaseShare
                                         WHERE CaseId IN: caseList]){
                                             if(caseShareRec.UserOrGroupId != null && caseShareRec.UserOrGroup.Name != null){
                                                 shareRecKey = String.valueOf(caseShareRec.UserOrGroupId)+String.valueOf(caseShareRec.CaseId);
                                                 
                                                 keyVsCaseShareRecordMap.put(shareRecKey,caseShareRec);
                                             } 
                                         }
            // itterate over case
            for(Case c: (List<Case>)caseList){
                if(c.Action_Plan_Owner__c!= null && contactIdToUserIdMap.containsKey(c.Action_Plan_Owner__c)){
                    Id userId = contactIdToUserIdMap.get(c.Action_Plan_Owner__c);
                    if(userId!=null){
                        // create unique key and search for that in caseShare Key Map.(keyVsCaseShareRecordMap)
                        actionPlanKey = String.valueOf(userId)+ String.valueOf(c.Id);
                        if(!keyVsCaseShareRecordMap.containsKey(actionPlanKey)){
                            CaseShare caseShareRec = getCaseShareRecords(c.Id, 'Edit', userId);
                            caseShareRecordsList.add(caseShareRec); // public List for upsert Record.
                            caseShareGlobalList.add(caseShareRec); // global List for E-mail.
                            
                          // else create a case Share record for that user with Edit Access  
                        }else if(keyVsCaseShareRecordMap.get(actionPlanKey).CaseAccessLevel == 'Read'){
                            
                            keyVsCaseShareRecordMap.get(actionPlanKey).CaseAccessLevel = 'Edit';
                            caseShareRecordsList.add(keyVsCaseShareRecordMap.get(actionPlanKey));
                            caseShareGlobalList.add(keyVsCaseShareRecordMap.get(actionPlanKey));
                        }
                        
                    }
                    
                }
                if(c.Issue_Owner__c != null && contactIdToUserIdMap.containsKey(c.Issue_Owner__c)){
                    Id userId = contactIdToUserIdMap.get(c.Issue_Owner__c);
                    if(userId!=null){
                        //System.debug('issue owner '+ userId);
                        // create unique key and search for that in caseShare Key Map.(keyVsCaseShareRecordMap)
                        issueOwnerKey = String.valueOf(userId)+ String.valueOf(c.Id);
                        if(!keyVsCaseShareRecordMap.containsKey(issueOwnerKey)){
                            CaseShare caseShareRec = getCaseShareRecords(c.Id, 'Edit', userId);
                            caseShareRecordsList.add(caseShareRec);
                            caseShareGlobalList.add(caseShareRec);
                          
                            //else create a case Share record for that user with Edit Access 
                        }else if(keyVsCaseShareRecordMap.get(issueOwnerKey).CaseAccessLevel == 'Read'){
                            
                            keyVsCaseShareRecordMap.get(issueOwnerKey).CaseAccessLevel = 'Edit';
                            caseShareRecordsList.add(keyVsCaseShareRecordMap.get(issueOwnerKey));
                            caseShareGlobalList.add(keyVsCaseShareRecordMap.get(issueOwnerKey)); 
                        }
                        
                    }
                    
                }
                // create unique key and search for that in caseShare Key Map.(keyVsCaseShareRecordMap)
                if(c.Severity_Assessor_User__c != null && userIdSet.contains(c.Severity_Assessor_User__c)){
                    severityAssessorKey = String.valueOf(c.Severity_Assessor_User__c)+ String.valueOf(c.Id);
                    if(!keyVsCaseShareRecordMap.containsKey(severityAssessorKey)){
                        CaseShare caseShareRec = getCaseShareRecords(c.Id, 'Edit', c.Severity_Assessor_User__c);
                        caseShareRecordsList.add(caseShareRec);
                        caseShareGlobalList.add(caseShareRec);
                    
                        //else create a case Share record for that user with Edit Access 
                    }else if(keyVsCaseShareRecordMap.get(severityAssessorKey).CaseAccessLevel == 'Read'){
                        
                        keyVsCaseShareRecordMap.get(severityAssessorKey).CaseAccessLevel = 'Edit';
                        caseShareRecordsList.add(keyVsCaseShareRecordMap.get(severityAssessorKey));
                        caseShareGlobalList.add(keyVsCaseShareRecordMap.get(severityAssessorKey));
                    }
                }
                if(c.Security_Assurance_SME_User__c != null && userIdSet.contains(c.Security_Assurance_SME_User__c)){
                    securitySMEKey = String.valueOf(c.Security_Assurance_SME_User__c)+ String.valueOf(c.Id);
                    if(!keyVsCaseShareRecordMap.containsKey(securitySMEKey)){
                        CaseShare caseShareRec = getCaseShareRecords(c.Id, 'Edit', c.Security_Assurance_SME_User__c);
                        
                        caseShareRecordsList.add(caseShareRec);
                        caseShareGlobalList.add(caseShareRec);
                    }else if(keyVsCaseShareRecordMap.get(securitySMEKey).CaseAccessLevel == 'Read'){
                        
                        keyVsCaseShareRecordMap.get(securitySMEKey).CaseAccessLevel = 'Edit';
                        caseShareRecordsList.add(keyVsCaseShareRecordMap.get(securitySMEKey));
                        caseShareGlobalList.add(keyVsCaseShareRecordMap.get(securitySMEKey));
                    }
                }   
            }
            
            if(!caseShareRecordsList.isEmpty()){
                upsert caseShareRecordsList;
            }
        }
    }
    
    public static CaseShare getCaseShareRecords(id caseId, String accessLevel, Id userGroupId){
        //List<CaseShare> caseShareToBeCreatedList = new List<CaseShare>();
        
        CaseShare createCaseShareRec = new CaseShare();
        createCaseShareRec.CaseId = caseId;
        createCaseShareRec.CaseAccessLevel = accessLevel;
        createCaseShareRec.UserOrGroupId = userGroupId;
        //caseShareToBeCreatedList.add(createCaseShareRec);
        return createCaseShareRec;
    }
    
    public static string getTableBody(List<caseShare> caseShareList){
        Map<Id,String> userIdToNameMap = new Map<Id,String>();
        Map<Id,String> caseIdToCaseNumberMap = new Map<Id, String>();
        List<Case_Visibility__c> caseVisibilityList = new List<Case_Visibility__c>();
        
        userIdToNameMap = IEM_CaseShareUtility.getUserName(caseShareList);
        caseIdToCaseNumberMap = IEM_CaseShareUtility.getCaseNumber(caseShareList);        
        
        String htmlBody = '';
        //open table..Case SHare Records Table
        htmlBody = '<table border="1" style="border-collapse: collapse"><caption><b>Sharing Records Added Details</b></caption></p><tr><th>Case Id</th><th>Case Number</th><th>User Or GroupId</th><th>User Or Group Name</th><th>Access Level</th></tr>';
        //iterate over list and output columns/data into table rows...
        for(caseShare caseShareRec : caseShareList){
            String caseNumber;
            String userOrGroupName;
            String caseId = caseShareRec.CaseId; if(caseShareRec.CaseId == null){caseId = '[Not Provided]';}
            if(caseIdToCaseNumberMap.containsKey(caseShareRec.CaseId)){
                String caseNumberT = caseIdToCaseNumberMap.get(caseShareRec.CaseId);
                caseNumber = caseNumberT;
            }else{
                String caseNumberT = '[Not Provided]'; 
                caseNumber = caseNumberT;
            }
            String userOrGroupId = caseShareRec.UserOrGroupId;
            String accessLevel = caseShareRec.CaseAccessLevel;
            if(userIdToNameMap.containsKey(caseShareRec.UserOrGroupId)){
                String userOrGroupNameT = userIdToNameMap.get(caseShareRec.UserOrGroupId);
                userOrGroupName = userOrGroupNameT;
                system.debug('userOrGroupName--> '+ userOrGroupNameT);
            }else{
                String userOrGroupNameT = '[Unable to Identify]';
                userOrGroupName = userOrGroupNameT;
            }
            //if(caseShareRec.UserOrGroup.Name == null){userOrGroupId = '[Not Provided]';}
            htmlBody += '<tr><td>' + CaseId + '</td><td>' + caseNumber + '</td><td>' + userOrGroupId + '</td><td>' + userOrGroupName + '</td><td>'+ accessLevel  + '</td></tr>';
        }
        //close table...
        htmlBody += '</table><p/>';
        //Case Visibility Share Records to the related case
        htmlBody += '<table border="1" style="border-collapse: collapse"><caption><b>Additional Sharing discovered beyond Case Visibility Record Detail</b></caption></p><tr><th>RecordId</th><th>Name</th><th>CaseId</th><th>Contact Id</th><th>Contact Name</th><th>Access Level</th></tr>';
        caseVisibilityList = IEM_CaseShareUtility.getCaseVisibilityRecords(caseShareList);
        for(Case_Visibility__c casevisiblityRec : caseVisibilityList){
            String caseVisibilityId = casevisiblityRec.Id;
            String caseVisibilityName = casevisiblityRec.Name;
            String caseNumber = string.valueOf(casevisiblityRec.Case__c);
            String contact = string.valueOf(casevisiblityRec.Contact__c);
            String contactName = string.valueOf(casevisiblityRec.Contact__r.Name);
            string accesslevel = casevisiblityRec.Access_Level__c;
            
            htmlBody += '<tr><td>' + caseVisibilityId + '</td><td>' + caseVisibilityName + '</td><td>' + caseNumber + '</td><td>' + contact + '</td><td>' + contactName +'</td><td>'+ accesslevel  + '</td></tr>';
        }
        //close table...
        htmlBody += '</table>';
        
        return htmlBody;
    }
    
    public void finish(Database.BatchableContext BC){
        String htmlBodyStr;
        if(caseShareGlobalList!=null && !caseShareGlobalList.isEmpty()){
              htmlBodyStr = getTableBody(caseShareGlobalList);
        }else{
             htmlBodyStr = '<html><body> The batch ran successfully with no changes made in System</body></html>';
        }
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddress = new String[] {emailAddress};
        mail.setToAddresses(toAddress);
        mail.setSubject('Apex Sharing Records Details');
        mail.setHtmlBody(htmlBodyStr);
        //mail.setPlainTextBody('The Apex Sharing Records finished processing');
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });

        
    }
    public void execute(SchedulableContext SC) {
        if(!Test.isRunningTest()){
            database.executebatch(new IEM_CaseOwnerSharingAccess_Batch());
        }  
    }  
}
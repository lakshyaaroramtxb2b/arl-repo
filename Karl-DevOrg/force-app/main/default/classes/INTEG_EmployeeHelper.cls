public with sharing class INTEG_EmployeeHelper {

    public static void hackLastWorkDay(List<INTEG_Employee__c> employees, Map<Id, INTEG_Employee__c> oldMap) {
    
        for (INTEG_Employee__c e : employees) {
            if (!e.INTEG_Active__c && oldMap.get(e.Id).INTEG_Active__c && 
                oldMap.get(e.Id).INTEG_Last_Day_Worked__c == null && e.INTEG_Last_Day_Worked__c == null) {                
                e.INTEG_Last_Day_Worked__c = e.INTEG_Org62UserLastModifiedDate__c.dateGMT();                                    
            }else if(e.INTEG_Active__c && e.INTEG_Last_Day_Worked__c != null){
                e.INTEG_Last_Day_Worked__c = null;
            }        
        
        }
    
    }

    public static void populateManagerName(List<INTEG_Employee__c> employees, Map<Id, INTEG_Employee__c> oldMap) {
    
        for (INTEG_Employee__c e : employees) {
            if (String.isNotBlank(e.INTEG_Manager_Level_10__c)) setManagerName(e, e.INTEG_Manager_Level_10__c);
            else if (String.isNotBlank(e.INTEG_Manager_Level_9__c)) setManagerName(e, e.INTEG_Manager_Level_9__c);
            else if (String.isNotBlank(e.INTEG_Manager_Level_8__c)) setManagerName(e, e.INTEG_Manager_Level_8__c);
            else if (String.isNotBlank(e.INTEG_Manager_Level_7__c)) setManagerName(e, e.INTEG_Manager_Level_7__c);
            else if (String.isNotBlank(e.INTEG_Manager_Level_6__c)) setManagerName(e, e.INTEG_Manager_Level_6__c);
            else if (String.isNotBlank(e.INTEG_Manager_Level_5__c)) setManagerName(e, e.INTEG_Manager_Level_5__c);
            else if (String.isNotBlank(e.INTEG_Manager_Level_4__c)) setManagerName(e, e.INTEG_Manager_Level_4__c);
            else if (String.isNotBlank(e.INTEG_Manager_Level_3__c)) setManagerName(e, e.INTEG_Manager_Level_3__c);
            else if (String.isNotBlank(e.INTEG_Manager_Level_2__c)) setManagerName(e, e.INTEG_Manager_Level_2__c);
            else if (String.isNotBlank(e.INTEG_Manager_Level_1__c)) setManagerName(e, e.INTEG_Manager_Level_1__c);
        }
    
    }

    private static void setManagerName(INTEG_Employee__c employee, String managerLevel) {
        employee.INTEG_Manager_Name__c = managerLevel.substringBefore('(').trim();
    }
    
    public static void populateActiveEmployeeFlag(List<INTEG_Employee__c> lstEmployees){
        for(INTEG_Employee__c employee : lstEmployees){
            employee.INTEG_Active__c = true;
        }
    }
        

}
/*

    Developer Name: Jorge L Caceres - jcaceres@salesforce.com | September 2015
    
    Description: Controller for TM_Matrix page.
    
    Modifications:
        Jorge L Caceres - jcaceres@salesforce.com | January 2017
        - use the new CSIRT environment object

*/

public with sharing class TM_Matrix {

    private Cookie alohaHintCookie = new Cookie('TMMalohaHint', '1', null, 315360000, false);

    public TM_Matrix() {
        // set aloha hint cookie if internal user or requested in url parm
        IF (Auth.CommunitiesUtil.isInternalUser() ||
            ApexPages.currentPage().getParameters().containsKey('TMMalohaHint'))           
            ApexPages.currentPage().setCookies(new Cookie[]{alohaHintCookie});                   
    }

    public static String baseURL { get; private set; } 
                                 {
                                 // if loading from community use the provided base url, else hack.
                                 If (Site.getSiteType() == 'ChatterNetwork') {
                                     baseURL = URL.getSalesforceBaseUrl().toExternalForm() + 
                                               (String.isEmpty(Site.getPathPrefix())? 'TMM' : Site.getPathPrefix());
                                 } else {  
                                     Boolean isSandbox = [select isSandbox from Organization limit 1].isSandbox;
                                     // this is a hack to get the baseURL for the org native salesforce pages NOT for the site
                                     if (isSandbox) baseURL = 'https://' +ApexPages.currentPage().getHeaders().get('X-Salesforce-Forwarded-To');
                                        else baseURL = 'https://security.my.salesforce.com/';
                                     }
                                 }
    
    public static String matrixBaseURL   { get; private set; } 
                                             {
                                             matrixBaseURL = baseURL;
                                             if (UserInfo.getUserType() != 'Guest') {
                                                 ConnectApi.CommunityPage cs = ConnectApi.Communities.getCommunities();                                             
                                                 for (ConnectApi.Community c :cs.communities) {
                                                     if (c.siteUrl.endsWithIgnoreCase('TMM')) matrixBaseURL = c.siteUrl;
                                                 }
                                             }
                                         }
    
    public static boolean isAuthenticated { get; private set; } {
        isAuthenticated = (UserInfo.getUserType() != 'Guest');
    }
    
    public static String visibleBizUnits { get; private set; } {
        if (isAuthenticated) {
            visibleBizUnits = JSON.serialize(
            [SELECT Id, Name FROM Account WHERE RecordTypeId = :TM_Constants.BU_ACCOUNT_REC_TYPE_ID and TMM_Tracking__c = true]
            );
        } else {
            visibleBizUnits =  JSON.serialize(new List<Account>());
        }

    }
    public Pagereference init () {
        // build relayState parm by passingthru query parms
        Map<String, String> queryParms = ApexPages.currentPage().getParameters(); 
        String relayState = '/TMM';
        relayState += concatParms(queryParms);

        // build aloha redirect page for non authenticated security org users
        Boolean isSandbox = [select isSandbox from Organization limit 1].isSandbox;

        String fullAloha = 'https://security.my.salesforce.com//servlet/networks/session/create' 
                         + '?url=https%3A%2F%2Fsecurityorg.force.com{startURL}{urlparms}&site=0DM30000000CbAz'
                         + '&refURL=http%3A%2F%2Fsecurityorg.force.com%2FTMM%2Flogin&inst=3';        

        // if request contains startURL use it, else send to matrix
        if (queryParms.containsKey('startURL')) {
            fullAloha = fullAloha.replace('{startURL}', queryParms.get('startURL'));
            queryParms.remove('startURL');
        }   else fullAloha = fullAloha.replace('{startURL}', '%2FTMM');  

        // if request contains friendlyURL from url rewritter class use it, else use incoming queryparms.
        if (queryParms.containsKey('friendlyUrl')) fullAloha = fullAloha.replace('{urlparms}', queryParms.get('friendlyUrl'));
            else fullAloha = fullAloha.replace('{urlparms}', concatParms(queryParms));  

        pageReference alohaLogin = new pageReference(fullAloha);        

        // can't continue until user authenticates
        if (Auth.CommunitiesUtil.isGuestUser() || 
            !URL.getSalesforceBaseUrl().toExternalForm().contains('securityorg.force.com')) {

            // if aloha user bypass community login            
            if (ApexPages.currentPage().getCookies().containsKey('TMMalohaHint') && !isSandbox){ 
                return alohaLogin;                 
            } else if (queryParms.get('aloha') == '1' && !isSandbox)  { 
                return alohaLogin;
            } else if (Auth.CommunitiesUtil.isInternalUser() && !isSandbox)  { 
                return alohaLogin;
            } else {                    
                // not aloha user
                PageReference loginPage;
                // if already in community goto login page (if not authenticated) else redirect to community
                If (matrixBaseURL.contains(URL.getSalesforceBaseUrl().toExternalForm())) { 
                    if (Auth.CommunitiesUtil.isGuestUser()) loginPage = page.CommunitiesLogin;
                    else return null;              
                } else {
                    loginPage = new PageReference(matrixBaseURL); // redirect to community
                }
                loginPage.setRedirect(true); // load a new view state
                loginPage.getParameters().put('RelayState', relayState);
                if (queryParms.containsKey('friendlyUrl')) loginPage.getParameters().put('startURL', 'TMM' + queryParms.get('friendlyUrl'));
                    else loginPage.getParameters().put('startURL', 'TMM' + concatParms(queryParms));
                return loginPage;           
            }
            // already authenticated
            } else {
                return null;
            }
    }
        
    @RemoteAction
    public static List<TM_AbstractObjective__c> getAllAbstracts() {
    
        If (Schema.sObjectType.TM_AbstractObjective__c.isAccessible()) {
            return [SELECT Name,Id,UUID__c,Description__c,Why__c,Requirements__c,ImplementationAdvice__c,
                ValidationAdvice__c,WhoCanHelp__c,SeeAlso__c,Deprecates__c,Dependencies__c,Tags__c, Security_Control_ID__c,
                (SELECT Level__c,Category__c,Priority__c,Environment_Type__c FROM TM_Placements__r) 
                FROM TM_AbstractObjective__c];
        } else {
            return null;
        }
    }
    
    @RemoteAction
    public static List<TM_Objective__c> getAllConcretes(Id bizUnitId) {
        // Please ensure selected fields here are the same as in getConcretesByUuid:
        return [
            SELECT Id, Name, TM_Placement__c, Business_Unit__c, CSIRT_Environment__r.IRCloud__Account__c, Status__c,
              ValidationOwner__r.Id, ValidationOwner__r.Name, ValidationOwner__r.SmallPhotoUrl,
              ImplementationOwner__r.Id, ImplementationOwner__r.Name, ImplementationOwner__r.SmallPhotoUrl,
              SecurityOwner__r.Id, SecurityOwner__r.Name, SecurityOwner__r.SmallPhotoUrl
            FROM TM_Objective__c
            WHERE Business_Unit__c = :bizUnitId OR CSIRT_Environment__r.IRCloud__Account__c = :bizUnitId
        ];
    }
    
    @RemoteAction
    public static List<IRCloud__Environment__c> getAllEnvironments(Id bizUnitId) {
       return [select Id, Name, RecordType.DeveloperName FROM IRCloud__Environment__c WHERE IRCloud__Account__c = :bizUnitId];
    }

    @RemoteAction
    public static TM_AbstractObjective__c getAbstractByUuid(String uuid) {
    
        If (Schema.sObjectType.TM_AbstractObjective__c.isAccessible()) {
            return [SELECT Name,Id,UUID__c,Description__c,Why__c,Requirements__c,ImplementationAdvice__c,
                ValidationAdvice__c,WhoCanHelp__c,SeeAlso__c,Deprecates__c,Dependencies__c,Tags__c,
                (SELECT Level__c,Category__c,Priority__c,Environment_Type__c FROM TM_Placements__r) 
                FROM TM_AbstractObjective__c
                WHERE UUID__c = :uuid];
        } else {
            return null;
        }
    }
    
    @RemoteAction
    public static List<TM_Objective__c> getConcretesByUuid(Id bizUnitId, String uuid) {
        // Please ensure selected fields here are the same as in getAllConcretes:
        return [
            SELECT Id, Name, TM_Placement__c, Business_Unit__c, CSIRT_Environment__r.IRCloud__Account__c, Status__c,
              ValidationOwner__r.Id, ValidationOwner__r.Name, ValidationOwner__r.SmallPhotoUrl,
              ImplementationOwner__r.Id, ImplementationOwner__r.Name, ImplementationOwner__r.SmallPhotoUrl,
              SecurityOwner__r.Id, SecurityOwner__r.Name, SecurityOwner__r.SmallPhotoUrl
            FROM TM_Objective__c
            WHERE TM_Placement__r.TM_AbstractObjective__r.UUID__c = :uuid
              AND (Business_Unit__c = :bizUnitId OR CSIRT_Environment__r.IRCloud__Account__c = :bizUnitId)
        ];
    }
    
    public static String currentUserInfo { get; private set; } 
    {
        String id = UserInfo.getUserId();
        currentUserInfo = JSON.serialize([SELECT Id, Name, SmallPhotoUrl FROM User WHERE Id = :id]);
    }
    
    private static List<SObject> selectStar(String sobjectName, String whereClause) {
        /* An equivalent to SQL Select * FROM object WHERE clause
           Sample usage:
           selectStar(SObjectType.CSIRT_Environment__c.getName(),
                         'WHERE Account__c = \'' + String.escapeSingleQuotes(String.valueOf(bizUnitId)) + '\'');
        */
        Set<String> fieldNames = schema.describeSObjects(new List<String> {sobjectName})[0].fields.getMap().keyset();
        List<String> iterableFields = new List<String>(fieldNames);
        return Database.query(String.format('SELECT {0} FROM {1} {2}', 
                              new List<String> {String.join(iterableFields, ','), 
                              sobjectName, 
                              (whereClause == null? '' : whereClause)})); 
    }
    
    private string concatParms(Map<String,String> queryParms) {

        String urlParms = '';
        try {
            Integer parmNo = 1;
            for (String parm : queryParms.keySet()) {
                if (parm != 'sdtd') {
                    if (parmNo == 1) urlParms += '%3F';
                       else urlParms += '%26';
                    parmNo ++;
                    urlParms += parm + '%3D' + queryParms.get(parm);
                }
            }                
        } catch (exception e) {
            // fail silently if there are no url parms
        }
        
        return urlParms;
    
    }

}
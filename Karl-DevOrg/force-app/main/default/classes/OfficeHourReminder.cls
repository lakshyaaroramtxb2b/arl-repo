//add to scheduler:
//system.schedule('Office Hour Reminder', OfficeHourReminder.CRON_EXP, new OfficeHourReminder());
global class OfficeHourReminder implements Schedulable {
    public static void ScheduleMe(){
        string tsb = '0 ';
        string tsa = ' * ? * TUE,THU';
        system.schedule('Office Hours Scheduler (0)', tsb + '0' + tsa, new OfficeHourReminder());
        system.schedule('Office Hours Scheduler (15)', tsb + '15' + tsa, new OfficeHourReminder());
        system.schedule('Office Hours Scheduler (30)', tsb + '30' + tsa, new OfficeHourReminder());
        system.schedule('Office Hours Scheduler (45)', tsb + '45' + tsa, new OfficeHourReminder());
    }
    public static void doMail(){
       string email_to = 'appxsecurityreview@salesforce.com';
        integer threshold = 100 * 60 * 15; //every 15 minutes
        threshold = 408000; //debug
        List<Partner_Appointment__c> appts = [SELECT Id,Company__c,ProdSec_Member__c FROM Partner_Appointment__c WHERE Time_Until_Appointment__c <= :threshold AND Time_Until_Appointment__c > 0] ;
        for (Partner_Appointment__c appt : appts){
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

            mail.setToAddresses(new String[]{email_to});
            mail.setReplyTo('appxsecurityreview@salesforce.com');
            mail.setSenderDisplayName('OfficeHours');
            if (appt.ProdSec_Member__c == null || appt.ProdSec_Member__c == ''){
                //unclaimed office hour!
                mail.setSubject('Unclaimed upcoming Office Hour with ' + appt.Company__c);
                mail.setPlainTextBody('There is an unclaimed office hour appointment with ' + appt.Company__c + ' that is unclaimed!  You should claim it!\r\n' +
                'https://security.my.salesforce.com/' + appt.Id + ' \r\n');
                mail.setEmailPriority('High');
            } else {
                mail.setSubject('Claimed upcoming Office Hour with ' + appt.Company__c);
                mail.setPlainTextBody(appt.ProdSec_Member__c + 'has an upcoming office hour appointment with ' + appt.Company__c + '\r\n' +
                'https://security.my.salesforce.com/' + appt.Id + ' \r\n');
                mail.setEmailPriority('Low');
            }
            Messaging.sendEmail(new Messaging.SingleEmailMessage[]{mail});
  
        }
    }
    global void execute(SchedulableContext ctx) {
        OfficeHourReminder.doMail();
    }
}
/** 
 * @author Ben Harvie
 * @email ben.harvie@salesforce.com
 * @description This class provides the methods for generating the Followup Lightning Web Components.
 * Note that this was modelled on KARL_ARLReportController class. 
 */
public with sharing class KARL_FollowUpReportController {
    
    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Build the relevant report items for the reportwrapper, which get displayed via the LWC component 
     * Karl_Followup_Datatable. Note that this is a multi-object reportwrapper.
     * @param auditCycleId - record Id for audit cycle
     * @param auditScopeId - picklist value for audit scope
     * @param auditTeamId - record Id for the audit team (for auditorView = true)
     * @param auditorView - boolean value set by LWC metadata configuration
     * @param filterState - filter values for Type
     * @param filterAssigned - filter values for Assigned
     * @param filterAssignment - filter values for Assignment (Salesforce or Auditor) -- picklist
     * @return Report Wrapper contents for Wire data feed
     */
    @AuraEnabled(cacheable=true)
    public static List<ReportWrapper> getReport(String auditCycleId, String auditScopeId, String auditTeamId, Boolean auditorView, 
                                                String filterState, String filterAssigned, String filterAssignment){

        List<ReportWrapper> items = new List<ReportWrapper>();
        // Step 1: Build Custom Query for State Toggle (Cycle_Request_Status__c)
        Map<String, String> stateMapInternal = new Map<String, String>{
            'all' => '',
            'new' => 'New',
            'triaged' => 'Triaged',
            'inProgress' => 'In Progress',
            'readyForReview' => 'Ready for Review',
            'closed' => 'Closed',
            'followUp' => 'Follow-up'
        };

        String queryFilter = '';
        String queryState = stateMapInternal.get(filterState);
        if(filterState != 'all' && filterState != 'open'){
            queryFilter = 'AND KARL_Follow_up_Status__c =: queryState ';
        }else if (filterState == 'open'){
            // List of non-closed statuses for querying
            List<String> openStates = new List<String>{
                'New',
                'Triaged',
                'In Progress',
                'Ready for Review',
                'Follow-up'
            };
            queryFilter = 'AND KARL_Follow_up_Status__c in: openStates ';
        }

        String queryAssigned = '';
        if(filterAssigned != 'allAssigned'){
            String currentUserEmail = userInfo.getUserEmail();
            queryAssigned = 'AND (KARL_Followup_Auditor_POC__r.Email =: currentUserEmail OR KARL_Followup_SCEA_POC__r.Email =: currentUserEmail) ';
        }
        
        String queryAssignment = '';
        String currentAssignment = '';
        if(filterAssignment == 'currentSalesforce'){
            currentAssignment = 'Salesforce';
            queryAssignment = 'AND KARL_Current_Assignment__c =: currentAssignment ';
        }else if (filterAssignment == 'currentAuditor'){
            currentAssignment = 'Auditor';
            queryAssignment = 'AND KARL_Current_Assignment__c =: currentAssignment ';
        }
        
        // Step 3: Determine where we are coming from, to generate the links appropriately for Community vs. Salesforce
        Network karlNetwork;
        String auditorloginurl = '';
        String auditorcommunityHomeUrl = '';
        Integer index;
        if(!Test.isRunningTest()){
            karlNetwork = [SELECT Id FROM Network WHERE Name = 'KARL Community' WITH SECURITY_ENFORCED];
            auditorloginurl = Network.getLoginUrl(karlNetwork.Id);
            index = auditorloginurl.lastIndexOf('/login');
            auditorcommunityHomeUrl = auditorloginurl.substring(0, index + 1);
            //System.debug('MyAuditorDebugURL: ' + auditorcommunityHomeUrl);
        }

        String baseUrl = '';
        String baseRecordURL = '';
        String baseUrlSuffix = '';
        // Check if we're on the KARL Community or not
        if (Site.getSiteId() != null) {
            baseUrl = auditorcommunityHomeUrl; // we're on the community
            baseRecordURL = baseUrl + 'detail/';
        } else {
            baseUrl = URL.getSalesforceBaseUrl().toExternalForm();
            baseRecordURL = baseUrl + '/one/one.app?#/sObject/'; // force rendering in Lightning instead of Classic
            baseUrlSuffix = '/view'; // Required for Lightning record view
        }

        String scopeFilter = '';
        if(auditScopeId != 'allScopes' && auditScopeId != 'noScopes'){
            scopeFilter = 'AND KARL_Audit_Scope__c =: auditScopeId ';
        }else{
            scopeFilter = 'AND KARL_Audit_Scope__c !=: auditScopeId ';
        }

        // Step 1: Query all Follow-ups (filtered for Audit Team) this audit Cycle
        for (KARL_Cycle_Follow_up__c cycleFollowups : Database.query('SELECT Id, Name, CreatedDate, KARL_Assigned_to_me__c, '+
                                                                'KARL_Followup_Auditor_POC__c, KARL_Followup_SCEA_POC__c, '+
                                                                'KARL_Followup_Auditor_POC__r.FirstName, KARL_Followup_Auditor_POC__r.LastName, '+
                                                                'KARL_Followup_SCEA_POC__r.FirstName, KARL_Followup_SCEA_POC__r.LastName, '+
                                                                'KARL_Current_Assignment__c, KARL_Request_Master_Data__c, '+
                                                                'KARL_Followup_Cycle_Request_Item__c, KARL_Age__c, '+
                                                                'KARL_Followup_Description__c, KARL_Follow_up_Type__c, '+
                                                                'KARL_Evidence_Link__c, KARL_Follow_up_Status__c, KARL_Audit_Scope__c, '+
                                                                'KARL_Salesforce_Comments__c, KARL_Auditor_Comments__c, Potential_issue__c '+
                                                                'FROM KARL_Cycle_Follow_up__c '+
                                                                'WHERE KARL_Followup_Audit_Cycle__c =: auditCycleId ' + queryFilter +
                                                                'AND KARL_Followup_Audit_Team__c =: auditTeamId '+ queryAssigned +
                                                                scopeFilter + queryAssignment +
                                                                'WITH SECURITY_ENFORCED')){
            
            ReportWrapper rw    = new ReportWrapper();
            // Primary Information
            rw.Id               = cycleFollowups.Id;
            rw.pscopeName       = cycleFollowups.KARL_Audit_Scope__c;
            rw.followupCreated  = cycleFollowups.CreatedDate;
            rw.followupName     = cycleFollowups.Name;
            rw.followupLink     = baseRecordURL + cycleFollowups.Id + baseUrlSuffix; 
            rw.followupDesc     = cycleFollowups.KARL_Followup_Description__c;
            rw.followupType     = cycleFollowups.KARL_Follow_up_Type__c;
            rw.followupStatus   = cycleFollowups.KARL_Follow_up_Status__c;
            rw.followupAssign   = cycleFollowups.KARL_Current_Assignment__c;
            rw.followupAge      = cycleFollowups.KARL_Age__c;
            rw.potentialIssue   = cycleFollowups.Potential_issue__c ? 'action:record' : '';
            
            // Status Comments
            rw.commentsSFDC     = cycleFollowups.KARL_Salesforce_Comments__c;
            rw.commentsAuditor  = cycleFollowups.KARL_Auditor_Comments__c;

            // Contact Information
            String grcFullNameAuditor   = 'Unassigned';
            if(!String.isBlank(cycleFollowups.KARL_Followup_Auditor_POC__c)){
                grcFullNameAuditor = cycleFollowups.KARL_Followup_Auditor_POC__r.FirstName + ' ' + cycleFollowups.KARL_Followup_Auditor_POC__r.LastName;
            }
            String grcFullNameInternal  = 'Unassigned';
            if(!String.isBlank(cycleFollowups.KARL_Followup_SCEA_POC__c)){
                grcFullNameInternal = cycleFollowups.KARL_Followup_SCEA_POC__r.FirstName + ' ' + cycleFollowups.KARL_Followup_SCEA_POC__r.LastName;
            }

            rw.pocAuditor       = grcFullNameAuditor;
            rw.pocGRC           = grcFullNameInternal;
                                                                    
            // Related Information
            rw.relatedReq      = cycleFollowups.KARL_Request_Master_Data__c;
            rw.relatedCreq     = cycleFollowups.KARL_Followup_Cycle_Request_Item__c;
            rw.relatedEvidence = cycleFollowups.KARL_Evidence_Link__c;

            items.add(rw);
        }
        return items;


    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description This is a wrapper class to capture the ARL line items at a row level.
     * @return nothing
     */
    public class ReportWrapper{
        @AuraEnabled public String Id;
        @AuraEnabled public Datetime followupCreated;
        @AuraEnabled public String pscopeName;
        @AuraEnabled public String followupName;
        @AuraEnabled public String followupDesc;
        @AuraEnabled public String followupLink;
        @AuraEnabled public String pocAuditor;
        @AuraEnabled public String pocGRC;
        @AuraEnabled public String followupType;
        @AuraEnabled public String followupStatus;
        @AuraEnabled public String followupAssign;
        @AuraEnabled public Decimal followupAge;
        @AuraEnabled public String potentialIssue;
        @AuraEnabled public String commentsSFDC;
        @AuraEnabled public String commentsAuditor;
        @AuraEnabled public String relatedCreq;
        @AuraEnabled public String relatedEvidence;
        @AuraEnabled public String relatedReq;
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Apex driven update function for multi-line edits from the LWC. Expects that the 
     * fieldName is the key with the value being passed as the value, for JSON serialization.
     * @param data
     * @return message of success or failure
     */
    @AuraEnabled
    public static String updateFollowup(Object data){
        List<KARL_Cycle_Follow_up__c> recordsForUpdate = (List<KARL_Cycle_Follow_up__c>)JSON.deserialize(
            JSON.serialize(data),
            List<KARL_Cycle_Follow_up__c>.class
        );
        String msg = '';
        try {
            if(Schema.sObjectType.KARL_Cycle_Follow_up__c.isUpdateable() 
                    && KARL_Cycle_Follow_up__c.KARL_Follow_up_Status__c.getDescribe().isUpdateable()
                    && KARL_Cycle_Follow_up__c.KARL_Current_Assignment__c.getDescribe().isUpdateable()){
                        update recordsForUpdate;
                        return 'Success: KARL records updated successfully';
            }else{
                return 'Error: Insufficient permissions to update records.';
            }
        }catch(DmlException e){
            //Any type of Validation Rule error message, Required field missing error message, Trigger error message etc..
            //we can get from DmlException
             
            //Get All DML Messages
            for (Integer i = 0; i < e.getNumDml(); i++) {
                //Get Validation Rule & Trigger Error Messages
                msg =+ e.getDmlMessage(i) +  '\n' ;
            }
            //throw DML exception message
            throw new AuraHandledException(msg);
             
        }catch (Exception e) {
            throw new AuraHandledException(''+e.getMessage());
            //return 'The following exception has occurred: ' + e.getMessage();
        }
    }
}
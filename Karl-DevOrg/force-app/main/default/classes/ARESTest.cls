public class ARESTest {
    //static IRCloudARES__ARES_Endpoint__mdt endpointMeta = [SELECT IRCloudARES__Endpoint_Root_URL__c, IRCloudARES__API_token__c FROM IRCloudARES__ARES_Endpoint__mdt WHERE IRCloudARES__Test_Context__c = :TestContext.testCase LIMIT 1];
    static IRCloudARES__ARES_Endpoint__mdt endpointMeta = [SELECT IRCloudARES__Endpoint_Root_URL__c, IRCloudARES__API_token__c FROM IRCloudARES__ARES_Endpoint__mdt LIMIT 1];
    static String endpointURL = endpointMeta.IRCloudARES__Endpoint_Root_URL__c;
    static String apiToken = endpointMeta.IRCloudARES__API_token__c;
    
    private class InitTask{
        public Id objid;
        public Id skillid;
        public String object_type;
        
        public InitTask(Id oid, Id sid, Boolean csirtTask){
            objid = oid;
            skillid = sid;
            object_type = 'Case';
            if (csirtTask){
                object_type = 'Task';
            }
        }
    }
    
    private static boolean evaluateTask(Id objectid){
        Schema.sObjectType entityType = objectid.getSObjectType();
        if (entityType == IRCloud__CSIRT_Task__c.sObjectType){
            return True;
        } else {
            return False;
        }
    }
    
    private static boolean evaluateCase(Id objectid){
        Schema.sObjectType entityType = objectid.getSObjectType();
        if (entityType == Case.sObjectType){
            return True;
        } else{
            return False;
        }
    }
    
    public static HttpResponse callARES(Id objectid, Id skillid, String route){
        String reqURL;
        if(route.equalsIgnoreCase('Internal')){
            reqURL = endpointURL + '/route/internal';
        } else if(route.equalsIgnoreCase('Non-Attributal')){
            reqURL = endpointURL + '/route/nonattrib';
        } else if(route.equalsIgnoreCase('Default')){
            reqURL = endpointURL + '/route/default';
        } else {
            System.debug('ARES - postSkill - This skill is unrouted');
             return null;//Return because we don't want to default to the default route in case a mistake was made in calling and it was supposed to be non-attributal or internal
        }


        Boolean csirtTask = evaluateTask(objectid);
        Boolean csirtCase = evaluateCase(objectid);
        
        String postData;
        if(csirtTask){
            InitTask ctask = new InitTask(objectid, skillid, True);
            postData = JSON.serializePretty(ctask);
        } else {
            InitTask ctask = new InitTask(objectid, skillid, False);
            postData = JSON.serializePretty(ctask);
        }
        
        System.debug('IRCLOUD-ARES JSON OUT: ' + postData);
        System.debug('IRCLOUD-ARES REQUEST URL: ' + reqURL);
        
        // Initiate Callout   
        HttpRequest req = new HttpRequest();
        req.setMethod('POST');
        req.setEndpoint(reqURL);
        req.setHeader('Content-Type', 'application/json');
        req.setHeader('Authorization', 'Token ' + apiToken);
        req.setBody(postData);    

        Http http = new Http();
        HTTPResponse res = http.send(req);
        
        System.debug('IRCLOUD-ARES HTTP-RESPONSE: ' + res.getStatusCode());

        if (res.getStatusCode() != 200) {
            System.debug('Error from ' + req.getEndpoint() + ' : ' +
              res.getStatusCode() + ' ' + res.getStatus());
        }
        
        return res;
    }
    
    @future (callout=True)
    public static void postSkill(Id objectid, Id skillid, String route){
        callARES(objectid, skillid, route);
    }
}
global with sharing class HW_OspreyNotificationHandler implements Database.Batchable<SObject>, Database.AllowsCallouts, Database.Stateful{
    public String query;
    public Set<String> teams;
    public String alertType;
    global Database.QueryLocator start(Database.BatchableContext bc){
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext bc, List<SObject> scope){
        try{
            for(SObject rec : scope){
                Heroku_Warden_users__c obj = (Heroku_Warden_users__c)rec;
                HttpRequest req = new HttpRequest();
                req.setHeader('Content-Type', 'application/json');
                req.setHeader('Accept', 'application/vnd.heroku+json; version=3');
                if(alertType == 'removeInactiveUsers'){
                    //send call outs to Heroku API to delete user accounts
                    req.setMethod('DELETE');
                    String uripath = '/teams/'+(String)obj.Heroku_Warden_organizations__r.name__c+'/members/'+(String)obj.email__c;
                    req.setEndpoint('callout:herokuwarden_mad'+uripath);
                    //System.debug(uripath);
                }
                //req.setBody('{"description":"sample authorization"}');
                Http http = new Http();
                HttpResponse res = http.send(req);
                System.debug(res.getBody());
            }
        }catch(Exception ex){
            System.debug('Exception occured'+ex.getMessage()+ex.getStackTraceString());
        }
    }
    global void finish(Database.BatchableContext bc){}

}
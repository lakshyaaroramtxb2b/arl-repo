public with sharing class EmailTemplatesPageController {
	public List<EmailTemplate> emailTempList {get;set;}

	public EmailTemplatesPageController() {
		emailTempList = [SELECT Id, Name, DeveloperName, Description, CreatedDate, 
							CreatedBy.Name, LastModifiedDate, LastModifiedBy.Name
						FROM EmailTemplate
						WHERE FolderId = '00l3A000002btOkQAI'
						ORDER BY Name ASC];	
	}
}
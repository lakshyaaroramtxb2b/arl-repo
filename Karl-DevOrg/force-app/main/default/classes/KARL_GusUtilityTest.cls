/**
* @author Mahima Aggarwal
* @email mahima.aggarwal@mtxb2b.com
* @description Test class for KARL_GusUtility
*/
@isTest
public class KARL_GusUtilityTest {
     /**
    * @author Lakshya Arora
    * @email lakshya.arora@mtxb2b.com
    * @desc method to create the test data.
    */   
	@testSetup
    private static void makeData(){
        User opsAdminUser = ARL_TestDataFactory.createOpsAdmin();
        System.runAs(opsAdminUser){
            Account acc = new Account(Name = 'salesforce.com - ESA Office Hours');
            insert acc;
            
            Contact con = new Contact(LastName = 'test', AccountId = acc.Id,Email = 'test@gmail.com');
            insert con;
            
            Contact con2 = new Contact(LastName = 'test2', AccountId = acc.Id,Email = 'test2@gmail.com');
            insert con2;
            
            Request_Item__c reqItem = ARL_TestDataFactory.createRequestItem();
            reqItem.Primary_Scope__c = 'SS';
            reqItem.KARL_Evidence_Submission_Workflow__c = true;
            reqItem.Create_GUS_Case__c = true;
            insert reqItem;
            
            Audit_Cycle__c auditCycle = ARL_TestDataFactory.createAuditCycle();
            insert auditCycle;
            
            
            KARL_Gus_Product_Tag__c defaultProductTag = ARL_TestDataFactory.createKarlGusProductTag(true,true);
            insert defaultProductTag;
            
            List<KARL_Auto_Assignment_Configuration__c> autoAssignmentConfigList = ARL_TestDataFactory.createAutoAssignmentConfig(defaultProductTag.Id,4);
            autoAssignmentConfigList[1].Priority_0_Assignment_Date__c = System.today().addDays(5);
            autoAssignmentConfigList[2].Priority_0_Assignment_Date__c = System.today().addDays(-1);
            autoAssignmentConfigList[3].Priority_0_Assignment_Date__c = System.today();
            insert autoAssignmentConfigList;
            
            List<Audit_Cycle_Coverage__c> auditCycleCoverageList = new List<Audit_Cycle_Coverage__c>();
            Map<Integer,String> priorityToScopeMap = new Map<Integer,String>();
            priorityToScopeMap.put(0,'AS');
            priorityToScopeMap.put(1,'AUST');
            priorityToScopeMap.put(2,'B2BC');
            priorityToScopeMap.put(3,'CHAT');
            for(Integer i=0;i<4;i++){
                Audit_Cycle_Coverage__c auditCycleCoverage = new Audit_Cycle_Coverage__c();
                auditCycleCoverage.Audit_Cycle__c = auditCycle.Id;
                auditCycleCoverage.Area__c = 'iRAP';
                auditCycleCoverage.Scope__c = priorityToScopeMap.get(i);
                auditCycleCoverage.Auto_Assignment_Configuration__c = autoAssignmentConfigList[i].Id;
                auditCycleCoverageList.add(auditCycleCoverage);
            }
            insert auditCycleCoverageList;
            
            
            KARL_Audit_Team__c auditTeam = ARL_TestDataFactory.createAuditTeam('test');
            insert auditTeam;
            
            KARL_Audit_Team_Contacts__c auditTeamCon = ARL_TestDataFactory.createAuditTeamContact(auditTeam.Id,con.Id);
            insert auditTeamCon;
            
            List<Cycle_Request_Item__c> cycleReqItemList = new List<Cycle_Request_Item__c>();
            for(Integer i=0;i<4;i++){
                Cycle_Request_Item__c cycleReqItem = ARL_TestDataFactory.createCycleRequestItem(reqItem.id,null);  
                cycleReqItem.Cycle_Request_Status__c = 'New';
                cycleReqItem.KARL_Update_GUS__c = true;
                cycleReqItem.Request_Tech_Details__c = 'tech details old';
                cycleReqItem.Request_Assignee__c = con.Id;
                cycleReqItem.Audit_Cycle__c = auditCycle.Id;
                cycleReqItem.Audit_Cycle_Coverage__c = auditCycleCoverageList[i].Id;
                cycleReqItem.Request_GUS__c = null;
                cycleReqItemList.add(cycleReqItem);
            }
            insert cycleReqItemList;
        }
    }
    
     /**
    * @author Lakshya Arora
    * @email lakshya.arora@mtxb2b.com
    * @desc method to test the processGUSRecordCreationFunctionality
    */   
	@isTest
    private static void processGUSRecordCreationTest(){
        List<User> opsAdminUserList = ARL_TestDataFactory.fetchUser('abc@b.com.d');
        if(!opsAdminUserList.isEmpty()){
            System.runAs(opsAdminUserList[0]){
                Set<Id> auditCycleIdSet = new Set<Id>();
                Set<Id> cycleReqItemIds = new Set<Id>();
                Set<Id> dependentCycleItemsIdSet = new Set<Id>();
                for(Audit_Cycle__c auditCycleObj : [SELECT Id FROM Audit_Cycle__c]){
                    auditCycleIdSet.add(auditCycleObj.Id);
                }
                for(Cycle_Request_Item__c cycleReqItemObj : [SELECT Id,Audit_Cycle_Coverage__r.Auto_Assignment_Configuration__r.Priority_0_Assignment_Date__c
                                                             FROM Cycle_Request_Item__c]){
                                                                 if(cycleReqItemObj.Audit_Cycle_Coverage__r.Auto_Assignment_Configuration__r.Priority_0_Assignment_Date__c <= System.today()){
                                                                     cycleReqItemIds.add(cycleReqItemObj.Id);
                                                                 }
                                                                 else{
                                                                     dependentCycleItemsIdSet.add(cycleReqItemObj.Id);
                                                                 }
                                                             }
                Test.startTest();
                KARL_GusUtility.processGUSRecordCreation(cycleReqItemIds,auditCycleIdSet,dependentCycleItemsIdSet);
                Test.stopTest();
                System.assert(![SELECT Id FROM KARL_GUS_SLA_Tracking__c].isEmpty());
            }    
        }
    }
}
/**
 * @author Lakshya Arora
 * @email lakshya.arora@mtxb2b.com
 * @description This is trigger handler class which handles the operations before and after any DML transaction on Audit Scope
 * @note As we have external sharing model as private for Audit Scope the trigger should have accessed to all records for maintaining the 1-1 relationship,
 *  that's why inherited sharing is used as trigger runs in system mode
*/
public inherited sharing class KARL_AuditScopeTriggerHandler{
    /**
    * @author Lakshya Arora
    * @email lakshya.arora@mtxb2b.com
    * @description Handles before insert operations
    * @param New List of KARL_Audit_Scope_Master_Data__c
    */
    public static void beforeInsertOperations(List<KARL_Audit_Scope_Master_Data__c> newList){
        KARL_AuditScopeTriggerHelper.uniqueAuditScopeValidation(newList,null);
    }

     /**
    * @author Lakshya Arora
    * @email lakshya.arora@mtxb2b.com
    * @description Handles before update operations
    * @param1 New List of KARL_Audit_Scope_Master_Data__c
    * @param2 OldMap -> Map of Id vs old KARL_Audit_Scope_Master_Data__c record info
    */
    public static void beforeUpdateOperations(List<KARL_Audit_Scope_Master_Data__c> newList,Map<Id,KARL_Audit_Scope_Master_Data__c> oldMap){
        KARL_AuditScopeTriggerHelper.uniqueAuditScopeValidation(newList,oldMap);
    }
}
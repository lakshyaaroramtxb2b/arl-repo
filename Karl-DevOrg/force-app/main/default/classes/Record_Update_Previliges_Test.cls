@isTest
public class Record_Update_Previliges_Test {

   @isTest
   static void RecordUpdateTest() {
       
      Test.startTest();
      Profile P = [SELECT Id FROM Profile WHERE Name='InfoSecBasic'];

      User U = New User(LastName='uppTest',FirstName='surTest',Email='suresh.uppala@salesforce.com',username='suptest9182@salesforce.com',profileId=P.Id,Alias='su9182',
                        EmailEncodingKey='UTF-8',LanguageLocaleKey='en_US',LocaleSidKey='en_US',TimeZoneSidKey='America/Los_Angeles');

      Insert U;
      inserUserPerms(U.Id);
      Test.stopTest();

       List<Esa_Entity__C> lstEsaEntity = new List<Esa_Entity__c>();
       Esa_Entity__c esaEntity = new Esa_Entity__c(Name='TestEntity1',EntityCode__c='Entity1');
       lstEsaEntity.add(esaEntity);
       esaEntity = new Esa_Entity__c(Name='TestEntity2',EntityCode__c='Entity2');
       lstEsaEntity.add(esaEntity);

       Insert lstEsaEntity;

       List<ESA_Service_Item_Category__c> lstItmCategory = new List<ESA_Service_Item_Category__c>();

       ESA_Service_Item_Category__c itmCategory = new ESA_Service_Item_Category__c(Name='TestCategory1',Esa_Entity__c=lstEsaEntity[0].Id);
       lstItmCategory.add(itmCategory);
       itmCategory = new ESA_Service_Item_Category__c(Name='TestCategory2',Esa_Entity__c=lstEsaEntity[1].Id);
       lstItmCategory.add(itmCategory);

       Insert lstItmCategory;


       List<ESA_Service_Item__c> lstServiceItems = new List<ESA_Service_Item__c>();
       ESA_Service_Item__c serviceItem = new ESA_Service_Item__c(Name='Service Item 1',ItemCategory__c=lstItmCategory[0].Id);
       lstServiceItems.add(serviceItem);
       serviceItem = new ESA_Service_Item__c(Name='Service Item 1',ItemCategory__c=lstItmCategory[1].Id);
       lstServiceItems.add(serviceItem);

       Insert lstServiceItems;

       List<ESA_Security_Question__c> lstQuestions = new List<ESA_Security_Question__c>();
       ESA_Security_Question__c question = new ESA_Security_Question__c(Question__c='My Test Question 1',Data_Type__c = 'Text (255)');
       lstQuestions.add(question);
       question = new ESA_Security_Question__c(Question__c='My Test Question 2',Data_Type__c = 'Text (255)');
       lstQuestions.add(question);

       Insert lstQuestions;

       List<ESA_Service_Item_Question__c> lstSitemQuestions = new List<ESA_Service_Item_Question__c>();
       ESA_Service_Item_Question__c esItemQue = new ESA_Service_Item_Question__c(ESA_Service_Item__c=lstServiceItems[0].Id,ESA_Security_Question__c=lstQuestions[0].Id);
       lstSitemQuestions.add(esItemQue);
       esItemQue = new ESA_Service_Item_Question__c(ESA_Service_Item__c=lstServiceItems[0].Id,ESA_Security_Question__c=lstQuestions[0].Id);
       lstSitemQuestions.add(esItemQue);
       esItemQue = new ESA_Service_Item_Question__c(ESA_Service_Item__c=lstServiceItems[1].Id,ESA_Security_Question__c=lstQuestions[1].Id);
       lstSitemQuestions.add(esItemQue);

       Insert lstSitemQuestions;

       update lstQuestions[0];

       //User U = [SELECT Id FROM USER WHERE username='suptest9182@salesforce.com' LIMIT 1];

       system.runAs(U) {

        // check the permissions for ESA_Entity__c Object.

        update lstEsaEntity[0];

        try {
          update lstEsaEntity[1];
        }Catch(Exception ex){
            system.assertEquals(True, ex.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION'),'Expecting Custom Validation Exception');
        }

        // Check the Permissions for ESA_Service_Item_Category__c Object

        update lstItmCategory[0];

        try {
          update lstItmCategory[1];
        }Catch(Exception ex){
            system.assertEquals(True, ex.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION'),'Expecting Custom Validation Exception');
        } 

        // Check the Permissions for ESA_Service_Item__c object

        update lstServiceItems[0];

        try {
          update lstServiceItems[1];
        }Catch(Exception ex){
            system.assertEquals(True, ex.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION'),'Expecting Custom Validation Exception');
        } 

        // Check the permissions for ESA_Security_Question__c Object.

        update lstQuestions[0]; 
        try {
            update lstQuestions[1];
            system.assertEquals(True, False,'Expecting a Custom Validation Exception...');
        }Catch(Exception ex){
            system.assertEquals(True, ex.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION'),'Expecting Custom Validation Exception');
        }

        // Check the permissions for ESA_Service_Item_Question__c Object

        delete lstSitemQuestions[0];

        try {
          delete lstSitemQuestions[2];
          system.assertEquals(True, False,'Expecting a Custom Validation Exception...');
        }Catch(Exception ex) {
            Boolean errorExpected = false;
            String errMessage = ex.getMessage();
            if(errMessage.contains('INSUFFICIENT_ACCESS_OR_READONLY') || errMessage.contains('FIELD_CUSTOM_VALIDATION_EXCEPTION')) {
              errorExpected = true;
            }
            system.assertEquals(True, errorExpected,'This is Expected Validation Exception');
          }
       }
   }

   @future
    public static void inserUserPerms(Id uId) {
      PermissionSet ps = [SELECT ID From PermissionSet WHERE Name = 'ESA_Security_Request_Admin'];
      insert new PermissionSetAssignment(AssigneeId = uId, PermissionSetId = ps.Id );

      ps = [SELECT ID From PermissionSet WHERE Name = 'ESA_Security_Request_User'];
      insert new PermissionSetAssignment(AssigneeId = uId, PermissionSetId = ps.Id );

      Group usrGrp = new Group(Name = 'ESA_OH_Entity1');
      insert usrGrp;

      insert new GroupMember(GroupId = usrGrp.Id, UserOrGroupId = uId);

      insert new Group(Name='ESA_OH_Entity2');
    }
}
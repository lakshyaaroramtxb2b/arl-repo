public class TestProxy {
    public String getScary(){
        HttpRequest req = new HttpRequest();

        req.setMethod('GET');
        
        
        req.setEndpoint('https://apm1-was.ops.sfdc.net/extrahop');
        //req.setEndpoint('http://public-proxy1-0-sjl.data.sfdc.net:8080/nagios/cgi-bin/status.cgi?host=all&servicestatustypes=28');
        //req.setEndpoint('http://na15-monitor1-1-was.ops.sfdc.net/nagios/cgi-bin/status.cgi?host=all&servicestatustypes=28');
      
        Http http = new Http();
        HTTPResponse res = http.send(req);
        return res.getBody();

    }
}
@isTest
public class IEM_ExecutiveSummaryComponentContTest {
	
    static testMethod void getRecordTest() {
		Map<String,Map<String,String>> dependenciesMap = new Map<String,Map<String,String>>();
        Profile profile = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User userRec = new User(Alias = 'standt', Email='standarduser@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = profile.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='standarduser994@testorg.com');
        insert userRec;
        List<Case> caseList = IEM_TestDataFactory.createCaseRecords(1,true);
        test.startTest();
       // System.runAs(userRec) {
        	Case retrievedRecord = new Case();
            retrievedRecord = IEM_ExecutiveSummaryComponentController.getRecord(caseList[0].id);
            system.assertEquals(retrievedRecord.id, caseList[0].id);
       // }
        test.stopTest();
    }
    static testMethod void getMileStonesTest() {
		Map<String,Map<String,String>> dependenciesMap = new Map<String,Map<String,String>>();
        Profile profile = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User userRec = new User(Alias = 'standt', Email='standarduser@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = profile.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='standarduser994@testorg.com');
        insert userRec;
        List<Case> caseList = IEM_TestDataFactory.createCaseRecords(1,true);
        List<Milestone__c> milestoneList = IEM_TestDataFactory.createMilestoneRecords(5,caseList[0].id,true);
        test.startTest();
        System.runAs(userRec) {
        	IEM_ExecutiveSummaryComponentController.MilestoneWrapper retrievedRecord = 
                new IEM_ExecutiveSummaryComponentController.MilestoneWrapper();
            retrievedRecord = IEM_ExecutiveSummaryComponentController.getMileStones(caseList[0].id);
            //system.assert(retrievedRecord.milestoneList!=null && !retrievedRecord.milestoneList.isEmpty());
        }
        test.stopTest();
    }
}
/***********************************************************
* Handler for ProgramTrigger
* Created by 	- Prashant Gupta
* Date - 09-12-2019
************************************************************/
public class PRT_ProgramTriggerHandler {
    
    /***********************************************************
    * Created by 	- Prashant Gupta
    * Date 		- 09-12-2019
    * Before Update method Being Called from Trigger
    ************************************************************/
    public static void beforeUpdate(List<Program__c> newList, Map<id,Program__c> newMap){
        PRT_ProgramTriggerHelper.setSubProgramName(newList,newMap);
    }
    
    /***********************************************************
    * Created by 	- Prashant Gupta
    * Date 		- 09-12-2019
    * Before Insert method Being Called from Trigger
    ************************************************************/
    public static void beforeInsert(List<Program__c> newList){     
    }
    
    /***********************************************************
    * Created by 	- Prashant Gupta
    * Date 		- 09-12-2019
    * After Update method Being Called from Trigger
    ************************************************************/
    public static void afterUpdate(List<Program__c> newList, Map<id,Program__c> oldMap){ 
        
        PRT_ProgramTriggerHelper.setBudgetAllocationToPortfolio(newList,oldMap); 
        system.debug('recursive' + PRT_Constants.RECURSIVE_TRIGGER_CHECK);
        IF(PRT_Constants.RECURSIVE_TRIGGER_CHECK){
            PRT_Constants.RECURSIVE_TRIGGER_CHECK =FALSE;
            
            // Please Ensure that tHe Future Method Call will always be at the bottom of the call list.            
            if(!System.isBatch() && !System.isFuture() && PRT_Constants.INTEGRATION_SETTING.GUS_Program_Integration__c){
                PRT_ProgramTriggerHelper.updateInsertGUSRecords(newList,oldMap);            
            }   
        }
    }
    
    /***********************************************************
    * Created by 	- Prashant Gupta
    * Date 		- 09-12-2019
    * After Insert method Being Called from Trigger
    ************************************************************/
    public static void afterInsert(List<Program__c> newList){
        PRT_ProgramTriggerHelper.setBudgetAllocationToPortfolio(newList,null);         
        // Please Ensure that tHe Future Method Call will always be at the bottom of the call list.
        if(!System.isBatch() && !System.isFuture() && PRT_Constants.INTEGRATION_SETTING.GUS_Program_Integration__c){
            PRT_ProgramTriggerHelper.updateInsertGUSRecords(newList,null);            
        } 
    }
}
@isTest
public class PRT_SLADueDateBatchTest {
    @TestSetup
    static void makeData(){
        SLA__c slaRecord = new SLA__c();
        slaRecord.Due_Date__c = Date.today();
        slaRecord.Reporting_Period__c = 'Daily';
        slaRecord.Automated__c = true;
        INSERT slaRecord;
        SLA__c slaRecord2 = new SLA__c();
        slaRecord2.Due_Date__c = Date.today();
        slaRecord2.Reporting_Period__c = 'Weekly';
        INSERT slaRecord2;
        SLA__c slaRecord3 = new SLA__c();
        slaRecord3.Due_Date__c = Date.today();
        slaRecord3.Reporting_Period__c = 'Monthly';
        INSERT slaRecord3;
        SLA__c slaRecord4 = new SLA__c();
        slaRecord4.Due_Date__c = Date.today();
        slaRecord4.Reporting_Period__c = 'Quarterly';
        INSERT slaRecord4;
        SLA__c slaRecord5 = new SLA__c();
        slaRecord5.Due_Date__c = Date.today();
        slaRecord5.Reporting_Period__c = 'Annually';
        INSERT slaRecord5;

        SLA_Metric__c slaMetric = new SLA_Metric__c();
        slaMetric.SLA__c = slaRecord.Id;
        slaMetric.Due_Date__c = Date.today();
        INSERT slaMetric;
        SLA_Metric__c slaMetric2 = new SLA_Metric__c();
        slaMetric2.SLA__c = slaRecord2.Id;
        slaMetric2.Due_Date__c = Date.today();
        INSERT slaMetric2;
        SLA_Metric__c slaMetric3 = new SLA_Metric__c();
        slaMetric3.SLA__c = slaRecord3.Id;
        slaMetric3.Due_Date__c = Date.today();
        INSERT slaMetric3;
        SLA_Metric__c slaMetric4 = new SLA_Metric__c();
        slaMetric4.SLA__c = slaRecord4.Id;
        slaMetric4.Due_Date__c = Date.today();
        INSERT slaMetric4;
        SLA_Metric__c slaMetric5 = new SLA_Metric__c();
        slaMetric5.SLA__c = slaRecord5.Id;
        slaMetric5.Due_Date__c = Date.today();
        INSERT slaMetric5;
    }

    @isTest
    static void slaDueDateBatchTest() {
        List<SLA__c> slaList = new List<SLA__c>([SELECT Id, Reporting_Period__c, Reporting_Start_Date__c, Due_Date__c, Operation__c FROM SLA__c]);
        PRT_SLADueDateBatch.mockSLAList.addAll(slaList);
        Test.startTest();
            Database.executeBatch(new PRT_SLADueDateBatch());
        Test.stopTest();
    }
}
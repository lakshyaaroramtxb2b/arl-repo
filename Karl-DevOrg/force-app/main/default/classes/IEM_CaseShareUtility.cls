public class IEM_CaseShareUtility {
    
    public static Map<Id,String> getUserName(List<caseShare> caseShareRecordList){
        Map<Id,String> userIdToNameMap = new Map<Id,String>();
        List<Id> userIdList = new List<Id>();
        for(CaseShare caseShareRec:caseShareRecordList){
            if(caseShareRec.UserOrGroupId!=null){
                userIdList.add(caseShareRec.UserOrGroupId);
            }
        }
        for(User u: [SELECT id, Name FROM User WHERE id=:userIdList LIMIT 100]){
            userIdToNameMap.put(u.id,u.Name);
        }
        return userIdToNameMap;
    }
    public static Map<Id,String> getCaseNumber(List<caseShare> caseShareRecordList){
        List<Id> caseIdList = new List<Id>();
        Map<Id,String> caseIdToCaseNumberMap = new Map<Id, String>();
        for(CaseShare caseShareRec:caseShareRecordList){
            if(caseShareRec.caseId !=null){
                caseIdList.add(caseShareRec.caseId);
            }
        }
        for(case c: [SELECT id, CaseNumber 
                     FROM Case
                    WHERE id =: caseIdList]){
            caseIdToCaseNumberMap.put(c.Id, c.CaseNumber);
        }
        return caseIdToCaseNumberMap;
    }
    public static List<Case_Visibility__c> getCaseVisibilityRecords(List<caseShare> caseShareRecordList){
        List<Case_Visibility__c> caseVisibilityList = new List<Case_Visibility__c>();
        List<Id> caseIdList = new List<Id>();
         for(CaseShare caseShareRec : caseShareRecordList){
            if(caseShareRec.caseId !=null){
                caseIdList.add(caseShareRec.caseId);
            }
        }
        for(Case_Visibility__c caseVisibilityRecord : [SELECT id, Name,Case__c,Contact__c,Contact__r.Name,Access_Level__c
                                                       FROM Case_Visibility__c
                                                      WHERE case__c IN: caseIdList]){
                                                           caseVisibilityList.add(caseVisibilityRecord);
                                                       }
        return caseVisibilityList;
    }
}
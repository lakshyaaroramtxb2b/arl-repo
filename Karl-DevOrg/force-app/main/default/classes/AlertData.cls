public with sharing class AlertData 
{
    public String jsonString {get;set;}
    public List<Case> conts {get; set;}
    
    //Constructor
    public AlertData()
    {
        jsonString = prepareData();
    }

    //Temp Method to prepare the Data
    private String prepareData()
    {
        //conts = [SELECT Id, Owner.Name, CreatedDate, RecordType.Name, Status, Subject, Description from Case WHERE RecordType.Name = 'CSIRT Security Alert' AND (Status = 'New' OR Status = 'In-Progress') AND Owner.Name = 'Adam McNeilly'];
        
        conts = [SELECT Id, Owner.Name, CreatedDate, RecordType.Name, Status, Subject, Description from Case WHERE RecordType.Name = 'CSIRT Alert' AND (Status = 'New' OR Status = 'In-Progress')]; // AND Owner.Name = 'Adam McNeilly'];
        
        
        return JSON.serialize(conts);
    }
}
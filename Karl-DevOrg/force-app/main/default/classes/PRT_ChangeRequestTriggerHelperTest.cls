/***********************************************************
* Test class for PRT_ChangeRequestTriggerHelper
* Created by 	- Prashant Gupta
************************************************************/
@isTest
public class PRT_ChangeRequestTriggerHelperTest {
	@testSetup
    public static void testSetup(){
        List<User> userList = new List<User>();
        userList = PRT_TestDataFactory.createUsers(3,'System Administrator',true);
    }
    
    static testMethod void createInternalUserSharingTest(){
        List<User> userList = new List<User>([SELECT Id FROM USER WHERE Email Like 'PRTUser%']);
        List<Program__c> programList = new List<Program__c>(PRT_TestDataFactory.createPrograms(2,true));
        List<Project__c> projectList = new List<Project__c>(PRT_TestDataFactory.createProjects(2,false));
        for(integer i=0; i<2; i++){
            projectList[i].Program__c = programList[i].id;
            projectList[i].Project_Manager__c = userList[0].id;
            projectList[i].Executive_Sponsor_Internal__c = userList[1].id;
            projectList[i].Sponsor_Internal__c = userList[2].id;
        }
        insert projectList;
        
        test.startTest();
        List<Change_Request__c> changeRequestList = new List<Change_Request__c>(PRT_TestDataFactory.createChangeRequest(5,projectList[0].Id,false));  
        for(Change_Request__c co : changeRequestList ){
            co.project__c = projectList[1].Id;
            co.Proposed_Scope__c = 'test scope';
            co.Scope_Change_Reason__c = 'test scope reason';
            co.Scope__c = true;
            co.Subject__c = 'test';
        }
        INSERT changeRequestList;
        
        Map<id,List<Change_Request__c>> projectVsPurchaseOrderMap = new Map<id,List<Change_Request__c>>(); 
        for(Change_Request__c co : changeRequestList){
            
            if(!projectVsPurchaseOrderMap.containsKey(co.Project__c)){
                projectVsPurchaseOrderMap.put(co.Project__c, new List<Change_Request__c>());
            }
            projectVsPurchaseOrderMap.get(co.Project__c).add(co);          
        }
        
        List<Change_Request__Share> crsList= new List<Change_Request__Share>([SELECT Id,AccessLevel FROM Change_Request__Share WHERE AccessLevel ='Read']);
        System.assertEquals(crsList.size()+'','10');
        List<Change_Request__Share> changeRequestSharingToDelete = new List<Change_Request__Share>([SELECT Id 
                                                                                    FROM Change_Request__Share 
                                                                                    WHERE RowCause = 'Manual' ]);
        System.assertEquals(changeRequestSharingToDelete.size()+'','10');
        test.stopTest();
    }
    
    static testMethod void updateNewBudgetToProjectOnApprovalTest(){
        List<Project__c> projectList = new List<Project__c>(PRT_TestDataFactory.createProjects(1,true));
        List<Change_Request__c> changeRequestList = new List<Change_Request__c>(PRT_TestDataFactory.createChangeRequest(1,projectList[0].Id,false));
        
        for(Change_Request__c co : changeRequestList) {
            co.Budget__c = true;
            co.Budget_Change_Reason__c = 'test scope';
            co.Proposed_Budget__c = 'test scope reason';
            co.New_Budget_Amount__c = 500;
        }
        INSERT changeRequestList;      
        List<Change_Request__c> updateChangeReqList = new List<Change_Request__c>([SELECT Id, Budget__c, Budget_Change_Reason__c, Proposed_Budget__c, New_Budget_Amount__c, Status__c, Subject__c FROM Change_Request__c]);
        
        test.startTest();
        
        for(Change_Request__c co : updateChangeReqList){
            co.Status__c = PRT_Constants.PRT_CR_STATUS_APPROVED; 
            co.Subject__c = 'test';
        }   	
        UPDATE updateChangeReqList;
      
        test.stopTest();
    }
    
    static testMethod void createMilestoneToProjectOnApprovalTest(){
        List<Project__c> projectList = new List<Project__c>(PRT_TestDataFactory.createProjects(1,true));
        List<Change_Request__c> changeRequestList = new List<Change_Request__c>(PRT_TestDataFactory.createChangeRequest(5,projectList[0].Id,false));
        
        for(Change_Request__c co : changeRequestList) {
            co.Scope__c = true;
            co.Proposed_Scope__c = 'test scope';
            co.Scope_Change_Reason__c = 'test scope reason';
        }
        INSERT changeRequestList;      
        List<Change_Request__c> updateChangeReqList = new List<Change_Request__c>([SELECT Id, Budget__c, Budget_Change_Reason__c, Proposed_Budget__c, New_Budget_Amount__c, Status__c, Subject__c FROM Change_Request__c]);
        test.startTest();
        for(Change_Request__c co : updateChangeReqList){
            co.Status__c = PRT_Constants.PRT_CR_STATUS_APPROVED;
            co.Subject__c = 'test';
        }
        UPDATE updateChangeReqList;
        
        List<Milestone_PRT__c> milestonelist = new List<Milestone_PRT__c>([SELECT Id,Description__c FROM Milestone_PRT__c WHERE Project__c  IN :projectList]);
        System.assertEquals(milestoneList.size()+'','5');
        
        test.stopTest();      
    }
    
    static testMethod void checkRequiredFieldTest(){
        List<Project__c> projectList = new List<Project__c>(PRT_TestDataFactory.createProjects(1,true));
        List<Change_Request__c> changeRequestList = new List<Change_Request__c>(PRT_TestDataFactory.createChangeRequest(1,projectList[0].Id,false));
        for(Change_Request__c co : changeRequestList) {
            co.Proposed_Scope__c = 'Test Proposed_Scope__c';
            co.Scope_Change_Reason__c = 'Test Scope_Change_Reason__c';
            co.Scope__c = true;
        }
        INSERT changeRequestList;      
        List<Change_Request__c> updateChangeReqList = new List<Change_Request__c>([SELECT Id, Budget__c, Budget_Change_Reason__c, Proposed_Budget__c, New_Budget_Amount__c, Status__c, Subject__c FROM Change_Request__c]);
        for(Change_Request__c co : updateChangeReqList ){
            co.Status__c = PRT_Constants.PRT_CR_STATUS_REJECTED;
            co.Proposed_Scope__c = null;
            co.Scope_Change_Reason__c = null;
            co.Scope__c = false;
        }
        try{
            UPDATE updateChangeReqList;
        }catch(exception e){}

    }
    static testMethod void checkRequiredFieldTest1(){
        List<Project__c> projectList = new List<Project__c>(PRT_TestDataFactory.createProjects(1,true));
        List<Change_Request__c> changeRequestList = new List<Change_Request__c>(PRT_TestDataFactory.createChangeRequest(1,projectList[0].Id,false));
        for(Change_Request__c co : changeRequestList) {
            co.Proposed_Scope__c = 'Test Proposed_Scope__c';
            co.Scope_Change_Reason__c = 'Test Scope_Change_Reason__c';
            co.Scope__c = true;
        }
        INSERT changeRequestList;      
        List<Change_Request__c> updateChangeReqList = new List<Change_Request__c>([SELECT Id, Budget__c, Budget_Change_Reason__c, Proposed_Budget__c, New_Budget_Amount__c, Status__c, Subject__c FROM Change_Request__c]);
        
        for(Change_Request__c co : updateChangeReqList ){
            co.Status__c = PRT_Constants.PRT_CR_STATUS_APPROVED;
            co.Proposed_Scope__c = null;
            co.Scope_Change_Reason__c = null;
            co.Scope__c = false;
        }
        try{
            UPDATE updateChangeReqList;
        }catch(exception e){}

    }
    static testMethod void checkRequiredFieldTest3(){
        List<Project__c> projectList = new List<Project__c>(PRT_TestDataFactory.createProjects(1,true));
        List<Change_Request__c> changeRequestList = new List<Change_Request__c>(PRT_TestDataFactory.createChangeRequest(1,projectList[0].Id,false));
        for(Change_Request__c co : changeRequestList) {
            co.Proposed_Allocation__c = 'Test Proposed_Allocation__c';
            co.Allocation_Change_Reason__c = 'Test Allocation_Change_Reason__c';
            co.Allocation__c = true;
        }
        INSERT changeRequestList;      
        List<Change_Request__c> updateChangeReqList = new List<Change_Request__c>([SELECT Id, Budget__c, Budget_Change_Reason__c, Proposed_Budget__c, New_Budget_Amount__c, Status__c, Subject__c FROM Change_Request__c]);
        
        for(Change_Request__c co : updateChangeReqList ){
            co.Status__c = PRT_Constants.PRT_CR_STATUS_APPROVED;
            co.Proposed_Allocation__c = null;
            co.Allocation_Change_Reason__c = null;
            co.Allocation__c = false;
        }
        try{
            UPDATE updateChangeReqList;
        }catch(exception e){}

    }
     static testMethod void checkRequiredFieldTest4(){
        List<Project__c> projectList = new List<Project__c>(PRT_TestDataFactory.createProjects(1,true));
        List<Change_Request__c> changeRequestList = new List<Change_Request__c>(PRT_TestDataFactory.createChangeRequest(1,projectList[0].Id,false));
        for(Change_Request__c co : changeRequestList) {
            co.Proposed_Budget__c = 'Test Proposed_Budget__c';
            co.Budget_Change_Reason__c = 'Test Budget_Change_Reason__c';
            co.New_Budget_Amount__c = 600;
            co.Budget__c = true;
        }
        INSERT changeRequestList;      
        List<Change_Request__c> updateChangeReqList = new List<Change_Request__c>([SELECT Id, Budget__c, Budget_Change_Reason__c, Proposed_Budget__c, New_Budget_Amount__c, Status__c, Subject__c FROM Change_Request__c]);
        
        for(Change_Request__c co : updateChangeReqList ){
            co.Status__c = PRT_Constants.PRT_CR_STATUS_APPROVED;
            co.Proposed_Budget__c = null;
            co.Budget_Change_Reason__c = null;
            co.Budget__c = false;
        }
        try{
            UPDATE updateChangeReqList;
        }catch(exception e){}
    }
}
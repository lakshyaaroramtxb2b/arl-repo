public with sharing class WebAppScanner {
    
    public String fullname{get; set;}
    public String email{get; set;}
    public Boolean checked{get; set;}
    public String notification {get;set;}
    public String program{get; set;}
    public String appname{get; set;}
    public String company{get; set;}
    
    public PageReference process() {
    
        
        Pattern validEmail = Pattern.compile('^.+@.+$');
        Pattern whiteSpace = Pattern.compile('^\\s*$');
        Matcher emailMatcher = validEmail.matcher(email);
        if ((email == null) || fullname == null || program == null || appname == null || company == null ||
            (validEmail.matcher(email).matches() == False) || (whiteSpace.matcher(email).matches()==True)){
            //PageReference p = new PageReference('/webappscanner#invalid');
            //p.setRedirect(false);
            //return p;
            this.notification = 'Please fill out the form entirely and be sure to enter a valid email address.';
            return null;
        }
        if(checked == False)
        {
            //PageReference pu = new PageReference('/webappscanner?unchecked');
            //pu.setRedirect(false);
            //return pu;
            this.notification = 'The checkbox above must be checked in order to receive a copy of the Web Application Security Scanner.';
            return null;
            
        }

        WebAppScanner__c scan = new WebAppScanner__c();
        scan.Name__c = fullname;
        scan.email__c = email;
        scan.AcceptConditions__c = checked;
        scan.program__c = program;
        scan.appname__c = appname;
        scan.company__c = company;
        insert scan;
        this.notification = 'Thank you for submitting.  Our team will be in contact with you soon.';
        return null;
        //PageReference pa = new PageReference('/webappscanner?complete');
        //pa.setRedirect(false);
        //return pa;

    }


}
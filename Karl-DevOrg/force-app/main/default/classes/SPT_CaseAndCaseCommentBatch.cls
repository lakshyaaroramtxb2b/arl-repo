/***************SPT_CaseAndCaseCommentBatch****************
@Author Prashant Gupta
@Date 11/25/2019
@Modified by - Himanshu Shrimali

-----------------------------------------------------
SUPPORTFORCE/ENTERPRISE SECURITY INTEGRATION COMMANDS
-----------------------------------------------------
Run the following to do a full Case sync:
Id batchJobId = Database.executeBatch(new SPT_CaseAndCaseCommentBatch(false, true), {batchSize});

Run the following to do a ONE TIME Case Comment load of all comments from Supportforce.
DO NOT run this more than once.
Id batchJobId = Database.executeBatch(new SPT_CaseCommentInitialBatch(), {batchSize});

Run the following to schedule a repeated 5 minute batch to pick up deltas:
Id batchJobId = Database.executeBatch(new SPT_CaseAndCaseCommentBatch(false, false), {batchSize});

DO NOT run the following line - we need to remove the full-sync option for Case Comments in this class
    and limit the Case Comment full sync to only the SPT_CaseCommentInitialBatch class.
Id batchJobId = Database.executeBatch(new SPT_CaseAndCaseCommentBatch(true, true), {batchSize});
**********************************************/

Public class SPT_CaseAndCaseCommentBatch implements Database.Batchable<sObject> , Schedulable, Database.Stateful {
    //@TestVisible
    //private static list<SPT_Object> mockallSupportForceCaseList = new list<SPT_Object>();//For test class

    public Boolean queryCaseComments = false;
    public Boolean fullSync = false;
    public Integer minutesBack = 0;

    // Default Constructor
    public SPT_CaseAndCaseCommentBatch(){}

    // Below constructor helps the batch class to run first for case and then for case comments
    public SPT_CaseAndCaseCommentBatch(Boolean isCaseComments, Boolean isFullSync){
        queryCaseComments = isCaseComments;
        fullSync = isFullSync;
    }

    // This method collect the external case or case comment records to process
    public Database.QueryLocator start(Database.BatchableContext BC){
        String query = '';
        BatchSchedule__c b = BatchSchedule__c.getOrgDefaults();
        BatchSchedule__c scheduleBatch = new BatchSchedule__c();
        if(b.Last_Schedule__c != null){
           scheduleBatch = [SELECT id, Last_Schedule__c,LastModifiedDate FROM BatchSchedule__c WHERE Last_Schedule__c!=null Order By LastModifiedDate DESC LIMIT 1];
        }
        
        
        // If queryCaseComments is true, retrieve external case comments
        if (queryCaseComments){
            String whereClause;
            if(fullSync){
                whereClause = 'WHERE CreatedById__c != \'' + SPT_Constants.OUTBOUND_USER_ID + '\'';
            }
// BUG FIX - Needed to use the Last Schedule batch date info, it was still using LAST_N_DAY:1
            else if(scheduleBatch.Last_Schedule__c !=null){
                //String dateTimeBatch = string.valueOf(scheduleBatch.Last_Schedule__c); 
                String dateTimeBatch = scheduleBatch.Last_Schedule__c.format('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');                
                whereClause = 'WHERE LastModifiedDate__c >= '+dateTimeBatch;
            }else{                
                whereClause = 'WHERE LastModifiedDate__c >= LAST_N_DAYS:1';
            }
            query = SPT_Utility.getExternalCaseCommentRecords('', whereClause);
        }
        // Else, retrieve external cases
        else{
            String whereClause;
            if(fullSync){
                whereClause = 'WHERE RecordTypeId__c = \'' + SPT_Constants.SF_RECORDTYPE_ID + '\'';
            }
            else if(scheduleBatch.Last_Schedule__c !=null){
                //String dateTimeBatch = string.valueOf(scheduleBatch.Last_Schedule__c); 
                String dateTimeBatch = scheduleBatch.Last_Schedule__c.format('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');
                
                System.debug('test1');
                
                whereClause = 'WHERE LastModifiedDate__c >= '+dateTimeBatch+' AND RecordTypeId__c =  \'' + SPT_Constants.SF_RECORDTYPE_ID + '\'';
                SYstem.debug('test2');
            }else{
                whereClause = 'WHERE LastModifiedDate__c >= LAST_N_DAYS:1 AND RecordTypeId__c = \'' + SPT_Constants.SF_RECORDTYPE_ID + '\'';
            }
            String extraFields = 'Id, OwnerId__c, ContactId__c, RecordTypeId__c, LastModifiedDate__c, ParentId__c, CreatedbyId__c, CreatedDate__c';
            System.debug('whereClause batch -> '+ whereClause);
            query = SPT_Utility.getExternalCaseRecords(extraFields, whereClause);
        }
        System.debug('SOQL Query: '+query);
        
        //query = 'SELECT  Last_Customer_Comment_Time_c__c, ExternalId, Subject__c FROM Case__x WHERE CreatedDate__c < =:dateTimeBatch ';

        if(Test.isRunningTest()){
            return Database.getQueryLocator([SELECT Id FROM User LIMIT 1]);
        }else{
            return Database.getQueryLocator(query);
        }
    }

    // This method process each batch of records
    public void execute(Database.BatchableContext BC, List<sObject> supportforceRecordsList) {
        List<sObject> sfRecordsList = new List<sObject>();
        if(Test.isRunningTest()){
            List<sObject> externalObjectList = new List<sObject>();
            Case__x externalCaseRecord = new Case__x();
            externalCaseRecord.RecordTypeId__c = SPT_Constants.SF_RECORDTYPE_ID;
            externalCaseRecord.Status__c = 'New';
            externalCaseRecord.Priority__c = 'Low';
            externalCaseRecord.Description__c = 'Test Description';

            CaseComment__x externalCaseCommentRecord = new CaseComment__x();
            date todaysDate = date.today();
            externalCaseCommentRecord.CommentBody__c = 'Test Comment Body Published_Date:'+todaysDate;
            externalCaseCommentRecord.IsPublished__c = true;
            externalCaseCommentRecord.ParentId__c = externalCaseRecord.Id;
            //externalCaseCommentRecord.CreatedById__c= SPT_Constants.INBOUND_USER_ID;
            BatchSchedule__c b = BatchSchedule__c.getOrgDefaults();
            BatchSchedule__c scheduleBatch = new BatchSchedule__c();
            if(b.Last_Schedule__c != null){
                scheduleBatch = [SELECT id, Last_Schedule__c,LastModifiedDate FROM BatchSchedule__c WHERE Last_Schedule__c!=null Order By LastModifiedDate DESC LIMIT 1];
            }
            if(queryCaseComments) {
                externalObjectList.add(externalCaseCommentRecord);
                processCaseCommentRecords(externalObjectList);
                System.debug('externalObjectList-> '+ externalObjectList);
            }else {
                externalObjectList.add(externalCaseRecord);
                processCaseRecords(externalObjectList);
            }
        }else{
            BatchSchedule__c b = BatchSchedule__c.getOrgDefaults();
            BatchSchedule__c scheduleBatch = new BatchSchedule__c();
            if(b.Last_Schedule__c != null){
                scheduleBatch = [SELECT id, Last_Schedule__c,LastModifiedDate FROM BatchSchedule__c WHERE Last_Schedule__c!=null Order By LastModifiedDate DESC LIMIT 1];
            }
            
            /*
             * Below condition will help in processing case records first and case comment records next.
             * This ensures that respective cases are present before we sync the case comments.
             */
            if(queryCaseComments) {
                for(CaseComment__x sfRecord : (List<CaseComment__x>)supportforceRecordsList) {
                    if(fullSync || (!fullSync && sfRecord.LastModifiedDate__c >= scheduleBatch.Last_Schedule__c) || Test.isRunningTest()) {
                        String commentBody = sfRecord.CommentBody__c;
                        if((sfRecord.CreatedById__c!=SPT_Constants.OUTBOUND_USER_ID) || (sfRecord.CreatedById__c==SPT_Constants.OUTBOUND_USER_ID && commentBody.contains(System.Label.SPT_BotTag))) {
                            sfRecordsList.add(sfRecord);
                        }
                    }
                }
                processCaseCommentRecords(sfRecordsList);
            }
            else {
                for(Case__x sfRecord : (List<Case__x>)supportforceRecordsList) {
                    if(fullSync || (!fullSync && sfRecord.LastModifiedDate__c >= scheduleBatch.Last_Schedule__c) || Test.isRunningTest()) {
                        sfRecordsList.add(sfRecord);
                    }
                }
                processCaseRecords(sfRecordsList);
            }
        }
    }

    // Process external Case records which should be synced in both orgs
    public void processCaseRecords(List<Case__x> sfRecordsList) {
        Map<Id, Id> sfCaseIdToSecurityCaseIdMap = new Map<Id, Id>();
        Map<String, String> statusValuesMap = new Map<String, String>();
        Map<Id, String> sfOnwerIdToEmpNumMap = new Map<Id, String>();
        Map<Id, String> sfOnwerIdToEmailMap = new Map<Id, String>();
        Map<String, Id> sfEmpNumToSecUserIdMap = new Map<String, Id>();
        Map<String, Id> sfEmailToSecUserIdMap = new Map<String, Id>();
        Map<Id, String> sfContactIdToEmailMap = new Map<Id, String>();
        Map<String, Id> emailToSecContactIdMap = new Map<String, Id>();
        Map<String, Id> secQueueNameToIdMap = new Map<String, Id>();
        Map<Id, String> sfQueueIdToNameMap = new Map<Id, String>();
        List<Case> securityCaseList = new List<Case>();
        Set<Id> sfCaseIdSet = new Set<Id>();
        Set<Id> sfContactIdSet = new Set<Id>();
        Set<Id> sfOwnerIdSet = new Set<Id>();
        Set<Id> sfParentCaseIdSet = new Set<Id>();
        Set<Id> nullEmpNumUserSet = new Set<Id>();
        Set<Id> sfOwnerQueueIdSet = new Set<Id>();
        List<String> queueNamesList = new List<String>{'Trust - Application Security Assurance', 'Trust - Enterprise Security Infra', 'Trust - Mergers and Acquisitions'};
        List<Id> sfQueueIdsList = new List<Id>{(Id)SPT_Constants.SF_TRUST_APPLICATION_QUEUE_ID, (Id)SPT_Constants.SF_TRUST_ENTERPRISE_QUEUE_ID, (Id)SPT_Constants.SF_TRUST_MERGERS_QUEUE_ID};
        Id recordTypeId = SPT_Constants.SEC_RECORDTYPE_ID;

        // Creating a map of Queue Name To it's Id in security org.
        for(Group queueRecord : [SELECT Id, Type, Name FROM Group WHERE Type = 'Queue' AND Name IN :queueNamesList ORDER BY Name ASC]) {
            secQueueNameToIdMap.put(queueRecord.Name, queueRecord.Id);
        }

        // Creating a map of Queue Id To it's Name in SF org.
        for(Integer i=0; i<queueNamesList.size(); i++) {
            sfQueueIdToNameMap.put(sfQueueIdsList.get(i), queueNamesList.get(i));
        }

        // Stores the status field mapping values in the statusValuesMap
        for(SPT_Status_Field_Mapping__mdt statusValue: SPT_Utility.statusPicklistMappingList) {
            statusValuesMap.put(statusValue.MasterLabel, statusValue.Security_Field_Value__c);
        }

        // This for loop creates set of supportForce caseIds, ownerIds, contactIds and parentCaseIds
        for(Case__x caseRecord: sfRecordsList) {
            sfCaseIdSet.add(caseRecord.ExternalID);
            // [KK] Not every case has a parent, so only add to this set if it's not null.
            // Otherwise we will get a bunch of null values in the set.
            if (caseRecord.ParentId__c != null) {
                sfParentCaseIdSet.add(caseRecord.ParentId__c);
            }
            sfContactIdSet.add(caseRecord.ContactId__c);
            String ownerId = String.valueOf(caseRecord.OwnerId__c);
            if(!String.isEmpty(ownerId) && ownerId.substring(0, 3) != '00G') {
                sfOwnerIdSet.add(caseRecord.OwnerId__c);
            }
        }
        System.debug('Total no. of cases: '+sfCaseIdSet.size());
        System.debug('Total no. of parent cases: '+sfParentCaseIdSet.size());
        System.debug(sfContactIdSet);

        // Below 2 for loops create owner field mapping based on it's employee number
        if ((sfOwnerIdSet != null && !sfOwnerIdSet.isEmpty()) || test.isRunningTest()) {
            // leverage the fact that we store the supportforce userid in the local contact
            for (Contact secConRecord: [SELECT Id, Employee_ID__c, Email, SupportForce_User__c 
                                        FROM Contact WHERE SupportForce_User__c IN: sfOwnerIdSet]) {
                if (secConRecord.Email == null) {
                    nullEmpNumUserSet.add(secConRecord.SupportForce_User__c);
                } else {
                    sfOnwerIdToEmpNumMap.put(secConRecord.SupportForce_User__c, secConRecord.Employee_ID__c);
                    sfOnwerIdToEmailMap.put(secConRecord.SupportForce_User__c, secConRecord.Email);
                }
            }
            System.debug('Owner Map 1: ' + sfOnwerIdToEmpNumMap);

            for (User secUserRecord: [SELECT Id, EmployeeNumber, Email, AccountId  
                                      FROM User 
                                      WHERE Email IN: sfOnwerIdToEmailMap.values()
                                      AND AccountId = null]) {
                sfEmpNumToSecUserIdMap.put(secUserRecord.EmployeeNumber, secUserRecord.Id);
                sfEmailToSecUserIdMap.put(secUserRecord.Email, secUserRecord.Id);
            }
            System.debug('Owner Map 2: ' + sfEmpNumToSecUserIdMap);
        }

        // Below 2 for loops create contact field mapping based on it's email
        if(sfContactIdSet != null && !sfContactIdSet.isEmpty()) {
            for (Contact__x sfContact: [SELECT Id, ExternalId, Email__c FROM Contact__x WHERE Id IN: sfContactIdSet AND RecordTypeId__c IN :SPT_Constants.SF_CONTACTS_RECORD_TYPE_ID]) {
                sfContactIdToEmailMap.put(sfContact.ExternalId, sfContact.Email__c);
            }
            System.debug('Contact Map 1: ' + sfContactIdToEmailMap);

            for (Contact secContact: [SELECT Id, Email FROM Contact WHERE Email IN: sfContactIdToEmailMap.values()
                                      AND RecordTypeId IN :SPT_Constants.SPT_EMPLOYEE_RECORD_TYPE_ID]) {
                emailToSecContactIdMap.put(secContact.Email, secContact.Id);
            }
            System.debug('Contact Map 2: ' + emailToSecContactIdMap);
        }

        for (Case securityCaseRecords: [SELECT Id, SupportForce_Case_Id__c FROM Case
                                        WHERE SupportForce_Case_Id__c IN: sfCaseIdSet
                                        OR SupportForce_Case_Id__c IN: sfParentCaseIdSet]) {
            sfCaseIdToSecurityCaseIdMap.put(securityCaseRecords.SupportForce_Case_Id__c, securityCaseRecords.Id);
        }
        System.debug('Map Value: ' + sfCaseIdToSecurityCaseIdMap);

        // This for loop is handling the sync of every single case record
        for(Case__x sfCaseRecord: sfRecordsList) {
            Case securityCaseRecord = new Case();
            Boolean valuesChanged;

            // Below condition takes the internal case if it's respective external case record is already present.
            if (sfCaseIdToSecurityCaseIdMap != null && sfCaseIdToSecurityCaseIdMap.containsKey(sfCaseRecord.ExternalID)) {
                securityCaseRecord.Id = sfCaseIdToSecurityCaseIdMap.get(sfCaseRecord.ExternalID);
            }
            // Mappping of Parent Case
            if (sfCaseRecord.ParentId__c!=null && sfCaseIdToSecurityCaseIdMap != null && sfCaseIdToSecurityCaseIdMap.containsKey(sfCaseRecord.ParentId__c)) {
                securityCaseRecord.ParentId = sfCaseIdToSecurityCaseIdMap.get(sfCaseRecord.ParentId__c);
            }
            else if(sfCaseRecord.ParentId__c == null) {
                securityCaseRecord.ParentId = null;
            }

            // Mapping of Case Owner
            if(sfOnwerIdToEmailMap != null && sfOnwerIdToEmailMap.containsKey(sfCaseRecord.OwnerId__c) &&
               sfEmailToSecUserIdMap != null && sfEmailToSecUserIdMap.containsKey(sfOnwerIdToEmailMap.get(sfCaseRecord.OwnerId__c))) {
                   securityCaseRecord.OwnerId = sfEmailToSecUserIdMap.get(sfOnwerIdToEmailMap.get(sfCaseRecord.OwnerId__c));
               }
            else if(sfQueueIdToNameMap != null && sfQueueIdToNameMap.containsKey(sfCaseRecord.OwnerId__c) &&
                    secQueueNameToIdMap != null && secQueueNameToIdMap.containsKey(sfQueueIdToNameMap.get(sfCaseRecord.OwnerId__c))){
                        securityCaseRecord.OwnerId = secQueueNameToIdMap.get(sfQueueIdToNameMap.get(sfCaseRecord.OwnerId__c));
                    }
            else {
                securityCaseRecord.OwnerId = secQueueNameToIdMap.get('Trust - Application Security Assurance');
            }

            // Mapping of Case Contact
            if (sfContactIdToEmailMap != null && sfContactIdToEmailMap.containsKey(sfCaseRecord.ContactId__c) &&
                emailToSecContactIdMap != null && emailToSecContactIdMap.containsKey(sfContactIdToEmailMap.get(sfCaseRecord.ContactId__c))) {
                    securityCaseRecord.ContactId = emailToSecContactIdMap.get(sfContactIdToEmailMap.get(sfCaseRecord.ContactId__c));
                }
            else {
                securityCaseRecord.ContactId = null;
            }

            // This utility method returns true if any of the concerned field value is changed.
            valuesChanged = SPT_Utility.isFieldValueChanged(securityCaseRecord, sfCaseRecord);
            // If values are changed, then sync
            if (valuesChanged) {
                // Below utility method syncs all the fields which are in SPT Case Field Mapping metadata
                SPT_Utility.syncInternalAndExternalCaseRecords(securityCaseRecord, sfCaseRecord, true);
                // Record Type Mapping
                securityCaseRecord.RecordTypeId = recordTypeId;
            }
            securityCaseList.add(securityCaseRecord);
        }
        System.debug('Case List: ' + securityCaseList);
        if (securityCaseList != null && !securityCaseList.isEmpty()) {
            upsert securityCaseList;
        }
    }

    // Process Case Comments records which should be synced in both the orgs
    // Changes Done to Map the created by owner id from spt to entsec console.
    public void processCaseCommentRecords(List<CaseComment__x> sfRecordsList) {
        Map<Id,String> sptIdVsEmployeeIdMap = new Map<Id,String>();
        Map<String,ID> employeeIDVsUserIDMap = new Map<String,Id>();
        Map<Id, Id> sfCaseIdToSecurityCaseIdMap = new Map<Id, Id>();
        Map <Id, String> sfUserIdToNameMap = new Map<Id, String>();
        //List<Case> secCaseList = new List<Case>();
        List<CaseComment> securityCaseCommentList = new List<CaseComment>();
        List<CaseComment> secCaseCommentList = new List<CaseComment>();
        List<SupportForceUser__x> sfUsersList = new List<SupportForceUser__x>();
        Set<Id> sfCaseCommentParentIdSet = new Set<Id>();
        Set<Id> sfCaseCommentUserIdSet = new Set<Id>();
        Set<Id> sfCaseCommentExternalIdSet = new Set<Id>();
     
        // This for loop creates set of parentCaseId and userId
        for (CaseComment__x caseCommentRecord: sfRecordsList) {
            sfCaseCommentParentIdSet.add(caseCommentRecord.ParentId__c);
            sfCaseIdToSecurityCaseIdMap.put(caseCommentRecord.ParentId__c, null);
            sfCaseCommentUserIdSet.add(caseCommentRecord.CreatedById__c);
        }
        System.debug('CaseComm User Id Set' + sfCaseCommentUserIdSet);

        // Creating a map of case comment creator Id to Name
        if(!test.isRunningTest()){
            for (SupportForceUser__x sfUser: [SELECT Id, Name__c,EmployeeNumber__c, ExternalId FROM SupportForceUser__x WHERE ExternalId IN: sfCaseCommentUserIdSet]) {
            System.debug('USER NAME: ' + sfUser.Name__c);
            sfUserIdToNameMap.put(sfUser.ExternalId, sfUser.Name__c);
            if(sfUser.EmployeeNumber__c!=null){
                sptIdVsEmployeeIdMap.put(sfUser.ExternalId, sfUser.EmployeeNumber__c);
                employeeIDVsUserIDMap.put(sfUser.EmployeeNumber__c, null);
            }

        }
        System.debug('UserId TO Name Map: ' + sfUserIdToNameMap);
        }else{
            user u = [SELECT id,EmployeeNumber from user where EmployeeNumber != null LIMIT 1];
                employeeIDVsUserIDMap.put(u.EmployeeNumber, null);

        }


        for(User userRec : [SELECT ID, EmployeeNumber
                            From User
                            WHERE EmployeeNumber IN :employeeIDVsUserIDMap.keySet() ]){
                                if(employeeIDVsUserIDMap!=null && employeeIDVsUserIDMap.containsKey(userRec.EmployeeNumber)){
                                    employeeIDVsUserIDMap.put(userRec.EmployeeNumber, userRec.Id);
                                }
                            }

        for(Case caseRecord : [SELECT Id, SupportForce_Case_Id__c FROM Case WHERE SupportForce_Case_Id__c IN :sfCaseCommentParentIdSet]) {
            sfCaseIdToSecurityCaseIdMap.put(caseRecord.SupportForce_Case_Id__c, caseRecord.Id);
        }
        // Creating a map of supportforce case Id to actual case Id
        for (CaseComment comment: [SELECT Id, CommentBody, ParentId, Parent.SupportForce_Case_Id__c FROM CaseComment
                                   WHERE Parent.SupportForce_Case_Id__c IN: sfCaseCommentParentIdSet]) {
            String caseCommentId = comment.CommentBody.substringBetween('Record_Id: ', '\n');
            if (caseCommentId != null || caseCommentId != '') {
                sfCaseCommentExternalIdSet.add(caseCommentId);
            }
            //sfCaseIdToSecurityCaseIdMap.put(comment.Parent.SupportForce_Case_Id__c, comment.ParentId);
        }
        System.debug('Map Value: ' + sfCaseIdToSecurityCaseIdMap);

        // Case Comment Sync
        System.debug('Map Values 2: ' + sfCaseCommentExternalIdSet);

        for (CaseComment__x sfCaseCommentRecord: sfRecordsList) {
            if(sfCaseIdToSecurityCaseIdMap != null && sfCaseIdToSecurityCaseIdMap.containsKey(sfCaseCommentRecord.ParentId__c) &&
               sfCaseCommentExternalIdSet != null && !sfCaseCommentExternalIdSet.contains(sfCaseCommentRecord.ExternalId)) {
                   CaseComment securityCaseCommentRecord = new CaseComment();
                   String caseCommentUserName = sfUserIdToNameMap.get(sfCaseCommentRecord.CreatedById__c);
                   System.debug('INSIDE LOPP USER NAME : ' + caseCommentUserName);
                   String metadata = '\n<<metadata>>\nRecord_Id: ' + sfCaseCommentRecord.ExternalId + '\nOriginal_Author: ' + caseCommentUserName + '\nPublished_Date: ' + sfCaseCommentRecord.CreatedDate__c;
                   Integer metadataLength = metadata.length();
                   String text = sfCaseCommentRecord.CommentBody__c.replaceAll('\n','--n--');
                   Integer newLineCharLength = text.countMatches('--n--')*4;
                   Integer requiredLength = 3700 - metadataLength - newLineCharLength;
                   String templateBody;
                   if(sfCaseCommentRecord.CommentBody__c.length() <= requiredLength) {
                       templateBody = sfCaseCommentRecord.CommentBody__c + metadata;
                   }
                   else {
                       templateBody = sfCaseCommentRecord.CommentBody__c.substring(0, requiredLength-5) + ' ...' + metadata;
                   }
                   securityCaseCommentRecord.ParentId = sfCaseIdToSecurityCaseIdMap.get(sfCaseCommentRecord.ParentId__c);
                   securityCaseCommentRecord.IsPublished = sfCaseCommentRecord.IsPublished__c;
                   securityCaseCommentRecord.CommentBody = templateBody;
                   // changes by Swarnima - for commentCaseCreatedId SM - Start
                   if(sptIdVsEmployeeIdMap.containsKey(sfCaseCommentRecord.CreatedbyId__c) && employeeIDVsUserIDMap.containsKey(sptIdVsEmployeeIdMap.get(sfCaseCommentRecord.CreatedbyId__c))){
                       securityCaseCommentRecord.CreatedbyId = employeeIDVsUserIDMap.get(sptIdVsEmployeeIdMap.get(sfCaseCommentRecord.CreatedbyId__c));
                   }
                   // SM - Stop
                   //securityCaseCommentRecord.CreatedbyId = sfCaseCommentRecord.CreatedbyId__c;
                   //changes for comment Date - Start
                   securityCaseCommentRecord.CreatedDate = sfCaseCommentRecord.CreatedDate__c;
                   //Stop
                   securityCaseCommentRecord.CommentBody = templateBody;
                   if(securityCaseCommentRecord.CommentBody.length()<=4000 && securityCaseCommentRecord.ParentId!=null){
                       securityCaseCommentList.add(securityCaseCommentRecord);
                   }
               }
        }
        System.debug('Comment: ' + securityCaseCommentList);
        if(securityCaseCommentList != null && !securityCaseCommentList.isEmpty()) {
            //INSERT securityCaseCommentList;
            Database.SaveResult[] srList = Database.insert(securityCaseCommentList, false);
            System.debug('SaveResult 1: '+srList);
            String backlash = '\\';
            String backlashReplacement = '---';
            List<CaseComment> reinsertCommentsList = new List<CaseComment>();
            for(CaseComment comment : securityCaseCommentList) {
                if(comment.Id == null) {
                    String templateBody = comment.CommentBody.substringBefore('\n<<metadata>>');
                    String metadataBody = comment.CommentBody.substringAfter('\n<<metadata>>');
                    String newTemplateBody = templateBody.replaceAll('\\<.*?>','');
                    comment.CommentBody = newTemplateBody + '\n<<metadata>>' + metadataBody;
                    System.debug('Updated comment2: '+comment.CommentBody);
                    reinsertCommentsList.add(comment);
                }
            }

            if(reinsertCommentsList != null && !reinsertCommentsList.isEmpty()) {
                Database.SaveResult[] resultList = Database.insert(reinsertCommentsList, true);
                System.debug('SaveResult 2: '+resultList);
            }
        }
    }

    // Post-processing operations
    public void finish(Database.BatchableContext BC) {
        List<BatchSchedule__c> bactchScheduleList = new List<BatchSchedule__c>();
        for(BatchSchedule__c b: [SELECT id,Last_Schedule__c 
                                 FROM BatchSchedule__c WHERE Name = 'Schedule']){
			// TODO - This should not use system.now as we are missing any update that may had happened in SF
			// while this job was running                                    
            b.Last_Schedule__c = system.now();                        
            bactchScheduleList.add(b);
        }

		// BUG FIX - this is BAD, the anchor is being updated right after processing CASES so no comments get picked up
		// so changing to update after case comments
        if (!queryCaseComments) {
            if(!fullSync) {  
               //b.Last_Schedule__c = System.schedule('Batch 2', cron, new SPT_CaseAndCaseCommentBatch(false,false));
               Database.executeBatch(new SPT_CaseAndCaseCommentBatch(true, fullSync));
            }
            //BatchSchedule__c batchRec = [SELECT id, Name,Last_Schedule__c FROM BatchSchedule__c LIMIT 1] 
/* move to the ELSE 
            if(bactchScheduleList != null && !bactchScheduleList.isEmpty()){
                update bactchScheduleList;
            }
*/
        } else {
            System.debug('END BATCH');
// BUG FIX - needed to update the anchor when both batches are done
            if(bactchScheduleList != null && !bactchScheduleList.isEmpty()){
                update bactchScheduleList;
            }
            if(!Test.isRunningTest()) {
                SPT_CaseAndCaseCommentBatch batch = new SPT_CaseAndCaseCommentBatch(false, fullSync);
                System.scheduleBatch(batch, 'SPT_CaseAndCaseCommentBatch', 4);
            }
        }
        
    }

    // Required when we implement Database.Stateful
    public void execute(SchedulableContext SC) {
        if(!Test.isRunningTest()) {
        Database.executebatch(new SPT_CaseAndCaseCommentBatch(false, fullSync));
        }
    }
}
public class SerializedDefenseHookController {

    public boolean defaultOptionFlag { get; set; }  

    public SerializedDefenseHookController() {
        defaultOptionFlag = false;
    }

    public String getEndpoint() {
        return URL.getSalesforceBaseUrl().toExternalForm() + '/services/admin/v40.0/defenseHookPublication';
    }

    // CHECK CSIRT1 CERT EXISTS
    public Boolean getCsirt1CertExists() {
        return DefenseControllerUtil.checkCertExists('csirt_1');
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * Alert for new instances
     */

    // See comment at top of UnupdatedInstanceAlertJob re:why to handle variable number of PreviousInstanceList rather than assuming 1
    public String getNewInstanceAlert() {
        String alert = '';
        String prefix = '';
        
        List<PreviousInstanceList__c> pinstlists = [select newInstanceAlert__c from PreviousInstanceList__c];
        for(PreviousInstanceList__c p : pinstlists) {
            if(p.newInstanceAlert__c != null && p.newInstanceAlert__c.length() > 0) {
                alert += prefix + p.newInstanceAlert__c;
                prefix = ' \n';
            }
        }
        return alert;
    }
    
    public boolean getNewInstanceAlertPresent() {
        return (getNewInstanceAlert().length() > 0);
    }
    
    public void clearNewInstanceAlert() {
        if(true) {
            List<PreviousInstanceList__c> pinstlists = [select id, Instances__c, newInstanceAlert__c from PreviousInstanceList__c];
            for(PreviousInstanceList__c p : pinstlists) {
                p.newInstanceAlert__c = '';
            }
            
            if(pinstlists.size() > 0) { update pinstlists; }
        }
    }
    /////////////////////////////////////////////////////////////////////////////////////////////////
    
    public String getSerializedDefenseHook() {
        return new DefenseHookProcessor().getSerializedDefenseHook(this.defaultOptionFlag);
    }
    
    public String getRawDefenseHook() {
        Map<String, Object> defenseHook = new DefenseHookProcessor().getDefenseHook(this.defaultOptionFlag);
        return JSON.serializePretty(defenseHook);
    }   

}
public class CSIRTAlert {
    public String DetectionAlertId;
    public String DetectionAlertCriteriaId;
    public String DetectionAlertCriteriaName;
    public String Environment;
    public String Subject;
    public String Description;
    public String Severity;
    public String RecipientTeam;
    //orion: alert score
    public String OrionAlertScore;

    //orion: alert score added as one input parameter
    public CSIRTAlert(String DetectionAlertId, String Environment, String Subject, String Description, String Severity, String RecipientTeam, String detectionCriteriaId, String detectionCriteriaName, String OrionAlertScore){
        this.DetectionAlertId = DetectionAlertId;
        this.DetectionAlertCriteriaId = detectionCriteriaId;
        this.DetectionAlertCriteriaName = detectionCriteriaName;
        this.Environment = Environment;
        this.Subject = Subject;
        this.Description = Description;
        this.Severity = Severity;
        this.RecipientTeam = RecipientTeam;
        //orion: alert score
        this.OrionAlertScore = OrionAlertScore;
    }
}
public class SecToolsQuizScorecardController {

    public SecToolsQuizConstants qc = new SecToolsQuizConstants();
    private Set<String> incorrectTopicSet = new Set<String>();
    public List<String> incorrectTopics {get;set;}
    public String sId {get;set;}
    public Integer allCount { get; set; }
    public Integer answerCount {get;set;}
    public Integer correctCount { get; set; }
    public QuizUser__c user {get;set;}
    private datetime oldestAllowed; 
    public String debugField {get;set;}
    
    transient List<QuizAnswer__c> allAnswers;
    transient List<QuizAnswer__c> missedAnswers = new List<QuizAnswer__c>();
    
    public SecToolsQuizScorecardController() {
        sId = ApexPages.CurrentPage().getParameters().get('sId');
        correctCount = 0;
        
    }
    
    public PageReference init() {
        if (sId == null) {
            return qc.register; 
        }
        else {
            // load the correct QuizUser from the sId passed in the params
            // redirect them to the registration page if they don't have one
            oldestAllowed = datetime.now().addDays(0-qc.quizValidDays);
            List<QuizUser__c> activeUsers = [Select Id,Key__c,Question_Count__c,quiz_completion__c,lastActive__c,Name from QuizUser__c 
                      where Key__c =: sId and created__c >=: oldestAllowed limit 1];
            if (activeUsers.size() > 0){
                user = activeUsers.get(0);
            }
            if (user == null) {
                return qc.register;
            }
        }
       
        allAnswers = [select Name,Id,answer__c,false_picks__c,isCorrect__c,missed_topics__c 
                          from QuizAnswer__c where user__c =: user.Id];
        allCount = [select count() from QuizQuestion__c where active__c = 1];
        answerCount = allAnswers.size();
        if ((allAnswers.size() < user.Question_Count__c) && (allCount > allAnswers.size())) {
           // hey! They're not done!  They didn't hit their max question count and we didn't 
           // go through all of the questions
           PageReference pr = new PageReference(qc.quizQuestion.getUrl()+'?sId='+user.Key__c);
           pr.setRedirect(true);
           return pr;
        }
        for (QuizAnswer__c ans: allAnswers) {
            if (ans.isCorrect__c != 1){
                missedAnswers.add(ans);
                if (ans.false_picks__c != null) {
                    // these were their false positives
                    // if they incorrectly chose it, they don't know what it is
                    for (String s:ans.false_picks__c.split(';')){
                        incorrectTopicSet.add(s);
                    }
                }
                if (ans.missed_topics__c != null) {
                    // these were the options that they failed to get
                    for (String s:ans.missed_topics__c.split(';')){
                        incorrectTopicSet.add(s);
                    }
                }
            }
            else {
                correctCount +=1; //hey, they got it!
            }
        }
        incorrectTopics = new List<String>();
        for (String s: incorrectTopicSet) {
          //lame that we can't pass a Set to apex:repeat
          incorrectTopics.add(s);
        }
        
        if (user.quiz_completion__c == null) {
            user.quiz_completion__c = datetime.now();
        }
        user.quiz_score__c = numPercentCorrect();
        update user;
        return null;
        
    }
                
                
                

    public String getMissedTopicStyle() {
        // kill the "missed topics" section if necessary
        if (incorrectTopics.size() > 0) {
            return '';
        }
        else {
            return 'display:none;';
        }
    }

    public Long numPercentCorrect() {
        System.debug('*******');
        System.debug(user);
        System.debug(user.Question_Count__c);
        return Math.roundToLong(((Double)correctCount/user.Question_Count__c)*100);
    }
    
    public String getPercentCorrect() {
        return String.valueOf(numPercentCorrect())+'%';
    }

}
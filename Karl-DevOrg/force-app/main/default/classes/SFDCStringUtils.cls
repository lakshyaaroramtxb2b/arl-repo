/**
 * OWASP Enterprise Security API (ESAPI)
 * 
 * This file is part of the Open Web Application Security Project (OWASP)
 * Enterprise Security API (ESAPI) project. For details, please see
 * <a href="http://www.owasp.org/index.php/ESAPI">http://www.owasp.org/index.php/ESAPI</a>.
 *
 * Copyright (c) 2010 - Salesforce.com
 * 
 * The Apex ESAPI implementation is published by Salesforce.com under the New BSD license. You should read and accept the
 * LICENSE before you use, modify, and/or redistribute this software.
 * 
 * @author Yoel Gluck (securecloud .at. salesforce.com) <a href="http://www.salesforce.com">Salesforce.com</a>
 * @created 2010
 */

/**
 * A collection of utility methods to perform String analysis and transformations
 */
public with sharing class SFDCStringUtils {

	private SFDCStringUtils() {
		// prevent instantiation
	}

    /**
     * Split really big strings!! Avoids System.LimitException "Regex too complicated"
     * that happens with strings longer than ~500k characters (varies depending on substring).
     *
     * Use at your own risk!! May have minor differences with what'd you get by using 
     * String.split() directly or missed edge cases.
     *
     * NB: `substring` has to work as both a literall string for indexOf and as regex 
     * for split
     *
     * @author Ralph Callaway (ralph@callaway.cloud)
     * 
     * @param  str       String to split
     * @param  substring Substring delimeter used to split
     * @return           List that contains each substring of the `str` that is terminated
     *                   by either the delimeter `substring` or the end of the string
     */
    @TestVisible private static Integer MAX_SPLIT_LENGTH = 500000;
    public static String[] bigSplit(String str, String substring) {
        if (str == null || substring == null) {
            throw new System.NullPointerException();
        }

        // short enough to use string.split() method
        if (str.length() < MAX_SPLIT_LENGTH) {
            return str.split(substring);
        }

        // no instances of substring, can just return now
        if (str.indexOf(substring) == -1) {
            return new String[]{ str };
        }

        // we have instances, look for farthest potential split point
        Integer splitPoint = str.substring(0, MAX_SPLIT_LENGTH).lastIndexOf(substring);

        // found a good split point, use string split on that then
        // recurse on remainder
        if (splitPoint != -1) {
            String[] results = str.substring(0, splitPoint).split(substring);
            results.addAll(bigSplit(str.substring(splitPoint + substring.length()), substring));
            return results;
        } else {
            // Didn't find a split point in searchable length for string.split()
            // must be in the remainder, recurse and prepend searched string
            // to first result.
            // Have to recurse a bit earlier in case substring straddled
            // at the boundary of max searchable length
            Integer recursePoint = MAX_SPLIT_LENGTH - substring.length();
            String[] results = bigSplit(str.substring(recursePoint), substring);
            results[0] = str.substring(0, recursePoint) + results[0];
            return results;
        }
    }

    public static List<List<String>> slice(List<String> objs, Integer size){
        /* Apex List class is missing a slice() method. 
         * This one is from https://success.salesforce.com/ideaView?id=08730000000BqGW
         */
        List<List<String>> resultList = new List<List<String>>();
        Integer numberOfChunks = objs.size() / size;
        for(Integer j = 0; j < numberOfChunks; j++ ){
            List<String> someList = new List<String>();
            for(Integer i = j * size; i < (j+1) * size; i++){
                someList.add(objs[i]);
            }
            resultList.add(someList);
        }

        if(numberOfChunks * size < objs.size()){
            List<String> aList = new List<String>();
            for(Integer k = numberOfChunks * size ; k < objs.size(); k++){
                aList.add(objs[k]);
            }
            resultList.add(aList);
        }
        return resultList;
    }

    /**
     * Check to ensure that a {@code String} is not null or empty (after optional
     * trimming of leading and trailing whitespace). 
     * 
     * @param str   The {@code String} to be checked.
     * @param trim  If {@code true}, the string is first trimmed before checking
     *              to see if it is empty, otherwise it is not.
     * @return      True if the string is null or empty (after possible
     *              trimming); otherwise false.
     */
    public static boolean notNullOrEmpty(String str, boolean trim) {
        if ( trim ) {
            return !( str == null || str.trim().equals('') );
        } else {
            return !( str == null || str.equals('') );
        }
    }
    
    /**
     * Returns true if String is empty ('') or null.
     */
    public static boolean isEmpty(String str) {
        return str == null || str.length() == 0;
    }
    
	/**
	 * Converts an array of Strings to a Set of Integers. 
	 * @param array the contents of the new Set
	 * @return a Set containing the elements in the array
	 */
	public static Set<Integer> stringArrayToIntegerSet(String[] arr) {
		Set<Integer> toReturn = new Set<Integer>();
		for (String c : arr) {
			toReturn.add(SFDCCharacter.toInt(c));
		}
		return toReturn;
	}
	
	public static String[] unionStringArrays(String[] a, String[] b) {
		
		Set<String> tmp = new Set<String>();
		String[] ret;
		Integer i;
		
		// add all unique strings
		tmp.addAll(a);
		tmp.addAll(b);
		
		List<String> tmpList = new List<String>();
		tmpList.addAll(tmp);
		tmpList.sort();
		
		// get all strings into an array of strings
		ret = new String[tmpList.size()];
		i = 0;
		for (String s : tmpList) {
			ret[i] = s;
			i++;
		}
		
		return ret;
	}
}
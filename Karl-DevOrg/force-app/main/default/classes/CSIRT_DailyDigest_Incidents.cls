public class CSIRT_DailyDigest_Incidents {
	List<Case> incidents;    
    public List<Case> getIncidents(){
        if(incidents == null)
        	incidents = [SELECT IRCloud__Incident_Severity__c, Days_Open__c, Subject, IRCloud__Email_Case_Reference_ID__c, ResponseForce__Incident_Name_New__c, IRCloud__Case_Summary__c FROM Case WHERE IsClosed = False AND Include_in_Daily_Digest__c = True AND RecordTypeId = '0123A000001dxqi' ORDER BY IRCloud__Incident_Severity__c, Days_Open__c];
        return incidents;
    }

}
/**
* @author Lakshya Arora
* @email lakshya.arora@mtxb2b.com
* @description Helper class for Task trigger
*/
public class KARL_TaskTriggerHelper {
    /**
* @author Lakshya Arora
* @email lakshya.arora@mtxb2b.com
* @description Update KARL_Cycle_Audit_Report_Items__c status to readytosign when it's respective task gets completed
*/
    public static void markRelatedCycleAuditReadyToSign(List<Task> newList,Map<Id,Task> oldMap){
        Set<Id> cycleAuditReportsIdSet = new Set<Id>();
        for(Task taskObj : newList){
            if(taskObj.WhatId != null && taskObj.WhatId.getSobjectType() == KARL_Cycle_Audit_Report_Items__c.getSobjectType()
            && oldMap != null && taskObj.Status != oldMap.get(taskObj.Id).Status
            && taskObj.Status == KARL_Constants.TASK_STATUS_COMPLETED){
                cycleAuditReportsIdSet.add(taskObj.WhatId);
            }
        }
        if(!cycleAuditReportsIdSet.isEmpty()){
            List<KARL_Cycle_Audit_Report_Items__c> cycleAuditReportsList = new List<KARL_Cycle_Audit_Report_Items__c>();
            for(KARL_Cycle_Audit_Report_Items__c cycleAuditReportObj : [SELECT Id,Status__c 
                                                                FROM KARL_Cycle_Audit_Report_Items__c 
                                                                WHERE Id IN:cycleAuditReportsIdSet 
                                                                WITH SECURITY_ENFORCED]){
                cycleAuditReportObj.Status__c = KARL_Constants.REPORT_STATUS_READY_TO_SIGN;
                cycleAuditReportsList.add(cycleAuditReportObj);
            }
            if(!cycleAuditReportsList.isEmpty())
            update cycleAuditReportsList;
        }
    }
}
public with sharing class SPT_CaseDataTableController {
    //@AuraEnabled
    /*public static List<Case> getAllCases(List<String> statusList, String noOfDays, Boolean newCases){
        List<Case> allCases = new List<Case>();
        List<ID> caseIdList = new List<ID>();
        List<String> filterConditionsList = new List<String>();
        List<String> newStatusList = new List<String>{'New', 'Review', 'Incomplete', 'Backlog'};
        FINAL Integer LIMITCOUNTER = 500;
        System.debug('Status List: '+statusList);
        System.debug('recentlyContacted: '+noOfDays);
        String caseQuery = 'SELECT Id, Status, Origin, Description,'+
                    ' Enterprise_Security_Case_Number__c, Subject,'+
                    ' Priority, OwnerId, Owner.Name,'+
                    ' FROM Case WHERE RecordTypeId = '+ '\''+SPT_Constants.SEC_RECORDTYPE_ID+'\''+
                    ' AND Enterprise_Security_Case_Number__c!=null'+
                ' AND Owner.Name = \'Trust - Application Security Assurance\'';
        if(statusList!=null && !statusList.isEmpty()){
            caseQuery += ' AND Status IN: statusList';
            //filterConditionsList.add('Status IN: statusList');
        }
        else {
            caseQuery += ' AND Status = \'\'';
            //filterConditionsList.add('Status = \'\'');
        }
        if(noOfDays != null && noOfDays != ''){
            for(CaseComment comment : Database.query('SELECT ID, ParentId, Parent.Status, CommentBody FROM CaseComment'+
                                        ' WHERE Parent.RecordTypeId = \''+SPT_Constants.SEC_RECORDTYPE_ID+'\''+
                                        ' AND Parent.Enterprise_Security_Case_Number__c!=null'+
                                        ' AND CommentBody LIKE \'%<<metadata>>%\''+
                                        ' AND LastModifiedDate >= LAST_N_DAYS:'+noOfDays+
                                        ' ORDER BY LastModifiedDate DESC')) {
              if(!caseIdList.contains(comment.ParentID)){
                    caseIdList.add(comment.ParentID);                              
                }                
            }
            System.debug('=== Case Id List === '+caseIdList);
            if(caseIdList!=null) {
                //filterConditionsList.add('Id IN: caseIdList');
                caseQuery += ' AND Id IN: caseIdList';
            }
        }
        if(newCases) {
            //filterConditionsList.add('Status = \'New\'');
            caseQuery += ' AND Status IN: newStatusList';
        }

        //Adding Ordering of cases
        if(noOfDays == null || noOfDays == '') {
            caseQuery += ' ORDER BY LastModifiedDate ASC';
        }
        //Adding Limit
        caseQuery += ' LIMIT '+LIMITCOUNTER;
        System.debug('====== CASE QUERY ======'+caseQuery);
        allCases = Database.query(caseQuery);
        List<Case> updatedCaseList = new List<Case>();
        if(caseIdList != null && !caseIdList.isEmpty()) {
            for(Id caseId : caseIdList) {
                for(Case caseRecord : allCases) {
                    if(caseRecord.Id == caseId) {
                        updatedCaseList.add(caseRecord);
                    }
                }
            }
            return updatedCaseList;
        }        
        return allCases;
    }*/

    @AuraEnabled
    public static Boolean updateCases(List<Case> updatedCaseList){
        try {
            UPDATE updatedCaseList;
            return true;
        }
        catch(Exception e) {
            return false;
        }
    }

    // Wrapper class for the toggle button
    public class FilterOptionsWrapper {
        @AuraEnabled public String label;
        @AuraEnabled public String value;
        @AuraEnabled public String type;
       
        public FilterOptionsWrapper(String label, String value, String type) {
            this.label = label;
            this.value = value;
            this.type = type;
           
        }
        public FilterOptionsWrapper(String label, String value) {
            this.label = label;
            this.value = value;
        }
    }

   /* @AuraEnabled
    public static List<FilterOptionsWrapper> getAllStatus(){
        List<FilterOptionsWrapper> filterOptionsList = new List<FilterOptionsWrapper>();
        filterOptionsList.add(new FilterOptionsWrapper('All', 'All'));
        filterOptionsList.add(new FilterOptionsWrapper('All Open', 'All Open'));
        filterOptionsList.add(new FilterOptionsWrapper('All Closed', 'All Closed'));
       
        List<String> closeStatusList = new List<String>();
        List<CaseStatus> statusList = new List<CaseStatus>([SELECT Id, IsClosed, MasterLabel FROM CaseStatus]);
        for(CaseStatus stat : statusList) {
            closeStatusList.add(stat.MasterLabel);
        }
        System.debug('List: '+closeStatusList);
        List<SPT_Status_Field_Mapping__mdt> statusMetadataList = new List<SPT_Status_Field_Mapping__mdt>([SELECT Id, MasterLabel, Security_Field_Value__c FROM SPT_Status_Field_Mapping__mdt]);
        System.debug('Metadata Val: '+statusMetadataList);
        List<String> entSecStatusList = new List<String>();
        for(SPT_Status_Field_Mapping__mdt statusObj : statusMetadataList) {
            entSecStatusList.add(statusObj.Security_Field_Value__c);
        }
        System.debug('EntSec Val: '+entSecStatusList);
        List<String> entSecClosedStatusList = new List<String>();
        for(String closedVal : closeStatusList) {
            if(entSecStatusList.contains(closedVal)) {
                FilterOptionsWrapper filterOption = new FilterOptionsWrapper(closedVal, closedVal);
                filterOptionsList.add(filterOption);
            }
        }
        return filterOptionsList;
    }
    */
    @AuraEnabled
    public static List<FilterOptionsWrapper> getAllStatus(){
        List<FilterOptionsWrapper> filterOptionsList = new List<FilterOptionsWrapper>();
        filterOptionsList.add(new FilterOptionsWrapper('All', 'All', 'None'));
        filterOptionsList.add(new FilterOptionsWrapper('All Open', 'All Open', 'None'));
        filterOptionsList.add(new FilterOptionsWrapper('All Closed', 'All Closed', 'None'));
        filterOptionsList.add(new FilterOptionsWrapper('All Others', 'All Others', 'None'));
       
        List<SPT_Status_Field_Mapping__mdt> statusMetadataList = new List<SPT_Status_Field_Mapping__mdt>([SELECT Id, MasterLabel, Security_Field_Value__c, Status_Type__c  FROM SPT_Status_Field_Mapping__mdt]);
        System.debug('Metadata Val: '+statusMetadataList);
        Map<String, String> entSecStatusMap = new Map<String, String>();
       
        for(SPT_Status_Field_Mapping__mdt statusObj : statusMetadataList) {
            entSecStatusMap.put(statusObj.Security_Field_Value__c, statusObj.Status_Type__c);
        }
        List<String> entSecClosedStatusList = new List<String>();
        for(CaseStatus caseStatus : [SELECT Id, IsClosed, MasterLabel FROM CaseStatus Where MasterLabel IN :entSecStatusMap.KeySet()]) {
            FilterOptionsWrapper filterOption = new FilterOptionsWrapper(caseStatus.MasterLabel, caseStatus.MasterLabel, entSecStatusMap.get(caseStatus.MasterLabel));
            filterOptionsList.add(filterOption);
            System.debug('filterOptionsList--> '+ filterOptionsList);
        }
        return filterOptionsList;
    }
   
    @AuraEnabled
    public static Boolean checkNumber(String noOfDays) {
        if(noOfDays=='')
            return true;
        return Pattern.matches('^\\d+$', noOfDays);
    }
   
    /*
* Created by - Banshi Lal Dangi
* Date - 12-05-2020
* Modified by/Date - Added more option In case needed Action Line 201- Swarnima
* This functon is used to return Lightning datatable columns as well as data to be displayed based on a fieldset.
*/
    @AuraEnabled
    public static DataTableResponse getCaseRecords(List<String> statusList, String noOfDays, Boolean newCases,String strObjectName, String strFieldSetName, String ownerId){
        //Get the fields from FieldSet
        Schema.SObjectType SObjectTypeObj = Schema.getGlobalDescribe().get(strObjectName);
        Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();            
        Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get(strFieldSetName);
       
        //To hold the table hearders
        List<DataTableColumns> lstDataColumns = new List<DataTableColumns>();
       
        //Field to be queried - fetched from fieldset
        List<String> lstFieldsToQuery = new List<String>();
       
        //The final wrapper response to return to component
        DataTableResponse response = new DataTableResponse();
       
        for( Schema.FieldSetMember eachFieldSetMember : fieldSetObj.getFields() ){
            String dataType = String.valueOf(eachFieldSetMember.getType()).toLowerCase();
            //This way we can set the type of a column
            //We do not get the exact type from schema object which matches to lightning:datatable component structure
            if(dataType == 'datetime'){
                dataType = 'date';
            }
            //Create a wrapper instance and store label, fieldname and type.
            DataTableColumns datacolumns = new DataTableColumns( String.valueOf(eachFieldSetMember.getLabel()) ,
                                                                String.valueOf(eachFieldSetMember.getFieldPath()),
                                                                String.valueOf(eachFieldSetMember.getType()).toLowerCase() );
lstDataColumns.add(datacolumns);
            lstFieldsToQuery.add(String.valueOf(eachFieldSetMember.getFieldPath()));
        }
       
       
        System.debug('recentlyContacted: '+noOfDays);
        List<Case> allCases = new List<Case>();
        List<ID> caseIdList = new List<ID>();
        Set<Id> caseIdSet = new Set<Id>();
        List<String> filterConditionsList = new List<String>();
        List<String> newStatusList = new List<String>{'New', 'Review', 'Incomplete', 'Backlog', 'In Progress','In-Progress', 'Waiting for Customer Response'};
        Set<String> statusAtAllOpenCheckBoxList = new Set<String>();
        FINAL Integer LIMITCOUNTER = 500;
        String caseQuery = 'SELECT Id,' + String.join(lstFieldsToQuery, ',') +
                    ' FROM Case WHERE RecordTypeId = '+ '\''+SPT_Constants.SEC_RECORDTYPE_ID+'\''+
                    ' AND Enterprise_Security_Case_Number__c!=null';
               
        if(String.isNotBlank(ownerId)){
            caseQuery += ' AND OwnerId = :ownerId';
        }
        if(statusList!=null && !statusList.isEmpty()){
            caseQuery += ' AND Status IN: statusList';
            //filterConditionsList.add('Status IN: statusList');
        }
        else {
            caseQuery += ' AND Status = \'\'';
            //filterConditionsList.add('Status = \'\'');
        }
        System.debug('test1');
        if(noOfDays != null && noOfDays != ''){
            String closedStatus = 'Closed';
            if(Integer.valueOf(noOfDays) > 105 ){
                noOfDays = '105';
            }
            //caseQuery += ' AND Status != '+'Closed'+' ';
            for(CaseComment comment : Database.query('SELECT ID, ParentId, Parent.Status, CommentBody FROM CaseComment'+
                                        ' WHERE Parent.RecordTypeId = \''+SPT_Constants.SEC_RECORDTYPE_ID+'\''+
                                        ' AND Parent.Enterprise_Security_Case_Number__c!=null'+
                                        ' AND CommentBody LIKE \'%<<metadata>>%\''+
                                        //'AND Parent.Status != \''+ closedStatus + '\''+ 
                                        'AND Parent.isClosed != true'+
                                        ' AND LastModifiedDate >= LAST_N_DAYS:'+noOfDays+ 
                                        ' ORDER BY LastModifiedDate DESC LIMIT 500' )) {
                                            System.debug('comment -> '+ comment);//SM
              if(!caseIdList.contains(comment.ParentID)){
                    caseIdList.add(comment.ParentID); 
                  caseIdSet.addAll(caseIdList); //extra Sm
                }                
            }
            system.debug('test 2');
            System.debug('=== Case Id List === '+caseIdList);
            
            if(caseIdList!=null) {
                //filterConditionsList.add('Id IN: caseIdList');
                caseQuery += ' AND Id IN: caseIdList';
            }
            
        }
        
        if(newCases) {
            if((statusList.contains('Review') && statusList.contains('Ready for Assignment') && statusList.contains('In-Progress') && statusList.contains('New') && statusList.contains('In Progress')) && !statusList.contains('Closed') && !statusList.contains('Security - Approved Restricted') && !statusList.contains('Withdrawn') && !statusList.contains('Routed to a Different Security team') && !statusList.contains('Security - Approved') && !statusList.contains('Security- Not Approved') ){
				statusList.addAll(newStatusList);
                for(String s: statusList){
                    if(!s.contains('Ready for Assignment')){
                        statusAtAllOpenCheckBoxList.add(s);
                    }
                }
                //statusList.remove('Ready for Assignment');
                //statusAtAllOpenCheckBoxList.addAll(statusList);                
                caseQuery = 'SELECT Id,' + String.join(lstFieldsToQuery, ',') +
                    ' FROM Case WHERE RecordTypeId = '+ '\''+SPT_Constants.SEC_RECORDTYPE_ID+'\''+
                    ' AND Enterprise_Security_Case_Number__c!=null AND Status IN:statusAtAllOpenCheckBoxList';
            }else{
                 //filterConditionsList.add('Status = \'New\'');
            caseQuery += ' AND Status IN: newStatusList';
            }
           
        }
        System.debug('caseQuery after new case --> '+ caseQuery);
// Swarnima Chnage -start
        //Adding Ordering of cases
        if(noOfDays == null || noOfDays == '') {
            caseQuery += ' ORDER BY LastModifiedDate DESC';
        }
// SM chnage - Stop
        //Adding Limit
        caseQuery += ' LIMIT '+LIMITCOUNTER;
        System.debug('====== CASE QUERY ======'+caseQuery);
        //Form an SOQL to fetch the data - Set the wrapper instance and return as response
        if(! lstDataColumns.isEmpty()){            
            response.lstDataTableColumns = lstDataColumns;
            allCases = Database.query(caseQuery);
            response.lstDataTableData = allCases;
        }
       
        List<Case> updatedCaseList = new List<Case>();
        if(caseIdList != null && !caseIdList.isEmpty()) {
            for(Id caseId : caseIdList) {
                for(Case caseRecord : allCases) {
                    if(caseRecord.Id == caseId) {
                        updatedCaseList.add(caseRecord);
                    }
                }
            }
            response.lstDataTableData =  updatedCaseList;
        }  
        return response;
    }
   
    //Wrapper class to hold Columns with headers
    public class DataTableColumns {
        @AuraEnabled
        public String label {get;set;}
        @AuraEnabled      
        public String fieldName {get;set;}
        @AuraEnabled
        public String type {get;set;}
        @AuraEnabled
        public Boolean sortable = true;
       
        //Create and set three variables label, fieldname and type as required by the lightning:datatable
        public DataTableColumns(String label, String fieldName, String type){
            this.label = label;
            this.fieldName = fieldName;
            this.type = type;            
        }
    }
   
    //Wrapper calss to hold response - This response is used in the lightning:datatable component
    public class DataTableResponse {
        @AuraEnabled
        public List<DataTableColumns> lstDataTableColumns {get;set;}
        @AuraEnabled
        public List<sObject> lstDataTableData {get;set;}                
       
        public DataTableResponse(){
            lstDataTableColumns = new List<DataTableColumns>();
            lstDataTableData = new List<sObject>();
        }
    }
}
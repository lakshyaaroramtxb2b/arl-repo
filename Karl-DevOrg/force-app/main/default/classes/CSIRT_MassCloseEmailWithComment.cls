public with sharing class CSIRT_MassCloseEmailWithComment {
    public class ActionException extends Exception {}
    
    private ApexPages.StandardSetController setCon;
    public String closingComment {get;set;}
    public String closingStatus {get;set;}
    public boolean ispublic {get;set;}
    
    public CSIRT_MassCloseEmailWithComment(ApexPages.StandardSetController controller) {
        setCon = controller;
    }

    public PageReference addcomment() {
        if (!Case.Status.getDescribe().isUpdateable()) {
            throw new ActionException('You don\'t have update access to Case->Status fool!');
        }
        
        Case[] selectedCases = (List<Case>) setCon.getRecords();
        Id[] selectedCaseIds = new Id[]{};
        for (Case c : selectedCases) { selectedCaseIds.add(c.Id); }
        selectedCases = [SELECT Id,Status,ParentId,case.contact.Name,case.contact.Email FROM Case WHERE Id IN:selectedCaseIds]; //added ParentId to get the ParentId. Don't think its neccessary.
        
        CaseComment[] comments = new CaseComment[]{};  
        for (Case c : selectedCases) { 
            if (closingComment != null && ispublic == true) {
                comments.add(new CaseComment(parentId=c.Id,CommentBody=closingComment,isPublished=True)); 
            }
             if (closingComment != null && ispublic != true) {
                comments.add(new CaseComment(parentId=c.Id,CommentBody=closingComment,isPublished=False)); 
            }
                
            c.Status = closingStatus;
            c.OwnerId = UserInfo.getUserId();
        }
        if (comments.size() > 0) {
            insert comments; 
        }
        update selectedCases;
        
        // lame service console wizardRetUrl parameter
        String ret = ApexPages.currentPage().getParameters().get('retURL') != null ? ApexPages.currentPage().getParameters().get('retURL') : ApexPages.currentPage().getParameters().get('wizardRetURL');
        return new PageReference(ret);
    }
    
    public List<SelectOption> getClosedStates() {
        
        List<SelectOption> states = new List<SelectOption>();
        for (PicklistEntry pl : Case.Status.getDescribe().getPicklistValues()) {
            // i can't figure out how to check if a state is officially a closed state.
            // I don't think we support it so here's my ghetto workaround
            if (pl.getLabel().startsWithIgnoreCase('closed')) {
               states.add(new SelectOption(pl.getValue(),pl.getLabel() ) );
            }
        }
        return states;
        
        /*Schema.sObjectType objType = Case.getSObjectType();
        list<SelectOption> options = CSIRT_picklistSelectUtil.getPicklistValues(objType, 'Status');
      	return options;
        
    }
    public List<SelectOption> getClosedOrigin() {
      
        Schema.sObjectType objType = Case.getSObjectType(); 
        list<SelectOption> options = CSIRT_picklistSelectUtil.getPicklistValues(objType, 'Origin');
      	return options;*/
    }
}
public class BugReport {

    public String reportb64 {get; private set;}

    public BugReport(ApexPages.StandardController stdController) {
    
        stdController.addFields(new List<String> {'Body'}); 
        Document bugReport = (Document)stdController.getRecord();               
        this.reportb64 = EncodingUtil.base64Encode(bugReport.Body); 
        
    }
}
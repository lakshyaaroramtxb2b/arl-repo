public with sharing class PRT_SLATriggerHandler {
    public static void beforeInsert(List<SLA__c> slaList, Map<Id,SLA__c> idToSLAMap){
        PRT_SLATriggerHelper.setDefaultDueDate(slaList, idToSLAMap);
    }

    public static void afterInsert(List<SLA__c> newList, Map<Id, SLA__c> oldMap) {
        System.debug('INSIDE AFTER INSERT');
        PRT_SLATriggerHelper.createSLAMetricRecord(newList, null);
    }

    public static void beforeUpdate(List<SLA__c> slaList, Map<Id,SLA__c> idToSLAMap){
        List<SLA__c> newSLAList = new List<SLA__c>();
        if(slaList!=null && !slaList.isEmpty()) {
            for(SLA__c slaRec : slaList) {
                if(idToSLAMap.get(slaRec.Id).Reporting_Period__c != slaRec.Reporting_Period__c) {
                    newSLAList.add(slaRec);
                }
            }
        }
        PRT_SLATriggerHelper.setDefaultDueDate(newSLAList, idToSLAMap);
    }

    public static void afterUpdate(List<SLA__c> newList, Map<Id, SLA__c> oldMap) {
        PRT_SLATriggerHelper.createSLAMetricRecord(newList, oldMap);
    }
}
public with sharing class SecToolsWebAppScannerRequestController {
    private static final Id ORG_WIDE_EMAIL_ADDRESS_ID = '0D2300000004DuF';

    public static PageReference redirect_to_ZAP() {
        
        return Page.Security_Tools_WebApp_ZapBrowserSetup;
    
    }
    
    public PageReference forceSSL(){
        return SecUtilitySSL.ForceSSLAndRedirectIfNot();
        
    }
    public static boolean login_user_api(String loginUser, String pass) {
        if (loginUser == null || pass == null || loginUser == '' || pass == '') return false;
        if (!loginUser.toLowerCase().endsWith('@partnerforce.com')) return false; //restrict to partnerforce emails
        try {
            partnerSoapSforceCom.Soap conn = new partnerSoapSforceCom.Soap();
            partnerSoapSforceCom.LoginResult lr = conn.login(loginUser, pass);
            return false;
        } catch (Exception e){
            if (e.getMessage().contains('API_CURRENTLY_DISABLED')){
                return true;
            } else {
                return false;
            }
        }
        return false;
    }    

    
        
    
    public String fullname{get; set;}
    public String email{get; set;}
    public Boolean checked{get; set;}
    public String notification {get;set;}
    public String program{get; set;}
    public String appname{get; set;}
    public String company{get; set;}
    
    public PageReference process() {
    
        
        Pattern validEmail = Pattern.compile('^.+@.+$');
        Pattern whiteSpace = Pattern.compile('^\\s*$');
        Matcher emailMatcher = validEmail.matcher(email);
        if ((email == null) || fullname == null || program == null || appname == null || company == null ||
            (validEmail.matcher(email).matches() == False) || (whiteSpace.matcher(email).matches()==True)){
            //PageReference p = new PageReference('/webappscanner#invalid');
            //p.setRedirect(false);
            //return p;
            this.notification = 'Please fill out the form entirely and be sure to enter a valid email address.';
            return null;
        }
        if(checked == False)
        {
            //PageReference pu = new PageReference('/webappscanner?unchecked');
            //pu.setRedirect(false);
            //return pu;
            this.notification = 'The checkbox above must be checked in order to receive a copy of the Web Application Security Scanner.';
            return null;
            
        }

        
        this.notification = 'Thank you for submitting.  Our team will be in contact with you soon.';
        return null;
        //PageReference pa = new PageReference('/webappscanner?complete');
        //pa.setRedirect(false);
        //return pa;

    }
    
    @RemoteAction
    public static Map<Boolean,String> requestLicense(String name,String email, String companyName, String applicationName,String isvProgram,Boolean conditionsAccepted, String ppU, String ppP){
        Pattern validEmail = Pattern.compile('^.+@.+\\..+$');
        Matcher emailMatcher = validEmail.matcher(email);


        if (!login_user_api(ppU, ppP)){
            return new Map<Boolean, String>{False=>'Invalid Partner Portal credentials. Please check to make sure they work on the Partner Portal login page.'};
        }

        if ((name == '') || (name == null) || (email == null) || (emailMatcher.matches()==False) || (email == null) || (companyName == '') || (companyName == null) || (applicationName == '') || (applicationName == null) || (isvProgram == '') || (isvProgram == null) || (!email.contains('@')) || (conditionsAccepted == False)){
            return new Map<Boolean,String> {False=>'Please complete all fields and accept Terms and Conditions'};
        }

        WebAppScanner__c scan = new WebAppScanner__c();
        

        scan.X62_Partner_Eligible__c = 'Unknown';

        //validate partner status with ISV opps of correct level
        try {
            AppX62.PartnerAccountInfo partnerAppInfo = AppX62.getQualifiedAppsForPartnerforceUser(ppU);
            if (partnerAppInfo != null){
                //fill account info
                scan.X62_Account_ID__c = partnerAppInfo.AccountId;
                scan.X62_Account_Name__c = partnerAppInfo.AccountName;
                
                if (partnerAppInfo.hasValidAppsForBurpLicense()){
                    scan.X62_Partner_Eligible__c = 'Yes';
                } else {
                    //no ISV partner apps are available at all
                    //this partnerforce account is "not a partner"
                    scan.X62_Partner_Eligible__c = 'No';
                    scan.Reason_for_Denying__c = 'No Partner Offering';
                    scan.Status__c = 'Not-Approved';
                }
            } else {
                return new Map<Boolean,String> {False=>'Unable to locate account for your Partner Portal username. '};
                //no partner account??
                //this theoretically should never happen due to the data model
                //in 62 since every user has to be part of an account.
            }
        } catch (Exception e){
            //debug stuff
            AppX62.emailAMessage('jdolph@salesforce.com', 'burp scan partner validation error - Password is probably busted', ''+e.getMessage());
            return new Map<Boolean,String> {False=>'Unable to validate your Partner Portal credentials due to a service issue.  Please try again in a few minutes.'};
            
            //for now eat the catch in case something went wrong in the flow with 62 validation
            //so we dont lose the data. in the future if we decide to make the 62 lookup a full
            //gatekeeper, we should make some gui response about the service being down or something
        }

        
        scan.Name__c = name;
        scan.email__c = email;
        scan.AcceptConditions__c = conditionsAccepted;
        scan.program__c = isvProgram;
        scan.appname__c = applicationName;
        scan.company__c = companyName;
        scan.Partner_Portal_Username__c = ppU;
        
        try {
            insert scan;
        
            
            Messaging.reserveSingleEmailCapacity(1);
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String[] toAddresses = new String[] {email};
            mail.setToAddresses(toAddresses);
            //mail.setReplyTo('appxburprequesthandler@4-37kslu4xbnu4oyekv2vw13rpgq8m9fqmh7ilg1chb93tmwyw.3-jsbieac.3.apex.salesforce.com');
            mail.setSubject('Burp License Application Received');
            mail.setOrgWideEmailAddressId(ORG_WIDE_EMAIL_ADDRESS_ID);
            
            string body = 'Hello ' + name + ', \r\n' + 
            '\r\n' + 
            'We have received your Burp License Application for ' + applicationName + '.  If you are eligible for a license and do not already have an existing one for your company, the vendor will send the registration info to you within 7 days.'+
            '\r\n\r\n' + 
            'Thanks, ' + 
            '\r\n' + 
            'Salesforce.com Product Security Team';
            
            mail.setPlainTextBody(body);
            
            Messaging.sendEmail(new Messaging.SingleEmailMessage[]{mail});
        
        }
        catch (Exception e) {
            // invalid email or something. DB-level validation failed
            return new Map<Boolean,String> {False=>'Invalid submission. Please complete all fields properly.'};
        }
        if (scan.X62_Partner_Eligible__c == 'No'){
             return new Map<Boolean,String> {False=>'We were unable to find any eligible applications on file for the partner portal credentials you provided.  Please check to see that you are using the correct credentials for your company and application. If you are still encountering this error, please contact your Account Executive to ensure that your ISV application records are linked correctly with your partner portal account.'};
        } else {
           return new Map<Boolean,String> {True=>'Thank you for submitting.  Our team will be in contact with you soon.'};
        }
    }


}
/**
 * @author Ben Harvie
 * @email ben.harvie@salesforce.com
 * @description This is schedulable and batch class to close Cycle Requests upon completion of all ARIs
 */
public with sharing class KARL_CycleRequestClosureBatch implements Database.Batchable<sObject>, Schedulable{
    /**
    * @author Ben Harvie
    * @email ben.harvie
    * @description This method is for schedulable interface to execute in batches of 200
    */
    public void execute(SchedulableContext sc) {
        database.executebatch( new KARL_CycleRequestClosureBatch(), 200);
    }

    /**
    * @author Ben Harvie
    * @email ben.harvie
    * @description get the context for the batch class - audit cycle must be active
    */
    public Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator([SELECT Id,Cycle_Request_Status__c FROM Cycle_Request_Item__c
                                        WHERE Audit_Cycle__r.Active__c = true]);
    }

    /**
    * @author Ben Harvie
    * @email ben.harvie
    * @description process the cycle request updates / batch class execution
    */
    public void execute(Database.BatchableContext BC, List<Cycle_Request_Item__c> cycleRequestItemList){
        System.debug(LoggingLevel.DEBUG, '--- START Processing Cycle Request Closure Batch ---');
        // Get all associated Auditor Request Items
        List<KARL_Auditor_Request_Item__c> allAuditorReqItem = [SELECT Id,KARL_Status__c,KARL_Cycle_Request_Item__c,
                                                                KARL_Cycle_Request_Item__r.Cycle_Request_Status__c
                                                                FROM KARL_Auditor_Request_Item__c
                                                                WHERE KARL_Cycle_Request_Item__c IN :cycleRequestItemList];

        // Populate Map of Cycle Requests and Auditor Request Items
        Map<Id,List<KARL_Auditor_Request_Item__c>> creqWithAri = new Map<Id,List<KARL_Auditor_Request_Item__c>>();
        for(KARL_Auditor_Request_Item__c auditorReqItem : allAuditorReqItem){
            if(!creqWithAri.containsKey(auditorReqItem.KARL_Cycle_Request_Item__c)){
                creqWithAri.put(auditorReqItem.KARL_Cycle_Request_Item__c,new List<KARL_Auditor_Request_Item__c>());
            }
            creqWithAri.get(auditorReqItem.KARL_Cycle_Request_Item__c).add(auditorReqItem);
        }

        // For each Cycle Request, determine if all Auditor Request Items are in status 'Testing Completed'
        Set<Id> updtCycleReqItemSet = new Set<Id>();
        for(Id cycleReqItemId : creqWithAri.keySet()){
            Boolean flag = true; // default to true
            if(creqWithAri.get(cycleReqItemId) != null &&
               !creqWithAri.get(cycleReqItemId).isEmpty() ){
                
                // Check each of the ARIs
                for(KARL_Auditor_Request_Item__c auditorReqItem : creqWithAri.get(cycleReqItemId)){
                    if(auditorReqItem.KARL_Status__c != 'Testing Completed'){
                        flag = false; // if ANY Audit Request isn't complete; break the flag
                        break;
                    }
                }
                
                // Update the Set (unique Id) for the ARI Testing Completed
                if(flag){
                    updtCycleReqItemSet.add(cycleReqItemId);
                }
            }
        }

        System.debug(LoggingLevel.DEBUG, 'List of Cycle Requests to update: ' + updtCycleReqItemSet);

        // Get the Cycle Requests that need to be updated
        List<Cycle_Request_Item__c> updtCycleReqItems = new List<Cycle_Request_Item__c>();
        updtCycleReqItems = [ SELECT Id,Cycle_Request_Status__c
                            FROM Cycle_Request_Item__c
                            WHERE Id IN :updtCycleReqItemSet];

        // Set the status
        for(Cycle_Request_Item__c cycleReqItem : updtCycleReqItems){
            cycleReqItem.Cycle_Request_Status__c = 'Closed';
        }
        
        // Save/commit the updates
        if( !updtCycleReqItems.isEmpty() && Schema.sObjectType.Cycle_Request_Item__c.isUpdateable() 
               && Cycle_Request_Item__c.Cycle_Request_Status__c.getDescribe().isUpdateable()){
                update updtCycleReqItems;
        }

        System.debug(LoggingLevel.DEBUG, '--- END Processing Cycle Request Closure Batch ---');
    }

    /**
    * @author Ben Harvie
    * @email ben.harvie
    * @description finish the batch class (no actions)
    */
    public void finish(Database.BatchableContext BC) {
    }

}
/**
* @author Lakshya Arora
* @email lakshya.arora@mtxb2b.com
* @description Handler class for Task trigger operations
*/
public class KARL_TaskTriggerHandler {
    public static void afterUpdateOperations(List<Task> newList,Map<Id,Task> oldMap){
        KARL_TaskTriggerHelper.markRelatedCycleAuditReadyToSign(newList,oldMap);
    }
}
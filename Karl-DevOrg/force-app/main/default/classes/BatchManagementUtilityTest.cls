/*
* Class Name: BatchManagementUtilityTest
* Description: Test class for Batch Utility Manager to automatically start aborted/not
* active batches and notify those responsible
* Author/Date: Luke Slevin / 10.22.2019
* Date New/Modified: 10.22.2019
*
*/
@isTest
public with sharing class BatchManagementUtilityTest {
    static testMethod void checkForRunningBatches()
    {
        BatchManagementUtility utilityBatch = new BatchManagementUtility();
        DataBase.executeBatch(utilityBatch);

    }

}
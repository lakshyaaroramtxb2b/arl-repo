public class LeaderIndvidualRecord {
	public String id {get; set;}
    public String caseId {get; set;}
    public String Name {get; set;}
    public String subject {get; set;}
    public String caseOwner {get; set;}
    public String status {get; set;}
    public String subStatus {get; set;}
    public String csirtAdjudication {get; set;}
    public String csirtSummary {get; set;}
    public String alertCriteria {get; set;}
    public String alertCriteriaName {get; set;}
    public String createdBy {get; set;}
    public DateTime createdDate {get; set;}
}
/***********************************************************
 * Handler class for SPT_CaseTrigger
 * Created by 	- Swarnima Singh Mandhata
 * Date 		- 23rd December 2019
 ************************************************************/
public class SPT_CaseTriggerHandler {
    public static void beforeInsert(List<Case> caseList){
    }

    public static void afterInsert(List<Case> caseList){ 
        SPT_CaseTriggerHelper.updateCaseStatusRecord(caseList, null);      
    }

    public static void beforeUpdate(List<Case> caseList, Map<Id,Case> idToCaseMap){
    }

    // Below method runs on after update of case record
    public static void afterUpdate(List<Case> caseList, Map<Id,Case> idToCaseMap){
        // Below check prevents Future to Future and Batch to Future method calling
        if(!(System.isBatch() || System.isFuture())){
            SPT_CaseTriggerHelper.updateCaseStatusRecord(caseList, idToCaseMap);   
            SPT_CaseTriggerHelper.updateInsertSupportCaseRecord(caseList, idToCaseMap);
        }
    }
}
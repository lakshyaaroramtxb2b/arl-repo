public class CSIRT_HandleDetectionAlert {
    Private static Map<String, Id> environments = new Map<String, Id>();
    Private static Map<String, Id> rts = new Map<String, Id>();
    Private static Map<Id, Map<String, String>> dads = new Map<Id, Map<String, String>>();
    Private static List<Case> new_Cases = new List<Case>();
    Private static Map<String, ID> assetMap = new Map<String, ID>();
    Private static Map<String, ID> userAccountMap = new Map<String, ID>();
    Private static Map<Id, Detection_Alert__c> detectionAlertMap;
    Private static Map<Id, Detection_Alert_Criteria__c> alertCriteriaMap;
    Private static List<CSIRTAlert> alertList = new List<CSIRTAlert>();
    
    @InvocableMethod (label='ProcessAlert' description='Processes the Alert')
    public static void ProcessAlert(List<IntakeAlert> alerts){
        
        for(IRCloud__Environment__c e : [SELECT SDCloud__Environment_Log_Name__c, Id FROM IRCloud__Environment__c]){
            environments.put(e.SDCloud__Environment_Log_Name__c, e.Id);
        }
    
        for(RecordType rt : [SELECT DeveloperName, Id FROM RecordType WHERE Sobjecttype='Case']){
            rts.put(rt.DeveloperName, rt.Id);
        }
    
        Set<Id> daIDs = new Set<Id>();
        
        for(IntakeAlert alert : alerts){
            daIDs.add(alert.DetectionAlert); 
        }
        
        Set<String> hostnames = new Set<String>();
        Set<String> userAccounts = new Set<String>();
        Set<String> ipAddresses = new Set<String>();
        
        
        Set<Id> alertCriteriaIds = new Set<Id>();
        detectionAlertMap = new Map<Id, Detection_Alert__c>([SELECT Id, DetectionAlertCriteria__c FROM Detection_Alert__c WHERE Id IN :daIDs]);
        for(Detection_Alert__c da : detectionAlertMap.values()){
            alertCriteriaIds.add(da.DetectionAlertCriteria__c);
        }
        
        alertCriteriaMap = new Map<Id, Detection_Alert_Criteria__c>([SELECT Id, Name FROM Detection_Alert_Criteria__c WHERE Id IN :alertCriteriaIds]);
        
        
        for(Detection_Alert_Data__c dad : [SELECT Detection_Alert__c, FieldName__r.Name, FieldValue__c FROM Detection_Alert_Data__c WHERE Detection_Alert__c IN :daIDs]){
            if (dads.containsKey(dad.Detection_Alert__c)){
                dads.get(dad.Detection_Alert__c).put(dad.FieldName__r.Name, dad.FieldValue__c);
            } else {
                dads.put(dad.Detection_Alert__c, new Map<String, String>{dad.FieldName__r.Name => dad.FieldValue__c});
            }
            
            if (dad.FieldName__r.Name == 'HostnameSource' || dad.FieldName__r.Name == 'HostnameDestination'){
                if (!hostnames.contains(dad.FieldValue__c)){
                    hostnames.add(dad.FieldValue__c);
                }
            }
            
            if (dad.FieldName__r.Name == 'UserDestination' || dad.FieldName__r.Name == 'UserSource'){
                if (!userAccounts.contains(dad.FieldValue__c)){
                    userAccounts.add(dad.FieldValue__c);
                }
            }
            
            if (dad.FieldName__r.Name == 'IPSource' || dad.FieldName__r.Name == 'IPDestination'){
                if (!ipAddresses.contains(dad.FieldValue__c)){
                    ipAddresses.add(dad.FieldValue__c);
                }
            }
        }

        for(IntakeAlert alert : alerts){
            System.debug('INTAKE ALERT CREATE');
            CreateAlert(alert); 
        }
        
        SendResponseOrgAlerts alertSender = new SendResponseOrgAlerts(alertList);
        
        ID jobID = system.enqueueJob(alertSender);
        
    }
    
    public static void CreateAlert(IntakeAlert alert) {
        try{
            System.debug('Creating Response Org Alert');
            
            Id detectionCriteriaId = detectionAlertMap.get(alert.DetectionAlert).DetectionAlertCriteria__c;
            String detectionCriteriaName = alertCriteriaMap.get(detectionCriteriaId).Name;
			CSIRTAlert newAlert =  new CSIRTAlert((String)alert.DetectionAlert, alert.Environment, alert.Subject, alert.Description, alert.Severity, alert.RecipientTeam, (String)detectionCriteriaId, detectionCriteriaName, alert.OrionAlertScore);
			alertList.add(newAlert);
        } catch (Exception e){
            System.debug(e);
        }
    }
    
       
    public class IntakeAlert {
    
        @InvocableVariable(required=true)
        public Id DetectionAlert;
    
        @InvocableVariable(required=true)
        public String Environment;
        
        @InvocableVariable(required=true)
        public String Subject;
    
        @InvocableVariable(required=true)
        public String Description;
        
        @InvocableVariable(required=false)
        public String Severity;
        
        @InvocableVariable(required=false)
        public String RecipientTeam;
        
        @InvocableVariable(required=true)
        public Datetime EventTimestamp;
        
        // orion: alert score
        @InvocableVariable(required=false)
        public String OrionAlertScore;
    }
}
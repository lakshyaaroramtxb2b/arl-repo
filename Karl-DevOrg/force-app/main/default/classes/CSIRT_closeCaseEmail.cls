public with sharing class CSIRT_closeCaseEmail {

    private final Case c;
    private ApexPages.StandardController con;
    public String closingComment {get;set;}  
    public String closingStatus {get;set;}
    public String Email_category {get;set;}
    public String emailTemplate {get;set;}
    public String parentCaseNumber {get;set;}
    public boolean isPublic {get;set;}
    public boolean sendTemplate {get;set;}
 
    public CSIRT_closeCaseEmail(ApexPages.StandardController stdController) 
    {
    	con = stdController;
        this.c = (case)con.getRecord();
        Boolean nonemp = false;                                                         //Variable to track if non-employee
        List<case> nonempcase = new List<case>();                   //List to store non employee cases for display later
        Document_Links__c Links = Document_Links__c.getOrgDefaults();
        String etLink = Links.ETEmailguide__c;
        System.debug('********** the value of the email is ' + c.Contact.Email);                                                         
        if (string.isnotblank(c.Contact.Email)) 
        {
        	if (!c.Contact.Email.contains('@salesforce.com') && !c.Contact.Email.contains('@exacttarget.com'))
            {
                System.debug('******** Contact Email ' + c.Contact.Email);
                nonemp = true;
                nonempcase.add(c);
            }
			if (c.Contact.Email.contains('@exacttarget.com'))
			{
				ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,
                'One or more contacts for the case/s you have selected is an ExactTarget employee. Please follow the guidelines in the Security@ExactTarget Email Handling document.'));
        	}                    
        }
    if (nonempcase.size() > 0)
    {
        if (nonempcase.size() ==1)
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,
            'A contact for a case you have selected is not a salesforce.com employee'));
        }
        else 
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,
            'You have selected cases whose contacts are not salesforce.com employees'));
        } 
    }
}

public PageReference closeCases() 
{
	Id parentCaseIDparent = null;       //creating an ID to hold the value of the proposed parent cases parent (if it exists)
	Id parentCaseID = null;             //Id of propsed parent           
	
    //Check if the supplied proposed parent case number is not blank
    if (string.isNotBlank(parentCaseNumber))
    {       
        System.debug('*************************' + ' ParentCaseNumber is : ' + parentCaseNumber);
        List<Case> casea = [                      //Select the id and parent Id of the proposed parent case.
        Select Id, parentId, caseNumber                         
        from case 
        Where CaseNumber = :parentCaseNumber];
        
        if (!casea.isEmpty())
        {
            parentCaseID = casea[0].Id;     //put the selected IDs into their respective IDs.
            parentCaseIDparent = casea[0].ParentId;
            System.debug('*************************' + ' ParentCaseID is : ' + parentCaseID);
            System.debug('*************************' + ' ParentCaseIDparent is : ' + parentCaseIDparent);
        }
        else if (casea.size()==0)
        {
        	String errorMessage4 = 'The case number entered to assign as parent is invalid';
       		ApexPages.Message myMsg4 = new ApexPages.Message(ApexPages.Severity.ERROR,errorMessage4);
            ApexPages.addMessage(myMsg4);
            return ApexPages.currentPage();
        }
	}
                
    else if ((closingStatus == 'Closed - Investigation' || closingStatus == 'Closed - Incident') && (parentCaseID==null || parentCaseId==' '))
    {
        String errorMessage2 = 'You need to add a parent if you are closing with status Closed - Investigation '+
                                                  '<br> or Closed - Incident. Please create an incident or investigation case first'+
                                                  '<br> and then put the case number in the Parent Case field to close these cases.';
        ApexPages.Message myMsg2 = new ApexPages.Message(ApexPages.Severity.ERROR,errorMessage2);
                ApexPages.addMessage(myMsg2);
                return ApexPages.currentPage();
    }
    
    case c1 = (case)con.getRecord();
    Id caseid;
    caseid = c1.Id;
    c1 = [SELECT Id,Status,ParentId,caseNumber,recordtypeId 
    FROM Case
    WHERE Id = :c1.id ]; 
        
    CaseComment[] comments = new CaseComment[]{}; 
    Boolean validParent = true;
    
    System.debug('*************************' + ' ParentCaseID before assigning is : ' + parentCaseID);
            //Make sure selected case isn't the parent of the submitted parent case
    if (isParent(c1, parentCaseIDparent)) 
    {
            validParent=false;
            String errorMessage = 'Invalid Parent - Selected case is the parent of submitted parent'+
                                                      '<br>Selected case with issue: '+c.caseNumber;
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,errorMessage);
                    ApexPages.addMessage(myMsg);
    }
        //Make sure a case isn't being assigned as a parent of itself
    if (c1.caseNumber==parentCaseNumber) 
    {
    validParent=false;
    String errorMessage = 'Invalid Parent - You are trying to make a case a parent of itself.'+
                                              '<br>Selected case with issue: '+c.caseNumber;
    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,errorMessage);
            ApexPages.addMessage(myMsg);
	}
  
    if (string.isnotblank(closingComment) && isPublic == true) 
    {
        System.debug('Closing public comment is: ' + closingcomment);
        comments.add(new CaseComment(parentId=c1.Id,CommentBody=closingComment,isPublished=True)); 
    }
    
    if (string.isnotblank(closingComment) && isPublic != true) 
    {
        System.debug('Closing comment is: ' + closingcomment);
        comments.add(new CaseComment(parentId=c1.Id,CommentBody=closingComment,isPublished=False)); 
    }
    
    if (parentCaseID!=null)
    {
        c1.ParentId = parentCaseID;
        System.debug('*************************' + ' ParentCaseID AFTER assigning is : ' + parentCaseID);
    }
            
    /*Force user to categorize email cases on close */
    List<RecordType> typeList = new List<RecordType>(CSIRT_caseCache.EmailRtId());
    RecordType r = typeList.get(0);
    System.debug('***** Email_Category is: ' + Email_category);
    if (c1.RecordTypeId == r.id)
    {
    	System.debug('***** Case Status is: ' + c.status);
    	System.debug('***** chosen closing status is: ' + closingStatus);
    	
    	if ((string.isblank(email_category) || email_category.equals('--None--')) 
    	&& closingStatus.contains('Closed'))
    	{
    		String errorMessage = 'When closing an email record type you must select a category.';
                ApexPages.Message myMsg3 = new ApexPages.Message(ApexPages.Severity.ERROR,errorMessage);
                        ApexPages.addMessage(myMsg3);
                        return ApexPages.currentPage();
    	}
    c1.email_category__c = Email_Category;
    System.debug('***** c.email_category__c ' + c1.email_category__c);
    
    } 

            c1.Status = closingStatus;
            c1.OwnerId = UserInfo.getUserId();
            System.debug('email template value is ' + emailTemplate);
            if(emailTemplate != null)
            {
            	System.debug('c.Send_Response_Template__c value is ' + c1.Send_Response_Template__c);	
            	c1.Send_Response_Template__c = emailTemplate;
            }

        System.debug('$$$$$$$' + validParent);
        if (validParent)
        {
            if (comments.size() > 0) 
            {
                insert comments; 
            }
                
            update c1; 
                
            //May Have to update in order to return the correct page in console.
            PageReference pageRef1 = new PageReference('/' + c1.id);
            return pageRef1;
        }
        else
        {
                PageReference pageRef = ApexPages.currentPage();
                return pageref;
        }
    }
    
    private Boolean isParent(Case selectedCase, Id parentCaseIdparent)
    {
        if (parentCaseIDparent == selectedCase.id) 
        {
                System.debug('parentcaseID =  ' + parentCaseIDparent +' selectedcase.parentid= ' + selectedCase.parentId);
                System.debug('******* Case Number: ' + selectedCase.caseNumber);
                return true;
        }
        else 
        {
                return false; 
        }
    }
     
    public List<SelectOption> getClosedStates() 
    {   
        List<SelectOption> states = new List<SelectOption>();
        for (PicklistEntry pl : Case.Status.getDescribe().getPicklistValues()) 
        {
            if (!pl.getLabel().containsIgnoreCase('On Hold') && 
            !pl.getLabel().containsIgnoreCase('Escalated') &&
            !pl.getLabel().containsIgnoreCase('New') &&
            !pl.getLabel().containsIgnoreCase('Updated'))
            { 
            	states.add(new SelectOption(pl.getValue(),pl.getLabel() ) ); 
        	}	           
       	}
        return states;       
    }
	
	public List<SelectOption> getResponseTemplate() 
    {
        List<SelectOption> template = new List<SelectOption>();
        template.add(new SelectOption('','--None--'));
        List<EmailTemplate> elist = [
	        Select Name, Id, DeveloperName 
	        From EmailTemplate 
	        Where folder.developername = 'CSIRT_Templates'];
	    for (emailTemplate e : elist)
        {
           	template.add(new SelectOption(e.Name ,e.Name)); 
        }	
    return template;       
	}
	
	public List<SelectOption> getEmailCategory() 
	{
	    List<SelectOption> categories = new List<SelectOption>();
	    categories.add(new SelectOption('','--None--'));
	    for (PicklistEntry pl : Case.Email_category__c.getDescribe().getPicklistValues()) 
	    {
	         categories.add(new SelectOption(pl.getValue(),pl.getLabel() ) ); 	           
	    }
	    return categories;
	}
}
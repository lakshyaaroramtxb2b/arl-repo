@RestResource(urlMapping='/JSCaseGraphControl')
global with sharing class JSCaseGraphControllerExtension {

    private final Case seedCase;
    
    static Integer searchDepth = 0;
    static Integer kMaxSearchDepth = 90;
    
    public JSCaseGraphControllerExtension(ApexPages.StandardController stdController) {
        this.seedCase = (Case)stdController.getRecord();
    }

	@HttpGet
    global static String doGet() {
        return 'Worked';
    }
    
	@RemoteAction
	global static String getCase(String caseID) {
        
        JSGraph graph = new JSGraph();
        
        try
        {
            //get seed case
            Case c = [SELECT Id, CaseNumber, Detection_Logic__r.Name, Status, CreatedDate, Subject, Alert_Name__c, Alert_Source_IP__c, Destination_IP_Address__c 
                      FROM Case 
                      WHERE CaseNumber = :caseID LIMIT 1];
            
            System.debug('Root ID :' + c.Id);
            System.debug('Root IPs ' + c.Alert_Source_IP__c + ', ' + c.Destination_IP_Address__c);
            
            Set<ID> processedCases = new Set<ID>();
            
            //seed case
            digestAlert(c, processedCases, graph, true);
            
            //done return JSON result
            return JSON.serializePretty(graph);
    	}
        catch (exception e)
    	{
        	return 'ERROR: ' + e.getMessage() + ',' + e.getStackTraceString();
    	}
	}
    
    //only create node if we dont yet have a node with that key
    public static JSCaseNode createOrGetNode(Case c, JSCaseNode.ENodeType nodeType, JSGraph graph)
    {
        String key = getCaseKey(c, nodeType);
        
        JSCaseNode node = graph.getNodeByKey(key);
        
        if(node == null)
        {
            node = new JSCaseNode(c, key, nodeType);
            
            graph.addNode(node);
        }
        else
        {
            node.addCaseRef(c);
        }
        
        return node;
    }

    public static void digestAlert(Case c, Set<ID> processedCases, JSGraph graph, Boolean recursive)
    {
        //governer limit protection
        searchDepth++;
        if(searchDepth > (kMaxSearchDepth))
        	return;
        //
        //
        
        System.debug('digestAlert caseid = ' + c.Id);
        
        processedCases.add(c.Id);
        
        //build graph from current alert
        
        JSCaseNode n0 = createOrGetNode(c, JSCaseNode.ENodeType.SOURCE_ENDPOINT, graph);
        JSCaseNode n1 = createOrGetNode(c, JSCaseNode.ENodeType.EVENT, graph);
        
        JSCaseEdge e0 = graph.getEdge(n0, n1);
        
        if(e0 == null)
        	e0 = graph.addEdge(n0, n1);
        else
            e0.addInstance();
        
        JSCaseNode n2 = createOrGetNode(c, JSCaseNode.ENodeType.DESTINATION_ENDPOINT, graph);
        
        JSCaseEdge e1 = graph.getEdge(n1, n2);
        
        if(e1 == null)
        	e1 = graph.addEdge(n1, n2);
		else
            e1.addInstance();
        
        mapNodeRelationShips(n0, n1, n2, e0, e1);
        
        //we want to show the path of the alert
        if(searchDepth == 1) //becouse we increment at start of function
        {
            e0.color = 'red';
            e1.color = 'red';
            
            n0.size *= 2;
            n1.size *= 2;
            n2.size *= 2;
        }
        
        //recusive
        if(recursive)
        {
            //We currently only search for matches with a SRC and DST that are not full so that we can split into 3 parts
            //should change so it look for events with at least one part not null
            
            //simular cases by event
            //need a way to prevent endless loop. keep track of caseIds processed and not reprocesses?
            List<Case> simCasesByEvent = [SELECT Id, CaseNumber, Status, CreatedDate, Detection_Logic__r.Name, Subject, Alert_Name__c, Alert_Source_IP__c, Destination_IP_Address__c 
                                          FROM Case 
                                          WHERE /* Alert_Name__c =: c.Alert_Name__c AND Remove this to get all alerts ?*/
                                                ((Alert_Source_IP__c =: c.Alert_Source_IP__c AND Alert_Source_IP__c <> '') OR (Destination_IP_Address__c =: c.Destination_IP_Address__c AND Destination_IP_Address__c <> '') 
                                                 OR 
                                                 (Destination_IP_Address__c =: c.Alert_Source_IP__c AND Destination_IP_Address__c <> '') OR (Alert_Source_IP__c =: c.Destination_IP_Address__c AND Alert_Source_IP__c <> ''))
                                                AND CreatedDate = LAST_N_DAYS:30
                                         		AND Id NOT IN: processedCases];
            
            System.debug('ID :' + c.Id);
            System.debug('For IPs ' + c.Alert_Source_IP__c + ', ' + c.Destination_IP_Address__c);
            System.debug('Query result count = ' +  simCasesByEvent.size());
    
            for(Case ac : simCasesByEvent)
            {
                digestAlert(ac, processedCases, graph, true);
            }
        }
    }
    
    public Static void mapNodeRelationShips(JSCaseNode n0, JSCaseNode n1, JSCaseNode n2,
                                           	JSCaseEdge e0, 	JSCaseEdge e1)
    {
        n0.relatedNodeKeys.add(n1.key);
        n0.relatedNodeKeys.add(n2.key);
        n0.relatedEdgeKeys.add(e0.id);
        n0.relatedEdgeKeys.add(e1.id);
        
        n1.relatedNodeKeys.add(n0.key);
        n1.relatedNodeKeys.add(n2.key);
        n1.relatedEdgeKeys.add(e0.id);
        n1.relatedEdgeKeys.add(e1.id);
        
        n2.relatedNodeKeys.add(n0.key);
        n2.relatedNodeKeys.add(n1.key);
        n2.relatedEdgeKeys.add(e0.id);
        n2.relatedEdgeKeys.add(e1.id);
    }
    
    public static void increaseEdgeCount()
    {
		
    }
    
    private static String genNullKey(Case c, JSCaseNode.ENodeType nodeType)
    {
        return c.CaseNumber + ':' + nodeType + ':::IS_A_NULL_KEY';
    }
    
    public static String getCaseKey(Case c, JSCaseNode.ENodeType nodeType)
    {
        if(nodeType == JSCaseNode.ENodeType.SOURCE_ENDPOINT)
        {
            //DESTINATION
            //ENDPOINT
            
            if(!String.isBlank(c.Alert_Source_IP__c) )
            	return c.Alert_Source_IP__c;
            else
                return genNullKey(c, nodeType);
        }
        else if(nodeType == JSCaseNode.ENodeType.DESTINATION_ENDPOINT)
        {
            //SOURCE
            //ALERT OR INCIDENT
            
            if(!String.isBlank(c.Destination_IP_Address__c) )
            	return c.Destination_IP_Address__c;
            else
                return genNullKey(c, nodeType);
        }
        else if(nodeType == JSCaseNode.ENodeType.EVENT)
        {
            //SOURCE
            //ALERT OR INCIDENT

            if(!String.isBlank(c.Alert_Name__c) )
            	return c.Alert_Name__c;
            else if (!String.isBlank(c.Subject) )
                return c.Subject;
            else
                return genNullKey(c, nodeType);
        }

        return null;
    }
    
    public static String generateKeyForNullCase()
    {
    	Blob b = Crypto.GenerateAESKey(128);
		String h = EncodingUtil.ConvertTohex(b);
        
		return h.SubString(0,8)+ '-' + h.SubString(8,12) + '-' + h.SubString(12,16) + '-' + h.SubString(16,20) + '-' + h.substring(20);
    }
}
@isTest
public with sharing class PRT_V2MOMHealthViewControllerTest {

    @testSetup 
    static void testSetup(){
        List<User> userList = new List<User>();
        userList = PRT_TestDataFactory.createUsers(3,'System Administrator',true);
    }
    @isTest
    public static void getV2momMethods(){
        User userRecord = [SELECT Id,Name FROM USER WHERE Email Like 'PRTUser%' LIMIT 1];
        userRecord.FirstName = 'Test V2MOM Method Health';
        userRecord.EmployeeNumber = '725957';
        update userRecord;
        String userId = 'EU101';
        String userId2 = 'EX010';
        String userId3 = 'EMD101';
        String userId4 = 'EMS101';
        String publishDate = String.ValueOf(date.today());
        // Prepare external user
        
        String org62JSON = '{"attributes":{"type":"Org62User__x","url":"/services/data/v48.0/sobjects/Org62User__x/0054D000001YKEcQAO"},"EmployeeNumber__c":"77456","ExternalId":"'+ userId+'"}';
        String v2MomJSON = '{"attributes":{"type":"V2MOM_c__x","url":"/services/data/v48.0/sobjects/V2MOM_c__x/0054D000001WzcyQAC"},"V2MOM_User_c__c":"'+ userId+'","ExternalId":"'+userId2+'","Name__c":"Test 123","Published_on_c__c":"'+publishDate+'"}';
        String measureJSON = '{"attributes":{"type":"Measure_c__x","url":"/services/data/v48.0/sobjects/Measure_c__x/0054D000001VnAMQA0"},"MeasureName_c__c":"test measure1","ExternalId":"'+userId3+'","V2MOM_c__c":"'+userId2+'","Method_c__c":"'+userId4+'"}';
        String methodJSON = '{"attributes":{"type":"V2MOM_Method_c__x","url":"/services/data/v48.0/sobjects/V2MOM_Method_c__x/0054D000001WzcyQAC"},"Name__c":"test method1","Description_c__c":"test desc","V2MOM_c__c":"'+userId2+'","ExternalId":"'+userId4+'","CreatedDate__c":"'+ publishDate+'"}';
        
       
        PRT_V2MOMHealthViewController.mockallOrg62Users.add((Org62User__x)JSON.deserialize(org62JSON,Org62User__x.class ));
        PRT_V2MOMHealthViewController.mockallV2MOMList.add((V2MOM_c__x)JSON.deserialize(v2MomJSON,V2MOM_c__x.class ));
        PRT_V2MOMHealthViewController.mockallMethods.add((V2MOM_Method_c__x)JSON.deserialize(methodJSON,V2MOM_Method_c__x.class ));
        PRT_V2MOMHealthViewController.mockallMeasures.add((Measure_c__x)JSON.deserialize(measureJSON,Measure_c__x.class ));
        
        // Prepare projectList for Measures v2momMapping       
        List<Project__c> projectList = new List<Project__c>();
        Project__c projectObject = new Project__c();
        projectObject.V2MOM_Measure__c = userId4;
        projectObject.Project_Start_Date__c = Date.today();
        projectObject.Projected_Date_of_Completion__c = Date.today();
        projectObject.Overall_Project_Health__c = 'At Risk';
        projectList.add(projectObject);
        projectObject = new Project__c();
        projectObject.Project_Start_Date__c = Date.today();
        projectObject.Projected_Date_of_Completion__c = Date.today();
        projectObject.V2MOM_Method__c = userId3;
        projectList.add(projectObject);
        PRT_V2MOMHealthViewController.mockallProjects.addAll(projectList);
        
        List<V2MomMapping__c> vmapList = new List<V2MomMapping__c>();
        //Map<String,String> dependantIdVsControllingId = new Map<String,String>();
        V2MomMapping__c vmap = new V2MomMapping__c();
        vmap.Controlling_Measure__c = userRecord.Id;
        vmap.Dependent_Method__c = userInfo.getUserId();
        //dependantIdVsControllingId.put(vmap.Dependent_Method__c,vmap.Controlling_Measure__c);
        vmapList.add(vmap);
        insert vmapList; 

        test.startTest();
        PRT_V2MOMHealthViewController.fetchExternalUser('testNull');
        PRT_V2MOMHealthViewController.fetchV2MOMMethods(userRecord.Id);
        test.stopTest();   
    }
}
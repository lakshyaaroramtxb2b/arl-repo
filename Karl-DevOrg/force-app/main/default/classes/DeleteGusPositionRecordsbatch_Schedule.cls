/**
 * @author: Suresh Uppala
 */
public class DeleteGusPositionRecordsbatch_Schedule implements Schedulable {
    public void execute(SchedulableContext sc) {
        Database.executeBatch((Database.Batchable<SObject>) 
            Type.forName('DeleteGusPositionRecordsbatch').newInstance(), 100);
    }
}
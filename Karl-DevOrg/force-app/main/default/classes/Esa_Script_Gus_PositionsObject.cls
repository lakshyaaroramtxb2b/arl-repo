public class Esa_Script_Gus_PositionsObject {
//Esa_Script_Gus_PositionsObject.updateParentCloudInfo();
//Esa_Script_Gus_PositionsObject.updateHiringManagersInfo();
//Esa_Script_Gus_PositionsObject.updateLastModifiedByInfo();
//Esa_Script_Gus_PositionsObject.updateRecruiterInfo();
//Esa_Script_Gus_PositionsObject.updateEmployeeName();


public static void updateEmployeeName(){
 List<Headcount__c> lstHeadCounts  = new List<Headcount__c>();
lstHeadCounts = [SELECT Id,
                        Employee_Name_Text__c, 
                        Headcount_Name__r.Name__c,
                        Headcount_Name__c
                 FROM Headcount__c 
                 WHERE Employee_Name_Text__c=null
                 AND Headcount_Name__c != null
                 LIMIT 500];

System.debug('##### Total Count ####'+lstHeadCounts.size());
Set<id> headCountIds = new Set<id>();

for(Headcount__c headCount : lstHeadCounts) {
    headCount.Employee_Name_Text__c = headCount.Headcount_Name__r.Name__c;
}
if(lstHeadCounts.size() > 0) {
  update lstHeadCounts;
}

}



public static void updateParentCloudInfo(){
 List<Headcount__c> lstHeadCounts  = new List<Headcount__c>();
lstHeadCounts = [SELECT Id,
                        Parent_Cloud__c, 
                        Parent_Cloud_Text__c 
                 FROM Headcount__c 
                 WHERE Parent_Cloud_Text__c=null
                 AND Parent_Cloud__c != null
                 LIMIT 500];

System.debug('##### Total Count ####'+lstHeadCounts.size());
Set<id> parentCloudIds = new Set<id>();

for(Headcount__c headCount : lstHeadCounts) {
    parentCloudIds.add(headCount.Parent_Cloud__c);
}

List<ADM_Parent_Cloud_c__x> lstParentClound = new List<ADM_Parent_Cloud_c__x>();
lstParentClound = [SELECT ExternalId,Name__c FROM ADM_Parent_Cloud_c__x WHERE ExternalId IN : parentCloudIds];

System.debug('##########'+lstParentClound.size());

Map<Id,String> mapParentCloudInfo = new Map<Id,String>();
for(ADM_Parent_Cloud_c__x parentCloud : lstParentClound){
    mapParentCloudInfo.put(parentCloud.ExternalId, parentCloud.Name__c);
}


for(Headcount__c headCount : lstHeadCounts){
    headCount.Parent_Cloud_Text__c = mapParentCloudInfo.get(headCount.Parent_Cloud__c);
}

update lstHeadCounts;
}

//Update Hiring Managers Name
public static void updateHiringManagersInfo(){
List<Headcount__c> lstHeadCounts  = new List<Headcount__c>();
lstHeadCounts = [SELECT Id,
                        Hiring_Manager__c, 
                        Hiring_Manager_Text__c
                 FROM Headcount__c 
                 WHERE Hiring_Manager_Text__c=null
                 AND Hiring_Manager__c != null
                 LIMIT 500];

System.debug('##### Total Count ####'+lstHeadCounts.size());
Set<id> hiringManagerIds = new Set<id>();

for(Headcount__c headCount : lstHeadCounts) {
    hiringManagerIds.add(headCount.Hiring_Manager__c);
}

List<GusUser__x> lstHiringManagers = new List<GusUser__x>();
lstHiringManagers = [SELECT ExternalId,Name__c FROM GusUser__x WHERE ExternalId IN : hiringManagerIds];

System.debug('##########'+lstHiringManagers.size());

Map<Id,String> mapGusHiringManageraInfo = new Map<Id,String>();
for(GusUser__x gusUser : lstHiringManagers){
    mapGusHiringManageraInfo.put(gusUser.ExternalId, gusUser.Name__c);
}


for(Headcount__c headCount : lstHeadCounts){
    headCount.Hiring_Manager_Text__c = mapGusHiringManageraInfo.get(headCount.Hiring_Manager__c);
}

update lstHeadCounts;
}

// Last Modified by Id Info
public static void updateLastModifiedByInfo(){
    List<Headcount__c> lstHeadCounts  = new List<Headcount__c>();
    lstHeadCounts = [SELECT Id,
                            LastModifiedBy__c, 
                            LastModifiedBy_Text__c
                     FROM Headcount__c 
                     WHERE LastModifiedBy_Text__c=null
                     AND LastModifiedBy__c != null
                     LIMIT 500];

    System.debug('##### Total Count ####'+lstHeadCounts.size());
    Set<id> lastModifiedUserIds = new Set<id>();

    for(Headcount__c headCount : lstHeadCounts) {
        lastModifiedUserIds.add(headCount.LastModifiedBy__c);
    }

    List<GusUser__x> lstLastModifiedUsers = new List<GusUser__x>();
    lstLastModifiedUsers = [SELECT ExternalId,Name__c FROM GusUser__x WHERE ExternalId IN : lastModifiedUserIds];

    System.debug('##########'+lstLastModifiedUsers.size());

    Map<Id,String> mapLastModifiedUsersInfo = new Map<Id,String>();
    for(GusUser__x gusUser : lstLastModifiedUsers){
        mapLastModifiedUsersInfo.put(gusUser.ExternalId, gusUser.Name__c);
    }


    for(Headcount__c headCount : lstHeadCounts){
        headCount.LastModifiedBy_Text__c = mapLastModifiedUsersInfo.get(headCount.LastModifiedBy__c);
    }

update lstHeadCounts;
}

//Update Recruiter Info
public static void updateRecruiterInfo(){
    List<Headcount__c> lstHeadCounts  = new List<Headcount__c>();
    lstHeadCounts = [SELECT Id,
                            Recruiter__c, 
                            Recruiter_Text__c
                     FROM Headcount__c 
                     WHERE Recruiter_Text__c=null
                     AND Recruiter__c    != null
                     LIMIT 500];

    System.debug('##### Total Count ####'+lstHeadCounts.size());
    Set<id> lastRecruiterUserIds = new Set<id>();

    for(Headcount__c headCount : lstHeadCounts) {
        lastRecruiterUserIds.add(headCount.Recruiter__c);
    }

    List<GusUser__x> lstRecruiterUsers = new List<GusUser__x>();
    lstRecruiterUsers = [SELECT ExternalId,Name__c FROM GusUser__x WHERE ExternalId IN : lastRecruiterUserIds];

    System.debug('##########'+lstRecruiterUsers.size());

    Map<Id,String> mapRecruiterUsersInfo = new Map<Id,String>();
    for(GusUser__x gusUser : lstRecruiterUsers){
        mapRecruiterUsersInfo.put(gusUser.ExternalId, gusUser.Name__c);
    }


    for(Headcount__c headCount : lstHeadCounts){
        headCount.Recruiter_Text__c = mapRecruiterUsersInfo.get(headCount.Recruiter__c);
    }

update lstHeadCounts;
}

public static void deleteGusDeletedPositons(){

LIST<Headcount__c> lstSecOrgPositions = [SELECT Id
                                         FROM Headcount__c 
                                         WHERE Gus_Position__c = null];
                                         
if(lstSecOrgPositions.size()> 0) {
  delete lstSecOrgPositions;
}                                

/*

LIST<Headcount__c> lstSecOrgPositions = [SELECT Id, 
                                                Gus_Position_Id__c 
                                         FROM Headcount__c 
                                         WHERE LastModifiedDate != TODAY];

Set<Id> setPositionsIds = new Set<Id>();
for(Headcount__c hCount : lstSecOrgPositions){
    setPositionsIds.add(hCount.Gus_Position_Id__c);
}

List<Headcount_c__x> lstGusHeadCounts = [SELECT ExternalId 
                                         FROM Headcount_c__x
                                         WHERE ExternalId IN: setPositionsIds];

system.debug('######'+ lstGusHeadCounts.size());

if(lstGusHeadCounts.size() == 0) {
    delete lstSecOrgPositions;
}

if(lstGusHeadCounts.size() > 0){

Map<Id,Id> mapPositions = new Map<Id,Id>();

for(Headcount_c__x hCount : lstGusHeadCounts){
    mapPositions.put(hCount.ExternalId, hcount.ExternalId);
}

List<Headcount__c> lstDeletedHCounts = new List<Headcount__c>();
for(Headcount__c hCount : lstSecOrgPositions){
    if(!mapPositions.containsKey(hCount.Gus_Position_Id__c)){
        lstDeletedHCounts.add(hCount);
    }
}
 
 system.debug('Deleted Records : '+lstDeletedHCounts.size());
 if(lstDeletedHCounts.size() >= 0) {
    delete lstDeletedHCounts;
}

} */


}

}
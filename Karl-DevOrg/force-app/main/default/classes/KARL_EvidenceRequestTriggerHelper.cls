/**
* @author Vishnu Kumar
* @email vishnu.kumar@mtxb2b.com
* @desc This is helper class for EvidenceRequestTrigger.
*/
public with sharing class KARL_EvidenceRequestTriggerHelper {
    public static set<String> running;
    public static Set<Id> evidId = new Set<Id>();
    
    /**
     * @author Mahima Aggarwal
     * @email mahima.aggarwal@mtxb2b.com
     * @desc This method get triggered from Evidence Request trigger.
    */
    public static void run(){
        //After Insert
        if( Trigger.isAfter && Trigger.isInsert ){
            addDirectLinkToEvidence((List<KARL_Evidence_Request__c >)Trigger.new);
            
        }
        if(Trigger.isAfter && Trigger.isUpdate){
            addDirectLinkToEvidence((List<KARL_Evidence_Request__c >)Trigger.new);
            changeGusCaseStatus((List<KARL_Evidence_Request__c >)Trigger.new,(Map<Id,KARL_Evidence_Request__c>)Trigger.oldMap);
        }
    }

    /**
     * @author Mahima Aggarwal
     * @email mahima.aggarwal@mtxb2b.com
     * @desc This method adds link of Evidence Upload community to Direct Link field of Evidence Request
    */
    private static void addDirectLinkToEvidence(List<KARL_Evidence_Request__c > newList){
        for(Id recursionCheck : Trigger.newMap.keySet()){
            if(evidId.contains(recursionCheck)){
                System.debug(LoggingLevel.DEBUG, 'Already processed this record >> ' + recursionCheck);
                return; // Kill the run - we've already processed this Id
            }else{
                System.debug(LoggingLevel.DEBUG, 'New record to process >> ' + recursionCheck);
                evidId.add(recursionCheck); // add Id to set so we don't run it again
            }
        }

        List<KARL_Evidence_Request__c> evidenceRequestList = new List<KARL_Evidence_Request__c>();
        Network karlEvidenceNetwork;
        String loginurl,communityHomeUrl ='';
        Integer index;
        if(!Test.isRunningTest()){
            karlEvidenceNetwork = [SELECT Id FROM Network WHERE Name ='KARL Evidence Request' ];
            loginurl = Network.getLoginUrl(karlEvidenceNetwork.id);
            index = loginurl.lastIndexOf('/login');
            communityHomeUrl = loginurl.substring(0, index + 1);
            System.debug('MyDebugURL: ' + communityHomeUrl);
        }
        String hyperlink = communityHomeUrl + 'evidence-upload?id=';

        for(KARL_Evidence_Request__c evidenceReq : newList){
            KARL_Evidence_Request__c evidenceRequestToUpdate = new KARL_Evidence_Request__c();
            evidenceRequestToUpdate.Id = evidenceReq.Id;
            evidenceRequestToUpdate.KARL_Direct_Link__c = hyperlink + evidenceReq.Id;
            evidenceRequestList.add(evidenceRequestToUpdate);
        }

        //updating evidenceRequestList
        if( !evidenceRequestList.isEmpty() && Schema.sObjectType.KARL_Evidence_Request__c.isUpdateable() 
        && KARL_Evidence_Request__c.Id.getDescribe().isAccessible() 
        && KARL_Evidence_Request__c.KARL_Direct_Link__c.getDescribe().isUpdateable() ){
            update evidenceRequestList;
        }

    }
    /**
     * @author Swarnima Mandhata
     * @email swarnima.singh@mtxb2b.com
     * @desc This method change the status of Gus Case when 
    */
    public static void changeGusCaseStatus(List<KARL_Evidence_Request__c > newList, Map<Id,KARL_Evidence_Request__c> oldMap){
        
        Set<Id> cycleItemIdSet = new Set<Id>();
        for(KARL_Evidence_Request__c evidence: newList){
            if(oldMap != null && evidence.Evidence_Status__c != oldMap.get(evidence.id).Evidence_Status__c 
            && (evidence.Evidence_Status__c == KARL_Constants.EVIDENCE_STATUS_ReturnToEngineer || evidence.Evidence_Status__c == KARL_Constants.EVIDENCE_STATUS_ProviderToAuditor)){
                if(evidence.KARL_Cycle_ARL_Item__c != null){
                    cycleItemIdSet.add(evidence.KARL_Cycle_ARL_Item__c);
                }
               
            }
        }
        if(!cycleItemIdSet.isEmpty() && !System.isBatch()){
            changeGusCaseStatusFuture(cycleItemIdSet);
        }
    }
    
    @future
    public static void changeGusCaseStatusFuture(Set<Id> cycleItemIdSet){
        Map<String,String> evidenceIdToCycleItemIdMap = new Map<String,String>();
        Map<Id,KARL_Evidence_Request__c>  idToEvidenceReqRecordMap = new Map<Id,KARL_Evidence_Request__c>();
        Map<String, String> cycleRequestIfToWorkId = new Map<String,String>();
        List<ADM_Work_c__x> updateGusWorkRecordList = new List<ADM_Work_c__x>();

        for(KARL_Evidence_Request__c evidenceReq: [SELECT Id, Name, KARL_Cycle_ARL_Item__c, Evidence_Status__c FROM KARL_Evidence_Request__c 
                                                    WHERE KARL_Cycle_ARL_Item__c IN:cycleItemIdSet ]){
                                                        if(!evidenceIdToCycleItemIdMap.containsKey(evidenceReq.Id)){
                                                            evidenceIdToCycleItemIdMap.put(evidenceReq.Id,evidenceReq.KARL_Cycle_ARL_Item__c);
                                                            idToEvidenceReqRecordMap.put(evidenceReq.Id,evidenceReq);
                                                        }
                                                    }
        if(!evidenceIdToCycleItemIdMap.isEmpty()){
            for(Cycle_Request_Item__c cycleRequestItem: [SELECT Id, Request_GUS__c FROM Cycle_Request_Item__c
                                                         WHERE Id IN:evidenceIdToCycleItemIdMap.values()]){
                                                            if(!cycleRequestIfToWorkId.containsKey(cycleRequestItem.Id)){
                                                                cycleRequestIfToWorkId.put(cycleRequestItem.id,cycleRequestItem.Request_GUS__c);
                                                            }
                                                         }
        }
        
        for(String evidenceId: evidenceIdToCycleItemIdMap.keyset()){
            String cycleRequestId = evidenceIdToCycleItemIdMap.get(evidenceId);
                if(cycleRequestIfToWorkId.containsKey(cycleRequestId)){
                    ADM_Work_c__x workObj = new ADM_Work_c__x();
                    workObj.ExternalId = cycleRequestIfToWorkId.get(cycleRequestId);
                    if(idToEvidenceReqRecordMap.get(evidenceId).Evidence_Status__c == KARL_Constants.EVIDENCE_STATUS_ReturnToEngineer){
                        workObj.Status_c__c = KARL_Constants.WORK_STATUS_IN_PROGRESS;
                    }else{
                    workObj.Status_c__c = KARL_Constants.WORK_STATUS_CLOSED;
                }
                updateGusWorkRecordList.add(workObj);
            }
       }
        if(!updateGusWorkRecordList.isEmpty() && !Test.isRunningTest()){
            try{
                Database.updateImmediate(updateGusWorkRecordList);
            }catch(exception e){
                System.debug('Error: '+ e.getMessage());
            }
        }
    }
}
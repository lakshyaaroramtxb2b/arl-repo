/*
     * Created by - Prashant Gupta
     * Class is used to store all constant values in a single place to help facilitate updating picklist values
     * as and when needed with minimum code change.
    */
public class IEM_Constants {
    
    //IEM CASE STATUS
    public static final String IEM_CASE_STATUS_SUBMITTED = 'Submitted';
    public static final String IEM_CASE_STATUS_INVESTIGATING = 'Investigating';
    public static final String IEM_CASE_STATUS_SEVERITY_ASSESSMENT = 'Severity Assessment';
    public static final String IEM_CASE_STATUS_ACTION_PLAN = 'Action Plan';
    public static final String IEM_CASE_STATUS_INTERNAL_REVIEW = 'Internal Review';
    public static final String IEM_CASE_STATUS_SECURITY_DECISION = 'Security Decision';
    public static final String IEM_CASE_STATUS_BUSINESS_DECISION = 'Business Decision';
    public static final String IEM_CASE_STATUS_MONITORING = 'Monitoring';
    public static final String IEM_CASE_STATUS_VALIDATING_RESOLUTION = 'Validating Resolution';
    public static final String IEM_CASE_STATUS_CLOSURE = 'Closure';
    public static final String IEM_CASE_STATUS_CLOSED = 'Closed';
    
    //ESA
    public static final String IEM_ESA_ACCOUNT = 'salesforce.com - ESA Office Hours';
    
    //IEM Severity_Assessment_Status__c STATUS
    public static final String IEM_SEVERITY_STATUS_COMPLETED = 'Completed';
    
    // IEM Validity Status
    public static final String IEM_VALIDITY_STATUS_VALID = 'Valid';
    // IEM Approval process
    public static final String IEM_APPROVAL_PROCESS_STATUS_PENDING = 'Pending';
    
    
    //IEM Bussiness Decision
    public static final String IEM_BUSINESS_DECISION_APPROVED = 'Approved';
    public static final String IEM_BUSINESS_DECISION_REJECTED = 'Rejected';   
    
    //IEM Security Decision
    public static final String IEM_SECURITY_DECISION_APPROVED = 'Approved';
    public static final String IEM_SECURITY_DECISION_REJECTED = 'Rejected';  
    
    //IEM Issue Identified by
    
    //IEM Security Decision
    public static final String IEM_IIB_INFRASTRUCTURE_SECURITY = 'Infrastructure Security';
    public static final String IEM_IIB_PRODUCT_SECURITY = 'Product Security';
    
    //IEM Generic 
    public static final String IEM_NOT_STARTED = 'Not Started';
    public static final String IEM_IN_PROGRESS = 'In Progress';
    public static final String IEM_COMPLETED = 'Completed';
    public static final String IEM_SUBMITTED = 'Submitted';
    public static final String IEM_NOT_APPROVED = 'Not Approved';
    public static final String IEM_ACTIVE = 'Active';
    public static final String IEM_INACTIVE = 'Inactive';
    public static final String IEM_REJECTED = 'Rejected';
    public static final String IEM_PRE_ASSIGNED = 'Pre-assigned';
    public static final String IEM_EXCEPTION = 'Exception';
    public static final String IEM_EXTENSION = 'Extension';
    public static final String IEM_YES = 'Yes';
    
    //GUS Work Status
    public static final String GUS_WORK_STATUS_NEW = 'New';
    public static final String GUS_WORK_STATUS_CLOSED = 'Closed';
    public static final String GUS_WORK_STATUS_FIXED = 'Fixed';
    public static final String GUS_WORK_STATUS_DUPLICATE = 'Duplicate';
    public static final String GUS_WORK_STATUS_NEVER = 'Never';
    public static final String GUS_WORK_STATUS_IN_PROGRESS = 'In Progress';
    
    //Milestone status
    public static final String MILESTONE_STATUS_PENDING = 'Pending';
    public static final String MILESTONE_STATUS_IN_PROGRESS = 'In Progress';
    public static final String MILESTONE_STATUS_COMPLETED = 'Completed';
    
    //SBE status
    public static final String SBE_STATUS_IEM_ANALYST = 'Review by I&EM analyst';
    public static final String SBE_STATUS_AWAITING_BUSINESS = 'Awaiting Business Approval';
    public static final String SBE_STATUS_AWAITING_SECURITY = 'Awaiting Security Approval';
    public static final String SBE_STATUS_SECURITY_DECISION = 'Security Decision';
    public static final String SBE_STATUS_BUSINESS_DECISION = 'Business Decision';
    
    
    public static final String SBE_STATUS_APPROVED = 'Approved';
    public static final String SBE_STATUS_In_Progress = 'In Progress';
    public static final String SBE_STATUS_DENIED = 'Denied';
    public static final String SBE_STATUS_CLOSED = 'Closed';
    public static final String SBE_STATUS_EXPIRED = 'Expired';
    public static final String SBE_STATUS_DRAFT = 'Draft';
    public static final String SBE_STATUS_VALIDATING_RESOLUTION = 'Validating Resolution';
    
    
    //Extension Status
    public static final String SBE_STATUS_EXTENSION_EXPIRED = 'Expired';
    public static final String SBE_STATUS_EXTENSION_APPROVED = 'Approved';
    public static final String SBE_STATUS_EXTENSION_REJECTED = 'Rejected';
    
    
    
    //Contact Record type Name
    public static final String CONTACT_RECORD_TYPE_SALESFORCE_EMPLOYEE = 'Salesforce_Employee';
    
    
    //CASE Status Check 
    public static final Map<String,Integer> IEM_CASE_STATUS_ORDER = new Map<String,Integer>
                        {
                            'Submitted'=>1,
                            'Investigating'=>2,
                            'Severity Assessment'=>3,
                            'Action Plan'=>4,
                            'Internal Review'=>5,
                            'Security Decision'=>6,
                            'Business Decision'=>7,
                            'Monitoring'=>8,
                            'Validating Resolution'=>9,
                            'Closed'=>10
                        }; 
    
    Public static ID IEM_CASERECORDTYPEID = Schema.SObjectType.Case.getRecordTypeInfosByName().get('IEM').getRecordTypeId();
    Public static ID IEM_CONTACTRECORDTYPEID = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Salesforce Employee').getRecordTypeId();
 
    //Theme
    public static final String PRIMARY_THEME_NAME = 'GRCOPS-Primary';
    public static final String INTAKE_THEME_NAME_PREFIX = 'GRCOPS-INTAKE';

    //Analyst group
    public static final String ISSUE_ANALYST_GROUP_NAME = 'GRC_IEM_Case_Analysts';    

   //Issue source
   public static final String IEM_ISSUE_SOURCE_SLA = 'Vulnerabilities over SLA';
    
   //Security Standard Affected
   public static final String IEM_SECURITY_STANDARD_AFFECTED = 'SFSS-090 Vulnerability Rankings'; 
    //SEVERITY MAPPING
    public static final Map<String,String> IEM_TO_SBE_SEVERITY_MAPPING = new Map<String,String>
                        {
                            'Very High'=>'Very High (EVP)',
                            'High'=>'High (SVP)',
                            'Moderate'=>'Moderate (VP)',
                            'Low'=>'Low (SDir)',
                            'Very Low'=>'Very Low (Dir)'
                        };
    //SEVERITY MAPPING
    public static final Map<String,String> SBE_TO_SEVERITY_MAPPING = new Map<String,String>
                        {
                            'Very High (EVP)'=>'Very High',
                            'High (SVP)'=>'High',
                            'Moderate (VP)'=>'Moderate',
                            'Low (SDir)'=>'Low',
                            'Very Low (Dir)'=>'Very Low'
                        }; 
       
}
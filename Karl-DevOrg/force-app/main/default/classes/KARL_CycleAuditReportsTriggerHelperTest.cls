/**
* @author Lakshya Arora
* @email lakshya.arora@mtxb2b.com
* @description Test class for KARL_CycleAuditReportCtrl
*/
@isTest
public class KARL_CycleAuditReportsTriggerHelperTest {
    /**
* @author Lakshya Arora
* @email lakshya.arora@mtxb2b.com
* @desc Test data setup.
*/
    @testSetup
    private static void makeData(){
        User opsAdminUser = ARL_TestDataFactory.createOpsAdmin();
        System.runAs(opsAdminUser){
            Map<Integer,String> priorityToScopeMap = new Map<Integer,String>();
            priorityToScopeMap.put(0,'AS');
            priorityToScopeMap.put(1,'AUST');
            priorityToScopeMap.put(2,'B2BC');
            priorityToScopeMap.put(3,'CHAT');
            
            Audit_Cycle__c auditCycle = ARL_TestDataFactory.createAuditCycle();
            insert auditCycle;
            KARL_Audit_Firm__c auditFirm = ARL_TestDataFactory.createAuditFirm('testAuditFirm');
            insert auditFirm;
            
            // Create two distinct audit teams
            KARL_Audit_Team__c auditTeam = ARL_TestDataFactory.createAuditTeam('testAuditTeam1');
            auditTeam.KARL_Audit_Firm__c = auditFirm.Id;
            insert auditTeam;
            
            List<KARL_Audit_Scope_Master_Data__c> auditScopeList = new List<KARL_Audit_Scope_Master_Data__c>();
            for(Integer i=0;i<4;i++){
                KARL_Audit_Scope_Master_Data__c auditScope = ARL_TestDataFactory.createAuditScope();
                auditScope.KARL_Scope__c = priorityToScopeMap.get(i);
                auditScopeList.add(auditScope);
            }
            insert auditScopeList;
            
            List<KARL_Audit_Scope_Reports__c> auditScopeReportList = new List<KARL_Audit_Scope_Reports__c>();
            for(Integer i=0;i<4;i++){
                KARL_Audit_Scope_Reports__c auditScopeReport = ARL_TestDataFactory.createAuditScopeReport(auditScopeList[i].Id,auditFirm.Id);
                auditScopeReportList.add(auditScopeReport);
            }
            insert auditScopeReportList;
            
            KARL_Gus_Product_Tag__c defaultProductTag = ARL_TestDataFactory.createKarlGusProductTag(true,true);
            insert defaultProductTag;
            
            List<KARL_Auto_Assignment_Configuration__c> autoAssignmentConfigList = ARL_TestDataFactory.createAutoAssignmentConfig(defaultProductTag.Id,4);
            autoAssignmentConfigList[1].Priority_0_Assignment_Date__c = System.today().addDays(5);
            autoAssignmentConfigList[2].Priority_0_Assignment_Date__c = System.today().addDays(-1);
            autoAssignmentConfigList[3].Priority_0_Assignment_Date__c = System.today();
            insert autoAssignmentConfigList;
            
            List<Audit_Cycle_Coverage__c> auditCycleCoverageList = new List<Audit_Cycle_Coverage__c>();
            
            for(Integer i=0;i<4;i++){
                Audit_Cycle_Coverage__c auditCycleCoverage = new Audit_Cycle_Coverage__c();
                auditCycleCoverage.Audit_Cycle__c = auditCycle.Id;
                auditCycleCoverage.Area__c = 'iRAP';
                auditCycleCoverage.Scope__c = priorityToScopeMap.get(i);
                auditCycleCoverage.Auto_Assignment_Configuration__c = autoAssignmentConfigList[i].Id;
                auditCycleCoverage.KARL_Audit_Team__c = auditTeam.Id;
                auditCycleCoverageList.add(auditCycleCoverage);
            }
            insert auditCycleCoverageList;
            
            List<KARL_Cycle_Audit_Report_Items__c> cycleAuditScopeReportList = new List<KARL_Cycle_Audit_Report_Items__c>();
            for(Integer i=0;i<4;i++){
                KARL_Cycle_Audit_Report_Items__c  cycleAuditReportItemObj = ARL_TestDataFactory.createCycleAuditScopeReport(auditScopeReportList[i].Id, auditCycle.Id);
                cycleAuditReportItemObj.Status__c = KARL_Constants.REPORT_STATUS_PENDING;
                cycleAuditScopeReportList.add(cycleAuditReportItemObj);
            }
            cycleAuditScopeReportList[0].Status__c = KARL_Constants.REPORT_STATUS_READY_TO_SIGN;
            insert cycleAuditScopeReportList;
            
        }
    }
    
    /**
* @author Lakshya Arora
* @email lakshya.arora@mtxb2b.com
* @desc This is used to test the readiness deactivated functionality
* @note: won't create task and connected api as they are not allowed in test class
*/
    @isTest
    private static void readinessDeactivateTest(){List<User> opsAdminUserList = ARL_TestDataFactory.fetchUser('abc@b.com.d');
                                                  if(!opsAdminUserList.isEmpty()){
                                                      System.runAs(opsAdminUserList[0]){
                                                          List<KARL_Cycle_Audit_Report_Items__c> updateCycleAuditReportItemList = new List<KARL_Cycle_Audit_Report_Items__c>();
                                                          for(KARL_Cycle_Audit_Report_Items__c cycelAuditReportItemRecord : [SELECT Id,Status__c
                                                                                                                             FROM KARL_Cycle_Audit_Report_Items__c
                                                                                                                             WHERE Status__c =: KARL_Constants.REPORT_STATUS_READY_TO_SIGN]){
                                                                                                                                 cycelAuditReportItemRecord.Status__c = KARL_Constants.REPORT_STATUS_PENDING;
                                                                                                                                 updateCycleAuditReportItemList.add(cycelAuditReportItemRecord);
                                                                                                                             }
                                                          Test.startTest();
                                                          System.debug('updateCycleAuditReportItemList = '+updateCycleAuditReportItemList.size());
                                                          update updateCycleAuditReportItemList;
                                                          System.assert([SELECT Id,Status__c FROM KARL_Cycle_Audit_Report_Items__c WHERE Status__c =: KARL_Constants.REPORT_STATUS_READY_TO_SIGN].isEmpty()); 
                                                      }
                                                  }
                                                 }
    
    /**
* @author Lakshya Arora
* @email lakshya.arora@mtxb2b.com
* @desc This is used to test the functionality of send email on ready to sign status change
*/
    @isTest
    private static void sendEmailOnReadyToSignTest(){
        List<User> opsAdminUserList = ARL_TestDataFactory.fetchUser('abc@b.com.d');
        if(!opsAdminUserList.isEmpty()){
            System.runAs(opsAdminUserList[0]){
                List<KARL_Cycle_Audit_Report_Items__c> updateCycleAuditReportItemList = new List<KARL_Cycle_Audit_Report_Items__c>();
                for(KARL_Cycle_Audit_Report_Items__c cycelAuditReportItemRecord : [SELECT Id,Status__c,Auditor_Readiness_Confirmation__c,Management_Responses_Approved__c,
                                                                                   Audit_Report_Final_Draft_Confirmed__c,Approved_by_Legal__c,Audit_Letter_Temps_Formatted__c,
                                                                                   Basis_of_Assertion_BOA__c
                                                                                   FROM KARL_Cycle_Audit_Report_Items__c
                                                                                   WHERE Status__c =: KARL_Constants.REPORT_STATUS_PENDING]){
                                                                                       cycelAuditReportItemRecord.Status__c = KARL_Constants.REPORT_STATUS_READY_TO_SIGN;
                                                                                       cycelAuditReportItemRecord.Auditor_Readiness_Confirmation__c = true;
                                                                                       cycelAuditReportItemRecord.Management_Responses_Approved__c = true;
                                                                                       cycelAuditReportItemRecord.Audit_Report_Final_Draft_Confirmed__c = true ;
                                                                                       cycelAuditReportItemRecord.Approved_by_Legal__c = true;
                                                                                       cycelAuditReportItemRecord.Audit_Letter_Temps_Formatted__c = true;
                                                                                       cycelAuditReportItemRecord.Basis_of_Assertion_BOA__c = true;
                                                                                       
                                                                                       updateCycleAuditReportItemList.add(cycelAuditReportItemRecord);
                                                                                   }
                if(!updateCycleAuditReportItemList.isEmpty())
                    update updateCycleAuditReportItemList;
                System.assert(![SELECT Id,Status__c FROM KARL_Cycle_Audit_Report_Items__c WHERE Status__c =: KARL_Constants.REPORT_STATUS_READY_TO_SIGN].isEmpty()); //as only ready to sign updated to Pending after update
            }
        }
    }
    
    /**
* @author Lakshya Arora
* @email lakshya.arora@mtxb2b.com
* @desc This is used to test the functionality of send email on ready to sign status change
* @note: won't create chatter as connected api  are not allowed in test class
*/
    @isTest
    private static void sendEmailAndCreateChatterOnPublishTest(){
        List<User> opsAdminUserList = ARL_TestDataFactory.fetchUser('abc@b.com.d');
        if(!opsAdminUserList.isEmpty()){
            System.runAs(opsAdminUserList[0]){
                List<KARL_Cycle_Audit_Report_Items__c> updateCycleAuditReportItemList = new List<KARL_Cycle_Audit_Report_Items__c>();
                for(KARL_Cycle_Audit_Report_Items__c cycelAuditReportItemRecord : [SELECT Id,Status__c,Auditor_Readiness_Confirmation__c,Management_Responses_Approved__c,
                                                                                   Audit_Report_Final_Draft_Confirmed__c,Approved_by_Legal__c,Audit_Letter_Temps_Formatted__c,
                                                                                   Basis_of_Assertion_BOA__c
                                                                                   FROM KARL_Cycle_Audit_Report_Items__c
                                                                                   WHERE Status__c =: KARL_Constants.REPORT_STATUS_PENDING
                                                                                  ]){
                                                                                      cycelAuditReportItemRecord.Auditor_Readiness_Confirmation__c = true;
                                                                                      cycelAuditReportItemRecord.Management_Responses_Approved__c = true;
                                                                                      cycelAuditReportItemRecord.Audit_Report_Final_Draft_Confirmed__c = true ;
                                                                                      cycelAuditReportItemRecord.Approved_by_Legal__c = true;
                                                                                      cycelAuditReportItemRecord.Audit_Letter_Temps_Formatted__c = true;
                                                                                      cycelAuditReportItemRecord.Basis_of_Assertion_BOA__c = true;
                                                                                      
                                                                                      updateCycleAuditReportItemList.add(cycelAuditReportItemRecord);
                                                                                  }
                if(!updateCycleAuditReportItemList.isEmpty()){
                    update updateCycleAuditReportItemList;
                    for(KARL_Cycle_Audit_Report_Items__c cycelAuditReportItemRecord:updateCycleAuditReportItemList){
                        cycelAuditReportItemRecord.Status__c = KARL_Constants.REPORT_STATUS_PUBLISHED;
                    }
                    update updateCycleAuditReportItemList;
                    System.assert(![SELECT Id,Status__c FROM KARL_Cycle_Audit_Report_Items__c WHERE Status__c =: KARL_Constants.REPORT_STATUS_PUBLISHED].isEmpty()); 
                }
            }
        }
    }
    
      /**
* @author Lakshya Arora
* @email lakshya.arora@mtxb2b.com
* @desc This is used to test the functionality of create chatter on status issued
* @note: won't create chatter as connected api  are not allowed in test class
*/
    @isTest
    private static void createChatterOnIssuedTest(){
        List<User> opsAdminUserList = ARL_TestDataFactory.fetchUser('abc@b.com.d');
        if(!opsAdminUserList.isEmpty()){
            System.runAs(opsAdminUserList[0]){
                List<KARL_Cycle_Audit_Report_Items__c> updateCycleAuditReportItemList = new List<KARL_Cycle_Audit_Report_Items__c>();
                for(KARL_Cycle_Audit_Report_Items__c cycelAuditReportItemRecord : [SELECT Id,Status__c,Auditor_Readiness_Confirmation__c,Management_Responses_Approved__c,
                                                                                   Audit_Report_Final_Draft_Confirmed__c,Approved_by_Legal__c,Audit_Letter_Temps_Formatted__c,
                                                                                   Basis_of_Assertion_BOA__c
                                                                                   FROM KARL_Cycle_Audit_Report_Items__c
                                                                                   WHERE Status__c =: KARL_Constants.REPORT_STATUS_PENDING
                                                                                  ]){
                                                                                      cycelAuditReportItemRecord.Auditor_Readiness_Confirmation__c = true;
                                                                                      cycelAuditReportItemRecord.Management_Responses_Approved__c = true;
                                                                                      cycelAuditReportItemRecord.Audit_Report_Final_Draft_Confirmed__c = true ;
                                                                                      cycelAuditReportItemRecord.Approved_by_Legal__c = true;
                                                                                      cycelAuditReportItemRecord.Audit_Letter_Temps_Formatted__c = true;
                                                                                      cycelAuditReportItemRecord.Basis_of_Assertion_BOA__c = true;
                                                                                      
                                                                                      updateCycleAuditReportItemList.add(cycelAuditReportItemRecord);
                                                                                  }
                if(!updateCycleAuditReportItemList.isEmpty()){
                    update updateCycleAuditReportItemList;
                    for(KARL_Cycle_Audit_Report_Items__c cycelAuditReportItemRecord:updateCycleAuditReportItemList){
                        cycelAuditReportItemRecord.Status__c = KARL_Constants.REPORT_STATUS_ISSUED;
                    }
                    Test.startTest();
                    update updateCycleAuditReportItemList;
                    Test.stopTest();
                    System.assert(![SELECT Id,Status__c FROM KARL_Cycle_Audit_Report_Items__c WHERE Status__c =: KARL_Constants.REPORT_STATUS_ISSUED].isEmpty()); 
                }
            }
        }
    }
    
    /**
* @author Lakshya Arora
* @email lakshya.arora@mtxb2b.com
* @desc This is used to test the functionality of create on final review
* @note: won't create chatter as connected api  are not allowed in test class
*/
    @isTest
    private static void createChatterOnFinalReviewTest(){
        List<User> opsAdminUserList = ARL_TestDataFactory.fetchUser('abc@b.com.d');
        if(!opsAdminUserList.isEmpty()){
            System.runAs(opsAdminUserList[0]){
                List<KARL_Cycle_Audit_Report_Items__c> updateCycleAuditReportItemList = new List<KARL_Cycle_Audit_Report_Items__c>();
                for(KARL_Cycle_Audit_Report_Items__c cycelAuditReportItemRecord : [SELECT Id,Status__c,Auditor_Readiness_Confirmation__c,Management_Responses_Approved__c,
                                                                                   Audit_Report_Final_Draft_Confirmed__c,Approved_by_Legal__c,Audit_Letter_Temps_Formatted__c,
                                                                                   Basis_of_Assertion_BOA__c
                                                                                   FROM KARL_Cycle_Audit_Report_Items__c
                                                                                   WHERE Status__c =: KARL_Constants.REPORT_STATUS_PENDING
                                                                                  ]){
                                                                                      cycelAuditReportItemRecord.Auditor_Readiness_Confirmation__c = true;
                                                                                      cycelAuditReportItemRecord.Management_Responses_Approved__c = true;
                                                                                      cycelAuditReportItemRecord.Audit_Report_Final_Draft_Confirmed__c = true ;
                                                                                      cycelAuditReportItemRecord.Approved_by_Legal__c = true;
                                                                                      cycelAuditReportItemRecord.Audit_Letter_Temps_Formatted__c = true;
                                                                                      cycelAuditReportItemRecord.Basis_of_Assertion_BOA__c = true;
                                                                                      
                                                                                      updateCycleAuditReportItemList.add(cycelAuditReportItemRecord);
                                                                                  }
                if(!updateCycleAuditReportItemList.isEmpty()){
                    update updateCycleAuditReportItemList;
                    for(KARL_Cycle_Audit_Report_Items__c cycelAuditReportItemRecord:updateCycleAuditReportItemList){
                        cycelAuditReportItemRecord.Status__c = KARL_Constants.Report_Status_Final_Review;
                    }
                    Test.startTest();
                    update updateCycleAuditReportItemList;
                    Test.stopTest();
                    System.assert(![SELECT Id,Status__c FROM KARL_Cycle_Audit_Report_Items__c WHERE Status__c =: KARL_Constants.Report_Status_Final_Review].isEmpty()); 
                }
            }
        }
    }
}
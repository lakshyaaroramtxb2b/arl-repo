public class SPT_CaseTimelineController {
    // SPT_CaseDataWrapper List initialized
    public static List<SPT_CaseDataWrapper> caseWrapperList = new List<SPT_CaseDataWrapper>();
    
    // Below method gets all the case history and case comments and return it as a wrapper
	@AuraEnabled
    public static ReturnDataWrapper getCaseData(String caseID, List<String> selectedPicklistValues, Boolean isToggle){
        Map<id,String> imageURLList = new Map<id,String>();
        List<String> fieldSet = getFieldSetMembers('Case','SPT_CaseHistory');
        Set<ID> userIdSet = new Set<Id>();
        ReturnDataWrapper data = new ReturnDataWrapper();
        String inboundUserId = Label.SPT_InboundUserId;
        String sortingOrder = isToggle ? 'ASC' : 'DESC';
        System.debug('FieldSet values : '+fieldSet);
        System.debug('Selected picklist values: '+selectedPicklistValues);

       /* if(Test.isRunningTest()) {
            CaseHistory caseH = new CaseHistory();
            caseH.Field = 'Status';
            caseH.Field = 'Contact';
           
        }
        else {*/
            // Iterates over case history
            for(CaseHistory caseH : [SELECT ID,CaseId,Field,NewValue,OldValue,CreatedDate,CreatedByID,CreatedBy.Name 
                                     FROM CaseHistory WHERE CaseId=:caseID ORDER BY CreatedDate ASC]){
                // Stores case history of created type
                if(CaseH.Field == 'Created' ){
                    data.caseCreated = new SPT_CaseDataWrapper(caseH.CreatedDate,'CaseCreated',CaseH.CreatedBy.Name,
                                                            null,null,null,(String)caseH.CreatedById, sortingOrder); 
                }
                else if(fieldSet.contains(caseH.Field) || (caseH.Field == 'Owner' && fieldSet.contains('OwnerId'))){
                    // Stores case history when owner is changed
                    if(CaseH.Field == 'Owner' && selectedPicklistValues.contains(CaseH.Field)) {
                        if(!Pattern.compile('[a-zA-Z0-9]{18}|[a-zA-Z0-9]{15}').matcher((String)caseH.NewValue).matches()) {
                            caseWrapperList.add(new SPT_CaseDataWrapper(caseH.CreatedDate, 'CaseOwnerChanged', CaseH.CreatedBy.Name,
                                                                        (String)caseH.OldValue, (String)caseH.NewValue,
                                                                        (String)caseH.Field, (String)caseH.CreatedById, sortingOrder));  
                        }
                    }
                    // Stores case history when status is changed to closed
                    else if(CaseH.Field == 'Status' && caseH.NewValue == 'Closed'){
                        data.caseClosed = new SPT_CaseDataWrapper(caseH.CreatedDate,'CaseClosed',CaseH.CreatedBy.Name,
                                                                null,null,null,(String)caseH.CreatedById, sortingOrder);       
                        data.currentCaseStatus = 'Closed';
                    }
                    // Stores case history when status is changed
                    else if(CaseH.Field == 'Status' && selectedPicklistValues.contains(CaseH.Field)){
                        caseWrapperList.add(new SPT_CaseDataWrapper(caseH.CreatedDate,'CaseStatusChanged',CaseH.CreatedBy.Name,
                                                                    (String)caseH.OldValue,(String)caseH.NewValue,
                                                                    (String)caseH.Field, (String)caseH.CreatedById, sortingOrder));
                        data.currentCaseStatus = (String)caseH.NewValue;
                    }
                    // Stores case history anything apart from owner and status is changed
                    else if(CaseH.Field != 'Status' && CaseH.Field != 'Owner') {
                        caseWrapperList.add(new SPT_CaseDataWrapper(caseH.CreatedDate,'CaseFieldHistory',CaseH.CreatedBy.Name,
                                                                    (String)caseH.OldValue,(String)caseH.NewValue,
                                                                    (String)caseH.Field, (String)caseH.CreatedById, sortingOrder));
                    }
                    userIdSet.add(caseH.CreatedByID);
                }
            }
       // }
        System.debug('Current Case Status : '+data.currentCaseStatus);
        System.debug('Case Created Status : '+data.caseCreated);
        System.debug('Case Closed Status : '+data.caseClosed);

        // Iterates over case comments
        for(CaseComment comments : [SELECT Id, ParentId, CreatedDate, CommentBody, CreatedByID, CreatedBy.Name, IsPublished FROM CaseComment WHERE ParentId= :caseID] ){
            // Stores the template body written before the <<metadata>> tag
            String templateBody = comments.CommentBody.substringBefore('\n<<metadata>>');
            // Stores the comment user name written after the Original_Author tag
            String commentUserName = comments.CommentBody.substringBetween('Original_Author: ', '\n');
            Datetime commentDate;
            // Stores the comment date/time written after the Published_Date: tag
            String commentDateTime = comments.CommentBody.substringAfter('Published_Date: ');
            if(commentDateTime == '') {
                commentDate = comments.createdDate;
            }
            else {
                commentDate = Datetime.valueOfGmt(commentDateTime);
            }
            if(commentUserName == '' || commentUserName == null) {
                commentUserName = comments.CreatedBy.Name;
            }

            // Checks if comment is public
            if(comments.IsPublished) {
                // Stores the public case comments of supportforce org
             	if(commentUserName!='Intake App' && comments.CommentBody.contains(System.Label.SPT_BotTag) && selectedPicklistValues.contains('Public Inbound Comments')) {
                     System.debug('1');
                     System.debug('commentUserName'+ commentUserName);
                     System.debug('comments.CreatedById'+ comments.CreatedById);
                     System.debug('selectedPicklistValues'+ selectedPicklistValues);


                    caseWrapperList.add(new SPT_CaseDataWrapper(commentDate,'PublicCaseCommentsInbound',
                                                                templateBody,commentUserName,
                                                                (ID)comments.CreatedById, sortingOrder));
                }
                // Stores the public case comments of security org
                else if(comments.CreatedById != inboundUserId && selectedPicklistValues.contains('Public Outbound Comments')){
                    System.debug('2');
                     System.debug('commentUserName'+ commentUserName);
                     System.debug('comments.CreatedById'+ comments.CreatedById);
                     System.debug('selectedPicklistValues'+ selectedPicklistValues);

                    caseWrapperList.add(new SPT_CaseDataWrapper(comments.createdDate,'PublicCaseComments',
                                                                templateBody,comments.CreatedBy.Name,
                                                                (ID)comments.CreatedById, sortingOrder));   
                }    
            }  
            // Checks if comment is private
            else {
                // Stores the private case comments of supportforce org
                if(commentUserName!='Intake App' && comments.CommentBody.contains(System.Label.SPT_BotTag) && selectedPicklistValues.contains('Private Inbound Comments')) {
                    caseWrapperList.add(new SPT_CaseDataWrapper(commentDate,'PrivateCaseCommentsInbound',
                                                                templateBody,commentUserName,
                                                                (ID)comments.CreatedById, sortingOrder));
                }
                // Stores the private case comments of security org.
                else if(comments.CreatedById != inboundUserId && selectedPicklistValues.contains('Private Outbound Comments')){
                    caseWrapperList.add(new SPT_CaseDataWrapper(comments.createdDate,'PrivateCaseComments',
                                                                templateBody,comments.CreatedBy.Name,
                                                                (ID)comments.CreatedById, sortingOrder));   
                } 
            }
            userIdSet.add(comments.CreatedByID);
        }
        // If user present, set its image avatar
        if(userIdSet!=null && !userIdSet.isEmpty()){
            for(User userRec : [SELECT ID, FullPhotoUrl, SmallPhotoUrl FROM USER WHERE ID IN :userIdSet] ){
                imageURLList.put(userRec.id,userRec.SmallPhotoUrl);
            }
        }
        // Sorts the case wrapper list
        caseWrapperList.sort();
               
        //Create Return DataSet
        data.caseWrapperList = caseWrapperList;
        data.imageURLList = imageURLList;
        data.processData();
        return data;
    }

    // Returns field set members
    public static List<String> getFieldSetMembers(String objectTypeName, String fieldSetName) {
        List<String> fieldSet = new List<String>();
        DescribeSObjectResult[] describes = Schema.describeSObjects(new List<String>{objectTypeName});    
        if (describes != null && describes.size() > 0) {
            // There should only be the one match for the one object type name
            Schema.FieldSet fs = describes[0].fieldSets.getMap().get(fieldSetName); 
            for(Schema.FieldSetMember record : fs.fields){
                fieldSet.add(record.getFieldPath());
            }
            return fieldSet;
        } else {
            return null;
        }
	}
    
    // Wrapper class for the toggle button
    public class FilterOptionsWrapper {
        @AuraEnabled public String label;
        @AuraEnabled public String value;
        
        public FilterOptionsWrapper(String label, String value) {
            this.label = label;
            this.value = value;
        }
    }
    
    // Return all the filter values for multi-select picklist
    @AuraEnabled
    public static List<FilterOptionsWrapper> getAllFilterOptions() {
        List<FilterOptionsWrapper> filterOptionsList = new List<FilterOptionsWrapper>();
        List<String> allOptionsList = new List<String>{'Select All', 'Public Inbound Comments','Public Outbound Comments', 'Status', 'Owner', 'Private Inbound Comments', 'Private Outbound Comments'};
        for(String option : allOptionsList) {
            FilterOptionsWrapper filterOption = new FilterOptionsWrapper(option, option);
        	filterOptionsList.add(filterOption);        
        }
        System.debug('All Filter Options: '+filterOptionsList);
        return filterOptionsList;
    }

    // Wrapper class for the case data 
    public class ReturnDataWrapper{    	
        @AuraEnabled public List<SPT_CaseDataWrapper> caseWrapperList = new List<SPT_CaseDataWrapper>();
        @AuraEnabled public SPT_CaseDataWrapper caseCreated = null;
        @AuraEnabled public SPT_CaseDataWrapper caseClosed = null;
        @AuraEnabled public Map<id,String> imageURLList = new Map<id,String>();
        @AuraEnabled public String currentCaseStatus = '';
        public ReturnDataWrapper(){}        
        
        /*
        * This method process the case history and case comments and provide header to the first data of a particular date.
        * For the rest, needHeader is false
        */
        public void processData(){
            if(caseWrapperList!=null && !caseWrapperList.isEmpty()){               
                Date startDate = null;         
                // Iterates over all the case data       
                for(SPT_CaseDataWrapper wrapper : caseWrapperList){
                    system.debug('wrapper.createdByID'+wrapper.createdByID);
                    // Provide header to the first data where it's start date not equal to the stored startDate
                    if(startDate==null || wrapper.dateTag!=startDate) {
                        wrapper.needHeader = true;
                        startDate = wrapper.dateTag;
                    }
                    if(wrapper.createdByID!=null && imageURLList.containsKey(wrapper.createdByID)){
                        wrapper.photoUrl = imageURLList.get(wrapper.createdByID);
                    }
                }
            }
        }
    }
}
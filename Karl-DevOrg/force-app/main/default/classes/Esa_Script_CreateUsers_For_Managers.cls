public class Esa_Script_CreateUsers_For_Managers{
 //Run Following methods
 //Esa_Script_CreateUsers_For_Managers.createUsersforManagers();
 //Esa_Script_CreateUsers_For_Managers.assignPermissionSet();
 //Esa_Script_CreateUsers_For_Managers.shareEntityManagers();
 
 //step 1 create users for missing managers
 public static void createUsersforManagers(){
     List<Contact> lstConts = [SELECT Name, Email,
                                    FirstName,
                                    LastName,
                                    Employee_ID__c,
                                    Company__c,
                                    Department,
                                    Title
                            FROM Contact 
                            WHERE Management_Chain_Level_03__c = 'James Alkove (735580)' 
                            AND Is_Manager__c = true];

  Set<String> setEmails = new Set<String>();
  for(Contact cont : lstConts) {
      setEmails.add(cont.Email);
  }

  List<User> lstUsers = [SELECT Id,UserName,Email 
                         FROM User 
                         WHERE accountId=null 
                         AND isActive=true 
                         AND Email IN : setEmails];

  Map<String,User> mapUsers = new Map<String,User>();
  for(User u : lstUsers){
     mapUsers.put(u.Email, u);
  }

  List<Contact> lstContacts = new List<Contact>();
  for(Contact cont : lstConts) {
     if(mapUsers.get(cont.Email) == null) {
        lstContacts.add(cont);
     }
   }
   
   system.debug('$$$$$$$'+ lstContacts.size());
   
   lstUsers = new List<User>();
   string aliasName = '';
   for(Contact cont : lstContacts) {
     User usr = new User();
     usr.UserName = cont.Email.substringbefore('@')+'@security.salesforce.com';
     usr.firstName = cont.FirstName;
     usr.LastName = cont.LastName;
     usr.ProfileId = '00e3A000002CLgo';
     usr.CompanyName = cont.Company__c;
     usr.Department = cont.Department;
     usr.EmployeeNumber = cont.Employee_ID__c;
     usr.Title = cont.Title;
     usr.Email = cont.Email;

     aliasName = (cont.FirstName.Left(1) + cont.LastName.Left(4)).Trim();
     usr.Alias = aliasName;
     usr.CommunityNickname = aliasName + String.valueOf(DateTime.now().millisecondGMT());
     usr.FederationIdentifier = cont.Email;
     usr.TimeZoneSidKey = 'America/Los_Angeles';
     usr.EmailEncodingKey = 'ISO-8859-1';
     usr.LanguageLocaleKey = 'en_US';
     usr.LocaleSidKey = 'en_US';
     lstUsers.add(usr);
   }
    
    system.debug('######'+lstUsers.size());
    Database.DMLOptions dlo = new Database.DMLOptions();
    dlo.EmailHeader.triggerUserEmail = false;
    database.insert(lstUsers, dlo);  

 
 }
 
 //Assign Esa_Security_request_User permission set to Managers.
 public static void assignPermissionSet(){
    //assign permission sets .

List<Contact> lstConts = [SELECT Email
                           FROM Contact 
                           WHERE Management_Chain_Level_03__c = 'James Alkove (735580)' 
                           AND Is_Manager__c = true];

  Set<String> setEmails = new Set<String>();
  for(Contact cont : lstConts) {
      setEmails.add(cont.Email);
  }

  List<User> lstUsers = [SELECT Id 
                              FROM User 
                              WHERE accountId=null 
                                     AND isActive=true 
                                     AND Email IN : setEmails];



List<PermissionSetAssignment> lstPermsetAssignments = new List<PermissionSetAssignment>();

for(User usr : lstUsers) {
    PermissionSetAssignment pAssignment = new PermissionSetAssignment();
    pAssignment.PermissionSetId = '0PS300000004WiL';
    pAssignment.AssigneeId = usr.Id;
    lstPermsetAssignments.add(pAssignment);
}

Database.saveResult[] saveResult = database.insert(lstPermsetAssignments,false);
for(Integer i=0;i<saveResult.size();i++){
   if (!saveResult.get(i).isSuccess()){
      Database.Error error = saveResult.get(i).getErrors().get(0);
      system.debug('Exception Message *****'+error.getMessage()); 
   }
}

 }
 
 
// Share BUS entity to newly managers in security org.

public static void shareEntityManagers(){
 List<Contact> lstConts = [SELECT Email
                           FROM Contact 
                           WHERE Management_Chain_Level_03__c = 'James Alkove (735580)' 
                           AND Is_Manager__c = true];

  Set<String> setEmails = new Set<String>();
  for(Contact cont : lstConts) {
      setEmails.add(cont.Email);
  }

  List<User> lstUsers = [SELECT Id 
                         FROM User 
                         WHERE accountId=null 
                         AND isActive=true 
                         AND Email IN : setEmails];



List<ESA_Entity__Share> lstEntityShare = new List<ESA_Entity__Share>();

for(User usr : lstUsers) {
    ESA_Entity__Share entityShare = new ESA_Entity__Share();
    entityShare.ParentId='a5X3A000000LTgAUAW';
    entityShare.AccessLevel='Read';
    entityShare.Rowcause='Manual';
    entityShare.UserOrGroupId = usr.Id;
    lstEntityShare.add(entityShare);
}

Database.saveResult[] saveResult = database.insert(lstEntityShare,false);
for(Integer i=0;i<saveResult.size();i++){
   if (!saveResult.get(i).isSuccess()){
      Database.Error error = saveResult.get(i).getErrors().get(0);
      system.debug('Exception Message *****'+error.getMessage()); 
   }
}

}
 
  
}
public with sharing class EmailHoneypotController extends InternalSiteController {
    private Email_Honeypot__c honeypot;
    public Boolean editMode {get;private set;}
    public String editUrl {get;private set;}
    private EmailHoneypotSettings__c sets;
    
    public String SourceEmail {get;set;}
    public String ContactEmail {get;set;}
    public String ContactName {get;set;}
    public String Description {get;set;}
    public String Vendor {get;set;}
    
    public String fakeName {get;set;}
    public String fakeEmail {get;private set;}
    private Notifier.NamedNotifier logger;
    private Pattern SFDC_EMAIL = pattern.compile('[a-zA-Z\\.\\-0-9]+@(contractor\\.)?salesforce.com');
    
    public EmailHoneypotController() {
        // InternalSiteController does security checks.  Call this first
        super();
        logger = Notifier.getNotifier('EmailHoneyPot');
        
        sets = EmailHoneypotSettings__c.getOrgDefaults();
        String encEntry = ApexPages.currentPage().getParameters().get('entry');
        if (encEntry!=null) {
            // They passed an encrypted ID in for the entry.  We're in edit mode
            EncryptionSettings__c enc = EncryptionSettings__c.getOrgDefaults();
            Blob key = EncodingUtil.base64Decode(enc.Symmetric_Key__c);
            
            // <ducks>....
            Id hId;
            try {
                hId = Id.valueOf(Crypto.decryptWithManagedIV('AES256',key,EncodingUtil.base64Decode(encEntry)).toString());
            } catch (Exception e) {
                // behave normally as to not disclose padding errors and the like
                logger.log(Notifier.Severity.WARNING,'Unable to decrypt "entry" parameter->'+e.getMessage());
                return;
            }
            
            // Since we're editing, we want to intialize some fields.  These are all strings because we're on Sites and 
            // dealing with CRUD/FLS bypassing
            honeypot = [SELECT Name,Authorized_Source_Email__c,Contact_Email__c,Contact_Name__c,Description__c,Email__c,Vendor__c,
                               Fake_Name__c
                        FROM Email_Honeypot__c
                        WHERE Id=: hId];
            SourceEmail = honeypot.Authorized_Source_Email__c;
            ContactEmail = honeypot.Contact_Email__c;
            ContactName  = honeypot.Contact_Name__c;
            Description  = honeypot.Description__c;
            Vendor       = honeypot.Vendor__c;
            fakeName     = honeypot.Fake_Name__c;
            fakeEmail    = honeypot.Email__c;
            String url = Site.getCurrentSiteUrl().substring(0,Site.getCurrentSiteUrl().length()-1)+
                           Page.MonitoredEmail.getUrl()+'?entry='+EncodingUtil.urlEncode(encEntry,'UTF-8');
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info,
                                       'This page can be used to make updates to this monitored address.  Please save the below link or add it to your bookmarks.<br /><br />'+
                                       url));
                               
            editMode = True;
        } else {
            honeypot = new Email_Honeypot__c();
            editMode = False;
            generateName();
        }
        
    }
    
    public PageReference modifyRegistry() {
        // May be new or an edit
        honeypot.Authorized_Source_Email__c = SourceEmail;
        if (!isSfdcEmail(ContactEmail)) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'SFDC Contact Email must be a @salesforce.com email address'));
            return null;
        }
        honeypot.Contact_Email__c = ContactEmail;
        honeypot.Contact_Name__c = ContactName;
        honeypot.Description__c = Description;

        if (editMode == True) {
            // that's really all we want to let them edit.  The VF page should reflect this logic in the read only fields
            update honeypot;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Record updated'));
            return null;
        } else {
            // it's new so they have more flexibility
            honeypot.Vendor__c = Vendor;
            honeypot.Fake_Name__c = fakeName;
            honeypot.Email__c = fakeEmail;
            honeypot.Name = fakeEmail+' - '+Vendor;
            Database.UpsertResult sr = Database.upsert(honeypot,False);
            if (sr.isSuccess() == False) {
                for (Database.Error err: sr.getErrors()) {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,err.getMessage()));
                }
            } else {
                EncryptionSettings__c enc = EncryptionSettings__c.getOrgDefaults();
                Blob key = EncodingUtil.base64Decode(enc.Symmetric_Key__c);
                String encEntry = EncodingUtil.base64encode(Crypto.encryptWithManagedIV('AES256',key,Blob.valueOf(honeypot.Id)));
                //String url = Site.getCurrentSiteUrl()+Page.EmailHoneypot.name+'?entry='+EncodingUtil.urlEncode(encEntry,'UTF-8');
                // Redirect them to the new edit URL that will have the confirmation message
                PageReference target = new PageReference(Page.MonitoredEmail.getUrl()+'?entry='+EncodingUtil.urlEncode(encEntry,'UTF-8'));
                target.setRedirect(True);
                return target;
            }    
        }
        
        return null;
    }
    
    public Boolean isSfdcEmail(String email) {
        return SFDC_EMAIL.matcher(email).matches();
    }
    
    public PageReference generateName() {
        fakeName = FIRST_NAMES.get(Math.mod(Math.abs(Crypto.getRandomInteger()),FIRST_NAMES.size())-1)+' '+LAST_NAMES.get(Math.mod(Math.abs(Crypto.getRandomInteger()),LAST_NAMES.size())-1);
        
        fakeEmail = fakeName.split(' ').get(0).substring(0,1).toLowerCase()+fakeName.split(' ').get(1).toLowerCase()+'@'+sets.Email_Domain__c;
        return null;
    }
    
    private static final List<String> FIRST_NAMES = new String[] {'Abhinesh', 'Accounts', 'Achilles', 'Adam', 'Admin', 'Adrianne', 'Adua', 'Agnes', 'Ahmad', 'Alain', 'Alan', 'Aldo', 'Alejandro', 'Alesha', 'Alessandro', 'Alexandra', 'Alexis', 'Alice', 'Alicia', 'Alison', 'Allan', 'Amanda', 'Amber', 'Amelia', 'Amy', 'Andre', 'Andrea', 'Andreas', 'Andreea', 'Andrew', 'Andy', 'Angela', 'Anika', 'Ankit', 'Ankush', 'Ann', 'Anna', 'Anne', 'Annette', 'Anthony', 'Antonia', 'Antonino', 'Antonio', 'Anu', 'Anuradha', 'Apoorv', 'April', 'Archana', 'Ari', 'Arndt', 'Art', 'Aruna', 'Ashley', 'Atp', 'Audrey', 'Aurelie', 'Austin', 'Balam', 'Barbara', 'Barney', 'Basak', 'Bennett', 'Bernadette', 'Bert', 'Berta', 'Beth', 'Bev', 'Bill', 'Bob', 'Bonnie', 'Brad', 'Brady', 'Brandy', 'Brendan', 'Brian', 'Brigid', 'Brigitte', 'Britney', 'Bruce', 'Bruno', 'Bud', 'Bulent', 'Cam', 'Carl', 'Carla', 'Carlos', 'Carly', 'Carolina', 'Caroline', 'Carrie', 'Cary', 'Casey', 'Cassie', 'Catherine', 'Cecile', 'Charles', 'Charlie', 'Chelsea', 'Cheri', 'Cheryl', 'Chike', 'Chip', 'Chris', 'Chrisi', 'Christa', 'Christian', 'Christina', 'Christine', 'Christoph', 'Christophe', 'Christopher', 'Chuck', 'Claude', 'Clifford', 'Clodagh', 'Clove', 'Colin', 'Colleen', 'Colm', 'Conor', 'Convio', 'Corinn', 'Cory', 'Courtney', 'Criag', 'Cynthia', 'Dale', 'Dan', 'Daniel', 'Darin', 'Darren', 'Daryl', 'Dave', 'David', 'Deanna', 'Debbie', 'Denise', 'Dennis', 'Derek', 'Desiree', 'Diana', 'Diane', 'Dirk', 'Dominique', 'Don', 'Doug', 'Douglas', 'Dugan', 'Duncan', 'Ed', 'Edward', 'Elaine', 'Eli', 'Eliaz', 'Elisabeth', 'Elizabeth', 'Emily', 'Emma', 'Enno', 'Eric', 'Erin', 'Eriyah', 'Eugenia', 'Eva', 'Evan', 'Everett', 'Ewen', 'Faith', 'Feli', 'Filipp', 'Francois', 'Frank', 'Fred', 'Frederic', 'Frederique', 'Front', 'Gabriel', 'Gail', 'Gary', 'Gathering', 'George', 'Gerald', 'Gerry', 'Gil', 'Gilbert', 'Gilles', 'Gina', 'Giovanni', 'Giuseppe', 'Glenys', 'Graham', 'Greg', 'Gregg', 'Guillaume', 'Guy', 'Halden', 'Halina', 'Hannah', 'Heather', 'Hector', 'Helen', 'Helmut', 'Henrik', 'Hila', 'Holger', 'Howard', 'Hrund', 'Hugh', 'Hugo', 'Hussain', 'Ian', 'Indra', 'Ivie', 'Jalal', 'James', 'Jan', 'Jane', 'Janet', 'Jared', 'Jarrod', 'Jason', 'Javier', 'Jay', 'Jayne', 'Jb', 'Jean', 'Jeanne', 'Jeff', 'Jeffrey', 'Jelle', 'Jen', 'Jena', 'Jenn', 'Jennifer', 'Jenny', 'Jerald', 'Jeremy', 'Jeri', 'Jerome', 'Jerry', 'Jess', 'Jesse', 'Jessica', 'Jill', 'Jim', 'Jithin', 'Joan', 'Joanna', 'Joanne', 'Jodi', 'Joe', 'Joel', 'Jofre', 'Johan', 'John', 'Johnna', 'Jolen', 'Jon', 'Jonathan', 'Jonny', 'Jordan', 'Joseph', 'Joyce', 'Juan', 'Juli', 'Julia', 'Julian', 'Juliana', 'Julie', 'Julieta', 'Julio', 'Julissa', 'Karen', 'Karl', 'Kat', 'Kathleen', 'Kathryn', 'Kathy', 'Katie', 'Kay', 'Kc', 'Keenan', 'Keith', 'Kelli', 'Kelly', 'Ken', 'Kenitha', 'Kenneth', 'Kermit', 'Kevin', 'Kim', 'Kit', 'Kobi', 'Kondu', 'Kristen', 'Kristian', 'Kristina', 'Krithika', 'Kyle', 'Kymmy', 'Larisa', 'Larry', 'Laurel', 'Laurence', 'Laurette', 'Lawrence', 'Leah', 'Lee', 'Leela', 'Leigh', 'Leona', 'Leonardo', 'Leslie', 'Lewis', 'Lidia', 'Liesl', 'Linda', 'Lindsey', 'Lisa', 'Lise', 'Lisha', 'Liz', 'Lo', 'Lore', 'Lpl', 'Lucas', 'Luis', 'Luke', 'Lynn', 'Maarten', 'Maggie', 'Mahendar', 'Manish', 'Manny', 'Manus', 'Manya', 'Marc', 'Margaret', 'Maria', 'Marianne', 'Mark', 'Markus', 'Marloes', 'Marshelle', 'Martin', 'Marvin', 'Mary', 'Maryann', 'Masato', 'Mason', 'Matt', 'Matthew', 'Mattias', 'Maxime', 'Mckay', 'Mehdi', 'Melissa', 'Mendy', 'Meryl', 'Michael', 'Miguel', 'Mike', 'Miki', 'Miled', 'Mina', 'Ming', 'Mitch', 'Mohamed', 'Mohammed', 'Mohini', 'Molly', 'Monique', 'Nada', 'Nagasri', 'Nancy', 'Nanette', 'Nasser', 'Natalie', 'Nate', 'Neal', 'Neeraj', 'Neil', 'Nicholas', 'Nichole', 'Nicolas', 'Nicole', 'Nicolle', 'Nita', 'Norm', 'Ole', 'Olga', 'Osr', 'Pankaj', 'Pascal', 'Patrick', 'Patti', 'Paul', 'Paula', 'Pauline', 'Pelle', 'Perla', 'Peter', 'Phil', 'Philipp', 'Pierre', 'Prabhakar', 'Proposta', 'Quinton', 'Rachel', 'Ralph', 'Ramon', 'Randy', 'Raphael', 'Ravikumar', 'Ray', 'Rebecca', 'Reem', 'Reinaldo', 'Rhonda', 'Ricardo', 'Rich', 'Richard', 'Rick', 'Rima', 'Rob', 'Robbie', 'Robert', 'Rod', 'Roger', 'Roland', 'Ron', 'Ronald', 'Ross', 'Ruben', 'Russell', 'Ryan', 'Sabine', 'Sachin', 'Sam', 'Samantha', 'Sami', 'Sandeep', 'Sandra', 'Sandy', 'Sanjith', 'Santosh', 'Sara', 'Sarah', 'Sathish', 'Satvir', 'Scott', 'Sean', 'Sebasten', 'Seema', 'Segen', 'Serena', 'Shalini', 'Shannon', 'Sharon', 'Shashidhar', 'Shawn', 'Sheila', 'Shelby', 'Shelley', 'Sheryl', 'Shintaro', 'Shirley', 'Simon', 'Sonia', 'Sramana', 'Srikanth', 'Stan', 'Stephan', 'Stephanie', 'Stephanye', 'Stephen', 'Steve', 'Steven', 'Stuart', 'Sujan', 'Suman', 'Sumit', 'Suresh', 'Susan', 'Susanne', 'Suzi', 'Sybil', 'Sylvia', 'Takayuki', 'Tamany', 'Tara', 'Tassee', 'Tatiana', 'Ted', 'Terri', 'Terry', 'Teuta', 'Theresa', 'Thierry', 'Thomas', 'Tim', 'Tina', 'Tiny', 'Toby', 'Todd', 'Todor', 'Tom', 'Tommy', 'Tony', 'Torsten', 'Tracie', 'Tracy', 'Traffic', 'Travis', 'Trempe', 'Tricia', 'Tyler', 'Tyrone', 'Ulrike', 'Valencia', 'Valerie', 'Vanessa', 'Varonda', 'Varun', 'Vg', 'Vicki', 'Vickie', 'Victor', 'Victoria', 'Viktoria', 'Vinay', 'Vinayprasad', 'Vinesh', 'Viviana', 'Voucher', 'Wale', 'Wendy', 'William', 'Wingu', 'Wolfram', 'Xen', 'Yayoi', 'Yuki', 'Zach', 'Zheng', 'Zoran'};
    private static final List<String> LAST_NAMES = new String [] {'Abdulnar', 'Achaval', 'Adler', 'Admin', 'Aherne', 'Aima', 'Albanna', 'Alderson', 'Alessi', 'Alexander', 'Alexandre', 'Allen', 'Altergott', 'Anderson', 'Ang', 'Angell', 'Anglin', 'Annis', 'Arca', 'Arendes', 'Arksn', 'Arndt', 'Arnoul', 'Aronow', 'Arora', 'Asada', 'Astadia', 'Augustini', 'Awdas', 'Baer', 'Bailey', 'Balakumar', 'Bamford', 'Bancilhon', 'Barbieri', 'Barman', 'Baro', 'Barrow', 'Basiplikci', 'Bastarache', 'Battaini', 'Bauer', 'Bayer', 'Becheru', 'Beckers', 'Belchhaus', 'Bellagha', 'Bellamy', 'Benarroch', 'Benbow', 'Beneton', 'Bennett', 'Bentson', 'Beyer', 'Bhagat', 'Biggle', 'Black', 'Blackwell', 'Blanco', 'Blumenfeld', 'Blyth', 'Blythe', 'Bobrovskaya', 'Boer', 'Bolognia', 'Bolsinger', 'Bonner', 'Boomsma', 'Borden', 'Bossut', 'Bourne', 'Bouvier', 'Bowling', 'Boyd', 'Boyle', 'Breese', 'Breitmeyer', 'Brewster', 'Bridges', 'Bright', 'Brostek', 'Brown', 'Brunette', 'Bryant', 'Burkett', 'Burrows', 'Busads', 'Butcher', 'Byers', 'Caban', 'Cabrera', 'Caicedo', 'Calantoni', 'Caldwell', 'Callum', 'Calvert', 'Campbell', 'Canepa', 'Carneiro', 'Carsley', 'Carson', 'Carter', 'Cartwright', 'Cavolina', 'Cesarini', 'Chahrour', 'Chapman', 'Charles', 'Charman', 'Chaturvedi', 'Chesney', 'Chillingworth', 'Chiu', 'Christiansen', 'Chua', 'Churchill', 'Clark', 'Close', 'Cochran', 'Codik', 'Cohen', 'Coles', 'Collins', 'Conner', 'Connolly', 'Conrad', 'Conrads', 'Constable', 'Cook', 'Cornelius', 'Cornette', 'Costello', 'Couch', 'Cowdin', 'Cox', 'Craft', 'Cramer', 'Crestodina', 'Crisan', 'Crossley', 'Crouch', 'Cutmore', 'Dabis', 'Dahlberg', 'Daily', 'Dalessandro', 'Daly', 'Damron', 'Daniel', 'Dao', 'Darey', 'Davis', 'Day', 'Dearmon', 'Deblander', 'Dejarnette', 'Delazo', 'Deluca', 'Demaestri', 'Demuth', 'Denger', 'Dennison', 'Deregt', 'Desk', 'Desmas', 'Desravines', 'Dewindt', 'Dewitt', 'Diamond', 'Dilliott', 'Dineen', 'Dodes', 'Donaldson', 'Dons', 'Doran', 'Dorenkott', 'Dorn', 'Doster', 'Doyle', 'Dubinsky', 'Dunn', 'Ebrahim', 'Ebsworth', 'Edminster', 'Edsall', 'Edwards', 'Eisenberg', 'Elkins', 'Elleston', 'Ellman', 'Elvas', 'Emmert', 'Engler', 'Epps', 'Epstein', 'Erdem', 'Erickson', 'Eriksson', 'Esposito', 'Estes', 'Estrada', 'Evans', 'Even', 'Fahner', 'Faraone', 'Fernandez', 'Fetcho', 'Ficca', 'Fitzgerald', 'Fjelberg', 'Flaherty', 'Fletcher', 'Flynn', 'Fogel', 'Foote', 'Ford', 'Foulk', 'Fournier', 'Foy', 'Franco', 'Frazier', 'Freeman', 'Freher', 'Freytag', 'Friedberg', 'Fuss', 'Galloway', 'Garcia', 'Gardner', 'Garrett', 'Garrido', 'Gaskins', 'Gateley', 'Gause', 'Gci', 'Geary', 'Gee', 'Genovese', 'George', 'Gertz', 'Gibson', 'Giddon', 'Gilbert', 'Gilyova', 'Glad', 'Gladysz', 'Glenn', 'Glenwright', 'Glessing', 'Godsey', 'Goldberg', 'Gommers', 'Good', 'Gore', 'Gorman', 'Govindasamy', 'Gowda', 'Grabo', 'Graham', 'Granoff', 'Grant', 'Greene', 'Greenhalgh', 'Grilli', 'Grossetti', 'Guarino', 'Guerin', 'Guest', 'Guettier', 'Guillouzonic', 'Gulitzdk', 'Gumpper', 'Gustafson', 'Haley', 'Hamm', 'Hammer', 'Hammond', 'Hannequart', 'Hansen', 'Harris', 'Harrison', 'Hart', 'Hartley', 'Hassard', 'Haston', 'Hatch', 'Hawk', 'Hay', 'Hazony', 'Healy', 'Hegde', 'Helms', 'Hennessey', 'Hernandez', 'Higgs', 'Hill', 'Hintzen', 'Hoffman', 'Hoffmann', 'Hogue', 'Holdaway', 'Holden', 'Hoshiko', 'Howard', 'Huff', 'Hughes', 'Hull', 'Hunsicker', 'Hunt', 'Huynh', 'Hyland', 'Hytrek', 'Iespa', 'Info', 'Innamorati', 'Isaacson', 'Isham', 'Jabs', 'Jackson', 'Jacobs', 'Jacobsen', 'Jain', 'Jalbert', 'Jata', 'Jaussi', 'Jenkins', 'Johns', 'Johnson', 'Joho', 'Jones', 'Jorgenson', 'Joshi', 'Jutnicke', 'Kahn', 'Kander', 'Kapa', 'Kapoor', 'Kardjiev', 'Karkan', 'Karlis', 'Kauer', 'Kearney', 'Keck', 'Keegan', 'Keen', 'Keenan', 'Kelly', 'Kenney', 'Kidder', 'King', 'Kingston', 'Kinnealey', 'Kiss', 'Kitchen', 'Klein', 'Kline', 'Knott', 'Koblischke', 'Koch', 'Kohout', 'Koivu', 'Kong', 'Koops', 'Kopelovich', 'Kopp', 'Koss', 'Kramer', 'Krasnikova', 'Krawchuk', 'Krishna', 'Kubiza', 'Kuder', 'Kwong', 'Lacap', 'Ladousse', 'Lahey', 'Lai', 'Lane', 'Lang', 'Langford', 'Laquinon', 'Larsen', 'Laver', 'Leach', 'Leads', 'Leavitt', 'Lebahn', 'Lee', 'Leger', 'Lehner', 'Leinen', 'Leland', 'Lena', 'Lentze', 'Lenz', 'Leong', 'Leslie', 'Levien', 'Lewis', 'Lhoest', 'Licona', 'Lilly', 'Lincoln', 'Lindholm', 'Liubinskas', 'Lloret', 'Loadman', 'Lopez', 'Loucel', 'Lowder', 'Lowelle', 'Lubitz', 'Lugina', 'Lund', 'Lungu', 'Lynch', 'Lyonnais', 'Lyons', 'Maahs', 'Machenheimer', 'Machert', 'Mackinnon', 'Mackintosh', 'Madsen', 'Maggard', 'Magnan', 'Mahalingam', 'Malamphy', 'Mandilaras', 'Mannone', 'Marcoux', 'Marek', 'Margulies', 'Mark', 'Marks', 'Marom', 'Martinez', 'Marvill', 'Mary', 'Matthijs', 'Maubane', 'Maucher', 'Maxcy', 'May', 'Mazzei', 'Mcauley', 'Mcburney', 'Mccabe', 'Mccann', 'Mccauley', 'Mcclendon', 'Mcdonald', 'Mcelveen', 'Mcgrath', 'Mckeon', 'Mcknight', 'Mckown', 'Mclennan', 'Mcpherson', 'Mead', 'Meadows', 'Meganathan', 'Mello', 'Menelly', 'Merritt', 'Mesh', 'Messing', 'Metzler', 'Meyn', 'Mickle', 'Midiri', 'Millan', 'Miller', 'Milovanovic', 'Milsom', 'Miranda', 'Mitra', 'Mitric', 'Miu', 'Mohamed', 'Mohan', 'Moitoza', 'Monnickendam', 'Monserand', 'Monti', 'Moons', 'Moore', 'Moores', 'Moorlehi', 'Moran', 'Morgan', 'Moroianu', 'Morris', 'Morrow', 'Moser', 'Mossing', 'Mousaad', 'Mouw', 'Moylette', 'Muex', 'Mulherin', 'Mullen', 'Mundhenk', 'Murch', 'Murphy', 'Nagawa', 'Naik', 'Nailady', 'Nambiar', 'Navarro', 'Neilly', 'Neumann', 'Newbold', 'Newnham', 'Nicolaou', 'Nicolia', 'Nidever', 'Niegergall', 'Nightingale', 'Nigo', 'Noakes', 'Nohr', 'Norbury', 'Novak', 'Oestreich', 'Okonkwo', 'Oktem', 'Oliviero', 'Ollunga', 'Olsen', 'Olson', 'Opat', 'Opheim', 'Orrell', 'Osideko', 'Owen', 'Pabst', 'Paetzel', 'Paez', 'Pakenham', 'Palau', 'Palm', 'Palmer', 'Pareek', 'Parker', 'Parnaby', 'Parsons', 'Partners', 'Parzkata', 'Payable', 'Payne', 'Peake', 'Peeler', 'Pellisier', 'Pelsser', 'Peltier', 'Peng', 'Penland', 'Perrin', 'Peterson', 'Pettinato', 'Peyrard', 'Pflanzl', 'Phillips', 'Piesiak', 'Pinkerton', 'Pitman', 'Plumbley', 'Poitevint', 'Poole', 'Potharaju', 'Pottier', 'Poulin', 'Poulton', 'Power', 'Pradeau', 'Prajer', 'Prasad', 'Pratt', 'Preziosi', 'Price', 'Prodoehl', 'Pundanera', 'Raffal', 'Rajput', 'Ram', 'Ramsdell', 'Rancon', 'Ranson', 'Ray', 'Reach', 'Reed', 'Reineke', 'Reis', 'Remaley', 'Reynolds', 'Rich', 'Richard', 'Richardson', 'Richmond', 'Ricker', 'Riis', 'Rizopulos', 'Robbins', 'Robles', 'Roche', 'Rocker', 'Rodger', 'Rodriguez', 'Roeller', 'Rogers', 'Rolshovan', 'Romero', 'Rose', 'Rosenberger', 'Ross', 'Rostant', 'Rountree', 'Rousseau', 'Rozario', 'Ruggiero', 'Russell', 'Ruth', 'Saji', 'Sales', 'Salisbury', 'Samaras', 'Sangha', 'Santamaria', 'Saperstein', 'Sapra', 'Sasko', 'Saulet', 'Scanland', 'Scarr', 'Schavrien', 'Scheffel', 'Schmaus', 'Schnieder', 'Schot', 'Schroeter', 'Schwuchow', 'Scott', 'Scout', 'Scsales', 'Seneviratne', 'Serafina', 'Serpa', 'Serrano', 'Servino', 'Severson', 'Shankman', 'Sharma', 'Sharp', 'Shea', 'Shearer', 'Sheintzait', 'Shemesh', 'Sherrie', 'Shiwota', 'Shockman', 'Shoemaker', 'Shuford', 'Simpson', 'Sinclair', 'Skaggs', 'Skelton', 'Skjerven', 'Smith', 'Snead', 'Snitowsky', 'Snyman', 'Sobelman', 'Sokoliuk', 'Soldevila', 'Solutions', 'Soto', 'Soularue', 'Spaletto', 'Speer', 'Spinelli', 'Spinner', 'Spiro', 'Sprouse', 'Stamper', 'Staniforth', 'Stanley', 'Starcevic', 'Steinman', 'Stephanos', 'Stephens', 'Stiller', 'Stone', 'Stough', 'Strehle', 'Streich', 'Stroscio', 'Studer', 'Stumberg', 'Sudhir', 'Sulieman', 'Sumner', 'Supko', 'Suzuki', 'Svoboda', 'Swackhamer', 'Sweet', 'Synkoski', 'Tadikonda', 'Takemoto', 'Tamura', 'Taniguchi', 'Tarabaih', 'Taskar', 'Team', 'Teasdale', 'Tellado', 'Temme', 'Temp', 'Terrell', 'Terry', 'Tham', 'Thell', 'Thompson', 'Threats', 'Tillott', 'Tishkina', 'Tocci', 'Tomlinson', 'Toni', 'Torres', 'Tran', 'Tremblay', 'Trench', 'Tringali', 'Trost', 'Tsung', 'Tumurugoti', 'Ubeda', 'Ulep', 'Unnithan', 'Urbain', 'Urdang', 'User', 'Vaala', 'Vaananen', 'Vaden', 'Vandergrift', 'Vandromme', 'Vargas', 'Varjan', 'Veinotte', 'Vela', 'Vergeer', 'Verma', 'Vermillion', 'Vespa', 'Vignon', 'Vila', 'Vinick', 'Viola', 'Vromans', 'Waite', 'Waldo', 'Walker', 'Wallace', 'Wallenbrock', 'Walsh', 'Walter', 'Ward', 'Warner', 'Warren', 'Watson', 'Webb', 'Webber', 'Wee', 'Wei', 'Weiner', 'Westenkirchner', 'White', 'Whitlock', 'Wiewall', 'Wigu', 'Wilbraham', 'Wilden', 'Wilfong', 'Wilhelm', 'Williams', 'Willier', 'Willliams', 'Willox', 'Winfield', 'Wise', 'Wisniewski', 'Wodzisz', 'Wolf', 'Wong', 'Wood', 'Woolley', 'Work', 'Wright', 'Wurzinger', 'Yahi', 'Yakubik', 'Young', 'Younger', 'Zaferidou', 'Zanat', 'Zandler', 'Zaslavsky', 'Zepeda', 'Zey', 'Zhu', 'Zielinski', 'Zimmerman', 'Zink', 'Zitzloff', 'Zolner'};
}
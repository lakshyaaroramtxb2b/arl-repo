@isTest
public class PRT_V2MOMChangeReqTriggerHelperTest {
    @TestSetup 
    public static void makeData() {
        List<User> userList = new List<User>(PRT_TestDataFactory.createUsers(1, 'System Administrator', true));    
    }
    
    @isTest
    public static void approvedChangeManuallyTest() {
        Portfolio__c portfolio = new Portfolio__c();
        portfolio.Name = 'Security GRC';
        INSERT portfolio;

        V2MOM_Change_Request__c changeRequest = new V2MOM_Change_Request__c();
        changeRequest.Status__c = 'New';
        changeRequest.Portfolio__c = portfolio.Id;
        INSERT changeRequest;
        
        V2MOM_Change_Request__c changeRequest2 = new V2MOM_Change_Request__c();
        changeRequest2.Status__c = 'Draft';
        changeRequest2.Portfolio__c = portfolio.Id;
        INSERT changeRequest2;

        Method_and_Measure_Change__c measureChange = new Method_and_Measure_Change__c();
        measureChange.Change_Request__c = changeRequest.Id;
        measureChange.Detailed_Justification__c = 'Test Detailed Justification';
        measureChange.Measure_Comment__c = 'Test Comment';
        measureChange.Measure_Name__c = 'Test Measure Name';
        measureChange.Measure_Target_Value__c = 12.00;
        measureChange.Target_Date_of_Change__c = Date.today().addDays(6);
        INSERT measureChange;

        Method_and_Measure_Change__c methodChange = new Method_and_Measure_Change__c();
        methodChange.Change_Request__c = changeRequest.Id;
        methodChange.Detailed_Justification__c = 'Test Detailed Justification';
        methodChange.Method_Description__c = 'Test Description';
        methodChange.Method_Name__c = 'Test Method Name';
        methodChange.Target_Date_of_Change__c = Date.today().addDays(6);
        INSERT methodChange;
        
        List<User> userList = new List<User>([SELECT Id FROM USER WHERE Email Like 'PRTUser%']);
        
        PRT_V2MOMChangeReqTriggerHelper.mockedOwnerIds.add(String.valueOf(userList[0].Id));
        if(changeRequest!=null) {
            changeRequest.Status__c = 'Approved - Changed Manually';
            UPDATE changeRequest; 
        }
    }
}
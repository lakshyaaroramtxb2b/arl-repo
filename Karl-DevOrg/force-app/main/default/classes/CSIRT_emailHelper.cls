public with sharing class CSIRT_emailHelper {
    public void trgCommentEmail(List<Case> lstNewCases, List<Case> lstOldCases) {
        
        List<RecordType> rts = new List<RecordType>(CSIRT_caseCache.EmailRtId());
        
        System.debug('******rts is  : ' + rts[0].developerName);      
                          
        List<Case> cases = [ 
        SELECT ContactId, RecordTypeId, Contact.Email, OwnerId, Status, IRCloud__Email_To__c, Send_Response_Template__c
        FROM Case
        WHERE Id IN :lstNewCases AND (Contact.Email != '' or contact.email != null)];
        
        if (cases.size()>0){
        System.debug('******Size of Case list is : ' + cases.size());
        System.debug('******Contact details in first case are: ' + cases[0].contactid + cases[0].contact.Email);
        
        Map<Id, Case> mapOldCases = new Map<Id, Case>();
        
        for (Case o : lstOldCases){
            mapOldCases.put(o.Id, o);
        }
        
        System.debug('******Size of RTS : ' + rts.size());
        if (rts != null && rts.size() ==1 ) {
            
            Id emailid = rts[0].id;
            System.debug('EMAIL RTID is :' + emailid);
                
                List<Messaging.SingleEmailMessage> ackmessages = new List<Messaging.SingleEmailMessage>();
                List<Messaging.SingleEmailMessage> templatemessages = new List<Messaging.SingleEmailMessage>();
                
                for (Case c : cases ) {
                    System.debug('******c.recordtypeid is : ' +c.RecordTypeId);
                    System.debug('********contactemail is : '+ c.contact.email);
                        if (emailid == c.recordTypeId) {
                            System.debug('******recordtype id is : ' + emailid);
                            String devname = rts[0].developerName;
                            System.debug('******c.recordtypeid is : ' + devname);               
                            
                            Case oldCase = null;
                            if (mapOldCases != null) {
                                // apparently Trigger.oldMap doesn't exist for inserts
                                oldCase = mapOldCases.get(c.Id);
                                System.debug('***************** The old value of case status is: ' + oldcase.status + ' and the new case status is ' + c.status);
                            }  
                            
                            System.debug('size of list is : ' + cases.size() );
                            String email = c.Contact.Email;
                            System.Debug('email is ' + email);
                            String uid = userinfo.getUserId(); 
                            String pid = userinfo.getProfileId();
                            String owner = c.OwnerId;
                            String ToAddress = c.IRCloud__Email_To__c; 
                                
                            //check that this is when moved into in-progress for the first time
                            if (c.Status == 'In-Progress' && oldCase.Status == 'New') {//The only time email will be sent is when it moves to 'In-Progress' for the first time.
    
                                if (email.contains('@salesforce.com') || email.contains('@exacttarget.com')) {
                    
                                    //Send from the security@email address
                                    OrgWideEmailAddress emailAddress = [
                                        SELECT Id
                                        FROM OrgWideEmailAddress
                                        WHERE Address = 'security@salesforce.com'
                                    ];
                    
                                //select the correct templates to send
                                    EmailTemplate acktemplate = [
                                        SELECT Id
                                        FROM EmailTemplate
                                        WHERE Name = 'CSIRT Inbound Ack' //replace this with the Ack template
                                    ];      
                                  
                                    //Add emails to the list of emails to be sent
                                    Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
                                    message.setOrgWideEmailAddressId(emailAddress.Id);
                                    message.setTargetObjectId(c.ContactId);
                                    message.setWhatId(c.Id);
                                    message.setTemplateId(acktemplate.Id);
                                    ackmessages.add(message);
                                }
                            }
                            System.debug('******c.Send_Response_Template__c : ' +c.Send_Response_Template__c);
                            System.debug('******oldCase.Send_Response_Template__c : ' +oldCase.Send_Response_Template__c);
                            if (c.Send_Response_Template__c != null && (c.Send_Response_Template__c != oldCase.Send_Response_Template__c)) {
                                System.debug('*******entered email loop' );
                                if (email.contains('@salesforce.com') || email.contains('@exacttarget.com')) {
                    
                                    //Send from the security@email address
                                    OrgWideEmailAddress emailAddress = [
                                        SELECT Id
                                        FROM OrgWideEmailAddress
                                        WHERE Address = :ToAddress
                                    ];
                    
                                //select the correct templates to send
                                    EmailTemplate useTemplate = [
                                        SELECT Id
                                        FROM EmailTemplate
                                        WHERE Name = :c.Send_Response_Template__c 
                                    ];      
                                  
                                    //Add emails to the list of emails to be sent
                                    Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
                                    message.setOrgWideEmailAddressId(emailAddress.Id);
                                    message.setTargetObjectId(c.ContactId);
                                    message.setWhatId(c.Id);
                                    message.setTemplateId(usetemplate.Id);
                                    templatemessages.add(message);
                            }
                        
                        }
        
                    }               
                }
        
                
                if (ackmessages.size()>0){ 
                    System.debug('******ackmessages size is : ' + ackmessages.size());
                    try{
                        Messaging.sendEmail(ackmessages);
                    } catch(exception e){
                        System.debug(e);
                    }
                    	 
                    
                }
                if(templatemessages.size()>0){
                    System.debug('******templatemessages size is : ' + templatemessages.size());
                    try{
                        Messaging.sendEmail(templatemessages);
                    } catch(exception e) {
                        System.debug(e);
                    }
   	
                }
            }
        }
    }    
}
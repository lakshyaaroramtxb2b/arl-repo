public class jsonPrettyControllerExtension {
    
    private Detection_Security_Event__c  secEvt;
    
     public jsonPrettyControllerExtension(ApexPages.StandardController stdController) {
        this.secEvt = (Detection_Security_Event__c)stdController.getRecord();
         
        this.secEvt = [SELECT Id, FieldMapRaw__c 
                       FROM Detection_Security_Event__c  
                   	   WHERE Id =: this.secEvt.Id];
    }
    
	public String getbuildTable() {
        
        String htmlStr = '<div style=""><ul>';
        
        JSONParser parser = JSON.createParser(this.secEvt.FieldMapRaw__c);
        
		while (parser.nextToken() != null) {  
            
           if ((parser.getCurrentToken() == JSONToken.FIELD_NAME || parser.getCurrentToken() == JSONToken.VALUE_STRING )) {  

               htmlStr += '<li>' + parser.getText() + '</li>';
               htmlStr += '<ul>';
               
               parser.nextToken();
               
               String searchUrl = 'https://security.my.salesforce.com/_ui/search/ui/UnifiedSearchResults?searchType=2&sen=a13&sen=a35&sen=a1r&sen=a2S&sen=a34&sen=a37&sen=a2u&sen=a36&sen=a16&sen=a1V&sen=00O&sen=a2c&sen=005&sen=500&sen=a1G&sen=a2G&sen=a0i&sen=a2N&sen=a2P&sen=a31&sen=a2O&str=';
               
               htmlStr += '<li><a target="_blank" href="' + searchUrl + parser.getText() + '">' + parser.getText() + '</a></li>';
               htmlStr += '</ul>';
               
           } 
            
      	}  
        
        htmlStr += '</ul></div>';

        return htmlStr;
    }
}
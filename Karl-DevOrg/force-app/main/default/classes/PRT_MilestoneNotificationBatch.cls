public class PRT_MilestoneNotificationBatch implements Database.Batchable<SObject>, Schedulable {
    @TestVisible
    private static List<Milestone_PRT__c> mockMilestoneList = new List<Milestone_PRT__c>();

    public Database.QueryLocator start(Database.BatchableContext BC){
        Date todaysDate = Date.today();
        Set<String> lifecycleStageValues = new Set<String>{'Project Kickoff', 'Planning', 'Monitoring', 'Executing'};
        String fields = 'Id, Name, Status__c, Due_Date__c, Start_Date__c, Project__c, Project__r.Status__c, Project__r.Project_Manager__c, Project__r.Sponsor_Internal__c, Project__r.Portfolio_Manager__c';
        String objectName = 'Milestone_PRT__c';
        String whereClause = 'Status__c = \'Not Started\' AND (Start_Date__c = NULL OR Start_Date__c < :todaysDate) AND Project__r.Status__c IN: lifecycleStageValues';
        String query = 'SELECT '+fields+' FROM '+objectName+' WHERE '+whereClause;
        System.debug('Query: '+query);
        return Database.getQueryLocator(query);
    }

    public void execute(Database.BatchableContext bc, List<SObject> milestoneList) {
        System.debug('Milestone List: '+milestoneList);
        System.debug('Milestone List: '+milestoneList.size());
        Date todaysDate = Date.today();
        List<ConnectApi.BatchInput> batchInputList = new List<ConnectApi.BatchInput>();

        for(Milestone_PRT__c milestone : (List<Milestone_PRT__c>)milestoneList) {
            Date startDate = milestone.Start_Date__c;
            Integer daysBetween;
            String action1 = '- If progress toward the milestone has been started, please update the Milestone Status accordingly.';
            String action2 = '- If progress toward the milestone is significantly delayed, please ensure that the delay is reflected in your weekly status report and a change request is initiated to address any issues.';
            String msg = 'Milestone '+milestone.Name+' currently has a Status of Not Started and the Start Date has passed or has not been established. Please take one of the following actions:\n\n'+action1+'\n'+action2;
            List<String> projManager = new List<String>();
            List<String> projSponsor = new List<String>();
            List<String> portfolioManager = new List<String>();

            if(milestone.Project__r.Project_Manager__c != null) {
                projManager.add(milestone.Project__r.Project_Manager__c);
            }
            if(milestone.Project__r.Sponsor_Internal__c != null) {
                projSponsor.add(milestone.Project__r.Sponsor_Internal__c);
            }
            if(milestone.Project__r.Portfolio_Manager__c != null) {
                portfolioManager.add(milestone.Project__r.Portfolio_Manager__c);
            }
            System.debug('Current Milestone Start Date: '+startDate);
            System.debug('Todays Date: '+todaysDate);
            if(startDate != null) {
                daysBetween = startDate.daysBetween(todaysDate);
                System.debug('Difference: '+daysBetween);
                if(daysBetween == 1) {
                    if(projManager.size() > 0)
                        batchInputList.add(chatterUser(msg, projManager, String.valueOf(milestone.Id)));
                    if(projSponsor.size() > 0)
                        batchInputList.add(chatterUser(msg, projSponsor, String.valueOf(milestone.Id)));
                }
                else if(daysBetween > 1){
                    if(Test.isRunningTest() || DateTime.now().format('E') == 'Fri') {
                        if(projManager.size() > 0)
                            batchInputList.add(chatterUser(msg, projManager, String.valueOf(milestone.Id)));
                        if(projSponsor.size() > 0)
                            batchInputList.add(chatterUser(msg, projSponsor, String.valueOf(milestone.Id)));
                        if(portfolioManager.size() > 0)
                            batchInputList.add(chatterUser(msg, portfolioManager, String.valueOf(milestone.Id)));
                    }
                }
                System.debug('Project Manager: '+projManager);
                System.debug('Project Sponsor: '+projSponsor);
                System.debug('Portfolio Manager: '+portfolioManager);
            }
            else {
                if(Test.isRunningTest() || DateTime.now().format('E') == 'Fri') {
                    if(projManager.size() > 0)
                        batchInputList.add(chatterUser(msg, projManager, String.valueOf(milestone.Id)));
                    if(projSponsor.size() > 0)
                        batchInputList.add(chatterUser(msg, projSponsor, String.valueOf(milestone.Id)));
                    if(portfolioManager.size() > 0)
                        batchInputList.add(chatterUser(msg, portfolioManager, String.valueOf(milestone.Id)));
                }
                System.debug('Project Manager: '+projManager);
                System.debug('Project Sponsor: '+projSponsor);
                System.debug('Portfolio Manager: '+portfolioManager);
            }
        }
        System.debug('Batch Input List: '+batchInputList);

        if(!Test.isRunningTest() && batchInputList!=null && !batchInputList.isEmpty()) {
            ConnectApi.ChatterFeeds.postFeedElementBatch(Network.getNetworkId(), batchInputList);
        }
    }

    public static ConnectApi.BatchInput chatterUser(String message, List<String> userId, String recordId) {
        ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
        ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
        ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
        ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();

        messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();

        mentionSegmentInput.id = userId.get(0);
        messageBodyInput.messageSegments.add(mentionSegmentInput);

        textSegmentInput.text = '\n'+message;
        messageBodyInput.messageSegments.add(textSegmentInput);

        feedItemInput.body = messageBodyInput;
        feedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
        feedItemInput.subjectId = recordId;
        
        System.debug('Feed Item Input: '+feedItemInput);

        ConnectApi.BatchInput batchInput = new ConnectApi.BatchInput(feedItemInput);

        return batchInput;
    }

    public void finish(Database.BatchableContext bc){
        
    } 

    public void execute(SchedulableContext SC) {
        Database.executebatch(new PRT_MilestoneNotificationBatch());
    }
}
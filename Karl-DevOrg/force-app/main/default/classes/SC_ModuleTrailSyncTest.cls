/*
 * Apex Tests for the ModuleTrailSync class used to sync Modules and Trails
 */

@isTest
public class SC_ModuleTrailSyncTest {

    static testMethod void testTrailBatch() {
        Test.setMock(HttpCalloutMock.class, new TrailResponseMock());

        Test.startTest();
        Database.executeBatch(new SC_TrailSyncBatch(0));
        Test.stopTest();

        List<Training_Course__c> courses = [
            select Provider__c, Sync_Id__c, Org_Wide_Email_Id__c
            from Training_Course__c
        ];

        System.assertEquals(1, courses.size(), 'Wrong number of Trails');
    }

    static testMethod void testModuleBatch() {
        Test.setMock(HttpCalloutMock.class, new ModuleResponseMock());

        Test.startTest();
        Database.executeBatch(new SC_ModuleSyncBatch(0));
        Test.stopTest();

        List<Training_Course__c> courses = [
            select Provider__c, Sync_Id__c
            from Training_Course__c
        ];

        System.assertEquals(1, courses.size(), 'Wrong number of Modules');
    }

    static testMethod void testRelationshipBatch() {
        Training_Course__c trial = new Training_Course__c(
            Sync_Id__c = 'trail_get-to-know-regulatory-compliance-at-salesforce',
            Name = 'Get to Know Regulatory Compliance at Salesforce',
            Is_Active__c = false,
            Course_Level__c = TrainingCourse_Constants.COURSE_LEVEL_TRAIL,
            Last_Import_Date__c = System.today(),
            Provider__c = 'Trailhead',
            Provider_ID__c = 'get-to-know-regulatory-compliance-at-salesforce',
            URL__c = 'https://developer.salesforce.com/trailhead/trail/get-to-know-regulatory-compliance-at-salesforce/'
        );
        insert trial;

        Training_Course__c module = new Training_Course__c(
          Sync_Id__c = 'module_tech-products-audit-basics',
          Name = 'Tech Products Audit Basics',
          Is_Active__c = false,
          Course_Level__c = 'Module',
          Last_Import_Date__c = System.today(),
          Provider__c = 'Trailhead',
          Provider_ID__c = 'tech-products-audit-basics',
          URL__c = 'https://developer.salesforce.com/trailhead/module/tech-products-audit-basics/'
        );
        insert module;

        Test.setMock(HttpCalloutMock.class, new TrailResponseMock());

        Test.startTest();
        Database.executeBatch(new SC_RelationshipSyncBatch(0));
        Test.stopTest();

        List<Training_Course_Relationship__c> relCourses = [
            select Id
            from Training_Course_Relationship__c
        ];
        System.assertEquals(1, relCourses.size(), 'Wrong number of relationships');
    }

    public class TrailResponseMock implements HttpCalloutMock {

        public HTTPResponse respond(HTTPRequest req) {
            HttpResponse res = new HttpResponse();

            res.setHeader('Content-Type', 'application/json');
            res.setBody(SC_TrailResponseTest.TRAIL_RESPONSE);
            res.setStatusCode(200);

            return res;
        }
    }

    public class ModuleResponseMock implements HttpCalloutMock {

        public HTTPResponse respond(HTTPRequest req) {
            HttpResponse res = new HttpResponse();

            res.setHeader('Content-Type', 'application/json');
            res.setBody(SC_ModuleResponseTest.MODULE_RESPONSE);
            res.setStatusCode(200);

            return res;
        }
    }
}
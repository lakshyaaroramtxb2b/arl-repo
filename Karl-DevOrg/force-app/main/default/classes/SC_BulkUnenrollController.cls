/**
 * @author: ralph@callaway.cloud
 */
public class SC_BulkUnenrollController {

    public Boolean excludeFutureEnrollment { get; set; }
    public String emailText { get; set; }
    public Training_Course__c course { get; set; }

    public SC_BulkUnenrollController(ApexPages.StandardController sc) {
        course = (Training_Course__c) sc.getRecord();
        excludeFutureEnrollment = true;
    }

    public void processEmails() {
        Set<String> uniqueEmails = getUniqueEmails(emailText);
        SC_Unenroll_Batch.Options opts = new SC_Unenroll_Batch.Options(course.Id, uniqueEmails);
        opts.excludeFutureEnrollments = this.excludeFutureEnrollment;
        SC_Unenroll_Batch batchUnenroller = new SC_Unenroll_Batch(opts);
        Database.executeBatch(batchUnenroller);
        ApexPages.addMessage(new ApexPages.Message(
            ApexPages.Severity.CONFIRM,
            'Unenrollment Started',
            'Processing ' + uniqueEmails.size() + ' unique emails for unenrollment. Check your email for results.'));
    }

    private Set<String> getUniqueEmails(String emailText) {
        System.assert(String.isNotBlank(emailText), 'email text is required');

        Set<String> result = new Set<String>();
        for (String e : SFDCStringUtils.bigSplit(emailText, '\n')) {
            if (String.isNotBlank(e)) {
                result.add(e.trim().toLowerCase());
            }
        }
        return result;
    }
}
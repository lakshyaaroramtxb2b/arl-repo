/**
* @File Name          : DLM_countDocumentsOnContactBatch.cls
* @Description        : This batch count all the documents related to Content_Custodian__c OR Content_Owner__c on 
*                       Is Document object and populate it to DLM doc# on contact object
* @Author             : Swarnima Singh Mandhata 2020 - 07 - 06
* @Group              : 
* @Last Modified By   : Swarnima Singh Mandhata
* @Last Modified On   : 2020 -07-08
* @Modification Log   : 
* Ver       Date            Author                 Modification
* 1.0    2020 - 07 - 08   Swarnima     Initial Version
**/

public class DLM_countDocumentsOnContactBatch implements Database.Batchable<sObject>, schedulable{
    Public static ID DLM_CONTACTRECORDTYPEID = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Salesforce Employee').getRecordTypeId();
    public Database.QueryLocator start(Database.BatchableContext BC){
        String query = 'SELECT id,Content_Custodian__c,Content_Owner__c,LastModifiedDate FROM IS_Document__c WHERE (Content_Custodian__c != null OR Content_Owner__c != null)'+ 
                        ' AND Content_Custodian__r.RecordTypeId = \'' + DLM_CONTACTRECORDTYPEID +'\'' +
                        ' AND Content_Owner__r.RecordTypeId = \'' + DLM_CONTACTRECORDTYPEID +'\'';
        return Database.getQueryLocator(query);
    }
    
    public void execute(Database.BatchableContext BC, List<sObject> sObjectList){
        Datetime tenMinutesAgo = Datetime.now().addMinutes(-10);
        
        List<IS_Document__c> isDocumentList = (List<IS_Document__c>) sObjectList;
        List<Contact> contactList = new List<Contact>();
        Map<Id,Integer> contactIdToCountMap = new Map<Id,Integer>();
        Set<Id> contactIdSet = new Set<Id>();
        for(IS_Document__c documentRecord : isDocumentList){
            if(documentRecord.Content_Owner__c !=null && documentRecord.Content_Custodian__c !=null && (documentRecord.Content_Owner__c == documentRecord.Content_Custodian__c)){
                if(!contactIdToCountMap.containsKey(documentRecord.Content_Owner__c)){
                    contactIdToCountMap.put(documentRecord.Content_Owner__c,1);
                }else{
                    contactIdToCountMap.put(documentRecord.Content_Owner__c,contactIdToCountMap.get(documentRecord.Content_Owner__c)+1);   
                }
            }
            if(documentRecord.Content_Owner__c != NULL && (documentRecord.Content_Owner__c != documentRecord.Content_Custodian__c)){
                if(!contactIdToCountMap.containsKey(documentRecord.Content_Owner__c)){
                    contactIdToCountMap.put(documentRecord.Content_Owner__c,1);
                }else{
                    contactIdToCountMap.put(documentRecord.Content_Owner__c,contactIdToCountMap.get(documentRecord.Content_Owner__c)+1);   
                } 
            }
            if(documentRecord.Content_Custodian__c != NULL && (documentRecord.Content_Owner__c != documentRecord.Content_Custodian__c)){
                if(!contactIdToCountMap.containsKey(documentRecord.Content_Custodian__c)){
                    contactIdToCountMap.put(documentRecord.Content_Custodian__c,1);
                }else{
                    contactIdToCountMap.put(documentRecord.Content_Custodian__c,contactIdToCountMap.get(documentRecord.Content_Custodian__c)+1);   
                } 
            }
        }
        for(Contact contactRecord :[SELECT Id, Name, DLM_Doc__c ,Org62_User_ID__c
                                    FROM Contact 
                                    WHERE Org62_User_ID__c != null AND Id In: contactIdToCountMap.keySet()]){
                                        if(contactIdToCountMap.containsKey(contactRecord.Id)){
                                            contactRecord.DLM_Doc__c = contactIdToCountMap.get(contactRecord.Id);
                                        }else{
                                            contactRecord.DLM_Doc__c = 0;
                                        }
                                        contactList.add(contactRecord);
                                    }
        system.debug('contactList-> '+ contactList);
        if(!contactList.isEmpty()){
            Update contactList;
        }
    }
    public void finish(Database.BatchableContext BC){
        DLM_countDocumentsOnContactBatch batch = new DLM_countDocumentsOnContactBatch();
        if(!Test.isRunningTest()){
            System.scheduleBatch(batch, 'DLM_countDocumentsOnContactBatch',9);
        }
    }
    public void execute(SchedulableContext SC) {
        DLM_countDocumentsOnContactBatch extecuteBatch = new DLM_countDocumentsOnContactBatch();
        database.executeBatch(extecuteBatch);
    }
    
}
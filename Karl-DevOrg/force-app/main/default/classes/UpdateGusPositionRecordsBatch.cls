global class UpdateGusPositionRecordsBatch implements Database.Batchable<sObject>, Database.Stateful {

    public String SOURCE_FILE = 'UpdateGusPositionRecordsBatch';
    public List<Headcount__c> lstHeadCounts = new List<Headcount__c>();
    
    public Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([
                                         SELECT Id
                                         FROM Headcount__c]);
    }

    public void execute(Database.BatchableContext bc, List<Headcount__c> lstSecHeadCounts) {
        List<Headcount__c> lstSecOrgHeadCounts = new List<Headcount__c>();
        String latestId;
        Set<Id> setheadCountIds = new Set<Id>();
        
        for(Headcount__c headCount : lstSecHeadCounts){
          setheadCountIds.add(headCount.Id);
        }

        lstHeadCounts = [SELECT Id,
                                Employee_Name_Text__c, 
                                Headcount_Name__r.Name__c,
                                Parent_Cloud__r.Name__c, 
                                Parent_Cloud_Text__c,
                                Hiring_Manager__r.Name__c, 
                                Hiring_Manager__r.FederationIdentifier__c,
                                Hiring_Manager_Text__c,
                                Hiring_Manager_Email__c,
                                Manager_Employee_Record__c,
                                Recruiter__r.Name__c, 
                                Recruiter_Text__c
                                //GUS_Position__r.Name__c
                        FROM Headcount__c
                        WHERE ID IN : setheadCountIds];
        Set<String> setManagerEmails = new Set<String>();
        for(Headcount__c headCount : lstHeadCounts) {
            setManagerEmails.add(headCount.Hiring_Manager_Email__c);
            setManagerEmails.add(headCount.Hiring_Manager__r.FederationIdentifier__c);
        }
        List<INTEG_Employee__c> lstEmployees = [SELECT ID,
                                                      Integ_Email__c,
                                                      Federation_Identifier__c
                                               FROM INTEG_Employee__c 
                                               WHERE Integ_Email__c IN : setManagerEmails];

        Map<String,String> mapManagerEmpRecords = new Map<String,String>();
        for(INTEG_Employee__c employee : lstEmployees){
            mapManagerEmpRecords.put(employee.Integ_Email__c, employee.Id);
            mapManagerEmpRecords.put(employee.Federation_Identifier__c, employee.Id);
        }
        for(Headcount__c headCount : lstHeadCounts) {
            headCount.Employee_Name_Text__c = headCount.Headcount_Name__r.Name__c;
            headCount.Parent_Cloud_Text__c = headCount.Parent_Cloud__r.Name__c;
            headCount.Hiring_Manager_Text__c = headCount.Hiring_Manager__r.Name__c;
            headCount.Recruiter_Text__c = headCount.Recruiter__r.Name__c;
            if(mapManagerEmpRecords.get(headCount.Hiring_Manager_Email__c) != null){
                headCount.Manager_Employee_Record__c = mapManagerEmpRecords.get(headCount.Hiring_Manager_Email__c);    
            }else if(mapManagerEmpRecords.get(headCount.Hiring_Manager__r.FederationIdentifier__c) != null) {
                headCount.Manager_Employee_Record__c = mapManagerEmpRecords.get(headCount.Hiring_Manager__r.FederationIdentifier__c);
            }
            
            /*if(headcount.GUS_Position__r.Name__c == null){
               headcount.GUS_Position__c = null;
            }*/
        } 

        List<Database.SaveResult> saveResults = Database.Update(lstHeadCounts, false);
                
        Integer numberOfInsertedPositions = 0;
        Integer numberOfUpdatedPositions = 0;
        String finalErrorText = '';
        for (Database.SaveResult sr : saveResults) {
            if (!sr.isSuccess()) 
                for(Database.Error err : sr.getErrors()) {
                            finalErrorText += err.getStatusCode() + ': ' + err.getMessage() + '\n';
                }
        }
        
        Esa_DebugService.writeMessage('UpdateGusPositionRecords Completed:');
        if(string.isNotBlank(finalErrorText)) {
            system.debug('##########'+finalErrorText);
            Esa_DebugService.writeMessage('UpdateGusPositionRecords.finish: Failed with errors');
            Exception ex;
            Esa_DebugService.writeException(ex, 1, SOURCE_FILE, finalErrorText);
        }
    }
    

    public void finish(Database.BatchableContext BC) {
         Esa_Script_Gus_PositionsObject.deleteGusDeletedPositons();
    }

}
global with sharing class RetrieveNextUtils {
    webService static Id retrieveNextCase(String userId)
    {
        //Really we're only specifying the user ID for the sake of the test methods
        if (userId=='') {
            //Use the currently running user
            userId = UserInfo.getUserId();
        }
        
        //First find out which queues this user is a member of
        List<Id> listGroupIds = getQueuesForUser(userId);
        
        if(listGroupIds.size()>0) 
        {
            //Find an open case that is assigned to one of those queues
            
            
            Case[] caseObj = [select c.Id from Case c where 
                                                        c.IsClosed=false 
                                                        and c.OwnerId in :listGroupIds
                                                        and c.RecordTypeId in ('01230000001GdDR','01230000001GZBb','01230000001GgU1')
                                                        ORDER BY c.CaseNumber ASC
                                                        limit 1];
                                                
            if (caseObj.size() > 0) {
                 Case[] caseChg = [select c.Id,c.OwnerId from Case c where 
                                                        c.Id=:caseObj[0].Id
                                                        for update];        
                //If we found one, assign it to the current user
                caseChg[0].OwnerId = userId;
                caseChg[0].Status = 'In-Progress';
                update caseChg[0];
                
                return caseChg[0].Id;
            }
        }
        
        return null;
    }
    
    //Returns a list of ids of queues that this user is a member of
    public static List<Id> getQueuesForUser(String userId) 
    {
        List<Id> listGroupIds = new List<Id>();
        Set<Id> userGroups = getGroupsForUser(userId);
        for (Id grpId:userGroups) {
            List<GroupMember> listGroupMembers = [Select g.GroupId From GroupMember g 
                                                    where g.Group.Type='Queue'
                                                    and g.UserOrGroupId=:grpId
                                                    and g.Group.Id!='00G3000000403qn'];
                                                
            if (listGroupMembers!=null && listGroupMembers.size()>0) {      
                for (GroupMember gm:listGroupMembers) {
                    listGroupIds.add(gm.GroupId);
                }
            }
        }
        return listGroupIds;
    }
    
public static Set<Id> getGroupsForUser(Id userId){

//Declaring a Set as we don't want Duplicate Group Ids
Set<Id> results = new Set<Id>();

//Groups directly associated to user
Set<Id> groupwithUser = new Set<Id>();

//Populating the Group with User with GroupId we are filtering only for Group of Type Regular,Role and RoleAndSubordinates
for(GroupMember  u :[select groupId from GroupMember where UserOrGroupId=:userId and Group.Type = 'Regular'])
{
    groupwithUser.add(u.groupId);
}

//Combining both the Set
results.addAll(groupwithUser);

//Traversing the whole list of Groups to check any other nested Group
Map<Id,Id> grMap = new Map<Id,Id>();
for(GroupMember gr : [select id,UserOrGroupId,Groupid from GroupMember where Group.Type = 'Regular'])
{
    grMap.put(gr.UserOrGroupId,gr.Groupid);
}
for(Id i :results)
{
    if(grMap.containsKey(i))
    {
        results.add(grMap.get(i));
    }
}

results.remove('00G3000000403qn');
return results;
}
    
    public static Group createTestGroup()
    {
        Group g = new Group(Type='Queue',Name='testRetrieveNextCase');
        insert g;
        
        //Make this queue assignable to leads and cases
        List<QueueSobject> qs = new List<QueueSobject>();
        qs.add(new QueueSobject(QueueId=g.Id,SObjectType='Case'));
        qs.add(new QueueSobject(QueueId=g.Id,SObjectType='Lead'));        
        insert qs;
        
        return g;
    }
    
    static User createTestUser() {
        User user = new User();
        user.Username = 'test'+System.currentTimeMillis()+'@RetrieveNextUtils.com';
        user.LastName = 'LastTestName';
        user.Email = 'test@RetrieveNextUtils.com';
        user.alias = 'testAl';
        user.TimeZoneSidKey = 'America/New_York';
        user.LocaleSidKey = 'en_US';
        user.EmailEncodingKey = 'ISO-8859-1';
        user.ProfileId = [select id from Profile where Name='System Administrator'].Id;
        user.LanguageLocaleKey = 'en_US';
        insert user;
        //setUser(user);
        
        return user;
     }
    
    public static testMethod void testRetrieveNextCase()
    {    
      User u = createTestUser();
       
        Group g = createTestGroup();
        
        GroupMember gm = new GroupMember(UserOrGroupId=u.Id,GroupId=g.Id);
        insert gm;
        
        Test.startTest();  
        //We have to runAs so that we don't get a MIXED_DML_EXCEPTION
        System.runAs(u) {
          Case c = new Case(Subject='Test',OwnerId=g.Id);
          insert c;
          
          Id caseId = retrieveNextCase(u.Id);
          System.assertEquals(caseId,c.Id);
          
          Case ownedCase = [select OwnerId from Case where Id=:c.Id];
          System.assertEquals(ownedCase.OwnerId,u.Id);
        }
    }
        
    
    public static testMethod void testNegativeRetrieveNextCase()
    {    
      User u = createTestUser();
               
        Group g = createTestGroup();
        
        Test.startTest();  
        
        //We have to runAs so that we don't get a MIXED_DML_EXCEPTION
        System.runAs(u) {
          
          //Do not insert this user in the queue -- he should not get the case
          Case c = new Case(Subject='Test',OwnerId=g.Id);
          insert c;
          
          Id caseId = retrieveNextCase(u.Id);
          System.assertEquals(caseId,null);
          
          Case ownedCase = [select OwnerId from Case where Id=:c.Id];
          System.assertNotEquals(ownedCase.OwnerId,u.Id);
        }
    }
}
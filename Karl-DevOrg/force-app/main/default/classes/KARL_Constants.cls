public inherited sharing class KARL_Constants {
    
    //Auditor ARL Item
    public static final String KARL_AUDITOR_STATUS_FOLLOWUPS = 'Follow-ups';
    public static final String FOLLOWUPTYPE_EVIDENCEQUESTION = 'Evidence Question';
    public static final String FOLLOWUPSTATUS_NEW = 'New';

    //KARL Scope Picklist values present on Audit Scope
    public static final String KARL_SCOPE_ADVERTISING_STUDIO = 'AS';

    //Evidence Request Status
    public static String EVIDENCE_STATUS_ReturnToEngineer = 'Returned to Engineer';
    public static String EVIDENCE_STATUS_ProviderToAuditor = 'Provided to Auditor';
    public static String EVIDENCE_STATUS_Pending_Engineer = 'Pending Engineer';

    //Task status
    public static String TASK_STATUS_OPEN = 'Open';
    public static String TASK_STATUS_COMPLETED = 'Completed';

    //GUS Work Status 
    public static String WORK_STATUS_CLOSED = 'Closed';
    public static String WORK_STATUS_IN_PROGRESS = 'In Progress';
    
    // Audit Cycle Report
    public static String REPORT_STATUS_PENDING = 'Pending';
    public static String REPORT_STATUS_READY_TO_SIGN = 'Ready to Sign';
    public static String REPORT_STATUS_In_Progress_With_Docusign = 'In Progress with Docusign';
    public static String REPORT_STATUS_Docusign_COMPLETED = 'DocuSign Completed';
    public static String REPORT_STATUS_ISSUED = 'Issued';
    public static String REPORT_STATUS_PUBLISHED = 'Published';
    public static String REPORT_STATUS_REQUEST_ISSUANCE = 'Request Issuance';
    public static String REPORT_STATUS_FINAL_REVIEW = 'Final Review';

    // KARL_Service_Desk Record Type Ids
    public static String EVIDENCE_REQUEST_RECORDTYPEID = Schema.SObjectType.KARL_Service_Desk__c.getRecordTypeInfosByDeveloperName().get('Evidence_Request').getRecordTypeId();
    public static String REQUEST_MASTER_DATA_RECORDTYPEID = Schema.SObjectType.KARL_Service_Desk__c.getRecordTypeInfosByDeveloperName().get('Request_Master_Data').getRecordTypeId();

    //Profile Name
    public static String PROFILE_ESA_CCP = 'ESA CCP';

    //Service Desk PICKLIST Values
    public static String STATUS_SUBMIT_FOR_APPROVAL = 'Submitted for Approval';
    public static String STATUS_DRAFT = 'Draft';
    public static String STATUS_APPROVED = 'Approved';
    public static String STATUS_REJECTED = 'Rejected';
    public static String NEWOREXISTING_NEW = 'New';
    public static String NEWOREXISTING_EXISTING = 'Existing';
    public static String TYPE_INQUIRY = 'Inquiry';
    public static String AOC_SOC1 = 'SOC 1';
    public static String AOC_SOC2 = 'SOC 2';


    // Grouped Areas of Compliance
    public static Map<String, String> groupedAreaMap = new Map<String, String>{
        'SOC 1' => 'SOC',
        'SOC 2' => 'SOC',
        'SOC 3' => 'SOC',
        'FedRAMP Moderate' => 'FedRAMP',
        'FedRAMP High' => 'FedRAMP',
        'iRAP Official: Sensitive' => 'iRAP',
        'iRAP Official: Protected' => 'iRAP'
    };
    
    // Grouped Areas of Compliance for Querying Purposes
    public static Map<String, List<String>> aocQueryMap = new Map<String, List<String>>{
        'SOC' => new List<String> {'SOC 1','SOC 2','SOC 3'},
        'FedRAMP' => new List<String> {'FedRAMP Moderate','FedRAMP High'},
        'ICT' => new List<String> {'SFDC ICT'},
        'ICTUnique' => new List<String> {'SFDC ICT'},
        'ICTExclude' => new List<String> {'SFDC ICT'},
        'iRAP' => new List<String> {'iRAP','iRAP Official: Sensitive','iRAP Official: Protected'},
        'ISO_All' => new List<String> {'ISO 27001','ISO 27017','ISO 27018','NEN7510','ASIP Sante HDS'}
    };
}
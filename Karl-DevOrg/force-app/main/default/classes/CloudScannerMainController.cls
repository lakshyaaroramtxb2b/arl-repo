public without sharing class CloudScannerMainController {


    public String Scanorgid { get; set; }
    public String Scanprofile { get; set; }
    public String searchterm { get; set; }
    public String username { get; set; }
    public list<CloudScanWrapper> Scans {get;set;}
    public boolean ScansFound {get;set;}
    public integer PendingScans {get;set;}
    public integer CompletedScans {get;set;}
    public integer RemainingCredits {get;set;}
    public boolean ShowScannerNews {get;set;}
    public string ScannerNewsText {get;set;}
    private map <string,CloudScanWrapper> csw;
    private map<Scan_Queue__c,string> ScanQueueInfoMap;
    private Id AccountId;
    private static string salt='BaSsOfsso5gBUupELXUsiiuIJqy3OMuKMqP0qNbh';
    private List <Scan_Queue__c> ScanQueue;
    private List <CodeScan__c> CodeScans;
    private map<string,String> OrgIDKindMap;
    private map<id,string> CodeScanScanQueue;
    
    private map<id,string> ScanInfoPDFMap;
    private map<id,string> ScanInfoXMLMap;
    public CloudScannerMainController()
    {
        init();
    }
    
    public void init()
    {
       
       
        //system.debug([SELECT Id FROM ESA_Security_Request__c WHERE Requestor__c =:userid AND ESA_Service_Item__r.ItemCategory__r.ESA_Entity__r.EntityCode__c= :'partner' AND CreatedDate = LAST_N_DAYS:30
                                        //   AND (Status__c = 'Incomplete' OR (Status__c = 'In Progress' AND DAY_ONLY(Office_Hours_Reservation__c) >= :anchorDate))
                                    //       order BY CreatedDate desc LIMIT 1]);
        
        if(chimera__GeneralPortalSettings__c.getInstance('ScannerNewsDisabled')==null || chimera__GeneralPortalSettings__c.getInstance('ScannerNewsDisabled').chimera__Value__c=='Yes')
            ShowScannerNews=false;
        else
            ShowScannerNews=true;
        ScannerNewsText=chimera__GeneralPortalSettings__c.getInstance('ScannerNewsText')!=null?chimera__GeneralPortalSettings__c.getInstance('ScannerNewsText').chimera__Value__c:'';
        ScansFound=false;
        GetAccountDetails();
        getOrgIds();
        csw=new map <string,CloudScanWrapper>();
        ScanQueueInfoMap=new map<Scan_Queue__c,string>();
        
        ScanQueue=new List<Scan_Queue__c>();
        
        ScanInfoPDFMap=new map<id,string>();
        ScanInfoXMLMap=new map<id,string>();
        system.debug('orgid:'+OrgIDKindMap);
        for(Scan_Queue__c cq:[SELECT id,  Scan_Info__c,Comments__c, Status__c, CodeScan__r.id,Scan_Info_Status__c FROM Scan_Queue__c where CodeScan__r.OrgId__c IN :OrgIDKindMap.keyset() AND CreatedDate > 2016-01-01T00:00:00Z ])
        {
            if(cq.Scan_Info__c==null)
                cq.Scan_Info__c='01pQ00000000000';//hacks - Invalid Id
            ScanQueueInfoMap.put(cq,cq.Scan_Info__c);
            ScanQueue.add(cq);
        }
        system.debug('ScanQueueInfoMap:'+ScanQueueInfoMap);
        for(Attachment at:[select id,Name,parentid from Attachment where parentId IN :ScanQueueInfoMap.values()])
        {
            if(at.Name.contains('.pdf') ||  at.Name.contains('.html.zip'))//.html.zip
                ScanInfoPDFMap.put(at.parentid,at.id);
            if(at.Name.contains('.xml') || at.Name.contains('.xml.zip'))//.xml.zip
                ScanInfoXMLMap.put(at.parentid,at.id);
        }
        //Salesforce cannot check null in VF page https://success.salesforce.com/issues_view?id=a1p30000000T3k2AAC
        //Need to do some hacks
        for(Scan_Queue__c queue:ScanQueueInfoMap.keyset())
        {
            CloudScanWrapper cswtemp=new CloudScanWrapper();
            cswtemp.CodeScanId=queue.CodeScan__r.id;
            cswtemp.ScanQueueId=queue.id;
            cswtemp.SIStatus=queue.Scan_Info_Status__c;
            cswtemp.SQComments=queue.Comments__c;
            cswtemp.SQstatus=queue.Status__c;
            if(ScanInfoPDFMap.get(ScanQueueInfoMap.get(queue))!=null)
                cswtemp.pdfid=ScanInfoPDFMap.get(ScanQueueInfoMap.get(queue));
            else
                cswtemp.pdfid='NONE';
            if(ScanInfoXMLMap.get(ScanQueueInfoMap.get(queue))!=null)
                cswtemp.xmlid=ScanInfoXMLMap.get(ScanQueueInfoMap.get(queue));
            else
                cswtemp.xmlid='NONE';
            csw.put(''+queue.CodeScan__r.id+':'+queue.id,cswtemp);
        }
        //for(CodeScan__c cs:[select id, Username__c, CreatedDate, OrgID__c, WorkState__c,Comments__c from CodeScan__c where OrgId__c IN :OrgIDKindMap.keyset() AND CreatedDate > 2016-01-01T00:00:00Z ORDER BY CreatedDate ASC] )
        for(CodeScan__c cs:[select id, Username__c, CreatedDate, OrgID__c, WorkState__c,Comments__c from CodeScan__c where OrgId__c IN :OrgIDKindMap.keyset() ORDER BY CreatedDate ASC] )
        {
            boolean isFound=false;
            for(String nm:csw.keyset())
            {
                if(nm.contains(cs.id))
                {
                    csw.get(nm).status=cs.WorkState__c;
                    csw.get(nm).username=cs.Username__c;
                    csw.get(nm).createddate=cs.CreatedDate;
                    csw.get(nm).OrgID=cs.OrgID__c;
                    csw.get(nm).CSComments=cs.Comments__c;
                    csw.get(nm).status=cs.WorkState__c;
                    csw.get(nm).CodeScanId=cs.id;
                    isFound=true;
                }
            }
            if(isFound==false)
            {
                //Get only code scans after 2016
               if(cs.CreatedDate.year()>=2018)
                {
                    CloudScanWrapper cswtemp=new CloudScanWrapper();
                    cswtemp.status=cs.WorkState__c;
                    cswtemp.username=cs.Username__c;
                    cswtemp.createddate=cs.CreatedDate;
                    cswtemp.OrgID=cs.OrgID__c;
                    cswtemp.CSComments=cs.Comments__c;
                    cswtemp.status=cs.WorkState__c;
                    cswtemp.CodeScanId=cs.id;
                    csw.put(''+cs.id,cswtemp);
                }
            }
        }
        for(string cs:csw.keyset())
            csw.get(cs).SetStatus();
        CountScans(csw);
        Scans=SortScans(csw);
        if(Scans.size()>0)
            ScansFound=true;
    }
    
    public class CloudScanWrapper{
    public string CodeScanId {get;set;}
    public string ScanQueueId {get;set;}
    public string username {get;set;}
    public string status {get;set;}
    public string SIStatus {get;set;}
    public string SQStatus {get;set;}
    public string ExtStatus {get;set;}
    public string SQComments {get;set;}
    public string CSComments {get;set;}
    public string Comments {get;set;}
    public string OrgID {get;set;}
    public datetime createddate {get;set;}
    public string pdfid {get;set;}
    public string xmlid {get;set;}
    //public string csrfpdfid {get;set;}
    //public string csrfxmlid;
    public CloudScanWrapper()
        {
            pdfid='NONE';
            xmlid='NONE';
        }
        public string getcsrfxmlid()
        {
            return EncodingUtil.urlEncode(genCSRF(xmlid),'UTF-8');
        }
         public string getcsrfpdfid()
        {
            return EncodingUtil.urlEncode(genCSRF(pdfid),'UTF-8');
        }
        /*
        "job requested" <-- Code scan status is everything other than "never" or "download completed"
        "job accepted -- waiting to scan" <--CodeScan status is "download completed"
        "job rejected" <-- Code Scan status is "never", in which case you may want to display the comment field.
        
        "scanning" <-- SQ is Waiting for scan to finish
        "Scan completed. Waiting for report generation" SQ is done 
        "failed" <-- SQ is failed you may want to display the scan queue comment field. to explain the failure.
        "paused" SQ is paused
        
        if(SQ is done)
        {
            
            'Scan completed. Waiting for report generation'  =>  If Scan_Info.Status = Null | 'New'
            'Done'  == >  If Scan_Info.Status = 'Done'
            'Scan completed. Report Generation Failed' ==> If Scan_Info.Status = 'Failed'
            'Scan completed. Report Generation in Process' ==> For other values of Scan_Info.Status
        }
        */
        public void SetStatus()
        {
            ExtStatus='';
            if(status!=null)
            {
                if(status=='Never')
                   {
                       ExtStatus='Job Rejected';
                       Comments=CSComments;
                   }
                if(status=='Download Completed')
                    ExtStatus='Job Accepted - Waiting To Scan';
            }
            if(SQstatus!=null)
            {
                if(SQstatus=='Waiting for scan to finish')
                    ExtStatus='Scanning';
                if(SQstatus=='Done')
                    ExtStatus='Scan completed. Waiting for report generation';
                if(SQstatus=='Failed')
                    {
                        ExtStatus='Failed';
                        Comments=SQComments;
                    }
                if(SQstatus=='Paused')
                    ExtStatus='Paused';
            }
            if(SQstatus=='Done')
            {
                if(SIStatus=='')
                    ExtStatus='Scan completed. Waiting for report generation';
                else if(SIStatus=='Done')
                    ExtStatus='Done';
                else if(SIStatus=='Failed')
                    ExtStatus='Scan completed. Report Generation Failed';
                else
                    ExtStatus='Scan completed. Report Generation in Process';
            }
            if(ExtStatus=='')
                ExtStatus='Job Requested';
        }
    
    }
    public void CountScans(map<string,CloudScanWrapper> Scans)
    {
        PendingScans=0;
        CompletedScans=0;
        for(string str:Scans.keyset())
        {
            if(Scans.get(str).Extstatus=='Done' || Scans.get(str).Extstatus=='Job Rejected' || Scans.get(str).Extstatus=='Failed' )
                CompletedScans++;
            else 
                PendingScans++;
        }
    
    }
    
    public list<CloudScanWrapper> SortScans(map<string,CloudScanWrapper> UnsortedScan)
    {
        //return new map<string,CloudScanWrapper>();
        // behold the Vinay's Sort!! *everyone claps* *claps* *claps* *claps* *claps* *claps* (child crying in background)
        list<CloudScanWrapper> SortedScan=new list<CloudScanWrapper>();
        map<string,CloudScanWrapper> TempScan=UnsortedScan.clone();
        while(TempScan.size()>0)
        {
            string leastname=null; //more like highestname .. too lazy to change name
            for(String str:TempScan.keyset())
            {
                if(leastname==null || TempScan.get(str).createddate.getTime()>TempScan.get(leastname).createddate.getTime())
                    leastname=str;
            }
            SortedScan.add(UnsortedScan.get(leastname));
            TempScan.remove(leastname);
        }
        return SortedScan;
    }
    public List<SelectOption> getOrgIds() 
    {
            //Old code - Remove later
            /*OrgIDKindMap=new map<id,string>();
            List<SelectOption> options = new List<SelectOption>();
            Id userId = UserInfo.getUserId();
            Id AccountId=null;
            List<User> users = [select AccountId from User where Id=:userId];
            if(users[0].accountid==null)
            {
                AccountId='001Q000000uuiXV';//001Q000000uuiXV
            }
            else
                AccountId=users[0].accountid;
            if(AccountId!=null)
            {
                Account acc=[select Name from Account where id=:AccountId];
                SelectOption option = new SelectOption(acc.Name,Acc.Name+' (Publisher Org)');
                OrgIDKindMap.put(acc.Name,Acc.Name+' (Publisher Org)');
                options.add(option);
                
                for(Contributing_Org__c co:[select Org_Id__c from Contributing_Org__c where Account__c=:AccountId])
                {
                      option = new SelectOption(co.Org_Id__c,co.Org_Id__c+' (Contributing Org)');
                      OrgIDKindMap.put(co.Org_Id__c,co.Org_Id__c+' (Contributing Org)');
                      options.add(option);
                      
                } 
            }*/
            
            OrgIDKindMap=new map<string,string>();
            List<SelectOption> options = new List<SelectOption>();
            if(AccountId!=null)
            {
                //Account acc=[select Name from Account where id=:AccountId];
                SelectOption option;
                //OrgIDKindMap.put(acc.Name,Acc.Name+' (Publisher Org)');
                //options.add(option);
                
                for(Contributing_Org__c co:[select Org_Id__c, Publisher_Org__c, Credits__c from Contributing_Org__c where Account__c=:AccountId ORDER BY Publisher_Org__c DESC])
                {
                      string OrgType='';
                      string orgId=String.valueOf(co.Org_Id__c).substring(0, 15);
                      if(co.Publisher_Org__c)
                          OrgType='';
                      else
                          OrgType='';
                      Orgtype+=' - '+co.Credits__c+' Scans Available';
                      option = new SelectOption(orgId,orgId+OrgType);
                      OrgIDKindMap.put(orgId,orgId+OrgType);
                      options.add(option);
                      
                }
            }
        
       
        return options;
    }
    public List<SelectOption> getScanProfiles() 
    {
        List<SelectOption> options = new List<SelectOption>();
        SelectOption option = new SelectOption('PortalAll','Security and Quality Rules');
        options.add(option);
        option = new SelectOption('PortalSecurity','Security Rules');
        options.add(option);
        option = new SelectOption('PortalQuality','Quality Rules');
        options.add(option);
        return options;
    }
    public PageReference SubmitScan() {
        Contributing_Org__c FoundCO=null;
        Id char18ScanOrgid=Scanorgid;
        system.debug('18 char Scan org id='+char18ScanOrgid);
        if(OrgIDKindMap.get(Scanorgid)!=null)
        {
            for(Contributing_Org__c co:[select id,Credits__c from Contributing_Org__c where Org_Id__c=:char18ScanOrgid])
                FoundCO=co;
        }
        if(FoundCO==null)
            {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Invalid Request'));
                return null;
            }
        if(FoundCO.Credits__c<1)
            {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'You do not have enough credits to perform this scan'));
                return null;
            }
        CodeScan__c cs=new CodeScan__c();
        cs.Name='CloudScan-'+Scanorgid;
        cs.X62OrgUsername__c= UserInfo.getUserEmail();
        cs.OrgID__c=Scanorgid;
        cs.Username__c=username;
        cs.Account_Type__c='SecurityReview';
        cs.ScanType__c=Scanprofile;
        cs.Contributing_Org__c=FoundCO.id;
        cs.Job_Type__c='PartnerPortal';
        insert cs;
        //Update of credits will happen after scan is completed
        //FoundCO.Credits__c--;
        //update FoundCO;
        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.CONFIRM,'Scan has been submitted! Job ID: '+cs.id));
        //W-6165132 fix
        //init();
        //
        //
        PageReference pageRef = new PageReference(ApexPages.currentPage().getUrl());
		pageRef.setRedirect(true);
		return pageRef;
        //return null;
    }
    public void GetAccountDetails()
    {
        Id userId = UserInfo.getUserId();
        List<User> users = [select AccountId, Account.Total_Popcrab_Portal_Credits__c from User where Id=:userId];
        if(users[0].accountid==null)
        {
            AccountId='001Q000000uuiXV';//001Q000000uuiXV
            RemainingCredits=45;
        }
        else
        {
            AccountId=users[0].accountid;
            RemainingCredits=Integer.Valueof(users[0].Account.Total_Popcrab_Portal_Credits__c);
        }
    }
    public PageReference Search() {
        ScansFound=false;
        map<string,CloudScanWrapper> TempScan=new map<string,CloudScanWrapper>();
        for(string str:csw.keyset())
        {
            if(csw.get(str).CodeScanId==searchterm ||  csw.get(str).OrgID==searchterm || csw.get(str).username==searchterm)
                TempScan.put(str,csw.get(str));
        }
        if(searchterm=='')
             TempScan=csw.clone();
        Scans=SortScans(TempScan);
        if(Scans.size()>0)
            ScansFound=true;
        return null;
    }
    public PageReference reset() {
        init();
        searchterm='';
        return null;
    }
    public static string genCSRF(string attid)
    {
        return EncodingUtil.base64Encode(Crypto.generateMac('hmacSHA512', Blob.valueOf(Userinfo.getSessionId()), Blob.valueOf(salt+attid)));
    }
}
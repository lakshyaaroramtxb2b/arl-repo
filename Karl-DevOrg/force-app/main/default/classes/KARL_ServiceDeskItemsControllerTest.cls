/**
* @author Lakshya Arora
* @email lakshya.arora@mtxb2b.com
* @description Class for unit testing the KARL_ServiceDeskItemsController
*/
@isTest
public with sharing class KARL_ServiceDeskItemsControllerTest {
    /**
* @author Lakshya Arora
* @email lakshya.arora@mtxb2b.com
* @description Unit test the getSubmittedServiceDeskInfo functionality
*/
    @isTest
    private static void getSubmittedServiceDeskInfoTest(){
        User opsAdminUser = ARL_TestDataFactory.createOpsAdmin();
        System.runAs(opsAdminUser){
            Account acc = ARL_TestDataFactory.createAccount('Salesforce - ESA Office Hours');
            insert acc;
            Contact con = ARL_TestDataFactory.createContact(acc.Id, 'test community user name', 'comm123@invalid.com');
            insert con;
            String REQUEST_MASTER_DATA_RECORDTYPEID = KARL_Constants.REQUEST_MASTER_DATA_RECORDTYPEID;
            List<KARL_Service_Desk__c> karlServiceDeskList = new List<KARL_Service_Desk__c>();
            for(Integer i=1;i<=6;i++){
                KARL_Service_Desk__c serviceDeskRecord = ARL_TestDataFactory.createKARLServiceDesk(REQUEST_MASTER_DATA_RECORDTYPEID);
                serviceDeskRecord.Requested_By__c = con.Id;
                karlServiceDeskList.add(serviceDeskRecord);
            }
            Test.startTest();
            insert karlServiceDeskList;
            Test.stopTest();
            KARL_ServiceDeskItemsController.testContactId = con.Id;
            System.assert(!KARL_ServiceDeskItemsController.getSubmittedServiceDeskInfo().isEmpty());
        }
    }
}
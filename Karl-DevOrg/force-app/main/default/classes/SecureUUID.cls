public class SecureUUID {

    private static Map < String, String > ba98 = new Map < String, String > ();
    static{
      ba98.put('0', '8');
      ba98.put('1', '9');
      ba98.put('2', 'a');
      ba98.put('3', 'b');
      ba98.put('4', '8');
      ba98.put('5', '9');
      ba98.put('6', 'a');
      ba98.put('7', 'b');
      ba98.put('8', '8');
      ba98.put('9', '9');
      ba98.put('a', 'a');
      ba98.put('b', 'b');
      ba98.put('c', '8');
      ba98.put('d', '9');
      ba98.put('e', 'a');
      ba98.put('f', 'b');
    }

    public static String NewV4() {
        String rand = EncodingUtil.convertToHex(Crypto.generateAesKey(128)).toLowerCase();
        String y = ba98.get(rand.subString(16,17));

        return rand.subString(0,8)
          + '-' + rand.subString(8,12)
          + '-4' + rand.subString(13,16)
          + '-' + y + rand.subString(17,20)
          + '-' + rand.subString(20);
    }

}
/**
 * @author Ben Harvie
 * @email ben.harvie@salesforce.com
 * @description Test class for KARL_ControlTestTrigger
*/
@isTest
public with sharing class KARL_ControlTestTriggerHelperTest {
    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Test data setup.
    */
    @testSetup
    private static void testSetup(){
        User ospAdmin = ARL_TestDataFactory.createNamedOpsAdmin('TestSetup', 'User', 'testsetup@example.com');
        
        System.runAs(ospAdmin){
            Control__c controlItem = ARL_TestDataFactory.createControl();
            insert controlItem;

            Control_Scope__c controlScope = ARL_TestDataFactory.createMappedControlScope(controlItem.Id);
            insert controlScope;

            Control_Test__c controlTest = ARL_TestDataFactory.createControlTest(controlScope.id);
            insert controlTest;
        }
    }
    
    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description In this test, the test is updating the control scope names match
    */
	@istest
    public static void checkNames(){ 
        User u = ARL_TestDataFactory.createOpsAdmin();
        String controlIdentifier = '';

        System.runAs(u) {
            Control_Test__c ctItem = [SELECT Id,Control_Scope__r.KARL_Triggered_Scope_Name__c,KARL_Control_Scope_Number__c,
                                        Test_Name__c,KARL_Control_Scope_Name_Indexed__c, KARL_Control_Number_Indexed__c,
                                        KARL_CT_Scope__c, Control_Scope__r.Control_Scope_Control_Number__c,
                                        KARL_Control_Scope_Number_Indexed__c, Control_Scope__r.Scope_Name__c,
                                        Control_Test_Control_Number__c,Control_Scope__r.Modified_Control_Identifier__c 
                                        FROM Control_Test__c WITH SECURITY_ENFORCED];
  		    
            // Set controlIdentifier based on criteria
            if(!String.isBlank(ctItem.Control_Scope__r.Modified_Control_Identifier__c)){
                controlIdentifier = ctItem.Control_Scope__r.Modified_Control_Identifier__c;
            }else{
                controlIdentifier = ctItem.Control_Test_Control_Number__c + '-' + ctItem.Control_Scope__r.Scope_Name__c;
            }
            // Expecting original values to be equal, the revised new value should be overwritten
            System.assertEquals( ctItem.KARL_Control_Scope_Name_Indexed__c, controlIdentifier + ' - ' + ctItem.Test_Name__c, 'Scope names do not match' ); 
        }
    }
}
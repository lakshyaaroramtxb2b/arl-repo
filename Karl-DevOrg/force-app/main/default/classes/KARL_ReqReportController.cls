/** 
 * @author Ben Harvie
 * @email ben.harvie@salesforce.com
 * @description This class provides the methods for generating the Request Master Data
 * Lightning Web Component resources.
 */
public with sharing  class KARL_ReqReportController {
    public static Set<Id> filteredRequests = new Set<Id>();
    public static Map<String, List<String>> requestUniqueControls   = new Map<String, List<String>>();
    public static Map<String, List<String>> requestUniqueScopes     = new Map<String, List<String>>();
    public static Map<String, List<String>> requestTestGroups       = new Map<String, List<String>>();
    public static Map<String, List<String>> requestUniqueRequirements = new Map<String, List<String>>();
    public static Boolean onlyControls  = false;
    public static Boolean noControls    = false;
    public static Boolean bothControls  = false;
    
    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Reusable method for generating the list of filteredRequests
     * @param auditCycleId - record Id for audit cycle
     * @param auditScopeId - picklist value for audit scope
     * @param areaOfCompliance - selected area of compliance from Button Group
     */
    @AuraEnabled(cacheable=true)
    public static void generateData(String auditScopeId, String [] areaOfCompliance, String [] mappingFilter, String [] filterAssigned, 
                                    String [] activeStatus, String filterType, String [] qaReview, String [] gusEnabled, String [] placeholderFilter){
        // Create filter strings 
        String scopeFilterReq       = '';
        String scopeFilterReqct     = '';
        String activeFilterReq      = '';
        String activeFilterReqct    = '';
        String queryAssignedReq     = '';
        String queryAssignedReqct   = '';
        String queryTypeFilterReq   = '';
        String queryTypeFilterReqct = '';
        String aocFilterReq         = ''; // for controls
        String aocFilterReqct       = ''; // for controls
        String areaQueryReqct       = ''; // for requirements

        // Create filter for Audit Scopes
        if(auditScopeId != 'allScopes' && auditScopeId != 'noScopes'){
            scopeFilterReq      = 'Primary_Scope__c =: auditScopeId ';
            scopeFilterReqct    = 'Test__r.Control_Scope__r.Scope_Name__c =: auditScopeId ';
        }else{
            scopeFilterReq   = 'Primary_Scope__c !=: auditScopeId ';
            scopeFilterReqct = 'Test__r.Control_Scope__r.Scope_Name__c !=: auditScopeId ';
        }

        // Create filter for active status (activeReq or inactiveReq)
        Set<Boolean> statusList = new Set<Boolean>();
        for(String activeState : activeStatus){
            if(activeState == 'activeReq'){
                statusList.add(true);
            }else{
                statusList.add(false);
            }
        }
        if(statusList.size() == 1){
            activeFilterReq     = 'AND Active__c =: statusList ';
            activeFilterReqct   = 'AND Request__r.Active__c =: statusList ';
        }

        // Create filter for Assignee
        for(String filterAssignee : filterAssigned){
            if(filterAssignee == 'assignedToMe'){
                String currentUserEmail = userInfo.getUserEmail();
                queryAssignedReq    = 'AND KARL_GRC_Assignee__r.Email =: currentUserEmail ';
                queryAssignedReqct  = 'AND Request__r.KARL_GRC_Assignee__r.Email =: currentUserEmail ';
            }
        }

        // Create filter for Request Type
        Map<String, String> filterTypeMap = new Map<String, String>{
            'typeDoc'       => 'Document (Policy Standard Procedures)',
            'typePop'       => 'Population',
            'typeSelfcol'   => 'Self Collection'
        };
        
        String queryType        = filterTypeMap.get(filterType);
        String typeFieldReq     = 'Type__c';
        String typeFieldReqct   = 'Request__r.Type__c';
        List<String> typeMulti  = new List<String>();

        // Build Map of Lists which contains request type groupings - not necessary for 1:1 associations
        Map<String, List<String>> typeMultiMap = new Map<String, List<String>>{
            'typeSample'    => new List<String> {'Samples','Configuration Samples','Sample of One'},
            'typeConfig'    => new List<String> {'Configuration','Configuration Samples','Configuration Observation'},
            'typeObs'       => new List<String> {'Observation','Configuration Observation'},
            'typeOthers'    => new List<String> {'Others - General Evidence','Knowledge','Inquiry'}
        };

        if(filterType != 'typeAll' && !typeMultiMap.containsKey(filterType)){
            queryTypeFilterReq      = 'AND ' + typeFieldReq + ' =: queryType ';
            queryTypeFilterReqct    = 'AND ' + typeFieldReqct + ' =: queryType ';
        }else if(typeMultiMap.containsKey(filterType)){
            typeMulti = typeMultiMap.get(filterType);
            queryTypeFilterReq      = 'AND ' + typeFieldReq + ' in: typeMulti ';
            queryTypeFilterReqct    = 'AND ' + typeFieldReqct + ' in: typeMulti ';
        }

        // Create filter for QA Review
        Boolean qaBool = false;
        for(String qaState : qaReview){
            if(qaState == 'qaReview'){
                qaBool = true;
            }
        }
        // We only filter if it is set (otherwise show all)
        String qaFilterReq      = qaBool == true ? 'AND Quality_Review_Required__c =: qaBool ' : ''; // generates F/P ApexSOQLInjection
        String qaFilterReqct    = qaBool == true ? 'AND Request__r.Quality_Review_Required__c =: qaBool ' : ''; // generates F/P ApexSOQLInjection

        // Create filter for Area of Compliance
        Map<String, String> areaMap = KARL_Constants.groupedAreaMap;
        Map<String, List<String>> areaQueryMap = KARL_Constants.aocQueryMap;

        // Create list of relevant AoCs
        List<String> areaList = new List<String>();
        String areaListString = '';
        for(string areaOfComp : areaOfCompliance){
            if(areaOfComp != 'allAoc'){
                if(areaQueryMap.containsKey(areaOfComp)){
                    areaList.addAll(areaQueryMap.get(areaOfComp));
                }else{
                    areaList.add(areaOfComp);
                }  
                areaListString  = '\'' + String.join(areaList, '\',\'') + '\'';
                aocFilterReq    = 'AND Areas_of_Compliance__c INCLUDES( ' + areaListString + ') '; // For Controls
                aocFilterReqct  = 'AND Request__r.Areas_of_Compliance__c INCLUDES( ' + areaListString + ') '; // For Controls
                areaQueryReqct  = 'AND Area_Requirement__r.Area_of_Compliance__c IN: areaList '; // For Requirements        
            }
        }

        // Create Mapping Filter (for data management)
        // Note allMap we don't need to do anything
        String mappingFilterReq     = '';
        String mappingFilterReqct   = '';
        String nullValue = null;
        for(String mappingElement : mappingFilter){
            if(mappingElement == 'ctrlsOnly'){
                mappingFilterReq    = 'AND id =: nullValue '; // evaluate to false always
                mappingFilterReqct  = ''; // no need to do anything
                onlyControls    = true;
            }else if(mappingElement == 'ctrlsNone'){
                noControls      = true;
            }else if(mappingElement == 'bothCtrls'){
                bothControls    = true;
            }
        }

        // Create GUS Filter (for data management)
        Boolean qusBool = false;
        for(String gusState : gusEnabled){
            if(gusState == 'gusEnabled'){
                qusBool = true;
            }
        }
        // We only filter if it is set (otherwise show all)
        String gusFilterReq      = qusBool == true ? 'AND Create_GUS_Case__c =: qusBool ' : '';
        String gusFilterReqct    = qusBool == true ? 'AND Request__r.Create_GUS_Case__c =: qusBool ' : '';

        // Create Placeholder (PL-99) Filter (for data management)
        String placeholderFilterReq     = '';
        String placeholderFilterReqct   = '';
        Boolean inactiveValue           = true; // so we can bind it
        for(String placeholderValue : placeholderFilter){
            // note use of += here to build the query (could be multiple selections)
            if(placeholderValue == 'pl99'){
                placeholderFilterReq    += 'AND id =: nullValue '; // evaluate to false always
                placeholderFilterReqct  += 'AND test__r.KARL_Control_Scope_Number_Indexed__c LIKE \'PL-99%\' '; // starts with PL-99
            }else if(placeholderValue == 'inactiveCtrls'){
                placeholderFilterReq    += 'AND id =: nullValue '; // evaluate to false always
                placeholderFilterReqct  += 'AND test__r.Control_Scope__r.KARL_Inactive__c =: inactiveValue ';
            }
        }

        // Generate dataset for Controls
        for( Request_Item_Control__c reqItemCtrl : Database.query('SELECT Id, Request__r.Name, Full_Control_Scope__c, ' +
                                                    'toLabel(Test__r.Control_Scope__r.Scope_Name__c), ' +
                                                    'Test__r.Test_Group__r.Name ' +
                                                    'FROM Request_Item_Control__c ' +
                                                    'WHERE ' + scopeFilterReqct + activeFilterReqct + queryAssignedReqct + queryTypeFilterReqct + aocFilterReqct + 
                                                    String.escapeSingleQuotes(qaFilterReqct) + mappingFilterReqct + String.escapeSingleQuotes(gusFilterReqct) + 
                                                    placeholderFilterReqct +
                                                    'WITH SECURITY_ENFORCED') ){
            // Step 1: Create the new Lists if one doesn't already exist for this REQ
            if(!filteredRequests.contains( reqItemCtrl.Request__c )){
                filteredRequests.add( reqItemCtrl.Request__c );
                // Create Map of Lists for capturing requirement mappings
                requestUniqueControls.put(reqItemCtrl.Request__r.Name,new List<String>());
                requestUniqueScopes.put(reqItemCtrl.Request__r.Name,new List<String>());
                requestTestGroups.put(reqItemCtrl.Request__r.Name,new List<String>());
            }

            // Step 2: Capture Unique Controls
            // Check if the value already exists in the list, if it does not: add it
            if ( !requestUniqueControls.get(reqItemCtrl.Request__r.Name).contains(reqItemCtrl.Full_Control_Scope__c) ){
                requestUniqueControls.get(reqItemCtrl.Request__r.Name).add(reqItemCtrl.Full_Control_Scope__c);
            }

            // Step 3: Capture Unique Scopes
            // Check if the value already exists in the list, if it does not: add it
            if ( !requestUniqueScopes.get(reqItemCtrl.Request__r.Name).contains(reqItemCtrl.Test__r.Control_Scope__r.Scope_Name__c) ){
                requestUniqueScopes.get(reqItemCtrl.Request__r.Name).add(reqItemCtrl.Test__r.Control_Scope__r.Scope_Name__c);
            }

            // Step 4: Capture Unique Walkthroughs (f.k.a. Control Test Groups)
            // Check if the value already exists in the list, if it does not: add it
            // On this logical operation we also check if there is a Test Group or not, so we don't add null values
            if ( !requestTestGroups.get(reqItemCtrl.Request__r.Name).contains(reqItemCtrl.Test__r.Test_Group__r.Name) && !String.isBlank(reqItemCtrl.Test__r.Test_Group__r.Name) ){
                requestTestGroups.get(reqItemCtrl.Request__r.Name).add(reqItemCtrl.Test__r.Test_Group__r.Name);
            }
        }

        // True-up controls data (show other controls mapped to requests not just those which are scoped)
        for( Request_Item_Control__c reqItemCtrlTrueup : Database.query('SELECT Id, Request__r.Name, Full_Control_Scope__c ' +
                                                    'FROM Request_Item_Control__c ' +
                                                    'WHERE Request__c IN: filteredRequests ' + mappingFilterReqct +
                                                    'WITH SECURITY_ENFORCED')){
                // Add the control values for non-scoped components
                if ( requestUniqueControls.containsKey(reqItemCtrlTrueup.Request__r.Name) && 
                    !requestUniqueControls.get(reqItemCtrlTrueup.Request__r.Name).contains(reqItemCtrlTrueup.Full_Control_Scope__c) ){
                    requestUniqueControls.get(reqItemCtrlTrueup.Request__r.Name).add(reqItemCtrlTrueup.Full_Control_Scope__c);
                }
        }

        // Generate dataset for Primary Scope
        for( Request_Item__c reqItem : Database.query('SELECT Id, Primary_Scope__c, Name ' +
                                                    'FROM Request_Item__c ' +
                                                    'WHERE ' + scopeFilterReq + activeFilterReq + queryAssignedReq + queryTypeFilterReq + aocFilterReq + 
                                                    String.escapeSingleQuotes(qaFilterReq) + mappingFilterReq + String.escapeSingleQuotes(gusFilterReq) +
                                                    placeholderFilterReq +
                                                    'WITH SECURITY_ENFORCED') ){
            // Step 1: Create the new Lists if one doesn't already exist for this REQ
            if(!filteredRequests.contains( reqItem.Id )){
                filteredRequests.add( reqItem.Id );
                requestUniqueScopes.put(reqItem.Name,new List<String>());
            }
            if ( !requestUniqueScopes.get(reqItem.Name).contains(reqItem.Primary_Scope__c) ){
                requestUniqueScopes.get(reqItem.Name).add(reqItem.Primary_Scope__c);
            }
        }

        // Generate dataset for Requirements
        for( Request_Item_Area__c reqItemArea : Database.query('SELECT Id, Request_Master_Data__r.Name,' +
                'toLabel(Area_Requirement__r.Area_of_Compliance__c), Requirement_ID__c ' +
                'FROM Request_Item_Area__c ' +
                'WHERE Request_Master_Data__c IN: filteredRequests ' + areaQueryReqct +
                'WITH SECURITY_ENFORCED')){
                
                // Establish the new map w/ string if one doesn't exist
                if ( ! requestUniqueRequirements.containsKey(reqItemArea.Request_Master_Data__r.Name) ) {
                    requestUniqueRequirements.put(reqItemArea.Request_Master_Data__r.Name, new List<String>());
                }
                
                // Add values into the lists as needed
                if( !requestUniqueRequirements.get(reqItemArea.Request_Master_Data__r.Name).contains(reqItemArea.Requirement_ID__c) ){
                    requestUniqueRequirements.get(reqItemArea.Request_Master_Data__r.Name).add(reqItemArea.Requirement_ID__c);
                }
        }

        // Debug Queries
        /*System.debug(LoggingLevel.DEBUG, '--- START KARL Data Debug Output ---');
        System.debug(LoggingLevel.DEBUG, 'Scope SOQL Filter >> ' + scopeFilterReqct);
        System.debug(LoggingLevel.DEBUG, 'Active Status SOQL Filter >> ' + activeFilterReqct);
        System.debug(LoggingLevel.DEBUG, 'Assignment SOQL Filter >> ' + queryAssignedReqct);
        System.debug(LoggingLevel.DEBUG, 'Request Type SOQL Filter >> ' + queryTypeFilterReqct);
        System.debug(LoggingLevel.DEBUG, 'QA Review SOQL Filter >> ' + qaFilterReqct);
        System.debug(LoggingLevel.DEBUG, 'Area of Compliance SOQL Filter - Controls >> ' + aocFilterReqct);
        System.debug(LoggingLevel.DEBUG, 'Area of Compliance SOQL Filter - Requirements >> ' + areaQueryReqct);
        System.debug(LoggingLevel.DEBUG, 'Area of Compliance List >> ' + areaList);
        System.debug(LoggingLevel.DEBUG, 'Area of Compliance List String >> ' + areaListString);
        System.debug(LoggingLevel.DEBUG, 'GUS Filter >> ' + gusFilterReqct);
        System.debug(LoggingLevel.DEBUG, 'Placeholder Filter REQ >> ' + placeholderFilterReq);
        System.debug(LoggingLevel.DEBUG, 'Placeholder Filter REQCT >> ' + placeholderFilterReqct);
        System.debug(LoggingLevel.DEBUG, 'Mapping Filter REQ >> ' + mappingFilterReq);
        System.debug(LoggingLevel.DEBUG, 'Mapping Filter REQCT >> ' + mappingFilterReqct);
        System.debug(LoggingLevel.DEBUG, 'Filtered Requests >> ' + filteredRequests);
        System.debug(LoggingLevel.DEBUG, '--- END KARL Data Debug Output ---');*/
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Build the relevant report items for the reportwrapper, which get displayed via the LWC component 
     * Karl_ARL_Datatable. Note that this is a multi-object reportwrapper.
     * @param auditCycleId - record Id for audit cycle
     * @param auditScopeId - picklist value for audit scope
     * @param areaOfCompliance - selected area of compliance from Button Group
     * @param filterState - filter values for Status
     * @param filterAssigned - filter values for Assigned
     * @param filterType - filter based on Request Type
     * @return Report Wrapper contents for Wire data feed
     */
    @AuraEnabled(cacheable=true)
    public static List<ReportWrapper> getReport(String auditScopeId, String [] areaOfCompliance, String [] mappingFilter, String [] filterAssigned,
                                                 String filterType, String [] activeStatus, String [] qaReview, String [] gusEnabled, String [] placeholderFilter){  
        
        // Step 1: Create a Set (unique) of all Request Item Ids from Cycle Request Items
        generateData(auditScopeId, areaOfCompliance, mappingFilter, filterAssigned, activeStatus, filterType, qaReview, gusEnabled, placeholderFilter);             

        // Step 2: Return Datatable Data
        return getMasterDataReport();
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Build the Master Data Report
     * @return master data report
     */
    private static List<ReportWrapper> getMasterDataReport(){
        String baseUrl = URL.getSalesforceBaseUrl().toExternalForm();
        String baseRecordURL = baseUrl + '/one/one.app?#/sObject/'; // force rendering in Lightning instead of Classic
        String baseUrlSuffix = '/view'; // Required for Lightning record view

        List<ReportWrapper> items = new List<ReportWrapper>();

        // Create map of available picklist items (so we can get the label)
        list<Schema.PicklistEntry> scopePicklist = Request_Item__c.Primary_Scope__c.getDescribe().getPicklistValues();
        map<String,String> scopeLabels = new map<String,String>();
        for (Schema.PicklistEntry pe : scopePicklist){
            scopeLabels.put(pe.getValue(), pe.getLabel());
        }

        for( Request_Item__c reqMasterData : Database.query('SELECT Id, Request_Name__c, Active__c, '+
                                                                    'Quality_Review_Required__c, '+
                                                                    'Type__c, Primary_Scope__c, Create_GUS_Case__c, '+
                                                                    'Request_Description__c, Priority__c, '+
                                                                    'KARL_Evidence_Submission_Workflow__c, '+
                                                                    'Name, KARL_GRC_Assignee__r.FirstName, '+
                                                                    'KARL_GRC_Assignee__r.LastName '+
                                                                    'FROM Request_Item__c  '+
                                                                    'WHERE Id in: filteredRequests '+
                                                                    'WITH SECURITY_ENFORCED')){
            Boolean displayRecord = false;
            ReportWrapper rw = new ReportWrapper();
            rw.Id               = reqMasterData.Id;
            // Information derived from Request Item Control Test (Request_Item_Control__c)
            if(requestUniqueControls.get(reqMasterData.Name) != null){
                rw.controlNames     = String.join(requestUniqueControls.get(reqMasterData.Name), '\r\n');
                rw.controlLabel     = 'slds-text';
                if(rw.controlNames.startsWith('PL-99')){
                    rw.controlLabel     = 'slds-badge slds-theme_info';
                }
                if(onlyControls || bothControls){ displayRecord = true; }
            }else{
                rw.controlNames     = 'No controls';
                if(noControls || bothControls){ displayRecord = true; }
                rw.controlLabel     = 'slds-badge slds-theme_warning';
            }

            rw.scopeNames       = String.join(requestUniqueScopes.get(reqMasterData.Name), '\r\n');
            
            if(requestTestGroups.get(reqMasterData.Name) != null){
                rw.testGroups       = String.join(requestTestGroups.get(reqMasterData.Name), '\r\n');
            }

            if(requestUniqueRequirements.containsKey(reqMasterData.Name)){
                // Join requirements on a new line
                rw.testRequirements = String.join(requestUniqueRequirements.get(reqMasterData.Name), '\r\n');
            }else{
                // No requirements mapped
                rw.testRequirements = '';
            }

            // Core information about the REQ
            rw.requestNumber        = reqMasterData.Name;
            rw.primaryScope         = scopeLabels.get(reqMasterData.Primary_Scope__c);
            rw.requestName          = reqMasterData.Request_Name__c;
            rw.requestPriority      = reqMasterData.Priority__c;
            rw.requestREQLink       = baseRecordURL + reqMasterData.Id + baseUrlSuffix;
            rw.requestDescription   = reqMasterData.Request_Description__c;
            rw.requestType          = reqMasterData.Type__c;
            rw.requestQa            = reqMasterData.Quality_Review_Required__c;
            rw.requestGus           = reqMasterData.Create_GUS_Case__c;
            rw.requestWorkflow      = reqMasterData.KARL_Evidence_Submission_Workflow__c ? 'action:record' : '';
            rw.grcAssignee          = reqMasterData.KARL_GRC_Assignee__r.FirstName + ' ' + reqMasterData.KARL_GRC_Assignee__r.LastName;

            if(displayRecord){ items.add(rw); }
        }
        return items;
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Apex driven update function for multi-line edits from the LWC. Expects that the 
     * fieldName is the key with the value being passed as the value, for JSON serialization.
     * @param data
     * @return message of success or failure
     */
    @AuraEnabled
    public static string updateRequest(Object data){
        List<Request_Item__c> recordsForUpdate = (List<Request_Item__c>)JSON.deserialize(
            JSON.serialize(data),
            List<Request_Item__c>.class
        );
        System.debug('To update >> ' + recordsForUpdate);
        String msg = '';
        try {
            if(Schema.sObjectType.Request_Item__c.isUpdateable() 
                    && Request_Item__c.Request_Name__c.getDescribe().isUpdateable()
                    && Request_Item__c.Quality_Review_Required__c.getDescribe().isUpdateable()
                    && Request_Item__c.Create_GUS_Case__c.getDescribe().isUpdateable()
                    && Request_Item__c.Priority__c.getDescribe().isUpdateable()){
                        update recordsForUpdate;
                        return 'Success: KARL records updated successfully [Salesforce]';
            }else{
                return 'Error: Insufficient permissions to update records [Salesforce].';
            }
        }catch(DmlException e){
            //Any type of Validation Rule error message, Required field missing error message, Trigger error message etc..
            //we can get from DmlException
             
            //Get All DML Messages
            for (Integer i = 0; i < e.getNumDml(); i++) {
                //Get Validation Rule & Trigger Error Messages
                msg =+ e.getDmlMessage(i) +  '\n' ;
            }
            //throw DML exception message
            throw new AuraHandledException(msg);
             
        }catch (Exception e) {
            throw new AuraHandledException(''+e.getMessage());
            //return 'The following exception has occurred: ' + e.getMessage();
        }
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description This is a wrapper class to capture the ARL line items at a row level.
     * @return nothing
     */
    public class ReportWrapper{
        @AuraEnabled public String Id;
        @AuraEnabled public String controlNames;
        @AuraEnabled public String controlLabel;
        @AuraEnabled public String scopeNames;
        @AuraEnabled public String primaryScope;
        @AuraEnabled public String testGroups;
        @AuraEnabled public String testRequirements;
        @AuraEnabled public String requestNumber;
        @AuraEnabled public String requestName;
        @AuraEnabled public String requestPriority;
        @AuraEnabled public String requestREQLink;
        @AuraEnabled public String requestDescription;
        @AuraEnabled public String requestType;
        @AuraEnabled public String requestWorkflow;
        @AuraEnabled public String grcAssignee;
        @AuraEnabled public Boolean requestQa;
        @AuraEnabled public Boolean requestGus;
    }
}
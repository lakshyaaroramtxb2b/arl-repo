global class SyncSupportforceUserInfo implements Database.Batchable<sObject>, Database.Stateful {

    public String SOURCE_FILE = 'SyncSupportforceUserInfo';
    
    public Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([SELECT Id,
                                                Federation_Identifier__c,
                                                SupportForce_User__c
                                         FROM INTEG_Employee__c
                                         WHERE Duplicate__c = false 
                                         AND Federation_Identifier__c != null 
                                         AND SupportForce_User__c = null
                                         AND INTEG_Active__c = true]);
    }

    public void execute(Database.BatchableContext bc, List<INTEG_Employee__c> lstEmployees) {
        
        Set<String> setFederationIds = new Set<String>();
        
        for(INTEG_Employee__c employee : lstEmployees){
          setFederationIds.add(employee.Federation_Identifier__c);
        }

        List<SupportForceUser__x> lstUsers = [SELECT ExternalId,
                                                     FederationIdentifier__c
                                              FROM SupportForceUser__x 
                                              WHERE FederationIdentifier__c IN : setFederationIds
                                              AND IsActive__c = true
                                              AND ContactId__c = null];

        if(lstUsers.size() > 0){

            Map<String,String> mapSptUsers = new Map<String,String>();

            for(SupportForceUser__x sptUser : lstUsers){
                mapSptUsers.put(sptUser.FederationIdentifier__c, sptUser.ExternalId);
            }

            for(INTEG_Employee__c employee : lstEmployees){
                employee.SupportForce_User__c = mapSptUsers.get(employee.Federation_Identifier__c);
            }

            List<Database.SaveResult> saveResults = Database.Update(lstEmployees, false);
            String finalErrorText = '';
            for (Database.SaveResult sr : saveResults) {
                if (!sr.isSuccess()) 
                    for(Database.Error err : sr.getErrors()) {
                                finalErrorText += err.getStatusCode() + ': ' + err.getMessage() + '\n';
                    }
            }
            
            Esa_DebugService.writeMessage('SyncSupportforceUserInfo Completed:');
            if(string.isNotBlank(finalErrorText)) {
                Esa_DebugService.writeMessage('SyncSupportforceUserInfo.finish: Failed with errors');
                Exception ex;
                Esa_DebugService.writeException(ex, 1, SOURCE_FILE, finalErrorText);
            }
        }

    }

    public void finish(Database.BatchableContext BC) {
         
    }

}
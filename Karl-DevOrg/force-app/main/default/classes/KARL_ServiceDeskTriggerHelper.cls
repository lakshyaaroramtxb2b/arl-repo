/**
 * @author Lakshya Arora
 * @email lakshya.arora@mtxb2b.com
 * @description This is helper class for Karl_serviceDeskTrigger
 * @note Inherited sharing because we have external sharing model as private for contact, scope notifications configuration the trigger 
 * should have accessed to all records for approval emails, that's why inherited sharing is used as trigger runs in system mode.
 * If run with 'WITH Sharing' then it won't be able to access the contact records and approval notifications won't get send to contact records.
*/
public inherited sharing class KARL_ServiceDeskTriggerHelper {
    @TestVisible private static Map<Id,Id> contactIdToUserTestMap = new Map<Id,Id>();
    /**
    * @author Lakshya Arora
    * @email lakshya.arora@mtxb2b.com
    * @description Populate name as auto number based on record type
    * @note Call on Before insert
    * @param New List of KARL_Service_Desk__c
    */
    public static void populateAutoNumberAndSubject(List<KARL_Service_Desk__c> newList){
        Boolean isEvidenceRequestExist = false;
        Boolean isMasterDataExist = false;
        Integer latestEvidenceAutoNumber = 0;
        Integer latestRequestMasterDataAutoNumber = 0;
        Set<Id> evidenceRequestIdSet = new Set<Id>();
        Map<Id,String> evidenceRequestItemidToNameMap = new Map<id,String>();
        for(KARL_Service_Desk__c karlServiceDeskObj : newList){
            if(karlServiceDeskObj.Evidence_Request_Name__c != null){
                evidenceRequestIdSet.add(karlServiceDeskObj.Evidence_Request_Name__c);
            }
            if(!isEvidenceRequestExist && karlServiceDeskObj.RecordTypeId == KARL_Constants.EVIDENCE_REQUEST_RECORDTYPEID){
                isEvidenceRequestExist = true;
            }
            else if(!isMasterDataExist && karlServiceDeskObj.RecordTypeId == KARL_Constants.REQUEST_MASTER_DATA_RECORDTYPEID){
                isMasterDataExist = true;
            }
        }
        if(isEvidenceRequestExist){
            latestEvidenceAutoNumber = getLatestServiceDeskNoByRecordTypeId(KARL_Constants.EVIDENCE_REQUEST_RECORDTYPEID);
        }
        if(isMasterDataExist){
            latestRequestMasterDataAutoNumber = getLatestServiceDeskNoByRecordTypeId(KARL_Constants.REQUEST_MASTER_DATA_RECORDTYPEID);
        }
        if(!evidenceRequestIdSet.isEmpty()){
            for(KARL_Evidence_Request__c erObj : [SELECT Id,Name FROM KARL_Evidence_Request__c WHERE Id IN :evidenceRequestIdSet WITH SECURITY_ENFORCED]){
                evidenceRequestItemidToNameMap.put(erObj.Id,erObj.Name);
            }
        }
        for(KARL_Service_Desk__c karlServiceDeskObj : newList){
            if(karlServiceDeskObj.RecordTypeId == KARL_Constants.EVIDENCE_REQUEST_RECORDTYPEID){
                latestEvidenceAutoNumber++;
                karlServiceDeskObj.Auto_Number_Apex__c = Decimal.valueOf(latestEvidenceAutoNumber);
                karlServiceDeskObj.Name = generateAutoNumber(String.valueOf(latestEvidenceAutoNumber),4,'T-ER-');
                if(evidenceRequestItemidToNameMap.containsKey(karlServiceDeskObj.Evidence_Request_Name__c))
                karlServiceDeskObj.Subject__c = 'Access Request: '+evidenceRequestItemidToNameMap.get(karlServiceDeskObj.Evidence_Request_Name__c);
            }
            else if(karlServiceDeskObj.RecordTypeId == KARL_Constants.REQUEST_MASTER_DATA_RECORDTYPEID){
                latestRequestMasterDataAutoNumber++;
                karlServiceDeskObj.Auto_Number_Apex__c = Decimal.valueOf(latestRequestMasterDataAutoNumber);
                karlServiceDeskObj.Name = generateAutoNumber(String.valueOf(latestRequestMasterDataAutoNumber),4,'T-REQ-');
                if(karlServiceDeskObj.New_Or_Existing__c == KARL_Constants.NEWOREXISTING_EXISTING){
                    karlServiceDeskObj.Subject__c = 'Update Existing';
                }
                else{
                    karlServiceDeskObj.Subject__c = 'New Request';
                    
                }
                karlServiceDeskObj.Subject__c += ' - ' + karlServiceDeskObj.New_Request_Name__c;
            }
        }
    }

    /**
    * @author Lakshya Arora
    * @email lakshya.arora@mtxb2b.com
    * @description Populate GRC assignee user used in approval process when grc assignee contact lookup added/changes
    * @param1 New List of KARL_Service_Desk__c
    * @param2 OldMap -> Map of Id vs old KARL_Service_Desk__c record info
    * @note Call on Before insert,Before Update
    */
    public static void populateGRCAssigneeUser(List<KARL_Service_Desk__c> newList,Map<Id,KARL_Service_Desk__c> oldMap){
        Set<Id> grcAssigneeContactIdSet = new Set<Id>();
        for(KARL_Service_Desk__c serviceDeskObj : newList){
            if(oldMap == null || oldMap.get(serviceDeskObj.Id).GRC_Assignee__c != serviceDeskObj.GRC_Assignee__c){
                if(serviceDeskObj.GRC_Assignee__c == null){
                    serviceDeskObj.GRC_Assignee_User__c = null;
                }
                else{
                    grcAssigneeContactIdSet.add(serviceDeskObj.GRC_Assignee__c);
                }
            }
        }

        if(!grcAssigneeContactIdSet.isEmpty()){
            Map<Id,Id> contactIdToUserMap = new Map<Id,Id>();
            //No security enforced as we need to access all the users
            for(User userinfoObj : [SELECT Id,ContactId 
                                    FROM User 
                                    WHERE ContactId IN:grcAssigneeContactIdSet 
                                    LIMIT 50000]){
                contactIdToUserMap.put(userinfoObj.contactId,userinfoObj.Id);
            }
            if(!contactIdToUserMap.isEmpty()){
                for(KARL_Service_Desk__c serviceDeskObj : newList){
                    if(serviceDeskObj.GRC_Assignee__c != null && (oldMap == null || oldMap.get(serviceDeskObj.Id).GRC_Assignee__c != serviceDeskObj.GRC_Assignee__c)){
                       if(contactIdToUserMap.containsKey(serviceDeskObj.GRC_Assignee__c)){
                        serviceDeskObj.GRC_Assignee_User__c = contactIdToUserMap.get(serviceDeskObj.GRC_Assignee__c);
                       }else{
                        serviceDeskObj.GRC_Assignee_User__c = null;
                       } 
                    }
                }
            }
        }
    }

    /**
    * @author Lakshya Arora
    * @email lakshya.arora@mtxb2b.com
    * @description Update Rejected comments
    * @param1 New List of KARL_Service_Desk__c
    * @param2 OldMap -> Map of Id vs old KARL_Service_Desk__c record info
    * @note Call on Before Update
    */
    public static void updateRejectedComments(List<KARL_Service_Desk__c> newList,Map<Id,KARL_Service_Desk__c> oldMap){
        Set<Id> serviceDeskIdSet = new Set<Id>(); 
        for(KARL_Service_Desk__c serviceDeskObj : newList){
            if(serviceDeskObj.Status__c == KARL_Constants.STATUS_REJECTED
                    && oldMap != null 
                    && oldMap.get(serviceDeskObj.Id).Status__c != serviceDeskObj.Status__c){
                        serviceDeskIdSet.add(serviceDeskObj.Id);
            }
        }
        if(!serviceDeskIdSet.isEmpty()){
            Map<Id,String> serviceDeskIdToCommentsMap = new Map<Id,String>();
            //can not use security enforced as need to access the processSteps Object
            for(KARL_Service_Desk__c serviceDeskObj : [SELECT Id,
                                                        (SELECT Comments 
                                                        FROM ProcessSteps 
                                                        ORDER BY SystemModstamp 
                                                        DESC LIMIT 1) 
                                                      FROM KARL_Service_Desk__c c 
                                                      WHERE Id IN: serviceDeskIdSet]){
                                                        for (ProcessInstanceHistory pihObj : serviceDeskObj.ProcessSteps){
                                                            serviceDeskIdToCommentsMap.put(serviceDeskObj.Id,pihObj.comments);
                                                        }
            }
            if(!serviceDeskIdToCommentsMap.isEmpty()){
                for(KARL_Service_Desk__c serviceDeskObj : newList){
                    if(serviceDeskObj.Status__c == KARL_Constants.STATUS_REJECTED
                            && oldMap != null 
                            && oldMap.get(serviceDeskObj.Id).Status__c != serviceDeskObj.Status__c){
                                if(serviceDeskIdToCommentsMap.containsKey(serviceDeskObj.Id)){
                                    if(String.isBlank(serviceDeskIdToCommentsMap.get(serviceDeskObj.Id))){
                                        serviceDeskObj.addError('For rejection, please add rejection reason to the comments box. Please reject again.');
                                    }
                                    else{
                                        serviceDeskObj.Rejection_Reason__c = serviceDeskIdToCommentsMap.get(serviceDeskObj.Id);
                                    }
                                }
                    }
                }
            }
            
        }
    }  
    
     /**
    * @author Lakshya Arora
    * @email lakshya.arora@mtxb2b.com
    * @description Create chatter when status changes to approved or rejected
    * @param1 New List of KARL_Service_Desk__c
    * @param2 OldMap -> Map of Id vs old KARL_Service_Desk__c record info
    * @note Call on After Update
    */
    public static void sendApprovalProcessEmails(List<KARL_Service_Desk__c> newList,Map<Id,KARL_Service_Desk__c> oldMap){
        Map<String,String> BASEURLMAP = KARL_Utility.getBaseURL();
        String BASERECORDURL = BASEURLMAP.get('baseRecordURL');
        String BASEURLSUFFIX = BASEURLMAP.get('baseUrlSuffix');
        FINAL String ORGWIDEDISPLAYNAME = System.label.SCEA_KARL;
        // prepare initial email steps
        List<Messaging.SingleEmailMessage> mailList = new List<Messaging.SingleEmailMessage>();
        List<OrgWideEmailAddress> orgwideEmailAddressList = KARL_Utility.getOrgwideEmailAddressIdByDisplayName(ORGWIDEDISPLAYNAME);
        Map<String,EmailTemplate> developerNameToEmailTemplateObjMap = KARL_Utility.getEmailTemplateBydevelopernameMap(new List<String>{
                                                                                                                        'KARL_Service_Desk_Approved_Email_Template',
                                                                                                                        'KARL_Service_Desk_Rejection_Email_Template',
                                                                                                                        'KARL_Service_Desk_Requester_Submit_Email_Template'});
        Map<String,List<EmailTemplate>> serviceDeskStatusToEmailTemplateMap = new Map<String,List<EmailTemplate>>{KARL_Constants.STATUS_APPROVED  => new List<EmailTemplate>{developerNameToEmailTemplateObjMap.get('KARL_Service_Desk_Approved_Email_Template')}, 
                                                                                                                    KARL_Constants.STATUS_REJECTED => new List<EmailTemplate>{developerNameToEmailTemplateObjMap.get('KARL_Service_Desk_Rejection_Email_Template')}, 
                                                                                                                    KARL_Constants.STATUS_SUBMIT_FOR_APPROVAL => new List<EmailTemplate>{developerNameToEmailTemplateObjMap.get('KARL_Service_Desk_Requester_Submit_Email_Template')}};
        Map<String,String> primaryScopeToGroupEmailAliasMap = new Map<String,String>();
        Set<String> primaryScopeIdSet = new Set<String>();
        Set<Id> requestedByIdSet = new Set<Id>();
        //prepare email data
        if(!orgwideEmailAddressList.isEmpty()){
            for(KARL_Service_Desk__c serviceDeskObj : newList){
                if((serviceDeskObj.Status__c == KARL_Constants.STATUS_REJECTED || serviceDeskObj.Status__c == KARL_Constants.STATUS_APPROVED || serviceDeskObj.Status__c == KARL_Constants.STATUS_SUBMIT_FOR_APPROVAL) 
                     && serviceDeskObj.GRC_Assignee__c != null
                     && (oldMap == null || oldMap.get(serviceDeskObj.Id).Status__c != serviceDeskObj.Status__c)
                    ){
                    requestedByIdSet.add(serviceDeskObj.GRC_Assignee__c);
                    if(serviceDeskObj.Business_Unit__c != null)
                    primaryScopeIdSet.add(serviceDeskObj.Business_Unit__c);
                    if(serviceDeskObj.Requested_By__c != null)
                    requestedByIdSet.add(serviceDeskObj.Requested_By__c);
                }
            }
            Map<String,Id> primaryScopeToBULeadMap = new Map<String,Id>();
            if(!primaryScopeIdSet.isEmpty()){
                for(KARL_Audit_Scope_Master_Data__c auditMasterDataObj : [SELECT Id,KARL_BU_Lead__c,KARL_Scope__c 
                                                                            FROM KARL_Audit_Scope_Master_Data__c 
                                                                            WHERE KARL_Scope__c IN :primaryScopeIdSet 
                                                                            WITH SECURITY_ENFORCED]){
                    primaryScopeToBULeadMap.put(auditMasterDataObj.KARL_Scope__c,auditMasterDataObj.KARL_BU_Lead__c);
                }
                for(KARL_Scope_Notifications_Config__c scopeNotificationConfigObj : [SELECT Id,Scope__c,Group_Email_Alias__c 
                                                                                        FROM KARL_Scope_Notifications_Config__c
                                                                                        WHERE Scope__c IN :primaryScopeIdSet 
                                                                                        AND Mute_Emails__c = false
                                                                                        WITH SECURITY_ENFORCED]){
                    primaryScopeToGroupEmailAliasMap.put(scopeNotificationConfigObj.Scope__c,scopeNotificationConfigObj.Group_Email_Alias__c);
                }
            }
            Map<Id,String> requestedByIdToNameMap = new Map<Id,String>();
            if(!requestedByIdSet.isEmpty()){
                for(Contact conObj : [SELECT Id,Name 
                                        FROM Contact 
                                        WHERE Id IN :requestedByIdSet 
                                        WITH SECURITY_ENFORCED]){
                    requestedByIdToNameMap.put(conObj.Id,conObj.Name);
                }
            }
            for(KARL_Service_Desk__c serviceDeskObj : newList){
                if((serviceDeskObj.Status__c == KARL_Constants.STATUS_REJECTED || serviceDeskObj.Status__c == KARL_Constants.STATUS_APPROVED || serviceDeskObj.Status__c == KARL_Constants.STATUS_SUBMIT_FOR_APPROVAL) 
                    && serviceDeskObj.GRC_Assignee__c != null
                    && (oldMap == null || oldMap.get(serviceDeskObj.Id).Status__c != serviceDeskObj.Status__c)){
                        if(serviceDeskStatusToEmailTemplateMap.containsKey(serviceDeskObj.Status__c)){
                            for(EmailTemplate emailTemplateObj : serviceDeskStatusToEmailTemplateMap.get(serviceDeskObj.Status__c)){
                                Set<String> toAddressSet = new Set<String>();
                                Set<String> ccAddressSet = new Set<String>();
                                if(serviceDeskObj.Status__c == KARL_Constants.STATUS_SUBMIT_FOR_APPROVAL){
                                    if(serviceDeskObj.Requested_By__c != null){
                                        toAddressSet.add(serviceDeskObj.Requested_By__c);
                                    }
                                    if(primaryScopeToGroupEmailAliasMap.containsKey(serviceDeskObj.Business_Unit__c)){
                                        ccAddressSet.add(primaryScopeToGroupEmailAliasMap.get(serviceDeskObj.Business_Unit__c));
                                    }
                                    if(primaryScopeToBULeadMap.containsKey(serviceDeskObj.Business_Unit__c)){
                                        ccAddressSet.add(primaryScopeToBULeadMap.get(serviceDeskObj.Business_Unit__c));
                                    }
                                    // if(emailTemplateObj.developerName == 'KARL_Service_Desk_Requester_Submit_Email_Template'){
                                        
                                    // }
                                    // else if(emailTemplateObj.developerName == 'KARL_Submitted_for_Approval_Email_Template'){
                                    //     if(primaryScopeToBULeadMap.containsKey(serviceDeskObj.Business_Unit__c)){
                                    //         toAddressSet.add(primaryScopeToBULeadMap.get(serviceDeskObj.Business_Unit__c));
                                    //     }
                                    // }
                                }
                                else{ // i.e. on reject and approved status
                                    if(serviceDeskObj.Requested_By__c != null){
                                        toAddressSet.add(serviceDeskObj.Requested_By__c);
                                    }
                                    if(primaryScopeToBULeadMap.containsKey(serviceDeskObj.Business_Unit__c)){
                                        ccAddressSet.add(primaryScopeToBULeadMap.get(serviceDeskObj.Business_Unit__c));
                                    }
                                    if(primaryScopeToGroupEmailAliasMap.containsKey(serviceDeskObj.Business_Unit__c)){
                                        ccAddressSet.add(primaryScopeToGroupEmailAliasMap.get(serviceDeskObj.Business_Unit__c));
                                    }
                                    if(serviceDeskObj.New_Or_Existing__c == KARL_Constants.NEWOREXISTING_EXISTING){
                                        ccAddressSet.add(serviceDeskObj.GRC_Assignee__c);
                                    }
                                }
                                if(!toAddressSet.isEmpty() || !ccAddressSet.isEmpty()){
                                    String htmlBody = emailTemplateObj.htmlvalue;
                                    emailTemplateObj.Subject = emailTemplateObj.Subject.replace('{!serviceDeskName}',serviceDeskObj.Name);
                                    if(String.isNotBlank(requestedByIdToNameMap.get(serviceDeskObj.Requested_By__c)))
                                    htmlBody = htmlBody.replace('{!requestedBy}',requestedByIdToNameMap.get(serviceDeskObj.Requested_By__c));
                                    if(String.isNotBlank(requestedByIdToNameMap.get(serviceDeskObj.GRC_Assignee__c)))
                                    htmlBody = htmlBody.replace('{!grcAssignee}',requestedByIdToNameMap.get(serviceDeskObj.GRC_Assignee__c));
                                    htmlBody = htmlBody.replace('{!serviceDeskName}',serviceDeskObj.Name);
                                    if(String.isNotBlank(serviceDeskObj.Rejection_Reason__c))
                                    htmlBody = htmlBody.replace('{!rejectionComments}',serviceDeskObj.Rejection_Reason__c);
                                    htmlBody = htmlBody.replace('{!serviceDeskLink}',BASERECORDURL+serviceDeskObj.Id+BASEURLSUFFIX);
                                    List<String> toAddressList = new List<String>();
                                    toAddressList.addAll(toAddressSet);
                                    List<String> ccAddressList = new List<String>();
                                    ccAddressList.addAll(ccAddressSet);
                                    mailList.add(KARL_Utility.createMessage(new Messaging.SingleEmailMessage(),emailTemplateObj,htmlBody,toAddressList,ccAddressList,orgwideEmailAddressList[0].Id));
                                }
                            } 
                        }
                }
            }
            
        }
        else{
            System.debug(LoggingLevel.DEBUG, 'Org Wide Email Address not found.');
        }
        
        if(!mailList.isEmpty()){
            Messaging.SendEmailResult[] emailResults =  Messaging.sendEmail( mailList, false);
            for(Messaging.SendEmailResult result : emailResults){
                if(!result.isSuccess()){
                    System.debug(LoggingLevel.DEBUG,'Error Received');
                    for(Database.Error error : result.getErrors()){
                        System.debug(LoggingLevel.DEBUG,error.getMessage());
                    }
                }
            }
        }     
    }


      /**
    * @author Lakshya Arora
    * @email lakshya.arora@mtxb2b.com
    * @description Handle operations After request got approved
    * @param1 New List of KARL_Service_Desk__c
    * @param2 OldMap -> Map of Id vs old KARL_Service_Desk__c record info
    * @note Call on After Update
    */
    public static void afterApprovedOperations(List<KARL_Service_Desk__c> newList,Map<Id,KARL_Service_Desk__c> oldMap){
        List<Request_Item__c> requestItemList = new List<Request_Item__c>();
        Map<Id,Request_Item__c> serviceDeskIdToRequestItemMap = new Map<Id,Request_Item__c>();
        Map<Id,Set<String>> requestItemIdToAreaRequirementMap = new Map<Id,Set<String>>();
        Map<Id,Set<String>> requestItemIdToControlTestMap = new Map<Id,Set<String>>();
        Map<Id,KARL_Service_Desk__c> newServiceDeskIdToMap = new Map<Id,KARL_Service_Desk__c>();
        List<KARL_Evidence_Request__Share> evidenceShareList = new List<KARL_Evidence_Request__Share>();
        Set<Id> requestedByIdSet = new Set<Id>();
        List<SObject> junctionObjectList = new List<SObject>();
        List<SObject> destructiveJuntionObjectList = new List<SObject>();
        for(KARL_Service_Desk__c serviceDeskObj : newList){
            if(oldMap != null && oldMap.get(serviceDeskObj.Id).Status__c != serviceDeskObj.Status__c && serviceDeskObj.Status__c == KARL_Constants.STATUS_APPROVED){
                if(serviceDeskObj.RecordTypeId == KARL_Constants.REQUEST_MASTER_DATA_RECORDTYPEID){
                    Request_Item__c reqItemObj = new Request_Item__c();
                    if(String.isNotBlank(serviceDeskObj.New_Request_Name__c)){
                        reqItemObj.Request_Name__c = serviceDeskObj.New_Request_Name__c;
                    }
                    if(serviceDeskObj.Area_of_Compliance__c != null)
                    reqItemObj.Areas_of_Compliance__c = serviceDeskObj.Area_of_Compliance__c;
                    if(serviceDeskObj.Business_Unit__c != null)
                    reqItemObj.Primary_Scope__c = serviceDeskObj.Business_Unit__c;
                    reqItemObj.Request_Description__c = serviceDeskObj.Request_Description__c;
                    if(serviceDeskObj.Type__c != null)
                    reqItemObj.Type__c = serviceDeskObj.Type__c;
                    if(serviceDeskObj.New_Or_Existing__c == KARL_Constants.NEWOREXISTING_EXISTING){
                        reqItemObj.Id = serviceDeskObj.Request_Name__c;
                        reqItemObj.Update_Current_Audit_Cycle_s__c = 'Yes';
                        if(String.isNotBlank(serviceDeskObj.Area_Requirement_Ids__c)){
                            Set<String> areaRequirementIdSet = new Set<String>();
                            areaRequirementIdSet.addAll(serviceDeskObj.Area_Requirement_Ids__c.split(','));
                            requestItemIdToAreaRequirementMap.put(serviceDeskObj.Request_Name__c,areaRequirementIdSet);
                        }
                        else{
                            requestItemIdToAreaRequirementMap.put(serviceDeskObj.Request_Name__c,new Set<String>());
                        }
                        if(String.isNotBlank(serviceDeskObj.Control_Number_Ids__c)){
                            Set<String> controlNumberIdSet = new Set<String>();
                            controlNumberIdSet.addAll(serviceDeskObj.Control_Number_Ids__c.split(','));
                            requestItemIdToControlTestMap.put(serviceDeskObj.Request_Name__c,controlNumberIdSet);
                        }
                        else{
                            requestItemIdToControlTestMap.put(serviceDeskObj.Request_Name__c,new Set<String>());
                        }
                    }
                    else{ //For New
                        reqItemObj.Update_Current_Audit_Cycle_s__c = 'No';
                        reqItemObj.Priority__c = 'Wave 3';
                        // reqItemObj.KARL_GRC_Assignee__c = serviceDeskObj.GRC_Assignee__c;
                        serviceDeskIdToRequestItemMap.put(serviceDeskObj.Id,reqItemObj);
                        newServiceDeskIdToMap.put(serviceDeskObj.Id,serviceDeskObj);
                    }
                    requestItemList.add(reqItemObj);
                }
                else{
                    //evidence request
                    if(serviceDeskObj.Evidence_Request_Name__c != null && serviceDeskObj.Requested_By__c != null){
                        requestedByIdSet.add(serviceDeskObj.Requested_By__c);
                    }   
                }
            }
        }
        if(!requestedByIdSet.isEmpty()){
            Map<Id,Id> contactIdToUserIdMap = new Map<Id,Id>();
            for(User userInfoObj : [SELECT Id,contactId FROM User WHERE ContactId IN :requestedByIdSet]){
                contactIdToUserIdMap.put(userInfoObj.contactId,userInfoObj.Id);
            }
            if(!contactIdToUserIdMap.isEmpty()){
                for(KARL_Service_Desk__c serviceDeskObj : newList){
                    if(oldMap != null && oldMap.get(serviceDeskObj.Id).Status__c != serviceDeskObj.Status__c && serviceDeskObj.Status__c == KARL_Constants.STATUS_APPROVED
                        && serviceDeskObj.RecordTypeId == KARL_Constants.EVIDENCE_REQUEST_RECORDTYPEID && serviceDeskObj.Evidence_Request_Name__c != null && serviceDeskObj.Requested_By__c != null
                        && contactIdToUserIdMap.containsKey(serviceDeskObj.Requested_By__c)){
                        KARL_Evidence_Request__Share evidenceShareObj = new KARL_Evidence_Request__Share();
                        evidenceShareObj.ParentId = serviceDeskObj.Evidence_Request_Name__c;
                        evidenceShareObj.UserOrGroupId = contactIdToUserIdMap.get(serviceDeskObj.Requested_By__c);
                        evidenceShareObj.AccessLevel = 'edit';
                        evidenceShareObj.RowCause = 'Manual';
                        evidenceShareList.add(evidenceShareObj);
                    }
                }
            }
        }
        if(!requestItemList.isEmpty()){
            upsert requestItemList;
            List<KARL_Service_Desk__c> serviceDeskList = new List<KARL_Service_Desk__c>();
            for(Id serviceDeskId : serviceDeskIdToRequestItemMap.keySet()){
                KARL_Service_Desk__c serviceDeskObj = new KARL_Service_Desk__c();
                serviceDeskObj.Id = serviceDeskId;
                serviceDeskObj.Request_Name__c = serviceDeskIdToRequestItemMap.get(serviceDeskId).Id;
                serviceDeskList.add(serviceDeskObj);
                if(newServiceDeskIdToMap.containsKey(serviceDeskId)){
                    KARL_Service_Desk__c existingServiceDeskObj = newServiceDeskIdToMap.get(serviceDeskId);
                    if(String.isNotBlank(existingServiceDeskObj.Area_Requirement_Ids__c)){
                        for(String areaRequirementId : existingServiceDeskObj.Area_Requirement_Ids__c.split(',')){
                            junctionObjectList.add(new Request_Item_Area__c(Request_Master_Data__c = serviceDeskObj.Request_Name__c,Area_Requirement__c = areaRequirementId));
                        }
                    }
                    if(String.isNotBlank(existingServiceDeskObj.Control_Number_Ids__c)){
                        for(String controlTestId : existingServiceDeskObj.Control_Number_Ids__c.split(',')){
                            junctionObjectList.add(new Request_Item_Control__c(Request__c = serviceDeskObj.Request_Name__c,Test__c = controlTestId));
                        }
                    }
                }
            }
            if(!serviceDeskList.isEmpty()){
                update serviceDeskList;
            }
        }
        if(!requestItemIdToAreaRequirementMap.isEmpty()){
            for(Request_Item__c reqItemObj : [SELECT Id,Request_Name__c,KARL_GRC_Assignee__c,Type__c,Areas_of_Compliance__c,Primary_Scope__c,Request_Description__c,
                                                    (SELECT Id FROM Request_Item_Areas__r),
                                                    (SELECT Id FROM Request_Item_Controls__r)
                                                FROM Request_Item__c 
                                                WHERE Id IN :requestItemIdToAreaRequirementMap.keySet()
                                                WITH SECURITY_ENFORCED]){
                Set<String> areaRequirementIdSet = requestItemIdToAreaRequirementMap.get(reqItemObj.Id);
                Set<String> controlTestIdSet = requestItemIdToControlTestMap.get(reqItemObj.Id);
                for(Request_Item_Area__c reqItemAreaObj : reqItemObj.Request_Item_Areas__r){
                    if(areaRequirementIdSet.contains(reqItemAreaObj.Id)){
                        areaRequirementIdSet.remove(reqItemAreaObj.Id);
                    }
                    else{
                        destructiveJuntionObjectList.add(reqItemAreaObj);
                    }
                }
                requestItemIdToAreaRequirementMap.put(reqItemObj.Id,areaRequirementIdSet);
                for(Request_Item_Control__c reqItemControlObj : reqItemObj.Request_Item_Controls__r){
                    if(controlTestIdSet.contains(reqItemControlObj.Id)){
                        controlTestIdSet.remove(reqItemControlObj.Id);
                    }
                    else{
                        destructiveJuntionObjectList.add(reqItemControlObj);
                    }
                }
                requestItemIdToControlTestMap.put(reqItemObj.Id,controlTestIdSet);
            }
            for(Id reqItemId : requestItemIdToAreaRequirementMap.keySet()){
                for(String areaRequirementId : requestItemIdToAreaRequirementMap.get(reqItemId)){
                    junctionObjectList.add(new Request_Item_Area__c(Request_Master_Data__c = reqItemId,Area_Requirement__c = areaRequirementId));
                }
            }
            for(Id reqItemId : requestItemIdToControlTestMap.keySet()){
                for(String controlTestId : requestItemIdToControlTestMap.get(reqItemId)){
                    junctionObjectList.add(new Request_Item_Control__c(Request__c = reqItemId,Test__c = controlTestId));
                }
            }
        }
        if(!destructiveJuntionObjectList.isEmpty()){
            delete destructiveJuntionObjectList;
        }
        if(!junctionObjectList.isEmpty()){
            insert junctionObjectList;
        }
        if(!evidenceShareList.isEmpty()){
            insert evidenceShareList;
        }
    }

     /**
    * @author Lakshya Arora
    * @email lakshya.arora@mtxb2b.com
    * @description Create SLA Tracking records
    * @param1 New List of KARL_Service_Desk__c
    * @param2 OldMap -> Map of Id vs old KARL_Service_Desk__c record info
    * @note Call on after insert,after Update
    */
    public static void createSLATrackingRecords(List<KARL_Service_Desk__c> newList,Map<Id,KARL_Service_Desk__c> oldMap){
        List<KARL_SLA__c> slaTrackingList = new List<KARL_SLA__c>();
        Set<Id> serviceDeskId = new Set<Id>();
        for(KARL_Service_Desk__c serviceDeskObj : newList){
            if(oldMap == null || oldMap.get(serviceDeskObj.Id).Status__c != serviceDeskObj.Status__c){
                KARL_SLA__c slaTrackingObj = new KARL_SLA__c();
                slaTrackingObj.Date_Changed__c = datetime.now();
                slaTrackingObj.Changed_By__c = serviceDeskObj.lastModifiedById;
                slaTrackingObj.KARL_Service_Desk__c = serviceDeskObj.Id;
                slaTrackingObj.Start_Date__c = Datetime.now();
                slaTrackingObj.Start_Status__c = serviceDeskObj.Status__c;
                slaTrackingList.add(slaTrackingObj);
                if(oldMap != null){
                    serviceDeskId.add(serviceDeskObj.Id);
                }
            }
        }
        if(!serviceDeskId.isEmpty()){
            for(KARL_Service_Desk__c serviceDeskObj : [SELECT Id,lastModifiedById,Status__c,
                                                    (SELECT Id,Changed_By__c,Date_Changed__c,End_Status__c,End_Date__c FROM KARL_SLAs__r ORDER BY CreatedDate DESC LIMIT 1)
                                                 FROM KARL_Service_Desk__c 
                                                 WHERE Id IN :serviceDeskId 
                                                 WITH SECURITY_ENFORCED]){
                if(!serviceDeskObj.KARL_SLAs__r.isEmpty()){
                    KARL_SLA__c oldRecord = serviceDeskObj.KARL_SLAs__r[0];
                    oldRecord.Changed_By__c = serviceDeskObj.lastModifiedById;
                    oldRecord.Date_Changed__c = datetime.now();
                    oldRecord.End_Status__c = serviceDeskObj.Status__c;
                    oldRecord.End_Date__c = Datetime.now();
                    slaTrackingList.add(oldRecord);
                }
            }
        }
        if(!slaTrackingList.isEmpty()){
            upsert slaTrackingList;
        } 
    }


    /**
    * @author Lakshya Arora
    * @email lakshya.arora@mtxb2b.com
    * @description get latest autonumber by recordtype Id
    * @param recordTypeId of KARL_Service_Desk Object
    * @return latest autonumber
    */
    private static Integer getLatestServiceDeskNoByRecordTypeId(String serviceDeskRecordTypeId){
        Integer latestRecordNumber = 0;
        List<KARL_Service_Desk__c> serviceDeskList = new List<KARL_Service_Desk__c>([SELECT Id,Name,Auto_Number_Apex__c 
                                                                                    FROM KARL_Service_Desk__c 
                                                                                    WHERE RecordTypeId =:serviceDeskRecordTypeId 
                                                                                    WITH SECURITY_ENFORCED
                                                                                    Order By Auto_Number_Apex__c DESC 
                                                                                    LIMIT 1
                                                                                    ]);
        if(!serviceDeskList.isEmpty()){
            if(serviceDeskList[0].Auto_Number_Apex__c != null){
                latestRecordNumber = Integer.valueOf(serviceDeskList[0].Auto_Number_Apex__c);
            }   
        }
        return latestRecordNumber;
    }

    /**
    * @author Lakshya Arora
    * @email lakshya.arora@mtxb2b.com
    * @description generate auto number
    * @param1 autonumber String
    * @param2 preccedingZeroesLength
    * @param3 prefix for autonumber
    * @return Autonumber of record type
    */
    private static String generateAutoNumber(String autoNumber,Integer preceedingZeroes,String prefix){
        while(autoNumber.length() < preceedingZeroes){
            autoNumber = '0'+autoNumber;
        }
        return prefix+autoNumber;
    }




     /**
    * @author Lakshya Arora
    * @email lakshya.arora@mtxb2b.com
    * @description Create chatter when status changes to approved or rejected
    * @param1 New List of KARL_Service_Desk__c
    * @param2 OldMap -> Map of Id vs old KARL_Service_Desk__c record info
    * @note Call on Before Update
    */
    //commented as we need only email notifications
    // public static void createChatterOnStatusChange(List<KARL_Service_Desk__c> newList,Map<Id,KARL_Service_Desk__c> oldMap){
    //     List<ConnectApi.BatchInput> batchInputs = new List<ConnectApi.BatchInput>(); 
    //     for(KARL_Service_Desk__c serviceDeskObj : newList){
    //         if((serviceDeskObj.Status__c == KARL_Constants.STATUS_REJECTED || serviceDeskObj.Status__c == KARL_Constants.STATUS_APPROVED) 
    //                 && oldMap != null 
    //                 && oldMap.get(serviceDeskObj.Id).Status__c != serviceDeskObj.Status__c){
    //                     ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
    //                     ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
    //                     ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
    //                     ConnectApi.TextSegmentInput textSegmentInput;
    //                     messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();
    //                     textSegmentInput = new ConnectApi.TextSegmentInput();
    //                     mentionSegmentInput.id = serviceDeskObj.CreatedById;
    //                     messageBodyInput.messageSegments.add(mentionSegmentInput);
    //                     if(serviceDeskObj.Status__c == KARL_Constants.STATUS_REJECTED){
    //                         textSegmentInput.text = ' Approval request for '+serviceDeskObj.Name+' is rejected. Reason = '+serviceDeskObj.Rejection_Reason__c;
    //                     }
    //                     else if(serviceDeskObj.Status__c == KARL_Constants.STATUS_APPROVED && serviceDeskObj.RecordTypeId == KARL_Constants.REQUEST_MASTER_DATA_RECORDTYPEID){
    //                         textSegmentInput.text = ' Approval request for '+serviceDeskObj.Name+' is approved ';
    //                     }
    //                     messageBodyInput.messageSegments.add(textSegmentInput);
    //                     feedItemInput.body = messageBodyInput;
    //                     feedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
    //                     feedItemInput.subjectId = serviceDeskObj.Id;
    //                     batchInputs.add(new ConnectApi.BatchInput(feedItemInput));
    //         }
    //     }
    //     if(!batchInputs.isEmpty() && !Test.isRunningTest()){
    //         ConnectApi.ChatterFeeds.postFeedElementBatch(Network.getNetworkId(), batchInputs);
    //     } 
    // }
}
/**
* @author Lakshya Arora
* @email lakshya.arora@mtxb2b.com
* @desc Class handles the karl_completion_readiness operations
*/  
public with sharing class KARL_FinalizationReadinessController {

     /**
    * @author Lakshya Arora
    * @email lakshya.arora@mtxb2b.com
    * @desc method to fetch the Cycle audit report Information.
    * @param: recordId -> Cycle Audit report record Id
    */
    @AuraEnabled
    public static FinalizationReadinessWrapper getFinalizationReadinessInfo(String recordId){
        FinalizationReadinessWrapper finalizationReadinessWrapObj = new FinalizationReadinessWrapper();
        for(KARL_Cycle_Audit_Report_Items__c cycleAuditReportItemObj : [SELECT Id,Auditor_Readiness_Confirmation__c,Management_Responses_Approved__c,Audit_Report_Final_Draft_Confirmed__c,
                                                                        Approved_by_Legal__c,Audit_Letter_Temps_Formatted__c,Basis_of_Assertion_BOA__c,VP_Approved__c,Status__c
                                                                        FROM KARL_Cycle_Audit_Report_Items__c
                                                                        WHERE Id =:recordId
                                                                        WITH SECURITY_ENFORCED]){
            finalizationReadinessWrapObj.auditorReadinessConfirmation = cycleAuditReportItemObj.Auditor_Readiness_Confirmation__c;
            finalizationReadinessWrapObj.managementResponseApproved = cycleAuditReportItemObj.Management_Responses_Approved__c;
            finalizationReadinessWrapObj.auditReportFinalDrafConfirmed = cycleAuditReportItemObj.Audit_Report_Final_Draft_Confirmed__c;
            finalizationReadinessWrapObj.approvedByLegal = cycleAuditReportItemObj.Approved_by_Legal__c;
            finalizationReadinessWrapObj.auditLetterTempsFormatted = cycleAuditReportItemObj.Audit_Letter_Temps_Formatted__c;
            finalizationReadinessWrapObj.basisOfAssertionBOA = cycleAuditReportItemObj.Basis_of_Assertion_BOA__c;
            finalizationReadinessWrapObj.vpApproved = cycleAuditReportItemObj.VP_Approved__c;
            finalizationReadinessWrapObj.statusReadyToSign = cycleAuditReportItemObj.Status__c == 'Ready to Sign' ? true : false;
            finalizationReadinessWrapObj.statusPending = cycleAuditReportItemObj.Status__c == 'Pending' ? true : false;
        }
        System.debug('1 >> ' + finalizationReadinessWrapObj);
        return finalizationReadinessWrapObj;
    }

    /**
    * @author Lakshya Arora
    * @email lakshya.arora@mtxb2b.com
    * @desc method to update the Cycle audit report record.
    * @param: fieldName -> name of field,
    * @param: fieldValue -> value of field
    * @param: recordId -> Cycle Audit report record Id
    */
    @AuraEnabled
    public static void updateCycleAuditReport(String fieldName,Boolean fieldValue,String recordId){
      KARL_Cycle_Audit_Report_Items__c cycleAuditReportObj = new KARL_Cycle_Audit_Report_Items__c();
      cycleAuditReportObj.put('Id',recordId); 
      cycleAuditReportObj.put(fieldName,fieldValue);
      try{
        update cycleAuditReportObj;
      }
      catch(Exception e){
        if(e.getMessage().contains(KARL_CycleAuditReportsTriggerHelper.KARL_STATUS_ERROR)){
          throw new AuraHandledException('Error Occurred: '+KARL_CycleAuditReportsTriggerHelper.KARL_STATUS_ERROR);
        }
        else if(e.getMessage().contains(KARL_CycleAuditReportsTriggerHelper.KARL_ONLY_STATUS_CHANGE)){
          throw new AuraHandledException('Error Occurred: '+KARL_CycleAuditReportsTriggerHelper.KARL_ONLY_STATUS_CHANGE);
        }
        else{
          throw new AuraHandledException('Error Occurred: '+e.getMessage());
        }
      }
    }

    public class FinalizationReadinessWrapper{
        @AuraEnabled public Boolean auditorReadinessConfirmation;
        @AuraEnabled public Boolean managementResponseApproved;
        @AuraEnabled public Boolean auditReportFinalDrafConfirmed;
        @AuraEnabled public Boolean approvedByLegal;
        @AuraEnabled public Boolean auditLetterTempsFormatted;
        @AuraEnabled public Boolean basisOfAssertionBOA;
        @AuraEnabled public Boolean vpApproved;
        @AuraEnabled public Boolean statusReadyToSign;
        @AuraEnabled public Boolean statusPending;
    }
}
public class ESA_RequestCancelAppointment {
    
    @future(callout=true)
    public static void webServiceCallout (String reqId){
     
        Esa_Cancel_Appointment.cancel_Appointment(reqId);
   
    }
    
    
    
    public static void cancelAppoOnReq (List <Case> casesL){
        
  
        for (Case cs: casesL){
            
                if (cs.ESA_Security_Request__c != null && cs.Cancel_Office_Hours__c){
                    
                        if ( !system.isFuture()){
                          webServiceCallout(cs.ESA_Security_Request__c);
                    }
                    
                       
                        
                    }
                    
                }
            }
        }
@isTest
public class SPT_CaseDetailPageController_Test {
    @TestSetup
    static void makeData(){
        List<Account> accountList = SPT_TestUtility.createAccountRecords('SPT Test Account', 1, true);
        List<Contact> contactList = SPT_TestUtility.createContactRecords('SPT Test Contact', 1, false);
        List<Profile> p = [SELECT Id FROM Profile WHERE Name = 'System Administrator']; 
        User u = new User(Email='test-user@email.com', ProfileId = p[0].Id, 
                          UserName='test-user@email.com', alias='tuser1', CommunityNickName='tuser1', 
                          TimeZoneSidKey='America/New_York', LocaleSidKey='en_US', EmailEncodingKey='ISO-8859-1', 
                          LanguageLocaleKey='en_US', FirstName = 'Test', LastName = 'User',
                          EmployeeNumber = '123456' );
        INSERT u;

        SupportForceUser__x sfUserRecord = new SupportForceUser__x();
        Database.insertAsync(sfUserRecord);

        for(Integer i=0; i<contactList.size(); i++) {
            Contact contactRecord = contactList.get(i);
            contactRecord.AccountId = accountList.get(i).Id;
        }
        if(!contactList.isEmpty()) {
            INSERT contactList;
        }

        Contact__x sfContactRecord = new Contact__x();
        Database.insertAsync(sfContactRecord);

        Case__x externalCaseRecord = new Case__x();
        Database.insertAsync(externalCaseRecord);

        List<Case> internalCaseList = SPT_TestUtility.createCaseRecords('New', 'USD', 'Web', 1, false);
        for(Case internalCaseRecord : internalCaseList) {
            internalCaseRecord.SupportForce_Case_Id__c = externalCaseRecord.Id;
            internalCaseRecord.OwnerId = u.Id;
            internalCaseRecord.ContactId = contactList.get(0).Id;
            internalCaseRecord.Description = 'Test Description';
        }
        INSERT internalCaseList;
        
        List<Case> largeDescriptionCaseList = SPT_TestUtility.createCaseRecords('New', 'USD', 'Email', 1, false);
        for(Case internalCaseRecord : largeDescriptionCaseList) {
            internalCaseRecord.Description = 'Desk.com is a saas help desk and customer support product accessible through the cloud. Desk.com is owned by Salesforce.com and was previously known as Assistly. Desk.com is headquartered in San Francisco, California. After being acquired by Salesforce.com for $50 million in 2011[55] Assistly was re-branded as Desk.com in 2012[56] as a customer support software.[57] Desk.com is a Saas customer service application. The product differentiates itself from Salesforces other service platform in that Desk.com specifically targets small businesses with its features and functions.[58] Desk.com integrates with a variety of products and third-party applications including Salesforce CRM, Salesforce IQ,[59] Atlassian JIRA, Mailchimp[60] and other apps. Desk.com also supports up to 50 languages.[61] Salesforce announced the retirement of desk.com, replacing it with Service Cloud Lightning. After March 13, 2018 no new desk.com licenses were sold, and the retirement date was announced as March 13, 2020.[62] Do.com was a cloud-based task management system for small groups and businesses, introduced in 2011 and discontinued in 2014.[63][64][65] Salesforce did not offer any reason for shutting down the service, however, it provided an Export tool to save data entered within the Do.com interface. The Do.com domain was sold to a startup in 2014.[66] Configuration Salesforce users can configure their CRM application. In the system, there are tabs such as "Contacts," "Reports," and "Accounts." Each tab contains associated information. Configuration can be done on each tab by adding user-defined custom fields.[67] Configuration can also be done at the "platform" level by adding configured applications to a Salesforce instance, that is adding sets of customized / novel tabs for specific vertical- or function-level (Finance, Human Resources, etc.) features.';
        }
        INSERT largeDescriptionCaseList;
    }

    @isTest
    public static void validateCaseUpdateTest() {
        Map<String, String> fieldsData = new Map<String, String>();
        List<Case> internalCaseList = new List<Case>([SELECT Id FROM Case]);
        Test.startTest();
        SPT_CaseDetailPageController.validateCaseUpdate(internalCaseList.get(0).Id, fieldsData);
        Test.stopTest();
    }

    @isTest
    public static void refreshContTest() {
        List<Case> internalCaseList = new List<Case>([SELECT Id FROM Case]);
        Test.startTest();
        SPT_CaseDetailPageController.refreshRecordCont(internalCaseList.get(0).Id);
        Test.stopTest();
    }

    @isTest
    public static void updateInsertSupportCaseRecordTest() {
        List<Case> internalCaseList = new List<Case>([SELECT Id FROM Case]);
        for(Case caseRecord : internalCaseList) {
            caseRecord.Status = 'Review';
        }
        if(internalCaseList != null && !internalCaseList.isEmpty()) {
            UPDATE internalCaseList;
        }
    }

    @isTest
    public static void getEntSecRecordTypeIdTest() {
        String recordTypeId = SPT_CaseDetailPageController.getEntSecRecordTypeId();
        System.assertEquals(true, recordTypeId != null);
    }
    
    @isTest
    public static void getLayoutFieldsTest() {
        List<String> layoutFieldsList = new List<String>();
        layoutFieldsList = SPT_CaseDetailPageController.getLayoutFields();
        System.assertEquals(false, layoutFieldsList.contains('Description'));
    }
    
    @isTest
    public static void getSmallDescriptionValueTest() {
        List<Case> internalCaseList = new List<Case>([SELECT Id FROM Case]);
        SPT_CaseDetailPageController.DescriptionWrapper wrapper = SPT_CaseDetailPageController.getDescriptionValue(internalCaseList.get(0).Id, 'Show Less');
        System.assertEquals(false, wrapper.descriptionVal.contains(' ... '));
    }
    
    @isTest 
    public static void getLargeDescriptionValueTest() {
		List<Case> internalCaseList = new List<Case>([SELECT Id FROM Case WHERE Origin = 'Email']);
        SPT_CaseDetailPageController.DescriptionWrapper wrapper = SPT_CaseDetailPageController.getDescriptionValue(internalCaseList.get(0).Id, 'Show Less');
        System.assertEquals(true, wrapper.descriptionVal.contains(' ... '));
        
        SPT_CaseDetailPageController.DescriptionWrapper wrapper2 = SPT_CaseDetailPageController.getDescriptionValue(internalCaseList.get(0).Id, 'Show More');
        System.assertEquals(false, wrapper2.descriptionVal.contains(' ... '));
    }
}
global class ProcessSDPAlerts2 implements Messaging.InboundEmailHandler {
 
 // Processes health monitoring alerts sent to sdp_alerts@salesforce.com
 
  global Messaging.InboundEmailResult handleInboundEmail(Messaging.inboundEmail email, 
                                                       Messaging.InboundEnvelope env){
 
    // Create an InboundEmailResult object for returning the result of the 
    // Apex Email Service
    Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();
  
    String myText= '';
    String mySubject = '';
    Integer SDPAlertType;
    String caseStatus = 'New';   
    
    mySubject = email.subject;
    
    If (email.plainTextBody != null) {
        myText = email.plainTextBody;
        }
    else
        {
        myText = email.htmlBody;
        }
    System.debug('DEBUG: Subject - ' + mySubject);    
    System.debug('DEBUG: Body - ' + myText); 
// The following fields may be parsed from alerts sent to sdp_alerts@salesforce.com:

    String alertType = 'UNKNOWN'; // required. 'UNKNOWN' is the default.
    String alertSystem = ''; // optional
    String alertService; // optional

// Identify SDP Alert Type(s)

// Alert Type 1 - Argus alert

    Pattern argusPattern = Pattern.compile('\\[Argus\\].*');
    Matcher isArgus = argusPattern.matcher(mySubject);

    if( isArgus.matches() ){
         SDPAlertType = 1;
         System.debug('DEBUG: SDPAlertType = ArgusAlert'); 
    }

// Alert Type 2 - Sourcefire

    Pattern sourcefirePattern = Pattern.compile('SF Health Monitor Alert.*');
    Matcher isSF = sourcefirePattern.matcher(mySubject);

    if( issf.matches() ){
        SDPAlertType = 2;
        System.debug('DEBUG: SDPAlertType = SourcefireAlert');
        }
    
// Process Argus Alert

    if (SDPAlertType == 1)    
    {    
        Pattern argusAlertType = Pattern.compile('.*Alert: ([A-Z,a-z,-]*).*');
        Matcher pmSubject = argusAlertType.matcher(mySubject);

        if( pmSubject.matches() ){
            alertType = pmSubject.group(1);
            System.debug('DEBUG: Matched - ' + pmSubject.group(1)); 
        }
        // TBD - strip FQDN.
        Pattern argusSystem = Pattern.compile('(?s).+?Triggered on Metric:.+?device=([^\\^}^\\.]+).+');
        Matcher pmBody = argusSystem.matcher(myText);

        if( pmBody.matches() ){
            alertSystem = pmBody.group(1);
            System.debug('DEBUG: Matched - ' + pmBody.group(1)); 
        }
        
        String clearedString = 'was Cleared';
        
        if ( myText.indexOf(clearedString) != -1 ){
                    System.debug('DEBUG: Matched Clear Alert'); 
            alertType = alertType + '-Cleared';
           }
      }     
// Sourcefire alert

    if (SDPAlertType == 2)    
    {    
       Pattern sfAlertType = Pattern.compile('(?s).+?Module: ([a-zA-Z ]*).+');
        Matcher pmBodyAlert = sfAlertType.matcher(myText);

        if( pmBodyAlert.matches() ){
            String regExp = ' ';
            alertType = pmBodyAlert.group(1).replaceAll(regExp,'_');
            System.debug('DEBUG: Matched - ' + alertType); 
        }

        Pattern sfSystem = Pattern.compile('(?s)Health Monitor Alert from ([A-Za-z-0-9]*).*');
        Matcher pmBodySystem = sfSystem.matcher(myText);

        if( pmBodySystem.matches() ){
            alertSystem = pmBodySystem.group(1);
            System.debug('DEBUG: Matched - ' + pmBodySystem.group(1)); 
        }
        
        

// Add processing for other alert types here. For example, Splunk index and search latency alerts.
    
    
   

    }
    try {

    String caseSubject = mySubject;
    String caseDescription = myText;
    
    List<RecordType> rtIDCase = [SELECT Id
    FROM RecordType
    WHERE Name = 'SDC Alert'];
    
    // Lookup SDP Alert ID.
    
    List<SDCloud__SDP_Alert__c> alertTypeId = [SELECT Id, SDCloud__Status__c FROM SDCloud__SDP_Alert__c
    WHERE Name = :alertType];
    
    String myalertId;
    String myalertStatus;
    
    IF (alertTypeId.isEmpty() == FALSE)
    {
        myalertId = alertTypeId.get(0).Id;
        myalertStatus = alertTypeId.get(0).SDCloud__Status__c;
        if (myalertStatus == 'Not Active')
        {
            System.debug('Alert Type not active, closing case......');
            caseStatus = 'Closed';
        }
    }
        else // handling case where alert matches parsing logic above but alert type not found.
        {
                List<SDCloud__SDP_Alert__c> alertTypeId2 = [SELECT Id, SDCloud__Status__c FROM SDCloud__SDP_Alert__c
    WHERE Name = 'UNKNOWN'];
                  myalertId = alertTypeId2.get(0).Id;
        myalertStatus = alertTypeId2.get(0).SDCloud__Status__c;
  
        }
   
   // Lookup Service ID.
    
    List<SDCloud__Service__c> serviceId = [SELECT Id
    FROM SDCloud__Service__c
    WHERE Name = :alertService];
    
    String myserviceId;
    
    IF (serviceId.isEmpty() == FALSE)
    {
        myserviceId = serviceId.get(0).Id;
    }
    
   List<Case> openCases = new List<Case>();     
   List<SDCloud__System__c> matchingSystems;
       
    if (alertSystem.length()>0)
    {
    
    
        // Find the system.
        System.debug('Finding the system.....');
        matchingSystems = [SELECT Id, SDCloud__Status__c
        FROM SDCloud__System__c
        WHERE SDCloud__Hostname__c LIKE :(alertSystem + '%')];  
    
        // If the system is not active, set case to Closed.
        IF (matchingSystems.isEmpty() == FALSE) {
            IF (matchingSystems.get(0).SDCloud__Status__c <> 'Active') {
                System.debug('System not active - closing case....');
                caseStatus = 'Closed';
                }
              openCases = [SELECT Id FROM Case WHERE SDCloud__SDP_Alert__c = :myalertId AND Status <> 'Closed' AND Id IN (SELECT SDCloud__Case__c FROM SDCloud__System_to_Case__c WHERE SDCloud__System__c = :matchingSystems.get(0).Id)];

        }}

    System.debug('ABOUT TO CREATE CASE....');

    IF (openCases.isEmpty() == FALSE) {
    FeedItem fp = new FeedItem();
    fp.Body = 'New alert - ' + myText;
    fp.parentId = openCases.get(0).Id;
    insert fp;

    }
    else
    {
     Case caseObj = new Case(Status = caseStatus,
    recordtypeid = rtIDCase.get(0).Id,
    subject = caseSubject, 
    description = caseDescription,
    SDCloud__Impacted_Service__c = myserviceId,
    SDCloud__SDP_Alert__c = myalertId);   
        
    insert caseObj;
    
    
    
    // Case created. Now create the system <-> case relationship.
    
    IF (alertSystem.length()>0)
    {
    IF (matchingSystems.isEmpty() == FALSE)
    {
    System.debug('Creating system <-> case relationship.....');
    SDCloud__System_to_Case__c STCObj = new SDCloud__System_to_Case__c(SDCloud__Case__c = caseObj.Id,
    SDCloud__System__c = matchingSystems.get(0).Id);
    
    insert STCObj;     
    
    }}

}
    }

   catch (QueryException e) {
       System.debug('Exception');
   }
   
   result.success = true;
   
   // Return the result for the Apex Email Service
   return result;
  }
}
public class ESA_CaseReminderEmail {
    
    public static List<Messaging.SingleEmailMessage> allmsg = new List<Messaging.SingleEmailMessage>(); 
    public static List<CaseComment> comments = new List <CaseComment>();
    //invocable method for process builder remainder
    //parameters id 
    @InvocableMethod
    public static void templateEmailSender (List <Case> caseId){
      
		EmailTemplate template;

        // we check all the conditions for triggering the email, field updates and comment
        for (Case caseForEmail: caseId){
            if (caseForEmail.Reminder1__c != null && caseForEmail.Reminder2__c == null && caseForEmail.Send_Close__c == false){
            
            	template = [SELECT Id, Body FROM EmailTemplate WHERE Name =: caseForEmail.Reminder1_Email_Template__c];
            	Date reminder = caseForEmail.Reminder1__c;
            	sendEmail(caseForEmail, template, reminder);           
        }
            else if (caseForEmail.Reminder1__c != null && caseForEmail.Reminder2__c != null && caseForEmail.Reminder3__c == null
                     && caseForEmail.Send_Close__c == false){
                template = [SELECT Id, Body FROM EmailTemplate WHERE Name =: caseForEmail.Reminder2_Email_Template__c];
            	Date reminder = caseForEmail.Reminder2__c;
            	sendEmail(caseForEmail, template, reminder);
                
            }
			else if (caseForEmail.Reminder1__c != null && caseForEmail.Reminder2__c != null && caseForEmail.Reminder3__c != null 
                    && caseForEmail.Reminder4__c == null && caseForEmail.Send_Close__c == false){
                template = [SELECT Id, Body FROM EmailTemplate WHERE Name =: caseForEmail.Reminder3_Email_Template__c];
            	Date reminder = caseForEmail.Reminder3__c;
            	sendEmail(caseForEmail, template, reminder);
                
            }
            else if (caseForEmail.Reminder1__c != null && caseForEmail.Reminder2__c != null && caseForEmail.Reminder3__c != null 
                     && caseForEmail.Reminder4__c != null && caseForEmail.Send_Close__c == false){
                         
                        template = [SELECT Id, Body FROM EmailTemplate WHERE Name =: caseForEmail.Reminder4_Email_Template__c];
            			Date reminder = caseForEmail.Reminder4__c;
            			sendEmail(caseForEmail, template, reminder);
                         
                         
                     }
            else if (caseForEmail.Send_Close__c == true && caseForEmail.Closing_Date__c != null){
                
                template = [SELECT Id, Body FROM EmailTemplate WHERE Name =: caseForEmail.Closing_template__c];
            	Date reminder = caseForEmail.Closing_Date__c;
            	sendEmail(caseForEmail, template, reminder);
                
            }
            
            
        }
        
        try {
            if(!Test.isRunningTest()) {
    			Messaging.sendEmail(allmsg);
                for (Messaging.SingleEmailMessage ml: allmsg){
            
            
            		createComment(ml.getWhatId(), ml.getPlainTextBody());
            
            
        }
              insert comments;
            	          	
            }
          
    		return;
	} catch (Exception e) {
    	System.debug(e.getMessage());
}
        
       
        
        
    }
        //method for sending the email for all the previous conditions
    public static void sendEmail (Case caseForEmail, EmailTemplate template, date reminder){
        
        //List<Messaging.SingleEmailMessage> allmsg = new List<Messaging.SingleEmailMessage>();              
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setUseSignature(false);
        
        OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'entsecapps@salesforce.com'];
        if ( owea.size() > 0 ) {
            mail.setOrgWideEmailAddressId(owea.get(0).Id); 
        }
        
        
        string [] toaddress;
        
        Contact contact = [select email, firstName, lastName from Contact where id =: caseForEmail.ContactId];
        
        toaddress = new String []{contact.email};
            
        mail.setToAddresses(toaddress);
        mail.setTemplateId(template.id);
        mail.setTargetObjectId(contact.Id); 
        mail.setWhatId(caseForEmail.id);
        mail.setSaveAsActivity(true);
        allmsg.add(mail);
       	//createComment(caseForEmail.id, allmsg[0].getPlainTextBody());
        
       /* try {
            //if(!Test.isRunningTest()) {
    			Messaging.sendEmail(allmsg);
            	//createComment(caseForEmail.id, allmsg[0].getPlainTextBody());
            	
            	
            //}
          
    		return;
	} catch (Exception e) {
    	System.debug(e.getMessage());
}*/
       
    }
//create comment method
    public static void createComment (id caseId, string tBody)    {
        
        CaseComment cc = new CaseComment();
        cc.CommentBody = tBody;
        cc.ParentId = caseId;
        cc.IsPublished = true;
        comments.add(cc);
        //insert cc;
        
        
    }
        
        
        
        
        
        
        
    }
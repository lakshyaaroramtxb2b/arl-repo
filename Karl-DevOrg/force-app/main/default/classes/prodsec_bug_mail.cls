global class prodsec_bug_mail Implements Schedulable{
    
    global void execute(SchedulableContext sc)
        {
            sendMail();
        }
    
    public List<bug_management__c> getbug_list(){
       
       string value = 'SELECT id, Gus_work_id__c, Name, Bug_Title__c, Cloud_name__c, Age__c, Status__c, Priority__c , Prodsec_owner__c FROM bug_management__c' +
            ' Where Closed__c = 0 AND RF_Blocker__c = TRUE AND comment_5__c = FALSE AND Action_item__c includes (\'Bug is Over Half Life\') ORDER BY Prodsec_owner__c ';
        List<bug_management__c> bugs = new List<bug_management__c>();
        
        bugs = database.query(value);
        return bugs;
    }
    
    public static void sendMail(){
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
		//List <Contact> Con= [select id, name from Contact where name =:'Prashanth Kannan']; 
		list<string> val = new list<string>();
        List<String> sendTo = new List<String>();
      	//sendTo.add('pkannan@salesforce.com');
      	mail.setToAddresses(sendTo); 
        
        mail.setReplyTo('prodsec@salesforce.com');
        Contact[] dummyContact = [Select Id From Contact Where Email = 'prodsec@salesforce.com' AND LastName = 'Product Security' Limit 1];
        
		if(dummyContact.isEmpty()){
    		Contact tmp = New Contact(LastName = 'Product Security', email = 'prodsec@salesforce.com');
    		insert tmp;
    		dummyContact.add(tmp);
		}
		system.debug(dummyContact[0].Id);
        
		//mail.setToAddresses(val);
		mail.setTargetObjectId(dummyContact[0].id); 
		mail.setSenderDisplayName('Product Security Bug Review'); 
		mail.setUseSignature(false); 
		mail.setBccSender(false); 
		mail.setSaveAsActivity(false); 
		EmailTemplate et=[Select id from EmailTemplate where Name=:'Bug Review Invite']; 
		mail.setTemplateId(et.id); 
		Messaging.SendEmailResult [] SendMail = Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
         
         

}
global class DCStatsBatchService implements Database.Batchable<Sobject>, Database.Stateful {
    
    global Map<Id, Integer> dseMap;

    global Contact con;
    
    global Database.QueryLocator start(Database.BatchableContext info) {
        /*String query =    ' SELECT Id, DetectionSecurityEventCriteria__c '
                        + ' FROM  '
                        + '    Detection_Security_Event__c '
                        + ' WHERE ' 
                        + '     CreatedDate = YESTERDAY ';*/
        
        String query =    ' SELECT Id '
                        + ' FROM  '
                        + ' Detection_Security_Event_Criteria__c ';
        
        dseMap = new Map<Id, Integer>();

        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<Sobject> scope) {
        
        for (Sobject so : scope)  {
            
            Detection_Security_Event_Criteria__c dsec = (Detection_Security_Event_Criteria__c)so;
            
            /*AggregateResult ar = [SELECT Count(id) 
             						FROM Detection_Security_Event__c 
             						WHERE DetectionSecurityEventCriteria__c =: dsec.id
                                    AND CreatedDate = YESTERDAY];*/
            
            //Integer v = Integer.valueOf(ar.get('Count(id)'));
            
            dseMap.put(dsec.id, 0);
			
            /*
            Detection_Security_Event__c dse = (Detection_Security_Event__c)so;
            
            if(dseMap.containsKey(dse.DetectionSecurityEventCriteria__c)) {
                
                Integer val = dseMap.get(dse.DetectionSecurityEventCriteria__c);
                val = val + 1;
                
                dseMap.put(dse.DetectionSecurityEventCriteria__c, val);
            }
            else
            {
                dseMap.put(dse.DetectionSecurityEventCriteria__c, 1);
            }
			*/
        }
        
    }

    global void finish(Database.BatchableContext BC) {
        
        for(id v : dseMap.keySet())
        {
            Integer count = dseMap.get(v);
 			
            DC_Stat__c  s = new DC_Stat__c ();
            
			s.Detection_Security_Event_Criteria__c = v;
			s.Events_Per_Day__c = count;
            //s.date__c = date.today().addDays(-1);
            
            insert s;
        }
    }
}
public with sharing class Squawker_NewRuleController {
    
    public List<SelectOption> logicFeildsOptions = new List<SelectOption>();
    public String objectToFollow {get;set;}
    
    private Boolean newRule;
    
    //logic rules
    ////NOTE feild values must have __c value for custom values or alert rule breaks
    public String ruleOne_Field {get;set;}
    public String ruleOne_Operation {get;set;}
    public String ruleOne_Value {get;set;}
    public String ruleTwo_Field {get;set;}
    public String ruleTwo_Operation {get;set;}
    public String ruleTwo_Value {get;set;}
    public String ruleThree_Field {get;set;}
    public String ruleThree_Operation {get;set;}
    public String ruleThree_Value {get;set;}
    
    //Our newly created rule object to be inserted into the DB
    private SquawkerRule__c squawkerRule;
    private Map<String, Schema.SObjectType> schemaMap;

    //Groups that will get notfications. SFDC public groups
    public SelectOption[] selectedGroups { get; set; }
    public SelectOption[] allGroups { get; set; }
    
    public String allOrgObjects;
 
    public Squawker_NewRuleController() 
    {   
        schemaMap = Schema.getGlobalDescribe();

		if(ApexPages.currentPage().getParameters().get('id') == null)
        {
        	squawkerRule = new SquawkerRule__c();
            
            newRule = true;
        }
        else
        {
        	SquawkerRule = [Select Id, Name, Description__c, ObjectToWatch__c, ObjectToWatchFriendly__c, 
                	AlertLogic__c, GroupsToNotify__c, Last_Edit_Date__c, LastModifiedBy.Name, 
                   	Groups_To_Notify_Friendly__c, RuleLogic__c
            		FROM SquawkerRule__c
                    WHERE Id = :ApexPages.currentPage().getParameters().get('id')];
            
            newRule = false;
            
            //Load rule data
            LoadCustomControlsFromRuleData();
        }
        
        BuildGroupLists();
    }
    
    private void LoadCustomControlsFromRuleData()
    {
        objectToFollow = SquawkerRule.ObjectToWatch__c ;
		getUpdateLogicFeilds(); 
        
        List<SquawkRuleLogic> ruleLogic = (List<SquawkRuleLogic>)JSON.deserialize(SquawkerRule.RuleLogic__c, List<SquawkRuleLogic>.class);
        
        System.debug('ruleLogic.size() = ' + ruleLogic.size());
        System.debug(ruleLogic);
        
        if(ruleLogic.size() > 0)
        {
            ruleOne_Field = ruleLogic[0].field;
            ruleOne_Operation = ruleLogic[0].op;
            ruleOne_Value = ruleLogic[0].value;
        }
        
        if(ruleLogic.size() > 1)
        {
            ruleTwo_Field = ruleLogic[1].field;
            ruleTwo_Operation = ruleLogic[1].op;
            ruleTwo_Value = ruleLogic[1].value;
        }
        
        if(ruleLogic.size() > 2)
        {
            ruleThree_Field = ruleLogic[2].field;
            ruleThree_Operation = ruleLogic[2].op;
            ruleThree_Value = ruleLogic[2].value;
        }
        
        selectedGroups = new List<SelectOption>();
        
        List<String> labelParts = SquawkerRule.Groups_To_Notify_Friendly__c.split(',');
        List<String> valueParts = SquawkerRule.GroupsToNotify__c.split(',');
        
		for(Integer i = 0; i < labelParts.size(); i++)
        	selectedGroups.add(new SelectOption(valueParts[i], labelParts[i]));            
    }

    //No control exists for multiple select control unless custom object has multiple options
    //so we have created our own SEE the component MultiselectPicklist
    private void BuildGroupLists()
    {
        //Otherwise AJAX calls cause the selected groups to reset
        if(selectedGroups == null)
            selectedGroups = new List<SelectOption>();
        
        List<Group> publicGroups = [SELECT Id, Name FROM Group WHERE Type = 'Regular'];

        allGroups = new List<SelectOption>();

        for ( Group g : publicGroups ) {

            SelectOption newOtp = new SelectOption(g.Id, g.Name);

            //if AJAX call then we dont't want to add item already moved to the Selected groups list
            if(!IsGroupCurrentlySeelcted(selectedGroups, newOtp))
                allGroups.add(newOtp);
        }
    }

    private Boolean IsGroupCurrentlySeelcted(SelectOption[] currentSelectedOptions, SelectOption cOpt)
    {
        for(Integer i = 0; i < currentSelectedOptions.size(); i++)
        {
            if(currentSelectedOptions[i] == cOpt)
                return true;
        }

        return false;
    }

    public void RefreshGroups()
    {
        BuildGroupLists();
    }

    public SquawkerRule__c getSquawkerRule() {
        return SquawkerRule;
    }
    
    public List<SelectOption> getAllOrgObjects() 
    {
        List<SelectOption> options = new List<SelectOption>();
        
        Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe(); 
        Set<String> standardObjects = new Set<String>();
        Set<String> customObjects = new Set<String>();
        Set<String> sObjects = new Set<String>();
        
        for(Schema.SObjectType d : gd.values())
        {
            Schema.DescribeSObjectResult ds = d.getDescribe();
            if(!ds.isCreateable())
              continue;
            if(ds.isCustom() == false && ds.getRecordTypeInfos().size() > 0)
                sObjects.add(ds.getName()); //std
            else if(ds.isCustom())
                sObjects.add(ds.getName()); //custom
        }
        
        for(String name : sObjects)
        {
            options.add(new SelectOption(name, name.replace('__c', '')));
        }
        
        options.sort();
        
        options.add(0, new SelectOption('none','--None--'));
    
        return options;
    }
    
    public List<SelectOption> getLogicFeildsOptions()
    {
        return logicFeildsOptions;
    }
    
    public void getUpdateLogicFeilds()
    {
        logicFeildsOptions.clear();
                     
        Schema.DescribeSObjectResult abrDSR = schemaMap.get(objectToFollow).getDescribe();
        
        Map<String, Schema.SObjectField> fieldMap = abrDSR.fields.getMap();
        
        for(Schema.SObjectField val : fieldMap.Values())
        {    
            Schema.DescribeFieldResult feildData = val.getDescribe();
            
            //we cannot use no non filterable fields in WHERE clause
            if(feildData.Filterable)
				logicFeildsOptions.add(new SelectOption(feildData.getName(), feildData.getLabel()));
        }
        
        logicFeildsOptions.sort();
        
        logicFeildsOptions.add(0, new SelectOption('none','--None--'));
        
        //logicFeildsOptions.add(new SelectOption('e','a'));
    }
    
    public List<SelectOption> getSupportedLogicRuleOperators() 
    {
        List<SelectOption> options = new List<SelectOption>();
        
        options.add(new SelectOption('none','--None--'));
        options.add(new SelectOption('=','equals'));
        options.add(new SelectOption('!=','not equal to'));
        /*options.add(new SelectOption('starts with','starts with'));*/
        /*options.add(new SelectOption('contains','contains'));*/
        /*options.add(new SelectOption('does not contain','does not contain'));*/
        options.add(new SelectOption('<','less than'));
        options.add(new SelectOption('>','greater than'));
        options.add(new SelectOption('<=','less or equal'));
        options.add(new SelectOption('>=','greater or equal'));
        /*options.add(new SelectOption('includes','includes'));*/
        
        return options;
    }
    
    public String getObjectToFollow() {
            return objectToFollow;
    }
    
    //true if rule passed check
    private boolean CreateUpdateObjectAndFlowRule()
    {
        
        squawkerRule.ObjectToWatch__c = objectToFollow;
        squawkerRule.ObjectToWatchFriendly__c = squawkerRule.ObjectToWatch__c.replace('__c', '');

        List<SquawkRuleLogic> ruleLogic = new List<SquawkRuleLogic>();

        if(ruleOne_Field != null && ruleOne_Field != '' && ruleOne_Field != 'none')
        {
            SquawkRuleLogic r1 = new SquawkRuleLogic();
            r1.field = ruleOne_Field;
            r1.op = ruleOne_Operation;
            r1.value = ruleOne_Value.unescapeHtml4();
            ruleLogic.add(r1);
        }
        
        if(ruleTwo_Field != null && ruleTwo_Field != '' && ruleTwo_Field != 'none')
        {
            SquawkRuleLogic r2 = new SquawkRuleLogic();
            r2.field = ruleTwo_Field;
            r2.op = ruleTwo_Operation;
            r2.value = ruleTwo_Value.unescapeHtml4();
            ruleLogic.add(r2);
        }
        
        if(ruleThree_Field != null && ruleThree_Field != '' && ruleThree_Field != 'none')
        {
            SquawkRuleLogic r3 = new SquawkRuleLogic();
            r3.field = ruleThree_Field;
            r3.op = ruleThree_Operation;
            r3.value = ruleThree_Value.unescapeHtml4();
            ruleLogic.add(r3);
        }
        
        squawkerRule.RuleLogic__c = JSON.serialize(ruleLogic);
        
        //follow groups
        squawkerRule.GroupsToNotify__c = '';
        
        for(SelectOption aOption : selectedGroups)
        {
            squawkerRule.GroupsToNotify__c += aOption.getValue() + ',';
        }
        
        //remove last comma as we prefix in loop each time regardless if last value
        squawkerRule.GroupsToNotify__c = squawkerRule.GroupsToNotify__c.Substring(0,squawkerRule.GroupsToNotify__c.length()-1);

        //follow groups frineldy name
        squawkerRule.Groups_To_Notify_Friendly__c = '';
        
        for(SelectOption aOption : selectedGroups)
        {
            squawkerRule.Groups_To_Notify_Friendly__c += aOption.getLabel() + ',';
        }
        
        //remove last comma as we prefix in loop each time regardless if last value
        squawkerRule.Groups_To_Notify_Friendly__c = squawkerRule.Groups_To_Notify_Friendly__c.Substring(0,squawkerRule.Groups_To_Notify_Friendly__c.length()-1);
        
        squawkerRule.Last_Edit_Date__c = DateTime.now();

		//test generated rule        
        if(TestRule(ruleLogic, squawkerRule) == false)
        {
            return false;
        }
        
        if(newRule)
        {
        	//insert object
        	insert SquawkerRule;
            return true;
        }
        
        //update
        update SquawkerRule;
        return true;
    }
    
    public boolean TestRule(List<SquawkRuleLogic> ruleLogic, SquawkerRule__c rule)
    {
        String queryString = new Squawker_QueryGenerator().createQueryString(ruleLogic, rule, DateTime.now());
   		
        try
        {
        	Database.query(queryString);
        }
        catch(Exception e)
        {
            System.debug('TestRule() failed - ' + e);
            
        	return false;    
        }
        
        return true;
    }

    //Functions
    public PageReference save() {

        if(CreateUpdateObjectAndFlowRule() == false)
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Error generating rule. check that the "Rule conditions" are valid');
            ApexPages.addMessage(myMsg);
            
            return null;            
        }
        
        if(newRule)
        {
        	PageReference sucessRuleCreated = new PageReference('/apex/Squawk_ViewRules?sfdc.tabName=01rS0000000DNn9');
        	sucessRuleCreated.setRedirect(true);

        	return sucessRuleCreated;
        }
        
        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.CONFIRM,'Rule has been successfully updated');
        ApexPages.addMessage(myMsg);
        
        return null;
    }
}
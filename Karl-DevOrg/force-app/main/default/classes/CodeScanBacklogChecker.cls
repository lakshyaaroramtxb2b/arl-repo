global with sharing class CodeScanBacklogChecker
{ 
    public static void run(BatchNotifier bn) { 
     Integer lookback = 600; //lookback in hours -- we don't search from beginning of time!
     
     Backlog_Configuration__c config = [SELECT Max_Working_Queue_Size__c,
                                               Max_Backlog_Size__c,
                                               Max_Unworked_Scan_Hours__c,
                                               Max_Allowed_Scan_Hours__c,
                                               Max_Queue_Wait_Hours__c,
                                               PausedDelay__c,
                                               StuckScanDelay__c
                                            FROM Backlog_Configuration__c
                                            WHERE Active__c = True LIMIT 1];
                                            
     CodeScan__c [] longQueueWait  = [SELECT Name,Created__c,Id,Username__c FROM CodeScan__c WHERE 
                                             WorkState__c != 'Download Completed' 
                                             AND WorkState__c != 'Never'
                                             AND WorkState__c != 'Paused'
                                             AND WorkState__c != ''
                                             AND WorkState__c != 'Redundant' 
                                             AND Created__c <: Datetime.now().addHours(0-Math.round(config.Max_Queue_Wait_Hours__c))
                                             AND Created__c >: Datetime.now().addHours(-lookback)]; //do a roughly 2 week lookback

     CodeScan__c [] queueLen  = [SELECT Name,Created__c,Id,Username__c FROM CodeScan__c WHERE 
                                             WorkState__c != 'Download Completed' 
                                             AND WorkState__c != 'Never'
                                             AND WorkState__c != 'Paused'
                                             AND WorkState__c != 'Redundant'
                                             AND WorkState__c != ''
                                             AND Created__c >: Datetime.now().addHours(-lookback)];
     
     CodeScan__c [] unknownStateLen  = [SELECT Name,Created__c,Id,Username__c FROM CodeScan__c WHERE
                                             WorkState__c != 'New'
                                             AND WorkState__c != 'Passed To Worker'
                                             AND WorkState__c != 'Download Started'
                                             AND WorkState__c != 'Download Completed'
                                             AND WorkState__c != 'Never'
                                             AND WorkState__c != 'Paused'
                                             AND WorkState__c != 'Redundant'
                                             AND WorkState__c != ''
                                             AND Created__c >: Datetime.now().addHours(-lookback)];
                                             
     CodeScan__c [] pausedScans = [ SELECT Name, Created__c, Id, Username__c FROM CodeScan__c WHERE 
                                             WorkState__c = 'Paused'
                                             AND Created__c < :Datetime.now().addHours(0-Math.round(config.PausedDelay__c))
                                             AND Created__c >: Datetime.now().addHours(-lookback) LIMIT 100];
     
     CodeScan__c [] stuckScans = [ SELECT Name, Created__c, Id, Username__c FROM CodeScan__c WHERE 
                                             WorkState__c != 'Paused'
                                             AND WorkState__c != 'New'
                                             AND WorkState__c != 'Never'
                                             AND WorkState__c != 'Download Completed'
                                             AND WorkState__c != ''
                                             AND WorkState__c != 'Redundant'
                                             AND LastModifiedDate < :Datetime.now().addHours(0-Math.round(config.StuckScanDelay__c)) 
                                             AND Created__c >: Datetime.now().addHours(-lookback) LIMIT 20];
                                             
     if (pausedScans.size() > config.PausedDelay__c) {
        bn.addNotifier(Notifier.Severity.Error, 'CodeScanBacklogChecker : Paused scans ' + String.valueOf(pausedScans.size()) + ' will be reset to New state ',makeExtra(pausedScans)); 
        rollback(pausedScans);
     }
     
     if (stuckScans.size() > config.StuckScanDelay__c) {
        bn.addNotifier(Notifier.Severity.Error, 'CodeScanBacklogChecker : Stuck scans ' + String.valueOf(pausedScans.size()) + ' may be reset to New state',makeExtra(stuckScans)); 
        rollback(stuckScans);
     }
     
     if (queueLen.size() > config.Max_Backlog_Size__c) {
         bn.addNotifier(Notifier.Severity.ERROR,'CodeScanBacklogChecker : Unstarted scan queue length of '+String.valueOf(queueLen.size())+' exceeds max size of '+String.valueOf(config.Max_Backlog_Size__c),makeExtra(queueLen));
     }
     
     if (longQueueWait.size() > 0) {
         bn.addNotifier(Notifier.Severity.ERROR,'CodeScanBacklogChecker : ' + String.valueOf(longQueueWait.size())+' scan(s) waiting in queue (not in progress) longer than allowable '+String.valueOf(config.Max_Queue_Wait_Hours__c)+' hours',makeExtra(longQueueWait));
         rollback(longQueueWait);
     }

     if (unknownStateLen.size() > 0) {
         bn.addNotifier(Notifier.Severity.ERROR,'CodeScanBacklogChecker : ' + String.valueOf(unknownStateLen.size())+' scan(s) in in unknown states',makeExtra(unknownStateLen));
     }

     bn.addNotifier(Notifier.Severity.DEBUG,'CodeScanBacklogChecker : complete: queueLen['+String.valueOf(queueLen.size())+'] longQueueWait['+String.valueOf(longQueueWait.size())+'] unknownStateLen['+String.valueOf(unknownStateLen.size())+']');
   }
   
   private static List<String> makeExtra(List<CodeScan__c> objs) {
       List<String> ret = new List<String>();
       for (CodeScan__c c : objs) {
           ret.add(c.Created__c.format() + '  '+c.Username__c+'  '+c.Name);
       }
       return ret;
   }
   
   private static void rollback(List<CodeScan__c> objs) {
       for (CodeScan__c c : objs) {
           c.WorkState__c = 'New';
       }  
       
       try {
               update objs;
           } catch (DMLException e) {
               System.debug('Error rolling back scan '+ e.getMessage());
           }
       }
   
   public static testMethod void doTest() {
       delete [select Id from Backlog_Configuration__c];
       insert new Backlog_Configuration__c(Max_Working_Queue_Size__c=75,
                                           Max_Backlog_Size__c=6,
                                           Max_Unworked_Scan_Hours__c=24,
                                           Max_Allowed_Scan_Hours__c=12,
                                           Max_Queue_Wait_Hours__c=12,
                                           PausedDelay__c=1,
                                           StuckScanDelay__c=1,
                                           Active__c = True);
       List<CodeScan__c> testSet = new List<CodeScan__c>();
       
       // paused scan reset
       testSet.add(new CodeScan__c(name='PausedResetWait-Test', Created__c = Datetime.now().addHours(-3), Username__c='test@test.org', WorkState__c='Paused'));
       
       // longQueueWait
       testSet.add(new CodeScan__c(name='longQueueWait-Test',Created__c=Datetime.now().addHours(-3),Username__c='test@test.org'));
       
       // overlyLongScan
       testSet.add(new CodeScan__c(name='overlyLongScan-Test',Created__c=Datetime.now().addHours(-9),inProgress__c=True,Username__c='test@test.org'));
       
       // tooOldUnworked
       testSet.add(new CodeScan__c(name='tooOldUnworked-Test',Created__c=Datetime.now().addHours(-5),inProgress__c=True,Username__c='test@test.org'));
       
       // workingLen, trigger sets Created__c
       testSet.add(new CodeScan__c(name='workingLen-Test1',Username__c='test@test.org'));
       testSet.add(new CodeScan__c(name='workingLen-Test2',Username__c='test@test.org'));
       testSet.add(new CodeScan__c(name='workingLen-Test3',Username__c='test@test.org'));
       
       // queueLen, trigger sets Created__c
       testSet.add(new CodeScan__c(name='queueLen-Test1',Username__c='test@test.org'));
       testSet.add(new CodeScan__c(name='queueLen-Test2',Username__c='test@test.org'));
       testSet.add(new CodeScan__c(name='queueLen-Test3',Username__c='test@test.org'));
       testSet.add(new CodeScan__c(name='queueLen-Test4',Username__c='test@test.org'));
       testSet.add(new CodeScan__c(name='queueLen-Test5',Username__c='test@test.org'));
       testSet.add(new CodeScan__c(name='queueLen-Test6',Username__c='test@test.org'));
       testSet.add(new CodeScan__c(name='queueLen-Test7',Username__c='test@test.org'));
       
       insert testSet;
       
       BatchNotifier bn = new BatchNotifier('PopcrabMonitorTests');
       run(bn);
       bn.processBatch();
   }
}
/**
 * Acts as the container class Esa_for all exceptions in events app
 */
public with sharing class Esa_Exceptions {

    /**
     * Mirrors java.lang.Esa_UnsupportedOperationException, should be used
     * for similar purposes in events app.
     */
    public class Esa_UnsupportedOperationException extends Exception {
    }

    /**
     * Mirrors java.lang.Esa_IllegalArgumentException, should be used
     * for similar purposes in events app.
     */
    public class Esa_IllegalArgumentException extends Exception {

    }

    /**
     * Mirrors java.lang.Esa_IllegalStateException, should be used
     * for similar purposes in events app.
     */
    public class Esa_IllegalStateException extends Exception {

    }

    /**
     * General app exception in Events app
     */
    public class Esa_AppException extends Exception {

    }

    /**
     * General app exception in Events app which does not contain any debuggable
     * info and therefore should not be logged.  Use it in services where you have
     * already logged the useful info and you need to throw a different exception
     * just to indicate to the caller (controller) that the operation failed.
     */
    public class Esa_NoDebugAppException extends Exception {

    }

    /**
     * Exception raised during a REST API call.
     */
    public class RestException extends Exception {

    }

    /**
     * Rate limit for Events App Rest request exceeded
     */
    public class Esa_RestRateException extends Exception {}

}
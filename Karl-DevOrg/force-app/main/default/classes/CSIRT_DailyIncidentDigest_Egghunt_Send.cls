global class CSIRT_DailyIncidentDigest_Egghunt_Send Implements Schedulable{
	global void execute(SchedulableContext sc){
        List<Id> contactId = new List<Id>{'0033000001pU5yU','0033A0000284s3C','0033A000022EFDo','0033000001l0LdU','0033000001jeldo','0033000001l0L8M','0033000001l0Ks3','0033000001uyAvh'};
        for(Id c : contactId){
        	sendmail(c);  
        }
	}
 
	public void sendmail(Id contactId){
		Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        email.setOrgWideEmailAddressId('0D2300000004E4Z');
        email.setTemplateId('00X3A00000253P6');
        email.setTargetObjectId(contactId);
        email.saveAsActivity = False;
		Messaging.sendEmail(New Messaging.SingleEmailMessage[]{email});
	}
}
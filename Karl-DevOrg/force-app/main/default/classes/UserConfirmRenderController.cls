public class UserConfirmRenderController {
    
    public String dacId {get;set;}
    public String daId {get;set;}
    public String emailText{get;set;} 
    
    public UserConfirmRenderController()
    {
        try
        {
            dacId = apexpages.currentpage().getparameters().get('dac');
            daId = apexpages.currentpage().getparameters().get('da');
    
            if(dacId instanceOf Id && true)
            {
                Detection_Alert_Criteria__c dac = [SELECT Id, Name, User_Verification_Email_Message__c
                                                   FROM Detection_Alert_Criteria__c
                                                   WHERE id =: dacId LIMIT 1];
                
                if(dac != null)
                {
                    Detection_Alert__c da;
                    
                    if(daId == null)
                    {
                        da = [SELECT Id, Name, DetectionAlertCriteria__c
                                 FROM Detection_Alert__c
                                 WHERE DetectionAlertCriteria__c =: dac.Id
                                 ORDER BY createdDate DESC 
                                 LIMIT 1];
                    }
                    else
                    {
                        da = [SELECT Id, Name, DetectionAlertCriteria__c
                                 FROM Detection_Alert__c
                                 WHERE Id =: daId 
                                 LIMIT 1];
                    }
                    
                    if(da != null)
                    {
                        string emailStr = 'test';
                        
                        if(!String.isEmpty(dac.User_Verification_Email_Message__c))
                            emailStr = dac.User_Verification_Email_Message__c;
                        
                        String emailTemplate = encodeEmailTemplate(emailStr, da.Id);
                        
                        EmailTemplate et = [SELECT Id, HtmlValue 
                                            FROM EmailTemplate 
                                            WHERE DeveloperName = 'Detection_User_Confirm' 
                                            LIMIT 1];
    
                        et.HtmlValue = et.HtmlValue.replace('{!AlertsPendingJudication__c.Email_Body__c}', emailTemplate);
                        et.HtmlValue = et.HtmlValue.replace('{!AlertsPendingJudication__c.DA_Ref__c}', da.Name);
                        et.HtmlValue = et.HtmlValue.replace(']]>', '');
    
                        emailText = et.HtmlValue;
                    }
                    else
                    {
                        emailText = 'No DA objects for this DAC. Cant simulate an email template. Create an alert first.';
                    }
                }
            } 
        }
        catch(Exception ex)
        {
            emailText = ex.getStackTraceString();
        }
    }
    
    private String encodeEmailTemplate(String emailT, Id daId)
    {
       String encodedString = emailT; 

       Detection_Alert_Event_Map__c da = [SELECT Id, Detection_Alert__c, Detection_Security_Event__r.FieldMapRaw__c 
                                		  FROM Detection_Alert_Event_Map__c
                                		  WHERE Detection_Alert__c =: daId 
                                          LIMIT 1];
       
       if(da != null)
       {
           System.debug(da.Detection_Security_Event__r.FieldMapRaw__c);
           
           Map<String, Object> unraw = (Map<String, Object>) JSON.deserializeUntyped(da.Detection_Security_Event__r.FieldMapRaw__c);
           
           Pattern p = Pattern.compile('##([^#]+)##');
           Matcher m = p.matcher(encodedString);
           
           while (m.find()) {
                String s = m.group();
                s = s.replaceAll('##', '');

                string v = (String) unraw.get(s);

                if(String.isNotBlank(v))
                {
					encodedString = encodedString.replace('##' + s + '##', v);
                }
            }
       }
       
       return encodedString;
   }
}
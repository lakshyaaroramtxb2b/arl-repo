public class JSCaseNode
{
    public enum ENodeType {EVENT, SOURCE_ENDPOINT, DESTINATION_ENDPOINT}

    public String id;
    public String key;
    public ENodeType nodeType;
    private String label;
    public String description;
    public String src;
    public String dst;
    
    private Boolean hasNullKey = false;
    
    //each node has a related event, src or dst elements, as each case is borken up into three elements
    ////used to highlight all relvent items
    public Set<String> relatedNodeKeys;
	public Set<String> relatedEdgeKeys;
    
    //specfic for node image defined by lib
    public String url;
    public String type;
    public String color;
    
    public String caseNumber;

    public Integer x;
    public Integer y;
    public Integer size;
    
    //how many times this event in tree. used in graph as related events for the alert type
    public String alertName;
    public String alertSubject;
    public String alertDescription;
    public String detectionLogic;
    public Integer relatedEvents;
  
    public class CaseReference
    {
        String CaseId;
        String CaseNumber;
        String Status;
    }
    
    public Set<CaseReference> caseRefs = new Set<CaseReference>();
    
    public void SetHasNullKey()
    {
        this.hasNullKey = true;
        this.SetLabel(' ');
    }
    
    public void addCaseRef(Case c)
    {
        CaseReference cf = new CaseReference();
        cf.CaseId = c.Id;
        cf.CaseNumber = c.CaseNumber;
        cf.Status = c.Status;

        if(!doesCaseRefExist(c))
        {
            this.relatedEvents++;
        	this.caseRefs.add(cf);
    	}
    }
    
    boolean doesCaseRefExist(Case c)
    {
        for(CaseReference cf : this.caseRefs)
        {
            if(cf.CaseNumber.equalsIgnoreCase(c.CaseNumber))
                return true;
        }
        
        return false;
    }
    
    public void SetLabel(String txt)
    {
        Integer kMaxLabelLength = 40;
     
        if(txt != null && txt != '')
        {
            if(txt.length() > kMaxLabelLength)
                this.label = txt.substring(0, kMaxLabelLength) + '...';
            else
                this.label = txt;      
        }
        else
            this.label = 'Undefined';      
    }
    
    public JSCaseNode(Case c, String key, ENodeType nodeType)
    {
        try
        {
            //NOTE ID SET WHEN ADDED TO GRAPH
            
            this.nodeType = nodeType;
            
            this.caseNumber = c.CaseNumber;

            if(key.contains(':::IS_A_NULL_KEY'))
            	this.SetHasNullKey();
			else
                this.SetLabel(key);
            
            this.relatedEvents = 0;
            
            this.alertName = c.Alert_Name__c;
            this.alertSubject = c.Subject;
            //this.alertDescription = c.Description;
            this.detectionLogic = c.Detection_Logic__r.Name;
            
            relatedNodeKeys = new Set<String>();
            relatedEdgeKeys = new Set<String>();
            
            this.key = key;
            
            this.color = 'black';
            
            if(this.nodeType == ENodeType.SOURCE_ENDPOINT || this.nodeType == ENodeType.DESTINATION_ENDPOINT)
            {
                if(!IPConverter.isValidIP(this.key))
                {
                    this.url = '/resource/1440746550000/CaseGraph/img/null.png';
                }
                else if(!IPConverter.isPrivateSpace(this.key))
                	this.url = '/resource/1440746550000/CaseGraph/img/externalNode.png';
               	else
                    this.url = '/resource/1440746550000/CaseGraph/img/node.png';
                
            	this.type = 'image';
            }
            else if(this.nodeType == ENodeType.EVENT)
            {
                //SOURCE
                //ALERT OR INCIDENT
                //
                
                this.url = '/resource/1440746550000/CaseGraph/img/event.png';
            	this.type = 'image';
                //this.color = 'black';
            }
            
            //key is used to group nodes form alerts toegther so that we dont duplicate alerts to show on graph
            this.key = this.key.toLowerCase();
            
            //this.src = c.Alert_Source_IP__c;
            //this.dst = c.Destination_IP_Address__c;
            
            this.description = '';
            this.addCaseRef(c);

            Integer canvasWidth = 1000;
            
            //might need to create circle around parent node e.g. using Sin Cos
            this.x = getRandomInt(-canvasWidth, canvasWidth);
            this.y = getRandomInt(-canvasWidth, canvasWidth);
            
            this.size = 1;
        }
        catch(Exception e)
        {
            System.debug('In JSCaseNode constructor. case sObject does not contain feild. Defaults returned');
            
            return;
        }
    }
    
    public static Integer getRandomInt (Integer lower, Integer upper)
	{
		return Math.round(Math.random() * (upper - lower)) + lower;
	}
}
global  class DCAlertReporter implements Schedulable {
    
    global void execute(SchedulableContext ctx) 
    {
        List<Detection_Alert__c> alerts = [SELECT Id, OwnerId, owner.name, Response_Org_Case_ID__c , CreatedDate, Subject__c, DetectionAlertCriteria__r.OwnerId, CSIRT_Outcome__c, CSIRT_Adjudication__c 
                                           FROM Detection_Alert__c 
                                           WHERE RecipientTeam__c = 'CSIRT'
                                           	AND CreatedDate = LAST_N_DAYS:1];
        
        List<AggregateResult> alertsDACs = [SELECT DetectionAlertCriteria__c
                                            FROM Detection_Alert__c 
                                            WHERE RecipientTeam__c = 'CSIRT'
                                             AND CreatedDate = LAST_N_DAYS:1
                                            GROUP BY DetectionAlertCriteria__c];
        
        integer fpCount = database.countQuery('SELECT count() FROM Detection_Alert__c WHERE CSIRT_Adjudication__c= \'False Positive\' AND ReportedOn__c = false AND RecipientTeam__c = \'CSIRT\' AND CreatedDate = LAST_N_DAYS:1');
   		integer waitingCount = database.countQuery('SELECT count() FROM Detection_Alert__c WHERE CSIRT_Adjudication__c= \'\' AND ReportedOn__c = false AND RecipientTeam__c = \'CSIRT\' AND CreatedDate = LAST_N_DAYS:1');
        
        Double countMinusWaiting = (Double)alerts.size() - waitingCount;
        
        decimal fpPercentage = ((Double)fpCount / (countMinusWaiting)) * 100.0;
        
        decimal avgHour = (Double)alerts.size() / 24;
        
        String statsHtml = '<table><tr><td>Total: ' + alerts.size() + ', Total excluding \'waiting\' ' + ((integer)countMinusWaiting) + '</td></tr>';
        statsHtml += '<tr><td>Average ' + avgHour.SetScale(2) + ' alerts per hour. ' + alertsDACs.size() + ' distinct DAC\'s </td></tr>';
        statsHtml += '<tr><td><b>TP: ' + (countMinusWaiting - fpCount) + ' (' + (100.0 - fpPercentage).SetScale(2) + '%) </b></td></tr>';
        statsHtml += '<tr><td><b>FP: ' + fpCount + ' (' + fpPercentage.SetScale(2) + '%) </b></td></tr>';

        String messageBody = '<!DOCTYPE html><html><head><style>table {font-family: arial, sans-serif;border-collapse: collapse;'
                             + 'width: 100%; } td, th { border: 1px solid #dddddd; text-align: left; padding: 8px; }'
                             + 'tr:nth-child(even) {background-color: #dddddd;}</style></head><body>'
            				 + '<h2>Alerts Sent to ResponseOrg (Last 24 Hours)</h2>' + statsHtml + '<table><tr>'
                             + '<th>Alert</th><th>Date</th><th>Owner</th><th>Adjudication</th><th>Outcome</th><th>IR Link</th></tr>';      
   
        for(Detection_Alert__c alert : alerts)
        {
            messageBody += '<tr>';
            messageBody += '	<td><a href="https://security.my.salesforce.com/' + alert.id + '">' + alert.Subject__c + '</a></td>';
            messageBody += '    <td>' + alert.CreatedDate + '</td>';
            messageBody += '    <td>' + alert.owner.name + '</td>';
            messageBody += '    <td>' + (alert.CSIRT_Adjudication__c == null ? 'Waiting on STAR' : alert.CSIRT_Adjudication__c)  + '</td>';
            messageBody += '    <td>' + (alert.CSIRT_Outcome__c == null ? '-' : alert.CSIRT_Outcome__c)  + '</td>';
            messageBody += '    <td><a href="https://response.my.salesforce.com/' + alert.Response_Org_Case_ID__c + '">link</a></td>';
            messageBody += '</tr>';
        
            //alert.ReportedOn__c = true;
        }

        messageBody += '</table></body></html>';
        
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(new String[] {'detectionCloud@salesforce.com'});
        mail.setSubject('Daily DC Alert Summary');
        mail.setHtmlBody(messageBody); 
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });

        //UPDATE alerts;
    }

}
public class SecurityAssessmentsController {
    @TestVisible private enum Stages { AUTHORIZE, ACCEPT_SAA, PENTEST_INFO, REQUEST_COMPLETE, REQUEST_ERROR, EDIT_EXISTING, CHOOSE_ENV}
    @TestVisible public Stages stage {get;private set;}
    public boolean show_authorize {get;private set;}
    public boolean show_saa {get;private set;}
    public boolean show_form {get;private set;}
    public boolean show_edit {get;private set;}
    public boolean show_env {get;private set;}

    public string target_environment {get;set;}
    public boolean requires_additional_target_info {get;set;}
    private Customer_Assessment_Areas__mdt[] assessmentAreas;
    
    public static Boolean ISSANDBOX {get
        {
            if (ISSANDBOX == null) {
                ISSANDBOX = [SELECT isSandbox FROM Organization limit 1].isSandbox;
            }
            return ISSANDBOX;  
        } 
        private set;
    }

    public static final string SAA_DOC_NAME = 'Salesforce_Security_Assesment_Agreement';
    public static final string OAUTH_AUTHORIZE_ENDPOINT = 'https://login.salesforce.com/services/oauth2/authorize';
    public static final string OAUTH_TOKEN_ENDPOINT = 'https://login.salesforce.com/services/oauth2/token';
    /*public static final string OAUTH_CALLBACK_URL = ISSANDBOX? URL.getSalesforceBaseUrl().toExternalForm() + '/securityassessments' :
                                                    'https://security.secure.force.com/securityassessments'; */
    public static final string OAUTH_CALLBACK_URL = 'https://securityorg.force.com/securityassessments/SecurityAssessments';
    public static final string OAUTH_CLIENT_ID = '3MVG99OxTyEMCQ3i.Cy5vQYJe7mVOVkqCcjnjh4UxgTbP1hFlYpkEPGq67yMpb5o4ft1ienxiQ1RMjAdd32ZV';
    private static final string OAUTH_CLIENT_SECRET = '916510964017196769';
    private static Boolean wasSFDCOauthCallback = false;
    private static String hashId;
    
    // this google client id is registered under project "Security Assessments" for "esa.coordinator@salesforce.com" service google account
	// available at https://console.developers.google.com
    public static final string GOOGLE_CLIENT_ID = '619385653197-4vjr49mgbmnhls0msh25u86q7tl5to4k.apps.googleusercontent.com';
            
    private static final String MARKETING_CLOUD = 'Marketing_Cloud_Services';

    public string gclientId {get {return GOOGLE_CLIENT_ID;}}

    public boolean SAA_Accepted {get;set;}
    public string company_name {get;set;}
    public string member_id {get;set;}
    public string company_org_id {get;set;}
    public string pm_name {get;set;}
    public string pm_workphone {get;set;}
    public string pm_workphone_helpText {get; set;}
    public string pm_email {get;set;}
    public string pm_cellphone {get;set;}
    public string pm_username {get;set;}
    public string pm_first_name {get;set;}
    public string pm_last_name {get;set;}
    public string pm_user_id {get;set;}

    // FB, linkedin login
    public string social_token {get;set;}

    // VF hints. For revisit/edit, only show the auth provider used
    public Boolean showFB {get;set;}
    public Boolean showLI {get;set;}
    public Boolean showGoogle {get;set;}
    public Boolean showSFDC {get;set;}

    public Boolean isMarketingCloud {get; set;}

    public Map<String,Boolean> mapSubEnvironments {get; set;}

    // opaque oauth state. Used to hold record ID through the SFDC oauth flow
    // when the user is coming back to view or edit a record
    private ID oauth_state;

    // Used to hold customer assessment for re-visit/edit
    public Customer_Assessment__c record {get;private set;}

    // Users may only edit record when it's not approved
    public boolean record_editable {get; private set;}

    // Store the oauth userInfo the user authenticated with
    @TestVisible
    private Stringable userInfo;

    public String now {get;set;}
    public final string saaDocId {get; private set;}

    public List<SelectOption> server_load_entries {get; private set;}

    // This is stupid but we keep hitting problems where it's complaining
    // about tests doing having uncommitted work and doing DML before callouts.
    // You're supposed to be able to do Test.start()/end() but that doesn't seem
    // to actually work
    public HttpResponse testResponse;
    
    private class AssessmentException extends Exception {}

    public SecurityAssessmentsController() {
        
        // show all social auth providers by default
        showFB = showGoogle = showLI = showSFDC = True;
        record_editable = False;

        SAA_Accepted = FALSE;

        List<Document> docs = [SELECT Id FROM Document WHERE DeveloperName = :SAA_DOC_NAME LIMIT 1];
        if (docs.size() == 1){
           saaDocId = docs[0].id;
        }else{
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Unable to load page. Please contact support. Error Code : 1',''));
            saaDocId = NULL;
        }

        server_load_entries = new List<SelectOption>();
        Schema.DescribeFieldResult dfr = Customer_Assessment__c.Server_Load__c.getDescribe();

        for(Schema.PicklistEntry entry : dfr.getPicklistValues()){
            string value = entry.getValue();
            server_load_entries.add(new SelectOption(value,value));
        }

        try {
        } catch (Exception e) {
        }


        if (record != null) {
            // checkAndProcessSFDCOauthCode() will need to use this state to keep track of the original re-visit/edit request
            oauth_state = record.id;
            SAA_Accepted = True;
        } else {
            // They're not loading a record, give them the environment selector by default
            changeStage(stages.CHOOSE_ENV);
        }

        // This may also load "record" if we're being invoked as an oauth callback
        wasSFDCOauthCallback = checkAndProcessSFDCOauthCode();

        if (record != null) {
          hideUnusedAuthProviders(false);
          changeStage(Stages.AUTHORIZE);
        } else {
            getEnvironments();
        }

        if(string.isNotBlank(target_environment)) {
            String subEnvironments;
            try {
                subEnvironments = [SELECT Sub_Environments__c FROM Customer_Assessment_Areas__mdt
                                   WHERE DeveloperName =:target_environment].Sub_Environments__c;
            }catch(exception ex) {
                subEnvironments = null;
            }

            if(subEnvironments !=null) {
                Set<String> subEnvsSet = new Set<String>();
                subEnvsSet.addAll(subEnvironments.split(','));

                mapSubEnvironments = new Map<String,Boolean>();
                for(String st : subEnvsSet) {
                    mapSubEnvironments.put(st,false);
                }
            }
        }

        if(target_environment == MARKETING_CLOUD) {
            isMarketingCloud = true;
        } else {
            isMarketingCloud = false;
        }

    }
    
    public pageReference loginUser() {
        if (wasSFDCOauthCallback) {

            if (ApexPages.currentPage().getParameters().get('state').startsWith('HASHID')) {
                hashId = ApexPages.currentPage().getParameters().get('state').replace('HASHID', '');
            }
            return authenticateUser();
    
        }
        return null;
    }
        
    public pageReference authenticateUser() {
        
        if (String.isEmpty(hashId)) hashId = ApexPages.currentPage().getParameters().get('id');
        String userName = pm_email + '.securityassessments';
        String userpassword = '001GrJ!' + pm_email.substringBefore('@').left(30).reverse();
        String accountId = '0013A00001YZGrJ';
        String userId;
        String startURL = '/s/?entityCode=PSIRT';
        if (hashId != null) startURL += '&id=' + hashId;
    
        User u;    
        
        try {
            u = [SELECT Id, UserName, FirstName, LastName, Email, ProfileId,
                 CommunityNickname FROM User WHERE UserName = :userName]; 
            userId = u.Id;
        } catch (exception e) {}
            
        if (u == null) {
            u = new User();
            u.Username = userName;
            u.FirstName = pm_first_name;       
            u.LastName = pm_last_name;       
            u.Email = pm_email;
            u.profileId  = '00e3A0000028mPm';
            u.CommunityNickname = pm_email.substringBefore('@').left(37)+'00e'+Math.round(Math.random()*100000);
            
            userId = Site.createPortalUser(u, accountId, userpassword);
        }
    
        if (userId != null) { 
            
            try {
                addPermissionSet(userId);    
            } catch (exception e) {}
            
            if (userpassword != null && userpassword.length() > 1) {
                return Site.login(u.userName, userpassword, startURL);
            }
            else {
                PageReference page = System.Page.SiteRegisterConfirm;
                page.setRedirect(true);
                return page;
            }
        }     

        return null;
    }
    
    @future
    public static void addPermissionSet(String userId) {
        try {
            insert new PermissionSetAssignment (AssigneeId = userId, PermissionSetId = '0PS3A000000LMU8');
        } catch (exception e) {}
    }

    @TestVisible
    private void changeStage(Stages newStage) {
        stage = newStage;
        show_authorize = show_form = show_saa = show_edit = show_env = False;

        if (stage == Stages.ACCEPT_SAA) {
            show_saa = True;
        } else if (stage == Stages.PENTEST_INFO) {
            show_form = True;
        } else if (stage == Stages.EDIT_EXISTING) {
            show_form = True;
            show_edit = True;
        } else if (stage == Stages.CHOOSE_ENV) {
            show_env = True;
        } else if (stage == Stages.REQUEST_COMPLETE) {
            // pass
        } else if (stage == Stages.AUTHORIZE) {
            show_authorize=True;
        }
    }

    private boolean checkAndProcessSFDCOauthCode(){
        /*
           This page is also used as the callback for the SFDC oauth login. Check for the code, query for
           user details, and populate user information if the code is available
        */

        string code = ApexPages.currentPage().getParameters().get('code');
        if (code == NULL || code == '') return False;

        if (ApexPages.currentPage().getParameters().get('state') != null) {
            // state parm is also use to pass the target environment during call back for new request
            if (ApexPages.currentPage().getParameters().get('state').startsWith('HASHID')) {
                hashId = ApexPages.currentPage().getParameters().get('state').replace('HASHID', '');
            } else if (!ApexPages.currentPage().getParameters().get('state').startsWith('TARGETENV')){
                // This is a callback to a revisit/edit request. We previously passed the record ID as the state
            } else {
                target_environment = ApexPages.currentPage().getParameters().get('state').replace('TARGETENV', '');
            }
        }

        HttpRequest req = new HttpRequest();
        req.setMethod('POST');
        req.setHeader('Accept', 'application/json');
        req.setEndpoint(OAUTH_TOKEN_ENDPOINT);

        string body = 'grant_type=authorization_code';
        body += '&code=' + code;
        body += '&client_id=' + OAUTH_CLIENT_ID;
        body += '&client_secret=' + OAUTH_CLIENT_SECRET;
        body += '&redirect_uri=' + OAUTH_CALLBACK_URL;
        req.setBody(body);

        Http http = new Http();
        HttpResponse res;
        try{
            res = http.send(req);
        }catch(exception e){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Unable to process request. Please contact support. Error Code : 2',''));
        }

        if (res.getStatusCode() == 200){

            OauthTokenResponse oauthToken = (OauthTokenResponse) JSON.deserialize(res.getBody(), OauthTokenResponse.class);

            req = new HttpRequest();
            req.setMethod('GET');
            req.setHeader('Accept', 'application/json');
            req.setHeader('Authorization', 'Bearer ' + oauthToken.access_token);
            req.setEndpoint(oauthToken.id);

            http = new Http();

            try{
                res = http.send(req);

            }catch(exception e){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Unable to process request. Please contact support. Error Code : 3',''));
            }

            if (res.getStatusCode() == 200){
                IdentityResponse userData = (IdentityResponse) JSON.deserialize(res.getBody(), IdentityResponse.class);
                pm_name = userData.display_name;
                pm_email = userData.email;
                pm_cellphone = userData.mobile_phone;
                company_org_id = userData.organization_id;
                pm_user_id = userData.user_id;
                pm_username = userData.username;
                pm_first_name = userData.first_name;
                pm_last_name = userData.last_name;

                userInfo = userData;
                postLogin();

                return True;
                
                
               

            }else{
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Unable to process request. Please contact support. Error Code : 4',''));
            }
        }else{
              ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Unable to process request. Please contact support. Error Code : 5',''));
              System.debug('received bad status code from OAUTH response: ' + res.toString());
        }
        return False;
    }
    
    public void toggleSaa(){
        changeStage( SAA_Accepted ? Stages.PENTEST_INFO : Stages.ACCEPT_SAA);
    }

    public string getOauthAuthorizeURL(){
        /* SFDC Oauth */
        String url = OAUTH_AUTHORIZE_ENDPOINT + '?response_type=code&client_id=' + OAUTH_CLIENT_ID + '&redirect_uri=' + OAUTH_CALLBACK_URL;
        hashId = ApexPages.currentPage().getParameters().get('id');
            if (oauth_state != null) {
                // The user came for re-visit/edit. Pass the record ID as the state so it
                // gets sent back on the callback
                url += '&state='+ oauth_state;
            } else if (hashId != null) {
                url += '&state=HASHID' + hashId;
            } else {
                // new request so need to pass the selected environment
                url += '&state=TARGETENV' + target_environment;
            }
        
        return url;
    }

    public PageReference handleFBLogin() {
        // actionFunction should have just populated social_token. Call the FB API to get user information
        String url = 'https://graph.facebook.com/me?fields=name,email&access_token='+social_token;
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(url);
        req.setMethod('GET');
        req.setTimeout(60*1000);
        HttpResponse res = h.send(req);
        String response = res.getBody();
        if (res.getStatusCode() != 200) {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Unable to process request. Please contact support. Error Code : 6 ' + response,''));
            return null;
        }

        FBUserInfo u = new FBUserInfo().parse(response);
        pm_name = u.name;
        pm_email = u.email;
        pm_first_name = pm_name.substringBefore(' ');
        pm_last_name = pm_name.substringAfter(' ');
        if (pm_last_name == null) pm_last_name = pm_name.replace(' ','');
            
        userInfo = u;
        postLogin();
        return authenticateUser();
    }

    public PageReference handleLinkedInLogin() {
        // actionFunction should have just populated social_token. Call the LinkedIn API to get user information
        String url = 'https://api.linkedin.com/v1/people/~:(id,firstName,lastName,emailAddress,positions,phone-numbers)?format=json&oauth_token='+social_token;
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(url);
        req.setMethod('GET');
        req.setTimeout(60*1000);
        HttpResponse res = h.send(req);
        String response = res.getBody();
        if (res.getStatusCode() != 200) {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Unable to process request. Please contact support. Error Code : 7',''));
            return null;
        }

        LinkedInUserInfo u = new LinkedInUserInfo().parse(response);
        pm_name = u.name;
        pm_email = u.email;
        pm_first_name = u.firstName;
        pm_last_name = u.lastName;
        company_name = u.companyName;

        userInfo = u;
        postLogin();
        return authenticateUser();

    }

    public pageReference handleGoogleLogin() {

        // actionFunction should have just populated social_token. Call the Google API to get user information
        String url = 'https://www.googleapis.com/oauth2/v3/tokeninfo?id_token='+social_token;
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(url);
        req.setMethod('GET');
        req.setTimeout(60*1000);
        HttpResponse res;
        if ((Test.isRunningTest()) && (testResponse != null)) {
            res = testResponse;
        } else {
            res = h.send(req);
        }
        String response = res.getBody();
        if (res.getStatusCode() != 200) {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Unable to process request. Please contact support. Error Code : 9',''));
            return null;
        }

        GoogleUserInfo u = new GoogleUserInfo().parse(response);
        if (u == null) {
            // They tried to pass of a JWT with a different target audience
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Unable to process request. Please contact support. Error Code : 10',''));
            return null;
        }
        pm_name = u.name;
        pm_email = u.email;
        pm_first_name = u.given_name;
        pm_last_name = u.family_name;

        userInfo = u;
        postLogin();
        return authenticateUser();
    }

    public List<SelectOption> getTestEnvironments() {
        List<SelectOption> te = new List<SelectOption>();
        te.add(new SelectOption('Requesting a test Account','Requesting a test Account'));
        te.add(new SelectOption('Using my own account','Using my own account'));
        return te;
    }


    @TestVisible
    private void postLogin() {
        /*
           After the user logs in, we need to set some flags based on what they're doing. This
           method is also responsible for the access control check during revisit/edit
        */
        
        if (record == null) {
            // They're creating a record. Next stage is SAA
            changeStage(Stages.ACCEPT_SAA);
        } else {
            // Revisit/edit. Check the auth against the record's auth_info__c
            if (!isCurrentAuthSameAsRecordAuth()) {
                record = null;
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Unable to process request. Please contact support. Error Code : 8',''));
                changeStage(Stages.REQUEST_ERROR);
            } else {
                changeStage(Stages.EDIT_EXISTING);
                if (record.Approved__c == False) {
                    // The record is only editable if not approved
                    record_editable = True;
                }
            }

        }
    }

    private Boolean isCurrentAuthSameAsRecordAuth() {
        // Take a userinfo class that implements Stringable and compare it to the saved Auth_Info__c field
        // of a record. This is used to verify that the user attempting to load the record is the same
        // user that created the record
        if ((record == null) || (record.Auth_Info__c == null)) {
            return False;
        }
        return userInfo.toString() == record.Auth_Info__c;
    }

    private void hideUnusedAuthProviders(boolean SFDCOnly) {
       if ((!sfdcOnly) && ((record == null) || (record.Auth_Info__c == null))) {
           return;
       }

       if (sfdcOnly || (record.Auth_Info__c.startsWith('SFDC'))) {
           showFB = showLI = showGoogle = False;
       } else if (record.Auth_Info__c.startsWith('FB')) {
           showSFDC = showLI = showGoogle = False;
       } else if (record.Auth_Info__c.startsWith('LI')) {
           showSFDC = showFB = showGoogle = False;
       } else if (record.Auth_Info__c.startsWith('GOOGLE')) {
           showSFDC = showFB = showLI = False;
       }

    }

    @TestVisible
    public PageReference setTargetEnv() {
        if (target_environment == 'Other') {
            requires_additional_target_info = True;
        }

        Boolean found = False;
        for (Customer_Assessment_Areas__mdt area : assessmentAreas) {
            if (target_environment == area.DeveloperName) {
                found = True;

                if(target_environment.startsWith('Heroku')) {
                    PageReference p = new PageReference('https://help.heroku.com/pentest-requests/new');
                    p.setredirect(true);
                    return p;
                }

                if (area.Is_SFDC_Platform__c) {
                    // only show SFDC login per requirements
                    hideUnusedAuthProviders(true);
                }
                break;
            }
        }
        if (found) {
            changeStage(Stages.AUTHORIZE);
        }
        return null;
    }

    public List<SelectOption> getEnvironments() {
       if (assessmentAreas == null) {
           assessmentAreas = [SELECT Label,DeveloperName,Is_SFDC_Platform__c FROM Customer_Assessment_Areas__mdt ORDER BY Label];
       }

       List<SelectOption> res = new List<SelectOption>();
       res.add(new SelectOption('', '--Select--'));

       for (Customer_Assessment_Areas__mdt area : assessmentAreas) {
         res.add(new SelectOption(area.DeveloperName, area.Label));
       }
       return res;
    }

    // All userinfo classes must support toString so we can compare them to stored record auth
    public interface Stringable { String toString(); }

    /* Begin SFDC Oauth classes */
    private class OauthTokenResponse{

        public string id;
        public string access_token;

    }
    private class IdentityResponse implements Stringable{

        public string user_id;
        public string organization_id;
        public string email;
        public string display_name;
        public string mobile_phone;
        public string username;
        public string first_name;
        public string last_name;
        public string sobjects;

        public Map<string,string> urls;

        public override String toString() {
            return 'SFDC|'+user_id+':'+organization_id;
        }

    }
    /* End SFDC Oauth classes */

    /* Begin Facebook oauth classes */
    public class FBUserInfo implements Stringable{

        public String name;
        public String email;
        public String id;

        public FBUserInfo parse(String json) {
            return (FBUserInfo) System.JSON.deserialize(json, FBUserInfo.class);
        }

        public override String toString() {
            return 'FB|'+id;
        }
    }
    /* End Facebook oauth classes */

    /* Begin Google oauth classes */
    public class GoogleUserInfo implements Stringable{

        public String name;
        public String email;
        public String given_name;
        public String family_name;
        public String sub;
        public String aud; // audience, must be checked that it matches client ID

        public GoogleUserInfo parse(String json) {
            GoogleUserInfo usr = (GoogleUserInfo) System.JSON.deserialize(json, GoogleUserInfo.class);
            if (usr.aud != GOOGLE_CLIENT_ID) {
                return null;
            }
            return usr;
        }

        public override String toString() {
            return 'GOOGLE|'+sub;
        }
    }
    /* End Google oauth classes */

    /* Begin LinkedIn oauth classes */
    public class Company {
            public String name;
        }

    public class Values {
        public Company company;
        public Boolean isCurrent;
        public Company location;
        public String title;
    }

    public class Positions {
        public List<Values> values;
    }

    public class LinkedInUserInfo implements Stringable {

        public String emailAddress;
        public String firstName;
        public String lastName;
        public String id;
        public Positions positions;

        public override String toString() {
            return 'LI|'+id;
        }

        public string companyName {
            get {
                if ((this.positions != null) && (this.positions.values.size() > 0)) {
                    return this.positions.values.get(0).company.name;
                }
                return null;
            }
        }
        public string email {
            get {
                return this.emailAddress;
            }
        }

        public string name {
            get {
                String f = firstName;
                if (String.isBlank(firstName)) { firstName = '';}
                String l = lastName;
                if (String.isBlank(lastName)) { lastName = '';}
                return f+' '+l;
            }
        }

        public LinkedInUserInfo parse(String json) {
            return (LinkedInUserInfo) System.JSON.deserialize(json, LinkedInUserInfo.class);
        }

    }
    /* End LinkedIn oauth classes */
}
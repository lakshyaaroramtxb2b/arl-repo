/**
 * @File Name          : IEM_CaseVisibilityTriggerHandler.cls
 * @Description        : 
 * @Author             : Banshi
 * @Group              : 
 * @Last Modified By   : Banshi
 * @Last Modified On   : 11/14/2019, 12:48:50 PM
 * @Modification Log   : 
 * Ver       Date            Author      		    Modification
 * 1.0    11/14/2019   Banshi     Initial Version
**/
public class IEM_CaseVisibilityTriggerHandler {
    /*
	 * 	Method is used to track all method calls on after Insert
     *  This method takes input of List of Case_Visibility__c
     */
    public static void afterInsert(List<Case_Visibility__c> newList){
        IEM_CaseVisibilityTriggerHelper.createShareRecordsForCommunityUsers(newList);
    }
    /*
	 * 	Method is used to track all method calls on after Delete
     *  This method takes input of List of Case_Visibility__c
     */
    public static void afterDelete(List<Case_Visibility__c> oldList){
        IEM_CaseVisibilityTriggerHelper.deleteShareRecordsForCommunityUsers(oldList);
    }
    
}
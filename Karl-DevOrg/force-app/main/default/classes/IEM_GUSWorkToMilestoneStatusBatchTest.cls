@isTest
public class IEM_GUSWorkToMilestoneStatusBatchTest {
    testmethod static void testEnrollment(){
        Contact contact = new Contact();
        contact.LastName = 'Test';
        contact.Employee_ID__c = 'MTX101';
        INSERT contact;
        
        Case newcase = new Case();
        newcase.RecordTypeId = IEM_Constants.IEM_CASERECORDTYPEID;
        newcase.Cloud__c = 'EntSecTools';
        newcase.Description = 'Test';
        newcase.Team_Responsible__c = 'TRUST Enterprise Security Tools Development';
        newcase.Subject = 'Test Subject';
        INSERT newcase;
        
        List<Milestone__c> milestoneList = IEM_TestDataFactory.createMilestoneRecords(1,newcase.id,true);
             
        ADM_Work_c__x work = new ADM_Work_c__x();
        work.Subject_c__c = 'Test Subject';
        work.ExternalId= 'EX101';
        work.Status_c__c = IEM_Constants.GUS_WORK_STATUS_IN_PROGRESS;
        work.Feature_ID_c__c = milestoneList[0].Id;
        IEM_GUSWorkToMilestoneStatusBatch.mockallGusWorkList.add(work);
      
        Test.startTest();
       	SchedulableContext sc = null;
        IEM_GUSWorkToMilestoneStatusBatch IEMGus = new IEM_GUSWorkToMilestoneStatusBatch();
        IEMGus.execute(sc);
        Test.stopTest();
        List<Milestone__c> updateMilestoneList= [Select Id,Status__c From Milestone__c Where Status__c= :IEM_Constants.MILESTONE_STATUS_IN_PROGRESS];
        system.assertEquals(updateMilestoneList.size(), 1);
    }
}
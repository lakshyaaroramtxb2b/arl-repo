/*
 * Apex class to support Trail enrollment
 */

public class SC_TrainingCourseTakenManager {

    public static final String PATH = TrainingCourseTaken_Constants.COURSE_LEVEL_TRAIL;

    private static Set<Id> contactIds = new Set<Id>();
    private static Map<String, Training_Course_Taken__c> moduleTakenMap =
        new Map<String, Training_Course_Taken__c>();
    private static Map<Id, List<Training_Course__c>> pathToModuleMap;
    private static Map<Id, Set<Id>> pathTakenToModuleRelMap;

    public static void processModuleEnrollment(Training_Course_Taken__c[] pTakens) {
        Set<Id> moduleIds = new Set<Id>();
        Set<Id> contactIds = new Set<Id>();
        Map<Id, Training_Course_Relationship__c> moduleToTrailMap = new Map<Id, Training_Course_Relationship__c>();
        Set<Id> trailIds = new Set<Id>();
        // variable has a concatenated key trailId + contactId
        Set<String> hasTrailTaken = new Set<String>();
        Training_Course_Taken__c tmpTaken;
        Map<String, Training_Course_Taken__c> insertTakensMap = new Map<String, Training_Course_Taken__c>();

        for (Training_Course_Taken__c taken :pTakens) {
            if (taken.Course_Level__c == TrainingCourseTaken_Constants.COURSE_LEVEL_MODULE &&
                taken.Training_Course__c != null &&
                taken.Contact__c != null
            ) {
                moduleIds.add(taken.Training_Course__c);
                contactIds.add(taken.Contact__c);
            }
        }

        if (!moduleIds.isEmpty() && !contactIds.isEmpty()) {
            for (Training_Course_Relationship__c courseRel :[
                select Id, Related_Module__c, Related_Path__c, Related_Path__r.Tracked__c
                from Training_Course_Relationship__c
                where Related_Module__c in :moduleIds
            ]) {
                moduleToTrailMap.put(courseRel.Related_Module__c, courseRel);
            }

            for (Training_Course_Relationship__c rel :moduleToTrailMap.values()) {
                trailIds.add(rel.Related_Path__c);
            }

            for (Training_Course_Taken__c taken :[
                select Training_Course__c, Contact__c
                from Training_Course_Taken__c
                where Training_Course__c in :trailIds
                and Contact__c in :contactIds
            ]) {
                hasTrailTaken.add(String.valueOf(taken.Training_Course__c) + String.valueOf(taken.Contact__c));
            }

            for (Training_Course_Taken__c taken :pTakens) {
                if (taken.Course_Level__c == TrainingCourseTaken_Constants.COURSE_LEVEL_MODULE &&
                    taken.Training_Course__c != null &&
                    taken.Contact__c != null &&
                    moduleToTrailMap.containsKey(taken.Training_Course__c) &&
                    !hasTrailTaken.contains(
                        String.valueOf(moduleToTrailMap.get(taken.Training_Course__c).Related_Path__c) +
                        String.valueOf(taken.Contact__c)) &&
                    moduleToTrailMap.get(taken.Training_Course__c).Related_Path__r.Tracked__c
                ) {
                    tmpTaken = (Training_Course_Taken__c)Training_Course_Taken__c.sObjectType.newSObject(
                        TrainingCourseTaken_Constants.RT_ID_VOLUNTARY,
                        true
                    );
                    tmpTaken.Attendee_Email__c = taken.Attendee_Email__c;
                    tmpTaken.Contact__c = taken.Contact__c;
                    tmpTaken.Progress__c = 0.0;
                    tmpTaken.Status__c = TrainingCourseTaken_Constants.STATUS_COURSE_NOT_STARTED;
                    tmpTaken.Training_Course__c = moduleToTrailMap.get(taken.Training_Course__c).Related_Path__c;

                    insertTakensMap.put(
                        String.valueOf(moduleToTrailMap.get(taken.Training_Course__c).Related_Path__c) +
                            String.valueOf(taken.Contact__c),
                        tmpTaken
                    );
                }
            }

            if (!insertTakensMap.isEmpty()) {
                insert insertTakensMap.values();
            }
        }
    }

    public static void processTrailEnrollment(Training_Course_Taken__c[] pTakens) {
        List<Training_Course_Taken__c> pathsTaken = new List<Training_Course_Taken__c>();
        Set<Id> trainingCourseIds = new Set<Id>();
        Set<Id> pathTakenIds = new Set<Id>();
        List<Training_Course_Taken__c> modulesTaken = new List<Training_Course_Taken__c>();
        List<Training_Course_Taken_Relationship__c> tctrs = new List<Training_Course_Taken_Relationship__c>();

        for (Training_Course_Taken__c taken :pTakens) {
            if (taken.Course_Level__c == SC_TrainingCourseTakenManager.PATH &&
                    taken.Training_Course__c != null &&
                    taken.Contact__c != null
            ) {
                pathsTaken.add(taken);
                trainingCourseIds.add(taken.Training_Course__c);
                SC_TrainingCourseTakenManager.contactIds.add(taken.Contact__c);
                pathTakenIds.add(taken.Id);
            }
        }

        if (!pathsTaken.isEmpty()) {
            // populate the path to module map will all the related modules in a trail.
            SC_TrainingCourseTakenManager.pathToModuleMap =
                SC_TrainingCourseTakenUtil.getPathToModuleMap(trainingCourseIds);

            SC_TrainingCourseTakenManager.findTakenModules();

            // created TCTR for each module under the path for the Contact and look at modules already taken.
            for (Training_Course_Taken__c taken :pTakens) {
                if (SC_TrainingCourseTakenManager.pathToModuleMap.containsKey(taken.Training_Course__c)) {
                    for (Training_Course__c module :SC_TrainingCourseTakenManager.pathToModuleMap.get(
                        taken.Training_Course__c
                    )) {
                        String moduleKey = '' + module.Id + taken.Contact__c;
                        if (!SC_TrainingCourseTakenManager.moduleTakenMap.containsKey(moduleKey)) {
                            Training_Course_Taken__c tmpTaken = (Training_Course_Taken__c) 
                                Training_Course_Taken__c.sObjectType.newSObject(TrainingCourseTaken_Constants.RT_ID_VOLUNTARY, true);                
                            tmpTaken.Contact__c = taken.Contact__c;
                            tmpTaken.Training_Course__c = module.Id;
                            modulesTaken.add(tmpTaken);
                            SC_TrainingCourseTakenManager.moduleTakenMap.put(moduleKey, tmpTaken);
                        } 
                    }
                }
            }

            if (!modulesTaken.isEmpty()) {
                insert modulesTaken;
            }

            // running getPathToModuleMap and findTakenModules again to get all the newly created Training_Course_Taken__c records.
            SC_TrainingCourseTakenManager.pathToModuleMap =
                SC_TrainingCourseTakenUtil.getPathToModuleMap(trainingCourseIds);

            SC_TrainingCourseTakenManager.findTakenModules();

            SC_TrainingCourseTakenManager.getCurrentTakenRelationships(pathTakenIds);

            for (Training_Course_Taken__c taken :pTakens) {
                if (SC_TrainingCourseTakenManager.pathToModuleMap.containsKey(taken.Training_Course__c)) {
                    for (Training_Course__c module :SC_TrainingCourseTakenManager.pathToModuleMap.get(
                        taken.Training_Course__c
                    )) {
                        if (SC_TrainingCourseTakenManager.moduleTakenMap.containsKey(module.Id + '' + taken.Contact__c)) {
                            if (!SC_TrainingCourseTakenManager.pathTakenToModuleRelMap.containsKey(
                                    SC_TrainingCourseTakenManager.moduleTakenMap.get(module.Id + '' + taken.Contact__c).Id
                            )) {
                                tctrs.add(new Training_Course_Taken_Relationship__c(
                                    Related_Path_Taken__c = taken.Id,
                                    Related_Module_Taken__c =
                                        SC_TrainingCourseTakenManager.moduleTakenMap.get(
                                            module.Id + '' + taken.Contact__c).Id
                                ));
                            }
                        }
                    }
                }
            }

            if (!tctrs.isEmpty()) {
                insert tctrs;
            }

            // update path taken stats if needed
            // this is a bit of recursion, but is required since we need the relationship
            // records and modules to be created first
            Training_Course_Taken__c[] pathsTakenUpdated = getPathTakenUpdates(modulesTaken);
            if (!pathsTakenUpdated.isEmpty()) {
                update pathsTakenUpdated;
            }
        }
    }

    private static Training_Course_Taken__c[] getPathTakenUpdates(Training_Course_Taken__c[] modulesTaken) {
        Map<Id, Training_Course_Taken__c> pathsTakenCalc = new Map<Id, Training_Course_Taken__c>(
            SC_TrainingCourseTakenModuleManager.getRelatedPathTakenStatus(modulesTaken));
        Map<Id, Training_Course_Taken__c> pathsTakenCur = new Map<Id, Training_Course_Taken__c>([
            SELECT Date_Completed__c, Date_Started__c, Progress__c, Status__c
            FROM Training_Course_Taken__c WHERE Id IN :pathsTakenCalc.keySet()]);
        Training_Course_Taken__c[] pathsTakenUpdated = new Training_Course_Taken__c[0];
        for (Training_Course_Taken__c pathTaken : pathsTakenCur.values()) {
            if (
                pathTaken.Date_Completed__c != pathsTakenCalc.get(pathTaken.Id).Date_Completed__c ||
                pathTaken.Date_Started__c != pathsTakenCalc.get(pathTaken.Id).Date_Started__c ||
                pathTaken.Progress__c != pathsTakenCalc.get(pathTaken.Id).Progress__c ||
                pathTaken.Status__c != pathsTakenCalc.get(pathTaken.Id).Status__c
            ) {
                pathTaken.Date_Completed__c = pathsTakenCalc.get(pathTaken.Id).Date_Completed__c;
                pathTaken.Date_Started__c = pathsTakenCalc.get(pathTaken.Id).Date_Started__c;
                pathTaken.Progress__c = pathsTakenCalc.get(pathTaken.Id).Progress__c;
                pathTaken.Status__c = pathsTakenCalc.get(pathTaken.Id).Status__c;
                pathsTakenUpdated.add(pathTaken);
            }
        }
        return pathsTakenUpdated;
    }

    private static void findTakenModules() {
        Set<Id> moduleIds = new Set<Id>();

        for(Id i :SC_TrainingCourseTakenManager.pathToModuleMap.keySet()) {
            for (Training_Course__c module :SC_TrainingCourseTakenManager.pathToModuleMap.get(i)) {
                moduleIds.add(module.Id);
            }
        }

        for (Training_Course_Taken__c moduleTaken :[
            select Id, Training_Course__c, Contact__c
            from Training_Course_Taken__c
            where Training_Course__c in :moduleIds
            and Contact__c in :SC_TrainingCourseTakenManager.contactIds
        ]) {
            SC_TrainingCourseTakenManager.moduleTakenMap.put(
                moduleTaken.Training_Course__c + '' + moduleTaken.Contact__c,
                moduleTaken
            );
        }
    }

    private static void getCurrentTakenRelationships(Set<Id> pTakens) {
        SC_TrainingCourseTakenManager.pathTakenToModuleRelMap = new Map<Id, Set<Id>>();

        for (Training_Course_Taken_Relationship__c rel :[
            select Related_Module_Taken__c, Related_Path_Taken__c
            from Training_Course_Taken_Relationship__c
            where Related_Path_Taken__c in :pTakens
        ]) {
            if (SC_TrainingCourseTakenManager.pathTakenToModuleRelMap.containsKey(rel.Related_Module_Taken__c)) {
                SC_TrainingCourseTakenManager.pathTakenToModuleRelMap.get(
                    rel.Related_Module_Taken__c).add(
                        rel.Id
                );
            } else {
                SC_TrainingCourseTakenManager.pathTakenToModuleRelMap.put(
                    rel.Related_Module_Taken__c,
                    new Set<Id> {rel.Id});
            }
        }
    }
}
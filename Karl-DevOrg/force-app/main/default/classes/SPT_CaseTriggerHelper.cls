/***********************************************************
* Helper class for SPT_CaseTriggerHandler
* Created by - Swarnima Singh Mandhata
* Date      - 23rd December 2019
************************************************************/
public class SPT_CaseTriggerHelper {
    
     public static void populateLastUpdateByCaseOwner(List<Case> newList, Map<Id,Case> oldMap){
        // Checking if any of the field values are changed 
        for(Case caseRec : newList){
            if(oldMap!=null && (caseRec.SPT_Proposed_Start_Date__c != oldMap.get(caseRec.Id).SPT_Proposed_Start_Date__c ||
                                caseRec.SPT_Project_Size__c != oldMap.get(caseRec.Id).SPT_Project_Size__c ||
                                caseRec.SPT_Vendor__c != oldMap.get(caseRec.Id).SPT_Vendor__c ||
                                caseRec.SPT_Agreed_To_Terms__c != oldMap.get(caseRec.Id).SPT_Agreed_To_Terms__c ||
                                caseRec.Status != oldMap.get(caseRec.Id).Status) ||
                                caseRec.Subject != oldMap.get(caseRec.Id).Subject ||
                                caseRec.Priority != oldMap.get(caseRec.Id).Priority ||
                                caseRec.Origin != oldMap.get(caseRec.Id).Origin ||
                                caseRec.ContactId != oldMap.get(caseRec.Id).ContactId ||
                                caseRec.ParentId != oldMap.get(caseRec.Id).ParentId ||
                                caseRec.OwnerId != oldMap.get(caseRec.Id).OwnerId ||
                                caseRec.Internal_Support_Category__c != oldMap.get(caseRec.Id).Internal_Support_Category__c ||
                                caseRec.SPT_Data_Sensitivity__c != oldMap.get(caseRec.Id).SPT_Data_Sensitivity__c ||
                                caseRec.SPT_Need_By_Date__c != oldMap.get(caseRec.Id).SPT_Need_By_Date__c) {  
                                caseRec.SPT_Last_Case_Owner_Comment_Time__c = System.now();                    
            }
        }
     }
    
    /*
    * Below method checks certain case field values are changed or not.
    * If yes, calls Future method.
    */
    public static void updateInsertSupportCaseRecord(List<Case> newList, Map<Id,Case> oldMap){
        Set<Id> caseIdSet = new Set<Id>(); 
        
        // Checking if any of the field values are changed 
        for(Case caseRec : newList){
            if(oldMap!=null && (caseRec.SPT_Proposed_Start_Date__c != oldMap.get(caseRec.Id).SPT_Proposed_Start_Date__c ||
                                caseRec.SPT_Project_Size__c != oldMap.get(caseRec.Id).SPT_Project_Size__c ||
                                caseRec.SPT_Vendor__c != oldMap.get(caseRec.Id).SPT_Vendor__c ||
                                caseRec.SPT_Agreed_To_Terms__c != oldMap.get(caseRec.Id).SPT_Agreed_To_Terms__c ||
                                caseRec.Status != oldMap.get(caseRec.Id).Status) ||
                                caseRec.Subject != oldMap.get(caseRec.Id).Subject ||
                                caseRec.Priority != oldMap.get(caseRec.Id).Priority ||
                                caseRec.Origin != oldMap.get(caseRec.Id).Origin ||
                                caseRec.ContactId != oldMap.get(caseRec.Id).ContactId ||
                                caseRec.ParentId != oldMap.get(caseRec.Id).ParentId ||
                                caseRec.OwnerId != oldMap.get(caseRec.Id).OwnerId ||
                                caseRec.Internal_Support_Category__c != oldMap.get(caseRec.Id).Internal_Support_Category__c ||
                                caseRec.SPT_Data_Sensitivity__c != oldMap.get(caseRec.Id).SPT_Data_Sensitivity__c ||
                                caseRec.SPT_Need_By_Date__c != oldMap.get(caseRec.Id).SPT_Need_By_Date__c) {  
                                   //caseRec.SPT_Last_Case_Owner_Comment_Time__c = datetime.now(); 
                caseIdSet.add(caseRec.Id); 
                                    
            }
        }
        
        // Calling future method if there are changes in the fields of case
        if(caseIdSet!=null && !caseIdSet.isEmpty()){
            System.debug('>>>>>>' + 'Future method');
            SPT_CaseTriggerHelper.callFutureMethod(caseIdSet);
        }
    }

    /*
    * This method handles the sync of case field values between security and supportForce org.
    */
    @future(callout=true)
    public static void callFutureMethod(Set<Id> caseRecordIdSet){
        Map<Id, String> IdVsVendorMap = new Map<Id,String>();
        Map<Id, String> secOwnerIdToEmpNumMap = new Map<Id, String>();
        Map<Id, String> secOwnerIdToEmpEmailMap = new Map<Id, String>();
        Map<String, Id> sfEmpNumToOwnerIdMap = new Map<String, Id>();
        Map<String, Id> secEmpEmailTosfUserId = new Map<String, Id>();
        Map<Id, String> contactIdToEmailMap = new Map<Id, String>();
        Map<String, Id> sfEmailToExternalIdMap = new Map<String, Id>();
        Map<Id, String> secQueueIdToNameMap = new Map<Id, String>();
        Map<String, Id> sfQueueNameToIdMap = new Map<String, Id>();
        List<Case__x> updateExternalCaseList = new List<Case__x>();
        List<Case> internalCaseList = new List<Case>();
        List<String> queueNamesList = new List<String>{'Trust - Application Security Assurance', 'Trust - Enterprise Security Infra', 'Trust - Mergers and Acquisitions'};
        List<Id> sfQueueIdsList = new List<Id>{(Id)SPT_Constants.SF_TRUST_APPLICATION_QUEUE_ID, (Id)SPT_Constants.SF_TRUST_ENTERPRISE_QUEUE_ID, (Id)SPT_Constants.SF_TRUST_MERGERS_QUEUE_ID};
        Set<Id> result = new Set<Id>();
        Set<Id> ownerIdSet = new Set<Id>();
        Set<Id> contactIdSet = new Set<Id>();
        Set<Id> parentCaseIdSet = new Set<Id>();

        // This loop creates set of ownerIds, contactIds and parentCaseIds
        for(Case caseRecord : [SELECT ID,Subject,Description,SPT_Proposed_Start_Date__c,Enterprise_Security_Case_Number__c,SPT_Project_Size__c,
                               SPT_Agreed_To_Terms__c, SPT_Vendor__c,Status,SupportForce_Case_Id__c, RecordTypeId, Origin, Priority, OwnerId, 
                               ContactId, ParentId, Parent.SupportForce_Case_Id__c, Internal_Support_Category__c, SPT_Data_Sensitivity__c,
                               SPT_Need_By_Date__c, SPT_Communication_Flag__c, SPT_Last_Case_Comment__c,    
                               SPT_Last_Case_Owner_Comment_Time__c, SPT_Last_Customer_Comment_Time__c
                               FROM Case 
                               WHERE Id IN :caseRecordIdSet]) {
            if(caseRecord.ContactId != null)
                contactIdSet.add(caseRecord.ContactId);
            if(caseRecord.ParentId != null)
                parentCaseIdSet.add(caseRecord.Parent.SupportForce_Case_Id__c);
            String ownerId = String.valueOf(caseRecord.OwnerId);
            if(ownerId!= null && ownerId.substring(0, 3) != '00G') {
                ownerIdSet.add(caseRecord.OwnerId);
            }
            internalCaseList.add(caseRecord);
        }

        // Creating a map of Queue Id To it's Name in security org.
        for(Group queueRecord : [SELECT Id, Type, Name FROM Group WHERE Type = 'Queue' AND Name IN :queueNamesList ORDER BY Name ASC]) {
            secQueueIdToNameMap.put(queueRecord.Id, queueRecord.Name);
        }

        // Creating a map of Queue Name To it's Id in SF org.
        for(Integer i=0; i<queueNamesList.size(); i++) {
            sfQueueNameToIdMap.put(queueNamesList.get(i), sfQueueIdsList.get(i));
        }

        //Below 2 for loops create owner field mapping based on it's employee number NOW BASED ON EMAIL
        if(ownerIdSet != null && !ownerIdSet.isEmpty()) {
            for(User secUserRecord : [SELECT Id, EmployeeNumber, Email FROM User WHERE Id IN :ownerIdSet]) {
				// BELOW lines replaced by email logic now
                //if(secUserRecord.EmployeeNumber != null) 
                //    secOwnerIdToEmpNumMap.put(secUserRecord.Id, secUserRecord.EmployeeNumber);
                if(secUserRecord.Email != null)
                    secOwnerIdToEmpEmailMap.put(secUserRecord.Id, secUserRecord.Email);                                    
            }
            System.debug('Owner Map 1: '+secOwnerIdToEmpNumMap);

			// Leverage the fact that contact now has supportforce user id
            for (Contact secConRecord: [SELECT Id, Email, SupportForce_User__c FROM Contact 
                                        WHERE Email IN: secOwnerIdToEmpEmailMap.values()
                						AND RecordTypeId IN :SPT_Constants.SPT_EMPLOYEE_RECORD_TYPE_ID]) {
            	secEmpEmailTosfUserId.put(secConRecord.Email, secConRecord.SupportForce_User__c);                    
            }            
            
			// BELOW lines replaced by email logic now
            //for(SupportForceUser__x sfUserRecord : [SELECT Id, ExternalId, EmployeeNumber__c FROM SupportForceUser__x WHERE EmployeeNumber__c IN :secOwnerIdToEmpNumMap.values()]) {
            //    sfEmpNumToOwnerIdMap.put(sfUserRecord.EmployeeNumber__c, sfUserRecord.ExternalId);
            //}
            System.debug('Owner Map 2: '+sfEmpNumToOwnerIdMap);
        }

        // Below 2 for loops create contact field mapping based on it's email
        if(contactIdSet!=null && !contactIdSet.isEmpty()) {
            for(Contact secContact : [SELECT Id, Email FROM Contact WHERE Id IN :contactIdSet]) {
                contactIdToEmailMap.put(secContact.Id, secContact.Email);
            }
            System.debug('Customer Map 1: '+contactIdToEmailMap);

            for(Contact__x sfContact : [SELECT Id, ExternalId, Email__c, RecordTypeId__c FROM Contact__x WHERE Email__c IN :contactIdToEmailMap.values() AND RecordTypeId__c IN :SPT_Constants.SF_CONTACTS_RECORD_TYPE_ID]) {
                sfEmailToExternalIdMap.put(sfContact.Email__c, sfContact.ExternalId);
            }
            System.debug('Customer Map 2: '+sfEmailToExternalIdMap);
        }
        
        // This for loop is handling the sync of every single case record
        for(Case caseRecord : internalCaseList){
            Case__x supportExternalCase = new Case__x();
            system.debug(caseRecord.parentID);
            system.debug(caseRecord.Parent.SupportForce_Case_Id__c);
            String externalCaseParentId = String.valueOf(caseRecord.Parent.SupportForce_Case_Id__c);
            // Mappping of Parent Case
            if(caseRecord.ParentId != null && caseRecord.Parent.SupportForce_Case_Id__c!=null) {
                supportExternalCase.ParentId__c = externalCaseParentId;
            }
            else {
                supportExternalCase.ParentId__c = null;
            }

            // Mapping of Case Owner
            // BELOW lines replaced by email logic
            //if(secOwnerIdToEmpNumMap!=null && secOwnerIdToEmpNumMap.containsKey(caseRecord.OwnerId)
            //   && sfEmpNumToOwnerIdMap!=null && sfEmpNumToOwnerIdMap.containsKey(secOwnerIdToEmpNumMap.get(caseRecord.OwnerId))) {
            //    supportExternalCase.OwnerId__c = sfEmpNumToOwnerIdMap.get(secOwnerIdToEmpNumMap.get(caseRecord.OwnerId));
            //}
            if(secOwnerIdToEmpEmailMap!=null && secOwnerIdToEmpEmailMap.containsKey(caseRecord.OwnerId)
               && secEmpEmailTosfUserId!=null && secEmpEmailTosfUserId.containsKey(secOwnerIdToEmpEmailMap.get(caseRecord.OwnerId))) {
                supportExternalCase.OwnerId__c = secEmpEmailTosfUserId.get(secOwnerIdToEmpEmailMap.get(caseRecord.OwnerId));
            }
            else if(secQueueIdToNameMap!=null && secQueueIdToNameMap.containsKey(caseRecord.OwnerId)
                    && sfQueueNameToIdMap!=null && sfQueueNameToIdMap.containsKey(secQueueIdToNameMap.get(caseRecord.OwnerId))) {
                supportExternalCase.OwnerId__c = sfQueueNameToIdMap.get(secQueueIdToNameMap.get(caseRecord.OwnerId));
            }
            else {
                supportExternalCase.OwnerId__c = (Id)SPT_Constants.SF_TRUST_APPLICATION_QUEUE_ID;
            }

            // Mapping of Case Contact
            if(contactIdToEmailMap!=null && contactIdToEmailMap.containsKey(caseRecord.ContactId)
               && sfEmailToExternalIdMap!=null && sfEmailToExternalIdMap.containsKey(contactIdToEmailMap.get(caseRecord.ContactId))) {
                supportExternalCase.ContactId__c = sfEmailToExternalIdMap.get(contactIdToEmailMap.get(caseRecord.ContactId));
            }
            else {
                supportExternalCase.ContactId__c = null;
            }

            if(!Test.isRunningTest() && caseRecord.SupportForce_Case_Id__c !=null){
               // supportExternalCase.ExternalId = caseRecord.SupportForce_Case_Id__c;
                updateExternalCaseList.add(supportExternalCase);
                system.debug('>>>> list to be update >>>>>' + updateExternalCaseList);
            }
            else if(Test.isRunningTest()) {
                updateExternalCaseList.add(supportExternalCase);
            }
            if(caseRecord.SPT_Last_Case_Owner_Comment_Time__c != null){
                supportExternalCase.Last_Case_Owner_Comment_Time_c__c = caseRecord.SPT_Last_Case_Owner_Comment_Time__c;
                updateExternalCaseList.add(supportExternalCase);
            }

            // Below utility method syncs all the fields which are in SPT Case Field Mapping metadata
            SPT_Utility.syncInternalAndExternalCaseRecords(caseRecord, supportExternalCase, false);
        }      
               
        if(updateExternalCaseList!=null && !updateExternalCaseList.isEmpty()){ 
            result.addAll(updateExternalRecords(updateExternalCaseList, 'Update'));
            System.debug('Result: '+result);
        }       
    }
    

    // Below method after proper sync, update those synced external records
    public static Set<id> updateExternalRecords(List<SObject> records, String operation){
        List<Database.SaveResult> result = new List<Database.SaveResult>();

        if(operation=='Insert'){
            result = Database.insertImmediate(records);            
        }else{
           // result = Database.updateImmediate(records);  
            for(sObject record : records){
                sendToSupportForce((Case__x)record);
            }
        }
        System.debug('##result##'+result);
        
        // Iterate through each returned result
        Set<Id> recordIDSet = new Set<ID>();
        for (Database.SaveResult resultRecord : result) {
            if (resultRecord.isSuccess()) {
                // Operation was successful, so get the ID of the record that was processed
                System.debug('?? '+ resultRecord.getId());
                recordIDSet.add(resultRecord.getId());
            }
            else {
                System.debug('Result: '+resultRecord.getErrors());
            }
        }
        return recordIDSet;
    }
    
    public static void sendToSupportForce(Case__x externalCase) {
        try{
system.debug('>>>> externalCase.OwnerId__c: ' + externalCase.OwnerId__c);            
            String NS_SOAP = 'http://schemas.xmlsoap.org/soap/envelope/';
            String NS_SF = 'urn:partner.soap.sforce.com';
            HttpRequest soapreq = new HttpRequest();
            soapreq.setMethod('POST');
            soapreq.setTimeout(60000);
            soapreq.setEndpoint('callout:SupportForceNC/services/Soap/u/45.0');
            soapreq.setHeader('Content-Type', 'text/xml;charset=UTF-8');
            soapreq.setHeader('SOAPAction', '""');
            soapreq.setBody('<Envelope xmlns="http://schemas.xmlsoap.org/soap/envelope/"><Header/>' +
                        '<Body><login xmlns="urn:partner.soap.sforce.com"><username>' +
                        '{!HTMLENCODE($Credential.UserName)}' +
                        '</username><password>' +
                        '{!HTMLENCODE($Credential.Password)}' +
                        '</password></login></Body></Envelope>');
            HttpResponse res =  new Http().send(soapreq);

            if(res.getStatusCode() != 200) {
                Dom.Document responseDocument = res.getBodyDocument();
                Dom.Xmlnode rootElm = responseDocument.getRootElement(); // soapenv:Envelope
                Dom.Xmlnode bodyElm = rootElm.getChildElement('Body', NS_SOAP); // soapenv:Body
                Dom.Xmlnode faultElm = bodyElm.getChildElement('Fault', NS_SOAP); // soapenv:Fault
                String exceptionText = 'Login Failure';
                if (faultElm != null) {
                    Dom.Xmlnode faultStringElm = faultElm.getChildElement('faultstring', null);
                    if (faultStringElm!=null && faultStringElm.getText() != null) {
                        exceptionText = faultStringElm.getText();
                    }
                }
                System.debug('Exception' + exceptionText);
                throw new CalloutException();
            }
            
            String sessionId;
            Dom.Document responseDocument = res.getBodyDocument();
            Dom.Xmlnode rootElm = responseDocument.getRootElement(); // soapenv:Envelope
            Dom.Xmlnode bodyElm = rootElm.getChildElement('Body', NS_SOAP); // soapenv:Body
            Dom.Xmlnode loginResponseElm = bodyElm.getChildElement('loginResponse', NS_SF); // loginResponse
            if(!Test.isRunningTest()) {
                Dom.Xmlnode resultElm = loginResponseElm.getChildElement('result', NS_SF); // result
                Dom.Xmlnode sessionIdElm = resultElm.getChildElement('sessionId', NS_SF); // sessionId
                sessionId = sessionIdElm.getText();
                System.debug(sessionId);
            }
            
            HttpRequest req = new HttpRequest();
            String endPointUrl = 'callout:SupportForceNC/services/data/v48.0/sobjects/Case/';
            endPointUrl += externalCase.ExternalId;
            endPointUrl += '?_HttpMethod=PATCH';
            // req.setEndpoint('callout:Enterprise_Security_to_SPF/services/data/v40.0/limits/recordCount?sObjects=Case');
            // req.setEndpoint('callout:Enterprise_Security_to_SPF/services/data/v48.0/sobjects/Case/5008A000005tc6sQAA?_HttpMethod=PATCH');
            req.setEndpoint(endPointUrl);
            req.setHeader('Content-Type', 'application/json');
            req.setHeader('Sforce-Auto-Assign', 'FALSE');
            req.setHeader('Authorization', 'Bearer ' + sessionId);
            req.setMethod('POST');
            
            
            
            /* String jsonBody = JSON.serialize(
new Map<String, String> {
'Subject' => 'Test'}); */
            // CaseWrapper body = new CaseWrapper(externalCase); 
            Map<String, Object> externalCaseMap = new Map<String, Object>( externalCase.getPopulatedFieldsAsMap() );
            externalCaseMap.remove( 'ExternalId' );
            String body = JSON.serialize(externalCaseMap).replaceAll('_c__c','TMP_SPT');
            body = body.replaceAll('__c','');
            body = body.replaceAll('TMP_SPT','__c');
            System.debug(body);
            
            req.setBody(body);
            //  req.setBody(JSON.serialize(externalCase).replaceAll('_c__c','__c'));
            
            Http http = new Http();
            HTTPResponse response = http.send(req);
            response = http.send(req);
            if (response.getStatusCode() == 200 || response.getStatusCode() == 204) {
                System.debug('Response-' + response);
                System.debug('Body-' + response.getBody());
            }
             
        }
        catch(System.CalloutException e){
            System.debug('Error-' + e.getMessage());   
        }
    }

    /**
     * Below method update data of the case status object records
     */
    
    
    public static void updateCaseStatusRecord(List<Case> newList, Map<Id, Case> oldMap) {
        List<Case_Status__c> updatedCaseStatusList = new List<Case_Status__c>();
        List<Case> caseList = new List<Case>();
        Set<Id> caseIdSet = new Set<Id>();
        for(Case caseRecord : newList) {
            if(oldMap==null || caseRecord.Status != oldMap.get(caseRecord.Id).Status) {
                caseIdSet.add(caseRecord.Id);
                caseList.add(caseRecord);
            }
        }

        if(caseIdSet!=null && !caseIdSet.isEmpty()) { 
            if(oldMap!=null) {
                for(Case_Status__c caseStatus : [SELECT Id, Case__c, Start_Date__c, End_Date__c, Duration__c, Status__c FROM Case_Status__c WHERE Case__c IN :caseIdSet AND End_Date__c = null]) {
                    System.debug('Case Status: '+caseStatus);
                    Case_Status__c cstatus = new Case_Status__c();
                    cstatus.End_Date__c = Date.today();
                    cstatus.id = caseStatus.id;
                    updatedCaseStatusList.add(cstatus);
                }   
            }

            for(Case caseRecord : caseList) {
                Case_Status__c caseStatus = new Case_Status__c();
                caseStatus.Case__c = caseRecord.Id;
                caseStatus.Status__c = caseRecord.Status;
                caseStatus.Start_Date__c = Date.today();
                updatedCaseStatusList.add(caseStatus);
            }
            
            if(updatedCaseStatusList!=null && !updatedCaseStatusList.isEmpty()) {
                UPSERT updatedCaseStatusList;
            }   
        }
    }
    
    class CalloutException extends Exception {}
}
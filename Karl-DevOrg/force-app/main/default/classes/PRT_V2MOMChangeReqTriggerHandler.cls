public with sharing class PRT_V2MOMChangeReqTriggerHandler {
    public static void afterUpdate(List<V2MOM_Change_Request__c> changeReqList, Map<Id,V2MOM_Change_Request__c> idToChangeReqMap){
        PRT_V2MOMChangeReqTriggerHelper.notifyUserOnApproval(changeReqList, idToChangeReqMap);
        PRT_V2MOMChangeReqTriggerHelper.createUserStoryWorkRecord(changeReqList, idToChangeReqMap);
    }
}
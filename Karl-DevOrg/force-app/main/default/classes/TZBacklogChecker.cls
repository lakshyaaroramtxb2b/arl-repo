global with sharing class TZBacklogChecker {
    //transfer ownership
public class BaseException extends Exception {}

    public static void run(BatchNotifier bn) { 

        Backlog_Configuration__c config = [SELECT Max_Working_Queue_Size__c,
                                               Max_Backlog_Size__c,
                                               Max_Unworked_Scan_Hours__c,
                                               Max_Allowed_Scan_Hours__c,
                                               Max_Queue_Wait_Hours__c,
                                               TZ_Org_Username__c,
                                               TZ_Org_Password__c
                                            FROM Backlog_Configuration__c
                                            WHERE Active__c = True LIMIT 1];    

        partnerSoapSforceCom.Soap papi;
        partnerSoapSforceCom.LoginResult loginResult;
        partnerSoapSforceCom.QueryResult queryResult;
        
        papi = new partnerSoapSforceCom.Soap();
        try {
            loginResult = papi.login(config.TZ_Org_Username__c, config.TZ_Org_Password__c);
            } catch ( Exception e) {
                System.debug(e.getMessage());
                return;
            }
        
        
        papi.endpoint_x = loginResult.serverUrl;
        papi.SessionHeader = new partnerSoapSforceCom.SessionHeader_element();
        papi.SessionHeader.sessionId = loginResult.sessionId;
        
        sobjectPartnerSoapSforceCom.sObject_x [] queueLen = papi.query('SELECT Id, Name FROM SecurityReview__c WHERE (App__r.RecordType.Name = \'TrialTemplate\' or App__r.RecordType.Name = \'Package\') and (ScanStatus__c=\'Ready for Scan\' or ScanStatus__c=\'In Progress\')').records;
        // we don't have a status change for when it starts to actually scan (we should add that status and then monitor it)
        //sobjectPartnerSoapSforceCom.sObject_x [] workingLen = papi.query('SELECT Id FROM SecurityReview__c WHERE (App__r.RecordType.Name = \'TrialTemplate\' or App__r.RecordType.Name = \'Package\') and ScanStatus__c=\'In Progress\'').records;
        
        // This doesn't take into consideration that the ScanStarted__c flag may have just been flipped due to code pulling problems (does this comment make sense for TZ Trial too?)
        // we don't have a status change for when it starts to actually scan (we should add that status and then monitor it)
        sobjectPartnerSoapSforceCom.sObject_x [] overlyLongScan = papi.query('SELECT Id, Name FROM SecurityReview__c WHERE (App__r.RecordType.Name = \'TrialTemplate\' or App__r.RecordType.Name = \'Package\') and ScanStatus__c=\'In Progress\' AND LastModifiedDate < ' + buildSOQLDate(config.Max_Allowed_Scan_Hours__c)).records;

        // It's just not getting any attention at all, not even marked as in queue which should be relatively automatic
        sobjectPartnerSoapSforceCom.sObject_x [] longQueueWait = papi.query('SELECT Id, Name FROM SecurityReview__c WHERE (App__r.RecordType.Name = \'TrialTemplate\' or App__r.RecordType.Name = \'Package\') and ScanStatus__c=\'Ready for Scan\' AND LastModifiedDate < ' + buildSOQLDate(config.Max_Queue_Wait_Hours__c)).records;

        // Check for paused jobs
        sobjectPartnerSoapSforceCom.sObject_x [] pausedJobs = papi.query('SELECT Id, Name FROM SecurityReview__c WHERE (App__r.RecordType.Name = \'TrialTemplate\' or App__r.RecordType.Name = \'Package\') and ScanStatus__c=\'Paused\'').records;


         if (safeSize(queueLen) > config.Max_Backlog_Size__c) {
             bn.addNotifier(Notifier.Severity.ERROR,'TZBacklogChecker : Unstarted Scan queue length of '+String.valueOf(safeSize(queueLen))+' exceeds max size of '+String.valueOf(config.Max_Backlog_Size__c),makeExtra(queueLen));
         }
         /*
         if (safeSize(workingLen) > config.Max_Working_Queue_Size__c) {
             bn.addNotifier(Notifier.Severity.ERROR,'TZBacklogChecker : Total scans in progress of '+String.valueOf(safeSize(workingLen))+' exceeds max size of '+String.valueOf(config.Max_Working_Queue_Size__c),makeExtra(workingLen));
         }
         */
         
         if (safeSize(pausedJobs) > 0) {
              bn.addNotifier(Notifier.Severity.ERROR,'TZBacklogChecker : ' + String.valueOf(safeSize(pausedJobs))+' scan(s) are paused.',makeExtra(pausedJobs));

         }
         
         if (safeSize(overlyLongScan) > 0) {
             bn.addNotifier(Notifier.Severity.ERROR,'TZBacklogChecker : ' + String.valueOf(safeSize(overlyLongScan))+' scan(s) running longer than allowable '+String.valueOf(config.Max_Allowed_Scan_Hours__c)+' hours',makeExtra(overlyLongScan));
         }
         if (safeSize(longQueueWait) > 0) {
             bn.addNotifier(Notifier.Severity.ERROR,'TZBacklogChecker : ' + String.valueOf(safeSize(longQueueWait))+' scan(s) waiting in queue (not inProgress) longer than allowable '+String.valueOf(config.Max_Queue_Wait_Hours__c)+' hours',makeExtra(longQueueWait));
         }
         //bn.addNotifier(Notifier.Severity.DEBUG,'TZBacklogChecker : complete: QueueLen['+String.valueOf(safeSize(queueLen))+'] WorkingLen['+String.valueOf(safeSize(workingLen))+'] overlyLongScan['+String.valueOf(safeSize(overlyLongScan))+'] longQueueWait['+safeSize(longQueueWait)+']');
         bn.addNotifier(Notifier.Severity.DEBUG,'TZBacklogChecker : complete: QueueLen['+String.valueOf(safeSize(queueLen))+'] overlyLongScan['+String.valueOf(safeSize(overlyLongScan))+'] longQueueWait['+safeSize(longQueueWait)+']');
    }
   
    private static List<String> makeExtra(List<sobjectPartnerSoapSforceCom.sObject_x> objs) {
        List<String> ret = new List<String>();
        if (objs == null)
            return ret;
        for (sobjectPartnerSoapSforceCom.sObject_x c : objs) {
            ret.add(c.Id + ' - ' + c.Name);
        }
        return ret;
    }
    
    private static String buildSOQLDate(Decimal offset) {
        Datetime x = Datetime.now().addHours(0-Math.round(offset));
        return x.format('yyyy-MM-dd')+ 'T' + x.format('HH:mm:ss')+ '-08:00';    
    }
    
    private static Integer safeSize(sobjectPartnerSoapSforceCom.sObject_x [] arr) {
        if (arr == null)
            return 0;
        else
            return arr.size();
    }
    
    public static testMethod void doTest() {
        delete [select Id from Backlog_Configuration__c];
        insert new Backlog_Configuration__c(Max_Working_Queue_Size__c=2,
                                       Max_Backlog_Size__c=6,
                                       Max_Unworked_Scan_Hours__c=4,
                                       Max_Allowed_Scan_Hours__c=8,
                                       Max_Queue_Wait_Hours__c=1,
                                       Active__c = True);
        List<Scan_Queue__c> testSet = new List<Scan_Queue__c>();
        /*
        // longQueueWait
        testSet.add(new Scan_Queue__c(name='longQueueWait-Test',Created__c=Datetime.now().addHours(-3),Username__c='test@test.org'));
        
        // overlyLongScan
        testSet.add(new Scan_Queue__c(name='overlyLongScan-Test',Created__c=Datetime.now().addHours(-9),inProgress__c=True,ScanStarted__c=True,Username__c='test@test.org'));
        
        // tooOldUnworked
        testSet.add(new Scan_Queue__c(name='tooOldUnworked-Test',Created__c=Datetime.now().addHours(-5),inProgress__c=True,Username__c='test@test.org'));
        
        // workingLen, trigger sets Created__c
        testSet.add(new Scan_Queue__c(name='workingLen-Test1',ScanStarted__c=True,Username__c='test@test.org'));
        testSet.add(new Scan_Queue__c(name='workingLen-Test2',ScanStarted__c=True,Username__c='test@test.org'));
        testSet.add(new Scan_Queue__c(name='workingLen-Test3',ScanStarted__c=True,Username__c='test@test.org'));
        
        // queueLen, trigger sets Created__c
        testSet.add(new Scan_Queue__c(name='queueLen-Test1',Username__c='test@test.org'));
        testSet.add(new Scan_Queue__c(name='queueLen-Test2',Username__c='test@test.org'));
        testSet.add(new Scan_Queue__c(name='queueLen-Test3',Username__c='test@test.org'));
        testSet.add(new Scan_Queue__c(name='queueLen-Test4',Username__c='test@test.org'));
        testSet.add(new Scan_Queue__c(name='queueLen-Test5',Username__c='test@test.org'));
        testSet.add(new Scan_Queue__c(name='queueLen-Test6',Username__c='test@test.org'));
        testSet.add(new Scan_Queue__c(name='queueLen-Test7',Username__c='test@test.org'));
        
        insert testSet;
        
        run();
        */
    }   
}
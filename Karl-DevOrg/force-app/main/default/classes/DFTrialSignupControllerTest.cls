@isTest                                
public class DFTrialSignupControllerTest {
    // Trial Success
    static testmethod void testDF14SignupSuccess() {
        insert new Trial_Id__c(name='DF14',Trial_Id__c='0TT500000004xdw');
        DFTrialSignupController dfsc = new DFTrialSignupController();
        dfsc.msa = true;
        dfsc.contactOK = true;
        dfsc.firstName = 'Bob';
        dfsc.lastName = 'Bob'; 
        dfsc.company = 'Salesforce.com';
        dfsc.email = 'ktobener@salesforce.com' ;
        dfsc.createSignup();
        system.debug(dfsc.publickey);
        dfsc.reset();
    }
    // Trial Fail
    static testmethod void testDF14SignupFail() {
        insert new Trial_Id__c(name='DF14',Trial_Id__c='0TT500000004xdw');
        DFTrialSignupController dfsc = new DFTrialSignupController();
        dfsc.msa = false;
        dfsc.contactOK = false;
        dfsc.firstName = '';
        dfsc.lastName = ''; 
        dfsc.company = '';
        dfsc.email = '' ;
        dfsc.createSignup();
    }    
}
/*
* Class Name: GRC_DeploymentUtility
* Description: Utility class for updating existing records.
* Author/Date:  Banshi / 01.29.2020
* Date New/Modified: 01.29.2020
*
*/
public class GRC_DeploymentUtility {
    public static void updateEsaHVCPToCCPProfile(){
        GRC_DeploymentUtilityBatch bc = new GRC_DeploymentUtilityBatch();
        bc.ACTION_UPDATE_ESA_PROFILE = true;
        Database.executeBatch(bc);
    }
    public static void populateExternalFields(){
        GRC_DeploymentUtilityBatch bc = new GRC_DeploymentUtilityBatch();
        bc.ACTION_UPDATE_EXTERNAL_FIELDS = true;
        Database.executeBatch(bc);
    }
}
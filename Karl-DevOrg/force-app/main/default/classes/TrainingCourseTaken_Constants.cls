/*
Developer: Ralph Callaway <ralph@callaway.cloud>
Description:
    Cosntants for the Training_Course_Taken__c object
*/
public class TrainingCourseTaken_Constants {

    public static final Id RT_ID_MANDATORY = '01230000001GfjC';
    public static final Id RT_ID_VOLUNTARY = '01230000001GfjD';

    public static final String COURSE_LEVEL_MODULE = TrainingCourse_Constants.COURSE_LEVEL_MODULE;
    public static final String COURSE_LEVEL_TRAIL = TrainingCourse_Constants.COURSE_LEVEL_TRAIL;

    public static final String STATUS_COURSE_COMPLETED = 'Course Completed';
    public static final String STATUS_COURSE_STARTED = 'Course Started';
    public static final String STATUS_COURSE_NOT_STARTED = 'Course Not Started';
}
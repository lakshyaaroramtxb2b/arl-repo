/* 

    Developer Name: Jorge L Caceres - jcaceres@salesforce.com | February 2015
    
    Description:
        Action based on email content.
      
*/ 
global class ESAaravoEmailHandler implements Messaging.InboundEmailHandler {

    /* tags for parsing text */

    private static final String SUPPLIER_TAG = 'SUPPLIER';    
    private static final String SUPPLIER_COUNTRY_TAG = 'SUPPLIER COUNTRY';    
    private static final String BUSINESS_PROCESS_TAG = 'BUSINESS PROCESS';    
    private static final String BUSINESS_PROCESS_ID_TAG = 'BUSINESS PROCESS ID';    
    private static final String TASK_NAME_TAG = 'TASK NAME';    
    private static final String REQUESTER_NAME_TAG = 'REQUESTER';    
    private static final String BUSINESSCONTACT_EMAIL_TAG = 'REQUESTER EMAIL';    
    private static final String TAKE_ACTION_BY_TAG = 'TAKE ACTION BY OR BEFORE';    
    private static final String DESCRIPTION_BEGIN_TAG = 'requires your timely review and approval:';
    private static final String ON_BEHALF_OF_NAME = 'ON BEHALF OF NAME';
    private static final String ON_BEHALF_OF_EMAIL = 'ON BEHALF OF EMAIL';
    private static final List<String> TAGS = new List<String>{SUPPLIER_TAG, SUPPLIER_COUNTRY_TAG, BUSINESS_PROCESS_TAG, BUSINESS_PROCESS_ID_TAG,TASK_NAME_TAG, 
        REQUESTER_NAME_TAG, BUSINESSCONTACT_EMAIL_TAG, TAKE_ACTION_BY_TAG, DESCRIPTION_BEGIN_TAG, ON_BEHALF_OF_NAME, ON_BEHALF_OF_EMAIL};

    /* constants */

    @TestVisible private static final String SUBJECT = 'Supplier Portal Security Review';    
    @TestVisible private static final String PRIORITY = '3 - Low';    
    @TestVisible private static final String STATUS = 'Review';
    @TestVisible private static final String SF_SEC_RECORDTYPEID = '0120000000001TgAAI';
    @TestVisible private static final String SF_SEC_EMP_RECORDTYPEID = '0120000000000HPAAY';
    @TestVisible private static final String INTERNAL_SUPPORT_CATEGORY = 'Security: Enterprise Security: Vendor Security Testing';
    @TestVisible private static final String SF_ORGID;  // needs to be a method in SF UTIL to get current org ID
        {SF_ORGID = sfutil.ESASupportForceUtil.getIsProduction()? '00D00000000hg76' : '00DV00000088Yba';}            
    @TestVisible private static final String E2C_ADDRESS;
        {E2C_ADDRESS = sfutil.ESASupportForceUtil.getIsProduction()? 'trust_esa@x-1m850z6xp13rkhsi53dzdee2nzirigrv0yi2jciafwxg62uxik.0-hg76eaa.na5.case.salesforce.com' :
                                                                     'sectech@35i5v46j592kmbhfogkofotnvl5iu4vyheyomt8zewopgj5o4j.v-88ybamae.cs12.case.sandbox.salesforce.com';}            
    @TestVisible private static final String SUPPORTFORCE_LINK;
        {SUPPORTFORCE_LINK = sfutil.ESASupportForceUtil.getIsProduction()? 'https://concierge.it.salesforce.com/tickets/SupportForce-' :
                                                                           'https://supportforce--full2.cs12.my.salesforce.com/';}   
    
    @TestVisible private static final String SUPPORTFORCE_CASE_LINK;
        {SUPPORTFORCE_CASE_LINK = sfutil.ESASupportForceUtil.getIsProduction()? 'https://supportforce.lightning.force.com/' :
                                                                           'https://supportforce--spfuat.lightning.force.com/';}   
    
    
    private static final String CASE_COUNT_QUERY = 'RecordTypeId = \'' + SF_SEC_RECORDTYPEID + '\''
                                                 + ' and Status = \'' + STATUS + '\''
                                                 + ' and Vendor__c != null ';
                                                                                    
    /* class variables */

    @TestVisible private Case sfCase;
    @TestVisible private CaseComment sfCaseComment;
    @TestVisible private Contact businessContact;
    @TestVisible private Contact__x businessContactX;
    
    private static final String CASE_COMMENT = ''
        + 'Hello,\n\n'
        + 'We are tracking Trust approval for {vendor} on this ticket. '
        + 'Your ticket is #{count} in line for triage. '
        + 'We will follow up with additional requirements and questions in the order your request was received.\n\n'
        + 'Regards,\n'
        + 'Vendor & Application Security Team';

    private static final String CASE_EMAIL = ''
        + 'Hello {requester},\n\n'
        + 'A ticket has been created to track security approval for {vendor}, '
        + 'the vendor you have requested in the supplier portal. '
        + 'To view next steps and follow up, please login to Concierge or use this URL:\n\n'
        + '{supportforcelink}{caseid}\n\n'
        + '{IntakeMessage} \n {IntakeUrl}\n\n'
        + 'Regards,\n'
        + 'Vendor & Application Security Team\n\n\n\n\n'
        + '{email2caseref}';

    private static final String DUPLICATE_CASE_EMAIL = ''
        + 'Hello {requester},<br/><br/>'
        + 'We were unable to create a ticket for you for {vendor} '
        + 'as we found an existing case for it created within the last {days} days. <br/><br/> '
        + 'You can review the existing case in SupportForce using the following link: <br/><br/>'
        + '{supportforcelink}{caseid}<br/><br/>'
        + 'Regards,<br/>'
        + 'Vendor & Application Security Team';

    private static final String CASE_SUBJECT = 'Security Review For Supplier Portal Request: {vendor}';

    private static final String ORG_WIDE_EMAIL_ADDRESS = ESA_AppConstants.ORG_WIDE_EMAIL_ADDRESS;    
    private static OrgWideEmailAddress owea;
       {owea = [SELECT Address,Id,DisplayName FROM OrgWideEmailAddress WHERE DisplayName = :ORG_WIDE_EMAIL_ADDRESS];}
    
    Map<String, String> sfCaseCustomFields = new Map<String, String>();  
    String emailBody;  

    /* main processing method for email service */

    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
        
        // Aravo emails contain only Html body so we need to remove html tags and some other cleaning
        emailBody = email.HtmlBody;
        emailBody = emailBody.replace('*', '').replaceAll('<[^>]*>', ''); // remove html tags
        for (String tag : TAGS) emailBody = emailBody.replace((tag + '\n'), (tag + ' ')).replace((tag + '\r'), (tag + ' ')); // align tags with value in the same line
        emailBody = emailBody.replaceAll('(\r?\n)+', '\n'); // remove double spacing

        // try to get the business contact using the requesterEmail from the Aravo Case
        businessContact = new Contact();   
        businessContactX = new Contact__x();
        try {
        if (!test.isRunningTest()) {
            businessContactX = [SELECT ExternalId, 
                                       FirstName__c, 
                                       LastName__c 
                                FROM Contact__x 
                                WHERE Email__c =:getBusinessContactEmail()
                                AND RecordTypeId__c =:SF_SEC_EMP_RECORDTYPEID LIMIT 1];
            //   businessContact = sfutil.ESASupportForceUtil.getUserContact(getBusinessContactEmail()); 
        }
        } catch (exception e) {
            businessContactX = null;
            system.debug (e);
        }
 
        // create supportforce case
        Id aravoCaseId = createCase();     
        if (aravoCaseId != null) {
            Id aravoCaseCommentId = createComment(aravoCaseId);
            sendEmail(aravoCaseId);
            String vendorName = JSON.serialize(getSupplierName()).left(50).replaceAll('\"', '');
            updateCaseCommentWithExistinngCases(vendorName, aravoCaseId);
        }
                       
        return null;
    }
    
    /* private methods */ 
    
    @TestVisible
    private Id createCase() {
        String vendorName = JSON.serialize(getSupplierName()).left(50).replaceAll('\"', '');
        String openVendorCaseId;
        if(String.isNotBlank(vendorName)) {
            openVendorCaseId = existingOpenVendorCases(vendorName);
        }
        if(String.isNotBlank(openVendorCaseId)) {
            sendCasecreationFailureEmail(openVendorCaseId);
            return null;
        }
        sfCase = new Case();
        sfCase.OwnerId = getCaseOwnerId();
        sfCase.Subject = SUBJECT + ' - ' + getSupplierName();
        sfCase.Priority = PRIORITY;
        sfCase.Status = STATUS;
        sfCase.RecordTypeId = SF_SEC_RECORDTYPEID;
        sfCaseCustomFields.put('Internal_Support_Category__c', INTERNAL_SUPPORT_CATEGORY);
        sfCaseCustomFields.put('Deployment_Start_Date__c', JSON.serialize(getNeedByDate()).replaceAll('\"', '')); // required SF field
        sfCaseCustomFields.put('Need_By_Date__c', JSON.serialize(getNeedByDate()).replaceAll('\"', '')); 
        sfCaseCustomFields.put('Vendor__c', JSON.serialize(getSupplierName()).left(50).replaceAll('\"', '')); 
        sfCase.Description = getDescription();
        //if (businessContact.Id != null) sfCase.ContactId = businessContact.Id;
        if (businessContactX != null) sfCase.ContactId = businessContactX.ExternalId;
        String sfCaseJson = addCustomFields(JSON.serialize(sfCase), sfCaseCustomFields);           
        Id newCaseId;
        if (!test.isRunningTest()) newCaseId = sfutil.ESASupportForceUtil.insertCase(sfCaseJson);  
        return newCaseId;
    }

    /**
     * To get Any open cases for current vendor in supportforce
     * @param  vendorName [current vendor name]
     * @return            [True - Open cases , False - No open cases]
     */
    
    @TestVisible
    private String existingOpenVendorCases(String vendorName){
        DateTime caseCreatedDate = Date.today().addDays(-Integer.valueOf(Label.Esa_Aaravo_Case_Age));
        List<Case__x> lstCases = [SELECT ExternalID 
                                  FROM Case__x 
                                  WHERE Vendor_c__c =:vendorName
                                  AND RecordTypeId__c =:SF_SEC_RECORDTYPEID
                                  AND Internal_Support_Category_c__c =:INTERNAL_SUPPORT_CATEGORY
                                  AND Status__c != 'Closed'
                                  AND CreatedDate__c >= :caseCreatedDate];
        if(lstCases.size() > 0) {
            return lstCases[0].ExternalID;
        } else{
            return null;
        }
    }

    /**
     * Get Existing Closed cases current vendor
     * @param  vendorName [current vendor name]
     * @return            []
     */

    @future (callout=true)
    public static void updateCaseCommentWithExistinngCases(String vendorName, String aravoCaseId){
        List<Case__x> lstCases = [SELECT CaseNumber__c 
                                  FROM Case__x 
                                  WHERE Vendor_c__c =:vendorName
                                  AND RecordTypeId__c =:SF_SEC_RECORDTYPEID
                                  AND Internal_Support_Category_c__c =:INTERNAL_SUPPORT_CATEGORY
                                  AND Status__c = 'Closed'];
        
        String currentCases;

        for(Case__x caseX : lstCases){
            if(String.isBlank(currentCases)){
                currentCases = Label.ESA_Intake_Bot + '\n' + 'Existing Closed Cases of this Vendor';
            }
            currentCases += '  ' + caseX.CaseNumber__c; 
        }

        if(String.isNotBlank(currentCases)){
            CaseComment__x cComment = new CaseComment__x();
            cComment.ParentId__c = aravoCaseId;
            cComment.CommentBody__c = currentCases;
            Database.SaveResult sr = Database.insertImmediate(cComment);
        }
        
    }
    
    @TestVisible
    private Id createComment(Id aravoCaseId) {
        sfCaseComment = new CaseComment();
        string comment = CASE_COMMENT;  
        comment = comment.replace('{vendor}',getSupplierName().trim());  
        system.debug(CASE_COUNT_QUERY);
        comment = comment.replace('{count}',String.valueOf(sfutil.ESASupportForceUtil.getCaseCount(CASE_COUNT_QUERY) + 1));          
        sfCaseComment.CommentBody = comment;
        sfCaseComment.IsPublished = true;                   
        sfCaseComment.ParentId = aravoCaseId;
        Id newCommentId;
        if (!test.isRunningTest()) newCommentId = sfutil.ESASupportForceUtil.insertCaseComment(JSON.serialize(sfCaseComment));   
        return newCommentId;      
    }
    
    @TestVisible
    private void sendEmail(Id aravoCaseId) {
        if (getBusinessContactEmail() == null) return;
        String emailBody = CASE_EMAIL;
        emailBody = emailBody.replace('{vendor}',getSupplierName());  
        //if (businessContact.Id != null) emailBody = emailBody.replace('{requester}',getBusinessContactName()); 
        if (businessContactX != null) emailBody = emailBody.replace('{requester}',getBusinessContactName()); 
            else emailBody = emailBody.replace('{requester}',getBusinessContactEmail());
        if (aravoCaseId != null) {
            emailBody = emailBody.replace('{supportforcelink}',SUPPORTFORCE_LINK);  
            emailBody = emailBody.replace('{caseid}',aravoCaseId);  
            emailBody = emailBody.replace('{email2caseref}',ESAcaseRefUtil.caseRefId(SF_ORGID, aravoCaseId));

            if(Label.Esa_Intake_Url != 'Test') {
                String caseNumber = [SELECT CaseNumber__c 
                                  FROM Case__x 
                                WHERE ExternalID =: aravoCaseId].CaseNumber__c;
                emailBody = emailBody.replace('{IntakeMessage}',Label.Esa_Intake_App_Message);
                emailBody = emailBody.replace('{IntakeUrl}',Label.Esa_Intake_Url + caseNumber);    
            }else{
                emailBody = emailBody.replace('{IntakeMessage} \n {IntakeUrl}\n\n','');
            }   
        }

        String emailSubject = CASE_SUBJECT;
        emailSubject = emailSubject.replace('{vendor}',getSupplierName().trim());  
            
        Messaging.SingleEmailMessage mail= new Messaging.SingleEmailMessage();
        mail.setToAddresses(new String[] {getBusinessContactEmail()});
        mail.setCcAddresses(new String[] {E2C_ADDRESS});
        mail.setOrgWideEmailAddressId(owea.Id);
        mail.setReplyTo(E2C_ADDRESS);
        mail.setSubject(emailSubject);
        mail.setPlainTextBody(emailBody);
        Messaging.sendEmail(new Messaging.Email[] {mail}); 
        createSPTCaseComment (aravoCaseId, mail.getPlainTextBody());
        
    }
    
    /**
     * Create Supportforce case comment.
     */
    @future (callout=true) 
    private static void createSPTCaseComment (id caseId, string tBody)    {
        
        CaseComment__x cc = new CaseComment__x();
        cc.CommentBody__c = tBody;
        cc.ParentId__c = caseId;
        cc.IsPublished__c = true;
        DataBase.saveResult sr = Database.insertImmediate(cc);
   
    }
    
    
    private String getSupplierName() {
        try {
            if (emailBody.contains(SUPPLIER_COUNTRY_TAG)) {
                return capitalizeAll(emailBody.substringBetween(SUPPLIER_TAG, SUPPLIER_COUNTRY_TAG).trim()); 
            }
            else {
                return capitalizeAll(emailBody.substringBetween(SUPPLIER_TAG, BUSINESS_PROCESS_TAG).trim());            
            }         
        } catch (exception e) {
            return null;
        }
    }
    
    private String getBusinessContactName() {
        //if (businessContact.Id == null) return '*** Not able to find matching email in supportforce ***';
        if (businessContactX == null) return '*** Not able to find matching email in supportforce ***';
        try {
            //return (businessContact.FirstName == null? '' : businessContact.FirstName) + (businessContact.LastName == null? '' : (' ' + businessContact.LastName));
            return (businessContactX.FirstName__c == null? '' : businessContactX.FirstName__c) + (businessContactX.LastName__c == null? '' : (' ' + businessContactX.LastName__c));
        } catch (exception e) {
            return null;
        }
    }

    private String getBusinessContactEmail() {
        try {
            String email;
            email = emailBody.substringBetween(ON_BEHALF_OF_EMAIL, TAKE_ACTION_BY_TAG).trim().toUpperCase().replace('TABLEAU.COM', 'salesforce.com').toLowerCase();

            if(String.isBlank(email)){
                email = emailBody.substringBetween(BUSINESSCONTACT_EMAIL_TAG, ON_BEHALF_OF_NAME).trim().toUpperCase().replace('TABLEAU.COM', 'salesforce.com').toLowerCase();    
            }
            if (email.contains(' ')) email = email.substringBefore(' ').trim();
            return email;
        } catch (exception e) {
            return null;
        }
    }

    private String getDescription() {
        try {
            String description = emailBody.substringBetween(DESCRIPTION_BEGIN_TAG, TAKE_ACTION_BY_TAG).trim();
            String bp = description.substringBetween(BUSINESS_PROCESS_TAG, BUSINESS_PROCESS_ID_TAG);
            description = description.replace(bp, ' ' + bp.trim() + '\n');
            if (description.countMatches(getBusinessContactEmail()) > 1) {
                description = description.replaceFirst((getBusinessContactEmail()),'').trim();
            }
            description = description.replace(BUSINESSCONTACT_EMAIL_TAG, 'BUSINESS CONTACT' + ' ' + getBusinessContactName());
            return description;
        } catch (exception e) {
            return null;
        }
    }

    private Date getNeedByDate() {
        try {
            String cleanDate = emailBody.substringAfter(TAKE_ACTION_BY_TAG).normalizeSpace().left(10).trim().substringBeforeLast('/');
            cleanDate += '/' + emailBody.substringAfter(TAKE_ACTION_BY_TAG).normalizeSpace().left(10).trim().substringafterLast('/').left(4);
            return Date.parse(cleanDate);        
        } catch (exception e) {
            return null;
        }
    }
    
    private String getCaseOwnerId() {
        try {
            return [select Queue_ID__c from ESA_Case_Queue__c where Queue_Picklist_Name__c = 'ASA'].Queue_ID__c;
        } catch (exception e) {
            return null;
        }
    }    

    private String capitalizeAll(String inputString) {
        String workString = inputString.toLowerCase();         
        for (String s : workString.split(' ', -2)) {
            workString = workString.replace(s, s.capitalize());    
        }
        return workString;
    }
        
    private String addCustomFields(string jsonString, map<String, String> customFields) {
        // generate JSON for custom fields
        JSONGenerator customFieldsJSON = JSON.createGenerator(true);
        customFieldsJSON.WriteStartObject();
        for (String customField : customFields.keySet()) {
           customFieldsJSON.writeStringField(customField, customFields.get(customField));   
        }
        customFieldsJSON.WriteEndObject();
        
        // append to incoming json string and return
        string r = (jsonString.removeEnd('}') + 
                   ',' + customFieldsJSON.getAsString().replace('{','').replace('}','') 
                   + '}');
        return r;                
    }  

    private void sendCasecreationFailureEmail(Id aravoCaseId){
        Messaging.SingleEmailMessage mail= new Messaging.SingleEmailMessage();
        OrgWideEmailAddress owea = [SELECT Address,Id,DisplayName FROM OrgWideEmailAddress
                                    WHERE DisplayName = :ESA_AppConstants.ORG_WIDE_EMAIL_ADDRESS];
        mail.setSubject('Unable to create case - Duplicate case found');
        mail.setOrgWideEmailAddressId(owea.Id);
        mail.setToAddresses(new String[] {getBusinessContactEmail()});
        mail.setReplyTo(E2C_ADDRESS);
        String emailBody = DUPLICATE_CASE_EMAIL;
        //if (businessContact.Id != null) emailBody = emailBody.replace('{requester}',getBusinessContactName());
        if (businessContactX != null) emailBody = emailBody.replace('{requester}',getBusinessContactName());  
        else emailBody = emailBody.replace('{requester}',getBusinessContactEmail());

        emailBody = emailBody.replace('{vendor}',getSupplierName());
        emailBody = emailBody.replace('{days}',Label.Esa_Aaravo_Case_Age);
        emailBody = emailBody.replace('{supportforcelink}',SUPPORTFORCE_CASE_LINK);  
        emailBody = emailBody.replace('{caseid}',aravoCaseId); 
        mail.setHtmlBody(emailBody);
        Messaging.sendEmail(new Messaging.Email[] {mail});
    }
        
}
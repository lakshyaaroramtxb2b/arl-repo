public with sharing class InternalDRAssessmentsController {
    public boolean globalError {get;set;}
    
    public Datetime start_time {get;set;}
    public Boolean startTimeError { get; set; }
    public Datetime end_time {get;set;}
    public Boolean endTimeError { get; set; }
    public String source_ips {get;set;}
    public Boolean sourceIPsError { get; set; }
    public String dest_ips {get;set;}
    public Boolean destIPsError { get; set; }
    public Integer server_load {get;set;}
    public Boolean serverLoadError {get;set;}
    public String tools_used {get;set;}
    public Boolean toolsUsedError {get;set;}
    public String tests_performed {get;set;}
    public Boolean testsPerformedError {get;set;}
    public Boolean usernamesError {get;set;}
    public String usernames {get;set;}
    public Boolean orgIdsError {get;set;}
    public String org_ids {get;set;}
    
    public String ae_name {get;set;}
    public Boolean aeNameError {get;set;}
    
    public String tester_name {get;set;}
    public Boolean testerNameError {get;set;}
    public String tester_email {get;set;}
    public Boolean testerEmailError {get;set;}
    public String tester_work_phone {get;set;}
    public Boolean testerWorkPhoneError {get;set;}
    public String tester_cell_phone {get;set;}
    public Boolean testerCellPhoneError {get;set;}    
    public String pm_name {get;set;}
    public Boolean pmNameError {get;set;}
    public String pm_email {get;set;}
    public Boolean pmEmailError {get;set;}
    public String pm_work_phone {get;set;}
    public Boolean pmWorkPhoneError {get;set;}
    public String pm_cell_phone {get;set;}
    public Boolean pmCellPhoneError {get;set;}
    
    public Boolean showSubmit {get;private set;}

    public String company {get;set;}
    public Boolean companyError {get;set;}

    public String notification {get;private set;}
    public String notificationLevel {get;private set;}
    public String now {get;set;}

    public InternalDRAssessmentsController() {
        globalError = False;
        showSubmit = True;
        now = getFirstAvailableDate();
        Cookie trackingCookie = ApexPages.currentPage().getCookies().get('siteId');
        if ((trackingCookie == null) || (trackingCookie.getValue() == '') || (trackingCookie.getValue() == null)) {
            Cookie c = new Cookie('siteId', String.valueOf(Crypto.getRandomLong()), null, -1, false);
            ApexPages.currentPage().setCookies(new Cookie[] { c });
        }        
    }
    
    private String getFirstAvailableDate() {
        Datetime today = Datetime.now();
        // 5 business days
        Integer addDays = 5;
        String day = today.format('E');
        if ((day == 'Tue') || (day == 'Wed') || (day == 'Thu') || (day == 'Fri')) {
            // add the weekend
            addDays += 2;
        } else if (day == 'Sat') {
            // just sunday
            addDays += 1;
        }
        
        return today.addDays(addDays).formatGmt('yyyy-MM-dd\'T\'HH:mm:00\'Z\'');
    }
    public String mapServerLoad(Integer val) {
        if (val == 1) {
            return 'Negligible';
        } else if (val == 2) {
            return 'Light';
        } else if (val == 3) {
            return 'Moderate';
        } else if (val == 4) {
            return 'Heavy';
        } else {
            serverLoadError = True;
            globalError = True;
            return null;
        }
    }
    public PageReference schedule() {
        Customer_Assessment__c assess = new Customer_Assessment__c();
        assess.Name = company;
        assess.Start_Time__c = start_time;
        assess.End_Time__c = end_time;
        assess.Source_IPs__c = source_ips;
        assess.Destination_IPs__c = dest_ips;
        assess.Server_Load__c = mapServerLoad(server_load);
        assess.Tools_Used__c = tools_used;
        assess.Tests_Performed__c = tests_performed;
        assess.Usernames__c = usernames;
        assess.Org_IDs__c = org_ids;
        assess.AE_Name__c = ae_name;
        assess.Tester_Name__c = tester_name;
        assess.Tester_Email__c = tester_email;
        assess.Tester_Work_Phone__c = tester_work_phone;
        assess.Tester_Cell_Phone__c = tester_cell_phone;
        assess.PM_Name__c = pm_name;
        assess.PM_Email__c = pm_email;
        assess.PM_Work_Phone__c = pm_work_phone;
        assess.PM_Cell_Phone__c = pm_cell_phone;
        if (!globalError) {
            insert assess;
            notification = 'Scan registered successfully';
            notificationLevel = 'success';
            showSubmit = False;
        }
        
        return null;
    }
    private String escape(String a) {
        return a.replace('&','&').replace('"','"').replace('<','<').replace('>','>');
    }
}
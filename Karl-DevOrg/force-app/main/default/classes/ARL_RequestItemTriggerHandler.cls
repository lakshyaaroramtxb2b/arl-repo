/**
* @author Vishnu Kumar
* @email vishnu.kumar@mtxb2b.com
* @description This is trigger handler class for request item.
*/
public with sharing class ARL_RequestItemTriggerHandler{	
    public static Set<Id> recordIds = new Set<Id>();
    public static Set<Id> recordIdsforCreq = new Set<Id>();

    /**
     * @author Vishnu Kumar
     * @email vishnu.kumar@mtxb2b.com
     * @description This method get triggered from request item trigger.
    */
    public static void run(){
        //After Update
        if( Trigger.isAfter && Trigger.isUpdate ){
            captureRequestItemVersion();
            creqUpdate();
            createGusRecordsOnWaveChange((List<Request_Item__c>)Trigger.new,(Map<Id, Request_Item__c>)Trigger.oldMap);
        }
        //After Insert
        else if( Trigger.isAfter && Trigger.isInsert ){
            captureRequestItemVersion();
        }
    }

    /**
     * @author Vishnu Kumar
     * @email vishnu.kumar@mtxb2b.com
     * @description This method create version item if Request Item is created or updated with certain fields.
    */
    private static void captureRequestItemVersion(){
        /**
         * @author Ben Harvie
         * @email ben.harvie@salesforce.com
         * @description Recursion logic based on set of Ids
         */
        for(Id recursionCheck : Trigger.newMap.keySet()){
            if(recordIds.contains(recursionCheck)){
                System.debug(LoggingLevel.DEBUG, 'Already processed this record >> ' + recursionCheck);
                return; // Kill the run - we've already processed this Id
            }else{
                System.debug(LoggingLevel.DEBUG, 'New record to process >> ' + recursionCheck);
                recordIds.add(recursionCheck); // add Id to set so we don't run it again
            }
        }

        //Variable Declarations
        List<Request_Item__c> ris = new List<Request_Item__c>();
        List<Request_Item_Version__c> rivs = new List<Request_Item_Version__c>();
        Map<Id, Request_Item__c> oldMap = (Map<Id, Request_Item__c>)Trigger.oldMap;
        Map<Id, Request_Item__c> newMap = (Map<Id, Request_Item__c>)Trigger.newMap;
        
        //Filling map of Request ID and their Cycle Request Items
        Map<Id, List<Cycle_Request_Item__c> > requestIDAndCycleRequestItems = new Map<Id, List<Cycle_Request_Item__c> >();
        for( Cycle_Request_Item__c cri : [SELECT id,Request__c,Audit_Cycle__r.Active__c FROM Cycle_Request_Item__c WHERE Request__c in: (List<Request_Item__c>)Trigger.New ]){
            List<Cycle_Request_Item__c> cris = new List<Cycle_Request_Item__c>();
            if( requestIDAndCycleRequestItems.containsKey(cri.Request__c) ){
                cris = requestIDAndCycleRequestItems.get(cri.Request__c);
            }
            cris.add(cri);
            
            requestIDAndCycleRequestItems.put(cri.Request__c , cris);
        }
        
        //Creating Request Item Version
        for(Request_Item__c rItem : (List<Request_Item__c>)Trigger.New ){
            Request_Item__c oldR = Trigger.isUpdate ? oldMap.get(rItem.Id) : null;
			
            //Check
            // 1. If is for insert then create version record
            // 2. If is for update then check certain fields and if value changed then create version record
            if( Trigger.isInsert ||
              ( Trigger.isUpdate && (rItem.Request_Name__c <> oldR.Request_Name__c ||
                rItem.Request_Description__c <> oldR.Request_Description__c || 
                rItem.Request_Tech_Details__c <> oldR.Request_Tech_Details__c) )
              ){
                Decimal version = Trigger.isUpdate ? (rItem.Version__c + 1) : 1;
                    
                rivs.add(new Request_Item_Version__c(
                    Name = rItem.Name + '-' + version,
                    Request_Item__c = rItem.Id,
                    Request_Name__c = rItem.Request_Name__c,
                    Request_Description__c = rItem.Request_Description__c,
                    Request_Tech_Details__c = rItem.Request_Tech_Details__c,
                    Version__c = version
                ));
                                  
                //Update request item with version
                Request_Item__c ri = new Request_Item__c();
                ri.Id = rItem.Id;
                ri.Version__c = version;
                ri.Update_Current_Audit_Cycle_s__c = '';
                ris.add(ri);
        	}
        }
        
        if( !rivs.isEmpty() && Schema.sObjectType.Request_Item_Version__c.isCreateable() 
          	&& Request_Item_Version__c.Request_Item__c.getDescribe().isCreateable() 
          	&& Request_Item_Version__c.Request_Name__c.getDescribe().isCreateable() 
          	&& Request_Item_Version__c.Request_Description__c.getDescribe().isCreateable() 
          	&& Request_Item_Version__c.Request_Tech_Details__c.getDescribe().isCreateable() 
          	&& Request_Item_Version__c.Version__c.getDescribe().isCreateable() ){
            insert rivs; 
        }
                        
        if( !ris.isEmpty() && Schema.sObjectType.Request_Item__c.isUpdateable() 
           && Request_Item__c.Version__c.getDescribe().isUpdateable()
           && Request_Item__c.Update_Current_Audit_Cycle_s__c.getDescribe().isUpdateable()){ 
            update ris; 
        }
        
        //Update Cycle_Request_Item__c with current version if user selected Update_Current_Audit_Cycle_s__c as Yes
        List<Cycle_Request_Item__c> crItemsToUpdate = new List<Cycle_Request_Item__c>();
        for( Request_Item_Version__c riv : rivs ){
            //Update the Cycle Request Item if user set 'Yes' to update current audit cycle
            if( newMap.get(riv.Request_Item__c).Update_Current_Audit_Cycle_s__c == 'Yes' && 
               requestIDAndCycleRequestItems.containsKey(riv.Request_Item__c) ){
                   for( Cycle_Request_Item__c cri : requestIDAndCycleRequestItems.get(riv.Request_Item__c) ){
                       //Update Request Item Version if Audit Cycle is True
                       if( cri.Audit_Cycle__r.Active__c ){
                           cri.Request_Item_Version__c = riv.Id;
                           cri.KARL_Indexed_Request_Name__c  = riv.Request_Name__c  ;
                           cri.Request_Tech_Details__c  = riv.Request_Tech_Details__c ;
                           cri.Request_Description__c  = riv.Request_Description__c ;
                           crItemsToUpdate.add(cri);
                       }
                }
            }
        }
        
        if( !crItemsToUpdate.isEmpty() && Schema.sObjectType.Cycle_Request_Item__c.isUpdateable() 
          	&& Cycle_Request_Item__c.Request_Item_Version__c.getDescribe().isUpdateable() 
           	&& Cycle_Request_Item__c.Request_Tech_Details__c.getDescribe().isUpdateable()
           	&& Cycle_Request_Item__c.KARL_Indexed_Request_Name__c.getDescribe().isUpdateable()
           	&& Cycle_Request_Item__c.Request_Description__c.getDescribe().isUpdateable()){
            update crItemsToUpdate; 
        }
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description This method updates the CREQ for "simple" data without requiring a new version
    */
    private static void creqUpdate(){
        /**
         * @author Ben Harvie
         * @email ben.harvie@salesforce.com
         * @description Recursion logic based on set of Ids
         */
        for(Id recursionCheck : Trigger.newMap.keySet()){
            if(recordIdsforCreq.contains(recursionCheck)){
                System.debug(LoggingLevel.DEBUG, 'Already processed this record >> ' + recursionCheck);
                return; // Kill the run - we've already processed this Id
            }else{
                System.debug(LoggingLevel.DEBUG, 'New record to process >> ' + recursionCheck);
                recordIdsforCreq.add(recursionCheck); // add Id to set so we don't run it again
            }
        }
       
        //Filling map of Request ID and their Cycle Request Items
        Map<Id, List<Cycle_Request_Item__c> > requestIDAndCycleRequestItems = new Map<Id, List<Cycle_Request_Item__c> >();
        for( Cycle_Request_Item__c cycleRequestItem : [SELECT id,Request__c,Audit_Cycle__r.Active__c 
                                            FROM Cycle_Request_Item__c 
                                            WHERE Request__c in: (List<Request_Item__c>)Trigger.New ]){
            List<Cycle_Request_Item__c> cycleRequests = new List<Cycle_Request_Item__c>();
            if( requestIDAndCycleRequestItems.containsKey(cycleRequestItem.Request__c) ){
                cycleRequests = requestIDAndCycleRequestItems.get(cycleRequestItem.Request__c);
            }
            cycleRequests.add(cycleRequestItem);
            
            requestIDAndCycleRequestItems.put(cycleRequestItem.Request__c , cycleRequests);
        }

        // Create map of available picklist items (so we can get the label)
        list<Schema.PicklistEntry> scopePicklist = Request_Item__c.Primary_Scope__c.getDescribe().getPicklistValues();
        map<String,String> scopeLabels = new map<String,String>();
        for (Schema.PicklistEntry pe : scopePicklist){
            scopeLabels.put(pe.getValue(), pe.getLabel());
        }

        
        //Update Cycle_Request_Item__c with simple data when Update_Current_Audit_Cycle_s__c is Yes
        List<Cycle_Request_Item__c> creqsToUpdate = new List<Cycle_Request_Item__c>();
        for( Request_Item__c requestItem : (List<Request_Item__c>)Trigger.New ){
            //Update the Cycle Request Item if user set 'Yes' to update current audit cycle
            if( requestItem.Update_Current_Audit_Cycle_s__c == 'Yes' && 
                requestIDAndCycleRequestItems.containsKey(requestItem.Id) ){
                
                for( Cycle_Request_Item__c creqItem : requestIDAndCycleRequestItems.get(requestItem.Id) ){
                    //Update Request Item Version if Audit Cycle is True
                    if( creqItem.Audit_Cycle__r.Active__c ){
                        creqItem.Request_Assignee__c            = requestItem.KARL_GRC_Assignee__c ;
                        creqItem.Request_Link__c                = requestItem.Self_Collect_Link__c;
                        creqItem.Request_Environments__c        = requestItem.Request_Environments__c;
                        creqItem.Request_Area__c                = requestItem.Areas_of_Compliance__c;
                        creqItem.Primary_Scope_Name__c          = scopeLabels.get(requestItem.Primary_Scope__c);
                        creqsToUpdate.add(creqItem);
                    }
                }
            }
        }

        // Update the Cycle Requests
        if( !creqsToUpdate.isEmpty() && Schema.sObjectType.Cycle_Request_Item__c.isUpdateable() 
            && Cycle_Request_Item__c.Request_Assignee__c.getDescribe().isUpdateable()
            && Cycle_Request_Item__c.Request_Link__c.getDescribe().isUpdateable()
            && Cycle_Request_Item__c.Request_Environments__c.getDescribe().isUpdateable()
           	&& Cycle_Request_Item__c.Request_Area__c.getDescribe().isUpdateable()){ 
            update creqsToUpdate; 
        }
    }


     /**
     * @author Lakshya Arora
     * @email lakshya.arora@mtxb2b.com
     * @description This method create gus work record based on wave change
    */ 
    private static void createGusRecordsOnWaveChange(List<Request_Item__c> newList, Map<Id,Request_Item__c> oldMap){
        Set<Id> cycleIds = new Set<Id>();
        Set<Id> requestItemIdSet = new Set<Id>();
        Set<Id> processCycleIdSet = new Set<Id>();
        Set<Id> dependentCycleIdSet  = new Set<Id>();
        for( Request_Item__c riObj : (List<Request_Item__c >)Trigger.New ){
           if(oldMap != null && riObj.Priority__c != oldMap.get(riObj.Id).Priority__c ){ //removed items which were duplicate
            requestItemIdSet.add(riObj.Id);
           }
        }

        if(!requestItemIdSet.isEmpty()){
            System.debug(LoggingLevel.DEBUG, 'createGusRecordsOnWaveChange: requestItemIdSet on priority change = '+requestItemIdSet);
            for(Cycle_Request_Item__c cri: [SELECT Id,Audit_Cycle__c,Audit_Cycle_Coverage__r.Scope__c,Audit_Cycle_Coverage__r.Area__c,Audit_Cycle_Coverage__r.KARL_Audit_Team__c,
                                            Request_Priority__c,Is_Gus_Record_Created__c,Date_for_Work_Record_Creation__c,
                                            Audit_Cycle_Coverage__r.Auto_Assignment_Configuration__r.Priority_0_Assignment_Date__c,
                                            Audit_Cycle_Coverage__r.Auto_Assignment_Configuration__r.Priority_1_Assignment_Date__c,
                                            Audit_Cycle_Coverage__r.Auto_Assignment_Configuration__r.Priority_2_Assignment_Date__c,
                                            Audit_Cycle_Coverage__r.Auto_Assignment_Configuration__r.Priority_3_Assignment_Date__c
                                            FROM Cycle_Request_Item__c 
                                            WHERE Audit_Cycle_Coverage__r.Auto_Assignment_Configuration__c != null
                                            AND Request__c IN:requestItemIdSet
                                            AND Audit_Cycle__r.Active__c = true 
                                            WITH SECURITY_ENFORCED]){
                                                if(!(!cri.Is_Gus_Record_Created__c && cri.Date_for_Work_Record_Creation__c == null)){
                                                    // If the assignment date is before today, add to the processing list
                                                    if((cri.Request_Priority__c == 'Wave 0' &&  cri.Audit_Cycle_Coverage__r.Auto_Assignment_Configuration__r.Priority_0_Assignment_Date__c  <= System.today()) 
                                                    || (cri.Request_Priority__c == 'Wave 1' &&  cri.Audit_Cycle_Coverage__r.Auto_Assignment_Configuration__r.Priority_1_Assignment_Date__c  <= System.today()) 
                                                    || (cri.Request_Priority__c == 'Wave 2' &&  cri.Audit_Cycle_Coverage__r.Auto_Assignment_Configuration__r.Priority_2_Assignment_Date__c  <= System.today()) 
                                                    || (cri.Request_Priority__c == 'Wave 3' &&  cri.Audit_Cycle_Coverage__r.Auto_Assignment_Configuration__r.Priority_3_Assignment_Date__c  <= System.today()) 
                                                    ){
                                                        processCycleIdSet.add(cri.Id);
                                                        cycleIds.add(cri.Audit_Cycle__c);
                                                    }
                                                    else{
                                                        // Dependent cycle IDs will have the Date of Creation added
                                                        dependentCycleIdSet.add(cri.Id);
                                                    }
                                                }    
                                                
            }
            System.debug(LoggingLevel.DEBUG, 'createGusRecordsOnWaveChange: processCycleIdSet On Wave change = '+processCycleIdSet);
            System.debug(LoggingLevel.DEBUG, 'createGusRecordsOnWaveChange: dependentCycleIdSet On Wave change = '+dependentCycleIdSet);
            KARL_GusUtility.processGUSRecordCreation(processCycleIdSet, cycleIds, dependentCycleIdSet);
        }
    }
}
/* 

    Developer Name: Rashmi Krishnan (Circa summer 2014)
    
    Description:

    Spec Reference: https://developers.google.com/accounts/docs/OAuth2WebServer#offline
    Spec Reference: https://developers.google.com/google-apps/calendar/v3/reference/
    All the Google OAuth API calls are as per spec reference.
    
    Note that there are various methods that have return type as Httpresponse. These methods are written in 
    such a way to test HTTP callouts.  For testing HTTP callouts refer: 
    https://developer.salesforce.com/blogs/developer-relations/2013/03/testing-apex-callouts-using-httpcalloutmock.html
    
    Modified by: Jorge L Caceres - jcaceres@salesforce.com | October 2014
        - Refactor code in general to make it into a managed package
        - Use a Protected Custom Setting to store secrets rather than a custom object.
        
    Modified by: Jorge L Caceres - jcaceres@salesforce.com | January 6, 2015
        - Allow instatiating with calendar ID.
        
    Modified by: Jorge L Caceres - jcaceres@salesforce.com | March 11, 2015
        - Change logic to refresh token to defer the update of the custom setting to allow other 
          callouts to happen (callouts are not allow after any DML)
    
*/

public with sharing class TD_gmailInterface {
    
    private final String GOOGLE_CALENDAR_ID = 'salesforce.com_64mro2rakmpphlhd85676t7q98%40group.calendar.google.com';
    private String calendarID;
            
    public TD_gmailInterface() {
        this.calendarID = GOOGLE_CALENDAR_ID;
    }
                
    public TD_gmailInterface(String calendarID) {
        this.calendarID = calendarID;
    }
    

    private static TD_gmailSetup__c theGoogleAuth;
    private static Boolean upateTheGoogleAuth = false;
    
    /*
        Get client ID
    */
    private static String client_id {get {
            return oauth_creds.Client_ID__c;
        }
    }
    
    /*
        Calendar to save the booked timeslot
    */
    private String BookingCalendarID {
        get {
            return this.calendarID;
        }
    }
    
    /*
        Get client secret
    */
    private static String client_secret {
        get {
            return oauth_creds.Client_Secret__c;
        }
    }
    
    public String eventIDString {
        get; set;
    }
    
    /*
        API to check for token validity
    */
    private static String tokenInfo {
        get {
            return 'https://www.googleapis.com/oauth2/v1/tokeninfo';
        } 
    }
    
    /*
        If access type is offline then request for refresh token as well else just access token.
    */
    private String access_type {
        get {
            return (isRefreshTokenSet())? 'online':'offline';
        }
    }
    private String calendarAPI {
        get {
            return 'https://www.googleapis.com/calendar/v3/calendars/';
        }
    }
    
    private String signup {
        get {
            return '/apex/TD_gmailResponse';
        }   
    }
    
    /*
        API to get authorization code
    */
    private String oAuthUrl {get {
            return 'https://accounts.google.com/o/oauth2/auth';
        }
    }
        
    /*
        API to declare scope
    */
    private String oAuthCalendarAPI { get {
            return 'https://www.googleapis.com/auth/calendar';
        }
    }
    
    /*
        API to get OAuth Token
    */  
    private static String oAuthTokenAPI { get {
            return 'https://accounts.google.com/o/oauth2/token';
        }
    }
    
    /*
        Get google oauth credentials which has access token, refresh token and the token type
    */
    private static TD_gmailSetup__c oauth_creds {
        get {
            if (theGoogleAuth == null) {
                List<TD_gmailSetup__c> googleAuth = [SELECT Access_token__c, Token_Type__c, Refresh_Token__c, Client_Id__c, Client_Secret__c, Code__c
                                                       FROM TD_gmailSetup__c WHERE name = 'TrustDeliverables' LIMIT 1];
                theGoogleAuth = googleAuth.isEmpty()?null:googleAuth[0];
            }
            return theGoogleAuth;
        }
    }
    
    /*
        Get and set access token
    */
    private static String access_token {get {
        TD_gmailSetup__c googleAuth = oauth_creds;
        return (googleAuth != null)?googleAuth.Access_token__c:null;
        } 
        set {
            TD_gmailSetup__c googleAuth = oauth_creds;
            if(googleAuth != null) {
                googleAuth.Access_token__c = value;
                System.debug('access token value: ' + value);
                upateTheGoogleAuth = true;
                //update googleAuth; defer update to avoid error           
                }
        }
    }
    
    /*
        Get and set token type
    */
    private static String token_type {get {
            TD_gmailSetup__c googleAuth = oauth_creds;
            return (googleAuth != null)?googleAuth.Token_Type__c:null;
        } 
        set {
            TD_gmailSetup__c googleAuth = oauth_creds;
            if(googleAuth != null) {
                googleAuth.Token_Type__c = value;
                upateTheGoogleAuth = true;                
                //update googleAuth; defer update to avoid error           
                }
        }
    }
    
    /*
        Get and set refresh token
    */
    private static String refresh_token {get {
            TD_gmailSetup__c googleAuth = oauth_creds;
            return (googleAuth != null)?googleAuth.Refresh_Token__c:null;
        } 
        set {
            TD_gmailSetup__c googleAuth = oauth_creds;
            if(googleAuth != null) {
            
                googleAuth.Refresh_Token__c = value;
                System.debug('refresh token value: ' + value);
                upateTheGoogleAuth = true;                
                //update googleAuth; defer update to avoid error           
            }
        }
    }
    
    /*
        Get and set authorization code
    */
    private static String authorization_code {get {
            TD_gmailSetup__c googleAuth = oauth_creds;
            return (googleAuth != null)?googleAuth.Code__c:null;
        } 
        set {
            TD_gmailSetup__c googleAuth = oauth_creds;
            if(googleAuth != null) {
            
                googleAuth.Code__c = value;
                System.debug('authorization code value: ' + value);
                upateTheGoogleAuth = true;                
                //update googleAuth; defer update to avoid error           
            }
        }
    }
    /*
        Create request token url.
        Please note that the redirect uri should match with the Google API console app redirect uri.
        Any mismatch in this value or client id or client secret will result in an error
    */
    private String createRequestURL(String host) {
        String rediruri = 'https://'+ host + signup;
        System.debug('host:'+rediruri);
        String authuri = oAuthUrl + '?client_id=' + client_id + 
                   '&redirect_uri=' + rediruri + '&scope=' + EncodingUtil.urlEncode(oAuthCalendarAPI, 'UTF-8') +  
                   '&approval_prompt=force&response_type=code&access_type='+ access_type;
        return authuri; 
    }
    
    /*
        Request for authorization code which can be exchanged for token
    */
    private Pagereference requestToken(String host) {
        String authuri = createRequestURL(host);
        system.debug('authuri: ' + authuri);
        Pagereference authPage = new PageReference(authuri);    
        return authPage;
    }
    
    private void getAccessToken(String code) {
        try {
            Httpresponse resp = null;
            /*
                When the test is running do not call request access token because test fails otherwise.
                There are separate test cases to test these HTTP requests.  There is currently no way 
                to test mixture of HTTP callout and normal function methods
            */
            if(!Test.isRunningTest())
                resp = requestAccessToken(code);
                system.debug('>>> requestAccessToken response: ' + resp);
                    
            // Parse response
            // Get access token, token_type
            // For offline access, response will contain refresh token as well.  Store this refresh token 
            // and when the access token expires, use this refresh token to get new access token
            // Refresh token is valid until the application is revoked
            if(resp != null && resp.getStatusCode() == 200) {
                String responseBody = resp.getBody();
                System.debug('#####' + responseBody);
                Map<String, String> json_parsed = (Map<String, String>)System.Json.deserialize(resp.getBody(), Map<String, String>.class);
                System.debug('Parsed response : ' + json_parsed);
                access_token = json_parsed.get('access_token');
                token_type = json_parsed.get('token_type');
                if(json_parsed.get('refresh_token') != null) {
                    refresh_token = json_parsed.get('refresh_token');
                }
            } else {
                throw new gOauthException('Something Went Wrong!!');
            }
        } catch(gOauthException e) {
            
        }
    }

    private class gOauthException extends Exception {}
    
    /*
        Request for access token
    */
    private Httpresponse requestAccessToken(String code) {
        try {
            System.debug('code auth : ' + code);
            Http h1 = new Http();
            Httprequest req1 = new Httprequest();
            req1.setMethod('POST');
            req1.setEndpoint(oAuthTokenAPI);
            String reqBody = ('code='+ EncodingUtil.urlEncode(code, 'UTF-8') + '&client_id=' + client_id + '&client_secret=' + client_secret + '&grant_type=authorization_code&redirect_uri=' + EncodingUtil.urlEncode('https://' + ApexPages.currentPage().getHeaders().get('Host') + signup, 'UTF-8'));
            system.debug('reqBody: ' + reqBody);
            req1.setBody(reqBody);
            Httpresponse resp = h1.send(req1);
            system.debug('response: ' + resp);
            return resp;
        } catch (gOauthException e) {
            system.debug(e);
            return null;
        }
    }

    
    private String testJSON() {
        return '{"items":[{"kind": "calendar#event","etag": "\\"-kakMffdIzB99fTAlD9HooLp8eo/MjgxMjI3Mjc4MTUxMjAwMA\\"","id": "82994akpm87pe8ie3kbgoaqoe4", "status": "confirmed", "htmlLink": "https://www.google.com/calendar/event?eid=ODI5OTRha3BtODdwZThpZTNrYmdvYXFvZTQgMWYydDJoZWNpZjQ1aDMxczE0cmRpbTlobzhAZw","created": "2014-07-23T17:26:30.000Z", "updated": "2014-07-23T17:26:30.756Z", "summary": "Enterprise Security Team Office Hours", "description": "Case Number: 77777\\ntest next day event.. 23rd July events blocked", "location": "SF-1 Cal-29th-Kamea 5","creator":{"email": "testu5678@gmail.com", "displayName": "Test User" }, "organizer": {  "email": "1f2t2hecif45h31s14rdim9ho8@group.calendar.google.com", "displayName": "Enterprise Security Team", "self": true }, "start": {"dateTime": "2014-07-24T08:30:00-07:00" }, "end": {  "dateTime": "2014-07-24T09:00:00-07:00" }, "iCalUID": "82994akpm87pe8ie3kbgoaqoe4@google.com", "sequence": 0, "attendees": [  {   "email": "rashmi.krishnan@salesforce.com", "responseStatus": "needsAction" } ], "reminders": {  "useDefault": true }}]}';
    }

    
    /*
        If event ID is being passed then edit google calendar event else simply create a new event and save event ID to database
    */
    public void insertOreditCalendarEvent(String name, String[] email, String description, Date dT, String eventID) {
        insertOreditCalendarEvent(name, email, description, dT, eventID, null);     
    }
    
    public void insertOreditCalendarEvent(String name, String[] email, String description, Date dT, String eventID, String location) {
        
        PageReference init = initOAuth(true); // initialize oauth token but defer updates to custom setting to allow other callouts     
        
        Httpresponse resp1 = null;

        /*
            When the test is running do not call request access token because test fails otherwise.
            There are separate test cases to test these HTTP requests.  There is currently no way 
            to test mixture of HTTP callout and normal function methods
        */
        
        if(!Test.isRunningTest())
            resp1 = upsertCalendarEvent(name, email, description, dT, eventID, location);

        if(resp1 == null) {
            throw new gOauthException('Something went wrong!');
        }

        System.debug('Response ' + resp1.getBody());

        if(resp1.getStatusCode() != 200) {
            throw new gOauthException('Non 200 HTTP Response: ' + resp1.getStatusCode() + '\n Response: ' + resp1.getBody());
        }

        if(eventID == null) {
            eventIDString = getID(resp1.getBody());
        } else {
            eventIDString = eventID;
        }
        
        // update credentials if needed but only if not deferUpdate        
        if (upateTheGoogleAuth) {
            update theGoogleAuth;
        }
        
                    
    }
    
    /*
        Update calendar event or insert new one
    */
    private Httpresponse upsertCalendarEvent(String name, String[] email, String description, Date dT, String eventID, String location) {
        system.debug('upsertCalendarEvent parms: name->' + name + ' email->' + email + ' description -> ' + ' date-> ' + dt + ' eventId-> ' + eventID);
 
        try {
            Http h2 = new Http();
            Httprequest req2 = new Httprequest();
            if(eventID == null) {
                req2.setMethod('POST');
                req2.setEndpoint(calendarAPI + BookingCalendarID + '/events' + '?sendNotifications=false');
            } else {
                req2.setMethod('PUT');
                req2.setEndpoint(calendarAPI + BookingCalendarID + '/events/' + eventID + '?sendNotifications=false');
            }
            system.debug('EndPoint: ' + req2.getendpoint());
            /*
                Set authorization header for access token
            */
            req2.setHeader('Authorization', token_type + ' ' + access_token);
            req2.setHeader('Content-Type', 'application/json');
            /*
                Create json for request body
            */
            String postBody = createJSONTime(name, dT, dT, description, email, location);
            System.debug('jsonString:'+postBody);
            req2.setBody(postBody);
            Httpresponse resp1 = h2.send(req2);
            return resp1;
        } catch(gOauthException e) {
            return null;
        }
    }
    
    /*
        Delete calendar event
    */
    public void deleteEvent(String event_id) {

        PageReference init = initOAuth(true); // initialize oauth token but defer updates to custom setting to allow other callouts      
        
        try {
            Httpresponse resp1 = null;
            /*
                When the test is running do not call request access token because test fails otherwise.
                There are separate test cases to test these HTTP requests.  There is currently no way 
                to test mixture of HTTP callout and normal function methods
            */

            if(!Test.isRunningTest())
                resp1 = deleteEventRequest(event_id);
            if(resp1 == null)
                throw new gOauthException('Something Went Wrong!');
            System.debug('Failed response ' + resp1.getBody());
            Set<Integer> validResponseCodes = new Set<Integer>{200, 204};
            if(!validResponseCodes.contains(resp1.getStatusCode())) {
                throw new gOauthException('Non 200 HTTP Response: ' + resp1.getStatusCode() + '\n Response: ' + resp1.getBody());
            }
        } catch(gOauthException e) {
            System.debug('Request not completed successfully!! ' + e);
        }
        
        // update credentials if needed but only if not deferUpdate        
        if (upateTheGoogleAuth) {
            update theGoogleAuth;
        }

    }
    
    /*
        Delete calendar event
    */
    private Httpresponse deleteEventRequest(String event_id) {
        try {
            Http h2 = new Http();
            Httprequest req2 = new Httprequest();
            req2.setMethod('DELETE');
            req2.setEndpoint(calendarAPI + BookingCalendarID + '/events/' + event_id + '?sendNotifications=true');
            req2.setHeader('Authorization', token_type + ' ' + access_token);
            Httpresponse resp1 = h2.send(req2);
            return resp1;
        } catch (gOauthException e) {
            return null;
        }
    }
    
    /*
        Check if current access token is valid. If it is not then ask for new token
    */
    private static Boolean checkTokenValidity() {
        try {
            if(access_token == null) 
                return false;

            Httpresponse resp1 = null;
            /*
                When the test is running do not call request access token because test fails otherwise.
                There are separate test cases to test these HTTP requests.  There is currently no way 
                to test mixture of HTTP callout and normal function methods
            */

            if(!Test.isRunningTest())
                resp1 = checkTokenValidityRequest();
            else
                return true;
            if(resp1 == null)
                throw new gOauthException('Something Went Wrong!!');

            System.debug('token: ' + resp1.getBody() + 'response: ' + resp1.getStatusCode());
            if(resp1.getStatusCode() == 400) {
                return false;
            }
            return true;    
        } catch (gOauthException e) {
            System.debug('Request not completed successfully!! ' + e);
            return false;
        }
    }
    
    /*
        Check if current access token is valid. If it is not then ask for new token
    */
    private static Httpresponse checkTokenValidityRequest() {
        try {
            String tokenURL = tokenInfo;
            Http h2 = new Http();
            Httprequest req2 = new Httprequest();
            req2.setMethod('GET');
            req2.setEndpoint(tokenURL);
            req2.setBody('access_token=' + access_token);
            Httpresponse resp1 = h2.send(req2);
            return resp1;
        } catch (gOauthException e) {
            return null;                
        }
    }

    /*
        Refresh access token when access token expires
        Future callout used for 
    */
    // @future(callout=true)
    private static void refreshToken() {
        try {
            if(!checkTokenValidity()) {
                System.debug('In refresh token!!!!');
                Httpresponse resp = null;
                /*
                    When the test is running do not call request access token because test fails otherwise.
                    There are separate test cases to test these HTTP requests.  There is currently no way 
                    to test mixture of HTTP callout and normal function methods
                */

                if(!Test.isRunningTest())
                    resp = sendRefreshTokenRequest();
                if(resp != null) {
                    if(resp.getStatusCode() == 400) {
                        System.debug('bad token: ' + resp.getBody() + 'bad response: ' + resp.getStatusCode());
                    } else if(resp.getStatusCode() == 200){
                        Map<String, String> json_parsed = (Map<String, String>)System.Json.deserialize(resp.getBody(), Map<String, String>.class);
                        System.debug('Parsed response : ' + json_parsed);
                        access_token = json_parsed.get('access_token');
                        token_type = json_parsed.get('token_type');
                    }
                } else {
                    throw new gOauthException('Something Went Wrong!!');
                }
            }
        } catch (gOauthException e) {
            System.debug('Request not completed successfully!! ' + e);
        }
    }
    
    /*
        Send refresh token request
    */
    private static Httpresponse sendRefreshTokenRequest() {
        try {
            Http h2 = new Http();
            Httprequest req2 = new Httprequest();
            req2.setMethod('POST');
            req2.setHeader('Content-Type', 'application/x-www-form-urlencoded');
            req2.setEndpoint(oAuthTokenAPI);
            req2.setBody('refresh_token=' + refresh_token + '&client_id=' + client_id + '&client_secret=' + client_secret + '&grant_type=refresh_token');
            Httpresponse resp = h2.send(req2);
            return resp;
        } catch (gOauthException e) {
            return null;
        }
    }
    
    /*
        Check if refresh token is being set
    */
    public Boolean isRefreshTokenSet() {
        return refresh_token == null ? false:true;
    }


    public Pagereference initOAuth() {
        return initOAuth(false);        
    }

    public Pagereference initOAuth(Boolean deferUpdate) {
        Pagereference authpage;

        /*
        Currently refresh token is already set.
        This if loop checks if access_token and refresh_token both are valid.  If both don't exist,
        go through the normal google oauth flow in which first request for authorization code and then 
        exchange authorization code for access token.
        To remove the refresh token dependency, revoke access to app and then set it up again.
        */

        if(!checkTokenValidity() && !isRefreshTokenSet()) {
            System.debug('access token not valid and refresh token not valid');
            
            /*
                Request token url
            */
            
           
            if(authorization_code == null) {
                //Authenticate to google
                // at the moment this util cannot handle getting a new access token
                return requestToken('' + ApexPages.currentPage().getHeaders().get('Host'));     
            }
          
            
            /*
                Exchange authorization code for access token
            */
            
           
            if(authorization_code != null) {
                getAccessToken(authorization_code);
                //Update google oath object with access token
                
                authpage = new Pagereference(signup);
                //return authpage;
            }
            
        } else if(!checkTokenValidity() && isRefreshTokenSet()) {
            /*
                Access token is not valid but refresk token is.
                In this case, ask for access token using refresh token.
                Refresh token whenever access token expires.
            */
                System.debug('access token not valid');
                refreshToken();
                authpage = null; //new Pagereference(myReservations);
                //return authpage;

            } else if(checkTokenValidity()) {
                /*
                Access token is valid
            */
            System.debug('access token valid');
        }
        
        // update credentials if needed but only if not deferUpdate
        if (!deferUpdate && upateTheGoogleAuth) {
            update theGoogleAuth;
        }
       
        return authPage;
    
    }
    
    /* util methods */
    
    /*
        Parse the json response to get event ID for particular calendar event
    */
    private String getID(String responseBody) {
        JSONParser parser = JSON.createParser(responseBody);
        String htmlLink;
        while (parser.nextToken() != null) {
            if ((parser.getCurrentToken() == JSONToken.FIELD_NAME)){
                String fieldName = parser.getText();
                parser.nextToken();
                if(fieldName == 'id') {
                    htmlLink = parser.getText();
                    break;
                }
            }
        }
        String IDString;
        if(htmlLink != null || htmlLink != '') {
            IDString = htmlLink; 
        } else {
            IDString = '';
        }
        return IDString;
    }    

    
    /*
        Create json string in a format usable by Google Calendar API
    */
    public String createJSONTime(String name, Date sT, Date eT, String description, String[] email) {
        return createJSONTime(name, sT, eT, description, email, null);    
    }

    public String createJSONTime(String name, Date sT, Date eT, String description, String[] email, String location) {
        try {
            // Reference: https://developer.salesforce.com/page/Getting_Started_with_Apex_JSON
            JSONGenerator generator = JSON.createGenerator(true);
            generator.writeStartObject();
            
            generator.writeFieldName('end');
            generator.writeStartObject();        
            generator.writeObjectField('date', eT);         
            generator.writeEndObject();

            generator.writeFieldName('start');
            generator.writeStartObject();        
            generator.writeObjectField('date', sT);         
            generator.writeEndObject();
            
            generator.writeFieldName('end.date');
            generator.writeStartObject();        
            generator.writeObjectField('date', eT);         
            generator.writeEndObject();

            generator.writeFieldName('start.date');
            generator.writeStartObject();        
            generator.writeObjectField('date', sT); 
            generator.writeEndObject();

            generator.writeFieldName('attendees');
            generator.writeStartArray();            
            for (string e : email) {
                generator.writeStartObject();                   
                generator.writeObjectField('email', e); 
                generator.writeEndObject();
            }
            generator.writeEndArray();
            
            generator.writeStringField('guestsCanInviteOthers', 'false');
            String summary = 'Trust Deliverable Due: ' + name;
            generator.writeStringField('summary', summary);
            generator.writeStringField('description', description);
            generator.writeStringField('status', 'confirmed');
            generator.writeStringField('transparency', 'transparent');
            generator.writeStringField('location', 'Trust Deliverables');
            
            generator.writeFieldName('reminders');
            generator.writeStartObject();        
            generator.writeObjectField('useDefault', false);         
            generator.writeEndObject();
            
            generator.writeEndObject();
            
            String jsonString = generator.getAsString();
            system.debug('>>> body: ' + jsonString);
            
            return jsonString;
        } catch(Exception e) {
            
            return null;
        }
    }     

}
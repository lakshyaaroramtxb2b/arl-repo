@isTest
public class DLM_Is_StakeholderTriggerHelperTest {
    
    @isTest
    public static void preventDuplicateStakeholdersTest(){
        List<Contact> conList = new List<Contact>();
        List<Account> accountList = DLM_TestDataFactory.createAccounts(1, true);
        List<Contact> contactList = DLM_TestDataFactory.createContacts(1, accountList[0].id, false);
        
        for(contact con : contactList){
            //con.Business_Unit__c = 'test busines unit';
            con.RecordTypeId = DLM_TestDataFactory.DLM_CONTACTRECORDTYPEID;
            con.Org62_User_ID__c = 'QWERTYUIOPASDFGYUJ';
            conList.add(con);
        }
        Insert conList;
        
        List<IS_Standard__c> standardList = new List<IS_Standard__c>();
        
        IS_Standard__c standardRec = new IS_Standard__c();
        standardRec.Name = 'Test Std';
        standardRec.SFSS_Content_Owner_1__c = conList[0].id;
        standardRec.Standard_Hyperlink__c = 'www.google.com';
        standardList.add(standardRec);
        
        insert standardList;
        
        
        List<IS_Stakeholder__c> stakeholderList = DLM_TestDataFactory.createIsStakeholder(1, conList, standardList[0].id, true);
        try{
            List<IS_Stakeholder__c> stakeholderList1 = DLM_TestDataFactory.createIsStakeholder(1, conList, standardList[0].id, true);
        }catch(exception e){
            System.debug('Error: '+ e.getMessage());
        }
        
    }
}
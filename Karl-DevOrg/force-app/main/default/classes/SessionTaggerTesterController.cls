public class SessionTaggerTesterController {

   public String instancesChosenPublish {get; set;}
   public String userId{get; set;}
   public String orgId{get; set;}
   public String ruleIds{get; set;}
   public String sessionHash{get; set;}   
   public String ruleMatcher{get; set;}      
   
   public List<Map<String, Object>> getTagSessionHistory() {
        List<SfdcAuditableActionResult> freezeHistory = [SELECT Body,CreatedDate,EndPoint,Id,IsCompleted,IsDeleted,LastModifiedById,LastModifiedDate,Message,RequestId,Status,SystemModstamp, Request.Action, Request.Body, Request.CreatedBy.Name FROM SfdcAuditableActionResult where request.action like 'malwareSessionHammer' order by createddate desc];   
        List<Map<String, Object>> result = new List<Map<String, Object>>();
        for (SfdcAuditableActionResult thisResult: freezeHistory) {
            Map<String, Object> thisResultMap = new Map<String,Object>();
            thisResultMap.put('CreatedBy', thisResult.Request.CreatedBy.Name);            
            thisResultMap.put('CreatedDate', thisResult.createdDate);
            thisResultMap.put('Endpoint', thisResult.EndPoint);
            thisResultMap.put('RequestAction', thisResult.Request.Action);
            thisResultMap.put('Status', thisResult.Status);            
            thisResultMap.put('Message', thisResult.Message);   
            thisResultMap.put('RequestBody', thisResult.Request.Body.toString());                                 
            thisResultMap.put('ResultBody', thisResult.Body.toString());                     
            result.add(thisResultMap);
        }
        return result;
   }
   
   public List<SelectOption> getAllInstances() {
        List<String> allInstances = malwareDefense.MalwareRuleUtil.getAllInstances();
        List<SelectOption> options = new List<SelectOption>();
        if(allInstances == null) { return options; }
        for(String inst : allInstances) {
            options.add(new SelectOption(inst, inst));
        }        
        
        return options;
    }
    
    public String getEndpoint() {
        return 'https://security.my.salesforce.com/services/admin/v40.0/malwareSessionHammer/';
    }    
    
    public void tagSession() {
        try {
        
            if (String.isBlank(userId) || String.isBlank(orgId) || String.isBlank(InstancesChosenPublish)) {
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR ,'Missing Argument, make sure you provide every required fields.' );
                ApexPages.addMessage(myMsg);
                return;
            }
            
            if (String.isBlank(ruleIds) && String.isBlank(ruleMatcher) ) {
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR ,'Missing rule argument, make sure you provide either a rule id or a rulematcher expression.' );
                ApexPages.addMessage(myMsg);
                return;
            }
            
            Map<String, Object> bodyMap = new Map<String, Object>();
            bodyMap.put('orgId', orgId);            
            bodyMap.put('userId', userId);
            
            if (!String.isBlank(ruleMatcher)) {
                bodyMap.put('ruleMatcher', ruleMatcher);
            }
            
            if (!String.isBlank(sessionHash)) {
                bodyMap.put('sessionHash', sessionHash);
            }
            
            if (ruleIds != null && ruleIds.length() > 0) {
                List<String> ruleIdList = ruleIds.split(',');
                if (ruleIdList.size() > 0) {
                    bodyMap.put('ruleIds', ruleIdList);
                }
            }
            else {
                bodyMap.put('ruleIds', new List<String>());                
            }
            
            String body = JSON.serializePretty(bodyMap);
            String sid = UserInfo.getSessionId();
            Http h = new Http();
            HttpRequest req = new HttpRequest();
            req.setHeader('Authorization', 'Bearer ' + sid);
            req.setHeader('X-SFDC-INSTANCE', instancesChosenPublish);
            req.setHeader('Content-Type', 'application/json');
            req.setEndpoint(getEndpoint());
            req.setMethod('POST');
            req.setBody(body);
            HttpResponse res = h.send(req);
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Session Tagging Submitted with Request ID : ' + res.getBody());
            ApexPages.addMessage(myMsg);
        }
        catch (Exception ex) {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,ex.getMessage());
            ApexPages.addMessage(myMsg);
        }    
    }    

}
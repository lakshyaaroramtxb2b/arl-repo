/**
 * @author Ben Harvie
 * @email ben.harvie@salesforce.com
 * @description Test class for KARL_AreaRequirementTriggerHelper
*/
@isTest
public with sharing class KARL_AreaRequirementTriggerHelperTest {
    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Test data setup.
    */
    @testSetup
    private static void testSetup(){
        Area_Requirement__c areaRequirement_1 = ARL_TestDataFactory.createAreaRequirement('1.1.1', 'PCI DSS');
        insert areaRequirement_1;

        // More than 1, so we test recursion doesn't block additional items
        Area_Requirement__c areaRequirement_2 = ARL_TestDataFactory.createAreaRequirement('2.1.1', 'PCI DSS');
        insert areaRequirement_2;
    }
    
    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description In this test, the test is updating the Full Reference and asserting that it was updated correctly
    */
	@istest
    public static void unitTest(){ 
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User u = new User(Alias='standt', Email='standarduser@testorg.com', EmailEncodingKey='UTF-8', LastName='ARL Test User',
                         LanguageLocaleKey='en_US', LocaleSidKey='en_US', ProfileId = p.Id, TimeZoneSidKey='America/Los_Angeles', UserName='stan123@test.com');
        INSERT u;
        PermissionSet pSet = [SELECT Id, Label FROM PermissionSet WHERE Label LIKE '%SCEA Ops Admin%'];
        PermissionSetAssignment psa = new PermissionSetAssignment(PermissionSetId = pSet.Id, AssigneeId = u.Id);
        INSERT psa;
        
        System.runAs(u) {
            Area_Requirement__c areaItem = [SELECT Id, Requirement_Reference__c, toLabel(Area_of_Compliance__c), KARL_Full_Requirement_Reference__c 
                                                    FROM Area_Requirement__c LIMIT 1];
  		
            // Indexed value should be a concatenation of KARL_Audit_Scope__r.Name and Report Type
            String expectedName = areaItem.Area_of_Compliance__c + ' - ' + areaItem.Requirement_Reference__c;
            
            // Expecting a match on the concatenated name and the saved trigger name
            System.assertEquals( areaItem.KARL_Full_Requirement_Reference__c, expectedName, 'Names are not matching properly' );   
        }
    }
}
@isTest
private class THValidityRestTest {

    @IsTest
    static void thValidtyTest() {

       Trailhead_Validity__c tValidity = new Trailhead_Validity__c();
       tValidity.Name = 'TestValidty';
       tValidity.Recent_Validity_Time__c = DateTime.Now();
       insert tValidity;


       String since = '2008-06-02T08:47:01Z';
       RestContext.request = new RestRequest();
       RestContext.request.params.put(THValidityRest.SINCE_PARAM, since);

       String orgValidtyVal = THValidityRest.getOrgValidty();

       system.assertEquals('false',orgValidtyVal,'Org is Invalid');



       since = '2028-06-02T08:47:01Z';
       RestContext.request.params.put(THValidityRest.SINCE_PARAM, since);
       orgValidtyVal = THValidityRest.getOrgValidty();

       system.assertEquals('true',orgValidtyVal,'Org is Valid');

      since = null;
      RestContext.request.params.put(THValidityRest.SINCE_PARAM, since);
      orgValidtyVal = THValidityRest.getOrgValidty();
      system.assertEquals('Invalid Org Creation Date',orgValidtyVal,'Org Creation Creation Date is Null');       

      since = 'ABC';
      RestContext.request.params.put(THValidityRest.SINCE_PARAM, since);
      orgValidtyVal = THValidityRest.getOrgValidty();
      system.assertEquals(true,orgValidtyVal.contains('Invalid format'),'Invalid Date Format'); 

    }
}
public with sharing class CDI_ExportCsv {
    public string header{get;set;}
    public List<wrapper> lstwrapper {get; set;}
    public class wrapper{
        public string name {get; set;}
        public string description{get; set;} 
        public string dataAssetType{get;set;}
        public string parentDataAsset{get; set;}
        public string sourcesystemname{get; set;}
        public string sourcedataassetcode{get; set;}
        public string linktobalancingarguments{get; set;}
        public string linktoexcpnforbasisforprocessing{get; set;}
        public boolean isEncrypted{get; set;}
        public decimal dataRetentionPeriod{get; set;}
        public boolean canconsumeraccessdata{get; set;}
        public string dataClassification{get; set;}
        public string dataCategory{get; set;}
        public string dataStore{get; set;}
        public string dataIdentificationCategory{get; set;}
        public string customerDataCategory{get; set;}
        public string cryptoAlgorithm{get; set;}
        public boolean isPersonalDataLimitedToPurpose{get; set;}
        public boolean isDeleted{get; set;}      
        public string basisForProcessing{get; set;}
        public boolean hasDPIABeenPerformed{get; set;}
        public string timeUnit{get; set;}
        public string retentionProcess{get; set;}
        public string gusTeam{get; set;}
        public string status{get; set;}
        public string comments{get; set;}
}
    public boolean isExcel {get;set;}
    public boolean isCsv {get;set;}   
    public CDI_ExportCsv(){
        lstwrapper = new List<wrapper>();     
        header = 'Data Asset Name,Data Asset Description,Data Asset Type,Parent Data Asset,Data Retention Period,Source System Name,Source Data Asset Code,Link To Balancing Argument Document,Link To Excepn For Basis For Processing,IsEncrypted,Is Personal Data Limited To Purpose,Has DPIA Been Performed,Can Consumers Access Data,Crypto Algorithm,Data Store,Data Classification,Data Category,Data Identification Category,Basis For Processing,Time Unit,Retention Process,Customer Data Category,GUS Team,IsDeleted,Status,Comments'; 
    }
    public void exportToCsv(){
       String currentdatastoreId=ApexPages.CurrentPage().getparameters().get('datastoreId');
         List<CDI_Data_Asset__c> dataassets = [SELECT Name,Data_Asset_Description__c,Data_Asset_Type__c,Parent_Data_Asset__r.Name,Data_Retention_Period__c,Source_System_Name__c,Source_Data_Asset_Code__c,Link_To_Balancing_Argument_Document__c,Link_To_Excepn_For_Basis_For_Processing__c,IsEncrypted__c,Is_Personal_Data_Limited_To_Purpose__c,Has_DPIA_Been_Performed__c,Can_Consumers_Access_Data__c,Data_Classification__r.Name,Data_Category__r.Name,Data_Identification_Category__r.Name,Crypto_Algorithm__r.Name,Data_Store__r.Name,Basis_For_Processing__r.Name,Time_Unit__r.Name,Retention_Process__r.Name,Customer_Data_Category__r.Name,GUS_Team__r.Name,IsDeleted__c,Status__c,Comments__c FROM CDI_Data_Asset__c WHERE Data_Store__c =: currentdatastoreId WITH SECURITY_ENFORCED];
              for(CDI_Data_Asset__c asset :dataassets){
                  wrapper w = new wrapper();
                  w.name = asset.Name ;
                  String description=asset.Data_Asset_Description__c==null?'':asset.Data_Asset_Description__c.contains('"')?asset.Data_Asset_Description__c.replaceAll('"',''):asset.Data_Asset_Description__c;
                  w.description = description.contains(',')?'"'+description+'"':description;
                  w.dataAssetType=asset.Data_Asset_Type__c;
                  w.parentDataAsset = asset.Parent_Data_Asset__r.Name;
                  String sourcesystemname=asset.Source_System_Name__c==null?'':asset.Source_System_Name__c.contains('"')?asset.Source_System_Name__c.replaceAll('"',''):asset.Source_System_Name__c;
                  w.sourcesystemname=sourcesystemname.contains(',')?'"'+sourcesystemname+'"':sourcesystemname;
                  String sourcedataassetcode=asset.Source_Data_Asset_Code__c==null?'':asset.Source_Data_Asset_Code__c.contains('"')?asset.Source_Data_Asset_Code__c.replaceAll('"',''):asset.Source_Data_Asset_Code__c;
                  w.sourcedataassetcode=sourcedataassetcode.contains(',')?'"'+sourcedataassetcode+'"':sourcedataassetcode;
                  String linktobalancingarguments=asset.Link_To_Balancing_Argument_Document__c==null?'':asset.Link_To_Balancing_Argument_Document__c.contains('"')?asset.Link_To_Balancing_Argument_Document__c.replaceAll('"',''):asset.Link_To_Balancing_Argument_Document__c;
                  w.linktobalancingarguments=linktobalancingarguments.contains(',')?'"'+linktobalancingarguments+'"':linktobalancingarguments;
                  String linktoexcpnforbasisforprocessing=asset.Link_To_Excepn_For_Basis_For_Processing__c==null?'':asset.Link_To_Excepn_For_Basis_For_Processing__c.contains('"')?asset.Link_To_Excepn_For_Basis_For_Processing__c.replaceAll('"',''):asset.Link_To_Excepn_For_Basis_For_Processing__c;
                  w.linktoexcpnforbasisforprocessing=linktoexcpnforbasisforprocessing.contains(',')?'"'+linktoexcpnforbasisforprocessing+'"':linktoexcpnforbasisforprocessing;
                  w.isEncrypted=asset.IsEncrypted__c;
                  w.dataRetentionPeriod=asset.Data_Retention_Period__c;
                  w.canconsumeraccessdata=asset.Can_Consumers_Access_Data__c;
                  w.dataClassification=asset.Data_Classification__r.Name;
                  w.dataCategory=asset.Data_Category__r.Name;
                  w.dataStore=asset.Data_Store__r.Name;
                  w.dataIdentificationCategory=asset.Data_Identification_Category__r.Name;
                  w.customerDataCategory=asset.Customer_Data_Category__r.Name;
                  w.cryptoAlgorithm=asset.Crypto_Algorithm__r.Name;
                  w.isPersonalDataLimitedToPurpose=asset.Is_Personal_Data_Limited_To_Purpose__c;
                  w.isDeleted=asset.IsDeleted__c;      
                  w.basisForProcessing=asset.Basis_For_Processing__r.Name;
                  w.hasDPIABeenPerformed=asset.Has_DPIA_Been_Performed__c;
                  w.timeUnit=asset.Time_Unit__r.Name;
                  w.retentionProcess=asset.Retention_Process__r.Name;
                  w.gusTeam=asset.GUS_Team__r.Name;
                  w.status=asset.Status__c;
                  String comments=asset.Comments__c==null?'':asset.Comments__c.contains('"')?asset.Comments__c.replaceAll('"',''):asset.Comments__c;
                  w.comments=comments.contains(',')?'"'+comments+'"':comments;
                  lstwrapper.add(w);               
              }   
         
              system.debug('lstwrapper :'+lstwrapper.size());
    }
 
   

}
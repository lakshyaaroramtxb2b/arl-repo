/**
 * @File Name          : IEM_CaseOwnerSharingAccess_BatchTest.cls
 * @Description        : 
 * @Author             : Swarnima Singh Mandhata
 * @Group              : 
 * @Last Modified By   : Swarnima Singh Mandhata
 * @Last Modified On   : 05/12/2020
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    05/12/2020   Swarnima     Initial Version
**/
@isTest
public class IEM_CaseOwnerSharingAccess_BatchTest {
     
    @testSetup
    public static void testSetup(){
        User admin = IEM_TestDataFactory.createUser(); 
        
        List<User> portalUser = new List<User>();        
        System.runAs(admin){
            //Create Accounts
            List<Account> accounts = new List<Account>();
            accounts= IEM_TestDataFactory.createAccounts(1,true);
            //Create Contacts
            List<Contact> contacts = new List<Contact>();
            contacts= IEM_TestDataFactory.createContacts(4,accounts[0].id,false);
            contacts[0].LastName = 'Test user M 1';
            contacts[0].Employee_ID__c = 'E-208283764';
            contacts[0].title = 'Director';
            contacts[1].LastName = 'Test user M 2';
            contacts[1].Employee_ID__c = 'E-208283765';
            contacts[1].title = 'Senior Director';
            contacts[2].LastName = 'Test user M 3';
            contacts[2].Employee_ID__c = 'E-208283766';
            contacts[2].title = 'VP';
            
            contacts[3].LastName = 'Issue Owner';
            contacts[3].Employee_ID__c = 'E-208283767';
            contacts[3].title = 'VP';
            contacts[3].Management_Chain_Level_03__c = 'Test user M 1 (E-208283764)';
            contacts[3].Management_Chain_Level_02__c = 'Test user M 2 (E-208283765)';
            contacts[3].Management_Chain_Level_01__c = 'Test user M 3 (E-208283766)';
            contacts[0].recordTypeid = IEM_Constants.IEM_CONTACTRECORDTYPEID;
            contacts[1].recordTypeid = IEM_Constants.IEM_CONTACTRECORDTYPEID;
            contacts[2].recordTypeid = IEM_Constants.IEM_CONTACTRECORDTYPEID;
            contacts[3].recordTypeid = IEM_Constants.IEM_CONTACTRECORDTYPEID;
            contacts[0].Org62_User_ID__c = '2F01I4D0000009R4yq';
            contacts[1].Org62_User_ID__c = '2F01I4D0000009R5yq';
            contacts[2].Org62_User_ID__c = '2F01I4D0000009R6yq';
            contacts[3].Org62_User_ID__c = '2F01I4D0000009R7yq';
            insert contacts;
            //Create poratl User
            portalUser = IEM_TestDataFactory.createPortalUserList(contacts,true);
            
           
            
        }
        
    }
    testMethod static void executeBatch(){
        //UserRole userRole = [SELECT Id FROM UserRole LIMIT 1];       
        List<user> portalUser = [SELECT id,contactID,profileId,profile.Name from User where Username LIKE '%testPortalIEMHVCPUser%'];
        System.debug('portalUser ---> '+ portalUser);
       //Create Accounts
        List<Account> accounts = new List<Account>();
        accounts= IEM_TestDataFactory.createAccounts(1,true);
        //Create Contacts
        List<Contact> contacts = new List<Contact>();
        contacts= IEM_TestDataFactory.createContacts(2,accounts[0].id,true);

        //Create Case         
        List<Case> caseList = IEM_TestDataFactory.createIntakeIssueCase(2,contacts,false);        
        caseList[0].Requestor__c = portalUser[0].contactID;
        caseList[0].Issue_Owner__c = portalUser[0].contactID;
        caseList[0].Action_Plan_Owner__c = portalUser[1].contactID;
        caseList[0].Severity_Assessor_User__c = portalUser[0].Id;
        caseList[0].Security_Assurance_SME_User__c = portalUser[1].id;
        caseList[0].SLA_Extension_Id__c = 'EX101';
        insert caseList;
        
         Case_Visibility__c casevisRec = new Case_Visibility__c();
            casevisRec.Case__c = caseList[0].id;
        casevisRec.Contact__c = contacts[0].id;
        casevisRec.Access_Level__c = 'Read';
        //casevisRec.Name = 'Test case Visibility';
        insert casevisRec;
      
        List<CaseShare> caseShareRec = [SELECT ID, UserOrGroupId,CaseId, CaseAccessLevel FROM CaseShare WHERE CaseId IN:caseList];
       
        system.debug('caseShareRec -> '+ caseShareRec);
        
        Test.startTest();
        IEM_CaseOwnerSharingAccess_Batch.getCaseShareRecords(caseList[0].id, 'Read', portalUser[0].Id);
        system.debug('test3');
        IEM_CaseOwnerSharingAccess_Batch.getTableBody(caseShareRec);
        IEM_CaseOwnerSharingAccess_Batch iemCaseBatch = new IEM_CaseOwnerSharingAccess_Batch();
        User u = [SELECT Id FROM User WHERE Id=: userInfo.getUserId() LIMIT 1];
        try{
            System.runAs(u){
                 Database.executeBatch(iemCaseBatch);
            }  
        }
        catch(exception e){
            SYstem.debug('Error' + e.getMessage());
        }
        Test.stopTest();
    }
}
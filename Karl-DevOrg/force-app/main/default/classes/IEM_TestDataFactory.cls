@isTest
public class IEM_TestDataFactory {
    
    public static List<Case> createCaseRecords(Integer numCase, Boolean doInsert) {
        List<Case> caseList = new List<Case>();
        for(integer i=0; i<numCase; i++ ){
            caseList.add(new Case(Subject ='Test Case'+i, Status='Submitted', 
                                  recordtypeId= IEM_Constants.IEM_CASERECORDTYPEID));
        }
        if(doInsert){
            insert caseList;
        }
        return caseList;
    }
    
    public static List<Case> createIntakeIssueCase(Integer numCase,List<Contact> contactList, Boolean doInsert) {
        List<Case> caseList = new List<Case>();        
        for(integer i=0; i<numCase; i++ ){
            Case caseRec = new Case(Subject ='Intake Issue Number '+i, Status=IEM_Constants.IEM_CASE_STATUS_INVESTIGATING, 
                                    recordtypeId= IEM_Constants.IEM_CASERECORDTYPEID); 
            caseRec.Description = 'Intake Issue Number '+i;
            caseRec.Existing_Work_Story__c = 'Yes';
            caseRec.Team_Performing_Audit__c = 'Security Compliance';
            caseRec.Security_Standard_Affected__c = 'SFSS-001 Acceptable Use';
            caseRec.Control_Activities_Affected__c = 'Test Control_Activities_Affected__c';
            caseRec.Audit_or_Readiness_Finding__c = 'External Audit';
            caseRec.External_Certification_Impact__c = 'No';
            caseRec.Reported_Issue_Owner__c = 'Test Reported_Issue_Owner__c';
            caseRec.Reported_Issue_Owner__c = 'Test Reported_Issue_Owner__c';
            caseRec.Reported_Team__c = 'Test Reported_Team__c';
            caseRec.Reported_Action_Plan_Owner__c = 'Test Reported_Action_Plan_Owner__c';
            caseRec.Highest_Data_Classification__c = 'Public';
            caseRec.Reported_Cloud__c = 'Test Reported_Cloud__c';
            caseRec.Residual_Severity__c = 'High';    
            caseRec.Validity__c = 'Valid';
            caseRec.Activity_Status__c = 'Active';
            
            caseRec.Issue_Owner__c = contactList[0].id;
            caseRec.Action_Plan_Owner__c = contactList[1].id;            
            
            caseList.add(caseRec);
        }
        if(doInsert){
            insert caseList;
        }
        return caseList;
    }
    
    public static List<Milestone__c> createMilestoneRecords(Integer num, Id caseId, Boolean doInsert) {
        List<Milestone__c> milestoneList = new List<Milestone__c>();
        for(integer i=0; i<num; i++ ){
            milestoneList.add(new Milestone__c(Definition_of_Done__c ='Test Milestone DOD'+i,
                                               Due_Date__c = Date.today().addDays(5),
                                               Description__c ='Test Milestone Description'+i,
                                               Name ='Test Milestone'+i,
                                               Status__c='Pending',
                                               Case__c = caseid));
        }
        if(doInsert){
            insert milestoneList;
        }
        return milestoneList;
    }
    public static List<Account> createAccounts(Integer num, Boolean doInsert) {
        List<Account> accounts = new List<Account>();
        for(integer i=0; i<num; i++ ){
            accounts.add(new Account(Name = IEM_Constants.IEM_ESA_ACCOUNT, Website = 'http://www.interactiveties.com/'));
        }
        if(doInsert){
            insert accounts; //insert the account list
        }
        return accounts;
    }
    public static List<Account> createAccountsESA(Integer num, Boolean doInsert) {
        List<Account> accounts = new List<Account>();
        for(integer i=0; i<num; i++ ){
            accounts.add(new Account(Name = IEM_Constants.IEM_ESA_ACCOUNT, Website = 'http://www.interactiveties.com/'));
        }
        if(doInsert){
            insert accounts; //insert the account list
        }
        return accounts;
    }
    public static List<Contact> createContacts(Integer num,Id accountId, Boolean doInsert) {
        List<Contact> contacts = new List<Contact>();
        for(integer i=0; i<num; i++ ){
            contacts.add(new Contact(AccountId = accountId, FirstName = 'Test Contact' + i, LastName = 'Record', email='test'+i+'@abcTesting.com')); //new Contact detail
        }
        if(doInsert){
            insert contacts; //insert the account list
        }
        return contacts;
    }
    public static List<GroupMember> createGroupRecords(Set<Id> userIdSet) {
        //Create Parent Group
        Group grp = new Group();
        grp.name = 'GRC IEM Case Analysts';
        grp.Type = 'Regular'; 
        Insert grp; 
        List<GroupMember> memberList = new List<GroupMember>();
        for(Id userId : userIDSet){
            //Create Group Member
            GroupMember grpMem1 = new GroupMember();
            grpMem1.UserOrGroupId = userId;
            grpMem1.GroupId = grp.id;
            memberList.add(grpMem1);            
        }
        if(memberList!=null && !memberList.isEmpty()){
            insert memberList;
        }
        system.debug('memberList' + memberList);
        
        return memberList;
    }
    
    public static List<GroupMember> createGroupMemberRecords(Set<Id> userIdSet, Id groupId) {
        List<GroupMember> memberList = new List<GroupMember>();
        for(Id userId : userIDSet){
            //Create Group Member
            GroupMember grpMem1 = new GroupMember();
            grpMem1.UserOrGroupId = userId;
            grpMem1.GroupId = groupId;
            memberList.add(grpMem1);            
        }
        if(memberList!=null && !memberList.isEmpty()){
            insert memberList;
        }
        system.debug('memberList' + memberList);
        
        return memberList;
    }
    public static User createUser(){
        UserRole userRole = [SELECT Id FROM UserRole LIMIT 1];
        
        Profile profile = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        //Create Admin User
        User user = new User(Alias = 'sta23', Email='IEMSystemAdmin@testorg'+System.now().millisecond()+'.com', 
                             EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                             LocaleSidKey='en_US', ProfileId = profile.Id, UserRoleId = userRole.Id,
                             TimeZoneSidKey='America/Los_Angeles', UserName='standarduser994'+System.now().millisecond()+'@testorg.com');
        
        //insert user;
        return user;
    }
    public static User createPortalUser(Contact c){       
        User user;
            
            user = new User();
            user.ProfileID = [Select Id From Profile Where Name='ESA HVCP'].id;
            user.EmailEncodingKey = 'ISO-8859-1';
            user.LanguageLocaleKey = 'en_US';
            user.TimeZoneSidKey = 'America/New_York';
            user.LocaleSidKey = 'en_US';
            user.FirstName = 'first';
            user.LastName = 'last';
            user.Username = 'testPortalHVCPUser@domain.com';   
            user.CommunityNickname = 'testUser123';
            user.Alias = 't1';
            user.Email = 'no@email.com';
            user.IsActive = true;
            user.ContactId = c.Id;
            
            //insert user;
        return user;
    }
    
    public static List<User> createPortalUserList(List<Contact> contacts, Boolean doInsert){       
        List<User> userList= new List<User>();
        for(integer i = 0; i<contacts.size(); i++)	{
        	User user;
            user = new User();
            user.ProfileID = [Select Id From Profile Where Name='ESA CCP'].id;
            user.EmailEncodingKey = 'ISO-8859-1';
            user.LanguageLocaleKey = 'en_US';
            user.TimeZoneSidKey = 'America/New_York';
            user.LocaleSidKey = 'en_US';
            user.FirstName = 'first'+i;
            user.LastName = 'last'+i;
            user.Username = 'testPortalIEMHVCPUser'+i+'@domain.com';   
            user.CommunityNickname = 'testUser123'+i;
            user.Alias = 't1'+i;
            user.Email = 'no'+i+'@email.com';
            user.IsActive = true;
            user.ContactId = contacts[i].Id;
            user.EmployeeNumber = contacts[i].Employee_ID__c;
            userList.add(user);
        }
        if(doInsert && !userList.isEmpty()){
        	insert userList;
        }
        return userList;
    } 
    
    public static List<User> createESACCPPortalUserList(List<Contact> contacts, Boolean doInsert){       
        List<User> userList= new List<User>();
        for(integer i = 0; i<contacts.size(); i++)	{
        	User user;
            user = new User();
            user.ProfileID = [Select Id From Profile Where Name='ESA CCP'].id;
            user.EmailEncodingKey = 'ISO-8859-1';
            user.LanguageLocaleKey = 'en_US';
            user.TimeZoneSidKey = 'America/New_York';
            user.LocaleSidKey = 'en_US';
            user.FirstName = 'first'+i;
            user.LastName = 'last'+i;
            user.Username = 'testPortalIEMHVCPUser'+i+'@domain.com';   
            user.CommunityNickname = 'testUser123'+i;
            user.Alias = 't1'+i;
            user.Email = 'no'+i+'@email.com';
            user.IsActive = true;
            user.ContactId = contacts[i].Id;
            user.EmployeeNumber = contacts[i].Employee_ID__c;
            userList.add(user);
        }
        if(doInsert && !userList.isEmpty()){
        	insert userList;
        }
        return userList;
    } 
    
     public static List<Case_Visibility__c> createCaseVisibilties( List<Case> cases,List<Contact> contacts, Boolean doInsert){       
         List<Case_Visibility__c> caseVisibilityList= new List<Case_Visibility__c>();
         Case_Visibility__c caseVisibility = new Case_Visibility__c();
         caseVisibility.Case__c = cases[0].Id;
         caseVisibility.Contact__c = contacts[0].Id;
         caseVisibility.Access_Level__c = 'Read';
         caseVisibilityList.add(caseVisibility);
         if(doInsert && !caseVisibilityList.isEmpty()){
             insert caseVisibilityList;
         }
         return caseVisibilityList;
    }
}
Global class BatchMassDeleteRecs Implements Database.batchable<sobject>{

     global Database.QueryLocator start(Database.BatchableContext BC){
      return Database.getQueryLocator('select id from guests_owd_entityperms__c');
     }

     global  void execute(Database.BatchableContext BC,List<SObject> scope){
         delete scope;
    }

    global void finish(Database.BatchableContext BC){
    }
}
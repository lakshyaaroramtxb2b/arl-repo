/**
 * @author: Suresh Uppala
 * @description: scheduler for SyncGusPositionRecords_Schedule
 */
public class SyncGusPositionRecords_Schedule implements Schedulable {
    public void execute(SchedulableContext sc) {
        Database.executeBatch((Database.Batchable<SObject>) 
            Type.forName('SyncGusPositionRecords').newInstance(), 50);
    }
}
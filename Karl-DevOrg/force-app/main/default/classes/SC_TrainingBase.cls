global class SC_TrainingBase {
    
    global interface CoursesCompletedState {
        List<CoursesCompletedResult> getResults();
    }
    
    global interface CoursesCompletedResult { }
    
    global interface ProviderConnector {
        Integer syncCompleted(CoursesCompletedState[] states);
        Iterable<CoursesCompletedState> getCoursesCompletedStateIterator();
    }
    
 
}
global class Metroid_Batch_Utilization implements Database.Batchable<List<string>> {
	
	global Iterable<List<string>> start(Database.BatchableContext BC) {
		List<List<string>> SLL = new List<List<string>>();
		SLL.add(new List<string>{'Vendor Security','Application Security'});
		SLL.add(new List<string>{'Infrastructure Security'});

		return SLL;
	}

   	global void execute(Database.BatchableContext info, List<List<String>> stringLs) {
		Metroid_Armory MA = new Metroid_Armory();
		 MA.Scan_OH_Utilization(stringLs[0],-1);
	}
	
	global void finish(Database.BatchableContext info) {
		
	}
	
}
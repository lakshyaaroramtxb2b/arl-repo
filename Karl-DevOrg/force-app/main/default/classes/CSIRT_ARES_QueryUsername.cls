@RestResource(urlMapping='/ARES/QueryUsername/*')
global with sharing class CSIRT_ARES_QueryUsername {
    global class QueryResponse {
        public String username;
        public String status;
        public Map<String,String> result;
        
        public QueryResponse(String username){
            this.username = username;
            System.debug('Username: '+ username);

        }
        
        public void Errored(){
            this.status = 'ERROR';
            System.debug('Cannot Query ' + username);
        }
        
        public void Success(Map<String,String> btResult){
            this.status = 'SUCCESS';
            this.result = btResult;
        }
    }

    
	@HttpPost 
	global static QueryResponse queryUsername(String username) {
        Map<String,String> btResult = new Map<String,String>();
		QueryResponse response = new QueryResponse(username);
        
        try{
        	btResult = SfdcOps.getCustomerInfo(username);
        }
        catch(Exception e){
        	response.Errored();
            return response;
        }
        
        response.Success(btResult);
        return response;
        
   }
}
/**
 * @author: ralph@callaway.cloud
 * @description: Constants for Contact object
 */
public class Contact_Constants {
    
    // RecordTypeId
    public static final Id RT_ID_SALESFORCE_EMPLOYEE = '01230000001GfV5AAK';
}
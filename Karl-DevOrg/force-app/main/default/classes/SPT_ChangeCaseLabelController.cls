public with sharing class SPT_ChangeCaseLabelController {
    public static SPT_ChangeCaseLabelService service = new SPT_ChangeCaseLabelService();

        @AuraEnabled 
        public static String getCaseOrigin(String caseId){
            return service.getCaseOrigin(caseId);
        }
    
}
global class Metroid_Scheduler implements Schedulable{

    /*
    To Schedule:
    
    Metroid_Scheduler MS = new Metroid_Scheduler();
    system.schedule('Batch Metroid Update', '0 01 0 * * ?', MS);
    
    */

    global void execute(SchedulableContext sc){

        Metroid_Batch_Availability mba = new Metroid_Batch_Availability();
        database.executeBatch(mba,1);

        if(date.today().day()==1){
            Metroid_Batch_Utilization mbu = new Metroid_Batch_Utilization();
            database.executeBatch(mbu,1);
        }

    }

}
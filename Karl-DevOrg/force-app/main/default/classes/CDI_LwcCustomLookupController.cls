public with sharing class CDI_LwcCustomLookupController {
    public CDI_LwcCustomLookupController() {
        
    }
    @AuraEnabled(cacheable=true)
    public static List<SObJectResult> getResults(String ObjectName, String fieldName, String secondaryFieldName,String value) {  
        List<SObJectResult> sObjectResultList = new List<SObJectResult>();
        system.debug(fieldName+'-------------'+ObjectName+'---------------'+secondaryFieldName);
        
        if(ObjectName.equalsIgnoreCase('CDI_Service__c') && Schema.sObjectType.CDI_Service__c.fields.Name.isAccessible() )
        {
            for(sObject so : Database.query('Select Id,'+fieldName+' FROM '+ObjectName+' WHERE '+fieldName+' LIKE \'%' + String.escapeSingleQuotes(value) + '%\' WITH SECURITY_ENFORCED')) {
                String fieldvalue = (String)so.get(fieldName);
                String Id=(String)so.get('Id');
                sObjectResultList.add(new SObjectResult(fieldvalue,fieldvalue,'', Id));
            }
            
        }   
        else if(ObjectName.equalsIgnoreCase('CDI_GUS_User__c') && Schema.sObjectType.CDI_GUS_User__c.fields.Name.isAccessible() && Schema.sObjectType.CDI_GUS_User__c.fields.Email_Id__c.isAccessible())
        {    
            for(sObject so : Database.query('Select Id, '+fieldName+', '+secondaryFieldName+' FROM '+ObjectName+' WHERE '+fieldName+' LIKE \'%' + String.escapeSingleQuotes(value) + '%\' WITH SECURITY_ENFORCED')) {
                String fieldvalue = (String)so.get(fieldName);
                String secondaryfieldvalue = so.get(secondaryFieldName)== null?'':(String)so.get(secondaryFieldName);
                String id=(String)so.get('Id');
                sObjectResultList.add(new SObjectResult(fieldvalue,fieldvalue+'.'+secondaryfieldvalue,secondaryfieldvalue, id));           
            }
            
        }
        else if(Objectname.equalsIgnoreCase('CDI_Datastore_Type__c'))
        {
            for(sObject so : Database.query('Select Id,'+fieldName+' FROM '+ObjectName+' WHERE '+fieldName+' LIKE \'%' + String.escapeSingleQuotes(value) + '%\' WITH SECURITY_ENFORCED')) {
                String fieldvalue = (String)so.get(fieldName);
                String Id=(String)so.get('Id');
                sObjectResultList.add(new SObjectResult(fieldvalue,fieldvalue,'', Id));
            }
        }
        return sObjectResultList;
        
    }
    
    public class SObJectResult {
        @AuraEnabled
        public String recName;
        @AuraEnabled
        public Id recId;
        @AuraEnabled
        public String secrecNames;
        @AuraEnabled
        public String nameandemail;
        
        public SObJectResult(String recNameTemp,String nameandemailTemp,String secrecNamesTemp, Id recIdTemp) {
            
            recName = recNameTemp;
            secrecNames =secrecNamesTemp;
            recId = recIdTemp;
            nameandemail=nameandemailTemp;
        }
    }
   
}
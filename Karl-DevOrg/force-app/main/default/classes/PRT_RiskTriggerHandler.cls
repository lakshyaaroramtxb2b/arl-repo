/***********************************************************
 * Handler Class for PRT_RiskTrigger
* Created by 	- Prashant Gupta
* Date 		- 09-09-2019
************************************************************/
public class PRT_RiskTriggerHandler {
	/***********************************************************
     * Created by 	- Prashant Gupta
     * Date 		- 09-09-2019
     ************************************************************/
    public static void beforeInsert(List<Risk__c> newList){
        PRT_Utility.updateProgramLookups(newList,null);
        //PRT_RiskTriggerHelper.updateCurrentState(newList,null);
        PRT_RiskTriggerHelper.updateSponsorFromProject(newList,null);
        PRT_RiskTriggerHelper.updateSlippageCommentOfProject(newList, null);
     }
    
	/***********************************************************
     * Created by 	- Prashant Gupta
     * Date 		- 09-09-2019
     ************************************************************/
    public static void beforeUpdate(List<Risk__c> newList, Map<id,Risk__c> oldMap){        
        PRT_Utility.updateProgramLookups(newList,oldMap);
        //PRT_RiskTriggerHelper.updateCurrentState(newList,oldMap);
        PRT_RiskTriggerHelper.checkRequiredFields(newList,oldMap);
        PRT_RiskTriggerHelper.updateSlippageCommentOfProject(newList, oldMap);
    }
}
global class IntakeBPOTrustSelfRegistration extends IntakeWhiteListedSelfRegistration implements Auth.ConfigurableSelfRegHandler {
    global override String getEntityCode() {
        return 'BPO';
    }
        
    global override String[] getPermissionSetNames() {
        return new String[]{'ESA_Security_Request_Customer'};
    }
}
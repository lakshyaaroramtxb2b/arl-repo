global class INTEG_EmployeeToContactMapperBatch implements Database.Batchable<sObject>,Database.AllowsCallouts{

    global final INTEG_EmployeeToContactMapper mapper; 
    // We only update contacts if their employee records that have been modified since this date
    global final DateTime modifiedSince;

    global INTEG_EmployeeToContactMapperBatch(DateTime oldest){
        modifiedSince = oldest;
        mapper = new INTEG_EmployeeToContactMapper();
    }

    global Database.QueryLocator start(Database.BatchableContext BC){
        return INTEG_EmployeeToContactMapper.getEmployeesQueryLocator(modifiedSince);
    }
 
    global void execute(Database.BatchableContext BC, List<INTEG_Employee__c> scope){
        System.debug('Running with '+String.valueOf(scope.size())+' employees in batch');
        Integer res = mapper.updateContactsForEmployees(scope);    
        System.debug('Updated '+String.valueOf(res)+' contact records in batch');
    }

    global void finish(Database.BatchableContext BC){

    }

}
public with sharing class ChangeCaseLabelController {
    public static ChangeCaseLabelService service = new ChangeCaseLabelService();

        @AuraEnabled 
        public static String getCaseOrigin(String caseId){
            return service.getCaseOrigin(caseId);
        }
    
}
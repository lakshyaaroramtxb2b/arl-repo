global class SPT_CaseDataWrapper implements Comparable {
    
    @auraEnabled Public DateTime timeTag = null;
    @auraEnabled Public String historyType = null;
    @auraEnabled Public String content = null;
    // actor - used to display the user name for each comment
    @auraEnabled Public String actor = null;
    @auraEnabled Public String oldValue = null;
    @auraEnabled Public String newValue = null;
    @auraEnabled Public String fieldName = null;
    @auraEnabled Public ID createdByID = null;
    // photoUrl - avatar
    @auraEnabled Public String photoUrl = null;
    // dateTag - used for headers
    @auraEnabled Public Date dateTag = null;
    @auraEnabled Public String initials = '';
    @auraEnabled Public Boolean needHeader = false;
    public String sortingOrder = null;
    
    // Parameterized constructor to store case History values
    public SPT_CaseDataWrapper(DateTime timeTag, String historyType,
                               String actor, String oldValue, String newValue,
                               String fieldName, Id createdByID, String sortingOrder) {
        this.timeTag = timeTag;
        this.historyType = historyType;
        this.actor = actor;
        if(oldValue==null || oldValue==''){                                      
            this.oldValue = 'Blank Value';
        }else{                                      
            this.oldValue = oldValue;
        }
        if(newValue==null || newValue==''){                                      
            this.newValue = 'Blank Value';
        }else{                                      
            this.newValue = newValue;
        }
        this.newValue = newValue;
        this.fieldName = fieldName;
        this.createdByID = createdByID;
        this.dateTag = timeTag.date();
        this.sortingOrder = sortingOrder;
    }
    
    // Parameterized constructor to store case comments values
    public SPT_CaseDataWrapper(DateTime timeTag, String historyType, String content, String actor,
                               Id createdByID, String sortingOrder){
        this.timeTag = timeTag;
        this.historyType = historyType;
        this.content = content;
        this.actor = actor;
        this.dateTag = timeTag.date();
        this.createdByID = createdByID;
        this.sortingOrder = sortingOrder;
    }
    
    // Compare project based on the score number.
    global Integer compareTo(Object compareTo) {
        // Cast argument to SPT_CaseDataWrapper
        SPT_CaseDataWrapper compareToProject = (SPT_CaseDataWrapper)compareTo;
        
        // The return value of 0 indicates that both elements are equal.
        Integer returnValue = 0;
        if(sortingOrder == 'ASC') {
            if (timeTag < compareToProject.timeTag) {
                // Set return value to a positive value.
                returnValue = 1;
            } else if (timeTag > compareToProject.timeTag) {
                // Set return value to a negative value.
                returnValue = -1;
            }
        }
        else {
            if (timeTag > compareToProject.timeTag) {
                // Set return value to a positive value.
                returnValue = 1;
            } else if (timeTag < compareToProject.timeTag) {
                // Set return value to a negative value.
                returnValue = -1;
            }
        }        
        return returnValue;       
    }
}
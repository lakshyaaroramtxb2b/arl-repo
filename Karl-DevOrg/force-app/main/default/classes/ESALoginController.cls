/*
    Developer Name: Jorge L Caceres - jcaceres@salesforce.com | October 2014
    Description: extract from original ESAWizzardController for handling user authentication
*/

public without sharing Class ESALoginController {

    private static final String GUEST_USER_TYPE = 'Guest';
    private static final String COMMUNITY_SITE_TYPE = 'ChatterNetwork';

    private static final String PARTNER_SITE_NAME = 'Source_Scanner_New';
    private static final String PARTNER_SITE_LOGIN_URL = 'https://login.salesforce.com/services/auth/sso/00D30000000JsbiEAC/SourceScanner' +
                                                         '?site=https://security.secure.force.com/sourcescanner';
    
    private static final String ENTITY_CODE_ALL = 'ALL';
    private static final String ENTITY_CODE_ENTSEC = 'EntSec';

    private Map<String, String> queryParms;

    public Pagereference authenticateUser () {
        // get query parms
        try {
            this.queryParms = ApexPages.currentPage().getParameters();
        } catch (exception e) {
            this.queryParms = new map<String, String>();
        }
        
        if (isRedirectToLtngCommunity()) {
            PageReference ltngCommunityPage = new PageReference(getLtngCommunityURL()); 
            // passthru query parms
            try {
                for (String parm : queryParms.keySet()) {
                    ltngCommunityPage.getParameters().put(parm, queryParms.get(parm));
                }
            } catch (exception e) {
                // ignore
            }
            return ltngCommunityPage;
        }

        // can't continue until user authenticates
        if (UserInfo.getUserType() == GUEST_USER_TYPE &&
            Site.getSiteType() == COMMUNITY_SITE_TYPE) {
                return setLoginPage(page.CommunitiesLogin, '/ESA');
        // special handling for partners login
        } else if (UserInfo.getUserType() == GUEST_USER_TYPE &&
            Site.getName() == PARTNER_SITE_NAME) {
            PageReference loginPage = setLoginPage(new PageReference(PARTNER_SITE_LOGIN_URL), '/ESA_Wizard');
            return loginPage;

        // redirects to community if coming via the old force.com site
        } else if (UserInfo.getUserType() == GUEST_USER_TYPE &&
            Site.getSiteType() != COMMUNITY_SITE_TYPE) {
            PageReference loginPage = new PageReference('https://securityorg.force.com/ESA');
            loginPage.setRedirect(true); // load a new view state
            // passthru query parms
            try {
                for (String parm : queryParms.keySet()) loginPage.getParameters().put(parm, queryParms.get(parm));
            } catch (exception e) {
                // ignore
            }
            return loginPage;
        }

        return null;
    }

    private PageReference setLoginPage(PageReference loginPage, String path) {
        loginPage.setRedirect(true); // load a new view state
        // passthru query parms in the relayState parm
        String relayState = '/' + path;
        String urlParms = '';
        try {
            Integer parmNo = 1;
            for (String parm : queryParms.keySet()) {
                if (parmNo == 1) urlParms += '?';
                   else urlParms += '&';
                parmNo ++;
                urlParms += parm + '=' + queryParms.get(parm);
            }
        } catch (exception e) {
            Esa_DebugService.WriteException(e,'Init()','There are no url parms');
            // fail silently if there are no url parms
        }
        relayState += urlParms;
        loginPage.getParameters().put('RelayState', relayState);
        loginPage.getParameters().put('startURL', relayState);
        return loginPage;
    }
    
    private boolean isRedirectToLtngCommunity() {
        String entityCode = queryParms.get('entityCode');
        String id = queryParms.get('Id');
        
        if (String.isBlank(entityCode) && String.isBlank(id)) {
            entityCode = ENTITY_CODE_ENTSEC;
        }
       
        Map<String, String> entityConfigMap = getLtngCommunityEntityConfigMap();
        
        return (entityConfigMap.keySet().contains(ENTITY_CODE_ALL) || entityConfigMap.keySet().contains(entityCode)) && !getLtngCommunityURL().containsIgnoreCase(Site.getBaseRequestUrl());
    }
    
    private String getLtngCommunityURL() {
        Map<String, String> entityConfigMap = getLtngCommunityEntityConfigMap();
        String entityCode = queryParms.get('entityCode');
        if (String.isBlank(entityCode)) {
            entityCode = ENTITY_CODE_ENTSEC;
        }
        
        String path = entityConfigMap.get(ENTITY_CODE_ALL);
        if (entityConfigMap.containsKey(entityCode)) {
            path = entityConfigMap.get(entityCode);
        } 
        
        if (path.startsWithIgnoreCase('http')) {
           return path; 
        }
        return Site.getBaseRequestUrl().replace(Site.getPathPrefix(), path);
    }
    
    private Map<String, String> getLtngCommunityEntityConfigMap() {
        Map<String, String> entityConfigMap = new Map<String, String>();
        try {
            List<String> redirectEntityCodes = Label.ESA_LtngCommunityEntityCodes.split(',');
            for (String entity : redirectEntityCodes) {
                List<String> config = entity.split(':', 2); // Only split until first :
                // Entity configuration might look like 'EntSec,BUS, ProdSec:/prodsec/s, Partner:http://somedomain/somepath'
                entityConfigMap.put(config.get(0).trim(), (config.size() > 1 ? config.get(1).trim() : 'https://securityorg.force.com/IntakeApp/s'));
            }
        } catch (exception e) {
            // Safe check
        }
        return entityConfigMap;
    }
}
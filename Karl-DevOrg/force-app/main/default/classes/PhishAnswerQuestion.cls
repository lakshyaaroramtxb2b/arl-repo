public class PhishAnswerQuestion
{

    PhishingQuizConstants qc = new PhishingQuizConstants();
    public Id qId {get;set;}
    public String sId;
    public PhishingQuizQuestion__c question;
    public PhishingQuizUser__c user;
    public String userAnswer {get;set;} //value set by the single and multi select
    public String answer;
    public Integer answeredCount {get;set;}
    String country = null;

    public Integer totalQuestionCount {get;set;}
    private datetime oldestAllowed;
    
    public PhishAnswerQuestion() {
        qId = ApexPages.CurrentPage().getParameters().get('qId');
        sId = ApexPages.CurrentPage().getParameters().get('sId');
        //userAnswer = new List<String>();
    }
    public PhishAnswerQuestion(String q,String s,PhishingQuizUser__c u) {
        // constructor for testing
        qId = q;
        sId = s;
        //userAnswer = u;
    }
    
    public String getAnswer() {
                  return answer;
            }

            public void setAnswer(String data) {
                  answer= data;
            }

    

    public PageReference init() {
       if (sId == null) {
            return qc.register; 
        }
        else {
            oldestAllowed = datetime.now().addDays(0-qc.quizValidDays);
            // load the correct QuizUser from the sId passed in the params
            // redirect them to the registration page if they don't have one
            List<PhishingQuizUser__c> activeUsers = [Select Id,Key__c,lastActive__c,Name from PhishingQuizUser__c 
                      where Key__c =: sId and created__c >=: oldestAllowed limit 1];
            if (activeUsers.size() > 0) {
                user = activeUsers.get(0);
            }
            if (user == null) {
                return qc.register;
            }
                         
            user.lastActive__c = datetime.now();
            update user;
        }

        // pull the question from qId
        if (qId != null) {
            question = [Select Name, answer__c, link__c from PhishingQuizQuestion__c where Id=: qId and active__c = 1 
                            limit 1];
                            
            // check to make sure they haven't already answered this question
            if (question != null) {
                Integer hasAnswered = [select count() from PhishingQuizAnswer__c where PhishingQuizQuestion__c =: qId
                                          and PhishingQuizUser__c =: user.Id];
                if (hasAnswered > 0) {
                    // bastards already answered this question.  Forced browsing!
                    question = null;
                }                 
            }         
        }
        // Either qId was a bad question or they didn't have a qId.  Give them a random 
        // question that they haven't filled out
        if ((qId == null) || (question == null)) {
            PhishingQuizQuestion__c next= getNextQuestion();
            if (next==null) {
                PageReference pr = new PageReference(qc.quizDone.getUrl()+'?sId='+sId);
                pr.setRedirect(True);
                return pr;
            }
            PageReference pr = new PageReference(qc.quizQuestion.getUrl()+'?qId='+next.Id+'&sId='+sId);
            pr.setRedirect(True);
            return pr;
        }
        else {
            // question should be loaded and ready
            
            // set up the options for the "PickCorrectExample" question type
            /*if (question.type__c == 'PickCorrectExample') {
                correctFixNumber = math.mod(math.abs(Crypto.getRandomInteger()),4);
                fixChoices.add(question.correct_line_opt1__c);
                fixChoices.add(question.correct_line_opt2__c);
                fixChoices.add(question.correct_line_opt3__c);
                if (correctFixNumber == 3) {
                    // The random slot for the correct answer is at the end, that's easy
                    fixChoices.add(question.correct_version__c);
                }
                else {
                    // pull out this item and move it to the end.  Insert the correct example in its place
                    String temp = fixChoices.get(correctFixNumber); 
                    fixChoices.set(correctFixNumber,question.correct_version__c);
                    fixChoices.add(temp);
                }
                correctFixNumber +=1; // 0-indexed vs the user's 1-index answer
            }*/ 
            answeredCount = [select count() from PhishingQuizAnswer__c where PhishingQuizUser__c =: user.Id];
            totalQuestionCount = [select count() from PhishingQuizQuestion__c where active__c = 1];    
            return null;
        }

        
    }
    
    public PhishingQuizQuestion__c getNextQuestion() {
        // Get the questions that the QuizUser hasn't answered
        List<PhishingQuizQuestion__c> all = [Select Id from PhishingQuizQuestion__c where active__c = 1
                                           and Id not in 
                                           (select PhishingQuizQuestion__c from PhishingQuizAnswer__c where 
                                               PhishingQuizUser__c =: user.Id)
                                        ];
        // It looks like they're done
        if (all.size() == 0) {
                return null;
            }
        
        Integer rint = math.mod(math.abs(Crypto.getRandomInteger()),all.size());
        return all.get(rint);
    }
    public String getLink() {        return question.link__c;    }
    
    public PageReference submitAnswer() {
        PhishingQuizAnswer__c a = new PhishingQuizAnswer__c(Name=user.name+':'+question.Name,
                                            PhishingQuizUser__c=user.Id,
                                            PhishingQuizQuestion__c=question.Id);
        
         if (this.answer == question.answer__c) {

                a.isCorrect__c=1;

            }
            else
            {
                a.isCorrect__c=0;
            }
                                    
        /*if ((question.type__c=='MultiSelect') || (question.type__c=='SingleSelect')) {
            // We'll assume that no items selected means that they don't believe
            // any of the vuln classes apply
            if ((userAnswer==null) || (userAnswer.size() == 0)) {
                a.answer__c = null;
            } 
            else {
                a.answer__c = stringListToDelimString(userAnswer);
            }
            if (a.answer__c == question.answers__c) {
                a.isCorrect__c=1;
            }
            else { 
                 if (question.additional_topic__c != null){
                     // This is so the question author can add a topic
                     // to be shown to the user in their scorecard
                     a.missed_topics__c = question.additional_topic__c;
                 }
                  
                 // so we know the answer wasn't exactly right
                 // check for missed vuln classes and false positives
                 Set<String> userAns = new Set<String>();
                 userAns.addAll(userAnswer);
                 Set<String> correctAns = new Set<String>();
                 if (question.answers__c != null) { // it could be correct
                     correctAns.addAll(question.answers__c.split(';'));
                 }
                 for (String b:correctAns) {
                     if (userAns.contains(b)) {
                         userAns.remove(b); // check this one off
                     }
                     else if (a.missed_topics__c== null){
                         // missed one (this is the first one missed)
                         a.missed_topics__c = b+';';
                     }
                     else {
                         // missed one, not the first time
                         a.missed_topics__c += b+';';
                     }
                 }
                 //trim extra ;
                 if ((a.missed_topics__c != null) && (a.missed_topics__c.length() > 0)){
                     a.missed_topics__c = a.missed_topics__c.substring(0,a.missed_topics__c.length()-1); 
                 }
                 // anything left in userAns is a false positive
                 for (String b:userAns) {
                     if (a.false_picks__c == null) {
                         // first one.  For some reason, we can't just
                         // do a.false_picks__c = '';
                         a.false_picks__c = b+';';
                     }
                     else {
                         a.false_picks__c += b+';';
                     }
                 }
                 if ((a.false_picks__c != null) && (a.false_picks__c.length() > 0)) {
                     a.false_picks__c = a.false_picks__c.substring(0,a.false_picks__c.length()-1);
                 }
             
                 a.isCorrect__c = 0;
             }

        }

        if (question.type__c =='PickCorrectExample') {
            if (fixAnswer==null) {
                return null; //they didn't answer
            }
            a.answer__c = fixChoices.get(fixAnswer-1); // we added one to make it 0-indexed before
            if (correctFixNumber == integer.valueOf(fixAnswer)){
                a.isCorrect__c=1;
            } else { a.isCorrect__c =0;}
        }*/
   
        insert a;
        
        PhishingQuizQuestion__c next = getNextQuestion();
        if (next==null) {
            PageReference pr = new PageReference(qc.quizDone.getUrl()+'?sId='+sId);
            pr.setRedirect(True);
            return pr;
        }
        else {
            PageReference pr = new PageReference(qc.quizQuestion.getUrl()+'?qId='+next.Id+'&sId='+sId);
            pr.setRedirect(True);
            return pr;
        }
    }
    
 
    
    public List<SelectOption> getChoices() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Legitimate','Legitimate'));
        options.add(new SelectOption('Fraud','Fraud'));
        return options;
    }
    
    
    /*public String getanswer() {
        return 'foo';
    }

    public void setCountry(String country) { this.country = country; }*/
}
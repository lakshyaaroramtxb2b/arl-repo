@isTest
public class DLM_IsDocumentTriggerHelper_Test {
    @testSetup
    public static void testSetup(){
         EmailTemplate e = new EmailTemplate (developerName = 'test',FolderId = userInfo.getUserId(), TemplateType= 'Text', Name = 'test',HTMLValue = '<p> test data body </p>'); // plus any other fields that you want to set
        
        insert e;
    }
    @isTest
    public static void testAfterUpdate(){
        List<Is_Document__c> documentUpdateList = new List<Is_Document__c>();
        List<Is_Document__c> documentUpdateList1 = new List<Is_Document__c>();
        List<Contact> contactUpdateList = new List<Contact>();
        List<Account> accountList = DLM_TestDataFactory.createAccounts(1, true);
        List<Contact> contactList = DLM_TestDataFactory.createContacts(1,accountList[0].id , false);
        EmailTemplate temp = [SELECT Id, Name, DeveloperName, BrandTemplateId, Subject, Description, Body, FolderName,HTMLValue
                                         FROM EmailTemplate where Name = 'test' LIMIT 1];
        for(contact con : contactList){
            con.RecordTypeId = DLM_TestDataFactory.DLM_CONTACTRECORDTYPEID;
            if(con.Org62_User_ID__c ==null){
                con.Org62_User_ID__c = 'qwertyuiopasdfghjk';
            }
            contactUpdateList.add(con);
        }
        Insert contactUpdateList;
        
        List<IS_Document__c> documentList = DLM_TestDataFactory.createIsDocument(1, false);
        for(IS_Document__c doc: documentList){
            doc.Content_Custodian__c = contactUpdateList[0].Id;
            doc.Content_Owner__c = contactUpdateList[0].Id;
            //doc.EmailTemplateName__c = 'DLM Stakeholder Change Business Unit';
            documentUpdateList.add(doc);
        }
        Insert documentUpdateList;
       
        for(IS_Document__c docRec : documentUpdateList){
            docRec.EmailTemplateName__c = temp.Name;
            documentUpdateList1.add(docRec);
        }
        update documentUpdateList1;
        
        List<GusUser__x> userRecordList = new List<GusUser__x>();
        GusUser__x user = new GusUser__x();
        user.ExternalId = 'ex1000000000000001';
        user.EmployeeNumber__c = '12345';
        userRecordList.add(user);
        DLM_IsDocumentTriggerHelper.mockallGusUserList.addAll(userRecordList);
    }
    
    @isTest
    public static void testCreateWorkGus(){
        
    }
}
global class CSIRT_DailyDigestSend Implements Schedulable{
	global void execute(SchedulableContext sc){
        List<Id> userId = new List<Id>{'0053000000AYGgR','0053000000BOI96','0053A00000Cre9p','0053000000A0s0V','0053A00000Cre9k'};
        for(Id u : userId){
        	sendmail(u);  
        }
	}
 
	public void sendmail(Id userId){
		Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        email.setOrgWideEmailAddressId('0D2300000004E4Z');
        email.setTemplateId('00X3A00000253BA');
        email.setTargetObjectId(userId);
        email.saveAsActivity = False;
		Messaging.sendEmail(New Messaging.SingleEmailMessage[]{email});
	}
}
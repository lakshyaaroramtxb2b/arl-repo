/***************IEM_CaseWorkTriggerHelper****************
@Author Banshi
@Date 08/25/2019
@Description Apex helper class for IEM_CaseWorkTriggerHandler class
**********************************************/

public class IEM_CaseWorkTriggerHelper {
	 @TestVisible
    public static Map<Id,String> workRecordMap = new Map<Id,String>();
    
      /*
	* This functon is used to populate Work_Subject__c.
	*/
    public static void populateExternalObjectFields(List<Case_Work__c> newList, Map<Id,Case_Work__c> oldMap){
        Set<Id> workIds = new Set<Id>();
        Set<Id> caseWorkIds = new Set<Id>();
        for(Case_Work__c caseWork : newList){
            if((oldMap == null && caseWork.Work__c != null) || ( oldMap != null && caseWork.Work__c != oldMap.get(caseWork.id).Work__c)){
                caseWorkIds.add(caseWork.Id);
            }
        }
        if(!caseWorkIds.isEmpty() && !System.isFuture() && !System.isBatch()){
            populateFieldsFuture(caseWorkIds);
        }
    }
    
    /*
	* Created by - Banshi
	* Future method to populate Case_Work__c.Work_Subject__c and Case_Work__c.Work_Name__c
	*/
    @Future
    public Static Void populateFieldsFuture(Set<Id> caseWorkIds){
        List<Case_Work__c> newList = new List<Case_Work__c>();
        for(Case_Work__c caseWork : [Select id,Work__r.Name__c,Work__r.Subject_c__c From Case_Work__c Where Id IN :caseWorkIds]){
            caseWork.Work_Name__c = caseWork.Work__r.Name__c;
            caseWork.Work_Subject__c = caseWork.Work__r.Subject_c__c;
            newList.add(caseWork);
        }
        update newList;
    }
}
/** 
 * @author Lakshya Arora
 * @email lakshya.arora@mtxb2b.com
 * @description This class will test the functionality of KARL_DocusignOperationsController
 */
@isTest
public class KARL_DocusignOperationsControllerTest {
     /** 
 * @author Lakshya Arora
 * @email lakshya.arora@mtxb2b.com
 * @description Method to create the test data
 */
    @testSetup
    private static void makeData(){
        User opsAdminUser = ARL_TestDataFactory.createOpsAdmin();
        System.runAs(opsAdminUser){
            Map<Integer,String> priorityToScopeMap = new Map<Integer,String>();
            priorityToScopeMap.put(0,'AS');
            priorityToScopeMap.put(1,'AUST');
            priorityToScopeMap.put(2,'B2BC');
            priorityToScopeMap.put(3,'CHAT');
            
            Account acc = new Account();
            acc.Name = 'salesforce.com - ESA Office Hours';
            insert acc;
            
            Contact stakeHolder = new Contact(AccountId = acc.Id,FirstName = 'Test ',LastName = 'Contact'+String.valueOf(MATH.random()), Email = 'test@contact.com.invalid',
                                             RecordTypeId = ARL_TestDataFactory.CONTACT_CUSTOMER_RECORDTYPEID);
            insert stakeHolder;
            
            Audit_Cycle__c auditCycle = ARL_TestDataFactory.createAuditCycle();
            auditCycle.OwnerId = Userinfo.getUserId();
            insert auditCycle;
            KARL_Audit_Firm__c auditFirm = ARL_TestDataFactory.createAuditFirm('testAuditFirm');
            insert auditFirm;
            
            KARL_Audit_Firm_Stakeholders__c auditFirmStakeholder = ARL_TestDataFactory.createAuditFirmStakeholders(auditFirm.Id, stakeholder.Id);
            insert auditFirmStakeholder;
            
            // Create two distinct audit teams
            KARL_Audit_Team__c auditTeam = ARL_TestDataFactory.createAuditTeam('testAuditTeam1');
            auditTeam.KARL_Audit_Firm__c = auditFirm.Id;
            insert auditTeam;
            
            List<KARL_Audit_Scope_Master_Data__c> auditScopeList = new List<KARL_Audit_Scope_Master_Data__c>();
            for(Integer i=0;i<4;i++){
                KARL_Audit_Scope_Master_Data__c auditScope = ARL_TestDataFactory.createAuditScope();
                auditScope.KARL_Scope__c = priorityToScopeMap.get(i);
                auditScope.KARL_BU_Lead__c = stakeHolder.Id;
                auditScopeList.add(auditScope);
            }
            insert auditScopeList;
            
            List<KARL_Audit_Scope_Reports__c> auditScopeReportList = new List<KARL_Audit_Scope_Reports__c>();
            for(Integer i=0;i<4;i++){
                KARL_Audit_Scope_Reports__c auditScopeReport = ARL_TestDataFactory.createAuditScopeReport(auditScopeList[i].Id,auditFirm.Id);
                auditScopeReportList.add(auditScopeReport);
            }
            insert auditScopeReportList;
            
            List<KARL_Cycle_Audit_Report_Items__c> cycleAuditScopeReportList = new List<KARL_Cycle_Audit_Report_Items__c>();
            KARL_Cycle_Audit_Report_Items__c  cycleAuditReportItemObj = ARL_TestDataFactory.createCycleAuditScopeReport(auditScopeReportList[0].Id, auditCycle.Id);
            cycleAuditReportItemObj.Status__c = KARL_Constants.REPORT_STATUS_PENDING;
            cycleAuditScopeReportList.add(cycleAuditReportItemObj);
            insert cycleAuditScopeReportList;
            
            ContentVersion contentVersion = new ContentVersion(
                Title = 'Test content',
                PathOnClient = 'testContents.jpg',
                VersionData = Blob.valueOf('Test Content'),
                IsMajorVersion = true
            );
            insert contentVersion;    
            List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
            
            //create ContentDocumentLink  record 
            ContentDocumentLink cdl = New ContentDocumentLink();
            cdl.LinkedEntityId = cycleAuditScopeReportList[0].id;
            cdl.ContentDocumentId = documents[0].Id;
            cdl.shareType = 'V';
            insert cdl;
        }  
    }
        /** 
    * @author Lakshya Arora
    * @email lakshya.arora@mtxb2b.com
    * @description Method to test the functionality of sendSignedDocumentToAuditors
    */
    @isTest
    private static void sendSignedDocumentToAuditorsTest(){
        User opsAdminUser = [SELECT Id FROM User WHERE Username = 'abc@b.com.d'];
        System.runAs(opsAdminUser){
            List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
            List<KARL_Cycle_Audit_Report_Items__c> cycleAuditScopeReportList = [SELECT Id FROM KARL_Cycle_Audit_Report_Items__c];
            Test.startTest();
            KARL_DocusignOperationsController.DocusignOperationWrapper docusignWrapperObj = KARL_DocusignOperationsController.sendSignedDocumentToAuditors(cycleAuditScopeReportList[0].Id, new List<String>{documents[0].Id});
            Test.stopTest();
            System.assertEquals('Mail Sent successfully',docusignWrapperObj.message);
            System.assert(docusignWrapperObj.isSuccess);
        }
    }
    
       /** 
    * @author Lakshya Arora
    * @email lakshya.arora@mtxb2b.com
    * @description Method to test the functionality of downloadLatestDocusignDocumentTest of report published
    */
    @isTest
    private static void downloadLatestDocusignDocumentTest1(){
        User opsAdminUser = [SELECT Id FROM User WHERE Username = 'abc@b.com.d'];
        System.runAs(opsAdminUser){
            List<KARL_Cycle_Audit_Report_Items__c> cycleAuditScopeReportList = [SELECT Id FROM KARL_Cycle_Audit_Report_Items__c];
            Test.startTest();
            KARL_DocusignOperationsController.DocusignOperationWrapper docusignWrapperObj = KARL_DocusignOperationsController.downloadLatestDocusignDocument(cycleAuditScopeReportList[0].Id);
            Test.stopTest();
            System.assertEquals('Report not available for download until Issued',docusignWrapperObj.message,'Test Failed');
            System.assert(!docusignWrapperObj.isSuccess);
        }
    }
    
        /** 
    * @author Lakshya Arora
    * @email lakshya.arora@mtxb2b.com
    * @description Method to test the functionality of downloadLatestDocusignDocumentTest of report published
    */
    @isTest
    private static void downloadLatestDocusignDocumentTest2(){
        User opsAdminUser = [SELECT Id FROM User WHERE Username = 'abc@b.com.d'];
        System.runAs(opsAdminUser){
            List<KARL_Cycle_Audit_Report_Items__c> cycleAuditScopeReportList = [SELECT Id,Status__c,Auditor_Readiness_Confirmation__c,Management_Responses_Approved__c,
                                                                                Audit_Report_Final_Draft_Confirmed__c,Approved_by_Legal__c,Audit_Letter_Temps_Formatted__c,
                                                                                Basis_of_Assertion_BOA__c
                                                                                FROM KARL_Cycle_Audit_Report_Items__c];
            cycleAuditScopeReportList[0].Status__c = KARL_Constants.REPORT_STATUS_PUBLISHED;
            cycleAuditScopeReportList[0].Auditor_Readiness_Confirmation__c = true;
            cycleAuditScopeReportList[0].Management_Responses_Approved__c = true;
            cycleAuditScopeReportList[0].Audit_Report_Final_Draft_Confirmed__c = true ;
            cycleAuditScopeReportList[0].Approved_by_Legal__c = true;
            cycleAuditScopeReportList[0].Audit_Letter_Temps_Formatted__c = true;
            cycleAuditScopeReportList[0].Basis_of_Assertion_BOA__c = true;
            update cycleAuditScopeReportList;
            Test.startTest();
            KARL_DocusignOperationsController.DocusignOperationWrapper docusignWrapperObj = KARL_DocusignOperationsController.downloadLatestDocusignDocument(cycleAuditScopeReportList[0].Id);
            Test.stopTest();
            System.assertEquals('File downloaded successfully',docusignWrapperObj.message);
            System.assert(docusignWrapperObj.isSuccess);
        }
    }
       /** 
    * @author Lakshya Arora
    * @email lakshya.arora@mtxb2b.com
    * @description Method to test the functionality of getAllFiles related to Cycle Audit Report Id
    */
    @isTest
    private static void getAllFilesTest(){
        User opsAdminUser = [SELECT Id FROM User WHERE Username = 'abc@b.com.d'];
        System.runAs(opsAdminUser){
            List<KARL_Cycle_Audit_Report_Items__c> cycleAuditScopeReportList = [SELECT Id,Status__c,Auditor_Readiness_Confirmation__c,Management_Responses_Approved__c,
                                                                                Audit_Report_Final_Draft_Confirmed__c,Approved_by_Legal__c,Audit_Letter_Temps_Formatted__c,
                                                                                Basis_of_Assertion_BOA__c
                                                                                FROM KARL_Cycle_Audit_Report_Items__c];
            Test.startTest();
            List<KARL_DocusignOperationsController.FileWrapper> fileWrapperList = KARL_DocusignOperationsController.getAllFiles(cycleAuditScopeReportList[0].Id);
            Test.stopTest();
            System.assert(!fileWrapperList.isEmpty(),'get all files Test Failed');
        }
    }
}
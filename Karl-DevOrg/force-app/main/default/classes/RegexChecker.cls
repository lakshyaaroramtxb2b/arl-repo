global class RegexChecker {
    
    @future (callout=true)
	public static void benchmarkRegex(Id record) {
    	Detection_Field_Extraction__c rec = [SELECT Id, Field_Extraction_Regex__c, Log_Example__c
                                               FROM Detection_Field_Extraction__c 
                                               WHERE Id =: record];
        
        System.debug('RegexChecker A');
        
        if(rec != null) {

            String body = 'regex=' + EncodingUtil.urlEncode(rec.Field_Extraction_Regex__c, 'UTF-8')
                	+ '&regex_sample_log=' + EncodingUtil.urlEncode(rec.Log_Example__c, 'UTF-8');
            
            HttpRequest req = new HttpRequest();
            
            req.setTimeout(30000);
            
			req.setHeader('Content-Type', 'application/x-www-form-urlencoded');

    		req.setBody(body);
    		req.setMethod('POST');
    		req.setEndpoint('https://intense-retreat-42839.herokuapp.com/');
            
    		//req.setEndpoint('http://requestb.in/1jmccbu1');
            //req.setCompressed(true);
            
            System.debug('RegexChecker B'); 

    		Http http = new Http();
    		HttpResponse res = http.send(req); 
            
            String [] retVals = res.getBody().split(',');
            
            rec.debug__c = res.getBody();
            
            if(retVals[0] == '-1')
     			rec.Regex_Benchmark_Match__c = 'Error sample regex does not match';
            else
                rec.Regex_Benchmark_Match__c = retVals[0] + ' ms.';
            
			rec.Regex_Benchmark_Non_Match__c = retVals[1] + ' ms.';
            
            //System.debug('RegexChecker R = ' + rec.Regex_Benchmark_Match__c);
 
            update rec;
        }
  	}
}
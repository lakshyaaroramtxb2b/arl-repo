public class ESA_ApprovalInfoValidation {
    
    
    public static void infoValidation (List <ESA_Approval_Information__c> approvalL){
        
        Set <Id> approvalIds = new Set <Id> ();
        Set <Id> serviceItemIds = new Set <Id>();
        Set <Id> categoriesIds = new Set <Id>();
        
        for (ESA_Approval_Information__c app : approvalL){
            serviceItemIds.add(app.Service_Item__c);
            categoriesIds.add(app.Service_Item_Category__c);
            approvalIds.add(app.Id);
                 
        }
        
 
        List <ESA_Approval_Information__c> serviceApprovalL = [SELECT Id, Service_Item__c 
                                                               FROM ESA_Approval_Information__c
                                                               WHERE Service_Item__c IN: serviceItemIds 
                                                               AND Id NOT IN: approvalIds];
        
        system.debug(serviceApprovalL);
        
        List <ESA_Approval_Information__c> categoryApprovalL = [SELECT Id, Service_Item_Category__c 
                                                               FROM ESA_Approval_Information__c
                                                               WHERE Service_Item_Category__c IN: categoriesIds
                                                               AND Id NOT IN: approvalIds];
        
        Map <Id, ESA_Approval_Information__c> serviceApprovalMap = new Map <Id, ESA_Approval_Information__c> (serviceApprovalL);
        Map <Id, ESA_Approval_Information__c> categoryApprovalMap = new Map <Id, ESA_Approval_Information__c> (categoryApprovalL);
        
        
        for (ESA_Approval_Information__c app: approvalL){
            
            if (app.Service_Item__c != null && serviceApprovalL.size() > 0){
                system.debug(app);
                for (ESA_Approval_Information__c serv: serviceApprovalL){
                    
                    if (serv.Service_Item__c == app.Service_Item__c){
                        
                        app.Service_Item__c.addError('This service item has already an approval information record');
                    }
                    
                }
        
            }
            
            else if (app.Service_Item_Category__c != null && categoryApprovalL.size() >0){
                
                for (ESA_Approval_Information__c serv: categoryApprovalL){
                    
                    if (serv.Service_Item_Category__c == app.Service_Item_Category__c){
                        
                        app.Service_Item_Category__c.addError('This service item category has already an approval information record');
                    }
                }
                
                
            }
            
            
            
            
        }
       
    }

}
global class DetectionEventCriteriaHealthCheck implements Schedulable {
    
    global void execute(SchedulableContext ctx) {
        
        Database.executeBatch(new DCHealthBatchService());
        
    }
}
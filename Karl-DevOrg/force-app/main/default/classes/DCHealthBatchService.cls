global class DCHealthBatchService implements Database.Batchable<Sobject>, Database.Stateful {
    
    global Map<Id, Integer> dseMap;

    global Contact con;
    global EmailTemplate et;

    global Database.QueryLocator start(Database.BatchableContext info) {
        String query =    ' SELECT Id, DetectionSecurityEventCriteria__c '
                        + ' FROM  '
                        + '    Detection_Security_Event__c '
                        + ' WHERE ' 
                        + '     CreatedDate = YESTERDAY ';
        
        dseMap = new Map<Id, Integer>();

        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<Sobject> scope) {
        
        for (Sobject so : scope)  {

            Detection_Security_Event__c dse = (Detection_Security_Event__c)so;
            
            if(dseMap.containsKey(dse.DetectionSecurityEventCriteria__c)) {
                
                Integer val = dseMap.get(dse.DetectionSecurityEventCriteria__c);
                val = val + 1;
                
                dseMap.put(dse.DetectionSecurityEventCriteria__c, val);
            }
            else
            {
                dseMap.put(dse.DetectionSecurityEventCriteria__c, 1);
            }
        }
        
    }
    
    global void finish(Database.BatchableContext BC) {
        
        List<id> unhealthEvtCriteria = new List<id>();
        Map<id, Integer> countDB = new Map<id, Integer>();
        
        string [] toAddress= New string[]{'amcneilly@salesforce.com'};  
        
        List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
        
        con = [SELECT Id, Name, Email FROM Contact WHERE email =: 'amcneilly@salesforce.com' LIMIT 1];
        et = [SELECT id, body, HtmlValue FROM EmailTemplate WHERE DeveloperName  = 'Delinquent_Detection_Security_Criteria'];

        for(id v : dseMap.keySet())
        {
            if(dseMap.get(v) >= 2000) {
                
                unhealthEvtCriteria.add(v); 
                countDB.put(v, dseMap.get(v));
                
            }
        }

        List<Detection_Security_Event_Criteria__c> arEvts = [
            
                                        SELECT  
                                                OwnerId, Id, createdby.name, createdby.email, Name, Rule_Name__c
                                        FROM 
                                                Detection_Security_Event_Criteria__c 
                                        WHERE 
                                                Known_Noisy_Event__c = false
                                                AND Id IN : unhealthEvtCriteria
                                        
                                        ];
        
        for(Detection_Security_Event_Criteria__c rec : arEvts)
        {
            
            rec.lastEventCount__c = String.valueOf( countDB.get(rec.Id) ); 
            update rec;
            
            Contact con = [SELECT Id, Name, Email FROM Contact WHERE email =: rec.createdby.email LIMIT 1];
            EmailTemplate et = [SELECT id, body, HtmlValue FROM EmailTemplate WHERE DeveloperName  = 'Delinquent_Detection_Security_Criteria'];

            string [] ccAddress= New string[]{'detectioncloud@salesforce.com'};
    
            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
            
            email.setCcAddresses(ccAddress);
            email.setWhatId(rec.Id);
            email.setTargetObjectId(con.id);
            email.setTemplateId(et.id);
            email.setSaveAsActivity(false);
            
            emails.add(email);
        }

        
        Messaging.sendEmail(emails);

        countDB.clear();
        dseMap.clear();  
         
    }
    
    global void SendSummaryEmail(Database.BatchableContext BC) {
        
		Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage() ;
        
		String[] toAddresses = new String[] {'amcneilly@salesforce.com'};
		email.setToAddresses(toAddresses) ;

		email.setSubject('Summary - Delinquent Detection Security Criterias');
        
		String body = '<html lang="ja"><body>';
        
        /*+ 
                          '<br><br>'+'This email alert is to bring to your notice that the licence of the client :'+'  '+'<b>'+accountName+'</b>'+'  '+'is going to expiry within'+'<b>'+' '+noOfdays+' '+'</b>'+
                          '</body></html>';*/
        
		email.setHtmlBody(body);
        
		Messaging.sendEmail(new Messaging.SingleEmailMessage[] { email });
        
    }
}
public class AnswerQuestion {
    public class TestException extends Exception {} 
    
    public QuizConstants qc = new QuizConstants();
    public Id qId {get;set;}
    public String sId;
    public QuizQuestion__c question;
    public QuizUser__c user;
    public List<String> userAnswer {get;set;} //value set by the single and multi selects
    public Integer answeredCount {get;set;}
    private Integer totalQuestionsAvailable; // Total number of questions in the system
    public Integer totalQuestionCount {get;set;}  // Number of questions that the user will get
    private datetime oldestAllowed;
    private Map<Integer,Integer> splits;
    
    // These are used in the PickCorrectExample type
    public List<String> fixChoices = new List<String>();
    public Integer fixAnswer {get;set;}
    public Integer correctFixNumber {get;set;} //hopefully they don't check the viewstate
        
    public AnswerQuestion() {
        qId = ApexPages.CurrentPage().getParameters().get('qId');
        sId = ApexPages.CurrentPage().getParameters().get('sId');
        userAnswer = new List<String>();
        
        Cookie trackingCookie = ApexPages.currentPage().getCookies().get('siteId');
        if ((trackingCookie == null) || (trackingCookie.getValue() == '') || (trackingCookie.getValue() == null)) {
            Cookie c = new Cookie('siteId', String.valueOf(Crypto.getRandomLong()), null, -1, false);
            ApexPages.currentPage().setCookies(new Cookie[] { c });
        }
        
    }
    public AnswerQuestion(String q,String s,QuizUser__c u) {
        // constructor for testing
        qId = q;
        sId = s;
        userAnswer = new List<String>();
    }
    public PageReference init() {
        if (sId == null) {
            return qc.register; 
        }
        else {
            oldestAllowed = datetime.now().addDays(0-qc.quizValidDays);
            // load the correct QuizUser from the sId passed in the params
            // redirect them to the registration page if they don't have one
            List<QuizUser__c> activeUsers = [Select Id,Key__c,lastActive__c,Question_Count__c,
                                             Name from QuizUser__c 
                                             where Key__c =: sId 
                                                   and created__c >=: oldestAllowed limit 1];
            if (activeUsers.size() > 0) {
                user = activeUsers.get(0);
            }
            if (user == null) {
                return qc.register;
            }
                         
            user.lastActive__c = datetime.now();
            update user;
        }
        
        // pull the question from qId
        if (qId != null) {
            question = [Select Name,answers__c,apex_code__c,bad_linenums__c,correct_line_opt1__c,
                            correct_line_opt2__c,correct_line_opt3__c,correct_version__c,
                            scontrol_code__c,type__c,vf_code__c,answeroptions__c,answer_prompt_override__c, 
                            additional_topic__c from QuizQuestion__c where Id=: qId and active__c = 1 
                            limit 1];
                            
            // check to make sure they haven't already answered this question
            if (question != null) {
                Integer hasAnswered = [select count() from QuizAnswer__c where QuizQuestion__c =: qId
                                          and user__c =: user.Id];
                if (hasAnswered > 0) {
                    // bastards already answered this question.  Forced browsing!
                    question = null;
                    qId = null;
                }                 
            }         
        }
        
        answeredCount = [select count() from QuizAnswer__c where user__c =: user.Id];
        if (answeredCount >= user.Question_Count__c ) {
            // we'll let them off easy
            PageReference pr = new PageReference(qc.quizDone.getUrl()+'?sId='+sId);
            pr.setRedirect(True);
            return pr;
        }
        
        totalQuestionsAvailable = [select count() from QuizQuestion__c where active__c = 1];
        totalQuestionCount = math.min(totalQuestionsAvailable,user.Question_Count__c).intValue(); // how many they're going to get
        setSplits();

        // Either qId was a bad question or they didn't have a qId.  Give them a random 
        // question that they haven't filled out
        if ((qId == null) || (question == null)) {
            QuizQuestion__c next= getNextQuestion();
            if (next==null) {

                PageReference pr = new PageReference(qc.quizDone.getUrl()+'?sId='+sId);
                pr.setRedirect(True);
                return pr;
            }
            PageReference pr = new PageReference(qc.quizQuestion.getUrl()+'?qId='+next.Id+'&sId='+sId);
            pr.setRedirect(True);
            return pr;
        }
        else {
            // question should be loaded and ready
            
            // set up the options for the "PickCorrectExample" question type
            if (question.type__c == 'PickCorrectExample') {
                correctFixNumber = math.mod(math.abs(Crypto.getRandomInteger()),4);
                fixChoices.add(question.correct_line_opt1__c);
                fixChoices.add(question.correct_line_opt2__c);
                fixChoices.add(question.correct_line_opt3__c);
                if (correctFixNumber == 3) {
                    // The random slot for the correct answer is at the end, that's easy
                    fixChoices.add(question.correct_version__c);
                }
                else {
                    // pull out this item and move it to the end.  Insert the correct example in its place
                    String temp = fixChoices.get(correctFixNumber); 
                    fixChoices.set(correctFixNumber,question.correct_version__c);
                    fixChoices.add(temp);
                }
                correctFixNumber +=1; // 0-indexed vs the user's 1-index answer
            }    
            return null;
        }
             
    }
    public QuizQuestion__c getNextQuestion() {
          if ((answeredCount >= user.Question_Count__c ) || (answeredCount >= totalQuestionsAvailable)) {
            // we'll let them off easy
            return null;
          }
          // See if we need to give any more
          Integer easyAnsweredCount = [SELECT count() FROM QuizQuestion__c WHERE active__c = 1
                                   AND Difficulty__c =: String.valueOf(1) 
                                   AND Id IN (select QuizQuestion__c from QuizAnswer__c where 
                                               user__c =: user.Id)];                                 
          if (splits.get(1) > easyAnsweredCount) {
              // they need an easy question
              List<QuizQuestion__c> all = [SELECT Id FROM QuizQuestion__c WHERE active__c = 1
                                           AND Difficulty__c =: String.valueOf(1)
                                           AND Id NOT IN 
                                           (select QuizQuestion__c from QuizAnswer__c where 
                                               user__c =: user.Id)
                                        ];
        
        
              Integer rint = math.mod(math.abs(Crypto.getRandomInteger()),all.size());
              return all.get(rint);
          }
          
          // well, they answered enough easy questions.  Let's try medium    
          Integer medAnsweredCount = [SELECT count() FROM QuizQuestion__c WHERE active__c = 1
                                   AND Difficulty__c =: String.valueOf(2) 
                                   AND Id IN (select QuizQuestion__c from QuizAnswer__c where 
                                               user__c =: user.Id)];
          if (splits.get(2) > medAnsweredCount) {
              // they need a medium question
              List<QuizQuestion__c> all = [SELECT Id FROM QuizQuestion__c WHERE active__c = 1
                                           AND Difficulty__c =: String.valueOf(2)
                                           AND Id NOT IN 
                                           (select QuizQuestion__c from QuizAnswer__c where 
                                               user__c =: user.Id)
                                        ];
        
        
              Integer rint = math.mod(math.abs(Crypto.getRandomInteger()),all.size());
              return all.get(rint);
          }    
          
          // OK, well, they obviously need a hard question.  Let's optimize and skip the SOQL check
          List<QuizQuestion__c> all = [SELECT Id FROM QuizQuestion__c WHERE active__c = 1
                                           AND Difficulty__c =: String.valueOf(3)
                                           AND Id NOT IN 
                                           (select QuizQuestion__c from QuizAnswer__c where 
                                               user__c =: user.Id)
                                        ];
          Integer rint = math.mod(math.abs(Crypto.getRandomInteger()),all.size());
          return all.get(rint);
    }
    
    // We use all of these getters so we don't have to give anyone CRUD/FLS to the QuizQuestion objects
    // or fields.  Cheating bastards....
    public String getVf_code() {
        return question.vf_code__c;
    }
    public String getApex_code() {
        return question.apex_code__c;
    }
    public String getScontrol_code() {
        return question.scontrol_code__c;
    }
    
    // The list of possible answers to be displayed for SingleSelect and MultiSelects
    public List<SelectOption> getAnswerOptions(){
        List<SelectOption> opts = new List<SelectOption>();
        for (String opt:question.answeroptions__c.split(';')){
            opts.add(new SelectOption(opt,opt));
        }
        return opts;
    }
    
    public String stringListToDelimString(List<String> str){
        if (str == null) {
            return null;
        }
        String tmp = '';
        for (String s : str){
            tmp+=s+';';
        }
        if (tmp.length() <=1 ) {
            //odd
            return '';
        }
        return tmp.substring(0,tmp.length()-1);
    }
    public PageReference submitAnswer() {
        question = [Select Name,answers__c,apex_code__c,bad_linenums__c,correct_line_opt1__c,
                            correct_line_opt2__c,correct_line_opt3__c,correct_version__c,
                            scontrol_code__c,type__c,vf_code__c,answeroptions__c,answer_prompt_override__c, 
                            additional_topic__c from QuizQuestion__c where Id=: qId and active__c = 1 
                            limit 1];
                            
        // check to make sure they haven't already answered this question
        Integer hasAnswered = [select count() from QuizAnswer__c where QuizQuestion__c =: qId
                                          and user__c =: user.Id];
        if (hasAnswered > 0) {
             // bastards already answered this question.  Forced browsing!
             return null;        
        }
        
        Cookie trackingCookie = ApexPages.currentPage().getCookies().get('siteId');
        String trackingString = '';
        if ((trackingCookie == null)) {
            new Notifier(Notifier.Severity.WARNING,'DevSecurityQuiz: Question answered without cookie [User:'+user.Email__c+']');   
        } else {
            trackingString = trackingCookie.getValue();
        }
        
        QuizAnswer__c a = new QuizAnswer__c(Name=user.name+':'+question.Name,
                                            user__c=user.Id,
                                            QuizQuestion__c=question.Id,
                                            Tracking_Cookie__c = trackingString,
                                            IP__c = ApexPages.currentPage().getHeaders().get('X-Forwarded-For'));
                                            
        if ((question.type__c=='MultiSelect') || (question.type__c=='SingleSelect')) {
            // We'll assume that no items selected means that they don't believe
            // any of the vuln classes apply
            if ((userAnswer==null) || (userAnswer.size() == 0)) {
                a.answer__c = null;
            } 
            else {
                a.answer__c = stringListToDelimString(userAnswer);
            }
            if (a.answer__c == question.answers__c) {
                a.isCorrect__c=1;
            }
            else { 
                 if (question.additional_topic__c != null){
                     // This is so the question author can add a topic
                     // to be shown to the user in their scorecard
                     a.missed_topics__c = question.additional_topic__c+';';
                 }
                  
                 // so we know the answer wasn't exactly right
                 // check for missed vuln classes and false positives
                 Set<String> userAns = new Set<String>();
                 userAns.addAll(userAnswer);
                 Set<String> correctAns = new Set<String>();
                 if (question.answers__c != null) { // it could be correct
                     correctAns.addAll(question.answers__c.split(';'));
                 }
                 for (String b:correctAns) {
                     if (userAns.contains(b)) {
                         userAns.remove(b); // check this one off
                     }
                     else if (a.missed_topics__c== null){
                         // missed one (this is the first one missed)
                         a.missed_topics__c = b+';';
                     }
                     else {
                         // missed one, not the first time
                         a.missed_topics__c += b+';';
                     }
                 }
                 //trim extra ;
                 if ((a.missed_topics__c != null) && (a.missed_topics__c.length() > 0)){
                     a.missed_topics__c = a.missed_topics__c.substring(0,a.missed_topics__c.length()-1); 
                 }
                 // anything left in userAns is a false positive
                 for (String b:userAns) {
                     if (a.false_picks__c == null) {
                         // first one.  For some reason, we can't just
                         // do a.false_picks__c = '';
                         a.false_picks__c = b+';';
                     }
                     else {
                         a.false_picks__c += b+';';
                     }
                 }
                 if ((a.false_picks__c != null) && (a.false_picks__c.length() > 0)) {
                     a.false_picks__c = a.false_picks__c.substring(0,a.false_picks__c.length()-1);
                 }
             
                 a.isCorrect__c = 0;
             }

        }

        if (question.type__c =='PickCorrectExample') {
            if (fixAnswer==null) {
                return null; //they didn't answer
            }
            a.answer__c = fixChoices.get(fixAnswer-1); // we added one to make it 0-indexed before
            if (correctFixNumber == integer.valueOf(fixAnswer)){
                a.isCorrect__c=1;
            } else { 
                a.isCorrect__c =0;
                a.missed_topics__c = question.additional_topic__c;
            }
        }
   
        insert a;
        
        answeredCount += 1;
        
        QuizQuestion__c next = getNextQuestion();
        if (next==null) {
            PageReference pr = new PageReference(qc.quizDone.getUrl()+'?sId='+sId);
            pr.setRedirect(True);
            return pr;
        }
        else {
            PageReference pr = new PageReference(qc.quizQuestion.getUrl()+'?qId='+next.Id+'&sId='+sId);
            pr.setRedirect(True);
            return pr;
        }
    }
    public String getAltPrompt() {
        return question.answer_prompt_override__c;
    }
    public String getQtype() {
        return question.type__c;
    }
    public String getExample4() {
        return fixChoices.get(3);
    }
    public String getExample3() {
        return fixChoices.get(2);
    }
    public String getExample2() {
        return fixChoices.get(1);
    }
    public String getExample1() {
        return fixChoices.get(0);
    }
    public List<SelectOption> getfixMeopts() {
        List<SelectOption> opts = new List<SelectOption>();
        for (Integer i=1;i<=4;i++) {
            opts.add(new SelectOption(String.valueOf(i),String.valueOf(i)));
        }
        return opts;
    }
    public void setSplits() { 
        Integer easyCount = [SELECT count() FROM QuizQuestion__c WHERE active__c = 1
                                   AND Difficulty__c =: String.valueOf(1)];
        Integer medCount = [SELECT count() FROM QuizQuestion__c WHERE active__c = 1
                                   AND Difficulty__c =: String.valueOf(2)];
        Integer hardCount = [SELECT count() FROM QuizQuestion__c WHERE active__c = 1
                                   AND Difficulty__c =: String.valueOf(3)];
        if (totalQuestionsAvailable <= user.Question_Count__c) {
            // hey!  There aren't enough questions, we're done
            splits = new Map<Integer,Integer>();
            splits.put(1,easyCount);
            splits.put(2,medCount);
            splits.put(3,hardCount);
            return;
        }
        
        this.splits = new Map<Integer,Integer>();
        Map<Integer,Integer> bl = new Map<Integer,Integer>();
        bl.put(1,easyCount);
        bl.put(2,medCount);
        bl.put(3,hardCount);
        Boolean isOddSplit;
        if (math.mod(user.Question_Count__c.longValue(),bl.keySet().size()) != 0) { isOddSplit = True;}
        else { isOddSplit = False;}
        
        Boolean updated = True;
        Integer total = user.Question_Count__c.intValue();  // number of questions this user gets
        Integer evenSplit = math.floor(total / bl.keySet().size()).intValue();
        Integer target; // just something so we can include odd splits
        List<Integer> keysToRemove = new List<Integer>(); //lame, "cannot modify a collection while iterating"
        while (updated) {
            updated = False;
            for (Integer bucket : bl.keySet()) {
                if (isOddSplit) { 
                    // the low bucket gets favored
                    target = evenSplit + 1;
                    // but only the low bucket
                    isOddSplit = False; 
                } 
                else { 
                    target = evenSplit;
                }
                      
                if (bl.get(bucket) <= target) {
                    // we're using all the questions from this bucket
                    this.splits.put(bucket,bl.get(bucket));
                    total = total - bl.get(bucket); // we'll need to split the remaining questions
                    keysToRemove.add(bucket);
                    if (keysToRemove.size() == bl.keySet().size()) {
                        // well, we're using all our questions (and we shouldn't have hit this point)
                        return;
                    }
                    
                    evenSplit = math.floor(total / (bl.keySet().size()-keysToRemove.size())).intValue();
                    if (math.mod(total,bl.keySet().size()-keysToRemove.size()) != 0) { isOddSplit = True;}
                    else { isOddSplit = False;}
                    updated = True;
                }
            }
            for (Integer key : keysToRemove) {
                bl.remove(key);
            }
            keysToRemove.clear();      
            // so splits now contains all questions buckets where there weren't enough 
            // questions to meet the minimum requirements.  The remaining buckets
            // have enough to split the rest evenly
            for (Integer bucket : bl.keySet()) {
                if (isOddSplit) { 
                    // the low bucket gets favored
                    target = evenSplit + 1;
                    // but only the low bucket
                    isOddSplit = False; 
                } 
                else { 
                    target = evenSplit;
                }
                this.splits.put(bucket,target);
            }
        }
      }
           
}
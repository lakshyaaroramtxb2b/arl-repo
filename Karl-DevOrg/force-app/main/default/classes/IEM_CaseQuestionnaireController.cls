/***********************************************************
* Controller for CaseQuestionnaire Component
* Created by   - Prashant Gupta
* Date         - 16th July 2019
************************************************************/
public without sharing class IEM_CaseQuestionnaireController {
    /*
     * Created by - Prashant Gupta
     * This functon is used to get question dependencies from the metadata.
	*/
    @auraEnabled
    public static Map<String,Map<String,String>> getDependencies(){
        Map<String,Map<String,String>> dependenciesMap = new Map<String,Map<String,String>>();
        for(IEM_Questionnaire_Mapping__mdt mapping : [SELECT MasterLabel,DeveloperName,Dependent_Field__c,Expected_Value__c,Field__c FROM IEM_Questionnaire_Mapping__mdt WHERE Active__c = true ]){
            if(!dependenciesMap.containskey(mapping.Field__c)){
                dependenciesMap.put(mapping.Field__c,new Map<String,String>());
            }
            dependenciesMap.get(mapping.Field__c).put(mapping.Expected_Value__c,mapping.Dependent_Field__c);
        }
        
        return dependenciesMap;
    }
    /*
     * Created by - Prashant Gupta
     * This functon is used to get IEM case record type Id.
	*/
    @auraEnabled
    public static String getRecordId(){
        return IEM_Constants.IEM_CASERECORDTYPEID;
    }
    
    /*
     * Created by - Prashant Gupta
     * This functon is used to share case record
	*/
    @AuraEnabled
    public static boolean createUserSharingAccess(String caseRecordID) {
        List<CaseShare> sharingRecords = new List<CaseShare>();        
        if(UserInfo.getUserType()=='Standard'){
            sharingRecords.add(IEM_CaseTriggerHelper.prepareShareObject(caseRecordID,UserInfo.getUserId(),'Edit'));
        }        
        if(sharingRecords!=null && !sharingRecords.isEmpty()){
            insert sharingRecords;
            return true;
        }        
        return false;
    }
    
   /*
     * Created by - Prashant Gupta
     * This functon is used to get help url from metadata
	*/
    @AuraEnabled
    public static String getHelpURL(String urlToGet) {
        IEM_Help_Link__mdt linkUrl = new IEM_Help_Link__mdt();
        linkUrl= [SELECT  Label, URL__c FROM IEM_Help_Link__mdt WHERE Label = :urlToGet LIMIT 1 ];
        if(linkUrl!=null && linkUrl.URL__c!='')
            return linkUrl.URL__c;
        else
            return null;
    }
    
    /*
     * Created by - Prashant Gupta
     * This functon is used to get Issue Owner Contact Id.
	*/
    @AuraEnabled
    public static Id getIssueOwner(String teamResponsibleId){
        List<Contact> conRecord = new List<Contact>();
        if(teamResponsibleId!='' && teamResponsibleId!=null){
            List<ADM_Scrum_Team_c__x> team = new List<ADM_Scrum_Team_c__x>([SELECT id, Product_Owner_c__c FROM ADM_Scrum_Team_c__x WHERE externalId = :teamResponsibleId LIMIT 1 ]);
            if(team!=null && !team.isEmpty()){
                conRecord = new List<contact> ([SELECT id, Name FROM Contact WHERE Org62_User_ID__c = :team[0].Product_Owner_c__c LIMIT 1]);
            }
        }
        if(!conRecord.isEmpty()){
            return conRecord[0].id;
        }else{
            return null;
        }
    }
    
    
    /*
     * Created by - Banshi
     * This functon is used to next issue owner and the available calendar slots for appointment.
	*/
    @AuraEnabled
    public static OwnerAvailability getOwnerAvailableSlots(){
        OwnerAvailability ownerWrapper = new OwnerAvailability();
        //Id ownerId = IEM_CaseQuestionnaireController.getNextCaseOwner();
        IEM_CaseOwer caseOwner = new IEM_CaseOwer(); //To get next case owner
        Id ownerId;
        if(Test.isRunningTest()){
            ownerId = userInfo.getUserId();
        }else{
             ownerId = caseOwner.getNextCaseOwner();
        }
        ownerWrapper.ownerId = ownerId;
        ownerWrapper.caseRequestor = getRequestorContactID(UserInfo.getUserId());
        List<Slots> availableSlots = new List<Slots>();
        if(String.isNotEmpty(ownerId)){
            DateTime dt = system.now();
            DateTime endDateTime = dt.addDays(60);
            Integer slotLimit = 1;
            Set<DateTime> bookedSlots = new Set<DateTime>();
            for(Event e : [Select Id,StartDateTime From Event Where Subject= 'Office Hours Appointment' AND StartDateTime > :dt AND OwnerId = :ownerId AND EndDateTime <= :endDateTime]){
                bookedSlots.add(e.StartDateTime);
            }
            for(Event e : [Select Id,StartDateTime,EndDateTime, OwnerId, Subject From Event Where Subject='Office Hours' AND StartDateTime > :dt AND OwnerId = :ownerId]){
                if(slotLimit>5){
                    break;
                }
                DateTime startDateTime = e.StartDateTime;
                do{
                   if(! (bookedSlots.contains(startDateTime)) ){
                        Slots slot = new Slots();
                        slot.slotNumber = slotLimit;
                        slot.startDateTime =startDateTime;
                        slot.endDateTime = startDateTime.addMinutes(30);
                        slot.ownerId = e.OwnerId;
                        slot.label = startDateTime.format()+' - '+slot.endDateTime.format();
                        availableSlots.add(slot);
                        slotLimit++;
                    }
                    startDateTime = startDateTime.addMinutes(30);
                }while(startDateTime < e.EndDateTime && slotLimit <= 5);
            }
            
        }
        ownerWrapper.slots = availableSlots;
        return ownerWrapper;
    }
    
    /*
     * Created by - Prashant Gupta
     * This functon is used to get list of all the required fields.
	*/
    @auraEnabled
    public static List<String> getrequiredFieldsSet(DateTime dt, Id ownerId){
        List<String> RequiredFieldsList = new List<String>();
        for(Schema.FieldSetMember field : SObjectType.Case.FieldSets.IMT_RequiredFields.getFields()) {
            RequiredFieldsList.add(field.getFieldPath());
        }
        return RequiredFieldsList;
    }
    
    /*
     * Created by - Banshi
     * This functon is used to get current logged user contact Id
	*/
    public static Id getRequestorContactID(Id userId){
        User userRec = [SELECT id,EmployeeNumber,User.Profile.UserLicense.Name, contactId FROM User where id = :userId Limit 1 ];
        
        if(userRec.contactId!=null){
            return userRec.contactId;
        }else if(userRec.EmployeeNumber!=null){  
            List<Contact> conRec = new List<Contact>([SELECT ID from Contact where Employee_ID__c=:userRec.EmployeeNumber 
                                                      AND recordType.DeveloperName =: IEM_Constants.CONTACT_RECORD_TYPE_SALESFORCE_EMPLOYEE 
                                                      Limit 1 ] );
            if(conRec!=null && !conRec.isEmpty()){
                    return conRec[0].id;
            }
            return null;           
        }
        
        return null;
    }
    
    /*
     * Created by - Banshi
     * This functon is used create event for appointment booking.
	*/
    @AuraEnabled
    public static Boolean createEvent(DateTime dt, Id ownerId, String CaseID){
        Boolean isSuccess = false;
        try{
            List<Case> caseRec = new List<Case>([SELECT id, CaseNumber FROM Case WHERE id =:CaseID ]);
            Event eve = new Event(OwnerId =ownerId,StartDateTime =dt,EndDateTime = dt.addMinutes(30),
                                  whatId = CaseId, Subject = 'Office Hours Appointment' );
            if(!caseRec.isEmpty()){                
                eve.Description = 'Office Hours Appointment for Case Number : '+ caseREc[0].caseNumber;
            }
            insert eve;
            isSuccess = true;
        }
        catch(Exception e){
        }
        return isSuccess;
    }
    
    /*
     * Created by - Banshi
     * Wrapper class for next owner hours availability.
	*/
    public class OwnerAvailability{
        @AuraEnabled public Id ownerId;
        @AuraEnabled public Id caseRequestor;
        @AuraEnabled public List<Slots> slots;
        public OwnerAvailability(){
            this.slots = new List<Slots>();
        }
    }
    
    public class Slots{
        @AuraEnabled public Integer slotNumber;
        @AuraEnabled public DateTime startDateTime;
        @AuraEnabled public DateTime endDateTime;
        @AuraEnabled public Id ownerId;
        @AuraEnabled public String label;
    }
}
/***********************************************************
* Wrapper class for Project Object used while comparing the 2 Project records
* to determine the ranks. This Class implements Comparable.
* Created by    - Prashant Gupta
* Date      - 24th July 2019
************************************************************/

public class PRT_ProjectWrapper implements Comparable{
    public Project__c project;
    
    // Constructor
    public PRT_ProjectWrapper(Project__c p) {
        project = p;
    }
    
    // Compare project based on the score number.
    public Integer compareTo(Object compareTo) {
        // Cast argument to PRT_ProjectWrapper
        PRT_ProjectWrapper compareToProject = (PRT_ProjectWrapper)compareTo;
        
        // The return value of 0 indicates that both elements are equal.
        Integer returnValue = 0;
        if (project.Overall_Score__c > compareToProject.project.Overall_Score__c) {
            // Set return value to a positive value.
            returnValue = -1;
        } else if (project.Overall_Score__c < compareToProject.project.Overall_Score__c) {
            // Set return value to a negative value.
            returnValue = 1;
        }
        
        return returnValue;       
    }
    
}
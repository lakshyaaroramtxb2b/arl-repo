/*

    Developer Name: Jorge L Caceres - jcaceres@salesforce.com | September 2015
    
    Description: Container for all TMM related constants for easy one place reference

*/

public with sharing class TM_Constants {
    
    public static final String GUEST_USER_TYPE                {get; private set;} 
    public static final String COMMUNITY_SITE_TYPE            {get; private set;} 
    public static final String BU_ENVIRONMENT_TYPE            {get; private set;} 
    
    public static final String BU_ACCOUNT_REC_TYPE            {get; private set;} 
    public static final String BU_ACCOUNT_REC_TYPE_ID         {get; private set;}
    public static final String CUSTOMER_ACCOUNT_REC_TYPE      {get; private set;}
    public static final String CUSTOMER_ACCOUNT_REC_TYPE_ID   {get; private set;}
    
    public static final String PARENT_INFRA_DEV_QE_ENVIRONMENT_REC_TYPE_ID            {get; private set;} 
    public static final String PARENT_INFRA_IT_ENVIRONMENT_REC_TYPE_ID                {get; private set;}
    public static final String PARENT_INFRA_PROD_ENVIRONMENT_REC_TYPE_ID              {get; private set;}
    public static final String PARENT_SERVICE_ENVIRONMENT_REC_TYPE_ID                 {get; private set;}
    public static final String OTHER_ENVIRONMENT_REC_TYPE_ID                          {get; private set;}
    
    public static final String PARENT_INFRA_DEV_QE_ENVIRONMENT_NAME                   {get; private set;} 
    public static final String PARENT_INFRA_IT_ENVIRONMENT_NAME                       {get; private set;}
    public static final String PARENT_INFRA_PROD_ENVIRONMENT_NAME                     {get; private set;}
    public static final String PARENT_SERVICE_ENVIRONMENT_NAME                        {get; private set;}
    public static final String OTHER_ENVIRONMENT_NAME                                 {get; private set;}
    
    public static final String TMM_QUEUE_NAME                 {get; private set;} 
    public static final Id TMM_QUEUE_ID                       {get; private set;}
    
    public static final Map<String, String> SCS_TO_TMM_STATUS {get; private set;}
    public static final Map<String, Integer> TMM_STATUS_SEQ {get; private set;}
    
    private static Id currentAdminUserId;
    private static Boolean currentUserIsAdmin;
    public static Boolean Current_User_Is_Admin {
        get {
            // only get if the first time or the current user has changed
            Boolean isAdmin = false;
            if (currentAdminUserId == null || currentAdminUserId != UserInfo.getUserId()) {
                currentAdminUserId = UserInfo.getUserId();              
                for (PermissionSetAssignment pa : [SELECT AssigneeId FROM PermissionSetAssignment WHERE AssigneeId = :UserInfo.getUserId() AND PermissionSet.Label = 'TM Admin']) {
                    isAdmin = true;
                } 
            // else return current value for property
            } else {
                isAdmin = currentUserIsAdmin;
            } 
            currentUserIsAdmin = isAdmin;        
            return isAdmin;
        }        
            private set;
        }
    
    public static final Set<String> VALID_OBJECTIVE_STATUS {get; private set;}
    public static final Set<String> VALID_VALIDATOR_STATUS {get; private set;}
    public static final Set<String> VALID_IMPLEMENTER_STATUS {get; private set;}

    public static final Id PERM_SET_VALIDATOR_ID           {get; private set;} 
    public static final Id PERM_SET_IMPLEMENTER_ID         {get; private set;} 
    public static final Id PERM_SET_ADMIN_ID               {get; private set;} 
        
    static {
        GUEST_USER_TYPE = 'Guest';
        COMMUNITY_SITE_TYPE = 'ChatterNetwork';
        BU_ENVIRONMENT_TYPE = 'Business Unit';
        BU_ACCOUNT_REC_TYPE = 'Salesforce Business Unit';
        CUSTOMER_ACCOUNT_REC_TYPE = 'Salesforce Customer';
        TMM_QUEUE_NAME = 'TMM_Team';
        
        SCS_TO_TMM_STATUS = new Map<String, String>{'Red' => 'Failed', 'Yellow' => 'Working: In-Progress', 'Green' => 'Completed', '' => 'Not Started'};
        TMM_STATUS_SEQ = new Map<String, Integer>{'Failed' => 3, 'Working: In-Progress' => 2, 'Completed' => 1};

        TMM_QUEUE_ID = [Select Id From Group 
                        where DeveloperName = :TMM_QUEUE_NAME and Type = 'Queue'].Id;

        BU_ACCOUNT_REC_TYPE_ID = [Select Id From RecordType 
                                  where Name = :BU_ACCOUNT_REC_TYPE 
                                  and SobjectType = 'Account'
                                  order by NamespacePrefix DESC limit 1].Id;        
        
        CUSTOMER_ACCOUNT_REC_TYPE_ID = [Select Id From RecordType 
                                  where Name = :CUSTOMER_ACCOUNT_REC_TYPE
                                  and SobjectType = 'Account'
                                  order by NamespacePrefix DESC limit 1].Id;
        
        PARENT_INFRA_DEV_QE_ENVIRONMENT_NAME = 'Parent_Infrastructure_Dev_QE';
        PARENT_INFRA_IT_ENVIRONMENT_NAME = 'Parent_Infrastructure_IT';
        PARENT_INFRA_PROD_ENVIRONMENT_NAME = 'Parent_Infrastructure_Production';
        PARENT_SERVICE_ENVIRONMENT_NAME = 'Parent_Service';
        OTHER_ENVIRONMENT_NAME = 'Parent_Other';
        
        PARENT_INFRA_DEV_QE_ENVIRONMENT_REC_TYPE_ID = [SELECT Id FROM RecordType WHERE sObjectType = 'IRCloud__Environment__c' AND DeveloperName = :PARENT_INFRA_DEV_QE_ENVIRONMENT_NAME 
                                                       order by NamespacePrefix DESC limit 1].Id;
        PARENT_INFRA_IT_ENVIRONMENT_REC_TYPE_ID = [SELECT Id FROM RecordType WHERE sObjectType = 'IRCloud__Environment__c' AND DeveloperName = :PARENT_INFRA_IT_ENVIRONMENT_NAME 
                                                   order by NamespacePrefix DESC limit 1].Id;
        PARENT_INFRA_PROD_ENVIRONMENT_REC_TYPE_ID = [SELECT Id FROM RecordType WHERE sObjectType = 'IRCloud__Environment__c' AND DeveloperName = :PARENT_INFRA_PROD_ENVIRONMENT_NAME 
                                                     order by NamespacePrefix DESC limit 1].Id;
        PARENT_SERVICE_ENVIRONMENT_REC_TYPE_ID = [SELECT Id FROM RecordType WHERE sObjectType = 'IRCloud__Environment__c' AND DeveloperName = :PARENT_SERVICE_ENVIRONMENT_NAME 
                                                  order by NamespacePrefix DESC limit 1].Id;
        OTHER_ENVIRONMENT_REC_TYPE_ID = [SELECT Id FROM RecordType WHERE sObjectType = 'IRCloud__Environment__c' AND DeveloperName = :OTHER_ENVIRONMENT_NAME 
                                         order by NamespacePrefix DESC limit 1].Id;

        for (PermissionSet ps : [SELECT Id, Label FROM PermissionSet WHERE Label IN ('TM Validator','TM Implementer','TM Admin')]) {
            if (ps.Label == 'TM Validator') PERM_SET_VALIDATOR_ID = ps.Id;
            else if (ps.Label == 'TM Implementer') PERM_SET_IMPLEMENTER_ID = ps.Id; 
            else if (ps.Label == 'TM Admin') PERM_SET_ADMIN_ID = ps.Id; 
        }
        
        VALID_IMPLEMENTER_STATUS = new Set<String>{'Working: In-Progress', 'Working: Ready'};
        VALID_VALIDATOR_STATUS = new Set<String>{'Working: In-Progress', 'Working: Validating', 'Failed', 'Completed'};        
        VALID_OBJECTIVE_STATUS = new Set<String>();
        for (Schema.PicklistEntry pl : TM_Objective__c.Status__c.getDescribe().getPicklistValues()) {
            VALID_OBJECTIVE_STATUS.add(pl.getLabel());  
        }
        
    }

}
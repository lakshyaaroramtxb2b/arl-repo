/**
 * @author Lakshya Arora
 * @email lakshya.arora@mtxb2b.com
 * @description Class for unit testing the KARL_MultiSelectLookupController
 */
@isTest
public with sharing class KARL_MultiSelectLookupControllerTest {
    /**
     * @author Lakshya Arora
     * @email lakshya.arora@mtxb2b.com
     * @description Method will unit test the getRecordsAsPerObjectAndFieldList Fucntionality 
     * by validating the correct number of records
     */
    @isTest
    private static void getRecordsAsPerObjectAndFieldListTest(){
        User opsAdminUser = ARL_TestDataFactory.createOpsAdmin();
        System.runAs(opsAdminUser){
            Request_Item__c reqItemObj = ARL_TestDataFactory.createRequestItem();
            insert reqItemObj;
            Control__c controlObj = ARL_TestDataFactory.createControl();
            insert controlObj;
            Control_Scope__c controlScopeObj = ARL_TestDataFactory.createControlScope();
            controlScopeObj.Control_Name__c = controlObj.Id;
            insert controlScopeObj;
            Control_Test__c controlTest = ARL_TestDataFactory.createControlTest(controlScopeObj.Id);
            controlTest.KARL_Control_Scope_Number_Indexed__c = 'Index';
            insert controlTest;
            Request_Item_Control__c itemControl = ARL_TestDataFactory.createRequestItemControl(reqItemObj.Id,controlTest.Id);
            insert itemControl;
            List<KARL_MultiSelectLookupController.SObJectResult> recordsDataList = KARL_MultiSelectLookupController.getResults('Control_Test__c','AC',new List<String>());
            System.assert(!recordsDataList.isEmpty());
            Area_Requirement__c reqArea = ARL_TestDataFactory.createAreaRequirement('ref','SOC 1');
            insert reqArea;
            Request_Item_Area__c reqItemArea = ARL_TestDataFactory.mapAreaRequirement(reqItemObj.Id,reqArea.Id);
            insert reqItemArea;
            List<KARL_MultiSelectLookupController.SObJectResult> emptyRecordsDataList = KARL_MultiSelectLookupController.getResults('Area_Requirement__c','AR',new List<String>{recordsDataList[0].recId});
            System.assert(!emptyRecordsDataList.isEmpty()); 
        }
    }
}
public class CSIRT_DailyDigest_SecReports {
	List<Case> reports;  
    public List<Case> getReports(){
        if(reports == null)
        	reports = [SELECT Id, Owner.Name, CaseNumber, Subject, Days_Open__c, Status, IRCloud__Case_Summary__c FROM Case WHERE IsClosed = False AND RecordTypeId IN ('0123A000001dxqh','0123A000001dxqg','0123A000000ATCf') AND Days_Open__c > 4 AND CSIRT_Test_Alert__c = False ORDER BY Days_Open__c DESC];
        return reports;
    }

}
@isTest
public class PRT_V2MOMChangeRequestCtrlTest {
    @TestSetup
    static void makeData(){
        Portfolio__c portfolio = new Portfolio__c();
        portfolio.Name = 'Security GRC';
        INSERT portfolio;

        V2MOM_Change_Request__c changeRequest = new V2MOM_Change_Request__c();
        changeRequest.Status__c = 'New';
        changeRequest.Portfolio__c = portfolio.Id;
        INSERT changeRequest;
        
        V2MOM_Change_Request__c changeRequest2 = new V2MOM_Change_Request__c();
        changeRequest2.Status__c = 'Draft';
        changeRequest2.Portfolio__c = portfolio.Id;
        INSERT changeRequest2;

        Method_and_Measure_Change__c measureChange = new Method_and_Measure_Change__c();
        measureChange.Change_Request__c = changeRequest.Id;
        measureChange.Detailed_Justification__c = 'Test Detailed Justification';
        measureChange.Measure_Comment__c = 'Test Comment';
        measureChange.Measure_Name__c = 'Test Measure Name';
        measureChange.Measure_Target_Value__c = 12.00;
        measureChange.Target_Date_of_Change__c = Date.today().addDays(6);
        INSERT measureChange;

        Method_and_Measure_Change__c methodChange = new Method_and_Measure_Change__c();
        methodChange.Change_Request__c = changeRequest.Id;
        methodChange.Detailed_Justification__c = 'Test Detailed Justification';
        methodChange.Method_Description__c = 'Test Description';
        methodChange.Method_Name__c = 'Test Method Name';
        methodChange.Target_Date_of_Change__c = Date.today().addDays(6);
        INSERT methodChange;
    }

    @isTest 
    static void securityGRCRecordTest() {
        PRT_V2MOMChangeRequestCtrl.RecordsData data = PRT_V2MOMChangeRequestCtrl.securityGRCRecord();
        System.assertEquals('Security GRC', data.name);
    }

    @isTest
    static void findRecordsTest() {
        List<PRT_V2MOMChangeRequestCtrl.RecordsData> data = PRT_V2MOMChangeRequestCtrl.findRecords('Portfolio__c', 'Name', 'GRC', '', '');
        System.assertEquals(1, data.size());
        
        List<PRT_V2MOMChangeRequestCtrl.RecordsData> data2 = PRT_V2MOMChangeRequestCtrl.findRecords('Portfolio__c', 'Name', '', '', '');
        System.assertEquals(0, data2.size());
    }

    @isTest
    static void createChangeRequestTest() {
        List<Portfolio__c> portfolioList = new List<Portfolio__c>([SELECT Id, Name FROM Portfolio__c]);
        List<Map<String, String>> methodOrMeasureData = new List<Map<String, String>>();
        Map<String, String> data = new Map<String, String>();
        data.put('detailedJustification', 'detailedJustification');
        data.put('portfolioId', ''+portfolioList.get(0).Id);
        data.put('isMethod', 'false');
        data.put('targetDateOfChange', '2020-04-02');
        data.put('measureTargetValue', '12');
        data.put('updatedDescOrComm', 'Test Comment');
        data.put('methodOrMeasureName', 'Test Measure');
        
        Map<String, String> data2 = new Map<String, String>();
        data2.put('portfolioId', ''+portfolioList.get(0).Id);
        data2.put('isMethod', 'true');
        data2.put('targetDateOfChange', '2020-04-02');
        data2.put('detailedJustification', 'detailedJustification');
        data2.put('updatedDescOrComm', 'Test Description');
        data2.put('methodOrMeasureName', 'Test Method');
        
        methodOrMeasureData.add(data);
        methodOrMeasureData.add(data2);
        String id = PRT_V2MOMChangeRequestCtrl.createChangeRequest(methodOrMeasureData);  
        System.assertNotEquals('', id);  
    }

    @isTest
    static void getMethodAndMeasureRecordsTest() {
        List<V2MOM_Change_Request__c> changeRequest = new List<V2MOM_Change_Request__c>([SELECT Id, Status__c, Portfolio__c 
                                                                                        FROM V2MOM_Change_Request__c WHERE Status__c = 'New']);
        List<PRT_V2MOMChangeRequestCtrl.MethodAndMeasureData> data = PRT_V2MOMChangeRequestCtrl.getMethodAndMeasureRecords(changeRequest.get(0).Id);
        System.assertEquals(false, data.get(0).isMethod);
        
        List<V2MOM_Change_Request__c> changeRequest2 = new List<V2MOM_Change_Request__c>([SELECT Id, Status__c, Portfolio__c 
                                                                                        FROM V2MOM_Change_Request__c WHERE Status__c = 'Draft']);
        List<PRT_V2MOMChangeRequestCtrl.MethodAndMeasureData> data2 = PRT_V2MOMChangeRequestCtrl.getMethodAndMeasureRecords(changeRequest2.get(0).Id);
       // System.assertEquals(false, data2.get(0).isMethod);
    }

    @isTest 
    static void updateMethodAndMeasureChangeTest() {
        List<Portfolio__c> portfolioList = new List<Portfolio__c>([SELECT Id, Name FROM Portfolio__c]);
        List<V2MOM_Change_Request__c> changeRequest = new List<V2MOM_Change_Request__c>([SELECT Id, Status__c, Portfolio__c 
                                                                                        FROM V2MOM_Change_Request__c WHERE Status__c = 'New']);
        List<Method_and_Measure_Change__c> measureChangeList = new List<Method_and_Measure_Change__c>([SELECT Id FROM Method_and_Measure_Change__c
                                                                                                        WHERE Measure_Name__c LIKE '%Test%']);
        List<Method_and_Measure_Change__c> methodChangeList = new List<Method_and_Measure_Change__c>([SELECT Id FROM Method_and_Measure_Change__c
                                                                                                        WHERE Method_Name__c LIKE '%Test%']);                                                                                                                
        
        List<Map<String, String>> methodOrMeasureData = new List<Map<String, String>>();
        Map<String, String> measureData = new Map<String, String>();
        measureData.put('detailedJustification', 'detailedJustification');
        measureData.put('portfolioId', ''+portfolioList.get(0).Id);
        measureData.put('isMethod', 'false');
        measureData.put('targetDateOfChange', '2020-04-20');
        measureData.put('measureTargetValue', '12');
        measureData.put('updatedDescOrComm', 'Test Comment');
        measureData.put('recordId', String.valueOf(measureChangeList.get(0).Id));
        measureData.put('methodOrMeasureName', 'Test Measure');

        Map<String, String> methodData = new Map<String, String>();
        methodData.put('detailedJustification', 'detailedJustification');
        methodData.put('portfolioId', ''+portfolioList.get(0).Id);
        methodData.put('isMethod', 'true');
        methodData.put('targetDateOfChange', '2020-04-20');
        methodData.put('updatedDescOrComm', 'Test Desc');
        methodData.put('recordId', String.valueOf(methodChangeList.get(0).Id));
        methodData.put('methodOrMeasureName', 'Test Method');

        methodOrMeasureData.add(methodData);
        methodOrMeasureData.add(measureData);

        String result = PRT_V2MOMChangeRequestCtrl.updateMethodAndMeasureChange(methodOrMeasureData, String.valueOf(changeRequest.get(0).Id));
        System.assertEquals('2', result);
    }

    @isTest 
    static void getMethodAndTheirMeasuresTest() {
        V2MOM_c__x v2mom = new V2MOM_c__x(
            ExternalId = 'v101'/*,
            Name__c = 'Test V2MOM'*/
        );
        
        V2MOM_Method_c__x method = new V2MOM_Method_c__x(
            ExternalId = 'method101'/*,
            Name__c = 'Test Method'*/
        );
        PRT_V2MOMChangeRequestCtrl.mockedMethodList.add(method);
        
        Measure_c__x measure = new Measure_c__x(
            ExternalId = 'measure101'/*,
            MeasureName_c__c = 'Test Measure'*/
        );
        PRT_V2MOMChangeRequestCtrl.mockedMeasureList.add(measure);

        List<PRT_V2MOMChangeRequestCtrl.MethodAndTheirMeasures> result = PRT_V2MOMChangeRequestCtrl.getMethodAndTheirMeasures(v2mom.Id);
        System.debug('Result: '+result);
        System.assertEquals(null, result.get(0).methodName);
    }

    @isTest 
    static void deleteMethodOrMeasureChangeTest() {
        List<Method_and_Measure_Change__c> measureChangeList = new List<Method_and_Measure_Change__c>([SELECT Id FROM Method_and_Measure_Change__c
                                                                                                        WHERE Measure_Name__c LIKE '%Test%']);
        PRT_V2MOMChangeRequestCtrl.deleteMethodOrMeasureChange(String.valueOf(measureChangeList.get(0).Id));
        List<Method_and_Measure_Change__c> changeList = new List<Method_and_Measure_Change__c>([SELECT Id FROM Method_and_Measure_Change__c]);
        System.assertEquals(1, changeList.size());
    }

    @isTest 
    static void getRelatedMethodTest() {
        V2MOM_Method_c__x method = new V2MOM_Method_c__x(
            ExternalId = 'method101'/*,
            Name__c = 'Test Method'*/
        );
        PRT_V2MOMChangeRequestCtrl.mockedMethodList.add(method);
        
        Measure_c__x measure = new Measure_c__x(
            ExternalId = 'measure101'/*,
            MeasureName_c__c = 'Test Measure'*/
        );
        PRT_V2MOMChangeRequestCtrl.mockedMeasureList.add(measure);
        

        Map<String, String> result = PRT_V2MOMChangeRequestCtrl.getRelatedMethod(method.Id);
        System.assertEquals(2, result.size());
    }
}
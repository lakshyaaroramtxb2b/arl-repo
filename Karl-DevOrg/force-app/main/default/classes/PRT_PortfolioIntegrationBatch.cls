/*created batch class for mapping GUS portfolio external Record to security Org portfolio
* Created By - Swarnima Singh Mandhata
* Modified By - Swarnima S Mandhata Line-22 added filter criteria on 05/05/2020.
-* date - 17-01-2020
* 
*/
global class PRT_PortfolioIntegrationBatch implements Database.Batchable<sObject>,Schedulable{
    public PRT_PortfolioIntegrationBatch() {
    }
    @TestVisible
    private static list<PPM_Portfolio_c__x> mockallGusPortfolioList = new list<PPM_Portfolio_c__x>();
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        
        if(Test.isRunningTest()){
            return Database.getQueryLocator([SELECT Id FROM User LIMIT 1]);    
        }else{
            String portfolioExternalId = System.Label.PRT_FY_21_Security_GRC;

            String query = 'SELECT Id, ExternalId,Name__c, LastModifiedDate__c,Portfolio_Goals_c__c,Portfolio_Health_c__c, Portfolio_Health_Color_c__c, Portfolio_Manager_c__c, Portfolio_Owner_c__c,Portfolio_Summary_c__c,OwnerId__c FROM PPM_Portfolio_c__x';
            query+= ' WHERE LastModifiedDate__c >=LAST_N_DAYS:1';
            query +=  ' AND ExternalId =\'' + portfolioExternalId +'\'';
             
            return Database.getQueryLocator(query);
        }
    }
    
    global void execute(Database.BatchableContext bc, List<sObject> gusPortfolioRecordList){
        System.debug('>>>>> ' + gusPortfolioRecordList);
        if(Test.isRunningTest()){
            gusPortfolioRecordList = mockallGusPortfolioList;
        }
        Datetime fiveMinsAgo = Datetime.now().addMinutes(-5);
        
        List<Portfolio__c> portfolioList = new List<Portfolio__c>();
        if(!gusPortfolioRecordList.isEmpty()){
            Set<Id> externalPortfolioIdSet = new Set<Id>();
            Set<Id> gusRecUserIdSet = new Set<Id>();
            for(PPM_Portfolio_c__x gusPortfolio :(List<PPM_Portfolio_c__x>)gusPortfolioRecordList){
                // Need to change the field names here
                if(gusPortfolio.LastModifiedDate__c >=fiveMinsAgo || Test.isRunningTest()){
                    externalPortfolioIdSet.add(gusPortfolio.ExternalId);
                    SYstem.debug('gusPortfolio.ExternalId --> '+ gusPortfolio.ExternalId);
                }
                if(gusPortfolio.OwnerId__c!=null || test.isRunningTest()){
                    gusRecUserIdSet.add(gusPortfolio.OwnerId__c);
                }
                if(gusPortfolio.Portfolio_Manager_c__c!=null || test.isRunningTest()){
                    gusRecUserIdSet.add(gusPortfolio.Portfolio_Manager_c__c);
                }
            }
            Map<Id,String> gusIdVsEmployeeIdMap = new Map<Id,String>();
            Map<String,ID> employeeIDVsUserIDMap = new Map<String,Id>();

            for(User__x userRec : [SELECT ID, ExternalID, EmployeeNumber__c FROM User__x WHERE ExternalID in: gusRecUserIdSet]){
                if(userRec.EmployeeNumber__c!=null || Test.isRunningTest()){
                    gusIdVsEmployeeIdMap.put(userRec.ExternalId, userRec.EmployeeNumber__c);
                    employeeIDVsUserIDMap.put(userRec.EmployeeNumber__c, null);
                    
                }
            }
         

            for(User userRec : [SELECT ID, EmployeeNumber From User WHERE EmployeeNumber IN :employeeIDVsUserIDMap.keySet()]){
                if(employeeIDVsUserIDMap!=null && employeeIDVsUserIDMap.containsKey(userRec.EmployeeNumber)){
                    employeeIDVsUserIDMap.put(userRec.EmployeeNumber, userRec.Id);
                }
            }
            Map<String,Portfolio__c> gusrecordToPortfolioMap = new Map<String,Portfolio__c>();
            for(Portfolio__c portfolioRec :[SELECT ID,Name,Portfolio_Goals__c,GUS_Portfolio__c,Portfolio_Health__c,Portfolio_Manager__r.EmployeeNumber,
                                            Portfolio_Owner__r.EmployeeNumber,Portfolio_Summary__c 
                                            FROM Portfolio__c 
                                            WHERE GUS_Portfolio__c  IN : externalPortfolioIdSet]){
                                                if(Test.isRunningTest()){
                                                    gusrecordToPortfolioMap.put('EX101',portfolioRec);
                                                }else{
                                                    gusrecordToPortfolioMap.put(portfolioRec.GUS_Portfolio__c,portfolioRec);
                                                }
                                            }
            System.debug('>>>> external Ids >>>' + externalPortfolioIdSet);
            List<Portfolio__c> portfolioInsertList = new List<Portfolio__c>();
            for(PPM_Portfolio_c__x gusObject : (List<PPM_Portfolio_c__x>)gusPortfolioRecordList){
                if(gusObject.LastModifiedDate__c >=fiveMinsAgo || Test.isRunningTest()){
                    Portfolio__c portfolioRec = new Portfolio__c();
                    if(gusrecordToPortfolioMap.containsKey(gusObject.ExternalId)){
                        portfolioRec = gusrecordToPortfolioMap.get(gusObject.ExternalId);
                    }else{                        
                        portfolioRec.GUS_Portfolio__c = gusObject.ExternalId;
                    }                        
                    portfolioRec.Portfolio_Health__c  = gusObject.Portfolio_Health_c__c;
                    portfolioRec.Portfolio_Goals__c = gusObject.Portfolio_Goals_c__c;
                    portfolioRec.Name = gusObject.Name__c;
                    portfolioRec.Portfolio_Summary__c = gusObject.Portfolio_Summary_c__c; 
                    System.debug('Portfolio_Summary__c -> '+ portfolioRec.Portfolio_Summary__c);
                    portfolioRec.Portfolio_Active__c = true;
                    System.debug('gusIdVsEmployeeIdMap' + gusIdVsEmployeeIdMap);
                    System.debug('employeeIDVsUserIDMap' + employeeIDVsUserIDMap);
                    if(gusIdVsEmployeeIdMap.containsKey(gusObject.Portfolio_Manager_c__c) && employeeIDVsUserIDMap.containsKey(gusIdVsEmployeeIdMap.get(gusObject.Portfolio_Manager_c__c)))
                        portfolioRec.Portfolio_Manager__c = employeeIDVsUserIDMap.get(gusIdVsEmployeeIdMap.get(gusObject.Portfolio_Manager_c__c));
                    if(gusIdVsEmployeeIdMap.containsKey(gusObject.Portfolio_Owner_c__c) && employeeIDVsUserIDMap.containsKey(gusIdVsEmployeeIdMap.get(gusObject.Portfolio_Owner_c__c)))
                        portfolioRec.Portfolio_Owner__c = employeeIDVsUserIDMap.get(gusIdVsEmployeeIdMap.get(gusObject.Portfolio_Owner_c__c));
                    portfolioList.add(portfolioRec);}
            }
        }
        System.debug('>>> list to be update >>>>' + portfolioList);
        if(!portfolioList.isEmpty()){
            upsert portfolioList;                
        }
    }
    
    global void finish(Database.BatchableContext bc){
        PRT_PortfolioIntegrationBatch batch = new PRT_PortfolioIntegrationBatch();
        if(!Test.isRunningTest()){
            System.scheduleBatch(batch, 'PRT_PortfolioIntegrationBatch',4);
        }
        
        
    }
    
    public void execute(SchedulableContext SC) {
        database.executebatch(new PRT_PortfolioIntegrationBatch());
    } 
}
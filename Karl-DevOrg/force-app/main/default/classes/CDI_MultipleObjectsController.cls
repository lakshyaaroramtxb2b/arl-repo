public with sharing class CDI_MultipleObjectsController {     
    @AuraEnabled
    public static void saveDataStoreContacts(List<CDI_GUS_User__c> gususerList, Id parentRecordId){
        System.debug(gususerList);
        CDI_Data_Store__c datastore=[Select Name from CDI_Data_Store__c where Id=:parentRecordId];
        List<CDI_Data_Store_Contact__c> dsclist=new List<CDI_Data_Store_Contact__c>();
        for(CDI_GUS_User__c gususer:gususerList)
        {
            System.debug('GUS User Data '+gususer.Name);
            String[] gususervalues=gususer.Name.split(',');
            CDI_Data_Store_Contact__c dsc=new CDI_Data_Store_Contact__c();
            dsc.Data_Store__c=parentRecordId;
            dsc.Contact__r=new CDI_GUS_User__c(External_Id__c=gususervalues[1]);
            dsclist.add(dsc);
        }
        insert dsclist;
    }
    @AuraEnabled
    public static void saveDataStoreServices(List<CDI_Data_Store_Service__c> dssList, Id parentRecordId){
        System.debug(dssList);
        for(CDI_Data_Store_Service__c dss:dssList)
        {
            dss.Data_Store__c=parentRecordId;
        }
        Insert dssList;
    }
    @AuraEnabled
    public static void saveDatastoreType(String datastoretypeId,Id datastoreId)
    {
        System.debug('Hello '+datastoretypeId);
        if(datastoretypeId != null)
        {
            try{
            CDI_Data_Store__c datastore =new CDI_Data_Store__c();
            datastore.Id=datastoreId;
            datastore.Datastore_Type__c=datastoretypeId;
            update datastore;
            System.debug('No exception');                
            }
            catch(Exception e)
            {
                e.getStackTraceString();
                System.debug('Inside exception');
                System.debug(e.getStackTraceString());
                List<CDI_Datastore_Type__c> datastoretypes=[Select Id from CDI_Datastore_Type__c where Name=:datastoretypeId];
                System.debug(datastoretypes);
                CDI_Data_Store__c datastore =new CDI_Data_Store__c();
                datastore.Id=datastoreId;
                datastore.Datastore_Type__c=datastoretypes[0].Id;
                update datastore;     
            }
        }
    }
}
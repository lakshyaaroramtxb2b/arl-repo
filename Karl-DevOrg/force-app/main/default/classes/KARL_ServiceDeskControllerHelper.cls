/**
 * @author Lakshya Arora
 * @email lakshya.arora@mtxb2b.com
 * @description Handle Without Sharing queries
 * @note -> Without Sharing because as Default External access of cycle arl items and Audit scope reports are private,
 * GRC Community users need those records to populate the GRC Assignee User on newly Service Desk Record. If we make it as with Sharing,
 * GRC Community users won't be able to access those records. Thus grc assignee will always be blank.
 */
public without sharing class KARL_ServiceDeskControllerHelper {
    /**
    * @author Lakshya Arora
    * @email lakshya.arora@mtxb2b.com
    * @description get Audit Scope records by KARL Scope field
    * param >karlScope -> KARL_Scope__c value
    */
    public static List<KARL_Audit_Scope_Master_Data__c> getAuditScopeByKARLScope(String karlScope){
        return  [SELECT Id,KARL_BU_Lead__c 
                    FROM KARL_Audit_Scope_Master_Data__c 
                    WHERE KARL_Scope__c = :karlScope
                    WITH SECURITY_ENFORCED
                    LIMIT 1];
    }

    /**
    * @author Lakshya Arora
    * @email lakshya.arora@mtxb2b.com
    * @description get Cycle ARL Item List By Id
    * param > cycleArlItemId -> Id of Cycle ARL Item record
    */
    public static List<Cycle_Request_Item__c> getCycleARLItemById(String cycleArlItemId){
        return  [SELECT Id,Request__r.KARL_GRC_Assignee__c,
                    Audit_Cycle_Coverage__r.Cycle_Audit_Report_Items__r.Audit_Reports_Master_Data__r.KARL_Audit_Scope__r.KARL_BU_Lead__c
                    FROM Cycle_Request_Item__c 
                    WHERE Id = :cycleArlItemId 
                    WITH SECURITY_ENFORCED];
    }

    
}
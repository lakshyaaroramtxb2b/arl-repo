public with sharing class GusSLATrackingTriggerHelper {
    
    public static void run(){
        if( Trigger.isBefore && Trigger.isUpdate ){
            updateOldStatus((List<KARL_GUS_SLA_Tracking__c >)Trigger.new , (Map<Id, KARL_GUS_SLA_Tracking__c>)Trigger.oldMap);
        }
    }
    
    public static void updateOldStatus(List<KARL_GUS_SLA_Tracking__c > newList, Map<Id,KARL_GUS_SLA_Tracking__c> oldMap){
        System.debug('test1');
        for(KARL_GUS_SLA_Tracking__c slaTrack: newList){
             System.debug('test2');
            if(String.isNotEmpty(slaTrack.End_Status__c)) {
                if(Trigger.isInsert ){
                    System.debug('test3');
                    slaTrack.KARL_End_Date__c = datetime.now();
                }
                if(Trigger.isUpdate && slaTrack.End_Status__c != oldMap.get(slaTrack.id).End_Status__c){
                    System.debug('test4');
                    slaTrack.Start_Date__c = slaTrack.KARL_End_Date__c;
                    slaTrack.KARL_End_Date__c = datetime.now();
                    slaTrack.Start_Status__c = oldMap.get(slaTrack.id).End_Status__c;
                }                
            }
        }
    }
    

}
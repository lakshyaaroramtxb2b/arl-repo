/*created batch class for mapping GUS Project external Record to security Org Project
* Created By - Swarnima Singh Mandhata
* date - 06-12-2019
* 
*/
public class PRT_ProjectBatch implements Database.Batchable<sObject>,Schedulable{
    @TestVisible
    private static list<PPM_Project_c__x> mockallGusProjectList = new list<PPM_Project_c__x>();
    
    public Database.QueryLocator start(Database.BatchableContext BC) {
        
        if(Test.isRunningTest()){
            return Database.getQueryLocator([SELECT Id FROM User LIMIT 1]);    
        }else{
            return Database.getQueryLocator([SELECT Id FROM PPM_Project_c__x 
                                             Where LastModifiedDate__c >=LAST_N_DAYS:1]);    
        }
    }
    public void execute(Database.BatchableContext bc, List<sObject> ProjectRecordList){
        List<PPM_Project_c__x> gusProjectRecordList = new List<PPM_Project_c__x>();
        if(Test.isRunningTest()){
            gusProjectRecordList = mockallGusProjectList;
        }else{
            gusProjectRecordList = [SELECT Id, ExternalId,Name__c, LastModifiedDate__c, Actual_End_Date_c__c, 
            Actual_Start_Date_c__c, Description_c__c, Project_Health_c__c, 
            Build_Planned_End_Date_c__c,Business_Case_c__c, Customer_POC_c__c, Delivery_Team_Priority_c__c, Project_Deliverables_c__c, 
            Project_Health_Comments_c__c 
            FROM PPM_Project_c__x 
            Where ID IN :ProjectRecordList];
        }
        Datetime oneHourBack = Datetime.now().addMinutes(-5);
        if(!gusProjectRecordList.isEmpty()){
            Set<Id> externalProjectIdSet = new Set<Id>();
            for(PPM_Project_c__x gusProject :(List<PPM_Project_c__x>)gusProjectRecordList){
                // Need to change the field names here
                if(gusProject.LastModifiedDate__c >=oneHourBack || Test.isRunningTest()){
                    externalProjectIdSet.add(gusProject.ExternalId);
                }
            }
            Map<String,Project__c> gusrecordToProjectMap = new Map<String,Project__c>();
            for(Project__c project :[SELECT Id,Name,Projected_Date_of_Completion__c , Project_Start_Date__c, Status__c,Gus_Project__c, Delivery_Team_Priority__c, Customer_POC__c, 
                                     Project_Health_Comments__c, Deliverables__c, Project_Overview_Purpose__c
                                     FROM Project__c 
                                     WHERE Gus_Project__c  IN : externalProjectIdSet]){
                                         if(Test.isRunningTest()){
                                             gusrecordToProjectMap.put(project.Gus_Project__c,project);
                                         }else{
                                             gusrecordToProjectMap.put(project.Gus_Project__c,project);
                                         }
                                     }
            System.debug('>>>> external Ids >>>' + externalProjectIdSet);
            List<Project__c> projectUpdateList = new List<Project__c>();
            for(PPM_Project_c__x gusObject : (List<PPM_Project_c__x>)gusProjectRecordList){
                if(gusObject.LastModifiedDate__c >=oneHourBack || Test.isRunningTest()){
                    if(gusrecordToProjectMap.containsKey(gusObject.ExternalId)){
                        Project__c project =  gusrecordToProjectMap.get(gusObject.ExternalId);
                        if(project.Projected_Date_of_Completion__c  != gusObject.Actual_End_Date_c__c || 
                           project.Project_Start_Date__c != gusObject.Actual_Start_Date_c__c || project.Name != gusObject.Name__c 
                           || project.Project_Overview_Purpose__c != gusObject.Business_Case_c__c || project.Customer_POC__c != gusObject.Customer_POC_c__c 
                           || project.Delivery_Team_Priority__c != gusObject.Delivery_Team_Priority_c__c || project.Deliverables__c != gusObject.Project_Deliverables_c__c
                           || project.Project_Health_Comments__c != gusObject.Project_Health_Comments_c__c) {
                               project.Projected_Date_of_Completion__c  = gusObject.Actual_End_Date_c__c;
                               project.Project_Start_Date__c = gusObject.Actual_Start_Date_c__c;
                               project.Name = gusObject.Name__c;
                               project.Project_Overview_Purpose__c = gusObject.Business_Case_c__c;
                               project.Customer_POC__c = gusObject.Customer_POC_c__c;
                               project.Delivery_Team_Priority__c = gusObject.Delivery_Team_Priority_c__c;
                               project.Deliverables__c = gusObject.Project_Deliverables_c__c;
                               project.Project_Health_Comments__c = gusObject.Project_Health_Comments_c__c;
                               //gusObject.Project_Health_c__c
                               
                               //project.Description__c = gusObject.Description_c__c;
                               //project.Status__c = gusObject.Project_Health_c__c;
                               //project.Due_Date__c = gusObject.Planned_End_Date_c__c;
                               projectUpdateList.add(project);
                           }
                    }
                }
            }
            System.debug('>>> list to be update >>>>' + projectUpdateList);
            
            update projectUpdateList;
        }
    }
    public void finish(Database.BatchableContext bc){
        PRT_ProjectBatch batch = new PRT_ProjectBatch();
        if(!Test.isRunningTest()){
            System.scheduleBatch(batch, 'PRT_ProjectBatch',4);   
        }
    }
    public void execute(SchedulableContext SC) {
       database.executebatch(new PRT_ProjectBatch());
    } 
    
    
}
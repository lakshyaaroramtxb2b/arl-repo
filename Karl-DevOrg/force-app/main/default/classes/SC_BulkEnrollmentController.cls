/**
 * @author: ralph@callaway.cloud
 * @description: bulk enrollment page
 */
public class SC_BulkEnrollmentController {

    public SC_Enroller.Options opts { get; set; }
    public String emails { get; set; }
    public Id emailTemplateId { get; set; }
    public List<SelectOption> emailTemplates { get; set; }
    public Training_Course_Taken__c dateInput { get; set; }
    public Boolean submitted { get; set; }

    public SC_BulkEnrollmentController(ApexPages.StandardController controller) {
        opts = new SC_Enroller.Options();
        opts.isMandatory = true;
        opts.sendNotification = true;
        opts.courseId = controller.getId();
        emailTemplateId = defaultEmailTemplate();
        emailTemplates = loadTemplates(SC_Constants.SC_EMAIL_FOLDER_ID);
        submitted = false;
        dateInput = new Training_Course_Taken__c(Due_Date__c = Date.today().addDays(30));
    }

    public void submit() {
        opts.dueDate = (opts.isMandatory) ? dateInput.Due_Date__c : null;
        opts.templateId = (opts.sendNotification) ? emailTemplateId : null;
        String courseOrgWideId = [SELECT Org_Wide_Email_Id__c FROM Training_Course__c 
                                  WHERE Id = :opts.courseId].Org_Wide_Email_Id__c;
        if (String.isBlank(courseOrgWideId)) courseOrgWideId = SC_Constants.SC_ORG_WIDE_EMAIL_ID;                          
        opts.orgWideEmailId = (opts.sendNotification) ? courseOrgWideId : null;
        if (!opts.isValid()) {
            ApexPages.addMessage(new ApexPages.Message(
                ApexPages.Severity.ERROR,
                'Invalid Enrollment Options',
                String.join(opts.getErrors(),'\n')));
            return;
        }
        Set<String> uniqueEmails = collectEmails(this.emails);
        SC_Enrollment_Batch batchEnroller = new SC_Enrollment_Batch(uniqueEmails, opts);
        Database.executeBatch(batchEnroller);
        ApexPages.addMessage(new ApexPages.Message(
            ApexPages.Severity.CONFIRM,
            'Enrollment Started',
            'Processing ' + uniqueEmails.size() + ' unique emails for enrollment. Check your email for results.'));
        submitted = true;
    }

    private Id defaultEmailTemplate() {
        for (EmailTemplate template : [
            SELECT Id FROM EmailTemplate WHERE DeveloperName = :Label.Default_Bulk_Enroll_Notification_Template
        ]) {
            return template.Id;
        }
        return null;
    }

    private SelectOption[] loadTemplates(Id folderId) {
        SelectOption[] templates = new SelectOption[0];
        for (EmailTemplate template :[
            select Id, Name
            from EmailTemplate
            where FolderId = :folderId
            order by Name asc
        ]) {
            templates.add(new SelectOption(
                template.Id,
                template.Name
            ));
        }
        return templates;
    }

    private Set<String> collectEmails(String emails) {
        Set<String> return_set = new Set<String>();
        for (String e : SFDCStringUtils.bigSplit(emails, '\n')) {
            if (String.isNotBlank(e)) {
                return_set.add(e.trim());
            }
        }
        return return_set;
    }
}
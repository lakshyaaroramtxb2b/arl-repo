/***********************************************************
* Helper class for PRT_AssignmentTriggerHelper
* Created by - Prashant Gupta
* Date      - 01 jan 2020
************************************************************/
public class PRT_AssignmentWeekTriggerHelper {
	public static void rollupAssignmentWeekCapacity(List<Assignment_Week__c> newList, Map<id,Assignment_Week__c> oldMap){
        Set<ID> assignmentIDs = new Set<ID>();
        for(Assignment_Week__c aw : newList){
            if(aw.Number_of_Hours__c!=null && (oldMap==null || aw.Number_of_Hours__c!=oldMap.get(aw.id).Number_of_Hours__c)){
                assignmentIDs.add(aw.Assignment__c);
                if(oldMap!=null)
                    assignmentIDs.add(oldMap.get(aw.id).Assignment__c);
            }
        }
        Map<id,Assignment__c> awMap = new Map<id,Assignment__c>();
        if(assignmentIDs!=null && !assignmentIDs.isEmpty()){
            for(Assignment_Week__c aw: [SELECT id,Number_of_Hours__c ,Assignment__c FROM Assignment_Week__c WHERE assignment__c IN: assignmentIDs] ){
           		system.debug('??' + aw);
                if(awMap.containsKey(aw.Assignment__c)){
                system.debug('??' + awMap);        
                    if(aw.Number_of_Hours__c!=null)             
                         awMap.get(aw.Assignment__c).Total_Hours__c = awMap.get(aw.Assignment__c).Total_Hours__c + aw.Number_of_Hours__c;
                }else{
                	awMap.put(aw.Assignment__c,new Assignment__c(id=aw.Assignment__c, Total_Hours__c = 0));  
                    if(aw.Number_of_Hours__c!=null)             
                         awMap.get(aw.Assignment__c).Total_Hours__c = awMap.get(aw.Assignment__c).Total_Hours__c + aw.Number_of_Hours__c;
                
                }
                system.debug('??' + awMap);
            }
        } 
        if(awMap!=null && !awMap.isEmpty())
            update awMap.values();
    } 
}
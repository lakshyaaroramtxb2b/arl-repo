/**
 * @author: ralph@callaway.cloud
 * @description: Constants for Contact__x object
 */
public class ContactX_Constants {

	// Non-Unique INTEG_Employee_Id__c values
	public static final String EMPLOYEE_ID_CONTRACTOR1 = 'CNTRCT';
	public static final String EMPLOYEE_ID_CONTRACTOR2 = 'CTRCT';
	public static final String EMPLOYEE_ID_NA = 'N/A';
	public static final String EMPLOYEE_ID_TBD = 'TBD';
	public static Set<String> EMPLOYEE_ID_NON_UNIQUE_VALS = new Set<String>(new String[] { 
		EMPLOYEE_ID_CONTRACTOR1,
		EMPLOYEE_ID_CONTRACTOR2,
		EMPLOYEE_ID_NA,
		EMPLOYEE_ID_TBD});

	// RecordTypeId__c values
	public static final Id RT_ID_DEACTIVE = '01200000000024SAAQ';
	public static final Id RT_ID_INTERNAL = '0120000000000HPAAY';
	public static final Id RT_ID_INTERNAL_CONTRACTOR = '0120000000001xqAAA';

	// Status_c__c values
	public static final String STATUS_ACTIVE = 'Active';
	public static final String STATUS_DEACTIVE = 'Deactive';
}
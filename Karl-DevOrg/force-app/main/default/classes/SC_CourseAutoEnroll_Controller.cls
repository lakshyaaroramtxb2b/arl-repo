/**
 * @author: ralph@callaway.cloud
 */
public class SC_CourseAutoEnroll_Controller {
    
    private Id courseId;

    public SC_CourseAutoEnroll_Controller(ApexPages.StandardController controller) {
        this.courseId = controller.getId();
    }

    public void runAutoEnroll() {
        SC_AutoEnroll autoEnroller = new SC_AutoEnroll(courseId);
        Integer autoEnrollCandidates = autoEnroller.run();
        if (autoEnrollCandidates > 0) {
            ApexPages.addMessage(new ApexPages.Message(
                ApexPages.Severity.CONFIRM,
                'Auto Enrollment Started',
                'Processing ' + autoEnrollCandidates + ' candidates for auto enrollment. Check your email for results.'));    
        } else {
            ApexPages.addMessage(new ApexPages.Message(
                ApexPages.Severity.INFO,
                'Auto Enrollment Skipped',
                'No auto enrollment candidates found.'));    
        }
        SC_AutoUnenroll autoUnenroller = new SC_AutoUnenroll(courseId);
        Integer autoUnenrollCandidates = autoUnenroller.run();
        if (autoUnenrollCandidates > 0) {
            ApexPages.addMessage(new ApexPages.Message(
                ApexPages.Severity.CONFIRM,
                'Auto Unenrollment Started',
                'Processing ' + autoUnenrollCandidates + ' candidates for auto unenrollment. Check your email for results.'));    
        } else {
            ApexPages.addMessage(new ApexPages.Message(
                ApexPages.Severity.INFO,
                'Auto Unenrollment Skipped',
                'No auto unenrollment candidates found.'));    
        }
    }
}
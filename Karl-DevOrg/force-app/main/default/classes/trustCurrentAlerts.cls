public class trustCurrentAlerts {
    
    private String currentAlert;
    private static Integer nDays = 15; // the nDays in which the event is considred to be current
    private static String nTTL = '300'; // this is the TTL the results should be kept before requesting a refresh

    public trustCurrentAlerts() {

        Security_Incident_Current_Alerts_Text__c alertLangObj = null;
        Security_Incident__c alertObj = null;
        String lang = ApexPages.currentPage().getParameters().get('lang');
        String previewId = ApexPages.currentPage().getParameters().get('preview');
        
        
        
        
        /* get current alert language template */
        
        if (lang == null || lang.equals('') == true) {
            currentAlert =      '{"magic":"SFDCTRUSTMAGIC-AK2hxT3ZrE",\n' 
                            +   '"underMaintenance":false,\n'
                            +   '"display":false,\n'
                            +   '"error":"Missing lang parameter"\n'
                            +   '}';
            return;
        }
        
        // Trust alerts supersede all other Trust postings. Return active alert if it exists.
        
        trust_Alert__c[] alerts = [select AlertName__c, Alert_URL__c from Trust_Alert__c where Active__c =: true limit 1];
        
        if (alerts.size() > 0) {
            
            String str = alerts[0].AlertName__c;
            String alertURL = alerts[0].Alert_URL__c;
            
            currentAlert =      '{"magic":"SFDCTRUSTMAGIC-AK2hxT3ZrE",\n' 
                            +   '"underMaintenance":false,\n'
                            +   '"display":true,\n'
                            +   '"lang":"' + ESAPI.encoder().SFDC_JSENCODE(lang) + '",\n'
                            +   '"text":"' + ESAPI.encoder().SFDC_JSENCODE(str) + '",\n'
                            +   '"url":"' + ESAPI.encoder().SFDC_JSENCODE(alertURL) + '",\n'
                            +   '"TTL":' + nTTL + '\n'
                            +   '}';
            return;
            }
        
        // No active Trust alerts, continue with existing logic to display recent e-mail threats.
        
        try {
            // The query would never return more than one object because we have set the lang as a unique value - so no need to worry about "limit 1". 
            alertLangObj = [select Text__c, Language_ID__c from Security_Incident_Current_Alerts_Text__c where Language_ID__c =:lang limit 1];
        } catch (Exception e) {}

        if (alertLangObj == null) {
            currentAlert =      '{"magic":"SFDCTRUSTMAGIC-AK2hxT3ZrE",\n' 
                            +   '"underMaintenance":false,\n'
                            +   '"display":false,\n'
                            +   '"error":"Requested lang not found"\n'
                            +   '}';
            return;
        }
        
        /* get current alert object - if preview was provided - include that in the query */
        
        if (previewId == null) {
            // select latest object that was approved and is within nDays days from today
            try {
                alertObj = Database.query('select ID, Name, Email_subject__c from Security_Incident__c ' 
                                        + 'where Security_Approval_Status__c = \'Closed\' and Replaced_By__c = null ' 
                                        + 'and Date__c >= LAST_N_DAYS:' + nDays + ' '
                                        + 'order by Date__c DESC limit 1');
            } catch (Exception e) {}
        }
        else {
            // select latest object that was approved and not replaced - include the preview object if it is the latest
            
            // start by getting the preview object
            Security_Incident__c previewObj = null;
            try {
                previewObj = [select ID, Name, Replaces__c from Security_Incident__c where id =:previewId];
            } catch (Exception e) {}
            if (previewObj == null) {
                currentAlert =      '{"magic":"SFDCTRUSTMAGIC-AK2hxT3ZrE",\n' 
                                +   '"underMaintenance":false,\n'
                                +   '"display":false,\n'
                                +   '"error":"Preview object not found"\n'
                                +   '}';
                return;
            }
            
            if (previewObj.Replaces__c == null) {
                // The preview object is not replacing an existing one - get the latest objet that was approved or the preview object
                try {
                    alertObj = Database.query('select ID, Name, Email_subject__c from Security_Incident__c ' 
                                            + 'where (Security_Approval_Status__c = \'Closed\' or id =\'' + String.escapeSingleQuotes(previewId) + '\') and Replaced_By__c = null ' 
                                            + 'and Date__c >= LAST_N_DAYS:' + nDays + ' '
                                            + 'order by Date__c DESC limit 1');
                } catch (Exception e) {}
            }
            else {
                // the preview object is intending to replace an existing one - search for the latest excluding the object intended to be replaced by the preview object
                try {
                    alertObj = Database.query('select ID, Name, Email_subject__c from Security_Incident__c ' 
                                            + 'where (Security_Approval_Status__c = \'Closed\' or id =\'' + String.escapeSingleQuotes(previewId) + '\') and Replaced_By__c = null ' 
                                            + 'and id != \'' + String.escapeSingleQuotes(previewObj.Replaces__c) + '\' and Date__c >= LAST_N_DAYS:' + nDays + ' '
                                            + 'order by Date__c DESC limit 1');
                } catch (Exception e) {}
            }

            // make sure the preview object was returned
            if (alertObj == null || previewObj.Name != alertObj.Name) {
                currentAlert =      '{"magic":"SFDCTRUSTMAGIC-AK2hxT3ZrE",\n' 
                                +   '"underMaintenance":false,\n'
                                +   '"display":false,\n'
                                +   '"error":"Preview object was not selected"\n'
                                +   '}';
                return;
            }
        }
        
        if (alertObj == null) {
            // no current alert
            currentAlert =      '{"magic":"SFDCTRUSTMAGIC-AK2hxT3ZrE",\n' 
                            +   '"underMaintenance":false,\n'
                            +   '"display":false,\n'
                            +   '"lang":"' + ESAPI.encoder().SFDC_JSENCODE(lang) + '",\n'
                            +   '"TTL":' + nTTL + '\n'
                            +   '}';
            return;
        }
        
        String str = alertLangObj.Text__c;
        
        str = str.replace('$STRING', '"' + alertObj.Email_subject__c + '"');
        
        currentAlert =      '{"magic":"SFDCTRUSTMAGIC-AK2hxT3ZrE",\n' 
                        +   '"underMaintenance":false,\n'
                        +   '"display":true,\n'
                        +   '"lang":"' + ESAPI.encoder().SFDC_JSENCODE(lang) + '",\n'
                        +   '"text":"' + ESAPI.encoder().SFDC_JSENCODE(str) + '",\n'
                        +   '"TTL":' + nTTL + '\n'
                        +   '}';
    }

    public String getCurrentAlert() {
        return currentAlert;
    }
}
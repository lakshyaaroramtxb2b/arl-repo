/**
 * Update External Case/Work status to Intake Requests.
** ** Id batchJobId = Database.executeBatch(new SyncExternalCaseAndWorkStatusUpdate(), 100);
*/
public class SyncExternalCaseAndWorkStatusUpdate implements Schedulable, Database.Batchable<sObject>  {

    public String SOURCE_FILE = 'SyncExternalCaseAndWorkStatusUpdate';

    public Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([
             SELECT Id, 
                    Case_Work_Closed_Date__c, 
                    Case_Work_Status__c,
                    Status__c,
                    Supportforce_CaseId__c,
                    Internal_Status__c,
                    Case_Work_Last_Modified_Date__c
             FROM ESA_Security_Request__c
             WHERE Supportforce_CaseId__c!=null AND
             (Case_Work_Closed_Date__c = null OR Case_Work_Closed_Date__c >= LAST_WEEK)
             ORDER BY Case_Work_Status__c ASC NULLS FIRST
        ]);
    }

    public void execute(SchedulableContext ctx) {
        Database.executebatch(new SyncExternalCaseAndWorkStatusUpdate(),100);
    }

    public void execute(Database.BatchableContext BC, List<ESA_Security_Request__c> lstIntakeRequests) {
        try {
            updateIntakeRequestStatus(lstIntakeRequests);
        } catch (Exception e) {
            Esa_DebugService.WriteException(e, 'SyncExternalCaseAndWorkStatusUpdate', 'Exception in execute method');
        } finally {
            
        }
    }


    private void updateIntakeRequestStatus(List<ESA_Security_Request__c> lstIntakeRequests){
        
        String caseId;
        Set<Id> caseIds = new Set<Id>();
        Set<Id> workIds = new Set<Id>();

        for(ESA_Security_Request__c inTakeRequest : lstIntakeRequests){
            caseId = inTakeRequest.Supportforce_CaseId__c;
            if(caseId != null) {
                if(caseId.startsWith('500')) {
                    caseIds.add(caseId);
                }else {
                    workIds.add(caseId);
                }
            }
        }
        
        getExternalCaseWorkStatus(lstIntakeRequests, caseIds, workIds);
        
        List<Database.SaveResult> saveResults = Database.update(lstIntakeRequests,false);

        String finalErrorText = '';
        for (Database.SaveResult sr : saveResults) {
            if (!sr.isSuccess()) {
                for(Database.Error err : sr.getErrors()) {
                    finalErrorText += err.getStatusCode() + ': ' + err.getMessage() + '\n';
                }
            }
        }
        
        Esa_DebugService.writeMessage('SyncExternalCaseWorkStatus Completed:');
        if(string.isNotBlank(finalErrorText)) {
            Esa_DebugService.writeMessage('SyncExternal Case And Work Status Update.finish: Failed with errors');
            Exception ex;
            Esa_DebugService.writeException(ex, 1, SOURCE_FILE, finalErrorText);
        }
    }

    

    private void getExternalCaseWorkStatus(List<ESA_Security_Request__c> lstSecuirityRequests, Set<Id> caseIds, Set<Id> workIds) {
        
        Map<Id,Case__x> mapCases = new Map<Id,Case__x>();
        Map<Id,ADM_Work_c__x> mapWork = new Map<Id,ADM_Work_c__x>();

        List<Case__x> lstCases = new List<Case__x>();
        List<ADM_Work_c__x> lstWork = new List<ADM_Work_c__x>();


        if(caseIds.size() > 0) {
            lstCases = [SELECT ExternalId,Status__c,
                               ClosedDate__c,
                               IsClosed__c,
                               LastModifiedDate__c
                        FROM Case__x
                        WHERE ExternalId IN : caseIds];
            for(Case__x ca : lstCases) {
                    mapCases.put(ca.ExternalId,ca);
            }
        }

        if(workIds.size() > 0) {
            lstWork = [SELECT ExternalId,Status_c__c,
                              Closed_On_c__c,
                              Closed_c__c,
                              LastModifiedDate__c 
                       FROM ADM_Work_c__x
                       WHERE ExternalId IN : workIds];
            for(ADM_Work_c__x ca : lstWork) {
                    mapWork.put(ca.ExternalId,ca);
            }
        }

        for(ESA_Security_Request__c eRequest : lstSecuirityRequests) {
            if(mapCases.containsKey(eRequest.Supportforce_CaseId__c)) {

                Case__x ca = mapCases.get(eRequest.Supportforce_CaseId__c);
                eRequest.Case_Work_Closed_Date__c = ca.ClosedDate__c;
                eRequest.Case_Work_Status__c      = ca.Status__c;
                eRequest.Case_Work_Last_Modified_Date__c = ca.LastModifiedDate__c;
                if(ca.IsClosed__c == true) {
                    eRequest.Status__c = 'Closed';
                    if(eRequest.Internal_Status__c != ESA_AppConstants.INTERNAL_STATUS_APPROVED
                           && eRequest.Internal_Status__c != 'Rejected') {
                       eRequest.Internal_Status__c = null;
                    }
                } else if(ca.Status__c != 'New') {
                    eRequest.Status__c = 'In Progress';
                    eRequest.Internal_Status__c = ca.Status__c;
                }

            }else if(mapWork.containsKey(eRequest.Supportforce_CaseId__c)) {

                ADM_Work_c__x work = mapWork.get(eRequest.Supportforce_CaseId__c);
                eRequest.Case_Work_Closed_Date__c = work.Closed_On_c__c;
                eRequest.Case_Work_Status__c      = work.Status_c__c;
                eRequest.Case_Work_Last_Modified_Date__c = work.LastModifiedDate__c;
                if(work.Closed_c__c == 1) {
                    eRequest.Status__c = 'Closed';
                    if(eRequest.Internal_Status__c != ESA_AppConstants.INTERNAL_STATUS_APPROVED
                           && eRequest.Internal_Status__c != 'Rejected') {
                       eRequest.Internal_Status__c = null;
                    }
                }else if(work.Status_c__c != 'New') {
                    eRequest.Status__c = 'In Progress';
                    eRequest.Internal_Status__c = work.Status_c__c;
                }
            }
        }
        
    }    

    public void finish(Database.BatchableContext BC) {
         if(!Test.isRunningTest()) {
           SyncExternalCaseAndWorkStatusUpdate batch = new SyncExternalCaseAndWorkStatusUpdate();
           System.scheduleBatch(batch, 'SyncExternalCaseAndWorkStatusUpdate', 10, 100);
        }
    }
}
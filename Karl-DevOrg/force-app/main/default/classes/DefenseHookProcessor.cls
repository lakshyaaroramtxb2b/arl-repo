public class DefenseHookProcessor {

    // Returns the plaintext version of the Defense Hook
    public Map<String, Object> getDefenseHook(boolean defaultOptionFlag) {
        
        Map<String, Object> processedDefenseHook = new Map<String, Object>();
        try {
            // We expect there to be only one
            defense_hook__c defenseHook = [select login__c, postaloha__c, postlightning__c, fingerprint__c, config__c from defense_hook__c];
            DefenseControllerUtil.addAttributeIfNonNull('LOGIN', defenseHook.login__c, processedDefenseHook);
            DefenseControllerUtil.addAttributeIfNonNull('POSTALOHA', defenseHook.postaloha__c, processedDefenseHook);
            DefenseControllerUtil.addAttributeIfNonNull('POSTLIGHTNING', defenseHook.postlightning__c, processedDefenseHook);
            DefenseControllerUtil.addAttributeIfNonNull('FINGERPRINT', defenseHook.fingerprint__c, processedDefenseHook);
            
            String config = defenseHook.config__c;
            Map<String, Object> deserializedConfig;
            if (config != null) {
                deserializedConfig = (Map<String, Object>)JSON.deserializeUntyped(config);                
            } else {
                deserializedConfig = new Map<String, Object>();
            }
            deserializedConfig.put('USEINPOOL', String.valueOf(defaultOptionFlag));
            DefenseControllerUtil.addAttributeIfNonNull('CONFIG', deserializedConfig, processedDefenseHook);
            
        } catch (Exception e) {
            processedDefenseHook.clear();
            processedDefenseHook.put('ERROR', 'Failed parsing the defense hook. Ensure there is only one defense hook in place and try again.');
        }
        return processedDefenseHook;
   }

   // Returns the encoded version of the Defense Hook
   public String getSerializedDefenseHook(boolean defaultOptionFlag) {
        Map<String, Object> jwtBody = new Map<String, Object>();
        final String certName = 'md_cert';
        jwtBody.put('devName', certName);
        
        Map<String, Object> header = DefenseControllerUtil.getHeader();        
        Map<String, Object> defenseHook = getDefenseHook(defaultOptionFlag);
        jwtBody.put('LOGIN', defenseHook.get('LOGIN'));
        jwtBody.put('POSTALOHA', defenseHook.get('POSTALOHA'));
        jwtBody.put('POSTLIGHTNING', defenseHook.get('POSTLIGHTNING'));
        jwtBody.put('FINGERPRINT', defenseHook.get('FINGERPRINT'));
        jwtBody.put('CONFIG', defenseHook.get('CONFIG'));
        
        String base64Header = DefenseControllerUtil.base64UrlEncode(Blob.valueOf(JSON.serializePretty(header)));
        String base64Body = DefenseControllerUtil.base64UrlEncode(Blob.valueOf(JSON.serializePretty(jwtBody)));
        
        // Prep the data to be signed
        String toBeSigned = base64Header + '.' + base64Body;

        String result;
        try {
            Blob signature = Crypto.signWithCertificate('RSA-SHA256', Blob.valueOf(toBeSigned), certName);
            String base64Signature = DefenseControllerUtil.base64UrlEncode(signature);
            result = base64Header + '.' + base64Body + '.' + base64Signature;
        }
        catch (Exception ex) {
            result = 'Missing md_cert certificate. Cannot publish. Create a certificate with the dev name "md_cert" and export the public key cert for checkin.';
        }
        return result;
   }

}
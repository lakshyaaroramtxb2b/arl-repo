/**
 * @author Mahima Aggarwal
 * @email mahima.aggarwal@mtxb2b.com
 * @desc Controller for KARL_ExecuteAuditorReqItemShareBatch aura(quick action)
 */
public with sharing class KARL_ExecuteAudReqItemShareBatch {
    /**
     * @author Mahima Aggarwal
     * @email mahima.aggarwal@mtxb2b.com
     * @desc This method runs the auditor request item sharing batch
     */
    @AuraEnabled
    public static void executeAuditorReqItemShareBatch(Integer minutesLength){
        KARL_ShareAuditorReqItemToAuditFirmBatch objBatch = new KARL_ShareAuditorReqItemToAuditFirmBatch(minutesLength);
        Database.executebatch(objBatch,100);
    }
}
@isTest
public class KARL_ScopeNotificationConfigurationTest {
    @testSetup
    public static void makeData(){
        KARL_Scope_Notifications_Config__c scopeNotification = new KARL_Scope_Notifications_Config__c();
        scopeNotification.Group_Email_Alias__c = 'testnotification@mail.com';
        scopeNotification.Scope__c = 'CS';
        scopeNotification.Mute_Emails__c = false;
        insert scopeNotification;
        
        Account acc = new Account(Name = 'salesforce.com - ESA Office Hours');
        insert acc;
        
        Contact con = new Contact(LastName = 'test', AccountId = acc.Id,Email = 'test@gmail.com');
        insert con;
        
        Request_Item__c reqItem = ARL_TestDataFactory.createRequestItem();
        reqItem.Primary_Scope__c = 'CS';
        reqItem.KARL_Evidence_Submission_Workflow__c = true;
        reqItem.Create_GUS_Case__c = true;
        insert reqItem;
        
        Cycle_Request_Item__c cycleReqItem = ARL_TestDataFactory.createCycleRequestItem(reqItem.id,null);
        cycleReqItem.Cycle_Request_Status__c = 'Provided To Auditor';
        cycleReqItem.Request_GUS__c = 'testWorkId';
        cycleReqItem.KARL_Update_GUS__c = true;
        cycleReqItem.Request_Tech_Details__c = 'tech details old';
        cycleReqItem.Request_Assignee__c = con.Id;
        insert cycleReqItem;
        
        KARL_Evidence_Request__c evidenceReq = new KARL_Evidence_Request__c();
        evidenceReq.KARL_Submitted__c = false;
        evidenceReq.KARL_Cycle_ARL_Item__c = cycleReqItem.Id;
        insert evidenceReq;
        System.debug('evidenceReq status-> '+ evidenceReq.KARL_Primary_Scope__c);
        
    }
    @isTest
    public static void batchTest(){
        database.executebatch(new KARL_ScopeNotificationConfiguration());
    }
}
public with sharing class CSIRT_createIncident 
{
    private ApexPages.StandardController standardController;
	public String Subject {get;set;} 
	public String Severity {get;set;}
	public String Description {get;set;}
	public String investigationType {get;set;}
	public String investigationCategory {get;set;}
	public String incidentType {get;set;}
	public String incidentCategory {get;set;}
    Public Account acctid {get;set;}
    Public Case newCase {get; set;}
    
	
    public CSIRT_createIncident(ApexPages.StandardController standardController) 
    { 
        this.standardController = standardController;
        //create the new case in the constructor
        this.newCase = new Case();
    }

    public PageReference Create_Incident()
    { 
       
        List<RecordType> typeList = new List<RecordType>(CSIRT_caseCache.Incident());
        system.debug('********** the size of the record type list is' + typeList.size());
        
        if(typeList.size() > 0)
        {
    		RecordType rt = typeList.get(0);
    		System.Debug('********** ' + 'the value of incident id is' + rt);
        	newCase.RecordTypeId = rt.id;
        }
        
		
        insert newCase;
        
        //get the Id for the newly created case
        Id incidentId = newCase.Id;
        
        //assign the Id for the new case as the parent of the current record
        Case curcase = (Case) standardController.getRecord(); 
        curcase.ParentId = incidentId;
        curcase.Status = 'Closed - Incident';
        update curcase;
         
        PageReference pageRef = new PageReference('/'+ newCase.Id);
        return pageRef;
    }    
}
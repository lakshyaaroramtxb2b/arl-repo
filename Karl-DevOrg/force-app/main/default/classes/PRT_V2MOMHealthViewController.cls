/*
 * Modified by - Swarnima Singh Mandhata
 * Date - 05/13/2020
*/
public with sharing class PRT_V2MOMHealthViewController {
    // Test data for test class
    
    @TestVisible
    private static list<sObject> mockallV2MOMList = new list<sObject>();
    @TestVisible
    private static list<sObject> mockallOrg62Users = new list<sObject>();
    @TestVisible 
    private static list<sObject> mockallMethods = new list<sObject>();
    @TestVisible 
    private static list<sObject> mockallMeasures = new list<sObject>();
    @TestVisible 
    private static list<Project__c> mockallProjects = new list<Project__c>();
    
    /**
* @description: Get external user id from current user
* @author: Virendra Yadav 
*/
    @AuraEnabled(cacheable=true)
    public static Org62User__x fetchExternalUser(String userId){
        List<User> currentUserRecord = [SELECT Id, Name, employeenumber FROM User WHERE Id=: userId LIMIT 1];
        if(!currentUserRecord.isEmpty()) {
            List<Org62User__x> externalUserRecord = new List<Org62User__x>();
            if(Test.isRunningTest()){
                externalUserRecord = mockallOrg62Users;
            } else {
                externalUserRecord = [SELECT Id, name__c,externalId, EmployeeNumber__c FROM Org62User__x where EmployeeNumber__c =: currentUserRecord[0].employeenumber AND EmployeeNumber__c != null];
            }
            if(!externalUserRecord.isEmpty()) {
                return externalUserRecord[0];
            }
        }
        return null;
    }
    
    /**
* @description 
* @author virendra.yadav
* @param userId 
* @return List<V2MomMethodWrapper> 
**/
    @AuraEnabled(cacheable=true)
    public static List<V2MomMethodWrapper> fetchV2MOMMethods(String userId){
        
        Set<String> v2momIds = new Set<String>();
        Map<String, V2MomMethodWrapper> v2momMethodMap = new Map<String, V2MomMethodWrapper>();        
        Map<String, MeasuresWrapper> v2momMeasureMap = new Map<String, MeasuresWrapper>();        
        Map<String, String> v2MOM_MeasureVsMethod_Map = new Map<String, String>();         
        try {
            // Set the v2MomIds
            List<V2MOM_c__x> v2momList = new List<V2MOM_c__x>();
            v2momList = [SELECT Id, V2MOM_User_c__c, ExternalId FROM V2MOM_c__x WHERE V2MOM_User_c__c =: fetchExternalUser(userId).externalId and Status_c__c = 'Published' Order by CreatedDate__c DESC LIMIT 1];
            if(Test.isRunningTest()) {
                v2momList = mockallV2MOMList;
            }            
            for (V2MOM_c__x v2momRecord : v2momList) {
                v2momIds.add(v2momRecord.ExternalId);
                System.debug('v2momIds-->' + v2momIds);
            }
            
            // Set the v2momMethodMap
            List<V2MOM_Method_c__x> methodList = new List<V2MOM_Method_c__x>();
            //List<V2MOM_Method_c__x> currentYearV2MomList = new List<V2MOM_Method_c__x>();
            for(V2MOM_Method_c__x v2MomMethodRecords : [SELECT Id,ExternalId, CreatedDate__c, Name__c, Description_c__c, Priority_c__c, Method_Owners_c__c 
                                                        FROM V2MOM_Method_c__x 
                                                        WHERE V2MOM_c__c IN : v2momIds Order by Priority_c__c ASC,CreatedDate__c DESC]){
                                                            Date startDayOfFiscalYear = Date.newInstance(date.today().year(), 2, 1);
                                                            Date lastDayOfFiscalMonth = Date.newInstance(date.today().addYears(1).year(), 1, 31);
                                                                  methodList.add(v2MomMethodRecords);
                                                        }
            //methodList = [SELECT Id,ExternalId, Name__c, Description_c__c, Priority_c__c, Method_Owners_c__c FROM V2MOM_Method_c__x WHERE V2MOM_c__c IN : v2momIds Order by Priority_c__c ASC];
            if(Test.isRunningTest()) {
                methodList = mockallMethods;
            }
            for (V2MOM_Method_c__x v2momMethodRecord : methodList) {
                V2MomMethodWrapper v2momMethodWrapperRecord = new V2MomMethodWrapper();
                v2momMethodWrapperRecord.id = v2momMethodRecord.Id;
                v2momMethodWrapperRecord.externalId = v2momMethodRecord.ExternalId;
                v2momMethodWrapperRecord.methodName = v2momMethodRecord.Name__c;
                v2momMethodWrapperRecord.description = v2momMethodRecord.Description_c__c;
                v2momMethodWrapperRecord.priority = v2momMethodRecord.Priority_c__c;
                v2momMethodWrapperRecord.methodOwners = v2momMethodRecord.Method_Owners_c__c;
                v2momMethodMap.put(v2momMethodRecord.ExternalId, v2momMethodWrapperRecord);            
            }
            // v2momMethodMap = getContactForMethod(v2momMethodMap);
            
            // Set v2mom measures in to v2momMethodMap
            List<Measure_c__x> measureList = new List<Measure_c__x>();
            measureList = [SELECT Id, MeasureName_c__c, externalId, DueDate_c__c, Priority_c__c, Status_c__c, Method_c__c FROM Measure_c__x WHERE Method_c__c IN : v2momMethodMap.keySet() Order by Priority_c__c ASC];
            if(Test.isRunningTest()) {
                measureList = mockallMeasures;
            }
            for (Measure_c__x v2momMeasureRecord : measureList) {
                MeasuresWrapper v2momMeasureWrapperRecord = new MeasuresWrapper();
                v2momMeasureWrapperRecord.id = v2momMeasureRecord.Id;
                v2momMeasureWrapperRecord.externalId = v2momMeasureRecord.externalId;
                v2momMeasureWrapperRecord.measureName = v2momMeasureRecord.MeasureName_c__c;
                v2momMeasureWrapperRecord.dueDate = v2momMeasureRecord.DueDate_c__c;
                v2momMeasureWrapperRecord.priority = v2momMeasureRecord.Priority_c__c;
                v2momMeasureWrapperRecord.status = v2momMeasureRecord.Status_c__c;
                v2momMeasureWrapperRecord.methodExternalId = v2momMeasureRecord.Method_c__c;
                v2momMeasureMap.put(v2momMeasureRecord.externalId, v2momMeasureWrapperRecord);
                if(Test.isRunningTest()) {
                    Measure_c__x mockMeasureRec = (Measure_c__x)mockallMeasures[0];
                    v2MOM_MeasureVsMethod_Map.put(mockMeasureRec.externalId,mockMeasureRec.externalId);
                }
                if(v2momMeasureRecord.Method_c__c!=null){
                    v2MOM_MeasureVsMethod_Map.put(v2momMeasureRecord.ExternalId,v2momMeasureRecord.Method_c__c );
                }                
            }
            v2momMeasureMap = getContactForMeasure(v2momMeasureMap);
            
            // Set projects to measures
            List<Project__c> projectList = new List<Project__c>();
            projectList = [SELECT Id, V2MOM_Measure__c, Name, Status__c, Project_Start_Date__c, Projected_Date_of_Completion__c,Overall_Project_Health__c FROM Project__c WHERE RecordTypeId =: PRT_Constants.PRT_PROJECT_PROJECT_RECORDTYPEID AND V2MOM_Measure__c IN : v2momMeasureMap.keySet()];
            if(Test.isRunningTest()) {
                projectList = mockallProjects;
            }
            for (Project__c projectRecord : projectList) {
                MeasuresWrapper measureRecord = v2momMeasureMap.get(projectRecord.V2MOM_Measure__c);
                if(Test.isRunningTest()) {
                    Measure_c__x measureRec = (Measure_c__x)mockallMeasures[0];
                    measureRecord = v2momMeasureMap.get(measureRec.externalId);
                }
                ProjectWrapper projectWrapperRecord = new ProjectWrapper();
                projectWrapperRecord.name = projectRecord.Name;
                if(projectRecord.Project_Start_Date__c != null) {
                    projectWrapperRecord.startDate = changeDateFormat(projectRecord.Project_Start_Date__c);
                }
                if(projectRecord.Projected_Date_of_Completion__c != null) {
                    projectWrapperRecord.endDate = changeDateFormat(projectRecord.Projected_Date_of_Completion__c);
                }
                projectWrapperRecord.status = projectRecord.Status__c;
                projectWrapperRecord.overallStatus = projectRecord.Overall_Project_Health__c;
                if(projectRecord.Overall_Project_Health__c != null) {
                    projectWrapperRecord.className = projectRecord.Overall_Project_Health__c.replace(' ','-');
                } else {
                    projectWrapperRecord.className = 'grey-tr';                 
                }
                
                measureRecord.projectList.add(projectWrapperRecord);
                v2momMeasureMap.put(projectRecord.V2MOM_Measure__c, measureRecord);
                if(v2MOM_MeasureVsMethod_Map.containsKey(measureRecord.externalId) && v2MOM_MeasureVsMethod_Map.get(measureRecord.externalId)!=null){
                    ProjectWrapper projectWrapperRecord2 = new ProjectWrapper();
                    projectWrapperRecord2.name = projectRecord.Name;
                    if(projectRecord.Project_Start_Date__c != null) {
                        projectWrapperRecord2.startDate = changeDateFormat(projectRecord.Project_Start_Date__c);
                    }
                    if(projectRecord.Projected_Date_of_Completion__c != null) {
                        projectWrapperRecord2.endDate = changeDateFormat(projectRecord.Projected_Date_of_Completion__c);
                    }
                    projectWrapperRecord2.status = projectRecord.Status__c;
                    projectWrapperRecord2.overallStatus = projectRecord.Overall_Project_Health__c;
                    if(projectRecord.Overall_Project_Health__c != null) {
                        projectWrapperRecord2.className = projectRecord.Overall_Project_Health__c.replace(' ','-');
                    } else {
                        projectWrapperRecord2.className = 'grey-tr';                 
                    }
                    V2MomMethodWrapper v2momMethodWrapperRecord = v2momMethodMap.get(v2MOM_MeasureVsMethod_Map.get(measureRecord.externalId));
                    v2momMethodWrapperRecord.projectList.add(projectWrapperRecord2);
                    v2momMethodMap.put(v2MOM_MeasureVsMethod_Map.get(measureRecord.externalId), v2momMethodWrapperRecord);   
                }
            }        
            for (MeasuresWrapper v2momMeasureRecord : v2momMeasureMap.values()) {
                V2MomMethodWrapper v2momMethodWrapperRecord = new V2MomMethodWrapper();
                if(Test.isRunningTest()) {
                    V2MOM_Method_c__x methodRec = (V2MOM_Method_c__x)mockallMethods[0];
                    v2momMethodWrapperRecord = v2momMethodMap.get(methodRec.externalId);                  
                } else {
                    v2momMethodWrapperRecord = v2momMethodMap.get(v2momMeasureRecord.methodExternalId);                    
                }
                v2momMethodWrapperRecord.measures.add(v2momMeasureRecord);
                v2momMethodMap.put(v2momMeasureRecord.methodExternalId, v2momMethodWrapperRecord);            
            }
            // Set projects in to v2momMethodMap
            List<Project__c> methodProjectList = new List<Project__c>();
            methodProjectList = [SELECT Id, V2MOM_Method__c, Name, Status__c, Project_Start_Date__c, Projected_Date_of_Completion__c,Overall_Project_Health__c FROM Project__c WHERE RecordTypeId =: PRT_Constants.PRT_PROJECT_PROJECT_RECORDTYPEID AND V2MOM_Method__c IN : v2momMethodMap.keySet()];
            if(Test.isRunningTest()) {
                methodProjectList = mockallProjects;
            }
            for (Project__c projectRecord : methodProjectList) {
                ProjectWrapper projectWrapperRecord = new ProjectWrapper();
                projectWrapperRecord.name = projectRecord.Name;
                if(projectRecord.Project_Start_Date__c != null) {
                    projectWrapperRecord.startDate = changeDateFormat(projectRecord.Project_Start_Date__c);
                }
                if(projectRecord.Projected_Date_of_Completion__c != null) {
                    projectWrapperRecord.endDate = changeDateFormat(projectRecord.Projected_Date_of_Completion__c);
                }
                projectWrapperRecord.status = projectRecord.Status__c;
                projectWrapperRecord.overallStatus = projectRecord.Overall_Project_Health__c;
                if(projectRecord.Overall_Project_Health__c != null) {
                    projectWrapperRecord.className = projectRecord.Overall_Project_Health__c.replace(' ','-');
                } else {
                    projectWrapperRecord.className = 'grey-tr';                 
                }
                V2MomMethodWrapper v2momMethodWrapperRecord = v2momMethodMap.get(projectRecord.V2MOM_Method__c);
                v2momMethodWrapperRecord.projectList.add(projectWrapperRecord);
                v2momMethodMap.put(projectRecord.V2MOM_Method__c, v2momMethodWrapperRecord);            
            }
            
            // Set the measures
            return v2momMethodMap.values();
            
        } catch (Exception e) {            
            return v2momMethodMap.values();
        }    
    }
    
    
    public static Map<String, MeasuresWrapper> getContactForMeasure(Map<String, MeasuresWrapper> v2momMeasureMapParent) {
        Map <String, V2MOMContactWrapper> v2momContactMap = new Map<String, V2MOMContactWrapper>();
        Map <String, String> v2MomMappingMap = new Map<String, String>();
        Map <String, String> v2MomMeasureMap = new Map<String, String>();        
        Map <String, String> v2MomMap = new Map<String, String>();
        Map <String, Map<String, String>> v2MomUserMapping = new Map<String, Map<String, String>>();
        Map <String, String> v2momMethodMeasureMap = new Map <String, String>();
        Map <String, String> org62UserMap = new Map<String, String>();
        
        // Setting v2momMapping
        List<V2MomMapping__c> v2momMappingList = new List<V2MomMapping__c>();
        v2momMappingList = [SELECT Name, Dependent_Method__c, Controlling_Measure__c, Id, OwnerId FROM V2MomMapping__c WHERE Controlling_Measure__c IN : v2momMeasureMapParent.keySet()];
        if(Test.isRunningTest()) {
            // v2momMappingList = mockall;
        }
        for(V2MomMapping__c v2MomMappingObject : v2momMappingList){
            v2MomMappingMap.put(v2MomMappingObject.Dependent_Method__c, v2MomMappingObject.Dependent_Method__c);
            v2momMethodMeasureMap.put(v2MomMappingObject.Dependent_Method__c, v2MomMappingObject.Controlling_Measure__c);
        }
        
        // setting methods
        List<V2MOM_Method_c__x> methodList = new List<V2MOM_Method_c__x>();
        for(V2MOM_Method_c__x v2MomMethodRecords : [SELECT Id, V2MOM_c__c,CreatedDate__c, Externalid 
                                                    FROM V2MOM_Method_c__x 
                                                    WHERE Externalid IN : v2MomMappingMap.keySet()  Order by Priority_c__c ASC]){
                                                        if(v2MomMethodRecords.CreatedDate__c.year() == date.today().year()){
                                                            methodList.add(v2MomMethodRecords);
                                                        }
                                                    }
        //methodList = [SELECT Id, V2MOM_c__c, Externalid FROM V2MOM_Method_c__x WHERE Externalid IN : v2MomMappingMap.keySet()  Order by Priority_c__c ASC];
        if(Test.isRunningTest()) {
            methodList = mockallMethods;
        }
        for(V2MOM_Method_c__x v2MomMethodObject : methodList){
            v2MomMappingMap.put(v2MomMappingMap.get(v2MomMethodObject.Externalid),v2MomMethodObject.V2MOM_c__c);
        }
        
        // setting v2moms
        List<V2MOM_c__x> v2momList = new List<V2MOM_c__x>();
        v2momList = [SELECT Id, V2MOM_User_c__c, Name__c, Externalid FROM V2MOM_c__x WHERE Externalid IN : v2MomMappingMap.values() order by CreatedDate__c DESC];
        if(Test.isRunningTest()) {
            v2momList = mockallV2MOMList;
        }
        for(V2MOM_c__x v2MomObject : v2momList){            
            for (String methodId : v2MomMappingMap.keySet()){
                if(v2MomMappingMap.get(methodId) == v2MomObject.Externalid) {
                    v2MomMap.put(methodId, v2MomObject.V2MOM_User_c__c);
                }
            }
            Map<String, String> v2momNameMap = new Map<String, String>();
            v2momNameMap.put('Name',v2MomObject.Name__c);
            v2MomNameMap.put('Id', v2MomObject.Id);
            v2MomUserMapping.put(v2MomObject.V2MOM_User_c__c, v2MomNameMap);
        }    
        
        // setting users
        List<Org62User__x> userList = new List<Org62User__x>();
        userList = [SELECT Id, ExternalId, EmployeeNumber__c FROM Org62User__x WHERE Externalid IN : v2MomMap.values()];
        if(Test.isRunningTest()) {
            userList = mockallOrg62Users;
        }
        for(Org62User__x org62UserObject : userList){  
            for (String methodId : v2MomMap.keySet()) {
                if(v2MomMap.get(methodId) == org62UserObject.ExternalId) {
                    org62UserMap.put(methodId, org62UserObject.EmployeeNumber__c);                    
                    v2MomUserMapping.put(org62UserObject.EmployeeNumber__c, v2MomUserMapping.get(org62UserObject.Externalid));       
                }
            }          
        }
        
        for(User userObject : [SELECT Name, Id,employeeNumber FROM User WHERE employeeNumber IN  : org62UserMap.values()]){
            for (String v2momMethodExternalId : org62UserMap.keySet()) {                
                MeasuresWrapper MeasuresWrapperObject =  v2momMeasureMapParent.get(v2momMethodMeasureMap.get(v2momMethodExternalId));
                // MeasuresWrapper MeasuresWrapperObject =  v2momMeasureMapParent.get(v2momMethodExternalId);
                V2MOMContactWrapper contactWrapperObject = new V2MOMContactWrapper();
                contactWrapperObject.name = userObject.Name;
                Map<String, String> v2momNameMap = v2MomUserMapping.get(userObject.employeeNumber);
                contactWrapperObject.v2mom = v2momNameMap.get('Name');
                contactWrapperObject.v2momId = v2momNameMap.get('Id');
                contactWrapperObject.Id = userObject.Id;
                if(!Test.isRunningTest()) {
                    MeasuresWrapperObject.v2momContacts.add(contactWrapperObject);                  
                    v2momMeasureMapParent.put(MeasuresWrapperObject.externalId, MeasuresWrapperObject);                     
                }
                
            }
        }
        return v2momMeasureMapParent;
    }
    
    public static String changeDateFormat(Date dateToFormat) {
        return  dateToFormat.month() + '-' + dateToFormat.day() + '-' +dateToFormat.year();
        
    }
    
    // Method wrapper for the Method object
    public class V2MomMethodWrapper{
        @AuraEnabled public String id; // done
        @AuraEnabled public String externalId; // done
        @AuraEnabled public String methodName; // done
        @AuraEnabled public String description; // done
        @AuraEnabled public String priority; // done
        @AuraEnabled public String methodOwners; // done
        @AuraEnabled public List<ProjectWrapper> projectList = new List<ProjectWrapper>(); // need to discuss
        @AuraEnabled public List<V2MOMContactWrapper> v2momContacts = new List<V2MOMContactWrapper>(); // need to discuss
        @AuraEnabled public List<MeasuresWrapper> measures = new List<MeasuresWrapper>(); // done
    }
    
    // Project wrapper for the project object
    public class ProjectWrapper {
        @AuraEnabled public String name;
        @AuraEnabled public String startDate;
        @AuraEnabled public String endDate;
        @AuraEnabled public String status;
        @AuraEnabled public String overallStatus;        
        @AuraEnabled public String className;
    }
    
    // Contact wrapper for the contact object
    public class V2MOMContactWrapper {
        @AuraEnabled public String name;
        @AuraEnabled public String v2mom;
        @AuraEnabled public String id;
        @AuraEnabled public String v2momId;
    }
    
    // Measure wrapper for the measure object
    public class MeasuresWrapper {
        @AuraEnabled public String id;
        @AuraEnabled public String externalId;
        @AuraEnabled public String measureName;
        @AuraEnabled public Date dueDate;
        @AuraEnabled public String priority;
        @AuraEnabled public String status;
        @AuraEnabled public String methodExternalId;
        @AuraEnabled public List<ProjectWrapper> projectList = new List<ProjectWrapper>();
        @AuraEnabled public List<V2MOMContactWrapper> v2momContacts = new List<V2MOMContactWrapper>();
    }
}
/*
* Created by - Himanshu Shrimali
* Date - 19-12-2019
* This is a utility class for all the Test classes.
*/
public class SPT_TestUtility {

    // This method returns a list of case records.
    public static List<Case> createCaseRecords(String status, String caseCurrency, String caseOrigin, Integer count, Boolean isInsert) {
        List<Case> caseList = new List<Case>();
        for(Integer i=0; i<count; i++) {
            Case caseRecord = new Case();
            caseRecord.Status = status;
            caseRecord.CurrencyIsoCode = caseCurrency;
            caseRecord.Origin = caseOrigin;
            caseList.add(caseRecord);
        }
        if(isInsert && !caseList.isEmpty()) {
            INSERT caseList;
        }
        return caseList;
    }

    // This method returns a list of account records.
    public static List<Account> createAccountRecords(String accountName, Integer count, Boolean isInsert) {
        List<Account> accountList = new List<Account>();
        for(Integer i=0; i<count; i++) {
            Account accountRecord = new Account();
            accountRecord.Name = accountName+' '+(i+1);
            accountList.add(accountRecord);
        }
        if(isInsert && !accountList.isEmpty()) {
            INSERT accountList;
        }
        return accountList;
    }

    // This method returns a list of account records.
    public static List<Contact> createContactRecords(String lastName, Integer count, Boolean isInsert) {
        List<Contact> contactList = new List<Contact>();
        for(Integer i=0; i<count; i++) {
            Contact contactRecord = new Contact();
            contactRecord.LastName = lastName;
            contactList.add(contactRecord);
        }
        if(isInsert && !contactList.isEmpty()) {
            INSERT contactList;
        }
        return contactList;
    }

    // This method returns a list of case comment records.
    public static List<CaseComment> createCaseCommentRecords(Boolean isPublished, Integer count, Boolean isInsert) {
        List<CaseComment> caseCommentList = new List<CaseComment>();
        for(Integer i=0; i<count; i++) {
            CaseComment caseCommentRecord = new CaseComment();
            caseCommentRecord.IsPublished = isPublished;
            caseCommentList.add(caseCommentRecord);
        }
        if(isInsert && !caseCommentList.isEmpty()) {
            INSERT caseCommentList;
        }
        return caseCommentList;
    }
}
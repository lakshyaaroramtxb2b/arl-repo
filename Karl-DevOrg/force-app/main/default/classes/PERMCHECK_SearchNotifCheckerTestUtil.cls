@IsTest
public class PERMCHECK_SearchNotifCheckerTestUtil {

    public static PERMCHECK_Search_Notification__c[] createSearchNotifications() {
        PERMCHECK_Search_Notification__c n1,n2,n3 = null;
        n1 = new PERMCHECK_Search_Notification__c(Name='joe@joe.com',PERMCHECK_Active__c=True, PERMCHECK_Last_Record_Time__c=Datetime.now());
        n2 = new PERMCHECK_Search_Notification__c(Name='frank@frank.com',PERMCHECK_Active__c=True, PERMCHECK_Last_Record_Time__c=Datetime.now());
        n3 = new PERMCHECK_Search_Notification__c(Name='steve@steve.com',PERMCHECK_Active__c=False, PERMCHECK_Last_Record_Time__c=Datetime.now());
        PERMCHECK_Search_Notification__c[] nts = new PERMCHECK_Search_Notification__c[]{n1, n2, n3};
        insert nts;
        
        return nts;
    }
    
    public static PERMCHECK_Search_Hit__c[] createHitsForNotification(PERMCHECK_Search_Notification__c noti) {
        PERMCHECK_Search_Hit__c n1,n2,n3 = null;
        
        DateTime old = noti.PERMCHECK_Last_Record_Time__c.addDays(-1);
        DateTime current = noti.PERMCHECK_Last_Record_Time__c.addDays(1);
        n1 = new PERMCHECK_Search_Hit__c(PERMCHECK_Email__c='joe@joe.com',PERMCHECK_First_Name__c='joe',PERMCHECK_Last_Name__c='doe', 
                                        PERMCHECK_Execution_Time__c=old,PERMCHECK_Search_Name__c='blah', PERMCHECK_New_Result__c=False);
        n2 = new PERMCHECK_Search_Hit__c(PERMCHECK_Email__c='frank@frank.com',PERMCHECK_First_Name__c='frank',PERMCHECK_Last_Name__c='frank', 
                                        PERMCHECK_Execution_Time__c=current, PERMCHECK_Search_Name__c='blah', PERMCHECK_New_Result__c=False);
        n3 = new PERMCHECK_Search_Hit__c(PERMCHECK_Email__c='steve@steve.com',PERMCHECK_First_Name__c='steve',PERMCHECK_Last_Name__c='frank',
                                        PERMCHECK_Execution_Time__c=current, PERMCHECK_Search_Name__c='blah', PERMCHECK_New_Result__c=True);
        PERMCHECK_Search_Hit__c[] nts = new PERMCHECK_Search_Hit__c[]{n1, n2, n3};
        insert nts;
        
        return nts;
        
    }
}
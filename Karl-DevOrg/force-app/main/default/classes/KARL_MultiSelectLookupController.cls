/**
 * @author Lakshya Arora
 * @email lakshya.arora@mtxb2b.com
 * @description Handle karl_multi_select_lookup LWC component operations
 */
public with sharing class KARL_MultiSelectLookupController {
    /**
    * @author Lakshya Arora
    * @email lakshya.arora@mtxb2b.com
    * @description return the object records filter by field value
    * param1 objectName -> name of object
    * param2 value -> Search string value which will use in filterfield -> Text input
    * param3 selectedRecId -> Select record id
    * @return List of Wrapper - RecordsData
    */
    @AuraEnabled(cacheable=true)
    public static List<SObJectResult> getResults(String objectName, String value, List<String> selectedRecId) {
        objectName = String.escapeSingleQuotes(objectName.trim());
        value = String.escapeSingleQuotes(value.trim());
        List<SObJectResult> sObjectResultList = new List<SObJectResult>();
        if(selectedRecId == null)
            selectedRecId = new List<String>();

        if(String.isNotEmpty(value)) {
            if(objectName == 'Control_Test__c'){
                String query = 'SELECT Id,Name,Test_Name__c,KARL_Control_Scope_Number_Indexed__c FROM Control_Test__c WHERE KARL_Control_Scope_Number_Indexed__c LIKE \'%' + value + '%\'  AND Id NOT IN: selectedRecId WITH SECURITY_ENFORCED LIMIT 20';
                for(Control_Test__c controlTestObj : Database.Query(query)) {
                    String secondaryName = controlTestObj.Name;
                    if(String.isNotBlank(controlTestObj.Test_Name__c)){
                        secondaryName = secondaryName + ' - '+ controlTestObj.Test_Name__c;
                    }
                    sObjectResultList.add(new SObjectResult(controlTestObj.Id,controlTestObj.KARL_Control_Scope_Number_Indexed__c,secondaryName));
                }
            }
            else if(objectName == 'Area_Requirement__c'){
                String query = 'SELECT Id,Name,Requirement_Title__c,Requirement_Reference__c,Area_of_Compliance__c FROM Area_Requirement__c WHERE Name LIKE \'%' + value + '%\' and Id NOT IN: selectedRecId WITH SECURITY_ENFORCED LIMIT 20';
                for(Area_Requirement__c areaReqObj : Database.Query(query)) {
                    String secondaryName = areaReqObj.Requirement_Reference__c;
                    if(String.isNotBlank(areaReqObj.Area_of_Compliance__c)){
                        secondaryName = secondaryName + ' - '+areaReqObj.Area_of_Compliance__c;
                    }
                    if(String.isNotBlank(areaReqObj.Requirement_Title__c)){
                        secondaryName = secondaryName + ' - '+ areaReqObj.Requirement_Title__c;
                    }
                    sObjectResultList.add(new SObjectResult(areaReqObj.Id,areaReqObj.Name,secondaryName));
                }
            }
        }
        return sObjectResultList;
    }
    
    public class SObjectResult {
        @AuraEnabled public Id recId;
        @AuraEnabled public String recName;
        @AuraEnabled public String secName;
        
        public SObjectResult(Id recId,String recName,String secName) {
            this.recId = recId;
            this.recName = recName;
            this.secName = secName;
        }
    }
}
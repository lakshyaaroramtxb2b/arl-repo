@isTest
public class SPT_CaseHistoryController_Test {
    @TestSetup
    static void makeData(){
        date todaysDate = date.today();
        List<String> templateBody = new List<String>{'Status - {!Case.Status}', 'Origin - {!Case.Origin}','Published_Date:'+todaysDate};
        List<Case> caseList = SPT_TestUtility.createCaseRecords('In-Progress', 'USD', 'Email', 1, true);
        caseList.addAll(SPT_TestUtility.createCaseRecords('On Hold', 'USD', 'Email', 1, true));
        List<CaseComment> caseCommentList = SPT_TestUtility.createCaseCommentRecords(true, 1, false);
        caseCommentList.addAll(SPT_TestUtility.createCaseCommentRecords(false, 1, false));

        for(Integer i=0; i<caseCommentList.size(); i++) {
            CaseComment caseCommentRecord = caseCommentList.get(i);
            caseCommentRecord.CommentBody = templateBody.get(i);
            caseCommentRecord.ParentId = caseList.get(0).Id;            
        }
        if(!caseCommentList.isEmpty()) {
            INSERT caseCommentList;
        }

        Profile p = [SELECT Id
                     FROM Profile 
                     WHERE Name = 'System Administrator'];
        User user1 = new User(Alias = 'standt', Email='systemAdmin1@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Test User 1', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='systemAdmin1@testorg.com');
        User user2 = new User(Alias = 'standt', Email='systemAdmin2@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Test User 2', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='systemAdmin2@testorg.com');
    }

    @isTest
    public static void getCaseDataTest() {
        List<String> selectedPicklistValues = new List<String>{'Status', 'Public Outbound Comments', 'Public Inbound Comments', 'Private Outbound Comments', 'Private Inbound Comments'};
        List<Case> caseList = new List<Case>([SELECT Id, Status, Origin, CurrencyIsoCode FROM Case WHERE Status = 'In-Progress']);
        
        Case updateCase = caseList.get(0);
        System.debug('Case Data: '+updateCase.Status+', '+updateCase.Origin+', '+updateCase.CurrencyIsoCode);
        updateCase.Status = 'On Hold';
        UPDATE updateCase;
        System.debug('Case Data Updated: '+updateCase.Status+', '+updateCase.Origin+', '+updateCase.CurrencyIsoCode);
        
        CaseHistory caseH = new CaseHistory();
        caseH.Field = 'Status';
        caseH.CaseId = updateCase.Id;
        
        SPT_CaseHistoryController.ReturnDataWrapper returnDataWrapper = SPT_CaseHistoryController.getCaseData(updateCase.Id, selectedPicklistValues, false);     
        System.assertEquals(2, returnDataWrapper.caseWrapperList.size()); 

        List<User> userList = new List<User>([SELECT Id FROM User LIMIT 1]);
        System.runAs(userList.get(0)) {
            List<CaseComment> caseCommentList = SPT_TestUtility.createCaseCommentRecords(true, 1, false);
            caseCommentList.addAll(SPT_TestUtility.createCaseCommentRecords(false, 1, false));

            for(Integer i=0; i<caseCommentList.size(); i++) {
                CaseComment caseCommentRecord = caseCommentList.get(i);
                caseCommentRecord.CommentBody = 'Hey {!Case.Status}'+ System.Label.SPT_BotTag;
                caseCommentRecord.ParentId = updateCase.Id;            
            }
            if(!caseCommentList.isEmpty()) {
                INSERT caseCommentList;
            }
        }
        returnDataWrapper = SPT_CaseHistoryController.getCaseData(updateCase.Id, selectedPicklistValues, true);     
        System.assertEquals(6, returnDataWrapper.caseWrapperList.size()); 
    }

    @isTest
    public static void getAllFilterOptionsTest() {
        List<SPT_CaseHistoryController.FilterOptionsWrapper> filterOptionsList = SPT_CaseHistoryController.getAllFilterOptions();
        System.assertEquals(7, filterOptionsList.size());
    }

    @isTest
    public static void coverSptCaseDataWrapper() {
        Datetime timeTag = Datetime.now();
        SPT_CaseDataWrapper dataWrapper = new SPT_CaseDataWrapper(timeTag, 'TestHistoryType', 'TestUser',
                                                                  'New', 'Closed', 'Status', null, 'ASC');
        // Empty value check
        dataWrapper = new SPT_CaseDataWrapper(timeTag, 'TestHistoryType', 'TestUser',
                                              '', '', 'Status', null, 'ASC');
    }
}
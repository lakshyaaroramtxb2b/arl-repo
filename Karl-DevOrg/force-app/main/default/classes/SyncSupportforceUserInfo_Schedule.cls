/**
 * @author: Suresh Uppala
 */
public class SyncSupportforceUserInfo_Schedule implements Schedulable {
    public void execute(SchedulableContext sc) {
        Database.executeBatch((Database.Batchable<SObject>) 
            Type.forName('SyncSupportforceUserInfo').newInstance(), 100);
    }
}
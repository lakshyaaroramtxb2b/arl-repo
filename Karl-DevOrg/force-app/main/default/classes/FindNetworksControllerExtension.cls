public class FindNetworksControllerExtension 
{
    private string ipString;
    private Long ipNum;
    private List<Networks__c> networks;
    private ApexPages.StandardController stdController;
    private boolean forceUpdate;
    
    public FindNetworksControllerExtension(ApexPages.StandardController stdController) {
        this.stdController = stdController;
        
        this.ipString = ApexPages.currentPage().getParameters().get('ip');
        
        this.ipNum = IPConverter.parseIp(this.ipString);  
        
        if(ApexPages.currentPage().getParameters().get('forceUpdate') == null)
        	forceUpdate = false;
        else
        	forceUpdate = boolean.valueOf(ApexPages.currentPage().getParameters().get('forceUpdate'));
    }

	public List<Networks__c> getNetworks() {

		return QueryNetworks();
	}
    
    public List<Networks__c> QueryNetworks() {
 
		networks = [SELECT Id, Name, Network_Description__c, Environment__c, Network_Name__c, Country__c, Start_IP__c, End_IP__c, RecordType.Name FROM Networks__c WHERE IP_Start_Num__c <= :this.ipNum AND IP_End_Num__c >= :this.ipNum];
        
		return networks;
	}
    
    public String ExtractValueMaxmindData(String searchTerm, String maxMindData)
    {
        Integer startIndex = maxMindData.indexOf(searchTerm);
        
        if(startIndex < 0)
			return '';
        
        Integer endIndex = maxMindData.indexOf(';', startIndex);
        
        if(endIndex < 0)
			endIndex = maxMindData.length();
        
        return maxMindData.substring(maxMindData.indexOf(searchTerm) + searchTerm.length(), endIndex);
    }
    
    public String getCalloutResponseContents(String url) {
        // Instantiate a new http object
        Http h = new Http();
    
        // Instantiate a new HTTP request, specify the method (GET) as well as the endpoint
        HttpRequest req = new HttpRequest();
        req.setEndpoint(url);
        req.setMethod('GET');
    
        // Send the request, and return a response
        HttpResponse res = h.send(req);
        
        return res.getBody();
  }
    
    public Boolean IsPrivateSpace(Long ip)
    {
        //10.0.0.0
        if( ip >= Long.valueOf('167772160') && ip <= Long.valueOf('184549375'))
            return true;
        
        //192.168.0.1
        if( ip >= Long.valueOf('3232235520') && ip <= Long.valueOf('3232301055'))
            return true;
        
        //172.0.0.0
        if( ip >= Long.valueOf('2886729728') && ip <= Long.valueOf('2887778303'))
            return true;
        
        return false;
    }
    
    public PageReference redirectToNetwork()
    { 
        if(networks == null) networks = [SELECT Id, Name, Network_Description__c, Environment__c, Network_Name__c, Country__c, Start_IP__c, End_IP__c FROM Networks__c WHERE IP_Start_Num__c <= :this.ipNum AND IP_End_Num__c >= :this.ipNum];
        
        //if record and is private space then we want to go straigh tot record as we wont have multiple
        if(!IsPrivateSpace(this.ipNum) && (networks.size() == 1 && !forceUpdate))
        {
            Pagereference pg =  new Pagereference('/' + networks[0].Id);
            pg.setRedirect(true);
                
            return pg;             
        }
        
        if(networks.size() == 0 || forceUpdate)
        {
            //No record exits for this lookup
            if(IsPrivateSpace(this.ipNum))
            {
                //cannot lookup
            	ApexPages.Message msg = new ApexPages.Message(Apexpages.Severity.WARNING, 'No record for SFDC IP address ' + this.ipString );
            	ApexPages.addMessage(msg);
            }
            else
            {
                //public we can lookup
                String maxMindData = getCalloutResponseContents('https://minfraud.maxmind.com/app/ccv2r?i=' + ipString + '&license_key=AtKgJhSXtOiO');
 
                Networks__c newNetwork = new Networks__c();
                
                if(maxMindData.indexOf('err=') > -1)
                {
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    String[] toAddresses = new String[] {'amcneilly@salesforce.com'};
                    mail.setToAddresses(toAddresses);
                    mail.setReplyTo('amcneilly@salesforce.com');
                    mail.setSenderDisplayName('adam mcneilly');
                    mail.setSubject('Maxmind Error on lookup');
                    mail.setPlainTextBody(maxMindData);
                    //Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
                }
                
                newNetwork.MaxMindResponseRaw__c = maxMindData;
                newNetwork.Start_IP__c = ipString;
                newNetwork.End_IP__c = ipString;
                newNetwork.IP_Start_Num__c = this.ipNum;
                newNetwork.IP_End_Num__c = this.ipNum;
                newNetwork.Network_Name__c = ipString;
                
                newNetwork.RecordTypeId = Schema.SObjectType.Networks__c.getRecordTypeInfosByName().get('Public').getRecordTypeId();
                
                newNetwork.LookupDate__c = DateTime.now();
                
                if(maxMindData != null)
                {
                    newNetwork.Network_Description__c = 'Public';
                    newNetwork.Country__c = ExtractValueMaxmindData('ip_countryName=', maxMindData);
                    newNetwork.City__c = ExtractValueMaxmindData('ip_city=', maxMindData);
                    newNetwork.IP_Domain__c = ExtractValueMaxmindData('p_domain=', maxMindData);
                    newNetwork.Corporate_Proxy__c = ExtractValueMaxmindData('ip_corporateProxy=', maxMindData) == 'Yes' ? true : false;
                    
                    newNetwork.GeoLat__c = ExtractValueMaxmindData('ip_latitude=', maxMindData);
                    newNetwork.GeoLong__c = ExtractValueMaxmindData('ip_longitude=', maxMindData);
                   
                    newNetwork.Anonymous_Proxy__c = ExtractValueMaxmindData('anonymousProxy=', maxMindData) == 'Yes' ? true : false;
                    newNetwork.Proxy_Score__c = ExtractValueMaxmindData('proxyScore=', maxMindData);
                    newNetwork.ISP__c = ExtractValueMaxmindData('ip_isp=', maxMindData);
                    newNetwork.ISP_Org__c = ExtractValueMaxmindData('ip_org=', maxMindData);
                    newNetwork.Net_Speed__c = ExtractValueMaxmindData('ip_netSpeedCell=', maxMindData);
                    newNetwork.service_level__c = ExtractValueMaxmindData('service_level=', maxMindData);
                    newNetwork.Risk_Score__c = ExtractValueMaxmindData('riskScore=', maxMindData);
                }
                
                insert newNetwork;
                
				Pagereference pg =  new Pagereference('/' + newNetwork.Id);
                pg.setRedirect(true);
                
                return pg;                
            }
        }

        return null;
    }
   
    public pagereference pageReturn()
    {
        Pagereference pg =  new Pagereference(ApexPages.currentPage().getParameters().get('retURL'));
        pg.setRedirect(true);
        
        return pg;
    }
}
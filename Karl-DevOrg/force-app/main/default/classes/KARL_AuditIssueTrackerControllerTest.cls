@isTest
public class KARL_AuditIssueTrackerControllerTest {
	/**
    * @author Lakshya Arora
    * @email lakshya.arora@mtxb2b.com
    * @desc method to test the getAuditIssueTrackingInfo functionality.
    */
    @isTest
    private static void getAuditIssueTrackingInfoTest(){
        List<Task> taskList = [SELECT Id,WhatId,Status FROM Task ];
        test.startTest();
        taskList[0].Status = KARL_Constants.TASK_STATUS_COMPLETED;
        update taskList;
        test.stopTest();
        System.assert([SELECT Id,Status__c FROM KARL_Cycle_Audit_Report_Items__c][0].Status__c == KARL_Constants.REPORT_STATUS_READY_TO_SIGN);
    }
}
@isTest
public class SPT_CaseAndCaseCommentBatch_Test {
    @TestSetup
    static void makeData(){
         date todaysDate = date.today();
        List<String> templateBody = new List<String>{'Status - {!Case.Status}', 'Origin - {!Case.Origin}','Published_Date:'+todaysDate};
        List<Case> caseList = SPT_TestUtility.createCaseRecords('In-Progress', 'USD', 'Email', 1, true);
        caseList.addAll(SPT_TestUtility.createCaseRecords('On Hold', 'USD', 'Email', 1, true));
        List<CaseComment> caseCommentList = SPT_TestUtility.createCaseCommentRecords(true, 1, false);
        caseCommentList.addAll(SPT_TestUtility.createCaseCommentRecords(false, 1, false));

        for(Integer i=0; i<caseCommentList.size(); i++) {
            CaseComment caseCommentRecord = caseCommentList.get(i);
            caseCommentRecord.CommentBody = templateBody.get(i);
            caseCommentRecord.ParentId = caseList[0].Id;            
        }

        Case__x externalCaseRecord = new Case__x();
        externalCaseRecord.RecordTypeId__c = SPT_Constants.SF_RECORDTYPE_ID;
        
        List<Contact> contactLIst = SPT_TestUtility.createContactRecords('TestSW',1,true);

        List<Case> internalExternalCaseList = SPT_TestUtility.createCaseRecords('New', 'USD', 'Web', 1, false);
        for(Case caseRecord : internalExternalCaseList) {
            caseRecord.RecordTypeId = SPT_Constants.SEC_RECORDTYPE_ID;
            caseRecord.ContactId = contactLIst[0].id;
        }
        INSERT internalExternalCaseList;   
        
        BatchSchedule__c b = new BatchSchedule__c();
        b.Name = 'Schedule';
        b.Last_Schedule__c = System.now();
        insert b;
    }
   
    @isTest
    static void checkBatchUpdate() {
        Test.startTest();
        user u= [SELECt id FROM user where id=: userInfo.getUserId()];
        SPT_CaseAndCaseCommentBatch batch1 = new SPT_CaseAndCaseCommentBatch();
        system.runAs(u){
            SPT_CaseAndCaseCommentBatch batch = new SPT_CaseAndCaseCommentBatch(false, false);
            Database.executeBatch(batch);
        }
        Test.stopTest();
    }
    @isTest
    static void checkBatchUpdate2(){
        test.startTest();
        //SPT_CaseAndCaseCommentBatch batch = new SPT_CaseAndCaseCommentBatch(false, false);
        test.stopTest();
    }
}
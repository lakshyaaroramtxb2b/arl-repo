/**
 * @File Name          : PRT_V2MOMNotificationBatch.cls
 * @Description        : Batch for V2MOM SLA send email notifications
 * @Author             : virendra.yadav
 * @Group              : 
 * @Last Modified By   : virendra.yadav, Swarnima Singh Mandhata
 * @Last Modified On   : 1/10/2020, 1:40:53 PM 
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    1/10/2020   virendra.yadav      Initial Version
 **/
public class PRT_V2MOMNotificationBatch implements Database.Batchable < sObject > , Schedulable {
    @TestVisible
    private static list < sObject > mockallGusV2MomList = new list < sObject > ();
    @TestVisible
    private static list<sObject> mockAllMeasureList = new list<sObject>();
    @TestVisible
    private static list<sObject> mockAllMethodList = new list<sObject>();
    @TestVisible 
    private static list<sObject> mockAllOrg62List = new list<sObject>();

    static Map < String, V2MOM_c__x > v2momUserMap = new Map < String, V2MOM_c__x > ();

    public Database.QueryLocator start(Database.BatchableContext BC) {
        if (Test.isRunningTest()) {
            return Database.getQueryLocator([SELECT Id FROM User LIMIT 1]);
        } else {
            return Database.getQueryLocator([SELECT Published_on_c__c, Id, V2MOM_User_c__c, ExternalId, Name__c, Status_c__c FROM V2MOM_c__x WHERE Published_on_c__c != null]);
        }
    }
    public void execute(Database.BatchableContext BC, List < sObject > v2momRecords) {
        Set < String > grcMembersEmployeeNumber = new Set < String > ();
        Date today = Date.today();
        Date oneDayPriorToDueDate = today.addDays(-4);
        Date dueDate = today.addDays(-5);
        List < Id > todayV2momUserList = new List < Id > ();
        List < Id > oneDayPriorV2momUserList = new List < Id > ();
        List < Id > dueDateV2momUserList = new List < Id > ();
        Set < Id > allV2momSet = new Set < Id > ();
        String chatterGroupName;
        if(!Test.isRunningTest()){
            chatterGroupName = 'Head of GRC';
        }else{
            chatterGroupName = 'Test Head of GRC';            
        }

        for (CollaborationGroupMember collGroupMemberList: [SELECT Id, CollaborationGroupId, MemberId, Member.EmployeeNumber 
                                                            FROM CollaborationGroupMember WHERE CollaborationGroup.Name =:chatterGroupName ]) {
            if (collGroupMemberList.Member.EmployeeNumber != null) {
                grcMembersEmployeeNumber.add(collGroupMemberList.Member.EmployeeNumber);
            }
            System.debug(collGroupMemberList);
        }
        if (Test.isRunningTest()) {
            v2momRecords = mockallGusV2MomList;
        }
        for (V2MOM_c__x v2momObject: (List < V2MOM_c__x > ) v2momRecords) {
            v2momUserMap.put(v2momObject.V2MOM_User_c__c, v2momObject);
            allV2momSet.add(v2momObject.ExternalId);
        }
        Set < String > employeeNumbers = new Set < String > ();
        List < User > users = new List < User > ();
        Map < String, String > employeeNumberVsExternalId = new Map < String, String > ();
        Set < String > allowedNotificationsUserEMpSet = new Set < String > ();
        if(test.isRunningTest()){
            //Org62User__x org62UserObject =(Org62User__x)mockAllOrg62List[0].getSObject('Org62User__x');
            Org62User__x org62UserObject = (Org62User__x)mockAllOrg62List[0];
            System.debug('org62UserObject --> ' + org62UserObject);
            
            if (org62UserObject.EmployeeNumber__c != null && org62UserObject.EmployeeNumber__c != '') {
                                                       employeeNumberVsExternalId.put(org62UserObject.externalId, org62UserObject.EmployeeNumber__c);
                                                   }
        }else{
            for (Org62User__x org62UserObject: [SELECT Id, name__c, externalId, EmployeeNumber__c FROM Org62User__x
                                                WHERE externalId IN: v2momUserMap.Keyset()
                                               ]) {
                                                   if (org62UserObject.EmployeeNumber__c != null && org62UserObject.EmployeeNumber__c != '') {
                                                       employeeNumberVsExternalId.put(org62UserObject.externalId, org62UserObject.EmployeeNumber__c);
                                                   }
                                               }
        }
        
        for (Contact conRec: [SELECT ID, Title, Employee_ID__c, Management_Chain_Level_01__c,
                Management_Chain_Level_02__c, Management_Chain_Level_03__c,
                Management_Chain_Level_04__c, Management_Chain_Level_05__c,
                Management_Chain_Level_06__c, Management_Chain_Level_07__c,
                Management_Chain_Level_08__c, Management_Chain_Level_09__c,
                Management_Chain_Level_10__c, Management_Chain_Level_11__c,
                Management_Chain_Level_12__c, Management_Chain_Level_13__c,
                Management_Chain_Level_14__c, Management_Chain_Level_15__c
                FROM Contact WHERE Employee_ID__c in: employeeNumberVsExternalId.values()
            ]) {
            if (grcMembersEmployeeNumber.contains(conRec.Employee_ID__c)) {
                allowedNotificationsUserEMpSet.add(conRec.Employee_ID__c);
            }
            String fieldName;
            for (Integer i = 1; i <= 15; i++) {
                if (i <= 9) {
                    fieldName = 'Management_Chain_Level_0' + i + '__c';
                } else {
                    fieldName = 'Management_Chain_Level_' + i + '__c';
                }
                if (conRec.get(fieldName) != null) {
                    String EmpNo = ((String) conRec.get(fieldName)).substringBetween('(', ')');
                    if (grcMembersEmployeeNumber.contains(EmpNo)) {
                        allowedNotificationsUserEMpSet.add(conRec.Employee_ID__c);
                    }
                }
            }

        }


        //List < V2MOM_Method_c__x > allMethods = new List < V2MOM_Method_c__x > ();
        Map<Id, V2MOM_Method_c__x> methodIDdoRecordMap = new Map<Id, V2MOM_Method_c__x>();
        if(test.isRunningTest()){
            V2MOM_Method_c__x method = (V2MOM_Method_c__x)mockAllMethodList[0];
            //allMethods.add(method);
            methodIDdoRecordMap.put(method.id,method);
        }else{
            for (V2MOM_Method_c__x method: [SELECT Id, ExternalId, V2MOM_c__c, Name__c
                                            FROM V2MOM_Method_c__x WHERE V2MOM_c__c IN: allV2momSet
                                           ]) {
                                               //allMethods.add(method);
                                               methodIDdoRecordMap.put(method.Id,method);
                                           }
        }
        
        Set < Id > mappingIDRec = new Set < ID > ();
        for (V2MomMapping__c v2momMapping: [SELECT Id, Dependent_Method__c FROM V2MomMapping__c
                WHERE Dependent_Method__r.V2MOM_c__c IN: allV2momSet
            ]) {
            mappingIDRec.add(v2momMapping.Dependent_Method__c);
        }
        Set < Id > notAllignedV2MomSet = new Set < ID > ();
        if(test.isRunningTest()){
            V2MOM_Method_c__x method = (V2MOM_Method_c__x)mockAllMethodList[0];
            if (!mappingIDRec.contains(method.ExternalId)) {
                notAllignedV2MomSet.add(method.V2MOM_c__c);
            }
        }else{
            for (V2MOM_Method_c__x method: methodIDdoRecordMap.values()) {
                if (!mappingIDRec.contains(method.ExternalId)) {
                    notAllignedV2MomSet.add(method.V2MOM_c__c);
                }
            }
        }
       
        for (V2MOM_c__x v2momObject: (List < V2MOM_c__x > ) v2momRecords) {
            if (notAllignedV2MomSet.contains(v2momObject.ExternalId)) {
                if (v2momObject.Published_on_c__c == today) {
                    todayV2momUserList.add(v2momObject.V2MOM_User_c__c);
                } else if (v2momObject.Published_on_c__c == oneDayPriorToDueDate) {
                    oneDayPriorV2momUserList.add(v2momObject.V2MOM_User_c__c);
                } else if (v2momObject.Published_on_c__c == dueDate) {
                    dueDateV2momUserList.add(v2momObject.V2MOM_User_c__c);
                }
            }
        }
        if (todayV2momUserList != null && !todayV2momUserList.isEmpty()) {
            String inputText_Today = Label.PRT_V2momNotification5Day;
            Set < User > todayV2momUsers = getContacts(todayV2momUserList, allowedNotificationsUserEMpSet);
            postChatterNotification(todayV2momUsers, inputText_Today);
            System.debug('todayV2momUsers ->' + todayV2momUsers);
            System.debug('inputText_Today ->' + inputText_Today);

        }
        if (oneDayPriorV2momUserList != null && !oneDayPriorV2momUserList.isEmpty()) {
            Set < User > oneDayPriorV2momUsers = getContacts(oneDayPriorV2momUserList, allowedNotificationsUserEMpSet);
            String inputText_One_day = Label.PRT_V2momNotificationToday;
            postChatterNotification(oneDayPriorV2momUsers, inputText_One_day);
        }
        if (dueDateV2momUserList != null && !dueDateV2momUserList.isEmpty()) {
            String inputText_Due_Date = Label.PRT_V2momNotificationDueDate;
            Set < User > dueDateV2momUsers = getContacts(dueDateV2momUserList, allowedNotificationsUserEMpSet);
            postChatterNotification(dueDateV2momUsers, inputText_Due_Date);
        }
    }
    public static void postChatterNotification(Set < User > userObjects, String notificationTemplate) {
        List < FeedItem > feedItemPostList = new List < FeedItem > ();
        String changedTextInput = '';
        String textBody;
        System.debug('userObject ' + userObjects);
        System.debug('textBody' + textBody);

        for (User user: userObjects) {

            textbody = notificationTemplate;
            FeedItem post = new FeedItem();
            post.ParentId = user.Id;
            System.debug('USer ' + user);
            ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
            mentionSegmentInput.id = user.id;
            if(v2momUserMap.containsKey(user.employeeNumber)){
                System.debug('user.employeeNumber-->'+ user.employeeNumber);
                V2MOM_c__x v2mom = v2momUserMap.get(user.employeeNumber);
                
                String recordUrl = +URL.getSalesforceBaseUrl().toExternalForm() + '/lightning/r/V2MOM_c__x/' + v2mom.Id + '/view\"';
                if (user != null) {
                    if (textBody != null) {
                        if (textBody.contains('{User Name}')) {
                            textBody = textBody.replace('{User Name}', user.Name);
                        }
                        System.debug('V2mom Record ' +v2mom + 'Text Body' +  textBody);
                        if (textBody.contains('{V2MOM Name}')) {
                            textBody = textBody.replace('{V2MOM Name}', v2mom.Name__c);
                        }
                        if (user.ManagerId != null && textBody.contains('{Manager Name}')) {
                            textBody = textBody.replace('{Manager Name}', user.Manager.Name);
                        }
                        if (textBody.contains('{Record Link}')) {
                            textBody = textBody.replace('{Record Link}', recordUrl);
                        }
                        changedTextInput = textBody;
                        system.debug('changed text ' + changedTextInput);
                    }
                    post.Body = changedTextInput;
                    feedItemPostList.add(post);
                }
            }
        }
        if (!feedItemPostList.isEmpty() || feedItemPostList != null) {
            insert feedItemPostList;
        }
    }

    /**
     * @description get contacts of v2momIdList
     * @author virendra.yadav  | 1/10/2020 
     * @param v2momUserList 
     * @return void 
     **/
    public static Set < User > getContacts(List < Id > v2momUserList, Set < String > allowedNotificationsUserEMpSet) {
        Set < String > employeeNumbers = new Set < String > ();
        Set < User > users = new Set < User > ();
        if (v2momUserList != null && !v2momUserList.isEmpty()) {
            if(test.isRunningTest()){
                Org62User__x org62UserObject = (Org62User__x)mockAllOrg62List[0];
                V2MOM_c__x v2momObject = v2momUserMap.get(org62UserObject.externalId);
                v2momUserMap.put(org62UserObject.EmployeeNumber__c, v2momObject);
                employeeNumbers.add(org62UserObject.EmployeeNumber__c);
            }else{
                for (Org62User__x org62UserObject: [SELECT Id, name__c, externalId, EmployeeNumber__c FROM Org62User__x WHERE externalId IN: v2momUserList]) {
                    V2MOM_c__x v2momObject = v2momUserMap.get(org62UserObject.externalId);
                    v2momUserMap.put(org62UserObject.EmployeeNumber__c, v2momObject);
                    employeeNumbers.add(org62UserObject.EmployeeNumber__c);
                }
            }
            
        }
        if (employeeNumbers != null && employeeNumbers.size() != 0) {
            for (User userObject: [SELECT ID, Name, ManagerId, Manager.Name, employeeNumber, Manager.email, Contact.name, contactId, email
                    FROM User WHERE employeeNumber IN: employeeNumbers
                    AND employeeNumber IN: allowedNotificationsUserEMpSet
                ]) {
                users.add(userObject);
            }
        }

        return users;
    }

    public void finish(Database.BatchableContext BC) {
        if (!Test.isRunningTest()) {
            PRT_V2MOMNotificationBatch batch = new PRT_V2MOMNotificationBatch();

        }
    }
    public void execute(SchedulableContext SC) {
        database.executebatch(new PRT_V2MOMNotificationBatch());
    }
}
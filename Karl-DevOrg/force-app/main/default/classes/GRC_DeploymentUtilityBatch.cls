/*
* Class Name: GRC_DeploymentUtilityBatch
* Description: Utility Batch class for updating existing records.
* Author/Date:  Banshi / 01.29.2020
* Date New/Modified: 01.29.2020
*/
global class GRC_DeploymentUtilityBatch implements Database.Batchable<sObject> {
    public Boolean ACTION_UPDATE_EXTERNAL_FIELDS = false;
    public Boolean ACTION_UPDATE_ESA_PROFILE = false;
    global Database.QueryLocator start(Database.BatchableContext bc) {
        // collect the batches of records or objects to be passed to execute
        String query;
        if(ACTION_UPDATE_EXTERNAL_FIELDS){
            query = 'SELECT id,Cloud__r.Name__c,Team_Responsible__r.Name__c,Security_Assessment_Record__r.Name__c,GUS_Cloud_Name__c,GUS_Team_Name__c,GUS_Security_Assessment_Name__c FROM Case Where RecordType.Name = \'IEM\' AND (GUS_Cloud_Name__c = null OR GUS_Team_Name__c = null OR GUS_Security_Assessment_Name__c =null) and (Cloud__c != null OR Team_Responsible__c != null OR Security_Assessment_Record__c != null) ORDER By CreatedDate DESC';
        }else if(ACTION_UPDATE_ESA_PROFILE){
            Profile p = [SELECT ID FROM profile WHERE name='ESA HVCP'];
            if(p!=null){
                String profileId = p.Id;
                if(Test.isRunningTest()){
                    query = 'SELECT id, ProfileId,UserRoleId From User Where ProfileId =:profileId limit 1';
                }
                else    {
                    query = 'SELECT id, ProfileId,UserRoleId From User Where ProfileId =:profileId';
               }
            }
        }
        
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext bc, List<sObject> records){
        if(ACTION_UPDATE_EXTERNAL_FIELDS){
            List<Case> caseList = new List<Case>();
            // process each batch of records
            for(Case newCase : (List<Case>)records){
                newCase.GUS_Team_Name__c  = newCase.Team_Responsible__r.Name__c;
                newCase.GUS_Cloud_Name__c   = newCase.Cloud__r.Name__c;
                newCase.GUS_Security_Assessment_Name__c   = newCase.Security_Assessment_Record__r.Name__c;
                caseList.add(newCase);
            }
            update caseList;
        }else if(ACTION_UPDATE_ESA_PROFILE){
            List<User> updatedUserlst = new List<User>();
            Profile newProfile = [SELECT ID FROM profile WHERE name='ESA CCP'];
            if(newProfile!=null){
                for(User u : (List<User>)records)
                {
                    u.profileID = newProfile.id;
                    updatedUserlst.add(u);
                }
                update updatedUserlst;
            }
            
        }
        
    }    
    global void finish(Database.BatchableContext bc){
        // execute any post-processing operations
    }    
}
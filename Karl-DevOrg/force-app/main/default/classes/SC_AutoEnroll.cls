/**
 * @author: ralph@callaway.cloud
 * @description: auto-enroll a course
 */
public class SC_AutoEnroll {
    
    private Training_Course__c course;

    public SC_AutoEnroll(Id courseId) {
        this(getCourse(courseId));
    }

    public SC_AutoEnroll(Training_Course__c course) {
        this.course = course;
    }

    /**
     * Run auto enrollment. Only runs if number of candidates is greater than zero
     * @return Number of candidates
     */
    public Integer run() {
        // check course eligibility
        System.assert(course.Tracked__c, 'Course not tracked');
        System.assert(course.Auto_Enrollment_Enabled__c, 'Auto enrollment not enabled');
        System.assertNotEquals(null, course.Auto_Enroll_Where_Clause_10K__c, 'Blank Auto Enroll Where Clause');

        // prepare options
        SC_Enroller.Options opts = new SC_Enroller.Options();
        opts.courseId = course.Id;
        opts.isMandatory = true;
        opts.sendNotification = true;
        opts.dueDate = (course.Default_Due_Date__c != null) ? 
            course.Default_Due_Date__c : 
            Date.today().addDays(course.Default_Due_Days__c.intValue());
        if (String.isBlank(course.Org_Wide_Email_Id__c)) {    
            opts.orgWideEmailId = SC_Constants.SC_ORG_WIDE_EMAIL_ID;
        } else {
            opts.orgWideEmailId = course.Org_Wide_Email_Id__c;
        }        
        opts.templateId = getTemplate(course.Default_Enrollment_Template_DevelopName__c);
        
        // check options validity
        System.assertNotEquals(null, opts.templateId, 'Template Not Found');
        System.assertNotEquals(null, opts.dueDate, 'Blank due date');
        
        // build filter
        Date minHireDate = Date.today().addDays(1); // don't enroll people on their first day
        String filter = 'Hire_Date__c < ' + DynamicSOQLHelper.format(minHireDate) + ' ' +
            'AND ' + course.Auto_Enroll_Where_Clause_10K__c; 
        
        // check candidate count and run if needed
        Integer candidates = getCandidateCount(course.Id, filter);
        if (candidates > 0) {
            Database.executeBatch(new SC_Enrollment_Batch(filter, opts));
        }
        return candidates;
    }

    private Id getTemplate(String developerName) {
        for (EmailTemplate template : [
            SELECT Id FROM EmailTemplate
            WHERE DeveloperName = :developerName
        ]) {
            return template.Id;
        }
        return null;
    }

    private static Training_Course__c getCourse(Id courseId) {
        return [
            SELECT 
                Auto_Enroll_Where_Clause_10K__c,
                Auto_Enrollment_Enabled__c,
                Default_Due_Date__c,
                Default_Due_Days__c,
                Default_Enrollment_Template_DevelopName__c,
                Tracked__c,
                Org_Wide_Email_Id__c,
                Id
            FROM Training_Course__c
            WHERE Id = :courseId
        ];
    }

    private Integer getCandidateCount(Id courseId, String filter) {
        String query = 'SELECT COUNT(Id) cnt FROM Contact ' + 
            // only valid contacts
            'WHERE RecordTypeId = ' + 
                DynamicSOQLHelper.format(Contact_Constants.RT_ID_SALESFORCE_EMPLOYEE) + ' ' +
            'AND Email != null ' +
            'AND Org62_User_Id__c LIKE ' +
                DynamicSOQLHelper.format('005%') + ' ' +
            // only enrollable contacts
            'AND Is_Active__c = true ' +
            'AND Is_Frozen__c = false ' + 
            'AND Is_On_Leave_Of_Absence__c = false ' +
            // only contacts matching auto-enroll filter
            'AND ' + filter + ' ' +
            // only contacts that don't already have a mandatory or blocked enrollment
            'AND Id NOT IN (' +
                'SELECT Contact__c FROM Training_Course_Taken__c ' +
                'WHERE Training_Course__c = ' + DynamicSOQLHelper.format(courseId) + ' ' +
                'AND (' +
                    'Block_Enrollment__c = true ' +
                    'OR RecordTypeId = ' + 
                        DynamicSOQLHelper.format(TrainingCourseTaken_Constants.RT_ID_MANDATORY) + '))'; 
        AggregateResult agg = Database.query(query);
        return (Integer) agg.get('cnt');
    }

}
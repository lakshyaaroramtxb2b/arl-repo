/*
 * class used to support Escalation Profiles
*/

public class SC_EscalationProfileEngine {

    private static Map<String, List<String>> errorProfileMap = new Map<String, List<String>>();

    private static final String DEFAULT_EMAIL_TEMPLATE = '';

    public static Map<Id, Training_Escalation_Profile__c> profileMap {
        get {
            if (SC_EscalationProfileEngine.profileMap == null) {
                SC_EscalationProfileEngine.profileMap = new Map<Id, Training_Escalation_Profile__c>([
                    select
                        Id,
                        Name,
                        First_Reminder_Days__c,
                        First_Reminder_Email_Template__c,
                        First_Reminder_Include_Manager__c,
                        Overdue_Reminder_Email_Template__c,
                        Overdue_Reminder_Frequency_Days__c,
                        Overdue_Reminder_Include_Manager__c,
                        Second_Reminder_Days__c,
                        Second_Reminder_Email_Template__c,
                        Second_Reminder_Include_Manager__c,
                        Third_Reminder_Days__c,
                        Third_Reminder_Email_Template__c,
                        Third_Reminder_Include_Manager__c
                    from Training_Escalation_Profile__c
                ]);
            }
            return SC_EscalationProfileEngine.profileMap;
        }
        private set;
    }

    public static Date upperDateBound {
        get {
            if (SC_EscalationProfileEngine.upperDateBound == null) {
                Decimal upperDays = 0.0;

                for (Training_Escalation_Profile__c profile :SC_EscalationProfileEngine.profileMap.values()) {
                    upperDays =
                        (profile.First_Reminder_Days__c > upperDays) ? profile.First_Reminder_Days__c : upperDays;
                }
                SC_EscalationProfileEngine.upperDateBound = System.today().addDays(Integer.valueOf(upperDays));
            }
            return SC_EscalationProfileEngine.upperDateBound;
        }
        private set;
    }

    public static Map<String, EmailTemplate> emailTemplateMap {
        get {
            if (SC_EscalationProfileEngine.emailTemplateMap == null) {
                Set<String> templateNames = new Set<String>();
                SC_EscalationProfileEngine.emailTemplateMap = new Map<String, EmailTemplate>();

                for (Training_Escalation_Profile__c profile :SC_EscalationProfileEngine.profileMap.values()) {
                    if (profile.First_Reminder_Email_Template__c != null) {
                        templateNames.add(profile.First_Reminder_Email_Template__c);
                    }
                    if (profile.Second_Reminder_Email_Template__c != null) {
                        templateNames.add(profile.Second_Reminder_Email_Template__c);
                    }
                    if (profile.Third_Reminder_Email_Template__c != null) {
                        templateNames.add(profile.Third_Reminder_Email_Template__c);
                    }
                    if (profile.Overdue_Reminder_Email_Template__c != null) {
                        templateNames.add(profile.Overdue_Reminder_Email_Template__c);
                    }
                }

                for (EmailTemplate temp :[
                    select Id, DeveloperName, Subject, HtmlValue
                    from EmailTemplate
                    where DeveloperName in :templateNames
                ]) {
                    SC_EscalationProfileEngine.emailTemplateMap.put(temp.DeveloperName, temp);
                }
            }
            return SC_EscalationProfileEngine.emailTemplateMap;
        }
        private set;
    }

    public static Set<Id> validProfileIds {
        get {
            if (SC_EscalationProfileEngine.validProfileIds == null) {
                Boolean isValid;
                SC_EscalationProfileEngine.validProfileIds = new Set<Id>();

                for (Training_Escalation_Profile__c profile :SC_EscalationProfileEngine.profileMap.values()) {
                    isValid = true;
                    if (profile.First_Reminder_Email_Template__c != null &&
                        !SC_EscalationProfileEngine.emailTemplateMap.containsKey(
                            profile.First_Reminder_Email_Template__c
                        )
                    ) {
                        SC_EscalationProfileEngine.buildErrorProfileMap(
                            profile,
                            'First_Reminder_Email_Template__c:' + profile.First_Reminder_Email_Template__c
                        );
                        isValid = false;
                    }
                       
                     if (profile.Second_Reminder_Email_Template__c != null &&
                        !SC_EscalationProfileEngine.emailTemplateMap.containsKey(
                            profile.Second_Reminder_Email_Template__c
                        )
                    ) {
                         SC_EscalationProfileEngine.buildErrorProfileMap(
                            profile,
                            'Second_Reminder_Email_Template__c:' + profile.Second_Reminder_Email_Template__c
                        );
                        isValid = false;
                    }
                    
                    if (profile.Third_Reminder_Email_Template__c != null &&
                        !SC_EscalationProfileEngine.emailTemplateMap.containsKey(
                            profile.Third_Reminder_Email_Template__c
                        )
                    ) {
                        SC_EscalationProfileEngine.buildErrorProfileMap(
                            profile,
                            'Third_Reminder_Email_Template__c:' + profile.Third_Reminder_Email_Template__c
                        );
                        isValid = false;
                    }
                    
                    if (profile.Overdue_Reminder_Email_Template__c != null &&
                        !SC_EscalationProfileEngine.emailTemplateMap.containsKey(
                            profile.Overdue_Reminder_Email_Template__c
                        )
                    ) {
                        SC_EscalationProfileEngine.buildErrorProfileMap(
                            profile,
                            'Overdue_Reminder_Email_Template__c:' + profile.Overdue_Reminder_Email_Template__c
                        );
                        isValid = false;
                    }

                    if (isValid) {
                        SC_EscalationProfileEngine.validProfileIds.add(profile.Id);
                    }
                }
            }
            return SC_EscalationProfileEngine.validProfileIds;
        }
        private set;
    }

    public static void shouldSendErrorEmail() {
        if (!SC_EscalationProfileEngine.errorProfileMap.isEmpty()) {
            SC_EscalationProfileEngine.sendProfileErrorEmail();
        }
    }

    private static void buildErrorProfileMap(Training_Escalation_Profile__c pProfile, String pFieldValue) {
        String madeUpKey = pProfile.Name + ':' + pProfile.Id;
        if (SC_EscalationProfileEngine.errorProfileMap.containsKey(madeUpKey)) {
            SC_EscalationProfileEngine.errorProfileMap.get(madeUpKey).add(pFieldValue);
        } else {
            SC_EscalationProfileEngine.errorProfileMap.put(
                madeUpKey,
                new String[] { pFieldValue }
            );
        }
    }
 
    // The email should include the name of the escalation profile that had the issue,
    // which field, and the current value for email template
    // checking to see if the errorEmailBody variable is different than ''
    private static void sendProfileErrorEmail() {
        
        String errorEmailBody = '';
    
        Messaging.SingleEmailmessage email = new Messaging.SingleEmailmessage();

        email.setOrgWideEmailAddressId(SC_Constants.SC_ORG_WIDE_EMAIL_ID);
        email.setToAddresses(System.Label.SC_Escalation_Template_Emails.split(','));
        email.setCharset('UTF-8');
        email.setSubject('Escalation Email Template Error ' + System.now().format());

        for (String profileKey :SC_EscalationProfileEngine.errorProfileMap.keySet()) {
            errorEmailBody +=
                'Escalation Profile: ' + profileKey.split(':')[0] +
                ' (' + System.URL.getSalesforceBaseURL().toExternalForm() +
                '/' + profileKey.split(':')[1] + ')\n';

            for (String fieldKey :SC_EscalationProfileEngine.errorProfileMap.get(profileKey)) {
                errorEmailBody +=
                    '  Field: ' + fieldKey.split(':')[0] + '\n' +
                    '  Current Value: ' + fieldKey.split(':')[1] + '\n\n';
            }
            errorEmailBody += '\n\n';
        }
        email.setPlainTextBody(errorEmailBody);

        Messaging.sendEmail(new Messaging.SingleEmailmessage[] { email });
    }
}
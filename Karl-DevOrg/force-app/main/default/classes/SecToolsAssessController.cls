/* 
    We've basically collapsed the SiteFlow and AppXReport controllers into the beast below. 
    It's not pretty or a good practice, but it looks nicer on the site to do it all in one page.
*/

public class SecToolsAssessController {
    public Flow__c flow {get;private set;}
    public List<NodeContainer> nc {get;private set;}
    public NodeContainer currentNode {get;private set;}
    
    public String questionAnswer {get;set;}
    private Map<String,Boolean> tfMap = new Map<String,Boolean>();
    private Map<String,String> strMap = new Map<String,String>();
    
    public List<SelectOption> flowChoices {get;private set;}
    public String flowChoice {get;set;}
    
    public Boolean isStarted {get;set;}
    public Boolean reportReady {get;set;}
    
    /* report stuff */
    public List<AreaWrapper> areas {get;set;}
    private static Set<String> CATS;
    public Boolean areAreas {get;set;}
    pattern VALID_KEYWORD = pattern.compile('[a-zA-Z0-9\\-_]+');
    
    static {
      CATS = new Set<String>();
      for (Schema.PicklistEntry val: AppX_Security_Area__c.fields.category__c.getDescribe().getPicklistValues()) {
          CATS.add(val.getValue());
      }
    }
      
    public SecToolsAssessController() {
        isStarted = False;
        reportReady = False;
    }
    public SecToolsAssessController(Flow__c flow) { // for testing
        setup(flow);
    }
    
    public PageReference getStarted() {
        isStarted = True;
        String fidstr = ApexPages.CurrentPage().getParameters().get('Id');
        Id fid = null;
        try {
            fid = (Id) fidstr;
        }
        catch (Exception e) {
        }
        Flow__c f = null;
        if (fid != null) {
            f = [SELECT Id,Public__c FROM Flow__c WHERE Id =: fid];
            if (f != null) {
                if (f.Public__c != True) {
                    f = null; // We only allow public Flow__c's in this controller since it's for Sites
                }
            }
        }
        setup(null);
        return null;
    }
    
    public void setup(Flow__c flow) {
        if (flow == null) {
            /* Soby - skip this since we only have 1 right now
            flowChoices = new List<SelectOption>();
            for (Flow__c fl : [SELECT Id,Name FROM Flow__c WHERE Public__c = True]) {
                flowChoices.add(new SelectOption(fl.Id,fl.Name));
            }
            return;
            */ 
            flow = [SELECT Id,Name FROM Flow__c WHERE Public__c = True LIMIT 1].get(0);
            
        }
        flow = [SELECT Name,StartNode__c,Id FROM Flow__c WHERE Id=: flow.Id];
        List<Node__c> nodes = [SELECT Id,Name,AppendTo__c,AppendVal__c,DecisionAndOr__c,DecisionRules__c,
                                      isYesNo__c,Next__c,NoNext__c,YesNext__c,SetFalse__c,SetTrue__c,
                                      Text__c,Type__c,URL__c FROM Node__c where Flow__c=:flow.Id];
        System.debug('Loaded '+Integer.valueOf(nodes.size())+' nodes');
        NodeContainer tmp;
        nc = new List<NodeContainer>();
        for (Node__c n : nodes) {
            tmp = new NodeContainer(n);
            nc.add(tmp);
            if (flow.StartNode__c == n.Id) {
                currentNode = tmp;
                System.debug('Flow "'+flow.Name+'" - loading start node "'+tmp.n.Name);
            }
        }
        if (currentNode == null) {
            System.debug('Flow "'+flow.Name+'" - start node (Id:'+flow.StartNode__c+') not found');
        }
        setupNode(currentNode);
    }
    
    public PageReference nodeChange() {
        System.debug('Changing node');
        NodeContainer next;
        Boolean userInteraction = False;
        while (!userInteraction) {
            if (currentNode.isQuestion()) {
                next = getContainerForId(currentNode.getQuestionNext(questionAnswer));
                setupNode(next);
            }
            else if (currentNode.isDecision()) {
                next = getContainerForId(currentNode.makeDecision(tfMap,strMap));
                setupNode(next);
            }
            else if (currentNode.isVariable()) {
                tfMap = currentNode.updateTfMap(tfMap);
                strMap = currentNode.updateStrMap(strMap);
                next = getContainerForId(currentNode.getVariableNext());
                setupNode(next);
            }
            if (next.isUserInteractive()) {
                    userInteraction = True;
            }
        }
        if (next.isUrl()) {
            /* Soby: Hijacked as part of my hack to smash all of this stuff together. Look at the 
             * comments in prepAndRunReport() to see my shame 
             */
            //return new PageReference(c.getUrl(tfMap,strMap));
            return prepAndRunReport();
        }
        return null; // Question node or other interactive node that staying on this page
            
    }
    
    private PageReference setupNode(NodeContainer c) {
        System.debug('Setting up node: '+c.n.Name);
        if (c.isUrl()) {
            /* Soby: Hijacked as part of my hack to smash all of this stuff together. Look at the 
             * comments in prepAndRunReport() to see my shame 
             */
            //return new PageReference(c.getUrl(tfMap,strMap));
            return prepAndRunReport();
        }
        else if (c.isQuestion()) {
            setupQuestionNode(c);
        }
        else if (c.isDecision()) {
            setupDecisionNode(c);
        }
        else if (c.isVariable()) {
            setupVariableNode(c);
        }
        return null;
    }
        
    private NodeContainer getContainerForId(Id nid) {
        for (NodeContainer b : nc) {
            if (b.n.Id == nid) {
                return b;
            }
        }
        throw new FlowException('Node not found: '+String.valueOf(nid));
    }
    private void setupQuestionNode(NodeContainer cur) {
        // nothing needed really
        currentNode = cur;
    }
    private void setupVariableNode(NodeContainer cur) {
        // nothing needed really
        currentNode = cur;
    }
    private PageReference prepAndRunReport() {
        // nothing needed really
        //currentNode = cur;
        
        /* Soby: OK, this is really ugly. This whole flow tool never really panned out
         * to be the flexible self-help tool that I wanted. Normally, URL node would redirect
         * the user to another element like the AppXReporting. We're going to shim in right here
         * and essentially hijack it to go to reporting directly. Yes, I know. I feel dirty
         */
         Map<String,List<String>> catsAndKeywords = new Map<String,List<String>>();
         for (String cat : strMap.keySet()) {
           catsAndKeywords.put(cat,strMap.get(cat).split('|'));
         }
         doReport(catsAndKeywords);
         return null;
    }
    private void setupDecisionNode(NodeContainer cur) {
        // nothing needed really
        currentNode = cur;
    }

    public List<SelectOption> getAnswerOptions() {
        return currentNode.getAnswerOptions();
    }
    public String getQuestion() {
        if (currentNode.isQuestion()) {
            return currentNode.getQuestion();
        }
        return null;
    }
    public PageReference startFlow() {
        PageReference pr = new PageReference(ApexPages.CurrentPage().getUrl());
        pr.getParameters().clear();
        pr.getParameters().put('id',flowChoice);
        pr.setRedirect(true);
        return pr;
    }
    
    /* Basically, this is the report generating controller pasted in here. Ugly, but fast - Soby */
    public PageReference doReport (Map <String,List<String>> categoriesAndKeywords) {
        List<String> tags;
        areAreas = False;
        Boolean noCategoriesGiven = True;
        areas = new List<AreaWrapper>();
        List<AppX_Security_Area__c> catAreas;

        Set<String> areaKeywords;
        for (String category : categoriesAndKeywords.keySet()) {
            catAreas = new List<AppX_Security_Area__c>();
            if (!CATS.contains(category)) {
                continue;
            }
            noCategoriesGiven = False; // they would get everything if no categories were given.
            tags = categoriesAndKeywords.get(category);
            if (tags.get(0) == ''){
              tags.remove(0); // bogus entry
            }
            // add the category name to the keywords to catch the generic category keyword
            // **We only do this if there's at least one existing keyword, otherwise this 
            //   category is probably N/A to the application.
            if (tags.size() > 0){
              tags.add(category.toLowerCase()); 
            }

            for (AppX_Security_Area__c area : [SELECT Id,Name,references__c,priority__c,
                                                      description__c,category__c,keywords__c
                                               FROM AppX_Security_Area__c 
                                               WHERE category__c INCLUDES (:category) ORDER BY priority__c DESC NULLS last]) {
                areaKeywords = setify(area.keywords__c.split(' '));
                
                for (String tag: tags) {
                    if (areaKeywords.contains(tag)) {
                        catAreas.add(area);
                        System.debug('Hit for tag "'+tag+'" in area "'+area.Name+'" keywords "'+area.keywords__c+'"');
                        break;
                    }
                    System.debug('Miss for tag "'+tag+'" in area "'+area.Name+'" keywords "'+area.keywords__c+'"');
                }
            }
            if (!catAreas.isEmpty()) {
                    areAreas = True;
                    AreaWrapper aw = new AreaWrapper(category,catAreas);
                    if (areas.isEmpty()) {
                        aw.isFirst = True;
                    }
                    areas.add(aw);
            }
        }
        if (areas.size() > 0) {
          areas.get(areas.size()-1).isLast = True;
        }
        
        // It looks like they didn't pass in any categories or tags
        // We'll assume this means that they want everything
        if (noCategoriesGiven == True) {
          Map<String,List<AppX_Security_Area__c>> catMap = new Map<String,List<AppX_Security_Area__c>>();
          for (AppX_Security_Area__c area : [SELECT Id,Name,references__c,priority__c,
                                                      description__c,category__c,keywords__c
                                               FROM AppX_Security_Area__c ORDER BY priority__c DESC NULLS last]) {
            for (String aName : area.category__c.split(';')) {
              if (!catMap.containsKey(aName)) {
                catMap.put(aName,new List<AppX_Security_Area__c>());
              }
              catMap.get(aName).add(area);
            }
          }
          Set<String> keyset = catMap.keySet();
          List<String> keylist = new List<String>(); 
          //this is dumb
          for (String s: keyset) {
            keylist.add(s);
          }
          keylist.sort(); // I want them in alphabetic order                                  
          for (String areaName : keylist){
            AreaWrapper aw = new AreaWrapper(areaName,catMap.get(areaName));
            if (areas.isEmpty()) {
              aw.isFirst = True;
            }
            areas.add(aw);
          }
          if (areas.size() > 0) {
            areas.get(areas.size()-1).isLast = True;
          }
        }
        reportReady = True;
        return null;
    }
    private Set<String> setify(List<String> l) {
        Set<String> r = new Set<String>();
        for (String s : l) {
            r.add(s);
        }
        return r;
    }
    public class AreaWrapper {
        public String category {get;private set;}
        public List<AreaDetailWrapper> areas {get;private set;} 
        public Boolean isFirst {get;set;}
        public Boolean isMid {get;set;}
        public Boolean isLast {get;set;}
        public AreaWrapper(String cat,List<AppX_Security_Area__c> ar) {
            isFirst = False;
            isMid = True; //default
            isLast = False;
            areas = new List<AreaDetailWrapper>();
            Integer count = 1;
            AreaDetailWrapper adw;
            for (AppX_Security_Area__c a : ar ) {
                adw = new AreaDetailWrapper(a);
                areas.add(adw);
            }
            category = cat;
        }

          
    }
    public class AreaDetailWrapper {
        public AppX_Security_Area__c ad;
        public String name {get {return ad.Name;} private set;}
        public String description {get {return ad.description__c;} private set;}
        public AreaDetailWrapper(AppX_Security_Area__c a) {
            ad = a;
        }
        public List<String> getReferenceList() {
            List<String> rl = new List<String>();
            if ((ad.references__c != '') && (ad.references__c != null)) {
              for (String s : ad.references__c.split('\\n')) {
                  rl.add(s.trim());
              }
            }
            return rl;
        }
    }    

    static testMethod void runTests() {
        Flow__c f = new Flow__c(Name='blah');
        insert f;
        Node__c qn = new Node__c(Type__c='Question',
                         Text__c='Hello?',
                         isYesNo__c=True,
                         Flow__c=f.Id,
                         X__c=1,
                         Y__c=2);
        insert qn;
        f.StartNode__c = qn.Id;
        update f;
        
        Node__c vn = new Node__c(Type__c='Variable',
                         SetTrue__c='blah',
                         Flow__c=f.Id,
                         X__c=1,
                         Y__c=2);
        insert vn;
        Node__c dn = new Node__c(Type__c='Decision',
                         DecisionAndOr__c='OR',
                         DecisionRules__c='blah == True',
                         Flow__c=f.Id,
                         X__c=1,
                         Y__c=2);
        insert dn;
        Node__c un = new Node__c(Type__c='URL',
                         URL__c='http://blah.com',
                         Flow__c=f.Id,
                         X__c=1,
                         Y__c=2);
        insert un;
        qn.YesNext__c=vn.Id;
        qn.NoNext__c=un.Id;
        vn.Next__c=dn.Id;
        dn.YesNext__c=un.Id;
        dn.NoNext__c=un.Id;
        update qn;
        update vn;
        update dn;
     
        SecToolsAssessController fc = new SecToolsAssessController((Flow__c)f);
        
                         
    }
}
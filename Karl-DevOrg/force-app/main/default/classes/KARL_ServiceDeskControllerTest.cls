/**
 * @author Lakshya Arora
 * @email lakshya.arora@mtxb2b.com
 * @description Class for unit testing the KARL_ServiceDeskController
 */
@isTest
public with sharing class KARL_ServiceDeskControllerTest {
    /**
     * @author Lakshya Arora
     * @email lakshya.arora@mtxb2b.com
     * @description Method will create the Test Data
    */
    @TestSetup
    private static void makeData(){
        User opsAdminUser = ARL_TestDataFactory.createOpsAdmin();
        System.runAs(opsAdminUser){
            Account acc = ARL_TestDataFactory.createAccount('salesforce.com');
            insert acc;
            Contact con = ARL_TestDataFactory.createContact(acc.Id,'test','test@gmail.com.invalid');
            con.recordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Salesforce Employee').getRecordTypeId();
            insert con;

            Audit_Cycle__c auditCycleObj = ARL_TestDataFactory.createAuditCycle();
            insert auditCycleObj;

            Request_Item__c reqItemObj = ARL_TestDataFactory.createRequestItem();
            reqItemObj.KARL_GRC_Assignee__c = con.Id;
            insert reqItemObj;

            Cycle_Request_Item__c cycleRequestItemObj = ARL_TestDataFactory.createCycleRequestItem(reqItemObj.Id,auditCycleObj.Id);
            insert cycleRequestItemObj;

            KARL_Evidence_Request__c evidenceRequestObj = ARL_TestDataFactory.createEvidenceRequest(cycleRequestItemObj.Id);
            insert evidenceRequestObj;
            
            KARL_Audit_Scope_Master_Data__c auditscopeMasteDataObj = ARL_TestDataFactory.createAuditScope();
            auditscopeMasteDataObj.KARL_BU_Lead__c = con.Id;
            auditscopeMasteDataObj.KARL_Scope__c = 'AS';
            insert auditscopeMasteDataObj;
        }
    }
    /**
     * @author Lakshya Arora
     * @email lakshya.arora@mtxb2b.com
     * @description Method will create the Service Desk record and validate createServiceDeskRecord functionality
    */
    @isTest
    private static void createServiceDeskRecordTest(){
        List<User> opsAdminUserList = ARL_TestDataFactory.fetchUser('abc@b.com.d');
        if(!opsAdminUserList.isEmpty()){
            System.runAs(opsAdminUserList[0]){
                Request_Item__c reqItemObj = [SELECT Id FROM Request_Item__c LIMIT 1];
                Karl_ServiceDeskController.ServiceDeskWrapper serviceDeskWrapperObj = new Karl_ServiceDeskController.ServiceDeskWrapper();
                serviceDeskWrapperObj.requestNameId = reqItemObj.Id;
                serviceDeskWrapperObj.newOrExisitng = KARL_Constants.NEWOREXISTING_NEW;
                serviceDeskWrapperObj.dataUpdateReason = 'Notes';
                serviceDeskWrapperObj.requestDescription = 'Description';
                serviceDeskWrapperObj.type = KARL_Constants.TYPE_INQUIRY;
                List<String> areaOfCompliance = new List<String>();
                areaOfCompliance.add(KARL_Constants.AOC_SOC1);
                areaOfCompliance.add(KARL_Constants.AOC_SOC2);
                serviceDeskWrapperObj.areaOfCompliance = areaOfCompliance;
                serviceDeskWrapperObj.businessUnit = 'AS';
                Karl_ServiceDeskController.ServiceDeskEvidenceWrapper serviceEvidenceWrapper = new  Karl_ServiceDeskController.ServiceDeskEvidenceWrapper();
                Test.startTest();
                Karl_ServiceDeskController.createServiceDeskRecord(JSON.serialize(serviceDeskWrapperObj),'');
                Test.stopTest();
                List<KARL_Service_Desk__c> serviceDeskList = [SELECT Id,RecordTypeId,Status__c FROM KARL_Service_Desk__c LIMIT 1];
                System.assert(!serviceDeskList.isEmpty());
                System.assertEquals(serviceDeskList[0].RecordTypeId,KARL_Constants.REQUEST_MASTER_DATA_RECORDTYPEID);
                System.assertEquals(serviceDeskList[0].Status__c,KARL_Constants.STATUS_SUBMIT_FOR_APPROVAL);
            }
        }
    }


    /**
     * @author Lakshya Arora
     * @email lakshya.arora@mtxb2b.com
     * @description Method will create the Service Desk record and validate createServiceDeskEvidenceRecord functionality
    */
    @isTest
    private static void createServiceDeskEvidenceRecordTest(){
        List<User> opsAdminUserList = ARL_TestDataFactory.fetchUser('abc@b.com.d');
        if(!opsAdminUserList.isEmpty()){
            System.runAs(opsAdminUserList[0]){
                KARL_Evidence_Request__c evidenceReqObj = [SELECT Id,KARL_Cycle_ARL_Item__c FROM KARL_Evidence_Request__c LIMIT 1];
                Karl_ServiceDeskController.ServiceDeskEvidenceWrapper serviceDeskEvidenceWrapperObj = new Karl_ServiceDeskController.ServiceDeskEvidenceWrapper();
                serviceDeskEvidenceWrapperObj.evidenceRequestId = evidenceReqObj.Id;
                serviceDeskEvidenceWrapperObj.describeAccessRequest = 'Notes';
                serviceDeskEvidenceWrapperObj.cycleArlItemId = evidenceReqObj.KARL_Cycle_ARL_Item__c;
                Test.startTest();
                Karl_ServiceDeskController.createServiceDeskEvidenceRecord(JSON.serialize(serviceDeskEvidenceWrapperObj));
                Test.stopTest();
                List<KARL_Service_Desk__c> serviceDeskList = [SELECT Id,RecordTypeId,Status__c FROM KARL_Service_Desk__c LIMIT 1];
                System.assert(!serviceDeskList.isEmpty());
                System.assertEquals(serviceDeskList[0].RecordTypeId,KARL_Constants.EVIDENCE_REQUEST_RECORDTYPEID);
                System.assertEquals(serviceDeskList[0].Status__c,KARL_Constants.STATUS_SUBMIT_FOR_APPROVAL);
            }
        }
    }
    
    /**
     * @author Lakshya Arora
     * @email lakshya.arora@mtxb2b.com
     * @description Method will unit test the getRequestMasterDataInfo functionality
    */
    @isTest
    private static void getRequestMasterDataInfoTest(){
        List<User> opsAdminUserList = ARL_TestDataFactory.fetchUser('abc@b.com.d');
        if(!opsAdminUserList.isEmpty()){
            System.runAs(opsAdminUserList[0]){
                Request_Item__c reqItem = [SELECT Id FROM Request_Item__c LIMIT 1];
                Request_Item_Control__c reqItemControl = new Request_Item_Control__c();
                reqItemControl.Request__c = reqItem.Id;
                insert reqItemControl;
                Test.startTest();
                Karl_ServiceDeskController.ServiceDeskWrapper serviceDeskWrapperObj = Karl_ServiceDeskController.getRequestMasterDataInfo(reqItem.Id);
                Test.stopTest();
                System.assert(serviceDeskWrapperObj != null);
            }
        }
    }
}
/**
 * @author Lakshya Arora
 * @email lakshya.arora@mtxb2b.com
* @desc This is schedulable and batch class to sync external gus product tag record with Gus_product_Tag__c recrods .
 */

public with sharing class KARL_GusProductTagSyncBatch implements Database.Batchable<sObject>, Schedulable{
    @testVisible Map<String,Boolean> scrumTeamIdToActiveMapForTestClass = new Map<String,Boolean>();
    /**
    * @author Lakshya Arora
    * @email lakshya.arora@mtxb2b.com
    * @desc This method is for schedulable interface to execute.
    */
    public void execute(SchedulableContext sc) {
        database.executebatch( new KARL_GusProductTagSyncBatch(),200);
    }

    /**
    * @author Lakshya Arora
    * @email lakshya.arora@mtxb2b.com
    * @desc NOT USING SECURITY_ENFORCED: Even though we have all permission intact,
                                         using SECURITY_ENFORCED in batch causes to 
                                         fail. Error name: Inernal salesforce.com error
                                         That's why not using SECURITY_ENFORCED
    */
    public Database.QueryLocator start(Database.BatchableContext BC){
        String query = 'SELECT Id,ExternalId,Description_c__c,Active_c__c,Name__c,Migration_PA_c__c,Migration_MFA_c__c,Team_c__c,Team_Tag_Key_c__c,Use_for_Automated_Tools_c__c '
                        + ' FROM ADM_Product_Tag_c__x '
                        + ' WHERE ExternalId != null AND LastModifiedDate__c = TODAY';
        return Database.getQueryLocator(query);
    }

   /**
    * @author Lakshya Arora
    * @email lakshya.arora@mtxb2b.com
    * @desc This method upsert the gus product tag
    */
    public void execute(Database.BatchableContext BC, List<ADM_Product_Tag_c__x> externalProductTagRecordList){
        Map<String,ADM_Product_Tag_c__x> externalIdToProductTagMap = new Map<String,ADM_Product_Tag_c__x>();
        Set<String> scrumTeamIdSet = new Set<String>();
        for(ADM_Product_Tag_c__x prodTagObj: externalProductTagRecordList){
            externalIdToProductTagMap.put(prodTagObj.ExternalId,prodTagObj);
            scrumTeamIdSet.add(prodTagObj.Team_c__c);
        }

        Map<String,Boolean> scrumTeamIdToActiveMap = new Map<String,Boolean>();
        if(!scrumTeamIdSet.isEmpty()){
            for(ADM_Scrum_Team_c__x scrumTeamObj : [SELECT Id,ExternalId ,Active_c__c 
                                                    FROM ADM_Scrum_Team_c__x 
                                                    WHERE ExternalId IN:scrumTeamIdSet
                                                    WITH SECURITY_ENFORCED ]){
                scrumTeamIdToActiveMap.put(scrumTeamObj.ExternalId,scrumTeamObj.Active_c__c);
            }
        }

        if(Test.isRunningTest()){
            scrumTeamIdToActiveMap = scrumTeamIdToActiveMapForTestClass;
        }
        List<KARL_GUS_Product_Tag__c> gusProdTagList = new List<KARL_GUS_Product_Tag__c>();
        for(KARL_GUS_Product_Tag__c gusProdTagObj : [SELECT Id,Name,Active__c ,Description__c ,Migration_MFA__c,Migration_PA__c,
                                                Team__c,Team_Tag_Key__c ,Use_for_Automated_Tools__c ,Product_Tag_External_Id__c  
                                                FROM KARL_GUS_Product_Tag__c 
                                                WHERE Product_Tag_External_Id__c IN:externalIdToProductTagMap.keySet()
                                                WITH SECURITY_ENFORCED]){
            ADM_Product_Tag_c__x prodTag = externalIdToProductTagMap.get(gusProdTagObj.Product_Tag_External_Id__c);
            gusProdTagList.add(processProductTag(gusProdTagObj,prodTag,scrumTeamIdToActiveMap));
            externalIdToProductTagMap.remove(gusProdTagObj.Product_Tag_External_Id__c);
        }
        if(!externalIdToProductTagMap.isEmpty()){
            for(String externalId : externalIdToProductTagMap.keySet()){
                gusProdTagList.add(processProductTag(new KARL_GUS_Product_Tag__c(),externalIdToProductTagMap.get(externalId),scrumTeamIdToActiveMap));
            }
        }
        if(!gusProdTagList.isEmpty())
        upsert gusProdTagList;
    }

    /**
    * @author Lakshya Arora
    * @email lakshya.arora@mtxb2b.com
    * @desc This method initializes the KARL_GUS_Product_Tag__c record
    * @return  KARL_GUS_Product_Tag__c object
    */
    private static KARL_GUS_Product_Tag__c processProductTag(KARL_GUS_Product_Tag__c gusProdTagObj,ADM_Product_Tag_c__x prodTag,Map<String,Boolean> scrumTeamIdToActiveMap){
        gusProdTagObj.Name  = prodTag.Name__c;
        gusProdTagObj.Active__c   = prodTag.Active_c__c; 
        gusProdTagObj.Description__c  = prodTag.Description_c__c; 
        gusProdTagObj.Migration_MFA__c  = prodTag.Migration_MFA_c__c; 
        gusProdTagObj.Migration_PA__c  =  prodTag.Migration_PA_c__c;
        gusProdTagObj.Team__c  = prodTag.Team_c__c;
        gusProdTagObj.Team_Tag_Key__c  =  prodTag.Team_Tag_Key_c__c;
        gusProdTagObj.Use_for_Automated_Tools__c  =  prodTag.Use_for_Automated_Tools_c__c;
        if(scrumTeamIdToActiveMap.containsKey(prodTag.Team_c__c)){
            gusProdTagObj.Active_Scrum_Team__c = scrumTeamIdToActiveMap.get(prodTag.Team_c__c);
        }
        gusProdTagObj.Product_Tag_External_Id__c = prodTag.ExternalId;
        return gusProdTagObj;
    }

    /**
    * @author Lakshya Arora
    * @email lakshya.arora@mtxb2b.com
    * @desc This method will call after all the operations get completed
    */
    public void finish(Database.BatchableContext BC){}

    /**
    * @author Lakshya Arora
    * @email lakshya.arora@mtxb2b.com
    * @desc This method will schedule the batch daily at end of day
    */
    public static void start(){
        System.schedule('Sync Gus Product Tags', '0 59 23 * * ? *', new KARL_GusProductTagSyncBatch());
    }
}
public class IPConverter {
    
    public static Long parseIp(String address) 
    {
        Long result = 0;
    
        // iterate over each octet
        for(String part : address.split(Pattern.quote('.'))) {
            // shift the previously parsed bits over by 1 byte
            result = result << 8;
            // set the low order bits to the current octet
            result |= Long.valueof(part);
        }
        
        return result;
	}
    
    public static Boolean isValidIP(String ipStr)
    {
		String ipRegex='^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$';
 		
        Pattern p = Pattern.compile(ipRegex);
        
        //validate your input    
        Matcher MyMatcher = p.matcher(ipStr);
        
        if (MyMatcher.matches()) {
        	return true;
        }
        
        return false;
    }
    
    public static Boolean isPrivateSpace(String ip)
    {
        return isPrivateSpace(parseIp(ip));
    }
    
    public static Boolean isPrivateSpace(Long ip)
    {
        //10.0.0.0
        if( ip >= Long.valueOf('167772160') && ip <= Long.valueOf('184549375'))
            return true;
        
        //192.168.0.1
        if( ip >= Long.valueOf('3232235520') && ip <= Long.valueOf('3232301055'))
            return true;
        
        //172.0.0.0
        if( ip >= Long.valueOf('2886729728') && ip <= Long.valueOf('2887778303'))
            return true;
        
        return false;
    }
}
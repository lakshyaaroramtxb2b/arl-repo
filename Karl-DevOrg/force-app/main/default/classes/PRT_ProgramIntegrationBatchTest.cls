@isTest
public class PRT_ProgramIntegrationBatchTest {
	@testSetup
    public static void testSetup(){
        List<User> userList = new List<User>();
        userList = PRT_TestDataFactory.createUsers(3,'System Administrator',true);
    }
    static testMethod void testExecuteMethod(){
        List<User> userList = new List<User>([SELECT Id FROM USER WHERE Email Like 'PRTUser%']);
        List<Portfolio__c> portfolioDataList = PRT_TestDataFactory.createPortfolio(1,true);

        program__c prog = new program__c();
        prog.Name = 'testProgram';
        prog.Program_Status__c = 'Active';
        prog.Program_Owner__c = userList[0].id;
        prog.Program_Manager__c = userList[1].id;
        prog.Executive_Sponsor__c = userList[2].id;
        prog.Portfolio__c = null;
        //prog.Budget_Allocated__c = 23.00;
        prog.Program_Summary__c ='test text Summary';
        insert prog;
        
        List<PPM_Program_c__x> progamList = new List<PPM_Program_c__x>();
        
        PPM_Program_c__x program = new PPM_Program_c__x();
        program.Name__c = 'Test Name';
        //program.ExternalId = '';
        program.Program_Goals_c__c = 'Test data ';
        program.Program_Health_c__c = 'Completed';
        program.Program_Health_Comments_c__c = '';
        program.Program_Manager_c__c = userList[0].id;
        program.Program_Owner_c__c = userList[0].id;
        program.Program_Summary_c__c = 'TEst';
        program.Executive_Sponsor_c__c = userList[0].id;
        program.Portfolio_c__c = portfolioDataList[0].Id;
        progamList.add(program);
        
        PRT_ProgramIntegrationBatch.mockallGusProjectList.addAll(progamList);
        
        Test.startTest();
            PRT_ProgramIntegrationBatch pib = new PRT_ProgramIntegrationBatch();
            Id batchId = Database.executeBatch(pib);
        Test.stopTest();
    }
}
public class AppXSecurityReportController {
    public List<AreaWrapper> areas {get;set;}
    private static Set<String> CATS;
    public Boolean areAreas {get;set;}
    
    pattern VALID_KEYWORD = pattern.compile('[a-zA-Z0-9\\-_]+');
    
    static {
      CATS = new Set<String>();
      for (Schema.PicklistEntry val: AppX_Security_Area__c.fields.category__c.getDescribe().getPicklistValues()) {
          CATS.add(val.getValue());
      }
    }
    
    public AppXSecurityReportController () {
        List<String> tags;
        areAreas = False;
        Boolean noCategoriesGiven = True;
        areas = new List<AreaWrapper>();
        List<AppX_Security_Area__c> catAreas;
        Map<String,String> params = ApexPages.CurrentPage().getParameters();
        Set<String> areaKeywords;
        for (String category : params.keySet()) {
            catAreas = new List<AppX_Security_Area__c>();
            if (!CATS.contains(category)) {
                continue;
            }
            noCategoriesGiven = False; // they would get everything if no categories were given.
            tags = params.get(category).split('\\|');
            if (tags.get(0) == ''){
              tags.remove(0); // bogus entry
            }
            // add the category name to the keywords to catch the generic category keyword
            // **We only do this if there's at least one existing keyword, otherwise this 
            //   category is probably N/A to the application.
            if (tags.size() > 0){
              tags.add(category.toLowerCase()); 
            }

            for (AppX_Security_Area__c area : [SELECT Id,Name,references__c,priority__c,
                                                      description__c,category__c,keywords__c
                                               FROM AppX_Security_Area__c 
                                               WHERE category__c INCLUDES (:category) ORDER BY priority__c DESC NULLS last]) {
                areaKeywords = setify(area.keywords__c.split(' '));
                
                for (String tag: tags) {
                    if (areaKeywords.contains(tag)) {
                        catAreas.add(area);
                        System.debug('Hit for tag "'+tag+'" in area "'+area.Name+'" keywords "'+area.keywords__c+'"');
                        break;
                    }
                    System.debug('Miss for tag "'+tag+'" in area "'+area.Name+'" keywords "'+area.keywords__c+'"');
                }
            }
            if (!catAreas.isEmpty()) {
                    areAreas = True;
                    AreaWrapper aw = new AreaWrapper(category,catAreas);
                    if (areas.isEmpty()) {
                        aw.isFirst = True;
                    }
                    areas.add(aw);
            }
        }
        if (areas.size() > 0) {
          areas.get(areas.size()-1).isLast = True;
        }
        
        // It looks like they didn't pass in any categories or tags
        // We'll assume this means that they want everything
        if (noCategoriesGiven == True) {
          Map<String,List<AppX_Security_Area__c>> catMap = new Map<String,List<AppX_Security_Area__c>>();
          for (AppX_Security_Area__c area : [SELECT Id,Name,references__c,priority__c,
                                                      description__c,category__c,keywords__c
                                               FROM AppX_Security_Area__c ORDER BY priority__c DESC NULLS last]) {
            for (String aName : area.category__c.split(';')) {
              if (!catMap.containsKey(aName)) {
                catMap.put(aName,new List<AppX_Security_Area__c>());
              }
              catMap.get(aName).add(area);
            }
          }
          Set<String> keyset = catMap.keySet();
          List<String> keylist = new List<String>(); 
          //this is dumb
          for (String s: keyset) {
            keylist.add(s);
          }
          keylist.sort(); // I want them in alphabetic order                                  
          for (String areaName : keylist){
            AreaWrapper aw = new AreaWrapper(areaName,catMap.get(areaName));
            if (areas.isEmpty()) {
              aw.isFirst = True;
            }
            areas.add(aw);
          }
          if (areas.size() > 0) {
            areas.get(areas.size()-1).isLast = True;
          }
        }
    }
    private Set<String> setify(List<String> l) {
        Set<String> r = new Set<String>();
        for (String s : l) {
            r.add(s);
        }
        return r;
    }
    public class AreaWrapper {
        public String category {get;private set;}
        public List<AreaDetailWrapper> areas {get;private set;} 
        public Boolean isFirst {get;set;}
        public Boolean isMid {get;set;}
        public Boolean isLast {get;set;}
        public AreaWrapper(String cat,List<AppX_Security_Area__c> ar) {
            isFirst = False;
            isMid = True; //default
            isLast = False;
            areas = new List<AreaDetailWrapper>();
            Integer count = 1;
            AreaDetailWrapper adw;
            for (AppX_Security_Area__c a : ar ) {
                adw = new AreaDetailWrapper(a);
                areas.add(adw);
            }
            category = cat;
        }

          
    }
    public class AreaDetailWrapper {
        public AppX_Security_Area__c ad;
        public String name {get {return ad.Name;} private set;}
        public String description {get {return ad.description__c;} private set;}
        public AreaDetailWrapper(AppX_Security_Area__c a) {
            ad = a;
        }
        public List<String> getReferenceList() {
            List<String> rl = new List<String>();
            if ((ad.references__c != '') && (ad.references__c != null)) {
              for (String s : ad.references__c.split('\\n')) {
                  rl.add(s.trim());
              }
            }
            return rl;
        }
    }    

}
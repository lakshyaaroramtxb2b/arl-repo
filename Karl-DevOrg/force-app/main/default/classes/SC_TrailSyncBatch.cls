/*
 * Apex Batch class used to sync Trails
 */

global class SC_TrailSyncBatch implements
    Database.Batchable<SC_TrailResponse.Data>,
    Database.AllowsCallouts,
    Database.Stateful {

    Integer offSet;
    Integer totalCount;

    global static final Integer OFF_SET_SIZE = 50;
    global static final String TRAIL = 'trail_';

    public SC_TrailSyncBatch(Integer pOffset) {
        this.offSet = pOffset;
    }

    global Iterable<SC_TrailResponse.Data> start(Database.BatchableContext info) {

        SC_TrailheadApi api = new SC_TrailheadApi();
        SC_TrailResponse trailResponse = api.getTrails(this.offSet);
        this.totalCount = trailResponse.total_count;

        return (Iterable<SC_TrailResponse.Data>)trailResponse.data;
    }

    global void execute(Database.BatchableContext info, SC_TrailResponse.Data[] pScope) {

        List<Training_Course__c> upsertCourses = new List<Training_Course__c>();

        for (SC_TrailResponse.Data trail :pScope) {
            upsertCourses.add(new Training_Course__c(
                Sync_Id__c = SC_TrailSyncBatch.TRAIL + trail.api_name,
                Name = trail.title,
                Is_Active__c = !trail.archived,
                Course_Level__c = TrainingCourse_Constants.COURSE_LEVEL_TRAIL,
                Last_Import_Date__c = System.today(),
                Provider__c = 'Trailhead',
                Provider_ID__c = trail.api_name,
                URL__c = trail.web_url
            ));
        }

        if (!upsertCourses.isEmpty()) {
            Database.upsert(
                upsertCourses,
                Training_Course__c.fields.Sync_Id__c
            );
        }
    }

    global void finish(Database.BatchableContext info) {
        this.offSet += SC_TrailSyncBatch.OFF_SET_SIZE;
        if (this.totalCount > this.offSet) {
            Database.executeBatch(new SC_TrailSyncBatch(this.offSet));
        } else if (!Test.isRunningTest()) {
            Database.executeBatch(new SC_ModuleSyncBatch(0));
        }
    }
}
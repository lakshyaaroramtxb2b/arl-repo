public with sharing class ESA_G2M_Start {
    ESAgtmOAuthUtil gtm = new ESAgtmOAuthUtil();

    public pageReference startG2M() {
    	PageReference startG2M;
    	
    	try {
    		String meetindId = ApexPages.currentPage().getParameters().get('meetingId');
    		string startURL = gtm.getStartURL(meetindId);
    		startG2M = new PageReference(startURL);    		
   		
    	} catch (exception e) {
    		startG2M = null;
    		ApexPages.addMessage(new ApexPages.Message(
    		  ApexPages.Severity.Error, 'Error occurred while tyring to start meeting: ' + e.getMessage()));    		
    	}
    	
    	return startG2M;
    	    	
    }
}
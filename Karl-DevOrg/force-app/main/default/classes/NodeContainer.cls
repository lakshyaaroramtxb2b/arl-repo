public class NodeContainer {
    public Node__c n;
    private static pattern CRIT_PATTERN = pattern.compile('([a-zA-Z0-9_\\-]+)\\s*(>|<|>=|<=|=|==|!=)\\s*([a-zA-Z0-9_\\-\'"]+)\\s*');
    public NodeContainer(Node__c node) {
        n = node;
    }
    
    /* 
       What am I section
    */
    public Boolean isQuestion() {
        return n.Type__c == 'Question';
    }
    public Boolean getIsQuestion() { return isQuestion();}
    
    public Boolean isDecision() {
        return n.Type__c == 'Decision';
    }
    public Boolean getIsDecision() { return isDecision();}
    
    public Boolean isVariable() {
        return n.Type__c == 'Variable';
    }
    public Boolean getIsVariable() { return isVariable(); }
    
    public Boolean isFinal() {
        return isUrl();
    }
    public Boolean getIsFinal() { return isFinal(); }
    
    public Boolean isUserInteractive() {
        return (isQuestion() || isFinal());
    }
    public Boolean getIsUserInteractive() { return isUserInteractive(); }
    
    public Boolean isUrl() {
        return n.Type__c == 'URL';
    }
    public Boolean getIsUrl() { return isUrl(); }

    /* 
        Stuff for URL nodes
    */
    public String getUrl(Map<String,Boolean> tfMap,Map<String,String> strMap) {
        String url = n.URL__c;
        String key = '';
        matcher url_match = pattern.compile('\\[\\s*([a-zA-Z0-9\\-_]*)\\s*\\]').matcher(url);
        while (url_match.find()) {
            key = url_match.group(1);
            if (tfMap.containsKey(key)) {
                url = url.replace(url_match.group(0),String.valueOf(tfMap.get(key)));
            }
            else if (strMap.containsKey(key)) {
                url = url.replace(url_match.group(0),strMap.get(key));
            }
            else {
                // That var wasn't declared.  Blank it out
                url = url.replace(url_match.group(0),'');
            }
        }
        return url;
      
    }
    
    /* 
        Stuff for question nodes
    */
    public String getQuestion() {
        return n.Text__c;
    }
    public List<SelectOption> getAnswerOptions() {
        List<SelectOption> opts = new List<SelectOption>();
        if (n.isYesNo__c == True) {
            opts.add(new SelectOption('Yes','Yes'));
            opts.add(new SelectOption('No','No'));
        } 
        return opts;
    }
    public Id getQuestionNext(String answer) {
        if (answer == 'Yes') {
            return n.YesNext__c;
        }
        else if (answer == 'No') {
            return n.NoNext__c;
        }
        else {
            throw new FlowException('"'+answer+'" is not a valid answer');
        }
    }
    
    /* 
        Stuff for variable nodes
    */
    public Map<String,Boolean> updateTfMap(Map<String,Boolean> tfMap) {
        List<String> keys;
        if (n.SetTrue__c != null) {
            keys = n.SetTrue__c.split('\n');
            for (String key : keys) {
                tfMap.put(key,True);
            }
        }
        if (n.SetFalse__c != null) {
            keys = n.SetFalse__c.split('\n');
            for (String key : keys) {
                tfMap.put(key,False);
            }
        }
        return tfMap;
    }
    public Map<String,String> updateStrMap(Map<String,String> strMap) {
        if (n.AppendTo__c != null) {
            List<String> keys = n.AppendTo__c.split('\n');
            for (String key : keys) {
                if (!strMap.containsKey(key)) {
                    strMap.put(key,n.AppendVal__c);
                }
                else {
                    strMap.put(key,strMap.get(key)+n.AppendVal__c);
                }
            }
        }
        return strMap;
    }
    public Id getVariableNext() {
        return n.Next__c;
    }
    
    /* 
        Stuff for decision nodes
    */
    public Id makeDecision(Map<String,Boolean> tfMap,Map<String,String> strMap) {
        Boolean andMode = n.DecisionAndOr__c == 'AND';
        Boolean decision = False;
        List<String> tfCriteriaRows = n.DecisionRules__c.split('\n',0);
        for (String row : tfCriteriaRows) {
            matcher m = CRIT_PATTERN.matcher(row);
            if ((!m.matches())){// || (!(m.groupCount() == 4))) {
                throw new FlowException('Invalid criteria: "'+row+'"');
            }
            CriteriaEvaluator c = new CriteriaEvaluator(m.group(1), // val1
                                                      m.group(2), // operator
                                                      m.group(3), // val2
                                                      tfMap,
                                                      strMap);
            if (andMode && (!c.isTrue())) {
                // if we're in andMode then a single False returns no
                return n.NoNext__c;
            }
            else if (!andMode && (c.isTrue())) {
                // if we're not in andMode (orMode) then a single
                // True returns yes
                return n.YesNext__c;
            }
        }
        
        // If we got to this point, we either 1) in andMode and we
        // had only True's or 2) in orMode and we had only falses
        if (andMode) {
            return n.YesNext__c;
        }
        return n.NoNext__c;  // all falses and orMode
            
    }
    
    public class CriteriaEvaluator {
        private Boolean bVal1  = null;
        private String strVal1 = null;
        private Boolean bVal2  = null;
        private String strVal2 = null;
        private String operator;
        private Map<String,Boolean> myTfMap;
        private Map<String,String> myStrMap;
        
        public CriteriaEvaluator(String val1,String oper, String val2,
                                 Map<String,Boolean> tfMap,
                                 Map<String,String> strMap) {
            operator = oper;
            myTfMap  = tfMap;
            myStrMap = strMap;
                 
            // Check if the first value is a boolean or string                     
            if (tfMap.containsKey(val1)) {
                bVal1 = tfMap.get(val1);
                if (pattern.compile('[tT][rR][uU][eE]').matcher(val2).matches()) {
                    bVal2 = True;
                }
                else {
                    bVal2 = False;
                }
            }
            else if (strMap.containsKey(val1)){
                strVal1 = strMap.get(val1);
                if (val2 == 'null') {
                    strVal2 = null;
                }
                else if (val2.startsWith('"') || val2.startsWith('\'')) {
                    // It's quoted.  Get rid of them.
                    val2 = val2.substring(1,val2.length()-1);
                }
                strVal2 = val2;
                
            }
            else {
                // OK, this var isn't defined.  Default to boolean false
                bVal1 = False;
            } 
            
        }
        public Boolean isTrue() {
            if ((operator == '=') || (operator == '==')) {
                // if bVal1 is null, these are strings
                if (bVal1 != null) {
                    return bVal1 == bVal2;
                }
                else {
                    return strVal1 == strVal2;
                }
            }
            else if (operator  == '!=') {
                // if bVal1 is null, these are strings
                if (bVal1 != null) {
                    return bVal1 != bVal2;
                }
                else {
                    return strVal1 != strVal2;
                }
            }
            else if (operator == '>') {
                // only valid for integer values
                return Integer.valueOf(strVal1) > Integer.valueOf(strVal2);
            }
            else if (operator == '<') {
                // only valid for integer values
                return Integer.valueOf(strVal1) < Integer.valueOf(strVal2);
            }
            else if (operator == '>=') {
                // only valid for integer values
                return Integer.valueOf(strVal1) >= Integer.valueOf(strVal2);
            }
            else if (operator == '<=') {
                // only valid for integer values
                return Integer.valueOf(strVal1) <= Integer.valueOf(strVal2);
            }
            throw new FlowException('Unknown operator: '+operator);
        }
                    
    }
            
}
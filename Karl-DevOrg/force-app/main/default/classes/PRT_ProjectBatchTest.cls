@isTest
public class PRT_ProjectBatchTest {
@testSetup
    public static void testSetup(){
        List<User> userList = new List<User>();
        userList = PRT_TestDataFactory.createUsers(3,'System Administrator',true);
    }
    static testMethod void testExecuteMethod(){
        List<User> userList = new List<User>([SELECT Id FROM USER WHERE Email Like 'PRTUser%']);
        List<Portfolio__c> portfolioDataList = PRT_TestDataFactory.createPortfolio(1,true);
        
        Project__c projectRec = new Project__c();
        projectRec.Name = 'Test';
        projectRec.Projected_Date_of_Completion__c = Date.today()+5;
        projectRec.Project_Start_Date__c = Date.today();
        projectRec.Status__c = 'Completion Sign-off	';
        projectRec.Gus_Project__c = '';
        projectRec.Delivery_Team_Priority__c = 12;
        projectRec.Customer_POC__c = 'Test';
        projectRec.Project_Health_Comments__c = 'Test';
        projectRec.Deliverables__c = 'Test data';
        projectRec.Project_Overview_Purpose__c = 'Test Data';
        insert projectRec;
        
        Project__c projectRec1 = new Project__c();
        projectRec1.Name = 'Test';
        projectRec1.Projected_Date_of_Completion__c = Date.today()+5;
        projectRec1.Project_Start_Date__c = Date.today();
        projectRec1.Status__c = 'Completion Sign-off	';
        projectRec1.Gus_Project__c = projectRec.Id;
        projectRec1.Delivery_Team_Priority__c = 12;
        projectRec1.Customer_POC__c = 'Test';
        projectRec1.Project_Health_Comments__c = 'Test';
        projectRec1.Deliverables__c = 'Test data';
        projectRec1.Project_Overview_Purpose__c = 'Test Data';
        insert projectRec1;
        
        List<PPM_Project_c__x> projectList = new List<PPM_Project_c__x>();
        PPM_Project_c__x project = new PPM_Project_c__x();
        project.ExternalId = projectRec.Id;
        project.Name__c = 'Test';
        project.Actual_End_Date_c__c = Date.today()+5;
        project.Actual_Start_Date_c__c = Date.today();
        project.Description_c__c = 'Test';
        project.Project_Health_c__c = 'Completed';
        project.Build_Planned_End_Date_c__c = Date.today()+5;
        project.Business_Case_c__c = 'Test Data';
        project.Customer_POC_c__c = 'Test';
        project.Delivery_Team_Priority_c__c = 23;
        project.Project_Deliverables_c__c = 'TestData';
        project.Project_Health_Comments_c__c = 'TEst Data';
        projectList.add(project);
        
        PRT_ProjectBatch.mockallGusProjectList.addAll(projectList);
        
        Test.startTest();
            PRT_ProjectBatch pib = new PRT_ProjectBatch();
            Id batchId = Database.executeBatch(pib);
        Test.stopTest();
    }
}
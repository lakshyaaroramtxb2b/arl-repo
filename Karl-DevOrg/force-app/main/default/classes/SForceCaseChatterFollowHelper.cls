/**
 * What are we doing here?
 * 
 * Ent Security Engineers want to discourage use of chatter and notify users to use case comments
 * 
 * Solution:
 * 1. Sec Org - Schecule JOB & Case Obj Triggers:  Listen to Case changes on Security Org (For EntSec RecType only)
 *    - When NEW: Chatter Subscribe - Add SForce Case entity to Chatter Stream (Sends email notifications on new posts)
 *    - On DELETE / UPDATE (Status is CLOSED): Chatter Unsubscribe - Remove SForce Case entity from Chatter Stream
 *    - Existing open cases / Any Trigger failures: Schedule Job will handle the Chatter Subscription
 * 
 * 2. SForce Case - Chatter Email Notification Parsing
 *    - Foward emails to SecOrg - EmailService
 *    - Parse email to find feedItemId
 *    - From feedItem find the Parent (To filter Cases of EntSec RecType only)
 *    - Post chatter comment (customizable with label) to notify user to add case comments instead
 *    - BOOKMARK the post to subscribe to notifications when Chatter comments are added (Streams only send on new posts NOT comments)
 * 
 * 3. FYI/TODO...
 *     - Enable any chatter related perms for Esa Coordinator
 *     - Profile - Ltng Exp to set streaming email prefs (IF no way to configure through REST is found)
 *     - Assumption 1-1 relation between secorg case & sf case
 *    
 */
public with sharing class SForceCaseChatterFollowHelper {

    private static final String urlDomainIdentifier = 'salesforce.com/';
    private static final String urlFeedItemIdentifier = '0D5';
    private final static String SFORCE_SERVICE_BASE_URL = ESARestHelper.getCalloutUrlForSfdcRest(ESARestHelper.SFORCE_NAMED_CREDENTIAL);
    private final static String SEC_ORG_SERVICE_BASE_URL = ESARestHelper.getCalloutUrlForSfdcRest(ESARestHelper.SECORG_NAMED_CREDENTIAL);
    // We don't want to hard code the RecordType Id's to make the code work seemlessly between sandbox and production etc
    private final static String ENTSEC_CASE_RECORDTYPE_NAME = 'Enterprise Security';
    private final static String SFORCE_ENTSEC_CASE_RECORDTYPE_NAME = 'Enterprise Security Request';
    private final static Id ENTSEC_CASE_RECORDTYPE_ID = Schema.SObjectType.Case.getRecordTypeInfosByName().get(ENTSEC_CASE_RECORDTYPE_NAME).getRecordTypeId();
    private static Id SFORCE_ENTSEC_CASE_RECORDTYPE_ID;
    private static List<String> CASE_CLOSED_STATUS;
    private static List<String> SFORCE_CASE_CLOSED_STATUS;
    private static Set<String> ENTSEC_APPS_TEAM_USER_IDS;

    public SForceCaseChatterFollowHelper() {
       
    }

    /*
    * We cache data that doesn't change frequently, as we don't want scheduled jobs to refetch the same data every 10 mins
    * Call this method to refresh the cached data may be once a day or so
    */
    public static void clearCache() {
        CASE_CLOSED_STATUS = null;
        SFORCE_CASE_CLOSED_STATUS = null;
        ENTSEC_APPS_TEAM_USER_IDS = null;
    }

    private static String findFeedItemId(String textBody) {
        // Find Matching URL Format '...salesforce.com/0D5..../...'
        Integer startIndex = textBody.indexOf(urlDomainIdentifier + urlFeedItemIdentifier);
        if (startIndex != -1) {
            // Now get the actual feedItem ID Start Index
            startIndex = startIndex + urlDomainIdentifier.length();
            return textBody.substring(startIndex).substringBefore('?').substringBefore('/');
        }
        return null;
    }
 
    private static String getSForceEntSecCaseId(String feedItemId, String sessionId) {
        
        String respBody = ESARestHelper.callRESTAPI(sessionId, SFORCE_SERVICE_BASE_URL + '/sobjects/FeedItem/' + feedItemId, 
                                                        ESARestHelper.METHOD_GET, 
                                                        ESARestHelper.REQ_HEADER_JSON, null);
        Map<String, Object> feedItem = (Map<String, Object>) JSON.deserializeUntyped(respBody);
        String parentId = (String) feedItem.get('ParentId');

        respBody = ESARestHelper.callRESTAPI(sessionId, SFORCE_SERVICE_BASE_URL + '/sobjects/Case/' + parentId, 
                                                        ESARestHelper.METHOD_GET, 
                                                        ESARestHelper.REQ_HEADER_JSON, null);
        Map<String, Object> caseObj = (Map<String, Object>) JSON.deserializeUntyped(respBody);
        String recTypeId = (String) caseObj.get('RecordTypeId');

        if(isSForceEntSecCaseRecType(recTypeId, sessionId)) {
            return (String) caseObj.get('Id');
        }

        return null;
    }

    private static boolean isSForceEntSecCaseRecType(String recTypeId, String sessionId) {
        if (String.isBlank(recTypeId)) {
            return false;
        }
        // Initialize
        if (SFORCE_ENTSEC_CASE_RECORDTYPE_ID == null) {
            String respBody = ESARestHelper.callRESTAPI(sessionId, SFORCE_SERVICE_BASE_URL + '/sobjects/RecordType/' + recTypeId, 
                                                            ESARestHelper.METHOD_GET, 
                                                            ESARestHelper.REQ_HEADER_JSON, null);
            Map<String, Object> recType = (Map<String, Object>) JSON.deserializeUntyped(respBody);
            String recTypeName = (String) recType.get('Name');
            if (SFORCE_ENTSEC_CASE_RECORDTYPE_NAME == recTypeName) {
                SFORCE_ENTSEC_CASE_RECORDTYPE_ID = (Id) recType.get('Id');
            }
        }

        return recTypeId == SFORCE_ENTSEC_CASE_RECORDTYPE_ID;
    }

    private static void addSForceCaseComment(String sessionId, String caseId, String comment) {
        String reqBody = '{"CommentBody" : "' + comment + '", "ParentId" : "' + caseId + '"}';
        String respBody = ESARestHelper.callRESTAPI(sessionId, SFORCE_SERVICE_BASE_URL + '/sobjects/CaseComment', 
                                                        ESARestHelper.METHOD_POST, ESARestHelper.REQ_HEADER_JSON, reqBody);
    }

    private static void addSForceFeedComment(String sessionId, String feedItemId, String comment) {
        String reqBody = '{"body":{"messageSegments":[{"type":"Text","text":"' + comment + '" }]}}';
        String respBody = ESARestHelper.callRESTAPI(sessionId, SFORCE_SERVICE_BASE_URL + '/chatter/feed-elements/' + feedItemId + '/capabilities/comments/items', 
                                                        ESARestHelper.METHOD_POST, ESARestHelper.REQ_HEADER_JSON, reqBody);       
    }

    private static void addBookmark(String sessionId, String feedItemId) {
        String resBody = ESARestHelper.callRESTAPI(sessionId, SFORCE_SERVICE_BASE_URL + '/chatter/feed-elements/' + feedItemId + '/capabilities/bookmarks', 
                                                        ESARestHelper.METHOD_PATCH, ESARestHelper.REQ_HEADER_JSON, 
                                                        '{"isBookmarkedByCurrentUser": true}');
    }

    private static void bookmarkRecentSFCaseFeedElements(String sessionId, String sfCaseId) {
        String respBody = ESARestHelper.callRESTAPI(sessionId, SFORCE_SERVICE_BASE_URL + '/chatter/feeds/record/' + sfCaseId + '/feed-elements?pageSize=10', 
                                                        ESARestHelper.METHOD_GET, ESARestHelper.REQ_HEADER_JSON, null);

        Map<String, Object> result = (Map<String, Object>) JSON.deserializeUntyped(respBody);
        List<Object> elements = (List<Object>) result.get('elements');
        for (Object elementObj : elements) {
            Map<String, Object> feedElement = (Map<String, Object>) elementObj;
            String feedElementId = (String) feedElement.get('id');
            if (!String.isBlank(feedElementId)) {
                addBookmark(sessionId, feedElementId);
            }
        }
    }

    private static Set<String> getEntSecAppUserIds() {
        if (ENTSEC_APPS_TEAM_USER_IDS == null) {
            ENTSEC_APPS_TEAM_USER_IDS = new Set<String>();
            // Team members
            for (Group pubGroup : [SELECT Id FROM Group WHERE Type='Regular' AND DeveloperName =: ESA_AppConstants.PUB_GROUP_ENT_SEC_APPS LIMIT 1]) {
                // For now we are not going to check for nested groups
                for (GroupMember member : [SELECT UserOrGroupId FROM GroupMember WHERE GroupId =: pubGroup.Id]) {
                    ENTSEC_APPS_TEAM_USER_IDS.add(member.UserOrGroupId);
                }
                break;
            }
            // Queue
            for (Group entSecAppsQueue : [SELECT Id FROM Group WHERE Type = 'Queue' AND DeveloperName =: ESA_AppConstants.QUEUE_SEC_ASSURANCE LIMIT 1]) {
                ENTSEC_APPS_TEAM_USER_IDS.add(entSecAppsQueue.Id);
                break;
            }
        }
        return ENTSEC_APPS_TEAM_USER_IDS;
    }

    public static void notifyOnEntSecCaseChatter(String emailPlainText) {
        String feedItemId = findFeedItemId(emailPlainText);
        // If feedItem is found
        if (!String.isBlank(feedItemId)) {
            String sessionId = ESARestHelper.authenticateWithSOAP(ESARestHelper.SFORCE_NAMED_CREDENTIAL);
            String sfCaseId = getSForceEntSecCaseId(feedItemId, sessionId);
            if (!String.isBlank(sfCaseId) && sfCaseOwnerIsEntSecAppsUser(sfCaseId)) {
                addBookmark(sessionId, feedItemId); // Adding bookmark will send notification emails when new comments are added
                addSForceFeedComment(sessionId, feedItemId, Label.ESA_SFCase_AutoBot_Chatter_Comment_Notify_About_Case_Comment);
                // Commenting to stop any unwanted noise due to too many msgs
                //addSForceCaseComment(sessionId, sfCaseId, Label.ESA_SFCase_AutoBot_Case_Comment_Notify_Team_Member_About_Chatter_Post);
            }
        }
    }

    public static boolean isEntSecRecTypeWithSFCase(Case caseRec) {
        return caseRec.RecordTypeId == ENTSEC_CASE_RECORDTYPE_ID && !String.isBlank(caseRec.SupportForce_Case_Id__c);
    }

    // Within EntSec we don't want to include all teams
    public static boolean ownerIsEntSecAppsUser(Case caseRec) {
        return getEntSecAppUserIds().contains(caseRec.OwnerId);
    }

    // Within EntSec we don't want to include all teams
    public static boolean sfCaseOwnerIsEntSecAppsUser(String sfCaseId) {
        for(Case caseObj : [SELECT Id, RecordTypeId, OwnerId, SupportForce_Case_Id__c, status, SupportForce_Case_Chatter_Subscription__c
                                FROM Case 
                                    WHERE RecordTypeId =: ENTSEC_CASE_RECORDTYPE_ID
                                        AND SupportForce_Case_Chatter_Subscription__c != null 
                                            AND SupportForce_Case_Id__c =: sfCaseId
                                                AND status NOT IN :getAllClosedStatus()
                                                    AND OwnerId IN :getEntSecAppUserIds()
                                    LIMIT 1]) {
            return true; // If found
        }

        return false;
    }

    public static boolean isCaseClosed(String status) { 
        return getAllClosedStatus().contains(status);
    }

    public static boolean isSForceCaseClosed(String sessionId, String status) { 
        return getAllSForceClosedStatus(sessionId).contains(status);
    }

    public static List<String> getAllClosedStatus() {
        if (CASE_CLOSED_STATUS == null) {
            CASE_CLOSED_STATUS = new List<String>();
            for (CaseStatus closedStatus :[SELECT Id, IsClosed, MasterLabel FROM CaseStatus WHERE IsClosed = true] ) {
                CASE_CLOSED_STATUS.add(closedStatus.MasterLabel);
            }
        }
        return CASE_CLOSED_STATUS;
    }

    public static List<String> getAllSForceClosedStatus(String sessionId) {
        if (SFORCE_CASE_CLOSED_STATUS == null) {
            SFORCE_CASE_CLOSED_STATUS = new List<String>();
            String respBody = ESARestHelper.callRESTQuery(sessionId, SFORCE_SERVICE_BASE_URL, ESARestHelper.REQ_HEADER_JSON,
                                'SELECT+MasterLabel+FROM+CaseStatus+WHERE+IsClosed=true');

            Map<String, Object> result = (Map<String, Object>) JSON.deserializeUntyped(respBody);
            List<Object> items = (List<Object>) result.get('records');
            for (Object closedStatusObj : items) {
                Map<String, Object> closedStatus = (Map<String, Object>) closedStatusObj;
                SFORCE_CASE_CLOSED_STATUS.add((String) closedStatus.get('MasterLabel'));
            }
        }
        return SFORCE_CASE_CLOSED_STATUS;
    }

    private static void unSubscribeToSFCase(String sfCaseId, String subscribtionId, String sessionId) {
        removeFromStream(sessionId, subscribtionId, sfCaseId);
    }

    private static String subscribeToSFCase(String caseId, String sfCaseId, String sessionId) {
        String streamId = getNextAvailableChatterStreamId(sessionId);
        if (streamId == null) {
            Boolean foundAndRemoved = cleanupStreams(sessionId);
            if (foundAndRemoved) {
                streamId = getNextAvailableChatterStreamId(sessionId);
            }   
        }

        // If no available streams
        if (streamId == null) { 
            throw new SObjectException('Cannot find/create chatter stream');
        }

        addToStream(sessionId, streamId, sfCaseId);
        bookmarkRecentSFCaseFeedElements(sessionId, sfCaseId);

        return streamId;
    }

    private static void addToStream(String sessionId, String streamId, String sfCaseId) {
        String respBody = ESARestHelper.callRESTAPI(sessionId, SFORCE_SERVICE_BASE_URL + '/chatter/streams/' + streamId, 
                                                        ESARestHelper.METHOD_PATCH, 
                                                        ESARestHelper.REQ_HEADER_JSON, 
                                                        '{"subscriptionsToAdd" : {"subscriptions" : [{"entityId" : "' + sfCaseId + '"}]}}');

    }

    private static void removeFromStream(String sessionId, String streamId, String sfCaseId) {
        String respBody = ESARestHelper.callRESTAPI(sessionId, SFORCE_SERVICE_BASE_URL + '/chatter/streams/' + streamId, 
                                                        ESARestHelper.METHOD_PATCH, 
                                                        ESARestHelper.REQ_HEADER_JSON, 
                                                        '{"subscriptionsToRemove" : {"subscriptions" : [{"entityId" : "' + sfCaseId + '"}]}}');
    }

    private static Boolean cleanupStreams(String sessionId) {
        Boolean found = false;
        // 100 Max Streams allowed, to get all streams set pageSize to 100
        // Get only Ent Sec related streams by passing query param q=nameprefix
        String respBody = ESARestHelper.callRESTAPI(sessionId, SFORCE_SERVICE_BASE_URL + '/chatter/streams?pageSize=100', 
                                                        ESARestHelper.METHOD_GET, 
                                                        ESARestHelper.REQ_HEADER_JSON, null);
        Map<String, Object> resultObj = (Map<String, Object>) JSON.deserializeUntyped(respBody);
        List<Object> streams = new List<Object>((List<Object>) resultObj.get('items'));
        for(Object streamObj : streams) {
            Map<String, Object> stream = (Map<String, Object>) streamObj;
            if (!isEntSecRelatedStream(stream)) {
                continue;
            }
            List<Object> subscriptions = new List<Object>((List<Object>) stream.get('subscriptions'));
            List<Map<String, Object>> subscriptionsToRemove = new List<Map<String, Object>>();
            for(Object subscriptionObj : subscriptions) {
                Map<String, Object> subscription = (Map<String, Object>) subscriptionObj;
                String sfCaseId = (String) subscription.get('id');
                respBody = ESARestHelper.callRESTAPI(sessionId, SFORCE_SERVICE_BASE_URL + '/sobjects/Case/' + sfCaseId, 
                                                        ESARestHelper.METHOD_GET, 
                                                        ESARestHelper.REQ_HEADER_JSON, null);
                Map<String, Object> sfCaseObj = (Map<String, Object>) JSON.deserializeUntyped(respBody);
                String status = (String) sfCaseObj.get('Status');
                if (isSForceCaseClosed(sessionId, status)) {
                    removeFromStream(sessionId, (String) stream.get('id'), (String) sfCaseObj.get('Id'));
                    found = true;
                }
            }  
        }

        return found;
    }

    private static String getNextAvailableChatterStreamId(String sessionId) {
        // 100 Max Streams allowed, to get all streams set pageSize to 100
        String respBody = ESARestHelper.callRESTAPI(sessionId, SFORCE_SERVICE_BASE_URL + '/chatter/streams?pageSize=100', 
                                                        ESARestHelper.METHOD_GET, 
                                                        ESARestHelper.REQ_HEADER_JSON, null);
        Map<String, Object> resultObj = (Map<String, Object>) JSON.deserializeUntyped(respBody);
        List<Object> streams = new List<Object>((List<Object>) resultObj.get('items'));

        for(Object streamObj : streams) {
            Map<String, Object> stream = (Map<String, Object>) streamObj;
            if (!isEntSecRelatedStream(stream)) {
                continue;
            }
            List<Object> subscriptions = new List<Object>((List<Object>) stream.get('subscriptions'));

            if (subscriptions.size() < 25) { // Max allowed is 25
                return (String) stream.get('id');
            }
        }

        // We reach here only when no available stream is found
        if (streams.size() < 100) { // 100 streams max allowed
            return getNewStreamId(sessionId);
        }

        // Couldn't create or find an available stream
        return null;
    }

    private static String getNewStreamId(String sessionId) {
        // Create Stream
        String respBody = ESARestHelper.callRESTAPI(sessionId, SFORCE_SERVICE_BASE_URL + '/chatter/streams', 
                                                        ESARestHelper.METHOD_POST, 
                                                        ESARestHelper.REQ_HEADER_JSON, 
                                                        '{"name" : "'+ SFORCE_ENTSEC_CASE_RECORDTYPE_NAME + System.now().format('yyyy-MM-dd-HH:mm:ss') + '"}');
        Map<String, Object> stream = (Map<String, Object>) JSON.deserializeUntyped(respBody);
        String streamId = (String) stream.get('id');
        // Set Notifications to EveryPost
        respBody = ESARestHelper.callRESTAPI(sessionId, SFORCE_SERVICE_BASE_URL + '/connect/chatter/subscriptions/notification/' 
                                                    + streamId + '/members/' + getSForceCurrentUserId(sessionId), 
                                                        ESARestHelper.METHOD_POST, 
                                                        ESARestHelper.REQ_HEADER_JSON, 
                                                        '{"notificationFrequency":"EachPost"}');
        return streamId;
    }

    private static String getSForceCurrentUserId(String sessionId) {
        String respBody = ESARestHelper.callRESTAPI(sessionId, SFORCE_SERVICE_BASE_URL + '/chatter/users/me', 
                                                        ESARestHelper.METHOD_GET, ESARestHelper.REQ_HEADER_JSON, null);
        Map<String, Object> userObj = (Map<String, Object>) JSON.deserializeUntyped(respBody);
        return (String) userObj.get('id');
    }

    private static Boolean isEntSecRelatedStream(Map<String, Object> stream) {
        String streamName = (String) stream.get('name');
        return streamName.startsWith(SFORCE_ENTSEC_CASE_RECORDTYPE_NAME);
    }

    // Returns the cases that needs subscription changes
    // Belongs to EntSecApps Team (Need to Subscribe or Unsubscribe)
    // Not Belong to EntSecApps Anymore but still has chatter subscription Id (Need to Unsubscribe)
    public static List<Case> getEntSecCasesToManageChatterSubscription() {
        List<Case> cases = new List<Case>();
        for(Case caseObj : [SELECT Id, RecordTypeId, OwnerId, SupportForce_Case_Id__c, status, SupportForce_Case_Chatter_Subscription__c
                                FROM Case 
                                    WHERE RecordTypeId =: ENTSEC_CASE_RECORDTYPE_ID
                                        AND ((SupportForce_Case_Chatter_Subscription__c = null AND status NOT IN :getAllClosedStatus() AND OwnerId IN :getEntSecAppUserIds())
                                                OR (SupportForce_Case_Chatter_Subscription__c != null AND (status IN :getAllClosedStatus() OR OwnerId NOT IN :getEntSecAppUserIds()))) 
                                        ORDER BY CreatedDate DESC LIMIT 10]) {
            cases.add(caseObj);
        }
        return cases;
    }

    public static Boolean isChatterSubscriptionChange(Case caseRec) {
        if (isChatterSubscribe(caseRec) || isChatterUnSubscribe((caseRec))) {
            return true;
        }
        return false;
    }

    private static Boolean isChatterSubscribe(Case caseRec) {
        return !isCaseClosed(caseRec.status) && ownerIsEntSecAppsUser(caseRec) && !hasChatterSubscription(caseRec);
    }

    private static Boolean isChatterUnSubscribe(Case caseRec) {
        return (isCaseClosed(caseRec.status) || !ownerIsEntSecAppsUser(caseRec)) && hasChatterSubscription(caseRec);
    }

    public static Boolean hasChatterSubscription(Case caseRec) {
        return !String.isBlank(caseRec.SupportForce_Case_Chatter_Subscription__c);
    }

    private static String manageChatterSubscription(String sessionId, Case caseRec) {

        try {
            if (isChatterSubscribe(caseRec)) {
                return subscribeToSFCase(caseRec.Id, caseRec.SupportForce_Case_Id__c, sessionId);
            } else if (isChatterUnSubscribe(caseRec)) {
                unSubscribeToSFCase(caseRec.Id, caseRec.SupportForce_Case_Chatter_Subscription__c, sessionId);
                return null;
            } 
        } catch (Exception e) {
            Esa_DebugService.WriteException(e, 'SForceCaseChatterFollowHelper', 
                String.format('Exception in manageChatterSubscription method, caseId={0}, supportForceCaseId={1}, chatterSubscription={2}',
                new List<String>{caseRec.Id, caseRec.SupportForce_Case_Id__c, caseRec.SupportForce_Case_Chatter_Subscription__c}
            ));
        }
        return caseRec.SupportForce_Case_Chatter_Subscription__c; // Return original
    }

    public class DeleteChatterSubscriptionJob implements Queueable, Database.AllowsCallouts { 
        List<Case> deletedCases = new List<Case>();
        String sessionId;

        public DeleteChatterSubscriptionJob(List<Case> caseList) {
            // Filter For Ent Sec Cases
            for(Case caseRec : caseList) {
                if (!String.isBlank(caseRec.SupportForce_Case_Chatter_Subscription__c) && isEntSecRecTypeWithSFCase(caseRec)) {
                    this.deletedCases.add(caseRec);
                }
            }
        }

        public void execute(QueueableContext context) {
            if (!this.deletedCases.isEmpty()) {
                sessionId = ESARestHelper.authenticateWithSOAP(ESARestHelper.SFORCE_NAMED_CREDENTIAL);
            } 
            for (Case caseRec : deletedCases) {
                removeFromStream(sessionId, caseRec.SupportForce_Case_Chatter_Subscription__c, caseRec.SupportForce_Case_Id__c);
            }
        }
    }

    public class ManageChatterSubscriptionJob implements Queueable, Database.AllowsCallouts {
        List<Case> casesToUpdate = new List<Case>();
        List<Case> cases = new List<Case>();
        String sessionId;

        public ManageChatterSubscriptionJob(List<Case> caseList) {
            cases.addAll(caseList);
        }

        public ManageChatterSubscriptionJob(List<Case> caseList, List<Case> caseListToUpdate, String sessionId) {
            // No need to filter as we come here on subsequent chaining calls
            this.cases.addAll(caseList);
            this.casesToUpdate.addAll(caseListToUpdate);
            this.sessionId = sessionId;
        }

        public void execute(QueueableContext context) {
            if (cases.size() > 0) {
                if (sessionId == null) {
                    sessionId = ESARestHelper.authenticateWithSOAP(ESARestHelper.SFORCE_NAMED_CREDENTIAL);
                } 
                // Pop the case
                Case caseRec = cases.remove(0);
                String subscriptionId = SForceCaseChatterFollowHelper.manageChatterSubscription(sessionId, caseRec);

                if (caseRec.SupportForce_Case_Chatter_Subscription__c != subscriptionId) {
                    casesToUpdate.add(new Case(Id = caseRec.Id, SupportForce_Case_Chatter_Subscription__c = subscriptionId));
                }
            }
            
            // Chained calls to subscribe one by one case to manage callouts efficiently by keeping within limits
            if (!cases.isEmpty()) { 
                System.enqueueJob(new ManageChatterSubscriptionJob(cases, casesToUpdate, sessionId));
            } else {
                // Finally execute update (FYI, updates inbetween callouts are not allowed)
                update casesToUpdate;
            }
        }
    }
}
public class  CDI_ExportCSV_Extention {
  @AuraEnabled(cacheable=true)
  public Static List<wrapperClass> getDataAssets(String currentId) {
    System.debug(currentId);
      List<wrapperClass> dataAssetData= new List<wrapperClass>();
        for(CDI_Data_Asset__c da:[SELECT Id,Name,Data_Asset_Description__c,Data_Asset_Type__c,Parent_Data_Asset__r.Name,Data_Retention_Period__c,Source_System_Name__c,Source_Data_Asset_Code__c,Link_To_Balancing_Argument_Document__c,Link_To_Excepn_For_Basis_For_Processing__c,IsEncrypted__c,Is_Personal_Data_Limited_To_Purpose__c,Has_DPIA_Been_Performed__c,Can_Consumers_Access_Data__c,Data_Classification__r.Name,Data_Category__r.Name,Data_Identification_Category__r.Name,Crypto_Algorithm__r.Name,Data_Store__r.Name,Basis_For_Processing__r.Name,Time_Unit__r.Name,Retention_Process__r.Name,Customer_Data_Category__r.Name,GUS_Team__r.Name,IsDeleted__c, Status__c,Comments__c FROM CDI_Data_Asset__c where Data_Store__c=:currentId])
      {
                wrapperClass wc = new wrapperClass();
                wc.Name=da.Name==null?'':da.Name;
                wc.Data_Asset_Description = da.Data_Asset_Description__c==null?'':da.Data_Asset_Description__c;
                wc.Data_Asset_Type=da.Data_Asset_Type__c== null?'':da.Data_Asset_Type__c;
                wc.Parent_Data_Asset = da.Parent_Data_Asset__r.Name == null?'':da.Parent_Data_Asset__r.Name;
                wc.Source_System_Name=da.Source_System_Name__c== null?'':da.Source_System_Name__c;
                wc.Source_Data_Asset_Code=da.Source_Data_Asset_Code__c==null?'':da.Source_Data_Asset_Code__c;
                wc.Link_To_Balancing_Arguments=da.Link_To_Balancing_Argument_Document__c==null?'':da.Link_To_Balancing_Argument_Document__c;
                wc.Link_To_Excpn_For_Basis_For_Processing=da.Link_To_Excepn_For_Basis_For_Processing__c==null?'':da.Link_To_Excepn_For_Basis_For_Processing__c;
                wc.IsEncrypted=da.IsEncrypted__c;
                wc.Data_Retention_Period=da.Data_Retention_Period__c==null?0:da.Data_Retention_Period__c;
                wc.Can_Consumer_Access_Data=da.Can_Consumers_Access_Data__c;
                wc.Data_Classification=da.Data_Classification__r.Name==null?'':da.Data_Classification__r.Name;
                wc.Data_Category=da.Data_Category__r.Name==null?'':da.Data_Category__r.Name;
                wc.Data_Store=da.Data_Store__r.Name==null?'':da.Data_Store__r.Name;
                wc.Data_Identification_Category=da.Data_Identification_Category__r.Name==null?'':da.Data_Identification_Category__r.Name;
                wc.Customer_Data_Category=da.Customer_Data_Category__r.Name==null?'':da.Customer_Data_Category__r.Name;
                wc.Crypto_Algorithm=da.Crypto_Algorithm__r.Name==null?'':da.Crypto_Algorithm__r.Name;
                wc.Is_Personal_Data_Limited_To_Purpose=da.Is_Personal_Data_Limited_To_Purpose__c;
                wc.IsDeleted=da.IsDeleted__c;      
                wc.Basis_For_Processing=da.Basis_For_Processing__r.Name==null?'':da.Basis_For_Processing__r.Name;
                wc.Has_DPIA_Been_Performed=da.Has_DPIA_Been_Performed__c;
                wc.Time_Unit=da.Time_Unit__r.Name==null?'':da.Time_Unit__r.Name;
                wc.Retention_Process=da.Retention_Process__r.Name==null?'':da.Retention_Process__r.Name;
                wc.Gus_Team=da.GUS_Team__r.Name==null?'':da.GUS_Team__r.Name;
                wc.Status=da.Status__c==null?'':da.Status__c;
                wc.Comments=da.Comments__c==null?'':da.Comments__c;
                System.debug(wc);
                dataAssetData.add(wc);
      }
      //System.debug(dataAssetData);
      return dataAssetData;
}
public class wrapperClass{
  
      @AuraEnabled public string Name;
      @AuraEnabled public string Data_Asset_Description;
      @AuraEnabled public string Data_Asset_Type;
      @AuraEnabled public string Parent_Data_Asset;
      @AuraEnabled public string Source_System_Name;
      @AuraEnabled public string Source_Data_Asset_Code;
      @AuraEnabled public string Link_To_Balancing_Arguments;
      @AuraEnabled public string Link_To_Excpn_For_Basis_For_Processing;
      @AuraEnabled public boolean IsEncrypted;
      @AuraEnabled public decimal Data_Retention_Period;
      @AuraEnabled public boolean Can_Consumer_Access_Data;
      @AuraEnabled public string Data_Classification;
      @AuraEnabled public string Data_Category;
      @AuraEnabled public string Data_Store;
      @AuraEnabled public string Data_Identification_Category;
      @AuraEnabled public string Customer_Data_Category;
      @AuraEnabled public string Crypto_Algorithm;
      @AuraEnabled public boolean Is_Personal_Data_Limited_To_Purpose;
      @AuraEnabled public boolean IsDeleted;     
      @AuraEnabled public string Basis_For_Processing;
      @AuraEnabled public boolean Has_DPIA_Been_Performed;
      @AuraEnabled public string Time_Unit;
      @AuraEnabled public string Retention_Process;
      @AuraEnabled public string Gus_Team;
      @AuraEnabled public string Status;
      @AuraEnabled public string Comments;
  }
}
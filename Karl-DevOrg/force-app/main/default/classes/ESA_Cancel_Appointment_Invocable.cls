global class ESA_Cancel_Appointment_Invocable {
    
    @InvocableMethod
    webservice static void cancel_Appointment(List <ESA_Security_Request__c> esaRequestL) {

        ESA_Security_Request__c esaRequest = esaRequestL [0];
        String aptEventId = esaRequest.GCal_EventId__c;
        

        Esa_Entity__c esaEntity = [SELECT Id,Google_Calendar_Name__c FROM Esa_Entity__c
                                   WHERE EntityCode__c =: esaRequest.Entity_Code__c];

        String calendarName = esaEntity.Google_Calendar_Name__c;

        try{
            if(!test.isRunningTest()) {
                esagutil.ESAGoogleOAuthUtil googleAPI = new esagutil.ESAGoogleOAuthUtil(calendarName);
                googleAPI.deleteEvent(aptEventId);
            }
            
            esaRequest.Office_Hours__c = null;
            esaRequest.Reservation_Key__c = null;
            esaRequest.GCal_EventId__c = null;
            update esaRequest;
          }
            catch (exception e){
                Esa_DebugService.WriteException(e,'ESAWizardController','CancelReservation - Unable to access GCal');
            }
        
           

        
    }
    
}
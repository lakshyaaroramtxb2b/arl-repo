/**
 * @author Ben Harvie
 * @email ben.harvie@salesforce.com
 * @description Test class for KARL_ShareFollowUpToAuditFirmBatch - adapted from MTX developed test
 * class developed for KARL_ShareAudReqItemToAuditFirmBatchTest
*/
@isTest
public with sharing class KARL_ShareFollowUpToAuditFirmBatchTest {
	/**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Test data setup.
    */
    @testSetup
    public static void testSetup(){
        UserRole userrole = [Select Id, DeveloperName From UserRole Where DeveloperName = 'CEO' Limit 1];

        User adminUser = [Select Id, UserRoleId From User Where Profile.Name='System Administrator' AND isActive = true Limit 1];
        
        adminUser.UserRoleId = userRole.Id;
        update adminUser;
        
        System.runAs(adminUser){
            Account acc = new Account(Name = 'salesforce.com - ESA Office Hours');
            insert acc;
            
            Contact con = new Contact(LastName = 'test',AccountId = acc.Id, Email = 'test@gmail.com');
            insert con;
            
            ARL_TestDataFactory.createCommunityUserwithSpecificCon(con.Id, 'a12345@bqww.com',true);
        }
    }
        
    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Testing insert and update of Audit Team contacts
    */
    @istest
    public static void unitTest(){ 
        User communityUser = [SELECT id FROM User WHERE FirstName = 'Community'];
        
        //Permission Set Assignment
        PermissionSet pSet = [SELECT Name FROM PermissionSet WHERE NAME = 'External_Auditor'];
        insert new PermissionSetAssignment(AssigneeId = communityUser.id, PermissionSetId = pSet.Id);
        
        Contact con = [SELECT Id FROM Contact LIMIT 1];
        System.runAs(communityUser){
            KARL_Audit_Firm__c auditFirm = ARL_TestDataFactory.createAuditFirm('testAuditFirm');
            insert auditFirm;
            
            KARL_Audit_Team__c auditTeam1 = ARL_TestDataFactory.createAuditTeam('test');
            auditTeam1.KARL_Audit_Firm__c = auditFirm.Id;
            insert auditTeam1;
            
            KARL_Audit_Team__c auditTeam2 = ARL_TestDataFactory.createAuditTeam('test');
            auditTeam2.KARL_Audit_Firm__c = auditFirm.Id;
            insert auditTeam2;
            
            KARL_Audit_Team_Contacts__c auditTeamCon1 = ARL_TestDataFactory.createAuditTeamContact(auditTeam1.Id,con.Id);
            
            KARL_Audit_Team_Contacts__c auditTeamCon2 = ARL_TestDataFactory.createAuditTeamContact(auditTeam2.Id,con.Id);
            
            Audit_Cycle__c auditCycle = ARL_TestDataFactory.createAuditCycle();
            insert auditCycle;

            KARL_Cycle_Follow_up__c followUp = ARL_TestDataFactory.createFollowUp(auditTeam1.Id,auditCycle.Id);
            insert followUp;
            
            Datetime yesterday = Datetime.now().addDays(-1);
        	Test.setCreatedDate(followUp.Id, yesterday);
        
            Map<Id,Set<Id>> auditFirmIdToContactIdMap = new Map<Id,Set<Id>>();
            auditFirmIdToContactIdMap.put(auditFirm.Id,new Set<Id>{auditTeamCon1.Id,auditTeamCon2.Id});
        
            Test.startTest();
            	KARL_ShareFollowUpToAuditFirmBatch objBatchAdd = new KARL_ShareFollowUpToAuditFirmBatch('Trigger',auditFirmIdToContactIdMap.keySet(),auditFirmIdToContactIdMap,'Add');
            	Database.executebatch(objBatchAdd,100);
            	KARL_ShareFollowUpToAuditFirmBatch objBatchRemove = new KARL_ShareFollowUpToAuditFirmBatch('Trigger',auditFirmIdToContactIdMap.keySet(),auditFirmIdToContactIdMap,'Remove');
            	Database.executebatch(objBatchRemove,100);
            
            	KARL_ShareFollowUpToAuditFirmBatch objBatch1 = new KARL_ShareFollowUpToAuditFirmBatch(0,true);
            	Database.executebatch(objBatch1,100);
            
            	KARL_ExecuteFollowUpShareBatch.executeFollowUpShareBatch(1);
            
            	String chron = '0 0 23 * * ?';        
                System.schedule('testScheduledApex', chron, new KARL_ShareFollowUpToAuditFirmBatch());
                
                List<KARL_Cycle_Follow_up__Share> shareRule = [SELECT id FROM KARL_Cycle_Follow_up__Share];
            
                // Expecting one sharing rule for Audit Team 1
                system.assertEquals(1, shareRule.size(), 'Sharing not set appropriately');
            Test.stopTest(); 
        }
    }
}
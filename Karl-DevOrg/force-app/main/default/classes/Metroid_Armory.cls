public with sharing class Metroid_Armory {

    Map<String,Schema.RecordTypeInfo> rtMapByName;

    /*
    3/2/2016 - This method invokes the ESAWizardController to discover the next available office hours slot, and find the difference in days. 
    */
    public void Scan_OH_Availability(string requestType){
        
        string SI = [select id from ESA_Service_Item__c where name =:requestType limit 1].id;
        ESA_AppointmentDate next; // ESAW.MetroidHelper(requestType);
        
        // NO LONGER A VALID REFERENCE
        //ESAWizardController ESAW = new ESAWizardController();
        //ESAW.theSecurityRequest = new ESA_Security_Request__c(ESA_Service_Item__c=SI);
        
     
        //For(List<ESA_AppointmentDate> L : ESAW.makeAppointmentMap().values()){
        //    For(ESA_AppointmentDate AD : L){
        //        if(AD.taken==false){
        //            if(next==null){
        //                next = AD;
        //            } else if (next.apptDate.date()>AD.apptDate.date()){
        //                next = AD;
        //            }
        //        }
        //    }
        //}
        
        if(next!=null){
            Metroid__c m = new Metroid__c();
            m.recordTypeId=grabRecordTypeID('Days To OH');
            m.name=requestType;
            m.date__c = datetime.now();
            m.Value_Integer__c = date.today().daysbetween(next.apptDate.date());
            insert m;
        }

    }


    /*
    3/2/106 - This method looks at the number of office hours in a given month, the number of holidays, and the total number of days available for OH.
    ASSUMPTION: Hardcoded multiplier of 4 assumes there are always 4 potential slots per day. Change if assumption becomes untrue.
    */
    public void Scan_OH_Utilization(string[] requestTypes,integer monthchange){

    	Date reportMonth = Date.Today().addMonths(monthchange);
    	Map<String, String> DAY_SHORT_NAME = new Map<String, String>{'Monday' => 'Mon', 'Tuesday' => 'Tue', 'Wednesday' => 'Wed', 'Thursday' => 'Thu', 'Friday' => 'Fri', 'Saturday' => 'Sat', 'Sunday' => 'Sun'}; 


    	// Get the utilized office hours for the previous month in the correct requestTypes
    	integer utilized = 0;
    	list<ESA_Security_Request__c> used = [select id from ESA_Security_Request__c 
																where 
    															ESA_Service_Item__r.ItemCategory__r.name in: requestTypes 
    															and
																CALENDAR_MONTH(Office_Hours_Reservation__c) =: reportMonth.month()
																and
																CALENDAR_YEAR(Office_Hours_Reservation__c) =: reportMonth.year()];															
		utilized = used.size();	


		// Get the total number of possible office hours slots for the month.
		integer total = 0;
		set<string> days = new set<string>();
		
		For(ESA_Service_Item_Category__c c : [select name, office_hours_days__c from ESA_Service_Item_Category__c where name in: requestTypes]){
			For(string s : c.office_hours_days__c.split(';', -2)){
				days.add(DAY_SHORT_NAME.get(s));
			}
		}

		for (Integer day = 1; day <= date.daysInMonth(reportMonth.year(), reportMonth.month()); day++) {
    		datetime dt = Datetime.newInstance(reportMonth.year(), reportMonth.month(), day);
    		if(days.contains(dt.format('E'))){	    			
    			total++;
    		}
		}


		//integer nwd = [select count() from ESA_Non_Working_Date__c  where Calendar_Month(Non_Working_Date__c) =: reportMonth.month() and Calendar_Year(Non_Working_Date__c) =: reportMonth.year()];
		List<ESA_Non_Working_Date__c> nwdL = [select id,Non_Working_Date__c from ESA_Non_Working_Date__c  where Calendar_Month(Non_Working_Date__c) =: reportMonth.month() and Calendar_Year(Non_Working_Date__c) =: reportMonth.year()];
		for(ESA_Non_Working_Date__c nwd : nwdL){
			if(days.contains(datetime.newinstance(nwd.Non_Working_Date__c.year(),nwd.Non_Working_Date__c.month(),nwd.Non_Working_Date__c.day()).format('E'))){
				total--;
			}
		}

  		
	    Metroid__c m = new Metroid__c();
        m.recordTypeId=grabRecordTypeID('OH Utilization');
        m.name=requestTypes[0];
        m.date__c = datetime.now();
        m.Value_Integer__c = utilized;
        m.Total_Integer__c = total*4;     
        insert m;
    }





    public id grabRecordTypeID(string rtname){
        if(rtMapByName==null){
            Schema.DescribeSObjectResult d = Schema.SObjectType.Metroid__c;
            rtMapByName = d.getRecordTypeInfosByName();
        }
        return rtMapByName.get(rtname).getRecordTypeID();
    }


}
/**
 * @File Name          : IEM_GroupsAndPermissionSetController.cls
 * @Description        : 
 * @Author             : Banshi
 * @Group              : 
 * @Last Modified By   : Banshi, Swarnima 
 * @Last Modified On   : 11/15/2019, 4:42:52 PM, 05/13/2020, 15:36:00
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    11/11/2019   Banshi     Initial Version
**/
public without sharing class IEM_GroupsAndPermissionSetController {
    @AuraEnabled(cacheable=true)
    public static List<Group> getPublicGroups(){
        Set<String> groupNameSet = new Set<String>();
        for(GRC_Public_Groups_and_Permission_Set__mdt publicGroupAndPermSetRec:[SELECT id ,label ,type__c 
                                                                                FROM GRC_Public_Groups_and_Permission_Set__mdt
                                                                                WHERE type__c!=null]
                                                                                ){
                                                                                    if(publicGroupAndPermSetRec.type__c == 'Public Group'){
                                                                                        groupNameSet.add(publicGroupAndPermSetRec.label);
                                                                                        System.debug('groupName --> '+ groupNameSet);
                                                                                    }
                                                                                }
        return [SELECT Id,Type,Name,DeveloperName,createdDate FROM Group WHERE Type= 'Regular' AND Name IN: groupNameSet];
    }
    @AuraEnabled(cacheable=true)
    public static List<PermissionSet> getPermissionSets(){
        Set<String> permSetNameSet = new Set<String>();
        for(GRC_Public_Groups_and_Permission_Set__mdt publicGroupAndPermSetRec:[SELECT id ,label ,type__c 
                                                                                FROM GRC_Public_Groups_and_Permission_Set__mdt
                                                                                WHERE type__c!=null]
                                                                                ){
                                                                                    if(publicGroupAndPermSetRec.type__c == 'Permission Set'){
                                                                                        permSetNameSet.add(publicGroupAndPermSetRec.label);
                                                                                        System.debug('permSetName --> '+ permSetNameSet);
                                                                                    }
                                                                                }
        List<PermissionSet> perm = [SELECT Name, Label, Id, Description, Type, CreatedDate FROM PermissionSet WHERE isOwnedByProfile = false AND Label IN: permSetNameSet Order By Name ASC];
        System.debug('perm record --> '+ perm);
        return [SELECT Name, Label, Id, Description, Type, CreatedDate FROM PermissionSet WHERE isOwnedByProfile = false AND Label IN: permSetNameSet Order By Name ASC];
    }
    @AuraEnabled(cacheable=true)
    public static List<PermissionSetGroup> getPermissionSetGroups(){
        Set<String> permSetGroupNameSet = new Set<String>();
        for(GRC_Public_Groups_and_Permission_Set__mdt publicGroupAndPermSetRec:[SELECT id ,label ,type__c 
                                                                                FROM GRC_Public_Groups_and_Permission_Set__mdt
                                                                                WHERE type__c!=null]
                                                                                ){
                                                                                    if(publicGroupAndPermSetRec.type__c == 'Permission Set Group'){
                                                                                        permSetGroupNameSet.add(publicGroupAndPermSetRec.label);
                                                                                        System.debug('permSetName --> '+ permSetGroupNameSet);
                                                                                    }
                                                                                }
        List<PermissionSetGroup> perm = [SELECT MasterLabel, DeveloperName, Description, CreatedDate, Id FROM PermissionSetGroup WHERE MasterLabel IN: permSetGroupNameSet ORDER BY MasterLabel ASC];
        System.debug('perm group record --> '+ perm);
        return [SELECT MasterLabel, DeveloperName, Description, CreatedDate, Id FROM PermissionSetGroup WHERE MasterLabel IN: permSetGroupNameSet ORDER BY MasterLabel ASC];
    }
    @AuraEnabled
    public static List<GetMembersWrapper> getMembers(String type, Id recordId){
        List<GetMembersWrapper>   memberIdNameWrapperList = new List<GetMembersWrapper>();
        List<Id> memberIds = new List<Id>();
        if(type == 'Group') {
            Set<Id> userOrGroupIds = new Set<Id>();
            for(GroupMember gm: [SELECT UserOrGroupId, GroupId FROM GroupMember Where GroupId =:recordId]){
                userOrGroupIds.add(gm.UserOrGroupId);
            }
            if(!userOrGroupIds.isEmpty()){
                for(User u: [SELECT Id,Name,Username from User WHERE Id IN :userOrGroupIds AND IsActive = true ORDER BY Username]){
                    //memberIds.add(u.Id);
                    GetMembersWrapper   memberIdNameWrapper = new GetMembersWrapper();
                    memberIdNameWrapper.MemberId = u.Id;
                    memberIdNameWrapper.Name = u.Username; 
                    memberIdNameWrapperList.add(memberIdNameWrapper);
                }
            }
        }else if(type == 'PermissionSetGroup'){
            for(PermissionSetAssignment pms: [SELECT Id, PermissionSetGroupId, AssigneeId,Assignee.Name,Assignee.userName, SystemModstamp FROM PermissionSetAssignment WHERE PermissionSetGroupId= :recordId ORDER BY Assignee.Name]){
                //memberIds.add(pms.AssigneeId);
                GetMembersWrapper   memberIdNameWrapper = new GetMembersWrapper();
                    memberIdNameWrapper.MemberId = pms.AssigneeId;
                    memberIdNameWrapper.Name = pms.Assignee.Username; 
                    memberIdNameWrapperList.add(memberIdNameWrapper);
            }
        }else{
            for(PermissionSetAssignment pms: [SELECT Id, PermissionSetId, AssigneeId,Assignee.Name,Assignee.userName, SystemModstamp FROM PermissionSetAssignment WHERE PermissionSetId= :recordId ORDER BY Assignee.Name]){
                //memberIds.add(pms.AssigneeId);
                GetMembersWrapper   memberIdNameWrapper = new GetMembersWrapper();
                    memberIdNameWrapper.MemberId = pms.AssigneeId;
                    memberIdNameWrapper.Name = pms.Assignee.Username; 
                    memberIdNameWrapperList.add(memberIdNameWrapper);
            }
        }
        System.debug(memberIdNameWrapperList);
        return memberIdNameWrapperList;
    }
    
    @AuraEnabled(cacheable=true)
    public static List<User> getUsers(){
        //List<Id> usersIdlist = getMembers(type, recordId);
       
        return [SELECT Id,Name,Username FROM User Where IsActive = true Order By Name ASC];
    }
    @AuraEnabled
    public static String updateMembers(String type, Id recordId, String newMemberData, List<Id> existingMembers){
        List<GetMembersWrapper> wrapperData = new List<GetMembersWrapper>();
        wrapperData = (List<GetMembersWrapper>) JSON.deserialize(newMemberData, List<GetMembersWrapper>.class);
        system.debug('type : '+ type);
        system.debug('recordId : ' + recordId);
        System.debug('newMemberData -> '+wrapperData);
            Set<Id> memberIdsToRemove = new Set<Id>();
            List<Id> newMemberIds = new List<Id>();
            //List<Id> existingMembers = new List<Id>();
            for(GetMembersWrapper getId: wrapperData){
                newMemberIds.add(getId.MemberId);
                System.debug('newMemberIds--> '+newMemberIds);
            }
            if(newMemberIds.isEmpty() && !existingMembers.isEmpty()){
                memberIdsToRemove.addAll(existingMembers);
                System.debug('memeberId to Remove'+ memberIdsToRemove);
            }

            for(Id existingMemberId : existingMembers){
                if(!newMemberIds.contains(existingMemberId)){
                    memberIdsToRemove.add(existingMemberId);
                }
            }
            Set<Id> memberIdsToAdd = new Set<Id>();
            for(Id newMemberId : newMemberIds){
                if(!existingMembers.contains(newMemberId)){
                    memberIdsToAdd.add(newMemberId);
                    
                }
                System.debug('memberIdsToAdd--> '+ memberIdsToAdd);
            }
        if(type == 'Group'){
            try{
                if(!memberIdsToRemove.isEmpty()){
                    DELETE [SELECT Id,UserOrGroupId,GroupId FROM GroupMember Where GroupId =:recordId AND UserOrGroupId IN : memberIdsToRemove];
                    system.debug('Delete');
                }   
                if(!memberIdsToAdd.isEmpty()){
                    List<GroupMember> listOfNewMembers = new List<GroupMember>();
                    for(Id newMemberId : memberIdsToAdd){
                        GroupMember newMember = new GroupMember();
                        newMember.UserOrGroupId = newMemberId;
                        newMember.GroupId = recordId;
                        listOfNewMembers.add(newMember);
                    }
                    insert listOfNewMembers;
                }
            }catch(Exception e){
                throw new AuraHandledException('Something went wrong!');
            }
        }else if(type == 'PermissionSetGroup'){
            try{
                if(!memberIdsToRemove.isEmpty()){
                    DELETE [SELECT Id FROM PermissionSetAssignment Where PermissionSetGroupId =:recordId AND AssigneeId IN : memberIdsToRemove];
                }   
                if(!memberIdsToAdd.isEmpty()){
                    List<PermissionSetAssignment> listOfNewMembers = new List<PermissionSetAssignment>();
                    for(Id newMemberId : memberIdsToAdd){
                        PermissionSetAssignment newMember = new PermissionSetAssignment();
                        newMember.PermissionSetGroupId = recordId;
                        newMember.AssigneeId = newMemberId;
                        listOfNewMembers.add(newMember);
                    }
                    insert listOfNewMembers;
                }
            }catch(Exception e){
                System.debug('##error'+e.getMessage());
                throw new AuraHandledException('Something went wrong!');
            }
        }else{
            try{
                if(!memberIdsToRemove.isEmpty()){
                    DELETE [SELECT Id FROM PermissionSetAssignment Where PermissionSetId =:recordId AND AssigneeId IN : memberIdsToRemove];
                }   
                if(!memberIdsToAdd.isEmpty()){
                    List<PermissionSetAssignment> listOfNewMembers = new List<PermissionSetAssignment>();
                    for(Id newMemberId : memberIdsToAdd){
                        PermissionSetAssignment newMember = new PermissionSetAssignment();
                        newMember.PermissionSetId = recordId;
                        newMember.AssigneeId = newMemberId;
                        listOfNewMembers.add(newMember);
                    }
                    insert listOfNewMembers;
                }
            }catch(Exception e){
                System.debug('##error'+e.getMessage());
                throw new AuraHandledException('Something went wrong!');
            }
        }
        
        return null;
    }

    // Retreive user after searching the string
    @AuraEnabled(Cacheable = true)
    public static list<User> retreiveUser(String strAccName, String newMemberIds) {
        list<User> listOfUser = new List<User>();
        List<Id> memberIdList = new lIst<Id>();
        List<GetMembersWrapper> wrapperData = new List<GetMembersWrapper>();
        wrapperData = (List<GetMembersWrapper>) JSON.deserialize(newMemberIds, List<GetMembersWrapper>.class);
        for(GetMembersWrapper getId: wrapperData){
            memberIdList.add(getId.MemberId);
            System.debug('newMemberIds--> '+newMemberIds);
        }
        
            strAccName = '%' + strAccName + '%';
        listOfUser = [SELECT Id,Name,Username 
                      FROM User 
                      Where (IsActive = true AND (Username LIKE :strAccName OR Name LIKE :strAccName)) AND Id NOT IN :memberIdList Order By Name ASC LIMIT 100 ];
        System.debug(listOfUser + 'Usrer list>>>');
        if(listOfUser.isEmpty()) {
            throw new AuraHandledException('No Record Found..');
            }
        
        return listOfUser; 
    }

    // Retreive user after searching the string
    @AuraEnabled(Cacheable = true)
    public static list<User> retreiveExistingUser(String strAccName, String newMemberIds) {
        list<User> listOfUser = new List<User>();
        List<Id> memberIdList = new lIst<Id>();
        List<GetMembersWrapper> wrapperData = new List<GetMembersWrapper>();
        wrapperData = (List<GetMembersWrapper>) JSON.deserialize(newMemberIds, List<GetMembersWrapper>.class);
        for(GetMembersWrapper getId: wrapperData){
            memberIdList.add(getId.MemberId);
            System.debug('newMemberIds--> '+newMemberIds);
        }
        
            strAccName = '%' + strAccName + '%';
        listOfUser = [SELECT Id,Name,Username 
                      FROM User 
                      Where (IsActive = true AND (Username LIKE :strAccName OR Name LIKE :strAccName)) AND Id IN :memberIdList Order By Name ASC LIMIT 100 ];
        System.debug(listOfUser + 'Usrer list>>>');
        if(listOfUser.isEmpty()) {
            throw new AuraHandledException('No Record Found..');
            }
        
        return listOfUser; 
    }
    public class GetMembersWrapper{
        @AuraEnabled public Id MemberId;
        @AuraEnabled public String Name;
    }
   
}
@IsTest
global class INTEG_LoggingTest {
    global class MockHttpResponseGenerator implements HttpCalloutMock {
        global Integer callCount = 0;
        global HTTPRequest lastRequest;
        global HTTPResponse respond(HTTPRequest req) {
            callCount += 1;
            lastRequest = req;
            HttpResponse res = new HttpResponse();
            res.setStatusCode(200);
            return res;
        }
    }
    
    @IsTest
    global static void testLimits() {
        MockHttpResponseGenerator mock = new MockHttpResponseGenerator();
        Test.setMock(HttpCalloutMock.class, mock);
        
        INTEG_Logging logger = INTEG_Logging.getLogger(INTEG_LoggingTest.class);
        logger.sendCalloutInTest = true;
        System.assert(logger.config != null);
        System.assert(logger.maxCallouts > 1);
        
        logger.error('Blah');
        System.assert(mock.callCount == 1);
        
        // this should add an error message to show a soft callout limit hit
        logger.maxCallouts = 3;
        logger.error('blah2');
        System.assert(mock.callCount == 2);
        System.debug(mock.lastRequest.getBody());
        System.assert(mock.lastRequest.getBody().contains('Hit soft callout limit'));
        
        // This shouldn't call out
        logger.critical('blah3');
        System.assert(mock.callCount == 2);
        
        // Nor should this
        logger.maxCallouts = 2;
        logger.critical('blah4');
        System.assert(mock.callCount == 2);
        
        // Or this
        logger.maxCallouts = 1;
        logger.critical('blah5');
        System.assert(mock.callCount == 2);
        
        
    } 
}
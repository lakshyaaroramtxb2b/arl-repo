@isTest
public class SBE_CaseTriggerHelperTest {
    
    @testSetup
    public static void testSetup(){
        User admin = IEM_TestDataFactory.createUser(); 
        
        List<User> portalUser = new List<User>();        
        System.runAs(admin){
            //Create Accounts
            List<Account> accounts = new List<Account>();
            accounts= IEM_TestDataFactory.createAccounts(1,true);
            //Create Contacts
            List<Contact> contacts = new List<Contact>();
            contacts= IEM_TestDataFactory.createContacts(4,accounts[0].id,false);
            contacts[0].LastName = 'Test user M 1';
            contacts[0].Employee_ID__c = 'E-208283764';
            contacts[0].title = 'Director';
            contacts[1].LastName = 'Test user M 2';
            contacts[1].Employee_ID__c = 'E-208283765';
            contacts[1].title = 'Senior Director';
            contacts[2].LastName = 'Test user M 3';
            contacts[2].Employee_ID__c = 'E-208283766';
            contacts[2].title = 'VP';
            
            contacts[3].LastName = 'Issue Owner';
            contacts[3].Employee_ID__c = 'E-208283767';
            contacts[3].title = 'VP';
            contacts[3].Management_Chain_Level_03__c = 'Test user M 1 (E-208283764)';
            contacts[3].Management_Chain_Level_02__c = 'Test user M 2 (E-208283765)';
            contacts[3].Management_Chain_Level_01__c = 'Test user M 3 (E-208283766)';
            contacts[0].recordTypeid = IEM_Constants.IEM_CONTACTRECORDTYPEID;
            contacts[1].recordTypeid = IEM_Constants.IEM_CONTACTRECORDTYPEID;
            contacts[2].recordTypeid = IEM_Constants.IEM_CONTACTRECORDTYPEID;
            contacts[3].recordTypeid = IEM_Constants.IEM_CONTACTRECORDTYPEID;
            contacts[0].Org62_User_ID__c = '2F01I4D0000009R4yq';
            contacts[1].Org62_User_ID__c = '2F01I4D0000009R5yq';
            contacts[2].Org62_User_ID__c = '2F01I4D0000009R6yq';
            contacts[3].Org62_User_ID__c = '2F01I4D0000009R7yq';
            insert contacts;
            //Create poratl User
            portalUser.addAll(IEM_TestDataFactory.createPortalUserList(contacts,true));                       
        }
        
    }
    
    @isTest
    static void updateSBERecordsTest() {
        UserRole userRole = [SELECT Id FROM UserRole LIMIT 1];       
        List<user> portalUser = [SELECT id,contactID from User where Username LIKE '%testPortalIEMHVCPUser%'];
        Map<String,Map<String,String>> dependenciesMap = new Map<String,Map<String,String>>();        
        
        //Create Accounts
        List<Account> accounts = new List<Account>();
        accounts= IEM_TestDataFactory.createAccounts(1,true);
        //Create Contacts
        List<Contact> contacts = new List<Contact>();
        contacts= IEM_TestDataFactory.createContacts(2,accounts[0].id,true);
        
        //Create Case         
        List<Case> caseList = IEM_TestDataFactory.createIntakeIssueCase(2,contacts,false);        
        caseList[0].Requestor__c = portalUser[0].contactID;
        caseList[0].Issue_Owner__c = portalUser[0].contactID;
        caseList[0].Action_Plan_Owner__c = portalUser[0].contactID;
        caseList[0].Severity_Assessor_User__c = userinfo.getUserId();
        caseList[0].SLA_Extension_Id__c = 'EX101';
        insert CaseList;
        
        TM_Security_Work_SLA_Extension_c__x workSLA = new TM_Security_Work_SLA_Extension_c__x();
        //workSLA.TM_Work_Subject_c__c = 'Test Subject';
        workSLA.ExternalId= 'EX101';
        workSLA.TM_Definition_of_Done_c__c= 'Completed';
        workSLA.Describe_Technical_Work_to_Remediate__c= 'Nothing';
        workSLA.Due_Date_Justification__c= 'No';
        workSLA.Technical_Description_of_Issue__c= 'No';
        workSLA.TM_SLA_Extension_Status_c__c = IEM_Constants.SBE_STATUS_DENIED;
        //workSLA.TM_Work_Cloud_c__c = 'Very High (EVP)';
        SBE_CaseTriggerHelper.mockedRequests.add(workSLA);
        
        test.startTest();
        
        caseList[0].Status = IEM_Constants.IEM_CASE_STATUS_INTERNAL_REVIEW;
        caseList[0].IEM_Case_Type__c = IEM_Constants.IEM_EXCEPTION;
        caseList[0].Security_Decision__c = IEM_Constants.IEM_SECURITY_DECISION_REJECTED;
        update caseList;
        
        test.stopTest();
    }
    
     @isTest
	static void syncApprovalToSBETest() {
        UserRole userRole = [SELECT Id FROM UserRole LIMIT 1];       
        List<user> portalUser = [SELECT id,contactID from User where Username LIKE '%testPortalIEMHVCPUser%'];
		Map<String,Map<String,String>> dependenciesMap = new Map<String,Map<String,String>>();        
        
        //Create Accounts
        List<Account> accounts = new List<Account>();
        accounts= IEM_TestDataFactory.createAccounts(1,true);
        //Create Contacts
        List<Contact> contacts = new List<Contact>();
        contacts= IEM_TestDataFactory.createContacts(2,accounts[0].id,true);
        
        //Create Case         
        List<Case> caseList = IEM_TestDataFactory.createIntakeIssueCase(2,contacts,false);        
        caseList[0].Requestor__c = portalUser[0].contactID;
        caseList[0].Issue_Owner__c = portalUser[0].contactID;
        caseList[0].Action_Plan_Owner__c = portalUser[0].contactID;
        caseList[0].Severity_Assessor_User__c = userinfo.getUserId();
        caseList[0].SLA_Extension_Id__c = 'EX101';
        caseList[0].Business_Decision__c = IEM_Constants.SBE_STATUS_EXTENSION_APPROVED;
         caseList[0].Business_Approver__c  = portalUser[0].Id;
        caseList[0].Action_Plan_Status__c = 'In Progress';
        insert CaseList;
        List<Milestone__c> milestoneList = IEM_TestDataFactory.createMilestoneRecords(1,CaseList[0].id,true);
        test.startTest();
        	
            caseList[0].Status = IEM_Constants.IEM_CASE_STATUS_SECURITY_DECISION;
            caseList[0].Action_Plan_Status__c = 'Submitted';
            caseList[0].Security_Reviewer__c = userinfo.getUserId();
            caseList[0].Root_Cause__c = 'test';
            caseList[0].Definition_of_Done__c = 'test data';
            caseList[0].Estimated_Remediation_Date__c = date.today();
            caseList[0].Action_Plan__c = 'test data';
        	//caseList[0].Number_of_Milestones__c = 1;
            update caseList;
        	
        test.stopTest();
    }
}
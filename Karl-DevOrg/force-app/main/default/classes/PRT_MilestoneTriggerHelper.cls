/***********************************************************
* Helper class for PRT_MilestoneTrigger
* Created by    - Prashant Gupta
* Date      - 24th July 2019
************************************************************/
public class PRT_MilestoneTriggerHelper {
    
    /*
    * Created by - Prashant Gupta 
    * Validate Dates on milstone and dependencies
    */
    public static void milestoneDateUpdate(List<Milestone_PRT__c> newList, Map<id,Milestone_PRT__c> oldMap){
        List<Milestone_PRT__c> childMilestoneList = new List<Milestone_PRT__c>();
        List<Milestone_PRT__c> parentMilestoneList = new List<Milestone_PRT__c>();
        for(Milestone_PRT__c mile : newList){
            if(mile.Due_Date__c!=null || mile.Start_Date__c!=null){
                    if(oldMap!=null && mile.Due_Date__c != oldMap.get(mile.id).Due_Date__c){
                        childMilestoneList.add(mile);                        
                    }else if (oldMap!=null && mile.Start_Date__c != oldMap.get(mile.id).Start_Date__c){
                        parentMilestoneList.add(mile);
                    }
            }
        }
        Map<id,Date> parentMilestoneVsMaximumDueDateMap = New Map<id,Date>();
        Map<id,Milestone_PRT__c> childMilestoneVsParentMap = New Map<id,Milestone_PRT__c>();
        for(Milestone_Dependency__c dependency : [SELECT id, Milestone__c,
                                                   Milestone__r.Start_Date__c,
                                                   Dependent_Milestone__c, 
                                                   Dependent_Milestone__r.Due_Date__c
                                                  FROM Milestone_Dependency__c 
                                                   WHERE Milestone__c IN :parentMilestoneList
                                                  OR Dependent_Milestone__c IN :childMilestoneList]){
            if(!parentMilestoneVsMaximumDueDateMap.containsKey(dependency.Milestone__c)
              || dependency.Dependent_Milestone__r.Due_Date__c > parentMilestoneVsMaximumDueDateMap.get(dependency.Milestone__c)){
                parentMilestoneVsMaximumDueDateMap.put(dependency.Milestone__c,dependency.Dependent_Milestone__r.Due_Date__c);
            }
            childMilestoneVsParentMap.put(dependency.Dependent_Milestone__c,
                                                      new Milestone_PRT__c(id=dependency.Milestone__c,
                                                                           Start_Date__c=dependency.Milestone__r.Start_Date__c));
            childMilestoneList.add(new Milestone_PRT__c (id=dependency.Dependent_Milestone__c,
                                                        Due_date__c=dependency.Dependent_Milestone__r.Due_Date__c ))    ;                  
                                                      
        }
        System.debug('>>>CHildMileStoneList = '+childmilestoneList);
        System.debug('>>>parentMileStoneList = '+parentmilestoneList);
        for(Milestone_PRT__c mile : childMilestoneList){
            if(childMilestoneVsParentMap.containsKey(mile.id)
               && childMilestoneVsParentMap.get(mile.id).Start_Date__c < mile.Due_Date__c){
                   mile.addError(Label.PRT_Milestone_Start_Date_Validation);
               }
        }
        for(Milestone_PRT__c mile : parentMilestoneList){
            if(parentMilestoneVsMaximumDueDateMap.containsKey(mile.id) && 
               parentMilestoneVsMaximumDueDateMap.get(mile.id)>mile.Start_Date__c){
                   mile.addError(Label.PRT_Milestone_Start_Date_Validation);
               }
        }
    }

    /*
    * Created by - Virendra Yadav
    * Update Epic record on the Milestone update
    * This future functon is used to create GUS Epic records
    * This fuctionality has been moved to future since external objects cannot be queried in trigger context.
    */
    public static void updateGusEpicRecords(List<Milestone_PRT__c> newList, Map<id,Milestone_PRT__c> oldMap){
        if(!System.isfuture() && !System.isBatch()) {
            Set<Id> milestonesIds = new Set<Id>();
            for (Milestone_PRT__c milestone : newList) {
                milestonesIds.add(milestone.id);
            }
            if(milestonesIds != null && !milestonesIds.isEmpty()) {        
                updateExternalGusEpicRecords(milestonesIds);
            }
        }
    }
    /*
    * Created by - Virendra Yadav
    * Update Epic record on the Milestone update
    * This future functon is used to create GUS Epic records
    * This fuctionality has been moved to future since external objects cannot be queried in trigger context.
    */
    @future
    public static void updateExternalGusEpicRecords(Set<Id> milestoneIds){
        List<ADM_Epic_c__x> GUS_list = new List<ADM_Epic_c__x>();
        Set<Id> epicExternalIds = new Set<Id>();        
        Map<Id,Id> epicIdExternalIdMap = new Map<Id,Id>();
        System.debug('milestoneIds: '+milestoneIds);
        if(milestoneIds!=null && !milestoneIds.isEmpty()){
            for(Milestone_PRT__c milestone : [SELECT ID,GUS_Epic__c,Project__r.status__c,Project__r.RecordTypeId,Project__r.Gus_Project__c FROM Milestone_PRT__c WHERE ID IN :milestoneIds AND GUS_Epic__c != null]){
                if(milestone.Project__r.RecordTypeId == PRT_Constants.PRT_PROJECT_PROJECT_RECORDTYPEID) {
                    System.debug('>>>>>>>>>>>ENTERED IN project recordType'+milestone);
                    epicExternalIds.add(milestone.GUS_Epic__c);                    
                }
            }

            for (ADM_Epic_c__x epic : [SELECT ExternalId, Id FROM ADM_Epic_c__x WHERE ExternalId IN :epicExternalIds]) {
                epicIdExternalIdMap.put(epic.ExternalId, epic.Id);
            }
            for(Milestone_PRT__c milestone : [SELECT ID,Completion_Date__c,Project__c,Project__r.RecordTypeId, Name, Start_Date__c, Description__c,GUS_Epic__c, Status__c, Due_Date__c, Project__r.status__c, Project__r.Gus_Project__c, Milestone_Health__c FROM Milestone_PRT__c WHERE ID IN :milestoneIds AND GUS_Epic__c != null]){
                if(milestone.Project__r.RecordTypeId == PRT_Constants.PRT_PROJECT_PROJECT_RECORDTYPEID) {
                    System.debug('>>>>>>>>>>>ENTERED IN project RecordType'+milestone);                    
                    ADM_Epic_c__x epicObject = new ADM_Epic_c__x(id=epicIdExternalIdMap.get(milestone.GUS_Epic__c));
                    epicObject.Project_c__c = milestone.Project__r.Gus_Project__c;
                    epicObject.End_Date_c__c = milestone.Completion_Date__c;
                    epicObject.Start_Date_c__c = milestone.Start_Date__c;
                    
                    epicObject.Description_c__c = milestone.Description__c.replaceAll('\\<.*?>',' ');
                    epicObject.Name__c = milestone.Name;
                    epicObject.Health_c__c = milestone.Status__c;
                    epicObject.Success_Criteria_c__c = milestone.Id;
                    epicObject.Epic_Health_Comments_c__c = milestone.Milestone_Health__c;

                    // epicObject.Project_c__c = milestone.Project__c; 
                    epicObject.Planned_End_Date_c__c = milestone.Due_Date__c;
                    System.debug('>>>>>>>>>>>ENTERED IN EPIC OBJECT'+epicObject);                    
                    GUS_list.add(epicObject);
                }
            }
            system.debug('GUS_list--> '+ GUS_list);
            try{
                if(GUS_list!=null && !GUS_list.isEmpty()){ 
                Database.updateImmediate(GUS_list);
            }
            }catch(Exception e){
                System.debug('Exception -> '+ e.getMessage());
            }
            
        }
    }
    public static void insertGusEpicRecords(List<Milestone_PRT__c> newList, Map<id,Milestone_PRT__c> oldMap){
        Set<Id> projectIds = new Set<Id>();
        for (Milestone_PRT__c milestone : newList) {
            if(milestone.Project__c != null) {
               projectIds.add(milestone.Project__c);
            }
        }
        if(projectIds != null && !projectIds.isEmpty() /*&& !test.isRunningTest()*/) {
            PRT_GUSSyncHelper.createEpicRecordsOnGUS(projectIds);
        }
    }
    /*
    * Created by - Prashant Gupta
    * Date - 23 Dec 2019
    */
   /* public static void milestoneValidationRule(List<Milestone_PRT__c> newList, Map<id,Milestone_PRT__c> oldMap){
        Map<id,Milestone_PRT__c> updatedMilestoneList = new Map<id,Milestone_PRT__c>();
        for(Milestone_PRT__c mile : newList){
            if(mile.Due_Date__c !=null && mile.Start_Date__c!=null && 
               (oldMap==null|| mile.Due_Date__c !=oldMap.get(mile.id).Due_Date__c ||  mile.Start_Date__c !=oldMap.get(mile.id).Start_Date__c)){
                updatedMilestoneList.put(mile.id,mile);
            }
        }
        for(Milestone_PRT__c mile : [SELECT ID, Start_Date__c, Due_Date__c ,Project__r.Projected_Date_of_Completion__c,Project__r.Project_Start_Date__c FROM Milestone_PRT__c WHERE id IN : updatedMilestoneList.keySet()]){
            if(mile.Project__r.Projected_Date_of_Completion__c!=null 
               && mile.Project__r.Project_Start_Date__c!=null 
               && mile.Due_Date__c!=null && mile.Start_Date__c!=null){
                   if((mile.Due_Date__c.daysBetween(mile.Start_Date__c)) < ((mile.Project__r.Projected_Date_of_Completion__c.daysBetween(mile.Project__r.Project_Start_Date__c))*0.25)){
                       updatedMilestoneList.get(mile.id).addError('Milestone length cannot be more than quarter of projects length.');
                   }
               }
        }
    }*/
    
    /* created by - Swarnima Mandhata
     * This methods Provids the validation rule for every stage*/
    public static void checkRequiredFields(List<Milestone_PRT__c> newList, Map<id,Milestone_PRT__c> oldMap){
        Map<String,Integer> statusVsNumber = new Map<String,Integer>();
        statusVsNumber.put(PRT_Constants.PRT_MILE_STATUS_NOT_STARTED,1);
        statusVsNumber.put(PRT_Constants.PRT_MILE_STATUS_ON_TRACK,2);
        statusVsNumber.put(PRT_Constants.PRT_MILE_STATUS_WATCH,3);
        statusVsNumber.put(PRT_Constants.PRT_MILE_STATUS_BLOCKED,4);
        statusVsNumber.put(PRT_Constants.PRT_MILE_STATUS_COMPLETE,5);
        System.debug('Map Of Milestone Status' + statusVsNumber);

        String type='Milestone_PRT__c';
        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Schema.SObjectType leadSchema = schemaMap.get(type);
        Map<String, Schema.SObjectField> fieldMap = leadSchema.getDescribe().fields.getMap();
        boolean hasError = False;

        for(Milestone_PRT__c milestone:newList){
            String error= 'Please populate these fields : ';
            if(oldmap!=null && milestone.Status__c !=oldMap.get(milestone.id).Status__c){
                if(statusVsNumber.get(milestone.status__c) >= 2){
                    if(milestone.Due_Date__c== null){
                        error += fieldMap.get('Due_Date__c').getDescribe().getLabel() + ', ';
                        hasError = True;
                    }
                    if(milestone.Program__c == null){
                        error += fieldMap.get('Program__c').getDescribe().getLabel() + ', ';
                        hasError = True;
                    }
                    if(milestone.Start_Date__c == null){
                        error += fieldMap.get('Start_Date__c').getDescribe().getLabel() + ', ';
                        hasError = True;
                    }
                    if(milestone.Percentage_Complete__c == null){
                        error += fieldMap.get('Percentage_Complete__c').getDescribe().getLabel() + ', ';
                        hasError = True;
                    }

                }
                if(statusVsNumber.get(milestone.status__c) == 5){
                    if(milestone.Completion_Date__c == null){
                        error += fieldMap.get('Completion_Date__c').getDescribe().getLabel() + ', ';
                        hasError = True;
                    }
                }
                if(hasError){
                    if(error.endsWith(', ')){
                        error= error.substringBeforeLast(', ');
                    }
                    milestone.addError(error);
                }  
            }

        }

    }
 
}
/**
 * @author: ralph@callaway.cloud
 */
public class SC_CleanInvalidEnrollments_Batch implements Database.Batchable<sObject>, Database.Stateful {
    
    public Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([
            SELECT 
                Contact__r.Email,
                Contact__r.Org62_User_Id__c,
                Contact__r.RecordType.DeveloperName,
                Training_Course__c,
                Id
            FROM Training_Course_Taken__c
            WHERE Contact__c = null 
                OR Contact__r.Email = null
                OR Contact__r.RecordTypeId != :Contact_Constants.RT_ID_SALESFORCE_EMPLOYEE
                OR (NOT Contact__r.Org62_User_Id__c LIKE '005%')
            ORDER BY Training_Course__c
        ]);
    }

    public void execute(Database.BatchableContext BC, List<Training_Course_Taken__c> enrollments) {
        Database.DeleteResult[] deleteResults = Database.delete(enrollments, false);
        logResults(deleteResults, enrollments);
    }

    private void logResults(Database.DeleteResult[] deleteResults, Training_Course_Taken__c[] enrollments) {
        for (Integer i = 0; i < deleteResults.size(); i++) {
            String[] row = new String[0];
            if (deleteResults[i].isSuccess()) {
                row.add('DELETED');
                row.add('');
            } else {
                row.add('ERROR');
                String[] errors = new String[0];
                for (Database.Error err : deleteResults[i].getErrors()) {
                    errors.add(String.valueOf(err));
                }
                row.add('"' + String.join(errors, '\n') + '"');
            }
            row.add(enrollments[i].Id);
            row.add(enrollments[i].Contact__c);
            row.add(enrollments[i].Contact__r.Email);
            row.add(enrollments[i].Contact__r.Org62_User_Id__c);
            row.add(enrollments[i].Contact__r.RecordType.DeveloperName);
            initResults(enrollments[i].Training_Course__c);
            results.get(enrollments[i].Training_Course__c).add(String.join(row, ','));
        }
    }
    
    public void finish(Database.BatchableContext BC) {
        if (!Test.isRunningTest() && !results.isEmpty()) {
            Map<Id, Training_Course__c> courses = new Map<Id, Training_Course__c>([
                SELECT Name FROM Training_Course__c WHERE Id IN :results.keySet()]);
            Attachment[] attachments = new Attachment[0];
            String[] courseNames = new String[0];
            for (Id courseId : results.keySet()) {
                Training_Course__c course = courses.get(courseId);
                courseNames.add(course.Name);
                Attachment attachment = new Attachment();
                attachment.Body = Blob.valueOf(String.join(results.get(courseId), '\n'));
                attachment.ContentType = 'text/csv';
                attachment.Name = course.Name + ' Clean Results ' + DateTime.now().format() + '.csv';
                attachment.ParentId = course.Id;
                attachments.add(attachment);    
            }
            insert attachments;
            Id[] attachmentIds = new Id[0];
            for (Attachment attachment : attachments) {
                attachmentIds.add(attachment.Id);
            }
            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
            email.setEntityAttachments(attachmentIds);
            email.setTargetObjectId(UserInfo.getUserId());
            email.setSubject(String.join(courseNames, '/') + ' Invalid Enrollments Cleanup Results ' + DateTime.now().format());
            email.setPlainTextBody('See attachments for results. Historical results are available in the training course attachment related list');
            email.setSaveAsActivity(false);
            email.setCCAddresses(Label.SC_TrainingBatchCC.split('\\s*,\\s*'));
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { email });
        }
    }

    String[] headerRow = new String[] {
        'Result',
        'Errors',
        'Enrollment Id',
        'Contact Id',
        'Contact Email',
        'Contact Org62 User Id',
        'Contact Record Type'
    };
    Map<Id, String[]> results = new Map<Id, String[]>();
    private void initResults(Id courseId) {
        if (!results.containsKey(courseId)) {
            results.put(courseId, new String[] { String.join(headerRow, ',') });    
        }
    }
}
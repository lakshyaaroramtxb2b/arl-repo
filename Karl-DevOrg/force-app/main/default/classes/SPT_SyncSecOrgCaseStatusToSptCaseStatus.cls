/**
 * Move the Security org Case Status (In Progress or Waiting on Customer Office Hours ) to Security org.
** Id batchJobId = Database.executeBatch(new SPT_SyncSecOrgCaseStatusToSptCaseStatus(), 100);
*/
public class SPT_SyncSecOrgCaseStatusToSptCaseStatus implements Schedulable, Database.Batchable<sObject>, Database.Stateful {
    
    public static SchedulableContext scCtx;
    public void execute(SchedulableContext ctx) {
      Database.executebatch(new SPT_SyncSecOrgCaseStatusToSptCaseStatus(), 100);
    }

    public Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([
            SELECT Id,
                   Enterprise_Security_Case_Number__c,
                   Status 
            FROM Case
            WHERE RecordTypeId =: SPT_Constants.SEC_RECORDTYPE_ID
            AND Status = 'In Progress'
            /*AND (Status = :Label.Esa_Case_Waiting_OH_Status
            OR Status = 'In Progress') */
        ]);
    }
    

    public void execute(Database.BatchableContext BC, List<Case> lstSecCases) {
        try {
            syncCaseStatus(lstSecCases);
        } catch (Exception e) {
            Esa_DebugService.WriteException(e, 'SPT_SyncSecOrgCaseStatusToSptCaseStatus', 'Exception in execute method');
        } finally {
            
        }
    }

    private void syncCaseStatus(List<Case> lstSecCases){
        
        Set<String> setCaseNumbers = new Set<String>();
        Map<String,String> mapCaseStatus = new Map<String, String>();
        
        for(Case secCase : lstSecCases){
            setCaseNumbers.add(secCase.Enterprise_Security_Case_Number__c);
            mapCaseStatus.put(secCase.Enterprise_Security_Case_Number__c, secCase.Status);
        }

        List<String> caseClosedStatus= String.ValueOf(Label.ESa_Closed_Case_Status).split(',');

        if(setCaseNumbers.size() > 0) {
            List<Case__x> lstSptCases = [SELECT ExternalId,
                                                Status__c,
                                                CaseNumber__c
                                        FROM Case__x
                                        WHERE CaseNumber__c IN :setCaseNumbers];

            List<Case__x> lstSptStatusChangeCases = new List<Case__x>();

            for(Case__x sptCase : lstSptCases) {
                
                if(!mapCaseStatus.containsKey(sptCase.CaseNumber__c)) continue;
                
                if(!caseClosedStatus.contains(sptCase.Status__c) && sptCase.Status__c != mapCaseStatus.get(sptCase.CaseNumber__c)) {
                    sptCase.Status__c = mapCaseStatus.get(sptCase.CaseNumber__c);
                    lstSptStatusChangeCases.add(sptCase);
                }
            }

            if(lstSptStatusChangeCases.size() > 0){
                List<Database.SaveResult> sr = Database.updateImmediate(lstSptStatusChangeCases);
            }

        }
        
    }

    public void finish(Database.BatchableContext BC) {
        if(!Test.isRunningTest()) {
           SPT_SyncSecOrgCaseStatusToSptCaseStatus batch = new SPT_SyncSecOrgCaseStatusToSptCaseStatus();
           System.scheduleBatch(batch, 'SPT_SyncSecOrgCaseStatusToSptCaseStatus', 5, 100);
        }
    }
}
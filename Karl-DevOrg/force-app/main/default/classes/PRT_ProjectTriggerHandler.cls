/***********************************************************
* Handler class for PRT_ProjectTrigger
* Created by 	- Prashant Gupta
* Date 		- 24th July 2019
************************************************************/
public class PRT_ProjectTriggerHandler {
    
    /***********************************************************
    * Created by 	- Prashant Gupta
    * Date 		- 24th July 2019
    * Before Update method Being Called from Trigger
    * Modified : 13-11-2019, Virendra Yadav, Added checkChildAssignmentLookups method
    ************************************************************/
    public static void beforeUpdate(List<Project__c> newList, Map<id,Project__c> newMap){
        //PRT_ProjectTriggerHelper.validateEditAccess(newList, newMap);
        PRT_ProjectTriggerHelper.updateChildProgramLookups(newList, newMap); 
        if(!PRT_ProjectTriggerHelper.PRT_PROJECT_RANK_UPDATE){
            PRT_ProjectTriggerHelper.checkRequiredFields(newList, newMap);   
        }
        PRT_ProjectTriggerHelper.convertProject(newList, newMap);       
        PRT_ProjectTriggerHelper.checkMilestoneClosure(newList, newMap); 
        PRT_ProjectTriggerHelper.populatePortfolioOwnerAndManager(newList, newMap);  
        //PRT_ProjectTriggerHelper.checkChildAssignmentLookups(newList, newMap); 
        //PRT_ProjectTriggerHelper.checkScoringFieldsForBusinessCase(newList, newMap); 
        PRT_ProjectTriggerHelper.autoPopulatePortfolio(newList,newMap);    
    }
    
    /***********************************************************
    * Created by 	- Prashant Gupta
    * Date 		- 24th July 2019
    * Before Insert method Being Called from Trigger
    ************************************************************/
    public static void beforeInsert(List<Project__c> newList){
        PRT_ProjectTriggerHelper.convertProject(newList,null);  
        PRT_ProjectTriggerHelper.populateBusinessCaseRequestor(newList,null);    
        PRT_ProjectTriggerHelper.autoPopulatePortfolio(newList,null);  
        PRT_ProjectTriggerHelper.populatePortfolioOwnerAndManager(newList, null);  
    }
    
    /***********************************************************
    * Created by 	- Prashant Gupta
    * Date 		- 24th July 2019
    * After Update method Being Called from Trigger
    ************************************************************/
    public static void afterUpdate(List<Project__c> newList, Map<id,Project__c> oldMap){
        PRT_ProjectTriggerHelper.updateProjectRanks(newList, oldMap);
        PRT_ProjectTriggerHelper.createProjectShareRecords(newList, oldMap);
        //PRT_ProjectTriggerHelper.populateV2MOMandMeasure(newList, oldMap);
        PRT_ProjectTriggerHelper.approveProject(newList, oldMap);      
        PRT_ProjectTriggerHelper.populateSponsorOnRisk(newList, oldMap); 
        PRT_ProjectTriggerHelper.createPSR_OnKickoff(newList, oldMap);
        if(!(System.isBatch() || System.isFuture()) && PRT_Constants.INTEGRATION_SETTING.GUS_Project_Integration__c){
            PRT_ProjectTriggerHelper.updateInsertGUSRecords(newList, oldMap);
        }
        PRT_ProjectTriggerHelper.setBudgetAllocationToProgram(newList, oldMap);         
		PRT_ProjectTriggerHelper.unlockApprovalRecords(newList, oldMap);
        PRT_ProjectTriggerHelper.createProjectShareRecordToDPM(newList, oldMap);
        
    }
    
    /***********************************************************
    * Created by 	- Prashant Gupta
    * Date 		- 24th July 2019
    * After Insert method Being Called from Trigger
    ************************************************************/
    public static void afterInsert(List<Project__c> newList){
        PRT_ProjectTriggerHelper.updateProjectRanks(newList, null);
        PRT_ProjectTriggerHelper.createProjectShareRecords(newList, null);
        PRT_ProjectTriggerHelper.setBudgetAllocationToProgram(newList, null);         
        PRT_ProjectTriggerHelper.updateInsertGUSRecords(newList, null);
        PRT_ProjectTriggerHelper.createProjectShareRecordToDPM(newList, null);
    }
}
/**
 * @author Lakshya Arora
 * @email lakshya.arora@mtxb2b.com
 * @description This class supports the GUS SLA Tracking object triggers
*/
public with sharing class KARL_GusSLATrackingTriggerHelper {
    /**
     * @author Lakshya Arora
     * @email lakshya.arora@mtxb2b.com
     * @description This method gets triggered from GUS SLA Tracking Object
    */
    public static void run(){
        if( Trigger.isBefore && Trigger.isUpdate ){
            updateOldStatus((List<KARL_GUS_SLA_Tracking__c >)Trigger.new , (Map<Id, KARL_GUS_SLA_Tracking__c>)Trigger.oldMap);
        }
    }
    
    /**
     * @author Lakshya Arora
     * @email lakshya.arora@mtxb2b.com
     * @description Update the old status when triggered
    */
    public static void updateOldStatus(List<KARL_GUS_SLA_Tracking__c > newList, Map<Id,KARL_GUS_SLA_Tracking__c> oldMap){
        for(KARL_GUS_SLA_Tracking__c slaTrack: newList){
            if(String.isNotEmpty(slaTrack.End_Status__c)) {
                if(Trigger.isInsert ){
                    slaTrack.KARL_End_Date__c = datetime.now();
                }
                if(Trigger.isUpdate && slaTrack.End_Status__c != oldMap.get(slaTrack.id).End_Status__c){
                    slaTrack.Start_Date__c = slaTrack.KARL_End_Date__c;
                    slaTrack.KARL_End_Date__c = datetime.now();
                    slaTrack.Start_Status__c = oldMap.get(slaTrack.id).End_Status__c;
                }                
            }
        }
    }

}
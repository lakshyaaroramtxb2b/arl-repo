@isTest
public class ChangeCaseLabelController_Test {
        
    @isTest
    public static void getCaseOriginTest(){
        Case caseRec = new Case();
        caseRec.subject = 'test sub';
        caseRec.Enterprise_Security_Case_Number__c = '12345';
        insert caseRec;

        String result = ChangeCaseLabelController.getCaseOrigin(caseRec.id);
        System.assertEquals(result != null, true);
    }

}
/**
 * Track the Secuirty Answers to InTake Requests
 * @author suresh.uppala
 */
global without sharing class SyncTravelApprovalTo62Org implements Schedulable,
                                                       Database.Batchable<sObject>,
                                                       Database.AllowsCallouts,
                                                       Database.Stateful {

    private static final String HOURLY = '0 0 0/1 1/1 * ? *'; //every hour
    private static final String SOURCE_FILE = 'SyncTravalApprovalTo62Org';

    global List<ESA_Security_Request__c> lstInTakeRequests;
    global Boolean errorFlag = false;

    private String fullErrorText;

    public SyncTravelApprovalTo62Org() {

    }

    public static String scheduleJob() {
        return scheduleJob(HOURLY);
    }

    public static Id scheduleJob(String cronExpression) {
        return scheduleJob(cronExpression, null);
    }

    public static void run() {
        datetime thisTime = system.now().addSeconds(15);
        integer minute = thisTime.minute();
        integer second = thisTime.second();
        integer hour = thisTime.hour();
        integer year = thisTime.year();
        integer month = thisTime.month();
        integer day = thisTime.day();

        String cronExpression = second + ' ' + minute + ' ' + hour + ' ' + day + ' ' + month + ' ? ' + year;
        scheduleJob(cronExpression, null);

    }

    public static Id scheduleJob(String cronExpression, String eventName) {
        String jobName = SOURCE_FILE + ' ' + System.now();
        String jobId = System.schedule(jobName, cronExpression, new SyncFieldTrackAndUnTrack());
        return jobId;
    }

    global void execute(SchedulableContext sc) {
        Database.executeBatch(this);
    }

    global List<ESA_Security_Request__c> start(Database.BatchableContext context){

        lstInTakeRequests = new List<ESA_Security_Request__c>();
        Set<Id> setSItems = new Set<Id>();

        try{
                //Get all travel requests which are approved and closed and > Today

                lstInTakeRequests = [SELECT Requestor__c,
                                            Email__c,
                                            Org62_Travel_Approval_Id__c,
                                            Tracked_Date_01__c,
                                            Tracked_Date_02__c,
                                            Tracked_String_03__c,
                                            Tracked_String_07__c,
                                            Tracked_String_08__c,
                                            Tracked_String_09__c,
                                            Tracked_String_10__c,
                                            Tracked_String_11__c,
                                            Tracked_Number_01__c,
                                            Tracked_Number_02__c,
                                            Tracked_Number_03__c,
                                            Tracked_Number_04__c,
                                            Approver_1__c,
                                            Approver_1__r.FederationIdentifier,
                                            Approver_2__c,
                                            Approver_2__r.FederationIdentifier,
                                            Approver_3__c,
                                            Approver_3__r.FederationIdentifier,
                                            CFO_Approval_Required__c
                                     FROM ESA_Security_Request__c
                                     WHERE  Internal_Status__c=:Esa_AppConstants.INTERNAL_STATUS_APPROVED
                                     AND  Status__c=:Esa_AppConstants.INTERNAL_STATUS_CLOSED
                                     AND Org62_Travel_Approval_Id__c=null
                                     AND Tracked_String_25__c = :Esa_AppConstants.TRAVEL_TYPE
                                     AND Tracked_Date_01__c > TODAY LIMIT 25];
                return lstInTakeRequests;
            }catch(Exception ex){
                errorFlag = true;
                return null;
            }
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope){
        List<ESA_Security_Request__c> listInTakeRequests = new List<ESA_Security_Request__c>();

        try {
                ESA_Security_Request__c esaRequest;
                Set<String> setUserFedIds = new Set<String>();
                Set<String> setRequestorEmails = new Set<String>();
                for(Sobject sObj : scope) {
                    esaRequest = (ESA_Security_Request__c)sObj;
                    listInTakeRequests.add(esaRequest);
                    if(esaRequest.Approver_1__c != null){
                        setUserFedIds.add(esaRequest.Approver_1__r.FederationIdentifier);
                    }
                    if(esaRequest.Approver_2__c != null){
                        setUserFedIds.add(esaRequest.Approver_2__r.FederationIdentifier);
                    }
                    if(esaRequest.Approver_3__c != null){
                        setUserFedIds.add(esaRequest.Approver_3__r.FederationIdentifier);
                    }
                    setRequestorEmails.add(esaRequest.Email__c);
                }

                //Get Org62 User ids of Approvers
                Map<String,String> mapApproverOrg62Ids = getOrg62ApproverUserIds(setUserFedIds);

                //Get Org 62 Requestor User Ids
                Map<String, String> mapRequestorOrg62Ids = getOrg62ReqestorUserIds(setRequestorEmails);

                //Get Org62 User Requestor Id's

                if(listInTakeRequests.size() == 0) {
                    errorFlag = true;
                    return;
                }

                List<Travel_Approval_c__x> lstTravelApprovals = new List<Travel_Approval_c__x>();

                Travel_Approval_c__x travelApproval;
                Map<Id,Id> mapTravelApprovalIds = new Map<Id,Id>();
                String travelCities;

                for(ESA_Security_Request__c inTakeRequest : listInTakeRequests) {
                    travelApproval = new Travel_Approval_c__x();
                    travelApproval.Reason_for_Travel_c__c  = inTakeRequest.Tracked_String_07__c;
                    travelApproval.Travel_Start_Date_c__c = inTakeRequest.Tracked_Date_01__c;
                    travelApproval.Travel_End_Date_c__c = inTakeRequest.Tracked_Date_02__c;
                    travelCities = '';
                    if(inTakeRequest.Tracked_String_08__c != null) {
                        travelCities = inTakeRequest.Tracked_String_08__c;
                    }
                    if(inTakeRequest.Tracked_String_09__c != null) {
                        if(travelCities != null) {
                            travelCities = travelCities + ' - ' + inTakeRequest.Tracked_String_09__c;
                        }else{
                            travelCities = inTakeRequest.Tracked_String_09__c;
                        }
                    }
                    travelApproval.To_From_c__c =  travelCities.abbreviate(45);
                    travelApproval.Description_of_Trip_c__c = inTakeRequest.Tracked_String_03__c;
                    travelApproval.Airfare_Train_c__c = inTakeRequest.Tracked_Number_03__c;
                    travelApproval.Hotel_c__c  = inTakeRequest.Tracked_Number_02__c;
                    travelApproval.Other_c__c = inTakeRequest.Tracked_Number_04__c;
                    travelApproval.Approval_Status_c__c = 'Approved';
                    travelApproval.Class_of_Service_c__c = inTakeRequest.Tracked_String_10__c;
                    travelApproval.Domestic_or_International_c__c = inTakeRequest.Tracked_String_11__c;
                    //travelApproval.Level_1_c__c = org62ApproverId;
                    travelApproval.CFO_Approval_Required_c__c = inTakeRequest.CFO_Approval_Required__c;

                    if(inTakeRequest.Approver_1__c != null){
                        travelApproval.Level_1_c__c = mapApproverOrg62Ids.get(inTakeRequest.Approver_1__r.FederationIdentifier);
                    }
                    if(inTakeRequest.Approver_2__c != null){
                        travelApproval.Level_2_c__c = mapApproverOrg62Ids.get(inTakeRequest.Approver_2__r.FederationIdentifier);
                    }
                    if(inTakeRequest.Approver_3__c != null){
                        travelApproval.Level_3_c__c = mapApproverOrg62Ids.get(inTakeRequest.Approver_3__r.FederationIdentifier);
                    }

                    Database.SaveResult sr = Database.insertImmediate(travelApproval);
                    if(sr.isSuccess()){
                        travelApproval = [SELECT ExternalId,
                                                 Approval_Status_c__c,
                                                 CFO_Approval_Required_c__c
                                          FROM Travel_Approval_c__x
                                          WHERE Id=:sr.getID()];
                        //Need to update status again as there is a WF rule which changing the status = new after creation
                        if(travelApproval.CFO_Approval_Required_c__c == false){
                            travelApproval.Approval_Status_c__c = 'Approved';
                            if(mapRequestorOrg62Ids.get(inTakeRequest.Email__c) != null) {
                                travelApproval.OwnerId__c = mapRequestorOrg62Ids.get(inTakeRequest.Email__c);
                            }
                            lstTravelApprovals.add(travelApproval);
                        }
                        inTakeRequest.Org62_Travel_Approval_Id__c = travelApproval.ExternalId;
                        /*if(travelApproval.CFO_Approval_Required_c__c == true){
                            Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
                            req1.setObjectId(travelApproval.id);
                            approval.ProcessResult result = Approval.process(req1);
                        }*/
                    }else{
                        List<Database.Error> err = sr.getErrors();
                        system.debug('###########'+ err[0].getMessage());
                    }
                }

                if(lstTravelApprovals.size() > 0) {
                    Database.updateImmediate(lstTravelApprovals);
                }
                if(listInTakeRequests.size() > 0) {
                    update listInTakeRequests;
                }


            }catch(Exception exc) {
                errorFlag = true;
                processException(exc);
            }
    }

    global void finish(Database.BatchableContext BC){

    }

    /**
     * Get Approver Org62 User id's
     * @param  setUserEmails [User Email id's]
     * @return               [Map of Org 62 User email and user id's]
     */

    private Map<String,String> getOrg62ApproverUserIds(Set<String> setUserFedIds){
        List<Contact> lstContacts = [SELECT Email,
                                            Org62_User_ID__c,
                                            Federation_Identifier__c
                                     FROM Contact
                                     WHERE (Email IN : setUserFedIds
                                     OR Federation_Identifier__c IN : setUserFedIds)
                                     AND Is_Active__c = true];

        Map<String,String> mapApproverOrg62Ids = new Map<String,String>();
        for(Contact cont : lstContacts){
            mapApproverOrg62Ids.put(cont.Federation_Identifier__c, cont.Org62_User_ID__c);
        }

        return mapApproverOrg62Ids;
    }

    /**
     * Get Requestor Org 62 User ids
     * @return [Map of Org62 User Id's]
     */
    private Map<String,String> getOrg62ReqestorUserIds(Set<String> setRequestorEmails){
        List<Contact> lstContacts = [SELECT Email,
                                            Org62_User_ID__c,
                                            Federation_Identifier__c
                                     FROM Contact
                                     WHERE (Email IN : setRequestorEmails
                                     OR Federation_Identifier__c IN : setRequestorEmails)
                                     AND Is_Active__c = true];

        Map<String,String> mapRequestorOrg62Ids = new Map<String,String>();
        for(Contact cont : lstContacts){
            mapRequestorOrg62Ids.put(cont.Federation_Identifier__c, cont.Org62_User_ID__c);
        }

        return mapRequestorOrg62Ids;
    }


    private void processException(Exception exc) {
        Esa_DebugService.writeMessage('SyncTravalApprovalTo62Org.finish: Failed with errors');
        if (exc instanceof DMLException) {
            String errorString = '';
            Integer numErrors = exc.getNumDml();
            fullErrorText = 'There were ' + numErrors + ' DML errors: \n';

            Esa_DebugService.writeException(exc, 1, SOURCE_FILE, fullErrorText);
        } else {
            //we got an unexpected non-dmlexception, write it
            fullErrorText = exc.getMessage();
            Esa_DebugService.writeException(exc, 1, SOURCE_FILE, 'Unhandled exception');
        }
    }
}
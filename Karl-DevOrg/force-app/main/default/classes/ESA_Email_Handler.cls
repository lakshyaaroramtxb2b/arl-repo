/* 

    Developer Name: Jorge L Caceres - jcaceres@salesforce.com | July 2019 
    
    Description:
        Create Intake Request from emails
      
*/ 
global class ESA_Email_Handler implements Messaging.InboundEmailHandler {

    /* constants */

    @TestVisible private static final String SOURCE = 'Email';
    @TestVisible private static final String SUBMITTED = 'Submitted';
    @TestVisible private static final String ENTITY_ID = 'a5X3A000000LRGZUA4';
    @TestVisible private static final String SERVICE_ID = 'a1i3A000001mcaFQAQ';
    @TestVisible private static final String ORGW_ID = '0D23A000000GqNM';
    @TestVisible private static final String NOREPLY = 'noreply-customerassessment@salesforce.com';

    private static final String REJECTION_SUBJECT = 'Your Request Has Been Rejected';
    private static final String REJECTION_EMAIL = ''
        + 'Hello,\n\n'
        + 'We apologize for the inconvenience but you must register your email address with us '
        + 'before submitting a request. Please login to the following '
        + 'URL to register your email address and try again.\n'
        + 'https://security.secure.force.com/securityassessments\n\n'
        + 'Regards,\n'
        + 'Salesforce Security Team\n\n\n\n\n';
                                                                                    
    /* class variables */

    @TestVisible private ESA_Security_Request__c  intakeRequest;

    /* main processing method for email service */

    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();

        // try to get the requester user using from email address
        User requesterUser = getRequesterUser(email.fromAddress);   
        
        // cannot process if user not found
        if (requesterUser == null) {
            sendRejectionEmail(email.fromAddress);
            return null;
        }
        
        // create Intake Request
        intakeRequest = new ESA_Security_Request__c();
        intakeRequest.Status__c  = SUBMITTED;
        intakeRequest.Requestor__c  = requesterUser.Id;
        intakeRequest.Name__c = requesterUser.Name;
        intakeRequest.Email__c = email.fromAddress;
        intakeRequest.ESA_Entity__c = ENTITY_ID;
        intakeRequest.ESA_Service_Item__c = SERVICE_ID;
        intakeRequest.Hash_Key__c = makeHashKey(intakeRequest, email);
        intakeRequest.Intake_Request_Source__c = SOURCE;
        insert intakeRequest;
        
        // save email as an activity
        saveInboundEmail(email, intakeRequest);
                       
        return null;
    }
    
    /* private methods */ 

    private void saveInboundEmail(Messaging.InboundEmail email, ESA_Security_Request__c securityRequest) {
        // create email message
        EmailMessage emailMsg = new EmailMessage();
        emailMsg.FromName = email.FromName;
        emailMsg.FromAddress = email.fromAddress;
        emailMsg.RelatedToId = securityRequest.Id;
        emailMsg.HtmlBody = email.htmlBody.abbreviate(1280);
        emailMsg.TextBody = email.plainTextbody.abbreviate(1280);
        emailMsg.Subject = email.subject; 

        insert emailMsg;
        
        //save attachments
        if (email.binaryAttachments != null) {
            for(Messaging.Inboundemail.binaryAttachment bAttachment:email.binaryAttachments) {
                Attachment attachment = new Attachment();
                attachment.Name = bAttachment.fileName;
                attachment.Body = bAttachment.body;
                attachment.ParentId = securityRequest.Id;
                insert attachment;
            }
        }
        if (email.textAttachments != null) {
            for(Messaging.Inboundemail.textAttachment tAttachment:email.textAttachments) {
                Attachment attachment = new Attachment();
                attachment.Name = tAttachment.fileName;
                attachment.Body = Blob.valueof(tAttachment.body);
                attachment.ParentId = securityRequest.Id;
                insert attachment;
            }
        }
        
    }   
    
    @TestVisible
    private void sendRejectionEmail(String fromAddress) {
        String emailBody = REJECTION_EMAIL;
        String emailSubject = REJECTION_SUBJECT;
            
        Messaging.SingleEmailMessage mail= new Messaging.SingleEmailMessage();
        mail.setToAddresses(new String[] {fromAddress});
        mail.setOrgWideEmailAddressId(ORGW_ID);
        mail.setReplyTo(NOREPLY);
        mail.setSubject(emailSubject);
        mail.setPlainTextBody(emailBody);
        Messaging.sendEmail(new Messaging.Email[] {mail});                
    }

    private String makeHashKey(ESA_Security_Request__c securityRequest, Messaging.InboundEmail email) {
        String key = String.valueOf(Math.Round(Math.random()*100000)) + String.valueOf(securityRequest.Requestor__c).substring(5) ;
        key += string.valueOf(securityRequest.Email__c.hashCode());
        key += string.valueOf(email.messageId.left(10).trim().hashCode());
        key += string.valueOf(string.valueOfGMT(datetime.now()).hashCode());
        key = key.replace('-', '');
        return key.trim();
    }
    
    private User getRequesterUser(String fromAddress) {
        // identify requester
        User requester; 
        String userName = fromAddress + '.securityassessments';
        for (User u : [SELECT Id, Name from USER WHERE Email = :fromAddress 
                       AND Profile.Name = 'Assessments HVCP']) {
            requester = u;            
        }
        If (requester != null) {
            return requester;    
        } else {
            //return null;
            return createRequesterUser(fromAddress);    
        }
    }
    
    private User createRequesterUser(String pm_email) {
		user u;

        try {
            // identify requester
            String userName = pm_email + '.securityassessments';
            String pm_first_name = pm_email.substringBefore('@');
            String pm_last_name = pm_email.substringAfter('@');
            String userpassword = '001GrJ!' + pm_email.substringBefore('@').left(30).reverse();
            String accountId = '0013A00001YZGrJ';
    
            // create required contact record
            Contact c = new Contact();
            c.AccountId = accountId;
            c.Email = pm_email;
            c.FirstName = pm_first_name;       
            c.LastName = pm_last_name;  
            insert c;
    
            // create required user record
            u = new User();
            u.Username = userName;
            u.FirstName = pm_first_name;       
            u.LastName = pm_last_name; 
            u.Alias = String.valueOf(c.Id).right(8);
            u.Email = pm_email;
            u.profileId  = '00e3A0000028mPm';
            u.ContactId = c.id;
            u.LocaleSidKey = 'en_US';
            u.TimeZoneSidKey = 'GMT';
            u.LanguageLocaleKey = 'en_US';
            u.EmailEncodingKey = 'UTF-8';
            insert u;
    
            // set standard password for user
            System.setPassword(u.id,userpassword);
        } catch (Exception e) {}
        return u;        
    }        
}
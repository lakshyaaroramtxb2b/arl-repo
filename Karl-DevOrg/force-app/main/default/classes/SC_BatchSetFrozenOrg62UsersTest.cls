@isTest
private class SC_BatchSetFrozenOrg62UsersTest {
	
	@isTest static void RunFrozenUserBatch() {
		RecordType rt = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = 'Salesforce_Employee' LIMIT 1];

		Contact con = new Contact();
		con.FirstName = 'Test';
		con.LastName = 'Con';
		con.Email = 'test@con.com';
		con.Is_Frozen__c = true;
		con.Org62_User_ID__c = '123456789asdfghjkl';
		con.RecordTypeId = rt.Id;
		insert con;

		Test.startTest();
			SC_SetFrozenOrg62Scheduler scheduleBatch = new SC_SetFrozenOrg62Scheduler();   
	        String chron = '0 0 23 * * ?';        
	        system.schedule('Test Sched', chron, scheduleBatch);
		Test.stopTest();
	}	
}
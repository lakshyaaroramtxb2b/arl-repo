public class SFDCHostDetail {
    
	public String jsonString {get;set;}

    //Constructor
    public SFDCHostDetail()
    {
        jsonString = prepareData();
    }

    //Temp Method to prepare the Data
    private String prepareData()
    {
        String strIP = ApexPages.currentPage().getParameters().get('ip');

        if(strIP != null)
        { 
         HttpRequest req = new HttpRequest();
         req.setEndpoint('https://hunter-sfm.internal.salesforce.com/cgi-bin/host_lookup.py?target=' + strIP);
         req.setMethod('GET');
        
         Http http = new Http();
         HTTPResponse res = http.send(req);
         
         return res.getBody();
        }
        
        return '[]';
    }
}
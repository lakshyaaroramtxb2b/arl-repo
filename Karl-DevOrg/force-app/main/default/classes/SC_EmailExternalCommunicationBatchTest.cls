/**
* @author Vishnu Kumar
* @email vishnu.kumar@mtxb2b.com
* @desc This is helper class for SC_Email_External_Communication_Batch.
*/
@isTest
public class SC_EmailExternalCommunicationBatchTest {
    public static String test_email_id = 'abcd.xy12@yopmail789.com'; 
    
	@TestSetup
    public static void setupTestData(){
        User usr = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        
        Contact cnt = new Contact(FirstName = 'Test', LastName = 'Name', Email = test_email_id);
        insert cnt;
        
        System.runAs(usr) {
            SC_Email_External_CommunicationTUtils.createEmailTemplates(true);
            SC_CISO_CampaignMember__c cm = SC_Email_External_CommunicationTUtils.createCampaignMemberData();
            cm.SuccessOrError__c = 'Pending';
            Update cm;
        }
    }
    
    @isTest
    public static void testSendEmails() {
        Test.startTest();
        
        User usr = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        List<EmailTemplate> templates =  SC_Email_External_Communication.fetchEmailTemplates();
        List<OrgWideEmailAddress> owds = [SELECT Id, IsAllowAllProfiles, DisplayName, Address FROM OrgWideEmailAddress WHERE IsAllowAllProfiles= TRUE];
        String ccAdd = 'abcd.xyz189@gmail.com,xyz.abcd12@gmail.com';
        
        List<SC_Email_External_Communication.StatusRecords> staticRecList = new List<SC_Email_External_Communication.StatusRecords>();
        List<SC_Email_External_Communication.AccountEmails> accountEmailList = new List<SC_Email_External_Communication.AccountEmails>();
        
        SC_Email_External_Communication.StatusRecords staticRec = new SC_Email_External_Communication.StatusRecords();
        staticRec.status = 'abc';
        staticRec.numberOfRec = 2;
        staticRec.selectedEmailTemplate = SC_Email_External_Communication.fetchEmailTemplates()[0].Name;
        staticRec.selectedFromAddress = owds[0].Address;
        staticRec.selectedCCAddresses = ccAdd;
        staticRecList.add(staticRec);
        
        SC_Email_External_Communication.AccountEmails accEmails = new SC_Email_External_Communication.AccountEmails();
        accEmails.id = [SELECT Id, Name, Email__c, FirstName__c, LastName__c FROM SC_CISO_CampaignMember__c LIMIT 1].Id;
        accEmails.aeEmail = test_email_id;
        accountEmailList.add(accEmails);
        
        System.runAs(usr){             
            SC_Email_External_Communication_Batch emailBatch = new SC_Email_External_Communication_Batch(
                JSON.serialize(staticRecList), 
                SC_Email_External_Communication.fetchEmailTemplates(), 
                SC_Email_External_Communication.getOrgWideEmailAddresses(), 
                accountEmailList);
            
            Database.executeBatch(emailBatch, 200);  
        }
        
        Test.stopTest(); 
        
        SC_CISO_CampaignMember__c cisoM = [SELECT Id, SuccessOrError__c FROM SC_CISO_CampaignMember__c limit 1];
        system.assertEquals('Success', cisoM.SuccessOrError__c);
    }  
}
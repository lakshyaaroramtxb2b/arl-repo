/**
 * @author: Suresh Uppala
 */
public class UpdateGusPositionRecordsBatch_Schedule implements Schedulable {
    public void execute(SchedulableContext sc) {
        Database.executeBatch((Database.Batchable<SObject>) 
            Type.forName('UpdateGusPositionRecordsBatch').newInstance(), 100);
    }
}
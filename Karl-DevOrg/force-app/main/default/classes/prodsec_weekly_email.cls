global class prodsec_weekly_email Implements Schedulable{
    
    
    global void execute(SchedulableContext sc)
        {
            sendMail();
        }
    
   
     
    public static void sendMail(){
        string toMail; 
    	string ccMail;
    	string repMail;
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        //string[] to = new string[] {'prodsec@salesforce.com'};
        string[] to = new string[] {'prodsec@salesforce.com'};
        //string[] cc = new string[] {ccMail};
            
       	OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'securecloud@salesforce.com'];
        
		repMail = 'prodsec@salesforce.com';
		if ( owea.size() > 0 ) {
    		email.setOrgWideEmailAddressId(owea.get(0).Id);
		}
        
        email.setToAddresses(to);
      
        if(repmail!=null && repmail!= '')
            email.setInReplyTo(repMail);
         
        email.setSubject('Weekly Bug Review Status Reminder');
         
        email.setHtmlBody('Hi all, <br/><br/><br/> Here is the list of open bugs requiring a status update for this week - <a href="https://security--c.na45.visual.force.com/apex/bug_review?currentuser=0"> Bug List </a> <br/>Please ensure you provide an update for your bugs by 3pm today to avoid attending the Bug Review meeting on Monday. <br/>Also, do look into open providence investigations <a href="https://gus--providence.gus.visual.force.com/apex/providencetriage">here</a>.<br/>Please contact Akshay Shetty if you have any questions regarding your bugs.<br/><br/> Thank you!');
        try{
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { email });
        }catch(exception e){
            apexpages.addmessage(new apexpages.message(apexpages.severity.error,e.getMessage()));
        }
         
        toMail = '';
        ccMail = '';
        repMail = '';
    }


}
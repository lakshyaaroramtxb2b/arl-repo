/**
** SyncSecurityAssessmentsToSforce sAssessments = new SyncSecurityAssessmentsToSforce();
* sAssessments.execute(null);
*/
public class SyncSecurityAssessmentsToSforce extends ESARecurringSchedulable {
    // Status mappings defined here will impact the logic in this file. So, don't move to AppConstants
    // We selectively change the case status if it has corresponding mapping below
    private static final Map<String, String> SF_CASE_CLOSED_STATUS = new Map<String, String>{'Approved' => 'Security - Approved',
                                                                                     'Duplicate' => 'Closed',
                                                                                     'Closed' => 'Closed',
                                                                                     'Waived' => 'Closed',
                                                                                     'Self Attested' => 'Incomplete',
                                                                                     'Rejected' => 'Security- Not Approved',
                                                                                     'Never' => 'Withdrawn',
                                                                                     'Code Review Approved' => 'Security - Approved'};

    // We selectively change the case status if it has corresponding mapping below
    private static final Map<String, String> SF_CASE_IN_PROGRESS_STATUS = new Map<String, String>{
                                                                                     'In Design Review' => 'In Progress',
                                                                                     'In Code Review' => 'In Progress'};

    private static final Set<String> GUS_SA_BEFORE_SUBMIT_STATUS = new Set<String>{'New'};
    // For now we only have one in the list 'Completed', may be in future 'Never', 'NA' etc
    private static final Set<String> GUS_TASK_DONE_STATUS = new Set<String>{'Completed'};

    public Integer getRecurringInterval() {
        return 10; // Every 10 mins
    }
    
    public void executeRecurring(SchedulableContext ctx) {
        syncSecAssessmentsToSForce();
        notifySAFeedChangesToSForce();
        remindSubmitOnSATaskCompletion();
    }

    private  static Set<String> getEntSecTeamMemberGusUserIds() {
        Set<String> entSecTeamMemberFedIds = new Set<String>{'esa.coordinator@salesforce.com'}; // Add ESA Coordinator by default
        Group pubGroup;
        for (Group grp : [SELECT Id FROM Group WHERE DeveloperName = 'Enterprise_Security' AND Type='Regular' LIMIT 1]) {
            pubGroup = grp;
            break;
        }
        if (pubGroup != null) {
            Set<String> userIds = new Set<String>();
            // For now we are not going to check for nexted groups
            for (GroupMember member : [SELECT UserOrGroupId FROM GroupMember WHERE GroupId =: pubGroup.Id]) {
                userIds.add(member.UserOrGroupId);
            }
            for (User user : [SELECT FederationIdentifier FROM User WHERE Id IN :userIds]) {
                entSecTeamMemberFedIds.add(user.FederationIdentifier);
            }
        }

        Set<String> entSecTeamMemberGusUserIds = new Set<String>();
        for(GusUser__x entSecTeamGusUser : [SELECT ExternalId, Email__c, FederationIdentifier__c 
                                                    FROM GusUser__x WHERE Email__c IN :entSecTeamMemberFedIds 
                                                        OR FederationIdentifier__c IN :entSecTeamMemberFedIds]) {
            entSecTeamMemberGusUserIds.add(entSecTeamGusUser.ExternalId);
        }
        return entSecTeamMemberGusUserIds;
    }
    
    private void syncSecAssessmentsToSForce() {
        Datetime lastModifiedDateTime = getLatestAssessmentsSyncDateTime();

        List<ADM_Security_Assessment_c__x> latestSecurityAssessments = [SELECT Id, Name__c, Project_Name_c__c, Overview_c__c, Status_c__c, ExternalId,
                                                                             Team_c__c,Team_c__r.Name__c,CreatedById__c, CreatedById__r.Email__c, LastModifiedDate__c
                                                                        FROM ADM_Security_Assessment_c__x
                                                                        WHERE ( Team_c__c =:Label.Esa_Gus_EnteSec_App_Team
                                                                        OR Primary_Contact_c__c =:Label.Esa_Gus_Primary_Contact_1
                                                                        OR Primary_Contact_c__c =:Label.Esa_Gus_Primary_Contact_2 )
                                                                        AND LastModifiedDate__c > :lastModifiedDateTime
                                                                        ORDER BY LastModifiedDate__c ASC
                                                                        LIMIT 50];

        Set<String> contactEmails = new Set<String>();
        Set<String> latestSecurityAssessmentIDs = new Set<String>();
        String contactEmail;
        for(ADM_Security_Assessment_c__x securityAssessment : latestSecurityAssessments) {
            contactEmail = parseEmail(securityAssessment.CreatedById__r.Email__c);
            contactEmails.add(contactEmail);
            latestSecurityAssessmentIDs.add(securityAssessment.ExternalId);
        }

        // Retrieve contacts records from supportforce
        List<Contact__x> contacts = [SELECT ExternalId,
                                               Email__c,
                                               Name__c
                                        FROM Contact__x 
                                        WHERE Email__c IN :contactEmails];

        Map<String,Contact__x> mapContacts = new Map<String,Contact__x>();
        for(Contact__x c : contacts) {
            mapContacts.put(c.Email__c, c);
        }

        // Get Existing Gus Reference records from security org
        List<Gus_Assessment_Reference__c> existingGUSAssessmentRefs = [SELECT Id,Support_Force_Case_Id__c, Assessment_Status__c, Security_Assessment_Id__c
                                                                            FROM Gus_Assessment_Reference__c
                                                                            WHERE Security_Assessment_Id__c IN : latestSecurityAssessmentIDs];

        Map<String,Gus_Assessment_Reference__c> mapExistingGUSAssessmentRefs = new Map<String,Gus_Assessment_Reference__c>();
        for(Gus_Assessment_Reference__c gusAssessReference : existingGUSAssessmentRefs) {
            mapExistingGUSAssessmentRefs.put(gusAssessReference.Security_Assessment_Id__c, gusAssessReference);
        }                          

        List<Gus_Assessment_Reference__c> updatedGusAssessRefs  = new List<Gus_Assessment_Reference__c>();
        List<Case__x> updatedCases = new List<Case__x>();

        boolean isSuccess = true;
        for(ADM_Security_Assessment_c__x securityAssessment : latestSecurityAssessments) {
            try {
                if(mapExistingGUSAssessmentRefs.containsKey(securityAssessment.ExternalId)) {
                        Gus_Assessment_Reference__c gusReference = mapExistingGUSAssessmentRefs.get(securityAssessment.ExternalId);

                        //Update case status in Gus Reference object and Supportforce case 
                        if(gusReference.Assessment_Status__c != securityAssessment.Status_c__c) {
                            gusReference.Assessment_Status__c = securityAssessment.Status_c__c;
                            updatedGusAssessRefs.add(gusReference);
                            
                            String updatedCaseStatus = null;
                            if (SF_CASE_CLOSED_STATUS.containsKey(securityAssessment.Status_c__c)) {
                                updatedCaseStatus = SF_CASE_CLOSED_STATUS.get(securityAssessment.Status_c__c);
                            } else if (SF_CASE_IN_PROGRESS_STATUS.containsKey(securityAssessment.Status_c__c)) {
                                updatedCaseStatus = SF_CASE_IN_PROGRESS_STATUS.get(securityAssessment.Status_c__c);
                            }
                            // Update the case status
                            if (!String.isBlank(updatedCaseStatus)) {
                                Case__X sfCase = new Case__x();
                                sfCase.Status__c = updatedCaseStatus;
                                sfCase.ExternalId = gusReference.Support_Force_Case_Id__c;
                                updatedCases.add(sfCase);
                            }
                        }
                } else {
                    Contact__x contact = mapContacts.get(parseEmail(securityAssessment.CreatedById__r.Email__c));
                    Case__X sfCase = new Case__x();
                    sfCase.OwnerId__c = ESA_AppConstants.SFORCE_SECURITY_QUEUE_ID;
                    sfCase.RecordTypeId__c = ESA_AppConstants.SF_SEC_RECORDTYPEID;
                    sfCase.Status__c = ESA_AppConstants.SFORCE_CASE_STATUS_NEW;
                    sfCase.Subject__c = 'SSDL - ' + securityAssessment.Name__c;
                    if(securityAssessment.Project_Name_c__c != null) {
                        sfCase.Subject__c += ' - ' + securityAssessment.Project_Name_c__c;
                    }
                    sfCase.Description__c = 'Created By :' + contact.Name__c + '\n ' + securityAssessment.Overview_c__c;
                    sfCase.ContactId__c = contact.ExternalId;
                    sfCase.Internal_Support_Category_c__c = ESA_AppConstants.INTERNAL_SUPPORT_CATEGORY_ASA;
                        
                    Database.SaveResult sr = Database.insertImmediate(sfCase);

                    if (checkAndWriteErrorsForDebug(sr)) {
                        // On Errors skip next steps and continue to next SA in loop
                        continue;
                    }

                    sfCase = getCase(sr.getID());

                    // Update Security assessment record with supportforce case URL.
                    // As it's rest calls and flow is very low, so updating this right away here.
                    try {
                        FeedItem__x workFeedItem = createGUSFeedItem(securityAssessment.ExternalId, String.format(Label.Esa_Supportforce_Case_Created, new String[]{sfCase.ExternalId}));
                        Database.SaveResult saveRes = Database.insertImmediate(workFeedItem);
                        checkAndWriteErrorsForDebug(saveRes);
                    } catch (Exception e) {
                        // Its OK to continue when feeditem creation fails as this is for info purposes only
                        Esa_DebugService.WriteException(e, 'SyncSecurityAssessmentsToSforce', 'Exception during GUS FeedItem creation');
                    }
                    
                    Gus_Assessment_Reference__c gusReference = new Gus_Assessment_Reference__c();
                    gusReference.Security_Assessment_Id__c = securityAssessment.ExternalId;
                    gusReference.Support_Force_Case_Id__c = sfCase.ExternalId;
                    gusReference.Assessment_Status__c = securityAssessment.Status_c__c;
                    // Set the feed modified date to now, so that we can listen to feed changes after this point
                    gusReference.Last_Feed_Modified_Date__c = Datetime.now();
                    updatedGusAssessRefs.add(gusReference); 
                }
            } catch (Exception e) {
                Esa_DebugService.WriteException(e, 'SyncSecurityAssessmentsToSforce', 'Exception during sync SA to SForce');
                // On any exception during any step catch the exception and continue processing the next record
                // Set isSucess to falso so that lastModifiedDateTime is updated accordingly
                isSuccess = false;
                continue;
            }
            
            // Update lastModifiedDateTime only until the record thats successfully updated
            // so that when the job runs next time it retries to update starting from previous failure
            if (isSuccess) { 
                lastModifiedDateTime = securityAssessment.LastModifiedDate__c;
            }
        }

        boolean canUpdateLastModifiedDateTime = true;
        if(updatedCases.size() > 0) {
            List<Database.SaveResult> saveResults = Database.updateImmediate(updatedCases);
            if (checkAndWriteErrorsForDebug(saveResults)) {
                // On Errors do not update last modified datetime
                canUpdateLastModifiedDateTime = false;
            }
        }

        if(updatedGusAssessRefs.size() > 0)  {
            try {
                //Create case comments to notify the status of security assessment record
                List<CaseComment__x> newCaseComments = new List<CaseComment__x>();
                
                for(Gus_Assessment_Reference__c gusReference : updatedGusAssessRefs) {
                    newCaseComments.add(createCaseComment(gusReference.Support_Force_Case_Id__c, String.format(Label.Esa_SA_Status_Info, new String[]{gusReference.Security_Assessment_Id__c, gusReference.Assessment_Status__c})));
                }
                List<Database.SaveResult> saveResults = Database.insertImmediate(newCaseComments);
                checkAndWriteErrorsForDebug(saveResults);
            } catch (Exception e) {
                // Its OK to continue when case comment creation fails as this is for info purposes only
                Esa_DebugService.WriteException(e, 'SyncSecurityAssessmentsToSforce', 'Exception during SForce case comment creation');
            }
            upsert updatedGusAssessRefs;
        }

        // When not errors then update lastsyncdatetime. So that next iteration picks up from baton
        if (canUpdateLastModifiedDateTime) {
            IntegrationLastSyncDate__c syncDate = IntegrationLastSyncDate__c.getValues('SecurityAssessmentsSync');
            syncDate.Security_Assessments_Last_Sync_DateTime__c = lastModifiedDateTime;
            update syncDate;
        }
    }
    
    @future
    private static void notifySAFeedChangesToSForce() {
        Set<String> entSecTeamGusUserIds = getEntSecTeamMemberGusUserIds();
        Set<String> closedSAStatus = SF_CASE_CLOSED_STATUS.keySet();
        // Excludes closed and existing records based on status and feed modified date checks
        List<Gus_Assessment_Reference__c> assessmentRecords = [SELECT Id,
                                               Support_Force_Case_Id__c,
                                               Assessment_Status__c,
                                               Security_Assessment_Id__c,
                                               Last_Feed_Modified_Date__c
                                        FROM Gus_Assessment_Reference__c
                                        WHERE Assessment_Status__c NOT IN : closedSAStatus];
        
        List<Case__x> cases = new List<Case__x>();
        List<CaseComment__x> caseComments = new List<CaseComment__x>();
        List<Gus_Assessment_Reference__c> updatedAssessmentRecords = new List<Gus_Assessment_Reference__c>();
        
        for(Gus_Assessment_Reference__c assessmentRec : assessmentRecords) {
            Datetime lastFeedCheckedDatetime = assessmentRec.Last_Feed_Modified_Date__c;
            if (lastFeedCheckedDatetime == null) {
                // For existing gusRef start listening to feed changes from start of today, hence not passing time
                Date today = Datetime.now().date();
                lastFeedCheckedDatetime = Datetime.newInstance(today.year(), today.month(), today.day());
            }
            // Update to now
            assessmentRec.Last_Feed_Modified_Date__c = Datetime.now();
            
            List<ADM_Security_Assessment_Feed__x> recentFeedItems = [SELECT Body__c, IsRichText__c, Type__c, LastModifiedDate__c, CommentCount__c, CreatedById__c
                                                                     FROM ADM_Security_Assessment_Feed__x 
                                                                          WHERE ParentId__c =: assessmentRec.Security_Assessment_Id__c
                                                                                AND LastModifiedDate__c >: lastFeedCheckedDatetime
                                                                          ORDER BY LastModifiedDate__c DESC LIMIT 10];
            if (recentFeedItems.size() > 0) {
                // Whether SA is changed since last feed changes
                Boolean isSAChangedByNonTeamMember = false;
                for (ADM_Security_Assessment_c__x sa : [SELECT LastModifiedById__c FROM ADM_Security_Assessment_c__x
                                                                WHERE Id =: assessmentRec.Security_Assessment_Id__c
                                                                    AND LastModifiedDate__c >=: lastFeedCheckedDatetime]) {
                    if (!entSecTeamGusUserIds.contains(sa.LastModifiedById__c)) {
                        isSAChangedByNonTeamMember = true;
                    }
                    break;
                }

                // Latest post displays on top in case comments
                Boolean isPostByNonTeamMemberFound = false;
                for(Integer i=recentFeedItems.size()-1; i>=0; i--) {
                    ADM_Security_Assessment_Feed__x feedItem = recentFeedItems.get(i);
                    Boolean isPostByTeamMember = entSecTeamGusUserIds.contains(feedItem.CreatedById__c);
                    Boolean hasComments = feedItem.CommentCount__c > 0;
                    if (isPostByTeamMember && !hasComments) {
                        // When post is by team member then ignore and proceed
                        // Ignore only when there are no comments
                        continue;
                    }
                    // We don't have hold on who is adding comments, So we are 100% sure its by a Non team member only when no comments are present
                    isPostByNonTeamMemberFound = (!isPostByTeamMember && !hasComments);
                    String feedBody = String.isBlank(feedItem.Body__c) ? Label.Esa_Default_FeedBodyText : feedItem.Body__c.stripHtmlTags();
                    
                    String feedComments = feedItem.CommentCount__c == 0 ? '' : '\n\n' + String.format(Label.Esa_FeedComment_Posted, new String[]{String.valueOf(feedItem.CommentCount__c)});
                    
                    String caseComment = String.format(Label.Esa_FeedItem_Posted, new String[]{assessmentRec.Security_Assessment_Id__c, feedBody.abbreviate(3000), feedComments});
                    caseComments.add(createCaseComment(assessmentRec.Support_Force_Case_Id__c, caseComment));
                }
                
                // Change case status on SA changes
                if ((isSAChangedByNonTeamMember || isPostByNonTeamMemberFound) && isValidCaseStatus(Label.Esa_CaseStatusOnSecAssessmentChange)) {
                    Case__x sfCase = getCase(assessmentRec.Support_Force_Case_Id__c);
                    if (canTriggerStatusChange(sfCase.Status__c)) {
                        sfCase.Status__c = Label.Esa_CaseStatusOnSecAssessmentChange;
                        cases.add(sfCase);
                    }
                }
            }
        }
        
        if(cases.size() > 0) { 
            List<Database.SaveResult> saveResults = Database.insertImmediate(cases);
            checkAndWriteErrorsForDebug(saveResults);
        }

        if(caseComments.size() > 0) {
            List<Database.SaveResult> saveResults = Database.insertImmediate(caseComments); 
            checkAndWriteErrorsForDebug(saveResults);
        } 

        upsert assessmentRecords; // To update last feed check time
    }

    @future
    private static void remindSubmitOnSATaskCompletion() {
        List<FeedItem__x> feedItems = new List<FeedItem__x>();
        List<Gus_Assessment_Reference__c> updatedAssessmentRecords = new List<Gus_Assessment_Reference__c>();

        List<Gus_Assessment_Reference__c> assessmentRecords = [SELECT Id,
                                               Support_Force_Case_Id__c,
                                               Assessment_Status__c,
                                               Security_Assessment_Id__c,
                                               Last_Feed_Modified_Date__c
                                        FROM Gus_Assessment_Reference__c
                                        WHERE Assessment_Status__c IN :GUS_SA_BEFORE_SUBMIT_STATUS
                                        AND Tasks_Status__c NOT IN : GUS_TASK_DONE_STATUS];

        for(Gus_Assessment_Reference__c assessmentRec : assessmentRecords) {
            List<Gus_Task__x> tasks = [SELECT Status__c FROM Gus_Task__x 
                                        WHERE Security_Assessment__c =: assessmentRec.Security_Assessment_Id__c];
            // If tasks are present                                             
            if (tasks.size() > 0) {
                Boolean isAllTasksCompleted = true;
                for(Gus_Task__x task : tasks) {
                    if (!GUS_TASK_DONE_STATUS.contains(task.Status__c)) {
                        isAllTasksCompleted = false;
                        break;
                    }
                }

                if (isAllTasksCompleted) {
                    // Set to Completed status (Loop executes only once)
                    for (String completedStatus : GUS_TASK_DONE_STATUS) {
                        assessmentRec.Tasks_Status__c = completedStatus;
                        // Just exit as we just want to set to one of completed status 
                        break;
                    }
                    updatedAssessmentRecords.add(assessmentRec);
                    // Remind to submit with chatter post
                    feedItems.add(createGUSFeedItem(assessmentRec.Security_Assessment_Id__c, Label.Esa_remindSASubmit));
                }
            }
        }

        if (feedItems.size() > 0) {
            List<Database.SaveResult> saveResults = Database.insertImmediate(feedItems);
            checkAndWriteErrorsForDebug(saveResults);
        }
        if (updatedAssessmentRecords.size() > 0) upsert updatedAssessmentRecords;
    }

    private static Boolean checkAndWriteErrorsForDebug(List<Database.SaveResult> saveResults) {
        Boolean foundErrors = false;
        for (Database.SaveResult saveRes : saveResults) {
            if (checkAndWriteErrorsForDebug(saveRes)) {
                foundErrors = true;
            }
        }
        return foundErrors;
    }

    private static Boolean checkAndWriteErrorsForDebug(Database.SaveResult saveRes) {
        Boolean foundErrors = !saveRes.isSuccess();
        if (foundErrors) {
            for (Database.Error error : saveRes.getErrors()) {
                Esa_DebugService.writeErrorMessage(error.getMessage(), Esa_DebugService.LEVEL_ERROR);
            } 
        }
        return foundErrors;
    }
    
    private static CaseComment__x createCaseComment(String caseId, String bodyText) {
        CaseComment__x sfCaseComment = new CaseComment__x();
        sfCaseComment.IsPublished__c = false;
        sfCaseComment.ParentId__c = caseId;
        sfCaseComment.CommentBody__c = bodyText;
        return sfCaseComment;
    }

    private static FeedItem__x createGUSFeedItem(String parentId, String bodyText) {
        FeedItem__x feedItem = new FeedItem__x();
        feedItem.ParentId__c = parentId;
        feedItem.Status__c = 'Published';
        feedItem.Body__c = bodyText;
        return feedItem;
    }
    
    private static Boolean isValidCaseStatus(String status) {
        for (Schema.PicklistEntry caseStatus : Case__x.Status__c.getDescribe().getPicklistValues()) {
            if (caseStatus.getValue().equals(status)) {
                return true;
            }
        }
        return false;
    }
    
    private static Boolean canTriggerStatusChange(String status) {
        for (String value : Label.Esa_onSAChangeCaseStatusTriggerList.split(',')) {
            // Trim and compare to avoid space issues with user entry
            if (status.equals(value.trim())) {
                return true;
            }
        }
        return false;
    }
    
    private static Case__x getCase(String id) {
        return [SELECT ExternalId,CaseNumber__c,Status__c FROM Case__x WHERE Id =: id OR ExternalId =: id];
    }

    private static String parseEmail(String email) {
        if (String.isBlank(email)) {
            return email;
        }
        return email.substringBefore('+').substringBefore('@') + '@' + email.substringAfter('@');
    }

    @TestVisible
    private Datetime getLatestAssessmentsSyncDateTime() {
    
        IntegrationLastSyncDate__c syncDate = IntegrationLastSyncDate__c.getValues('SecurityAssessmentsSync');
        
        if(syncDate == null || syncDate.Security_Assessments_Last_Sync_DateTime__c == null) {
            return Datetime.newInstance(1970, 1, 1, 0, 0, 0); //only executed first time we run batch job
        } else {
            return syncDate.Security_Assessments_Last_Sync_DateTime__c;
        }
    }
}
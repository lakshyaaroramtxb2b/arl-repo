/*
* Sync External Case/Bug status back to HRI_Vulnerability Record
* Id batchJobId = Database.executeBatch(new SyncHRIVulnerMissedSLANotification(), 100);
 */

public class SyncHRIVulnerMissedSLANotification implements Schedulable, Database.Batchable<sObject> {

    public String SOURCE_FILE = 'SyncHRIVulnerMissedSLANotifications';
    
    public void execute(SchedulableContext ctx) {
      Database.executebatch(new SyncHRIVulnerMissedSLANotification(), 100);
    }

    public Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([
                              SELECT Id, 
                                     Name,
                                     Gus_Bug__c,
                                     Supportforce_Case__c,
                                     SLA_Missed_Notification__c,
                                     Vuln_Resolved_Within_SLA__c
                              FROM HRI_Vulnerability__c
                              WHERE (Supportforce_Case__c != null OR Gus_Bug__c != null)
                              AND Vuln_Resolved_Within_SLA__c = 'False'
                              AND SLA_Missed_Notification__c = false
                              AND External_Case_Closed__c = false]);
    }
    
    public void execute(Database.BatchableContext BC, List<HRI_Vulnerability__c> lstVulnerabilities) {  

        // Update HRI Vulnerability Records with External Case/Bug Status
        Set<String> caseIds = new Set<String>();
                              
        if(lstVulnerabilities.size() > 0) {

          for(HRI_Vulnerability__c hriVuln : lstVulnerabilities){
                string comment = '\n -- Vulnerability bot --- \n vulnerability record ('+ hriVuln.Name +') over SLA \n\n';
                comment += 'Vulnerability ('+ hriVuln.Name +') assigned to you is over SLA. Immediate action is now ';
                comment += 'required. If you need more time to remediate the vulnerability, you should request an extension ';
                comment += 'from GRC here: https://securityorg.force.com/iem/s/.';
                Database.SaveResult sr;
                if(String.isNotBlank(hriVuln.Gus_Bug__c)){  
                    ADM_Work_c__x work = [SELECT Assignee__c 
                                          FROM ADM_Work_c__x WHERE Id=:hriVuln.Gus_Bug__c];
                    FeedItem__x workFeedItem = new FeedItem__x();
                    workFeedItem.Body__c = (comment.length() <= 4000)? comment : comment.subString(0, 4000);
                    workFeedItem.ParentId__c = hriVuln.Gus_Bug__c;
                    workFeedItem.Status__c = 'Published';
                    sr = Database.insertImmediate(workFeedItem);
                }else if(String.isNotBlank(hriVuln.Supportforce_Case__c)){
                    CaseComment__x sfCaseComment = new CaseComment__x();
                    sfCaseComment.IsNotificationSelected__c = true;
                    sfCaseComment.CommentBody__c = (comment.length() < 4000)? comment : comment.abbreviate(4000);
                    sfCaseComment.IsPublished__c = true;
                    sfCaseComment.ParentId__c = hriVuln.Supportforce_Case__c;
                    sr = Database.insertImmediate(sfCaseComment);
                }
                  
                hriVuln.SLA_Missed_Notification__c = true;

          }
        
          List<Database.SaveResult> saveResults = Database.update(lstVulnerabilities,false);

          String finalErrorText = '';
          Integer numberCasesUpdated = 0;
            for (Database.SaveResult sr : saveResults) {
                if (sr.isSuccess()) {
                            numberCasesUpdated++;
                        } else {
                            for(Database.Error err : sr.getErrors()) {
                                finalErrorText += err.getStatusCode() + ': ' + err.getMessage() + '\n';
                            }
                }
          }
            
          Esa_DebugService.writeMessage('SyncExternalCaseStatus HRI_Vulnerability Completed:');
          if(string.isNotBlank(finalErrorText)) {
              Esa_DebugService.writeMessage('Sync External Case Status Update - HRI Vulnerability. finish: Failed with errors');
              Exception ex;
              Esa_DebugService.writeException(ex, 1, SOURCE_FILE, finalErrorText);
          }

        }
    }

    public void finish(Database.BatchableContext BC) {
       /* if(!Test.isRunningTest()) {
           SyncHRIVulnerMissedSLANotification batch = new SyncHRIVulnerMissedSLANotification();
           System.scheduleBatch(batch, 'SyncHRIVulnerMissedSLANotification', 30, 100);
        } */
    }
}
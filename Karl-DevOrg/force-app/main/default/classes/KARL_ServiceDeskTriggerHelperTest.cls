/**
* @author Lakshya Arora
* @email lakshya.arora@mtxb2b.com
* @description Test class for KARL_ServiceDeskTriggerHelper
*/
@isTest
public class KARL_ServiceDeskTriggerHelperTest {
    /**
* @author Lakshya Arora
* @email lakshya.arora@mtxb2b.com
* @description Test data setup
*/
    @testSetup
    private static void makeData(){
        User opsAdminUser = ARL_TestDataFactory.createOpsAdmin();
        System.runAs(opsAdminUser){
            Account acc = ARL_TestDataFactory.createAccount('Salesforce - ESA Office Hours');
            insert acc;
            Contact con = ARL_TestDataFactory.createContact(acc.Id, 'test community user name', 'comm123@invalid.com');
            insert con;
            String EVIDENCE_REQUEST_RECORDTYPEID = KARL_Constants.EVIDENCE_REQUEST_RECORDTYPEID;
            String REQUEST_MASTER_DATA_RECORDTYPEID = KARL_Constants.REQUEST_MASTER_DATA_RECORDTYPEID;
            List<KARL_Service_Desk__c> karlServiceDeskList = new List<KARL_Service_Desk__c>();
            for(Integer i=1;i<=6;i++){
                if(Math.mod(i, 2) == 0){
                    karlServiceDeskList.add(ARL_TestDataFactory.createKARLServiceDesk(EVIDENCE_REQUEST_RECORDTYPEID));
                }
                else{
                    karlServiceDeskList.add(ARL_TestDataFactory.createKARLServiceDesk(REQUEST_MASTER_DATA_RECORDTYPEID));
                }
            }
            insert karlServiceDeskList;
        }
    }
    
    /**
* @author Lakshya Arora
* @email lakshya.arora@mtxb2b.com
* @description This is used to test the Auto number functioanlity
*/
    @isTest
    private static void populateAutoNumberTest(){
        List<User> opsAdminUserList = ARL_TestDataFactory.fetchUser('abc@b.com.d');
        if(!opsAdminUserList.isEmpty()){
            System.runAs(opsAdminUserList[0]){
                String EVIDENCE_REQUEST_RECORDTYPEID = KARL_Constants.EVIDENCE_REQUEST_RECORDTYPEID;
                String REQUEST_MASTER_DATA_RECORDTYPEID = KARL_Constants.REQUEST_MASTER_DATA_RECORDTYPEID;
                List<KARL_Service_Desk__c> karlServiceDeskList = new List<KARL_Service_Desk__c>();
                for(Integer i=1;i<=2;i++){
                    if(Math.mod(i, 2) == 0){
                        karlServiceDeskList.add(ARL_TestDataFactory.createKARLServiceDesk(EVIDENCE_REQUEST_RECORDTYPEID));
                    }
                    else{
                        karlServiceDeskList.add(ARL_TestDataFactory.createKARLServiceDesk(REQUEST_MASTER_DATA_RECORDTYPEID));
                    }
                }
                Test.startTest();
                insert karlServiceDeskList;
                Test.stopTest();
                System.assertEquals([SELECT Auto_Number_Apex__c FROM KARL_Service_Desk__c WHERE RecordTypeId =:EVIDENCE_REQUEST_RECORDTYPEID ORDER BY Auto_Number_Apex__c DESC LIMIT 1].Auto_Number_Apex__c,
                                    4);
                System.assertEquals([SELECT Auto_Number_Apex__c FROM KARL_Service_Desk__c WHERE RecordTypeId =:REQUEST_MASTER_DATA_RECORDTYPEID ORDER BY Auto_Number_Apex__c DESC LIMIT 1].Auto_Number_Apex__c,
                                    4);
            }
        }
    }
    
    
    /**
* @author Lakshya Arora
* @email lakshya.arora@mtxb2b.com
* @description This is used to test all the functionality
*/
    @isTest
    private static void testDMLoperations(){
        Map<Id,Id> contactIdToUserMap = new Map<Id,Id>();
        List<User> opsAdminUserList = ARL_TestDataFactory.fetchUser('abc@b.com.d');
        if(!opsAdminUserList.isEmpty()){
            System.runAs(opsAdminUserList[0]){
                List<Contact> contactList = new List<Contact>([SELECT Id FROM Contact WHERE Name = 'test community user name']);
                contactIdToUserMap.put(contactList[0].Id,opsAdminUserList[0].Id);
                KARL_ServiceDeskTriggerHelper.contactIdToUserTestMap = contactIdToUserMap;
                KARL_Service_Desk__c serviceDeskObj = new KARL_Service_Desk__c();
                serviceDeskObj = ARL_TestDataFactory.createKARLServiceDesk(KARL_Constants.REQUEST_MASTER_DATA_RECORDTYPEID);
                serviceDeskObj.New_or_Existing__c = 'New';
                serviceDeskObj.GRC_Assignee__c = contactList[0].Id;
                serviceDeskObj.requested_By__c = contactList[0].Id;
                serviceDeskObj.Request_Description__c = 'Request Description';
                serviceDeskObj.Type__c = 'Inquiry';
                serviceDeskObj.Business_Unit__c = 'AS';
                serviceDeskObj.Status__c = KARL_Constants.STATUS_SUBMIT_FOR_APPROVAL;
                Test.startTest();
                insert serviceDeskObj;
                Test.stopTest();
                serviceDeskObj.Status__c = KARL_Constants.STATUS_APPROVED;
                update serviceDeskObj;
                serviceDeskObj.Status__c = KARL_Constants.STATUS_REJECTED;
                try{
                    update serviceDeskObj;
                }
                catch(Exception e){
                    System.assert(e.getMessage().contains('For rejection, please add rejection reason to the comments box.'),'Rejection Comment Validation Failed');
                }
            }
        }
    }
}
public without sharing class trustPublisherController {
        
    public transient Blob thumbnailBlob { get; set; }
    public String thumbnailFileName { get; set; }
    public transient Blob imageBlob { get; set; }
    public String imageFileName { get; set; }
    
    public enum DisplayMode {EDIT, CONFIRM_CLONE, UPLOAD_THUMBNAIL, UPLOAD_IMAGE, PREVIEW_AND_SUBMIT_FOR_APPROVAL, ERROR}
    public DisplayMode dMode {get; set;}
    public String dModeForVF { get{return dMode.name();}} // need this because VF does not seem to allow access to the ENUM (https://community.salesforce.com/t5/Visualforce-Development/Having-trouble-referencing-a-user-defined-enum/m-p/168355)
    
    public String errorText {get; set;}
    
    public Security_Incident__c inc;
    ApexPages.StandardController ctrl;
    
    private String folderId;
    public String trustUrl {get; set;}
    private String instanceName;

    public trustPublisherController(ApexPages.StandardController controller) {
        ctrl = controller;
        inc = (Security_Incident__c)ctrl.getRecord();
        dMode = DisplayMode.EDIT;
        Security_Incident_Config__c cfg = [select Documents_Folder_ID__c, Trust_Recent_Phishing_URL__c, Instance_Name__c from Security_Incident_Config__c where Active__c = True limit 1];
        folderId = cfg.Documents_Folder_ID__c;
        trustUrl = cfg.Trust_Recent_Phishing_URL__c;
        instanceName = cfg.Instance_Name__c;
    }
    
    Public PageReference checkEditStatus() {
        // check if we are editing an existing object or a new object
        if (inc.id != null) {
            // if id != null we are in edit mode
            
            //if (inc.Security_Approval_Status__c == 'Open') {
            //    // Open should allow editing the current object
            //}
            if (inc.Security_Approval_Status__c == 'Pending') {
                errorText = 'Can\'t edit objects in pending status. Please ask the approvers to reject the object first.';
                dMode = DisplayMode.ERROR;
                return null;
            }
            else if (inc.Security_Approval_Status__c == 'Closed') {
                // Can't edit objects that were already approved - clone and edit
                // this will create a new clone object that after the approval process 
                // will replace the current object (using a trigger)
                
                if (inc.Replaced_By__c != null) {
                    // Can't edit objects that already have a replacment
                    // instead you should edit the replacement object
                    errorText = 'Can\'t edit objects that were already replaced by another object. Please edit the current object.';
                    dMode = DisplayMode.ERROR;
                    return null;
                }
                
                // make sure no other object is preparing to replace this object - even if it is not yet approved
                Integer replaceAlreadyInProgress = [select count() from Security_Incident__c where Replaces__c =:inc.id ];
                if (replaceAlreadyInProgress > 0) {
                    errorText = 'Can\'t edit object. Another replacment object was already created for it.';
                    dMode = DisplayMode.ERROR;
                    return null;
                }
                
                // Need to clone and edit new object
                dMode = DisplayMode.CONFIRM_CLONE;
                return null;
            }
        }
        return null;    
    }
    
    private Boolean saveImage() {
    
        if (imageBlob == null || imageFileName == null) {
            return false;
        }
        
        // save image to a new Document object and mark it as public
        Document tmp = new Document();
        tmp.body = imageBlob;
        tmp.name = 'Security_Incident__' + inc.Date__c + '__' + imageFileName;
        //tmp.folderid = '00l30000001n4Ov'; // Set folderId to the ID of the SecurityIncidentManager folder id.
        tmp.folderid = folderId; // Set folderId to the ID of the SecurityIncidentManager folder id.
        tmp.contentType = 'image/gif';
        tmp.type = 'gif';
        tmp.ispublic = true;
        insert tmp;
        // assign the new Document URL to the incident ThumbnailURL field
        //inc.imageURL__c = 'https://na7.salesforce.com/servlet/servlet.ImageServer?id=' + tmp.id + '&oid=' + UserInfo.getOrganizationId();
        //inc.imageURL__c = 'https://na1.salesforce.com/servlet/servlet.ImageServer?id=' + tmp.id + '&oid=' + UserInfo.getOrganizationId();
        inc.imageURL__c = 'https://' + instanceName + '.salesforce.com/servlet/servlet.ImageServer?id=' + tmp.id + '&oid=' + UserInfo.getOrganizationId();
        
        return true;
    }
    
    private Boolean saveThumbnail() {
    
        if (thumbnailBlob == null || thumbnailFileName == null) {
            return false;
        }
    
        // save thumbnail to a new Document object and mark it as public
        Document tmp = new Document();
        tmp.body = thumbnailBlob;
        tmp.name = 'Security_Incident__' + inc.Date__c + '__' + thumbnailFileName;
        //tmp.folderid = '00l30000001n4Ov'; // // Set folderId to the ID of the SecurityIncidentManager folder id.
        tmp.folderid = folderId; // // Set folderId to the ID of the SecurityIncidentManager folder id.
        tmp.contentType = 'image/gif';
        tmp.type = 'gif';
        tmp.ispublic = true;
        insert tmp;
        // assign the new Document URL to the incident ThumbnailURL field
        //inc.thumbnailURL__c = 'https://na7.salesforce.com/servlet/servlet.ImageServer?id=' + tmp.id + '&oid=' + UserInfo.getOrganizationId();
        //inc.thumbnailURL__c = 'https://na1.salesforce.com/servlet/servlet.ImageServer?id=' + tmp.id + '&oid=' + UserInfo.getOrganizationId();        
        inc.thumbnailURL__c = 'https://' + instanceName + '.salesforce.com/servlet/servlet.ImageServer?id=' + tmp.id + '&oid=' + UserInfo.getOrganizationId();
        
        return true;
    }

    public Boolean checkPreApprovedTemplates() {
        // check if templates claims pre-approved - but values use text that is not pre-approved
        
        Map<String, String> NOAPreApproved = new Map<String, String>{
            'Malicious link' => 'Email that provides a link to download a file that contains malicious software.',
            'Malicious attachment' => 'Email with malicious software attached.',
            'Link to phishing' => 'Email containing links to phishing sites purporting to be salesforce.com.',
            'Under investigation' => 'Currently under investigation.'
            };

        Map<String, String> DOEPreApproved = new Map<String, String>{
            'Installs malicious software' => 'Installs malicious software to compromise an end user computer.',
            'Phishing' => 'Purported salesforce.com login page that attempts to steal user credentials.',
            'Under investigation' => 'Currently under investigation.'
            };

        Map<String, String> DAPreApproved = new Map<String, String>{
            'Do Not open the link' => 'DO NOT open the link; delete the email immediately.  Using a known safe PC, login to all online accounts you suspect may be compromised and change passwords.  Review Salesforce best practices on https://trust.salesforce.com/trust/practices/.',
            'Do Not open the attachment' => 'DO NOT open the attachment; delete the email immediately.  If you suspect that a PC has been compromised by this attack, immediately disconnect it from your network and run an anti-virus or anti-spyware utility (e.g., Trend Micro, Symantec, McAfee). Using a known safe PC, login to all online accounts you suspect may be compromised and change passwords.  Review Salesforce best practices on https://trust.salesforce.com/trust/practices/.',
            'Do Not click on the link to download the file' => 'DO NOT click on the link to download the file; delete the email immediately.  If you suspect that a PC has been compromised by this attack, immediately disconnect it from your network and run an anti-virus or anti-spyware utility (e.g., Trend Micro, Symantec, McAfee). Using a known safe PC, login to all online accounts you suspect may be compromised and change passwords.  Review Salesforce best practices on https://trust.salesforce.com/trust/practices/.',
            'Under investigation' => 'Currently under investigation.'
            };

        // check Nature of attack
        if (inc.Nature_of_attack_template__c != 'Other') {
            String mapVal = NOAPreApproved.get(inc.Nature_of_attack_template__c);
            if (mapVal == null || mapVal != inc.Nature_of_attack__c) {
                errorText = 'Nature of attack template and value don\'t match';
                dMode = DisplayMode.ERROR;
                return false;
            }
        }

        // check Description of exploit
        if (inc.Description_of_exploit_template__c != 'Other') {
            String mapVal = DOEPreApproved.get(inc.Description_of_exploit_template__c);
            if (mapVal == null || mapVal != inc.Description_of_exploit__c) {
                errorText = 'Description of exploit template and value don\'t match';
                dMode = DisplayMode.ERROR;
                return false;
            }
        }

        // check Defensive action
        if (inc.Defensive_action_template__c != 'Other') {
            String mapVal = DAPreApproved.get(inc.Defensive_action_template__c);
            if (mapVal == null || mapVal != inc.Defensive_action__c) {
                errorText = 'Defensive action template and value don\'t match';
                dMode = DisplayMode.ERROR;
                return false;
            }
        }

        return true;
    }
    
    public PageReference saveEvent() {
        // confirm pre-approved values matche the template values
        if (checkPreApprovedTemplates() == false)
            return null;

        // check URLs because they will be added to the output page without escaping
        if (ESAPI.validator().SFDC_isValidRedirectLocation(inc.ImageURL__c, null) == false) {
                errorText = 'Invalid Image URL';
                dMode = DisplayMode.ERROR;
                return null;
        }
        
        if (ESAPI.validator().SFDC_isValidRedirectLocation(inc.ThumbnailURL__c, null) == false) {
                errorText = 'Invalid Thumbnail URL';
                dMode = DisplayMode.ERROR;
                return null;
        }

        // Save the Incident object (Use the standard controller save() function to save all the standard fields)
        PageReference pr = ctrl.save();
        // update inc refernce after save
        inc = (Security_Incident__c)ctrl.getRecord();
        
        dMode = DisplayMode.PREVIEW_AND_SUBMIT_FOR_APPROVAL;
        
        return null;
    }

    public PageReference createEvent() {
        // create the documents and set their urls in the object before saving it
        saveThumbnail();
        saveImage();
        return saveEvent();
    }

    public PageReference goToUpdateThumbnail() {
        dMode = DisplayMode.UPLOAD_THUMBNAIL;
        return null;
    }
    
    public PageReference updateThumbnail() {
        if (saveThumbnail() == false)
            return null;
        dMode = DisplayMode.EDIT;
        return null;
    }
    
   public PageReference goToUpdateImage() {
        dMode = DisplayMode.UPLOAD_IMAGE;
        return null;
    }    
        
    public PageReference updateImage() {
        if (saveImage() == false)
            return null;
        dMode = DisplayMode.EDIT;
        return null;
    }

    public PageReference submitForApproval() {
        // Create an approval request
        Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
        req1.setComments('Submitting request for approval from VF.');
        req1.setObjectId(inc.id);

        // Submit the approval request for the account  
        Approval.ProcessResult result = Approval.process(req1);

        // Verify the result  
        if (result.isSuccess() == false) {
            errorText = 'Failed to submit object for approval.';
            dMode = DisplayMode.ERROR;
            return null;
        }

        return showViewPage();
    }

    public PageReference showViewPage() {
        // redirect to view the current object
        ApexPages.StandardController sc = new ApexPages.StandardController(inc);
        return sc.view();    
    }

    public PageReference showEditPage() {
        // redirect to edit the current object
        ApexPages.StandardController sc = new ApexPages.StandardController(inc);
        return sc.edit();
    }

    public PageReference cloneAndEdit() {
        // use the clone() method with deep clone and using a diferent id
        Security_Incident__c newObject = inc.clone(false, true);
        
        // set replacement values
        // after approved will set replacment values also on the old object
        newObject.Replaces__c = inc.id;
        newObject.ReplacesURL__c = inc.Name + ' (https://' + instanceName + '.salesforce.com/' + inc.id + ')';
        newObject.Security_Approval_Status__c = 'Open';
        insert newObject;

        // redirect to edit the cloned object
        ApexPages.StandardController sc = new ApexPages.StandardController(newObject);
        return sc.edit();
    }
}
/**
        * Class Name: KARL_DocuSignService
        * Purpose: Service class for the docusign integration
        * Created By/Date: Swarnima Singh Mandhata 04/26/2020
        */
public with sharing class KARL_DocuSignService {

        
        static KARL_DocuSignConfiguration__c docusignConfig = KARL_DocuSignConfiguration__c.getInstance();
    
        static String integratorKey = docusignConfig.Integrator_Key__c; // integrator key (found on Preferences -> API page)
        static String username = docusignConfig.User_Name__c; // account email (or your API userId)
        static String password = docusignConfig.Password__c; // account password
    
        // construct the DocuSign authentication header
        static String authenticationHeader =
                '<DocuSignCredentials>' +
                        '<Username>' + username + '</Username>' +
                        '<Password>' + password + '</Password>' +
                        '<IntegratorKey>' + integratorKey + '</IntegratorKey>' +
                        '</DocuSignCredentials>';
    
        /*
         * Method Name: fetchRecipients
         * Description: Method to fetch the recipients
         * @return: Recipients
         * @param: String, AuthenticationInformation
        */
        public static Recipients fetchRecipients(String envelopeId, AuthenticationInformation authenticationInformation) {
            String url = authenticationInformation.baseURL + '/envelopes/' + envelopeId + '/recipients';
    
            HttpRequest request = new HttpRequest();
    
            request.setEndpoint(url);
    
            request.setMethod('GET');
            request.setHeader('X-DocuSign-Authentication', authenticationHeader);
            request.setHeader('Accept', 'application/json');
    
            http h = new http();
    
            HttpResponse response = new HttpResponse();
    
            response = h.send(request);
    
            return (Recipients) JSON.deserialize(response.getBody(), Recipients.class);
        }
    
    
        /*
         * Method Name: resendSigningDoc
         * Description: Method to resend signing document
         * @return: String
         * @param: String
        */
        public static String resendSigningDoc(String envelopeId) {
            //============================================================================
            // STEP 1 - Make the Login API call to retrieve your baseUrl and accountId
            //============================================================================
    
            AuthenticationInformation authenticationInformation = authenticate(docusignConfig.Docusign_URL__c, authenticationHeader);
    
            //============================================================================
            // STEP 2 - Signature Request from Template API Call
            //============================================================================
            Recipients recipients = fetchRecipients(envelopeId, authenticationInformation);
    
            if (recipients == null || recipients.signers.isEmpty()) {
                return null;
            }
    
            String signerInfo;
    
            for (Signer temp : recipients.signers) {
                if (temp.status == 'sent') {
                    signerInfo = '{"signers": [{"recipientId": "' + temp.recipientId + '","name": "' + temp.name + '","email": "' + temp.email + '"}]}';
                }
            }
    
            if (signerInfo == null) {
                return null;
            }
    
            String url = authenticationInformation.baseURL + '/envelopes/' + envelopeId + '/recipients?resend_envelope=true';
            System.debug('url>>'+url);
            HttpRequest request = new HttpRequest();
    
            request.setEndpoint(url);
    
            request.setMethod('PUT');
            request.setHeader('X-DocuSign-Authentication', authenticationHeader);
            request.setHeader('Content-Type', 'application/json');
            request.setHeader('Accept', 'application/xml');
            request.setBody(signerInfo);
    
            http h = new http();
            HttpResponse response = new HttpResponse();
            response = h.send(request);
    
            return null;
        }
    
        /*
         * Method Name: getSignedDocument
         * Description: Method to get the signed document
         * @param: String
         * @return: DocumentInformation
        */
        public static DocumentInformation getSignedDocument(String envelopeId) {
            //============================================================================
            // STEP 1 - Make the Login API call to retrieve your baseUrl and accountId
            //============================================================================
    
            AuthenticationInformation authenticationInformation = authenticate(docusignConfig.Docusign_URL__c, authenticationHeader);
    
            Date signingDate;
            String signingStatus;
    
            HttpRequest request = new HttpRequest();
            http httpCallout = new http();
            HttpResponse response = new HttpResponse();
    
            String url = authenticationInformation.baseURL + '/envelopes/' + envelopeId;
    
            request = new HttpRequest();
    
            request.setEndpoint(url);
    
            request.setMethod('GET');
            request.setHeader('X-DocuSign-Authentication', authenticationHeader);
            request.setHeader('Content-Type', 'application/xml');
            request.setHeader('Accept', 'application/xml');
    
            httpCallout = new http();
    
            response = new HttpResponse();
    
            response = httpCallout.send(request);
    
            Dom.Document doc = response.getBodyDocument();
    
            for (Dom.XmlNode node : doc.getRootElement().getChildElements()) {
    
                if (node.getName() == 'status') {
    
                    signingStatus = node.getText();
                }
    
                if (node.getName() == 'completedDateTime') {
    
                    if (node.getText() != null) {
    
                        List<String> tempList = node.getText().split('T');
    
                        if (tempList != null && tempList.size() != 0) {
    
                            tempList = tempList.get(0).split('-');
    
                            if (tempList != null && tempList.size() > 2) {
    
                                signingDate = Date.parse(tempList.get(1) + '/' + tempList.get(2) + '/' + tempList.get(0));
                            }
                        }
                    }
                }
            }
    
            if (signingStatus == 'completed') {
    
                url = authenticationInformation.baseURL + '/envelopes/' + envelopeId + '/documents/combined';
    
                request = new HttpRequest();
    
                request.setEndpoint(url);
    
                request.setMethod('GET');
                request.setHeader('X-DocuSign-Authentication', authenticationHeader);
                request.setHeader('Content-Type', 'application/json');
                request.setHeader('Accept', 'application/pdf');
    
                httpCallout = new http();
    
                response = new HttpResponse();
                // sending http request
                response = httpCallout.send(request);
    
                return new DocumentInformation(envelopeId, response.getBodyAsBlob(), signingDate, signingStatus, null);
            }
    
            return new DocumentInformation(envelopeId, null, signingDate, signingStatus, null);
        }
    
        /*
         * Method Name: sendSigningDocument
         * Description: Method to get signing URL and envelopId of the document
         * @return: DocumentInformation
         * @param: String, String, List<SignerInformation>, String, String, String, Boolean, String
        */
        public static DocumentInformation sendSigningDocument(String content, List<SignerInformation> signers,
                String emailSubject, String emailBody, String documentName, Boolean isGetURL, String returnPageURL) {
            //============================================================================
            // STEP 1 - Make the Login API call to retrieve your baseUrl and accountId
            //============================================================================
            
            AuthenticationInformation authenticationInformation = authenticate(docusignConfig.Docusign_URL__c, authenticationHeader);
    
            //============================================================================
            // STEP 2 - Signature Request from Template API Call
            //============================================================================
            HttpRequest request = new HttpRequest();
            http httpCallout = new http();
            HttpResponse response = new HttpResponse();
    
            String url = authenticationInformation.baseURL + '/envelopes';
    
            // this example uses XML formatted requests, JSON format is also accepted
            String body = '<envelopeDefinition xmlns=\'http://www.docusign.com/restapi\'>' +
                    '<AllowMarkup>false</AllowMarkup>' +
                    '<emailSubject><![CDATA[' + emailSubject + ']]></emailSubject><emailBlurb><![CDATA[' + emailBody + ']]></emailBlurb>' +
                    '<documents><document><name>' + documentName + '</name><documentId>1</documentId>' +
                    '<documentBase64>' + content + '</documentBase64>' +
                    '</document></documents>' +
                    '<accountId>' + authenticationInformation.accountId + '</accountId>' +
                    '<status>sent</status>' +
                    '<emailSubject>' + emailSubject + '</emailSubject>' +
                    '<recipients><signers>';
    
            for (SignerInformation signer : signers) {
                body += '<signer><email>' + signer.signerEmail + '</email>' +
                        '<name>' + signer.signerName + '</name><recipientId>' + signer.recipientId + '</recipientId><routingOrder>' +
                        signer.routingOrder + '</routingOrder>' +
                        (isGetURL && signer.routingOrder == 1 ? '<clientUserId>' + signer.recipientId + '</clientUserId>' : '') +
                        (signer.signLabel != null?
                         ('<tabs>'+
                          '<dateSignedTabs><dateSigned><tabLabel>' + signer.signLabel + 'Date</tabLabel><name>' + signer.signLabel + 'Date</name>' +
                         '<anchorString>' + signer.signLabel + 'Date</anchorString><anchorMatchWholeWord>true</anchorMatchWholeWord><anchorXOffset>0</anchorXOffset>' +
                         '<anchorYOffset>10</anchorYOffset></dateSigned></dateSignedTabs>'+
                          '<signHereTabs><signHere><anchorString>' +
                         signer.signLabel + '</anchorString><anchorMatchWholeWord>true</anchorMatchWholeWord><anchorXOffset>0</anchorXOffset><anchorYOffset>0</anchorYOffset>' +
                         '<anchorIgnoreIfNotPresent>false</anchorIgnoreIfNotPresent><anchorUnits>inches</anchorUnits></signHere></signHereTabs>'+
                          '</tabs>'):'')+
                        '</signer>';
            }
    
            body += '</signers></recipients></envelopeDefinition>';
            System.debug( 'envelopeDefinition::: '+body);
            request = new HttpRequest();
    
            request.setEndpoint(url);
    
            request.setMethod('POST');
            request.setHeader('X-DocuSign-Authentication', authenticationHeader);
            request.setHeader('Content-Type', 'application/xml');
            request.setHeader('Accept', 'application/xml');
            request.setHeader('Content-Length', String.valueOf(body.length()));
    
            if (body.contains('&')) {
                body = body.replace('&', '&amp;');
            }
    
            request.setBody(body);
    
            httpCallout = new http();
    
            response = new HttpResponse();
    
            response = httpCallout.send(request);
            System.debug('Response 1 : ' + response.getBody());
            String envelopeId = '';
            String uri = '';
    
            Dom.Document doc = response.getBodyDocument();
    
            for (Dom.XmlNode node : doc.getRootElement().getChildElements()) {
                if (node.getName() == 'envelopeId') {
    
                    envelopeId = node.getText();
                }
    
                if (node.getName() == 'uri') {
    
                    uri = node.getText();
                }
            }
    
            String signingUrl;
    
            if (isGetURL) {
                url = authenticationInformation.baseURL + uri + '/views/recipient';
                System.debug('URL : ' + url);
    
                body = '<recipientViewRequest xmlns=\'http://www.docusign.com/restapi\'>' +
                        '<returnUrl>' + returnPageURL + '</returnUrl>' +
                        '<authenticationMethod>email</authenticationMethod>' +
                        '<email>' + signers.get(0).signerEmail + '</email>' +
                        '<clientUserId>' + signers.get(0).recipientId + '</clientUserId>' +
                        '<userName>' + signers.get(0).signerName + '</userName>' +
                        '</recipientViewRequest>';
    
                request = new HttpRequest();
    
                request.setEndpoint(url);
    
                request.setMethod('POST');
                request.setHeader('X-DocuSign-Authentication', authenticationHeader);
                request.setHeader('Content-Type', 'application/xml');
                request.setHeader('Accept', 'application/xml');
                request.setHeader('Content-Length', String.valueOf(body.length()));
    
                if (body.contains('&')) {
                    body = body.replace('&', '&amp;');
                }
    
                request.setBody(body);
                System.debug('Request : ' + request);
                http h = new http();
    
                response = new HttpResponse();
    
                //Send http request
                response = h.send(request);
                System.debug('Response 2 : ' + response);
                doc = response.getBodyDocument();
    
                for (dom.XmlNode node : doc.getRootElement().getChildElements()) {
    
                    if (node.getName() == 'url') {
    
                        signingUrl = node.getText();
                    }
                }
            }
    
            return new DocumentInformation(envelopeId, null, null, null, signingUrl);
        }
    
    
        /*
         * Method Name: getSignedDocumentAfterOwner
         * Description: Method to get Document and Add into attachment the docusign api
         * @param: String, String, String, KARL_Cycle_Audit_Report_Items__c
         * @return: void
        */
        public static void getSignedDocumentAfterOwner(String envelopeId, String QuoteId, String AttachementId, KARL_Cycle_Audit_Report_Items__c cd){
            System.debug('Get Document : ' + QuoteId);
            AuthenticationInformation authenticationInformation = authenticate(docusignConfig.Docusign_URL__c, authenticationHeader);
    
            //============================================================================
            // STEP 2 - Signature Request from Template API Call
            //============================================================================
    
            String url = authenticationInformation.baseURL + '/envelopes/' + envelopeId + '/documents/combined';
            HttpRequest request = new HttpRequest();
            http httpCallout = new http();
            HttpResponse response = new HttpResponse();
    
            request = new HttpRequest();
    
            request.setEndpoint(url);
    
            request.setMethod('GET');
            request.setHeader('X-DocuSign-Authentication', authenticationHeader);
            request.setHeader('Content-Type', 'application/json');
            request.setHeader('Accept', 'application/pdf');
    
            httpCallout = new http();
    
            response = new HttpResponse();
            // sending http request
            response = httpCallout.send(request);
    
            saveSignedDocument(QuoteId, response.getBodyAsBlob(), envelopeId, null, AttachementId, cd);
    
        }
    
        /*
         * Method Name: saveSignedDocument
         * Description: Method to get Document and Add into attachment the docusign api
         * @param: String, Blob, String, Date, String, Contract_Document__c
         * @return: void
        */
        public static void saveSignedDocument(String quoteId, blob body, String envelopeId, Date signingDate, String AttachementId, KARL_Cycle_Audit_Report_Items__c cd){
            System.debug('In Save Document ' + quoteId);
            //Quote quoteObject = [SELECT Id, Name FROM Quote WHERE Id=: quoteId];
            KARL_DocuSignService.DocumentInformation ci = new KARL_DocuSignService.DocumentInformation(null,null,null,null,null);
            ci = KARL_DocuSignService.getSignedDocument(envelopeId);
            System.debug('Status :'+ci.signingStatus);
            if(ci.signingStatus == 'completed') {
                //cd.Application_Status__c = 'Signed';
                //cd.Signed_Date__c = ci.signingDate;
            }
            // I-31795 - To enable sign button when staus is not signed - start
            // else {
            //     cd.Application_Status__c = 'Initial Document';
            // }
            // I-31795 - To enable sign button when staus is not signed - end
            Delete [SELECT Id FROM Attachment WHERE Id =: AttachementId];
    
            Attachment attach = new Attachment();
            try {
                body = body;
            } catch (VisualforceException e) {
                system.debug('in the catch block');
                body = Blob.valueOf('Some Text');
            }
            attach.Body = body;
            //attach.Name = 'Document' + quoteObject.Name + '.pdf';
            attach.contentType = 'application/pdf';
            attach.IsPrivate = false;
            attach.parentId = quoteId;
            insert attach;
            
            //cd.Attachment__c = attach.Id;
            update cd;
    
    
        }
    
    
    
        /*
         * Method Name: authenticate
         * Description: Method to authenticate the docusign api
         * @param: String, String
         * @return: AuthenticationInformation
        */
        private static AuthenticationInformation authenticate(String docusignURL, String authenticationHeader) {
            String url = docusignURL + '/restapi/v2/login_information';
            String body = '';   // no request body for the login call
    
            HttpRequest request = new HttpRequest();
    
            request.setEndpoint(url);
    
            request.setMethod('GET');
            request.setHeader('X-DocuSign-Authentication', authenticationHeader);
            request.setHeader('Content-Type', 'application/xml');
            request.setHeader('Accept', 'application/xml');
    
            http httpCallout = new http();
            //Creating http response object
            HttpResponse response = new HttpResponse();
    
            response = httpCallout.send(request);
    
            Dom.Document doc = response.getBodyDocument();
    
            String baseUrl = '';
    
            String accountId = '';
    
            for (Dom.XmlNode node : doc.getRootElement().getChildElements()) {
    
                if (node.getName() == 'loginAccounts') {
    
                    for (Dom.XmlNode node1 : node.getChildElements()) {
    
                        if (node1.getName() == 'loginAccount') {
    
                            for (Dom.XmlNode node2 : node1.getChildElements()) {
    
                                if (node2.getName() == 'accountId') {
    
                                    accountId = node2.getText();
                                }
    
                                if (node2.getName() == 'baseUrl') {
    
                                    baseUrl = node2.getText();
                                }
                            }
                        }
                    }
                }
            }
    
            return new AuthenticationInformation(baseUrl, accountId);
        }
    
        //Wrapper class for the document information
        public class DocumentInformation {
            public String envelopeId;
            public blob documentData;
            public Date signingDate;
            public String signingStatus;
            public String url;
    
            public DocumentInformation(String envelopeId, blob documentData,
                    Date signingDate, String signingStatus, String url) {
                this.envelopeId = envelopeId;
                this.documentData = documentData;
                this.signingDate = signingDate;
                this.signingStatus = signingStatus;
                this.url = url;
            }
        }
    
        //Wrapper class for the signer information
        public class SignerInformation {
            public String signerName;
            public String signerEmail;
            public Integer recipientId;
            public Integer routingOrder;
            public String signLabel;
    
            public SignerInformation(String signerName, String signerEmail, Integer recipientId, Integer routingOrder, String signLabel) {
                this.signerName = signerName;
                this.signerEmail = signerEmail;
                this.recipientId = recipientId;
                this.routingOrder = routingOrder;
                this.signLabel = signLabel;
            }
        }
    
        //Wrapper class for the authentication information
        class AuthenticationInformation {
            String baseURL;
            String accountId;
    
            AuthenticationInformation(String baseURL, String accountId) {
                this.baseURL = baseURL;
                this.accountId = accountId;
            }
        }
        //Wrapper class for Recipients
        public class Recipients {
            public List<Signer> signers;
        }
    
        //Wrapper class for Signer
        public class Signer {
            public String status;
            public String email;
            public String name;
            public String recipientId;
            public String routingOrder;
            public String signedDateTime;
        }
}
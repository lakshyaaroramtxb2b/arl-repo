public class GUS_user_story {
    string record_type_id = '0129000000006gDAAQ';
    
    string Epic;
    
    public string selected_type{get;set;}
    public string subject{get; set;}
    public string points{get; set;}
    
    public ADM_Work_c__x work_recent{get;set;}
    
     /*
        a3QB00000000eozMAA  Prodsec AppExchange
    a3QB00000000eouMAA  Prodsec Bug Bounty
    a3QB00000000eopMAA  Prodsec Bug Management
    a3QB00000000jGnMAI  ProdSec Integration Baseline
    a3QB00000000epJMAQ  Prodsec Pop Up
    a3QB00000000encMAA  Prodsec Security Advisory
    a3QB00000000ep4MAA  Prodsec Tools
 
       */
    
    private static final Map<String, String> epic_id = new Map<String, String>{
        'App' => 'a3QB00000000eozMAA',
        'bounty' => 'a3QB00000000eouMAA',
        'bug_manage' => 'a3QB00000000eopMAA',
        'integ' => 'a3QB00000000jGnMAI',
        'Popup' => 'a3QB00000000epJMAQ',
        'sec_adv' => 'a3QB00000000encMAA',
        'tools' => 'a3QB00000000ep4MAA'
            
    };
        
   public pagereference set_field(){
       switch on selected_type{
           when 'App'{
               subject = '[AppExchange]- ';}
           when 'bounty'{
               subject ='[Bug Bounty]- ';
           }
           when 'bug_manage'{
               subject ='[Bug Management]- ';
           }
           
           
           
       }
            
       return null;
        }
    
    
    public GUS_user_story(){
        
        string name =userinfo.getName();
        subject = ApexPages.currentPage().getParameters().get('subject');
        points = ApexPages.currentPage().getParameters().get('points');
        if(!string.isNotBlank(subject))
            subject = '[AppExchange]- ';
        
        string email = [SELECT Email FROM User WHERE Name = :name AND ProfileId = '00e3A000001qRgIQAU'][0].Email;
        
        id gus_user_id =[SELECT ExternalId, Wall_Preference_c__c  from GusUser__x  where Email__c  = :email and IsActive__c = True][0].ExternalID;
       
        string team_id = 'a00B0000008LqH3IAK';
        
        //Finding the Right Sprint
        work_recent = [SELECT Name__c, Assignee__c, Assignee__r.name__c, Product_Owner_c__c, Product_Tag_c__c, QA_Engineer_c__c, RecordTypeId__c, Team__c, Sprint_c__c  FROM ADM_Work_c__x where Assignee__c = :gus_user_id  ORDER By LastModifiedDate__c DESC LIMIT 1][0];
        system.debug(work_recent);
        
        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Will Create User Stories Based on: ' + work_recent.get('Name__c'));
        ApexPages.addMessage(myMsg);
        //JSONParser parser = JSON.createParser(string.valueof(gus_user.get('Wall_Preference_c__c')));
        //system.debug(parser);
        //
        //
        
        
    }
    
    
    public List<SelectOption> getItems() {
            List<SelectOption> options = new List<SelectOption>();
            options.add(new SelectOption('App','Prodsec AppExchange'));
            options.add(new SelectOption('bounty','Prodsec Bug Bounty'));
            options.add(new SelectOption('bug_manage','Prodsec Bug Management'));
            options.add(new SelectOption('integ','ProdSec Integration Baseline'));
            options.add(new SelectOption('Popup','Prodsec Pop Up'));
            options.add(new SelectOption('sec_adv','Prodsec Security Advisory'));
            Options.add(new SelectOption('tools','Prodsec Tools'));
 
            return options;
        }
    
    public pagereference save(){
        try{
        ADM_Work_c__x new_US = new ADM_Work_c__x();
        new_US.Assignee__c = work_recent.Assignee__c;
        new_US.RecordTypeId__c =work_recent.RecordTypeId__c;
        new_US.sprint_c__c = work_recent.Sprint_c__c;
        new_US.QA_Engineer_c__c = work_recent.QA_Engineer_c__c;
        new_US.Product_Tag_c__c = work_recent.Product_Tag_c__c;
        new_US.Team__c = work_recent.Team__c;
        new_US.Subject_c__c =subject;
        new_US.Epic_c__c = epic_id.get(selected_type);
        new_US.Story_Points_c__c = integer.valueof(points);
      
        Database.insertAsync(new_US);
        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'A request to create a user story is made. You should receive an email');
        ApexPages.addMessage(myMsg);
        }
        catch(exception e){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL,'Creation of user story failed');
        ApexPages.addMessage(myMsg);
            
        }
        return null;
    }
    
    
  

}
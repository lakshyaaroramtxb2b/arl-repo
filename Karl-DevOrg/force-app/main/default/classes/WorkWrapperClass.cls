public with sharing class WorkWrapperClass {
    public id  Recordid;
    public integer  UniqueId;
    public string FromUser;
    public string ToUser;
    public id ToUserid;
    public decimal CurrentScore;
    public String badgeHashWord;
    public String badgeWord;
    public string message;
    public string status;
    public list<string> errorMessages;
    
    public WorkWrapperClass(){
        CurrentScore=0;
        message='';
        errorMessages=new list<string>();
    }
    

}
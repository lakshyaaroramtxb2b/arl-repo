global class CSIRT_DailyIncidentDigestSend Implements Schedulable{
	global void execute(SchedulableContext sc){
        List<Id> contactId = new List<Id>{'0033000001qwaR1', '0033A00002EecGdQAJ'};
        for(Id c : contactId){
        	sendmail(c);  
        }
	}
 
	public void sendmail(Id contactId){
		Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        email.setOrgWideEmailAddressId('0D2300000004E4Z');
        email.setTemplateId('00X3A000002537c');
        email.setTargetObjectId(contactId);
        email.saveAsActivity = False;
		Messaging.sendEmail(New Messaging.SingleEmailMessage[]{email});
	}
}
public class Bugs_To_Review_Controller {

   public List<Bugs_To_Review__c> bugs { get; set; }
  
   //used to get a hold of the account record selected for deletion
   public string SelectedBugsId { get; set; }
  
   public Bugs_To_Review_Controller() {
       //load account data into our DataTable
       LoadData();
   }
  
   private void LoadData() {
      // bugs = [Select id, number__c, link__c, reviewed__c, subject__c from Bugs_To_Review__c WHERE reviewed__c=false and assignee__c = :UserInfo.getUserName() limit 20];
      bugs = [Select id, number__c, link__c, reviewed__c, subject__c from Bugs_To_Review__c WHERE assignee__c = :UserInfo.getUserName() limit 20];
   }
  
   public void DeleteBugs()
   {
      // if for any reason we are missing the reference 
      if (SelectedBugsId == null) {
      
         return;
      }
     
      // find the account record within the collection
      Bugs_To_Review__c tobeDeleted = null;
      for(Bugs_To_Review__c b : bugs)
       if (b.Id == SelectedBugsId) {
          tobeDeleted = b;
          break;
       }
      
      //if account record found delete it
      if (tobeDeleted != null) {
       Delete tobeDeleted;
      }
     
      //refresh the data
      LoadData();
   }    
   
      public void UpdateBugs()
   {
      // if for any reason we are missing the reference 
      if (SelectedBugsId == null) {
      
         return;
      }
     
      // find the account record within the collection
      Bugs_To_Review__c tobeUpdated = null;
      for(Bugs_To_Review__c b : bugs)
       if (b.Id == SelectedBugsId) {
          tobeUpdated = b;
          break;
       }
      
      //if account record found delete it
      if (tobeUpdated != null) {
       Update tobeUpdated;
       system.debug('updated ' + tobeUpdated.Id);
      }
     
      //refresh the data
      LoadData();
   }  
  
  
}
public class CDI_SyncExternalOrgReports implements Database.Batchable<sObject>{
    public Database.QueryLocator start(Database.BatchableContext bc) {
        system.debug('###### Batch job begins');
        Set<String> setOfStr = new Set<String>{'QL-000184','QL-000148', 'QL-001086', 'QL-001093', 'QL-000059', 'QL-000073','QL-000053', 'QL-000204', 'QL-000065', 'QL-000011', 'QL-001033', 'QL-000016'};
            //return Database.getQueryLocator('select Name__c,(select Name__c  from Form_Question_Answer_Options__r),Form_c__r.Name__c,Form_Response_c__r.Name__c,Form_Response_c__r.Team_c__c,Answer_Long_Text_c__c,Answer_Text_c__c,Lookup_Value_Name_c__c,Number_of_Selected_Options_c__c,Question_c__r.Name__c,Form_Question_Link_c__r.Name__c  from ADM_Form_Question_Answer_c__x');
        return Database.getQueryLocator([SELECT Name__c,
                                             (SELECT Name__c FROM Form_Question_Answer_Options__r),
                                             Form_c__r.Name__c,
                                             Form_Response_c__r.Name__c,
                                             Form_Response_c__r.Team_c__c,
                                             Answer_Long_Text_c__c,
                                             Answer_Text_c__c,
                                             Lookup_Value_Name_c__c,
                                             Number_of_Selected_Options_c__c,
                                             Question_c__r.Name__c,
                                             Form_Question_Link_c__r.Name__c  
                                         FROM ADM_Form_Question_Answer_c__x
                                         WHERE Form_Question_Link_c__r.Name__c In :setOfStr]);
    }
     public void execute(Database.BatchableContext bc, List<ADM_Form_Question_Answer_c__x> listquestion){
         system.debug('#########'+listquestion.size());
         List<CDI_Form__c> formdatalist=new List<CDI_Form__c>();
        List<CDI_Form_Question_Answer__c> customquestionlist=new List<CDI_Form_Question_Answer__c>();
         for(ADM_Form_Question_Answer_c__x reports:listquestion)
        {
          for(ADM_Form_Question_Answer_Option_c__x listdata:reports.Form_Question_Answer_Options__r)
          {     
              If(reports.Form_Question_Link_c__r.Name__c == 'QL-000184')
              {
                 
              If(listdata.Name__c == 'Yes')
              {
                  CDI_Form_Question_Answer__c questionlinks=new CDI_Form_Question_Answer__c();
                  questionlinks.Name=reports.Form_Question_Link_c__r.Name__c;
                  questionlinks.External_Id__c=reports.Form_Question_Link_c__r.Name__c;
                  customquestionlist.add(questionlinks);
                  CDI_Form__c formlinks=new CDI_Form__c();
                  formlinks.Name=reports.Form_c__r.Name__c;
                  System.debug('LocalName'+formlinks.Name);
                  System.debug('Name'+reports.Form_c__r.Name__c);
                  formlinks.Answer_Number__c=reports.Name__c;
                  formlinks.External_Id__c=reports.Name__c;
                  formlinks.Question_Question_Name__c=reports.Question_c__r.Name__c;
                  formlinks.Form_Question_Answer__r=new CDI_Form_Question_Answer__c(External_Id__c=reports.Form_Question_Link_c__r.Name__c);
                  formlinks.Form_Response_Name__c=reports.Form_Response_c__r.Name__c;
                  formlinks.Team_Team_Name__c=reports.Form_Response_c__r.Team_c__c;
                  formlinks.Answer_Long_Text__c=reports.Answer_Long_Text_c__c;
                  formlinks.Answer_Text__c=reports.Answer_Text_c__c;
                  formlinks.Lookup_Value_Name__c=reports.Lookup_Value_Name_c__c;
                  formlinks.Number_of_Selected_Options__c=reports.Number_of_Selected_Options_c__c;
                  formlinks.Option__c=listdata.Name__c;
                   If(reports.Answer_Long_Text_c__c !=null)
                           {
                           formlinks.Check_Answer_Long_Text__c=True;
                           }
                  formdatalist.add(formlinks);
              }
              
          }
              else if(reports.Form_Question_Link_c__r.Name__c == 'QL-001048')
                  
              {
                  
              If(listdata.Name__c == 'Yes' || listdata.Name__c == 'Process or store confidential or restricted data?i.e PII, PCI or Payment Related' || listdata.Name__c == 'Process store confidential,restricted,regulated non-public customer data' || listdata.Name__c == 'Add new/modify cryptography design or implementation' || listdata.Name__c == 'New cross-cloud integration with other Salesforce cloud(s)' || listdata.Name__c =='Leverage external 3rd party vendor or services')
              {
                  CDI_Form_Question_Answer__c questionlinks=new CDI_Form_Question_Answer__c();
                  questionlinks.Name=reports.Form_Question_Link_c__r.Name__c;
                  questionlinks.External_Id__c=reports.Form_Question_Link_c__r.Name__c;
                  customquestionlist.add(questionlinks);
                  CDI_Form__c formlinks=new CDI_Form__c();
                  formlinks.Name=reports.Form_c__r.Name__c;
                   System.debug('LocalName'+formlinks.Name);
                  System.debug('Name'+reports.Form_c__r.Name__c);
                  formlinks.Answer_Number__c=reports.Name__c;
                  formlinks.External_Id__c=reports.Name__c;
                  formlinks.Question_Question_Name__c=reports.Question_c__r.Name__c;
                  formlinks.Form_Question_Answer__r=new CDI_Form_Question_Answer__c(External_Id__c=reports.Form_Question_Link_c__r.Name__c);
                  formlinks.Form_Response_Name__c=reports.Form_Response_c__r.Name__c;
                  formlinks.Team_Team_Name__c=reports.Form_Response_c__r.Team_c__c;
                  formlinks.Answer_Long_Text__c=reports.Answer_Long_Text_c__c;
                  formlinks.Answer_Text__c=reports.Answer_Text_c__c;
                  formlinks.Lookup_Value_Name__c=reports.Lookup_Value_Name_c__c;
                  formlinks.Number_of_Selected_Options__c=reports.Number_of_Selected_Options_c__c;
                  formlinks.Option__c=listdata.Name__c;
                   If(reports.Answer_Long_Text_c__c !=null)
                           {
                           formlinks.Check_Answer_Long_Text__c=True;
                           }
                  formdatalist.add(formlinks);
              }
              
          }
               else if(reports.Form_Question_Link_c__r.Name__c == 'QL-001086')
              {
                  System.debug('Inside loop QL-001086' );
              If(listdata.Name__c == 'Yes' || listdata.Name__c == 'Data Storage Security (Encryption, Transfer, addition or removal of storage)')
              {
                  CDI_Form_Question_Answer__c questionlinks=new CDI_Form_Question_Answer__c();
                  questionlinks.Name=reports.Form_Question_Link_c__r.Name__c;
                  questionlinks.External_Id__c=reports.Form_Question_Link_c__r.Name__c;
                  customquestionlist.add(questionlinks);
                  CDI_Form__c formlinks=new CDI_Form__c();
                  formlinks.Name=reports.Form_c__r.Name__c;
                  formlinks.Answer_Number__c=reports.Name__c;
                  formlinks.External_Id__c=reports.Name__c;
                  formlinks.Question_Question_Name__c=reports.Question_c__r.Name__c;
                  formlinks.Form_Question_Answer__r=new CDI_Form_Question_Answer__c(External_Id__c=reports.Form_Question_Link_c__r.Name__c);
                  formlinks.Form_Response_Name__c=reports.Form_Response_c__r.Name__c;
                  formlinks.Team_Team_Name__c=reports.Form_Response_c__r.Team_c__c;
                  formlinks.Answer_Long_Text__c=reports.Answer_Long_Text_c__c;
                  formlinks.Answer_Text__c=reports.Answer_Text_c__c;
                  formlinks.Lookup_Value_Name__c=reports.Lookup_Value_Name_c__c;
                  formlinks.Number_of_Selected_Options__c=reports.Number_of_Selected_Options_c__c;
                  formlinks.Option__c=listdata.Name__c;
                   If(reports.Answer_Long_Text_c__c !=null)
                           {
                           formlinks.Check_Answer_Long_Text__c=True;
                           }
                  formdatalist.add(formlinks);
              }
              
          }
               else if(reports.Form_Question_Link_c__r.Name__c == 'QL-001093' || reports.Form_Question_Link_c__r.Name__c == 'QL-000059' || reports.Form_Question_Link_c__r.Name__c == 'QL-000073' || reports.Form_Question_Link_c__r.Name__c == 'QL-000053')
              {
                  
              If(listdata.Name__c == 'Confidential' || listdata.Name__c == 'Restricted' || listdata.Name__c == 'Mission Critical')
              {
                  CDI_Form_Question_Answer__c questionlinks=new CDI_Form_Question_Answer__c();
                  questionlinks.Name=reports.Form_Question_Link_c__r.Name__c;
                  questionlinks.External_Id__c=reports.Form_Question_Link_c__r.Name__c;
                  customquestionlist.add(questionlinks);
                  CDI_Form__c formlinks=new CDI_Form__c();
                  formlinks.Name=reports.Form_c__r.Name__c;
                  formlinks.Answer_Number__c=reports.Name__c;
                  formlinks.External_Id__c=reports.Name__c;
                  formlinks.Question_Question_Name__c=reports.Question_c__r.Name__c;
                  formlinks.Form_Question_Answer__r=new CDI_Form_Question_Answer__c(External_Id__c=reports.Form_Question_Link_c__r.Name__c);
                  formlinks.Form_Response_Name__c=reports.Form_Response_c__r.Name__c;
                  formlinks.Team_Team_Name__c=reports.Form_Response_c__r.Team_c__c;
                  formlinks.Answer_Long_Text__c=reports.Answer_Long_Text_c__c;
                  formlinks.Answer_Text__c=reports.Answer_Text_c__c;
                  formlinks.Lookup_Value_Name__c=reports.Lookup_Value_Name_c__c;
                  formlinks.Number_of_Selected_Options__c=reports.Number_of_Selected_Options_c__c;
                  formlinks.Option__c=listdata.Name__c;
                   If(reports.Answer_Long_Text_c__c !=null)
                           {
                           formlinks.Check_Answer_Long_Text__c=True;
                           }
                  formdatalist.add(formlinks);
              }
              
          }
               else if(reports.Form_Question_Link_c__r.Name__c == 'QL-000204')
              {
                   System.debug('Inside loop QL-000204');
              If(listdata.Name__c == 'Yes' || listdata.Name__c == 'Storing Restricted or Mission Critical data in a 3rd party or non-production env')
              {
                  CDI_Form_Question_Answer__c questionlinks=new CDI_Form_Question_Answer__c();
                  questionlinks.Name=reports.Form_Question_Link_c__r.Name__c;
                  questionlinks.External_Id__c=reports.Form_Question_Link_c__r.Name__c;
                  customquestionlist.add(questionlinks);
                  CDI_Form__c formlinks=new CDI_Form__c();
                  formlinks.Name=reports.Form_c__r.Name__c;
                  formlinks.Answer_Number__c=reports.Name__c;
                  formlinks.External_Id__c=reports.Name__c;
                  formlinks.Question_Question_Name__c=reports.Question_c__r.Name__c;
                  formlinks.Form_Question_Answer__r=new CDI_Form_Question_Answer__c(External_Id__c=reports.Form_Question_Link_c__r.Name__c);
                  formlinks.Form_Response_Name__c=reports.Form_Response_c__r.Name__c;
                  formlinks.Team_Team_Name__c=reports.Form_Response_c__r.Team_c__c;
                  formlinks.Answer_Long_Text__c=reports.Answer_Long_Text_c__c;
                  formlinks.Answer_Text__c=reports.Answer_Text_c__c;
                  formlinks.Lookup_Value_Name__c=reports.Lookup_Value_Name_c__c;
                  formlinks.Number_of_Selected_Options__c=reports.Number_of_Selected_Options_c__c;
                  formlinks.Option__c=listdata.Name__c;
                   If(reports.Answer_Long_Text_c__c !=null)
                           {
                           formlinks.Check_Answer_Long_Text__c=True;
                           }
                  formdatalist.add(formlinks);
              }
              
          }
              else
              {
                  System.debug('Else Statement');
              }
          }
               if (reports.Form_Question_Link_c__r.Name__c == 'QL-000065' || reports.Form_Question_Link_c__r.Name__c == 'QL-000011' || reports.Form_Question_Link_c__r.Name__c == 'QL-001033' ||reports.Form_Question_Link_c__r.Name__c == 'QL-000016' )
              {
                  
                  CDI_Form_Question_Answer__c questionlinks=new CDI_Form_Question_Answer__c();
                  questionlinks.Name=reports.Form_Question_Link_c__r.Name__c;
                  questionlinks.External_Id__c=reports.Form_Question_Link_c__r.Name__c;
                  customquestionlist.add(questionlinks);
                  CDI_Form__c formlinks=new CDI_Form__c();
                  formlinks.Name=reports.Form_c__r.Name__c;
                  formlinks.Answer_Number__c=reports.Name__c;
                  formlinks.External_Id__c=reports.Name__c;
                  formlinks.Question_Question_Name__c=reports.Question_c__r.Name__c;
                  formlinks.Form_Question_Answer__r=new CDI_Form_Question_Answer__c(External_Id__c=reports.Form_Question_Link_c__r.Name__c);
                  formlinks.Form_Response_Name__c=reports.Form_Response_c__r.Name__c;
                  formlinks.Team_Team_Name__c=reports.Form_Response_c__r.Team_c__c;
                  formlinks.Answer_Long_Text__c=reports.Answer_Long_Text_c__c;
                  formlinks.Answer_Text__c=reports.Answer_Text_c__c;
                  formlinks.Lookup_Value_Name__c=reports.Lookup_Value_Name_c__c;
                  formlinks.Number_of_Selected_Options__c=reports.Number_of_Selected_Options_c__c;
                   If(reports.Answer_Long_Text_c__c !=null)
                           {
                           formlinks.Check_Answer_Long_Text__c=True;
                           }
                  formdatalist.add(formlinks);
              }
           
           }
         System.debug(formdatalist);
        Schema.SObjectField externalId=CDI_Form_Question_Answer__c.Fields.External_Id__c;
        Schema.SObjectField externalIdform=CDI_Form__c.Fields.External_Id__c;
        Database.upsert(customquestionlist,externalId,false); 
        Database.upsert(formdatalist,externalIdform,false); 
        }
    public void finish(Database.BatchableContext bc){
        
    }
}
/**
 * Trust Maturity Model v2
 * Test class for TM_Placement trigger that enforces the integrity of the Environment Type
 * (For each Cat+Level+Pri position, you can have 1 placement with envType BU, or multiple placements with envType 'Parent - whatever')
 **/

@isTest
public class TEST_TM_enforcePlacementIntegrityEnvType {
    
    static String businessUnitLabel = TM_Constants.BU_ENVIRONMENT_TYPE;
    static String parentEnvITLabel = 'Parent - Infrastructure (IT)';
    static String parentEnvProdLabel = 'Parent - Infrastructure (Production)';
    static String parentEnvServiceLabel = 'Parent - Service';


    static TM_Placement__c fillPlacement(String aoId, String category, String level, Integer priority, String envType) {
        TM_Placement__c p = new TM_Placement__c();
        p.TM_AbstractObjective__c = aoId;
        p.Category__c = category;
        p.Level__c = level;
        p.Priority__c = priority;
        p.Environment_Type__c = envType;

        return p;
    }

    static testMethod void testNoExistingPlacements(){

        // ---------------- CREATE TEST DATA 
        TM_AbstractObjective__c ao = TEST_TM_Util.createTMAbstractObjective();

            
        // ---------------- RUN TEST            
        test.startTest();
            
        // create placement
        TM_Placement__c p = new TM_Placement__c();
        p.TM_AbstractObjective__c = ao.Id;
        p.Category__c = 'PRO';
        p.Level__c = '1';
        p.Priority__c = 1;
        p.Environment_Type__c = businessUnitLabel;
        
        insert p;

        test.stopTest();
        
        
        // ---------------- VERIFY EXPECTED BEHAVIOR
        System.assertNotEquals(null, p.Id, 'Test placement should have been inserted.');
    }
    
    
    static testMethod void testConflictNewBUExistingBU(){
        
        // ---------------- CREATE TEST DATA 
        TM_AbstractObjective__c ao = TEST_TM_Util.createTMAbstractObjective();
        TM_Placement__c existingP = TEST_TM_Util.createTMPlacement(ao.Id, 'SIR', '1', 1, businessUnitLabel);

        
        // ---------------- RUN TEST
        test.startTest();
        
        TM_Placement__c newP;
        try {
            newP = TEST_TM_Util.createTMPlacement(ao.Id, 'SIR', '1', 1, businessUnitLabel);
        } catch (DmlException e) {
            // show error becuase you can't create another Placement w BU
        }
        
        test.stopTest();
        
        
        // ---------------- VERIFY EXPECTED BEHAVIOR
        System.assertEquals(null, newP, 'Test placement should not have been inserted.');        
        
    }

    
    static testMethod void testConflictNewBUExistingParentEnv(){
        
        // ---------------- CREATE TEST DATA 
        TM_AbstractObjective__c ao = TEST_TM_Util.createTMAbstractObjective();
        TM_Placement__c existingP = TEST_TM_Util.createTMPlacement(ao.Id, 'SID', '1', 1, parentEnvITLabel);

            
        // ---------------- RUN TEST
        test.startTest();
        
        TM_Placement__c newP;
        try {
            newP = TEST_TM_Util.createTMPlacement(ao.Id, 'SID', '1', 1, businessUnitLabel);
        } catch (DmlException e) {
            // show error becuase you can't create another Placement w BU
        }
        
        test.stopTest();
        
        
        // ---------------- VERIFY EXPECTED BEHAVIOR
        System.assertEquals(null, newP, 'Test placement should not have been inserted.');        
        
    }
    
    
        static testMethod void testConflictNewParentEnvExistingBU(){
        
        // ---------------- CREATE TEST DATA 
        TM_AbstractObjective__c ao = TEST_TM_Util.createTMAbstractObjective();
        TM_Placement__c existingP = TEST_TM_Util.createTMPlacement(ao.Id, 'SIR', '1', 1, businessUnitLabel);

        
        // ---------------- RUN TEST
        test.startTest();
        
        TM_Placement__c newP;
        try {
            newP = TEST_TM_Util.createTMPlacement(ao.Id, 'SIR', '1', 1, parentEnvProdLabel);
        } catch (DmlException e) {
            // show error becuase you can't create another Placement w BU
        }
        
        test.stopTest();
        
        
        // ---------------- VERIFY EXPECTED BEHAVIOR
        System.assertEquals(null, newP, 'Test placement should not have been inserted.');        
        
    }

    
    static testMethod void testNewParentEnvExistingParentEnvs(){
        
        // ---------------- CREATE TEST DATA 
        TM_AbstractObjective__c ao = TEST_TM_Util.createTMAbstractObjective();
        TM_Placement__c existingP = TEST_TM_Util.createTMPlacement(ao.Id, 'SID', '1', 1, parentEnvITLabel);
        TM_Placement__c existingP2 = TEST_TM_Util.createTMPlacement(ao.Id, 'SID', '1', 1, parentEnvServiceLabel);

            
        // ---------------- RUN TEST
        test.startTest();
        
        TM_Placement__c newP = TEST_TM_Util.createTMPlacement(ao.Id, 'SID', '1', 1, parentEnvProdLabel);
        
        test.stopTest();
        
        
        // ---------------- VERIFY EXPECTED BEHAVIOR
        System.assertNotEquals(null, newP.Id, 'Test placement should have been inserted.');     
        
    }
    
    static testMethod void testMultipleNewParentEnvsExistingParentEnvs(){
        
        // ---------------- CREATE TEST DATA 
        TM_AbstractObjective__c ao = TEST_TM_Util.createTMAbstractObjective();
        TM_Placement__c existingP = TEST_TM_Util.createTMPlacement(ao.Id, 'SID', '1', 1, parentEnvITLabel);
        TM_Placement__c existingP2 = TEST_TM_Util.createTMPlacement(ao.Id, 'SID', '1', 1, parentEnvProdLabel);
        TM_Placement__c existingP3 = TEST_TM_Util.createTMPlacement(ao.Id, 'SIR', '2', 2, parentEnvITLabel);
        TM_Placement__c existingP4 = TEST_TM_Util.createTMPlacement(ao.Id, 'SIR', '2', 2, parentEnvProdLabel);
        
            
        // ---------------- RUN TEST
        test.startTest();

        List<TM_Placement__c> newPlacements = new List<TM_Placement__c>();
        TM_Placement__c newP = fillPlacement(ao.Id, 'SID', '1', 1, parentEnvServiceLabel);
        newPlacements.add(newP);
        TM_Placement__c newP2 = fillPlacement(ao.Id, 'SIR', '2', 2, parentEnvServiceLabel);
        newPlacements.add(newP2);
        
        insert newPlacements;

        test.stopTest();

        // ---------------- VERIFY EXPECTED BEHAVIOR
        System.assertNotEquals(null, newP.Id, 'Test placement 1 should have been inserted.'); 
        System.assertNotEquals(null, newP2.Id, 'Test placement 2 should have been inserted.');
  
    }
    
    static testMethod void testConflictMultipleNewParentEnvsExistingParentEnv(){
        
                System.debug(LoggingLevel.ERROR, 'start test');
        
        // ---------------- CREATE TEST DATA 
        TM_AbstractObjective__c ao = TEST_TM_Util.createTMAbstractObjective();
        TM_Placement__c existingP = TEST_TM_Util.createTMPlacement(ao.Id, 'SID', '1', 1, parentEnvITLabel);
        TM_Placement__c existingP2 = TEST_TM_Util.createTMPlacement(ao.Id, 'SID', '1', 1, parentEnvProdLabel);

                System.debug(LoggingLevel.ERROR, 'done w setup');
            
        // ---------------- RUN TEST
        test.startTest();

        List<TM_Placement__c> newPlacements = new List<TM_Placement__c>();
        TM_Placement__c newP = fillPlacement(ao.Id, 'SID', '1', 1, parentEnvServiceLabel);
        newPlacements.add(newP);
        TM_Placement__c newP2 = fillPlacement(ao.Id, 'SIR', '1', 1, parentEnvProdLabel);
        newPlacements.add(newP2);
        
        try {
            insert newPlacements;
        } catch (DmlException e){
            //error thrown because one of the new placements is a duplicate
        }
        test.stopTest();
        
                System.debug(LoggingLevel.ERROR, 'end test before assertion');
        // ---------------- VERIFY EXPECTED BEHAVIOR

        // Note: on a bulk insert, if one record fails, by default all records are rolled back
        System.assertEquals(null, newP.Id, 'Test placement 1 should not have been inserted.'); 
        System.assertEquals(null, newP2.Id, 'Test placement 2 should not have been inserted.');
  
    }
}
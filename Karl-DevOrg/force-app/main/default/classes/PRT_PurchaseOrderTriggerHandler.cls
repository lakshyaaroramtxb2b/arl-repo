/***********************************************************
* Handler class for PRT_PurchaseOrderTrigger
* Created by 	- Prashant Gupta
************************************************************/
public class PRT_PurchaseOrderTriggerHandler {
	/***********************************************************
	 * before Insert method being called from trigger
     * Created by 	- Prashant Gupta
     * Date 		- 09-09-2019
     ************************************************************/
    public static void beforeInsert(List<Purchase_Order__c> newList){
        PRT_Utility.updateProgramLookups(newList,null);
     }
    
	/***********************************************************
	 * before update method being called from trigger
     * Created by 	- Prashant Gupta
     * Date 		- 09-09-2019
     ************************************************************/
    public static void beforeUpdate(List<Purchase_Order__c> newList, Map<id,Purchase_Order__c> oldMap){        
        PRT_Utility.updateProgramLookups(newList,oldMap);
    }
    
	/***********************************************************
	 * after Insert method being called from trigger
     * Created by 	- Prashant Gupta
     * Date 		- 09-09-2019
     ************************************************************/
    public static void afterInsert(List<Purchase_Order__c> newList){
        PRT_PurchaseOrderTriggerHelper.createInternalUserSharing(newList,null);
     }
    
	/***********************************************************
	 * after update method being called from trigger
     * Created by 	- Prashant Gupta
     * Date 		- 09-09-2019
     ************************************************************/
    public static void afterUpdate(List<Purchase_Order__c> newList, Map<id,Purchase_Order__c> oldMap){        
        PRT_PurchaseOrderTriggerHelper.createInternalUserSharing(newList,oldMap);
    }
}
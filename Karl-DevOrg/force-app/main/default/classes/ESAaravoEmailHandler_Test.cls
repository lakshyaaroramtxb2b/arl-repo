@isTest
private class ESAaravoEmailHandler_Test {

    static String testBody;
    static ESA_Case_Queue__c esaQueue;
    static final String QUEUEID = '00G70000001OtrU';
    
    static {
        testBody = 'A task within our Supplier Information Management system has been assigned to you and requires your timely review and approval: '
                 + 'been assigned to you and requires your timely review and approval:\n'
                 + 'SUPPLIER CARE PLUS MEDICINA ASSISTENCIAL LTDA\n'
                 + 'BUSINESS PROCESS Trust Review\n'
                 + 'TASK NAME Trust Review Task - CARE PLUS MEDICINA ASSISTENCIAL LTDA\n'
                 + 'REQUESTER Kevin Villalobos\n'
                 + 'REQUESTER EMAIL nlira@exacttarget.com\n'
                 + 'TAKE ACTION BY OR BEFORE 02/22/2016 \n'
                 + '\n'
                 + '\n'
                 + '\n'
                 + 'Access the system to review and take action by clicking the link below:\n'
                 + '\n'
                 + '\n'
                 + 'ACCESS HERE'
                 + '\n'
                 + 'Please contact vendorrecords@salesforce.com if you have any questions or need assistance. '
                 + '\n'
                 + 'Best regards'
                 + '\n'
                 + 'Supplier Relationship Management Team'
                 + 'Salesforce'
                 + 'This e-mail and any files transmitted with it are confidential and intended solely for the use of the individual or organization to whom they are addressed. Should you not be the intended addressee of this e-mail or his or her representative, please note that review, retransmission, dissemination or other use of or taking action in reliance upon this information is not permissible. Should you have received this e-mail in error, please notify the sender delete the email and the material from any computer. '
                 + 'You received this message because you are subscribed to the Google Groups "trust_esa" group.' 
                 + 'To unsubscribe from this group and stop receiving emails from it, send an email to trust_esa+unsubscribe@salesforce.com.' 
                 + 'To post to this group, send email to trust_esa@salesforce.com.' 
                 + 'To view this discussion on the web visit https://groups.google.com/a/salesforce.com/d/msgid/trust_esa/243174015.12221455903770187.JavaMail.simpletrade%40aravo.com.';
 
                 insert new ESA_Case_Queue__c(Name = 'Trust - Application Security Assurance', Queue_Picklist_Name__c = 'ASA', Queue_ID__c = QUEUEID);
 
    }

    static testMethod void testAravoEmailHandler() {
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        
        email.subject = 'ACTION REQUIRED - CARE PLUS MEDICINA ASSISTENCIAL LTDA';
        email.plainTextBody = testBody;
        env.fromAddress = 'vendorrecords@salesforce.com';
        
        ESAaravoEmailHandler aravoEmail = new ESAaravoEmailHandler();
        aravoEmail.handleInboundEmail(email, env );  
        
        test.startTest();
        
        Id testCaseId = aravoEmail.createCase();
        Case testCase = aravoEmail.sfCase; 
        
        system.assertEquals(testCase.OwnerId, QUEUEID);
        system.assertEquals(testCase.Subject, ESAaravoEmailHandler.SUBJECT + ' - Care Plus Medicina Assistencial Ltda');
        system.assertEquals(testCase.Priority, ESAaravoEmailHandler.PRIORITY);
        system.assertEquals(testCase.RecordTypeId, ESAaravoEmailHandler.SF_SEC_RECORDTYPEID);
        
        Id testCommentId = aravoEmail.createComment(testCaseId);
        aravoEmail.sendEmail(testCaseId);
        
        test.stopTest();
        
    }
}
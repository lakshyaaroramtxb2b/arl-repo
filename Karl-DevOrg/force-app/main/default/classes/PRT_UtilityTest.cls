/*
 * Test Class for PRT_Utility
* Created by - Prashant Gupta (MTX)
*/
@isTest
public class PRT_UtilityTest {
    
    @testSetup
    public static void testSetup(){
        List<User> userList = new List<User>();
        userList = PRT_TestDataFactory.createUsers(3,'System Administrator',true);
    }
    static testMethod void updateProgramTest() {
        List<Program__c> programList = new List<Program__c>(PRT_TestDataFactory.createPrograms(3,true));
        List<Project__c> projectList = new List<Project__c>(PRT_TestDataFactory.createProjects(2,false));
        for(integer i=0; i<2; i++){
            projectList[i].Program__c = programList[i].id;
        }
        insert projectList;
        test.startTest();
        List<Milestone_PRT__c> milestoneList= new List<Milestone_PRT__c>();
        List<Risk__c> riskList= new List<Risk__c>();
        List<Assignment__c> assignmentList= new List<Assignment__c>();
        List<Purchase_Order__c> purchaseOrderList= new List<Purchase_Order__c>();
        
        milestoneList = PRT_TestDataFactory.createMilestone(10,projectList[0].id,true);
        purchaseOrderList = PRT_TestDataFactory.createPurchaseOrder(10,projectList[0].id,true);
        
        projectList[0].Program__c = programList[2].id;
        update projectList;
        for(Purchase_Order__c rec : purchaseOrderList){
            rec.Project__c = projectList[1].id;
        }
        update purchaseOrderList;
        for(Purchase_Order__c rec : [SELECT id,Program__c FROM Purchase_Order__c WHERE id in :purchaseOrderList ]){
            System.assertEquals(rec.Program__c,programList[1].id); 
        }
        test.stopTest();
        
    }
    static testMethod void prepareShareObjectTest(){
        List<User> userList = new List<User>([SELECT Id FROM USER WHERE Email Like 'PRTUser%']);
        List<Program__c> programList = new List<Program__c>(PRT_TestDataFactory.createPrograms(3,true));
        List<Project__c> projectList = new List<Project__c>(PRT_TestDataFactory.createProjects(2,false));
        for(integer i=0; i<2; i++){
            projectList[i].Program__c = programList[i].id;
            projectList[i].Project_Manager__c = userList[0].id;
            projectList[i].Executive_Sponsor_Internal__c = userList[1].id;
            projectList[i].Sponsor_Internal__c = userList[2].id;
        }
        insert projectList;
        test.startTest();
        List<Milestone_PRT__c> milestoneList= new List<Milestone_PRT__c>();
        List<Risk__c> riskList= new List<Risk__c>();
        List<Assignment__c> assignmentList= new List<Assignment__c>();
        List<Purchase_Order__c> purchaseOrderList= new List<Purchase_Order__c>();
        
        milestoneList = PRT_TestDataFactory.createMilestone(10,projectList[0].id,true);
        riskList = PRT_TestDataFactory.createRisk(10,projectList[0].id,true);
        assignmentList = PRT_TestDataFactory.createAssignment(10,projectList[0].id,milestoneList[0].id,true);
        purchaseOrderList = PRT_TestDataFactory.createPurchaseOrder(10,projectList[0].id,true);
        
        projectList[0].Program__c = programList[2].id;
        update projectList;
        for(Assignment__c rec : assignmentList){
            rec.Project__c = projectList[1].id;
        }
        update assignmentList;
        
        for(Purchase_Order__c rec : purchaseOrderList){
            rec.Project__c = projectList[1].id;
        }
        update purchaseOrderList;
        for(Milestone_PRT__c mile : milestoneList){
            mile.Name = 'Update Name test';
            mile.Start_Date__c = date.today()+1;
            mile.Due_Date__c = date.today()+4;
            mile.Completion_Date__c  = date.today()+3;
        }
        update milestoneList;
        List<Purchase_Order__Share> poShareList = new List<Purchase_Order__Share>([SELECT id 
                                                                                   FROM Purchase_Order__Share 
                                                                                   WHERE ParentId in : purchaseOrderList]);
        System.assert(poShareList!=null);
        
        test.stopTest();
        
    }
    static testMethod void checkRequiredFieldsTest() {
        List<Program__c> programList = new List<Program__c>(PRT_TestDataFactory.createPrograms(3,true));
        List<Project__c> projectList = new List<Project__c>(PRT_TestDataFactory.createProjects(2,false));
        for(integer i=0; i<2; i++){
            projectList[i].Program__c = programList[i].id;
        }
        insert projectList;
        test.startTest();
        PRT_Utility.checkRequiredFields(projectList,Schema.SObjectType.Project__c.fieldSets.getMap().get('PRT_Project_Charter_Required_Fields'));
        
        List<Project__c> updatedProjectList = new List<Project__c>([SELECT id 
                                                             FROM Project__c 
                                                             WHERE ID in : projectList]);
        System.assert(updatedProjectList!=null);
        test.stopTest();
        
    }
}
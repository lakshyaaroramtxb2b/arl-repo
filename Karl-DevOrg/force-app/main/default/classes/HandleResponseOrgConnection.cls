/**
 * Created by dgatchell on 6/29/18.
 */

public class HandleResponseOrgConnection {
    /*
    public class ResponseOrgException extends Exception {}
    private AuthToken__c authToken = [SELECT Instance_URL__c, Token__c, Last_Update__c FROM AuthToken__c WHERE Name__c = 'ResponseOrg'];

    public AuthToken__c getResponseOrgToken(){
        AuthToken__c currentToken;
        if(this.authToken.Token__c == null){
			currentToken = this.connect();
        } else if(Datetime.now() > this.authToken.Last_Update__c.addMinutes(30)){
            System.debug('Session likely expired');
            currentToken = this.connect();
        } else {
            currentToken = this.authToken;
        }
        System.debug('Last Update Time of currentToken: ' + currentToken.Last_Update__c);
        return currentToken;
    }

    public AuthToken__c connect(){
        Remote_SAML_Setting__mdt responseOrgSetting = [SELECT Consumer_Key__c, Encoded_Private_Certificate__c, Subject__c FROM Remote_SAML_Setting__mdt WHERE Name__c = 'ResponseOrg'];
        	
        httpResponse response = new SAMLBearerAssertion(responseOrgSetting.Subject__c, responseOrgSetting.Encoded_Private_Certificate__c, responseOrgSetting.Consumer_Key__c).postSAML();

        if(response.getStatusCode() == 200){
            Map<String, Object> auth = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
            AuthToken__c currentToken = this.authToken;
            currentToken.Instance_URL__c = (String) auth.get('instance_url');
            currentToken.Token__c = (String) auth.get('access_token');
            currentToken.Last_Update__c = datetime.now();

            update currentToken;
            return currentToken;
        } else {
            throw new ResponseOrgException('ResponseOrg did not provide a token: ' + response.getStatusCode() + ' ' + response.getStatus());
        }
	

    }
    */
}
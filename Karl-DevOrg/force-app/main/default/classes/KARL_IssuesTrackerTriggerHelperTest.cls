/**
* @author Ben Harvie
* @email ben.harvie@salesforce.com
* @description This is a test class for KARL_FollowUpReportController, used for the Follow-up
* LWC component.
*/
@isTest
public with sharing class KARL_IssuesTrackerTriggerHelperTest {
/**
    * @author Ben Harvie
    * @email ben.harvie@salesforce.com
    * @description Test data setup.
    */
	@testSetup
    private static void testSetup(){
        /**
         * User to generate the test records
         */
        UserRole userrole = [SELECT Id, DeveloperName FROM UserRole WHERE DeveloperName = 'CEO' LIMIT 1];

        User adminUser = [SELECT Id, UserRoleId FROM User 
                            WHERE Profile.Name='System Administrator' AND isActive = true LIMIT 1];
        
        adminUser.UserRoleId = userRole.Id;
        update adminUser;

        System.runAs(adminUser){
            /**
             * Audit Cycle Setup
             */
            // Create shell audit cycle
            Audit_Cycle__c auditCycle = ARL_TestDataFactory.createAuditCycle();
            insert auditCycle;

            /**
             * Community User Setup
             */
            Account acc = new Account(Name = 'salesforce.com - ESA Office Hours');
            insert acc;
            
            Contact con = new Contact(LastName = 'test', AccountId = acc.Id, Email = 'test@gmail.com');
            insert con;

            ARL_TestDataFactory.createCommunityUserwithSpecificCon(con.Id, 'a12345@bqww.com', true);

            /**
             * Approver Creation
             */
            User u = ARL_TestDataFactory.createOpsAdmin();
            KARL_Issue_Tracker_Approvers__c issueApprover = ARL_TestDataFactory.createAuditIssueApprover(u.Id);
            insert issueApprover;

            /**
             * Issue Creation
             */
            KARL_Issue_Tracker__c issue = ARL_TestDataFactory.createAuditIssue(auditCycle.Id);
            insert issue;


        }
    }
    
    /**
    * @author Ben Harvie
    * @email ben.harvie@salesforce.com
    * @description This method tests the generation of the ARL Reports using the ARLReportController
    */
    @isTest
    private static void issueVersion(){
        User u = ARL_TestDataFactory.createAuditManager();
        Audit_Cycle__c auditCycle = [SELECT id FROM Audit_Cycle__c LIMIT 1];

        // Run the test as the SCEA Audit Manager
        System.runAs(u) {
            Test.startTest();
                KARL_Issue_Tracker__c issueItem = [SELECT Id FROM KARL_Issue_Tracker__c LIMIT 1];

                KARL_Issue_Tracker__c issueUpdate = new KARL_Issue_Tracker__c();
                issueUpdate.id = issueItem.Id;
                issueItem.KARL_KAI_Issue_Description__c = 'Updated description!';

                update issueItem;

                List<KARL_Issue_Tracker_Version__c> issueVersions = [SELECT Id FROM KARL_Issue_Tracker_Version__c];
                system.assertEquals(2, issueVersions.size(), 'Incorrect number of versions'); // Expecting 2x version record

            Test.stopTest();
        }
    }
}
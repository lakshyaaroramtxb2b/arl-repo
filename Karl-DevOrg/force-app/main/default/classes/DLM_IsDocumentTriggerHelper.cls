/***********************************************************
* Helper class for DLM_IsDocumentTriggerHelper
* Created by    - Swarnima Singh Mandhata
* Date      - 02 July 2020
************************************************************/
public class DLM_IsDocumentTriggerHelper {
    @testVisible
    private static list<GusUser__x> mockallGusUserList = new list<GusUser__x>();
    @testVisible
    Private static List<ADM_Work_c__x> maclAllGusAdmWorkList = new List<ADM_Work_c__x>();
    //Set product tag to Implementation Solutions
    public static void setProductTag(List<IS_Document__c> newList, Map<id,IS_Document__c> oldMap){
        Set<Id> isDocumentIdSet = new Set<Id>();
        for(IS_Document__c isDocumentRecord : newList){
            if((oldMap != NULL && isDocumentRecord.Product_Tag__c == NULL) || ( isDocumentRecord.Product_Tag__c == NULL )) {
                isDocumentIdSet.add(isDocumentRecord.Id);
            }
        }
        if(!isDocumentIdSet.isEmpty()){
            DLM_IsDocumentTriggerHelper.populateProductTag(isDocumentIdSet);
        }
    }
    
    @future
    public static void populateProductTag(Set<Id> isDocumentIdSet){
        String productTagId;
        List<IS_Document__c>  isDocumentList = new List<IS_Document__c>();
        for(ADM_Product_Tag_c__x productTagRec: [SELECT Id, ExternalId,Name__c 
                                                 FROM ADM_Product_Tag_c__x
                                                 WHERE Name__c =: Label.DLM_Product_Tag]){
                                                     productTagId = productTagRec.ExternalId;
                                                 }
        for(IS_Document__c docRec: [SELECT id,Product_Tag__c
                                    FROM IS_Document__c
                                    WHERE id In: isDocumentIdSet ]){
                                        docRec.Product_Tag__c = productTagId != NULL ?  productTagId : productTagId != NULL ? productTagId : NULL;
                                        isDocumentList.add(docRec);
                                    }
        if( !isDocumentList.isEmpty() ){
            update isDocumentList;
        }
    }
    
    public static void createWorkRecord_OnWorkflowRule(List<IS_Document__c> newList, Map<id,IS_Document__c> oldMap){
        Set<Id> isDocumentIdSet = new  Set<Id>();
        for(IS_Document__c isDocumentRecord : newList){
            if( isDocumentRecord.EmailTemplateName__c != NULL && oldMap.get(isDocumentRecord.Id).EmailTemplateName__c != isDocumentRecord.EmailTemplateName__c){ 
                isDocumentIdSet.add(isDocumentRecord.Id);
            }
        }
        if( !isDocumentIdSet.isEmpty() ){
            DLM_IsDocumentTriggerHelper.createWorkGusRecord(isDocumentIdSet);
        }
    }
    
    @future
    public static void createWorkGusRecord(Set<Id> isDocumentIdSet){
        List<String> emailTemplateNameList = new List<String>();
        Map<Id,String> isDocumentToUserId = new Map<Id,String>();
        Map<Id,User> contactIdToUserIdMap = new Map<Id,User>();
        Map<Id,String> contactIdEmployeeNumber = new Map<Id,String>();
        Map<IS_Document__c,List<String>> isDocumentRecordToEmailTemplateNameMap = new Map<IS_Document__c,List<String>>();
        Map<String,EmailTemplate> templateNameToEmailTemplateMap = new Map<String,EmailTemplate>();
        Map<String, Id> userEmpNumToGusIdMap = new Map<String, Id>();
        Map<String,Id> userEmpIdToContactGusIdMap = new Map<String, id>();
        String DLM_Gus_User_RECORDTYPEID = Label.GUS_Work_User_Story_RecordType_Id;
        String productTagId;
        List<IS_Document__c> isDocumentUpdateList = new List<IS_Document__c>(); 
        List<ADM_Work_c__x> gusWorkList = new List<ADM_Work_c__x>();
        
        for(ADM_Product_Tag_c__x productTagRec: [SELECT Id, ExternalId,Name__c 
                                                 FROM ADM_Product_Tag_c__x
                                                 WHERE Name__c =: Label.DLM_Product_Tag]){
                                                     productTagId = productTagRec.ExternalId;
                                                 }
        
        for(IS_Document__c isDocumentRecord: [SELECT Id,Name,Content_Owner__c,Content_Owner__r.Name,Content_Custodian__c,Content_Custodian__r.Name,Previous_Content_Owner__c,Previous_Custodian_Owner__c,Product_Tag__c,Respond_Due_Date_7_Days__c,
                                              Next_Review_Date__c,EmailTemplateName__c,workFlow_Active__c,OwnerId,Owner.Name,Content_Owner__r.Employee_ID__c
                                              FROM IS_Document__c
                                              WHERE ID IN:isDocumentIdSet]){              
                                                  if(isDocumentRecord.Content_Owner__r.Employee_ID__c != null){
                                                      
                                                      isDocumentToUserId.put(isDocumentRecord.Content_Owner__c,isDocumentRecord.Content_Owner__r.Employee_ID__c);
                                                  }
                                                  System.debug('isDocumentToUserId--> '+ isDocumentToUserId);
                                                  
                                                  if(!isDocumentRecordToEmailTemplateNameMap.containsKey(isDocumentRecord)){
                                                      isDocumentRecordToEmailTemplateNameMap.put(isDocumentRecord,new List<String>());
                                                  }
                                                  isDocumentRecordToEmailTemplateNameMap.get(isDocumentRecord).addAll(isDocumentRecord.EmailTemplateName__c.split(','));
                                                  
                                                  emailTemplateNameList.addAll(isDocumentRecord.EmailTemplateName__c.split(','));
                                              }
        System.debug('emailTemplateNameList-> '+ emailTemplateNameList);
        
        for(EmailTemplate emailTempRec: [SELECT Id, Name, DeveloperName, BrandTemplateId, Subject, Description, Body, FolderName,HTMLValue
                                         FROM EmailTemplate where Name IN : emailTemplateNameList]){
                                             templateNameToEmailTemplateMap.put(emailTempRec.Name,emailTempRec); 
                                             
                                         }
        System.debug('templateNameToEmailTemplateMap-> '+ templateNameToEmailTemplateMap);
        
                            /*for(User userRec: [SELECt ID, ContactId ,EmployeeNumber
                    FROM user 
                    WHERE EmployeeNumber IN : isDocumentToUserId.values() 
                    AND isActive = true]){
                    if(!contactIdToUserIdMap.containsKey(userRec.ContactId)){
                    contactIdToUserIdMap.put(userRec.ContactId,userRec);
                    }
                    if(contactIdEmployeeNumber.containsKey(userRec.Id)){
                    contactIdEmployeeNumber.put(userRec.Id,userRec.EmployeeNumber);
                    }
                    }*/
        
        for(GusUser__x gusUser : [SELECT Id, ExternalId, EmployeeNumber__c,ContactId__c 
                                  FROM GusUser__x 
                                  WHERE EmployeeNumber__c IN: isDocumentToUserId.Values()]) {
                                      if(!test.isRunningTest()){
                                          userEmpNumToGusIdMap.put(gusUser.EmployeeNumber__c, gusUser.ExternalId);
                                      }else{
                                          userEmpNumToGusIdMap.put(mockallGusUserList[0].EmployeeNumber__c, mockallGusUserList[0].ExternalId);
                                      }
                                      
                                      /*if(gusUser.ContactId__c != null){
                                        userEmpIdToContactGusIdMap.put(gusUser.EmployeeNumber__c,gusUser.ExternalId);
                                        }*/
                                      
                                  }
        
        for(IS_Document__c isDocumentRecord : isDocumentRecordToEmailTemplateNameMap.keySet()){
            for(String emailTemplateName : isDocumentRecordToEmailTemplateNameMap.get(isDocumentRecord)){
                ADM_Work_c__x gusWork = new ADM_Work_c__x();
                System.debug('DLM_Gus_User_RECORDTYPEID'+DLM_Gus_User_RECORDTYPEID);
                if(DLM_Gus_User_RECORDTYPEID != NULL ){                         //Map recordType ID
                    gusWork.RecordTypeId__c = DLM_Gus_User_RECORDTYPEID;
                }                
                if(isDocumentRecord.Product_Tag__c == NULL || isDocumentRecord.Content_Owner__c == NULL || isDocumentRecord.Content_Owner__r.Employee_ID__c == 'CTRCT'){
                    gusWork.Product_Tag_c__c = productTagId != NULL ? productTagId : NULL; //Map productTag
                }else{
                    gusWork.Product_Tag_c__c  = isDocumentRecord.Product_Tag__c ;
                }
                
                System.debug('gusWork.Product_Tag_c__c - '+ gusWork.Product_Tag_c__c);
                
                if(templateNameToEmailTemplateMap.containsKey(emailTemplateName)){          //Map Subject
                    gusWork.Subject_c__c = templateNameToEmailTemplateMap.get(emailTemplateName).Subject != null ? templateNameToEmailTemplateMap.get(emailTemplateName).Subject + ' - ' + isDocumentRecord.Name : 'No subject';
                    String gusSubject = gusWork.Subject_c__c;
                    if(gusSubject.length()>255){
                        gusSubject = gusSubject.substring(0, 255);
                        gusWork.Subject_c__c = gusSubject; 
                    }
                }
                System.debug('gusWork.Subject_c__c -> '+ gusWork.Subject_c__c);
                System.debug('templateNameToEmailTemplateMap- > '+ templateNameToEmailTemplateMap);
                System.debug('emailTemplateName->'+ emailTemplateName);
                
                if(templateNameToEmailTemplateMap.containsKey(emailTemplateName)){      //Map Description & Merge fields
                    String bodyDescription = templateNameToEmailTemplateMap.get(emailTemplateName).HTMLValue == null ?'': templateNameToEmailTemplateMap.get(emailTemplateName).HTMLValue.replaceAll('\\<.*?>',' ').trim().replaceAll('\\s+',' ');
                    if(bodyDescription != null && bodyDescription != ''){
                        if(bodyDescription.contains('{!IS_Document__c.Content_Custodian__c}')){
                            bodyDescription = bodyDescription.replace('{!IS_Document__c.Content_Custodian__c}', String.valueOf(isDocumentRecord.Content_Custodian__r.Name));
                        }
                        
                        if(bodyDescription.contains('{!IS_Document__c.Content_Owner__c}')){
                            bodyDescription = bodyDescription.replace('{!IS_Document__c.Content_Owner__c}', String.valueOf(isDocumentRecord.Content_Owner__r.name));
                        }
                        if(bodyDescription.contains('{!IS_Document__c.Previous_Content_Owner__c}')){
                            if(isDocumentRecord.Previous_Content_Owner__c != null && isDocumentRecord.Previous_Content_Owner__c != ''){
                                bodyDescription = bodyDescription.replace('{!IS_Document__c.Content_Owner__c}', isDocumentRecord.Previous_Content_Owner__c);
                            }  
                        }
                        if(bodyDescription.contains('{!IS_Document__c.Previous_Custodian_Owner__c}')){
                            if(isDocumentRecord.Previous_Custodian_Owner__c != null && isDocumentRecord.Previous_Custodian_Owner__c != ''){
                                bodyDescription = bodyDescription.replace('{!IS_Document__c.Previous_Custodian_Owner__c}', isDocumentRecord.Previous_Custodian_Owner__c);
                            }
                            
                        }
                        if(bodyDescription.contains('{!IS_Document__c.Name}')){
                            bodyDescription = bodyDescription.replace('{!IS_Document__c.Name}', isDocumentRecord.Name);
                        }
                        if(bodyDescription.contains('{!IS_Document__c.Respond_Due_Date_7_Days__c}')){
                            bodyDescription = bodyDescription.replace('{!IS_Document__c.Respond_Due_Date_7_Days__c}', String.valueOf(isDocumentRecord.Respond_Due_Date_7_Days__c));
                        }
                        if(bodyDescription.contains('{!IS_Document__c.OwnerFullName}')){
                            bodyDescription = bodyDescription.replace('{!IS_Document__c.OwnerFullName}', String.valueOf(isDocumentRecord.Owner.Name));
                        }
                        //gusWork.Details_c__c = bodyDescription;
                    }
                    gusWork.Details_c__c = bodyDescription;
                    
                }
                
                if( isDocumentToUserId.containsKey(isDocumentRecord.Content_Owner__c) &&
                   userEmpNumToGusIdMap.containsKey( isDocumentToUserId.get(isDocumentRecord.Content_Owner__c)) ){  // Map Assignee to from custodian owner
                      if(isDocumentToUserId.get(isDocumentRecord.Content_Owner__c) != 'CTRCT')    {
                       gusWork.Assignee_c__c = userEmpNumToGusIdMap.get( isDocumentToUserId.get(isDocumentRecord.Content_Owner__c) );
                      }
                   }
                
                if( isDocumentRecord.Next_Review_Date__c != NULL ){         //Map due date
                    gusWork.Due_Date_c__c = isDocumentRecord.Next_Review_Date__c;
                }
                gusWorkList.add(gusWork);  
            }
            isDocumentRecord.EmailTemplateName__c = NULL;
            isDocumentUpdateList.add(isDocumentRecord);
        }
        
        if( !gusWorkList.isEmpty() ) {
            System.debug('gusWorkList -> '+ gusWorkList);
            if(!test.isRunningTest()){
                List<Database.SaveResult> result = Database.insertImmediate(gusWorkList);
                System.debug('result : '+result);
            }     
        }
        
        if( !isDocumentUpdateList.isEmpty() ){
            System.debug('isDocumentUpdateList : '+isDocumentUpdateList);
            update isDocumentUpdateList;
        }
    }
}
// appointment dates
public class ESA_AppointmentDate {
    
    @AuraEnabled
    public String prettyDate {get; private set;}
    @AuraEnabled
    public String prettyTime {get; private set;}
    @AuraEnabled
    public String uglyDateTime {get; private set;}
    @AuraEnabled
    public DateTime apptDate {get; private set;}
    @AuraEnabled
    public Boolean taken {get; set;}
    @AuraEnabled
    public String myEventId {get; set;}
    
    private static final Map<String, String> ZONES = new Map<String, String>{'EST' => 'America/New_York',
                                                                             'PST' => 'America/Los_Angeles'};
    
    // constructors
    public ESA_AppointmentDate(DateTime apptDate) {
        this.prettyDate = prettyDate(apptDate);
        this.prettyTime = prettyTime(apptDate);
        this.uglyDateTime = string.valueOfGMT(apptDate);
        this.apptDate = apptDate;
        this.taken = false;                     
    }

    public ESA_AppointmentDate(DateTime apptDate, String zone) {
        this.prettyDate = prettyDate(apptDate, zone);
        this.prettyTime = prettyTime(apptDate, zone);
        this.uglyDateTime = string.valueOfGMT(apptDate);
        this.apptDate = apptDate;
        this.taken = false;                     
    }

    public static String prettyDateTime(DateTime uglyDate, String zone) {
        return String.format('{0} {1}', 
                                new List<String>{
                                    prettyDate(uglyDate, zone), 
                                    prettyTime(uglyDate, zone, false)
                                });
    }
        
    public static String prettyDate(DateTime uglyDate) {
        return uglyDate.format('EEE, MMM dd');
    }
        
    public static String prettyDate(DateTime uglyDate, String zone) {
        return uglyDate.format('EEE, MMM dd', zone(zone));
    }
    
    public static String prettyDate(Date uglyDate) {
        return prettyDate(DateTime.newInstance(uglyDate, Time.newInstance(0,0,0,0)) );
    }    
    
    public static String prettyTime(DateTime uglyTime) {
        return uglyTime.format('h:mm a (z)');
    }

    public static String prettyTime(DateTime uglyTime, String zone) {
        return prettyTime(uglyTime, zone, true);
    }
    
    private static String prettyTime(DateTime uglyTime, String zone, Boolean includeDaysBetween) {
        Integer daysBetween = dateForZone(uglyTime, 'PST').daysBetween(dateForZone(uglyTime, zone)); 
        String daysAdjustment = '';
        if (includeDaysBetween) {
            if (daysBetween > 0) {
                daysAdjustment = ' +' + String.valueOf(daysBetween) + ' day';
            } else If (daysBetween < 0) {
                daysAdjustment = ' -' + string.valueOf(daysBetween) + ' day';
            }
        }
        string prettyTime = uglyTime.format('h:mm a (z)', zone(zone)).replace(zone(zone), (zone(zone) + daysAdjustment));      
        return prettyTime;
    }
    
    private static String zone(String zone) {
        return (ZONES.get(zone) == null)? zone : ZONES.get(zone);
    }
    
    private static Date dateForZone(DateTime input, String zone) {
        String dateStr = input.format('Y-M-d', zone(zone));
        List<String> datecomps = dateStr.split('-');
        Integer y = Integer.valueOf(datecomps[0]);
        Integer m = Integer.valueOf(datecomps[1]);
        Integer d = Integer.valueOf(datecomps[2]);
        return date.newInstance(y, m, d);
    }
    
}
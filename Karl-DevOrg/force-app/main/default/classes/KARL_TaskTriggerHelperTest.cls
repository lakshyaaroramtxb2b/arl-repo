/**
* @author Lakshya Arora
* @email lakshya.arora@mtxb2b.com
* @description Test class for KARL_TaskTriggerHelper
*/
@isTest
public class KARL_TaskTriggerHelperTest {
    /**
* @author Lakshya Arora
* @email lakshya.arora@mtxb2b.com
* @desc method to create the test data.
*/   
    @testSetup
    private static void makeData(){
        User opsAdminUser = ARL_TestDataFactory.createOpsAdmin();
        System.runAs(opsAdminUser){
            Audit_Cycle__c auditCycle = ARL_TestDataFactory.createAuditCycle();
            insert auditCycle;
            KARL_Audit_Scope_Master_Data__c auditScope = ARL_TestDataFactory.createAuditScope();
            insert auditScope;
            KARL_Audit_Firm__c auditFirm = ARL_TestDataFactory.createAuditFirm('Test Audit Firm');
            insert auditFirm;
            KARL_Audit_Scope_Reports__c auditScopeReport = ARL_TestDataFactory.createAuditScopeReport(auditScope.Id,auditFirm.Id);
            insert auditScopeReport;
            KARL_Cycle_Audit_Report_Items__c cycleAuditReportObj = new KARL_Cycle_Audit_Report_Items__c();
            cycleAuditReportObj.Audit_Reports_Master_Data__c = auditScopeReport.Id;
            cycleAuditReportObj.Auditor_Readiness_Confirmation__c = true;
            cycleAuditReportObj.Management_Responses_Approved__c = true;
            cycleAuditReportObj.Audit_Report_Final_Draft_Confirmed__c = true ;
            cycleAuditReportObj.Approved_by_Legal__c = true;
            cycleAuditReportObj.Audit_Letter_Temps_Formatted__c = true;
            cycleAuditReportObj.Basis_of_Assertion_BOA__c = true;
            insert cycleAuditReportObj;
            Task cycleAuditReportTask = new Task();
            cycleAuditReportTask.OwnerId = UserInfo.getuserId(); //owner of task
            cycleAuditReportTask.Subject = 'Readiness Deactivated';
            cycleAuditReportTask.Description = 'Please review Audit Report';                
            cycleAuditReportTask.Status = KARL_Constants.TASK_STATUS_OPEN;
            cycleAuditReportTask.Priority = 'Normal';
            cycleAuditReportTask.WhatId = cycleAuditReportObj.Id;
            cycleAuditReportTask.ActivityDate = System.today();
            insert cycleAuditReportTask;
        }
    }
    
    /**
* @author Lakshya Arora
* @email lakshya.arora@mtxb2b.com
* @desc method to test the markRelatedCycleAuditReadyToSign functionality.
*/
    @isTest
    private static void markRelatedCycleAuditReadyToSignTest(){
        List<User> opsAdminUserList = ARL_TestDataFactory.fetchUser('abc@b.com.d');
        if(!opsAdminUserList.isEmpty()){
            System.runAs(opsAdminUserList[0]){
                List<Task> taskList = [SELECT Id,WhatId,Status FROM Task ];
                test.startTest();
                taskList[0].Status = KARL_Constants.TASK_STATUS_COMPLETED;
                update taskList;
                test.stopTest();
            }
        }
    }
    
}
@isTest
public class SBE_SLAExtensionToCaseTest {
    testmethod static void testEnrollment(){
        Account acct = new Account();
        acct.Name = 'salesforce.com - ESA Office Hours';
        insert acct;
        
        Contact contact = new Contact();
        contact.LastName = 'Test'; 
        contact.Employee_ID__c = 'MTX101';
        contact.AccountId = Label.ESA_Office_Hours_Account_ID;
        INSERT contact;
        
        ADM_Work_c__x work = new ADM_Work_c__x();
        work.Subject_c__c = 'Test Subject';
        work.ExternalId= 'EX101';
        work.Status_c__c = IEM_Constants.GUS_WORK_STATUS_IN_PROGRESS;
        SBE_SLAExtensionToCase.mockGusWorkMap.put('EX101', work);
        
        Case newcase = new Case();
        newcase.RecordTypeId = IEM_Constants.IEM_CASERECORDTYPEID;
        newcase.Cloud__c = 'EntSecTools';
        newcase.Description = 'Test';
        newcase.Team_Responsible__c = 'TRUST Enterprise Security Tools Development';
        newcase.Subject = 'Test Subject';
        newcase.IEM_Case_Type__c = 'Extension';
        INSERT newcase;
        
             
        TM_Security_Work_SLA_Extension_c__x workSLA = new TM_Security_Work_SLA_Extension_c__x();
        workSLA.ExternalId= 'EX101';
        workSLA.TM_Definition_of_Done_c__c= 'Completed';
        workSLA.Describe_Technical_Work_to_Remediate__c= 'Nothing';
        workSLA.Due_Date_Justification__c= 'No';
        workSLA.Technical_Description_of_Issue__c= 'No';
        workSLA.TM_SLA_Extension_Status_c__c = IEM_Constants.SBE_STATUS_IEM_ANALYST;
        workSLA.TM_Corresponding_Work_c__c = 'EX101';
        
        
        SBE_SLAExtensionToCase.mockallSBEList.add(workSLA);
        SBE_SLAExtensionToCase.mockGUSUserEmpMap.put('MTX101', 'EX101');
      
        Test.startTest();
        SBE_SLAExtensionToCase IEMGus = new SBE_SLAExtensionToCase();
        Database.executeBatch(IEMGus);
        Test.stopTest();
        List<Case> caseList = [Select Id from Case];
        System.assertEquals(2, caseList.size());
        List<Case_Work__c> caseWorkList = [Select Id from Case_Work__c Where Case__c = : caseList[0].Id];
        System.assertEquals(0, caseWorkList.size());
          
    }
}
public class ESA_Request_Collaborator_Controller {
    public static void sendCollabaratorNotifacation(List<ESA_Request_Collaborators__c> lstEsaCollaborators) {
        Map<String,String> mapCollabsEmail = new Map<String,String>();
        Map<String,String> mapCollabsDescri = new Map<String,String>();
        List<String> lstToEmails = new List<String>();
        Id securityRequestId = null;
        
        Set<Id> contactIds = new Set<Id>();

        for(ESA_Request_Collaborators__c collab : lstEsaCollaborators) {
            mapCollabsEmail.put(collab.Collaborator_Email__c,collab.Collaborator_Access__c);
            mapCollabsDescri.put(collab.Collaborator_Email__c,collab.Description__c);
            securityRequestId = collab.ESA_Security_Request__c;
            lstToEmails.add(collab.Collaborator_Email__c);
        }

        ESA_Security_Request__c securityRequest = [SELECT Id,Hash_Key__c,Entity_Code__c,
                                                   Service_Item_Name__c, Email__c,
                                                   Service_Item_Category__c, Requestor__r.Name
                                                   FROM ESA_Security_Request__c
                                                   WHERE Id=:securityRequestId];
        
        String entityName = [SELECT Name FROM ESA_Entity__c 
                             WHERE EntityCode__c =: securityRequest.Entity_Code__c].Name;

        
        ESA_Request_Collaborators__c collabUser = [SELECT Id,ESA_Security_Request__r.Service_Item_Name__c,
                                                   ESA_Security_Request__r.Service_Item_Category__c,Description__c
                                                   FROM ESA_Request_Collaborators__c
                                                   WHERE ID =: lstEsaCollaborators[0].Id];



        
       /* List<Contact> lstContacts = [SELECT Id From Contact WHERE Email IN : mapCollabsEmail.KeySet()];
        for(Contact c : lstContacts) {
            contactIds.add(c.Id);
        }*/

        String recordUrl = generateRequestURL(securityRequest);

        EmailTemplate emailTemplate = [SELECT Id, Subject, HtmlValue, Body FROM EmailTemplate 
                                       WHERE DeveloperName = 'ESA_Collaborator'];

        // process the merge fields
        String subject = emailTemplate.Subject;
        String htmlBody = emailTemplate.HtmlValue;
        String plainBody = emailTemplate.Body;

        if(securityRequest.Service_Item_Name__c != null){
            subject = subject.replace('{!serviceItemName}', securityRequest.Service_Item_Name__c);
            htmlBody = htmlBody.replace('{!serviceItemName}', securityRequest.Service_Item_Name__c);
            plainBody = plainBody.replace('{!serviceItemName}', securityRequest.Service_Item_Name__c);
        }
        
        if(securityRequest.Service_Item_Category__c != null) {
            htmlBody = htmlBody.replace('{!categoryName}', securityRequest.Service_Item_Category__c);
            plainBody = plainBody.replace('{!categoryName}', securityRequest.Service_Item_Category__c);
        }

        htmlBody = htmlBody.replace('{!ownerName}', securityRequest.Requestor__r.Name);
        plainBody = plainBody.replace('{!ownerName}', securityRequest.Requestor__r.Name);

        htmlBody = htmlBody.replace('{!ownerEmail}', securityRequest.Email__c);
        plainBody = plainBody.replace('{!ownerEmail}', securityRequest.Email__c);

        htmlBody = htmlBody.replace('{!entityName}', entityName);
        plainBody = plainBody.replace('{!entityName}', entityName);


        if(collabUser.Description__c == null || collabUser.Description__c == '') {
          collabUser.Description__c = 'Intake Request - Sharing';
        }

        htmlBody = htmlBody.replace('{!descriptionValue}', collabUser.Description__c);  
        plainBody = plainBody.replace('{!descriptionValue}', collabUser.Description__c);
        
        if(recordUrl !=null) {
            htmlBody = htmlBody.replace('{!intakeRequestUrl}', recordUrl);
            plainBody = plainBody.replace('{!intakeRequestUrl}', recordUrl);
        } else {
            lstEsaCollaborators[0].addError('Please Reach out admin - Request Url is not available.');
        }

        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();

        email.setReplyTo(UserInfo.getUserEmail());
        email.setSenderDisplayName(UserInfo.getName());
        email.setSubject(subject);
        email.setHtmlBody(htmlBody);
        email.setPlainTextBody(plainBody);
        email.setToAddresses(lstToEmails);
        Messaging.sendEmail(new Messaging.SingleEmailmessage[] {email});

        /*List<Messaging.SingleEmailmessage> lstEmailMessages = new List<Messaging.SingleEmailmessage>();


        for(String emailId : mapCollabsEmail.KeySet()) {
            email.setTa(cId);
            lstEmailMessages.add(email);
        }
        for(Id cId : contactIds) {
            email.setTargetObjectId(cId);
            lstEmailMessages.add(email);
        }

        if(lstEmailMessages.size() > 0) {
            Messaging.sendEmail(lstEmailMessages);
        } */
    }

    /**
     * Generate Security Request URl.
     */

    public static String generateRequestURL(ESA_Security_Request__c theSecurityRequest) {
        String requestUrl = null;
        
        Organization orgInfo = [SELECT IsSandbox FROM Organization LIMIT 1];
        if (theSecurityRequest.Hash_Key__c != null) {
            String siteUrl = URL.getSalesforceBaseUrl().toExternalForm()
                             + ((site.getSiteId() == null)? '/apex/ESA_App' : site.getPathPrefix());

            //if(siteUrl.containsIgnoreCase('ESA_App') == false) {
            //   siteUrl = siteUrl + '/ESA_App';  
           // }

            requestUrl = siteUrl
                         + '?Id='
                         + theSecurityRequest.Hash_Key__c +
                         + '&entityCode=' + theSecurityRequest.Entity_Code__c;

            /*requestUrl = URL.getSalesforceBaseUrl().toExternalForm()
                             + ((site.getSiteId() == null)? '/apex/ESA_App' : site.getPathPrefix())
                             + '?Id='
                             + theSecurityRequest.Hash_Key__c +
                             + '&entityCode=' + theSecurityRequest.Entity_Code__c; */
            if(orgInfo.IsSandbox) {
              //  requestUrl = 'https://suresh-securityorg.cs68.force.com/ESA?Id='
               //          + theSecurityRequest.Hash_Key__c +
               //          + '&entityCode=' + theSecurityRequest.Entity_Code__c;
            } else {
                //Comment this code once Collabartors feature is enabled through the community and 
                // Uncomment the bottom code.
               /* requestUrl = 'https://security--lightning--c.cs77.visual.force.com/apex/ESA_App?Id='
                         + theSecurityRequest.Hash_Key__c +
                         + '&entityCode=' + theSecurityRequest.Entity_Code__c; */
               /* requestUrl = URL.getSalesforceBaseUrl().toExternalForm()
                             + ((site.getSiteId() == null)? '/apex/ESA_App' : site.getPathPrefix())
                             + '?Id='
                             + theSecurityRequest.Hash_Key__c +
                             + '&entityCode=' + theSecurityRequest.Entity_Code__c;*/
                /* requestUrl = URL.getSalesforceBaseUrl().toExternalForm()
                        + ((site.getSiteId() == null)? '/ESA' : site.getPathPrefix())
                        + '?Id='
                        + theSecurityRequest.Hash_Key__c +
                        + '&entityCode=' + theSecurityRequest.Entity_Code__c; */
            }
        }
        return requestUrl;
    }

}
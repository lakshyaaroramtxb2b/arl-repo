/*created batch class for mapping GUS portfolio external Record to security Org portfolio
* Created By - Prashant Gupta
* Modified By - Swarnima S Mandhata Line-30 added filter criteria on 05/05/2020. 
-* date - 17-01-2020
* 
*/
global class PRT_ProgramIntegrationBatch implements Database.Batchable<sObject>,Schedulable {
    @TestVisible
    private static list<PPM_Program_c__x> mockallGusProjectList = new list<PPM_Program_c__x>();
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        System.debug('>' + System.now().addMinutes(-20));
        System.debug('>'+ [SELECT Id,ExternalId FROM PPM_Program_c__x Where LastModifiedDate__c > :System.now().addHours(-20)]);
        if(Test.isRunningTest()){
            return Database.getQueryLocator([SELECT Id FROM User LIMIT 1]);    
        }else{
            String portfolioExternalId = System.Label.PRT_FY_21_Security_GRC;

            String query = 'SELECT Id, Name__c, ExternalId,Program_Goals_c__c,Program_Health_c__c,Program_Health_Color_c__c,Program_Health_Comments_c__c, Program_Manager_c__c,Program_Owner_c__c,Program_Summary_c__c,Executive_Sponsor_c__c,Portfolio_c__c,LastModifiedDate__c FROM PPM_Program_c__x ';
            query+= ' WHERE LastModifiedDate__c >=LAST_N_DAYS:1';
            query +=  ' AND Portfolio_c__c =\'' + portfolioExternalId +'\'';

            return Database.getQueryLocator(query); 
            
        }
    }
    global void execute(Database.BatchableContext bc, List<sObject> gusProgramRecordList){  
        System.debug('>>>>> ' + gusProgramRecordList);
        if(Test.isRunningTest()){
            gusProgramRecordList = mockallGusProjectList;
        }
        List<Program__c> programList = new List<Program__c>();
        Datetime fiveMinsInterval = Datetime.now().addMinutes(-5);
        if(!gusProgramRecordList.isEmpty()){
            Set<Id> externalProgramIdSet = new Set<Id>();
            Set<Id> gusRecUserIdSet = new Set<Id>();
            Set<Id> gusRecPortfolioIdSet = new Set<Id>();
            Map<id,String> externalID_Vs_EmployeeNumber = new Map<id,String>();
            Map<String,Id> employeeNumber_Vs_InternalID = new Map<String,Id>();
            for(PPM_Program_c__x gusProgram :(List<PPM_Program_c__x>)gusProgramRecordList){
                // Need to change the field names here
                if(gusProgram.LastModifiedDate__c >=fiveMinsInterval || Test.isRunningTest()){
                    externalProgramIdSet.add(gusProgram.ExternalId);
                    if(gusProgram.Program_Owner_c__c!=null){
                        gusRecUserIdSet.add(gusProgram.Program_Owner_c__c);
                    }
                    if(gusProgram.Program_Manager_c__c!=null){
                        gusRecUserIdSet.add(gusProgram.Program_Manager_c__c);
                    }
                    if(gusProgram.Executive_Sponsor_c__c!=null){
                        gusRecUserIdSet.add(gusProgram.Executive_Sponsor_c__c);
                    }
                    if(gusProgram.Portfolio_c__c!=null){
                        gusRecPortfolioIdSet.add(gusProgram.Portfolio_c__c);
                    }
                }
                
            }
            Map<id,Portfolio__c> externalIdVSPortfolioMap = new Map<id,Portfolio__c>();
            for(Portfolio__c port : [SELECT id , GUS_Portfolio__c FROM Portfolio__c WHERE GUS_Portfolio__c IN :gusRecPortfolioIdSet ]){
                externalIdVSPortfolioMap.put(port.GUS_Portfolio__c,port);
            }
            for(User__x userRec : [SELECT ID, ExternalID, EmployeeNumber__c FROM User__x WHERE externalId in: gusRecUserIdSet]){
                if(userRec.EmployeeNumber__c!=null){
                    externalID_Vs_EmployeeNumber.put(userRec.externalId, userRec.EmployeeNumber__c);
                }
            }
            for(User userRec : [SELECT ID, EmployeeNumber From User WHERE EmployeeNumber IN :externalID_Vs_EmployeeNumber.Values()]){
                if(userRec.EmployeeNumber!=null){
                    employeeNumber_Vs_InternalID.put(userRec.EmployeeNumber, userRec.Id);
                }
            }
            Map<String,Program__c> gusrecordToProgramMap = new Map<String,Program__c>();
            for(Program__c progamRec :[SELECT Id, Portfolio__c,Portfolio__r.GUS_Portfolio__c, 
                                       Executive_Sponsor__c,
                                       Name,
                                       Executive_Sponsor__r.EmployeeNumber, 
                                       Program_Goals__c, 
                                       Program_Health__c, 
                                       Program_Health_Color__c, 
                                       Program_Health_Comments__c,
                                       Program_Owner__r.EmployeeNumber,
                                       Program_Manager__c, Parent_Program__r.Name,
                                       Program_Manager__r.EmployeeNumber,
                                       Program_Owner__c, Program_Summary__c,
                                       Parent_Program__r.GUS_Program__c,
                                       GUS_Program__c
                                       FROM Program__c 
                                       WHERE GUS_Program__c  IN : externalProgramIdSet]){
                                           if(Test.isRunningTest()){
                                               gusrecordToProgramMap.put('EX101',progamRec);
                                           }else{
                                               gusrecordToProgramMap.put(progamRec.GUS_Program__c,progamRec);
                                           }
                                       }
            for(PPM_Program_c__x gusObject :(List<PPM_Program_c__x>)gusProgramRecordList){
                if(gusObject.LastModifiedDate__c >=fiveMinsInterval || Test.isRunningTest()){
                    Program__c programRec = new Program__c();
                    if(gusrecordToProgramMap.containsKey(gusObject.ExternalId)){
                        programRec =  gusrecordToProgramMap.get(gusObject.ExternalId);
                    }else{
                        programRec = new Program__c();
                        programRec.GUS_Program__c = gusObject.ExternalId;
                    }
                    
                    programRec.Program_Health__c  = gusObject.Program_Health_c__c;
                    programRec.Program_Goals__c = gusObject.Program_Goals_c__c;
                    programRec.Name = gusObject.Name__c;
                    programRec.Program_Status__c = 'Active';
                    programRec.Program_Summary__c = gusObject.Program_Summary_c__c; 
                    programRec.Program_Health_Comments__c = gusObject.Program_Health_Comments_c__c;
                    if(externalIdVSPortfolioMap.containsKey(gusObject.Portfolio_c__c)){                        
                    	programRec.Portfolio__c = externalIdVSPortfolioMap.get(gusObject.Portfolio_c__c).id;
                    }
                    System.debug('employeeNumber_Vs_InternalID'+externalID_Vs_EmployeeNumber);
                    System.debug('employeeNumber_Vs_InternalID'+employeeNumber_Vs_InternalID);
                    System.debug('programRec.Program_Manager__c'+programRec.Program_Manager__c);
                    if(externalID_Vs_EmployeeNumber.containsKey(gusObject.Executive_Sponsor_c__c) && employeeNumber_Vs_InternalID.containsKey(externalID_Vs_EmployeeNumber.get(gusObject.Executive_Sponsor_c__c)))
                        programRec.Executive_Sponsor__c = employeeNumber_Vs_InternalID.get(externalID_Vs_EmployeeNumber.get(gusObject.Executive_Sponsor_c__c));
                    
                    if(externalID_Vs_EmployeeNumber.containsKey(gusObject.Program_Owner_c__c) && employeeNumber_Vs_InternalID.containsKey(externalID_Vs_EmployeeNumber.get(gusObject.Program_Owner_c__c)))
                        programRec.Program_Owner__c = employeeNumber_Vs_InternalID.get(externalID_Vs_EmployeeNumber.get(gusObject.Program_Owner_c__c));
                    
                    if(externalID_Vs_EmployeeNumber.containsKey(gusObject.Program_Manager_c__c) && employeeNumber_Vs_InternalID.containsKey(externalID_Vs_EmployeeNumber.get(gusObject.Program_Manager_c__c)))
                        programRec.Program_Manager__c = employeeNumber_Vs_InternalID.get(externalID_Vs_EmployeeNumber.get(gusObject.Program_Manager_c__c));
                    
                    programList.add(programRec);
                }
            }
        }
        System.debug('>>> list to be update >>>>' + programList);
        if(!programList.isEmpty()){
            upsert programList;            
        }
    }
    
    global void finish(Database.BatchableContext bc){
        PRT_ProgramIntegrationBatch batch = new PRT_ProgramIntegrationBatch();
        if(!Test.isRunningTest()){
            System.scheduleBatch(batch, 'PRT_ProgramIntegrationBatch',4);            
        }        
    }
    public void execute(SchedulableContext SC) {
        database.executebatch(new PRT_ProgramIntegrationBatch());
    } 
    
    
}
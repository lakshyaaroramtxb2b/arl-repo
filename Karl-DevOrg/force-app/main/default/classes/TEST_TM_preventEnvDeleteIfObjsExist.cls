/**
 * Trust Maturity Model v2
 * Nov 2015
 * Written By: Joanna Chen
 * 
 * Test class for the following functionality living in TM_preventEnvDeleteIfObjectivesExist:
 * 
 * [called by a Before Delete trigger on Environment]
 * Enforce the Trust Maturity Model constraint that an Environment cannot be deleted if it has TM Objectives associated with it.
 * There exists similar behavior for the Environment and Security Control Status objects.
**/

@isTest
public class TEST_TM_preventEnvDeleteIfObjsExist {

    private static String businessUnitLabel = TM_Constants.BU_ENVIRONMENT_TYPE;
    private static final String BU_ACCOUNT_REC_TYPE_ID = TM_Constants.BU_ACCOUNT_REC_TYPE_ID;
    private static final String OTHER_ENVIRONMENT_REC_TYPE_ID = TM_Constants.OTHER_ENVIRONMENT_REC_TYPE_ID;
    
    static testMethod void testCannotDeleteEnvWithObjs(){

   		// ---------------- CREATE TEST DATA 
		Account a = TEST_TM_Util.createAccount(BU_ACCOUNT_REC_TYPE_ID);
        IRCloud__Environment__c env = TEST_TM_Util.createEnvironment(OTHER_ENVIRONMENT_REC_TYPE_ID, a.Id); 

        TM_AbstractObjective__c ao = TEST_TM_Util.createTMAbstractObjective();
        TM_Placement__c p =  TEST_TM_Util.createTMPlacement(ao.Id, 'SIR', '1', 1, businessUnitLabel);
       	TM_Objective__c o = TEST_TM_Util.createTMObjectiveEnv(p.Id, env.Id);
        
            
        // ---------------- RUN TEST            
        test.startTest();
        try {
            delete (env);
        } catch (DmlException e) {
            //error thrown here since the delete should not succeed
        }         
        test.stopTest();
        
        
        // ---------------- VERIFY EXPECTED BEHAVIOR
        IRCloud__Environment__c verifyEnv = [SELECT Id, IsDeleted FROM IRCloud__Environment__c WHERE Id = :env.Id];
		System.assertEquals(false, verifyEnv.IsDeleted, 'Environment should not have been deleted.');
            
    }
    
    static testMethod void testCanDeleteEnvWithoutObjs(){

   		// ---------------- CREATE TEST DATA 
		Account a = TEST_TM_Util.createAccount(BU_ACCOUNT_REC_TYPE_ID);
        IRCloud__Environment__c env = TEST_TM_Util.createEnvironment(OTHER_ENVIRONMENT_REC_TYPE_ID, a.Id); 

        TM_AbstractObjective__c ao = TEST_TM_Util.createTMAbstractObjective();
        TM_Placement__c p =  TEST_TM_Util.createTMPlacement(ao.Id, 'SIR', '1', 1, businessUnitLabel);

            
        // ---------------- RUN TEST            
        test.startTest();
        try {
            delete (env);
        } catch (DmlException e) {
            //error thrown here since the delete should not succeed
        }         
        test.stopTest();
        
        
        // ---------------- VERIFY EXPECTED BEHAVIOR
        integer verifyEnvCount = [SELECT count() FROM Environment__c WHERE Id = :env.Id];
		System.assertEquals(0, verifyEnvCount, 'Environment should have been deleted.');
            
    }

}
/*
Copyright (c) 2010 salesforce.com, inc.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:

1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. The name of the author may not be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, 
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

By: Chris Kemp <ckemp@salesforce.com> and Sandy Jones <sajones@salesforce.com>
        with contributions from John Kucera <jkucera@salesforce.com> and
        Reid Carlberg <reid.carlberg@salesforce.com>
*/

public with sharing class CaseSwarmRuleSetup {

    String step1 = null;
    String withStatus = null;
    String withPriority = null;
    String withType = null;
    Id recordType = null;
    String[] usersSelected = new String[]{};
    Boolean notify = false;
    
    // Soby - additions
    public String compareOperator {get;set;}
    public String compareField1 {get;set;}
    public String compareField2 {get;set;}
    public String compareField3 {get;set;}
    public String compareField4 {get;set;}

    // we'll support each field as a text val or multiselect if it's a picklist
    public Boolean fieldChecks {get;set{ fieldChecks=value;}}
    public String compareVal1 {get;set;}
    public String compareVal2 {get;set;}
    public String compareVal3 {get;set;}
    public String compareVal4 {get;set;}
    
    public List<SelectOption> compareVal1Options {get;set;}
    public List<SelectOption> compareVal2Options {get;set;}
    public List<SelectOption> compareVal3Options {get;set;}
    public List<SelectOption> compareVal4Options {get;set;}

    private Map<String, Schema.DisplayType> fieldMap;
    private static Map<String, Schema.SObjectField> descMap;
    private static final Set<String> stringTypeNames;
    private static final Set<String> idTypeNames;
    private static final Set<String> pickListTypeNames;
    public List<SelectOption> selectableFields {get;private set;}
    
    static {
        idTypeNames = new Set<String> {'reference'};
        stringTypeNames = new Set<String> {'textarea','url','id','phone','string'};
        pickListTypeNames = new Set<String> {'multipicklist','combobox','picklist'};
        descMap = Schema.SObjectType.Case.fields.getMap();
    }
    
    public CaseSwarmRuleSetup() {
        fieldChecks = false;
        fieldMap = new Map<String,Schema.DisplayType>();
        
        selectableFields = new SelectOption[]{new SelectOption('','')};
        
        for (String fieldName: descMap.keySet()) {
            Schema.DescribeFieldResult d = descMap.get(fieldName).getDescribe();
            if (d.isAccessible() && ((stringTypeNames.contains(d.getType().name().toLowerCase())) || (pickListTypeNames.contains(d.getType().name().toLowerCase()))  || (idTypeNames.contains(d.getType().name().toLowerCase())) )) {
                fieldMap.put(fieldName,d.getType());
                selectableFields.add(new SelectOption(d.getName(),d.getLabel()));
            }
            system.debug(fieldName); 
        }

    }
    
    public List<SelectOption> getField1Options() {
        return getFieldOptions(compareField1,compareVal1Options);
    }
    public List<SelectOption> getField2Options() {
        return getFieldOptions(compareField2,compareVal2Options);
    }
    public List<SelectOption> getField3Options() {
        return getFieldOptions(compareField3,compareVal3Options);
    }
    public List<SelectOption> getField4Options() {
        return getFieldOptions(compareField4,compareVal4Options);
    }
    public List<SelectOption> getFieldOptions(String compareField,List<SelectOption> compareValOptions) {
        if (compareField == null) {return null;}
        
        if (pickListTypeNames.contains(fieldMap.get(compareField.toLowerCase()).name().toLowerCase())) {
            Schema.DescribeFieldResult d = descMap.get(compareField).getDescribe();
            compareValOptions = new List<SelectOption>();
            for (Schema.PicklistEntry ple : d.getPicklistValues() ) {
                compareValOptions.add(new SelectOption(ple.getValue(),ple.getLabel()));
            }
        } else {
            compareValOptions = null;
        }
        return compareValOptions;
    }
    
    
    public String getStep1() {
        return step1 ;
    }
                    
    public void setStep1(String step1 ) { 
        this.step1 = step1; 
    }

    public List<SelectOption> getStep1Options() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('All Cases','All Cases'));
        options.add(new SelectOption('Only Cases for Accounts I Own',     'Only Cases for Accounts I Own'));
        options.add(new SelectOption('Only Cases with a certain Status',  'Only Cases with a certain Status'));
        options.add(new SelectOption('Only Cases with a certain Priority','Only Cases with a certain Priority'));
        options.add(new SelectOption('Only Cases of a certain Type',      'Only Cases of a certain Type'));
        options.add(new SelectOption('Only Cases of a certain Record Type',      'Only Cases of a certain Record Type'));
        return options;
    }
    
    //added for virtual groups functionality. adam mcneilly 17/12/2014
    public PaginatedSelectList getGroupOptions() {
        PaginatedSelectList options = new PaginatedSelectList ();

        for (Group thisGroup: 
                [select Id, DeveloperName from Group where DeveloperName != '' order by DeveloperName]) {
                //:g value used so we can dtermine only using id value if it is user or group. this is limitation of listbox control
            options.add(new SelectOption(thisGroup.Id+':g', 'Group: ' + thisGroup.DeveloperName));
        }
        
        return options;
    }

    public String getWithStatus() {
        return withStatus;
    }
                    
    public void setWithStatus(String withStatus) { 
        this.withStatus = withStatus; 
    }

    public List<SelectOption> getStatusOptions() {
        List<SelectOption> options = new List<SelectOption>();

        Schema.DescribeFieldResult fieldResult = Case.Status.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        
        for( Schema.PicklistEntry f : ple) {
            options.add(new SelectOption(f.getLabel(), f.getValue()));
        }
       return options;
    }

    public String getWithPriority() {
        return withPriority;
    }
                    
    public void setWithPriority(String withPriority) { 
        this.withPriority = withPriority; 
    }

    public List<SelectOption> getPriorityOptions() {
        List<SelectOption> options = new List<SelectOption>();

        Schema.DescribeFieldResult fieldResult = Case.Priority.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        
        for( Schema.PicklistEntry f : ple) {
            options.add(new SelectOption(f.getLabel(), f.getValue()));
        }
       return options;
    }

    public String getWithType() {
        return withType;
    }
                    
    public void setWithType(String withType) { 
        this.withType = withType; 
    }

    public List<SelectOption> getTypeOptions() {
        List<SelectOption> options = new List<SelectOption>();

        Schema.DescribeFieldResult fieldResult = Case.Type.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        
        for( Schema.PicklistEntry f : ple) {
            options.add(new SelectOption(f.getLabel(), f.getValue()));
        }
       return options;
    }

    public String getRecordType() {
        return recordType;
    }
                    
    public void setRecordType(Id recordType) { 
        this.recordType = recordType; 
    }

    public List<SelectOption> getRecordTypeOptions() {
        List<SelectOption> options = new List<SelectOption>();

        for (RecordType f : [SELECT Name,Id FROM RecordType WHERE SObjectType='Case']) {
          options.add(new SelectOption(f.Id,f.Name));
        }
       return options;
    }
    
    public PaginatedSelectList getUserOptions() {
        PaginatedSelectList options = new PaginatedSelectList ();

        for (User thisUser: 
                [select Id, FirstName, LastName from User where isActive = true and UserType = 'Standard' order by LastName, FirstName]) {
            options.add(new SelectOption(thisUser.Id, thisUser.LastName + ', ' + thisUser.FirstName));
        }
        
        return options;
    }

    public String[] getUsersSelected() {
        return usersSelected;
    }
                    
    public void setUsersSelected(String[] usersSelected) { 
        this.usersSelected = usersSelected; 
    }

    public Boolean getNotify() {
        return notify;
    }
                    
    public void setNotify(Boolean notify) { 
        this.notify = notify; 
    }

    public PageReference action() {

        //CaseSwarmRuleSetup.addRules(this.getUsersSelected(), this.step1, this.withStatus, this.withPriority, this.withType, this.notify);
        addRules();
            
        // Redirect the user back to the original page
        PageReference pageRef = Page.SwarmRuleSaveSuccess;
        pageRef.setRedirect(true);
        return pageRef;
    }
    
    //Old method. removed when virtual groups functioanlity added. see redefinition of addRules()
    // Soby - I don't understand why the original addRules() is @future. It seems limiting to have to pass everything into a static method
    // Was it to increase DML limits and allow more inserts? If so, we don't have that issue in the security org
    /*
    public void addRules() {
        List<Case_Swarm_Rule__c> newRules = new List<Case_Swarm_Rule__c>();
    
        for (String userId: this.getUsersSelected()) {
            String ruleName = step1;
        
            Case_Swarm_Rule__c rule = new Case_Swarm_Rule__c();
            rule.Type__c = step1 ;
            if (step1.equals('Only Cases with a certain Status')) {
                rule.Case_Status__c = withStatus;
                ruleName = 'Only Cases with Status: ' + withStatus;
            } else if (step1.equals('Only Cases with a certain Priority')) {
                rule.Case_Priority__c = withPriority;
                ruleName = 'Only Cases with Priority: ' + withPriority;
            } else if (step1.equals('Only Cases of a certain Type')) {
                rule.Case_Type__c = withType;
                ruleName = 'Only Cases with Type: ' + withType;
            } else if (step1.equals('Only Cases of a certain Record Type')) {
                rule.Case_Record_Type__c = recordType;
                ruleName = 'Only Cases with Record Type: '+recordType;
            }

            rule.User__c = userId;
            rule.Description__c = ruleName;
            rule.Notify_on_Swarm__c = notify;
            
            // soby - part added for compare fields
            if (fieldChecks) {
                rule.Description__c += ' and matching fields';
                rule.Compare_Operator__c = compareOperator;
                
                if (String.isNotBlank(compareField1)) {
                    if  (idTypeNames.contains(descMap.get(compareField1).getDescribe().getType().name().toLowerCase())) {
                        id testId = Id.valueOf(compareVal1);
                    }
                    rule.Compare_Field_Name1__c = compareField1;
                    rule.Compare_Field_Value1__c = compareVal1;
                }
                if (String.isNotBlank(compareField2)) {
                    if  (idTypeNames.contains(descMap.get(compareField2).getDescribe().getType().name().toLowerCase())) {
                        id testId = Id.valueOf(compareVal2);
                    }
                    rule.Compare_Field_Name2__c = compareField2;
                    rule.Compare_Field_Value2__c = compareVal2;
                }
                if (String.isNotBlank(compareField3)) {
                    if  (idTypeNames.contains(descMap.get(compareField3).getDescribe().getType().name().toLowerCase())) {
                        id testId = Id.valueOf(compareVal3);
                    }
                    rule.Compare_Field_Name3__c = compareField3;
                    rule.Compare_Field_Value3__c = compareVal3;
                }
                if (String.isNotBlank(compareField4)) {
                    if  (idTypeNames.contains(descMap.get(compareField4).getDescribe().getType().name().toLowerCase())) {
                        id testId = Id.valueOf(compareVal4);
                    }
                    rule.Compare_Field_Name4__c = compareField4;
                    rule.Compare_Field_Value4__c = compareVal4;
                }
                
            }
            newRules.add(rule);
        }
        insert newRules;
    }
    */
    
    //added as par tof virutal groups functionality. adam mcneilly 2014
    private void createRule(List<Case_Swarm_Rule__c> newRules, String userId)
    {
        System.debug('createRule() - Adding userId = ' + userId);
         
        String ruleName = step1;
        
            Case_Swarm_Rule__c rule = new Case_Swarm_Rule__c();
            rule.Type__c = step1 ;
            if (step1.equals('Only Cases with a certain Status')) {
                rule.Case_Status__c = withStatus;
                ruleName = 'Only Cases with Status: ' + withStatus;
            } else if (step1.equals('Only Cases with a certain Priority')) {
                rule.Case_Priority__c = withPriority;
                ruleName = 'Only Cases with Priority: ' + withPriority;
            } else if (step1.equals('Only Cases of a certain Type')) {
                rule.Case_Type__c = withType;
                ruleName = 'Only Cases with Type: ' + withType;
            } else if (step1.equals('Only Cases of a certain Record Type')) {
                rule.Case_Record_Type__c = recordType;
                ruleName = 'Only Cases with Record Type: '+recordType;
            }

            rule.User__c = userId;
            rule.Description__c = ruleName;
            rule.Notify_on_Swarm__c = notify;
            
            // soby - part added for compare fields
            if (fieldChecks) {
                rule.Description__c += ' and matching fields';
                rule.Compare_Operator__c = compareOperator;
                
                if (String.isNotBlank(compareField1)) {
                    if  (idTypeNames.contains(descMap.get(compareField1).getDescribe().getType().name().toLowerCase())) {
                        id testId = Id.valueOf(compareVal1);
                    }
                    rule.Compare_Field_Name1__c = compareField1;
                    rule.Compare_Field_Value1__c = compareVal1;
                }
                if (String.isNotBlank(compareField2)) {
                    if  (idTypeNames.contains(descMap.get(compareField2).getDescribe().getType().name().toLowerCase())) {
                        id testId = Id.valueOf(compareVal2);
                    }
                    rule.Compare_Field_Name2__c = compareField2;
                    rule.Compare_Field_Value2__c = compareVal2;
                }
                if (String.isNotBlank(compareField3)) {
                    if  (idTypeNames.contains(descMap.get(compareField3).getDescribe().getType().name().toLowerCase())) {
                        id testId = Id.valueOf(compareVal3);
                    }
                    rule.Compare_Field_Name3__c = compareField3;
                    rule.Compare_Field_Value3__c = compareVal3;
                }
                if (String.isNotBlank(compareField4)) {
                    if  (idTypeNames.contains(descMap.get(compareField4).getDescribe().getType().name().toLowerCase())) {
                        id testId = Id.valueOf(compareVal4);
                    }
                    rule.Compare_Field_Name4__c = compareField4;
                    rule.Compare_Field_Value4__c = compareVal4;
                }
                
            }
            newRules.add(rule);
    }
    
    //Added for virutal group functionality. Adam Mcneilly 2014
    public void addRules() {
        List<Case_Swarm_Rule__c> newRules = new List<Case_Swarm_Rule__c>();
    
        for (String userId: this.getUsersSelected()) {
        
            
            if(userId.indexOf(':g') != -1)
            {
                //is a group
                System.debug('Item in list is Group');
                
                String groupId = userId.split(':')[0];

                System.debug('GroupId = ' + groupId);

                for (GroupMember gm : [SELECT GroupId, UserOrGroupId FROM GroupMember WHERE GroupId =: groupId]) {
                    
                    User selUser = [SELECT Id From User WHERE Id =: gm.UserOrGroupId];
                    
                    System.debug('**** Found user in Group = ' + selUser);
                    
                    //Groups of Groups is ignored due to complexitiy issues (depth is n of 1)
                    if(selUser != NULL)
                    {
                        createRule(newRules, selUser.Id);
                    }
                }

            }
            else
            {
                //is a user
                System.debug('Item in list is User');
                
                createRule(newRules, userId);
            }

            //add rule here
        }
        insert newRules;
    }
    
    @future
    public static void addRules(String[] usersSelected, String step1, String withStatus, String withPriority, String withType, Boolean notify) {
        List<Case_Swarm_Rule__c> newRules = new List<Case_Swarm_Rule__c>();
    
        for (String userId: usersSelected) {
            String ruleName = step1;
        
            Case_Swarm_Rule__c rule = new Case_Swarm_Rule__c();
            rule.Type__c = step1 ;
            if (step1.equals('Only Cases with a certain Status')) {
                rule.Case_Status__c = withStatus;
                ruleName = 'Only Cases with Status: ' + withStatus;
            } else if (step1.equals('Only Cases with a certain Priority')) {
                rule.Case_Priority__c = withPriority;
                ruleName = 'Only Cases with Priority: ' + withPriority;
            } else if (step1.equals('Only Cases of a certain Type')) {
                rule.Case_Type__c = withType;
                ruleName = 'Only Cases with Type: ' + withType;
            }

            rule.User__c = userId;
            rule.Description__c = ruleName;
            rule.Notify_on_Swarm__c = notify;
            newRules.add(rule);
        }
        insert newRules;
    }
}
/**
* @author Ben Harvie
* @email ben.harvie@salesforce.com
* @description This is trigger handler class for Area_Requirement__c.
*/
public with sharing class KARL_AreaRequirementTriggerHelper {
    public static Set<Id> recordIds = new Set<Id>();
    
    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description This method gets triggered from Area Requirements
    */
    public static void run(){
        // After Update
        if( Trigger.isAfter && Trigger.isUpdate ){
            updateReferenceIndex();
        }
        // After Insert
        else if( Trigger.isAfter && Trigger.isInsert ){
            updateReferenceIndex();
        }
    }
    
    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description This method updates the Reference Index field for searchability
    */
    private static void updateReferenceIndex(){
        /**
         * @author Ben Harvie
         * @email ben.harvie@salesforce.com
         * @description Recursion logic based on set of Ids
         */
        for(Id recursionCheck : Trigger.newMap.keySet()){
            if(recordIds.contains(recursionCheck)){
                System.debug(LoggingLevel.DEBUG, 'Already processed this record >> ' + recursionCheck);
                return; // Kill the run - we've already processed this Id
            }else{
                System.debug(LoggingLevel.DEBUG, 'New record to process >> ' + recursionCheck);
                recordIds.add(recursionCheck); // add Id to set so we don't run it again
            }
        }

        List<Area_Requirement__c> areaRequirements = new List<Area_Requirement__c>();

        for( Area_Requirement__c  areaItems : [SELECT Id,Requirement_Reference__c,toLabel(Area_of_Compliance__c),
                                                        KARL_Full_Requirement_Reference__c
                                                        FROM Area_Requirement__c 
                                                        WHERE Id in: (List<Area_Requirement__c >)Trigger.New ]){
            // Update only if different
            if( areaItems.KARL_Full_Requirement_Reference__c != (areaItems.Area_of_Compliance__c + ' - ' + areaItems.Requirement_Reference__c) ){
                // Set the Triggered Report Name in the Area Requirement Record
                Area_Requirement__c areaItem = new Area_Requirement__c();
                areaItem.Id = areaItems.Id;
                areaItem.KARL_Full_Requirement_Reference__c = areaItems.Area_of_Compliance__c + ' - ' + areaItems.Requirement_Reference__c;
                areaRequirements.add(areaItem);
            }
        }
        
        // Make the update accounting for sharing and permission sets
		if( !areaRequirements.isEmpty() && Schema.sObjectType.Area_Requirement__c.isUpdateable() 
          	&& Area_Requirement__c.KARL_Full_Requirement_Reference__c.getDescribe().isUpdateable() ){
            update areaRequirements;
        }
    }
}
public with sharing class BPO_API_Ctrl_EX {

   public BPO_API_Ctrl_EX() {}

   // Task Item Record Types
   public static String DATE_RESPONSE_TYPE = 'Date_Response';
   public static String BOOLEAN_RESPONSE_TYPE = 'Boolean_Response';
   public static String LONG_TEXT_RESPONSE_TYPE = 'Long_Text_Response';
    
   // API execution status
   public static String SUCCESS = 'success';
   public static String FAILURE = 'update';       
    
   private string recordId {get;set;}
   public BPO_API_Ctrl_EX(ApexPages.standardController std) {
      recordId = std.getRecord().id;
   }
   
   public Map<String, List<String>> abstractTaskCatsWithSubCats {
      private set;
      get {
         if (abstractTaskCatsWithSubCats == null) {
            abstractTaskCatsWithSubCats = PickListUtil.getDependentOptionsImpl(BPO_Abstract_Task__c.Sub_Category__c, 
                                                                               BPO_Abstract_Task__c.Category__c);
            return abstractTaskCatsWithSubCats;                       
         } else return abstractTaskCatsWithSubCats;
      }
   }
    
   public List<SelectOption> taskItemStatusOptions {
      get {
          List<SelectOption> options = new List<SelectOption>();
          Schema.DescribeFieldResult fieldResult = BPO_Task_Item__c.Status__c.getDescribe();
          List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            
          for( Schema.PicklistEntry f : ple) {
            options.add(new SelectOption(f.getLabel(), f.getValue()));
          }
          return options;
      }
   }
    
   public string taskItemId {get;set;}   
   public string taskItemLastModifiedDate {get;set;}
   public string taskItemResponseType {get;set;}
   public string taskItemResponse {get;set;}
   public string taskItemStatus {get;set;}
   // To send the status back to client
   public string taskItemUpdateStatus {get;set;}
    
   public void changeTaskItemStatus() {
      for (BPO_Task_Item__c taskItem : [SELECT Id, Name, LastModifiedDate, Required__c, Response_LongText__c, Response_Date__c, Response_Boolean__c, Response_Formula__c, Status__c, Task_Item_Description__c
                                          FROM BPO_Task_Item__c Where Id = :taskItemId]) {
          Datetime lastModifiedDate = taskItem.LastModifiedDate;
          if (lastModifiedDate > Datetime.valueOfGmt(taskItemLastModifiedDate)) {
             // ERROR, Changes are not saved 
             taskItemUpdateStatus = FAILURE;
          } else {  
             taskItemUpdateStatus = SUCCESS;           
             taskItem.Status__c = taskItemStatus;
             // Expecting only one record
             update taskItem;
          }                                   
      }
   }
   
   public void changeTaskItemResponse() {
      for (BPO_Task_Item__c taskItem : [SELECT Id, Name, LastModifiedDate, Required__c, Response_LongText__c, Response_Date__c, Response_Boolean__c, Response_Formula__c, Status__c, Task_Item_Description__c
                                          FROM BPO_Task_Item__c Where Id = :taskItemId]) {
          Datetime lastModifiedDate = taskItem.LastModifiedDate;
          if (lastModifiedDate > Datetime.valueOfGmt(taskItemLastModifiedDate)) {
             // ERROR, Changes are not saved 
             taskItemUpdateStatus = FAILURE;
          } else {
            taskItemUpdateStatus = SUCCESS;
            if (taskItemResponseType == Long_Text_Response_TYPE) {
              taskItem.Response_LongText__c = taskItemResponse;
            } else if (taskItemResponseType == Date_Response_TYPE) {
              taskItem.Response_Date__c = Date.parse(taskItemResponse);
            } else if (taskItemResponseType == Boolean_Response_TYPE) {
              taskItem.Response_Boolean__c = Boolean.valueOf(taskItemResponse);
            }
            // Expecting only one record
            update taskItem;
          }
      }
   }
    
    public List<BPO_Task__c> getAllTasksWithItems() {
      List<BPO_Task__c> tasks = [
         SELECT Id, Name, Task_Items_Completed_Required__c, Task_Items_NA_Status__c, Task_Items_Not_Started__c, BPO_Assessment__c,
           Description__c, Required__c, Status__c, BPO_Abstract_Task__r.Category__c, BPO_Abstract_Task__r.Sub_Category__c,
           (SELECT Id, Name, LastModifiedDate, RecordType.DeveloperName, Required__c, Response_LongText__c, Response_Date__c, Response_Boolean__c, Response_Formula__c, Status__c, Task_Item_Description__c
              FROM BPO_Task_Items__r)             
           FROM BPO_Task__c
           WHERE BPO_Assessment__c = :recordId
        ];
      return tasks;
   }   

}
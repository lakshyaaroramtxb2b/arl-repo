public class SyncCasesWithIntakeRequests implements Database.Batchable<sObject>, Database.Stateful {

    public String SOURCE_FILE = 'SyncCasesWithIntakeRequests';
    
    public Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator([SELECT Id,RecordTypeId,
                              Enterprise_Security_Case_Number__c,
                              ESA_Security_Request__c
                       FROM Case
                       WHERE Enterprise_Security_Case_Number__c!=null
                       AND ESA_Security_Request__c = null
                       AND RecordTypeId = :SPT_Constants.SEC_RECORDTYPE_ID]);
    }

    public void execute(Database.BatchableContext bc, List<Case> lstCases) {
        
        Set<String> setFederationIds = new Set<String>();
        
        Set<String> setSecOrgCaseNumbers = new Set<String>();
        Map<String,String> mapIntakeReuestInfo = new Map<String,String>();

        for(Case cas : lstCases){
            setSecOrgCaseNumbers.add(cas.Enterprise_Security_Case_Number__c);          
        }
        system.debug('#########'+setSecOrgCaseNumbers.size());
        if(setSecOrgCaseNumbers.size() > 0) {
            for(Esa_Security_Request__c securityRequest :[SELECT Id,
                                                                 Supportforce_Case_Number__c
                                                          FROM Esa_Security_Request__c
                                                          WHERE Entity_Code__c = :Label.Esa_EntSec_EntityCode
                                                          AND Office_Hours__c != null
                                                          AND Supportforce_Case_Number__c IN : setSecOrgCaseNumbers]){

                mapIntakeReuestInfo.put(securityRequest.Supportforce_Case_Number__c, securityRequest.Id);
            }

          for(Case cas : lstCases){
              if(mapIntakeReuestInfo.containsKey(cas.Enterprise_Security_Case_Number__c)){
                  cas.ESA_Security_Request__c = mapIntakeReuestInfo.get(cas.Enterprise_Security_Case_Number__c);
              }
          }
        }

        Database.update(lstCases, false);

    }

    public void finish(Database.BatchableContext BC) {
         
    }

}
/**
 * @author: ralph@callaway.cloud
 */
public class SC_CourseEmailCheck_Controller {

    public Training_Course_Taken__c tct { get; set; }
    
    private Training_Course__c course;
    private String courseOrgWideId;
    private transient Messaging.SingleEmailMessage[] emails;

    public SC_CourseEmailCheck_Controller(ApexPages.StandardController controller) {
        this.course = (Training_Course__c) controller.getRecord();
        courseOrgWideId = [SELECT Org_Wide_Email_Id__c FROM Training_Course__c 
                           WHERE Id = :this.course.Id].Org_Wide_Email_Id__c;
        if (String.isBlank(courseOrgWideId)) courseOrgWideId = SC_Constants.SC_ORG_WIDE_EMAIL_ID;                          
        tct = new Training_Course_Taken__c(
            Contact__c = getContactId(), 
            Training_Course__c = course.Id, 
            Due_Date__c = Date.today());
    }

    public void checkEmails() {
        tct = tct.clone(false);
        insert tct;
        emails = new Messaging.SingleEmailMessage[0];
        checkEmail(course.Default_Enrollment_Template_DevelopName__c);
        checkEmail(course.Training_Escalation_Profile__r.First_Reminder_Email_Template__c);
        checkEmail(course.Training_Escalation_Profile__r.Second_Reminder_Email_Template__c);
        checkEmail(course.Training_Escalation_Profile__r.Third_Reminder_Email_Template__c);
        checkEmail(course.Training_Escalation_Profile__r.Overdue_Reminder_Email_Template__c);
        Messaging.sendEmail(emails);
        ApexPages.addMessage(new ApexPages.Message(
            ApexPages.Severity.CONFIRM,
            'Sent ' + emails.size() + ' test emails'));
        delete tct;
    }

    private Id getContactId() {
        for (Contact c : [
            SELECT Id FROM Contact WHERE Email = :UserInfo.getUserEmail()
        ]) {
            return c.Id;
        }
        return null;
    }

    private void checkEmail(String templateName) {
        if (String.isNotBlank(templateName)) {
            Id templateId = getTemplateId(templateName);
            if (templateId == null) {
                ApexPages.addMessage(new ApexPages.Message(
                    ApexPages.Severity.ERROR,
                    'Email Template Not Found: ' + templateName));
            } else {
                emails.add(makeEmail(templateId));
            }
        }
    }

    private Messaging.SingleEmailMessage makeEmail(Id templateId) {
        Messaging.SingleEmailmessage email = new Messaging.SingleEmailmessage();
        email.setOrgWideEmailAddressId(courseOrgWideId);
        email.setTargetObjectId(tct.Contact__c);
        email.setSaveAsActivity(true);
        email.setCharset('UTF-8');
        email.setTemplateId(templateId);
        email.setWhatId(tct.Id);
        email.setBccAddresses(new String[] { UserInfo.getUserEmail() });
        return email;
    }

    private Id getTemplateId(String templateName) {
        for (EmailTemplate template : [
            SELECT Id FROM EmailTemplate WHERE DeveloperName = :templateName
        ]) {
            return template.Id;
        }
        return null;
    }
}
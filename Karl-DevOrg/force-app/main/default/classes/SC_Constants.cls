/**
 * @author: ralph@callaway.cloud
 * @description: misc, non-sobject constants for security communications
 */
public class SC_Constants {
    
    public static Id SC_EMAIL_FOLDER_ID = '00l3A000002btOkQAI';
    public static Id SC_ORG_WIDE_EMAIL_ID = '0D2300000004DwfCAE';
    
}
/***************IEM_CaseListController****************
@Author Banshi
@Date 08/20/2019
@Description Apex class to fetch list of cases in community, it is in without sharing because we are listing cases outside the sharing.
**********************************************/
public without sharing class IEM_CaseListController {
    /*
    * Created by - Banshi
    * This functon is used to get list of case records based on below criteria  
    * User Is Requestor and Status = Submitted OR 
    * User is Issue Owner and Issue Is Valid OR
    * User is Action Plan Owner and Issue is Valid OR
    * User is Business Reviewer
    */
    @AuraEnabled
    public static List<Case> getCases(){
        List<User> currentUser = [SELECT ContactId 
                                  FROM User 
                                  WHERE Id =:userinfo.getUserId() AND ContactId!=null];
        if(!currentUser.isEmpty() && String.isNotEmpty(currentUser[0].ContactId)){
            Id contactId = currentUser[0].ContactId;
            return [SELECT Id, CaseNumber, Subject, Status
                    FROM Case 
                    Where (Requestor__c = :contactId AND Status= :IEM_Constants.IEM_SUBMITTED) 
                    OR ( (Issue_Owner__c = :contactId OR Action_Plan_Owner__c =:contactId ) AND Validity__c=:IEM_Constants.IEM_VALIDITY_STATUS_VALID) 
                    OR (Business_Reviewer__c =:contactId) ORDER BY CreatedDate DESC];
        } 
        return null;
    }
    /*
    * Created by - Banshi
    * Get picklist values for any object, any field
    * @params
    * 	ObjectApi_name = Object API Name
    * 	Field_name = Field API Name
    * @return
    * List<String> = Picklist values
    */
    @AuraEnabled
    public static List<String> getPicklistValues(String ObjectApi_name, String Field_name){
        List<String> lstPickvals = new List<String>();
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get(ObjectApi_name);//From the Object Api name retrieving the SObject
        SObject Object_name = targetType.newSObject();
        Schema.SObjectType sobject_type = Object_name.getSObjectType(); //grab the sobject that was passed
        Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe(); //describe the sobject
        Map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap(); //get a map of fields for the passed sobject
        List<Schema.PicklistEntry> pick_list_values = field_map.get(Field_name).getDescribe().getPicklistValues(); //grab the list of picklist values for the passed field on the sobject
        for (Schema.PicklistEntry a : pick_list_values){
            //for all values in the picklist list
            lstPickvals.add(a.getValue());//add the value  to our final list
        }
        return lstPickvals;
    }
    
    /*
    * Created by - Banshi
    * Get picklist values for any object, any field
    * @params
    * 	Case caseRec = case object
    * @return
    * Case - Saved case
    */
    @AuraEnabled
    public static CaseWrapper saveCase(Case caseRec){
        if(caseRec != null){
            String msg = '';
            try{
                //update Case Record
                update caseRec; 
            }
            catch(DmlException e){
                //Any type of Validation Rule error message, Required field missing error message, Trigger error message etc..
                //we can get from DmlException
                
                //Get All DML Messages
                for (Integer i = 0; i < e.getNumDml(); i++) {
                    //Get Validation Rule & Trigger Error Messages
                    msg =+ e.getDmlMessage(i) +  '\n' ;
                }
                //throw DML exception message
                throw new AuraHandledException(msg);
            }
            catch(Exception e){
                //throw all other exception message
                throw new AuraHandledException(e.getMessage());
            }
        }
        return getCaseData(caseRec.Id, false);
    }
    
    /*
    * Created by - Banshi
    * Get list of related milestones by case ID
    * @params
    * 	Id caseId = case record Id
    * @return
    * CaseWrapper - Wrapper object
    */
    @AuraEnabled
    public static CaseWrapper getCaseData(Id caseId, Boolean isInitialLoad){
        CaseWrapper caseWrap = new CaseWrapper(); //Wrapper
        //Get loggedin user contactId
        List<User> currentUser = [SELECT ContactId 
                                  FROM User 
                                  WHERE Id =:userinfo.getUserId() AND ContactId!=null];
        Id contactId = currentUser[0].ContactId;
        if(!currentUser.isEmpty() && String.isNotEmpty(contactId)){
            String query = 'SELECT CaseNumber, Requestor__c, Action_Plan_Owner__c, Issue_Owner__c, '+
                                   'Subject, Status, Description, Team_Performing_Audit__c, '+
                                   'Identified_Date__c, Parent_Cloud__c , Existing_Work_Story__c ,Issue_Identified_By__c, '+
                                   'Security_Standard_Affected__c, Other_Security_Standard__c, Business_Justification__c, '+
                                   'IEM_Case_Type__c, Compensating_Controls__c, Issue_Source__c, Activity_Status__c,'+ 
                                   'APM_Cloud__c , Validity__c, APM_Product__c, Point_of_Contact_of_Audit__r.Name, '+
                                   'Control_Activities_Affected__c, Audit_or_Readiness_Finding__c, '+
                                   'External_Certifications_Impacted__c, Fiscal_Year_of_Audit__c, '+
                                   'Other_Team_Performed_Assessment__c, External_Certification_Impact__c , '+
                                   'Other_External_Certification__c, Common_Control_Framework_Reference__c,'+
                                   'Action_Plan_Status__c, Action_Plan_Owner_is_Aware__c, Mitigating_Controls__c, '+
                                   'External_Action_Plan_Link__c, Number_of_Milestones__c, Estimated_Remediation_Date__c , '+
                                   'Root_Cause__c , Action_Plan__c , Definition_of_Done__c , Known_Controls__c';
            
            //we can't query the external object after any DML, so quering only for first time.
            if(isInitialLoad){
                query += ' , Team_Responsible__r.Name__c, Cloud__r.Name__c, Security_Assessment_Record__r.Name__c ';
            }
            String status = 'Submitted';
            String validity = 'Valid';
            
            query += ' FROM Case WHERE Id =:caseId AND ( (Requestor__c = :contactId AND Status=:status)'+ 
                                       'OR ( Validity__c=:validity AND (Issue_Owner__c = :contactId OR Action_Plan_Owner__c =:contactId))'+
                                       'OR (Business_Reviewer__c =:contactId)) ORDER BY CreatedDate DESC ';
            List<Case> caseList = Database.query(query);
            
            if(!caseList.isEmpty()){
                caseWrap.isRequester = (contactId == caseList[0].Requestor__c ? true : false);
                caseWrap.isActionOwner = (contactId == caseList[0].Action_Plan_Owner__c ? true : false);
                caseWrap.isIssueOwner = (contactId == caseList[0].Issue_Owner__c ? true : false);
                caseWrap.actionPlanStatus = caseList[0].Action_Plan_Status__c;
                caseWrap.caseRecord = caseList[0];
                caseWrap.milestoneList = getMilestones(caseId);
                if(isInitialLoad){
                   	caseWrap.teamResponsibleName = caseList[0].Team_Responsible__r.Name__c;
                    caseWrap.cloudName = caseList[0].Cloud__r.Name__c;
                    caseWrap.securityAssessmentRecordName = caseList[0].Security_Assessment_Record__r.Name__c;
                }
            }
        }        
        return caseWrap;
    }
    
    /*
    * Created by - Banshi
    * To fetch all the related milestones of a case record.
    * @params
    * 	Id caseId = Id of the case record
    * @return
    * List<Milestone__c> - Milestone list
	*/
    @AuraEnabled
    public static List<Milestone__c> getMilestones(Id caseId){
        List<Milestone__c> milestoneList = [SELECT Id, Name, Status__c, Due_Date__c, 
                                          Completed_Date__c, Description__c, Definition_of_Done__c  
                                          FROM Milestone__c 
                                          WHERE Case__c = :caseId 
                                          ORDER BY CreatedDate DESC];
        return milestoneList;
    }
    /*
    * Created by - Banshi
    * To create a new milestone record.
    * @params
    * 	Milestone__c milestone = Milestone record to create
    * @return
    * CaseWrapper - Wrapper object
	*/
    @AuraEnabled
    public static CaseWrapper createMilestone(Milestone__c milestone)
    {
        List<Milestone__c> milestoneList = new List<Milestone__c>();
        if(milestone != null && String.isNotEmpty(milestone.Case__c)){
            upsert milestone;
            return getCaseData(milestone.Case__c, false);
        } 
        return null;
    }
    
    /*
    * Created by - Banshi
    * To delete milestone record.
    * @params
    * 	Id milestoneId = Milestone record Id to be deleted.ApexPages
    *   Id caseId = Parent case record Id 
    * @return
    * CaseWrapper - Wrapper object
	*/
    @AuraEnabled
    public static CaseWrapper deleteMilestoneRecord(Id milestoneId, Id caseId)
    {
        if(String.isNotBlank(milestoneId)){
            delete [Select Id From Milestone__c Where id=:milestoneId];
            return getCaseData(caseId, false);
        } 
        return null;
    }
    
    /*
    * Created by - Prashant
    * To check if there is any approval process pending for the logged community user.
    * @params
    * 	Id targetObjectId = Target object id to check approval process on.
    * @return
    * Boolean - True/False
    */
    @AuraEnabled
    public static Boolean getApprovalProcessAccess(Id targetObjectId){
        Id retVal = null;
        if(targetObjectId!=null){
            for(ProcessInstanceWorkitem workItem  : [SELECT Id,ActorId 
                                                     FROM ProcessInstanceWorkitem
                                                     WHERE ProcessInstance.TargetObjectId =: targetObjectId 
                                                     AND ProcessInstance.Status=:IEM_Constants.IEM_APPROVAL_PROCESS_STATUS_PENDING
                                                     AND actorId = :UserInfo.getUserId()]){                    
                                                         return true;
                                                     }
        }
        return false;
    }   
    
    /*
    * Created by - Prashant
    * To approve/reject approval process.
    * @params
    * 	Id targetObjectId = Target object id to check approval process on.
    *  String Action = Approv/Pending
    * @return
    * Boolean - True/False
    */
    @AuraEnabled    
    public static Boolean approveRejectRecord(Id targetObjectId, String Action, String commentMessage){
        Id retVal = null;
        if(targetObjectId!=null){
            for(ProcessInstanceWorkitem workItem  : [SELECT Id,ActorId 
                                                     FROM ProcessInstanceWorkitem
                                                     WHERE ProcessInstance.TargetObjectId =: targetObjectId 
                                                     AND ProcessInstance.Status=:IEM_Constants.IEM_APPROVAL_PROCESS_STATUS_PENDING
                                                     AND actorId = :UserInfo.getUserId()]){
                // Instantiate the new ProcessWorkitemRequest object and populate it
                Approval.ProcessWorkitemRequest req2 = new Approval.ProcessWorkitemRequest();
                req2.setAction(action);
                req2.setComments(commentMessage);
                //req2.setComments('Approving request.');
                
                req2.setWorkitemId(workItem.id);      
                // Submit the request for approval/Rejection
                Approval.ProcessResult result2 =  Approval.process(req2);
                return true;
            }
        }
        return false;
    }
    
    /*
    * Created by - Banshi
    * Wrapper class
    */
    public class CaseWrapper {
        @AuraEnabled
        public Boolean isActionOwner {get;set;} //Logged-In user is action owner or not
        @AuraEnabled
        public Boolean isRequester {get;set;} //Logged-In user is requester or not
        @AuraEnabled
        public Boolean isIssueOwner {get;set;} //Logged-In user is Issue Owner or not
        @AuraEnabled
        public String actionPlanStatus {get;set;}
        @AuraEnabled
        public String cloudName {get;set;}
        @AuraEnabled
        public String teamResponsibleName {get;set;}
        @AuraEnabled
        public String securityAssessmentRecordName {get;set;}
        @AuraEnabled
        public List<Milestone__c> milestoneList {get;set;} //List of related milestones
        @AuraEnabled
        public Case caseRecord {get;set;} //List of related milestones
        public CaseWrapper(){
            this.cloudName = '';
            this.teamResponsibleName = '';
            this.securityAssessmentRecordName = '';
            this.actionPlanStatus = '';
            this.caseRecord = new Case();
            this.milestoneList = new List<Milestone__c>();
        }
    }
}
public without sharing class SC_TrainingUtil {
    public static SC_TrainingBase.ProviderConnector connectorForProvider(SC_Training_Provider__c provider) {
        if (provider == null) { return null; }
        
        if (provider.Type__c == 'Trailhead') {
            return new SC_TrainingTrailheadConnector(provider);
        }
        return null;
    }
    
    public static SC_Training_Provider__c providerFor(String providerType) {
        SC_Training_Provider__c[] pvs = [SELECT Id, Last_Completed_Course_Sync__c, Last_Course_Sync__c, Type__c
                 FROM SC_Training_Provider__c WHERE Type__c =: providerType];
system.debug('>>> pvs.size(): ' + pvs.size());                         
system.debug('>>> pvs[0]: ' + pvs[0]);                                 
        if (pvs.size() == 0) {
            return null;
        }
        return pvs[0];
    }

    /**
     * Returns true if input is a valid 18 character user id
     * @param  input [description]
     * @return       [description]
     */
    public static Boolean isValidUserId(String input) {
        if (String.isBlank(input) || !input.startsWith('005') || input.length() != 18) {
            return false;
        }
        try {
            Id idVal = Id.valueOf(input);
            return idVal.getSobjectType() == User.getSObjectType();
        } catch (System.StringException e) {
            return false;
        }
    }

    public static SC_TrainingBase.ProviderConnector connectorForCourse(Training_Course__c course) {
        SC_Training_Provider__c provider = providerFor(Course.Provider__c);
        return connectorForProvider(provider);
    }
    
    public static List<List<String>> slice(List<String> objs, Integer size){
        return SFDCStringUtils.slice(objs, size);
    }
    
    public static List<List<SObject>> slice(List<SObject> objs, Integer size){
        /* Apex List class is missing a slice() method. 
         * This one is from https://success.salesforce.com/ideaView?id=08730000000BqGW
         */
        List<List<SObject>> resultList = new List<List<SObject>>();
        Integer numberOfChunks = objs.size() / size;
        for(Integer j = 0; j < numberOfChunks; j++ ){
            List<SObject> someList = new List<SObject>();
            for(Integer i = j * size; i < (j+1) * size; i++){
                someList.add(objs[i]);
            }
            resultList.add(someList);
        }

        if(numberOfChunks * size < objs.size()){
            List<SObject> aList = new List<SObject>();
            for(Integer k = numberOfChunks * size ; k < objs.size(); k++){
                aList.add(objs[k]);
            }
            resultList.add(aList);
        }
        return resultList;
    }
}
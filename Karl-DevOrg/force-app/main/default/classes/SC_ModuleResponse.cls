/*
 * Generated by JSON2Apex http://json2apex.herokuapp.com/
 * Module response
*/

global class SC_ModuleResponse {

    global Integer total_count;
  	global List<Data> data;

  	global class Roles {}

  	global class Data {
    		global Integer id;
    		global String api_name;
    		global String created_at;
    		global String updated_at;
    		global String title;
    		global String badge_icon;
    		global String description_html;
    		global String badge_title;
    		global String web_url;
    		global Boolean archived;
    		global List<Tags> tags;
    		global List<Tags> roles;
    		global List<Tags> products;
    		global List<Tags> levels;
    		global List<Units> units;
  	}

  	global class Tags {
    		global Integer id;
    		global String title;
  	}

  	global class Units {
    		global Integer id;
    		global String api_name;
    		global String created_at;
    		global String updated_at;
    		global String title;
    		global String description_html;
    		global String built_date;
    		global String web_url;
    		global Integer challenge_time;
    		global Integer total_points;
    		global Boolean archived;
    		global List<Tags> tags;
    		global List<Roles> roles;
    		global List<Roles> products;
    		global List<Roles> levels;
  	}

  	global static SC_ModuleResponse parse(String json) {
  		  return (SC_ModuleResponse) System.JSON.deserialize(json, SC_ModuleResponse.class);
  	}
}
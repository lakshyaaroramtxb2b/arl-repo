/**
 * @author Ben Harvie
 * @email ben.harvie@salesforce.com
 * @description This method is an adaptation of the MTX developed KARL_ShareFollowUpToAuditFirmBatch, extending
 * the capabilities to the Follow-up object (being released for the FY22 Spring Audits).
*/
public with sharing class KARL_ShareFollowUpToAuditFirmBatch implements Database.Batchable<sObject>,Schedulable {
    private String context;
    private Set<Id> firmIdSet;
    private String action;
    private Map<Id,Set<Id>> publicAuditFirmIdToContactIdMap;
    private Integer timeLength;
    private Boolean isAllTime;

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description method for scheduled (no parameters passed)
    */
    public KARL_ShareFollowUpToAuditFirmBatch(){
        context = 'ScheduledBatch';
        firmIdSet = new Set<Id>();
        publicAuditFirmIdToContactIdMap = new Map<Id,Set<Id>>();
        action = 'Add';
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description method for running context
     * @param runningContext - current state
     * @param firmSet - set of Audit Firms to share with
     * @param audFirmIdToContactIdMap - Map/Set of Audit Firms and their Contact Ids
     * @param actionToTake - Add or Remove
    */
    public KARL_ShareFollowUpToAuditFirmBatch(String runningContext,Set<Id> firmSet,Map<Id,Set<Id>> audFirmIdToContactIdMap,String actionToTake) {
        context = runningContext;
        firmIdSet = firmSet;
        publicAuditFirmIdToContactIdMap = audFirmIdToContactIdMap;
        action = actionToTake;
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description method for sharing old items (prior to class introduction)
     * @param daysLength - How far back to go (days)
     * @param isAll - Boolean for whether or not to process all
    */
    public KARL_ShareFollowUpToAuditFirmBatch(Integer daysLength, Boolean isAll){
        context = 'OldAuditorFollowUpItemsSharing';
        firmIdSet = new Set<Id>();
        publicAuditFirmIdToContactIdMap = new Map<Id,Set<Id>>();
        action = 'Add';
        timeLength = daysLength;
        isAllTime = isAll;
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description method quick action (aura button)
     * @param minutesLength - How far back to go (minutes)
    */
    public KARL_ShareFollowUpToAuditFirmBatch(Integer minutesLength){
        context = 'QuickAction';
        firmIdSet = new Set<Id>();
        publicAuditFirmIdToContactIdMap = new Map<Id,Set<Id>>();
        action = 'Add';
        timeLength = minutesLength;
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Start querying the database
     * @param BC - Batch context
     * @return QueryLocator value for the Batch Context
    */
    public Database.QueryLocator start(Database.BatchableContext BC) {
        String query = '';
        String filter = '';
        if(context == 'ScheduledBatch'){
            query = 'SELECT Id,KARL_Followup_Audit_Team__c, ' +
                    'KARL_Followup_Audit_Team__r.KARL_Audit_Firm__c ' +
                    'FROM KARL_Cycle_Follow_up__c '+
                    'WHERE CreatedDate = YESTERDAY '; 
        } 
        else if( context == 'Trigger'){
            if(firmIdSet != null && !firmIdSet.isEmpty()){
                query = 'SELECT Id,KARL_Followup_Audit_Team__c, ' +
                    'KARL_Followup_Audit_Team__r.KARL_Audit_Firm__c ' +
                    'FROM KARL_Cycle_Follow_up__c '+
                    'WHERE KARL_Followup_Audit_Team__r.KARL_Audit_Firm__c IN :firmIdSet '; //+ firmIds;
            }
        } else if( context == 'OldAuditorFollowUpItemsSharing'){
            query = 'SELECT Id,KARL_Followup_Audit_Team__c, ' +
                    'KARL_Followup_Audit_Team__r.KARL_Audit_Firm__c ' +
                    'FROM KARL_Cycle_Follow_up__c ';
            if(isAllTime){
                filter = '';
            } else{
                filter = ' WHERE CreatedDate = LAST_N_DAYS:'+timeLength;
            }
            query += filter;
        } else if( context == 'QuickAction'){
            Datetime nMinutesBack = Datetime.now().addMinutes(-1 * timeLength);
            query = 'SELECT Id,KARL_Followup_Audit_Team__c, ' +
                    'KARL_Followup_Audit_Team__r.KARL_Audit_Firm__c ' +
                    'FROM KARL_Cycle_Follow_up__c ' +
                    'WHERE CreatedDate >= :nMinutesBack';
            
        }
        System.debug(LoggingLevel.DEBUG, 'Query>>'+query);
        return Database.getQueryLocator(query);
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Execute the batch query for the followups provided
     * @param BC - Batch context
     * @param followUpItemList - List of all FollowUp items to be shared
    */
    public void execute(Database.BatchableContext BC, List<KARL_Cycle_Follow_up__c> followUpItemList) {
        if(context == 'ScheduledBatch' || context == 'OldAuditorFollowUpItemsSharing' || context == 'QuickAction'){
            Set<Id> auditFirmIdSet = new Set<Id>();
            Map<Id,Set<Id>> auditFirmIdToContactIdMap = new Map<Id,Set<Id>>();
            
            for(KARL_Cycle_Follow_up__c followUpItem : followUpItemList){
                auditFirmIdSet.add(followUpItem.KARL_Followup_Audit_Team__r.KARL_Audit_Firm__c);
            }

            for(KARL_Audit_Team_Contacts__c auditTeamCon : [SELECT Id,KARL_Audit_Team__c,KARL_Contact__c,
                                                            KARL_Audit_Team__r.KARL_Audit_Firm__c
                                                            FROM KARL_Audit_Team_Contacts__c 
                                                            WHERE KARL_Audit_Team__r.KARL_Audit_Firm__c IN : auditFirmIdSet]){ 
                if(!auditFirmIdToContactIdMap.containsKey(auditTeamCon.KARL_Audit_Team__r.KARL_Audit_Firm__c)){
                    auditFirmIdToContactIdMap.put(auditTeamCon.KARL_Audit_Team__r.KARL_Audit_Firm__c,new Set<Id>());
                }
                auditFirmIdToContactIdMap.get(auditTeamCon.KARL_Audit_Team__r.KARL_Audit_Firm__c).add(auditTeamCon.KARL_Contact__c);  
            }

            KARL_FollowUpSharingUtility.shareFollowUpItem(followUpItemList,auditFirmIdToContactIdMap);

        } else if(context == 'Trigger'){
            if(action == 'Add'){
                KARL_FollowUpSharingUtility.shareFollowUpItem(followUpItemList,publicAuditFirmIdToContactIdMap);
            } else if(action == 'Remove'){
                KARL_FollowUpSharingUtility.unShareFollowUpItem(followUpItemList,publicAuditFirmIdToContactIdMap);
            }
        }
        
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Post processing database steps (nothing presently)
     * @param BC - Batch context
    */
    public void finish(Database.BatchableContext BC) {
        // execute any post-processing operations like sending email
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Execute schedulable context
     * @param sc - scheduled context
    */
    public void execute(SchedulableContext sc) {
        KARL_ShareFollowUpToAuditFirmBatch b = new KARL_ShareFollowUpToAuditFirmBatch(); 
        Database.executeBatch(b);
    }
}
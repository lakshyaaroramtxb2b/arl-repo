/***************SBE_SLAToCaseStatusBatch****************
@Author Banshi
@Date 12-24-2019
@Modified by/ date - Swarnima singh Mandhata - T-09359 - 03/31/2020
@Description Batch class to create fetch SBE records on regular basis on certain creteria and then update crossponding case records status.
**********************************************/
 
public class SBE_SLAToCaseStatusBatch implements Database.Batchable<sObject> {
    @TestVisible
    private static list<TM_Security_Work_SLA_Extension_c__x> mockallSBEList = new list<TM_Security_Work_SLA_Extension_c__x>();//For test class
    
    public Database.QueryLocator start(Database.BatchableContext BC) {
        List<String> statusList = new List<String>();
        statusList.add(IEM_Constants.SBE_STATUS_DENIED);
        statusList.add(IEM_Constants.SBE_STATUS_CLOSED);
        statusList.add(IEM_Constants.SBE_STATUS_EXPIRED);
        statusList.add(IEM_Constants.SBE_STATUS_DRAFT);
        statusList.add(IEM_Constants.SBE_STATUS_VALIDATING_RESOLUTION);
        
        if(Test.isRunningTest()){
            return Database.getQueryLocator([SELECT Id FROM User LIMIT 1]);    
        }else{
            return Database.getQueryLocator([Select Id ,Security_Case_Number_c__c ,SA_Approver_c__c,TM_Work_Details_and_Steps_to_Reproduce_c__c ,TM_Corresponding_Work_c__c ,TM_Requested_Remediation_Date_c__c ,TM_SLA_Extension_Status_c__c,Technical_Description_of_Issue__c,Due_Date_Justification__c,Describe_Technical_Work_to_Remediate__c,TM_Definition_of_Done_c__c,TM_Approved_by_Business_c__c,TM_Approved_Remediation_Date_c__c,TM_Approved_by_Security_c__c,TM_Date_Submitted_for_Approval_c__c,TM_Resubmission_Date_c__c ,TM_Severity_Rating_Mapping_c__c ,TM_Date_Approved_c__c ,TM_Review_by_I_EM_analyst_begins_c__c,TM_Submitted_for_Business_Approval_c__c ,TM_Remediation_Plan_Author_c__c ,TM_Issue_Data_Classification_c__c ,TM_Link_to_Internal_Knowledge_Article_c__c ,TM_Work_Subject_c__c,TM_Remediation_Plan_Owner_c__c,TM_Work_Cloud_c__c ,ExternalId,LastModifiedDate__c From TM_Security_Work_SLA_Extension_c__x Where TM_SLA_Extension_Status_c__c IN :statusList AND  LastModifiedDate__c >=LAST_N_DAYS:1]);    
        }
    }
    public void execute(Database.BatchableContext BC, List<sObject> sbeRecords)
    {
        if(Test.isRunningTest()){
            sbeRecords =  mockallSBEList;
        }
        if(!sbeRecords.isEmpty()){
            Set<String> sbeIds = new Set<String>();
            for(TM_Security_Work_SLA_Extension_c__x sbeObject :(List<TM_Security_Work_SLA_Extension_c__x>)sbeRecords){
                 sbeIds.add(sbeObject.ExternalId);
            }
            Map<String,Case> sbeToCaseMap = new Map<String,Case>();
            for(Case c : [SELECT Id,SLA_Extension_Id__c,Status,IEM_Case_Type__c,Extension_Status__c FROM Case WHERE SLA_Extension_Id__c  IN : sbeIds]){
                if(Test.isRunningTest()){
                    sbeToCaseMap.put('EX101',c);
                }else{
                    sbeToCaseMap.put(c.SLA_Extension_Id__c,c);
                }
            }
            if(!sbeToCaseMap.isEmpty()) {
                List<Case> caseUpdateList = new List<Case>();
                for(TM_Security_Work_SLA_Extension_c__x sbeObject : (List<TM_Security_Work_SLA_Extension_c__x>)sbeRecords){
                    if(sbeToCaseMap.containsKey(sbeObject.ExternalId)){
                        Case caseToUpdate =  sbeToCaseMap.get(sbeObject.ExternalId);
                        caseToUpdate = SBE_MappingUtility.mapSLAToCase(caseToUpdate,sbeObject);
                        String caseStatusToUpdate;
                        if( sbeObject.TM_SLA_Extension_Status_c__c == IEM_Constants.SBE_STATUS_DENIED || sbeObject.TM_SLA_Extension_Status_c__c == IEM_Constants.SBE_STATUS_CLOSED || sbeObject.TM_SLA_Extension_Status_c__c == IEM_Constants.SBE_STATUS_DRAFT){
                            caseStatusToUpdate = IEM_Constants.IEM_CASE_STATUS_CLOSED;
                        }else if( sbeObject.TM_SLA_Extension_Status_c__c == IEM_Constants.SBE_STATUS_VALIDATING_RESOLUTION ){
                            caseStatusToUpdate = IEM_Constants.IEM_CASE_STATUS_VALIDATING_RESOLUTION;
                        }
                        if( sbeObject.TM_SLA_Extension_Status_c__c == IEM_Constants.SBE_STATUS_EXPIRED ){
                            if(caseToUpdate.Status != IEM_Constants.IEM_CASE_STATUS_MONITORING){
                                caseStatusToUpdate = IEM_Constants.IEM_CASE_STATUS_MONITORING;
                            }
                        }
                        if(caseToUpdate.IEM_Case_Type__c == IEM_Constants.IEM_EXCEPTION){
                            caseToUpdate.Extension_Status__c  = IEM_Constants.SBE_STATUS_EXTENSION_EXPIRED;
                        }
                        if(String.isNotEmpty(caseStatusToUpdate) && caseStatusToUpdate !=  caseToUpdate.Status ){
                            caseToUpdate.Status = caseStatusToUpdate;
                        }
                         caseUpdateList.add(caseToUpdate);
                    }
                }
                System.debug('##caseUpdateList##'+caseUpdateList);
                update caseUpdateList;
            }  
        }
        
    }
    
    public void finish(Database.BatchableContext BC) {
        // finish code
        SBE_SLAToCaseStatusBatch batch = new SBE_SLAToCaseStatusBatch();
        if(!Test.isRunningTest()){
            System.scheduleBatch(batch, 'SBE_SLAToCaseStatusBatch',5);
        }
        
    }
    
}
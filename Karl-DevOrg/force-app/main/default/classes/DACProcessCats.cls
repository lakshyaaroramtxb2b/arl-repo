public class DACProcessCats {

    public void StartA()
    {
        try {
            
        }
        catch (Exception ex) {
                Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
                message.toAddresses = new String[] { 'amcneilly@salesforce.com' };
                message.subject = 'DSECCategoryEnrichTrigger Error !!!!!!';
                message.plainTextBody = ex.getStackTraceString() + ' on line ' + ex.getLineNumber();
                Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage> {message};
                Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
         }
    }
    
    public void StartB()
    {
        try {
    
             List<Detection_Security_Event_Criteria__c> dsecs = [SELECT Id, V2SplunkSearch__c, Category__c  
                                                                 FROM Detection_Security_Event_Criteria__c];
             
             List<Detection_Security_Event_Criteria__c> dsecsToUpdate = new List<Detection_Security_Event_Criteria__c>();
             Set<Detection_Alert_Criteria__c> dacsToUpdate = new Set<Detection_Alert_Criteria__c>();
             
             List<DAC_linked_DSEC__c> linkedDACs = [SELECT DAC__c, DSEC__c, DAC__r.Categories__c
                                                    FROM DAC_linked_DSEC__c];
            
             List<SourceType_to_Category__c> sourceTypeMaps = [SELECT Category__c, Sourcetypes__c 
                                                               FROM SourceType_to_Category__c];
            
             for(Detection_Security_Event_Criteria__c dsec : dsecs)
             {
                 //only if V2SplunkSearch__c is new
                if( !String.isEmpty(dsec.V2SplunkSearch__c)  )
                {
                    Pattern p = Pattern.compile('sourcetype=[^ ]*');
                    Matcher pm = p.matcher(dsec.V2SplunkSearch__c);
                     
                    Set<String> matchedCateogires = new Set<String>();
      
                    while( pm.find() )
                    {
                        String rawStrType = pm.group();
                        
                        if(!String.isEmpty(rawStrType))
                        {
                            String foundSourceType = rawStrType.substring( rawStrType.indexOf('=') + 1, rawStrType.length());
                            
                            if(!String.isEmpty(foundSourceType))
                            {
                                //find approate map values
                                for(SourceType_to_Category__c sourceTypeMap : sourceTypeMaps)
                                {
                                    //loop over picklist
                                    for(string sourceType : sourceTypeMap.Sourcetypes__c.split(';'))
                                    {
                                        if(foundSourceType.contains('*'))
                                        {
                                            //e.g. sourcetype=sfdc* then sfdc-it sfdc-dc etc
                                            if(sourceType.toLowerCase().contains(foundSourceType.toLowerCase()))
                                            {
                                                if(!matchedCateogires.contains(sourceTypeMap.Category__c ))
                                                    matchedCateogires.add( sourceTypeMap.Category__c );
                                            }
                                        }
                                        else
                                        {
                                            if(sourceType.toLowerCase() == foundSourceType.toLowerCase())
                                            {
                                                if(!matchedCateogires.contains(sourceTypeMap.Category__c ))
                                                    matchedCateogires.add( sourceTypeMap.Category__c );
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                     
                     dsec.Category__c = '';
                     
                     //update dac
                     for(string category : matchedCateogires)
                     {
                        dsec.Category__c += category + ';';
                     }
    
                    dsecsToUpdate.add(dsec);
                    
                    //Now update DACS's that use this DSEC
                    //======================================
                    
                    for(DAC_linked_DSEC__c linkedDAC : linkedDACs)
                    {
                        if(linkedDAC.DSEC__c == dsec.Id)
                        {
                            //post.Body += '\nChecking ' + linkedDAC.DAC__c;
                            
                            if(String.isEmpty(linkedDAC.DAC__r.Categories__c))
                            {
                                //add all
                                //post.Body += '\nadding all';
                                
                                for( string dsecCat : dsec.Category__c.split(';'))
                                {
                                    //post.Body += '\nadding ' + dsecCat;
                                    
                                    //querk cannot += if no values otherwise get a null value
                                    if(String.isEmpty(linkedDAC.DAC__r.Categories__c))
                                    {
                                        //post.Body += '\na';
                                        linkedDAC.DAC__r.Categories__c = dsecCat + ';';
                                    }
                                    else
                                    {
                                        //post.Body += '\nb';
                                        linkedDAC.DAC__r.Categories__c += dsecCat + ';';
                                    }
                                        
                                    if(!dacsToUpdate.contains(linkedDAC.DAC__r))
                                        dacsToUpdate.add(linkedDAC.DAC__r);
                                }
                            }
                            else
                            {              
                                //add subset
                                //post.Body += '\nadding subset';
                                
                                Set<string> dacCats = new Set<String>(linkedDAC.DAC__r.Categories__c.split(';'));
                                
                                for( string dsecCat : dsec.Category__c.split(';'))
                                {
                                    if(!dacCats.contains(dsecCat))
                                    {
                                        //if it does not contain the cats we created above then add
                                        linkedDAC.DAC__r.Categories__c += dsecCat + ';';
                                        
                                        if(!dacsToUpdate.contains(linkedDAC.DAC__r))
                                            dacsToUpdate.add(linkedDAC.DAC__r);
                                    }
                                }
                            }
                        }
                    }
                    
                    //INSERT post;
                }
             }
             
             //UPDATE dsecsToUpdate;
             //UPDATE new List<Detection_Alert_Criteria__c>(dacsToUpdate);
         }
         catch (Exception ex) {
                Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
                message.toAddresses = new String[] { 'amcneilly@salesforce.com' };
                message.subject = 'DSECCategoryEnrichTrigger Error !!!!!!';
                message.plainTextBody = ex.getStackTraceString() + ' on line ' + ex.getLineNumber();
                Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage> {message};
                Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
         }
    }
    
}
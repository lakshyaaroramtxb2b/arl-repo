@IsTest
public class EmployeeManagerLadderTest {

    @IsTest
    static void testBatch() {
        List<INTEG_Employee__c> lstEmployee = new List<INTEG_Employee__c>();
        INTEG_Employee__c emp = new INTEG_Employee__c(Name='Suresh',INTEG_Manager_ID__c='0057000000hJXXX',
                                                      INTEG_Org62UserId__c ='00579393933gqugI',
                                                      INTEG_Last_Name__c='Suresh',isContractor__c=true);
        lstEmployee.add(emp);

        emp = new INTEG_Employee__c(Name='Jorge',INTEG_Manager_ID__c='0057000000hJYYY',
                                                      INTEG_Org62UserId__c ='0057000000hJXXX',
                                                      INTEG_Last_Name__c='Jorge',isContractor__c=true);
        lstEmployee.add(emp);

        emp = new INTEG_Employee__c(Name='Frank',INTEG_Manager_ID__c='0057000000hJZZZ',
                                                      INTEG_Org62UserId__c ='0057000000hJYYY',
                                                      INTEG_Last_Name__c='Frank',isContractor__c=true);
        lstEmployee.add(emp);

        emp = new INTEG_Employee__c(Name='Kurt',INTEG_Manager_ID__c='0057000000hJAAA',
                                                      INTEG_Org62UserId__c ='0057000000hJZZZ',
                                                      INTEG_Last_Name__c='Kurt',isContractor__c=true);
        lstEmployee.add(emp);

        emp = new INTEG_Employee__c(Name='Jim',INTEG_Manager_ID__c='0057000000hJBBB',
                                                      INTEG_Org62UserId__c ='0057000000hJAAA',
                                                      INTEG_Last_Name__c='Jim',isContractor__c=true);

        lstEmployee.add(emp);

        emp = new INTEG_Employee__c(Name='Randy',INTEG_Manager_ID__c='0057000000hJCCC',
                                                      INTEG_Org62UserId__c ='0057000000hJBBB',
                                                      INTEG_Last_Name__c='Randy',isContractor__c=true);

        lstEmployee.add(emp);

        emp = new INTEG_Employee__c(Name='Parker',INTEG_Manager_ID__c='0057000000hJDDD',
                                                      INTEG_Org62UserId__c ='0057000000hJCCC',
                                                      INTEG_Last_Name__c='Parker',isContractor__c=true);

        lstEmployee.add(emp);

        emp = new INTEG_Employee__c(Name='Marc',INTEG_Manager_ID__c='',
                                                      INTEG_Org62UserId__c ='0057000000hJDDD',
                                                      INTEG_Last_Name__c='Marc',isContractor__c=true);

        lstEmployee.add(emp);

        Insert lstEmployee;

        Test.startTest();
            EmployeeManagerLadder batchjob = new EmployeeManagerLadder();
            Database.executeBatch(batchjob);
        Test.stopTest();

        emp = [SELECT Id,INTEG_Manager_Level_1__c, INTEG_Manager_Level_2__c,
                      INTEG_Manager_Level_3__c, INTEG_Manager_Level_4__c, 
                      INTEG_Manager_Level_5__c, INTEG_Manager_Level_6__c,
                      INTEG_Manager_Level_7__c
                      FROM INTEG_Employee__c Where Name='Suresh'];

        
        system.assertEquals(emp.INTEG_Manager_Level_1__c,'Marc');
        system.assertEquals(emp.INTEG_Manager_Level_2__c,'Parker');
        system.assertEquals(emp.INTEG_Manager_Level_3__c,'Randy');
        system.assertEquals(emp.INTEG_Manager_Level_4__c,'Jim');
        system.assertEquals(emp.INTEG_Manager_Level_5__c,'Kurt');
        system.assertEquals(emp.INTEG_Manager_Level_6__c,'Frank');
        system.assertEquals(emp.INTEG_Manager_Level_7__c,'Jorge');

        emp = [SELECT Id,INTEG_Manager_Level_1__c, INTEG_Manager_Level_2__c,
                      INTEG_Manager_Level_3__c, INTEG_Manager_Level_4__c, 
                      INTEG_Manager_Level_5__c, INTEG_Manager_Level_6__c,
                      INTEG_Manager_Level_7__c
                      FROM INTEG_Employee__c Where Name='Jorge'];

        system.assertEquals(emp.INTEG_Manager_Level_1__c,'Marc');
        system.assertEquals(emp.INTEG_Manager_Level_2__c,'Parker');
        system.assertEquals(emp.INTEG_Manager_Level_3__c,'Randy');
        system.assertEquals(emp.INTEG_Manager_Level_4__c,'Jim');
        system.assertEquals(emp.INTEG_Manager_Level_5__c,'Kurt');
        system.assertEquals(emp.INTEG_Manager_Level_6__c,'Frank');


        emp = [SELECT Id,INTEG_Manager_Level_1__c, INTEG_Manager_Level_2__c,
                      INTEG_Manager_Level_3__c, INTEG_Manager_Level_4__c, 
                      INTEG_Manager_Level_5__c, INTEG_Manager_Level_6__c,
                      INTEG_Manager_Level_7__c
                      FROM INTEG_Employee__c Where Name='Marc'];

        system.assertEquals(emp.INTEG_Manager_Level_1__c,null);



    }

}
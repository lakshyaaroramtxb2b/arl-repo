public class addscantest {
   
    public Boolean IS_DELAYED = False;
    public String scantype {get; set;} 
    public static Integer MIN_SCAN_WAIT_HOURS = 1;
    public String btn { get; set; }
    public String enteredText {get; set;}    
    
    public addscantest() {
        if (IS_DELAYED== True) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'At this time the Force.com Security Source Code Scanner is experiencing delays.  Expect delays as we work through this issue.'));
        }
    }
    public PageReference process() {
        if(enteredText == null)
        {
            return null;
        }
        Pattern validEmail = Pattern.compile('^.+@.+$');
        Pattern whiteSpace = Pattern.compile('^\\s*$');
        Matcher emailMatcher = validEmail.matcher(enteredText);
        if ((enteredText == null) || 
            (validEmail.matcher(enteredText).matches() == False) || (whiteSpace.matcher(enteredText).matches()==True)){
            //PageReference p = new PageReference('/sourcescanner#invalid');
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'The username submitted is not a valid Salesforce username.'));
            //return p;
            return null;
        }
        
        // Enforce throttling and only allow scans every MIN_SCAN_WAIT_HOURS
        Datetime newest_allowed = Datetime.now().addHours(0-MIN_SCAN_WAIT_HOURS);
        List<CodeScan__c> latest = [SELECT Id,Created__c FROM CodeScan__c WHERE (Created__c != null) 
                                           AND (Created__c >:newest_allowed) 
                                           AND (Username__c =: enteredText)
                                           ORDER BY Created__c DESC
                                           LIMIT 1];
        if (latest.size() > 0){
            // They have one pending or completed already
            String nextAllowed = latest.get(0).Created__c.addHours(MIN_SCAN_WAIT_HOURS).format('MMM d, h:m a');
            String message = 'Due to high volume, an application can only be scanned every '+String.valueOf(MIN_SCAN_WAIT_HOURS);
            message += ' hour.  Please submit the application again after '+nextAllowed+'.  We apologize for any inconvenience.';
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,message));
            return null;
        }
        
        CodeScan__c scan = new CodeScan__c();
        scan.Username__c = enteredText;
        scan.ScanType__c = scantype;
        insert scan;
        PageReference pa = new PageReference('/sourcescanner#complete');
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Thank you for submitting a request for code to be scanned.  You will receive a report to the email address we have on record.  If you encounter any issues with the results, please email securecloud [at] salesforce [dot] com.'));
        //return pa;
        return null;
    }
    
    public List<SelectOption> getscantypes()
    {
      List<SelectOption> options = new List<SelectOption>();
        
       Schema.DescribeFieldResult fieldResult =
             CodeScan__c.ScanType__c.getDescribe();
       List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        
       for( Schema.PicklistEntry f : ple)
       {
          options.add(new SelectOption(f.getLabel(), f.getValue()));
       }       
       return options;
    }
    
        public  void setUser(String s)
    { enteredText = s; }
    
    public  string getUser()
    { return enteredText; }

}
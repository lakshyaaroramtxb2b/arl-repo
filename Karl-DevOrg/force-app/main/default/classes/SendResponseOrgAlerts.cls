public class SendResponseOrgAlerts implements Queueable, Database.AllowsCallouts{
    //private AuthToken__c currentToken = [SELECT Instance_URL__c, Token__c, Last_Update__c FROM AuthToken__c WHERE Name__c = 'ResponseOrg'];
    private String jsonBody;
    public class ResponseOrgCalloutException extends Exception {}
    
    public SendResponseOrgAlerts(List<CSIRTAlert> alertList){
        this.jsonBody = JSON.serialize(alertList);         
    }
    

    
    public void execute(QueueableContext context){
        /*
        Boolean tokenValidity = HandleResponseOrgConn.checkTokenValidity(this.currentToken);
        if(!tokenValidity){
            System.debug('Current Token not Valid');
            this.currentToken = ResponseOrgCallout.updateToken(this.currentToken);
        }
        
        String endpoint = this.currentToken.Instance_URL__c + '/services/apexrest/CSIRT/CreateDetectionAlert/';
        */
        String endpoint = 'callout:Response_Org/services/apexrest/CSIRT/CreateDetectionAlert/';
        HttpRequest request = new HttpRequest();
            
        request.setEndpoint(endpoint);
        request.setMethod('POST');
        
        request.setBody(this.jsonBody);
        system.debug('jsonBody: ' + this.jsonBody);
            
        //request.setHeader('Authorization', 'Bearer ' + this.currentToken.Token__c);
        request.setHeader( 'Content-Type', 'application/json' );
        request.setHeader( 'Accept', 'application/json' );
        Http http = new Http();
        HttpResponse response = http.send(request);
        if(response.getStatusCode() == 200){
            String respString = response.getBody();
        } else {
            throw new ResponseOrgCalloutException('ResponseOrg couldn\'t create an alert.' + response.getStatusCode() + ' ' + response.getStatus() + ' ' + response.getBody());
        }
        /*
        if(!tokenValidity){
            update this.currentToken; //Update the AuthToken record.
        }
		*/
    }
}
public with sharing class KARL_AuditorRequestItemTriggerHelper {
    public static set<String> running;
    
    /**
     * @author Mahima Aggarwal
     * @email mahima.aggarwal@mtxb2b.com
     * @desc This method get triggered from auditor request item trigger.
    */
    public static void run(){
        //After Update
        if( Trigger.isAfter && Trigger.isUpdate ){
            // Close Cycle Request Item disabled - due to sharing Auditors cannot see other ARI status.
            //closeCycleRequestItem((List<KARL_Auditor_Request_Item__c >)Trigger.new, (Map<Id, KARL_Auditor_Request_Item__c>)Trigger.oldMap);
        }
        //Before Update
        if( Trigger.isBefore && Trigger.isUpdate ){
            checkIfExternalAuditorCanUpdate((List<KARL_Auditor_Request_Item__c >)Trigger.new);
            createFollowUpRecordOnStatusChange((List<KARL_Auditor_Request_Item__c >)Trigger.new, (Map<Id,KARL_Auditor_Request_Item__c>)Trigger.oldMap);
        }
        //After Insert
        else if( Trigger.isAfter && Trigger.isInsert ){
            
        }
        //Before Insert
        else if( Trigger.isBefore && Trigger.isInsert ){
            updateAudtiorRequestItems((List<KARL_Auditor_Request_Item__c >)Trigger.new);
        }
    }

    /**
     * @author Lakshya Arora
     * @email lakshya.arora@mtxb2b.com
     * @desc This method create a new follow up record when status changes to followup
    */
    private static void createFollowUpRecordOnStatusChange(List<KARL_Auditor_Request_Item__c > newList,Map<Id,KARL_Auditor_Request_Item__c> oldMap){
        Set<Id> auditorReqItemIdSet = new Set<Id>();
        for(KARL_Auditor_Request_Item__c auditorReqItemObj : newList){
            if(auditorReqItemObj.KARL_Status__c == KARL_Constants.KARL_AUDITOR_STATUS_FOLLOWUPS && oldMap != null && oldMap.get(auditorReqItemObj.Id).KARL_Status__c != auditorReqItemObj.KARL_Status__c){
                auditorReqItemIdSet.add(auditorReqItemObj.Id);
            }
        }
        if(!auditorReqItemIdSet.isEmpty()){
             // As accessing User object fields, don't need Security Enforced
            User userinfoObj = [SELECT Id,ContactId FROM User WHERE Id =: UserInfo.getUserId()];

            //  prepare initial email steps
            Map<String,String> BASEURLMAP = KARL_Utility.getBaseURL();
            String BASERECORDURL = BASEURLMAP.get('baseRecordURL');
            String BASEURLSUFFIX = BASEURLMAP.get('baseUrlSuffix');
            FINAL String ORGWIDEDISPLAYNAME = System.label.SCEA_KARL;
            
            List<Messaging.SingleEmailMessage> mailList = new List<Messaging.SingleEmailMessage>();
            List<OrgWideEmailAddress> orgwideEmailAddressList = KARL_Utility.getOrgwideEmailAddressIdByDisplayName(ORGWIDEDISPLAYNAME);

            // As accessing Email Template Object, don't need Security Enforced
            EmailTemplate emailTemplateObj = [SELECT id,name,body,developername,htmlvalue ,Subject FROM EmailTemplate WHERE developername = 'KARL_Auditor_Request_Item_Followup'];
            String currentUserContactId;
            if(String.isNotBlank(userinfoObj.ContactId)){
                currentUserContactId = userinfoObj.ContactId;
            }
            // prepare initial email steps end
            Map<Id,KARL_Cycle_Follow_up__c> auditorRequestItemIdToFollowUpMap = new Map<Id,KARL_Cycle_Follow_up__c>();
            Map<Id,Contact> auditorRequestItemIdTOGRCAssigneeMap = new Map<Id,Contact>();
            for(KARL_Auditor_Request_Item__c auditorReqItemObj : [SELECT Id,KARL_Auditor_Status_Comments__c,KARL_Cycle_Request_Item__r.Audit_Cycle__c,
                                                                    KARL_Audit_Team__c,KARL_Primary_Scope__c,Request_Master_Data__r.KARL_GRC_Assignee__r.Name,Request_Master_Data__r.KARL_GRC_Assignee__r.Id 
                                                                    FROM KARL_Auditor_Request_Item__c 
                                                                    WHERE Id IN :auditorReqItemIdSet
                                                                    WITH SECURITY_ENFORCED]){
                KARL_Cycle_Follow_up__c cycleFollowUpObj = new KARL_Cycle_Follow_up__c();
                cycleFollowUpObj.KARL_Followup_Description__c = auditorReqItemObj.KARL_Auditor_Status_Comments__c;
                cycleFollowUpObj.KARL_Followup_Auditor_POC__c = currentUserContactId;
                cycleFollowUpObj.KARL_Followup_Audit_Cycle__c = auditorReqItemObj.KARL_Cycle_Request_Item__r.Audit_Cycle__c;
                cycleFollowUpObj.KARL_Followup_Audit_Team__c = auditorReqItemObj.KARL_Audit_Team__c;
                cycleFollowUpObj.KARL_Audit_Scope__c = auditorReqItemObj.KARL_Primary_Scope__c;
                cycleFollowUpObj.KARL_Follow_up_Type__c = KARL_Constants.FOLLOWUPTYPE_EVIDENCEQUESTION;
                cycleFollowUpObj.KARL_Follow_up_Status__c = KARL_Constants.FOLLOWUPSTATUS_NEW;
                if(auditorReqItemObj?.Request_Master_Data__r.KARL_GRC_Assignee__c != null){
                    auditorRequestItemIdTOGRCAssigneeMap.put(auditorReqItemObj.Id,auditorReqItemObj.Request_Master_Data__r.KARL_GRC_Assignee__r);
                }
                auditorRequestItemIdToFollowUpMap.put(auditorReqItemObj.Id,cycleFollowUpObj);
            }
            if(!auditorRequestItemIdToFollowUpMap.isEmpty()){
                insert auditorRequestItemIdToFollowUpMap.values();
                for(KARL_Auditor_Request_Item__c auditorReqItem : newList){
                    if(auditorRequestItemIdToFollowUpMap.containsKey(auditorReqItem.Id)){
                        auditorReqItem.Follow_up__c = auditorRequestItemIdToFollowUpMap.get(auditorReqItem.Id).Id;
                        if(!orgwideEmailAddressList.isEmpty()){
                            String htmlBody = emailTemplateObj.htmlvalue;
                            emailTemplateObj.Subject = emailTemplateObj.Subject.replace('{!ariName}',auditorReqItem.Name);
                            htmlBody = htmlBody.replace('{!ariName}',auditorReqItem.Name);
                            String grcAssigneeName = '';
                            if(auditorRequestItemIdTOGRCAssigneeMap.containsKey(auditorReqItem.Id)){
                                grcAssigneeName = auditorRequestItemIdTOGRCAssigneeMap.get(auditorReqItem.Id).Name;
                            }
                            htmlBody = htmlBody.replace('{!grcAssignee}',grcAssigneeName);
                            htmlBody = htmlBody.replace('{!followUpLink}',BASERECORDURL+auditorReqItem.Follow_up__c+BASEURLSUFFIX);
                            List<String> toAddressList = new List<String>();
                            if(auditorRequestItemIdTOGRCAssigneeMap.containsKey(auditorReqItem.Id)){
                                toAddressList.add(auditorRequestItemIdTOGRCAssigneeMap.get(auditorReqItem.Id).Id);
                            }
                            if(!toAddressList.isEmpty())
                            mailList.add(KARL_Utility.createMessage(new Messaging.SingleEmailMessage(),emailTemplateObj,htmlBody,toAddressList,null,orgwideEmailAddressList[0].Id));      
                        }
                    }
                }
                if(!mailList.isEmpty()){
                    Messaging.SendEmailResult[] emailResults =  Messaging.sendEmail( mailList, false);
                    for(Messaging.SendEmailResult result : emailResults){
                        if(!result.isSuccess()){
                            System.debug(LoggingLevel.DEBUG,'Error Received');
                            for(Database.Error error : result.getErrors()){
                                System.debug(LoggingLevel.DEBUG,error.getMessage());
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * @author Mahima Aggarwal
     * @email mahima.aggarwal@mtxb2b.com
     * @desc This method checks that if cycle request item's status is not provided to auditor the
     * external auditor cannot update it
    */
    private static void checkIfExternalAuditorCanUpdate(List<KARL_Auditor_Request_Item__c > newList){

        if(!FeatureManagement.checkPermission('ARL_Is_External_Auditor')){
            return;
        }

        Set<Id> cycleReqItemSet = new Set<Id>();
        Map<Id, Cycle_Request_Item__c> cycleReqItemIdTocycleReqItemMap = new Map<Id, Cycle_Request_Item__c>();
        
        for(KARL_Auditor_Request_Item__c auditorReqItem : newList){
            if(auditorReqItem.KARL_Cycle_Request_Item__c != null){
                cycleReqItemSet.add(auditorReqItem.KARL_Cycle_Request_Item__c);
            }
        }
        for(Cycle_Request_Item__c cycleReqItem : [ SELECT Id,Cycle_Request_Status__c
                                                FROM Cycle_Request_Item__c
                                                WHERE Id IN :cycleReqItemSet]){
            cycleReqItemIdTocycleReqItemMap.put(cycleReqItem.Id,cycleReqItem);
        }
        // Ben Harvie
        // Enabling ARI status updates to occur when CREQ != 'Provided to Auditor'
        for(KARL_Auditor_Request_Item__c auditorReqItem : newList){
            if(auditorReqItem.KARL_Cycle_Request_Item__c != null && (
            cycleReqItemIdTocycleReqItemMap.get(auditorReqItem.KARL_Cycle_Request_Item__c).Cycle_Request_Status__c != 'Provided to Auditor' &&
            cycleReqItemIdTocycleReqItemMap.get(auditorReqItem.KARL_Cycle_Request_Item__c).Cycle_Request_Status__c != 'Follow-ups' &&
            cycleReqItemIdTocycleReqItemMap.get(auditorReqItem.KARL_Cycle_Request_Item__c).Cycle_Request_Status__c != 'Not Applicable' &&
            cycleReqItemIdTocycleReqItemMap.get(auditorReqItem.KARL_Cycle_Request_Item__c).Cycle_Request_Status__c != 'Closed')){
                auditorReqItem.addError(Label.KARL_Cannot_Update_Auditor_Req_Item);
            }
        }

    }

    /**
     * @author Mahima Aggarwal
     * @email mahima.aggarwal@mtxb2b.com
     * @desc This method updates status of cycle request item to closed when Auditor_Request_Item's status
     * is set to testing completed
    */
    /*private static void closeCycleRequestItem(List<KARL_Auditor_Request_Item__c > newList, Map<Id, KARL_Auditor_Request_Item__c> oldMap){
        Set<Id> cycleReqItemSet = new Set<Id>();
        List<Cycle_Request_Item__c> updtCycleReqItems = new List<Cycle_Request_Item__c>();
        Map<Id,List<KARL_Auditor_Request_Item__c>> cycleReqIdVsAuditorReqItemsMap = new Map<Id,List<KARL_Auditor_Request_Item__c>>();
        Set<Id> updtCycleReqItemSet = new Set<Id>();
        
        for(KARL_Auditor_Request_Item__c auditorReqItem : newList){
            if(auditorReqItem.KARL_Cycle_Request_Item__c != null &&
               auditorReqItem.KARL_Status__c != oldMap.get(auditorReqItem.Id).KARL_Status__c){
                cycleReqItemSet.add(auditorReqItem.KARL_Cycle_Request_Item__c);
            }
        }

        List<KARL_Auditor_Request_Item__c> allAuditorReqItem = [SELECT Id,KARL_Status__c,KARL_Cycle_Request_Item__c,
                                                                KARL_Cycle_Request_Item__r.Cycle_Request_Status__c
                                                                FROM KARL_Auditor_Request_Item__c
                                                                WHERE KARL_Cycle_Request_Item__c IN :cycleReqItemSet];

        for(KARL_Auditor_Request_Item__c auditorReqItem : allAuditorReqItem){
            if(!cycleReqIdVsAuditorReqItemsMap.containsKey(auditorReqItem.KARL_Cycle_Request_Item__c)){
                cycleReqIdVsAuditorReqItemsMap.put(auditorReqItem.KARL_Cycle_Request_Item__c,new List<KARL_Auditor_Request_Item__c>());
            }
            cycleReqIdVsAuditorReqItemsMap.get(auditorReqItem.KARL_Cycle_Request_Item__c).add(auditorReqItem);
        }

        for(Id cycleReqItemId : cycleReqIdVsAuditorReqItemsMap.keySet()){
            Boolean flag = true;
            if(cycleReqIdVsAuditorReqItemsMap.get(cycleReqItemId) != null &&
               !cycleReqIdVsAuditorReqItemsMap.get(cycleReqItemId).isEmpty() ){

                for(KARL_Auditor_Request_Item__c auditorReqItem : cycleReqIdVsAuditorReqItemsMap.get(cycleReqItemId)){
                    if(auditorReqItem.KARL_Status__c != 'Testing Completed'){
                        flag = false;
                        break;
                    }
                }

                if(flag){
                    updtCycleReqItemSet.add(cycleReqItemId);
                }
            }
        }


        updtCycleReqItems = [ SELECT Id,Cycle_Request_Status__c
                            FROM Cycle_Request_Item__c
                            WHERE Id IN :updtCycleReqItemSet];
        for(Cycle_Request_Item__c cycleReqItem : updtCycleReqItems){
            cycleReqItem.Cycle_Request_Status__c = 'Closed';
        }
        if( !updtCycleReqItems.isEmpty() && Schema.sObjectType.Cycle_Request_Item__c.isUpdateable() 
               && Cycle_Request_Item__c.Cycle_Request_Status__c.getDescribe().isUpdateable()){
                update updtCycleReqItems;
        }

    }*/

    /**
     * @author Mahima Aggarwal
     * @email mahima.aggarwal@mtxb2b.com
     * @desc This method updates all Auditor Request Items to get in sync with cycle request item
    */
    private static void updateAudtiorRequestItems(List<KARL_Auditor_Request_Item__c > newList){
        Set<Id> cycleReqItemSet = new Set<Id>();
        Map<Id, Cycle_Request_Item__c> cycleReqItemIdTocycleReqItemMap = new Map<Id, Cycle_Request_Item__c>();
        
        for(KARL_Auditor_Request_Item__c auditorReqItem : newList){
            if(auditorReqItem.KARL_Cycle_Request_Item__c != null){
                cycleReqItemSet.add(auditorReqItem.KARL_Cycle_Request_Item__c);
            }
        }
        for(Cycle_Request_Item__c cycleReqItem : [ SELECT Id,Request_Area__c,Request_Description__c,
                                                SFDC_Comments__c, Request__c 
                                                FROM Cycle_Request_Item__c
                                                WHERE Id IN :cycleReqItemSet]){
            cycleReqItemIdTocycleReqItemMap.put(cycleReqItem.Id,cycleReqItem);
        }
        for(KARL_Auditor_Request_Item__c auditorReqItem : newList){
            if(auditorReqItem.KARL_Cycle_Request_Item__c != null){
                auditorReqItem.KARL_SFDC_Comments__c = cycleReqItemIdTocycleReqItemMap.get(auditorReqItem.KARL_Cycle_Request_Item__c).SFDC_Comments__c;
                auditorReqItem.KARL_Request_Description__c = cycleReqItemIdTocycleReqItemMap.get(auditorReqItem.KARL_Cycle_Request_Item__c).Request_Description__c;
                auditorReqItem.KARL_Request_Area__c = cycleReqItemIdTocycleReqItemMap.get(auditorReqItem.KARL_Cycle_Request_Item__c).Request_Area__c;
                auditorReqItem.Request_Master_Data__c = cycleReqItemIdTocycleReqItemMap.get(auditorReqItem.KARL_Cycle_Request_Item__c).Request__c;
            }
        }

    }
}
public with sharing class PRT_V2MOMChangeRequestCtrl {

    @TestVisible public static List<V2MOM_Method_c__x> mockedMethodList = new List<V2MOM_Method_c__x>();
    @TestVisible public static List<Measure_c__x> mockedMeasureList = new List<Measure_c__x>();

    public class RecordsData {
        @AuraEnabled public String id;
        @AuraEnabled public String name; 
        @AuraEnabled public String descOrComm;
        public RecordsData() {}
        public RecordsData(String id, String name, String descOrComm) {
            this.id = id;
            this.name = name;
            this.descOrComm = descOrComm;
        }
    }

    public class MethodAndMeasureData {
        @AuraEnabled public String v2MOMName;
        @AuraEnabled public String v2MOMId;
        @AuraEnabled public String portfolioName;
        @AuraEnabled public String portfolioId;
        @AuraEnabled public String recordId;
        @AuraEnabled public String methodOrMeasureName;
        @AuraEnabled public String descOrComment;
        @AuraEnabled public String measureTargetValue;
        @AuraEnabled public String detailedJustification;
        @AuraEnabled public String targetDateOfChange;
        @AuraEnabled public String changeRequestStatus;
        @AuraEnabled public Boolean isMethod;

        public MethodAndMeasureData(String v2MOMName, String v2MOMId, String portfolioId, String portfolioName,
                                    String recordId, String methodOrMeasureName, String descOrComment, String measureTargetValue,
                                    String detailedJustification, String targetDateOfChange, String changeRequestStatus, Boolean isMethod) {
            this.v2MOMName = v2MOMName;
            this.v2MOMId = v2MOMId;
            this.portfolioId = portfolioId;
            this.portfolioName = portfolioName;
            this.recordId = recordId;
            this.methodOrMeasureName = methodOrMeasureName;
            this.descOrComment = descOrComment;
            this.measureTargetValue = measureTargetValue;
            this.detailedJustification = detailedJustification;
            this.targetDateOfChange = targetDateOfChange;
            this.changeRequestStatus = changeRequestStatus;
            this.isMethod = isMethod;
        }
    }

    public class MethodAndTheirMeasures {
        @AuraEnabled public String methodId;
        @AuraEnabled public String methodName;
        @AuraEnabled public List<Map<String, String>> relatedMeasures;

        public MethodAndTheirMeasures() {}
        public MethodAndTheirMeasures(String methodId, String methodName, List<Map<String, String>> relatedMeasures) {
            this.methodId = methodId;
            this.methodName = methodName;
            this.relatedMeasures = relatedMeasures;
        }
    }

    @AuraEnabled
    public static RecordsData securityGRCRecord() {
        List<Portfolio__c> securityRecord = new List<Portfolio__c>([SELECT Id, Name FROM Portfolio__c WHERE Name =: System.Label.Security_GRC_Portfolio_Name]);
        if(securityRecord!=null && !securityRecord.isEmpty()) {
            RecordsData recordData = new RecordsData(securityRecord.get(0).Id, securityRecord.get(0).Name, '');
            return recordData;
        }
        return new RecordsData();
    }

    @AuraEnabled
    public static List<RecordsData> findRecords(String ObjectName, String fieldName, String value, String v2momId, String methodId){
        List<RecordsData> recordDataList = new List<RecordsData>();
        if(value == '') {
            return new List<RecordsData>();
        }
        String key = '%' + value + '%'; 
        String QUERY;
        if(ObjectName == 'V2MOM_c__x') {
            QUERY = 'Select Id, ExternalId, '+fieldName+' From '+ObjectName +' Where '+fieldName +' LIKE :key';
        }
        else if(ObjectName == 'V2MOM_Method_c__x') {
            QUERY = 'Select Id, ExternalId, Description_c__c, '+fieldName+' From '+ObjectName +' Where '+fieldName +' LIKE :key';
        }
        else if(ObjectName == 'Measure_c__x') {
            if(String.isBlank(methodId))
                QUERY = 'Select Id, ExternalId, Comment_c__c, '+fieldName+' From '+ObjectName +' Where '+fieldName +' LIKE :key';
            else 
                QUERY = 'Select Id, ExternalId, Comment_c__c, '+fieldName+' From '+ObjectName +' Where '+fieldName +' LIKE :key AND Method_c__c = \''+methodId+'\'';
        }
        else if(ObjectName == 'Portfolio__c') {
            QUERY = 'Select Id, '+fieldName+' From '+ObjectName +' Where '+fieldName +' LIKE :key';
        }
        if(v2momId!=null && v2momId!='') {
            QUERY += ' AND V2MOM_c__c =: v2momId';
        }
        QUERY += ' WITH SECURITY_ENFORCED LIMIT 5';
        System.debug('Query: '+ QUERY);
        List<SObject> sObjectList = Database.query(QUERY);
        System.debug('Query Result: '+sObjectList);
        if(ObjectName == 'V2MOM_c__x') {
            for(SObject s: sObjectList) {
                System.debug('V2MOM Ext Id: '+(String)s.get('ExternalId'));
                recordDataList.add(new RecordsData((String)s.get('ExternalId'), (String)s.get('Name__c'), ''));
            }
        }
        else if(ObjectName == 'V2MOM_Method_c__x') {
            for(SObject s: sObjectList) {
                recordDataList.add(new RecordsData((String)s.get('ExternalId'), (String)s.get('Name__c'), (String)s.get('Description_c__c')));
            }
        }
        else if(ObjectName == 'Measure_c__x') {
            for(SObject s: sObjectList) {
                recordDataList.add(new RecordsData((String)s.get('ExternalId'), (String)s.get('MeasureName_c__c'), (String)s.get('Comment_c__c')));
            }
        }
        else if(ObjectName == 'Portfolio__c') {
            for(SObject s: sObjectList) {
                recordDataList.add(new RecordsData((String)s.get('Id'), (String)s.get('Name'), ''));
            }
        }
        
        System.debug('Object List: '+sObjectList);
        return recordDataList;        
    }

    @AuraEnabled
    public static String createChangeRequest(List<Map<String, String>> methodOrMeasureData){
        List<V2MOM_Change_Request__c> changeRequestList = new List<V2MOM_Change_Request__c>();
        List<Method_and_Measure_Change__c> methodOrMeasureList = new List<Method_and_Measure_Change__c>();
        List<V2MOM_c__x> v2momList = new List<V2MOM_c__x>();
        List<Org62User__x> org62UserList = new List<Org62User__x>();
        List<User> userList = new List<User>();
        
        System.debug('Complete Data: '+methodOrMeasureData);
        
        Id pfId = (Id)methodOrMeasureData.get(0).get('portfolioId');
        List<Portfolio__c> portfolioList = new List<Portfolio__c>([SELECT Id, Portfolio_Manager__c FROM Portfolio__c WHERE Id =: pfId WITH SECURITY_ENFORCED]);

        if(!Test.isRunningTest()) {
            Id v2momId = (Id)methodOrMeasureData.get(0).get('v2momId');
            v2momList = [SELECT ExternalId, OwnerId__c, Name__c FROM V2MOM_c__x WHERE ExternalId =: v2momId];
            org62UserList = [SELECT Id, ExternalId, EmployeeNumber__c FROM Org62User__x WHERE Id =: v2momList.get(0).OwnerId__c];
            userList = [SELECT Id, EmployeeNumber FROM User WHERE EmployeeNumber =: org62UserList.get(0).EmployeeNumber__c];
        }

        V2MOM_Change_Request__c changeRequest = new V2MOM_Change_Request__c();
        if(!Test.isRunningTest()) {
            changeRequest.V2MOM__c = (Id)methodOrMeasureData.get(0).get('v2momId');
            changeRequest.V2MOM_Name__c = v2momList.get(0).Name__c;
            if(userList!=null && !userList.isEmpty()) {
                changeRequest.V2MOM_Owner__c = userList.get(0).Id;
            }
        }
        changeRequest.Status__c = 'Draft';
        changeRequest.Portfolio_Manager__c = portfolioList.get(0).Portfolio_Manager__c;
        changeRequest.Portfolio__c = (Id)methodOrMeasureData.get(0).get('portfolioId');
        
        INSERT changeRequest;

        List<V2MOM_Change_Request__c> recentChangeRequest = new List<V2MOM_Change_Request__c>([SELECT Id FROM V2MOM_Change_Request__c
                                                                                               ORDER BY CreatedDate DESC LIMIT 1]);

        for(Map<String, String> data : methodOrMeasureData) {
            Method_and_Measure_Change__c methodOrMeasureChange = new Method_and_Measure_Change__c();
            methodOrMeasureChange.Change_Request__c = changeRequest.Id;
            methodOrMeasureChange.Detailed_Justification__c = data.get('detailedJustification');
            methodOrMeasureChange.Target_Date_of_Change__c = Date.valueOf((String)data.get('targetDateOfChange'));
            methodOrMeasureChange.Requestor__c = UserInfo.getUserId();
            if(data.get('isMethod') == 'true') {
                if(data.get('isInsert') == 'false') {
                    methodOrMeasureChange.Method__c = (Id)data.get('methodOrMeasureId');
                    methodOrMeasureChange.Original_Method_Description__c = data.get('currentDescOrComm');
                }
                methodOrMeasureChange.Method_Name__c = data.get('methodOrMeasureName');
                methodOrMeasureChange.Method_Description__c = data.get('updatedDescOrComm');
            }
            else if(data.get('isMethod') == 'false') {
                if(data.get('isInsert') == 'false') {
                    methodOrMeasureChange.Measure__c = (Id)data.get('methodOrMeasureId');
                    methodOrMeasureChange.Method__c = (Id)data.get('relatedMethodId');
                    methodOrMeasureChange.Original_Measure_Comment__c = data.get('currentDescOrComm');
                }
                if((String)data.get('measureTargetValue') != '') {
                    methodOrMeasureChange.Measure_Target_Value__c = Integer.valueOf((String)data.get('measureTargetValue'));
                }   
                methodOrMeasureChange.Measure_Comment__c = data.get('updatedDescOrComm');
                methodOrMeasureChange.Measure_Name__c = data.get('methodOrMeasureName');
                methodOrMeasureChange.Method_Name__c = data.get('relatedMethodName');
            }
            methodOrMeasureList.add(methodOrMeasureChange);
        }
        INSERT methodOrMeasureList;
        return String.valueOf(recentChangeRequest.get(0).Id);
    }

    @AuraEnabled
    public static List<MethodAndMeasureData> getMethodAndMeasureRecords(String changeRequestId){
        List<MethodAndMeasureData> methodAndMeasureDataList = new List<MethodAndMeasureData>();
        List<V2MOM_Change_Request__c> changeRequest = new List<V2MOM_Change_Request__c>([SELECT Id, Portfolio__r.Id, Portfolio__r.Name, Status__c, V2MOM__r.ExternalId, V2MOM__r.Name__c
                                                                                        FROM V2MOM_Change_Request__c
                                                                                        WHERE Id =: changeRequestId 
                                                                                        WITH SECURITY_ENFORCED]);
        List<Method_and_Measure_Change__c> methodAndMeasureChanges = new List<Method_and_Measure_Change__c>([SELECT Id, Change_Request__r.Id, Detailed_Justification__c, Measure_Comment__c, 
                                                                                                            Measure_Name__c, Measure_Target_Value__c, Method_Description__c, Method_Name__c, Target_Date_of_Change__c
                                                                                                            FROM Method_and_Measure_Change__c
                                                                                                            WHERE Change_Request__r.Id =: changeRequestId 
                                                                                                            WITH SECURITY_ENFORCED]);
        if(methodAndMeasureChanges!=null && !methodAndMeasureChanges.isEmpty()) {
            for(Method_and_Measure_Change__c change : methodAndMeasureChanges) {
                String v2momName = changeRequest.get(0).V2MOM__r.Name__c;
                String v2momId = String.valueOf(changeRequest.get(0).V2MOM__r.ExternalId);
                String portfolioId = String.valueOf(changeRequest.get(0).Portfolio__r.Id);
                String portfolioName = changeRequest.get(0).Portfolio__r.Name;
                String recordId = String.valueOf(change.Id);
                String methodOrMeasureName = String.isBlank(change.Method_Name__c) ? change.Measure_Name__c : change.Method_Name__c;
                String descOrComment = String.isBlank(change.Method_Description__c) ? change.Measure_Comment__c : change.Method_Description__c;
                String measureTargetValue = String.valueOf(change.Measure_Target_Value__c);
                String detailedJustification = change.Detailed_Justification__c;
                String targetDateOfChange = String.valueOf(change.Target_Date_of_Change__c);
                String changeRequestStatus = changeRequest.get(0).Status__c;
                Boolean isMethod = String.isBlank(change.Method_Name__c) ? false : true;
                MethodAndMeasureData data = new MethodAndMeasureData(v2momName, v2momId, portfolioId, portfolioName, recordId, methodOrMeasureName, descOrComment, 
                                                                    measureTargetValue, detailedJustification, targetDateOfChange, changeRequestStatus, isMethod);
                methodAndMeasureDataList.add(data);
            }
            System.debug('Data Present: '+methodAndMeasureDataList);
            return methodAndMeasureDataList;
        }  
        else if(changeRequest!=null && !changeRequest.isEmpty()) {
            for(V2MOM_Change_Request__c cr : changeRequest) {
                String v2momName = changeRequest.get(0).V2MOM__r.Name__c;
                String v2momId = String.valueOf(changeRequest.get(0).V2MOM__r.ExternalId);
                String portfolioId = String.valueOf(changeRequest.get(0).Portfolio__r.Id);
                String portfolioName = changeRequest.get(0).Portfolio__r.Name;
                String changeRequestStatus = changeRequest.get(0).Status__c;
                MethodAndMeasureData data = new MethodAndMeasureData(v2momName, v2momId, portfolioId, portfolioName, '', '', '', 
                                                                    '', '', '', changeRequestStatus, false);
                methodAndMeasureDataList.add(data);
            }
            return methodAndMeasureDataList;
        }                                                                                                 
        return null;                                                                  
    }

    @AuraEnabled
    public static String updateMethodAndMeasureChange(List<Map<String, String>> methodAndMeasureData, String changeRequestId){
        List<Method_and_Measure_Change__c> updatedChangesList = new List<Method_and_Measure_Change__c>();
        List<Method_and_Measure_Change__c> methodAndMeasureChanges = new List<Method_and_Measure_Change__c>([SELECT Id, Change_Request__r.Id, Detailed_Justification__c, Measure_Comment__c, 
                                                                                                            Measure_Name__c, Measure_Target_Value__c, Method_Description__c, Method_Name__c, Target_Date_of_Change__c
                                                                                                            FROM Method_and_Measure_Change__c
                                                                                                            WHERE Change_Request__r.Id =: changeRequestId
                                                                                                            WITH SECURITY_ENFORCED]);
        if(methodAndMeasureChanges!=null && !methodAndMeasureChanges.isEmpty()) {
            for(Method_and_Measure_Change__c change : methodAndMeasureChanges) {
                for(Map<String, String> jsonData : methodAndMeasureData) {
                    Method_and_Measure_Change__c updatedData = new Method_and_Measure_Change__c();
                    if(String.valueOf(change.Id) == jsonData.get('recordId')) {
                        if(jsonData.get('isMethod') == 'true' && ( String.valueOf(change.Method_Name__c) != jsonData.get('methodOrMeasureName') || String.valueOf(change.Detailed_Justification__c) != jsonData.get('detailedJustification') || String.valueOf(change.Method_Description__c) != jsonData.get('descOrComment') || String.valueOf(change.Target_Date_of_Change__c) != jsonData.get('targetDateOfChange'))) {
                            updatedData.Id = change.Id;
                            updatedData.Method_Name__c = jsonData.get('methodOrMeasureName');
                            updatedData.Detailed_Justification__c = jsonData.get('detailedJustification');
                            updatedData.Method_Description__c = jsonData.get('descOrComment');
                            updatedData.Target_Date_of_Change__c = Date.valueOf(jsonData.get('targetDateOfChange'));
                            updatedData.Requestor__c = UserInfo.getUserId();
                            updatedChangesList.add(updatedData);
                        }
                        else if(jsonData.get('isMethod') == 'false' && ( String.valueOf(change.Measure_Name__c) != jsonData.get('methodOrMeasureName') || String.valueOf(change.Detailed_Justification__c) != jsonData.get('detailedJustification') || String.valueOf(change.Measure_Comment__c) != jsonData.get('descOrComment') || String.valueOf(change.Target_Date_of_Change__c) != jsonData.get('targetDateOfChange')) || String.valueOf(change.Measure_Target_Value__c) != jsonData.get('measureTargetValue')) {
                            updatedData.Id = change.Id;
                            updatedData.Measure_Name__c = jsonData.get('methodOrMeasureName');
                            updatedData.Detailed_Justification__c = jsonData.get('detailedJustification');
                            updatedData.Measure_Comment__c = jsonData.get('descOrComment');
                            updatedData.Target_Date_of_Change__c = Date.valueOf(jsonData.get('targetDateOfChange'));
                            updatedData.Measure_Target_Value__c = Decimal.valueOf(jsonData.get('measureTargetValue'));
                            updatedData.Requestor__c = UserInfo.getUserId();
                            updatedChangesList.add(updatedData);
                        }
                    }
                }
            }
        } 
        
        if(updatedChangesList!=null && !updatedChangesList.isEmpty()) {
            System.debug('Updated Change List: '+updatedChangesList);
            UPDATE updatedChangesList;
            return ''+updatedChangesList.size();
        }
        return '';
    }

    @AuraEnabled
    public static List<MethodAndTheirMeasures> getMethodAndTheirMeasures(String v2momId) {
        System.debug('V2MOM Id: '+v2momId);
        Set<Id> methodIdsSet = new Set<Id>();
        List<V2MOM_Method_c__x> methodsList = new List<V2MOM_Method_c__x>();
        List<Measure_c__x> measuresList = new List<Measure_c__x>();

        if(!Test.isRunningTest()) {
            methodsList = [SELECT Id, ExternalId, Name__c, V2MOM_c__c 
                           FROM V2MOM_Method_c__x 
                           WHERE V2MOM_c__c =: v2momId
                           WITH SECURITY_ENFORCED]; 
        }
        else {
            System.debug('Mocked Method: '+mockedMethodList);
            methodsList = mockedMethodList;
        }
        System.debug('Methods List: '+methodsList);

        if(!Test.isRunningTest() && methodsList!=null && !methodsList.isEmpty()) {
            for(V2MOM_Method_c__x method : methodsList) {
                methodIdsSet.add(method.ExternalId);
            }
        }

        if(!Test.isRunningTest()) {
            measuresList = [SELECT Id, ExternalId, MeasureName_c__c, Method_c__c 
                            FROM Measure_c__x 
                            WHERE Method_c__c IN: methodIdsSet
                            WITH SECURITY_ENFORCED];
        }
        else {
            System.debug('Mocked Measure: '+mockedMeasureList);
            measuresList = mockedMeasureList;
        }
        System.debug('Measures List: '+measuresList);

        Map<Id, List<Map<String, String>>> methodIdToMeasureDataMap = new Map<Id, List<Map<String, String>>>();
        if(measuresList!=null && !measuresList.isEmpty()) {
            for(Measure_c__x measure : measuresList) {
                if(!Test.isRunningTest() && !methodIdToMeasureDataMap.containsKey(measure.Method_c__c)) {
                    methodIdToMeasureDataMap.put(measure.Method_c__c, new List<Map<String, String>>());
                }
                Map<String, String> measureData = new Map<String, String>();
                measureData.put('Id', measure.ExternalId);
                measureData.put('Name', measure.MeasureName_c__c);
                if(!Test.isRunningTest()) {
                    methodIdToMeasureDataMap.get(measure.Method_c__c).add(measureData);
                }
            }
        }
        System.debug('MethodMeasure Map: '+methodIdToMeasureDataMap);
        
        List<MethodAndTheirMeasures> methodMeasureList = new List<MethodAndTheirMeasures>();
        if(methodsList!=null && !methodsList.isEmpty()) {
            for(V2MOM_Method_c__x method : methodsList) {
                MethodAndTheirMeasures methodAndTheirMeasures = new MethodAndTheirMeasures();
                methodAndTheirMeasures.methodId = method.ExternalId;
                methodAndTheirMeasures.methodName = method.Name__c;
                methodAndTheirMeasures.relatedMeasures = new List<Map<String, String>>();
                if(!Test.isRunningTest() && methodIdToMeasureDataMap!=null && !methodIdToMeasureDataMap.isEmpty()) {
                    methodAndTheirMeasures.relatedMeasures = methodIdToMeasureDataMap.get(method.ExternalId);
                }
                methodMeasureList.add(methodAndTheirMeasures);
            }
        }
        System.debug('Complete Data: '+methodMeasureList);
        return methodMeasureList;
    }

    @AuraEnabled
    public static Boolean deleteMethodOrMeasureChange(String recordId) {
        List<Method_and_Measure_Change__c> deleteChangeList = new List<Method_and_Measure_Change__c>([SELECT Id FROM Method_and_Measure_Change__c
                                                                                                      WHERE Id =: recordId]);
        if(deleteChangeList!=null && !deleteChangeList.isEmpty()) {
            DELETE deleteChangeList;
            return true;
        }
        return false;
    }

    @AuraEnabled
    public static Map<String, String> getRelatedMethod(Id measureId){
        List<V2MOM_Method_c__x> methodList = new List<V2MOM_Method_c__x>();
        List<Measure_c__x> measureList = new List<Measure_c__x>();
        
        if(!Test.isRunningTest()) {
            measureList = [SELECT Id, ExternalId, Method_c__c FROM Measure_c__x WHERE ExternalId =: measureId];
            if(measureList!=null && !measureList.isEmpty()) {
                methodList = new List<V2MOM_Method_c__x>([SELECT Id, Name__c, ExternalId 
                                                    FROM V2MOM_Method_c__x
                                                    WHERE ExternalId =: measureList.get(0).Method_c__c]);
            }
        }
        else {
            measureList = mockedMeasureList;
            methodList = mockedMethodList;
        }  
        
        if(methodList!=null && !methodList.isEmpty()) {
            Map<String, String> methodDataMap = new Map<String, String>();
            methodDataMap.put('relatedMethodId', String.valueOf(methodList.get(0).ExternalId));
            methodDataMap.put('relatedMethodName', String.valueOf(methodList.get(0).Name__c));
            System.debug('Method Data Map: '+methodDataMap);
            return methodDataMap;
        }
        return null;
    }
}
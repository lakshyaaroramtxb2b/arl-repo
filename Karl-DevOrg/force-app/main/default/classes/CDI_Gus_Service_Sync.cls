public class CDI_Gus_Service_Sync {
    public static void syncGusService(){
        String errors='';
        Integer rowsPassed=0;
        Integer rowsFailed=0;
        DateTime starttime=DateTime.now();
        String gusTeamId=null;
        List<Gus_Service_c__x> extGusServiceList=[select Name__c, IsDeleted__c, Capabilities_c__c, ADM_Cloud_c__c, CreatedDate__c, Description_c__c, Service_Id_c__c, Service_Owner_c__c, Service_Status_c__c, Team_c__r.name__c, Team_Contact_c__c from Gus_Service_c__x where IsDeleted__c=false];
        List<CDI_Service__c> cdiServiceList=new List<CDI_Service__c>();
        List<CDI_Gus_Team__c> gusTeamList=[select id,name from CDI_GUS_Team__c];
		Map <String, String> gusTeamInfo=new Map <String,String>();
		for (CDI_Gus_Team__c gusTeam:gusTeamList){
            gusTeamInfo.put(gusTeam.name,gusTeam.id);
        }
		for (String name:gusTeamInfo.keySet()){
            System.debug('GUS Team name:'+name);
            System.debug('GUS Team Id:'+gusTeamInfo.get(name));
		}
        for (Gus_Service_c__x extService:extGusServiceList){
            CDI_Service__c cdiService=new CDI_Service__c();
            cdiService.Name=extService.Name__c;
            cdiService.IsDeleted__c=extService.IsDeleted__c;
            cdiService.Capability__c=extService.Capabilities_c__c;
            //cdiService.CreatedDate=extService.CreatedDate__c;
            cdiService.Service_Description__c=extService.Description_c__c;
            cdiService.Source_Service_Code__c=extService.Service_Id_c__c;
            cdiService.Source_System_Name__c='Service Category';
            cdiService.Status__c='Active';
            System.debug('Team Contact:'+extService.Team_c__r.name__c);
            if (extService.Team_c__r.name__c!=null){
                if(gusTeamInfo.ContainsKey(extService.Team_c__r.name__c)){
                    //String gusTeam=gusTeamInfo.get(name);
                    //gusTeamId=string.valueOf(gusTeam);
                    //Id cdiGusTeamId=Id.valueOf(gusTeamId);
                    cdiService.Gus_Team__c=gusTeamInfo.get(extService.Team_c__r.name__c);
                    System.debug('Gus Team id for Service:'+gusTeamInfo.get(extService.Team_c__r.name__c));
    		} 
		}
            
            
            String Name=cdiService.Name+cdiService.Source_System_Name__c+cdiService.Source_Service_Code__c;
            cdiServiceList.add(cdiService);
        }
        Schema.SObjectField Name=CDI_Service__c.Fields.Name;
        List<Database.UpsertResult> upsertAssetResult=Database.upsert(cdiServiceList,Name,false);
        System.debug('Number of rows processed:'+upsertAssetResult.size());
        for(Integer index = 0; index < upsertAssetResult.size(); index++) {
            if(upsertAssetResult[index].isSuccess()) {
            rowsPassed++; 
            }
            else {
            rowsFailed++;
            Database.Error error=upsertAssetResult.get(index).getErrors().get(0);
            errors=error.getMessage();
                System.debug('Error:'+error);
            }
        }
        System.debug('Number of rows passed:'+rowsPassed);
        System.debug('Number of rows failed:'+rowsFailed);
    // Crete Batch Import Records. 
        CDI_Batch_Import__c batchImport = new CDI_Batch_Import__c();
        batchImport.Start_Time__c=starttime;
        batchImport.End_Time__c=DateTime.now();
        batchImport.Object_Type__c='GUS Service';
        batchImport.Errors__c=errors;
        batchImport.Rows_Succeeded__c=rowsPassed;
        batchImport.Rows_Failed__c=rowsFailed;
        System.debug('Finish GUS UDD Service Sync');
        insert batchImport;
    }

}
/***********************************************************
* Helper class for PRT_AssignmentTriggerHelper
* Created by    - Harinath Ch
* Updated by - Prashant Gupta
* Date      - 06th August 2019
************************************************************/
public class PRT_AssignmentTriggerHelper {
/***********************************************************
* Handler class for PRT_ProjectTrigger
* Created by    - Prashant Gupta
* Method Used to check for allocation of assignment
************************************************************/
   /* public static void checkAllocationBeforeInsert(List<Assignment__c> assignmentList){
        String WeeksofExceededAllocation = '';
        Boolean ExceededAllocation = false;
        Map<String, List<Assignment__c>> contactAssignmentMap = new  Map<String, List<Assignment__c>>();
        
        List<String> ContactIdList = new List<String>();
        for(Assignment__c assignment: assignmentList){
            ContactIdList.add(assignment.Contact__c);
        }
        
        for(Assignment__c assignment: [SELECT Id, Name, Contact__c, Start_Date__c, End_Date__c, 
                                       Percentage_of_Capacity__c, Contact__r.Name 
                                       FROM Assignment__c 
                                       WHERE Contact__c IN :ContactIdList]){
                                           if(contactAssignmentMap.containsKey(assignment.Contact__c)){
                                               contactAssignmentMap.get(assignment.Contact__c).add(assignment);
                                           }
                                           else{
                                               contactAssignmentMap.put(assignment.Contact__c, new List<Assignment__c>{assignment});
                                           }
                                       } 
       
        
        for(Assignment__c assignmentfinal: assignmentList){
            WeeksofExceededAllocation ='This Update would exceed the allocation for the weeks ';
            if(!assignmentfinal.Allow_Exceeding_Allocation_Limits__c){
                
                Decimal ContactTotalAllocation = assignmentfinal.Percentage_of_Capacity__c!=null ? assignmentfinal.Percentage_of_Capacity__c: 0;
                
                if(contactAssignmentMap.get(assignmentfinal.Contact__c)!= null && assignmentfinal.Start_Date__c != null && assignmentfinal.End_Date__c != null){
                    for(Date d=assignmentfinal.Start_Date__c.toStartofWeek();d <=assignmentfinal.End_Date__c;d=d.addDays(7) ){                
                        for(Assignment__c a : contactAssignmentMap.get(assignmentfinal.Contact__c)){
                            if(a.Start_Date__c <= d && a.End_Date__c > d){
                                ContactTotalAllocation +=a.Percentage_of_Capacity__c;
                            }
                        }
                        //system.debug('ContactTotalAllocation>>>>>>>>>>>>'+ContactTotalAllocation);
                        if(ContactTotalAllocation >100.00){
                            WeeksofExceededAllocation += d.format() + '(' + ContactTotalAllocation + ') \r\t';
                            ExceededAllocation = true;
                            assignmentfinal.addError(WeeksofExceededAllocation);
                        }
                    } 
                }
            }  
        }
    }*/
    
    
    public static void createAssignmentWeekRecord(List<Assignment__c> newList){
        List<Assignment_Week__c> assignmentWeekList = new List<Assignment_Week__c>();
        Map<String, Integer> dayofweekMap = new Map<String, Integer>{'Sunday' => 1,'Monday' => 2, 'Tuesday' => 3, 'Wednesday' => 4,'Thursday' => 5,'Friday' => 6,'Saturday' => 7};
            
            
            for(Assignment__c assign:newList){
                Date startDate = assign.Start_Date__c  ;
                Date endDate = assign.End_Date__c  ;
                System.debug('>>> end date >>>>' + endDate);
                
                if((assign.Start_Date__c!=Null) && (assign.End_Date__c!=null)){
                    // commented error line
                    Datetime dt = DateTime.newInstance(startDate, Time.newInstance(0, 0, 0, 0));
                    String dayOfWeek=dt.format('EEEE');
                    Integer weekday = dayofweekMap.get(dayOfWeek);
                    do{
                        Assignment_Week__c assignmentWeekRecord = new Assignment_Week__c();
                        assignmentWeekRecord.Contact__c = assign.Contact__c;
                        if(assign.Percentage_of_Capacity__c!=null){
                            assignmentWeekRecord.Percentage_of_Capacity__c = assign.Percentage_of_Capacity__c;
                            assignmentWeekRecord.Number_of_Hours__c = assignmentWeekRecord.Percentage_of_Capacity__c/2.5;
                        }else{
                            assignmentWeekRecord.Number_of_Hours__c = 0;
                        }
                        assignmentWeekRecord.Assignment__c = assign.Id;
                        if(weekday != null){
                            assignmentWeekRecord.Week_Start_Date__c =startDate.addDays(-weekday+1);
                            assignmentWeekRecord.Week_End_Date__c =startDate.addDays(7-weekday) ;
                            
                            weekday = null;
                        }
                        else{
                            assignmentWeekRecord.Week_Start_Date__c =startDate;
                            assignmentWeekRecord.Week_End_Date__c =startDate.addDays(6);
                        }
                        startDate = assignmentWeekRecord.Week_End_Date__c.addDays(1);
                        System.debug('>>> start date >>>> ' + assignmentWeekRecord.Week_Start_Date__c );
                        System.debug('>>> end date >>>> ' + assignmentWeekRecord.Week_End_Date__c);
                        assignmentWeekList.add(assignmentWeekRecord);
                        
                    } while(startDate < endDate);
                }
                
            }
        insert assignmentWeekList;
        
    }    
    
    public static void updateAssignmentWeekRecord(List<Assignment__c> newList, Map<Id,Assignment__c> oldMap ){
        List<Assignment_Week__c> newAssignmentWeekList = new List<Assignment_Week__c>();
        List<Assignment_Week__c> assignmentWeekToDeleteList = new List<Assignment_Week__c>();
        Map<Id,Assignment__c> updatedAssignmentMap = new Map<Id,Assignment__c>();
        Map<String, Integer> dayofweekMap = new Map<String, Integer>{'Sunday' => 1,'Monday' => 2, 'Tuesday' => 3, 'Wednesday' => 4,'Thursday' => 5,'Friday' => 6,'Saturday' => 7};
            Set<Id> assignmentIdSet = new Set<Id>();
        
        for(Assignment__c assign: newList){
            Date newStartDate= assign.Start_Date__c;
            Date oldStartDate = oldMap.get(assign.Id).Start_Date__c;
            Date newEndDate = assign.End_Date__c;
            Date oldEnddate = oldMap.get(assign.Id).End_Date__c;
            System.debug('>>> newStartDate >>>' + newStartDate);
            System.debug('>>> oldStartDate >>>' + newStartDate);
            System.debug('>>> newEndDate >>>' + newStartDate);
            System.debug('>>> newStartDate >>>' + newStartDate);
            
            if( ((newStartDate!= oldStartDate &&  newStartDate < oldStartDate )  && (newStartDate != newEndDate) &&((assign.Start_Date__c!=Null) && (assign.End_Date__c!=null)))
               || assign.Contact__c !=oldMap.get(assign.Id).Contact__c || assign.Percentage_of_Capacity__c!=oldMap.get(assign.Id).Percentage_of_Capacity__c){
                   
                   //Date startDate =newStartDate;
                   //Date endDate = oldMap.get(assign.Id).End_Date__c;
                   Datetime dt = DateTime.newInstance(oldStartDate, Time.newInstance(0, 0, 0, 0));
                   String dayOfWeek=dt.format('EEEE');
                   Integer weekday = dayofweekMap.get(dayOfWeek);
                   Date oldStartDateWeekStartdate = oldStartDate.addDays(-weekday+1);
                   System.debug('>>>> old start day week start date>>>' + oldStartDateWeekStartdate);
                   
                   Datetime dt1 = DateTime.newInstance(newStartDate, Time.newInstance(0, 0, 0, 0));
                   String dayOfWeek1=dt1.format('EEEE');
                   Integer weekday1 = dayofweekMap.get(dayOfWeek1);
                   Date newStartDateWeekStartdate = newStartDate.addDays(-weekday1+1);
                   System.debug('>>>>> new start day week start date >>' + newStartDateWeekStartdate);
                   
                   if(oldStartDateWeekStartdate != newStartDateWeekStartdate){
                       newAssignmentWeekList.addAll(PRT_AssignmentTriggerHelper.createRec(assign,newStartDate,oldStartDateWeekStartdate.addDays(-1)));
                   }
                   
                   
               }
            if(newEndDate!=oldEnddate && newEndDate > oldEnddate && (newStartDate != newEndDate)){
                   Datetime dt = DateTime.newInstance(oldEnddate, Time.newInstance(0, 0, 0, 0));
                   String dayOfWeek=dt.format('EEEE');
                   Integer weekday = dayofweekMap.get(dayOfWeek);
                   Date oldEndDateWeekEnddate = oldEnddate.addDays(7-weekday);
                   System.debug('>>>> old end day week end date>>>' + oldEndDateWeekEnddate);
                   
                   Datetime dt1 = DateTime.newInstance(newEndDate, Time.newInstance(0, 0, 0, 0));
                   String dayOfWeek1=dt1.format('EEEE');
                   Integer weekday1 = dayofweekMap.get(dayOfWeek1);
                   Date newEndDateWeekEnddate = newEndDate.addDays(7-weekday1);
                   System.debug('>>>>> new end day week end date >>' + newEndDateWeekEnddate);
                   
                   if(oldEndDateWeekEnddate != newEndDateWeekEnddate){
                       newAssignmentWeekList.addAll(PRT_AssignmentTriggerHelper.createRec(assign,oldEndDateWeekEnddate.addDays(+1),newEndDate));
                   }
                   
               }
            if((newStartDate > oldStartDate) || (newEndDate < oldEnddate)){
                   updatedAssignmentMap.put(assign.Id,assign);
                   
               }   
        }
        System.debug('>> updatedAssignmentMap >>>' + updatedAssignmentMap);
        if(!newAssignmentWeekList.isEmpty()){
            System.debug('>> newAssignmentWeekList1 >>>' + newAssignmentWeekList);
            insert newAssignmentWeekList;
        }
        for(Assignment_Week__c assignWeek : [SELECT Id, Name, Week_Start_Date__c, Assignment__c, Week_End_Date__c 
                                             FROM Assignment_Week__c
                                             WHERE Assignment__c IN : updatedAssignmentMap.keySet()]){
                                                        System.debug('>> assignWeek >>>' + assignWeek);
            if((assignWeek.Week_End_Date__c < updatedAssignmentMap.get(assignWeek.Assignment__c).Start_Date__c) 
                                                    || assignWeek.Week_Start_Date__c > updatedAssignmentMap.get(assignWeek.Assignment__c).End_Date__c ){
                                                        
                                                        assignmentWeekToDeleteList.add(assignWeek); 
                                                        System.debug('>> assignWeek >>>' + assignWeek);
                                                        System.debug('>> assignmentWeekToDeleteList >>>' + assignmentWeekToDeleteList);
                                                        
                                                    }
                                             }
        if(!assignmentWeekToDeleteList.isEmpty()){
            delete assignmentWeekToDeleteList;
        }
       
        
    }
    
    public static List<Assignment_Week__c> createRec(Assignment__c assign, Date newDate, Date oldDate ){
        List<Assignment_Week__c> assignmentWeekList = new List<Assignment_Week__c>();
        Map<String, Integer> dayofweekMap = new Map<String, Integer>{'Sunday' => 1,'Monday' => 2, 'Tuesday' => 3, 'Wednesday' => 4,'Thursday' => 5,'Friday' => 6,'Saturday' => 7};
            
            Date startDate = newDate;
        Date endDate = oldDate;
        Datetime dt = DateTime.newInstance(startDate, Time.newInstance(0, 0, 0, 0));
        String dayOfWeek=dt.format('EEEE');
        Integer weekday = dayofweekMap.get(dayOfWeek);
        if((newDate!=Null) && (oldDate!=null)){
            do{
                Assignment_Week__c assignmentWeekRecord = new Assignment_Week__c();
                assignmentWeekRecord.Contact__c = assign.Contact__c;
                if(assign.Percentage_of_Capacity__c!=null){
                assignmentWeekRecord.Percentage_of_Capacity__c = assign.Percentage_of_Capacity__c;
                assignmentWeekRecord.Number_of_Hours__c = assignmentWeekRecord.Percentage_of_Capacity__c/2.5;
                }
                assignmentWeekRecord.Assignment__c = assign.Id;
                if(weekday != null){
                    assignmentWeekRecord.Week_Start_Date__c =startDate.addDays(-weekday+1);
                    assignmentWeekRecord.Week_End_Date__c =startDate.addDays(7-weekday) ;
                    weekday = null;
                }
                else{
                    assignmentWeekRecord.Week_Start_Date__c =startDate;
                    assignmentWeekRecord.Week_End_Date__c =startDate.addDays(6);
                }
                startDate = assignmentWeekRecord.Week_End_Date__c.addDays(1);
                System.debug('>>> start date >>>> ' + assignmentWeekRecord.Week_Start_Date__c );
                System.debug('>>> end date >>>> ' + assignmentWeekRecord.Week_End_Date__c);
                assignmentWeekList.add(assignmentWeekRecord);
                
            } while(startDate < endDate);
        }
        return assignmentWeekList;
    }
    
    public static void createUpdateAssignmentForMilestone(List<Assignment__c> newList, Map<Id,Assignment__c> oldMap){
        Map<Id,Assignment__c> milestoneIdToAssignmentMap = new Map<Id,Assignment__c>();
        Map<Id,Assignment__c> projectToAssignmentMap = new Map<Id,Assignment__c>();
        for(Assignment__c assign:newList){
            if((oldMap == null && assign.Project__c!=null)||
               (oldMap == null &&   assign.Milestone__c != null) ||
               (assign.Project__c!=null && assign.Project__c!= oldMap.get(assign.Id).Project__c)||
               (assign.Milestone__c != null && assign.Milestone__c != oldMap.get(assign.Id).Milestone__c)){
                   if(assign.Project__c!=null){
                       projectToAssignmentMap.put(assign.Project__c,assign);
                   }else if(assign.Milestone__c!=null){
                       milestoneIdToAssignmentMap.put(assign.Milestone__c,assign); 
                   }
               }
            /* auto-populate picklist when project is not null*/ 
            if((oldMap == null && assign.Project__c!=null && assign.Operational_Area__c == null)
               || (oldMap!=null && assign.Project__c!=null && assign.Operational_Area__c == null)){
                   assign.Operational_Area__c = PRT_Constants.PRT_OPERATIONAL_AREA_PROJECT;
               }
        }
        if(milestoneIdToAssignmentMap!=null){
            for(Milestone_PRT__c milestoneRec: [SELECT Id, Project__c,Project__r.program__c,Program__c 
                                                FROM Milestone_PRT__c 
                                                WHERE Id IN :milestoneIdToAssignmentMap.keySet()]){
                                                    
                                                    if(milestoneRec.Project__c != null){
                                                        milestoneIdToAssignmentMap.get(milestoneRec.id).Project__c = milestoneRec.Project__c;
                                                        milestoneIdToAssignmentMap.get(milestoneRec.id).Program__c =  milestoneRec.Project__r.program__c;
                                                    }
                                                }
            
        }
        if(projectToAssignmentMap!=null){
            for(Project__c proj:[SELECT ID, program__c 
                                 FROM Project__c
                                 WHERE Id IN: projectToAssignmentMap.keySet()]){
                                     if(proj.Program__c != null){
                                         projectToAssignmentMap.get(proj.Id).Program__c = proj.Program__c;
                     }
             }
        }
        
    }
    
    public static void populateAssignmentName(List<Assignment__c> newList, Map<Id,Assignment__c> oldMap){
        Set<id> contactIDs = new Set<Id>();
        Set<id> projectIDs = new Set<Id>();
        Set<Assignment__c> assignToUpdate = new Set<Assignment__c>();
        for(Assignment__c assign : newList){
            if(oldMap==null || (assign.Project__c!=oldMap.get(assign.id).Project__c 
                                                             || assign.Contact__c!=oldMap.get(assign.id).Contact__c) && assign.Project__c!=null){
                if(assign.Project__c!=null){
                	projectIDs.add(assign.Project__c);
                	assignToUpdate.add(assign);
                }
                if(assign.Contact__c!=null && assign.Project__c!=null){
                	contactIDs.add(assign.Contact__c);
                	assignToUpdate.add(assign);        
                }
            }
        }
        Map<id,Project__c> projectMap = new Map<id,Project__c>();
        if(projectIDs!=null && !projectIDs.isEmpty()){
        	projectMap = new Map<id,Project__c>([SELECT ID, Name From Project__c WHERE id in :projectIDs ]);
        }
        Map<id,Contact> ContactMap = new Map<id,Contact>();
        if(Contactids!=null && !Contactids.isEmpty()){
        	ContactMap = new Map<id,Contact>([SELECT ID, Name From Contact WHERE id in :Contactids]);
        }
        if(!ContactMap.isEmpty() || !projectMap.isEmpty()){
            for(Assignment__c assign : assignToUpdate){
                String Name = '';
                if(ContactMap.containsKey(assign.Contact__c)){
                    Name += ContactMap.get(assign.Contact__c).Name;
                }
                if(projectMap.containsKey(assign.Project__c)){
                    Name += ' - ' +projectMap.get(assign.Project__c).Name;
                }
                if(name!=''){
                    assign.Name = name;
                }
            }    
        }
    }
}
public class FirstRangerController {

    @TestVisible private enum Stages { AUTHORIZE, ACCEPT_SAA, PENTEST_INFO, REQUEST_COMPLETE, REQUEST_ERROR, EDIT_EXISTING, CHOOSE_ENV}
    @TestVisible public Stages stage {get;private set;}
    public boolean show_authorize {get;private set;}
    public boolean show_saa {get;private set;}
    public boolean show_form {get;private set;}
    public boolean show_edit {get;private set;}
    public boolean show_env {get;private set;}

    public boolean requires_additional_target_info {get;set;}
    
    public static Boolean ISSANDBOX {get
        {
            if (ISSANDBOX == null) {
                ISSANDBOX = [SELECT isSandbox FROM Organization limit 1].isSandbox;
            }
            return ISSANDBOX;  
        } 
        private set;
    }

    public static final string OAUTH_AUTHORIZE_ENDPOINT = 'https://login.salesforce.com/services/oauth2/authorize';
    public static final string OAUTH_TOKEN_ENDPOINT = 'https://login.salesforce.com/services/oauth2/token';
    /*public static final string OAUTH_CALLBACK_URL = ISSANDBOX? URL.getSalesforceBaseUrl().toExternalForm() + '/FirstRanger' :
                                                    'https://security.secure.force.com/FirstRanger'; */
    public static final string OAUTH_CALLBACK_URL = 'https://securityorg.force.com/FirstRanger/FirstRanger';
    public static final string OAUTH_CLIENT_ID = '3MVG99OxTyEMCQ3i.Cy5vQYJe7mVOVkqCcjnjh4UxgTbP1hFlYpkEPGq67yMpb5o4ft1ienxiQ1RMjAdd32ZV';
    private static final string OAUTH_CLIENT_SECRET = '916510964017196769';
    private static Boolean wasSFDCOauthCallback = false;
    private static String hashId;

    public boolean SAA_Accepted {get;set;}
    public string company_name {get;set;}
    public string member_id {get;set;}
    public string company_org_id {get;set;}
    public string pm_name {get;set;}
    public string pm_workphone {get;set;}
    public string pm_workphone_helpText {get; set;}
    public string pm_email {get;set;}
    public string pm_cellphone {get;set;}
    public string pm_username {get;set;}
    public string pm_first_name {get;set;}
    public string pm_last_name {get;set;}
    public string pm_user_id {get;set;}

    // FB, linkedin login
    public string social_token {get;set;}

    // VF hints. For revisit/edit, only show the auth provider used
    public Boolean showFB {get;set;}
    public Boolean showLI {get;set;}
    public Boolean showGoogle {get;set;}
    public Boolean showSFDC {get;set;}

    public Boolean isMarketingCloud {get; set;}

    // opaque oauth state. Used to hold record ID through the SFDC oauth flow
    // when the user is coming back to view or edit a record
    private ID oauth_state;

    // Used to hold customer assessment for re-visit/edit
    public Customer_Assessment__c record {get;private set;}

    // Users may only edit record when it's not approved
    public boolean record_editable {get; private set;}

    // Store the oauth userInfo the user authenticated with
    @TestVisible
    private Stringable userInfo;

    public String now {get;set;}
    public final string saaDocId {get; private set;}

    public List<SelectOption> server_load_entries {get; private set;}

    // This is stupid but we keep hitting problems where it's complaining
    // about tests doing having uncommitted work and doing DML before callouts.
    // You're supposed to be able to do Test.start()/end() but that doesn't seem
    // to actually work
    public HttpResponse testResponse;
    
    private class AssessmentException extends Exception {}

    public FirstRangerController() {
        
        // show all social auth providers by default
        showFB = showGoogle = showLI = showSFDC = True;
        record_editable = False;

        SAA_Accepted = FALSE;

        if (record != null) {
            // checkAndProcessSFDCOauthCode() will need to use this state to keep track of the original re-visit/edit request
            oauth_state = record.id;
            SAA_Accepted = True;
        } else {
            // They're not loading a record, give them the environment selector by default
            changeStage(stages.CHOOSE_ENV);
        }

        // This may also load "record" if we're being invoked as an oauth callback
        wasSFDCOauthCallback = checkAndProcessSFDCOauthCode();

        if (record != null) {
          changeStage(Stages.AUTHORIZE);
        } 

    }
    
    public pageReference loginUser() {
        if (wasSFDCOauthCallback) {

            if (ApexPages.currentPage().getParameters().get('state') != null) {
                if (ApexPages.currentPage().getParameters().get('state').startsWith('HASHID')) {
                    hashId = ApexPages.currentPage().getParameters().get('state').replace('HASHID', '');
                }
            }
            
            return authenticateUser();
    
        }
        return null;
    }
        
    public pageReference authenticateUser() {
        
        if (String.isEmpty(hashId)) hashId = ApexPages.currentPage().getParameters().get('id');
        String userName = pm_email + '.FirstRanger';
        String userpassword = '001GrJ!' + pm_email.substringBefore('@').left(30).reverse();
        String accountId = '0013A00001YZGrJ';
        String userId;
        String startURL = '/s/?entityCode=AceFrs&si=a1i3A0000007AvCQAU';
        if (hashId != null) startURL += '&id=' + hashId;
    
        User u;    
        
        try {
            u = [SELECT Id, UserName, FirstName, LastName, Email, ProfileId,
                 CommunityNickname FROM User WHERE UserName = :userName]; 
            userId = u.Id;
        } catch (exception e) {}
            
        if (u == null) {
            u = new User();
            u.Username = userName;
            u.FirstName = pm_first_name;       
            u.LastName = pm_last_name;       
            u.Email = pm_email;
            u.profileId  = '00e3A0000028mPm';
            u.CommunityNickname = pm_email.substringBefore('@').left(37)+'00e'+Math.round(Math.random()*100000);
            u.TimeZoneSidKey = 'America/Los_Angeles';
            userId = Site.createPortalUser(u, accountId, userpassword);
        }
    
        if (userId != null) { 
            
            try {
                addPermissionSet(userId);    
            } catch (exception e) {}
            
            if (userpassword != null && userpassword.length() > 1) {
                return Site.login(u.userName, userpassword, startURL);
            }
            else {
                PageReference page = System.Page.SiteRegisterConfirm;
                page.setRedirect(true);
                return page;
            }
        }     

        return null;
    }
    
    @future
    public static void addPermissionSet(String userId) {
        try {
            insert new PermissionSetAssignment (AssigneeId = userId, PermissionSetId = '0PS3A000000LMU8');
        } catch (exception e) {}
    }

    @TestVisible
    private void changeStage(Stages newStage) {
        stage = newStage;
        show_authorize = show_form = show_saa = show_edit = show_env = False;

        if (stage == Stages.ACCEPT_SAA) {
            show_saa = True;
        } else if (stage == Stages.PENTEST_INFO) {
            show_form = True;
        } else if (stage == Stages.EDIT_EXISTING) {
            show_form = True;
            show_edit = True;
        } else if (stage == Stages.CHOOSE_ENV) {
            show_env = True;
        } else if (stage == Stages.REQUEST_COMPLETE) {
            // pass
        } else if (stage == Stages.AUTHORIZE) {
            show_authorize=True;
        }
    }

    private boolean checkAndProcessSFDCOauthCode(){
        /*
           This page is also used as the callback for the SFDC oauth login. Check for the code, query for
           user details, and populate user information if the code is available
        */

        string code = ApexPages.currentPage().getParameters().get('code');
        if (code == NULL || code == '') return False;

        if (ApexPages.currentPage().getParameters().get('state') != null) {
            if (ApexPages.currentPage().getParameters().get('state').startsWith('HASHID')) {
                hashId = ApexPages.currentPage().getParameters().get('state').replace('HASHID', '');
            } 
        }

        HttpRequest req = new HttpRequest();
        req.setMethod('POST');
        req.setHeader('Accept', 'application/json');
        req.setEndpoint(OAUTH_TOKEN_ENDPOINT);

        string body = 'grant_type=authorization_code';
        body += '&code=' + code;
        body += '&client_id=' + OAUTH_CLIENT_ID;
        body += '&client_secret=' + OAUTH_CLIENT_SECRET;
        body += '&redirect_uri=' + OAUTH_CALLBACK_URL;
        req.setBody(body);

        Http http = new Http();
        HttpResponse res;
        try{
            res = http.send(req);
        }catch(exception e){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Unable to process request. Please contact support. Error Code : 2',''));
        }

        if (res.getStatusCode() == 200){

            OauthTokenResponse oauthToken = (OauthTokenResponse) JSON.deserialize(res.getBody(), OauthTokenResponse.class);

            req = new HttpRequest();
            req.setMethod('GET');
            req.setHeader('Accept', 'application/json');
            req.setHeader('Authorization', 'Bearer ' + oauthToken.access_token);
            req.setEndpoint(oauthToken.id);

            http = new Http();

            try{
                res = http.send(req);

            }catch(exception e){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Unable to process request. Please contact support. Error Code : 3',''));
            }

            if (res.getStatusCode() == 200){
                IdentityResponse userData = (IdentityResponse) JSON.deserialize(res.getBody(), IdentityResponse.class);
                pm_name = userData.display_name;
                pm_email = userData.email;
                pm_cellphone = userData.mobile_phone;
                company_org_id = userData.organization_id;
                pm_user_id = userData.user_id;
                pm_username = userData.username;
                pm_first_name = userData.first_name;
                pm_last_name = userData.last_name;

                userInfo = userData;
                postLogin();

                return True;
                
                
               

            }else{
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Unable to process request. Please contact support. Error Code : 4',''));
            }
        }else{
              ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Unable to process request. Please contact support. Error Code : 5',''));
              System.debug('received bad status code from OAUTH response: ' + res.toString());
        }
        return False;
    }
    
    public void toggleSaa(){
        changeStage( SAA_Accepted ? Stages.PENTEST_INFO : Stages.ACCEPT_SAA);
    }

    public string getOauthAuthorizeURL(){
        /* SFDC Oauth */
        String url = OAUTH_AUTHORIZE_ENDPOINT + '?response_type=code&client_id=' + OAUTH_CLIENT_ID + '&redirect_uri=' + OAUTH_CALLBACK_URL;
        hashId = ApexPages.currentPage().getParameters().get('id');
            if (oauth_state != null) {
                // The user came for re-visit/edit. Pass the record ID as the state so it
                // gets sent back on the callback
                url += '&state='+ oauth_state;
            } else if (hashId != null) {
                url += '&state=HASHID' + hashId;
            } 
        
        return url;
    }

    @TestVisible
    private void postLogin() {
        /*
           After the user logs in, we need to set some flags based on what they're doing. This
           method is also responsible for the access control check during revisit/edit
        */
        
        if (record == null) {
            // They're creating a record. Next stage is SAA
            changeStage(Stages.ACCEPT_SAA);
        } else {
            // Revisit/edit. Check the auth against the record's auth_info__c
            if (!isCurrentAuthSameAsRecordAuth()) {
                record = null;
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Unable to process request. Please contact support. Error Code : 8',''));
                changeStage(Stages.REQUEST_ERROR);
            } else {
                changeStage(Stages.EDIT_EXISTING);
                if (record.Approved__c == False) {
                    // The record is only editable if not approved
                    record_editable = True;
                }
            }

        }
    }

    private Boolean isCurrentAuthSameAsRecordAuth() {
        // Take a userinfo class that implements Stringable and compare it to the saved Auth_Info__c field
        // of a record. This is used to verify that the user attempting to load the record is the same
        // user that created the record
        if ((record == null) || (record.Auth_Info__c == null)) {
            return False;
        }
        return userInfo.toString() == record.Auth_Info__c;
    }

    private void hideUnusedAuthProviders(boolean SFDCOnly) {
       if ((!sfdcOnly) && ((record == null) || (record.Auth_Info__c == null))) {
           return;
       }

       if (sfdcOnly || (record.Auth_Info__c.startsWith('SFDC'))) {
           showFB = showLI = showGoogle = False;
       } else if (record.Auth_Info__c.startsWith('FB')) {
           showSFDC = showLI = showGoogle = False;
       } else if (record.Auth_Info__c.startsWith('LI')) {
           showSFDC = showFB = showGoogle = False;
       } else if (record.Auth_Info__c.startsWith('GOOGLE')) {
           showSFDC = showFB = showLI = False;
       }

    }


    // All userinfo classes must support toString so we can compare them to stored record auth
    public interface Stringable { String toString(); }

    /* Begin SFDC Oauth classes */
    private class OauthTokenResponse{

        public string id;
        public string access_token;

    }
    private class IdentityResponse implements Stringable{

        public string user_id;
        public string organization_id;
        public string email;
        public string display_name;
        public string mobile_phone;
        public string username;
        public string first_name;
        public string last_name;
        public string sobjects;

        public Map<string,string> urls;

        public override String toString() {
            return 'SFDC|'+user_id+':'+organization_id;
        }

    }
    /* End SFDC Oauth classes */
}
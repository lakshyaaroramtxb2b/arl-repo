/**
 * Retrieves Contact Information(Only Contractors) from SupportForce and creates Employee__c in the target org
 * @author suresh.uppala
 */

global without sharing class SyncEmployeesFromSForce implements Schedulable,
                                                              Database.Batchable<sObject>,
                                                              Database.AllowsCallouts,
                                                              Database.Stateful {

    private static final String DAILY = '0 15 01 * * ?'; //every day
    //Execute Batch : SyncEmployeesFromSForce.scheduleJob('0 15 0 * * ? *');
    
    private static final String SOURCE_FILE = 'SyncEmployeesFromSForce';
    private static final Integer QUERY_LIMIT = 4000;

    private static final List<String> QUERY_FIELDS = new List<String> {'Id','ReportsTo.Name','ReportsTo.Employee_ID__c','Email',
                                                                       'ReportsTo.Email','ReportsTo.Title','lastModifiedDate',
                                                                       'Company_Agency_Name__c', 'Start_Date__c' };
    


    private Integer numberOfRecordsForSync = 0,
                    numberOfInsertedEmployees = 0;
                    
    private String fullErrorText;
    private Map<String,Id> mapEmployee;
                                                                 
    public SyncEmployeesFromSForce() {

    }

    public static String scheduleJob() {
        return scheduleJob(DAILY);
    }

    public static Id scheduleJob(String cronExpression) {
        return scheduleJob(cronExpression, null);
    }

    public static Id scheduleJob(String cronExpression, String eventName) {
        String jobName = SOURCE_FILE + ' ' + System.now();
        String jobId = System.schedule(jobName, cronExpression, new SyncEmployeesFromSForce());
        return jobId;
    }

    global void execute(SchedulableContext sc) {
        Database.executeBatch(this);
    }

    global List<sObject> start(Database.BatchableContext context){
        User[] users = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
        return users;
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        try {
                Datetime latestCreatedDate = getLatestEmployeeCreatedDate();
                Set<String> setManagerEmails = new Set<String>();

                getContractorEmailIds();

                List<sObject> lstSobject;
                List<INTEG_Employee__c> lstEmployees = new List<INTEG_Employee__c>();
                if(Test.isRunningTest()) {
                    lstSobject = getTestSobjectData();
                } else {
                    //lstSobject = sfutil.ESASupportForceUtil.fetchContactsFromSForceOrg(latestCreatedDate,QUERY_FIELDS,QUERY_LIMIT);
                    lstSobject = sfutil.ESASupportForceUtil.fetchContactsFromSupportForceOrg(latestCreatedDate,QUERY_FIELDS,QUERY_LIMIT,null);    
                }

                for(sObject sObj : lstSobject) {
                    Contact cont = (Contact) sObj;
                    INTEG_Employee__c employee = new INTEG_Employee__c();
                    employee.Id = mapEmployee.get(cont.Email);
                    employee.INTEG_Manager_Email__c = cont.ReportsTo.Email;
                    setManagerEmails.add(cont.ReportsTo.Email);
                    employee.INTEG_Manager_Title__c = cont.ReportsTo.Title;
                    employee.Company_Agency_Name__c = cont.Company_Agency_Name__c;
                    employee.SForceContactLastModifiedDate__c = cont.lastModifiedDate;
                    employee.INTEG_Hire_Date__c = cont.Start_Date__c;
                    lstEmployees.add(employee);
                }

                List<INTEG_Employee__c> lstManagerEmps = [SELECT INTEG_Org62UserId__c,INTEG_Email__c 
                                                          FROM INTEG_Employee__c
                                                          WHERE INTEG_Email__c IN : setManagerEmails];
                Map<String,Id> mapManagerIds = new Map<String,Id>();

                for(INTEG_Employee__c empl : lstManagerEmps) {
                    mapManagerIds.put(empl.INTEG_Email__c,empl.INTEG_Org62UserId__c);
                }

                for(INTEG_Employee__c emp : lstEmployees) {
                    emp.INTEG_Manager_ID__c = mapManagerIds.get(emp.INTEG_Manager_Email__c);
                }

                //upsert lstEmployees;

                //Schema.SObjectField extFld = INTEG_Employee__c.INTEG_Email__c;
                Database.saveResult[] updateResults = Database.update(lstEmployees,false);
                
            }catch(Exception exc) {
            processException(exc);
        }
    }

    private void getContractorEmailIds(){
        
        mapEmployee = new Map<String,ID>();

        List<INTEG_Employee__c> lstEmployees = [SELECT Id,INTEG_Email__c FROM INTEG_Employee__c
                                                WHERE isContractor__c = true];

        for(INTEG_Employee__c empl : lstEmployees) {
            mapEmployee.put(empl.INTEG_Email__c,empl.Id);
        }                                        
    } 

    @TestVisible
    private List<Sobject> getTestSobjectData() {
        List<sObject> lstSobj = new List<sObject>();
        Contact C;
        C = new Contact(firstName='abc',lastName='xyz',Business_Unit__c='ere',Company__c='444',CreatedDate=DateTime.Now(),
                           Division__c='3335',Email='suppala@gmail.com',Employee_ID__c='rif',Employee_Type__c='fulltime',Title='bafd',Phone='5103712635',
                           LeadSource='web');
        lstSobj.add((sObject)C);
        C = new Contact(firstName='abc1',lastName='xyz1',Business_Unit__c='ere',Company__c='444',CreatedDate=DateTime.Now(),
                           Division__c='3335',Email='suppala2@gmail.com',Employee_ID__c='rif',Employee_Type__c='fulltime',Title='bafd',Phone='5103712635',
                           LeadSource='web');
        lstSobj.add((sObject)C);
        return lstSobj;
    }

    @TestVisible
    private Datetime getLatestEmployeeCreatedDate() {
        List<INTEG_Employee__c> employee = [SELECT SForceContactLastModifiedDate__c
                                            FROM INTEG_Employee__c
                                            WHERE SForceContactLastModifiedDate__c != NULL
                                            ORDER BY SForceContactLastModifiedDate__c DESC NULLS LAST
                                            LIMIT 1];
        if(employee.isEmpty()) {
            return Datetime.newInstance(1970, 1, 1, 0, 0, 0); //only executed first time we run batch job
        } else {
            return employee[0].SForceContactLastModifiedDate__c;
        }
    }
    
    global void finish(Database.BatchableContext BC){
        //email the given address that the job is complete
        List<String> toAddresses = new List<String>();
        for(EmployeeSyncNotificationEmailIds__c emailIds:EmployeeSyncNotificationEmailIds__c.getAll().values()) {
            toAddresses.add(emailIds.EmailId__c);
        }

        String body = 'Sync from SupportForce complete.\n\n';
        if(String.isNotBlank(fullErrorText)) {
            body += 'No records inserted in target org. Insert failed with following error(s).' +
                    ' Please check the Events Debug Logs for more info:\n\n--------\n' + fullErrorText + '\n--------';
        } else {
            body += 'Number of records retrieved from Support Force org: ' + numberOfRecordsForSync;
            body += getInfoText();
        }

        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(toAddresses);
        mail.setReplyTo('noreply@salesforce.com');
        mail.setSubject('Email for Employee Sync from Supportforce Org');
        mail.setPlainTextBody(body);
        if(!Test.isRunningTest()) {
            //Messaging.sendEmail(new Messaging.SingleEmailMessage[]{mail});
        }
    }

    private String getInfoText() {
        return ('\nNumber of Employees Inserted/Updated in SecurityOrg : ' + numberOfInsertedEmployees);
    }

    private void processException(Exception exc) {
        /*Esa_DebugService.writeMessage('SyncEmployeesFromSForce.finish: Failed with errors');
        if (exc instanceof DMLException) {
            String errorString = '';
            Integer numErrors = exc.getNumDml();

            fullErrorText = 'There were ' + numErrors + ' DML errors: \n' +
                                   errorString + '\n';

            Esa_DebugService.writeException(exc, 1, SOURCE_FILE, fullErrorText);

        } else {
            //we got an unexpected non-dmlexception, write it
            fullErrorText = exc.getMessage();
            Esa_DebugService.writeException(exc, 1, SOURCE_FILE, 'Unhandled exception');
        }*/
    }
                                                  
                                                              
}
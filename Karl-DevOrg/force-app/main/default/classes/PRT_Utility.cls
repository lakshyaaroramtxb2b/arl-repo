/*
* Created by - Prashant Gupta
* Date - 09-09-2019
*/
public class PRT_Utility {
    
    /*
     * Created by - Prashant Gupta
     * Date - 09-09-2019
     * This functon is used to populate Program Lookups, 
     * This is being called from the child object triggers
     */
    public static void updateProgramLookups(List<sObject> newList, Map<id,sObject> oldMap){
        Map<id,Project__c> projectMap = new Map<id,Project__c>();
        set<id> projectIdSet = new Set<Id>();
        String projectFieldName = 'Project__c';
        List<sObject> updatedsObjectList = new List<sObject>();
        for(sObject record : newList){
            system.debug(record);
            if((oldMap==null &&  record.get(projectFieldName)!=null) || 
               (oldMap!=null && record.get(projectFieldName)!=null
                && record.get(projectFieldName) != oldMap.get((id)record.get('id')).get(projectFieldName))){
                    projectIdSet.add((id)record.get(projectFieldName));
                    updatedsObjectList.add(record);
            }
        } 
        system.debug('updatedsObjectList' + updatedsObjectList);
        system.debug('projectIdSet' + projectIdSet);
        projectMap = new Map<id,Project__c>([SELECT ID, Program__c FROM Project__c WHERE id in :projectIdSet ]);
        system.debug('projectMap' + projectMap);
        for(sObject mile :updatedsObjectList ){
            if(projectMap.containsKey((id)mile.get(projectFieldName)) && projectMap.get((id)mile.get(projectFieldName))!=null){
                mile.put('Program__c',(id)projectMap.get((id)mile.get(projectFieldName)).Program__c);
            }
            system.debug('mile' + mile);
        }
        
    }
    
    /*
     * Created by - Prashant Gupta
     * Date - 09-09-2019
     * This functon is used to populate Program Lookups, 
     * This overloaded function is being called from the parent object (Project__c) trigger
     */
    public static void updateProgramLookups(List<sObject> newList, Map<id,Project__c> projectMap, Boolean doUpdate){
        set<id> projectIdSet = new Set<Id>();
        String projectFieldName = 'Project__c';
        List<sObject> updatedsObjectList = new List<sObject>();
        for(sObject record : newList){
            system.debug(record);
            if(projectMap.containsKey((id)record.get(projectFieldName)) && projectMap.get((id)record.get(projectFieldName))!=null){
                record.put('Program__c',(id)projectMap.get((id)record.get(projectFieldName)).Program__c);
                updatedsObjectList.add(record);
            }
        }
        if(updatedsObjectList!=null && !updatedsObjectList.isEmpty() && doUpdate){
            update updatedsObjectList;
        }
    }
    
    /*
     * Created by - Prashant Gupta
     * This functon is used to create Sharing Record for project 
     * 
     */
    public static Project__Share prepareShareObject(Id projectId, Id userId, String accessType){
        Project__Share shareRecord = new Project__Share();
        shareRecord.ParentId  = projectId;
        shareRecord.UserOrGroupId = userId;
        shareRecord.AccessLevel  = accessType;
        shareRecord.RowCause = 'Manual';
        return shareRecord;
    }
    
    /*
     * Created by - Prashant Gupta
     * This functon is used to create Sharing Record for Change Request 
     * 
     */
    public static Change_Request__Share prepareChangeRequestShareObject(Id changeRequestId, Id userId, String accessType){
        Change_Request__Share shareRecord = new Change_Request__Share();
        shareRecord.ParentId  = changeRequestId;
        shareRecord.UserOrGroupId = userId;
        shareRecord.AccessLevel  = accessType;
        shareRecord.RowCause = 'Manual';
        return shareRecord;
    }
    
    
    /*
     * Created by - Prashant Gupta
     * This functon is used to create Sharing Record for Purchase Order 
     * 
     */
    public static Purchase_Order__Share preparePurchaseOrderShareObject(Id purchaseOrderId, Id userId, String accessType){
        Purchase_Order__Share shareRecord = new Purchase_Order__Share();
        shareRecord.ParentId  = purchaseOrderId;
        shareRecord.UserOrGroupId = userId;
        shareRecord.AccessLevel  = accessType;
        shareRecord.RowCause = 'Manual';
        return shareRecord;
    }
    
    
    /*
     * Created by - Prashant Gupta
     * This functon is used to check for required Fields on an object based on Fieldset Passed 
     * 
     */
    public Static void checkRequiredFields(List<sObject> recordList, Schema.FieldSet fieldSet){
        for(sObject record : recordList){
       		Boolean hasError = False; 
            String errorMsg  = null;
            for(Schema.FieldSetMember fld :fieldSet.getFields()) {
                system.debug('>>' + fld.getFieldPath() + '>>' + record.get(fld.getFieldPath()));
                if(record.get(fld.getFieldPath())==null 
                   || record.get(fld.getFieldPath())==''
                   //<p><br></p> denotes an empty rich text field
                  || record.get(fld.getFieldPath())=='<p><br></p>'){
                    hasError = true;
                       if(errorMsg==null)
                           errorMsg = fld.getLabel();
                       else
                           errorMsg += ', '+fld.getLabel();
                }
            }
            if(hasError){
                errorMsg +=' are required in order to Submit for Approval.';
                record.addError(errorMsg);
            }
        }
    }
    
     /*
     * Created by - Swarnima Singh Mandhata
     * Date - 02/14/2020
     * This functon is used for custom History Tracking
     */
    public static void milestoneHistoryTracking(List<sObject> newList,Map<id,sObject> oldMap,Schema.FieldSet fieldSet){
        List<History_Tracking__c> historyTrackingList = new List<History_Tracking__c>();
        for(sObject record:newList){
            for(Schema.FieldSetMember fld :fieldSet.getFields()){
                if(record.get(fld.getFieldPath()) != oldMap.get(record.id).get(fld.getFieldPath())){
                    History_Tracking__c historyTrackRecord =  new History_Tracking__c();
                    historyTrackRecord.Field__c = fld.getLabel();
                    historyTrackRecord.User__c = userInfo.getUserId();
                    historyTrackRecord.Object__c = String.valueOf((record.getSObjectType()));
                    historyTrackRecord.Date__c = date.today();
                    historyTrackRecord.Milestone__c = record.id;
                    historyTrackRecord.Old_Value__c = String.valueOf(oldMap.get(record.id).get(fld.getFieldPath()));
                    historyTrackRecord.New_Value__c = String.valueOf(record.get(fld.getFieldPath()));
                    historyTrackingList.add(historyTrackRecord);
                }
                
            }
            
        }
        System.debug('History tracking Record ' + historyTrackingList);
        if(historyTrackingList!=null || !historyTrackingList.isEmpty()){
            insert historyTrackingList;
        }
    }
    
}
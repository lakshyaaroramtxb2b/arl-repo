global with sharing class HW_AddonScheduler extends HW_RecordsStore{
    public HW_AddonScheduler(){}
    public static void unapprovedAddons(){
            HW_RecordsStore rs = new HW_RecordsStore();
            HW_RecordsStore.UserTypes ut = rs.usersCollection();
            Set<String> users = ut.users;
            Map<String, String> usernameMap = ut.usernameMap;
            Map<String, Set<String>> adminUserOrgs = ut.adminUserOrgs;
            Map<String, Set<String>> managerMap = ut.managerMap;
            Map<String, Integer> recsCount = ut.recsCount;

            //query unapproved addons
            Map<String, List<HW_RecordsStore.RecordsWithReducedAttributes>> unapproved_addons = new Map<String, List<HW_RecordsStore.RecordsWithReducedAttributes>>();
            //for(Heroku_Warden_addons__c rec : [SELECT name__c, LastModifiedDate, email__c, status__c, organizationId__c, Heroku_Warden_organizations__r.name__c, hk_addon_id__c, app_name__c, app_id__c FROM Heroku_Warden_addons__c WHERE Heroku_Warden_addonservices__r.status__c != 'approved' AND Heroku_Warden_addonservices__r.authorized__c = false AND LastModifiedDate >= LAST_N_DAYS:1]){
            for(Heroku_Warden_addons__c rec : [SELECT name__c, LastModifiedDate, email__c, status__c, organizationId__c, Heroku_Warden_organizations__r.name__c, hk_addon_id__c, app_name__c, app_id__c FROM Heroku_Warden_addons__c WHERE Heroku_Warden_addonservices__r.status__c != 'approved' AND Heroku_Warden_addonservices__r.human_name__c = 'SendGrid' AND Heroku_Warden_addonservices__r.authorized__c = false AND LastModifiedDate >= LAST_N_DAYS:1]){
                HW_RecordsStore.RecordsWithReducedAttributes ra = new HW_RecordsStore.RecordsWithReducedAttributes();
                ra.AddonName = rec.name__c;
                ra.Email = rec.email__c;
                ra.Status = rec.status__c;
                ra.OrgID = rec.organizationId__c;
                ra.OrgName = rec.Heroku_Warden_organizations__r.name__c;
                ra.AddonID = rec.hk_addon_id__c;
                ra.AppName = rec.app_name__c;
                ra.AppID = rec.app_id__c;
                ra.Query = 'get_unapproved_addons';
                ra.AlertType = 'Addon Unapproved';
                //ra.RecCount = recordsCount;
                if(unapproved_addons.containsKey(ra.OrgID)){
                    List<HW_RecordsStore.RecordsWithReducedAttributes> tmp = new List<HW_RecordsStore.RecordsWithReducedAttributes>(unapproved_addons.get(ra.OrgID));
                    tmp.add(ra);
                    unapproved_addons.put(ra.OrgID, tmp);
                }
                else{
                    unapproved_addons.put(ra.OrgID, new List<HW_RecordsStore.RecordsWithReducedAttributes>{ra});
                }
            }
            Map<String, List<HW_RecordsStore.RecordsWithReducedAttributes>> userMap = new  Map<String, List<HW_RecordsStore.RecordsWithReducedAttributes>>();
            for(String user: users){
                if(adminUserOrgs.containsKey(user)) {
                    for(String orgid: adminUserOrgs.get(user)){
                        //Adding records for unapproved addons
                        if(unapproved_addons.containsKey(orgid) && userMap.containsKey(user)){
                            List<HW_RecordsStore.RecordsWithReducedAttributes> tmp = new List<HW_RecordsStore.RecordsWithReducedAttributes>(userMap.get(user));
                            tmp.addAll(unapproved_addons.get(orgid));
                            userMap.put(user, tmp);
                        }
                        else if(unapproved_addons.containsKey(orgid)){
                            userMap.put(user, unapproved_addons.get(orgid));
                        }
                    }
                }
            }

           List<HW_RecordsStore.EmailsRecords> emailRecordList = new List<HW_RecordsStore.EmailsRecords>();
           for(String email: userMap.keySet()){
                List<HW_RecordsStore.RecordsWithReducedAttributes> recList = new List<HW_RecordsStore.RecordsWithReducedAttributes>(new Set<HW_RecordsStore.RecordsWithReducedAttributes>(userMap.get(email)));
                Integer len = recList.size();
                if(len > 10){
                    //split in to small chunks for limiting heap size for JSON serilization when iterating through the list
                    for(Integer i=0;i<len; i+=10){
                        List<HW_RecordsStore.RecordsWithReducedAttributes> tempList = new List<HW_RecordsStore.RecordsWithReducedAttributes>();
                        tempList.clear();
                        for(Integer j=i;j<i+10 && j<len;j++){
                            tempList.add(recList[j]);
                        }
                        HW_RecordsStore.EmailsRecords emailrec = new HW_RecordsStore.EmailsRecords();
                        emailrec.email=email;
                        emailrec.ownername=usernameMap.get(email);
                        emailrec.recsList=tempList;
                        emailrec.count =tempList.size();
                        emailrec.recsCount=recsCount;
                        emailRecordList.add(emailrec);
                    }
                }else {
                    HW_RecordsStore.EmailsRecords emailrec1 = new HW_RecordsStore.EmailsRecords();
                    emailrec1.email=email;
                    emailrec1.ownername=usernameMap.get(email);
                    emailrec1.recsList=recList;
                    emailrec1.count=len;
                    emailrec1.recsCount=recsCount;
                    emailRecordList.add(emailrec1);
                }   
            } 
            enrichPayload(emailRecordList, 'herokuwarden');
        }
        
        private static void enrichPayload(List<HW_RecordsStore.EmailsRecords> obj, String source_type){
            Map<String, Object> payloadMap = new Map<String, Object>();
            Integer recordsCount = obj.size();
            if(recordsCount < 100){
                payloadMap.put('records', obj);
                //payloadMap.put('query', query);
                payloadMap.put('source_type', source_type);
                CloudAMQPController.sendRequest(JSON.serialize(payloadMap));
            } else {
                List<HW_RecordsStore.EmailsRecords> tempList = new List<HW_RecordsStore.EmailsRecords>();
                for(Integer i=0;i<recordsCount; i+=100){
                    for(Integer j=i;j<i+100 && j<recordsCount;j++){
                        tempList.add(obj[j]);
                    }
                    payloadMap.put('records', tempList);
                    //payloadMap.put('query', query);
                    payloadMap.put('source_type', source_type);
                    CloudAMQPController.sendRequest(JSON.serialize(payloadMap));
                    tempList.clear();
                    payloadMap.clear();
                }
            }
    } 
    /*public with sharing class AddonProcessor implements Schedulable{
        public void execute(SchedulableContext ctx) {
            HW_AddonScheduler.unapprovedAddons();
        }
    }*/
}
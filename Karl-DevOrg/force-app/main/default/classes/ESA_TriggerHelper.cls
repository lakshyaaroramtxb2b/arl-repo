/*
 
    Developer Name: Jorge L Caceres - jcaceres@salesforce.com | August 2017    

    Description: Miscellaneous functios executed during triggers
    
*/

public class ESA_TriggerHelper {
    private static EsaService esaSvc = new EsaService();
    
    // populate the real entity code field with value from formula field as formula field cannot 
    // be used in sharing rules
    public static void populateEntityCode(List<ESA_Security_Request__c> newRequests) {    
        for (ESA_Security_Request__c request : newRequests) {
            request.Entity_Code2__c = request.Entity_Code__c;    
        }           
    }
    
    // assign general queue as owner
    public static void populateOwner(List<ESA_Security_Request__c> newRequests) {
        for (ESA_Security_Request__c request : newRequests) {
            //request.OwnerId = ESA_AppConstants.ESA_REQUEST_QUEUE_ID;    
        }                   
    }

    //Create Chatter Juction Record and Assign to Security Request
    public static void populateChatterJunctionRecordId(List<ESA_Security_Request__c> newRequests) {
        ESA_Security_Request_Chatter__c requestChatter;
        List<ESA_Security_Request_Chatter__c> lstRequestChatter = new List<ESA_Security_Request_Chatter__c>();

        Integer i=0;
        for(i=0;i<newRequests.size();i++) {
            requestChatter = new ESA_Security_Request_Chatter__c();
            lstRequestChatter.add(requestChatter);
        }
        insert lstRequestChatter;
        
        i = 0;
        for(ESA_Security_Request__c request : newRequests) {
            request.Chatter_Junction__c = lstRequestChatter[i].Id;
            i = i + 1;
        }
    }

    //Cancel Reservation incase SecurityRequest or Case/Work closed beforte the appointment date/time
    
    public static void cancelAppointmentsForClosedRequests(List<ESA_Security_Request__c> newRequests) {
         EsaService eService = new EsaService();
        for(ESA_Security_Request__c esaRequest : newRequests) {
            if(esaRequest.Status__c != 'Incomplete' && esaRequest.Case_Work_Closed_Date__c != null 
               && (esaRequest.Office_Hours_Reservation__c != null
                   && esaRequest.Office_Hours_Reservation__c >= esaRequest.Case_Work_Closed_Date__c)) {
                cancelAppointment(esaRequest);
            }
        }
    }

    private static void cancelAppointment(ESA_Security_Request__c securityRequest) {
        
        Esa_Entity__c esaEntity = [SELECT Google_Calendar_Name__c
                                   FROM ESA_Entity__C
                                   WHERE EntityCode__c =:securityRequest.Entity_Code__c];

        try{
              cancelReservation(securityRequest.GCal_EventId__c,esaEntity.Google_Calendar_Name__c);
              securityRequest.GCal_EventId__c = '';
              securityRequest.Office_Hours__c = '';
              securityRequest.Reservation_Key__c = '';
        }Catch(Exception ex){
          Esa_DebugService.WriteException(ex,'Unable to delete the calendar');
        }
       
    }

    @future (callout=true)
    public static void cancelReservation(String eventId, String calendarName) {
        // instantiate google util to access entity calendar
        esagutil.ESAGoogleOAuthUtil googleAPI;
        if (calendarName == null) {
            googleAPI = new esagutil.ESAGoogleOAuthUtil();  // this will default to esa.coordinator calendar
        } else {
            googleAPI = new esagutil.ESAGoogleOAuthUtil(calendarName);
        }
        googleAPI.deleteEvent(eventId);
    }
    
    public static void closeRequest(String secReqId, String extStatus, String comment) {
        esaSvc.closeInTakeRequest(secReqId, extStatus, comment);
    } 

    public static void manageSForceCaseChatterSubscription(List<Case> cases) {
        if (Limits.getQueueableJobs() >= Limits.getLimitQueueableJobs()) {
            return;
        }
        List<Case> casesToUpdate = new List<Case>();
        // Filter the records else might hit too many Queueable Job Limits
        for (Case caseRec : cases) {
            if (SForceCaseChatterFollowHelper.isEntSecRecTypeWithSFCase(caseRec) 
                    && SForceCaseChatterFollowHelper.isChatterSubscriptionChange(caseRec)) {
                casesToUpdate.add(caseRec);
            }
        }
        if (!casesToUpdate.isEmpty()) {
            System.enqueueJob(new SForceCaseChatterFollowHelper.ManageChatterSubscriptionJob(casesToUpdate));
        }  
    }

    public static void deleteSForceCaseChatterSubscription(List<Case> cases) {
        if (Limits.getQueueableJobs() >= Limits.getLimitQueueableJobs()) {
            return;
        }
        List<Case> casesToUpdate = new List<Case>();
        // Filter the records else might hit too many Queueable Job Limits
        for (Case caseRec : cases) {
            if (SForceCaseChatterFollowHelper.hasChatterSubscription(caseRec)) {
                casesToUpdate.add(caseRec);
            }
        }
        if (!casesToUpdate.isEmpty()) {
            System.enqueueJob(new SForceCaseChatterFollowHelper.DeleteChatterSubscriptionJob(casesToUpdate));
        }    
    }
    
}
/*
 
    Developer Name: Jorge L Caceres - jcaceres@salesforce.com | July 2018    

    Description: Rollup Data Center data into Provider
    
*/

public class DC_RollUp {

    public static void rollUpClouds (List<DC_DataCenter__c> dataCenters, Map<Id, DC_DataCenter__c>oldDataCenters) {

        // collect all providers
        Map<Id, DC_Provider__c> providersMap = new Map<Id, DC_Provider__c>();
        for (DC_DataCenter__c dc : dataCenters) {
            DC_Provider__c provider = providersMap.get(dc.DC_Provider__c);
            if (provider == null) { 
                provider = new DC_Provider__c(Id = dc.DC_Provider__c, Business_Unit__c = dc.Business_Unit__c);
                providersMap.put(dc.DC_Provider__c, provider);
            } else {
                Set<String> clouds = new Set<String>(provider.Business_Unit__c.split(';'));
                clouds.addAll(dc.Business_Unit__c.split(';'));
                Provider.Business_Unit__c = String.join(new List<String>(clouds), ';');
            }  
        }
        update providersMap.values();    

    }
}
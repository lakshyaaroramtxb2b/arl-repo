/*
 * test class to support the testing of escalation and notification of trail completion
*/
@isTest
private class SC_EscalationBatchTest {

    // this test method is very verbose on purpose I wanted to make sure I test as close
    // to production as posible

    static List<Training_Course__c> courses;
    
    static List<Training_Escalation_Profile__c> profiles;
    static Training_Escalation_Profile__c pro1;
    static Training_Escalation_Profile__c pro2;
    static Training_Escalation_Profile__c pro3;

    static List<Training_Course_Taken__c> takens;
    static Training_Course_Taken__c takenOverdueLight;
    static Training_Course_Taken__c takenFirstLight;
    static Training_Course_Taken__c takenSecondLight;
    static Training_Course_Taken__c takenThirdLight;

    static User testUser;
    static Contact testUserCon;

    static {
        SC_EscalationBatchTest.staticSetup();
    }

    static void staticSetup() {
        SC_EscalationBatchTest.profiles = new List<Training_Escalation_Profile__c>();

        SC_EscalationBatchTest.pro1 = new Training_Escalation_Profile__c(
            Name = 'Test Light',
            First_Reminder_Days__c = 14.0,
            First_Reminder_Email_Template__c = 'Training_Light_Escalation_1_Email',
            First_Reminder_Include_Manager__c = True,
            Overdue_Reminder_Email_Template__c = 'Training_Light_Escalation_1_Email',
            Overdue_Reminder_Frequency_Days__c = 3.0,
            Overdue_Reminder_Include_Manager__c = False,
            Second_Reminder_Days__c = 7.0,
            Second_Reminder_Email_Template__c = 'Training_Light_Escalation_1_Email',
            Second_Reminder_Include_Manager__c = False,
            Third_Reminder_Days__c = 3.0,
            Third_Reminder_Email_Template__c = 'Training_Light_Escalation_1_Email',
            Third_Reminder_Include_Manager__c = False
        );
        SC_EscalationBatchTest.profiles.add(SC_EscalationBatchTest.pro1);
        SC_EscalationBatchTest.pro2 = new Training_Escalation_Profile__c(
            Name = 'Test Standard',
            First_Reminder_Days__c = 13.0,
            First_Reminder_Email_Template__c = 'Training_Light_Escalation_1_Email',
            First_Reminder_Include_Manager__c = False,
            Overdue_Reminder_Email_Template__c = 'Training_Light_Escalation_1_Email',
            Overdue_Reminder_Frequency_Days__c = 2.0,
            Overdue_Reminder_Include_Manager__c = True,
            Second_Reminder_Days__c = 6.0,
            Second_Reminder_Email_Template__c = 'Training_Light_Escalation_1_Email',
            Second_Reminder_Include_Manager__c = False,
            Third_Reminder_Days__c = 2.0,
            Third_Reminder_Email_Template__c = 'Training_Light_Escalation_1_Email',
            Third_Reminder_Include_Manager__c = True
        );
        SC_EscalationBatchTest.profiles.add(SC_EscalationBatchTest.pro2);
        SC_EscalationBatchTest.pro3 = new Training_Escalation_Profile__c(
            Name = 'Test Wonder',
            First_Reminder_Days__c = 12.0,
            First_Reminder_Email_Template__c = 'Training_Light_Escalation_1_Email',
            First_Reminder_Include_Manager__c = False,
            Overdue_Reminder_Email_Template__c = 'Training_Light_Escalation_1_Email',
            Overdue_Reminder_Frequency_Days__c = 1.0,
            Overdue_Reminder_Include_Manager__c = True,
            Second_Reminder_Days__c = 5.0,
            Second_Reminder_Email_Template__c = 'Training_Light_Escalation_1_Email',
            Second_Reminder_Include_Manager__c = False,
            Third_Reminder_Days__c = 1.0,
            Third_Reminder_Email_Template__c = 'Training_Light_Escalation_1_Email',
            Third_Reminder_Include_Manager__c = True
        );
        SC_EscalationBatchTest.profiles.add(SC_EscalationBatchTest.pro3);
        insert SC_EscalationBatchTest.profiles;

        SC_EscalationBatchTest.courses = new List<Training_Course__c>();
        SC_EscalationBatchTest.courses.add(new Training_Course__c(
            Name = 'Test Training Course pro1',
            Provider__c = 'Trailhead',
            Tracked__c = True,
            Training_Escalation_Profile__c = pro1.Id,
            Is_Active__c = True,
            Course_Level__c = 'Trail',
            URL__c = 'www.test.com'
        ));

        SC_EscalationBatchTest.courses.add(new Training_Course__c(
            Name = 'Test Training Course pro2',
            Provider__c = 'Trailhead',
            Tracked__c = True,
            Training_Escalation_Profile__c = pro2.Id,
            Is_Active__c = True,
            Course_Level__c = 'Trail',
            URL__c = 'www.test.com'
        ));

        SC_EscalationBatchTest.courses.add(new Training_Course__c(
            Name = 'Test Training Course pro3',
            Provider__c = 'Trailhead',
            Tracked__c = True,
            Training_Escalation_Profile__c = pro3.Id,
            Is_Active__c = True,
            Course_Level__c = 'Trail',
            URL__c = 'www.test.com'
        ));
        insert SC_EscalationBatchTest.courses;

        SC_EscalationBatchTest.testUser = [select Id, ContactId from User where Contact.RecordType.Name = 'Salesforce Employee' limit 1];
        SC_EscalationBatchTest.testUserCon = new Contact(
            Id = SC_EscalationBatchTest.testUser.ContactId,
            Manager_Email__c = 'manager.test@test.com'
        );
        update SC_EscalationBatchTest.testUserCon;

        SC_EscalationBatchTest.takens = new List<Training_Course_Taken__c>();
        SC_EscalationBatchTest.takenOverdueLight = new Training_Course_Taken__c(
            RecordTypeId = TrainingCourseTaken_Constants.RT_ID_MANDATORY,
            Contact__c = testUser.ContactId,
            Due_Date__c = System.today().addDays(-1),
            Last_Escalation__c = System.today().addDays(-3),
            Escalation1__c = System.today().addDays(-14),
            Escalation2__c = System.today().addDays(-7),
            Escalation3__c = System.today().addDays(-3),
            Training_Course__c = courses[0].Id,
            Attendee_Email__c = 'test@test.com'
        );
        SC_EscalationBatchTest.takens.add(SC_EscalationBatchTest.takenOverdueLight);

        SC_EscalationBatchTest.takenFirstLight = new Training_Course_Taken__c(
            RecordTypeId = TrainingCourseTaken_Constants.RT_ID_MANDATORY,
            Contact__c = testUser.ContactId,
            Due_Date__c = System.today().addDays(14),
            Last_Escalation__c = null,
            Training_Course__c = courses[0].Id,
            Attendee_Email__c = 'test@test.com'
        );
        SC_EscalationBatchTest.takens.add(SC_EscalationBatchTest.takenFirstLight);

        SC_EscalationBatchTest.takenSecondLight = new Training_Course_Taken__c(
            RecordTypeId = TrainingCourseTaken_Constants.RT_ID_MANDATORY,
            Contact__c = testUser.ContactId,
            Due_Date__c = System.today().addDays(7),
            Last_Escalation__c = System.today().addDays(-14),
            Escalation1__c = System.today().addDays(-14),
            Training_Course__c = courses[0].Id,
            Attendee_Email__c = 'test@test.com'
        );
        SC_EscalationBatchTest.takens.add(SC_EscalationBatchTest.takenSecondLight);

        SC_EscalationBatchTest.takenThirdLight = new Training_Course_Taken__c(
            RecordTypeId = TrainingCourseTaken_Constants.RT_ID_MANDATORY,
            Contact__c = testUser.ContactId,
            Due_Date__c = System.today().addDays(3),
            Last_Escalation__c = System.today().addDays(-7),
            Escalation1__c = System.today().addDays(-14),
            Escalation2__c = System.today().addDays(-7),
            Training_Course__c = courses[0].Id,
            Attendee_Email__c = 'test@test.com'
        );
        SC_EscalationBatchTest.takens.add(SC_EscalationBatchTest.takenThirdLight);
        insert SC_EscalationBatchTest.takens;
    }


    static testMethod void testEscalationBatch() {

        Test.startTest();
        Database.executeBatch(new SC_EscalationBatch());
        Test.stopTest();

        Boolean hasTask = False;

        SC_EscalationBatchTest.takenOverdueLight = [
            select Id, Last_Escalation__c,
            (select Id, Subject from Tasks)
            from Training_Course_Taken__c
            where Id = :takenOverdueLight.Id
        ];
        System.assertEquals(
            System.today(),
            takenOverdueLight.Last_Escalation__c.date(),
            'Failed Overdue Light Last Escalation test'
        );
        for (Task t :takenOverdueLight.Tasks) {
            if (t.Subject.contains('Overdue')) {
                hasTask = True;
            }
        }
        System.assert(
            hasTask,
            'Failed to save the Overdue escalation task'
        );

        SC_EscalationBatchTest.takenFirstLight = [
            select Id, Escalation1__c, Last_Escalation__c, Date_of_Escalation_to_Manager__c,
            (select Id, Subject from Tasks)
            from Training_Course_Taken__c
            where Id = :takenFirstLight.Id
        ];
        System.assertEquals(
            System.today(),
            takenFirstLight.Escalation1__c,
            'Failed First Light Escalation1 Escalation test'
        );
        System.assertEquals(
            System.today(),
            takenFirstLight.Last_Escalation__c.date(),
            'Failed First Light Last Escalation test'
        );
        for (Task t :takenFirstLight.Tasks) {
            if (t.Subject.contains('First Reminder')) {
                hasTask = True;
            }
        }
        System.assert(
            hasTask,
            'Failed to save the first escalation task'
        );
        System.assertEquals(
            System.today(),
            takenFirstLight.Date_of_Escalation_to_Manager__c,
            'Failed First Light Escalation to Manager test'
        );

        SC_EscalationBatchTest.takenSecondLight = [
            select Id, Escalation2__c, Last_Escalation__c, Date_of_Escalation_to_Manager__c,
            (select Id, Subject from Tasks)
            from Training_Course_Taken__c
            where Id = :takenSecondLight.Id
        ];
        System.assertEquals(
            System.today(),
            takenSecondLight.Escalation2__c,
            'Failed Second Light Escalation2 Escalation test'
        );
        System.assertEquals(
            System.today(),
            takenSecondLight.Last_Escalation__c.date(),
            'Failed Second Light Last Escalation test'
        );
        hasTask = False;
        for (Task t :takenSecondLight.Tasks) {
            if (t.Subject.contains('Second Reminder')) {
                hasTask = True;
            }
        }
        System.assert(
            hasTask,
            'Failed to save the Second escalation task'
        );
        System.assertNotEquals(
            System.today(),
            takenSecondLight.Date_of_Escalation_to_Manager__c,
            'Failed Second Light Escalation to Manager test - not to change - negative test'
        );

        SC_EscalationBatchTest.takenThirdLight = [
            select Id, Escalation3__c, Last_Escalation__c,
            (select Id, Subject from Tasks)
            from Training_Course_Taken__c
            where Id = :takenThirdLight.Id
        ];
        System.assertEquals(
            System.today(),
            takenThirdLight.Escalation3__c,
            'Failed Third Light Escalation3 Escalation test'
        );
        System.assertEquals(
            System.today(),
            takenThirdLight.Last_Escalation__c.date(),
            'Failed Third Light Last Escalation test'
        );
        hasTask = False;
        for (Task t :takenThirdLight.Tasks) {
            if (t.Subject.contains('Third Reminder')) {
                hasTask = True;
            }
        }
        System.assert(
            hasTask,
            'Failed to save the Third escalation task'
        );
    }

    // negative test 1 runs the batch but the due date is greater than the esalation reminder
    static testMethod void testNegative1() {
        SC_EscalationBatchTest.takens = new List<Training_Course_Taken__c>();
        // set last escalation for 2 days ago, which is within the 3 day frequency of overdue reminders.
        SC_EscalationBatchTest.takenOverdueLight.Due_Date__c = System.today().addDays(-1);
        SC_EscalationBatchTest.takenOverdueLight.Last_Escalation__c = System.today().addDays(-1);
        SC_EscalationBatchTest.takenOverdueLight.Escalation1__c = System.today().addDays(-16);
        SC_EscalationBatchTest.takenOverdueLight.Escalation2__c = System.today().addDays(-9);
        SC_EscalationBatchTest.takenOverdueLight.Escalation3__c = System.today().addDays(-5);
        SC_EscalationBatchTest.takens.add(SC_EscalationBatchTest.takenOverdueLight);

        // set due date to more than the first reminder
        SC_EscalationBatchTest.takenFirstLight.Due_Date__c = System.today().addDays(15);
        SC_EscalationBatchTest.takenFirstLight.Last_Escalation__c = null;
        SC_EscalationBatchTest.takens.add(SC_EscalationBatchTest.takenFirstLight);

        
        // set due date to more than the second reminder
        SC_EscalationBatchTest.takenSecondLight.Due_Date__c = System.today().addDays(9);
        SC_EscalationBatchTest.takenSecondLight.Last_Escalation__c = System.today().addDays(-5);
        SC_EscalationBatchTest.takenSecondLight.Escalation1__c = System.today().addDays(-5);
        SC_EscalationBatchTest.takens.add(SC_EscalationBatchTest.takenSecondLight);
    
        // set due date to more than the third reminder
        SC_EscalationBatchTest.takenThirdLight.Due_Date__c = System.today().addDays(4);
        SC_EscalationBatchTest.takenThirdLight.Last_Escalation__c = System.today().addDays(-3);
        SC_EscalationBatchTest.takenThirdLight.Escalation1__c = System.today().addDays(-10);
        SC_EscalationBatchTest.takenThirdLight.Escalation2__c = System.today().addDays(-3);
        SC_EscalationBatchTest.takens.add(SC_EscalationBatchTest.takenThirdLight);

        update SC_EscalationBatchTest.takens;

        SC_EscalationBatchTest.takenOverdueLight = [
            select Id, Last_Escalation__c
            from Training_Course_Taken__c
            where Id = :SC_EscalationBatchTest.takenOverdueLight.Id
        ];

        Test.startTest();
        Database.executeBatch(new SC_EscalationBatch());
        Test.stopTest();

        SC_EscalationBatchTest.takenOverdueLight = [
            select Id, Last_Escalation__c
            from Training_Course_Taken__c
            where Id = :SC_EscalationBatchTest.takenOverdueLight.Id
        ];
        System.assertEquals(
            System.today().addDays(-2),
            takenOverdueLight.Last_Escalation__c.date(),
            'Failed Negative Overdue 1 Test - caused action'
        );

        SC_EscalationBatchTest.takenFirstLight = [
            select Id, Last_Escalation__c
            from Training_Course_Taken__c
            where Id = :SC_EscalationBatchTest.takenFirstLight.Id
        ];
        System.assertEquals(
            null,
            SC_EscalationBatchTest.takenFirstLight.Last_Escalation__c,
            'Failed Negative First Escalation 1 Test - caused action'
        );

        SC_EscalationBatchTest.takenSecondLight = [
            select Id, Last_Escalation__c
            from Training_Course_Taken__c
            where Id = :SC_EscalationBatchTest.takenSecondLight.Id
        ];
        System.assertEquals(
            System.today().addDays(-6),
            SC_EscalationBatchTest.takenSecondLight.Last_Escalation__c.date(),
            'Failed Negative Second Escalation 1 Test - caused action'
        );

        SC_EscalationBatchTest.takenThirdLight = [
            select Id, Last_Escalation__c
            from Training_Course_Taken__c
            where Id = :takenThirdLight.Id
        ];
        System.assertEquals(
            System.today().addDays(-4),
            SC_EscalationBatchTest.takenThirdLight.Last_Escalation__c.date(),
            'Failed Negative SeThirdcond Escalation 1 Test - caused action'
        );
    }

    // negative test 2 runs the batch after the each escalation was sent. ie. the next day for each.
    // note I am moving the due date instead of making the test run in the future
    static testMethod void testNegative2() {
        SC_EscalationBatchTest.takens = new List<Training_Course_Taken__c>();

        // set due date to less than the first reminder
        SC_EscalationBatchTest.takenFirstLight.Due_Date__c = System.today().addDays(13);
        SC_EscalationBatchTest.takenFirstLight.Escalation1__c = System.today().addDays(-1);
        SC_EscalationBatchTest.takenFirstLight.Last_Escalation__c = System.today().addDays(-1);
        SC_EscalationBatchTest.takens.add(SC_EscalationBatchTest.takenFirstLight);

        
        // set due date to less than the second reminder
        SC_EscalationBatchTest.takenSecondLight.Due_Date__c = System.today().addDays(6);
        SC_EscalationBatchTest.takenSecondLight.Last_Escalation__c = System.today().addDays(-1);
        SC_EscalationBatchTest.takenSecondLight.Escalation1__c = System.today().addDays(-7);
        SC_EscalationBatchTest.takenSecondLight.Escalation2__c = System.today().addDays(-1);
        SC_EscalationBatchTest.takens.add(SC_EscalationBatchTest.takenSecondLight);
    
        // set due date to less than the third reminder
        SC_EscalationBatchTest.takenThirdLight.Due_Date__c = System.today().addDays(2);
        SC_EscalationBatchTest.takenThirdLight.Last_Escalation__c = System.today().addDays(-1);
        SC_EscalationBatchTest.takenThirdLight.Escalation1__c = System.today().addDays(-11);
        SC_EscalationBatchTest.takenThirdLight.Escalation2__c = System.today().addDays(-4);
        SC_EscalationBatchTest.takenThirdLight.Escalation3__c = System.today().addDays(-1);
        SC_EscalationBatchTest.takens.add(SC_EscalationBatchTest.takenThirdLight);

        update SC_EscalationBatchTest.takens;

        Test.startTest();
        Database.executeBatch(new SC_EscalationBatch());
        Test.stopTest();

        SC_EscalationBatchTest.takenFirstLight = [
            select Id, Last_Escalation__c
            from Training_Course_Taken__c
            where Id = :SC_EscalationBatchTest.takenFirstLight.Id
        ];
        System.assertEquals(
            System.today().addDays(-2),
            SC_EscalationBatchTest.takenFirstLight.Last_Escalation__c.date(),
            'Failed Negative First Escalation 2 Test - caused action'
        );

        SC_EscalationBatchTest.takenSecondLight = [
            select Id, Last_Escalation__c
            from Training_Course_Taken__c
            where Id = :SC_EscalationBatchTest.takenSecondLight.Id
        ];
        System.assertEquals(
            System.today().addDays(-2),
            SC_EscalationBatchTest.takenSecondLight.Last_Escalation__c.date(),
            'Failed Negative Second Escalation 2 Test - caused action'
        );

        SC_EscalationBatchTest.takenThirdLight = [
            select Id, Last_Escalation__c
            from Training_Course_Taken__c
            where Id = :takenFirstLight.Id
        ];
        System.assertEquals(
            System.today().addDays(-2),
            SC_EscalationBatchTest.takenThirdLight.Last_Escalation__c.date(),
            'Failed Negative SeThirdcond Escalation 2 Test - caused action'
        );
    }

    // negative test 3 runs the batch for 2 and 3 escalation but 1 and 2 were never set respectively.
    static testMethod void testNegative3() {
        SC_EscalationBatchTest.takens = new List<Training_Course_Taken__c>();

        // set due date to less than the second reminder
        SC_EscalationBatchTest.takenSecondLight.Due_Date__c = System.today().addDays(6);
        SC_EscalationBatchTest.takenSecondLight.Last_Escalation__c = null;
        SC_EscalationBatchTest.takenSecondLight.Escalation1__c = null;
        SC_EscalationBatchTest.takenSecondLight.Escalation2__c = null;
        SC_EscalationBatchTest.takens.add(SC_EscalationBatchTest.takenSecondLight);
    
        // set due date to less than the third reminder
        SC_EscalationBatchTest.takenThirdLight.Due_Date__c = System.today().addDays(2);
        SC_EscalationBatchTest.takenThirdLight.Last_Escalation__c = null;
        SC_EscalationBatchTest.takenThirdLight.Escalation1__c = null;
        SC_EscalationBatchTest.takenThirdLight.Escalation2__c = null;
        SC_EscalationBatchTest.takenThirdLight.Escalation3__c = null;
        SC_EscalationBatchTest.takens.add(SC_EscalationBatchTest.takenThirdLight);

        update SC_EscalationBatchTest.takens;

        Test.startTest();
        Database.executeBatch(new SC_EscalationBatch());
        Test.stopTest();

        // with the current work flow these tests are true, I am not sure if ths is the desired outcome or
        // we need to make sure Escalation doesn't set Escalations at all.
        SC_EscalationBatchTest.takenSecondLight = [
            select Id, Last_Escalation__c, Escalation2__c, Escalation3__c
            from Training_Course_Taken__c
            where Id = :SC_EscalationBatchTest.takenSecondLight.Id
        ];
        System.assertEquals(
            System.today(),
            SC_EscalationBatchTest.takenSecondLight.Last_Escalation__c.date(),
            'Failed Negative Second Escalation 3 Test - Last Escalation was not set'
        );
        System.assertEquals(
            System.today(),
            SC_EscalationBatchTest.takenSecondLight.Escalation2__c,
            'Failed Negative Second Escalation 3 Test - Escalation1 was not set'
        );
        System.assertEquals(
            null,
            SC_EscalationBatchTest.takenSecondLight.Escalation3__c,
            'Failed Negative Second Escalation 3 Test - Escalation2 was set for some reason'
        );

        SC_EscalationBatchTest.takenThirdLight = [
            select Id, Last_Escalation__c, Escalation1__c, Escalation3__c
            from Training_Course_Taken__c
            where Id = :takenFirstLight.Id
        ];
        System.assertEquals(
            System.today(),
            SC_EscalationBatchTest.takenThirdLight.Last_Escalation__c.date(),
            'Failed Negative Second Escalation 3 Test - Last Escalation was not set'
        );
        System.assertEquals(
            System.today(),
            SC_EscalationBatchTest.takenThirdLight.Escalation1__c,
            'Failed Negative Second Escalation 3 Test - Escalation1 was not set'
        );
        System.assertEquals(
            null,
            SC_EscalationBatchTest.takenThirdLight.Escalation3__c,
            'Failed Negative Second Escalation 3 Test - Escalation3 was set for some reason'
        );
    }

    // Test overdue to not send the day of being overdue
    static testMethod void testOverdue() {
        SC_EscalationBatchTest.takenOverdueLight.Due_Date__c = System.today();
        SC_EscalationBatchTest.takenOverdueLight.Last_Escalation__c = System.today().addDays(-3);
        SC_EscalationBatchTest.takenOverdueLight.Escalation1__c = System.today().addDays(-14);
        SC_EscalationBatchTest.takenOverdueLight.Escalation2__c = System.today().addDays(-7);
        SC_EscalationBatchTest.takenOverdueLight.Escalation3__c = System.today().addDays(-3);

        update SC_EscalationBatchTest.takenOverdueLight;

        Test.startTest();
        Database.executeBatch(new SC_EscalationBatch());
        Test.stopTest();

        Boolean hasTask = False;

        SC_EscalationBatchTest.takenOverdueLight = [
            select Id, Last_Escalation__c,
            (select Id, Subject from Tasks)
            from Training_Course_Taken__c
            where Id = :takenOverdueLight.Id
        ];
        // note this is assertNOT as this test should not update the taken record.
        System.assertNotEquals(
            System.today(),
            takenOverdueLight.Last_Escalation__c.date(),
            'Failed Overdue Light Last Escalation test for same day'
        );
        for (Task t :takenOverdueLight.Tasks) {
            if (t.Subject.contains('Overdue')) {
                hasTask = True;
            }
        }
        System.assert(
            !hasTask,
            'Failed to save the Overdue escalation task for same day'
        );        
    }
}
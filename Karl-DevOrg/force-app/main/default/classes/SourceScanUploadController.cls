public with sharing class SourceScanUploadController {

    public String preset {get; set; }
    public String description {get; set;}               
    public String onBehalfOf {get; set;}
	public Attachment attachment {
      get {
          if (attachment == null) {
            attachment = new Attachment();
          }
          return attachment;
        }
      set;
      }
    
    public SourceScanUploadController() {
        
    }
    
    public List<SelectOption> getPresets() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('PortalSecurity','Portal Security'));
        options.add(new SelectOption('PortalAll','Portal Security and Quality'));
        return options;              
    }
    public void setPreset(String value) {
        preset = value;
    }
    
    public PageReference upload() {
        //validation
        if (!validateEmail(onBehalfOf)) {
            return null;
        }
        
        if (attachment.body == null) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'No Attachment provided'));
            return null;			
        }
        if ((description == null) || (description == '')) {
            description = 'Scan ran by support';
        }
        
		//create Zip Upload object
	
    	Zip_Upload__c zu = new Zip_Upload__c();
        
        zu.CodeAttached__c = true;
        zu.Email_Results_To__c = UserInfo.getUserEmail();
        zu.Requestor__c = onBehalfOf;
        zu.Scan_Type__c = preset;
        zu.Friendly_Name__c = description;
        zu.WorkState__c = 'New';
        
        try {
            insert zu;
        } catch (DMLException e) {
          	ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error creating job'));
            //abort if we can't create the job
		  	return null;
        } 
        
        //now create attachment
        attachment.OwnerId = UserInfo.getUserId();
        attachment.ParentId = zu.Id; 
        attachment.IsPrivate = false; //access should inherit from parent

        try {
        	insert attachment;
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Job Id: ' + String.valueOf(zu.Id) + ' created successfully'));
        } catch (DMLException e) {
          ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error uploading attachment'));
          //kill job. If this fails, we'll hit finally.
          	zu.WorkState__c = 'Never';
            zu.Comments__c = 'Failed trying to upload package';
            update zu;
          	return null;
        } finally {
            // clear the attachment so that it's not returned
            // in viewstate, creating viewstate too large errors.
          	attachment = new Attachment(); 
        }
        return null;
    }
    

    public static Boolean validateEmail(String email) {
        Boolean res = true;
            
        // source: http://www.regular-expressions.info/email.html
        String emailRegex = '^[a-zA-Z0-9._|\\\\%#~`=?&/$^*!}{+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$'; 
        Pattern MyPattern = Pattern.compile(emailRegex);
        Matcher MyMatcher = MyPattern.matcher(email);
    
        if (!MyMatcher.matches()) {
           ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL,'Invalid Email'));
           res = false;
        }
        return res;	    
    }
    
}
/**
 * @author Lakshya Arora
 * @email lakshya.arora@mtxb2b.com
* @desc This class contains resuable method to create the Gus work records
 */
public with sharing class KARL_GusUtility {
    public static Map<String,String> baseRecordUrl = KARL_Utility.getBaseURL();

    /**
     * @author Vishnu Kumar
     * @email vishnu.kumar@mtxb2b.com
     * @description This method trigger the future method to create GUS records from Cycle Request Item and find/update EPIC records.
    */

    @future
    public static void processGUSRecordCreation(Set<Id> cycleReqItemIds, Set<Id> cycleIds, Set<Id> dependentCycleItemsIdSet){
        System.debug(LoggingLevel.DEBUG, 'processGUSRecordCreation: processCycleIdSet= '+cycleReqItemIds);
        System.debug(LoggingLevel.DEBUG, 'processGUSRecordCreation: dependentCycleItemsIdSet = '+dependentCycleItemsIdSet);
        //Variables
        List<KARL_GUS_SLA_Tracking__c> gusSlaTrackingList = new List<KARL_GUS_SLA_Tracking__c>();
        List<ADM_Work_c__x> WorkList = new List<ADM_Work_c__x>();
        Map<Id, Cycle_Request_Item__c> cRewItemIds = new Map<Id, Cycle_Request_Item__c>();
        Set<String> epics = new Set<String>();

        //Changes by Mahima Aggarwal Start
        Map<Id,Id> cycleReqIdToEvidenceIdMap = new Map<Id,Id>();
        List<KARL_Evidence_Request__c> evidenceReqUpdateList = new List<KARL_Evidence_Request__c>();

        // Generate KARL Evidence Community URL
        Network karlEvidenceNetwork;
        String loginurl,communityHomeUrl ='';
        Integer index;
        if(!Test.isRunningTest()){
            karlEvidenceNetwork = [SELECT Id FROM Network WHERE Name ='KARL Evidence Request' ];
            loginurl = Network.getLoginUrl(karlEvidenceNetwork.id);
            index = loginurl.lastIndexOf('/login');
            communityHomeUrl = loginurl.substring(0, index + 1);
            System.debug(LoggingLevel.DEBUG, 'MyDebugURL: ' + communityHomeUrl);
        }

        for(KARL_Evidence_Request__c evidenceReq : [SELECT Id, KARL_Submitted__c ,KARL_Cycle_ARL_Item__c
                                                            FROM KARL_Evidence_Request__c
                                                            WHERE KARL_Cycle_ARL_Item__c IN : cycleReqItemIds
                                                            WITH SECURITY_ENFORCED]){
            cycleReqIdToEvidenceIdMap.put(evidenceReq.KARL_Cycle_ARL_Item__c,evidenceReq.Id);
        }
        //Changes by Mahima Aggarwal end

        // Create map of Google Drive folders for evidence links
        Map<Id, Map<String,String>> cycleDriveFolders = new Map<Id, Map<String,String>>();

        for(KARL_Audit_Cycle_Drive__c cycleDrives : [SELECT Id, KARL_Drive_Scope__c ,KARL_Drive_URL__c, KARL_Audit_Cycle__c
                                                            FROM KARL_Audit_Cycle_Drive__c
                                                            WHERE KARL_Audit_Cycle__c IN : cycleIds
                                                            WITH SECURITY_ENFORCED]){
            if( !cycleDriveFolders.containsKey(cycleDrives.KARL_Audit_Cycle__c) ){
                cycleDriveFolders.put(cycleDrives.KARL_Audit_Cycle__c,new Map<String, String>());
            }

            cycleDriveFolders.get(cycleDrives.KARL_Audit_Cycle__c).put(cycleDrives.KARL_Drive_Scope__c,cycleDrives.KARL_Drive_URL__c);
        }

        // Create map of prior GUS Work-item Ids
        Map<Id, Map<String, String>> priorGusRecords = new Map<Id, Map<String, String>>();

        // Need to get the most recent CREQ that had a GUS Work Id
        for(Request_Item__c priorCri : [SELECT Id, Name, (SELECT Id, CreatedDate, Name, Request_GUS__c, Request_GUS__r.Name__c, Request_GUS__r.DisplayUrl 
                                                FROM Cycle_Request_Items__r WHERE Request_GUS__c != NULL AND Audit_Cycle__c NOT IN: cycleIds 
                                                ORDER BY CreatedDate DESC LIMIT 1)
                                                FROM Request_Item__c
                                                WHERE Create_GUS_Case__c = true
                                                WITH SECURITY_ENFORCED]){
        // AND (Id NOT IN: cycleReqItemIds OR Id NOT IN: dependentCycleItemsIdSet)
            if( !priorGusRecords.containsKey(priorCri.Id) ){
                priorGusRecords.put(priorCri.Id,new Map<String, String>());
            }
            if(priorCri.Cycle_Request_Items__r != NULL && priorCri.Cycle_Request_Items__r.size() > 0){
                for(Cycle_Request_Item__c criItem : priorCri.Cycle_Request_Items__r){
                    priorGusRecords.get(priorCri.Id).put('creqName', priorCri.Name);
                    priorGusRecords.get(priorCri.Id).put('creqGusId', criItem.Request_GUS__c);
                    priorGusRecords.get(priorCri.Id).put('creqGusName', criItem.Request_GUS__r.Name__c);
                    priorGusRecords.get(priorCri.Id).put('creqGusUrl', criItem.Request_GUS__r.DisplayUrl);
                    priorGusRecords.get(priorCri.Id).put('creqCreatedDate', criItem.CreatedDate.format());
                }
            }
        }
        System.debug(LoggingLevel.DEBUG, 'Previous GUS Records >> ' + priorGusRecords);

        List<Cycle_Request_Item__c> updtCycleReqItems = new List<Cycle_Request_Item__c>();
        //Step 1: Create GUS Work Item without EPIC aligned
        //Create External Object Work Data if Create_GUS_Case__c is checked
        System.debug(LoggingLevel.DEBUG, 'Query loop start dependentCycleItemsIdSet = '+dependentCycleItemsIdSet);
        //System.debug(LoggingLevel.DEBUG, 'Query Result = '+[Select id from Cycle_Request_Item__c WHERE Id IN:dependentCycleItemsIdSet]); updated by lakshya arora - Query in debug consume time to execute 

        for( Cycle_Request_Item__c  cri : [SELECT id,epic__c,Team__c,Request_Tech_Details__c,Cycle_Request_Status__c,Audit_Cycle__r.Google_Drive_Link__c,
                                            Audit_Cycle__r.Name,Request__r.EPIC_Type__c,Request__r.Metricstream_ID__c,Request__r.Request_Environments__c,
                                            Request__r.Primary_Scope__c,Request__r.Areas_of_Compliance__c,Request__r.KARL_Previous_GUS_ID__c,
                                            Request__r.Suggested_Product_Tag__c,Request__r.Suggested_Product_Tag__r.Name,Request__r.Suggested_Product_Tag__r.Active__c,
                                            Request__r.Suggested_Product_Tag__r.Product_Tag_External_Id__c ,Request__r.Create_GUS_Case__c,Request__r.Name,
                                            Request__r.Type__c,Request__r.Automatic_Product_Tag_Assignment__c,
                                            Request__r.Request_Name__c,Request__r.KARL_GRC_Assignee__r.FirstName,Request__r.KARL_GRC_Assignee__r.LastName,
                                            Request__r.KARL_Evidence_Submission_Workflow__c, Audit_Cycle__c,Date_for_Work_Record_Creation__c,
                                            Request_Priority__c,Request__r.Suggested_Product_Tag__r.Team__r.Active_c__c,
                                            Audit_Cycle_Coverage__r.Auto_Assignment_Configuration__r.Priority_0_Assignment_Date__c,
                                            Audit_Cycle_Coverage__r.Auto_Assignment_Configuration__r.Priority_1_Assignment_Date__c,
                                            Audit_Cycle_Coverage__r.Auto_Assignment_Configuration__r.Priority_2_Assignment_Date__c,
                                            Audit_Cycle_Coverage__r.Auto_Assignment_Configuration__r.Priority_3_Assignment_Date__c,
                                            Audit_Cycle_Coverage__r.Auto_Assignment_Configuration__r.Default_Product_Tag__c,
                                            Audit_Cycle_Coverage__r.Auto_Assignment_Configuration__r.Audit_Period__c,
                                            Audit_Cycle_Coverage__r.Auto_Assignment_Configuration__r.Days_to_Complete__c,
                                            Audit_Cycle_Coverage__r.Auto_Assignment_Configuration__r.Default_Product_Tag__r.Product_Tag_External_Id__c,
                                            Request__r.LastModifiedById ,Request__r.LastModifiedDate,Request__r.KARL_GRC_Assignee__r.Name
                                        FROM Cycle_Request_Item__c WHERE Id IN: cycleReqItemIds OR Id IN: dependentCycleItemsIdSet
                                        WITH SECURITY_ENFORCED]){
                                            System.debug(LoggingLevel.DEBUG, 'Request_Priority__c = '+cri.Request_Priority__c );
                                            System.debug(LoggingLevel.DEBUG, 'dependentCycleItemsIdSet = '+dependentCycleItemsIdSet );
                                            System.debug(LoggingLevel.DEBUG, 'cri.Id = '+cri.Id);
                                                if(dependentCycleItemsIdSet.contains(cri.Id)){
                                                    if(cri.Request_Priority__c == 'Wave 0'){
                                                        cri.Date_for_Work_Record_Creation__c = cri.Audit_Cycle_Coverage__r.Auto_Assignment_Configuration__r.Priority_0_Assignment_Date__c;
                                                    }
                                                    else if(cri.Request_Priority__c == 'Wave 1'){
                                                        cri.Date_for_Work_Record_Creation__c = cri.Audit_Cycle_Coverage__r.Auto_Assignment_Configuration__r.Priority_1_Assignment_Date__c;
                                                    }
                                                    else if(cri.Request_Priority__c == 'Wave 2'){
                                                        cri.Date_for_Work_Record_Creation__c = cri.Audit_Cycle_Coverage__r.Auto_Assignment_Configuration__r.Priority_2_Assignment_Date__c;
                                                    }
                                                    else if(cri.Request_Priority__c == 'Wave 3'){
                                                        cri.Date_for_Work_Record_Creation__c = cri.Audit_Cycle_Coverage__r.Auto_Assignment_Configuration__r.Priority_3_Assignment_Date__c;
                                                    }
                                                    System.debug(LoggingLevel.DEBUG, 'cri.Date_for_Work_Record_Creation__c = '+cri.Date_for_Work_Record_Creation__c);
                                                    cri.Is_Gus_Record_Created__c= false;
                                                    updtCycleReqItems.add(cri);
                                                }
                                                else if( cycleReqItemIds.contains(cri.Id) && cri.Request__r.Create_GUS_Case__c ){                
                                                    System.debug(LoggingLevel.DEBUG, 'Immediate gus records = '+cri.Id);
                                                    ADM_Work_c__x objWork = new ADM_Work_c__x();
                                                    //Feature ID to keep Salesforce Record ID so it can updated External in Salesforce Record.
                                                    objWork.Feature_ID_c__c = cri.id;
                                                    
                                                    //Subject is mapped by following fields
                                                    objWork.Subject_c__c    = cri.Audit_Cycle__r.Name+'/'+cri.Request__r.Name+'/'+cri.Request__r.Type__c+'/'+cri.Request__r.Request_Name__c;
                                    
                                                    // Build the case details with boiler plate text, using HTML for RTF
                                                    String gusCaseBuilder = gusTicketFormat(cri, cycleReqIdToEvidenceIdMap, cycleDriveFolders, communityHomeUrl, priorGusRecords);
                                                    String grcFullName = cri.Request__r.KARL_GRC_Assignee__r.FirstName + ' ' + cri.Request__r.KARL_GRC_Assignee__r.LastName;

                                                    objWork.Customer_c__c       = grcFullName + '-' + cri.Audit_Cycle__r.Name;
                                                    objWork.Details_c__c        = gusCaseBuilder;
                                                    objWork.Status_c__c         = 'New';
                                                    objWork.Team__c             = System.label.ARL_GUS_Work_Scrum_Team_Id;
                                                    objWork.RecordTypeId__c     = System.label.GUS_Work_User_Story_RecordType_Id;
                                                    if(cri.Audit_Cycle_Coverage__r.Auto_Assignment_Configuration__r.Days_to_Complete__c != null){
                                                        objWork.Due_Date_c__c = KARL_Utility.calculateWorkingDate(Integer.valueOf(cri.Audit_Cycle_Coverage__r.Auto_Assignment_Configuration__r.Days_to_Complete__c), System.today());
                                                    }
                                                    
                                                    // Logic for Product Tag assignment
                                                    if(cri.Request__r.Suggested_Product_Tag__c != null 
                                                        && cri.Request__r.Suggested_Product_Tag__r.Active__c 
                                                        && cri.Request__r.Suggested_Product_Tag__r.Product_Tag_External_Id__c != null 
                                                        && cri.Request__r.Suggested_Product_Tag__r.Team__r.Active_c__c
                                                        && cri.Request__r.Automatic_Product_Tag_Assignment__c){
                                                        // Assign to the specified Product Tag
                                                        System.debug(LoggingLevel.DEBUG, 'Assigning to specified product tag : ' + cri.Request__r.Suggested_Product_Tag__r.Product_Tag_External_Id__c);
                                                        objWork.Product_Tag_c__c = cri.Request__r.Suggested_Product_Tag__r.Product_Tag_External_Id__c;
                                                    }
                                                    else{
                                                        // Assign to the default Product Tag
                                                        System.debug(LoggingLevel.DEBUG, 'Assigning to default product tag : ' + cri.Audit_Cycle_Coverage__r.Auto_Assignment_Configuration__r.Default_Product_Tag__r.Product_Tag_External_Id__c);
                                                        objWork.Product_Tag_c__c = cri.Audit_Cycle_Coverage__r.Auto_Assignment_Configuration__r.Default_Product_Tag__r.Product_Tag_External_Id__c;
                                                    }
                                                   
                                    
                                                    //Updating correponding evidence record with the GUS details so that
                                                    //this field can be refrenced into evidence upload community
                                                    //Note: This piece of code can be removed after we have proper sharing in place for evidence upload community
                                                    //Added by Mahima Aggarwal 10/29/2020               
                                                    if(cycleReqIdToEvidenceIdMap.get(cri.Id) != null){
                                                        KARL_Evidence_Request__c evReq = new KARL_Evidence_Request__c();
                                                        evReq.Id = cycleReqIdToEvidenceIdMap.get(cri.Id);
                                                        evReq.KARL_GUS_Ticket_Details__c = gusCaseBuilder;
                                    
                                                        evidenceReqUpdateList.add(evReq);
                                                    }

                                                    // Create SLA tracking record
                                                    KARL_GUS_SLA_Tracking__c gusTrackingObj = new KARL_GUS_SLA_Tracking__c();
                                                    gusTrackingObj.Assignee__c =  cri.Request__r.KARL_GRC_Assignee__r.Name;
                                                    gusTrackingObj.Changed_By__c = cri.Request__r.LastModifiedById;
                                                    gusTrackingObj.Cycle_ARL_Item__c = cri.Id;
                                                    gusTrackingObj.Date_Changed__c = cri.Request__r.LastModifiedDate.date();
                                                    gusTrackingObj.KARL_End_Date__c = null;
                                                    gusTrackingObj.End_Status__c = '';
                                                    gusTrackingObj.Start_Date__c = cri.Request__r.LastModifiedDate;
                                                    gusTrackingObj.Start_Status__c = 'New';
                                                    
                                                    // Add records to Lists for processing to database
                                                    gusSlaTrackingList.add(gusTrackingObj);
                                                    WorkList.add( objWork );   
                                                    cRewItemIds.put(cri.Id, cri);
                                                    epics.add( cri.epic__c );
                                                }
                                            
        }

        //STEP 2: Search for existing EPIC from GUS
        Map<String, ADM_Epic_c__x> epicNameAndEpic = selectEpicsByName(epics);

                
        //If work record is found then insert in External Object
        if( !WorkList.isEmpty() ){
            //STEP 3: Insert GUS work reocrds
            List<Database.SaveResult> result = new List<Database.SaveResult>();
            if(!Test.isRunningTest()){
                result = Database.insertImmediate(WorkList);
            }
                        
            //Inserted GUS work record ids
            Set<Id> gusWorkRecordIds = new Set<Id>();
            for (Database.SaveResult gusWork : result) {
                if (gusWork.isSuccess()) {
                    System.debug(LoggingLevel.DEBUG,'immediate gus work record created = '+gusWork.getId());
                    gusWorkRecordIds.add(gusWork.getId());
                }
                else{
                    for(Database.Error err : gusWork.getErrors()) {
                        System.debug(LoggingLevel.DEBUG,'Following erros in gus work record creation');                    
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug(LoggingLevel.DEBUG,'Gus fields that affected this error: ' + err.getFields());
                    }
                }
            }
                        
            //1. Update Cycle Request Item Object with GUS Work Record's External ID
            //2. Reset Feature_ID_c__c field to blank.
            
            List<ADM_Work_c__x> newWorkRecords = [SELECT Id,ExternalId,Feature_ID_c__c,Status_c__c,DisplayUrl,Assignee_c__c FROM ADM_Work_c__x WHERE Id IN :gusWorkRecordIds AND Feature_ID_c__c!=null];
            
            if(Test.isRunningTest()){
                ADM_Work_c__x admWork = new ADM_Work_c__x();
                admWork.ExternalId = 'test';
                admWork.Status_c__c = 'In-Progress';
                admWork.Team__c = 'test';
                admWork.Feature_ID_c__c = cRewItemIds.values()[0].Id;
                newWorkRecords.add(admWork);
            }
                
            Set<ADM_Epic_c__x> newEpics = new Set<ADM_Epic_c__x>();

            //STEP 4: Assign GUS Work ID to Cycle Request Item and Prepare a list of new EPIC if EPIC was not found in STEP 2
            for(ADM_Work_c__x gusWork : newWorkRecords){
                if( cRewItemIds.containsKey(gusWork.Feature_ID_c__c) ){
                    
                    Cycle_Request_Item__c cri = new Cycle_Request_Item__c(Id=gusWork.Feature_ID_c__c);
                    cri.Request_GUS__c  = gusWork.ExternalId;
                    cri.Assigned_To__c = gusWork.Assignee_c__c;
                    cri.GUS_URL_Direct_Link__c = gusWork.DisplayUrl;
                    cri.GUS_Status__c = gusWork.Status_c__c;
                    cri.Is_Gus_Record_Created__c = true;
                    cri.Date_for_Work_Record_Creation__c = null;
                    updtCycleReqItems.add(cri);
                    
                    //Check id EPIC already exists in the system or not. If not then create new
                    if( !( epicNameAndEpic.containsKey(cRewItemIds.get(gusWork.Feature_ID_c__c).epic__c) ) ){
                         ADM_Epic_c__x admApic = new ADM_Epic_c__x(); 
                         admApic.Name__c = cRewItemIds.get(gusWork.Feature_ID_c__c).epic__c;
                         admApic.Team_c__c = System.label.ARL_GUS_Work_Scrum_Team_Id;
                         admApic.Category_c__c = 'Operations';
                         newEpics.add(admApic);
                    }
                }
            }
                        
            //STEP 5: Insert new EPICs and requery them
            if( !newEpics.isEmpty() && !Test.isRunningTest() ){
                Database.insertImmediate( new List<ADM_Epic_c__x>(newEpics) );
                epicNameAndEpic = selectEpicsByName(epics);
            } 
                        
            //STEP 6: Set EPIC Ids in GUS Work
            for(ADM_Work_c__x gusWork : newWorkRecords){
                if( epicNameAndEpic.containsKey(cRewItemIds.get(gusWork.Feature_ID_c__c).epic__c) ){
                    gusWork.Epic_c__c = epicNameAndEpic.get(cRewItemIds.get(gusWork.Feature_ID_c__c).epic__c).ExternalId;
                }
                
                //Blaking out the Feature_ID_c__c from external GUS record
                gusWork.Feature_ID_c__c = '';
            }
            
            //STEP 7: Update GUS Work records
            if( !newWorkRecords.isEmpty() && !Test.isRunningTest() ){Database.updateImmediate(newWorkRecords);}
            
            //STEP 8: Update Cycle Request Item
        }

        
        if(!gusSlaTrackingList.isEmpty()){
            insert gusSlaTrackingList;
            System.debug(LoggingLevel.DEBUG,'gusSlaTrackingList = '+gusSlaTrackingList);
        }
        
        //Added by Mahima Aggarwal 10/29/2020
        if( !evidenceReqUpdateList.isEmpty() && Schema.sObjectType.KARL_Evidence_Request__c.isUpdateable() 
               && KARL_Evidence_Request__c.KARL_GUS_Ticket_Details__c.getDescribe().isUpdateable()){
            update evidenceReqUpdateList;
        }

        System.debug(LoggingLevel.DEBUG,'updtCycleReqItems = '+updtCycleReqItems);
            if( !updtCycleReqItems.isEmpty() && Schema.sObjectType.Cycle_Request_Item__c.isUpdateable() 
               && Cycle_Request_Item__c.Request_GUS__c.getDescribe().isUpdateable()
               && Cycle_Request_Item__c.Assigned_To__c.getDescribe().isUpdateable()
               && Cycle_Request_Item__c.GUS_URL_Direct_Link__c.getDescribe().isUpdateable()
               && Cycle_Request_Item__c.GUS_Status__c.getDescribe().isUpdateable()){
                update updtCycleReqItems;
                System.debug(LoggingLevel.DEBUG,'Updated');
            }
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description This method builds the GUS Ticket Details format in HTML and returns it
     * @param Cycle_Request_Item__c - sObject from SOQL query
     * @return string in HTML
    */
    public static string gusTicketFormat(Cycle_Request_Item__c cri, Map<Id,Id> cycleReqIdToEvidenceIdMap, Map<Id, Map<String,String>> cycleDriveFolders, String communityHomeUrl, Map<Id, Map<String,String>> priorGusRecords){
        // Replace line breaks with a html break since GUS has a Rich Text Field, vs. our Long Text
        String gusWorkDetails = cri.Request_Tech_Details__c.replaceAll('\n','<br>');
        String evidenceText;
        Id evidenceReqId;
        String driveLink;

        // Build Evidence Submission Text String based on Upload Workflow usage
        if(!cri.Request__r.KARL_Evidence_Submission_Workflow__c){
            if(!cycleDriveFolders.isEmpty()){ 
                if(cycleDriveFolders.get(cri.Audit_Cycle__c).containsKey(cri.Request__r.Primary_Scope__c)){
                    driveLink = cycleDriveFolders.get(cri.Audit_Cycle__c).get(cri.Request__r.Primary_Scope__c);
                } else {
                    driveLink = cri.Audit_Cycle__r.Google_Drive_Link__c;
                }
            } else {
                driveLink = cri.Audit_Cycle__r.Google_Drive_Link__c;
            }
            
            System.debug(LoggingLevel.DEBUG, 'Drive Link >> ' + driveLink);

            evidenceText = '<p>- Please upload evidence for this ticket directly to the folder in Google Drive: ' +
                     (String.isBlank(driveLink) ? '{{INSERT LINK HERE}}' : '' + driveLink ) + 
                     '<br>';
        } else{
            evidenceReqId = cycleReqIdToEvidenceIdMap.get(cri.Id);
            String hyperlink = communityHomeUrl + 'evidence-upload?id=' + evidenceReqId;
            if(evidenceReqId == null){
                evidenceText = '<p>- Please upload evidence for this ticket to the GRC Evidence Request Portal. ' +
                     '<br>';
            }else{
                evidenceText = '<p>- Please upload evidence for this ticket to the <a href="'+ hyperlink + '">GRC Evidence Request Portal</a>. ' +
                     '<br>';
            }
        }

        // Build the Prior GUS String
        String gusText = '';
        if(!priorGusRecords.isEmpty()){
            if(priorGusRecords.containsKey(cri.Request__c)){
                gusText = '<b>Previous GUS Work Item:</b> <a href="' + priorGusRecords.get(cri.Request__c).get('creqGusUrl') + '">' + priorGusRecords.get(cri.Request__c).get('creqGusName') + '</a><br>';
            }else{
                gusText = (String.isBlank(cri.Request__r.KARL_Previous_GUS_ID__c) ? '' : '<b>Previous GUS Work Item:</b> ' +cri.Request__r.KARL_Previous_GUS_ID__c + '<br>');  
            }
        }else{
            gusText = (String.isBlank(cri.Request__r.KARL_Previous_GUS_ID__c) ? '' : '<b>Previous GUS Work Item:</b> ' +cri.Request__r.KARL_Previous_GUS_ID__c + '<br>');
        }

        // Build Audit Period Text
        String auditPeriod = '';
        if(String.isBlank(cri.Audit_Cycle_Coverage__r.Auto_Assignment_Configuration__r.Audit_Period__c)){
            auditPeriod = 'For Spring Audits, the population should include November 01 through April 30. For Fall Audits, the population should include May 01 through October 31. Some audits may fall outside of this period, refer to the details above for any specific details.';
        }else{
            auditPeriod = cri.Audit_Cycle_Coverage__r.Auto_Assignment_Configuration__r.Audit_Period__c;
        }

        // Return formatted GUS Details
        return  '<p><b>Request Type:</b> ' +cri.Request__r.Type__c+ '</p><br>'+
                '<p>' +gusWorkDetails+ '</p><br>' + 
                gusText +
                //(String.isBlank(cri.Request__r.Metricstream_ID__c) ? '' : '<b>Old Request ID:</b> ' +cri.Request__r.Metricstream_ID__c + '<br>')+
                (String.isBlank(cri.Request__r.Areas_of_Compliance__c) ? '' : '<b>Applicable Frameworks:</b> ' +cri.Request__r.Areas_of_Compliance__c + '<br>')+
                (String.isBlank(cri.Request__r.Request_Environments__c) ? '' : '<b>Applicable Environments:</b> ' +cri.Request__r.Request_Environments__c + '<br>')+
                (String.isBlank(cri.Request__r.Suggested_Product_Tag__c) ? '' : '<b>Product Tag:</b> ' +cri.Request__r.Suggested_Product_Tag__r.Name + '<br>')+
                (String.isBlank(cri.Request__r.Primary_Scope__c) ? '' : '<b>Primary Scope (GRC Use):</b> ' +cri.Request__r.Primary_Scope__c )+ '<br><br>'+
                '<p>----------------------------------------</p><p><b>Audit Period:</b></p>'+
                '<p>' + auditPeriod + '</p>'+
                '<p>----------------------------------------</p><p><b>Directions for providing evidence:</b></p>'+
                evidenceText +
                '- Ensure that filenames of any uploaded files include the associated GUS Work ID, the REQ number from the description, and a brief description/title for the file being uploaded. Example: W-1234567 / REQ-0000001 / Tier 1 Vendor Population.</p>'+
                '<p>----------------------------------------</p><p><b>Acceptance Criteria (Completeness & Accuracy):</b></p>'+
                '<p>1) ALL screenshots and system output must include a timestamp (date + time)<br>'+
                '2) If output is generated (e.g. via a script or report):<br>'+
                '- <b>Scripts</b>: please provide the script (if possible) (a link to Git is sufficient)<br>'+
                '- <b>Reports</b>: please provide parameters/query for generating the report (screenshot is sufficient)<br>'+
                '3) If a sample is requested, please ensure that all members of the sample are included in evidence.<br>'+
                'Refer to https://salesforce.quip.com/odfFANN59XJg for additional guidance.</p>'+
                '<p>----------------------------------------</p><p><b>Directions for the status of this GUS ticket:</b></p>'+
                '<p>1) Please move ticket to "Triaged" when work has been acknowledged and accepted.<br>'+
                '2) Please move ticket to "Ready for Review" when work has been completed and evidence has been uploaded to Google Drive (link above).<br>'+
                '3) The Security GRC Compliance team will move tickets to "Closed" after reviewing evidence.</p><br>';
    }

    /**
     * @author Vishnu Kumar
     * @email vishnu.kumar@mtxb2b.com
     * @description This method query epic based on the list of names.
    */ 
    public static Map<String, ADM_Epic_c__x> selectEpicsByName(Set<String> epics){
        Map<String, ADM_Epic_c__x> epicNameAndEpic = new Map<String, ADM_Epic_c__x>();
        for( ADM_Epic_c__x epic : [SELECT id,Name__c,ExternalId FROM ADM_Epic_c__x WHERE Name__c in: epics] ){
            epicNameAndEpic.put(epic.Name__c, epic);
        }
        return epicNameAndEpic;
    }

}
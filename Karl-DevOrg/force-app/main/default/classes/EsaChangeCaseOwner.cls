public class EsaChangeCaseOwner{

  public case__x caseX {get; set;}
  public String caseOwner {get; set;}
  public String buttonMessage {get; set;}

  public EsaChangeCaseOwner(ApexPages.StandardController stdController) {
    caseX = (Case__x)stdController.getRecord();
    caseOwner = null;
    caseX = [SELECT CaseNumber__c, Case_Owner_c__c, OwnerId__c,ExternalId FROM Case__x WHERE Id=:caseX.Id];
  }

   public List<SelectOption> getCaseOwners() {
     List<SelectOption> lstCaseOwners = new List<SelectOption>();
      Esa_Supportforce_Case_Owners__mdt[] caseOwners = [SELECT MasterLabel,
                                                               Supportforce_User_Id__c
                                                        FROM Esa_Supportforce_Case_Owners__mdt];
    lstCaseOwners.add(new SelectOption('',''));
    for(Esa_Supportforce_Case_Owners__mdt caseOwner : caseOwners) {
      lstCaseOwners.add(new SelectOption(caseOwner.Supportforce_User_Id__c,caseOwner.MasterLabel));
    }

    return lstCaseOwners;
   }

   public void changeOwner(){
    caseX.OwnerId__c = caseOwner;
   }

   public PageReference updateCaseOwner(){
      PageReference p = new PageReference('/lightning/r/Case__x/'+caseX.Id+'/view');
      ApexPages.Message pageMessage;
      if(caseOwner == null) {
         pageMessage = new ApexPages.Message(ApexPages.Severity.FATAL, 'Please select Owner info');
         ApexPages.addMessage(pageMessage);
         return null;
      }
      Database.DMLOptions options = new Database.DMLOptions();
      options.assignmentRuleHeader.useDefaultRule = false;
      caseX.setOptions(options);
      Database.SaveResult sr = Database.updateImmediate(caseX);
      if (!sr.isSuccess()) {
          List<Database.Error> err = sr.getErrors();
          pageMessage = new ApexPages.Message(ApexPages.Severity.FATAL, err[0].getMessage());
          ApexPages.addMessage(pageMessage);
          return null;
       }
      return p;
   }

   public PageReference goBackCaseView(){
     PageReference p = new PageReference('/lightning/r/Case__x/'+caseX.Id+'/view');
     return p;
   }


}
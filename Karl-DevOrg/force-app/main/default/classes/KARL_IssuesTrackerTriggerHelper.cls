/**
* @author Ben Harvie
* @email ben.harvie@salesforce.com
* @description This is trigger handler class for the KARL Issue Tracker
*/
public with sharing class KARL_IssuesTrackerTriggerHelper {
    public static Set<Id> recordIdsApprovers = new Set<Id>();
    public static Set<Id> recordIdsVersions = new Set<Id>();
    
    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description This method gets triggered from Issue Tracker Trigger
    */
    public static void run(){
        // After Update
        if( Trigger.isAfter && Trigger.isUpdate ){
            captureIssueVersion(); // Needs to come first for oldMap
            updateApprovers();
        }
        // After Insert
        else if( Trigger.isAfter && Trigger.isInsert ){
            captureIssueVersion();
            updateApprovers();
        }
    }
    
    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description This method updates the Approvers based on the Approval Matrix
    */
    private static void updateApprovers(){
        // Recursion Manager
        System.debug(LoggingLevel.DEBUG, 'Updating Approvers >>');

        for(Id recursionCheck : Trigger.newMap.keySet()){
            if(recordIdsApprovers.contains(recursionCheck)){
                System.debug(LoggingLevel.DEBUG, 'Already processed this record >> ' + recursionCheck);
                return; // Kill the run - we've already processed this Id
            }else{
                System.debug(LoggingLevel.DEBUG, 'New record to process >> ' + recursionCheck);
                recordIdsApprovers.add(recursionCheck); // add Id to set so we don't run it again
            }
        }
        
        // Variable Declarations
        List<KARL_Issue_Tracker__c> issueTracker = new List<KARL_Issue_Tracker__c>();

        // Build map of the approval matrix
        Map<String, KARL_Issue_Tracker_Approvers__c> responseApprovers = new Map<String, KARL_Issue_Tracker_Approvers__c>();
        for ( KARL_Issue_Tracker_Approvers__c  approverItems : [SELECT Id,KARL_Scope__c,KARL_Approver1__c,KARL_Approver2__c,KARL_Approver3__c 
                                                                    FROM KARL_Issue_Tracker_Approvers__c] ){
            responseApprovers.put(approverItems.KARL_Scope__c, approverItems);
        }

        for( KARL_Issue_Tracker__c  itItems : [SELECT Id,KARL_KAI_Impacted_Scope__c, KARL_KAI_Approver_GRCLT__c,
                                                     KARL_KAI_Approver_SCCS__c, KARL_KAI_Approver_VP_Approver__c
                                                    FROM KARL_Issue_Tracker__c 
                                                    WHERE Id in: (List<KARL_Issue_Tracker__c>)Trigger.New ]){

            // Set the Approvers based on Scope selection, only if changed
            if( Trigger.isInsert ||
              ( Trigger.isUpdate && responseApprovers.containsKey(itItems.KARL_KAI_Impacted_Scope__c))){
                  if(itItems.KARL_KAI_Approver_GRCLT__c <> responseApprovers.get(itItems.KARL_KAI_Impacted_Scope__c).KARL_Approver1__c ||
                    itItems.KARL_KAI_Approver_SCCS__c <> responseApprovers.get(itItems.KARL_KAI_Impacted_Scope__c).KARL_Approver2__c ||
                    itItems.KARL_KAI_Approver_VP_Approver__c <> responseApprovers.get(itItems.KARL_KAI_Impacted_Scope__c).KARL_Approver3__c){
                        KARL_Issue_Tracker__c itItem = new KARL_Issue_Tracker__c();
                        itItem.Id = itItems.Id;
                        itItem.KARL_KAI_Approver_GRCLT__c = responseApprovers.get(itItems.KARL_KAI_Impacted_Scope__c).KARL_Approver1__c;
                        itItem.KARL_KAI_Approver_SCCS__c = responseApprovers.get(itItems.KARL_KAI_Impacted_Scope__c).KARL_Approver2__c;
                        itItem.KARL_KAI_Approver_VP_Approver__c = responseApprovers.get(itItems.KARL_KAI_Impacted_Scope__c).KARL_Approver3__c;
                        issueTracker.add(itItem);
                    }
            }
        }
        
        // Make the update accounting for sharing and permission sets
		if( !issueTracker.isEmpty() && Schema.sObjectType.KARL_Issue_Tracker__c.isUpdateable() 
          	&& KARL_Issue_Tracker__c.KARL_KAI_Approver_GRCLT__c.getDescribe().isUpdateable()
          	&& KARL_Issue_Tracker__c.KARL_KAI_Approver_SCCS__c.getDescribe().isUpdateable()
          	&& KARL_Issue_Tracker__c.KARL_KAI_Approver_VP_Approver__c.getDescribe().isUpdateable() ){
            update issueTracker;
        }
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description This method creates a Issue version when the Issue Description / Wording / Response fields change
    */
    private static void captureIssueVersion(){ 
        // Recursion Manager
        System.debug(LoggingLevel.DEBUG, 'Updating Versions >>');

        for(Id recursionCheck : Trigger.newMap.keySet()){
            if(recordIdsVersions.contains(recursionCheck)){
                System.debug(LoggingLevel.DEBUG, 'Already processed this record >> ' + recursionCheck);
                return; // Kill the run - we've already processed this Id
            }else{
                System.debug(LoggingLevel.DEBUG, 'New record to process >> ' + recursionCheck);
                recordIdsVersions.add(recursionCheck); // add Id to set so we don't run it again
            }
        }

        // Variable Declarations
        List<KARL_Issue_Tracker__c> issues = new List<KARL_Issue_Tracker__c>();
        List<KARL_Issue_Tracker_Version__c> issueVersions = new List<KARL_Issue_Tracker_Version__c>();
        Map<Id, KARL_Issue_Tracker__c> oldMap = (Map<Id, KARL_Issue_Tracker__c>)Trigger.oldMap;
       
        // Creating Control Vesrions
        for(KARL_Issue_Tracker__c issueItem : [SELECT Id,KARL_KAI_Issue_Description__c,KARL_Deviation_Wording__c,
                                                KARL_Management_Response__c,KARL_KAI_Additional_Information__c,KARL_Version__c
                                                FROM KARL_Issue_Tracker__c WHERE Id IN:(List<KARL_Issue_Tracker__c>)Trigger.New] ){
            KARL_Issue_Tracker__c oldIssue = Trigger.isUpdate ? oldMap.get(issueItem.Id) : null;
			
            //Check
            //1. If is for insert then create version record
            //2. If is for update then check certain fields and if value changed then create version record
            if( Trigger.isInsert ||
              ( Trigger.isUpdate && (
                    issueItem.KARL_KAI_Issue_Description__c <> oldIssue.KARL_KAI_Issue_Description__c ||
                    issueItem.KARL_Deviation_Wording__c <> oldIssue.KARL_Deviation_Wording__c ||
                    issueItem.KARL_Management_Response__c <> oldIssue.KARL_Management_Response__c ||
                    issueItem.KARL_KAI_Additional_Information__c <> oldIssue.KARL_KAI_Additional_Information__c) )
              ){

                Decimal oldVersion;
                if(issueItem.KARL_Version__c == null){
                    oldVersion = 1;
                }else{
                    oldVersion = issueItem.KARL_Version__c;
                }
            
                Decimal version = Trigger.isUpdate && issueItem.KARL_Version__c != null ? (oldVersion + 1) : 1;
                                    
                issueVersions.add(new KARL_Issue_Tracker_Version__c(
                    KARL_H_Audit_Issue_Tracker__c = issueItem.Id,
                    KARL_H_Issue_Description__c = issueItem.KARL_KAI_Issue_Description__c,
                    KARL_H_Auditor_Issue_Wording__c = issueItem.KARL_Deviation_Wording__c,
                    KARL_H_Management_Response__c = issueItem.KARL_Management_Response__c,
                    KARL_H_Additional_Information__c = issueItem.KARL_KAI_Additional_Information__c,
                    KARL_H_Version__c = version
                ));
                
                //Update control item with new version
                KARL_Issue_Tracker__c issueUpdateItem = new KARL_Issue_Tracker__c();
                issueUpdateItem.Id = issueItem.Id;
                issueUpdateItem.KARL_Version__c = version;
                issues.add(issueUpdateItem);
            }
        }

		if( !issueVersions.isEmpty() && Schema.sObjectType.KARL_Issue_Tracker_Version__c.isCreateable() 
          	&& KARL_Issue_Tracker_Version__c.KARL_H_Audit_Issue_Tracker__c.getDescribe().isCreateable() 
          	&& KARL_Issue_Tracker_Version__c.KARL_H_Auditor_Issue_Wording__c.getDescribe().isCreateable() 
          	&& KARL_Issue_Tracker_Version__c.KARL_H_Management_Response__c.getDescribe().isCreateable()
          	&& KARL_Issue_Tracker_Version__c.KARL_H_Additional_Information__c.getDescribe().isCreateable()
          	&& KARL_Issue_Tracker_Version__c.KARL_H_Version__c.getDescribe().isCreateable() ){
            insert issueVersions; 
        }
        
        if( !issues.isEmpty() && Schema.sObjectType.KARL_Issue_Tracker__c.isUpdateable() 
           && KARL_Issue_Tracker__c.KARL_Version__c.getDescribe().isUpdateable()){ 
            update issues; 
        }

    }
}
public class CSIRT_suppressionCheck 
{
 
    /*author: bmalcolmson@salesforce.com
     * December 2015
     * This class works with a process designed to 
     * automate handler actions on alerts that require
     * user confirmation.
     * The purpose of this class is to process suppression rules
     * If a user wants to prevent being alerted to certain 
     * activity because it is approved for a period of time, 
     * create a VIH_supperession_rule__c object and then alerts
     * will not be sent to the Q for specified criteria.
     */
    
            private static Map<String, String> caseFields = new Map<String, String>{
            'alert_Name__c' =>'alert_name__c',
            'alert_destination_user__c' => 'username__c',
            'alert_source_IP__c' => 'source_ip__c',
            'Alert_common_name__c' => 'common_name__c',
            'destination_hostname__c' => 'destination_domain__c',
            'Destination_IP_Address__c' => 'destination_ip__c',
            'subject' => 'subject__c',
            'Alert_Signature_Name__c' => 'Alert_Signature_Name__c',
            'sensor_name__c' => 'sensor_name__c'};
@InvocableMethod          
	public static void matchingSupRules(List<Case> ProcessedCases)
    {
        List<VIH_suppression_rule__c> ruleActive;
        
        //Lets get case details to prevent the Record is Read-only error
        List<Id> caseIds = new List<Id>();
    	for(Case c : ProcessedCases)
        {
			caseIds.add(c.Id);
        }
        List<case> upcase = new List<case>();
        List<Case> casedeets = new List<Case>();

        casedeets = [SELECT Id, alert_Name__c, alert_resource__c, 
                     alert_destination_user__c, Description, parentid,
                     alert_source_IP__c, Alert_common_name__c,
                     destination_hostname__c, Destination_IP_Address__c,
                     ContactId, status, subject, Alert_Signature_Name__c, sensor_name__c 
                     from Case where Id IN : caseIds];
        
        
        //Case details gathered!
        
        //Select current active rules
        ruleActive = [SELECT id, source_ip__c, common_name__c, destination_domain__c,
                            destination_ip__c, alert_name__c,
                            Duration_in_days__c, username__c, parent_case__c, alert_signature_name__c,
                      		sensor_name__c, subject__c
                            from VIH_suppression_rule__c 
                      		WHERE expiration_date__c >= TODAY];
        
        if(ruleActive.size() > 0)
        {
        //Match fields from rules to the current case to see if it meets the suppression rule criteria
        for (case processedCase : caseDeets)
            {
                
                VIH_suppression_rule__c matchedRule = findMatchingRule(processedCase, ruleActive);
                system.debug('O**** matchd rule = ' + matchedrule);

                if (matchedRule != null) 
                {
                    system.debug('OMFG ALL FIELDS MATCH');
                    
                    processedCase.status = 'Closed - Suppressed';
                    if(matchedRule.parent_case__c != processedcase.id)
                    {
                        	processedCase.parentid = matchedRule.Parent_Case__c;
                    }
                    upcase.add(processedCase);
                }
                system.debug('After if statement');
            }
            
        }
        if(upcase.size()>0){
            update upcase;
            system.debug('The case should now be closed - suppressed');
        }
    }
    private static VIH_suppression_rule__c findMatchingRule(Case processedCase, List<VIH_suppression_rule__c> ruleActive) {                                                            
        //Match first rule
        for (VIH_suppression_rule__c rule: ruleActive) {
            Integer matchedRules = 0;
            
            for (string field : caseFields.KeySet()) {
                String fieldValue;
                String ruleValue;
                
                if (processedCase.get(field) != null) { //want the key not the value
                    fieldValue = (String)processedCase.get(field);
                    fieldValue = fieldValue.deleteWhitespace();
                }
                                 
                if (rule.get(caseFields.get(field)) != null ) { //probably want the value not the field set
                    ruleValue = (String)rule.get(caseFields.get(field));
                	ruleValue = ruleValue.deleteWhitespace();
                }
                system.debug('fieldValue: '+fieldValue+' ruleValue: '+ruleValue);
                if (ruleValue == null ) {
                    matchedRules++;
                }
                else if (ruleValue != null && fieldvalue != null){
                    if(fieldValue.equals(ruleValue))
                        matchedRules++;
                }
                    
            }
            
            if (matchedRules == caseFields.KeySet().size()){
            	return rule;
        	}
        }
        return null;
    }
}
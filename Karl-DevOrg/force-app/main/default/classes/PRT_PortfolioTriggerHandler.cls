/***********************************************************
* Handler for PortfolioTrigger
* Created by 	- Prashant Gupta
* Date - 01-12-2019
************************************************************/
public class PRT_PortfolioTriggerHandler {
	
    /***********************************************************
    * Created by 	- Prashant Gupta
    * Date 		- 29th Nov 2019
    * Before Update method Being Called from Trigger
    ************************************************************/
    public static void beforeUpdate(List<Portfolio__c> newList, Map<id,Portfolio__c> newMap){
    }
    
    /***********************************************************
    * Created by 	- Prashant Gupta
    * Date 		- 29th Nov 2019
    * Before Insert method Being Called from Trigger
    ************************************************************/
    public static void beforeInsert(List<Portfolio__c> newList){      
    }
    
    /***********************************************************
    * Created by 	- Prashant Gupta
    * Date 		- 29th Nov 2019
    * After Update method Being Called from Trigger
    ************************************************************/
    public static void afterUpdate(List<Portfolio__c> newList, Map<id,Portfolio__c> oldMap){ 
      system.debug('>>>>>>>>>>>>>>>>>>' + !System.isBatch()+ !System.isFuture() + PRT_Constants.INTEGRATION_SETTING.GUS_Portfolio_Integration__c);
        if(!System.isBatch() && !System.isFuture() && PRT_Constants.INTEGRATION_SETTING.GUS_Portfolio_Integration__c){
            system.debug('>>>> integration value' + PRT_Constants.INTEGRATION_SETTING.GUS_Portfolio_Integration__c);
            PRT_PortfolioTriggerHelper.updateInsertGUSRecords(newList,oldMap);
        }
    }
    
    /***********************************************************
    * Created by 	- Prashant Gupta
    * Date 		- 29th Nov 2019
    * After Insert method Being Called from Trigger
    ************************************************************/
    public static void afterInsert(List<Portfolio__c> newList){
        if(!System.isBatch() && !System.isFuture() && PRT_Constants.INTEGRATION_SETTING.GUS_Portfolio_Integration__c){
            PRT_PortfolioTriggerHelper.updateInsertGUSRecords(newList,null);
        }
    }
}
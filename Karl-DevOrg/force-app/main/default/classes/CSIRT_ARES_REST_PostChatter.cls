@RestResource(urlMapping='/ARES/Org62/PostChatter/*')
global with sharing class CSIRT_ARES_REST_PostChatter {
	@HttpPost 
	global static void GetAccountContacts(Id ObjectId, String Message) {
        
        CSIRT_ARES_InternalOrgIntegration Org62 = new CSIRT_ARES_InternalOrgIntegration('ORG62');
        Org62.PostToChatter(ObjectId, Message);
        
        
   }
}
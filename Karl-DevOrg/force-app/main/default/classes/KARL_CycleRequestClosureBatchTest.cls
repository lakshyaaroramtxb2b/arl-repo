/**
 * @author Ben Harvie
 * @email ben.harvie@salesforce.com
 * @description Test class for Cycle Request Closure Batch (closing when ARI completed)
*/
@isTest
public with sharing class KARL_CycleRequestClosureBatchTest {
    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Test data setup.
    */
    @testSetup
    private static void testSetup(){
        /**
         * User to generate the test records
         */
        UserRole userrole = [SELECT Id, DeveloperName FROM UserRole WHERE DeveloperName = 'CEO' LIMIT 1];

        User adminUser = [SELECT Id, UserRoleId FROM User 
                            WHERE Profile.Name='System Administrator' AND isActive = true LIMIT 1];
        
        adminUser.UserRoleId = userRole.Id;
        update adminUser;

        System.runAs(adminUser){
            /**
             * Audit Firm and Team Setup
             */
            KARL_Audit_Firm__c auditFirm = ARL_TestDataFactory.createAuditFirm('testAuditFirm');
            insert auditFirm;
            
            // Create two distinct audit teams
            KARL_Audit_Team__c auditTeam1 = ARL_TestDataFactory.createAuditTeam('testAuditTeam1');
            auditTeam1.KARL_Audit_Firm__c = auditFirm.Id;
            insert auditTeam1;
            
            KARL_Audit_Team__c auditTeam2 = ARL_TestDataFactory.createAuditTeam('testAuditTeam2');
            auditTeam2.KARL_Audit_Firm__c = auditFirm.Id;
            insert auditTeam2;

            /**
             * Controls Setup
             */
            // Create empty control shell
            Control__c control = ARL_TestDataFactory.createControl();
            insert control;

            // Map to the control shell
            Control_Scope__c controlScope = ARL_TestDataFactory.createControlScope();
            controlScope.Control_Name__c = control.id; // Map to the created control
            controlScope.Scope_Name__c  = 'HK';
            insert controlScope;
            
            // Create control tests
            Control_Test__c controlTest1 = ARL_TestDataFactory.createControlTest(controlScope.id);
            controlTest1.Test_Name__c = 'test control test 1';
            insert controlTest1;
            
            Control_Test__c controlTest2 = ARL_TestDataFactory.createControlTest(controlScope.id);
            controlTest2.Test_Name__c = 'test control test 2';
            insert controlTest2;
            
            /**
             * Requests Master Data Setup
             */
            Request_Item__c reqItem1 = ARL_TestDataFactory.createRequestItem();
            reqItem1.Request_Name__c = 'Request 1';
            reqItem1.Primary_Scope__c = 'HK';
            reqItem1.Areas_of_Compliance__c  = 'SOC 2';
            reqItem1.Create_GUS_Case__c = true;
            insert reqItem1;

            Request_Item__c reqItem2 = ARL_TestDataFactory.createRequestItem();
            reqItem2.Request_Name__c = 'Request ';
            reqItem2.Primary_Scope__c = 'HK';
            reqItem2.Areas_of_Compliance__c  = 'HITRUST';
            reqItem2.Create_GUS_Case__c = true;
            insert reqItem2;
            
            // Map the request items to control tests
            Request_Item_Control__c requestControlMapping1 = ARL_TestDataFactory.createRequestItemControl(reqItem1.id,controlTest1.Id);
            insert requestControlMapping1;
            
            Request_Item_Control__c requestControlMapping2 = ARL_TestDataFactory.createRequestItemControl(reqItem2.id,controlTest2.Id);
            insert requestControlMapping2;

            /**
             * Audit Cycle Setup
             */
            // Create shell audit cycle
            Audit_Cycle__c auditCycle = ARL_TestDataFactory.createAuditCycle();
            insert auditCycle;
            
            // Configure audit cycle coverage
            Audit_Cycle_Coverage__c acc1 = ARL_TestDataFactory.createAuditCycleCoverage(auditCycle.Id);
            acc1.Area__c = 'HITRUST';
            acc1.Scope__c  = 'HK';
            acc1.KARL_Audit_Team__c = auditTeam1.id; // Map this to audit team 1
            insert acc1;
            
            Audit_Cycle_Coverage__c acc2 = ARL_TestDataFactory.createAuditCycleCoverage(auditCycle.Id);
            acc2.Area__c = 'FedRAMP Moderate';
            acc2.Scope__c  = 'HK';
            acc2.KARL_Audit_Team__c = auditTeam1.id; // Map this to audit team 1
            insert acc2;
            
            Audit_Cycle_Coverage__c acc3 = ARL_TestDataFactory.createAuditCycleCoverage(auditCycle.Id);
            acc3.Area__c = 'SOC 2';
            acc3.Scope__c  = 'HK';
            acc3.KARL_Audit_Team__c = auditTeam2.id; // Map this to audit team 2
            insert acc3;

            // Generate the Cycle and ARI Requests based on the test data setup above
            ARL_CreateCycleReqItemController.createCycleRequestItems(auditCycle.Id);
        }
    }
    
    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description In this test, we're generating an audit cycle and running the closure batch
    */
	@isTest
    public static void checkBatch(){ 
        User u = ARL_TestDataFactory.createOpsAdmin();
       
        // Run the test as the SCEA Ops Admin
        System.runAs(u) {
            Test.startTest();
                Cycle_Request_Item__c[] closedCreqs = [SELECT id,Cycle_Request_Status__c FROM Cycle_Request_Item__c WHERE Cycle_Request_Status__c = 'Closed'];

                KARL_CycleRequestClosureBatch targetBatch = new KARL_CycleRequestClosureBatch();
                DataBase.executeBatch(targetBatch,200);

                system.assertEquals(0, closedCreqs.size(), 'Incorrect number of closed CREQs');
            Test.stopTest();
        }
    }
}
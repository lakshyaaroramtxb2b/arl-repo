global with sharing class HW_UsersScheduler extends HW_RecordsStore{
    public HW_UsersScheduler() {}
    public static void nonCompliantUsers(){
        HW_RecordsStore rs = new HW_RecordsStore();
        HW_RecordsStore.UserTypes ut = rs.usersCollection();
        Set<String> users = ut.users;
        Map<String, String> usernameMap = ut.usernameMap;
        Map<String, Set<String>> adminUserOrgs = ut.adminUserOrgs;
        Map<String, Set<String>> managerMap = ut.managerMap;
        Map<String, Integer> recsCount = ut.recsCount;

        Map<String, List<HW_RecordsStore.RecordsWithReducedAttributes>> userMap = new  Map<String, List<HW_RecordsStore.RecordsWithReducedAttributes>>();
        
         //query records for no 2FA and SSO Enabled
         Map<String, List<HW_RecordsStore.RecordsWithReducedAttributes>> noSSO = new Map<String, List<HW_RecordsStore.RecordsWithReducedAttributes>>();
         for(Heroku_Warden_users__c rec: [SELECT user_name__c, LastModifiedDate, email__c, role__c, organizationId__c, Employee__r.INTEG_First_Name__c, Employee__r.INTEG_Last_Name__c, Employee__r.INTEG_Manager_Email__c, Employee__r.INTEG_Manager_Name__c, Heroku_Warden_organizations__r.name__c FROM Heroku_Warden_users__c WHERE Employee__c != null AND Employee__r.INTEG_Active__c = true AND federated__c = false AND two_factor_authentication__c = false AND LastModifiedDate >= LAST_N_DAYS:1]){
             HW_RecordsStore.RecordsWithReducedAttributes ra = new HW_RecordsStore.RecordsWithReducedAttributes();
             ra.UserName = rec.user_name__c;
             ra.Email = rec.email__c;
             ra.Role = rec.role__c;
             ra.OrgID = rec.organizationId__c;
             ra.OrgName = rec.Heroku_Warden_organizations__r.name__c;
             ra.FirstName = rec.Employee__r.INTEG_First_Name__c;
             ra.LastName = rec.Employee__r.INTEG_Last_Name__c;
             ra.ManagerEmail = rec.Employee__r.INTEG_Manager_Email__c;
             ra.ManagerName = rec.Employee__r.INTEG_Manager_Name__c;
             ra.Query = 'get_no2fasso_enabled_users';
             ra.AlertType = 'Enable SSO';
             //ra.RecCount = recordsCount;

             //add record to send alert
             if(noSSO.containsKey(ra.Email)){
                 List<HW_RecordsStore.RecordsWithReducedAttributes> tmp = new List<HW_RecordsStore.RecordsWithReducedAttributes>(noSSO.get(ra.Email));
                 tmp.add(ra);
                 noSSO.put(ra.Email, tmp);
             }
             else {
                 noSSO.put(ra.Email, new List<HW_RecordsStore.RecordsWithReducedAttributes>{ra});
             }

             //verify if manager has a Heroku account if not add records directly to the userMap to send alerts about direct reportee's
             /*if(users.contains(ra.ManagerEmail)){
                 noSSO.put(ra.Email, new List<HW_RecordsStore.RecordsWithReducedAttributes>{ra});
             }*/
             if(!users.contains(ra.ManagerEmail) && userMap.containsKey(ra.ManagerEmail)){
                 List<HW_RecordsStore.RecordsWithReducedAttributes> tmp = new List<HW_RecordsStore.RecordsWithReducedAttributes>(userMap.get(ra.ManagerEmail));
                 tmp.add(ra);
                 userMap.put(ra.ManagerEmail, tmp);
                 usernameMap.put(ra.ManagerEmail, ra.ManagerName);
             }
             else {
                 userMap.put(ra.ManagerEmail, new List<HW_RecordsStore.RecordsWithReducedAttributes>{ra});
                 usernameMap.put(ra.ManagerEmail, ra.ManagerName);
             }
         }

         //query inactive user records
         Map<String, List<HW_RecordsStore.RecordsWithReducedAttributes>> inactiveUsers = new Map<String, List<HW_RecordsStore.RecordsWithReducedAttributes>>();
         for(Heroku_Warden_users__c rec : [SELECT user_name__c, LastModifiedDate, email__c, role__c, organizationId__c, Employee__r.INTEG_Manager_Email__c, Employee__r.INTEG_Manager_Name__c, Employee__r.INTEG_First_Name__c, Employee__r.INTEG_Last_Name__c, Heroku_Warden_organizations__r.name__c FROM Heroku_Warden_users__c WHERE Employee__c != null AND Employee__r.INTEG_Active__c = false AND LastModifiedDate >= LAST_N_DAYS:1]){
             HW_RecordsStore.RecordsWithReducedAttributes ra = new HW_RecordsStore.RecordsWithReducedAttributes();
             ra.UserName = rec.user_name__c;
             ra.Email = rec.email__c;
             ra.Role = rec.role__c;
             ra.OrgID = rec.organizationId__c;
             ra.OrgName = rec.Heroku_Warden_organizations__r.name__c;
             ra.FirstName = rec.Employee__r.INTEG_First_Name__c;
             ra.LastName = rec.Employee__r.INTEG_Last_Name__c;
             ra.ManagerEmail = rec.Employee__r.INTEG_Manager_Email__c;
             ra.ManagerName = rec.Employee__r.INTEG_Manager_Name__c;
             ra.Query = 'get_inactive_users';
             ra.AlertType = 'Remove Inactive User';
             //ra.RecCount = recordsCount;
             if(inactiveUsers.containsKey(ra.OrgID)){
                 List<HW_RecordsStore.RecordsWithReducedAttributes> tmp = new List<HW_RecordsStore.RecordsWithReducedAttributes>(inactiveUsers.get(ra.OrgID));
                 tmp.add(ra);
                 inactiveUsers.put(ra.OrgID, tmp);
             }
             else {
                 inactiveUsers.put(ra.OrgID, new List<HW_RecordsStore.RecordsWithReducedAttributes>{ra});
             }    
         }

         //query records witn non SFDC email IDs
         Map<String, List<HW_RecordsStore.RecordsWithReducedAttributes>> externalUsers = new Map<String, List<HW_RecordsStore.RecordsWithReducedAttributes>>();
         for(Heroku_Warden_users__c rec : [SELECT user_name__c, LastModifiedDate, email__c, role__c, organizationId__c, Employee__r.INTEG_First_Name__c, Employee__r.INTEG_Last_Name__c, Heroku_Warden_organizations__r.name__c FROM Heroku_Warden_users__c WHERE role__c = 'admin' AND (NOT email__c like '%@salesforce.com') AND (NOT email__c like '%@heroku.com') AND (NOT email__c like '%@mulesoft.com') AND (NOT email__c like '%@salesforce.org') AND LastModifiedDate >= LAST_N_DAYS:1]){
             HW_RecordsStore.RecordsWithReducedAttributes ra = new HW_RecordsStore.RecordsWithReducedAttributes();
             ra.UserName = rec.user_name__c;
             ra.Email = rec.email__c;
             ra.Role = rec.role__c;
             ra.OrgID = rec.organizationId__c;
             ra.OrgName = rec.Heroku_Warden_organizations__r.name__c;
             ra.FirstName = rec.Employee__r.INTEG_First_Name__c;
             ra.LastName = rec.Employee__r.INTEG_Last_Name__c;
             ra.Query = 'get_non_sfdc_email_users';
             ra.AlertType = 'Remove External User';
             //ra.RecCount = recordsCount;
             if(externalUsers.containsKey(ra.OrgID)){
                 List<HW_RecordsStore.RecordsWithReducedAttributes> tmp = new List<HW_RecordsStore.RecordsWithReducedAttributes>(externalUsers.get(ra.OrgID));
                 tmp.add(ra);
                 externalUsers.put(ra.OrgID, tmp);
             }
             else {
                 externalUsers.put(ra.OrgID, new List<HW_RecordsStore.RecordsWithReducedAttributes>{ra});
             }  
         }

         //Map all alert types queried to corresponding individuals
         for(String user: users){
             if(noSSO.containsKey(user)){
                 userMap.put(user, noSSO.get(user));
             }
             //if user is a manager add direct reportee's alerts
             if(managerMap.containsKey(user)){
                 for(String staffEmail: managerMap.get(user)){
                     if(noSSO.containsKey(staffEmail) && userMap.containsKey(user)){
                         List<HW_RecordsStore.RecordsWithReducedAttributes> tmp = new List<HW_RecordsStore.RecordsWithReducedAttributes>(userMap.get(user));
                         tmp.addAll(noSSO.get(staffEmail));
                         userMap.put(user, tmp);
                     }
                     else if(noSSO.containsKey(staffEmail)){
                         userMap.put(user, noSSO.get(staffEmail));
                     }
                 }
             }
             if(adminUserOrgs.containsKey(user)) {
                 for(String orgid: adminUserOrgs.get(user)){
                     //Adding records for inactive users
                     if(inactiveUsers.containsKey(orgid) && userMap.containsKey(user)){
                         List<HW_RecordsStore.RecordsWithReducedAttributes> tmp = new List<HW_RecordsStore.RecordsWithReducedAttributes>(userMap.get(user));
                         tmp.addAll(inactiveUsers.get(orgid));
                         userMap.put(user,tmp);
                     }
                     else if(inactiveUsers.containsKey(orgid)){
                         userMap.put(user, inactiveUsers.get(orgid));
                     }
                    //Adding records for external users
                    if(externalUsers.containsKey(orgid) && userMap.containsKey(user)){
                        List<HW_RecordsStore.RecordsWithReducedAttributes> tmp = new List<HW_RecordsStore.RecordsWithReducedAttributes>(userMap.get(user));
                        tmp.addAll(externalUsers.get(orgid));
                        userMap.put(user,tmp);
                    }
                    else if(externalUsers.containsKey(orgid)){
                        userMap.put(user, externalUsers.get(orgid));
                    }
                 }
             }
         }

         List<HW_RecordsStore.EmailsRecords> emailRecordList = new List<HW_RecordsStore.EmailsRecords>();
           for(String email: userMap.keySet()){
                List<HW_RecordsStore.RecordsWithReducedAttributes> recList = new List<HW_RecordsStore.RecordsWithReducedAttributes>(new Set<HW_RecordsStore.RecordsWithReducedAttributes>(userMap.get(email)));
                Integer len = recList.size();
                if(len > 10){
                    //split in to small chunks for limiting heap size for JSON serialization when iterating through the list
                    for(Integer i=0;i<len; i+=10){
                        List<HW_RecordsStore.RecordsWithReducedAttributes> tempList = new List<HW_RecordsStore.RecordsWithReducedAttributes>();
                        tempList.clear();
                        for(Integer j=i;j<i+10 && j<len;j++){
                            tempList.add(recList[j]);
                        }
                        HW_RecordsStore.EmailsRecords emailrec = new HW_RecordsStore.EmailsRecords();
                        emailrec.email=email;
                        emailrec.ownername=usernameMap.get(email);
                        emailrec.recsList=tempList;
                        emailrec.count =tempList.size();
                        emailrec.recsCount=recsCount;
                        emailRecordList.add(emailrec);
                    }
                }else {
                    HW_RecordsStore.EmailsRecords emailrec1 = new HW_RecordsStore.EmailsRecords();
                    emailrec1.email=email;
                    emailrec1.ownername=usernameMap.get(email);
                    emailrec1.recsList=recList;
                    emailrec1.count=len;
                    emailrec1.recsCount=recsCount;
                    emailRecordList.add(emailrec1);
                }   
            } 
            enrichPayload(emailRecordList, 'herokuwarden');
    }

    private static void enrichPayload(List<HW_RecordsStore.EmailsRecords> obj, String source_type){
            Map<String, Object> payloadMap = new Map<String, Object>();
            Integer recordsCount = obj.size();
            if(recordsCount < 100){
                payloadMap.put('records', obj);
                //payloadMap.put('query', query);
                payloadMap.put('source_type', source_type);
                CloudAMQPController.sendRequest(JSON.serialize(payloadMap));
            } else {
                List<HW_RecordsStore.EmailsRecords> tempList = new List<HW_RecordsStore.EmailsRecords>();
                for(Integer i=0;i<recordsCount; i+=100){
                    for(Integer j=i;j<i+100 && j<recordsCount;j++){
                        tempList.add(obj[j]);
                    }
                    payloadMap.put('records', tempList);
                    //payloadMap.put('query', query);
                    payloadMap.put('source_type', source_type);
                    CloudAMQPController.sendRequest(JSON.serialize(payloadMap));
                    tempList.clear();
                    payloadMap.clear();
                }
            }
    } 
    
    /*public with sharing class UserProcessor implements Schedulable{
        public void execute(SchedulableContext ctx) {
            HW_UsersScheduler.nonCompliantUsers();
        }
    }*/
}
global class DetectionEventCriteriaHealthCheckBatch implements Database.Batchable<AggregateResult>{
	
    // The batch job starts
	global Iterable<AggregateResult> start(Database.BatchableContext bc){
		String query = 'SELECT Count(Id), DetectionSecurityEventCriteria__c, DetectionSecurityEventCriteria__r.createdby.name, DetectionSecurityEventCriteria__r.createdby.email '
                       + ' FROM Detection_Security_Event__c '
                       + ' WHERE CreatedDate = YESTERDAY '
                       + ' GROUP BY DetectionSecurityEventCriteria__c, DetectionSecurityEventCriteria__r.createdby.name, DetectionSecurityEventCriteria__r.createdby.email '
                       + ' HAVING COUNT(Id) > 2000';
        
		return new AggregateResultIterable(query);
  	} 
    
	// The batch job executes and operates on one batch of records
	global void execute(Database.BatchableContext bc, List<sObject> scope){ 
        
        for(sObject sObj : scope) {
          AggregateResult ar = (AggregateResult)sObj;
          System.debug('>>>> COUNT : ' + ar.get('Count(Id)'));
        }
        
	}
    
    // The batch job finishes
    global void finish(Database.BatchableContext bc) {
        
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
		string [] toaddress= New string[]{'amcneilly@salesforce.com'};
		email.setSubject('TEST');
		email.setPlainTextBody('DONE');
		email.setToAddresses(toaddress);
        
		Messaging.sendEmail(New Messaging.SingleEmailMessage[]{email});
        
    }
}
@RestResource(urlMapping='/THRest/*')

global class THValidityRest {

    public static final String SINCE_PARAM = 'since';

    /**
     * Gets a list of Accounts
     * @param  since  Created date, Fectch all accounts after the cretaed date.
     * @return  Boolean
     */
    @HttpGet
    global static String getOrgValidty() {

        try {
            RestRequest req = RestContext.request;
            DateTime since  = getDateParameter(SINCE_PARAM);
            if(since == null) {
                return 'Invalid Org Creation Date';
            }

            return THValidService.getOrgValidty(since);

        } catch (Exception ex) {
            return ex.getMessage();
        }
    }

    public static DateTime getDateParameter(String paramName) {
        String strvalue = RestContext.request.params.get(paramName);
        If (String.isBlank(strvalue)) return null;

        DateTime dt = (DateTime)JSON.deserialize( '"' + strvalue + '"', DateTime.class);
        return DateTime.newInstance(dt.getTime());
    }
}
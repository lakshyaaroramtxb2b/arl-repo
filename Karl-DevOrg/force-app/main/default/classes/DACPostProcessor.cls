public virtual class DACPostProcessor {
    
	public virtual void ProcessDAC(Boolean suppress, Detection_Alert__c da, String auditMessage)
    {
        try
        {
            if(!suppress)
            {
                da.Suppressed__c = true;
                UPDATE da;
                
                FeedItem post = new FeedItem();
                post.ParentId = da.Id;
                post.Body = 'Alert was suppressed by call to DACPostProcessor. Reason specfified: ' + auditMessage;
                INSERT post;
                
                //send to response org
                if(da.RecipientTeam__c == 'CSIRT')
                {
                    List<CSIRT_HANDLEDETECTIONALERT.IntakeAlert> alerts = new List<CSIRT_HANDLEDETECTIONALERT.IntakeAlert>();
                        
                        CSIRT_HANDLEDETECTIONALERT.IntakeAlert newAlert = new  CSIRT_HANDLEDETECTIONALERT.IntakeAlert();             
                        newAlert.DetectionAlert = da.Id;
                        newAlert.Environment = da.Environment__c;
                        newAlert.Subject = da.Subject__c;
                        newAlert.Description = da.AlertInformation__c;
                        newAlert.EventTimestamp = da.CreatedDate; 
                        newAlert.RecipientTeam = da.RecipientTeam__c;
                        newAlert.Severity = da.Severity__c;                    
                        alerts.add(newAlert);
                        
                    CSIRT_HANDLEDETECTIONALERT.ProcessAlert(alerts);
                }
            }
        }
        catch (Exception ex) 
        {
            Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
            message.toAddresses = new String[] { da.Owner.Email };
            message.subject = 'DACPostProcessor Error';
            message.plainTextBody = ex.getMessage();
            Messaging.SingleEmailMessage[] messages =   new List<Messaging.SingleEmailMessage> {message};
            Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
        }
    }
    
    public void EnrichEvent(Detection_Security_Event__c evt, String json)
    {
        try
        {
			//add json to event
            Map<String, Object> results = (Map<String, Object>)System.JSON.deserializeUntyped(evt.FieldMapRaw__c);
            
            if(results != null)
            	evt.FieldMapRaw__c = evt.FieldMapRaw__c.removeEnd('}') + ',' + json + '}';
            
            UPDATE evt;
        }
        catch (Exception ex) 
        {
            throw ex;
        }
    }
}
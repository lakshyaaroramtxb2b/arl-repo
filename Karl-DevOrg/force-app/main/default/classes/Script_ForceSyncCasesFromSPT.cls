public class Script_ForceSyncCasesFromSPT{

   public static void forceSyncCases() {


            List<Case__x> caseX = [SELECT CaseNumber__c,ExternalId
                                   FROM Case__x 
                                   WHERE IsClosed__c = false 
                                   AND RecordTypeId__c = '0120000000001TgAAI'];


            List<Case> lstCases = [SELECT Enterprise_Security_Case_Number__c,
                                          SupportForce_Case_Id__c
                                   FROM Case
                                   WHERE SupportForce_Case_Id__c!=null];

            Map<String,Id> mapSecCases = new Map<String,Id>();

            for(Case cas : lstCases){
                mapSecCases.put(cas.Enterprise_Security_Case_Number__c, cas.SupportForce_Case_Id__c);
            }

            Set<String> missingCases = new Set<String>();
            for(Case__x cas : caseX){
                if(!mapSecCases.containsKey(cas.CaseNumber__c)){
                    missingCases.add(cas.ExternalId);
                }
            }

            system.debug('Mising Cases ##'+missingCases.size());

            for(String st : missingCases){
              system.debug('##########'+st);
            }
            if(missingCases.size() > 0){
                List<CaseComment__x> lstCaseComments = new List<CaseComment__x>();
                for(String caseId : missingCases){
                    CaseComment__x sfCaseComment = new CaseComment__x();
                    sfCaseComment.CommentBody__c = Label.ESA_Intake_Bot +'\n Force Sync';
                    sfCaseComment.IsPublished__c = false;
                    sfCaseComment.ParentId__c = caseId;
                    lstCaseComments.add(sfCaseComment);
                
                }
                if(lstCaseComments.size() > 0) {
                    system.debug('@@@@@@@'+lstCaseComments.size());
                    //List<Database.SaveResult> sr = Database.InsertImmediate(lstCaseComments);   
                }
            }
    }

}
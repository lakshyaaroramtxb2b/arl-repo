public class Bug_review_meeting_email {
  public static void sendMail(){
        string toMail; 
    	string ccMail;
    	string repMail;
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        string[] to = new string[] {'pkannan@salesforce.com'};
        //string[] cc = new string[] {ccMail};
            
        OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'securecloud@salesforce.com'];
		repMail = 'pkannan@salesforce.com';
		if ( owea.size() > 0 ) {
    		email.setOrgWideEmailAddressId(owea.get(0).Id);
		}
        
        email.setToAddresses(to);
      
        if(repmail!=null && repmail!= '')
            email.setInReplyTo(repMail);
         
        email.setSubject('BUG REVIEW' + date.today().addDays(3).format());
         
        email.setHtmlBody('Hi all, <br/><br/><br/> See below for the list of bugs to be discussed during the bug review meeting on Monday. <br/> Please ensure you attend the Bug Review meeting if your name is mentioned in the list below. <br/>Please contact Akshay Shetty if you have any questions regarding your bugs.<br/><br/> Thank you!');
        try{
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { email });
        }catch(exception e){
            apexpages.addmessage(new apexpages.message(apexpages.severity.error,e.getMessage()));
        }
         
        toMail = '';
        ccMail = '';
        repMail = '';
    }




}
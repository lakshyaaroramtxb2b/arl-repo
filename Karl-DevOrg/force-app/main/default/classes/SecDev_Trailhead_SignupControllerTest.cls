@isTest                                
public class SecDev_Trailhead_SignupControllerTest {
    // Trial Success
    static testmethod void testSecDevTrailheadSignupSuccess() {
        insert new Trial_Id__c(name='SecDev_Trailhead',Trial_Id__c='0TT500000004xdw');
        SecDev_Trailhead_SignupController dfsc = new SecDev_Trailhead_SignupController();
        dfsc.msa = true;
        dfsc.contactOK = true;
        dfsc.firstName = 'Bob';
        dfsc.lastName = 'Bob'; 
        dfsc.company = 'Salesforce.com';
        dfsc.email = 'ktobener@salesforce.com' ;
        dfsc.createSignup();
        system.debug(dfsc.publickey);
        dfsc.reset();
    }
    // Trial Fail
    static testmethod void testSecDevTrailheadSignupFail() {
        insert new Trial_Id__c(name='SecDev_Trailhead',Trial_Id__c='0TT500000004xdw');
        SecDev_Trailhead_SignupController dfsc = new SecDev_Trailhead_SignupController();
        dfsc.msa = false;
        dfsc.contactOK = false;
        dfsc.firstName = '';
        dfsc.lastName = ''; 
        dfsc.company = '';
        dfsc.email = '' ;
        dfsc.createSignup();
    }    
}
/*
* Class Name: KARL_SendEmailToNotSubmitedEvidence 
* Description: Batch class to send email when evidence is not submitted with 24 hrs after saving.
* Author/Date: Swarnima Singh Mandhata
* Email: swarnima.singh@mtxb2b.com
* Date New/Modified: 
*/
public with sharing class KARL_SendEmailToNotSubmitedEvidence implements Database.Batchable<sObject>,Database.Stateful, schedulable{
    
    public Database.QueryLocator start(Database.BatchableContext BC){
        Date todaysDate = date.today();
        
        return Database.getQueryLocator([SELECT Id, Name, KARL_Submitted__c, KARL_Cycle_ARL_Item__c,KARL_Cycle_ARL_Item__r.Request_Assignee__c, KARL_Request_Type__c, KARL_Request_Name__c, KARL_Audit_Cycle__c, 
                                         KARL_Primary_Scope__c, KARL_Direct_Link__c, GUS_Details__c, KARL_Request_Assignee__c,KARL_GUS_Ticket_Details__c,KARL_Cycle_ARL_Item__r.Request_Assignee__r.email, 
                                         KARL_Request_Assignee_Name__c, Evidence_Upload_Date__c, Evidence_Status__c,Owner.Email,Owner.FirstName 
                                         FROM KARL_Evidence_Request__c WHERE KARL_Submitted__c = false AND Evidence_Upload_Date__c != null AND Evidence_Upload_Date__c <: todaysDate 
                                         AND KARL_Cycle_ARL_Item__r.Request_Assignee__r.email != null WITH SECURITY_ENFORCED]);
    }
    public void execute (Database.BatchableContext BC,List<sObject> scopeList){
        List<Messaging.SingleEmailMessage> mailList = new List<Messaging.SingleEmailMessage>();
        Set<String> toAddressSet = new Set<String>();
        EmailTemplate templateRec = [Select id,Subject, HtmlValue FROM EmailTemplate WHERE DeveloperName  = 'KARL_send_After_24_hour_Upload' LIMIT 1];
        String ScopeName = '';
        Date todaysDate;
        if(!scopeList.isEmpty()){
            for(KARL_Evidence_Request__c evidenceRequest: (List<KARL_Evidence_Request__c>)scopeList){
                if(String.isNotEmpty(evidenceRequest.KARL_Primary_Scope__c)){
                    ScopeName = evidenceRequest.KARL_Primary_Scope__c;
                }
                if(String.isNotEmpty(evidenceRequest.Owner.Email)){
                    toAddressSet.add(evidenceRequest.Owner.Email);
                }
                if(String.isNotEmpty(evidenceRequest.KARL_Cycle_ARL_Item__r.Request_Assignee__c)){
                    toAddressSet.add(evidenceRequest.KARL_Cycle_ARL_Item__r.Request_Assignee__c);
                }
            }
            for(KARL_Scope_Notifications_Config__c karlScope: [SELECT Id, Name, Scope__c,Group_Email_Alias__c, Mute_Emails__c,OwnerId,Owner.email 
                                                               FROM KARL_Scope_Notifications_Config__c
                                                               WHERE Scope__c =: ScopeName LIMIT 1]){
                                                                   
                                                                   if(String.isNotEmpty(karlScope.Group_Email_Alias__c)){
                                                                       toAddressSet.add(karlScope.Group_Email_Alias__c);
                                                                   }
                                                               }
            
            for(KARL_Evidence_Request__c evidenceRequest: (List<KARL_Evidence_Request__c>)scopeList){
                todaysDate = date.today();
                Date dayBeforeYesterdayDate = date.today().addDays(-2);
                if(evidenceRequest.Evidence_Upload_Date__c < todaysDate && evidenceRequest.Evidence_Upload_Date__c > dayBeforeYesterdayDate){
                    
                    Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
                    message.setTemplateId(templateRec.id);
                    message.setSubject(templateRec.Subject);
                    String htmlBody = templateRec.htmlvalue;
                    
                    //htmlBody = htmlBody.replace('@evidence ', evidenceReq.Name);
                    if(String.isNotEmpty(evidenceRequest.KARL_Primary_Scope__c)){
                        htmlBody = htmlBody.replace('@Scope', evidenceRequest.KARL_Primary_Scope__c);
                    }
                    htmlBody = htmlBody.replace('@evidenceName',evidenceRequest.Name);
                    if(String.isNotEmpty(evidenceRequest.KARL_Audit_Cycle__c)){
                        htmlBody = htmlBody.replace('@auditCycle', evidenceRequest.KARL_Audit_Cycle__c);
                    }
                     if(String.isNotEmpty(evidenceRequest.Evidence_Status__c)){
                        htmlBody = htmlBody.replace('@evidenceStatus', evidenceRequest.Evidence_Status__c);
                    }
                    if(String.isNotEmpty(evidenceRequest.Owner.FirstName)){
                        htmlBody = htmlBody.replace('@ownerFirstName',evidenceRequest.Owner.FirstName);
                    }else{
                        htmlBody = htmlBody.replace('@ownerFirstName','');
                    }
                     
                    String link = '';
                    link = System.Label.KARL_org_Link+evidenceRequest.Id;
                    htmlBody = htmlBody.replace('@link',link);

                    message.setHtmlBody(htmlBody);
                    if(!toAddressSet.isEmpty()){
                        message.setToAddresses(new list<String>(toAddressSet));
                    }
                    mailList.add(message);
                } 
            }
        }
        if(!mailList.isEmpty()){
            Messaging.SendEmailResult[] emailResults  = Messaging.sendEmail( mailList, false);
            for(Messaging.SendEmailResult result : emailResults){
                
                if(!result.IsSuccess()){
                    String errorLog ='';
                    for(Database.Error error :  result.getErrors()){
                        System.debug(error.getMessage());
                        errorLog+=error.getMessage();
                    }
                    System.debug(errorLog);
                }
            }
        }  
    }
    public void finish(Database.BatchableContext BC){
        
    }
    public void execute(SchedulableContext SC) {
        if(!Test.isRunningTest()){
            database.executebatch(new KARL_SendEmailToNotSubmitedEvidence());
        }   
    }
}
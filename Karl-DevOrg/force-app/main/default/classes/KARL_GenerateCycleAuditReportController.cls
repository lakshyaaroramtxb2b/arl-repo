/**
* @author Lakshya Arora
* @email lakshya.arora@mtxb2b.com
* @description This class is used in Generate Report Action
*/
public with sharing class KARL_GenerateCycleAuditReportController {
    /**
* @author Lakshya Arora
* @email lakshya.arora@mtxb2b.com
* @description This method creates cycle audit reports for each scope
* @param auditCycleId - record id for the audit cycle to generate requests for
*/
    @AuraEnabled
    public static String createCycleRequestItems(String auditCycleId){  
        List<KARL_Cycle_Audit_Report_Items__c> cycleAuditReportList = new List<KARL_Cycle_Audit_Report_Items__c>();
        Map<String,Audit_Cycle_Coverage__c> eligibleAuditCycleCoverageMap = new Map<String,Audit_Cycle_Coverage__c>();
        Map<String,KARL_Audit_Scope_Reports__c> auditMasterDataMap = new Map<String,KARL_Audit_Scope_Reports__c>();
        Set<String> duplicateRecordCheckSet = new Set<String>();
        Set<Id> auditFirmIdSet = new Set<Id>();
        Set<String> scopeSet = new Set<String>();
        Set<String> areaSet = new Set<String>();
        Integer totalAuditCoverage = 0;
        Integer successAuditCoverage = 0;
        Integer cycleAuditReportsCreated;
        Integer totalAuditCoverageRecords = 0;
        
        for(Audit_Cycle_Coverage__c auditCycleCoverageObj : [SELECT Id,Area__c,Scope__c,toLabel(Scope__c) scope,toLabel(Area__c) area,KARL_Audit_Team__c,
                                                             KARL_Audit_Team__r.KARL_Audit_Firm__c,DNC_Cycle_Audit_Report__c,
                                                             Audit_Cycle__r.Name,Audit_Cycle__r.Cycle_Audit_Reports_Created__c,Audit_Cycle__r.Cycle_Audit_Reports_Not_Created__c
                                                             FROM Audit_Cycle_Coverage__c
                                                             WHERE Audit_Cycle__c=: auditCycleId
                                                             WITH SECURITY_ENFORCED]){
                                                                 totalAuditCoverageRecords++;
                                                                 if(!auditCycleCoverageObj.DNC_Cycle_Audit_Report__c){
                                                                     System.debug('cycleAuditReportsCreated = '+cycleAuditReportsCreated);
                                                                     if(cycleAuditReportsCreated == null)
                                                                         cycleAuditReportsCreated =  auditCycleCoverageObj.Audit_Cycle__r.Cycle_Audit_Reports_Created__c != null ? Integer.valueOf(auditCycleCoverageObj.Audit_Cycle__r.Cycle_Audit_Reports_Created__c) : 0;
                                                                     String key = auditCycleCoverageObj.Scope__c+auditCycleCoverageObj.Area__c;
                                                                     duplicateRecordCheckSet.add(key);
                                                                     scopeSet.add(auditCycleCoverageObj.Scope__c);
                                                                     areaSet.add(auditCycleCoverageObj.Area__c);
                                                                     if(auditCycleCoverageObj.KARL_Audit_Team__c != null)
                                                                         auditFirmIdSet.add(auditCycleCoverageObj.KARL_Audit_Team__r.KARL_Audit_Firm__c);
                                                                     eligibleAuditCycleCoverageMap.put(key,auditCycleCoverageObj);
                                                                 }                        
                                                             }
        if(!eligibleAuditCycleCoverageMap.isEmpty()){
            if(!auditFirmIdSet.isEmpty() && !scopeSet.isEmpty()){
                for(KARL_Audit_Scope_Reports__c auditScopeReportObj : [SELECT Id,KARL_Audit_Scope__r.KARL_Scope__c,KARL_Audit_Firm__c,Report_Name__c
                                                                       FROM KARL_Audit_Scope_Reports__c 
                                                                       WHERE KARL_Audit_Scope__r.KARL_Scope__c IN:scopeSet
                                                                       AND KARL_Audit_Firm__c IN:auditFirmIdSet
                                                                       WITH SECURITY_ENFORCED]){
                                                                           auditMasterDataMap.put(auditScopeReportObj.KARL_Audit_Scope__r.KARL_Scope__c+auditScopeReportObj.KARL_Audit_Firm__c,auditScopeReportObj);
                                                                       }
            }
            for(KARL_Cycle_Audit_Report_Items__c carObj : [SELECT Id,Area_of_Compliance__c,Scope__c
                                                           FROM KARL_Cycle_Audit_Report_Items__c
                                                           WHERE Audit_Cycle__c=: auditCycleId
                                                           AND Scope__c IN: scopeSet
                                                           AND Area_of_Compliance__c IN: areaSet
                                                           WITH SECURITY_ENFORCED]){
                                                               String key = carObj.Scope__c+carObj.Area_of_Compliance__c;
                                                               System.debug(LoggingLevel.DEBUG,'Is duplicate = '+duplicateRecordCheckSet.contains(key));
                                                               if(duplicateRecordCheckSet.contains(key)){
                                                                   if(eligibleAuditCycleCoverageMap.containsKey(key)){
                                                                       eligibleAuditCycleCoverageMap.remove(key);
                                                                   }
                                                               }                                        
                                                           }
            for(String key : eligibleAuditCycleCoverageMap.keySet()){
                Audit_Cycle_Coverage__c auditCycleCoverageObj = eligibleAuditCycleCoverageMap.get(key);
                KARL_Cycle_Audit_Report_Items__c cycleAuditReportObj = new KARL_Cycle_Audit_Report_Items__c();
                cycleAuditReportObj.Area_of_Compliance__c = auditCycleCoverageObj.Area__c;
                cycleAuditReportObj.Scope__c = auditCycleCoverageObj.Scope__c;
                cycleAuditReportObj.Audit_Cycle__c = auditCycleId;
                String reportName = '';
                if(auditMasterDataMap.containsKey(auditCycleCoverageObj.scope__c+auditCycleCoverageObj.KARL_Audit_Team__r.KARL_Audit_Firm__c)){
                    KARL_Audit_Scope_Reports__c auditScopeReportObj = auditMasterDataMap.get(auditCycleCoverageObj.scope__c+auditCycleCoverageObj.KARL_Audit_Team__r.KARL_Audit_Firm__c);
                    cycleAuditReportObj.Audit_Reports_Master_Data__c = auditScopeReportObj.Id;
                    if(auditScopeReportObj.Report_Name__c != null)
                        reportName = auditScopeReportObj.Report_Name__c + ' - ';
                }
                String cycleAuditReportName = auditCycleCoverageObj.Audit_Cycle__r.Name + ' - '+reportName + auditCycleCoverageObj.get('area');
                if(cycleAuditReportName.length() > 80){
                    cycleAuditReportObj.Name = cycleAuditReportName.substring(0,80);
                }
                else{
                    cycleAuditReportObj.Name = cycleAuditReportName;
                }
                cycleAuditReportObj.Status__c = KARL_Constants.REPORT_STATUS_PENDING;
                cycleAuditReportList.add(cycleAuditReportObj);
            }
        }
        List<Audit_Cycle_Coverage__c> auditCycleCoverageList = new List<Audit_Cycle_Coverage__c>();
        if(!cycleAuditReportList.isEmpty()){
            totalAuditCoverage = cycleAuditReportList.size();
            Database.SaveResult[] results = Database.insert(cycleAuditReportList,false);
            for(Integer i = 0; i < results.size(); i++) {
                if(!results[i].isSuccess()) {
                    String errorMessage = '';
                    for(Database.error err : results[i].getErrors()){
                        if(errorMessage == ''){
                            errorMessage = err.getMessage();
                        }
                        else {
                            errorMessage = errorMessage + ','+err.getMessage();
                        }
                    }
                    System.debug(LoggingLevel.DEBUG, errorMessage);
                }
                else if(results[i].isSuccess()){
                    String key = cycleAuditReportList[i].Scope__c + cycleAuditReportList[i].Area_of_Compliance__c;
                    if(eligibleAuditCycleCoverageMap.containsKey(key)){
                        Audit_Cycle_Coverage__c auditCycleCoverageObj = eligibleAuditCycleCoverageMap.get(key);
                        System.debug('success id = '+results[i].getId());
                        auditCycleCoverageObj.Cycle_Audit_Report_Items__c = results[i].getId();
                        auditCycleCoverageList.add(auditCycleCoverageObj);
                    }
                    
                    successAuditCoverage++;
                }
            }
        }
        if(!auditCycleCoverageList.isEmpty()){
            update auditCycleCoverageList;
        }
        if(auditCycleId != '' || auditCycleId != null){
            if(cycleAuditReportsCreated == null)
            cycleAuditReportsCreated =0;
            Audit_Cycle__c auditCycle = new Audit_Cycle__c(Id = auditCycleId,
                                                           Cycle_Audit_Reports_Created__c = cycleAuditReportsCreated + successAuditCoverage,
                                                           Cycle_Audit_Reports_Not_Created__c = totalAuditCoverageRecords - (cycleAuditReportsCreated + successAuditCoverage) );
            update auditCycle;
        }
        return successAuditCoverage+' Reports Created and '+(totalAuditCoverage - successAuditCoverage)+' not created';
    }
}
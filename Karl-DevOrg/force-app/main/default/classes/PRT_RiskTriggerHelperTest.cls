/***********************************************************
* Test class for PRT_RiskTriggerHelper
* Created by 	- Prashant Gupta
************************************************************/
@isTest
public class PRT_RiskTriggerHelperTest {
    static testMethod void checkRequiredFieldTest(){
        List<Project__c> projectList = new List<Project__c>(PRT_TestDataFactory.createProjects(1,true));
        List<Risk__c> riskList = new List<Risk__c>(PRT_TestDataFactory.createRisk(1,projectList[0].Id,false));
        insert riskList;
        test.startTest();
        try{
            riskList[0].Status__c = PRT_Constants.PRT_RISK_STATUS_CLOSED;
            update riskList;
        }catch(exception e){
            System.assert(e.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION'));
        }
        
    }

    static testMethod void updateSlippageCommentOfProjectTest() {
        List<Project__c> projectList = new List<Project__c>(PRT_TestDataFactory.createProjects(1,false));
        projectList.get(0).RecordTypeId = PRT_Constants.PRT_PROJECT_PROJECT_RECORDTYPEID;
        INSERT projectList;
        List<Risk__c> riskList = new List<Risk__c>(PRT_TestDataFactory.createRisk(1,projectList[0].Id,false));
        riskList.get(0).Type__c = 'Scope';
        riskList.get(0).Due_Date__c = Date.today().addDays(5);
        riskList.get(0).Probability__c = 'High';
        riskList.get(0).Impact__c = 'High';
        riskList.get(0).Response_Strategy__c = 'Test';
        riskList.get(0).Action__c = 'Transfer';
        riskList.get(0).Status__c = 'In Progress';
        riskList.get(0).Description__c = 'Test 101 Desc';
        INSERT riskList;
        List<Project__c> updatedProjectList = new List<Project__c>([SELECT Slippage_Comments__c FROM Project__c]);
        System.assertEquals('- Test 101 Desc', updatedProjectList.get(0).Slippage_Comments__c);
        List<Risk__c> updateRiskList = new List<Risk__c>([SELECT Id, Description__c FROM Risk__c]);
        updateRiskList.get(0).Status__c = 'Closed';
        UPDATE updateRiskList;
        List<Project__c> updatedProject2List = new List<Project__c>([SELECT Slippage_Comments__c FROM Project__c]);
        System.assertEquals(null, updatedProject2List.get(0).Slippage_Comments__c);
    }    
}
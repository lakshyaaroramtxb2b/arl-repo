/*
Developer: Jake 9/17/18 <jake.hebert@callawaycloudconsulting.com>
Batch to sync frozen contacts with Org62 user records
JIRA SFS-103
*/

public class SC_SetFrozenOrg62Scheduler implements Schedulable {
	// Tested by SC_BatchSetFrozenOrg62UsersTest
	public void execute(SchedulableContext sc) {
		Database.executeBatch((Database.Batchable<SObject>) Type.forName('SC_BatchSetFrozenOrg62Users').newInstance());
	}
}
/*
     * Created by - Hari ch
     * Updated By - Prashant Gupta
     * Updated By - Virendra Yadav - added new constants for status
     * Class is used to store all constant values in a single place to help facilitate
     * as and when needed with minimum code change.
    */
public class PRT_Constants {
    public static  boolean RECURSIVE_TRIGGER_CHECK = TRUE;
    //PRT CASE STATUS
    public static final String PRT_PROJECT_STATUS_SCORED_BUSINESS_CASE = 'Review & Approval';
    public static final String PRT_PROJECT_STATUS_APPROVED = 'Approved';
    public static final String PRT_PROJECT_STATUS_SUBMITTED_BUSINESS_CASE = 'Awaiting Score';
    public static final String PRT_PROJECT_STATUS_DRAFT_BUSINESS_CASE = 'Draft';
    public static final String PRT_PROJECT_STATUS_ROC = 'Ready for Charter';
    public static final String PRT_PROJECT_STATUS_CHARTERING = 'Chartering';
    public static final String PRT_PROJECT_STATUS_PSR = 'Project Sponsor Review';
    public static final String PRT_PROJECT_CLOSURE_PROCESS = 'Closure Processes';
    public static final String PRT_PROJECT_STATUS_CANCELLED = 'Cancelled';
    public static final String PRT_PROJECT_CRR = 'Cancel Project';
    public static final String PRT_PROJECT_PCR = 'Portfolio Manager Review';
    public static final String PRT_PROJECT_HGRCCR = 'Executive Sponsor Review';
    //PRT Risk 
    public static final String PRT_RISK_CURRENT_STATE_ISSUE = 'Issue';
    public static final String PRT_RISK_CURRENT_STATE_RISK = 'Risk';
    
    
    public static final String PRT_CR_STATUS_PROJECTKICKOFF = 'Project Kickoff';
    public static final String PRT_CR_STATUS_EXECUTING = 'Executing';
    public static final String PRT_CR_STATUS_PLANNING = 'Planning';
    public static final String PRT_CR_STATUS_MONITORING = 'Monitoring';
        
    public static final String PRT_CR_STATUS_COMPLETIONSO = 'Completion Sign-off';
    public static final String PRT_CR_STATUS_CLOSED = 'Closed';
    
    /* milestone status */
    public static final String PRT_MILE_STATUS_COMPLETE = 'Completed';
    public static final String PRT_MILE_STATUS_NOT_STARTED = 'Not Started';
    public static final String PRT_MILE_STATUS_ON_TRACK = 'On Track';
    public static final String PRT_MILE_STATUS_WATCH = 'Watch';
    public static final String PRT_MILE_STATUS_BLOCKED = 'Blocked';

    /* risk Status*/
    public static final String PRT_RISK_STATUS_New = 'New';
    public static final String PRT_RISK_STATUS_ASSIGNED = 'Assigned';
    public static final String PRT_RISK_STATUS_IN_PROGRESS = 'In Progress';
    public static final String PRT_RISK_STATUS_CLOSED = 'Closed';
    public static final String PRT_RISK_STATUS_BLOCKED = 'Blocked';

    /*change Request*/
    public static final String PRT_CR_STATUS_DRAFT = 'Draft';
    public static final String PRT_CR_STATUS_SUBMITTED = 'Submitted';
    public static final String PRT_CR_STATUS_APPROVED = 'Approved';
    public static final String PRT_CR_STATUS_REJECTED = 'Rejected';

    
    Public static ID PRT_CURRENT_USER_ID = userInfo.getUserId();
    Public static ID PRT_PROJECT_BUSINESSCASE_RECORDTYPEID = Schema.SObjectType.Project__c.getRecordTypeInfosByName().get('Business Case').getRecordTypeId();
    Public static ID PRT_PROJECT_PROJECT_RECORDTYPEID = Schema.SObjectType.Project__c.getRecordTypeInfosByName().get('Project').getRecordTypeId();
    
    public static Id PRT_USER_STORY_RECORDTYPEID = Schema.SObjectType.agf__ADM_Work__c.getRecordTypeInfosByName().get('User Story').getRecordTypeId();
    public static Id PRT_BUG_RECORDTYPEID = Schema.SObjectType.agf__ADM_Work__c.getRecordTypeInfosByName().get('Bug').getRecordTypeId();
    public static Id PRT_INVESTIGATION_RECORDTYPEID = Schema.SObjectType.agf__ADM_Work__c.getRecordTypeInfosByName().get('Investigation').getRecordTypeId();
    
    /* change request record type*/
    // Public static ID PRT_CR_BUDGET_RECORDTYPEID = Schema.SObjectType.Change_Request__c.getRecordTypeInfosByName().get('Budget').getRecordTypeId();
    // Public static ID PRT_CR_SCOPE_RECORDTYPEID = Schema.SObjectType.Change_Request__c.getRecordTypeInfosByName().get('Scope').getRecordTypeId();
    // Public static ID PRT_CR_ALLOCATION_RECORDTYPEID = Schema.SObjectType.Change_Request__c.getRecordTypeInfosByName().get('Allocation').getRecordTypeId();
    // Public static ID PRT_CR_COMMUNICATION_RECORDTYPEID = Schema.SObjectType.Change_Request__c.getRecordTypeInfosByName().get('Communications').getRecordTypeId();
    // Public static ID PRT_CR_QUALITY_RECORDTYPEID = Schema.SObjectType.Change_Request__c.getRecordTypeInfosByName().get('Quality').getRecordTypeId();
    // Public static ID PRT_CR_RISK_MANAGEMENT_RECORDTYPEID = Schema.SObjectType.Change_Request__c.getRecordTypeInfosByName().get('Risk Management').getRecordTypeId();
    public static ID PRT_CR_PRTChangeReq_RECORDTYPEID = Schema.SObjectType.Change_Request__c.getRecordTypeInfosByName().get('PPM Change Request').getRecordTypeId();
    /* picklist values*/
    public static final String PRT_OPERATIONAL_AREA_PROJECT = 'Project';
	
	Public Static final PRT_Integration_Settings__c INTEGRATION_SETTING = PRT_Integration_Settings__c.getInstance(); 
    
    
}
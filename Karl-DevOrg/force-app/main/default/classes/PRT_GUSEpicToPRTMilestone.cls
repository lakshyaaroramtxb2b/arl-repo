/***************PRT_GUSEpicToPRTMilestone****************
@Author Virendra
@Date 03/12/2019
@Description Batch class to create fetch GUS EPIC records on regular basis on certain creteria and then create crossponding Milestone record.
**********************************************/
global class PRT_GUSEpicToPRTMilestone implements Database.Batchable<sObject>,Schedulable {
    @TestVisible
    private static list<ADM_Epic_c__x> mockallGudEpicList = new list<ADM_Epic_c__x>();//For test class
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        if(Test.isRunningTest()){
            return Database.getQueryLocator([SELECT Id FROM User LIMIT 1]);    
        }else{
            // NEED TO CHANGE THE FIELDS
            return Database.getQueryLocator([Select Id,ExternalId,LastModifiedDate__c,Project_c__c, End_Date_c__c, Start_Date_c__c, Description_c__c, Name__c, Health_c__c, Success_Criteria_c__c, Planned_End_Date_c__c, Epic_Health_Comments_c__c From ADM_Epic_c__x Where LastModifiedDate__c >=LAST_N_DAYS:1  AND Success_Criteria_c__c!=null]);    
        }
    }
    public void execute(Database.BatchableContext BC, List<sObject> gusRecords)
    {        
        if(Test.isRunningTest()){
            gusRecords =  mockallGudEpicList;
            System.debug('gusRecords-->' + gusRecords);
        }
        Datetime oneHourBack = Datetime.now().addMinutes(-5);
        if(!gusRecords.isEmpty()){
            Set<Id> milestoneIds = new Set<Id>();
            for(ADM_Epic_c__x gusObject :(List<ADM_Epic_c__x>)gusRecords){
                // Need to change the field names here
                if(gusObject.LastModifiedDate__c >=oneHourBack || Test.isRunningTest()){
                    if(!Test.isRunningTest()){
                        milestoneIds.add(gusObject.id);
                    }else{
                        milestoneIds.add(gusObject.Success_Criteria_c__c);
                    }
                    
                }
            }
            Map<String,Milestone_PRT__c> gusrecordToMileStoneMap = new Map<String,Milestone_PRT__c>();
            for(Milestone_PRT__c milestone : [SELECT Id,Completion_Date__c, Start_Date__c, Description__c,Name, Status__c, Project__c, Due_Date__c,GUS_Epic__c FROM Milestone_PRT__c WHERE Id  IN : milestoneIds]){
                if(Test.isRunningTest()){
                    gusrecordToMileStoneMap.put('EX101',milestone);
                }else{
                    gusrecordToMileStoneMap.put(milestone.GUS_Epic__c,milestone);
                }
            }
            List<Milestone_PRT__c> milestoneUpdateList = new List<Milestone_PRT__c>();
            for(ADM_Epic_c__x gusObject : (List<ADM_Epic_c__x>)gusRecords){
                if(gusObject.LastModifiedDate__c >=oneHourBack || Test.isRunningTest()){
                    if(gusrecordToMileStoneMap.containsKey(gusObject.ExternalId)){
                        Milestone_PRT__c milestone =  gusrecordToMileStoneMap.get(gusObject.ExternalId);
                        milestone.GUS_Epic__c = gusObject.Success_Criteria_c__c;
                        if(milestone.Completion_Date__c != gusObject.End_Date_c__c || 
                           milestone.Name != gusObject.Name__c ||                        
                           milestone.Start_Date__c != gusObject.Start_Date_c__c || milestone.Description__c != gusObject.Description_c__c || milestone.Status__c != gusObject.Health_c__c || milestone.Due_Date__c != gusObject.Planned_End_Date_c__c) {
                               milestone.Name = gusObject.Name__c;
                               milestone.Completion_Date__c = gusObject.End_Date_c__c;
                               milestone.Start_Date__c = gusObject.Start_Date_c__c;
                               milestone.Description__c = gusObject.Description_c__c;
                               milestone.Status__c = gusObject.Health_c__c;
                               milestone.Due_Date__c = gusObject.Planned_End_Date_c__c;
                               milestone.Milestone_Health__c = gusObject.Epic_Health_Comments_c__c;
                               milestoneUpdateList.add(milestone);
                           }
                    }
                }
            }
            update milestoneUpdateList;                
        }
        
    }
    
    public void finish(Database.BatchableContext BC) {
        
        PRT_GUSEpicToPRTMilestone batch = new PRT_GUSEpicToPRTMilestone();
        if(!Test.isRunningTest()){
            System.scheduleBatch(batch, 'PRT_GUSEpicToPRTMilestone',4);
        } 
    }
    public void execute(SchedulableContext SC) {
        database.executebatch(new PRT_GUSEpicToPRTMilestone());
    }
    
}
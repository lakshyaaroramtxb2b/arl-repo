public class theme_assignment {
    //Bug_update_time__c
    //last_update__c
    public static map<string,list<string>> theme_work;
    @future
    public static void theme_insert(set<id> set_bugs){
        
        
        
       list<bug_management__c> bug_management = new list<bug_management__c>();
       list<id> Gus_list= new list<id>();
        
       //Theme assignment for the bugs
       //for(bug_management__c val: [SELECT Gus_work_id__c FROM bug_management__c ])
       // {
        //    Gus_list.add(val.Gus_work_id__c);
        //}
        Gus_list.addall(set_bugs);
        integer list_length = Gus_list.size();
        system.debug(list_length);
        //Truncating the list so that the query length does not exceed the length (Upper limit ~ 1400 bugs)
        /**
        list<string> Gus_list_1= new list<string>();
        list<id> Gus_list_2= new list<id>();
        list<id> Gus_list_3= new list<id>();
       
        for(Integer i = 0; i< list_length; i++)
    	{
        	Switch on Math.mod(i,100){
            	when 0{
                //Gus_list_1.add(Gus_list[i]);
            }
            when 1{
                Gus_list_2.add(Gus_list[i]);
            }
            when 2{
                Gus_list_3.add(Gus_list[i]);
            }
        }}
        system.debug('Truncated');
       
        
        
        for(integer i=0; i<2; i++)
        {
           Gus_list_1.add(string.valueof(Gus_list[i])); 
        }
		**/
        theme_work = new map<string,list<string>>();
        system.debug(list_length);
        //Adding work & theme relationship
        theme_work_r(Gus_list);
        //theme_work_r(Gus_list_2);
        //theme_work_r(Gus_list_3);
        
 
        list<bug_management__c> final_insert = new list<bug_management__c>();
        
        system.debug(theme_work.keyset().size());
        for(string x:theme_work.keyset()){
            bug_management__c y = new bug_management__c();
            string allstring = string.join(theme_work.get(x),'; ');
            y.Name=x;
            y.Theme_list__c =allstring;
            final_insert.add(y);
        }
        upsert final_insert Name;
        
        
        
   /**
            
        //Inserting Action Item Operation
        list<bug_management__c> action_insert = new list<bug_management__c>();
        map<string, string> CloudName = new map<string, string>();
        for(bug_management__c val: [SELECT Gus_work_id__c, Name,Cloud_name__c, Priority__c, Age__c,Theme_list__c FROM bug_management__c]){
            
           // Cloudname.put(val.Name,val.Cloud_name__c);
            
            
            //Action Item Assignment
           
            String Action_item ='';
            if((val.get('Theme_list__c'))!=null)
            {
            if(!((string.valueof(val.get('Theme_list__c'))).contains('ProdSec Triaged'))){
                Action_item='Please Triage;';
            }}
            else 
                Action_item='Please Triage;';
            
            String Priority =string.valueof(val.Priority__c);
            switch on Priority{
                
                When 'P0'{
                    Action_item = Action_item + 'P0 Alert;';
                    
                    if(val.Age__c >= (7/2) )
                    Action_item = Action_item + 'Bug is Over Half Life;'; 
                    
                }
                
                When 'P1'{
                     if(val.Age__c >= (30/2) )
                         Action_item = Action_item + 'Bug is Over Half Life;'; 
                    
                }
                When 'P2'{
                     if(val.Age__c >= (90/2) )
                         Action_item = Action_item + 'Bug is Over Half Life;'; 
                    
                }  }
            system.debug(Action_item);
            val.action_item__c = Action_item;
            action_insert.add(val);
            
        }
        system.debug(action_insert);
        upsert action_insert Name;
        **/
       

        
}
    
    public static void theme_work_r(list<string> query_list){
        
        system.debug(query_list);
        string Theme_ids = null;
        list<string> themes;
        //string query = 'SELECT Theme_c__r.Name__c, Work_c__r.Name__c  FROM ADM_Theme_Assignment_c__x LIMIT 10';
        //
        system.debug('before loop');
        string query = 'SELECT Theme_c__r.Name__c, Work_c__r.Name__c  FROM ADM_Theme_Assignment_c__x WHERE Work_c__c IN :query_list';
        
        //system.debug(query);
        for(ADM_Theme_Assignment_c__x z:database.query(query))
        { 
            system.debug('inside loop');
           if(theme_work.containsKey(z.Work_c__r.Name__c ))
           		themes = theme_work.get(z.Work_c__r.Name__c );
           else
                themes = new list<string>();
           themes.add(string.valueof(z.Theme_c__r.Name__c));
           theme_work.put(string.valueof(z.Work_c__r.Name__c),themes);
        }
    }
    
    
}
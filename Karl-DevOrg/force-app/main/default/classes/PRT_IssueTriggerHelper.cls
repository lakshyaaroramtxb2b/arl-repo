public with sharing class PRT_IssueTriggerHelper {
    // This method updates the Slippage Comment of Project associated to Risk
    public static void updateSlippageCommentOfProject(List<Issues__c> newList, Map<Id, Issues__c> oldMap) {
        List<Project__c> updateProjectList = new List<Project__c>();
        Map<Id, List<String>> projIdToOldDescListMap = new Map<Id, List<String>>();
        Map<Id, List<String>> projIdToDescListMap = new Map<Id, List<String>>();
        Map<Id, Id> issueIdToProjIdMap = new Map<Id, Id>();
        Set<Id> projectIdSet = new Set<Id>();
        for(Issues__c issue : newList) {
            if(issue.Project__c != null) {
                if(!issueIdToProjIdMap.values().contains(issue.Project__c)) {
                    issueIdToProjIdMap.put(issue.Id, issue.Project__c);
                }
                if((oldMap==null && issue.Status__c != 'Closed') || (oldMap!=null && issue.Status__c != 'Closed' && (oldMap.get(issue.Id).Description__c != issue.Description__c || oldMap.get(issue.Id).Status__c == 'Closed'))) {
                    if(!projIdToDescListMap.containsKey(issue.Project__c)) {
                        projIdToDescListMap.put(issue.Project__c, new List<String>());
                    }
                    projIdToDescListMap.get(issue.Project__c).add(issue.Description__c);
                }
                if(oldMap != null) {
                    if(oldMap.get(issue.Id).Description__c != issue.Description__c || (oldMap.get(issue.Id).Status__c != issue.Status__c && issue.Status__c == 'Closed')) {
                        if(!projIdToOldDescListMap.containsKey(issue.Project__c)) {
                            projIdToOldDescListMap.put(issue.Project__c, new List<String>());
                        }
                        projIdToOldDescListMap.get(issue.Project__c).add(oldMap.get(issue.Id).Description__c);
                    }
                }
            }
        }
        System.debug('projIdToOldDescListMap: '+projIdToOldDescListMap);
        System.debug('projIdToDescListMap: '+projIdToDescListMap);

        List<Project__c> projectList = new List<Project__c>([SELECT Id, Slippage_Comments__c FROM Project__c 
                                                             WHERE Id IN: issueIdToProjIdMap.values()]);
        System.debug('project: '+projectList);
        if(projectList!=null && !projectList.isEmpty()) {
            for(Project__c project : projectList) {
                String finalDescription = '';
                Project__c updateProject = new Project__c();
                updateProject.Id = project.Id;
                finalDescription = project.Slippage_Comments__c == null ? '' : project.Slippage_Comments__c;

                System.debug('Actual Desc: '+finalDescription);
                
                if(oldMap!=null && projIdToOldDescListMap!=null && !projIdToOldDescListMap.isEmpty()) {
                    for(String oldDesc : projIdToOldDescListMap.get(project.Id)) {
                        if(oldDesc != null && finalDescription != '') {
                            String removeDesc = oldDesc.replaceAll('<[/a-zAZ0-9]*>','');
                            finalDescription = String.valueOf(finalDescription).remove('- '+removeDesc);
                        }
                    }
                }  
                System.debug('After Deleted Desc: '+finalDescription);
                if(projIdToDescListMap!=null && !projIdToDescListMap.isEmpty()) {
                    for(String description : projIdToDescListMap.get(project.Id)) {
                        if(description != null) {
                            finalDescription += '- '+String.valueOf(description)+' ';   
                        }
                    }
                }     

                finalDescription = finalDescription.replaceAll('<[/a-zAZ0-9]*>','');
                System.debug('Updated Desc: '+finalDescription);

                updateProject.Slippage_Comments__c = finalDescription;
                updateProjectList.add(updateProject);
            }
        }
        
        if(updateProjectList != null && !updateProjectList.isEmpty()) {
            UPDATE updateProjectList;
        }
    }
    
    public static void updateBecameIssueFromRisk(List<Issues__c> newList, Map<Id, Issues__c> oldMap) {
        for(Issues__c issue : newList) {
            if(issue.Risk__c != null) {
                issue.Became_Issue_from_Risk__c = true;
            }
            else {
                issue.Became_Issue_from_Risk__c = false;
            }
        }
    }
}
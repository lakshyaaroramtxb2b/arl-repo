@isTest
public class SPT_CaseCommentInitialBatch_Test {
    @TestSetup
    static void makeData(){
        Case__x externalCaseRecord = new Case__x();
        externalCaseRecord.RecordTypeId__c = SPT_Constants.SF_RECORDTYPE_ID;

        List<Case> internalExternalCaseList = SPT_TestUtility.createCaseRecords('New', 'USD', 'Web', 1, false);
        for(Case caseRecord : internalExternalCaseList) {
            caseRecord.RecordTypeId = SPT_Constants.SEC_RECORDTYPE_ID;
        }
        INSERT internalExternalCaseList;      
    }

    @isTest
    static void checkBatchUpdate() {
        Test.startTest();
        SPT_CaseCommentInitialBatch batch = new SPT_CaseCommentInitialBatch();
            Database.executeBatch(batch);
        Test.stopTest();
    }
}
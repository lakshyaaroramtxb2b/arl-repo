//Name of class is important. Use the ID in the URI for the DAC e.g. a2N3A0000004fgS_DACProcessor
//Note if DAC uses custom logic path e.g. DACProcessor  then Suppression / AIQ checks are skipped as you now own the logic
public class DACIDHERE_DACProcessor extends DACPostProcessor {
	
    public void Start(Detection_Alert__c da)
    {
        //Do stuff here API calls etc
        //
        
        //If Enrich event needed
        //super.EnrichEvent(Detection_Security_Event__c evt, String json)
        //
        
        //ProcessDAC(Boolean suppress, Detection_Alert__c da, String auditMessage)
        super.ProcessDAC(false, da, 'status message here');
    }  
}
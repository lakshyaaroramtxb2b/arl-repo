/* utility class to facility deleting large number of records
   by Jorge 
*/

public class DeleteBulkRecords implements Database.Batchable<sObject>{
    private String query;

    public DeleteBulkRecords(String query) {
        if (query == 'CaseComment') this.query = 'SELECT Id FROM CaseComment WHERE Parent.RecordTypeId = \'0123A000000Ac6AQAS\'';
        if (query == 'Case') this.query = 'SELECT Id FROM Case WHERE RecordTypeId = \'0123A000000Ac6AQAS\'';
    }

    public Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }

    public void execute(Database.BatchableContext BC, List<sObject> scope){
        delete scope;
    }

    public void finish(Database.BatchableContext BC){
        Messaging.SingleEmailMessage mail= new Messaging.SingleEmailMessage();
        mail.setToAddresses(new String[] {UserInfo.getUserEmail()});
        mail.setSubject('Bulk Delete Finished');
        mail.setPlainTextBody(this.query);
        Messaging.sendEmail(new Messaging.Email[] {mail});                
    }

    public static void run(String query) {        
    	Database.executeBatch(new DeleteBulkRecords(query));
    }

}
@isTest
class SC_BulkUnenrollControllerTest {
    /*static Id EMPLOYEE_RT {
        get {
            if (null == EMPLOYEE_RT) {
                EMPLOYEE_RT = [SELECT Id FROM RecordType WHERE Name = 'Salesforce Employee' AND SobjectType = 'Contact'].Id;
            }
            return EMPLOYEE_RT;
        }
        set;
    }

    @isTest static void testCtor() {
        System.runAs(TEST_TM_Util.generateUser()) {
            ApexPages.StandardController sc = new ApexPages.StandardController(new Training_Course__c());
            Test.startTest();
                SC_BulkUnenrollController c = new SC_BulkUnenrollController(sc);
                System.assertNotEquals(null, c.course);
            Test.stopTest();
        }
    }

    @isTest static void testGetUniqueEmails() {
        System.runAs(TEST_TM_Util.generateUser()) {
            Test.startTest();
                System.assertEquals(new Set<String>{'e'}, SC_BulkUnenrollController.getUniqueEmails('e'));
                System.assertEquals(new Set<String>{'e'}, SC_BulkUnenrollController.getUniqueEmails(' E\t'));
                System.assertEquals(new Set<String>{'e f'}, SC_BulkUnenrollController.getUniqueEmails('e F'));
                System.assertEquals(new Set<String>{'e','f'}, SC_BulkUnenrollController.getUniqueEmails('e \n\t F '));
                System.assertEquals(new Set<String>{'e','f'}, SC_BulkUnenrollController.getUniqueEmails('\n\ne \n\n\t F '));
            Test.stopTest();
        }
    }

    @isTest static void testGetMissingEmails() {
        System.runAs(TEST_TM_Util.generateUser()) {
            Set<String> uniqueEmails = new Set<String>();
            Training_Course_Taken__c[] taken = new Training_Course_Taken__c[]{};
            Test.startTest();
                System.assertEquals(new Set<String>(), SC_BulkUnenrollController.getMissingEmails(uniqueEmails, taken));

                uniqueEmails.add('e');
                uniqueEmails.add('f');
                System.assertEquals(new Set<String>{'e','f'}, SC_BulkUnenrollController.getMissingEmails(uniqueEmails, taken));

                taken.add(new Training_Course_Taken__c(Contact__r = new Contact(Email = 'E')));
                System.assertEquals(new Set<String>{'f'}, SC_BulkUnenrollController.getMissingEmails(uniqueEmails, taken));
                taken.add(new Training_Course_Taken__c(Contact__r = new Contact(Email = null)));
                System.assertEquals(new Set<String>{'f'}, SC_BulkUnenrollController.getMissingEmails(uniqueEmails, taken));
                taken.add(new Training_Course_Taken__c(Contact__r = new Contact(Email = 'f')));
                System.assertEquals(new Set<String>(), SC_BulkUnenrollController.getMissingEmails(uniqueEmails, taken));
                taken.add(new Training_Course_Taken__c(Contact__r = new Contact(Email = 'G')));
                System.assertEquals(new Set<String>(), SC_BulkUnenrollController.getMissingEmails(uniqueEmails, taken));

                uniqueEmails.add(null);
                System.assertEquals(new Set<String>(), SC_BulkUnenrollController.getMissingEmails(uniqueEmails, taken));
            Test.stopTest();
        }
    }

    @isTest static void testProcessEmails() {
        System.runAs(TEST_TM_Util.generateUser()) {
            Contact contact = generateContact(0);
            insert contact;
            SC_Training_Provider__c provider = createProvider();
            Training_Course__c t = SC_TrainingTestUtil.createCourseForProvider(provider);

            ApexPages.StandardController sc = new ApexPages.StandardController(t);
            SC_BulkUnenrollController c = new SC_BulkUnenrollController(sc);

            Test.startTest();
                c.emailText = 'a \n e@ccctest.com \n ';
                c.processEmails();
                System.assertEquals(2, ApexPages.getMessages().size());
                System.assertEquals(true, ApexPages.hasMessages(ApexPages.Severity.ERROR));
                System.assertEquals(false, ApexPages.hasMessages(ApexPages.Severity.INFO));

                Training_Course_Taken__c tt = SC_TrainingTestUtil.createCourseTakenForCourse(contact.Id, t);
                c.processEmails();
                System.assertEquals(2, ApexPages.getMessages().size());
                System.assertEquals(true, ApexPages.hasMessages(ApexPages.Severity.ERROR));
                System.assertEquals(false, ApexPages.hasMessages(ApexPages.Severity.INFO));

                contact.Email = 'e@ccctest.com'.toUpperCase();
                update contact;
                c.processEmails();
                System.assertEquals(3, ApexPages.getMessages().size());
                System.assertEquals(true, ApexPages.hasMessages(ApexPages.Severity.ERROR));
                System.assertEquals(true, ApexPages.hasMessages(ApexPages.Severity.INFO));
                System.assertEquals(contact.Id, [SELECT WhoId FROM Task][0].WhoId);
            Test.stopTest();
        }
    }

    @isTest static void testBigProcessEmails() {
        final Integer N = 100; // should handle 1k+
        String[] allEmails = new String[]{};
        System.runAs(TEST_TM_Util.generateUser()) {
            Contact[] contacts = new Contact[]{};
            for (Integer i = 0; i < N; ++i) {
                contacts.add(generateContact(i));
                allEmails.add(contacts[contacts.size() - 1].Email);
            }
            insert contacts;
            SC_Training_Provider__c provider = createProvider();
            Training_Course__c t = SC_TrainingTestUtil.createCourseForProvider(provider);

            ApexPages.StandardController sc = new ApexPages.StandardController(t);
            SC_BulkUnenrollController c = new SC_BulkUnenrollController(sc);

            Test.startTest();
                c.emailText = String.join(allEmails, '\n');
                c.processEmails();
                System.assertEquals(N, ApexPages.getMessages().size());
                System.assertEquals(true, ApexPages.hasMessages(ApexPages.Severity.ERROR));
            Test.stopTest();
        }
    }

    @isTest static void testProcessEmailsByTrainingAdmin() {
        User u = TEST_TM_Util.generateUser(TEST_TM_Util.TM_TMM_EXTERNAL_PROFILE_ID);
        insert u;
        insert new PermissionSetAssignment(AssigneeId = u.Id,
            PermissionSetId = [SELECT Id FROM PermissionSet WHERE Name = 'SC_Training_Admin'].Id);
        System.runAs(u) {
            Contact contact = generateContact(0);
            insert contact;
            SC_Training_Provider__c provider = createProvider();
            Training_Course__c t = SC_TrainingTestUtil.createCourseForProvider(provider);

            Test.setCurrentPage(Page.SC_BulkUnenroll);
            ApexPages.StandardController sc = new ApexPages.StandardController(t);
            SC_BulkUnenrollController c = new SC_BulkUnenrollController(sc);

            Test.startTest();
                c.emailText = 'a \n e@ccctest.com \n ';
                c.processEmails();
                System.assertEquals(2, ApexPages.getMessages().size());
                System.assertEquals(true, ApexPages.hasMessages(ApexPages.Severity.ERROR));
                System.assertEquals(false, ApexPages.hasMessages(ApexPages.Severity.INFO));
            Test.stopTest();
        }
    }

    static SC_Training_Provider__c createProvider() {
        SC_Training_Provider__c p = new SC_Training_Provider__c(
            Name = 'Trailhead',
            Type__c = 'Trailhead'
        );
        insert p;
        return p;
    }

    static Contact generateContact(Integer i) {
        Contact c = new Contact(
            LastName = 'c' + i,
            Org62_User_Id__c = TEST_TM_Util.generateRandomString(18),
            RecordTypeId = EMPLOYEE_RT,
            Email = 'e' + i + '@ccctest.com'
        );
        return c;
    }*/
}
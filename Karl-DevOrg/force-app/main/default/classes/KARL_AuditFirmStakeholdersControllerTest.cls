/** 
* @author Lakshya Arora
* @email lakshya.arora@mtxb2b.com
* @description This class will test the functionality of KARL_AuditFirmStakeholdersController
*/
@isTest
public class KARL_AuditFirmStakeholdersControllerTest {
    /** 
* @author Lakshya Arora
* @email lakshya.arora@mtxb2b.com
* @description Method to test the functionality of getAuditFirmStakeholders
*/
    @isTest
    private static void getAuditFirmStakeholdersTest(){
		User opsAdminUser = ARL_TestDataFactory.createOpsAdmin();
        System.runAs(opsAdminUser){
            Map<Integer,String> priorityToScopeMap = new Map<Integer,String>();
            priorityToScopeMap.put(0,'AS');
            priorityToScopeMap.put(1,'AUST');
            priorityToScopeMap.put(2,'B2BC');
            priorityToScopeMap.put(3,'CHAT');
            
            Account acc = new Account();
            acc.Name = 'salesforce.com - ESA Office Hours';
            insert acc;
            
            Contact stakeHolder = new Contact(AccountId = acc.Id,FirstName = 'Test ',LastName = 'Contact'+String.valueOf(MATH.random()), Email = 'test@contact.com.invalid',
                                             RecordTypeId = ARL_TestDataFactory.CONTACT_CUSTOMER_RECORDTYPEID);
            insert stakeHolder;
            
            Audit_Cycle__c auditCycle = ARL_TestDataFactory.createAuditCycle();
            insert auditCycle;
            KARL_Audit_Firm__c auditFirm = ARL_TestDataFactory.createAuditFirm('testAuditFirm');
            insert auditFirm;
            
            KARL_Audit_Firm_Stakeholders__c auditFirmStakeholder = ARL_TestDataFactory.createAuditFirmStakeholders(auditFirm.Id, stakeholder.Id);
            insert auditFirmStakeholder;
            
            // Create two distinct audit teams
            KARL_Audit_Team__c auditTeam = ARL_TestDataFactory.createAuditTeam('testAuditTeam1');
            auditTeam.KARL_Audit_Firm__c = auditFirm.Id;
            insert auditTeam;
            
            List<KARL_Audit_Scope_Master_Data__c> auditScopeList = new List<KARL_Audit_Scope_Master_Data__c>();
            for(Integer i=0;i<4;i++){
                KARL_Audit_Scope_Master_Data__c auditScope = ARL_TestDataFactory.createAuditScope();
                auditScope.KARL_Scope__c = priorityToScopeMap.get(i);
                auditScopeList.add(auditScope);
            }
            insert auditScopeList;
            
            List<KARL_Audit_Scope_Reports__c> auditScopeReportList = new List<KARL_Audit_Scope_Reports__c>();
            for(Integer i=0;i<4;i++){
                KARL_Audit_Scope_Reports__c auditScopeReport = ARL_TestDataFactory.createAuditScopeReport(auditScopeList[i].Id,auditFirm.Id);
                auditScopeReportList.add(auditScopeReport);
            }
            insert auditScopeReportList;
            
            List<KARL_Cycle_Audit_Report_Items__c> cycleAuditScopeReportList = new List<KARL_Cycle_Audit_Report_Items__c>();
            for(Integer i=0;i<4;i++){
                KARL_Cycle_Audit_Report_Items__c  cycleAuditReportItemObj = ARL_TestDataFactory.createCycleAuditScopeReport(auditScopeReportList[i].Id, auditCycle.Id);
                cycleAuditReportItemObj.Status__c = KARL_Constants.REPORT_STATUS_PENDING;
                cycleAuditScopeReportList.add(cycleAuditReportItemObj);
            }
            cycleAuditScopeReportList[0].Status__c = KARL_Constants.REPORT_STATUS_READY_TO_SIGN;
            insert cycleAuditScopeReportList;
            
            Test.startTest();
            List<KARL_Audit_Firm_Stakeholders__c> auditFirmStakehoderList= KARL_AuditFirmStakeholdersController.getAuditFirmStakeholders(cycleAuditScopeReportList[0].Id);
            System.assert(!auditFirmStakehoderList.isEmpty());
            Test.stopTest();
        }        
    }
}
@isTest
public class IEM_CaseListControllerTest {
    @testSetup
    public static void testSetup(){
        User admin = IEM_TestDataFactory.createUser(); 
        
        User portalUser = new User();        
        System.runAs(admin){
            //Create Account   
            Account a = new Account(Name='Test Account Name');
            insert a; 
            //Create Contact
            Contact c = new Contact(LastName = 'Contact Last Name', AccountId = a.id);
            insert c;
            //Create poratl User
            portalUser = IEM_TestDataFactory.createPortalUser(c);
            insert portalUser;    
            
            //PermissionSet ps = [SELECT Id FROM PermissionSet WHERE Name = 'GRC_IEM_Case_Read'];
			//insert new PermissionSetAssignment(AssigneeId = portalUser.id, PermissionSetId = ps.Id);
        }
        
    }
    @isTest
    static void getCase() {
        user portalUser = [SELECT id from User where Username = 'testPortalHVCPUser@domain.com'];
        
        test.startTest();
        System.runAs(portalUser){
            List<Case> caseList = IEM_TestDataFactory.createCaseRecords(5,false);
            insert CaseList; 
            caseList = IEM_CaseListController.getCases();
            system.assert(caseList!=null);
        }
        test.stopTest();
    }
    @isTest
    static void getPicklist(){
        String objectAPI = 'Case';
        String field = 'Action_Plan_Status__c';
       	List<String> PicklistValues = IEM_CaseListController.getPicklistValues(objectAPI,field);
            system.assert(PicklistValues!=null);
    }
    @isTest
    static void getCaseMilestonesTest() {
		user portalUser = [SELECT id,contactID FROM User WHERE Username = 'testPortalHVCPUser@domain.com' Limit 1];
        
        //Create Accounts
        List<Account> accounts = new List<Account>();
        accounts= IEM_TestDataFactory.createAccounts(1,true);
        //Create Contacts 
        List<Contact> contacts = new List<Contact>();
        contacts= IEM_TestDataFactory.createContacts(2,accounts[0].id,false);
        Contacts[0].LastName = 'Issue_Owner__c';
        Contacts[1].LastName = 'Action_Plan_Owner__c';
        insert contacts;
         
        //Create Case    
        List<Case> caseList = IEM_TestDataFactory.createCaseRecords(1,false);
        caseList[0].Action_Plan_Owner__c = contacts[0].id;
        caseList[0].Requestor__c	=portalUser.contactId;
        insert CaseList;
        
        test.startTest();
        caseList[0].Issue_Owner__c = contacts[1].id;
        System.runAs(portalUser) { 
            IEM_CaseListController.saveCase(caseList[0]);
        	
            IEM_CaseListController.CaseWrapper caseWrapperRec = IEM_CaseListController.getCaseData(CaseList[0].id, true);
            system.assert(CaseWrapperRec !=null);
            system.assert(CaseWrapperRec.caseRecord !=null);
            system.assert(CaseWrapperRec.milestoneList !=null);
        }
        test.stopTest();
    }
    
    static testMethod void createMilestoneTest() {
		user portalUser = [SELECT id,contactID FROM User WHERE Username = 'testPortalHVCPUser@domain.com' Limit 1];
        
        //Create Accounts
        List<Account> accounts = new List<Account>();
        accounts= IEM_TestDataFactory.createAccounts(1,true);
        //Create Contacts
        List<Contact> contacts = new List<Contact>();
        contacts= IEM_TestDataFactory.createContacts(2,accounts[0].id,false);
        Contacts[0].LastName = 'Issue_Owner__c';
        Contacts[1].LastName = 'Action_Plan_Owner__c';
        insert contacts;
         
        //Create Case    
        List<Case> caseList = IEM_TestDataFactory.createCaseRecords(1,false);
        caseList[0].Action_Plan_Owner__c = contacts[0].id;
        caseList[0].Requestor__c	=portalUser.contactId;
        insert CaseList;
        
        test.startTest();
        caseList[0].Issue_Owner__c = contacts[1].id;
        
        System.runAs(portalUser) { 
            IEM_CaseListController.saveCase(caseList[0]);
        	List<Milestone__c> newMilestoneList = IEM_TestDataFactory.createMilestoneRecords(1,caseList[0].id,False);
            IEM_CaseListController.CaseWrapper caseWrapperRec  = IEM_CaseListController.createMilestone(newMilestoneList[0]);
            system.assert(CaseWrapperRec !=null);
            system.assert(CaseWrapperRec.caseRecord !=null);
            system.assert(CaseWrapperRec.milestoneList !=null);
        }
        test.stopTest();
    }
    
    static testMethod void approvalsTest() {
		user portalUser = [SELECT id,contactID FROM User WHERE Username = 'testPortalHVCPUser@domain.com' Limit 1];
        
        //Create Accounts
        List<Account> accounts = new List<Account>();
        accounts= IEM_TestDataFactory.createAccounts(1,true);
        //Create Contacts
        List<Contact> contacts = new List<Contact>();
        contacts= IEM_TestDataFactory.createContacts(2,accounts[0].id,false);
        Contacts[0].LastName = 'Issue_Owner__c';
        Contacts[1].LastName = 'Action_Plan_Owner__c';
        insert contacts;
         
        //Create Case    
        List<Case> caseList = IEM_TestDataFactory.createCaseRecords(1,false);
        caseList[0].Action_Plan_Owner__c = contacts[0].id;
        caseList[0].Requestor__c	=portalUser.contactId;
        insert CaseList;
        
        test.startTest();
        System.runAs(portalUser) { 
        caseList[0].Issue_Owner__c = contacts[1].id;
            IEM_CaseListController.saveCase(caseList[0]);
        }
        caseList[0].Security_Reviewer__c = portalUser.id;
        caseList[0].Status = IEM_Constants.IEM_CASE_STATUS_SECURITY_DECISION;
        caseList[0].Residual_Severity__c = 'Very High';
        update caseList;
        
        System.runAs(portalUser) { 
        	List<Milestone__c> newMilestoneList = IEM_TestDataFactory.createMilestoneRecords(1,caseList[0].id,False);
            Boolean access = IEM_CaseListController.getApprovalProcessAccess(caseList[0].id);
            System.assert(access == true);
            access = IEM_CaseListController.approveRejectRecord(caseList[0].id,'Reject', 'Updated');
            System.assert(access == true);
        }
        test.stopTest();
    }
    
}
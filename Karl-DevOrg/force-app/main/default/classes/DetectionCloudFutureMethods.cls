global class DetectionCloudFutureMethods {
    
    @future(callout=true)
    public static void updateRecord(String tagName, Id tagId, String tagVal) {
        
        try {
            
            Detection_Log_Tagging_Value__c dltv = [SELECT Id, TagName__c, Detection_Log_Tagging__c, tagValue__c
                    FROM Detection_Log_Tagging_Value__c 
                    WHERE TagName__c =: tagName AND Detection_Log_Tagging__c =: tagId LIMIT 1];
                            
            dltv.tagValue__c = tagVal;
            
            update dltv;
        }    
        catch(Exception e) {
            
        	FeedItem post = new FeedItem();
        	post.ParentId = 'a353A000000HDNv';
       		post.Body = 'DetectionCloudFutureMethods ' + e.getMessage();
       		insert post;       
       } 
    }
    
    @future(callout=true)
    public static void cacheRecordUpdate(String tagName, Id tagId, String tagVal) {
        
        cachedTagUpdate__c cachedRec = new cachedTagUpdate__c();
        
        cachedRec.TagName__c = tagName;
        cachedRec.LogTaggingRecordId__c = tagId;
        cachedRec.tagValue__c = tagVal;
        
        insert cachedRec;
    }
}
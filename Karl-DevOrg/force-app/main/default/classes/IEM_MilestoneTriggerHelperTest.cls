@isTest
public class IEM_MilestoneTriggerHelperTest {
    @isTest
    static void updateMilestoneContactsTest() {
        Map<String,Map<String,String>> dependenciesMap = new Map<String,Map<String,String>>();
        Profile profile = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User userRec = new User(Alias = 'standt', Email='standarduser@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = profile.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='standarduser994@testorg.com');
        insert userRec;
        
        List<Account> accounts = new List<Account>();
        accounts= IEM_TestDataFactory.createAccounts(1,true);
        List<Contact> contacts = new List<Contact>();
        contacts= IEM_TestDataFactory.createContacts(2,accounts[0].id,true);
        
        test.startTest();
        System.runAs(userRec) {   
            List<Case> caseList = IEM_TestDataFactory.createCaseRecords(1,false);
            caseList[0].Action_Plan_Owner__c = contacts[0].id;
            caseList[0].Issue_Owner__c = contacts[1].id;
            insert CaseList;
            List<Milestone__c> milestoneList = IEM_TestDataFactory.createMilestoneRecords(5,caseList[0].id,true);
            for(Milestone__c milestoneRec : [SELECT id,Case_Action_Plan_Owner__c,Case_Issue_Owner__c 
                                             FROM Milestone__c 
                                             WHERE id IN:milestoneList]){
                system.assertEquals(milestoneRec.Case_Action_Plan_Owner__c,caseList[0].Action_Plan_Owner__c);
                system.assertEquals(milestoneRec.Case_Issue_Owner__c,caseList[0].Issue_Owner__c);
            }
            
        }
        test.stopTest();
    }
    /*@isTest
    static void updateGusWorkStatus() {
        Map<String,Map<String,String>> dependenciesMap = new Map<String,Map<String,String>>();
        Profile profile = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User userRec = new User(Alias = 'standt', Email='standarduser@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = profile.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='standarduser994@testorg.com');
        insert userRec;
        
        List<Account> accounts = new List<Account>();
        accounts= IEM_TestDataFactory.createAccounts(1,true);
        List<Contact> contacts = new List<Contact>();
        contacts= IEM_TestDataFactory.createContacts(2,accounts[0].id,true);
        
        test.startTest();
        System.runAs(userRec) {   
            List<Case> caseList = IEM_TestDataFactory.createCaseRecords(1,false);
            caseList[0].Action_Plan_Owner__c = contacts[0].id;
            caseList[0].Issue_Owner__c = contacts[1].id;
            caseList[0].Status = 'New';
            insert CaseList;
            
            List<Milestone__c> milestoneList = IEM_TestDataFactory.createMilestoneRecords(1,caseList[0].id,true);
            ADM_Work_c__x mockedGusWork = new ADM_Work_c__x(
                Feature_ID_c__c=milestoneList[0].Id,
                Subject_c__c='Test',
                ExternalId= 'EX101'
            );
            IEM_MilestoneTriggerHelper.mockedRequests.add(mockedGusWork);
            milestoneList[0].Status__c = IEM_Constants.MILESTONE_STATUS_IN_PROGRESS;
            update milestoneList[0];
            
        }
        test.stopTest();
    }
    */
}
/** 
 * @author Ben Harvie
 * @email ben.harvie@salesforce.com
 * @description This class provides the methods for the Audit Issues Tracker Lightning Web Components.
 * * Relevant Objects:
 * * KARL_Audit_Cycle__c
 * * KARL_Audit_Scope_In_Report__c
 * * KARL_Cycle_Audit_Report_Items__c
 */
public with sharing class KARL_AuditIssuesLwcController {
    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Get the KARL Audit Issues associated with the record
     * @param recordId - the record Id that the call comes from
     * @param objectApiName - the Object that the call comes from (for context awareness)
     * @return object including all Audit Issues
     */
    @AuraEnabled(cacheable=true)
    public static KARL_Issue_Tracker__c[] getScopeAuditIssues(String recordId, String objectApiName){
        // ! Note - object awareness not currently used
        // Collect the Cycle Audit Report record metadata
        List<KARL_Cycle_Audit_Report_Items__c> cycleReport = getParentCycleReport(recordId);
        List<KARL_Audit_Scope_Reports__c> subScopes = getSubScopes(cycleReport[0].Audit_Reports_Master_Data__c);
        Set<String> scopesList = new Set<String>();
        scopesList.add(cycleReport[0].Scope__c);
        for(Integer i = 0; i < subScopes.size(); i++){
            for(KARL_Audit_Scope_In_Report__c subReport : subScopes[i].KARL_Audit_Scope_Report__r){
                scopesList.add(subReport.KARL_Parent_Audit_Scope__r.KARL_Scope__c);
            }
        }
        System.debug(LoggingLevel.DEBUG, 'All Scopes >> ' + scopesList);
        // Return the associated Audit Issues
        return [SELECT Id, Name, Issue_Status__c, KARL_KAI_Issue_Title__c, KARL_Approved_by_Orchestration_Lead__c,
                KARL_Approved_by_SCCS__c, KARL_Approved_by_GRC_Executive__c, KARL_Control_ID__c, KARL_High_Impact__c,
                KARL_KAI_IEM_Case__c
                FROM KARL_Issue_Tracker__c 
                WHERE KARL_Audit_Cycle__c =: cycleReport[0].Audit_Cycle__c
                AND KARL_KAI_Impacted_Scope__c IN: scopesList
                AND KARL_Impacted_Frameworks__c includes(:cycleReport[0].Area_of_Compliance__c)
                WITH SECURITY_ENFORCED
                ORDER BY KARL_Control_ID__c];
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Get the Cycle Audit Report record
     * @param recordId - the record Id of the object to lookup
     * @return Cycle Audit Report object with selected fields
     */
    private static KARL_Cycle_Audit_Report_Items__c[] getParentCycleReport(String recordId){
        System.debug(LoggingLevel.DEBUG, 'Collecting parent Cycle Audit Report metadata >> ' + recordId);
        return [SELECT Audit_Cycle__c,Area_of_Compliance__c,Scope__c,Audit_Reports_Master_Data__c 
                FROM KARL_Cycle_Audit_Report_Items__c 
                WHERE Id =: recordId
                WITH SECURITY_ENFORCED];
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Get the Cycle Audit Report record
     * @param recordId - the record Id of the object to lookup
     * @return Cycle Audit Report object with selected fields
     */
    private static KARL_Audit_Scope_Reports__c[] getSubScopes(String recordId){
        System.debug(LoggingLevel.DEBUG, 'Collecting SubScopes from Audit Scope Report >> ' + recordId);
        return [SELECT Id, (SELECT KARL_Parent_Audit_Scope__r.KARL_Scope__c FROM KARL_Audit_Scope_Report__r)
                FROM KARL_Audit_Scope_Reports__c 
                WHERE Id =: recordId
                WITH SECURITY_ENFORCED];
    }
}
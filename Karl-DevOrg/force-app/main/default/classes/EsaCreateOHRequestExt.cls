public class EsaCreateOHRequestExt {

  public String serviceItem {get; set;}
  public Case__x casex {get; set;}

  public EsaCreateOHRequestExt(ApexPages.StandardController stdController) {
    casex = (Case__x)stdController.getRecord();
  }
  
  public List<SelectOption> getServiceItems() {
    List<ESA_Service_Item__c > esaSItems = [SELECT Id,Name
                                           FROM ESA_Service_Item__c
                                           WHERE ESA_Entity__c = 'EntSec'
                                           AND IsInActive__c = false];    
                                           
    List<SelectOption> lstServiceItems = new List<SelectOption>();
    for(ESA_Service_Item__c eSItem : esaSItems) {
      lstServiceItems.add(new SelectOption(eSItem.id,eSItem.Name));
    }
    
    return lstServiceItems; 
  }
  
  public PageReference createOHRequest() {
    User usr = [SELECT Id,Email,Name,Phone FROM User WHERE Email=:casex.ContactEmail__c];
    Esa_Security_Request__c securityRequest = new Esa_Security_Request__c();
    securityRequest.Email__c = userInfo.getUserEmail();
    securityRequest.Name__c = userInfo.getName();
    securityRequest.Requestor__c = userInfo.getUserId();
    User requestor = [SELECT Id,Phone FROM User WHERE Id =: Userinfo.getUserId()];
    securityRequest.Phone__c = requestor.Phone;
    
    return null;
  }
  
  private String makeHashKey(ESA_Security_Request__c securityRequest) {
        String key = String.valueOf(Math.Round(Math.random()*100000)) + String.valueOf(userInfo.getUserId().substring(5)) ;
        key += string.valueOf(securityRequest.Email__c.hashCode());
        key += string.valueOf(securityRequest.Name__c.trim().hashCode());
        key += string.valueOf(string.valueOfGMT(datetime.now()).hashCode());
        key = key.replace('-', '');
        return key.trim();
    }
  

}
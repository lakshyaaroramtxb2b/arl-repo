/**
 * @author Mahima Aggarwal
 * @email mahima.aggarwal@mtxb2b.com
 * @desc Test class for KARL_Utility
*/
@isTest
public with sharing class KARL_UtilityTest {

    /**
    * @author Ben Harvie
    * @email ben.harvie@salesforce.com
    * @description Test data setup.
    */
	@testSetup
    private static void testSetup(){
       User opsAdminUser = ARL_TestDataFactory.createOpsAdmin();
        System.runAs(opsAdminUser){
            
            Account acc = new Account(Name = 'salesforce.com - ESA Office Hours');
            insert acc;
            
            Contact con = new Contact(LastName = 'test', AccountId = acc.Id,Email = 'test@gmail.com');
            insert con;
            
            Contact con2 = new Contact(LastName = 'test2', AccountId = acc.Id,Email = 'test2@gmail.com');
            insert con2;
       
            
            /**
             * Audit Firm and Team Setup
             */
            KARL_Audit_Firm__c auditFirm = ARL_TestDataFactory.createAuditFirm('testAuditFirm');
            insert auditFirm;
            
            // Create two distinct audit teams
            KARL_Audit_Team__c auditTeam1 = ARL_TestDataFactory.createAuditTeam('testAuditTeam1');
            auditTeam1.KARL_Audit_Firm__c = auditFirm.Id;
            insert auditTeam1;
            
            KARL_Audit_Team__c auditTeam2 = ARL_TestDataFactory.createAuditTeam('testAuditTeam2');
            auditTeam2.KARL_Audit_Firm__c = auditFirm.Id;
            insert auditTeam2;

            /**
             * Audit Cycle Setup
             */
            // Create shell audit cycle
            Audit_Cycle__c auditCycle = ARL_TestDataFactory.createAuditCycle();
            insert auditCycle;
            
            KARL_Gus_Product_Tag__c defaultProductTag = ARL_TestDataFactory.createKarlGusProductTag(true,true);
            insert defaultProductTag;
            
            List<KARL_Auto_Assignment_Configuration__c> autoAssignmentConfigList = ARL_TestDataFactory.createAutoAssignmentConfig(defaultProductTag.Id,4);
            autoAssignmentConfigList[1].Priority_0_Assignment_Date__c = System.today().addDays(5);
            autoAssignmentConfigList[2].Priority_0_Assignment_Date__c = System.today().addDays(-1);
            autoAssignmentConfigList[3].Priority_0_Assignment_Date__c = System.today();
            insert autoAssignmentConfigList;
            
            List<Audit_Cycle_Coverage__c> auditCycleCoverageList = new List<Audit_Cycle_Coverage__c>();
            Map<Integer,String> priorityToScopeMap = new Map<Integer,String>();
            priorityToScopeMap.put(0,'AS');
            priorityToScopeMap.put(1,'AUST');
            priorityToScopeMap.put(2,'B2BC');
            priorityToScopeMap.put(3,'CHAT');
            for(Integer i=0;i<4;i++){
                Audit_Cycle_Coverage__c auditCycleCoverage = new Audit_Cycle_Coverage__c();
                auditCycleCoverage.Audit_Cycle__c = auditCycle.Id;
                auditCycleCoverage.Area__c = 'iRAP';
                auditCycleCoverage.Scope__c = priorityToScopeMap.get(i);
                auditCycleCoverage.Auto_Assignment_Configuration__c = autoAssignmentConfigList[i].Id;
                auditCycleCoverageList.add(auditCycleCoverage);
            }
            insert auditCycleCoverageList;
            
            
            KARL_Audit_Team__c auditTeam = ARL_TestDataFactory.createAuditTeam('test');
            insert auditTeam;
            
            KARL_Audit_Team_Contacts__c auditTeamCon = ARL_TestDataFactory.createAuditTeamContact(auditTeam.Id,con.Id);
            insert auditTeamCon;
            
            Request_Item__c reqItem = ARL_TestDataFactory.createRequestItem();
            reqItem.Primary_Scope__c = 'SS';
            reqItem.KARL_Evidence_Submission_Workflow__c = true;
            reqItem.Create_GUS_Case__c = true;
            insert reqItem;
            
            List<Cycle_Request_Item__c> cycleReqItemList = new List<Cycle_Request_Item__c>();
            for(Integer i=0;i<4;i++){
                Cycle_Request_Item__c cycleReqItem = ARL_TestDataFactory.createCycleRequestItem(reqItem.id,null);  
                cycleReqItem.Cycle_Request_Status__c = 'New';
                cycleReqItem.KARL_Update_GUS__c = true;
                cycleReqItem.Request_Tech_Details__c = 'tech details old';
                cycleReqItem.Request_Assignee__c = con.Id;
                cycleReqItem.Audit_Cycle__c = auditCycle.Id;
                cycleReqItem.Audit_Cycle_Coverage__c = auditCycleCoverageList[i].Id;
                cycleReqItem.Request_GUS__c = null;
                cycleReqItemList.add(cycleReqItem);
            }
            insert cycleReqItemList;
            
            List<KARL_Evidence_Request__c> karlEvidenceRequestList = new List<KARL_Evidence_Request__c>();
            for(Integer i=0;i<4;i++){
                KARL_Evidence_Request__c evidenceRequestItem = new KARL_Evidence_Request__c();
                evidenceRequestItem.KARL_Cycle_ARL_Item__c = cycleReqItemList[i].Id;
                karlEvidenceRequestList.add(evidenceRequestItem);
            }
            insert karlEvidenceRequestList;
            List<KARL_Auditor_Request_Item__c> auditorRequesTeamList = new List<KARL_Auditor_Request_Item__c>();
            for(Integer i=0;i<4;i++){
                KARL_Auditor_Request_Item__c auditorRequestTeamObj = new KARL_Auditor_Request_Item__c();
                auditorRequestTeamObj.KARL_Cycle_Request_Item__c = cycleReqItemList[i].Id;
                auditorRequestTeamObj.KARL_Audit_Team__c = auditTeam.Id;
                auditorRequesTeamList.add(auditorRequestTeamObj);
            }
            insert auditorRequesTeamList;
            KARL_Audit_Scope_Master_Data__c auditScope = ARL_TestDataFactory.createAuditScope();
            insert auditScope;
            KARL_Audit_Scope_Reports__c auditScopeReport = ARL_TestDataFactory.createAuditScopeReport(auditScope.Id,auditFirm.Id);
            insert auditScopeReport;
            List<KARL_Audit_Scope_In_Report__c> auditScopeInReportList = new List<KARL_Audit_Scope_In_Report__c>();
            KARL_Audit_Scope_In_Report__c parentAuditScopeInReport = new KARL_Audit_Scope_In_Report__c();
            parentAuditScopeInReport.KARL_Parent_Audit_Scope__c = auditScope.Id;
            parentAuditScopeInReport.KARL_Audit_Scope_Report__c = auditScopeReport.Id;
            auditScopeInReportList.add(parentAuditScopeInReport);
            KARL_Audit_Scope_In_Report__c masterDataAuditScopeInReport = new KARL_Audit_Scope_In_Report__c();
            masterDataAuditScopeInReport.KARL_Audit_Scope_Report__c = auditScopeReport.Id;
            masterDataAuditScopeInReport.KARL_Parent_Audit_Scope__c = auditScope.Id;
            auditScopeInReportList.add(masterDataAuditScopeInReport);
            KARL_Audit_Scope_In_Report__c auditScopeInReport = new KARL_Audit_Scope_In_Report__c();
            auditScopeInReport.KARL_Audit_Scope_Report__c = auditScopeReport.Id;
            auditScopeInReport.KARL_Parent_Audit_Scope__c = auditScope.Id;
            auditScopeInReportList.add(auditScopeInReport);
            insert auditScopeInReportList;
       }
    }
    
    /**
     * @author Mahima Aggarwal
     * @email mahima.aggarwal@mtxb2b.com
     * @desc Test fetchPicklist()
    */
    @isTest
    private static void testFetchPicklist(){
        List<KARL_Utility.SelectOptionWrapper> picklistValues =  KARL_Utility.fetchPicklist('ContentVersion', 'Status__c');
        System.assert(picklistValues!=null && picklistValues.size()>0);
    }
    
    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Test simple SOQL wire functions for LWCs
    */
    @isTest
    private static void lwcWires(){
      List<User> opsAdminUserList = ARL_TestDataFactory.fetchUser('abc@b.com.d');
        if(!opsAdminUserList.isEmpty()){
            System.runAs(opsAdminUserList[0]){
                List<Request_Item__c> requestItemList = new List<Request_Item__c>([SELECT Id FROM Request_Item__c WHERE Primary_Scope__c = 'SS']);
                List<KARL_Audit_Scope_Master_Data__c> auditScopeList = new List<KARL_Audit_Scope_Master_Data__c>([SELECT Id FROM KARL_Audit_Scope_Master_Data__c]); 
                List<KARL_Audit_Scope_Reports__c> auditScopeReportList = new List<KARL_Audit_Scope_Reports__c>([SELECT Id FROM KARL_Audit_Scope_Reports__c]); 
                Test.startTest();
                
                // Run tests for drop down menus data collection
                List<KARL_Audit_Team__c> auditTeams = KARL_Utility.getAuditTeams();
                System.assertEquals(3, auditTeams.size(), 'Incorrect number of audit teams' ); // Expect 2x
                
                List<Audit_Cycle__c> auditCycles = KARL_Utility.getAuditCycles();
                System.assertEquals(1, auditCycles.size(), 'Incorrect number of audit cycles' ); // Expect 1x
                
                Map<String, String> auditScopes = KARL_Utility.getAuditCycleScopes();
                System.assert(auditScopes!=null && auditScopes.size()>0);
                
                Cycle_Request_Item__c[] cycleRequestArray = KARL_Utility.getCycleReqItemRecords(requestItemList[0].Id);
                System.assert(!cycleRequestArray.isEmpty());
                
                KARL_Evidence_Request__c[] karlEvidenceArray = KARL_Utility.getCreqEvidenceRecords(cycleRequestArray[0].Id);
                System.assert(!karlEvidenceArray.isEmpty());
                
                KARL_Auditor_Request_Item__c[] auditorReqTeamArray = KARL_Utility.getAuditorReqItemRecords(cycleRequestArray[0].Id);
                System.assert(!auditorReqTeamArray.isEmpty());
                
                KARL_Audit_Scope_Reports__c[] auditScopeReportsArray = KARL_Utility.getPrimaryReports(auditScopeList[0].Id);
                System.assert(!auditScopeReportsArray.isEmpty());
                
                KARL_Audit_Scope_In_Report__c[] auditScopeInReportList= KARL_Utility.getRelatedReports(auditScopeReportList[0].Id,'' , '');
                System.assert(!auditScopeInReportList.isEmpty());
                KARL_Audit_Scope_In_Report__c[] auditScopeInReportList1= KARL_Utility.getRelatedReports(auditScopeList[0].Id,'Parent' , '');
                System.assert(!auditScopeInReportList1.isEmpty());
                KARL_Audit_Scope_In_Report__c[] auditScopeInReportList2= KARL_Utility.getRelatedReports(auditScopeList[0].Id,'' , 'KARL_Audit_Scope_Master_Data__c');
                System.assert(auditScopeInReportList2.isEmpty());
                
                String parentREQRecordId = KARL_Utility.getParentREQ('Cycle_Request_Item__c',cycleRequestArray[0].Id);
                System.assertEquals(parentREQRecordId,String.valueOf(requestItemList[0].Id));
                String parentREQRecordId1 = KARL_Utility.getParentREQ('',cycleRequestArray[0].Id);
                System.assertEquals(parentREQRecordId1,cycleRequestArray[0].Id);
                
                Test.stopTest();
            }
        }
    }
    
    @isTest
    private static void getBaseURLTest(){
        Map<String,String> baseUrlMap = KARL_Utility.getBaseURL();
        System.assert(baseUrlMap.get('baseRecordURL') != '');
        System.assert(baseUrlMap.get('baseUrlSuffix') != '');
       
    }
    
    @isTest
    private static void calculateWorkingDateTest(){
        Date weekStartDate = System.today().toStartOfWeek();
        Date comparisonDate = System.today().toStartOfWeek().addDays(4);
        System.assert(KARL_Utility.calculateWorkingDate(3,weekStartDate) == comparisonDate);
    }
    
    @isTest
    private static void getweekdayOfDateTest(){
        Date weekStartDate = System.today().toStartOfWeek();
        System.assertEquals(KARL_Utility.getweekdayOfDate(weekStartDate),'Sunday');
    }
}
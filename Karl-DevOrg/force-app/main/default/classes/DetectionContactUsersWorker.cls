global class DetectionContactUsersWorker implements Schedulable {

   public static String CRON_EXP = '0 15 * * * ?';
   //'0 30 * * * ?';
    
   global void execute(SchedulableContext ctx) {
   
       //Every X minutes

       List<AlertsPendingJudication__c> apjNeedingUpdate = new  List<AlertsPendingJudication__c>();
       
       List<AlertsPendingJudication__c> apjs = [SELECT Id, DA__c, DA__r.alertInformation__c, 
                                                		DAC__c, DAC__r.Action_for_confirmed_event__c,
                                                		DAC__r.User_Verification_Email_Mode__c, DAC__r.User_Verification_Static_Email__c,
                                                		Last_Contact_Time__c, User_Data_Dictionary_Feild__c, Status__c, 
                                                		Email_Frequency__c, Email__c, Escalated_to_ResponseOrg__c, Max_Wait_Time__c, 
                                                		CreatedDate, Attempts__c, User_Replied__c, Response__c, Email_Body__c, Log__c 
                                               	 FROM AlertsPendingJudication__c
                                               	 WHERE (Escalated_to_ResponseOrg__c = null 
                                                       	AND User_Replied__c = null)];
       
       List<Detection_Alert__c> enrichedDAstoUpdate = new List<Detection_Alert__c>();
       
       List<Id> daIds = new List<Id>(); 
       
       for(AlertsPendingJudication__c apj : apjs) {
           if(!daIds.contains(apj.DA__c))
               daIds.add(apj.DA__c);
       }
       
       List<Detection_Alert_Event_Map__c> daMapData = [SELECT Id, Detection_Alert__c, Detection_Alert__r.AlertInformation__c, Detection_Security_Event__r.FieldMapRaw__c 
                                                       FROM Detection_Alert_Event_Map__c
                                                       WHERE Detection_Alert__c IN: daIds];
       
       Map<Id, Detection_Alert__c> das = new Map<Id, Detection_Alert__c>([SELECT Id, AlertInformation__c
                                       										FROM Detection_Alert__c
                                       										WHERE Id IN: daIds]);
        
       
       for(AlertsPendingJudication__c apj : apjs)
       {
           try
           {
               if(userReplied(apj))
               {
                   //user has replied need to enrich DA
                   //setting timestamps indicates it has been enriched
                   
                   apj.User_Replied__c = DateTime.now();
                   apj.Log__c += 'User Replied\n';
                   
                   if(!apjNeedingUpdate.contains(apj))
                   		apjNeedingUpdate.add(apj);

                   enrichedDAstoUpdate.add(enrichAlert(apj, daMapData, das));
               }
               
               if(needsEscalation(apj) || isExpired(apj))
               {
                   apj.Log__c += 'Needs Escalation\n';
                   
                   if(isExpired(apj))
                   {
                   	 apj.Status__c = 'Expired';
                     apj.Log__c += 'Expired\n';
                   }
                   
                   //Note user notifed status set in click / DetectionConfirmActivityController
                   EscalateAlert(apj);
                   
 				   apj.Escalated_to_ResponseOrg__c = DateTime.now();
                   
                   if(!apjNeedingUpdate.contains(apj))
                   	apjNeedingUpdate.add(apj);
               }
               else if(isEmailRequired(apj))
               {
                   System.debug('EmailUser()');
                   
                   apj.Log__c += 'Email is Required\n';
                   
                   //send another email waiitng for reply
                   apj.Last_Contact_Time__c = DateTime.now();
                   
                   if(apj.Attempts__c == null)
                       apj.Attempts__c = 0;
                   
                   apj.Attempts__c = apj.Attempts__c + 1;
                   
                   //Email user
                   EmailUser(apj, daMapData);
                   
                   if(!apjNeedingUpdate.contains(apj))
                   		apjNeedingUpdate.add(apj);
               }
           }
           catch(Exception ex)
           {
               apj.Log__c += 'Error: ' + ex.getStackTraceString() + '\n';
               
               Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
               message.toAddresses = new String[] { 'amcneilly@salesforce.com' };
               message.subject = 'ERROR DetectionContactUsersWorker';
               message.plainTextBody = ex.getMessage() +  ex.getStackTraceString();
               Messaging.SingleEmailMessage[] messages =   new List<Messaging.SingleEmailMessage> {message};
               Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
           }
       }
       
       UPDATE apjNeedingUpdate;
       UPDATE enrichedDAstoUpdate;
   }
    
   private Detection_Alert__c enrichAlert(AlertsPendingJudication__c apj, List<Detection_Alert_Event_Map__c> daMapData, Map<Id, Detection_Alert__c> das)
   {
       Detection_Alert__c da = das.get(apj.DA__r.Id);
           
       if(da != null)
       {
            String alertInfo = da.alertInformation__c;
           
            String temp = '********** User Confirmed Alert **********\n\n';
            
            temp += 'User Status : ' + apj.Status__c + '\n';
            temp += 'User Notes : ' + apj.Response__c + '\n';
            temp += 'Email Address : ' + apj.Email__c + '\n';
            temp += 'Email Body : ' + apj.Email_Body__c + '\n\n\n';
           
            da.alertInformation__c = temp + alertInfo;
        }
       
       return apj.DA__r;
   }
    
   private void EscalateAlert(AlertsPendingJudication__c apj)
   {
       //TODO - send to ResponseOrg

       Detection_Alert__c detectionAlert = [SELECT Id, AlertVersion__c, HiddenDataMap__c, HiddenEventMap__c, AlertInformation__c, Environment__c, Subject__c, Severity__c, Description__c, RecipientTeam__c, CreatedDate, AttackIQ_Alert__c, RecipientTeamFormat__c
                                FROM Detection_Alert__c 
                                WHERE Id =: apj.DA__c LIMIT 1];
       
       
       //TODO enrich alert that user has indicated they are not aware of event
       
       Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
       message.toAddresses = new String[] { 'amcneilly@salesforce.com' };
       message.subject = 'DEBUG - Alert sent to ResponseOrg';
       message.plainTextBody = '';
       Messaging.SingleEmailMessage[] messages =   new List<Messaging.SingleEmailMessage> {message};
       Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
       
       
       if((detectionAlert.RecipientTeam__c == 'CSIRT' || detectionAlert.RecipientTeam__c == 'CSIRT - Test Queue' || detectionAlert.RecipientTeam__c == 'CSIRT - GIA')
               && detectionAlert.RecipientTeamFormat__c == 'CSIRTAlert') {
                
                CSIRT_HANDLEDETECTIONALERT.IntakeAlert newAlert = new  CSIRT_HANDLEDETECTIONALERT.IntakeAlert();
                
                newAlert.DetectionAlert = detectionAlert.Id;
                newAlert.Environment = detectionAlert.Environment__c;
                newAlert.Subject = detectionAlert.Subject__c;
                newAlert.Description = detectionAlert.AlertInformation__c;
                newAlert.EventTimestamp = detectionAlert.CreatedDate;
                
                newAlert.RecipientTeam = detectionAlert.RecipientTeam__c;
    
                newAlert.Severity = detectionAlert.Severity__c;
                
                //TODO Enable
                //CSIRT_HANDLEDETECTIONALERT.ProcessAlert(newAlerts);
            }
		
   }
    
    private Detection_Alert_Event_Map__c getDA(AlertsPendingJudication__c apj, List<Detection_Alert_Event_Map__c> daMapData)
    {
        Detection_Alert_Event_Map__c da;
        
        System.debug('getDA()');
        System.debug('daMapData size: ' + daMapData.size());
        System.debug('apj.DA__c' + apj.DA__c);
        
        for(Detection_Alert_Event_Map__c dax : daMapData)
        {
            System.debug('daMapData.Detection_Alert__c' + dax.Detection_Alert__c );
            
            if(((id)dax.Detection_Alert__c) == ((id)apj.DA__c))
            {
                da = dax;
                break;
            }
        }
        
        return da;
    }
    
    private void MissingEmailFieldBypass(AlertsPendingJudication__c apj)
    {
        //missing email event feild so we will bypass user verfication and send to responseorg
        
        apj.Log__c += 'missing email event feild so we will bypass user verfication and send to responseorg\n';
        apj.Status__c = 'Missing Field - Escalated';
        apj.Escalated_to_ResponseOrg__c = DateTime.now();
        
        EscalateAlert(apj);
        
        UPDATE apj;
    }
    
   private void EmailUser(AlertsPendingJudication__c apj, List<Detection_Alert_Event_Map__c> daMapData)
   {
       if(!String.isEmpty(apj.Email__c))
       {
           //DAC__r.User_Verification_Email_Mode__c, DAC__r.User_Verification_Static_Email__c
           
           if(apj.DAC__r.User_Verification_Email_Mode__c == 'Static Email (Static Email)')
           {
               if(String.isEmpty(apj.DAC__r.User_Verification_Static_Email__c))
               {
                   apj.Log__c += 'Missing static email value\n';
                   
                   apj.Email__c = null;
                       
                   MissingEmailFieldBypass(apj);
                       
                   return;
               }
               else
               {
                   apj.Email__c = apj.DAC__r.User_Verification_Static_Email__c;
               }
           }
           else
           {
            
               Detection_Alert_Event_Map__c da = getDA(apj, daMapData);
       
               if(da != null)
               {
                   Map<String, Object> unraw = (Map<String, Object>) JSON.deserializeUntyped(da.Detection_Security_Event__r.FieldMapRaw__c);
                   
                   string emailAddr = (String) unraw.get(apj.User_Data_Dictionary_Feild__c);
                    
                   if(String.isEmpty(emailAddr) || emailAddr == null)
                   {
                       apj.Log__c += 'DSEC is missing an email address field based on key ' + apj.User_Data_Dictionary_Feild__c + '\n';
                       apj.Email__c = null;
                       
                       MissingEmailFieldBypass(apj);
                       
                       return;
                   }
                   else
                   {
                       apj.Email__c = emailAddr;
                   }
               }
           	}

            UPDATE apj;
       }
       
       
        EmailTemplate et = [SELECT Id, body FROM EmailTemplate WHERE DeveloperName = 'Detection_User_Confirm'];
       
        //Email template cannot reference inner inner email body text. So we copy it over to local object
        //Possible issue is that the length of email body must be the same for both objects
        AlertsPendingJudication__c fData = [SELECT DAC__r.User_Verification_Email_Message__c 
                                            FROM AlertsPendingJudication__c 
                                            WHERE Id =: apj.Id LIMIT 1];
        
        //set HTML template encoding
        String emailTemplate = encodeEmailTemplate(fData.DAC__r.User_Verification_Email_Message__c, 
                                                       apj.DA__c, 
                                                       apj, 
                                                       daMapData);
        //extract email from event data
        if(String.isBlank(apj.Email__c))
        {
            apj.Email__c = GetEmailAddress(apj.User_Data_Dictionary_Feild__c, apj, daMapData);
        }
        
        if(emailTemplate == null)
        {
            apj.Log__c += 'Email encoding error. did not contain valid template\n';
        }
        else
        {         
            apj.Email_Body__c = emailTemplate;
            UPDATE apj;
           
            List<string> toAddress = new List<string>();
            //TODO change
            //toAddress.add('amcneilly@salesforce.com');
            toAddress.add(apj.Email__c);
            
            if(String.isEmpty(apj.Email__c))
            {
                apj.Log__c += 'Object does not contain email address for Email__c\n';
            }
            else
            { 
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    mail.setTemplateId(et.Id);
                    mail.setToAddresses(toAddress);
                    mail.setBccSender(false);
                    mail.setOrgWideEmailAddressId('0D2300000004E4ZCAU');
                    mail.setTargetObjectId('0033000001pVfGIAA0');
                    mail.setWhatId(apj.Id);
                    mail.setTreatTargetObjectAsRecipient(false);
                    mail.setSaveAsActivity(false);
               
                List<Messaging.SingleEmailMessage> allmsg = new List<Messaging.SingleEmailMessage>();
                allmsg.add(mail);
                
                Messaging.sendEmail(allmsg);
                
                apj.Log__c += 'Sent email to: ' + toAddress + '\n';
            }
        }
       
        UPDATE apj;
   }
    
    private Map<String, String> getEmailTemplates(String encodedString)
    {
        Map<String, String> templateData = new Map<String, String>();
       
        if(!encodedString.contains('\\?\\?'))
        {
            //matches anything if missing ??keywords??
            templateData.put('', encodedString);
            
            return templateData;
        }
            
        Pattern p = Pattern.compile('\\?\\?');
        Matcher m = p.matcher(encodedString);
       
        while (m.find())
        {
             String s = m.group();
             s = s.replaceAll('??', '');
            
             String keywords = s;
             String template = m.group();
            
             templateData.put(keywords, template);
        }
        
        return templateData;
    }
    
    private String GetEmailAddress(String emailField, AlertsPendingJudication__c apj, List<Detection_Alert_Event_Map__c> daMapData)
    {
        System.debug('**** apj ****');
        System.debug(apj);
        System.debug('**** daMapData **** ');
        System.debug(daMapData);
        
        Detection_Alert_Event_Map__c da = getDA(apj, daMapData);
        
        System.debug('****  da **** ');
        System.debug(da);
        System.debug('**** da.Detection_Security_Event__r.FieldMapRaw__c **** ');
        System.debug(da.Detection_Security_Event__r.FieldMapRaw__c); //error here
        System.debug('**** semailField **** ');
        System.debug(emailField);
        System.debug('================');
        System.debug('================');
        System.debug('================');
        
        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
               message.toAddresses = new String[] { 'amcneilly@salesforce.com' };
               message.subject = 'GetEmailAddress';
               message.plainTextBody = 'emailField ' + emailField;
               Messaging.SingleEmailMessage[] messages =   new List<Messaging.SingleEmailMessage> {message};
               Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
        
        if(!da.Detection_Security_Event__r.FieldMapRaw__c.contains(emailField))
        {
            return null;
        }
        
        Map<String, Object> unraw = (Map<String, Object>) JSON.deserializeUntyped(da.Detection_Security_Event__r.FieldMapRaw__c);
        
        String emailAddr = (String)unraw.get(emailField);
        
        if(!String.isBlank(emailAddr))
        {
            if(emailAddr.contains('@'))
            {
                return emailAddr;
            }
            else
            {
                return emailAddr + '@salesforce.com';
            }
        }
        
        return null;
    }
    
   private String encodeEmailTemplate(String emailT, Id daId, AlertsPendingJudication__c apj, List<Detection_Alert_Event_Map__c> daMapData)
   {
       String encodedString = emailT;
       
       Detection_Alert_Event_Map__c da = getDA(apj, daMapData);
       
       if(da != null)
       {
           System.debug(da.Detection_Security_Event__r.FieldMapRaw__c);
           
           Map<String, String> templates = getEmailTemplates(encodedString);

           Map<String, Object> unraw = (Map<String, Object>) JSON.deserializeUntyped(da.Detection_Security_Event__r.FieldMapRaw__c);

           //TODO HERE
           //=========

           for(String keywords : templates.keySet())
           { 
               if(keywords == '')
               {
                   //matches anything
                   return templateEncoder(encodedString ,unraw);
               }
               else
               {
                   //look for keywords in data
                   if(da.Detection_Security_Event__r.FieldMapRaw__c.contains(keywords))
                   		return templateEncoder(encodedString, unraw);
               }
           }         
       }
       
       return null;
   }
       
	private String templateEncoder(String encodedString, Map<String, Object> unraw)
    {
        Pattern p = Pattern.compile('##([^#]+)##');
        Matcher m = p.matcher(encodedString);
        
        while (m.find())
        {
            String s = m.group();
            s = s.replaceAll('##', '');
        
            string v = (String) unraw.get(s);
            
            if(String.isNotEmpty(v))
            {  
                if(v.contains('www.') || v.contains('http.'))
                {
                    v = v.replace('www.', 'www(dot)');
                    v = v.replace('.com', '(dot)com');
                }
            
                if(String.isNotBlank(v))
                {
                    encodedString = encodedString.replace('##' + s + '##', v);
                }
            }
        }
        
        return encodedString;
    }
    
    private Boolean userReplied(AlertsPendingJudication__c apj)
    {
        if(apj.User_Replied__c == null && apj.Status__c != 'Pending')
            return true;
        
        
        return false;
    }
    
   private Boolean needsEscalation(AlertsPendingJudication__c apj)
   {
       if(apj.Escalated_to_ResponseOrg__c == null 
          && (apj.Status__c == 'User Uncertain' || (apj.Status__c == 'User Confirmed' && apj.DAC__r.Action_for_confirmed_event__c  == 'Send to Alert Queue')))
           return true;
       
       return false;
   }
    
   private Boolean isExpired(AlertsPendingJudication__c apj)
   {     
       if(((DateTime.now().getTime() - apj.CreatedDate.getTime()) / 3600000) > apj.Max_Wait_Time__c)
       {
			return true;     
       }

       return false;
   }
    
   private Boolean isEmailRequired(AlertsPendingJudication__c apj)
   {
       Boolean checkWithUserRequired = false;
               
       if(apj.Last_Contact_Time__c == null)
       {
       		return true;
       }
       else
       {
           if(((DateTime.now().getTime() - apj.Last_Contact_Time__c.getTime()) / 3600000) > apj.Email_Frequency__c)
           {
            	return true;    
           }
       }
       
       return false;
   }
}
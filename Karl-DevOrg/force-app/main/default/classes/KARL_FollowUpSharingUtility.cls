/**
 * @author Ben Harvie
 * @email ben.harvie@salesforce.com
 * @description This method is an adaptation of the MTX developed KARL_AuditorReqItemSharingUtility, extending
 * the capabilities to the Follow-up object (being released for the FY22 Spring Audits).
*/
public with sharing class KARL_FollowUpSharingUtility {
    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description This method Shares (adds sharing) of the Follow-up items which are shared to auditFirmIdToContactIdMap's contacts
     * @param followUpItemList - list of all Follow Up items that we need to share
     * @param auditFirmIdToContactIdMap - mapping of audit firm contacts and their Contact Ids
    */
    public static void shareFollowUpItem(List<KARL_Cycle_Follow_up__c> followUpItemList,Map<Id,Set<Id>> auditFirmIdToContactIdMap){
        //System.debug(LoggingLevel.DEBUG, 'sharing execution start>>');
        /** 
         * ! Contact Collection & Setup
         * @author Ben Harvie
         * @description Collect a list of all Contacts in the system to build the 
         * possible targets of who the object records need to be shared with 
         */
        Set<Id> allContactIdSet = new Set<Id>();
        Map<Id,Id> contactIdToUserIdMap = new Map<Id,Id>();

        // Build the list of Audit Firm contacts
        for(Set<Id> contactIdSet : auditFirmIdToContactIdMap.values()){
            allContactIdSet.addAll(contactIdSet);
        }
        
        // Add to the map of contacts that we want to use
        for(User usr : [SELECT Id, ContactId, Contact.AccountId
                        FROM User 
                        WHERE ContactId IN :allContactIdSet]){
            contactIdToUserIdMap.put(usr.ContactId,usr.Id);
        }

        /** 
         * ! Target Object Record Collection & Setup
         * @author Ben Harvie
         * @description Collect a list of all of the shareable records in the target object
         * that we need to share with the relevant contacts.
         */
        Map<Id,Id> followUpItemIdToAuditFirmIdMap = new Map<Id,Id>();
        List<KARL_Cycle_Follow_up__Share> followUpItemShareList = new List<KARL_Cycle_Follow_up__Share>();

        // Build the map of all records we can share
        for(KARL_Cycle_Follow_up__c followUpItem : followUpItemList){
            followUpItemIdToAuditFirmIdMap.put(followUpItem.Id,followUpItem.KARL_Followup_Audit_Team__r.KARL_Audit_Firm__c);
        }

        // Cycle through the Follow-up items and build the list of records to be shared 
        for(KARL_Cycle_Follow_up__c followUpItem : followUpItemList){
            Id auditFirmId = followUpItemIdToAuditFirmIdMap.get(followUpItem.Id);
            if(auditFirmId != null && auditFirmIdToContactIdMap.get(auditFirmId) != null &&
                !auditFirmIdToContactIdMap.get(auditFirmId).isEmpty()){
                    for(Id conId : auditFirmIdToContactIdMap.get(auditFirmId)){
                        Id userId = contactIdToUserIdMap.get(conId);
                        if(userId != null){
                            KARL_Cycle_Follow_up__Share followUpItemShare = new KARL_Cycle_Follow_up__Share();
                            followUpItemShare.ParentId = followUpItem.Id; 
                            followUpItemShare.UserOrGroupId = userId;
                            followUpItemShare.AccessLevel = 'Edit';
                            followUpItemShare.RowCause = Schema.KARL_Cycle_Follow_up__Share.RowCause.Manual;

                            followUpItemShareList.add(followUpItemShare);
                        }
                    }
            }
        }

        // Debug Logging
        //System.debug(LoggingLevel.DEBUG, 'insert followUpItemShareList>>'+followUpItemShareList);

        // Save the result (additional sharing rules)
        List<Database.SaveResult> srFollowUpItemList = Database.insert(followUpItemShareList,false);
        for(Database.SaveResult sr : srFollowUpItemList){
            if(!sr.isSuccess()){
                System.debug(LoggingLevel.ERROR, 'Follow-up Item sharing Failed due to following Reasons:');
                for(Database.Error dr : sr.getErrors()){
                    System.debug(LoggingLevel.ERROR, 'Error Code: '+dr.getStatusCode());
                    System.debug(LoggingLevel.ERROR, 'Error Description: '+dr.getMessage());
                    System.debug(LoggingLevel.ERROR, 'Fields affected: '+dr.getFields());
                }
            }
        }
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description This method unShares (deletes sharing) of the Follow-up items which are shared to auditFirmIdToContactIdMap's contacts
     * @param followUpItemList - list of all Follow Up items that we need to share
     * @param auditFirmIdToContactIdMap - mapping of audit firm contacts and their Contact Ids
    */
    public static void unShareFollowUpItem(List<KARL_Cycle_Follow_up__c> followUpItemList,Map<Id,Set<Id>> auditFirmIdToContactIdMap){
        //System.debug(LoggingLevel.DEBUG, 'delete execution start>>');
        /** 
         * ! Contact Collection & Setup
         * @author Ben Harvie
         * @description Collect a list of all Contacts in the system to build the 
         * possible targets of who the object records need to be shared with (or delete)
         */
        Set<Id> allContactIdSet = new Set<Id>();
        Set<Id> userIdSet = new Set<Id>();

        for(Set<Id> contactIdSet : auditFirmIdToContactIdMap.values()){
            allContactIdSet.addAll(contactIdSet);
        }

        for(User usr : [SELECT Id, ContactId, Contact.AccountId
                        FROM User 
                        WHERE ContactId IN :allContactIdSet]){
            userIdSet.add(usr.Id);
        }

        /** 
         * ! Object Record Collection & Setup
         * @author Ben Harvie
         * @description Collect a list of all of the shareable records in the target object
         * that we need to remove the identified Contacts from.
         */
        Set<Id> followUpItemIdSet = new Set<Id>();
        String rowCauseTypeFollowUpItem = Schema.KARL_Cycle_Follow_up__Share.RowCause.Manual;

        List<KARL_Cycle_Follow_up__Share> followUpItemShareList = [SELECT Id 
                                                                FROM KARL_Cycle_Follow_up__Share
                                                                WHERE UserOrGroupId IN : userIdSet
                                                                AND ParentId IN : followUpItemIdSet
                                                                AND RowCause = :rowCauseTypeFollowUpItem
                                                                WITH SECURITY_ENFORCED];
        
        // Debug Logging
        //System.debug(LoggingLevel.DEBUG, 'Delete followUpItemShareList>>'+followUpItemShareList);

        // Delete the result (removal of sharing rules)
        Database.DeleteResult[] drFollowUpItemList = Database.delete(followUpItemShareList, false);

        for(Database.DeleteResult dr : drFollowUpItemList) {
            if (dr.isSuccess()) {
                // Operation was successful, so get the ID of the record that was processed
                System.debug(LoggingLevel.INFO, 'Successfully deleted with ID: ' + dr.getId());
            }
            else {
                System.debug(LoggingLevel.ERROR, 'Failed due to following Reasons:');                
                for(Database.Error err : dr.getErrors()) {
                    System.debug(LoggingLevel.ERROR, 'The following error has occurred.');                    
                    System.debug(LoggingLevel.ERROR, err.getStatusCode() + ': ' + err.getMessage());
                    System.debug(LoggingLevel.ERROR, 'fields that affected this error: ' + err.getFields());
                }
            }
        }
    }
}
/**
 * @File Name          : SPT_UserLookupController.cls
 * @Description        : Return list of user's and queue based on search parameter.
 * @Author             : Banshi
 * @Group              : 
 * @Last Modified By   : Banshi
 * @Last Modified On   : 5/14/2020, 3:54:20 PM 
 * @Modification Log   : 
 * Ver       Date            Author                 Modification
 * 1.0    5/14/2020   Banshi     Initial Version
**/
public class SPT_UserLookupController {
    @AuraEnabled
    public static List<SObJectResult> getResults(String value){
        List<SObJectResult> sObjectResultList = new List<SObJectResult>();
        
        List<List<sObject>> searchList = [FIND :value IN NAME FIELDS 
                                          RETURNING User(Name,Id where IsActive= true AND ContactId =''),Group(Name, Id WHERE Type='Queue')];
        User[] searchUsers = (User[])searchList[0];
        Group[] searchQueues = (Group[])searchList[1];
        for (User u : searchUsers) {
            sObjectResultList.add(new SObjectResult('User',u.Name, u.Id));
        }
        for (Group g : searchQueues) {
            sObjectResultList.add(new SObjectResult('Queue',g.Name, g.Id));
        }
        return sObjectResultList;
    }
  /*  @AuraEnabled
    public static List<SObJectResult> getResults(String value) {
        List<SObJectResult> sObjectResultList = new List<SObJectResult>();
        String ContactId = '';
        String groupType = 'Queue';
        for(User u : Database.Query('Select Id,Name FROM User WHERE Name LIKE \'%' + value + '%\' AND IsActive= true AND ContactId =:ContactId')) {
            sObjectResultList.add(new SObjectResult('User',u.Name, u.Id));
        }
        for(Group item : Database.Query('Select Id,Name FROM Group WHERE Name LIKE \'%' + value + '%\' AND Type=:groupType')){
            sObjectResultList.add(new SObjectResult('Queue',item.Name, item.Id));
        }
        
        return sObjectResultList;
    } */
    
    /* Created by - Lakshya Arora
    * Date - 26-05-2020
    * Modified by/Date - 
    * This functon is used to return set owner Id for SPT_customListView if logged in user belongs to Enterprise_Security Group
    */
    @AuraEnabled
    public static String isEnterpriseSecurityGroupMember(String currentUserId){
      //FINAL String ENTERPRISESECUIRTYGROUPDEVNAME ='Enterprise_Security';
      FINAL String ENTERPRISESECUIRTYGROUPDEVNAME = Label.SPT_PublicGroupName;
      FINAL String TRUSTAPPSECURITYASSURANCEDEVNAME= Label.SPT_QueueName;
         List<GroupMember> groupMemberList = new List<GroupMember>();
        if(test.isRunningTest()){
            groupMemberList = [SELECT GroupId,Group.developerName 
                               FROM GroupMember 
                               WHERE userorgroupid =:currentUserId and group.developerName =: ENTERPRISESECUIRTYGROUPDEVNAME];
        }else{
            groupMemberList = [SELECT GroupId,Group.developerName 
                               FROM GroupMember 
                               WHERE userorgroupid =:currentUserId and group.developerName =:ENTERPRISESECUIRTYGROUPDEVNAME];
        }
      List<Group> groupList = new List<Group>();  
      if(!groupMemberList.isEmpty()){
        groupList = [SELECT Id FROM Group WHERE Type = 'Queue' AND DeveloperNAME =:TRUSTAPPSECURITYASSURANCEDEVNAME];
      }
      if(!groupList.isEmpty()){
         return  groupList[0].Id;
      }
      return '';
    }
    
    //Wrapper class to return data
    public class SObJectResult {
        @AuraEnabled
        public String objectName;
        @AuraEnabled
        public String recName;
        @AuraEnabled
        public Id recId;
        
        public SObJectResult(String objectName,String recNameTemp, Id recIdTemp) {
            this.objectName = objectName;
            this.recName = recNameTemp;
            this.recId = recIdTemp;
        }
    }
}
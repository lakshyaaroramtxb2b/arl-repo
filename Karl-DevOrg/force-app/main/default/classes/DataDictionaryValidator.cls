public class DataDictionaryValidator
{
    @InvocableMethod
    public static void DDCheckRegex(List<Id> feIds)
    {
        List<Detection_Field_Extraction__c> extractionRules = [SELECT Id
                                                               FROM Detection_Field_Extraction__c
                                                               WHERE Id in :feIds];

        for(Detection_Field_Extraction__c feRule : extractionRules)
        {
			feRule.Extracted_Fields__c = 'Stuff done';
            
            UPDATE feRule;
        }
    }
}
public class PhishingScorecardController {

    public PhishingQuizConstants qc = new PhishingQuizConstants();
    private Set<String> incorrectTopicSet = new Set<String>();
    public List<String> incorrectTopics {get;set;}
    public String sId {get;set;}
    public Integer allCount { get; set; }
    public Integer correctCount { get; set; }
    public PhishingQuizUser__c user {get;set;}
    public List<PhishingQuizAnswer__c> scorecard;
    private datetime oldestAllowed; 
    public String debugField {get;set;}
    
    transient List<PhishingQuizAnswer__c> allAnswers;
    transient List<PhishingQuizAnswer__c> missedAnswers = new List<PhishingQuizAnswer__c>();
    
    public PhishingScorecardController() {
        sId = ApexPages.CurrentPage().getParameters().get('sId');
        correctCount = 0;
        
    }
    
    public class infowrapper {
        public String altlink {get;set;}  // you may have to do {get;set;} on these
        public String link {get;set;}
        public String correct {get;set;}
    }

    
    public PageReference init() {
        if (sId == null) {
           
            return qc.register; 
        }
        else {
            // load the correct QuizUser from the sId passed in the params
            // redirect them to the registration page if they don't have one
            oldestAllowed = datetime.now().addDays(0-qc.quizValidDays);
            List<PhishingQuizUser__c> activeUsers = [Select Id,Key__c,quiz_completion__c,lastActive__c,Name from PhishingQuizUser__c 
                      where Key__c =: sId and created__c >=: oldestAllowed limit 1];
            if (activeUsers.size() > 0){
                user = activeUsers.get(0);
            }
            if (user == null) {
                return qc.register;
            }
        }
       
        allAnswers = [select Name,Id,isCorrect__c 
                          from PhishingQuizAnswer__c where PhishingQuizUser__c =: user.Id];
        allCount = [select count() from PhishingQuizQuestion__c where active__c = 1];
       // scorecard = [select isCorrect__c, PhishingQuizQuestion__r.altlink__c, PhishingQuizQuestion__r.link__c from PhishingQuizAnswer__c
                        //where PhishingQuizUser__c =: user.Id];
        if (allCount > allAnswers.size()) {
           // hey! They're not done!
           PageReference pr = new PageReference(qc.quizQuestion.getUrl()+'?sId='+user.Key__c);
           pr.setRedirect(true);
           return pr;
        }
        for (PhishingQuizAnswer__c ans: allAnswers) {
            if (ans.isCorrect__c != 1){
                missedAnswers.add(ans);
                /*if (ans.false_picks__c != null) {
                    // these were their false positives
                    // if they incorrectly chose it, they don't know what it is
                    for (String s:ans.false_picks__c.split(';')){
                        incorrectTopicSet.add(s);
                    }
                }
                if (ans.missed_topics__c != null) {
                    // these were the options that they failed to get
                    for (String s:ans.missed_topics__c.split(';')){
                        incorrectTopicSet.add(s);
                    }
                }*/
            }
            else {
                correctCount +=1; //hey, they got it!
            }
        }
        incorrectTopics = new List<String>();
        for (String s: incorrectTopicSet) {
          //lame that we can't pass a Set to apex:repeat
          incorrectTopics.add(s);
        }
        
        if (user.quiz_completion__c == null) {
            user.quiz_completion__c = datetime.now();
        }
        user.quiz_score__c = numPercentCorrect();
        update user;
        return null;
        
    }
                
      public List<infowrapper> getTheInfo() {
        List<PhishingQuizAnswer__c> phish= [select isCorrect__c, PhishingQuizQuestion__r.altlink__c, PhishingQuizQuestion__r.link__c from PhishingQuizAnswer__c
                        where PhishingQuizUser__c =: user.Id];
        List<infowrapper> tmp = new List<infowrapper>();
        infowrapper x;
        Integer i =0;
        for (PhishingQuizAnswer__c p: phish) {
            if(i<=4) //bad code, but basically saying there'll only be 10 records and looping through the next set.
            {
                x = new infowrapper();
                x.altlink = p.PhishingQuizQuestion__r.altlink__c;
                x.link = p.PhishingQuizQuestion__r.link__c;
                if(p.isCorrect__c == 1)
                {
                    x.correct = 'Correct';
                }
                else
                {
                    x.correct = 'Incorrect';
                }
                tmp.add(x); //I think the call is push(), it may be append() or something
                i++;
            }
          }
          return tmp;
    }
    
          public List<infowrapper> getTheInfoNext() {
        List<PhishingQuizAnswer__c> phish= [select isCorrect__c, PhishingQuizQuestion__r.altlink__c, PhishingQuizQuestion__r.link__c from PhishingQuizAnswer__c
                        where PhishingQuizUser__c =: user.Id];
        List<infowrapper> tmp = new List<infowrapper>();
        infowrapper x;
        Integer i = 0;
        for (PhishingQuizAnswer__c p: phish) {
            i++;
            if(i>5) //bad code, but basically saying there'll only be 10 records and looping through the next set.
            {
                x = new infowrapper();
                x.altlink = p.PhishingQuizQuestion__r.altlink__c;
                x.link = p.PhishingQuizQuestion__r.link__c;
                if(p.isCorrect__c == 1)
                {
                    x.correct = 'Correct';
                }
                else
                {
                    x.correct = 'Incorrect';
                }
                tmp.add(x); //I think the call is push(), it may be append() or something
            }
          }
          return tmp;
    }

             
                

    public String getMissedTopicStyle() {
        // kill the "missed topics" section if necessary
        if (incorrectTopics.size() > 0) {
            return '';
        }
        else {
            return 'display:none;';
        }
    }

    public Long numPercentCorrect() {
        return Math.roundToLong(((Double)correctCount/allCount)*100);
    }
    
    public String getPercentCorrect() {
        return String.valueOf(numPercentCorrect())+'%';
    }

}
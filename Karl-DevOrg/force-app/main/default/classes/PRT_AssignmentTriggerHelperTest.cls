/*
* Created by - Prashant Gupta
* Test Class for PRT_AssignmentTriggerHelper
*/
@isTest
public class PRT_AssignmentTriggerHelperTest {
    @testSetup
    public static void testSetup(){ 
        List<User> userList = new List<User>();
        userList = PRT_TestDataFactory.createUsers(1,'System Administrator',true);
    }
    
    
    static testMethod void checkAllocationBeforeInsertTest(){
        List<Program__c> programListCreate = PRT_TestDataFactory.createPrograms(1,true);
        List<Program__c> programList = [SELECT Id FROM Program__c LIMIT 1];
        List<Contact> contactList = new List<Contact>(PRT_TestDataFactory.createContact(1,true));
        List<Project__c> projectList = new List<Project__c>(PRT_TestDataFactory.createProjects(1,false));
        projectList[0].Program__c = programList[0].Id;
        insert projectList;
        
        List<Milestone_PRT__c> milestoneList= new List<Milestone_PRT__c>();
        
        milestoneList = PRT_TestDataFactory.createMilestone(1,projectList[0].id,false);
        milestoneList[0].program__c =  programList[0].Id;
        insert milestoneList;
        List<Assignment__c> assignmentList1 = new List<Assignment__c>();
        assignmentList1 = PRT_TestDataFactory.createAssignment(2,projectList[0].id,milestoneList[0].id,false);
        for(Assignment__c assignment : assignmentList1){
            assignment.Start_Date__c = Date.newInstance(2019, 9, 3);
            assignment.End_Date__c = Date.newInstance(2019, 10,1);
            assignment.Percentage_of_Capacity__c = 50 ;
            assignment.Contact__c = contactList[0].Id;
            
        }
        PRT_Constants.RECURSIVE_TRIGGER_CHECK =true;
        insert assignmentList1;
        try{
            test.startTest();
            List<Assignment__c> assignmentList = new List<Assignment__c>();
            assignmentList = PRT_TestDataFactory.createAssignment(5,projectList[0].id,milestoneList[0].id,false);
            for(Assignment__c assignment : assignmentList){
                assignment.Start_Date__c = Date.newInstance(2019, 9, 3);
                assignment.End_Date__c = Date.newInstance(2019, 9,25);
                assignment.Percentage_of_Capacity__c = 50 ;
                assignment.Contact__c = contactList[0].Id;
                
            }
            PRT_Constants.RECURSIVE_TRIGGER_CHECK =true;
            insert assignmentList;
            
            
            Map<Id,List<Assignment__c>> userToAssignmentMap  = new Map<Id,List<Assignment__c>>();
            for(Assignment__c assignment : [SELECT Id, Name, Contact__c, Start_Date__c, End_Date__c, 
                                            Percentage_of_Capacity__c
                                            FROM Assignment__c 
                                            WHERE Contact__c IN :contactList
                                           ]){
                                               
                                               
                                               if(!userToAssignmentMap.containsKey(assignment.Contact__c)){
                                                   userToAssignmentMap.put(assignment.Contact__c,new List<Assignment__c>());
                                               }
                                               userToAssignmentMap.get(assignment.Contact__c).add(assignment);
                                               
                                           }
            
            for(Id userId : userToAssignmentMap.keySet()){
                for(Assignment__c assignment : userToAssignmentMap.get(userId)){
                    Decimal ContactTotalAllocation = assignment.Percentage_of_Capacity__c!=null ? assignment.Percentage_of_Capacity__c: 0;
                    for(Date d=assignment.Start_Date__c.toStartofWeek();d <=assignment.End_Date__c;d=d.addDays(7) ){                
                        if(assignment.Start_Date__c <= d && assignment.End_Date__c > d){
                            ContactTotalAllocation +=assignment.Percentage_of_Capacity__c;
                            
                            if(ContactTotalAllocation >100.00){
                                String WeeksofExceededAllocation='';
                                WeeksofExceededAllocation += d.format() + '(' + ContactTotalAllocation + ') \r\t';        
                                
                                
                            }  
                        }
                        
                    }
                }   
            }
            
            test.stopTest();
        }
        catch(Exception e){
            System.Assert(e.getMessage().contains('This Update would exceed the allocation for the weeks'));
        }
        
    }   
    
    /*Created by - Swarnima Singh Mandhata*/
    public static testMethod void InsertUpdateAssignmentWeekRecord(){
        List<Program__c> programListCreate = PRT_TestDataFactory.createPrograms(1,true);
        user u = [SELECT ID FROM USER LIMIT 1];
        List<Program__c> programList = [SELECT Id FROM Program__c LIMIT 1];
        List<Contact> contactList = new List<Contact>(PRT_TestDataFactory.createContact(1,true));
        List<Project__c> projectList = new List<Project__c>(PRT_TestDataFactory.createProjects(1,false));
        projectList[0].Project_Manager__c  = u.Id;
        projectList[0].Program__c = programList[0].Id;
        insert projectList;
         
        List<Milestone_PRT__c> milestoneList= new List<Milestone_PRT__c>();
        milestoneList = PRT_TestDataFactory.createMilestone(1,projectList[0].id,false);
        milestoneList[0].description__c = 'test description';
        insert milestoneList;
        
        List<Assignment__c> assignmentList1 = new List<Assignment__c>();
        assignmentList1 = PRT_TestDataFactory.createAssignment(1,projectList[0].id,milestoneList[0].id,false);
        
        assignmentList1[0].Start_Date__c = Date.newInstance(2019, 12, 19);
        assignmentList1[0].End_Date__c = Date.newInstance(2020,01 ,2);
        assignmentList1[0].Percentage_of_Capacity__c = 50 ;
        assignmentList1[0].Contact__c = contactList[0].Id;
        
        PRT_Constants.RECURSIVE_TRIGGER_CHECK =true;
        insert assignmentList1;
        test.startTest();
        assignmentList1[0].Start_Date__c = Date.newInstance(2019, 12, 12);
        update assignmentList1;
        test.stopTest();

        
    }
    
    
}
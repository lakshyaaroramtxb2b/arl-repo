/**
 * @author: ralph@callaway.cloud
 */
public class SC_CourseEscalations_Controller {
    
    private Id courseId;

    public SC_CourseEscalations_Controller(ApexPages.StandardController controller) {
        this.courseId = controller.getId();
    }

    public void run() {
        Database.executeBatch(new SC_EscalationBatch(courseId));
        ApexPages.addMessage(new ApexPages.Message(
            ApexPages.Severity.CONFIRM,
            'Course Escalations Started'));   
    }
}
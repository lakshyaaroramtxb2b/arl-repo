/**
 * @author Mahima Aggarwal
 * @email mahima.aggarwal@mtxb2b.com
 * @desc Test class for KARL_EvidenceUploadController
*/
@isTest
public with sharing class KARL_EvidenceUploadControllerTest {
    /**
     * @author Mahima Aggarwal
     * @email mahima.aggarwal@mtxb2b.com
     * @desc Test data setup.
    */
    @testSetup
    private static void testSetup(){
        User testUser = ARL_TestDataFactory.createExternalAuditor();
        User opsUser = ARL_TestDataFactory.createOpsAdmin();
        System.runAs(testUser){
            Account acc = new Account(Name = 'salesforce.com - ESA Office Hours');
            insert acc;
            
            Contact con = new Contact(LastName = 'test', AccountId = acc.Id,Email = 'test@gmail.com');
            insert con;
            
            Contact con2 = new Contact(LastName = 'test2', AccountId = acc.Id,Email = 'test2@gmail.com');
            insert con2;
            
            Request_Item__c reqItem = ARL_TestDataFactory.createRequestItem();
            reqItem.Primary_Scope__c = 'SS';
            reqItem.KARL_Evidence_Submission_Workflow__c = true;
            reqItem.Create_GUS_Case__c = true;
            insert reqItem;
            
            Cycle_Request_Item__c cycleReqItem = ARL_TestDataFactory.createCycleRequestItem(reqItem.id,null);
            cycleReqItem.Cycle_Request_Status__c = 'Provided To Auditor';
            cycleReqItem.Request_GUS__c = 'testWorkId';
            cycleReqItem.KARL_Update_GUS__c = true;
            cycleReqItem.Request_Tech_Details__c = 'tech details old';
            cycleReqItem.Request_Assignee__c = con.Id;
            insert cycleReqItem;
            
            KARL_Audit_Team__c auditTeam = ARL_TestDataFactory.createAuditTeam('test');
        	insert auditTeam;
            
            KARL_Auditor_Request_Item__c auditorReqItem = ARL_TestDataFactory.createAuditorRequestItem(cycleReqItem.Id,auditTeam.Id);
        	insert auditorReqItem;

            KARL_Evidence_Request__c evidenceReq = new KARL_Evidence_Request__c();
            evidenceReq.KARL_Submitted__c = false;
            evidenceReq.KARL_Cycle_ARL_Item__c = cycleReqItem.Id;
            insert evidenceReq;
            
             ContentVersion cVersion = new ContentVersion();
             cVersion.ContentLocation = 'S'; //S-Document is in Salesforce. E-Document is outside of Salesforce. L-Document is on a Social Netork.
             cVersion.PathOnClient = 'test.pdf';//File name with extention
             cVersion.Origin = 'H';//C-Content Origin. H-Chatter Origin.
             cVersion.Title = 'test.pdf';//Name of the file
             cVersion.VersionData = Blob.valueOf('UNIT.TEST');//File content
             cVersion.Status__c = 'Approved';
             Insert cVersion;
            
             ContentVersion cVersion2 = new ContentVersion();
             cVersion2.ContentLocation = 'S'; //S-Document is in Salesforce. E-Document is outside of Salesforce. L-Document is on a Social Netork.
             cVersion2.PathOnClient = 'test.pdf';//File name with extention
             cVersion2.Origin = 'H';//C-Content Origin. H-Chatter Origin.
             cVersion2.Title = 'test.pdf';//Name of the file
             cVersion2.VersionData = Blob.valueOf('UNIT.TEST');//File content
             cVersion2.Status__c = 'Approved';
             Insert cVersion2;
            
             //After saved the Content Verison, get the ContentDocumentId
             Id conDocument = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:cVersion.Id LIMIT 1].ContentDocumentId;
             
             //After saved the Content Verison, get the ContentDocumentId
             Id conDocument2 = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:cVersion2.Id LIMIT 1].ContentDocumentId;
            
             //Insert ContentDocumentLink
             ContentDocumentLink cDocLink = new ContentDocumentLink();
             cDocLink.ContentDocumentId = conDocument;//Add ContentDocumentId
             cDocLink.LinkedEntityId = evidenceReq.Id;//Add attachment parentId
             cDocLink.ShareType = 'V';//V - Viewer permission. C - Collaborator permission. I - Inferred permission.
             cDocLink.Visibility = 'InternalUsers';//AllUsers, InternalUsers, SharedUsers
             Insert cDocLink;
            
            //Insert ContentDocumentLink
             ContentDocumentLink cDocLink2 = new ContentDocumentLink();
             cDocLink2.ContentDocumentId = conDocument2;//Add ContentDocumentId
             cDocLink2.LinkedEntityId = evidenceReq.Id;//Add attachment parentId
             cDocLink2.ShareType = 'V';//V - Viewer permission. C - Collaborator permission. I - Inferred permission.
             cDocLink2.Visibility = 'InternalUsers';//AllUsers, InternalUsers, SharedUsers
             Insert cDocLink2;
        }
    }
    
    /**
     * @author Mahima Aggarwal
     * @email mahima.aggarwal@mtxb2b.com
     * @desc Utility method to get list of FileWrapper objects
    */
    public static List<KARL_EvidenceUploadController.FileWrapper> getFileWrapperList(String idField){ 
    	return KARL_EvidenceUploadController.getContentVersionRecords(idField);
    }

    /**
     * @author Mahima Aggarwal
     * @email mahima.aggarwal@mtxb2b.com
     * @desc Test getContentVersionRecords()
    */
    @istest
    public static void testGetEvidenceContentVersionRecords(){ 
        User usr = [Select id from User where FirstName = 'External_Auditor'];
        System.runAs(usr){
            KARL_Evidence_Request__c evidenceReq = [SELECT Id, KARL_Submitted__c 
                                                    FROM KARL_Evidence_Request__c
                                                    LIMIT 1];
            Test.startTest();
                List<KARL_EvidenceUploadController.FileWrapper> fileWrpList = (List<KARL_EvidenceUploadController.FileWrapper>)KARL_EvidenceUploadController.getEvidenceContentVersionRecords(evidenceReq.Id).get('fileWrapperList');
            Test.stopTest();
          
            System.assertEquals(fileWrpList.size() ,2);                                       
        }
    }
    
    /**
     * @author Mahima Aggarwal
     * @email mahima.aggarwal@mtxb2b.com
     * @desc Test submitContentVersionRecords()
    */
    @IsTest
    public static void testSubmitContentVersionRecords(){ 
        User usr = [Select id from User where FirstName = 'External_Auditor'];
        System.runAs(usr){
            KARL_Evidence_Request__c evidenceReq = [SELECT Id, KARL_Submitted__c 
                                                    FROM KARL_Evidence_Request__c
                                                    LIMIT 1];
            Test.startTest();
                List<KARL_EvidenceUploadController.FileWrapper> fileWrpList = getFileWrapperList(evidenceReq.Id);
                String wrpListString = JSON.serialize(fileWrpList);
                try{//writing try catch so that we dont have to set (SeeAllData=true) 
                    KARL_EvidenceUploadController.submitContentVersionRecords(wrpListString,evidenceReq.Id);
                }catch(Exception e){}
                
            Test.stopTest();
            KARL_Evidence_Request__c resultEvidenceReq = [SELECT Id, KARL_Submitted__c 
                                                    FROM KARL_Evidence_Request__c
                                                    LIMIT 1];
            //System.assertEquals(resultEvidenceReq.KARL_Submitted__c ,true);                                       
        }
    }

    /**
     * @author Mahima Aggarwal
     * @email mahima.aggarwal@mtxb2b.com
     * @desc Test updateDocument()
    */
    @istest
    public static void testupdateDocument(){ 
        User usr = [Select id from User where FirstName = 'External_Auditor'];
        System.runAs(usr){
            List<String> docIds = new List<String>();
            Id conDocument = [SELECT Id FROM ContentDocument LIMIT 1].Id;
            docIds.add(conDocument);
            KARL_Evidence_Request__c evidenceReq = [SELECT Id, KARL_Submitted__c 
                                                    FROM KARL_Evidence_Request__c
                                                    LIMIT 1];
            Test.startTest();
                KARL_EvidenceUploadController.updateDocument(docIds,evidenceReq.id);
            Test.stopTest();

            ContentDocumentLink cdl =  [SELECT Id, LinkedEntityId, ContentDocumentId, IsDeleted, SystemModstamp, ShareType, Visibility 
                                        FROM ContentDocumentLink 
                                        WHERE ContentDocumentId IN : docIds
                                        LIMIT 1];
            System.assertEquals(cdl.ShareType ,'I');
        }
    }
    
    /**
     * @author Mahima Aggarwal
     * @email mahima.aggarwal@mtxb2b.com
     * @desc Test getContentDistributionForFile()
    */
    @istest
    public static void testGetContentDistributionForFile(){ 
        KARL_Evidence_Request__c evidenceReq = [SELECT Id, KARL_Submitted__c 
                                                    FROM KARL_Evidence_Request__c
                                                    LIMIT 1];
        String evidenceId = evidenceReq.id;
        User usr = [Select id from User where FirstName = 'External_Auditor'];
        System.runAs(usr){
            Id conDocument = [SELECT Id FROM ContentDocument LIMIT 1].Id;
            Test.startTest();
                ContentDistribution conDis = KARL_EvidenceUploadController.getContentDistributionForFile(conDocument,evidenceId);
            Test.stopTest();

            System.assert(conDis.DistributionPublicUrl != null);
        }
    }
    
    /**
     * @author Mahima Aggarwal
     * @email mahima.aggarwal@mtxb2b.com
     * @desc Test submitReturnToEngineer()
    */
    @istest
    public static void testSubmitReturnToEngineer(){ 
        User usr = [Select id from User where FirstName = 'External_Auditor'];
        System.runAs(usr){
            KARL_Evidence_Request__c evidenceReq = [SELECT Id, KARL_Submitted__c 
                                                    FROM KARL_Evidence_Request__c
                                                    LIMIT 1];
            Test.startTest();
                List<KARL_EvidenceUploadController.FileWrapper> fileWrpList = getFileWrapperList(evidenceReq.Id);
                String wrpListString = JSON.serialize(fileWrpList);
            try{
                KARL_EvidenceUploadController.submitReturnToEngineer(wrpListString,evidenceReq.Id);
            }catch(exception e){
                System.debug('Error: '+ e.getMessage());
            }
                
            Test.stopTest();
            KARL_Evidence_Request__c resultEvidenceReq = [SELECT Id, KARL_Submitted__c 
                                                    FROM KARL_Evidence_Request__c
                                                    LIMIT 1];
            System.assertEquals(resultEvidenceReq.KARL_Submitted__c ,false);                                       
        }
    }
    
    /**
     * @author Mahima Aggarwal
     * @email mahima.aggarwal@mtxb2b.com
     * @desc Test submitProvidedToAuditor()
    */
    @istest
    public static void testSubmitProvidedToAuditor(){ 
        User usr = [Select id from User where FirstName = 'External_Auditor'];
        System.runAs(usr){
            KARL_Evidence_Request__c evidenceReq = [SELECT Id, KARL_Submitted__c 
                                                    FROM KARL_Evidence_Request__c
                                                    LIMIT 1];
            Test.startTest();
                List<KARL_EvidenceUploadController.FileWrapper> fileWrpList = getFileWrapperList(evidenceReq.Id);
            	fileWrpList[0].status = 'Rejected';
                String wrpListString = JSON.serialize(fileWrpList);
            try{
                 KARL_EvidenceUploadController.submitProvidedToAuditor(wrpListString,evidenceReq.Id);
            }catch(exception e){
                System.debug('Error: '+ e.getMessage());
            }
               
            Test.stopTest();
            fileWrpList[0].status = 'Approved';
            KARL_Evidence_Request__c resultEvidenceReq = [SELECT Id, KARL_Submitted__c,
                                                          KARL_Cycle_ARL_Item__c,KARL_Cycle_ARL_Item__r.Cycle_Request_Status__c
                                                          FROM KARL_Evidence_Request__c
                                                          LIMIT 1];
            System.assertEquals(resultEvidenceReq.KARL_Cycle_ARL_Item__r.Cycle_Request_Status__c ,'Provided to Auditor');                                       
        }
    }
    
    /**
     * @author Mahima Aggarwal
     * @email mahima.aggarwal@mtxb2b.com
     * @desc Test revokeFromAuditor()
    */
    @istest
    public static void testRevokeFromAuditor(){ 
        User usr = [Select id from User where FirstName = 'Ops_Admin'];
        System.runAs(usr){
            KARL_Evidence_Request__c evidenceReq = [SELECT Id, KARL_Submitted__c 
                                                    FROM KARL_Evidence_Request__c
                                                    LIMIT 1];
            Test.startTest();
                KARL_EvidenceUploadController.revokeFromAuditor(evidenceReq.Id);
            Test.stopTest();
            KARL_Evidence_Request__c resultEvidenceReq = [SELECT Id, KARL_Submitted__c,
                                                          KARL_Cycle_ARL_Item__c,KARL_Cycle_ARL_Item__r.Cycle_Request_Status__c
                                                          FROM KARL_Evidence_Request__c
                                                          LIMIT 1];
            System.assertEquals(resultEvidenceReq.KARL_Cycle_ARL_Item__r.Cycle_Request_Status__c ,'In Progress');                                       
        }
    }
    
    /**
     * @author Mahima Aggarwal
     * @email mahima.aggarwal@mtxb2b.com
     * @desc Test getCycleReqItemContentVersionRecords()
    */
    @istest
    public static void testGetCycleReqItemContentVersionRecords(){ 
        User usr = [Select id from User where FirstName = 'External_Auditor'];
        System.runAs(usr){
            KARL_Evidence_Request__c evidenceReq = [SELECT Id, KARL_Submitted__c,KARL_Cycle_ARL_Item__c 
                                                    FROM KARL_Evidence_Request__c
                                                    LIMIT 1];
            KARL_Auditor_Request_Item__c auditorReqItem = [SELECT Id FROM KARL_Auditor_Request_Item__c LIMIT 1];
            Test.startTest();
                List<KARL_EvidenceUploadController.FileWrapper> fileWrpList = (List<KARL_EvidenceUploadController.FileWrapper>)KARL_EvidenceUploadController.getCycleReqItemContentVersionRecords(evidenceReq.KARL_Cycle_ARL_Item__c).get('fileWrapperList');
            	List<KARL_EvidenceUploadController.FileWrapper> fileWrpList2 = (List<KARL_EvidenceUploadController.FileWrapper>)KARL_EvidenceUploadController.getCycleReqItemContentVersionRecords(auditorReqItem.Id).get('fileWrapperList');
            Test.stopTest();
            System.assertEquals(fileWrpList.size() ,0);  
            System.assertEquals(fileWrpList2.size() ,0);  
        }
    }
    
    /**
     * @author Mahima Aggarwal
     * @email mahima.aggarwal@mtxb2b.com
     * @desc Test deleteFile()
    */
    @istest
    public static void testDeleteFile(){ 
        User usr = [Select id from User where FirstName = 'External_Auditor'];
        System.runAs(usr){
            Id conDocument = [SELECT Id FROM ContentDocument LIMIT 1].Id;
            KARL_Evidence_Request__c evidenceReq = [SELECT Id, KARL_Submitted__c 
                                                    FROM KARL_Evidence_Request__c
                                                    LIMIT 1];
            Test.startTest();
                KARL_EvidenceUploadController.deleteFile(conDocument,evidenceReq.id);
            try{
                KARL_EvidenceUploadController.deleteFile(null,null);
            }catch(Exception e){}
            Test.stopTest();
        }
    }
    @istest
    public static void sendEmailOnEvidenceUploadTest(){ 
        KARL_Evidence_Request__c evidenceReq = [SELECT Id, KARL_Submitted__c 
                                                    FROM KARL_Evidence_Request__c
                                                    LIMIT 1];
        test.startTest();
        	KARL_EvidenceUploadController.sendEmailOnEvidenceUpload(evidenceReq.id);
        test.stopTest();
        
        }
 }
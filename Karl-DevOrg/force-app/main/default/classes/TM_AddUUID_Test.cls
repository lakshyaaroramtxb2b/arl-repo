@isTest(seeAllData=false)
public class TM_AddUUID_Test {
    @isTest static void testInsertWithoutUUID() {
      TM_AbstractObjective__c ao = new TM_AbstractObjective__c(Name='Do Stuff', UUID__c=NULL);
      Test.startTest();
      Database.SaveResult result = Database.insert(ao, true);
      List<TM_AbstractObjective__c> query = [SELECT Id, UUID__c FROM TM_AbstractObjective__c WHERE Id = :ao.Id];
      Test.stopTest();
      
      System.assert(result.isSuccess());
      System.assert(ao.UUID__c == NULL);
      System.assert(query[0].UUID__c != NULL); // was updated
    }
    
    @isTest static void testInsertWithUUID() {
      String testUUID = 'ffffffff-ffff-4fff-bfff-ffffffffffff';
      TM_AbstractObjective__c ao = new TM_AbstractObjective__c(Name='Do Stuff', UUID__c=testUUID);
      Test.startTest();
      Database.SaveResult result = Database.insert(ao, true);
      List<TM_AbstractObjective__c> query = [SELECT Id, UUID__c FROM TM_AbstractObjective__c WHERE Id = :ao.Id];
      Test.stopTest();
      
      System.assert(result.isSuccess());
      System.assert(ao.UUID__c == testUUID); // had one to begin with
      System.assert(query[0].UUID__c == testUUID); // not modified
    }
}
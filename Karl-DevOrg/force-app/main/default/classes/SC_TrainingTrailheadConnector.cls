public class SC_TrainingTrailheadConnector implements SC_TrainingBase.ProviderConnector {
    
    public static Id ORG_62_18_CHAR_ID = '00D000000000062EAA';
    
    private static final String API_TOKEN = [SELECT API_Token__c FROM Trailhead_Validity__c LIMIT 1].API_Token__c;

    public static String[] sync62OrgIds = new String[0]; // when set, sync is just run for specified 62 org ids

    public class SC_TrainingTrailheadConnectorException extends Exception {}
    
    SC_Training_Provider__c provider;
    
    public SC_TrainingTrailheadConnector(SC_Training_Provider__c prov) {
        provider = prov;
    }

    public Integer syncCompleted(SC_TrainingBase.CoursesCompletedState[] states) {
        /* Sync courses completed on or after provided date from the training provider to this org
         *
         * This will assume that course progress cannot go backwards. With Trailhead, a module appears 
         * for a user after they submit the first quiz
         *
         */
        
        RecordType voluntaryEnrollment = [SELECT Id FROM RecordType WHERE SObjectType='Training_Course_Taken__c' AND DeveloperName = 'Voluntary_Enrollment'];
        
        Training_Course_Taken__c[] toUpdate = new Training_Course_Taken__c[]{};
        Training_Course_Taken__c[] toCreate = new Training_Course_Taken__c[]{};
        
        String userSearchUrl = '/v1/search/users?';
        String body = '{}';
        if (provider.Last_Completed_Course_Sync__c != null) {
            body = '{"since":"'+provider.Last_Completed_Course_Sync__c.formatGMT('yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'')+'"}';
        }
        if (!sync62OrgIds.isEmpty()) {
            Map<String, Object> reqObj = new Map<String, Object>();
            reqObj.put('sfids', sync62OrgIds);
            body = JSON.serialize(reqObj);    
        }
        
        // Get the list of tracking courses we're tracking as being infosec relevant
        Map<String, Id> trackedCourseExtIds = new Map<String, Id>();
        for (Training_Course__c c : [
            SELECT Provider_ID__c 
            FROM Training_Course__c 
            WHERE Provider_ID__c != null 
                AND Provider__c = 'Trailhead'
                AND Course_Level__c = 'Module'
                AND Tracked__c = true
        ]) { 
            trackedCourseExtIds.put(c.Provider_ID__c, c.Id);
        }
        for (Training_Course_Relationship__c rel : [
            SELECT m.Provider_Id__c 
            FROM Training_Course_Relationship__c rel,
                rel.Related_Module__r m
            WHERE Related_Path__r.Tracked__c = true
                AND m.Provider__c = 'Trailhead'
                AND m.Provider_Id__c != null
                AND m.Id NOT IN :trackedCourseExtIds.values()
        ]) {
            trackedCourseExtIds.put(rel.Related_Module__r.Provider_Id__c, rel.Related_Module__c);
        }

        // 62orgId : Module[]
        Map<String, Module[]> toProcess = new Map<String, Module[]>{};
            
        for (CoursesCompletedState state : (CoursesCompletedState[]) states) {
            
            HttpResponse res = sendRequest(userSearchUrl + 'offset='+String.valueOf(state.offset), body);
            if (res.getStatusCode()==400 && res.getBody() == '{"message":"Search did not retrieve any records. Please try again."}') {
                continue; // Trailhead appears to return a 400 when no records are found, which is dumb
            }
            UserDataResponse r;
            Try {
            	r = (UserDataResponse) JSON.deserialize(res.getBody(), UserDataResponse.class);
            } catch (exception e) {continue;} // silently fail if bad json data
            if (r == null || r.data == null) continue;
            for (UserData ud : r.data) {
                String user62OrgId = null;
                Set<String> user62OrgIds = new Set<String>();
                for (SfdcIds orgIdObj : ud.salesforce_ids) {
                    if (orgIdObj.org_id == ORG_62_18_CHAR_ID) {
                        user62OrgIds.add(orgIdObj.user_id);
                    }
                }
                // trailhead may return multiple org62 ids for a user but only one corresponds to the real user in security org
                if (!user62OrgIds.isEmpty()) {
                    if (user62OrgIds.size() == 1) {
                        for (String s : user62OrgIds) {
                            user62OrgId = s;
                        }                            
                    } else {  
                        for (Contact c : [SELECT Org62_User_ID__c FROM Contact 
                                          WHERE Is_Active__c = True
                                          AND Org62_User_ID__c IN: user62OrgIds LIMIT 1]) {
                            user62OrgId = c.Org62_User_ID__c;   
                        }
                    }
                }
                
                if (user62OrgId == null) {
                    // not sure why they'd be in trailhead if they don't have a 62org ID
                    continue;
                }
                
                for (Module mod : ud.modules) {
                    if (!trackedCourseExtIds.containsKey(String.valueOf(mod.api_name))) { 
                        // not infosec relevant
                        continue;
                    }
                    if (!toProcess.containsKey(user62OrgId)) { toProcess.put(user62OrgId, new Module[]{}); }
                    toProcess.get(user62OrgId).add(mod);
                }
            }
        }
 
        Map<Id, Contact> referencedUsers = new Map<Id, Contact>();
        for (Contact c : [SELECT Id, Email, Org62_User_ID__c,(SELECT Id, Date_Completed__c, Date_Started__c, Last_Import_Date__c, Progress__c, Status__c, Training_Course__c, Training_Course__r.Provider_ID__c FROM Training_Courses_Taken__r WHERE Training_Course__r.Provider__c = 'Trailhead')
                          FROM Contact WHERE Org62_User_ID__c != null AND Org62_User_ID__c IN: toProcess.keySet()]) {
            referencedUsers.put(c.Org62_User_ID__c, c);
        }
        
        // We go through referencedUsers and not retrieved users because it's possible a retrieved user
        // references a user for which we do not have a contact yet. This integration doesn't actually
        // have enough information to create a skeleton contact so we have to skip them.
        for (Id org62Id : referencedUsers.keySet()) {
            Contact contact = referencedUsers.get(org62Id);
            Module[] mods = new List<Module>(toProcess.get(org62Id));
            if (mods.size() == 0) { continue; }
            
            for (Module mod : mods) {
                // While mod.api_name exists, it's actually for the Trailhead equivalent of 
                // Training_Course__c and not Training_Course_Taken__c. Trailhead seems to merge
                // completion-related fields directly into this object instead of exposing a separate,
                // uniquely identified record related to an individual's participation in a module.
                // Since there could conceivably be dupes, we'll match on Training_Course__r.Provider_ID__c and 
                // Training_Course_Taken__c.Date_Started__c (if not null)
                Training_Course_Taken__c match = null;
                for (Training_Course_Taken__c taken : contact.Training_Courses_Taken__r) {
                    if (taken.Training_Course__r.Provider_ID__c == String.valueOf(mod.api_name)) {
                        if ((taken.Date_Started__c != null) && (taken.Date_Started__c != mod.first_attempted_at.dateGMT())) {
                            // No match. Maybe they've taken this module multiple times
                            System.debug('Likely duplicate course');
                            System.debug(taken);
                            continue;
                        }
                        match = taken;
                        break;
                    }    
                }
                if (match != null) {
                    toUpdate.add(mapTrainingCourseTakenFields(match, mod));
                } else {
                    match = new Training_Course_Taken__c();
                    match.recordTypeId = voluntaryEnrollment.Id;
                    match.Training_Course__c = trackedCourseExtIds.get(String.valueOf(mod.api_name));
                    match.Contact__c = contact.Id;
                    match.Attendee_Email__c = contact.email;
                    toCreate.add(mapTrainingCourseTakenFields(match, mod));
                }
                
            }
        }
        
        Database.update(toUpdate, false);
        Database.insert(toCreate, false);
        
        return 0;
    }

    private Training_Course_Taken__c mapTrainingCourseTakenFields(Training_Course_Taken__c course, Module mod) {
        if (mod.first_attempted_at != null) {
            course.Date_Started__c = mod.first_attempted_at.dateGMT();
        }
        if (mod.finished_at != null) {
            course.Date_Completed__c = mod.finished_at.dateGMT();
        }
        
        if (mod.completed == true) {
            course.Status__c = 'Course Completed';
            course.Progress__c = 100;
        } else if (mod.first_attempted_at != null) {
            // Trailhead doesn't even create the record until the user submits the first quiz
            course.Progress__c = 50;
            course.Status__c = 'Course Started';
        } else {
            course.Progress__c = 0;
        }
        course.Last_Import_Date__c = Datetime.now().dateGMT();
        return course;
    }
    public HttpResponse sendRequest(String unqualifiedUrl) {
        /*
         *
         * Accept an unqualifed URL to trailhead (e.g. /v1/modules) and return an HttpRequest.
         * This exists to abstract away some of the auth and settings
         */
        HttpRequest req = new HttpRequest();
        req.setTimeout(100000);
        if (!unqualifiedUrl.startsWith('/')) { unqualifiedUrl = '/'+unqualifiedUrl; }
        req.setEndpoint('callout:Trailhead_Org62'+unqualifiedUrl);
        req.setMethod('GET');
        req.setHeader('Content-Type', 'application/json; charset=utf-8');
        req.setHeader('X-Api-Key', API_TOKEN);
        
        Integer RETRY_COUNT = 5; // TODO: Move this to a provider field if necessary
        Http http = new Http();
        HTTPResponse res;
        CalloutException ex;
        for (Integer i=1; i<=RETRY_COUNT; i++) {
            try {
                res = http.send(req);
                if (math.mod(res.getStatusCode(), 500) > 100) {
                    break;
                } else {
                    if (res.getStatusCode() != 200) {
                        System.debug('Bad status '+res.getStatusCode()+': '+res.getBody());
                    }
                }
            } catch (CalloutException e) {
                ex = e;
                System.debug(e.getMessage());   
            }
        }
        if (res != null) {
            return res;
        } else {
            throw ex;
        }
    }
    
    public HttpResponse sendRequest(String unqualifiedUrl, String jsonBody) {
        /*
         *
         * POST version of the above.
         * 
         * Accept a JSON string and n unqualifed URL to trailhead (e.g. /v1/modules) and 
         * return an HttpRequest.
         * This exists to abstract away some of the auth and settings
         */
        HttpRequest req = new HttpRequest();
        req.setTimeout(100000);
        if (!unqualifiedUrl.startsWith('/')) { unqualifiedUrl = '/'+unqualifiedUrl; }
        req.setEndpoint('callout:Trailhead_Org62'+unqualifiedUrl);
        req.setMethod('POST');
        req.setBody(jsonBody);
        req.setHeader('Content-Type', 'application/json; charset=utf-8');
        req.setHeader('X-Api-Key', API_TOKEN);
        
        Integer RETRY_COUNT = 5; // TODO: Move this to a provider field if necessary
        Http http = new Http();
        HTTPResponse res;
        CalloutException ex;
        for (Integer i=1; i<=RETRY_COUNT; i++) {
            try {
                res = http.send(req);
                if (math.mod(res.getStatusCode(), 500) > 100) {
                    break;
                } else {
                    if (res.getStatusCode() != 200) {
                        System.debug('Bad status '+res.getStatusCode()+': '+res.getBody());
                    }
                }
            } catch (CalloutException e) {
                ex = e;
                System.debug(e.getMessage());   
            }
        }
        if (res != null) {
            return res;
        } else {
            throw ex;
        }
    }
    
    public Iterable<SC_TrainingBase.CoursesCompletedState> getCoursesCompletedStateIterator() {
        // Trailhead support "since" for courses so we always need to get all of them
        // Fortunately, it does tell us the total records in the first request. It also paginates in 
        // groups of 50 automatically. The "50" used below is for offset and not limit.
        
        Integer OFFSET_SIZE = 50; // must match implicit page size of 50
        
        String userSearchUrl = '/v1/search/users?limit=1';
        String body = '{}';
        if (provider.Last_Completed_Course_Sync__c != null) {
            body = '{"since":"'+provider.Last_Completed_Course_Sync__c.formatGMT('yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'')+'"}';
        }
        if (!sync62OrgIds.isEmpty()) {
            Map<String, Object> reqObj = new Map<String, Object>();
            reqObj.put('sfids', sync62OrgIds);
            body = JSON.serialize(reqObj);    
        }
        HttpResponse res = sendRequest(userSearchUrl, body);
		if (res.getStatusCode() >= 500) return null;       
        if (res == null) return null;
system.debug('>>> body: ' + res.getBody());
        UserDataResponse r = (UserDataResponse) JSON.deserialize(res.getBody(), UserDataResponse.class);
        if (r == null || r.total_count == null) return null;
        Decimal operationsDec = r.total_count / OFFSET_SIZE;
        Integer actualOps = Math.ceil(operationsDec).intValue();

        CoursesCompletedState[] states = new CoursesCompletedState[] {};
        for (Integer offset=0; offset<=actualOps*OFFSET_SIZE;offset+=OFFSET_SIZE) {
            CoursesCompletedState st = new CoursesCompletedState();
            st.offset=offset;
            states.add(st);
        }
        return (Iterable<SC_TrainingBase.CoursesCompletedState>) states;
    }

    public class CoursesCompletedResult implements SC_TrainingBase.CoursesCompletedResult {}
    
    abstract class TrailheadStateBase {
        public Integer offset;
    }
    
    public class CoursesCompletedState extends TrailheadStateBase implements SC_TrainingBase.CoursesCompletedState {
        public List<SC_TrainingBase.CoursesCompletedResult> getResults() { return null; }
    }
    
    private class SFDCIds {
        private String org_id;
        private String user_id;
    }
    private class Module {
        // This combines fields from both module and the module 
        // object nested in users
        private Integer id;
        private String api_name;
        private String title;
        private String status;
        private Datetime first_attempted_at;
        private Datetime last_attempted_at;
        private Datetime finished_at;
        private Boolean completed;
        
        private Datetime created_at;
        private Datetime updated_at;
        private String badge_icon;
        private String badge_title;
        private Datetime built_date;
        private String web_url;
        private Boolean archived;
        private String type;
    }
    private class UserData {
        private String username;
        private String email;
        private String first_name;
        private String last_name;
        
        SFDCIds[] salesforce_ids;
        Module[] modules;
    }
    private class ModuleResponse {
        Integer total_count;
        Module[] data;
    }
    
    private class UserDataResponse {
        Integer total_count;
        UserData[] data;
    }
}
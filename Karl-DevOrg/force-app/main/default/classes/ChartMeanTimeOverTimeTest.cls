@isTest
public with sharing class ChartMeanTimeOverTimeTest {

    static testMethod void testCheckAccess() {
        // this would throw an exception if bad
        new ChartMeanTimeOverTime().checkAccess('Case',new String[] {'CreatedDate'});
    }
    
    static testMethod void testSameHour() {
        Datetime now = Datetime.now();
        Datetime ret = new ChartMeanTimeOverTime().sameHour(now);
        System.assert(now.yearGmt() == ret.yearGmt());
        System.assert(now.monthGmt() == ret.monthGmt());
        System.assert(now.dayGmt() == ret.dayGmt());
        System.assert(now.hourGmt() == ret.hourGmt());
        
    }
    
    static testMethod void testSameDay() {
        Datetime now = Datetime.now();
        Datetime ret = new ChartMeanTimeOverTime().sameDay(now);
        System.assert(now.yearGmt() == ret.yearGmt());
        System.assert(now.monthGmt() == ret.monthGmt());
        System.assert(now.dayGmt() == ret.dayGmt());
        System.assert(ret.hourGmt() == 12);  
    }
    
    static testMethod void testLastDateOfYearAtNoon() {
        // test day 6/14/2013 6:00 am GMT
        Datetime testDay = Datetime.newInstanceGmt(2013,6,14,6,0,0);
        Datetime ret = new ChartMeanTimeOverTime().lastDateOfYearAtNoon(testDay);
        System.assert(ret.yearGmt() == 2013);
        System.assert(ret.dayOfYearGmt() == 365);
        System.assert(ret.hourGmt() == 12);
    
    }
    
    static testMethod void testLastDateOfMonthAtNoon() {
        // test day 6/14/2013 6:00 am GMT. 30 Days in June
        Datetime testDay = Datetime.newInstanceGmt(2013,6,14,6,0,0);
        Datetime ret = new ChartMeanTimeOverTime().lastDateOfMonthAtNoon(testDay);
        System.assert(ret.yearGmt() == 2013);
        System.assert(ret.monthGmt() == testDay.monthGmt());
        System.assert(ret.dayGmt() == 30);
        
        // test day 2/14/2013 6:00 am GMT. 28 Days in Feb
        testDay = Datetime.newInstanceGmt(2013,2,14,6,0,0);
        ret = new ChartMeanTimeOverTime().lastDateOfMonthAtNoon(testDay);
        System.assert(ret.yearGmt() == 2013);
        System.assert(ret.monthGmt() == testDay.monthGmt());
        System.assert(ret.dayGmt() == 28);
        
        // test day 12/14/2013 6:00 am GMT. 31 Days in Dec
        testDay = Datetime.newInstanceGmt(2013,12,14,6,0,0);
        ret = new ChartMeanTimeOverTime().lastDateOfMonthAtNoon(testDay);
        System.assert(ret.yearGmt() == 2013);
        System.assert(ret.monthGmt() == testDay.monthGmt());
        System.assert(ret.dayGmt() == 31);
    }
    
    static testMethod void testClosestSaturdayAtNoon() {
        // Sat, June 15th at noon
        Datetime target = Datetime.newInstanceGmt(2013,6,15,12,0,0);
        
        // test day Sat 6/15/2013 6:00 am GMT. 
        Datetime testDay = Datetime.newInstanceGmt(2013,6,15,6,0,0);
        Datetime ret = new ChartMeanTimeOverTime().closestSaturdayAtNoon(testDay);
        System.assert(ret.yearGmt() == target.yearGmt());
        System.assert(ret.monthGmt() == target.monthGmt());
        System.assert(ret.dayGmt() == target.dayGmt());
        System.assert(ret.hourGmt() == target.hourGmt());
        System.assert(ret.minuteGmt() == target.minuteGmt());
        System.assert(ret.minuteGmt() == target.secondGmt());
        
        // test day Friday 6/14/2013 6:00 am GMT. 
        testDay = Datetime.newInstanceGmt(2013,6,14,6,0,0);
        ret = new ChartMeanTimeOverTime().closestSaturdayAtNoon(testDay);
        System.assert(ret.yearGmt() == target.yearGmt());
        System.assert(ret.monthGmt() == target.monthGmt());
        System.assert(ret.dayGmt() == target.dayGmt());
        System.assert(ret.hourGmt() == target.hourGmt());
        System.assert(ret.minuteGmt() == target.minuteGmt());
        System.assert(ret.minuteGmt() == target.secondGmt());
        
        // test day Thu 6/13/2013 6:00 am GMT. 
        testDay = Datetime.newInstanceGmt(2013,6,13,6,0,0);
        ret = new ChartMeanTimeOverTime().closestSaturdayAtNoon(testDay);
        System.assert(ret.yearGmt() == target.yearGmt());
        System.assert(ret.monthGmt() == target.monthGmt());
        System.assert(ret.dayGmt() == target.dayGmt());
        System.assert(ret.hourGmt() == target.hourGmt());
        System.assert(ret.minuteGmt() == target.minuteGmt());
        System.assert(ret.minuteGmt() == target.secondGmt());
        
        // test day Wed 6/12/2013 6:00 am GMT. 
        testDay = Datetime.newInstanceGmt(2013,6,12,6,0,0);
        ret = new ChartMeanTimeOverTime().closestSaturdayAtNoon(testDay);
        System.assert(ret.yearGmt() == target.yearGmt());
        System.assert(ret.monthGmt() == target.monthGmt());
        System.assert(ret.dayGmt() == target.dayGmt());
        System.assert(ret.hourGmt() == target.hourGmt());
        System.assert(ret.minuteGmt() == target.minuteGmt());
        System.assert(ret.minuteGmt() == target.secondGmt());
        
        // test day Tue 6/11/2013 6:00 am GMT. 
        testDay = Datetime.newInstanceGmt(2013,6,11,6,0,0);
        ret = new ChartMeanTimeOverTime().closestSaturdayAtNoon(testDay);
        System.assert(ret.yearGmt() == target.yearGmt());
        System.assert(ret.monthGmt() == target.monthGmt());
        System.assert(ret.dayGmt() == target.dayGmt());
        System.assert(ret.hourGmt() == target.hourGmt());
        System.assert(ret.minuteGmt() == target.minuteGmt());
        System.assert(ret.minuteGmt() == target.secondGmt());
        
        // test day Mon 6/10/2013 6:00 am GMT. 
        testDay = Datetime.newInstanceGmt(2013,6,10,6,0,0);
        ret = new ChartMeanTimeOverTime().closestSaturdayAtNoon(testDay);
        System.assert(ret.yearGmt() == target.yearGmt());
        System.assert(ret.monthGmt() == target.monthGmt());
        System.assert(ret.dayGmt() == target.dayGmt());
        System.assert(ret.hourGmt() == target.hourGmt());
        System.assert(ret.minuteGmt() == target.minuteGmt());
        System.assert(ret.minuteGmt() == target.secondGmt());
        
        // test day Sun 6/9/2013 6:00 am GMT. 
        testDay = Datetime.newInstanceGmt(2013,6,9,6,0,0);
        ret = new ChartMeanTimeOverTime().closestSaturdayAtNoon(testDay);
        System.assert(ret.yearGmt() == target.yearGmt());
        System.assert(ret.monthGmt() == target.monthGmt());
        System.assert(ret.dayGmt() == target.dayGmt());
        System.assert(ret.hourGmt() == target.hourGmt());
        System.assert(ret.minuteGmt() == target.minuteGmt());
        System.assert(ret.minuteGmt() == target.secondGmt());
        
        // test day Friday 6/14/2013 6:00 am GMT. 
        testDay = Datetime.newInstanceGmt(2013,6,14,6,0,0);
        ret = new ChartMeanTimeOverTime().closestSaturdayAtNoon(testDay);
        System.assert(ret.yearGmt() == target.yearGmt());
        System.assert(ret.monthGmt() == target.monthGmt());
        System.assert(ret.dayGmt() == target.dayGmt());
        System.assert(ret.hourGmt() == target.hourGmt());
        System.assert(ret.minuteGmt() == target.minuteGmt());
        System.assert(ret.minuteGmt() == target.secondGmt());
        
        // test day Friday 6/14/2013 6:00 am GMT. 
        testDay = Datetime.newInstanceGmt(2013,6,14,6,0,0);
        ret = new ChartMeanTimeOverTime().closestSaturdayAtNoon(testDay);
        System.assert(ret.yearGmt() == target.yearGmt());
        System.assert(ret.monthGmt() == target.monthGmt());
        System.assert(ret.dayGmt() == target.dayGmt());
        System.assert(ret.hourGmt() == target.hourGmt());
        System.assert(ret.minuteGmt() == target.minuteGmt());
        System.assert(ret.minuteGmt() == target.secondGmt());
    }
    
    public static testMethod void testDateBucketize() {
        Datetime now = Datetime.now();
        Datetime lastMinute = now.addMinutes(-1);
        if (lastMinute.hourGmt() != now.hourGmt()) {
            // subtracking a minute put it into a different hour. Let's go the other way
            lastMinute = now.addMinutes(1);
        }
        Datetime lastHour = now.addHours(-1);
        if (lastHour.dayGmt() != now.dayGmt()) {
            // subtracking an hour put it into a different day. Let's go the other way
            lastHour = now.addHours(1);
        }
        Datetime yesterday;
        if (now.dayGmt()!=0) {
            yesterday = now.addDays(-1);
        } else {
            yesterday = now.addDays(1);
        }

        Datetime lastWeek = now.addDays(-7);
        // I don't know what happens if we do addMonths(-1) and the previous month has fewer days
        // that today's day of month
        Datetime lastMonth;
        if (now.monthGmt() == 1) {
            lastMonth = Datetime.newInstanceGmt(now.yearGmt(),now.monthGmt()-1,15);
        } else {
            lastMonth = Datetime.newInstanceGmt(now.yearGmt(),now.monthGmt()+1,15);
        }
        Datetime lastYear = now.addDays(-365);
        
        Case c_now = new Case(CreatedDate=now);
        Case c_lm = new Case(CreatedDate=lastMinute);
        Case c_lh = new Case(CreatedDate=lastHour);
        Case c_yd = new Case(CreatedDate=yesterday);
        Case c_lw = new Case(CreatedDate=lastWeek);
        Case c_lmo = new Case(CreatedDate=lastMonth);
        Case c_ly = new Case(CreatedDate=lastYear);
        Case[] cases = new Case[]{c_now,c_lm,c_lh,c_yd,c_lw,c_lmo,c_ly};
        
        
        // bucket by hour test
        Map<Datetime,List<SObject>> results = new Map<Datetime,List<SObject>>();
        new ChartMeanTimeOverTime().dateBucketize(results,(Sobject[]) cases,'CreatedDate','hourly');
        Boolean foundCurrent = false;
        for (Datetime d : results.keySet()) {
            Set<SObject> bucketed = new Set<SObject>(results.get(d));
            
            if (d.getTime() == Datetime.newInstanceGmt(now.yearGmt(),now.monthGmt(),now.dayGmt(),now.hourGmt(),0,0).getTime()) {
                // current hour bucket
                foundCurrent = true;
                System.assert(bucketed.contains(c_now));
                System.assert(bucketed.contains(c_lm));
                System.assert(bucketed.size() == 2);
                
            }
        }
        System.assert(results.keySet().size() == 6);
        System.assert(foundCurrent == true);
        
        
        // bucket by day test
        results = new Map<Datetime,List<SObject>>();
        new ChartMeanTimeOverTime().dateBucketize(results,(Sobject[]) cases,'CreatedDate','daily');
        foundCurrent = false;
        for (Datetime d : results.keySet()) {
            Set<SObject> bucketed = new Set<SObject>(results.get(d));
            
            if (d.getTime() == Datetime.newInstanceGmt(now.yearGmt(),now.monthGmt(),now.dayGmt(),12,0,0).getTime()) {
                // current bucket
                foundCurrent = true;
                System.assert(bucketed.contains(c_now));
                System.assert(bucketed.contains(c_lm));
                System.assert(bucketed.contains(c_lh));
                System.assert(bucketed.size() == 3);
                
            }
        }
        System.assert(results.keySet().size() == 5);
        System.assert(foundCurrent == true);
        
        
        // bucket by week test
        results = new Map<Datetime,List<SObject>>();
        new ChartMeanTimeOverTime().dateBucketize(results,(Sobject[]) cases,'CreatedDate','weekly');
        foundCurrent = false;
        for (Datetime d : results.keySet()) {
            Set<SObject> bucketed = new Set<SObject>(results.get(d));
            
            if (d.getTime() == new ChartMeanTimeOverTime().closestSaturdayAtNoon(now).getTime()) {
                // current bucket
                foundCurrent = true;
                System.assert(bucketed.contains(c_now));
                System.assert(bucketed.contains(c_lm));
                System.assert(bucketed.contains(c_lh));
                System.assert(bucketed.contains(c_yd));
                System.assert(bucketed.size() == 4);
                
            }
        }
        System.assert(results.keySet().size() == 4);
        System.assert(foundCurrent == true);
        
        
        // bucket by month test
        results = new Map<Datetime,List<SObject>>();
        new ChartMeanTimeOverTime().dateBucketize(results,(Sobject[]) cases,'CreatedDate','monthly');
        foundCurrent = false;
        for (Datetime d : results.keySet()) {
            Set<SObject> bucketed = new Set<SObject>(results.get(d));
            
            if (d.getTime() == new ChartMeanTimeOverTime().lastDateOfMonthAtNoon(now).getTime()) {
                // current bucket
                foundCurrent = true;
                System.assert(bucketed.contains(c_now));
                System.assert(bucketed.contains(c_lm));
                System.assert(bucketed.contains(c_lh));
                System.assert(bucketed.contains(c_yd));
                System.assert(bucketed.contains(c_lw));
                System.assert(bucketed.size() == 5);
                
            }
        }
        System.assert(results.keySet().size() == 3);
        System.assert(foundCurrent == true);
        
        
        // bucket by year test
        results = new Map<Datetime,List<SObject>>();
        new ChartMeanTimeOverTime().dateBucketize(results,(Sobject[]) cases,'CreatedDate','yearly');
        foundCurrent = false;
        for (Datetime d : results.keySet()) {
            Set<SObject> bucketed = new Set<SObject>(results.get(d));
            
            if (d.getTime() == new ChartMeanTimeOverTime().lastDateOfYearAtNoon(now).getTime()) {
                // current bucket
                foundCurrent = true;
                System.assert(bucketed.contains(c_now));
                System.assert(bucketed.contains(c_lm));
                System.assert(bucketed.contains(c_lh));
                System.assert(bucketed.contains(c_yd));
                System.assert(bucketed.contains(c_lw));
                System.assert(bucketed.contains(c_lmo));
                System.assert(bucketed.size() == 6);
                
            }
        }
        System.assert(results.keySet().size() == 2);
        System.assert(foundCurrent == true);
        
    }
    
    
    public static testMethod void testComputeMeanTimeDifferenceMilliseconds() {
        Datetime now = Datetime.now();
        Datetime fiveSecs = now.addSeconds(5);
        Datetime fiveMinutes = now.addMinutes(5);
        Datetime fiveDays = now.addDays(5);
        Case c1 = new Case(CreatedDate=now,ClosedDate=fiveSecs); // ~5000 MS
        Case c2 = new Case(CreatedDate=now,ClosedDate=fiveMinutes); // ~300000 MS
        Case c3 = new Case(CreatedDate=fiveSecs,ClosedDate = fiveMinutes); // ~29500 MS
        Case c4 = new Case(CreatedDate=now,ClosedDate=fiveDays); // ~432000000 MS
        Long total = fiveSecs.getTime()-now.getTime() +
                     fiveMinutes.getTime()-now.getTime()+
                     fiveMinutes.getTime()-fiveSecs.getTime()+
                     fiveDays.getTime()-now.getTime();
        Double mean = total / 4;            
        
        Double ret = new ChartMeanTimeOverTime().computeMeanTimeDifferenceMilliseconds(new SObject[] {c1,c2,c3,c4}, 'CreatedDate','ClosedDate');
        System.assert(ret==mean);
    }
    
    

}
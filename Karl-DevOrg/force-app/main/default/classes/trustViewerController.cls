public without sharing class trustViewerController {

    private static final String[] WS_DECODED = new String[] { '  ', '\t', '\n' };
    private static final String[] WS_ENCODED = new String[] { ' &nbsp;', '&nbsp;&nbsp;&nbsp;&nbsp;', '<br />' };

    public Boolean incidentPage { get; set; }
    public Boolean mainPage { get; set; }
    
    public enum DisplayMode {LIST_PAGE, DETAILS_PAGE, ERROR}
    public DisplayMode dMode {get; set;}
    public String dModeForVF { get{return dMode.name();}} // need this because VF does not seem to allow access to the ENUM (http://community.salesforce.com/t5/Visualforce-Development/Having-trouble-referencing-a-user-defined-enum/m-p/168355)
    

    public String incidentFrame { get; set; }
    public String thumbsGallery { get; set; }
    public PubIncident [] arrPub {get; set;} // array of last three incidents
    public PubIncident incident {get; set;} // currently displayed incident in iframe
    
    public String errorText {get; set;}
    
    public class PubIncident {
        public String mDate{get; set;}
        public String mThumbnailURL{get; set;}
        public String mImageURL{get; set;}
        public String mPurportedSender{get; set;}
        public String mSpoofedOriginEmail{get; set;}
        public String mEmailSubject{get; set;}
        public String mContent{get; set;}
        public String mPayload{get; set;}
        public String mNatureOfAttack{get; set;}
        public String mDescriptionOfExploit{get; set;}
        public String mDefensiveAction{get; set;}
    }

    public trustViewerController() {
    
        Integer i;
    
        Security_Incident__c [] arr = null;
        
        errorText = 'EMPTY';
    
        String previewId = ApexPages.currentPage().getParameters().get('preview');
        if (previewId == null) {
            // select three latest objects that were approved and not replaced
            arr = [select ID,
                          Name,
                          Date__c,
                          ThumbnailURL__c,
                          ImageURL__c,
                          Purported_sender__c,
                          Spoofed_origin_email__c,
                          Email_Subject__c,
                          Content__c,
                          Payload__c,
                          Nature_of_attack__c,
                          Description_of_exploit__c,
                          Defensive_action__c
                   from Security_Incident__c 
                   where Security_Approval_Status__c = 'Closed' and Replaced_By__c = null
                   order by Date__c DESC 
                   limit 3];
        }
        else {
            // select three latest objects that were approved and not replaced - one of them being the preview object (as long as it is's date is in the latest three) 
            
            // start by getting the preview object
            Security_Incident__c previewObj = [select ID, Replaces__c from Security_Incident__c where id =:previewId];            
            
            if (previewObj.Replaces__c == null) {
                // The preview object is not replacing an existing one - get the three latest objets that are approved or is the preview object
                arr = [select ID,
                              Name,
                              Date__c,
                              ThumbnailURL__c,
                              ImageURL__c,
                              Purported_sender__c,
                              Spoofed_origin_email__c,
                              Email_Subject__c,
                              Content__c,
                              Payload__c,
                              Nature_of_attack__c,
                              Description_of_exploit__c,
                              Defensive_action__c
                       from Security_Incident__c 
                       where (Security_Approval_Status__c = 'Closed' or id =:previewId) and Replaced_By__c = null
                       order by Date__c DESC 
                       limit 3];
            }
            else {
                // the preview object is intending to replace an existing one - search for the latest three excluding the object intended to be replaced by the preview object
                arr = [select ID,
                              Name,
                              Date__c,
                              ThumbnailURL__c,
                              ImageURL__c,
                              Purported_sender__c,
                              Spoofed_origin_email__c,
                              Email_Subject__c,
                              Content__c,
                              Payload__c,
                              Nature_of_attack__c,
                              Description_of_exploit__c,
                              Defensive_action__c
                       from Security_Incident__c 
                       where (Security_Approval_Status__c = 'Closed' or id =:previewId) and Replaced_By__c = null and id !=:previewObj.Replaces__c
                       order by Date__c DESC 
                       limit 3];
            }

            // make sure the preview object is one of the objects returned
            
            for (i = 0; i < arr.size(); i++) {
                if (arr[i].id == previewId)
                    break;
            }

            if (i == arr.size()) {
                dMode = DisplayMode.ERROR;
                errorText = 'Preview object is not displayed in the output. This can happen for a few reasons: a) The date is older than three other objects. b) It is replaced by another object. c) It can\'t be found based on the Id provided in the url.';
                return;
            }
        }
        
        arrPub = new PubIncident[3];
        thumbsGallery='<table style="border-collapse: collapse; height: 114px;"><tbody><tr>';

        for (i = 0; i < arr.size(); i++) {
            arrPub[i] = new PubIncident();
            arrPub[i].mDate = arr[i].Date__c.format();
            arrPub[i].mThumbnailURL = arr[i].ThumbnailURL__c;
            arrPub[i].mImageURL = arr[i].ImageURL__c;
            arrPub[i].mPurportedSender = arr[i].Purported_sender__c;
            arrPub[i].mSpoofedOriginEmail = arr[i].Spoofed_origin_email__c;
            arrPub[i].mEmailSubject = arr[i].Email_Subject__c;
            // Because the "content" field can have some WS (NL, space, and tab) that we should keep in the ouput,
            //   we will encode the content to make sure it does not contain any HTML tags, then we will encode the
            //   WS characters to HTML so it keeps the layout.
            arrPub[i].mContent = ESAPI.encoder().SFDC_HTMLENCODE(arr[i].Content__c);
            arrPub[i].mContent = ESAPI.encoder().ENCODE(arrPub[i].mContent, WS_DECODED, WS_ENCODED);
            // Changing payload to escape html entities but preserve newline
            arrPub[i].mPayload = ESAPI.encoder().SFDC_HTMLENCODE(arr[i].Payload__c);
            arrPub[i].mPayload = ESAPI.encoder().ENCODE(arrPub[i].mPayload, WS_DECODED, WS_ENCODED);
            arrPub[i].mNatureOfAttack = ESAPI.encoder().SFDC_HTMLENCODE(arr[i].Nature_of_attack__c);
            arrPub[i].mNatureOfAttack = ESAPI.encoder().ENCODE(arrPub[i].mNatureOfAttack, WS_DECODED, WS_ENCODED);
            arrPub[i].mDescriptionOfExploit = ESAPI.encoder().SFDC_HTMLENCODE(arr[i].Description_of_exploit__c);
            arrPub[i].mDescriptionOfExploit = ESAPI.encoder().ENCODE(arrPub[i].mDescriptionOfExploit, WS_DECODED, WS_ENCODED);
            arrPub[i].mDefensiveAction = ESAPI.encoder().SFDC_HTMLENCODE(arr[i].Defensive_action__c);
            arrPub[i].mDefensiveAction = ESAPI.encoder().ENCODE(arrPub[i].mDefensiveAction, WS_DECODED, WS_ENCODED);
            
            if (ApexPages.currentPage().getParameters().get('id') == arr[i].ID)
                incident = arrPub[i];
                
            if (previewId == null)            
                thumbsGallery += '<td style="padding: 0px;"><a class="gallery" '
                    + 'onclick="navigateIncidentIframe(\'' + arr[i].ID + '\', null);" href="#" >'
                    + '<table style="border-collapse: collapse; padding: 0px;"><tbody><tr><td style="padding: 0px;">' 
                    + '<img src="' + arr[i].ThumbnailURL__c + '" width="83px" height="68px" border="0px" />'
                    + '</td></tr><tr><td style="padding: 0px;"><span style="font-size: 7.5pt; font-family: Arial; text-align: center;">' 
                    + ESAPI.encoder().SFDC_HTMLENCODE(Datetime.valueOf(arr[i].Date__c + '').format('MMM d, yyyy')) + '</span></td></tr></tbody></table></a></td>';
                //thumbsGallery += '<a onclick="navigateIncidentIframe(\'' + arr[i].ID + '\', null);" href="#" class="gallery slide" style="background:url(' + arr[i].ThumbnailURL__c + ');"><em></em></a>';
            else
                thumbsGallery += '<td style="padding: 0px;"><a class="gallery" '
                    + 'onclick="navigateIncidentIframe(\'' + arr[i].ID + '\', \'' + ESAPI.encoder().SFDC_JSINHTMLENCODE(previewId) + '\');" href="#" >'
                    + '<table style="border-collapse: collapse; padding: 0px;"><tbody><tr><td style="padding: 0px;">'
                    + '<img src="' + arr[i].ThumbnailURL__c + '" width="83px" height="68px" border="0px" />'
                    + '</td></tr><tr><td style="padding: 0px;"><span style="font-size: 7.5pt; font-family: Arial; text-align: center;">' + 
                    + ESAPI.encoder().SFDC_HTMLENCODE(Datetime.valueOf(arr[i].Date__c + '').format('MMM d, yyyy')) + '</span></td></tr></tbody></table></a></td>';
                //thumbsGallery += '<a onclick="navigateIncidentIframe(\'' + arr[i].ID + '\', \'' + previewId + '\');" href="#" class="gallery slide" style="background:url(' + arr[i].ThumbnailURL__c + ');"><em></em></a>';
        }

        thumbsGallery += '</tr></tbody></table>';
        
        if (previewId == null)
            incidentFrame = '<iframe scrolling="no" height="615" frameborder="0" width="565" src="?dev=0&id=' + arr[0].ID + '" marginheight="0" marginwidth="0" name="incidentFrame" id="incidentFrame"></iframe>';
        else
            incidentFrame = '<iframe scrolling="no" height="615" frameborder="0" width="565" src="?dev=0&id=' + arr[0].ID + '&preview=' + ESAPI.encoder().SFDC_URLENCODE(previewId) + '" marginheight="0" marginwidth="0" name="incidentFrame" id="incidentFrame"></iframe>';
        
        if (ApexPages.currentPage().getParameters().containsKey('id') == false)
            dMode = DisplayMode.LIST_PAGE;
        else
            dMode = DisplayMode.DETAILS_PAGE;
    } 
}
public class TrailHeadValidity {

  public Trailhead_Validity__c tHeadValidty {get; set;}
  public String thValidityDate {get; set;}
  public String releaseNotes {get; set;}


  public TrailHeadValidity(ApexPages.StandardController controller) {
     releaseNotes = 'https://security.my.salesforce.com/articles/Secure_Coding_Trailhead/Develop-Secure-Web-Apps-Release-Notes';
     try {
         tHeadValidty = [SELECT Name,Recent_Validity_Time__c FROM Trailhead_Validity__c LIMIT 1];
         DateTime dt = tHeadValidty.Recent_Validity_Time__c;
         tHeadValidty.Recent_Validity_Time__c = null;
         thValidityDate = dt.format('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');
     }catch(Exception ex) {
       thValidityDate = null;
       tHeadValidty = new Trailhead_Validity__c(Name='Trailhead Validity');
     }
  }

  public pageReference saveValidity() {
     upsert tHeadValidty;
     PageReference P = new PageReference('/apex/TrailHeadValidity');
     p.setRedirect(True);
     return p;
  }
}
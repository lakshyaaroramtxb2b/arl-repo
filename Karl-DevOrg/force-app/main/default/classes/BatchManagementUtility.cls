/*
* Class Name: BatchManagementUtility
* Description:Batch Utility Manager to automatically start aborted/not
* active batches and notify those responsible
* Author/Date: Luke Slevin / 10.22.2019
* Date New/Modified: 10.22.2019
*
*/

global class BatchManagementUtility implements Database.Batchable<sObject>,Database.Stateful, schedulable{
                
    String query = 'SELECT ID, MasterLabel,Launch_Code__c,Active__c,Logging_Level__c,' +
                   'Responsible_Email_Addresses__c FROM Batch_Management__mdt';
                   
    List<Batch_Management__mdt> jobsToReSchedule = new List<Batch_Management__mdt>();

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext bc, List<Batch_Management__mdt> scope){
        
        Map<String, CronTrigger> currentlyScheduledJobsCronTrigger = new Map<String, CronTrigger>();
        Map<String, ApexClass> currentlyScheduledJobsApexJob = new Map<String, ApexClass>();
        Set<Id> apexClassIds = new Set <Id>();
        List<Log__c> batchLogs = new List<Log__c>();
        
        //Check Apex Job object to see if there are any currently running jobs
        For(AsyncApexJob aaj: [SELECT ApexClassID, CompletedDate, ExtendedStatus, JobItemsProcessed, JobType, MethodName, NumberOfErrors, Status, 
                              TotalJobItems FROM AsyncApexJob WHERE JobType = 'BatchApex' AND (Status = 'Queued' OR Status = 'Preparing' OR Status = 'Processing')])  {
            apexClassIds.add(aaj.ApexClassId);
        }

        //Get the names of the Apex Classes that are running based upon the class Id's from the AsyncApexJob
        FOR(ApexClass ap: [ SELECT Id,Name FROM ApexClass WHERE ID IN: apexClassIds])  {
            currentlyScheduledJobsApexJob.put(ap.Name,ap);
        }
        
        //Check CronTrigger object to see if there are any currently running jobs
        For(CronTrigger ct: [SELECT Id, CronJobDetailId, CronJobDetail.Name, NextFireTime, PreviousFireTime, State, StartTime, EndTime, 
                            CronExpression, TimeZoneSidKey, OwnerId, TimesTriggered FROM CronTrigger WHERE State = 'Waiting'])  {
            currentlyScheduledJobsCronTrigger.put(ct.CronJobDetail.Name,ct);
        }   
        
        for(sObject sObj : scope) {
            Batch_Management__mdt bmm  = (Batch_Management__mdt)sObj;
            if(bmm.Active__c == true) {   
                if(currentlyScheduledJobsCronTrigger.containsKey(bmm.MasterLabel) || currentlyScheduledJobsApexJob.containsKey(bmm.MasterLabel)) {
                    Log__c batchLog = new Log__c();
                    batchLog.Log_Message__c = 'The Batch Class  ' + bmm.MasterLabel + ' is currently scheduled and running without issue.';
                    if(bmm.Logging_Level__c == 'Email')    {
                        batchLog.Send_Notification__c = true;
                    }
                    if(bmm.MasterLabel != null)    {
                        batchLog.Batch_Class__c = bmm.MasterLabel;
                    }
                    batchLogs.add(batchLog);
                }
                else {
                    /*Job is not currently scheduled the below will do the following:
                    1.Update the lsat checked date field to when the job was last ran and restarted
                    2.Reschedule the job based upon the launch code provided in the metadata
                    3.Send an email to the responsible email addresses on the metadata letting them know it was rescheduled */
                
                    jobsToReSchedule.add(bmm);
                    List<String> launchCode = bmm.Launch_Code__c.split(',');
                    
                    Type batchClassName = Type.forName(bmm.MasterLabel);
                    Schedulable instanceOfBatch =  (Schedulable) batchClassName.newInstance();
                    
                    //Schedule the batch job that was found to not be running
                    System.schedule(launchCode[0], launchCode[1],  instanceOfBatch);

                    Log__c batchLog = new Log__c();
                    batchLog.Log_Message__c = 'The Batch Class  ' + bmm.MasterLabel + 'was found to not be scheduled or running.  At this time this class has been rescheduled.';
                    if(bmm.Logging_Level__c == 'Email')    {
                        batchLog.Send_Notification__c = true;
                    }
                    if(bmm.Responsible_Email_Addresses__c != null)  {
                        List<String> emails = bmm.Responsible_Email_Addresses__c.split(',');
                        batchLog.Email_Recipient_1__c = emails[0];
                        
                        if(emails.size() == 2)    {
                            if(emails[1] != null )    {
                            batchLog.Email_Recipient_2__c = emails[1];
                            }
                        }
                        if(emails.size() == 3)    {
                            if(emails[2] != null )    {
                                batchLog.Email_Recipient_3__c = emails[2];
                            }
                
                        }
                    }
                    if(bmm.MasterLabel != null)    {
                        batchLog.Batch_Class__c = bmm.MasterLabel;
                    }
                    batchLogs.add(batchLog);
                }
            }
        }

        insert batchLogs;
    }
    
    global void finish(Database.BatchableContext bc){

        //Schedule the job to run 5 minutes from now
        BatchManagementUtility batch = new BatchManagementUtility();
        System.scheduleBatch(batch, 'BatchManagementUtility',5);
    }    

    global void execute(SchedulableContext sc) {
      
   }
}
global with sharing class ScanInfoBacklogProcessor {

    public static void run(BatchNotifier bn) { 
        Integer lookback = 600; //lookback in hours -- we don't search from beginning of time!
        Backlog_Configuration__c config = [SELECT PausedDelay__c 
                                          FROM Backlog_Configuration__c
                                          WHERE Active__c = True LIMIT 1];
        
        
        List<Scan_Info__c> pausedReports = [SELECT Name, Id, CreatedDate, scanner__c, ScanOriginID__c, Status__c
                                                FROM Scan_Info__c WHERE Status__c ='Paused'
                                                AND LastModifiedDate < :Datetime.now().addHours(0-Math.round(config.PausedDelay__c))
                                                AND CreatedDate >:Datetime.now().addHours(0-lookback) 
                                                AND Id IN (SELECT Scan_Info__c FROM Scan_Queue__c)
                                                LIMIT 100 ];
        
        
        if (pausedReports.size() > 0) {
            
            bn.addNotifier(Notifier.Severity.Error, 'PausedReportBacklog checker : Paused reports ' + String.valueOf(pausedReports.size()) + ' are being set to Redundant and new reports generated ',makeExtra(pausedReports)); 
            List<Scan_Info__c> failed = rollback(pausedReports);
            if (failed.size() > 0) {
                bn.addNotifier(Notifier.Severity.Error, 'PausedReportBacklog checker: Failed to rollback ' + String.valueOf(failed.size()) + ' reports, ', makeExtra(failed));
            } 
        }
    }
        
    private static List<String> makeExtra(List<Scan_Info__c> objs) {
            List<String> ret = new List<String>();
            for (Scan_Info__c c : objs) {
               ret.add(c.CreatedDate.format() + '  ' + c.ScanOriginID__c + '  ' + c.Name);
            }
            return ret;
        }
        
    private static List<Scan_Info__c> rollback(List<Scan_Info__c> objs) {
        /* We rollback scan info by setting scan queue to Scanning 
         * and then a new scan info is generated
         * which orphans this scan info so it wont be picked up again. 
         * 
         * We do this because trying to re-generate reports usually fails
         * as popcrab will delete the intermediate files necessary to generate
         * the report in order to avoid permanently storing these on disk
         */
        
        List<Scan_Info__c> failed = new List<Scan_Info__c>();

        //We have to lookup scan origin
        for (Scan_Info__c obj : objs) {
            //first set related scan queue to scanning
            try {
                Scan_Queue__c sq = [SELECT Id, Name, CreatedDate, Status__c, guid__c, Scan_Info__r.Id, Scan_Info__r.Status__c FROM Scan_Queue__c
                                     WHERE Scan_Info__r.Id=:obj.Id ORDER BY CreatedDate DESC LIMIT 1];

                //check to see if we are done or not
                if (sq.Status__c == 'Error Processing results' ||
                    sq.Status__c == 'Done') {
                        sq.Status__c = 'Scanning';
                        update(sq);    
                    }
    
                //now dispose of scan info
                obj.Status__c = 'Redundant';

            } catch (Exception e) {
                System.debug(e);
                failed.add(obj);
            }
            
        }
        update objs;        
        return failed;
    }
}
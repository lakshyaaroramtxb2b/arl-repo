@isTest
public class PRT_GUSEpicToPRTMilestoneTest {
    @testSetup
    public static void testSetup(){
        List<User> userList = new List<User>();
        userList = PRT_TestDataFactory.createUsers(3,'System Administrator',true);
    }
    @isTest
    static void testExecuteMethod(){
        List<ADM_Epic_c__x> epicGusList = new List<ADM_Epic_c__x>();
        List<User> userList = new List<User>([SELECT Id FROM USER WHERE Email Like 'PRTUser%']);
        List<Project__c> projectList = PRT_TestDataFactory.createProjects(1,true);
        List<Milestone_PRT__c> prtMilestoneList = PRT_TestDataFactory.createMilestone(1, projectList[0].id, false);
        prtMilestoneList[0].status__c = 'Not Started';
        prtMilestoneList[0].Completion_Date__c = date.today()+2;
        prtMilestoneList[0].Start_Date__c = date.today();
        prtMilestoneList[0].Description__c = 'test Description Milestone';
        prtMilestoneList[0].Due_Date__c = date.today()+2;
        prtMilestoneList[0].GUS_Epic__c = '';
        insert prtMilestoneList;
        
        ADM_Epic_c__x gusEpic = new ADM_Epic_c__x();
        gusEpic.Name__c = 'test Gus Epic Record';
        gusEpic.Start_Date_c__c = date.today();
        gusEpic.End_Date_c__c = date.today()+2;
        gusEpic.Description_c__c = 'test data description epic ';
        gusEpic.Health_c__c = 'Not Started';
        gusEpic.Planned_End_Date_c__c = date.today()+2;
        gusEpic.ExternalId = 'EX101';
        gusEpic.Success_Criteria_c__c = prtMilestoneList[0].Id;
        //gusEpic.Id = prtMilestoneList[0].Id;
        System.debug('gusEpicid -> '+ gusEpic.Id);
        epicGusList.add(gusEpic);
        
        PRT_GUSEpicToPRTMilestone.mockallGudEpicList.addAll(epicGusList);
        Test.startTest();
            PRT_GUSEpicToPRTMilestone gusMile = new PRT_GUSEpicToPRTMilestone();
            Id batchId = Database.executeBatch(gusMile);
        Test.stopTest();
    }

}
/**
* @File Name          : IEM_CaseVisibilityTriggerHelperTest.cls
* @Description        : 
* @Author             : Banshi
* @Group              : 
* @Last Modified By   : Banshi
* @Last Modified On   : 11/14/2019, 1:46:49 PM
* @Modification Log   : 
* Ver       Date            Author      		    Modification
* 1.0    11/14/2019   Banshi     Initial Version
**/
@isTest
public class IEM_CaseVisibilityTriggerHelperTest {
    
    @isTest
    static void createShareRecordsForInternalUsersTest() {
        User admin = IEM_TestDataFactory.createUser(); 
        
        List<User> portalUser = new List<User>();        
        System.runAs(admin){
            //Create Accounts
            Account testAccount = new Account(Name='salesforce.com - ESA Office Hours',Website='http://www.interactiveties.com/');
            Insert testAccount;
            //Create Contacts
            List<Contact> contacts = IEM_TestDataFactory.createContacts(2,testAccount.id,true);
            //Create poratl User
            portalUser.addAll(IEM_TestDataFactory.createESACCPPortalUserList(contacts,true));   
            
            //Create Case         
            List<Case> caseList = IEM_TestDataFactory.createIntakeIssueCase(2,contacts,true);        
            test.startTest();
            List<Case_Visibility__c> caseVisibilityList = IEM_TestDataFactory.createCaseVisibilties(caseList, contacts, true);
            System.debug('%%caseVisibilityList'+caseVisibilityList);
        
            test.stopTest();
            List<CaseShare> caseShareList = [SELECT id,UserOrGroupId,CaseAccessLevel,CaseId from CaseShare where CaseId = :caseList[0].Id AND UserOrGroupId = :portalUser[0].Id];
          	system.assertEquals(1, caseShareList.size());
            delete caseVisibilityList;
            caseShareList = [SELECT id,UserOrGroupId,CaseAccessLevel,CaseId from CaseShare where CaseId = :caseList[0].Id AND UserOrGroupId = :portalUser[0].Id];
          	system.assertEquals(0, caseShareList.size());
            
        }
    }
}
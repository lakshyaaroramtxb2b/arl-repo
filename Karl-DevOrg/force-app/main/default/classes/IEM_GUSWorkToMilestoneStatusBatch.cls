/***************IEM_GUSWorkToMilestoneStatusBatch****************
@Author Banshi
@Date 08/25/2019
@Modified by/ date - Swarnima singh Mandhata - T-09359 - 03/31/2020
@Description Batch class thttps://security--mtxdev.cs71.my.salesforce.com/_ui/common/apex/debug/ApexCSIPage#o create fetch GUS work records on regular basis on certain creteria and then create crossponding case records.
**********************************************/

public class IEM_GUSWorkToMilestoneStatusBatch implements Database.Batchable<sObject>,Schedulable {
    @TestVisible
    private static list<ADM_Work_c__x> mockallGusWorkList = new list<ADM_Work_c__x>();//For test class
    
    public Database.QueryLocator start(Database.BatchableContext BC) {
        if(Test.isRunningTest()){
            return Database.getQueryLocator([SELECT Id FROM User LIMIT 1]);    
        }else{
            return Database.getQueryLocator([Select Id,ExternalId,LastModifiedDate__c,Status_c__c,Feature_ID_c__c From ADM_Work_c__x Where LastModifiedDate__c >=LAST_N_DAYS:1]);    
        }
    }
    public void execute(Database.BatchableContext BC, List<sObject> gusRecords)
    {
        
        if(Test.isRunningTest()){
            gusRecords =  mockallGusWorkList;
        }
        Datetime oneHourBack = Datetime.now().addMinutes(-5);
        if(!gusRecords.isEmpty()){
            Set<Id> milestoneIds = new Set<Id>();
            for(ADM_Work_c__x gusObject :(List<ADM_Work_c__x>)gusRecords){
                milestoneIds.add(gusObject.ExternalId);
            }
            Map<String,MileStone__c> gusrecordToMileStoneMap = new Map<String,Milestone__c>();
            for(Milestone__c milestone : [SELECT Id,Status__c,GUS_Work__c FROM MileStone__c WHERE GUS_Work__c  IN : milestoneIds]){
                if(Test.isRunningTest()){
                    gusrecordToMileStoneMap.put('EX101',milestone);
                }else{
                    gusrecordToMileStoneMap.put(milestone.GUS_Work__c,milestone);
                }
            }
            List<Milestone__c> milestoneUpdateList = new List<Milestone__c>();
            for(ADM_Work_c__x gusObject : (List<ADM_Work_c__x>)gusRecords){
               if(gusrecordToMileStoneMap.containsKey(gusObject.ExternalId)){
                        Milestone__c milestone =  gusrecordToMileStoneMap.get(gusObject.ExternalId);
                        String milestoneStatusToUpdate;
                        if( gusObject.Status_c__c == IEM_Constants.GUS_WORK_STATUS_CLOSED){
                            milestoneStatusToUpdate = IEM_Constants.MILESTONE_STATUS_COMPLETED;
                        }
                        else if( gusObject.Status_c__c  == IEM_Constants.GUS_WORK_STATUS_NEW ){
                            milestoneStatusToUpdate = IEM_Constants.MILESTONE_STATUS_PENDING;
                        }
                        else if(gusObject.Status_c__c == IEM_Constants.GUS_WORK_STATUS_IN_PROGRESS ){               
                            milestoneStatusToUpdate = IEM_Constants.MILESTONE_STATUS_IN_PROGRESS;
                        }
                        if(String.isNotEmpty(milestoneStatusToUpdate) && milestoneStatusToUpdate !=  milestone.Status__c ){
                            milestone.Status__c = milestoneStatusToUpdate;
                            milestoneUpdateList.add(milestone);
                        }
                    }
                
            }
            update milestoneUpdateList;
                
        }
        
    }
    
    public void finish(Database.BatchableContext BC) {
        // finish code
        IEM_GUSWorkToMilestoneStatusBatch batch = new IEM_GUSWorkToMilestoneStatusBatch();
        if(!Test.isRunningTest()){
            System.scheduleBatch(batch, 'IEM_GUSWorkToMilestoneStatusBatch',5);
        }
    }
    public void execute(SchedulableContext SC) {
        database.executebatch(new IEM_GUSWorkToMilestoneStatusBatch());
    }
    
}
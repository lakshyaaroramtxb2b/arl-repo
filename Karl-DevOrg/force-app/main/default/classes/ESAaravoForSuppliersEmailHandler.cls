/* 

    Developer Name: Jorge L Caceres - jcaceres@salesforce.com | February 2020
    
    Description:
        Action based on email content.
      
*/ 
global class ESAaravoForSuppliersEmailHandler implements Messaging.InboundEmailHandler {

    /* tags for parsing text */

    private static final String SUPPLIER_TAG = 'SUPPLIER';    
    private static final String SUPPLIER_COUNTRY_TAG = 'SUPPLIER COUNTRY';    
    private static final String BUSINESS_PROCESS_TAG = 'BUSINESS PROCESS';    
    private static final String BUSINESS_PROCESS_ID_TAG = 'BUSINESS PROCESS ID';    
    private static final String TASK_NAME_TAG = 'TASK NAME';    
    private static final String REQUESTER_NAME_TAG = 'REQUESTER';    
    private static final String BUSINESSCONTACT_EMAIL_TAG = 'REQUESTER EMAIL';    
    private static final String TAKE_ACTION_BY_TAG = 'TAKE ACTION BY OR BEFORE';    
    private static final String DESCRIPTION_BEGIN_TAG = 'requires your timely review and approval:';
    private static final String ON_BEHALF_OF_NAME = 'ON BEHALF OF NAME';
    private static final String ON_BEHALF_OF_EMAIL = 'ON BEHALF OF EMAIL';
    private static final List<String> TAGS = new List<String>{SUPPLIER_TAG, SUPPLIER_COUNTRY_TAG, BUSINESS_PROCESS_TAG, BUSINESS_PROCESS_ID_TAG,TASK_NAME_TAG, 
        REQUESTER_NAME_TAG, BUSINESSCONTACT_EMAIL_TAG, TAKE_ACTION_BY_TAG, DESCRIPTION_BEGIN_TAG, ON_BEHALF_OF_NAME, ON_BEHALF_OF_EMAIL};

    /* constants */

    @TestVisible private static final String SUBJECT = 'ACTION NEEDED: Complete Tier 2 Intake Form for Marketing Compliance Approval - New Supplier: {vendor}';    
    @TestVisible private static final String PRIORITY = '3 - Standard';    
    @TestVisible private static final String STATUS = 'New';
    @TestVisible private static final String SF_SEC_RECORDTYPEID = '0120000000000BSAAY'; // Ticket Record Type
    @TestVisible private static final String INTERNAL_SUPPORT_CATEGORY = 'Marketing Compliance: New Supplier Request';
    @TestVisible private static final String SF_ORGID;  // needs to be a method in SF UTIL to get current org ID
        {SF_ORGID = sfutil.ESASupportForceUtil.getIsProduction()? '00D00000000hg76' : '00DV00000088Yba';}            
    @TestVisible private static final String E2C_ADDRESS;
        {E2C_ADDRESS = sfutil.ESASupportForceUtil.getIsProduction()? 'trust_esa@x-1m850z6xp13rkhsi53dzdee2nzirigrv0yi2jciafwxg62uxik.0-hg76eaa.na5.case.salesforce.com' :
                                                                     'sectech@35i5v46j592kmbhfogkofotnvl5iu4vyheyomt8zewopgj5o4j.v-88ybamae.cs12.case.sandbox.salesforce.com';}            
    @TestVisible private static final String SUPPORTFORCE_LINK;
        {SUPPORTFORCE_LINK = sfutil.ESASupportForceUtil.getIsProduction()? 'https://concierge.it.salesforce.com/tickets/SupportForce-' :
                                                                           'https://supportforce--full2.cs12.my.salesforce.com/';}   
    private static final String CASE_COUNT_QUERY = 'RecordTypeId = \'' + SF_SEC_RECORDTYPEID + '\''
                                                 + ' and Status = \'' + STATUS + '\''
                                                 + ' and Internal_Support_Category__c = \'' + INTERNAL_SUPPORT_CATEGORY + '\''
                                                 + ' and Vendor__c != null ';
                                                                                    
    /* class variables */

    @TestVisible private Case sfCase;
    @TestVisible private CaseComment sfCaseComment;
    @TestVisible private Contact businessContact;
    
    private static final String CASE_COMMENT = ''
        + 'Your request to bring on a new supplier for {vendor} '
        + 'triggered an additional review from the Marketing Compliance team.\n\n'

        + 'In order to expedite approvals, please complete the Tier 2 Intake Form '
        + 'so the Marketing Compliance team has sufficient information '
        + 'to evaluate your supplier.\n\n'

        + 'Failure to complete the questionnaire or submitting incomplete '
        + 'and/or inaccurate information may cause delays '
        + 'to evaluate your supplier.\n\n'
        
        + 'Complete this form: https://app.smartsheet.com/b/form/2cdd7d0ecf854f6586c600cf0203e792 \n\n'
        
        + 'Thank you,\n'
        + 'Marketing Compliance Team';

    private static final String CASE_EMAIL = ''
        + 'Hi {requester},\n\n'
        + 'Your request to bring on a new supplier for {vendor} '
        + 'triggered an additional review from the Marketing Compliance team.\n\n'

        + 'In order to expedite approvals, please complete the Tier 2 Intake Form '
        + 'so the Marketing Compliance team has sufficient information '
        + 'to evaluate your supplier.\n\n'

        + 'Failure to complete the questionnaire or submitting incomplete '
        + 'and/or inaccurate information may cause delays '
        + 'to evaluate your supplier.\n\n'
        
        + 'Complete this form: https://app.smartsheet.com/b/form/2cdd7d0ecf854f6586c600cf0203e792 \n\n'
        
        + 'Access your case here: {supportforcelink}{caseid}\n\n'
        
        + 'Thank you,\n'
        + 'Marketing Compliance Team\n\n\n\n\n'
        + '{email2caseref}';

    private static final String CASE_SUBJECT = 'Marketing Compliance Review For Supplier Portal Request: {vendor}';

    private static OrgWideEmailAddress owea;
       {owea = [SELECT Address,Id,DisplayName FROM OrgWideEmailAddress WHERE DisplayName = 'Marketing Compliance'];}
    
    Map<String, String> sfCaseCustomFields = new Map<String, String>();  
    String emailBody;  

    /* main processing method for email service */

    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
        
        // Aravo emails contain only Html body so we need to remove html tags and some other cleaning
        emailBody = email.HtmlBody;
        emailBody = emailBody.replace('*', '').replaceAll('<[^>]*>', ''); // remove html tags
        for (String tag : TAGS) emailBody = emailBody.replace((tag + '\n'), (tag + ' ')).replace((tag + '\r'), (tag + ' ')); // align tags with value in the same line
        emailBody = emailBody.replaceAll('(\r?\n)+', '\n'); // remove double spacing

        // try to get the business contact using the requesterEmail from the Aravo Case
        businessContact = new Contact();   
        try {
        if (!test.isRunningTest()) businessContact = sfutil.ESASupportForceUtil.getUserContact(getBusinessContactEmail());
        } catch (exception e) {system.debug (e);}
 
        // create supportforce case
        Id aravoCaseId = createCase();     
        if (aravoCaseId != null) {
            Id aravoCaseCommentId = createComment(aravoCaseId);
            sendEmail(aravoCaseId);
            String vendorName = JSON.serialize(getSupplierName()).left(50).replaceAll('\"', '');
            updateCaseCommentWithExistinngCases(vendorName, aravoCaseId);
        }
                       
        return null;
    }
    
    /* private methods */ 
    
    @TestVisible
    private Id createCase() {
        String vendorName = JSON.serialize(getSupplierName()).left(50).replaceAll('\"', '');
        Boolean openVendorCase = false;
        if(String.isNotBlank(vendorName)) {
            openVendorCase = existingOpenVendorCases(vendorName);
        }
        if(openVendorCase == true) {
            return null;
        }
        sfCase = new Case();
        sfCase.OwnerId = '00G70000002ps0x';
        sfCase.Subject = SUBJECT.replace('{vendor}',getSupplierName());  
        sfCase.Priority = PRIORITY;
        sfCase.Status = STATUS;
        sfCase.RecordTypeId = SF_SEC_RECORDTYPEID;
        sfCaseCustomFields.put('Internal_Support_Category__c', INTERNAL_SUPPORT_CATEGORY);
        sfCaseCustomFields.put('Deployment_Start_Date__c', JSON.serialize(getNeedByDate()).replaceAll('\"', '')); // required SF field
        sfCaseCustomFields.put('Need_By_Date__c', JSON.serialize(getNeedByDate()).replaceAll('\"', '')); 
        sfCaseCustomFields.put('Vendor__c', JSON.serialize(getSupplierName()).left(50).replaceAll('\"', '')); 
        sfCase.Description = getDescription();
        if (businessContact.Id != null) sfCase.ContactId = businessContact.Id;
        String sfCaseJson = addCustomFields(JSON.serialize(sfCase), sfCaseCustomFields);           
        Id newCaseId;
        if (!test.isRunningTest()) newCaseId = sfutil.ESASupportForceUtil.insertCase(sfCaseJson);  
        return newCaseId;
    }

    /**
     * To get Any open cases for current vendor in supportforce
     * @param  vendorName [current vendor name]
     * @return            [True - Open cases , False - No open cases]
     */
    
    @TestVisible
    private Boolean existingOpenVendorCases(String vendorName){
        List<Case__x> lstCases = [SELECT ExternalID 
                                  FROM Case__x 
                                  WHERE Vendor_c__c =:vendorName
                                  AND RecordTypeId__c =:SF_SEC_RECORDTYPEID
                                  AND Internal_Support_Category_c__c =:INTERNAL_SUPPORT_CATEGORY
                                  AND Status__c != 'Closed'];
        if(lstCases.size() > 0) {
            return true;
        } else{
            return false;
        }
    }

    /**
     * Get Existing Closed cases current vendor
     * @param  vendorName [current vendor name]
     * @return            []
     */

    @future (callout=true)
    public static void updateCaseCommentWithExistinngCases(String vendorName, String aravoCaseId){
        List<Case__x> lstCases = [SELECT CaseNumber__c 
                                  FROM Case__x 
                                  WHERE Vendor_c__c =:vendorName
                                  AND RecordTypeId__c =:SF_SEC_RECORDTYPEID
                                  AND Internal_Support_Category_c__c =:INTERNAL_SUPPORT_CATEGORY
                                  AND Status__c = 'Closed'];
        
        String currentCases;

        for(Case__x caseX : lstCases){
            if(String.isBlank(currentCases)){
                currentCases = 'Existing Closed Cases of this Vendor';
            }
            currentCases += '  ' + caseX.CaseNumber__c; 
        }

        if(String.isNotBlank(currentCases)){
            CaseComment__x cComment = new CaseComment__x();
            cComment.ParentId__c = aravoCaseId;
            cComment.CommentBody__c = currentCases;
            Database.SaveResult sr = Database.insertImmediate(cComment);
        }
        
    }
    
    @TestVisible
    private Id createComment(Id aravoCaseId) {
        sfCaseComment = new CaseComment();
        string comment = CASE_COMMENT;  
        comment = comment.replace('{vendor}',getSupplierName().trim());  
        sfCaseComment.CommentBody = comment;
        sfCaseComment.IsPublished = true;                   
        sfCaseComment.ParentId = aravoCaseId;
        Id newCommentId;
        if (!test.isRunningTest()) newCommentId = sfutil.ESASupportForceUtil.insertCaseComment(JSON.serialize(sfCaseComment));   
        return newCommentId;      
    }
    
    @TestVisible
    private void sendEmail(Id aravoCaseId) {
        if (getBusinessContactEmail() == null) return;
        String emailBody = CASE_EMAIL;
        emailBody = emailBody.replace('{vendor}',getSupplierName());  
        if (businessContact.Id != null) emailBody = emailBody.replace('{requester}',getBusinessContactName());  
            else emailBody = emailBody.replace('{requester}',getBusinessContactEmail());
        if (aravoCaseId != null) {
            emailBody = emailBody.replace('{supportforcelink}',SUPPORTFORCE_LINK);  
            emailBody = emailBody.replace('{caseid}',aravoCaseId);  
            emailBody = emailBody.replace('{email2caseref}',ESAcaseRefUtil.caseRefId(SF_ORGID, aravoCaseId));
        }

        String emailSubject = CASE_SUBJECT;
        emailSubject = emailSubject.replace('{vendor}',getSupplierName().trim());  
            
        Messaging.SingleEmailMessage mail= new Messaging.SingleEmailMessage();
        mail.setToAddresses(new String[] {getBusinessContactEmail()});
        mail.setCcAddresses(new String[] {owea.Address});
        mail.setOrgWideEmailAddressId(owea.Id);
        mail.setReplyTo(owea.Address);
        mail.setSubject(emailSubject);
        mail.setPlainTextBody(emailBody);
        Messaging.sendEmail(new Messaging.Email[] {mail});                
    }
    
    private String getSupplierName() {
        try {
            if (emailBody.contains(SUPPLIER_COUNTRY_TAG)) {
                return capitalizeAll(emailBody.substringBetween(SUPPLIER_TAG, SUPPLIER_COUNTRY_TAG).trim()); 
            }
            else {
                return capitalizeAll(emailBody.substringBetween(SUPPLIER_TAG, BUSINESS_PROCESS_TAG).trim());            
            }         
        } catch (exception e) {
            return null;
        }
    }
    
    private String getBusinessContactName() {
        if (businessContact.Id == null) return '*** Not able to find matching email in supportforce ***';
        try {
            return (businessContact.FirstName == null? '' : businessContact.FirstName) + (businessContact.LastName == null? '' : (' ' + businessContact.LastName));
        } catch (exception e) {
            return null;
        }
    }

    private String getBusinessContactEmail() {
        try {
            String email;
            email = emailBody.substringBetween(ON_BEHALF_OF_EMAIL, TAKE_ACTION_BY_TAG).trim().toUpperCase().replace('TABLEAU.COM', 'salesforce.com').toLowerCase();

            if(String.isBlank(email)){
                email = emailBody.substringBetween(BUSINESSCONTACT_EMAIL_TAG, ON_BEHALF_OF_NAME).trim().toUpperCase().replace('TABLEAU.COM', 'salesforce.com').toLowerCase();    
            }
            if (email.contains(' ')) email = email.substringBefore(' ').trim();
            return email;
        } catch (exception e) {
            return null;
        }
    }

    private String getDescription() {
        try {
            String description = emailBody.substringBetween(DESCRIPTION_BEGIN_TAG, TAKE_ACTION_BY_TAG).trim();
            String bp = description.substringBetween(BUSINESS_PROCESS_TAG, BUSINESS_PROCESS_ID_TAG);
            description = description.replace(bp, ' ' + bp.trim() + '\n');
            if (description.countMatches(getBusinessContactEmail()) > 1) {
                description = description.replaceFirst((getBusinessContactEmail()),'').trim();
            }
            description = description.replace(BUSINESSCONTACT_EMAIL_TAG, 'BUSINESS CONTACT' + ' ' + getBusinessContactName());
            return description;
        } catch (exception e) {
            return null;
        }
    }

    private Date getNeedByDate() {
        try {
            String cleanDate = emailBody.substringAfter(TAKE_ACTION_BY_TAG).normalizeSpace().left(10).trim().substringBeforeLast('/');
            cleanDate += '/' + emailBody.substringAfter(TAKE_ACTION_BY_TAG).normalizeSpace().left(10).trim().substringafterLast('/').left(4);
            return Date.parse(cleanDate);
        } catch (exception e) {
            return null;
        }
    }

    private String capitalizeAll(String inputString) {
        String workString = inputString.toLowerCase();         
        for (String s : workString.split(' ', -2)) {
            workString = workString.replace(s, s.capitalize());    
        }
        return workString;
    }
        
    private String addCustomFields(string jsonString, map<String, String> customFields) {
        // generate JSON for custom fields
        JSONGenerator customFieldsJSON = JSON.createGenerator(true);
        customFieldsJSON.WriteStartObject();
        for (String customField : customFields.keySet()) {
           customFieldsJSON.writeStringField(customField, customFields.get(customField));   
        }
        customFieldsJSON.WriteEndObject();
        
        // append to incoming json string and return
        string r = (jsonString.removeEnd('}') + 
                   ',' + customFieldsJSON.getAsString().replace('{','').replace('}','') 
                   + '}');
        return r;                
    }  
        
}
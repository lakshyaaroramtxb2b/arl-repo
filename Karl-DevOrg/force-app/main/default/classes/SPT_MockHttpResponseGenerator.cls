@isTest
global class SPT_MockHttpResponseGenerator implements HttpCalloutMock {
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {
        System.assertEquals('POST', req.getMethod());

        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'text/xml;charset=UTF-8');
        res.setBody('<Envelope xmlns="http://schemas.xmlsoap.org/soap/envelope/"><Header/>' +
                    '<Body><login xmlns="urn:partner.soap.sforce.com"><username>' +
                    '{!HTMLENCODE($Credential.UserName)}' +
                    '</username><password>' +
                    '{!HTMLENCODE($Credential.Password)}' +
                    '</password></login></Body></Envelope>');
        res.setStatusCode(200);
        return res;
    }
}
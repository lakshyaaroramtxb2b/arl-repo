/**
 * @author Ben Harvie
 * @email ben.harvie@salesforce.com
 * @description Test class for ControlScopeTrigger
*/
@isTest
public class ARL_ControlScopeTriggerHandlerTest {
    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Test data setup.
    */
    @testSetup
    private static void testSetup(){
        User ospAdmin = ARL_TestDataFactory.createNamedOpsAdmin('TestSetup', 'User', 'testsetup@example.com');
        
        System.runAs(ospAdmin){
            Control__c controlItem = ARL_TestDataFactory.createControl();
            insert controlItem;

            Control_Scope__c controlScope = ARL_TestDataFactory.createMappedControlScope(controlItem.Id);
            insert controlScope;

            KARL_GRC_Controls__c ccfControl = ARL_TestDataFactory.createCcfControl();
            insert ccfControl;
        }
    }
    
    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description In this test, the test is updating the description and asserting on whether the descriptions are equal
    */
	@istest
    public static void unitTest(){ 
        // Setup Ops Admin
        User u = ARL_TestDataFactory.createOpsAdmin();
        
        System.runAs(u) {
         	Control_Scope__c csItem = [SELECT id,Control_Description__c,Control_Name__r.Control_Description__c FROM Control_Scope__c LIMIT 1];
         	KARL_GRC_Controls__c ccfItem = [SELECT id FROM KARL_GRC_Controls__c LIMIT 1];
  		
            // Updating request item will create new Control Version
            String originalControlDescription = csItem.Control_Description__c;

            // Change to a new value
            csItem.Control_Description__c	= 'test12345';
            csItem.CCF_Control__c           = ccfItem.Id;
            csItem.KARL_CS_CCF_Alignment__c = true;
            update csItem; 
            
            // Expecting original values to be equal, the revised new value should be overwritten
            System.assertEquals( csItem.Control_Name__r.Control_Description__c, originalControlDescription, 'Descriptions are not equal' );   
        }
    }
}
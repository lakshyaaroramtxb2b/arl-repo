global with sharing class Esa_Cancel_Appointment {
    public ESA_Security_Request__c esaRequest {get; set;}
    private String esaRequestId;
    
    private static final String SUCCESS_MSG = 'Appointment canceled.';

    public Esa_Cancel_Appointment(ApexPages.StandardController stdController) {
        this.esaRequestId = stdController.getId();
    }
    
    public void cancelButton () {        
        String returnMessage = cancel_Appointment(this.esaRequestId); 
        if (returnMessage == SUCCESS_MSG) {
            ApexPages.addMessage(New ApexPages.Message(ApexPages.Severity.INFO, returnMessage));        
        } else {
            ApexPages.addMessage(New ApexPages.Message(ApexPages.Severity.ERROR, returnMessage));                    
        }     
    } 

    webservice static String cancel_Appointment(String esaRequestId) {

        ESA_Security_Request__c esaRequest = [SELECT Id,Office_Hours__c,Entity_Code__c,Supportforce_Case_Number__c,
                                              Reservation_Key__c, GCal_EventId__c,Supportforce_CaseId__c
                                              FROM ESA_Security_Request__c WHERE Id = :esaRequestId];

        String aptEventId = esaRequest.GCal_EventId__c;

        Esa_Entity__c esaEntity = [SELECT Id,Google_Calendar_Name__c FROM Esa_Entity__c
                                   WHERE EntityCode__c =: esaRequest.Entity_Code__c];

        String calendarName = esaEntity.Google_Calendar_Name__c;

       /* if (!ESA_Security_Request__c.sObjectType.getDescribe().isUpdateable()) {
            return 'You don\'t have permission to edit this record, Please reach out Administrator..!';
        } */

        if(string.isBlank(aptEventId) || aptEventId == null) {
            return 'No appointment scheduled for this request';
        }

        try {
            try {
                    if(!test.isRunningTest()) {
                        esagutil.ESAGoogleOAuthUtil googleAPI = new esagutil.ESAGoogleOAuthUtil(calendarName);
                        googleAPI.deleteEvent(aptEventId);
                    }
                } catch (exception e) {
                    Esa_DebugService.WriteException(e,'Esa_Cancel_Appointment','CancelReservation - Unable to access GCal');
                    String errorMsg = 'Not able to access google calendar!';
                    errorMsg += '\n Exception: ' + e;
                    return errorMsg;
                }
            
            if(esaRequest.Supportforce_Case_Number__c != null && esaRequest.Supportforce_Case_Number__c.startsWithIgnoreCase('W-') == false) {
                /*CaseComment sfCaseComment = new CaseComment();
                sfCaseComment.CommentBody = Label.ESA_Intake_Bot + '\n' + 'Appointment canceled by - '+UserInfo.getName();
                sfCaseComment.IsPublished = true;
                sfCaseComment.ParentId = esaRequest.Supportforce_CaseId__c;
                if(!test.isRunningTest()) {
                    String CaseCommentId = sfutil.ESASupportForceUtil.insertCaseComment(JSON.serialize(sfCaseComment));    
                } */
                
                CaseComment__x sfCaseComment = new CaseComment__x();
                sfCaseComment.CommentBody__c = Label.ESA_Intake_Bot + '\n' + 'Appointment canceled by - '+UserInfo.getName();
                sfCaseComment.IsPublished__c = true;
                sfCaseComment.ParentId__c = esaRequest.Supportforce_CaseId__c;
                Database.SaveResult sr = Database.insertAsync(sfCaseComment);
            }

            esaRequest.Office_Hours__c = null;
            esaRequest.Reservation_Key__c = null;
            esaRequest.GCal_EventId__c = null;
            update esaRequest;

        } catch (exception e) {
            Esa_DebugService.writeException(e, 1,'Unable to Cancel the Appointment', 'Unhandled exception');
            return e.getMessage();
        }

        return SUCCESS_MSG;
    }
}
/**
* @author Ben Harvie
* @email ben.harvie@salesforce.com
* @description This is a test class for KARL_ARLReportController, which is used throughout
* the KARL application within LWCs.
*/
@isTest
public with sharing class KARL_ARLReportControllerTest {

    /**
    * @author Ben Harvie
    * @email ben.harvie@salesforce.com
    * @description Test data setup.
    */
	@testSetup
    private static void testSetup(){

        // Utilize System Administrator to create a portal user
        User sysAdmin = ARL_TestDataFactory.createSystemAdmin();
        System.runAs(sysAdmin){
            Account acc = new Account(Name = 'salesforce.com - ESA Office Hours');
            insert acc;
            
            Contact con = new Contact(LastName = 'test', AccountId = acc.Id, Email = 'test@gmail.com');
            insert con;

            ARL_TestDataFactory.createCommunityUserwithSpecificCon(con.Id, 'a12345@bqww.com', true);
        }

        // Utilize Ops Admin for everything else
        User ospAdmin = ARL_TestDataFactory.createNamedOpsAdmin('TestSetup', 'User', 'testsetup@example.com');
        System.runAs(ospAdmin){
            /**
             * Audit Firm and Team Setup
             */
            KARL_Audit_Firm__c auditFirm = ARL_TestDataFactory.createAuditFirm('testAuditFirm');
            insert auditFirm;
            
            // Create two distinct audit teams
            KARL_Audit_Team__c auditTeam1 = ARL_TestDataFactory.createAuditTeam('testAuditTeam1');
            auditTeam1.KARL_Audit_Firm__c = auditFirm.Id;
            insert auditTeam1;
            
            KARL_Audit_Team__c auditTeam2 = ARL_TestDataFactory.createAuditTeam('testAuditTeam2');
            auditTeam2.KARL_Audit_Firm__c = auditFirm.Id;
            insert auditTeam2;

            /**
             * Controls Setup
             */
            // Create empty control shell
            Control__c control = ARL_TestDataFactory.createControl();
            insert control;

            // Map to the control shell
            Control_Scope__c controlScope = ARL_TestDataFactory.createControlScope();
            controlScope.Control_Name__c = control.id; // Map to the created control
            controlScope.Scope_Name__c  = 'HK';
            insert controlScope;
            
            // Create control tests
            Control_Test__c controlTest1 = ARL_TestDataFactory.createControlTest(controlScope.id);
            controlTest1.Test_Name__c = 'test control test 1';
            insert controlTest1;
            
            Control_Test__c controlTest2 = ARL_TestDataFactory.createControlTest(controlScope.id);
            controlTest2.Test_Name__c = 'test control test 2';
            insert controlTest2;
            
            /**
             * Requests Master Data Setup
             */
            Request_Item__c reqItem1 = ARL_TestDataFactory.createRequestItem();
            reqItem1.Request_Name__c = 'Request 1';
            reqItem1.Primary_Scope__c = 'HK';
            reqItem1.Areas_of_Compliance__c  = 'SOC 2';
            reqItem1.Create_GUS_Case__c = true;
            insert reqItem1;

            Request_Item__c reqItem2 = ARL_TestDataFactory.createRequestItem();
            reqItem2.Request_Name__c = 'Request ';
            reqItem2.Primary_Scope__c = 'HK';
            reqItem2.Areas_of_Compliance__c  = 'HITRUST';
            reqItem2.Create_GUS_Case__c = true;
            insert reqItem2;
            
            // Map the request items to control tests
            Request_Item_Control__c requestControlMapping1 = ARL_TestDataFactory.createRequestItemControl(reqItem1.id,controlTest1.Id);
            insert requestControlMapping1;
            
            Request_Item_Control__c requestControlMapping2 = ARL_TestDataFactory.createRequestItemControl(reqItem2.id,controlTest2.Id);
            insert requestControlMapping2;

            /**
             * Audit Cycle Setup
             */
            // Create shell audit cycle
            Audit_Cycle__c auditCycle = ARL_TestDataFactory.createAuditCycle();
            insert auditCycle;
            
            // Configure audit cycle coverage
            Audit_Cycle_Coverage__c acc1 = ARL_TestDataFactory.createAuditCycleCoverage(auditCycle.Id);
            acc1.Area__c = 'HITRUST';
            acc1.Scope__c  = 'HK';
            acc1.KARL_Audit_Team__c = auditTeam1.id; // Map this to audit team 1
            insert acc1;
            
            Audit_Cycle_Coverage__c acc2 = ARL_TestDataFactory.createAuditCycleCoverage(auditCycle.Id);
            acc2.Area__c = 'FedRAMP Moderate';
            acc2.Scope__c  = 'HK';
            acc2.KARL_Audit_Team__c = auditTeam1.id; // Map this to audit team 1
            insert acc2;
            
            Audit_Cycle_Coverage__c acc3 = ARL_TestDataFactory.createAuditCycleCoverage(auditCycle.Id);
            acc3.Area__c = 'SOC 2';
            acc3.Scope__c  = 'HK';
            acc3.KARL_Audit_Team__c = auditTeam2.id; // Map this to audit team 2
            insert acc3;

            /**
             * Area Requirement Setup
             */
            Area_Requirement__c areaRequirement_1 = ARL_TestDataFactory.createAreaRequirement('1.1.1', 'PCI DSS');
            insert areaRequirement_1;

            // More than 1, so we test recursion doesn't block additional items
            Area_Requirement__c areaRequirement_2 = ARL_TestDataFactory.createAreaRequirement('2.1.1', 'PCI DSS');
            insert areaRequirement_2;

            Request_Item_Area__c areaMap1 = ARL_TestDataFactory.mapAreaRequirement(reqItem1.Id, areaRequirement_1.Id);
            insert areaMap1;

            Request_Item_Area__c areaMap2 = ARL_TestDataFactory.mapAreaRequirement(reqItem1.Id, areaRequirement_2.Id);
            insert areaMap2;
        }
    }
    
    /**
    * @author Ben Harvie
    * @email ben.harvie@salesforce.com
    * @description This method tests the generation of the ARL Reports using the ARLReportController
    */
    @isTest
    private static void internalReport(){
        User u = ARL_TestDataFactory.createOpsAdmin();
       
        // Run the test as the SCEA Ops Admin
        System.runAs(u) {
            Test.startTest();

                // Generate the Cycle and ARI Requests based on the test data setup above
                Audit_Cycle__c auditCycle = [SELECT id FROM Audit_Cycle__c LIMIT 1];
                ARL_CreateCycleReqItemController.createCycleRequestItems(auditCycle.Id);

                // Run tests for drop down menus data collection
                List<KARL_Audit_Team__c> auditTeams = KARL_Utility.getAuditTeams();
                System.assertEquals(2, auditTeams.size(), 'Incorrect number of audit teams' ); // Expect 2x

                List<Audit_Cycle__c> auditCycles = KARL_Utility.getAuditCycles();
                System.assertEquals(1, auditCycles.size(), 'Incorrect number of audit cycles' ); // Expect 1x

                Map<String, String> auditScopes = KARL_Utility.getAuditCycleScopes();
                // No assert here - this is a picklist collection
                
                // Generate report (Salesforce internal)
                List<KARL_ARLReportController.ReportWrapper> reportItemsInternal = KARL_ARLReportController.getReport(auditCycles[0].Id, 'HK', null, false, 'All_Aoc', 'All', 'All_Assigned','typeSample');
                system.assertEquals(2, reportItemsInternal.size(), 'Incorrect number of items on the internal report'); // Expecting 2x CREQs

            Test.stopTest();
        }
    }
    
    /**
    * @author Ben Harvie
    * @email ben.harvie@salesforce.com
    * @description This method tests the generation of the ARL Reports using the ARLReportController
    */
    @isTest
    private static void internalAuditorFollowups(){
        // Setup the community user (External Auditor)
        User communityUser = [SELECT id, contactId FROM User WHERE UserName = 'a12345@bqww.com' LIMIT 1];
        ARL_TestDataFactory.applyExternalAuditorPerms(communityUser.Id);

        User opsAdmin = ARL_TestDataFactory.createOpsAdmin();
        User auditMgr = ARL_TestDataFactory.createAuditManager();
       
        Audit_Cycle__c auditCycle = [SELECT id FROM Audit_Cycle__c LIMIT 1];
        // Run the setup as the SCEA Ops Admin (required for future calls)
        System.runAs(opsAdmin) {
            Test.startTest();
                // Generate All Requests
                ARL_CreateCycleReqItemController.createCycleRequestItems(auditCycle.Id);

                // Set a status as Provided to Auditor
                Cycle_Request_Item__c cycleRequest = [SELECT id, Cycle_Request_Status__c FROM Cycle_Request_Item__c LIMIT 1];
                List<Object> jsonList = (List<Object>)JSON.deserializeUntyped('[{"Id": "' + cycleRequest.Id + '", "Cycle_Request_Status__c": "Provided to Auditor"}]');
                KARL_ARLReportController.updateStatus(jsonList, false);
            Test.stopTest();
        }
        // Run as Community User to set Follow-up Status
        System.runAs(communityUser){
            try{
                KARL_Auditor_Request_Item__c auditorRequest = [SELECT id, KARL_Status__c FROM KARL_Auditor_Request_Item__c WHERE KARL_Cycle_Request_Status__c = 'Provided to Auditor' LIMIT 1];
                List<Object> jsonList = (List<Object>)JSON.deserializeUntyped('[{"Id": "' + auditorRequest.Id + '", "KARL_Status__c": "Testing in Progress"}]');
                KARL_ARLReportController.updateStatus(jsonList, true);
            }catch(exception e){
                
            }
        }
        // Run the assertion tests as the SCEA Audit Manager
        System.runAs(auditMgr) {
            // Generate report (Salesforce internal)
            List<KARL_ARLReportController.AriReportWrapper> reportItemsInternal = KARL_ARLReportController.getAriReport(auditCycle.Id, 'HK', 'All_Aoc','All_Assigned');
            //system.assertEquals(1, reportItemsInternal.size(), 'Incorrect number of items on the internal report'); // Expecting 1x ARI in follow-up
        }
    }

    /**
    * @author Ben Harvie
    * @email ben.harvie@salesforce.com
    * @description This method tests the generation of the ARL Reports using the ARLReportController
    */
    @isTest
    private static void auditorReport(){
        // Setup the community user (External Auditor)
        User communityUser = [SELECT id, contactId FROM User WHERE UserName = 'a12345@bqww.com' LIMIT 1];
        ARL_TestDataFactory.applyExternalAuditorPerms(communityUser.Id);

        // Setup Ops Admin
        User u = ARL_TestDataFactory.createOpsAdmin();

        Contact con = [SELECT Id FROM Contact LIMIT 1];
        Audit_Cycle__c auditCycle = [SELECT Id FROM Audit_Cycle__c LIMIT 1];
        KARL_Audit_Team__c auditTeam = [SELECT Id FROM KARL_Audit_Team__c LIMIT 1];
       
        // Run the test as the SCEA Ops Admin
        System.runAs(u) {

            Test.startTest();

            // Generate the Cycle and ARI Requests based on the test data setup above
            ARL_CreateCycleReqItemController.createCycleRequestItems(auditCycle.Id);
            KARL_Audit_Firm__c auditFirm = [SELECT Id FROM KARL_Audit_Firm__c LIMIT 1];

            //List<KARL_Audit_Team__c> auditTeams = KARL_ARLReportController.getAuditTeams();
            //System.debug('Internal Audit Teams >> ' + auditTeams);
            //System.debug('Share with UID >> ' + communityUser.Id);
            //System.debug('Share with ICD >> ' + communityUser.contactId);
            //System.debug('Share with Con >> ' + con.Id);

            KARL_Audit_Team_Contacts__c auditTeamCon1 = ARL_TestDataFactory.createAuditTeamContact(auditTeam.Id, con.Id);
            insert auditTeamCon1;

            Test.stopTest();
        }

        // Run as Community User
        System.runAs(communityUser){

                // Run tests for drop down menus data collection
                List<KARL_Audit_Team__c> auditTeams = KARL_Utility.getAuditTeams();
                System.assertEquals(2, auditTeams.size(), 'Incorrect number of audit teams' ); // Expect 2x

                List<Audit_Cycle__c> auditCycles = KARL_Utility.getAuditCycles();
                System.assertEquals(1, auditCycles.size(), 'Incorrect number of audit cycles' ); // Expect 1x

                Map<String, String> auditScopes = KARL_Utility.getAuditCycleScopes();
                System.assert(auditScopes!=null && auditScopes.size()>0, 'Picklist returned empty');
                
                // Generate report (Salesforce internal)
                List<KARL_ARLReportController.ReportWrapper> reportItemsAuditor = KARL_ARLReportController.getReport(auditCycles[0].Id, 'HK', auditTeam.Id, true, 'All_Aoc', 'All_A', null, 'typeAll');
                system.assertEquals(1, reportItemsAuditor.size(), 'Incorrect number of items on the auditor report'); // Expecting 1x ARI
        }
    }
    
    /**
    * @author Ben Harvie
    * @email ben.harvie@salesforce.com
    * @description This method tests the update of SFDC Status
    */
    @isTest
    private static void updateSalesforceStatus(){
        // Setup Ops Admin
        User u = ARL_TestDataFactory.createOpsAdmin();
       
        // Run the test as the SCEA Ops Admin
        System.runAs(u) {
            Test.startTest();

                // Generate the Cycle and ARI Requests based on the test data setup above
                Audit_Cycle__c auditCycle = [SELECT id FROM Audit_Cycle__c LIMIT 1];
                ARL_CreateCycleReqItemController.createCycleRequestItems(auditCycle.Id);

                // Collect one of the CREQs
                // Cycle_Request_Item__c.Cycle_Request_Status__c
                Cycle_Request_Item__c cycleRequest = [SELECT id, Cycle_Request_Status__c FROM Cycle_Request_Item__c LIMIT 1];

                List<Object> jsonList = (List<Object>)JSON.deserializeUntyped('[{"Id": "' + cycleRequest.Id + '", "Cycle_Request_Status__c": "Provided to Auditor"}]');

                KARL_ARLReportController.updateStatus(jsonList, false);

                Cycle_Request_Item__c updatedCrequest = [SELECT id, Cycle_Request_Status__c FROM Cycle_Request_Item__c LIMIT 1];
                system.assertEquals('Provided to Auditor', updatedCrequest.Cycle_Request_Status__c, 'Status not updated'); 

            Test.stopTest();
        }
    }
    
    /**
    * @author Ben Harvie
    * @email ben.harvie@salesforce.com
    * @description This method tests the update of Auditor Status
    */
    @isTest
    private static void updateAuditorStatus(){
        // Setup the community user (External Auditor)
        User communityUser = [SELECT id, contactId FROM User WHERE UserName = 'a12345@bqww.com' LIMIT 1];
        ARL_TestDataFactory.applyExternalAuditorPerms(communityUser.Id);

        // Setup Ops Admin
        User u = ARL_TestDataFactory.createOpsAdmin();
        // Setup the community user (External Auditor)
        
        Contact con = [SELECT Id FROM Contact LIMIT 1];
        Audit_Cycle__c auditCycle = [SELECT Id FROM Audit_Cycle__c LIMIT 1];
        KARL_Audit_Team__c auditTeam = [SELECT Id FROM KARL_Audit_Team__c LIMIT 1];
       
        // Run the test as the SCEA Ops Admin
        System.runAs(u) {

            Test.startTest();

            // Generate the Cycle and ARI Requests based on the test data setup above
            ARL_CreateCycleReqItemController.createCycleRequestItems(auditCycle.Id);
            KARL_Audit_Firm__c auditFirm = [SELECT Id FROM KARL_Audit_Firm__c LIMIT 1];

            KARL_Audit_Team_Contacts__c auditTeamCon1 = ARL_TestDataFactory.createAuditTeamContact(auditTeam.Id, con.Id);
            insert auditTeamCon1;

            Cycle_Request_Item__c cycleRequest = [SELECT id, Cycle_Request_Status__c FROM Cycle_Request_Item__c LIMIT 1];

            List<Object> jsonList = (List<Object>)JSON.deserializeUntyped('[{"Id": "' + cycleRequest.Id + '", "Cycle_Request_Status__c": "Provided to Auditor"}]');

            KARL_ARLReportController.updateStatus(jsonList, false);

            Test.stopTest();
        }
        // Run the test as the SCEA Ops Admin
        System.runAs(communityUser) {
            // Collect one of the CREQs
            KARL_Auditor_Request_Item__c auditorRequest = [SELECT id, KARL_Status__c FROM KARL_Auditor_Request_Item__c LIMIT 1];

            List<Object> jsonList = (List<Object>)JSON.deserializeUntyped('[{"Id": "' + auditorRequest.Id + '", "KARL_Status__c": "Testing in Progress"}]');

            KARL_ARLReportController.updateStatus(jsonList, true);

            KARL_Auditor_Request_Item__c updatedArequest = [SELECT id, KARL_Status__c FROM KARL_Auditor_Request_Item__c LIMIT 1];
            system.assertEquals('Testing in Progress', updatedArequest.KARL_Status__c, 'Status not updated'); 
        }
    }
    
    /**
    * @author Lakshya Arora
    * @email lakshya.arora@salesforce.com
    * @description This method tests the areaFilters
    */
    @isTest
    private static void areaFiltersTest(){
        // Get the area filters
        System.assertEquals(' AND Request__r.Areas_of_Compliance__c includes(:areaOfCompliance) ',KARL_ARLReportController.areaFilters('Test'));
        System.assertEquals(' AND Request__r.Areas_of_Compliance__c includes(\'SFDC ICT\') ',KARL_ARLReportController.areaFilters('ICT'));
        System.assertEquals(' AND Request__r.Areas_of_Compliance__c = \'SFDC ICT\' ',KARL_ARLReportController.areaFilters('ICTUnique'));
        System.assertEquals(' AND Request__r.Areas_of_Compliance__c != \'SFDC ICT\' ',KARL_ARLReportController.areaFilters('ICTExclude'));
        System.assertEquals(' AND Request__r.Areas_of_Compliance__c includes(\'SOC 1\',\'SOC 2\',\'SOC 3\') ',KARL_ARLReportController.areaFilters('SOC'));
        System.assertEquals(' AND Request__r.Areas_of_Compliance__c includes(\'FedRAMP Moderate\',\'FedRAMP High\') ',KARL_ARLReportController.areaFilters('FedRAMP'));
        System.assertEquals(' AND Request__r.Areas_of_Compliance__c includes(\'iRAP\',\'iRAP Official: Sensitive\',\'iRAP Official: Protected\') ',KARL_ARLReportController.areaFilters('iRAP'));
        System.assertEquals(' AND Request__r.Areas_of_Compliance__c includes(\'ISO 27001\',\'ISO 27017\',\'ISO 27018\',\'NEN7510\',\'ASIP Sante HDS\') ',KARL_ARLReportController.areaFilters('ISO_All'));
    }
    
    /**
    * @author Lakshya Arora
    * @email lakshya.arora@salesforce.com
    * @description This method tests the areaFilters
    */
    @isTest
    private static void getUploadWorkflowReportTest(){
       User opsAdmin = ARL_TestDataFactory.createOpsAdmin();
        System.runAs(opsAdmin){
            Audit_Cycle__c auditCycle = [SELECT Id FROM Audit_Cycle__c LIMIT 1];
            Account acc = [SELECT Id FROM Account WHERE Name = 'salesforce.com - ESA Office Hours'];
            Contact con = new Contact(LastName = 'test', AccountId = acc.Id, Email = opsAdmin.Email);
            insert con;
            Request_Item__c reqItem = ARL_TestDataFactory.createRequestItem();
            insert reqItem;
            Cycle_Request_Item__c cycleReqItem = ARL_TestDataFactory.createCycleRequestItem(reqItem.Id,auditCycle.Id);
            cycleReqItem.Request_Assignee__c = con.Id;
            cycleReqItem.GUS_Status__c = 'Ready for Review';
            insert cycleReqItem;
            KARL_Evidence_Request__c evidenceReq = new KARL_Evidence_Request__c();
            evidenceReq.KARL_Cycle_ARL_Item__c = cycleReqItem.Id;
            evidenceReq.Evidence_Status__c = 'Ready for Review';
            insert evidenceReq;
            KARL_ARLReportController.filteredRequests.add(reqItem.Id);
            System.assert(!KARL_ARLReportController.getUploadWorkflowReport(auditCycle.Id,'','','').isEmpty());
            System.assert(!KARL_ARLReportController.getGusReport(auditCycle.Id,'','','').isEmpty());
        }
    }
}
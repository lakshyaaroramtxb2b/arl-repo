/**
 * @author Mahima Aggarwal
 * @email mahima.aggarwal@mtxb2b.com
 * @desc Test class for ARL_CycleRequestItemTriggerHelper
*/
@isTest
public class ARL_CycleRequestItemTriggerHelperTest {
	
    /**
     * @author Mahima Aggarwal
     * @email mahima.aggarwal@mtxb2b.com
     * @desc Test data setup.
    */
    @testSetup
    private static void testSetup(){
        User testUser = ARL_TestDataFactory.createExternalAuditor();
    }
        
    /**
     * @author Mahima Aggarwal
     * @email mahima.aggarwal@mtxb2b.com
     * @desc Testing with user who have External_Auditor permission set. If user changes Cycle Request Status to Follow-ups
     * then expecting a Auditor Status Tracking record for it.
    */
    @istest
    public static void unitTest(){ 
        Account acc = new Account(Name = 'salesforce.com - ESA Office Hours');
        insert acc;
        
        Contact con = new Contact(LastName = 'test', AccountId = acc.Id,Email = 'test@gmail.com');
        insert con;
        
        Contact con2 = new Contact(LastName = 'test2', AccountId = acc.Id,Email = 'test2@gmail.com');
        insert con2;
        
        Request_Item__c reqItem = ARL_TestDataFactory.createRequestItem();
        reqItem.Primary_Scope__c = 'SS';
        reqItem.KARL_Evidence_Submission_Workflow__c = true;
        reqItem.Create_GUS_Case__c = true;
        insert reqItem;
        
        Cycle_Request_Item__c cycleReqItem = ARL_TestDataFactory.createCycleRequestItem(reqItem.id,null);
        cycleReqItem.Cycle_Request_Status__c = 'New';
        cycleReqItem.Request_GUS__c = 'testWorkId';
        cycleReqItem.KARL_Update_GUS__c = true;
        cycleReqItem.Request_Tech_Details__c = 'tech details old';
        cycleReqItem.Request_Assignee__c = con.Id;
        insert cycleReqItem;
        
        KARL_Audit_Team__c auditTeam = ARL_TestDataFactory.createAuditTeam('test');
        insert auditTeam;
        
        KARL_Audit_Team_Contacts__c auditTeamCon = ARL_TestDataFactory.createAuditTeamContact(auditTeam.Id,con.Id);
        insert auditTeamCon;
        
        
        KARL_Auditor_Request_Item__c auditorReqItem = ARL_TestDataFactory.createAuditorRequestItem(cycleReqItem.Id,auditTeam.Id);
        insert auditorReqItem;
        
        cycleReqItem.Cycle_Request_Status__c = 'Provided to Auditor';
        cycleReqItem.SFDC_Comments__c = 'test';
        update cycleReqItem;
        
        /*User usr = [Select id from User where FirstName = 'External_Auditor'];
        System.debug('testUser = '+usr.Id);
        System.debug('perm set = '+[SELECT Id,PermissionSetId FROM PermissionSetAssignment WHERE AssigneeId =: usr.Id]);
        System.runAs(usr){
            cycleReqItem.Cycle_Request_Status__c = 'Follow-ups';
            cycleReqItem.Auditor_Status_Comments__c = 'test';
            cycleReqItem.Request_Tech_Details__c = 'tech details';
            cycleReqItem.Request_Assignee__c = con2.Id;
            update cycleReqItem;
            
            List<Auditor_Status_Tracking__c> crs = [select id from Auditor_Status_Tracking__c];
            
            //Expecting one record in Auditor Status Tracking as External Auditor has changed the Status to Follow-ups.
            system.assertEquals(2, crs.size() );
        }*/
    }
    
    /**
     * @author Lakshya Arora
     * @email lakshya.arora@mtxb2b.com
     */
    @istest
    public static void populateRequestAssignAndCreateSLATrackingRecordTest(){ 
        Account acc = new Account(Name = 'salesforce.com - ESA Office Hours');
        insert acc;
        
        Contact con = new Contact(LastName = 'test', AccountId = acc.Id,Email = 'test@gmail.com');
        insert con;
        
        Contact con2 = new Contact(LastName = 'test2', AccountId = acc.Id,Email = 'test2@gmail.com');
        insert con2;
        
        Request_Item__c reqItem = ARL_TestDataFactory.createRequestItem();
        reqItem.Primary_Scope__c = 'SS';
        reqItem.KARL_Evidence_Submission_Workflow__c = true;
        reqItem.Create_GUS_Case__c = true;
        insert reqItem;
        
        
        Cycle_Request_Item__c cycleReqItem1 = ARL_TestDataFactory.createCycleRequestItem(reqItem.id,null);
        cycleReqItem1.Cycle_Request_Status__c = 'New';
        cycleReqItem1.Request_GUS__c = 'testWorkId';
        cycleReqItem1.KARL_Update_GUS__c = true;
        cycleReqItem1.Request_Tech_Details__c = 'tech details old';
        cycleReqItem1.Request_Assignee__c = con.Id;
        insert cycleReqItem1;
        
        List<Cycle_Request_Item__c> cycleRequestItemList = new List<Cycle_Request_Item__c>();
        Cycle_Request_Item__c cycleReqItem = ARL_TestDataFactory.createCycleRequestItem(reqItem.id,null);
        cycleReqItem.Cycle_Request_Status__c = 'Ready for Review';
        cycleReqItem.Request_GUS__c = 'testWorkId';
        cycleReqItem.KARL_Update_GUS__c = true;
        cycleReqItem.Request_Tech_Details__c = 'tech details old';
        cycleReqItem.Request_Assignee__c = con.Id;
        cycleRequestItemList.add(cycleReqItem);
        cycleReqItem1.Cycle_Request_Status__c = 'Ready for Review';
        cycleRequestItemList.add(cycleReqItem1);
        upsert cycleRequestItemList;
    }
    
    @istest
    public static void gusWorkCreationRecordTest(){ 
         Account acc = new Account(Name = 'salesforce.com - ESA Office Hours');
        insert acc;
        
        Contact con = new Contact(LastName = 'test', AccountId = acc.Id,Email = 'test@gmail.com');
        insert con;
        
        Contact con2 = new Contact(LastName = 'test2', AccountId = acc.Id,Email = 'test2@gmail.com');
        insert con2;
        
        Request_Item__c reqItem = ARL_TestDataFactory.createRequestItem();
        reqItem.Primary_Scope__c = 'SS';
        reqItem.KARL_Evidence_Submission_Workflow__c = true;
        reqItem.Create_GUS_Case__c = true;
        insert reqItem;
        
        Audit_Cycle__c auditCycle = ARL_TestDataFactory.createAuditCycle();
        insert auditCycle;
        KARL_Gus_Product_Tag__c prodtag = new KARL_Gus_Product_Tag__c();
        prodtag.Active__c = true;
        prodtag.Active_Scrum_Team__c = true;
        insert prodtag;
        List<KARL_Auto_Assignment_Configuration__c> autoAssignmentConfigList = new List<KARL_Auto_Assignment_Configuration__c>();
        for(Integer i=0;i<4;i++){
            KARL_Auto_Assignment_Configuration__c aacObj = new KARL_Auto_Assignment_Configuration__c();
            //aacObj.Audit_Cycle__c = auditCycle.Id;
            aacObj.Priorities__c = i+'';
            aacObj.Priority_0_Assignment_Date__c = System.today();
            aacObj.Priority_1_Assignment_Date__c = System.today();
            aacObj.Priority_2_Assignment_Date__c = System.today();
            aacObj.Priority_3_Assignment_Date__c = System.today();
            aacObj.Days_to_Complete__c = 10;
            aacObj.Default_Product_Tag__c = prodtag.Id;
            aacObj.Enable_Chatter_Posts__c = true;
            autoAssignmentConfigList.add(aacObj);
        }
        autoAssignmentConfigList[1].Priority_0_Assignment_Date__c = System.today().addDays(5);
        autoAssignmentConfigList[2].Priority_0_Assignment_Date__c = System.today().addDays(-1);
        autoAssignmentConfigList[3].Priority_0_Assignment_Date__c = System.today();
        insert autoAssignmentConfigList;
        
        List<Audit_Cycle_Coverage__c> auditCycleCoverageList = new List<Audit_Cycle_Coverage__c>();
        Map<Integer,String> priorityToScopeMap = new Map<Integer,String>();
        priorityToScopeMap.put(0,'AS');
        priorityToScopeMap.put(1,'AUST');
        priorityToScopeMap.put(2,'B2BC');
        priorityToScopeMap.put(3,'CHAT');
        for(Integer i=0;i<4;i++){
            Audit_Cycle_Coverage__c auditCycleCoverage = new Audit_Cycle_Coverage__c();
            auditCycleCoverage.Audit_Cycle__c = auditCycle.Id;
            auditCycleCoverage.Area__c = 'iRAP';
            auditCycleCoverage.Scope__c = priorityToScopeMap.get(i);
            auditCycleCoverage.Auto_Assignment_Configuration__c = autoAssignmentConfigList[i].Id;
            auditCycleCoverageList.add(auditCycleCoverage);
        }
        insert auditCycleCoverageList;
        
         KARL_Audit_Team__c auditTeam = ARL_TestDataFactory.createAuditTeam('test');
        insert auditTeam;
        
        KARL_Audit_Team_Contacts__c auditTeamCon = ARL_TestDataFactory.createAuditTeamContact(auditTeam.Id,con.Id);
        insert auditTeamCon;
        
        List<Cycle_Request_Item__c> cycleReqItemList = new List<Cycle_Request_Item__c>();
        for(Integer i=0;i<4;i++){
            Cycle_Request_Item__c cycleReqItem = ARL_TestDataFactory.createCycleRequestItem(reqItem.id,null);  
            cycleReqItem.Cycle_Request_Status__c = 'New';
            cycleReqItem.KARL_Update_GUS__c = true;
            cycleReqItem.Request_Tech_Details__c = 'tech details old';
            cycleReqItem.Request_Assignee__c = con.Id;
            cycleReqItem.Audit_Cycle__c = auditCycle.Id;
            cycleReqItem.Audit_Cycle_Coverage__c = auditCycleCoverageList[i].Id;
            cycleReqItemList.add(cycleReqItem);
        }
        insert cycleReqItemList;
        
        
        for(Cycle_Request_Item__c criObj:cycleReqItemList){
            criObj.KARL_Update_GUS__c = true;
            criObj.GUS_Status__c = 'Ready for Review';
            criObj.Request_Tech_Details__c = 'Request Details changed for update trigger';
        }
        Test.startTest();
        update cycleReqItemList;
        Test.stopTest();
        Set<Id> cycleRequestItemIdSet = new Set<Id>();
        for(Cycle_Request_Item__c criObj:cycleReqItemList){
            cycleRequestItemIdSet.add(criObj.Id);
        }
        //Need to call explicitly as Request_Gus can not be created in test class so it will always be null
        ARL_CycleRequestItemTriggerHelper.processGUSRecordUpdates(cycleRequestItemIdSet,new Set<Id>{auditCycle.Id},new Set<Id>());
        System.assert(![SELECT Id FROM KARL_GUS_SLA_Tracking__c].isEmpty());
    }
}
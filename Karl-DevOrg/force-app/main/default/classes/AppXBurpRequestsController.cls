public with sharing class AppXBurpRequestsController {



    public boolean getIsInCopyMode() {
        return !'true'.equals(ApexPages.currentPage().getParameters().get('copyMode'));
    }


    private static final Id ORG_WIDE_EMAIL_ADDRESS_ID = '0D2300000004DuF';

    private void emailAMessage(String name, String to, String subject, String body){
      
            Messaging.reserveSingleEmailCapacity(1);
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String[] toAddresses = new String[] {to};
            mail.setToAddresses(toAddresses);
            mail.setReplyTo('noreply@salesforce.com');
            mail.setSubject(subject);
            mail.setOrgWideEmailAddressId(ORG_WIDE_EMAIL_ADDRESS_ID);
            
            string body2 = 'Hello ' + name + ', \r\n' + '\r\n' + 
            body + '\r\n' + '\r\nThanks,' + '\r\n' + 
            'Salesforce.com Product Security Team';
            
            mail.setPlainTextBody(body2);
            
            Messaging.sendEmail(new Messaging.SingleEmailMessage[]{mail});
    
    }
    
    

    //keeping these methods separate in case we want to do something special
    //like send emails or have separate functionality
    public PageReference reqIncompleteData() {
        WebAppScanner__c a = [Select Id, Status__c, Reason_for_denying__c, Name__c, appname__c, CreatedDate, company__c, email__c FROM WebAppScanner__c WHERE Id = :scanIdChosen LIMIT 1];
        a.status__c = 'Not-Approved';
        a.Reason_for_Denying__c = 'Incomplete Data';
        upsert a;
        
        emailAMessage(a.Name__c, a.email__c, 'Burp License Request Denied - Incomplete Data', 
        'Your Burp License Request for ' + a.company__c + ' was denied beacause the data you provided was either incomplete or missing.  Please make sure to provide your full name, full company name, company email address, and application name when submitting the request.');
                
        return null;
    }


    public PageReference reqExistingLicense() {
        WebAppScanner__c a = [Select Id, Status__c, Reason_for_denying__c, Name__c, appname__c, CreatedDate, company__c, email__c FROM WebAppScanner__c WHERE Id = :scanIdChosen LIMIT 1];
        a.status__c = 'Not-Approved';
        a.Reason_for_Denying__c = 'Existing License';
        upsert a;

        
        emailAMessage(a.Name__c, a.email__c, 'Burp License Request Denied - Existing License', 
        'Your Burp License Request for ' + a.company__c + ' was denied beacause we have already issued a license for your company this year.  Each qualified partner is eligible for only one Burp License per year. ');
        
        return null;
    }


    public PageReference reqNoPartnerOffering() {
        WebAppScanner__c a = [Select Id, Status__c, Reason_for_denying__c, Name__c, appname__c, CreatedDate, company__c, email__c FROM WebAppScanner__c WHERE Id = :scanIdChosen LIMIT 1];
        a.status__c = 'Not-Approved';
        a.Reason_for_Denying__c = 'No Partner Offering';
        upsert a;
        
        emailAMessage(a.Name__c, a.email__c, 'Burp License Request Denied - No Partner Offering', 
        'Your Burp License Request for ' + a.company__c + ' was denied beacause we could not confirm that you have at least one partner application on file.  Please contact your account executive to ensure that your records are up to date.');
        
        return null;
    }


    public PageReference reqDupe() {
        WebAppScanner__c a = [Select Id, Status__c, Reason_for_denying__c, Name__c, appname__c, CreatedDate, company__c, email__c FROM WebAppScanner__c WHERE Id = :scanIdChosen LIMIT 1];
        a.status__c = 'Not-Approved';
        a.Reason_for_Denying__c = 'Duplicate Request';
        upsert a;
        return null;
    }


    public PageReference reqApprove() {
        WebAppScanner__c a = [Select Id, Status__c, Reason_for_denying__c, Name__c, appname__c, CreatedDate, company__c, email__c FROM WebAppScanner__c WHERE Id = :scanIdChosen LIMIT 1];
        a.status__c = 'Approved';
        upsert a;
        
        emailAMessage(a.Name__c, a.email__c, 'Burp License Request Approved', 
        'Your Burp License Request for ' + a.company__c + ' was approved.  Please keep an eye out for an email from the vendor over the next couple of days.  If you have not recieved a license within 7 days, please contact us at appxsecurityreview@salesforce.com');

        return null;
    }

    public String scanIdChosen {get; set;}

    public WebAppScanner__c[] getPendingRequests(){
        return [SELECT Id,X62_Account_ID__c, X62_Account_Name__c, X62_Partner_Eligible__c, Partner_Portal_Username__c,appname__c, company__c, Name__c, email__c, CreatedDate,X62_Opportunity_Search__c FROM WebAppScanner__c WHERE status__c = 'New' ORDER BY CreatedDate ASC];
    }    
    
}
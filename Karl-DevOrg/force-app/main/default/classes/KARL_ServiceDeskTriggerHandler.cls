/**
 * @author Lakshya Arora
 * @email lakshya.arora@mtxb2b.com
 * @description This is trigger handler class which handles the opeartion on insert,update,delete
 * @note As we have external sharing model as private for contact, scope notifications configuration the trigger 
 * should have accessed to all records for approval emails, that's why inherited sharing is used as trigger runs in system mode
*/
public inherited sharing class KARL_ServiceDeskTriggerHandler {
    /**
    * @author Lakshya Arora
    * @email lakshya.arora@mtxb2b.com
    * @description Handles before insert operations
    * @param New List of KARL_Service_Desk__c
    */
    public static void beforeInsertOperations(List<KARL_Service_Desk__c> newList){
		  KARL_ServiceDeskTriggerHelper.populateAutoNumberAndSubject(newList);
      KARL_ServiceDeskTriggerHelper.populateGRCAssigneeUser(newList,null);
    }

    /**
    * @author Lakshya Arora
    * @email lakshya.arora@mtxb2b.com
    * @description Handles after insert operations
    * @param New List of KARL_Service_Desk__c
    */
    public static void afterInsertOperations(List<KARL_Service_Desk__c> newList){
		  KARL_ServiceDeskTriggerHelper.createSLATrackingRecords(newList,null);
      KARL_ServiceDeskTriggerHelper.sendApprovalProcessEmails(newList,null);
    }
    
     /**
    * @author Lakshya Arora
    * @email lakshya.arora@mtxb2b.com
    * @description Handles before update operations
    * @param1 New List of KARL_Service_Desk__c
    * @param2 OldMap -> Map of Id vs old KARL_Service_Desk__c record info
    */
    public static void beforeUpdateOperations(List<KARL_Service_Desk__c> newList,Map<Id,KARL_Service_Desk__c> oldMap){
      KARL_ServiceDeskTriggerHelper.populateGRCAssigneeUser(newList,oldMap);
      KARL_ServiceDeskTriggerHelper.updateRejectedComments(newList,oldMap);
      // KARL_ServiceDeskTriggerHelper.createChatterOnStatusChange(newList,oldMap);
    }

     /**
    * @author Lakshya Arora
    * @email lakshya.arora@mtxb2b.com
    * @description Handles after update operations
    * @param1 New List of KARL_Service_Desk__c
    * @param2 OldMap -> Map of Id vs old KARL_Service_Desk__c record info
    */
    public static void afterUpdateOperations(List<KARL_Service_Desk__c> newList,Map<Id,KARL_Service_Desk__c> oldMap){
      KARL_ServiceDeskTriggerHelper.sendApprovalProcessEmails(newList,oldMap);
      KARL_ServiceDeskTriggerHelper.afterApprovedOperations(newList,oldMap);
      KARL_ServiceDeskTriggerHelper.createSLATrackingRecords(newList,oldMap);
    }
}
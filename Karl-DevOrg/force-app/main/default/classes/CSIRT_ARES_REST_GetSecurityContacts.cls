@RestResource(urlMapping='/ARES/Org62/GetSecurityContacts/*')
global with sharing class CSIRT_ARES_REST_GetSecurityContacts {
	@HttpPost 
	global static String GetSecurityContacts(Id accountId) {
        
        CSIRT_ARES_InternalOrgIntegration Org62 = new CSIRT_ARES_InternalOrgIntegration('ORG62');
        return Org62.GetAccountSecurityContacts(accountId);
        
        
   }
}
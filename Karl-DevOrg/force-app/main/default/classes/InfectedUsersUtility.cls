/*
 * @Description: Utility methods to rollup infection counts from Case_to_User_Relationship__c to Dyre_Company_Org__c 
 * @Authour: Aliyu Turaki prodsec 
 * @Notes: 
*/
public class InfectedUsersUtility {
    
    private static final String DYRE_PARENT_CASE_SUBJECT = 'Dyre malware activity';
    
    // Bulkified
    public static void RollUpDyreInfectedUsers(List<Case_to_User_Relationship__c> infectedUsers, boolean isInsert){
        
        // Get the DYRE parent case
        List<Case> dyreParentCase = [SELECT id FROM Case WHERE subject = :DYRE_PARENT_CASE_SUBJECT LIMIT 1];
        if(dyreParentCase == NULL) return;
        string dyreParentCaseId =  dyreParentCase[0].id;
        
        
        // Get case to infected user counts
        Map<String, Integer> caseToInfectedUsers = new Map<String,Integer>();
        for(Case_to_User_Relationship__c infectedUser : infectedUsers){
            
            if(!caseToInfectedUsers.containsKey(infectedUser.case__c)) {
                
                caseToInfectedUsers.put(infectedUser.case__c, 1);
                
            }else{
                
                integer currentCount = caseToInfectedUsers.get(infectedUser.case__c) + 1;              
                caseToInfectedUsers.put(infectedUser.case__c, currentCount);
                
            }
            
            
        }
        
        // Exit if nothing to do.
       	if(caseToInfectedUsers.size() == 0) return;
        
        // Determine if infected users are DYRE related based on parent case id
        Map<Id, Integer> accountToInfectedUsers = new Map<Id, Integer>();
        for(Case dyreInfectedCase : [SELECT id, accountId FROM Case WHERE parentId = :dyreParentCaseId AND id IN :caseToInfectedUsers.keySet()]){
			
            if(!accountToInfectedUsers.containsKey(dyreInfectedCase.accountId)) {
                
                accountToInfectedUsers.put(dyreInfectedCase.accountId, caseToInfectedUsers.get(dyreInfectedCase.id));
                
            }else{
                
                integer currentCount = accountToInfectedUsers.get(dyreInfectedCase.accountId) + caseToInfectedUsers.get(dyreInfectedCase.id);
                
                accountToInfectedUsers.put(dyreInfectedCase.accountId, currentCount);
                
            }
       
        
        }
    
    	// Exit if nothing to do.
       	if(accountToInfectedUsers.size() == 0) return;
    
    	// Get 62Org account id to infected counts. 
        Map<String, Integer> Org62IdToInfectedUsers= new Map<String, Integer>();
    	for(Account acc : [SELECT Org62_Account_ID__c FROM Account 
                             WHERE id IN :accountToInfectedUsers.keySet()]){
        

           Org62IdToInfectedUsers.put(acc.Org62_Account_ID__c, accountToInfectedUsers.get(acc.id)) ;
                                 
        }
                           
  
                           
        // Exit if nothing to do.                                                                          
        if(Org62IdToInfectedUsers.size() == 0) return;     
    
    	// Rollup to Dyre_Company_Org__c
        List<Dyre_Company_Org__c> dyreOrgsToUpdate = New List<Dyre_Company_Org__c>(); 
    	for(Dyre_Company_Org__c dyreOrg : [SELECT id, X62_Org_Account_Id__c, Number_of_users_infected__c FROM Dyre_Company_Org__c
                                          	WHERE X62_Org_Account_Id__c IN : Org62IdToInfectedUsers.keySet()]){
                                                                                  
            if(dyreOrg.Number_of_users_infected__c == NULL) dyreOrg.Number_of_users_infected__c = 0;  
        	
            if(isInsert){
                
            	dyreOrg.Number_of_users_infected__c += Org62IdToInfectedUsers.get(dyreOrg.X62_Org_Account_Id__c);
                
            }else{
                
                dyreOrg.Number_of_users_infected__c -= Org62IdToInfectedUsers.get(dyreOrg.X62_Org_Account_Id__c);
            }
                                                
            dyreOrgsToUpdate.add(dyreOrg);                                 
                                                
       	}
        if(dyreOrgsToUpdate.size() > 0) {
           
            update dyreOrgsToUpdate;
            
        }     
        
    }

}
/*

    Developer Name: Jorge L Caceres - jcaceres@salesforce.com | February 2016
    
    Description: Handles setting up Accounts (BizUnits) for Trust Maturity 

*/

public class TM_BUsharingUtil implements Database.Batchable<sObject> {
        
    /* constructors */
    
    public TM_BUsharingUtil (String query) {
        objectivesQuery = query;     
    }
    
    /* class static variables */

    public static Map<String, Group> allGroupsMap {
        get {
                // only get if the first time
                if (allGroupsMap == null) {
                    allGroupsMap = new Map<String, Group> ();
                    for (Group g : [Select Id, DeveloperName, Name From Group]) {
                        allGroupsMap.put(g.DeveloperName, g);
                    }    
                } 
                return allGroupsMap;
            }        
        public set { allGroupsMap = value; }
        
        }
        
        private String objectivesQuery;

    /* public methods */

    // create all of the access control (group, groupmembers, sharingrules) required for a buz unit
    public static void createAccessControlObjects (List<Account> accounts, Map<Id, Account>oldAccounts) {
        
        List<Account> accountsToProcess = new List<Account>();        
        // if updating accounts only update if switching TMM tracking from false to true or groups are missing
        if (oldAccounts == null) accountsToProcess = accounts; // not an update trigger
        else {
            // identified accounts changed
            List<Account> accountsChanged = new List<Account>();
            for (Account a : accounts) {
                if ((a.TMM_Tracking__c && !oldAccounts.get(a.Id).TMM_Tracking__c) ||
                   (a.TMM_Tracking__c && (a.OwnerId != oldAccounts.get(a.Id).OwnerId)) ||
                   !allGroupsMap.containsKey(getBUDoersName(a.Id)) ||
                   !allGroupsMap.containsKey(getBUObserversName(a.Id))) {                  
                    accountsToProcess.add(a); 
                }                                       
            }
        }
 
        // process all accounts (the DMLs in here are not bulkified but OK since BUs are create one at a time) 
        for (Account acct : accountsToProcess) if (acct.TMM_Tracking__c) createPublicGroups(acct);     

    }
        
    // create share records for objectives
    public static void createObjectiveSharingRules (List<TM_Objective__c> objectives) {
        // process all incoming objectives 
        List<TM_Objective__Share> objectiveShares = new List<TM_Objective__Share>();
        for (TM_Objective__c objective : objectives) {
            try {
                // share with observers
                objectiveShares.add(new TM_Objective__Share(ParentId = objective.Id, AccessLevel = 'Read', 
                                        UserOrGroupId = allGroupsMap.get(getBUObserversName(Id.valueOf(objective.Business_Unit_Id_Source__c))).Id));
            } catch (exception e) {system.debug(e);}
            try {
                // share with doers
                objectiveShares.add(new TM_Objective__Share(ParentId = objective.Id, AccessLevel = 'Edit',
                                        UserOrGroupId = allGroupsMap.get(getBUDoersName(Id.valueOf(objective.Business_Unit_Id_Source__c))).Id));
            } catch (exception e) {system.debug(e);}
        }
        Database.insert(objectiveShares, false);   
    }
        
    // return doers group developerName for a particular account ID
    public static String getBUDoersName (Id acctId) {
        return getBUName(acctId) + '_Doers';
    }
    
    // return observers group developerName for a particular account ID
    public static String getBUObserversName (Id acctId) {
        // return observers group developerName 
        return getBUName(acctId) + '_Observers';
    }
        
    // return doers group ID for a particular account ID
    public static Id getBUDoersGroupId (Id acctId) {
        for (Group g : [select Id from Group where DeveloperName = :getBUDoersName(acctId)]) {
            return g.Id;
        }
        return null;
    }
        
    // return observers group ID for a particular account ID
    public static Id getBUObserversGroupId (Id acctId) {
        for (Group g : [select Id from Group where DeveloperName = :getBUObserversName(acctId)]) {
            return g.Id;
        }
        return null;
    }
    
    // convert old group names to new convention for developername
    public static void convertOldGroupNames () {
        // retrieve groups with old names
        List<Group> tmGroups = [select Id, Name, DeveloperName from Group 
                                where (DeveloperName like 'TM_BU%') and (NOT(DeveloperName like 'TM_BU_001%'))];
        List<String> tmBuzNames = new List<String>();

        // retrieve related accounts
        for (Group g : tmGroups) tmBuzNames.add(g.Name.replace('TM BU','').replace('Doers','').replace('Observers','').trim());
        List<Account> tmBuzAccounts = [select Id, Name from Account where Name IN : tmBuzNames 
                                       OR Name like '%Predictive Intelligence%']; // special handling
        Map<String, Account> tmBuzAccountsMap = new Map<String, Account>();
        for (Account a : tmBuzAccounts) {
            String buzNameForGroup = 'TM BU ' + a.Name;
            buzNameForGroup = buzNameForGroup.replace('(iGoDigital)',''); // special case
            buzNameForGroup = buzNameForGroup.replace('salesforce.com','Salesforce.com'); // special case
            tmBuzAccountsMap.put(buzNameForGroup.trim() + ' Doers', a);
            tmBuzAccountsMap.put(buzNameForGroup.trim() + ' Observers', a);
        }
        
        // update groups to new standard
        List<Group> groupsToUpdate = new List<Group>();
        for (Group g : tmGroups) {
            if (tmBuzAccountsMap.containsKey(g.Name)) {
                if (g.Name.contains('Doers')) g.DeveloperName = getBUDoersName(tmBuzAccountsMap.get(g.Name).Id);
                if (g.Name.contains('Observers')) g.DeveloperName = getBUObserversName(tmBuzAccountsMap.get(g.Name).Id);
                groupsToUpdate.add(g);
            } else {
            }   
        }
        if (!groupsToUpdate.isEmpty()) update groupsToUpdate;
    }

    // create public groups for an account
    public static void createPublicGroups (Account acct) {

        // create public groups
        Group doers;
        if (allGroupsMap.containsKey(getBUDoersName(acct.Id))) doers = allGroupsMap.get(getBUDoersName(acct.Id));
        else doers = new Group(DeveloperName = getBUDoersName(acct.Id), 
                                Name = getBUDoersName(acct),
                                Type = 'Regular');

        Group observers;
        if (allGroupsMap.containsKey(getBUObserversName(acct.Id))) observers = allGroupsMap.get(getBUObserversName(acct.Id));
        else observers = new Group(DeveloperName = getBUObserversName(acct.Id), 
                                Name = getBUObserversName(acct),
                                Type = 'Regular');        
        try {
            allGroupsMap.put(doers.DeveloperName, doers);
            allGroupsMap.put(observers.DeveloperName, observers);
            upsert new List<Group>{doers, observers}; 
        } catch (DmlException e)  {
            // fail silently as this means groups already exist            
        }

        // create account shares
        try {
            AccountShare shareWithObservers = new AccountShare(AccountId = acct.Id, 
                                                               UserOrGroupId = observers.Id, 
                                                               AccountAccessLevel = 'Read',
                                                               CaseAccessLevel = 'None',
                                                               ContactAccessLevel = 'None',
                                                               OpportunityAccessLevel = 'None');
           
            insert shareWithObservers;                                                                              
        } catch (DmlException e)  {
            // fail silently as this means accountshare already exist
        }

        createGroupMemberships(acct.Id, observers.Id, doers.Id);
                
    }
    
    
    /* Batch related methods */
    
    public Database.Querylocator start (Database.BatchableContext BC) {
        return Database.getQuerylocator(objectivesQuery);
    }

    public void execute (Database.BatchableContext BC, List<sObject> scope) {
        TM_BUsharingUtil.createObjectiveSharingRules((List<TM_Objective__c>) scope); 
    }
    
    public static void convertObjectives () {
        String query = 'Select Id, Business_Unit_Id_Source__c from TM_Objective__c';
        if (test.isRunningTest()) query+= ' limit 500';
        ID batchProcessId = Database.executeBatch(new TM_BUsharingUtil(query),1000);  
    }
    
    public void finish (Database.BatchableContext BC) {}
    

    /* private methods */  

    @future
    private static void createGroupMemberships (Id acctId, Id observersId, Id doersId) {
        // must be future to avoid mixed DML errors and use runAs during test context
        insert new GroupMember(GroupId = observersId, UserOrGroupId = doersId); // add doers to observers group
    }
    
    private static String getBUName (Account acct) {
        return 'TM BU ' + acct.Name.trim();
    }
    
    private static String getBUName (Id acctId) {
        return 'TM_BU_' + String.valueOf(acctId);
    }
    
    private static String getBUDoersName (Account acct) {
        // return group Name (not developer name) 
        return getBUName(acct) + ' Doers';
    }
    
    private static String getBUObserversName (Account acct) {
        // return group Name (not developer name) 
        return getBUName(acct) + ' Observers';
    }
        
}
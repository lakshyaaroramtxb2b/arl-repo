@isTest
public class SPT_CaseTriggerHelper_Test{
    @TestSetup
    static void makeData(){
        Case__x externalCaseRecord = new Case__x();
        Database.insertAsync(externalCaseRecord);

        List<Account> accountList = SPT_TestUtility.createAccountRecords('Test Account', 1, true);
        
        List<Contact> contactList = SPT_TestUtility.createContactRecords('Test Contact', 1, false);
        for(Contact con : contactList)
            con.AccountId = accountList.get(0).Id;
        INSERT contactList;

        List<Case> internalCase2List = SPT_TestUtility.createCaseRecords('New', 'USD', 'Email', 1, true);

        List<Case> internalCaseList = SPT_TestUtility.createCaseRecords('New', 'USD', 'Web', 1, false);
        for(Case caseRecord : internalCaseList) {
            caseRecord.SupportForce_Case_Id__c = externalCaseRecord.Id;
            caseRecord.RecordTypeId = SPT_Constants.SEC_RECORDTYPE_ID;
            caseRecord.ContactId = contactList.get(0).Id;
            caseRecord.ParentId = internalCase2List.get(0).Id;
        }
        INSERT internalCaseList;
    }

    @isTest
    public static void updateCaseTest() {
        
        List<Case> internalCaseList = new List<Case>([SELECT Id, SupportForce_Case_Id__c FROM Case WHERE Origin = 'Web']);
        Case caseRecord = internalCaseList.get(0);
        System.debug('SupporForce Id: '+caseRecord.SupportForce_Case_Id__c);
        caseRecord.Status = 'Incomplete';
        Test.startTest();
            Test.setMock(HttpCalloutMock.class, new SPT_MockHttpResponseGenerator());
            UPDATE caseRecord;
        Test.stopTest();
    }
}
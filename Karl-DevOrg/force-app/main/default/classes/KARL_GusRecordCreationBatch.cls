/**
 * @author Lakshya Arora
 * @email lakshya.arora@mtxb2b.com
 * @description This is schedulable and batch class to create Gus Work Records.
 */
public with sharing class KARL_GusRecordCreationBatch implements Database.Batchable<sObject>, Schedulable{
    
    /**
    * @author Lakshya Arora
    * @email lakshya.arora@mtxb2b.com
    * @description This method is for schedulable interface to execute.
    */
    public void execute(SchedulableContext sc) {
        database.executebatch( new KARL_GusRecordCreationBatch(),200);
    }

    /**
    * @author Lakshya Arora
    * @email lakshya.arora@mtxb2b.com
    * @desc This method will fetch the eligible Cycle ARL Items 
    */
    public Database.QueryLocator start(Database.BatchableContext BC){
        Date todayDate = System.today();
        string query =  'SELECT Id,Audit_Cycle__c,Audit_Cycle_Coverage__c,Request_Gus__c,Audit_Cycle_Coverage__r.Scope__c,Audit_Cycle_Coverage__r.Area__c,Audit_Cycle_Coverage__r.KARL_Audit_Team__c,Date_for_Work_Record_Creation__c,'
                        + 'epic__c,Team__c,Request_Tech_Details__c,Cycle_Request_Status__c,Audit_Cycle__r.Google_Drive_Link__c,'
                        + 'Audit_Cycle__r.Name,Request__r.EPIC_Type__c,Request__r.Metricstream_ID__c,Request__r.Request_Environments__c,'
                        + 'Request__r.Primary_Scope__c,Request__r.Areas_of_Compliance__c,Request__r.KARL_Previous_GUS_ID__c,'
                        + 'Request__r.Suggested_Product_Tag__c,Request__r.Suggested_Product_Tag__r.Name,Request__r.Suggested_Product_Tag__r.Active__c,'
                        + ' Request__r.Suggested_Product_Tag__r.Product_Tag_External_Id__c,'
                        + 'Request__r.Suggested_Product_Tag__r.Team__r.Active_c__c,'
                        + 'Request__r.Automatic_Product_Tag_Assignment__c,'
                        + 'Request__r.Create_GUS_Case__c,Request__r.Name,Request__r.Type__c,'
                        + 'Request__r.Request_Name__c,Request__r.KARL_GRC_Assignee__r.Firstname, '
                        + 'Request__r.KARL_GRC_Assignee__r.LastName, Request__r.KARL_Evidence_Submission_Workflow__c, '
                        + 'Audit_Cycle_Coverage__r.Auto_Assignment_Configuration__r.Audit_Period__c,'
                        + 'Audit_Cycle_Coverage__r.Auto_Assignment_Configuration__r.Days_to_Complete__c,'
                        + 'Audit_Cycle_Coverage__r.Auto_Assignment_Configuration__r.Default_Product_Tag__c,'
                        + 'Audit_Cycle_Coverage__r.Auto_Assignment_Configuration__r.Default_Product_Tag__r.Product_Tag_External_Id__c,'
                        + ' Request__r.LastModifiedById ,Request__r.LastModifiedDate,Request__r.KARL_GRC_Assignee__r.Name,CreatedById '
                        + ' FROM Cycle_Request_Item__c '
                        + ' WHERE  Date_for_Work_Record_Creation__c <= :todayDate AND Is_Gus_Record_Created__c = FALSE AND Audit_Cycle_Coverage__r.Auto_Assignment_Configuration__c != null'
                        + ' WITH SECURITY_ENFORCED';              
        return Database.getQueryLocator(query);
    }

   
    /**
    * @author Lakshya Arora
    * @email lakshya.arora@mtxb2b.com
    * @desc This method will create all eligibile gus records
    */
    public void execute(Database.BatchableContext BC, List<Cycle_Request_Item__c> cycleRequestItemList){
        Set<Id> cycleIds = new Set<Id>();
        Set<Id> cycleReqItemIds = new Set<Id>();
        
        for( Cycle_Request_Item__c  cri : cycleRequestItemList ){
            cycleReqItemIds.add(cri.Id);
            cycleIds.add(cri.Audit_Cycle__c);
        }

        // updated by laskshya arora -> can't use reusable method present in gus utility as it's in future context, can't call future method in batch
        processGUSRecordCreation(cycleReqItemIds, cycleIds,cycleRequestItemList); 
    }

    /**
     * @author Lakshya Arora
     * @email lakshya.arora@mtxb2b.com
     * @description This method trigger the future method to create GUS records from Cycle Request Item and find/update EPIC records.
     * NOTE: this is a modification of the same method in ARL_RequestItemTriggerHandler (created by Vishnu Kumar)
    */ 
    private static void processGUSRecordCreation(Set<Id> cycleReqItemIds, Set<Id> cycleIds,List<Cycle_Request_Item__c> cycleRequestItemList){
        System.debug(LoggingLevel.DEBUG, '----------------processGUSRecordCreation   Start--------------------');
        //Variables
        List<KARL_GUS_SLA_Tracking__c> gusSlaTrackingList = new List<KARL_GUS_SLA_Tracking__c>();
        List<ADM_Work_c__x> WorkList = new List<ADM_Work_c__x>();
        Map<Id, Cycle_Request_Item__c> cRewItemIds = new Map<Id, Cycle_Request_Item__c>();
        Set<String> epics = new Set<String>();

        
        Map<Id,Id> cycleReqIdToEvidenceIdMap = new Map<Id,Id>();
        List<KARL_Evidence_Request__c> evidenceReqUpdateList = new List<KARL_Evidence_Request__c>();

        // Generate KARL Evidence Community URL
        Network karlEvidenceNetwork;
        String loginurl,communityHomeUrl ='';
        Integer index;
        if(!Test.isRunningTest()){
            karlEvidenceNetwork = [SELECT Id FROM Network WHERE Name ='KARL Evidence Request' ];
            loginurl = Network.getLoginUrl(karlEvidenceNetwork.id);
            index = loginurl.lastIndexOf('/login');
            communityHomeUrl = loginurl.substring(0, index + 1);
            System.debug(LoggingLevel.DEBUG, 'MyDebugURL: ' + communityHomeUrl);
        }

        for(KARL_Evidence_Request__c evidenceReq : [SELECT Id, KARL_Submitted__c ,KARL_Cycle_ARL_Item__c
                                                            FROM KARL_Evidence_Request__c
                                                            WHERE KARL_Cycle_ARL_Item__c IN : cycleReqItemIds
                                                            WITH SECURITY_ENFORCED]){
            cycleReqIdToEvidenceIdMap.put(evidenceReq.KARL_Cycle_ARL_Item__c,evidenceReq.Id);
        }
        

        // Create map of Google Drive folders for evidence links
        Map<Id, Map<String,String>> cycleDriveFolders = new Map<Id, Map<String,String>>();

        for(KARL_Audit_Cycle_Drive__c cycleDrives : [SELECT Id, KARL_Drive_Scope__c ,KARL_Drive_URL__c, KARL_Audit_Cycle__c
                                                            FROM KARL_Audit_Cycle_Drive__c
                                                            WHERE KARL_Audit_Cycle__c IN : cycleIds
                                                            WITH SECURITY_ENFORCED]){
            if( !cycleDriveFolders.containsKey(cycleDrives.KARL_Audit_Cycle__c) ){
                cycleDriveFolders.put(cycleDrives.KARL_Audit_Cycle__c,new Map<String, String>());
            }

            cycleDriveFolders.get(cycleDrives.KARL_Audit_Cycle__c).put(cycleDrives.KARL_Drive_Scope__c,cycleDrives.KARL_Drive_URL__c);
        }

        // Create map of prior GUS Work-item Ids
        Map<Id, Map<String, String>> priorGusRecords = new Map<Id, Map<String, String>>();

        // Need to get the most recent CREQ that had a GUS Work Id
        for(Request_Item__c priorCri : [SELECT Id, Name, (SELECT Id, CreatedDate, Name, Request_GUS__c, Request_GUS__r.Name__c, Request_GUS__r.DisplayUrl 
                                                FROM Cycle_Request_Items__r WHERE Request_GUS__c != NULL AND Audit_Cycle__c NOT IN: cycleIds 
                                                ORDER BY CreatedDate DESC LIMIT 1)
                                                FROM Request_Item__c
                                                WHERE Create_GUS_Case__c = true
                                                WITH SECURITY_ENFORCED]){
        // AND (Id NOT IN: cycleReqItemIds OR Id NOT IN: dependentCycleItemsIdSet)
            if( !priorGusRecords.containsKey(priorCri.Id) ){
                priorGusRecords.put(priorCri.Id,new Map<String, String>());
            }
            if(priorCri.Cycle_Request_Items__r != NULL && priorCri.Cycle_Request_Items__r.size() > 0){
                for(Cycle_Request_Item__c criItem : priorCri.Cycle_Request_Items__r){
                    priorGusRecords.get(priorCri.Id).put('creqName', priorCri.Name);
                    priorGusRecords.get(priorCri.Id).put('creqGusId', criItem.Request_GUS__c);
                    priorGusRecords.get(priorCri.Id).put('creqGusName', criItem.Request_GUS__r.Name__c);
                    priorGusRecords.get(priorCri.Id).put('creqGusUrl', criItem.Request_GUS__r.DisplayUrl);
                    priorGusRecords.get(priorCri.Id).put('creqCreatedDate', criItem.CreatedDate.format());
                }
            }
        }
        System.debug(LoggingLevel.DEBUG, 'Previous GUS Records >> ' + priorGusRecords);

        //Step 1: Create GUS Work Item without EPIC aligned
        //Create External Object Work Data if Create_GUS_Case__c is checked
        System.debug(LoggingLevel.DEBUG, 'query cycleReqItemIds= '+cycleReqItemIds);
        for( Cycle_Request_Item__c  cri : cycleRequestItemList){
                                                 if( cri.Request__r.Create_GUS_Case__c ){                
                
                                                    ADM_Work_c__x objWork = new ADM_Work_c__x();
                                                    //Feature ID to keep Salesforce Record ID so it can updated External in Salesforce Record.
                                                    objWork.Feature_ID_c__c = cri.id;
                                                    
                                                    //Subject is mapped by following fields
                                                    objWork.Subject_c__c    = cri.Audit_Cycle__r.Name+'/'+cri.Request__r.Name+'/'+cri.Request__r.Type__c+'/'+cri.Request__r.Request_Name__c;

                                                    // Build the case details with boiler plate text, using HTML for RTF
                                                    String gusCaseBuilder = KARL_GusUtility.gusTicketFormat(cri, cycleReqIdToEvidenceIdMap, cycleDriveFolders, communityHomeUrl, priorGusRecords);
                                                    
                                                    objWork.Customer_c__c       = cri.Request__r.KARL_GRC_Assignee__r.FirstName + ' ' + cri.Request__r.KARL_GRC_Assignee__r.LastName + '-' + cri.Audit_Cycle__r.Name;
                                                    objWork.Details_c__c        = gusCaseBuilder;
                                                    objWork.Status_c__c         = 'New';
                                                    objWork.Team__c             = System.label.ARL_GUS_Work_Scrum_Team_Id;
                                                    objWork.RecordTypeId__c     = System.label.GUS_Work_User_Story_RecordType_Id;
                                                    if(cri.Date_for_Work_Record_Creation__c != null)
                                                    objWork.Due_Date_c__c = KARL_Utility.calculateWorkingDate(10, cri.Date_for_Work_Record_Creation__c);

                                                    // Logic for Product Tag assignment
                                                    if(cri.Request__r.Suggested_Product_Tag__c != null 
                                                        && cri.Request__r.Suggested_Product_Tag__r.Active__c 
                                                        && cri.Request__r.Suggested_Product_Tag__r.Product_Tag_External_Id__c  != null 
                                                        && cri.Request__r.Suggested_Product_Tag__r.Team__r.Active_c__c
                                                        && cri.Request__r.Automatic_Product_Tag_Assignment__c){
                                                        // Assign to the specified Product Tag
                                                        objWork.Product_Tag_c__c = cri.Request__r.Suggested_Product_Tag__r.Product_Tag_External_Id__c;
                                                    }
                                                    else{
                                                        // Assign to the default Product Tag
                                                        objWork.Product_Tag_c__c    = cri.Audit_Cycle_Coverage__r.Auto_Assignment_Configuration__r.Default_Product_Tag__r.Product_Tag_External_Id__c;
                                                    }

                                                    //Updating correponding evidence record with the GUS details so that
                                                    //this field can be refrenced into evidence upload community
                                                    //Note: This piece of code can be removed after we have proper sharing in place for evidence upload community
                                                   
                                                    if(cycleReqIdToEvidenceIdMap.get(cri.Id) != null){
                                                        KARL_Evidence_Request__c evReq = new KARL_Evidence_Request__c();
                                                        evReq.Id = cycleReqIdToEvidenceIdMap.get(cri.Id);
                                                        evReq.KARL_GUS_Ticket_Details__c = gusCaseBuilder;

                                                        evidenceReqUpdateList.add(evReq);
                                                    }

                                                    // Create SLA tracking record
                                                    KARL_GUS_SLA_Tracking__c gusTrackingObj = new KARL_GUS_SLA_Tracking__c();
                                                    gusTrackingObj.Assignee__c =  cri.Request__r.KARL_GRC_Assignee__r.Name;
                                                    gusTrackingObj.Changed_By__c = cri.CreatedById;
                                                    gusTrackingObj.Cycle_ARL_Item__c = cri.Id;
                                                    gusTrackingObj.Date_Changed__c = Datetime.now().date();
                                                    gusTrackingObj.KARL_End_Date__c = null;
                                                    gusTrackingObj.End_Status__c = '';
                                                    gusTrackingObj.Start_Date__c = Datetime.now();
                                                    gusTrackingObj.Start_Status__c = 'New';
                                                    
                                                    // Add records to Lists for processing to database
                                                    gusSlaTrackingList.add(gusTrackingObj);
                                                    WorkList.add( objWork );
                                                    cRewItemIds.put(cri.Id, cri);
                                                    epics.add( cri.epic__c );

                                                    System.debug(LoggingLevel.DEBUG, 'WorkList = '+WorkList); 
                                                }
                                            
        }

        //STEP 2: Search for existing EPIC from GUS
        Map<String, ADM_Epic_c__x> epicNameAndEpic = KARL_GusUtility.selectEpicsByName(epics);
                
        //If work record is found then insert in External Object
        if( !WorkList.isEmpty() ){
            //STEP 3: Insert GUS work reocrds
            List<Database.SaveResult> result = new List<Database.SaveResult>();
            if(!Test.isRunningTest()){
                System.debug(LoggingLevel.DEBUG, 'Insert gus work record' );
                result = Database.insertImmediate(WorkList);
                System.debug(LoggingLevel.DEBUG, 'Insert gus work record = '+result);
            }
                        
            //Inserted GUS work record ids
            Set<Id> gusWorkRecordIds = new Set<Id>();
            for (Database.SaveResult gusWork : result) {
                if (gusWork.isSuccess()) {
                    System.debug(LoggingLevel.DEBUG, 'Success gus work record = '+gusWork.getId());
                    gusWorkRecordIds.add(gusWork.getId());
                }
                else{
                    System.debug(LoggingLevel.DEBUG, 'Failure gus work record = ');
                    for(Database.Error error :  gusWork.getErrors()){
                        System.debug(LoggingLevel.ERROR, error.getMessage());
                    }
                }
            }
                        
            //1. Update Cycle Request Item Object with GUS Work Record's External ID
            //2. Reset Feature_ID_c__c field to blank.
            List<Cycle_Request_Item__c> updtCycleReqItems = new List<Cycle_Request_Item__c>();
            List<ADM_Work_c__x> newWorkRecords = [SELECT Id,ExternalId,Feature_ID_c__c,Status_c__c,DisplayUrl,Assignee_c__c 
                                                    FROM ADM_Work_c__x WHERE Id IN :gusWorkRecordIds AND Feature_ID_c__c!=null];
            
            if(Test.isRunningTest()){
                ADM_Work_c__x admWork = new ADM_Work_c__x();
                admWork.ExternalId = 'test';
                admWork.Status_c__c = 'In-Progress';
                admWork.Team__c = 'test';
                admWork.Feature_ID_c__c = cRewItemIds.values()[0].Id;
                newWorkRecords.add(admWork);
            }
                
            Set<ADM_Epic_c__x> newEpics = new Set<ADM_Epic_c__x>();

            //STEP 4: Assign GUS Work ID to Cycle Request Item and Prepare a list of new EPIC if EPIC was not found in STEP 2
            for(ADM_Work_c__x gusWork : newWorkRecords){
                if( cRewItemIds.containsKey(gusWork.Feature_ID_c__c) ){
                    
                    Cycle_Request_Item__c cri = new Cycle_Request_Item__c(Id=gusWork.Feature_ID_c__c);
                    cri.Request_GUS__c  = gusWork.ExternalId;
                    cri.Assigned_To__c = gusWork.Assignee_c__c;
                    cri.GUS_URL_Direct_Link__c = gusWork.DisplayUrl;
                    cri.GUS_Status__c = gusWork.Status_c__c;
                    cri.Is_Gus_Record_Created__c = true;
                    cri.Date_for_Work_Record_Creation__c = null;
                    updtCycleReqItems.add(cri);
                    System.debug(LoggingLevel.DEBUG, 'updtCycleReqItems = '+updtCycleReqItems);
                    
                    //Check id EPIC already exists in the system or not. If not then create new
                    if( !( epicNameAndEpic.containsKey(cRewItemIds.get(gusWork.Feature_ID_c__c).epic__c) ) ){
                         ADM_Epic_c__x admApic = new ADM_Epic_c__x(); 
                         admApic.Name__c = cRewItemIds.get(gusWork.Feature_ID_c__c).epic__c;
                         admApic.Team_c__c = System.label.ARL_GUS_Work_Scrum_Team_Id;
                         newEpics.add(admApic);
                    }
                }
            }
                        
            //STEP 5: Insert new EPICs and requery them
            if( !newEpics.isEmpty() && !Test.isRunningTest() ){
                Database.insertImmediate( new List<ADM_Epic_c__x>(newEpics) );
                epicNameAndEpic = KARL_GusUtility.selectEpicsByName(epics);
            } 
                        
            //STEP 6: Set EPIC Ids in GUS Work
            for(ADM_Work_c__x gusWork : newWorkRecords){
                if( epicNameAndEpic.containsKey(cRewItemIds.get(gusWork.Feature_ID_c__c).epic__c) ){
                    gusWork.Epic_c__c = epicNameAndEpic.get(cRewItemIds.get(gusWork.Feature_ID_c__c).epic__c).ExternalId;
                }
                
                //Blaking out the Feature_ID_c__c from external GUS record
                gusWork.Feature_ID_c__c = '';
            }
            
            //STEP 7: Update GUS Work records
            if( !newWorkRecords.isEmpty() && !Test.isRunningTest() ){Database.updateImmediate(newWorkRecords);}
            
            //STEP 8: Update Cycle Request Item
            System.debug(LoggingLevel.DEBUG, 'updtCycleReqItems= '+updtCycleReqItems);
            if( !updtCycleReqItems.isEmpty() && Schema.sObjectType.Cycle_Request_Item__c.isUpdateable() 
               && Cycle_Request_Item__c.Request_GUS__c.getDescribe().isUpdateable()
               && Cycle_Request_Item__c.Assigned_To__c.getDescribe().isUpdateable()
               && Cycle_Request_Item__c.GUS_URL_Direct_Link__c.getDescribe().isUpdateable()
               && Cycle_Request_Item__c.GUS_Status__c.getDescribe().isUpdateable()){
                update updtCycleReqItems;
            }
        }

        if(!gusSlaTrackingList.isEmpty()){
            insert gusSlaTrackingList;
            System.debug(LoggingLevel.DEBUG,'gusSlaTrackingList = '+gusSlaTrackingList);
        }

        if( !evidenceReqUpdateList.isEmpty() && Schema.sObjectType.KARL_Evidence_Request__c.isUpdateable() 
               && KARL_Evidence_Request__c.KARL_GUS_Ticket_Details__c.getDescribe().isUpdateable()){
            update evidenceReqUpdateList;
        }

        System.debug(LoggingLevel.DEBUG, '----------------processGUSRecordCreation   End--------------------');
    }


    /**
    * @author Lakshya Arora
    * @email lakshya.arora@mtxb2b.com
    * @desc This method will call after all the operations get completed
    */
    public void finish(Database.BatchableContext BC){}

    /**
    * @author Lakshya Arora
    * @email lakshya.arora@mtxb2b.com
    * @desc This method will schedule the batch daily at 7am
    */
    public static void start(){
        System.schedule('Daily Gus Record Creation', '0 0 7 * * ? *', new KARL_GusRecordCreationBatch());
    }
}
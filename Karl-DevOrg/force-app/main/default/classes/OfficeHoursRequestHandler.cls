/*
 * Email services are automated processes that use Apex classes
 * to process the contents, headers, and attachments of inbound
 * email.
 */
global class OfficeHoursRequestHandler implements Messaging.InboundEmailHandler{
    private Partner_Appointment__c p;
    private static final String ORG_WIDE_EMAIL_ADDRESS_ID = '0D2300000004DuF';


    /**
     * Parses through an InboundEmail object's attachments and returns the status of
     * whether or not the person sending it accepted, declined, or unknowned the event.
     * -1 = unknown
     * 0 = Declined
     * 1 = Accepted
     * 2 = Tentative
     *
     * TODO: Get rid of these magic numbers
     */
    public integer emailAcceptStatus(Messaging.InboundEmail email){
    
        string icsRegex = '(?i)partstat=(accepted|declined|tentative);?';
        for (Messaging.InboundEmail.TextAttachment b : email.textAttachments){
            if (b.mimeTypeSubType != 'text/calendar') continue;
            Matcher m = Pattern.compile(icsRegex).matcher(b.body);
            
            while (m.find()){
                if (m.group(1).toLowerCase().equals('accepted')) {
                    return 1;
                } else if (m.group(1).toLowerCase().equals('tentative')){
                    return 2;
                } else if (m.group(1).toLowerCase().equals('declined')){
                    return 0;
                }
                //only parse one match from the calendar invite
                break;
            }
            
            //only parse one calendar invite at the max
            break;
        }
        
        return -1;

    }
    public void debugMyInboundMessage(Messaging.InboundEmail email){
         string debugstr = 'Email from ' + email.fromAddress + '\r\n';
         debugstr += 'Our text attachment size is ';
            if (email.textAttachments == null){
                 debugstr += '0';
            } else { 
                debugstr += ''+email.textAttachments.size();
                debugstr += '\r\n';
                for (Messaging.InboundEmail.TextAttachment b : email.textAttachments){
                    debugstr += 'fileName: ' + b.fileName + '\r\n';
                    debugstr += 'mimeTypeSubType: ' + b.mimeTypeSubType + '\r\n';
                }
            }
            
        debugstr += '\r\nInvite Attendance: ' + emailAcceptStatus(email) + '\r\n';
            
            debugstr += '\r\nMatching for office hours id: \r\n';
            
           
            
            Messaging.SingleEmailMessage reply = new Messaging.SingleEmailMessage();
            reply.setOrgWideEmailAddressId(ORG_WIDE_EMAIL_ADDRESS_ID);
            reply.setToAddresses(new String[] {'appxsecurityreview@salesforce.com'});
            reply.setSubject('Debug ');
            reply.setPlainTextBody(debugstr);
            Messaging.sendEmail(new Messaging.Email[] {reply});


    }

    public string parseOfficeHourRefId(string sub){
        string idgrep = '\\[(OH-\\d+?)\\]';
        Matcher m = Pattern.compile(idgrep).matcher(sub);
        while (m.find()){
            return m.group(1);
            break;
        }
        return null;   
    }

    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
        // use the 'ref' number in the subject line to find the right appointment
        
        
        
        String subject = email.subject;
        string refIdMatch = parseOfficeHourRefId(email.subject);
        if (refIdMatch == null){
            //try oldschool way instead
            String[] tokens = subject.split('Ref:');

            p = [SELECT  Id, Name, Email__c, Name__c, Company__c, Description__c, ContactInfo__c, Date__c, RefId__c, ProdSec_Member__c, 
                Appointment_Owner__c, Appointment_Owner__r.Name, Appointment_Owner__r.Email FROM Partner_Appointment__c  WHERE Id =: tokens[tokens.size() - 1].trim() ];
            // check to see if someone already has the appointment
        } else {
            p = [SELECT  Id, Name, Email__c, Name__c, Company__c, Description__c, ContactInfo__c, Date__c, RefId__c, ProdSec_Member__c, 
                Appointment_Owner__c, Appointment_Owner__r.Name, Appointment_Owner__r.Email FROM Partner_Appointment__c WHERE RefId__c =:refIdMatch LIMIT 1];
        }
        
        integer acceptStatus = emailAcceptStatus(email);

        if (p.Appointment_Owner__c != null){
            if (acceptStatus == 1) { //user accepted it
                //get office hour accepted status
                Messaging.SingleEmailMessage reply = new Messaging.SingleEmailMessage();
                reply.setOrgWideEmailAddressId(ORG_WIDE_EMAIL_ADDRESS_ID);
                reply.setToAddresses(new String[] {email.fromAddress});
                reply.setSubject('Appointment already filled: ' + subject);
                reply.setPlainTextBody('You\'re off the hook; ' + p.Appointment_Owner__r.Name + ' has already accepted the meeting!\r\nI will automatically send you a cancellation request to keep your calendar clean!');
                Messaging.sendEmail(new Messaging.Email[] {reply});
                //todo find out why this is messaging everyone on the invite instead of just the single email address
                //(new OfficeHoursController()).sendCancellation(p, email.fromAddress);
                return result;
            } else if (acceptStatus == 0) { //user declined
                if (p.Appointment_Owner__r.Email == email.fromAddress){ //make sure we are the ones actually sending it
                    //send out the invitation to the team again since the assignee cancelled
                    (new OfficeHoursController()).sendInvitation(p, email.fromAddress);
                    p.Appointment_Owner__c = null;
                    update p;
                    return result;
                }
            }
        }
        
        // Cue on acceptance subject to change appointment record in security org.  Not the best way of doing things, since different clients
        // might use a different subject prefix.  Also, possibly localization issues.  Should really use headers in the calendar response.
        if (acceptStatus == 1){ //accepted
            // update appointment record with ProdSec member taking meeting
            p.ProdSec_Member__c = email.fromName;
            List<User> users = [SELECT Id, Name, Email FROM User WHERE Email = :email.fromAddress LIMIT 1];
            if (users.isEmpty()){
                //Couldn't find user by email in prodsec org 
                //potentially more logic here
            } else {
                p.Appointment_Owner__c = users[0].Id;
            }
                        
            update(p);
        }
       
        return result;
    }
}
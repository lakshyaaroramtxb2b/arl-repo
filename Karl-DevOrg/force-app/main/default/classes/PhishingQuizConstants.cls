public class PhishingQuizConstants {
    public PageReference register;
    public PageReference quizDone;
    public PageReference quizQuestion;
    public final Integer quizValidDays = 7;
    public final Boolean oneValidUserPerEmail = True;
    public String protoPrefix = 'https://';
    
    public PhishingQuizConstants() {
        String protoPrefix = 'https://'; // Damn it! How do you figure out if the req is secure?
        String mysite = Site.getCurrentSiteUrl();
        if ((mysite != null) && (mysite != '')) {
            register = new PageReference(mysite+'PhishQuizRegister');
            quizDone = new PageReference(mysite+'PhishQuizScorecard');
            quizQuestion = new PageReference(mysite+'PhishQuizQuestion');
        }
        else {
            register = new PageReference(protoPrefix+ApexPages.currentPage().getHeaders().get('Host')+'/apex/PhishQuizRegister');
            quizDone = new PageReference(protoPrefix+ApexPages.currentPage().getHeaders().get('Host')+'/apex/PhishQuizScorecard');
            quizQuestion = new PageReference(protoPrefix+ApexPages.currentPage().getHeaders().get('Host')+'/apex/PhishQuizQuestion');
        }
    }
    public String emailBody(PhishingQuizUser__c user,Boolean useHtml) {
        

        String s = 'Hello '+user.Name+',';
        if (useHtml = True) { s+= '<br /><br />'; } else { s+= '\n\n';}
        s += 'Thank you for registering for the salesforce.com Phishing and Fraud Detection ';
        s += 'Quiz.  Please use the below link to access the quiz.  Note that the ';
        s += 'quiz and results will only be available for the next ';
        s += String.valueOf(quizValidDays)+' days.';
        if (useHtml = True) { s+= '<br /><br />'; } else { s+= '\n\n';}
        if (useHtml = True) {
            s+= '<a href="'+quizQuestion.getUrl();
            s+= '?sId='+user.Key__c+'">';
            s+= quizQuestion.getUrl();
            s+= '?sId='+user.Key__c+'</a><br /><br />';
        }
        else {
            s+= quizQuestion.getUrl();
            s+= '?sId='+user.Key__c+'\n\n';
        }
        s+= 'Sincerely,';
        if (useHtml = True) { s+= '<br />'; } else { s+= '\n';}
        s+= 'Salesforce.com Product Security Team';
        return s;
    }
}
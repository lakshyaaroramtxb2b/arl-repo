/**
 * @author Mahima Aggarwal
 * @email mahima.aggarwal@mtxb2b.com
 * @description Test Data Factory for ARL Project
*/
@isTest
public class ARL_TestDataFactory {

    public final static String STANDARD_ALIAS = 'n@g.com';
    public final static String STANDARD_EMAIL = 'new@gmail.com';
    public final static String FIRSTNAME = 'Community';
    public final static String LASTNAME = 'User';
    public final static String EMAIL_ENCODING_KEY = 'UTF-8';
    public final static String TIMEZONE_SID_KEY = 'America/Phoenix';
    public final static String LOCALE_SID_KEY = 'en_US';
    public final static String LANGUAGE_LOCALE_KEY = 'en_US'; 
    public final static String COMMUNITY_PROFILE_NAME = 'ESA CCP';
    public final static String COMMUNITY_ACCOUNT = 'salesforce.com - ESA Office Hours';
    public final static String CONTACT_CUSTOMER_RECORDTYPEID = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Salesforce_Customer_Contact').getRecordTypeId();
    
    /**
     * @author Mahima Aggarwal
     * @email mahima.aggarwal@mtxb2b.com
     * @description This method creates Request Item record
     * @return new REQ item
    */
    public static Request_Item__c createRequestItem(){
        Request_Item__c reqItem = new Request_Item__c();
        reqItem.Create_GUS_Case__c = true;
        reqItem.Active__c = true;
        reqItem.Request_Description__c = 'Test';
        reqItem.Request_Tech_Details__c = 'test';
        reqItem.KARL_Collection_Method__c = 'Control Owner/Performer';
        reqItem.Type__c = 'Samples';
        reqItem.Priority__c = 'Wave 1'; 
        reqItem.Request_Name__c = 'Test Req';
        reqItem.Primary_Scope__c = 'CS';
        reqItem.Update_Current_Audit_Cycle_s__c = 'Yes';
        return reqItem;
    }
    
    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description This method creates an Audit Scope record
     * @return new Audit Scope
     */
    public static KARL_Audit_Scope_Master_Data__c createAuditScope(){
        KARL_Audit_Scope_Master_Data__c auditScope = new KARL_Audit_Scope_Master_Data__c();
        auditScope.Name = 'Test Audit Scope';
        auditScope.KARL_GRC_Team__c = 'Orchestration 1';
        return auditScope;
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description This method creates an Audit Scope Report record in an Audit Scope (incl Audit Firm)
     * @param auditScopeId - the Audit Scope to create a report for
     * @param auditFirmId - the Audit Firm preparing the Audit Report
     * @return an Audit Scope Report record
     */
    public static KARL_Audit_Scope_Reports__c createAuditScopeReport(String auditScopeId, String auditFirmId){
        KARL_Audit_Scope_Reports__c auditScopeReport = new KARL_Audit_Scope_Reports__c();
        auditScopeReport.KARL_Audit_Scope__c = auditScopeId;
        auditScopeReport.KARL_Report_Type__c = 'SOC 1';
        auditScopeReport.KARL_Audit_Firm__c = auditFirmId;
        auditScopeReport.KARL_Audit_Frequency__c = 'Annual (12 Months)';
        return auditScopeReport;
    }

    /**
     * @author Mahima Aggarwal
     * @email mahima.aggarwal@mtxb2b.com
     * @description This method creates Audit Cycle record
     * @return a new Audit Cycle
    */
    public static Audit_Cycle__c createAuditCycle(){
        Audit_Cycle__c auditCycle = new Audit_Cycle__c();
        auditCycle.Name = 'testAuditCycle';
        auditCycle.Google_Drive_Link__c = 'https://drive.google.com/test';
        auditCycle.Active__c = true;
        return auditCycle;
    }
    
    /**
     * @author Mahima Aggarwal
     * @email mahima.aggarwal@mtxb2b.com
     * @description This method creates Audit Cycle Coverage record
     * @param auditCycleId - the current Audit Cycle to configure
     * @return a configuration for an Audit Cycle Coverage
    */
    public static Audit_Cycle_Coverage__c createAuditCycleCoverage(String auditCycleId){
        Audit_Cycle_Coverage__c acc = new Audit_Cycle_Coverage__c();
        acc.Audit_Cycle__c = auditCycleId;
        return acc;
    }
    
    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description This method creates Control record
     * @return a new Control
    */
    public static Control__c createControl(){
        Control__c ctrl = new Control__c();
        ctrl.Name  = 'Test_Control';
        ctrl.Family_Name__c = 'AC';
        ctrl.Control_Identifier__c = '90';
        ctrl.Control_Description__c = 'Test';
        return ctrl;
    }
    
    /**
     * @author Mahima Aggarwal
     * @email mahima.aggarwal@mtxb2b.com
     * @description This method creates Control Scope record
     * @return an unmapped Control Scope
    */
    public static Control_Scope__c createControlScope(){
        Control_Scope__c cs = new Control_Scope__c();
        cs.Scope_Name__c  = 'HK';
        return cs;
    }
    
    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description This method creates Control Scope record mapped to a Control
     * @param controlId - the Control to map the Control Scope to
     * @return a mapped Control Scope
    */
    public static Control_Scope__c createMappedControlScope(String controlId){
        Control_Scope__c cs = new Control_Scope__c();
        cs.Control_Name__c = controlId;
        cs.Scope_Name__c  = 'HK';
        return cs;
    }
    
    /**
     * @author Mahima Aggarwal
     * @email mahima.aggarwal@mtxb2b.com
     * @description This method creates Control Test record
     * @param controlScopeId - the control scope to create a test for
     * @return new control test
    */
    public static Control_Test__c createControlTest(String controlScopeId){
        Control_Test__c ct = new Control_Test__c();
        ct.Test_Name__c = 'test1';
        ct.Control_Scope__c = controlScopeId;
        return ct;
    }
    
    /**
     * @author Mahima Aggarwal
     * @email mahima.aggarwal@mtxb2b.com
     * @description This method creates Request Item Control record
     * @param requestItemId - the REQ to create a mapping for
     * @param controlTestId - the Control Test to map to the REQ
     * @return a mapped Request to Control Test
    */
    public static Request_Item_Control__c createRequestItemControl(String requestItemId,String controlTestId){
        Request_Item_Control__c ric = new Request_Item_Control__c();
        ric.Request__c = requestItemId;
        ric.Test__c = controlTestId;
        return ric;
    }
    
    /**
     * @author Mahima Aggarwal
     * @email mahima.aggarwal@mtxb2b.com
     * @description This method creates Cycle Request Item record
     * @param requestItemId - the REQ to create a CREQ for
     * @param auditCycleId - the Audit Cycle to specify for the CREQ
     * @return a new CREQ
    */
    public static Cycle_Request_Item__c createCycleRequestItem(String requestItemId,String auditCycleId){
        Cycle_Request_Item__c cri = new Cycle_Request_Item__c();
        cri.Audit_Cycle__c = auditCycleId;
        cri.Request__c = requestItemId;
        return cri;
    }
    
    /**
     * @author Mahima Aggarwal
     * @email mahima.aggarwal@mtxb2b.com
     * @description This method creates Audit Cycle Coverage Item record
     * @param auditCycleCoverageId - which cycle scope coverage is relevant
     * @param cycleReqItemId - the CREQ to map
     * @return a cycle coverage item for duplication reduction
    */
    public static Audit_Cycle_Coverage_Item__c createAuditCycleCoverageItem(String auditCycleCoverageId,String cycleReqItemId){
        Audit_Cycle_Coverage_Item__c acci1 = new Audit_Cycle_Coverage_Item__c();
        acci1.Audit_Cycle_Coverage__c = auditCycleCoverageId;
        acci1.Cycle_Request_Item__c = cycleReqItemId;
        return acci1;
    }

    /**
     * @author Mahima Aggarwal
     * @email mahima.aggarwal@mtxb2b.com
     * @description This method creates Audit Firm
     * @param name - name of the audit firm to create
     * @return audit firm record
    */
    public static KARL_Audit_Firm__c createAuditFirm(String name){
        KARL_Audit_Firm__c auditFirm = new KARL_Audit_Firm__c();
     	auditFirm.Name = name;
        return auditFirm;
    }
    
    /**
     * @author Mahima Aggarwal
     * @email mahima.aggarwal@mtxb2b.com
     * @description This method creates Audit Team
     * @param name - name of the audit team to create
     * @return an audit team (without a firm)
    */
    public static KARL_Audit_Team__c createAuditTeam(String name){
        KARL_Audit_Team__c auditTeam = new KARL_Audit_Team__c();
        auditTeam.Name = name;
        auditTeam.KARL_Active__c = true;
        return auditTeam;
    }
    
    /**
     * @author Mahima Aggarwal
     * @email mahima.aggarwal@mtxb2b.com
     * @description This method creates Audit Team Contact
     * @param auditTeamId - audit team to create a contact for
     * @param contactId - contact lookup to map
     * @return a new audit team contact
    */
    public static KARL_Audit_Team_Contacts__c createAuditTeamContact(String auditTeamId,String contactId){
        KARL_Audit_Team_Contacts__c auditTeamCon = new KARL_Audit_Team_Contacts__c();
     	auditTeamCon.KARL_Audit_Team__c = auditTeamId;
        auditTeamCon.KARL_Contact__c = contactId;
        return auditTeamCon;   
    }
    
    /**
     * @author Mahima Aggarwal
     * @email mahima.aggarwal@mtxb2b.com
     * @description This method creates Auditor Request Item
     * @param cycleRequestItemId - the CREQ ID to be the parent for the ARI
     * @param auditTeamId - which audit team should be mapped
     * @return new ARI record
    */
    public static KARL_Auditor_Request_Item__c createAuditorRequestItem(String cycleRequestItemId, String auditTeamId){
        KARL_Auditor_Request_Item__c auditorReqItem = new KARL_Auditor_Request_Item__c();
     	auditorReqItem.KARL_Cycle_Request_Item__c = cycleRequestItemId;
        auditorReqItem.KARL_Audit_Team__c = auditTeamId;
        return auditorReqItem;
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description This method creates a Follow-Up Record
     * @param auditTeamId - the audit team to create a follow-up for
     * @param auditCycleId - the audit cycle for the follow-up
     * @return a new followup
    */
    public static KARL_Cycle_Follow_up__c createFollowUp(String auditTeamId, String auditCycleId){
        KARL_Cycle_Follow_up__c followUp = new KARL_Cycle_Follow_up__c();
        followUp.Name  = 'Test Follow-up';
        followUp.KARL_Follow_up_Type__c = 'Informational';
        followUp.KARL_Audit_Scope__c = 'SS';
        followUp.KARL_Followup_Description__c = 'Test';
        followUp.KARL_Current_Assignment__c = 'Salesforce';
        // Dynamic Information
        followUp.KARL_Followup_Audit_Cycle__c = auditCycleId;
        followUp.KARL_Followup_Audit_Team__c = auditTeamId;
        return followUp;
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description This method creates a Audit Issue Tracker Record
     * @param auditCycleId - the audit cycle for the audit issue
     * @return a new audit issue
    */
    public static KARL_Issue_Tracker__c createAuditIssue(String auditCycleId){
        KARL_Issue_Tracker__c issue = new KARL_Issue_Tracker__c();
        issue.KARL_KAI_Issue_Title__c  = 'Test Follow-up';
        issue.KARL_KAI_Issue_Type__c = 'Confirmed Exception';
        issue.KARL_KAI_Issue_Source__c = 'External Auditor';
        issue.KARL_KAI_Impacted_Scope__c = 'SS';
        issue.Issue_Status__c = 'New';
        issue.KARL_KAI_Theme__c = 'AC';
        issue.KARL_KAI_Issue_Description__c = 'Test Audit Issue';
        // Dynamic Information
        issue.KARL_Audit_Cycle__c = auditCycleId;
        return issue;
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description This method creates a Audit Issue Tracker Approver
     * @param approverId - the approvers user Id
     * @return a new audit issue approver record
    */
    public static KARL_Issue_Tracker_Approvers__c createAuditIssueApprover(String approverId){
        KARL_Issue_Tracker_Approvers__c issApp = new KARL_Issue_Tracker_Approvers__c();
        issApp.KARL_Scope__c  = 'SS';
        issApp.KARL_Approver1__c = approverId;
        issApp.KARL_Approver2__c = approverId;
        issApp.KARL_Approver3__c = approverId;

        return issApp;
    }
    
    
    /**
     * @author Mahima Aggarwal
     * @email mahima.aggarwal@mtxb2b.com
     * @description This method creates an External Auditor user
     * @return a test user (external auditor)
    */
    public static User createExternalAuditor(){
        List < Profile > standardProfileList = [SELECT Id FROM Profile WHERE Name = 'Standard User'];
        User testUser;
        if (!standardProfileList.isEmpty()) {
            Profile standardProfile = standardProfileList[0];
            testUser = new User(Alias = 'alias', Email = 'a@b.com', EmailEncodingKey = 'utf-8',
                                FirstName = 'External_Auditor', LastName = 'lname', LanguageLocaleKey = 'en_US',
                                LocaleSidKey = 'en_US', ProfileId = standardProfile.Id, TimeZoneSidKey = 'America/Los_Angeles',
                                UserName = 'a@b.com.d');
        }
        insert testUser;
        
        //Permission Set Assignment
        PermissionSet pSet = [SELECT Name FROM PermissionSet WHERE NAME = 'External_Auditor'];
        insert new PermissionSetAssignment(AssigneeId = testUser.id, PermissionSetId = pSet.Id);
        
        //Permission Set Assignment (GRC Central Settings)
        // Added 01/04/2020
        PermissionSet pSetGRC = [SELECT Name FROM PermissionSet WHERE NAME = 'GRC_Central_Settings'];
        insert new PermissionSetAssignment(AssigneeId = testUser.id, PermissionSetId = pSetGRC.Id);
        
        return testUser;
    }
    
    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description This method applies External Auditor Perms to a Community User
     * @param communityUserId - the ID of the Community User to apply permissions to
    */
    public static void applyExternalAuditorPerms(String communityUserId){
        //Permission Set Assignment
        PermissionSet pSet = [SELECT Name FROM PermissionSet WHERE NAME = 'External_Auditor'];
        insert new PermissionSetAssignment(AssigneeId = communityUserId, PermissionSetId = pSet.Id);
        
        //Permission Set Assignment (GRC Central Settings)
        PermissionSet pSetGRC = [SELECT Name FROM PermissionSet WHERE NAME = 'GRC_Central_Settings'];
        insert new PermissionSetAssignment(AssigneeId = communityUserId, PermissionSetId = pSetGRC.Id);
    }

    /**
     * @author Mahima Aggarwal
     * @email mahima.aggarwal@mtxb2b.com
     * @description This method creates ops admin user
     * @return a test user (SCEA Ops Admin)
    */
    public static User createOpsAdmin(){
        List < Profile > standardProfileList = [SELECT Id FROM Profile WHERE Name = 'Nothing'];
        User testUser;
        if (!standardProfileList.isEmpty()) {
            Profile standardProfile = standardProfileList[0];
            testUser = new User(Alias = 'alias', Email = 'abc@b.com', EmailEncodingKey = 'utf-8',
                                FirstName = 'Ops_Admin', LastName = 'lname', LanguageLocaleKey = 'en_US',
                                LocaleSidKey = 'en_US', ProfileId = standardProfile.Id, TimeZoneSidKey = 'America/Los_Angeles',
                                UserName = 'abc@b.com.d');
        }
        insert testUser;
        
        //Permission Set Assignment (SCEA Ops Admin)
        PermissionSet pSet = [SELECT Name FROM PermissionSet WHERE NAME = 'SCEA_Ops_Admin'];
        insert new PermissionSetAssignment(AssigneeId = testUser.id, PermissionSetId = pSet.Id);
        
        //Permission Set Assignment (GRC Central Settings)
        // Added 01/04/2020
        PermissionSet pSetGRC = [SELECT Name FROM PermissionSet WHERE NAME = 'GRC_Central_Settings'];
        insert new PermissionSetAssignment(AssigneeId = testUser.id, PermissionSetId = pSetGRC.Id);
        
        //Permission Set Assignment (SCEA Sharing)
        PermissionSet pSetSharing = [SELECT Name FROM PermissionSet WHERE NAME = 'SCEA_Sharing'];
        insert new PermissionSetAssignment(AssigneeId = testUser.id, PermissionSetId = pSetSharing.Id);
        
        return testUser;
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description This method creates a named ops admin user
     * @return a test user (SCEA Ops Admin)
    */
    public static User createNamedOpsAdmin(String fname, String lname, String email){
        List < Profile > standardProfileList = [SELECT Id FROM Profile WHERE Name = 'Nothing'];
        User testUser;
        if (!standardProfileList.isEmpty()) {
            Profile standardProfile = standardProfileList[0];
            testUser = new User(Alias = 'alias', Email = email, EmailEncodingKey = 'utf-8',
                                FirstName = fname, LastName = lname, LanguageLocaleKey = 'en_US',
                                LocaleSidKey = 'en_US', ProfileId = standardProfile.Id, TimeZoneSidKey = 'America/Los_Angeles',
                                UserName = email + '.test');
        }
        insert testUser;
        
        //Permission Set Assignment (SCEA Ops Admin)
        PermissionSet pSet = [SELECT Name FROM PermissionSet WHERE NAME = 'SCEA_Ops_Admin'];
        insert new PermissionSetAssignment(AssigneeId = testUser.id, PermissionSetId = pSet.Id);
        
        //Permission Set Assignment (GRC Central Settings)
        // Added 01/04/2020
        PermissionSet pSetGRC = [SELECT Name FROM PermissionSet WHERE NAME = 'GRC_Central_Settings'];
        insert new PermissionSetAssignment(AssigneeId = testUser.id, PermissionSetId = pSetGRC.Id);
        
        //Permission Set Assignment (SCEA Sharing)
        PermissionSet pSetSharing = [SELECT Name FROM PermissionSet WHERE NAME = 'SCEA_Sharing'];
        insert new PermissionSetAssignment(AssigneeId = testUser.id, PermissionSetId = pSetSharing.Id);
        
        return testUser;
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description This method establishes a SysAdmin with a role (for portal purposes)
     * @return a test user (System Administrator)
    */
    public static User createSystemAdmin(){
        UserRole userRole   = [SELECT Id, DeveloperName FROM UserRole WHERE DeveloperName = 'CEO' LIMIT 1];
        Profile userProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];

        User adminUser = new User(Alias = 'alias', Email = 'orgadmin@example.com', EmailEncodingKey = 'utf-8',
                                FirstName = 'Org_Admin', LastName = 'lname', LanguageLocaleKey = 'en_US',
                                LocaleSidKey = 'en_US', ProfileId = userProfile.Id, UserRoleId = userRole.Id, 
                                TimeZoneSidKey = 'America/Los_Angeles', UserName = 'orgadmin@example.com.test');

        return adminUser;
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description This method creates a SCEA Audit Manager
     * @return a test user (SCEA Audit Manager)
    */
    public static User createAuditManager(){
        List < Profile > standardProfileList = [SELECT Id FROM Profile WHERE Name = 'Nothing'];
        User testUser;
        if (!standardProfileList.isEmpty()) {
            Profile standardProfile = standardProfileList[0];
            testUser = new User(Alias = 'alias', Email = 'abc@b.com', EmailEncodingKey = 'utf-8',
                                FirstName = 'SCEA_Manager', LastName = 'lname', LanguageLocaleKey = 'en_US',
                                LocaleSidKey = 'en_US', ProfileId = standardProfile.Id, TimeZoneSidKey = 'America/Los_Angeles',
                                UserName = 'abc@b.com.e');
        }
        insert testUser;
        
        //Permission Set Assignment (SCEA Ops Admin)
        PermissionSet pSet = [SELECT Name FROM PermissionSet WHERE NAME = 'SCEA_Audit_Manager'];
        insert new PermissionSetAssignment(AssigneeId = testUser.id, PermissionSetId = pSet.Id);
        
        //Permission Set Assignment (GRC Central Settings)
        // Added 01/04/2020
        PermissionSet pSetGRC = [SELECT Name FROM PermissionSet WHERE NAME = 'GRC_Central_Settings'];
        insert new PermissionSetAssignment(AssigneeId = testUser.id, PermissionSetId = pSetGRC.Id);
        
        //Permission Set Assignment (SCEA Sharing)
        PermissionSet pSetSharing = [SELECT Name FROM PermissionSet WHERE NAME = 'SCEA_Sharing'];
        insert new PermissionSetAssignment(AssigneeId = testUser.id, PermissionSetId = pSetSharing.Id);
        
        return testUser;
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description This method creates a SCEA KARL Display
     * @return a test user (SCEA KARL Display)
    */
    public static User createDisplayUser(){
        List < Profile > standardProfileList = [SELECT Id FROM Profile WHERE Name = 'Nothing'];
        User testUser;
        if (!standardProfileList.isEmpty()) {
            Profile standardProfile = standardProfileList[0];
            testUser = new User(Alias = 'alias', Email = 'abc@b.com', EmailEncodingKey = 'utf-8',
                                FirstName = 'SCEA_Display', LastName = 'lname', LanguageLocaleKey = 'en_US',
                                LocaleSidKey = 'en_US', ProfileId = standardProfile.Id, TimeZoneSidKey = 'America/Los_Angeles',
                                UserName = 'display@b.com.e');
        }
        insert testUser;
        
        //Permission Set Assignment (SCEA Ops Admin)
        PermissionSet pSet = [SELECT Name FROM PermissionSet WHERE NAME = 'SCEA_Karl_Display'];
        insert new PermissionSetAssignment(AssigneeId = testUser.id, PermissionSetId = pSet.Id);
        
        //Permission Set Assignment (GRC Central Settings)
        PermissionSet pSetGRC = [SELECT Name FROM PermissionSet WHERE NAME = 'GRC_Central_Settings'];
        insert new PermissionSetAssignment(AssigneeId = testUser.id, PermissionSetId = pSetGRC.Id);
        
        return testUser;
    }

    /**
     * @author Mahima Aggarwal
     * @email mahima.aggarwal@mtxb2b.com
     * @description This method creates Community User
     * @param contactId - parent contact for the community user
     * @param usrName - username for the new user
     * @param isInsert - boolean to create a new record or not
     * @return a test user (Community)
    */
    public static User createCommunityUserwithSpecificCon(String contactId,String usrName, Boolean isInsert) {
        User testUser;
        //List<Account> communityAccountList = [SELECT Id FROM Account WHERE Name =: COMMUNITY_ACCOUNT];
        List<Profile> communityProfileList = [SELECT Id FROM Profile WHERE Name =: COMMUNITY_PROFILE_NAME];
        
        if (!communityProfileList.isEmpty()) {
            Profile standardProfile = communityProfileList[0];
            /* TODO: Refactor to also create account and contact in this method
            Account communityAccount = communityAccountList[0];

            Contact commContact = new Contact(FirstName = FIRSTNAME,
                                        LastName = LASTNAME,
                                        AccountId = communityAccount.Id,
                                        Email = STANDARD_EMAIL);
            if (isInsert){
                insert commContact;
            }*/

            testUser = new User( IsActive=true, 
                                CommunityNickname = 'test123',
                                Alias = STANDARD_ALIAS, 
                                Email = STANDARD_EMAIL, 
                                EmailEncodingKey = EMAIL_ENCODING_KEY,
                                FirstName = FIRSTNAME,
                                LastName = LASTNAME,
                                LanguageLocaleKey = LANGUAGE_LOCALE_KEY,
                                LocaleSidKey = LOCALE_SID_KEY,
                                ProfileId = standardProfile.Id,
                                TimeZoneSidKey = TIMEZONE_SID_KEY,
                                UserName = usrName);       
            testUser.ContactId = contactId;
        }
        
        if (isInsert){
            insert testUser;
        }

        return testUser;
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description This method creates an Area Requirement
     * @param reference - reference for the Area Requirement to create
     * @param areaofcompliance - picklist API value
     * @return audit firm object (requires insertion)
    */
    public static Area_Requirement__c createAreaRequirement(String reference, String areaofcompliance){
        Area_Requirement__c areaRequirement = new Area_Requirement__c();
        areaRequirement.Requirement_Reference__c = reference;
        areaRequirement.Areas_of_Compliance__c = areaofcompliance;
        areaRequirement.Description__c = 'Test Area Requirement';
        return areaRequirement;
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description This method maps an Area Requirement to a Request
     * @param requestId - id of the request to map
     * @param areaId - id of the area requirement to map
     * @return request item area object (needs inserted)
    */
    public static Request_Item_Area__c mapAreaRequirement(String requestId, String areaId){
        Request_Item_Area__c areaMap = new Request_Item_Area__c();
        areaMap.Request_Master_Data__c = requestId;
        areaMap.Area_Requirement__c = areaId;
        return areaMap;
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description This method creates an CCF Control Record
     * @return CCF Control Record (requires insertion)
    */
    public static KARL_GRC_Controls__c createCcfControl(){
        KARL_GRC_Controls__c ccfControl = new KARL_GRC_Controls__c();
        ccfControl.Name = 'x';
        ccfControl.Object_ID__c = 'EX-01';
        ccfControl.Object_Name__c = 'EX-01';
        ccfControl.Description__c = 'CCF Control Description';
        ccfControl.Short_Description__c = 'Test Case CCF Control';
        return ccfControl;
    }
    
     /**
      * @author Lakshya Arora
     * @email lakshya.arora@mtxb2b.com
     * @description This method creates an Internal KARL Gus Product Tag
     * @param isActive: whether an active product tag or inactive
     * @param isScrumTeamActive: whether scrum team is active or inactive
     * @return Internal KARL Gus Product Tag (requires insertion)
    */
    public static KARL_Gus_Product_Tag__c createKarlGusProductTag(Boolean isActive,Boolean isScrumTeamActive){
        KARL_Gus_Product_Tag__c prodtag = new KARL_Gus_Product_Tag__c();
        prodtag.Active__c = isActive;
        prodtag.Active_Scrum_Team__c = isScrumTeamActive;
        return prodtag;
    }
    
    /**
     * @author Lakshya Arora
     * @email lakshya.arora@mtxb2b.com
     * @description This method creates an Auto Assignment Configuration
     * @param defaultProductTagId - id of default product tag
     * @param noOfRecords - number of records for bulk testing
     * * @return KARL_Auto_Assignment_Configuration__c (requires insertion)
    */
    public static List<KARL_Auto_Assignment_Configuration__c> createAutoAssignmentConfig(String defaultProductTagId,Integer noOfRecords){
        List<KARL_Auto_Assignment_Configuration__c> autoAssignmentConfigList = new List<KARL_Auto_Assignment_Configuration__c>();
        for(Integer i=0;i<noOfRecords;i++){
            KARL_Auto_Assignment_Configuration__c aacObj = new KARL_Auto_Assignment_Configuration__c();
            aacObj.Priority_0_Assignment_Date__c = System.today();
            aacObj.Priority_1_Assignment_Date__c = System.today();
            aacObj.Priority_2_Assignment_Date__c = System.today();
            aacObj.Priority_3_Assignment_Date__c = System.today();
            aacObj.Days_to_Complete__c = 10;
            aacObj.Default_Product_Tag__c = defaultProductTagId;
            autoAssignmentConfigList.add(aacObj);
        }
        return autoAssignmentConfigList;
    }
    
     /**
     * @author Lakshya Arora
     * @email lakshya.arora@mtxb2b.com
     * @description This method creates a cycle audit scope report
     * @param auditMasterDataId - id of audit Scope Report
     * @param auditCycleId - id of Audit Cycle
     * @param noOfRecords - number of records for bulk testing
     * * @return KARL_Cycle_Audit_Report_Items__c (requires insertion)
    */
    public static KARL_Cycle_Audit_Report_Items__c createCycleAuditScopeReport(String auditMasterDataId,String auditCycleId){
        KARL_Cycle_Audit_Report_Items__c cycleAuditReportItem = new KARL_Cycle_Audit_Report_Items__c();
        cycleAuditReportItem.Audit_Cycle__c = auditCycleId;
        cycleAuditReportItem.Audit_Reports_Master_Data__c = auditMasterDataId;
        return cycleAuditReportItem;
    }
    
    /**
     * @author Lakshya Arora
     * @email lakshya.arora@salesforce.com
     * @description This method creates Audit Firm Stakeholder
     * @param auditFirmId - id of audit_firm__c
     * @param stakeholderId - id of Contact
     * @return Audit Firm Stakeholder (requires insertion)
    */
    public static KARL_Audit_Firm_Stakeholders__c createAuditFirmStakeholders(String auditFirmId, String stakeholderId){
        KARL_Audit_Firm_Stakeholders__c auditFirmStakeholderObj = new KARL_Audit_Firm_Stakeholders__c();
        auditFirmStakeholderObj.Audit_Firm__c = auditFirmId;
        auditFirmStakeholderObj.Stakeholder__c = stakeholderId;
        return auditFirmStakeholderObj;
    }
    
    /**
     * @author Lakshya Arora
     * @email lakshya.arora@salesforce.com
     * @description Fetch User by username
     * @param username
     * @return List of User record
    */
    public static List<User> fetchUser(String username){
        return [SELECT Id FROM USER WHERE username =: username];
    }

    /**
     * @author Lakshya Arora
     * @email lakshya.arora@mtxb2b.com
     * @description This method creates a KARL_Audit_Report_Finalization_Config__c record
     * @param1 cycleAuditReportId - Id of Cycle Audit Report Item record
     * @param2 auditFirmId - Id of Audit firm Id record
     * @param3 auditCycleCoverageId - Id of Audit Cycle Coverage record
     * @return KARL_Audit_Report_Finalization_Config__c (requires insertion)
    */
    public static KARL_Audit_Report_Finalization_Config__c createAuditFinalizationConfiguration(String cycleAuditReportId,String auditFirmId,String auditCycleCoverageId){
        KARL_Audit_Report_Finalization_Config__c finalizationConfigObj = new KARL_Audit_Report_Finalization_Config__c();
        if(String.isNotBlank(cycleAuditReportId))
        finalizationConfigObj.Cycle_Audit_Report_Items__c = cycleAuditReportId;
        if(String.isNotBlank(auditFirmId))
        finalizationConfigObj.Audit_Firm__c = auditFirmId;
        //if(String.isNotBlank(auditCycleCoverageId))
        //finalizationConfigObj.Audit_Cycle_Coverage__c = auditCycleCoverageId;
        return finalizationConfigObj;
    }

    /**
     * @author Lakshya Arora
     * @email lakshya.arora@mtxb2b.com
     * @description This method creates a KARL_Audit_Finalization_Signatories__c record
     * @param1 contactId - Id of Contact record
     * @param2 cycleAuditReportItemId - Id of Cycle Audit Report Item record
     * @return KARL_Audit_Finalization_Signatories__c (requires insertion)
    */
    public static KARL_Audit_Finalization_Signatories__c createAuditFinalizationSignatory(String contactId,String cycleAuditReportItemId){
        KARL_Audit_Finalization_Signatories__c finalizationSignatoriesObj = new KARL_Audit_Finalization_Signatories__c();
        finalizationSignatoriesObj.Contact__c = contactId;
        finalizationSignatoriesObj.KARL_Cycle_Audit_Report_Items__c = cycleAuditReportItemId;
        return finalizationSignatoriesObj;
    }

    /**
     * @author Lakshya Arora
     * @email lakshya.arora@mtxb2b.com
     * @description This method creates a KARL_Audit_Signature_Templates__c record
     * @param1 auditFinalizationConfigId - Id of KARL_Audit_Report_Finalization_Config__c record
     * @param2 templateId - Docusign Template Id
     * @param3 templateId - name of docusign template
     * @return KARL_Audit_Signature_Templates__c (requires insertion)
    */
    public static KARL_Audit_Signature_Templates__c createAuditSignatureTemplate(String auditFinalizationConfigId,String templateId,String name){
        KARL_Audit_Signature_Templates__c auditSignatureTemplateObj = new KARL_Audit_Signature_Templates__c();
        auditSignatureTemplateObj.Audit_Report_Finalization_Configuration__c = auditFinalizationConfigId;
        auditSignatureTemplateObj.KARL_Template_ID__c = templateId;
        auditSignatureTemplateObj.Name = name;
        return auditSignatureTemplateObj;
    }
    /**
     * @author Lakshya Arora
     * @email lakshya.arora@mtxb2b.com
     * @description This method creates a Account record
     * @param1 accountName - Name for account record
     * @return Account (requires insertion)
    */
    public static Account createAccount(String accountName){
        Account acc = new Account();
        acc.Name = accountName;
        return acc;
    }

    /**
     * @author Lakshya Arora
     * @email lakshya.arora@mtxb2b.com
     * @description This method creates a Contact record
     * @param1 accountId - Id of account record
     * @param2 lastName - Last name for contact record
     * @param3 email - email for contact record
     * @return Contact (requires insertion)
    */
    public static Contact createContact(String accountId,String lastName,String email){
        Contact con = new Contact();
        con.AccountId = accountId;
        con.LastName = lastName;
        con.Email = email;
        return con;
    }

    /**
     * @author Lakshya Arora
     * @email lakshya.arora@mtxb2b.com
     * @description This method creates a Contact record
     * @param1 cycleArlItemId - Id of Cycle Arl Item record
     * @return KARL_Evidence_Request__c (requires insertion)
    */
    public static KARL_Evidence_Request__c createEvidenceRequest(String cycleArlItemId){
        KARL_Evidence_Request__c evidenceRequestObj = new KARL_Evidence_Request__c();
        evidenceRequestObj.KARL_Cycle_ARL_Item__c = cycleArlItemId;
        return evidenceRequestObj;
    }


    /**
     * @author Lakshya Arora
     * @email lakshya.arora@mtxb2b.com
     * @description This method creates service desk record
     * @param1 cycleArlItemId - Record typeId of service desk record
     * @return KARL_Service_Desk__c (requires insertion)
    */
    public static KARL_Service_Desk__c createKARLServiceDesk(String recordTypeId){
        KARL_Service_Desk__c serviceDeskRecordObj = new KARL_Service_Desk__c();
        serviceDeskRecordObj.New_Request_Name__c = 'Request Name';
        serviceDeskRecordObj.RecordTypeId = recordTypeId;
        return serviceDeskRecordObj;
    }
}
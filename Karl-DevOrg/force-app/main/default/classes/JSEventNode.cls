public class JSEventNode
{
    public enum ENodeType {EVENT, SOURCE_ENDPOINT, DESTINATION_ENDPOINT}

    public String id;
    public String key;
    public ENodeType nodeType;
    private String label;
    public String description;
    public String src;
    public String dst;
    
    public String rawJsonEvtData;
    
    private String evtCriteria;
    
    private Boolean hasNullKey = false;
    
    //each node has a related event, src or dst elements, as each case is borken up into three elements
    ////used to highlight all relvent items
    public Set<String> relatedNodeKeys;
	public Set<String> relatedEdgeKeys;
    
    //specfic for node image defined by lib
    public String url;
    public String type;
    public String color;
    
    public String caseNumber;

    public Integer x;
    public Integer y;
    public Integer size;
    
    //how many times this event in tree. used in graph as related events for the alert type
    public String alertName;
    public String alertSubject;
    public String alertDescription;
    public String detectionLogic;
    public Integer relatedEvents;
  
    public class CaseReference
    {
        String CaseId;
        String CaseNumber;
        String Status;
    }
    
    public Set<CaseReference> caseRefs = new Set<CaseReference>();
    
    public void SetHasNullKey()
    {
        this.hasNullKey = true;
        this.SetLabel(' ');
    }
    
    //TODO
    public void addEventRef(Detection_Security_Event__c evt) {
    	
    }
    
    // public void addCaseRef(Case c)
    // {
    //     CaseReference cf = new CaseReference();
    //     cf.CaseId = c.Id;
    //     cf.CaseNumber = c.CaseNumber;
    //     cf.Status = c.Status;

    //     if(!doesCaseRefExist(c))
    //     {
    //         this.relatedEvents++;
    //     	this.caseRefs.add(cf);
    // 	}
    // }
    
    boolean doesCaseRefExist(Case c)
    {
        for(CaseReference cf : this.caseRefs)
        {
            if(cf.CaseNumber.equalsIgnoreCase(c.CaseNumber))
                return true;
        }
        
        return false;
    }
    
    public void SetLabel(String txt)
    {
        Integer kMaxLabelLength = 40;
     
        if(txt != null && txt != '')
        {
            if(txt.length() > kMaxLabelLength)
                this.label = txt.substring(0, kMaxLabelLength) + '...';
            else
                this.label = txt;      
        }
        else
            this.label = 'Undefined';   
        
        //label = evtCriteria;
    }
    
    public JSEventNode(Detection_Security_Event__c evt, String key, ENodeType nodeType)
    {
        try
        {
            //NOTE ID SET WHEN ADDED TO GRAPH
            //
            
            this.rawJsonEvtData = evt.FieldMapRaw__c;
            
            //String x1 = '{\"Environment\": \"SFDC-IT\", \"RegexMatch\": \"WINDOWS_FAILED_LOGON\"}';
            //String x1 = '{"Code": "W3sF4s", "RegexMatch": "12121212"}';
            //
            
            Integer startIndex = this.rawJsonEvtData.indexOf('RegexMatch":');
            Integer endIndex = this.rawJsonEvtData.indexOf('",', startIndex);
            //endIndex = startIndex+5;
            
            //String x1 = ' ' + startIndex + ' ' + endIndex; 45 75
            String x1 = this.rawJsonEvtData.substring(13+startIndex, endIndex) + ' / ' + key;
                
            //System.debug(x1);
            //System.debug(this.rawJsonEvtData);
            
            //JSONObject jsn =  new JSONObject( new JSONObject.JsonTokener(x1) ) ;
      
            this.nodeType = nodeType;
            
            //this.evtCriteria = evt.DetectionSecurityEventCriteria__r.Id;
            
            this.caseNumber = '';

            //TODO should not of commented out. need to double check null keys etc
            /*if(key.contains(':::IS_A_NULL_KEY'))
            	this.SetHasNullKey();
			else
                this.SetLabel(key);*/
            
            //JSONObject.value p = jsn.getValue('RegexMatch');
            //this.SetLabel( p.valueToString() );
            //
            
            this.SetLabel(x1);
            
            this.relatedEvents = 0;
            
            this.alertName = '';
            this.alertSubject = '';
            //this.alertDescription = c.Description;
            this.detectionLogic = '';
            
            relatedNodeKeys = new Set<String>();
            relatedEdgeKeys = new Set<String>();
            
            this.key = key;
            
            this.color = 'black';
            
            if(this.nodeType == ENodeType.SOURCE_ENDPOINT || this.nodeType == ENodeType.DESTINATION_ENDPOINT)
            {
                if(!IPConverter.isValidIP(this.key))
                {
                    this.url = '/resource/1440746550000/CaseGraph/img/null.png';
                }
                else if(!IPConverter.isPrivateSpace(this.key))
                	this.url = '/resource/1440746550000/CaseGraph/img/externalNode.png';
               	else
                    this.url = '/resource/1440746550000/CaseGraph/img/node.png';
                
            	this.type = 'image';
            }
            else if(this.nodeType == ENodeType.EVENT)
            {
                //SOURCE
                //ALERT OR INCIDENT
                //
                
                this.url = '/resource/144074655000a0/CaseGraph/img/event.png';
            	this.type = 'image';
                //this.color = 'black';
            }
            
            //key is used to group nodes form alerts toegther so that we dont duplicate alerts to show on graph
            this.key = this.key.toLowerCase();
            
            //this.src = c.Alert_Source_IP__c;
            //this.dst = c.Destination_IP_Address__c;
            
            this.description = '';
            //this.addCaseRef(c);

            Integer canvasWidth = 1000;
            
            //might need to create circle around parent node e.g. using Sin Cos
            this.x = getRandomInt(-canvasWidth, canvasWidth);
            this.y = getRandomInt(-canvasWidth, canvasWidth);
            
            this.size = 1;
        }
        catch(Exception e)
        {
            System.debug('In JSCaseNode constructor. case sObject does not contain feild. Defaults returned');
            
            return;
        }
    }
    
    public static Integer getRandomInt (Integer lower, Integer upper)
	{
		return Math.round(Math.random() * (upper - lower)) + lower;
	}
}
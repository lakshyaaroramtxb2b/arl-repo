/**
* @author Ben Harvie
* @email ben.harvie@salesforce.com
* @description This is trigger handler class for Control_Scope__c.
*/
public with sharing class ARL_ControlScopeTriggerHandler {
    public static Set<Id> recordIdsName = new Set<Id>();
    public static Set<Id> controlId = new Set<Id>();
    public static Set<Id> recordIdsBaseline = new Set<Id>();
    
    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description This method gets triggered from Control Scope Trigger
    */
    public static void run(){
        // Before Update
        if( Trigger.isBefore && Trigger.isUpdate ){
            populateName();
            captureCcfDescription();
        }
        // Before Insert
        else if( Trigger.isBefore && Trigger.isInsert ){
            populateName();
            captureCcfDescription();
        }
        // After Update
        if( Trigger.isAfter && Trigger.isUpdate ){
            updateBaselineControlDetails();
            updateScopeNameIndex();
            updateRelated();
        }
        // After Insert
        else if( Trigger.isAfter && Trigger.isInsert ){
            updateBaselineControlDetails();
            updateScopeNameIndex();
        }
    }
    
    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description This method updates the Scope Name Index as a label instead of API Value
    */
    private static void updateScopeNameIndex(){
        /**
         * @author Ben Harvie
         * @email ben.harvie@salesforce.com
         * @description Recursion logic based on set of Ids
         */
        for(Id recursionCheck : Trigger.newMap.keySet()){
            if(recordIdsName.contains(recursionCheck)){
                System.debug(LoggingLevel.DEBUG, 'Already processed this record >> ' + recursionCheck);
                return; // Kill the run - we've already processed this Id
            }else{
                System.debug(LoggingLevel.DEBUG, 'New record to process >> ' + recursionCheck);
                recordIdsName.add(recursionCheck); // add Id to set so we don't run it again
            }
        }

        List<Control_Scope__c> controlScopes = new List<Control_Scope__c>();

        for( Control_Scope__c  csItems : [SELECT Id,toLabel(Scope_Name__c),KARL_Triggered_Scope_Name__c 
                                        FROM Control_Scope__c WHERE Id in: (List<Control_Scope__c >)Trigger.New ]){
            // Update only if different
            if( csItems.KARL_Triggered_Scope_Name__c != csItems.Scope_Name__c ){
                // Set the Control Description in the Control Scope record
                Control_Scope__c csItem = new Control_Scope__c();
                csItem.Id = csItems.Id;
                csItem.KARL_Triggered_Scope_Name__c = csItems.Scope_Name__c;
                controlScopes.add(csItem);
            }
        }
        
        // Make the update accounting for sharing and permission sets
		if( !controlScopes.isEmpty() && Schema.sObjectType.Control_Scope__c.isUpdateable() 
          	&& Control_Scope__c.KARL_Triggered_Scope_Name__c.getDescribe().isUpdateable() ){
            update controlScopes;
        }
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Update the related Controls and Control Scopes when a change is made
    */
    public static void updateRelated(){
        System.debug(LoggingLevel.DEBUG, '--- START Updating Related Control Tests ---');
        //System.debug(LoggingLevel.DEBUG, 'Control Scope >> ' + (List<Control_Scope__c>)Trigger.New);
        for(Id recursionCheck : Trigger.newMap.keySet()){
            if(controlId.contains(recursionCheck)){
                System.debug(LoggingLevel.DEBUG, 'Already processed this record >> ' + recursionCheck);
                return; // Kill the run - we've already processed this Id
            }else{
                System.debug(LoggingLevel.DEBUG, 'New record to process >> ' + recursionCheck);
                controlId.add(recursionCheck); // add Id to set so we don't run it again
            }
        }

        // Update related Control Tests
        Map<Id, List<Control_Test__c>> controlTestItems = new Map <Id, List<Control_Test__c>>();
        for(Control_Test__c ctItem : [SELECT Id,Control_Scope__c FROM Control_Test__c 
                                WHERE Control_Scope__c IN: (List<Control_Scope__c>)Trigger.New
                                WITH SECURITY_ENFORCED]){
            List<Control_Test__c> ctItems = new List<Control_Test__c>();
            if( controlTestItems.containsKey(ctItem.Control_Scope__c) ){
                ctItems = controlTestItems.get(ctItem.Control_Scope__c);
            }
            ctItems.add(ctItem);
            
            controlTestItems.put(ctItem.Control_Scope__c, ctItems);              
        }
                                               
        System.debug('Related Control Tests >> ' + controlTestItems);

        // Lists for Update Tracking
        List<Control_Test__c > controlTestsToUpdate = new List<Control_Test__c>();

        // Process Triggered CCF Updates
        for(Control_Scope__c controlScope : (List<Control_Scope__c>)Trigger.New ){

            // Process each Control Test
            if(!controlTestItems.isEmpty()){
                System.debug(LoggingLevel.DEBUG, 'Processing >> ' + controlScope.Id);
                if(controlTestItems.containskey(controlScope.Id)){
                    for(Control_Test__c controlTestItem : controlTestItems.get(controlScope.Id)){
                        controlTestsToUpdate.add(controlTestItem);
                    }
                }
            }
        }

        // Update Control Tests
        if(!controlTestsToUpdate.isEmpty() && Schema.sObjectType.Control_Test__c.isUpdateable()){ 
            update controlTestsToUpdate;
        }

        System.debug(LoggingLevel.DEBUG, '--- END Updating Related Control Tests ---');
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description This method updates the Baseline Control Description, Name and Number in the Control Scope record.
    */
    private static void updateBaselineControlDetails(){
        /**
         * @author Ben Harvie
         * @email ben.harvie@salesforce.com
         * @description Recursion logic based on set of Ids
         */
        for(Id recursionCheck : Trigger.newMap.keySet()){
            if(recordIdsBaseline.contains(recursionCheck)){
                System.debug(LoggingLevel.DEBUG, 'Already processed this record >> ' + recursionCheck);
                return; // Kill the run - we've already processed this Id
            }else{
                System.debug(LoggingLevel.DEBUG, 'New record to process >> ' + recursionCheck);
                recordIdsBaseline.add(recursionCheck); // add Id to set so we don't run it again
            }
        }
        
        // Variable Declarations
        List<Control_Scope__c> controlScopes = new List<Control_Scope__c>();

        for( Control_Scope__c  csItems : [SELECT Id,Control_Description__c,Control_Name__c,Control_Name__r.Name,
                                        Control_Name__r.Control_Description__c,Control_Name__r.KARL_Indexed_Control_Number__c,
                                        KARL_Triggered_Control_Number__c, KARL_Triggered_Control_Name__c, Scope_Name__c,
                                        Triggered_Control_Identifier__c, Triggered_Full_Control_Name__c, Modified_Control_Identifier__c
                                        FROM Control_Scope__c WHERE Id in: (List<Control_Scope__c >)Trigger.New ]){
            // Make sure the Control is mapped to a Parent
            String ctrlIdentifier   = String.isBlank(csItems.Modified_Control_Identifier__c) ? csItems.Control_Name__r.KARL_Indexed_Control_Number__c : csItems.Modified_Control_Identifier__c;
            String fullControlId    = ctrlIdentifier + '-' + csItems.Scope_Name__c;
            String fullControlName  = fullControlId + ' - ' + csItems.Control_Name__r.Name;    

            if( csItems.Control_Name__c != null 
                && (csItems.Control_Description__c != csItems.Control_Name__r.Control_Description__c 
                || csItems.KARL_Triggered_Control_Number__c != csItems.Control_Name__r.KARL_Indexed_Control_Number__c 
                || csItems.KARL_Triggered_Control_Name__c != csItems.Control_Name__r.Name
                || csItems.Triggered_Control_Identifier__c != fullControlId
                || csItems.Triggered_Full_Control_Name__c != fullControlName )){
                // Set the Baseline Control Details in the Control Scope Record
                Control_Scope__c csItem = new Control_Scope__c();
                csItem.Id = csItems.Id;
                csItem.Triggered_Control_Identifier__c      = fullControlId;
                csItem.KARL_Triggered_Control_Number__c     = csItems.Control_Name__r.KARL_Indexed_Control_Number__c;
                csItem.KARL_Triggered_Control_Name__c	    = csItems.Control_Name__r.Name;
                csItem.Triggered_Full_Control_Name__c       = fullControlName;
                csItem.Control_Description__c = csItems.Control_Name__r.Control_Description__c;
                controlScopes.add(csItem);
            }
        }
        
        // Make the update accounting for sharing and permission sets
		if( !controlScopes.isEmpty() && Schema.sObjectType.Control_Scope__c.isUpdateable() 
          	&& Control_Scope__c.Control_Description__c.getDescribe().isUpdateable() 
          	&& Control_Scope__c.KARL_Triggered_Control_Name__c.getDescribe().isUpdateable() 
          	&& Control_Scope__c.KARL_Triggered_Control_Number__c.getDescribe().isUpdateable() 
          	&& Control_Scope__c.Triggered_Control_Identifier__c.getDescribe().isUpdateable()
          	&& Control_Scope__c.Triggered_Full_Control_Name__c.getDescribe().isUpdateable() ){
            update controlScopes;
        }
    }

    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description Populate the Name field with appropriate information
    */
    public static void populateName(){
        System.debug(LoggingLevel.DEBUG, '--- START Updating Control Scope Name ---');

        // Build list of controls
        Map<Id, Control__c> controlItems = new Map <Id, Control__c>([SELECT Id,Name,KARL_Indexed_Control_Number__c FROM Control__c 
                                                                        WITH SECURITY_ENFORCED]);

        // Update the name of the records
        Map<Id, Control_Scope__c> oldMap = (Map<Id, Control_Scope__c>)Trigger.oldMap;
        for(Control_Scope__c controlScope : (List<Control_Scope__c>)Trigger.New ){
            // Only process if Control Name is set
            if(!String.isBlank(controlScope.Control_Name__c)){  
                String ctrlIdentifier = String.isBlank(controlScope.Modified_Control_Identifier__c) ? controlItems.get(controlScope.Control_Name__c).KARL_Indexed_Control_Number__c : controlScope.Modified_Control_Identifier__c;
                String newName = ctrlIdentifier + '-' + controlScope.Scope_Name__c + ' - ' + controlItems.get(controlScope.Control_Name__c).Name;
                Integer maxLength = 80;
                if(newName.length() > maxLength){
                    newName = newName.substring(0, maxLength);
                }
                
                if( Trigger.isInsert ||
                    ( Trigger.isUpdate && (controlScope.Name <> newName) ) ){
                    controlScope.Name = newName;
                }
            }
        }
        System.debug(LoggingLevel.DEBUG, '--- END Updating Control Scope Name ---');
    }
    
    /**
     * @author Ben Harvie
     * @email ben.harvie@salesforce.com
     * @description This method captures the CCF Control Description when aligned (updating Modified Language)
    */
    private static void captureCcfDescription(){
        /**
         * @author Ben Harvie
         * @email ben.harvie@salesforce.com
         * @description Recursion logic based on set of Ids
         */

        // Build map of the CCF Controls
        //List<KARL_GRC_Controls__c> relevantControl;
        Map<Id, List<KARL_GRC_Controls__c>> ccfControls = new Map<Id, List<KARL_GRC_Controls__c>>();
        for ( KARL_GRC_Controls__c  ccfControlItems : [SELECT Id,Description__c,Short_Description__c
                                                        FROM KARL_GRC_Controls__c
                                                        WITH SECURITY_ENFORCED] ){
            if(!ccfControls.containsKey(ccfControlItems.Id)){
                ccfControls.put(ccfControlItems.Id,new List<KARL_GRC_Controls__c>());
            }
            ccfControls.get(ccfControlItems.Id).add(ccfControlItems);
        }
        
        for(Control_Scope__c controlScopeItem : (List<Control_Scope__c>)Trigger.New ){
            if(String.isNotBlank(controlScopeItem.CCF_Control__c) && controlScopeItem.KARL_CS_CCF_Alignment__c){
                System.debug(LoggingLevel.DEBUG, 'Aligning Control Descriptions with CCF : ' + controlScopeItem.Id);
                for(KARL_GRC_Controls__c ccfControlValues : ccfControls.get(controlScopeItem.CCF_Control__c)){
                    System.debug(LoggingLevel.DEBUG, 'CCF Control Values : ' + ccfControlValues);
                    System.debug(LoggingLevel.DEBUG, 'Current Control Values : ' + controlScopeItem);
                     // Update the Name
                    String curName      = controlScopeItem.Modified_Name__c;
                    String newName      = ccfControlValues.Short_Description__c;
                    Integer maxLength   = 80;
                    if(newName.length() > maxLength){
                        newName = newName.substring(0, maxLength);
                    }

                    // Update the Control Description
                    String controlDescription       = controlScopeItem.Control_Description__c;
                    String ccfControlDescription    = ccfControlValues.Description__c;

                    if( Trigger.isInsert ||
                    ( Trigger.isUpdate && (controlDescription <> ccfControlDescription || curName <> newName) ) ){
                        controlScopeItem.Modified_Language__c    = true;
                        controlScopeItem.Modified_Name__c        = newName;
                        controlScopeItem.Modified_Description__c = ccfControlDescription;
                    }
                }
            }else{
                System.debug(LoggingLevel.DEBUG, 'Control is not aligned with CCF : ' + controlScopeItem.Id);
            }
            
        }
    }
}
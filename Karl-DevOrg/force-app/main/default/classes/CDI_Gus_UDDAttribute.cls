public class CDI_Gus_UDDAttribute implements Database.Batchable<sObject>,Database.Stateful {
        DateTime starttime=DateTime.now();
        Integer rowsPassed=0;
    	Integer rowsFailed=0;
        Integer iterator=0;
        Set<String> externalIds=new Set<String>();
        Datetime lastExecution;
    String errors='';
    	String query;
     public Database.QueryLocator start(Database.BatchableContext bc) {

        List<CDI_Batch_Import__c>  batchimports=[Select name,Start_Time__c from CDI_Batch_Import__c Where Object_Type__c='GUS UDD Attributes' ORDER BY End_Time__c DESC limit 1];
		if (batchimports!=null && batchimports.size()>0){
            lastExecution = batchimports[0].Start_Time__c; 
            System.debug('Last Execution of UDD Entities on:'+lastExecution);
            query='select name__c,externalid,Description_c__c,entity_c__r.name__c,DataType_c__c,InternalDescription_c__c,IsDeleted__c  from MDS_Attribute_c__x where IsDeleted__c!=true AND LastModifiedDate__c >'+ lastExecution.formatGmt('yyyy-MM-dd\'T\'HH:mm:ss.SSSZ') +'AND (Status_c__c=\'Approved\' OR Status_c__c=\'Existing\') AND Type_c__c=\'Core\' ORDER BY Entity_c__c,Name__c,ExternalID DESC';
        }
        else {
            lastExecution = null; 
            System.debug('Last Execution of UDD Entities not found');
            query='select name__c,externalid,Description_c__c,entity_c__r.name__c,DataType_c__c,InternalDescription_c__c,IsDeleted__c  from MDS_Attribute_c__x where IsDeleted__c!=true  AND (Status_c__c=\'Approved\' OR Status_c__c=\'Existing\') AND Type_c__c=\'Core\' ORDER BY Entity_c__c,Name__c,ExternalID DESC';
            
        }
         return Database.getQueryLocator(query);
     }
    
    public void execute(Database.BatchableContext bc, List<MDS_Attribute_c__x> attrList){
        Set<CDI_Data_Asset__c> childDataAssetList=new Set<CDI_Data_Asset__c>();
         
        // Fetch Core DB id
       	CDI_Data_Store__c dataStore=[select id from CDI_Data_Store__c where Name='Core DB'];
        String errors='';
         // Fetch Data Asset of Type Table
        List <CDI_Data_Asset__c> parentDataAssetList=[select name,id from CDI_Data_Asset__c where Data_Asset_Type__c='Table' and IsDeleted__c!=true];
     	Map<String,ID> entityList=new Map<String,ID>();
        for(CDI_Data_Asset__c parentDataAsset:parentDataAssetList){
            entityList.put(parentDataAsset.Name,parentDataAsset.Id); 
         } 
         
         // Add Attributes to the Data Asset list.
         for (MDS_Attribute_c__x attr: attrList){
            if(!externalIds.contains('Core DB/'+attr.Entity_c__r.name__c+'/'+attr.Name__c)) {
            	if (entityList.ContainsKey(attr.Entity_c__r.name__c)){
        			CDI_Data_Asset__c dataAsset=new CDI_Data_Asset__c();
        			dataAsset.Name=attr.Name__c;		
        			String Name = attr.Name__c;
        			dataAsset.Data_Asset_Description__c=attr.Description_c__c;
            		dataAsset.Comments__c=attr.InternalDescription_c__c;
       				dataAsset.Source_Data_Asset_Code__c=attr.ExternalId;
            		dataAsset.Source_System_Name__c='UDD-GUS';
        			dataAsset.Data_Asset_Type__c='Column';
                    dataAsset.IsDeleted__c=attr.IsDeleted__c;
            		dataAsset.Is_Parent__c=False;
         			if (attr.DataType_c__c=='EncryptedText' || attr.DataType_c__c=='SfdcEncryptedText'){
             			dataAsset.IsEncrypted__c=True;
        			}
        			dataAsset.Status__c='Active';
        			dataAsset.Data_Store__c=datastore.Id;
        			dataAsset.Parent_Data_Asset__c=entityList.get(attr.Entity_c__r.name__c);
            		dataAsset.External_Id__c='Core DB/'+attr.Entity_c__r.name__c+'/'+dataAsset.Name;
                    externalIds.add('Core DB/'+Name);
        			childDataAssetList.add(dataAsset);
                    System.debug(childDataAssetList.size());
                }
            }    
         }
         
         
         // Upsert DataAsset List to CDI
         Schema.SObjectField externalId=CDI_Data_Asset__c.Fields.External_Id__c;
         List <CDI_Data_Asset__c> upsertDataAssetList=new List<CDI_Data_Asset__c>();
         upsertDataAssetList.addAll(childDataAssetList);
         System.debug(upsertDataAssetList.size());
         System.debug('List before upsert:'+upsertDataAssetList);
         List<Database.UpsertResult> upsertAssetResult=Database.upsert(upsertDataAssetList,externalId,false);
         System.debug('Number of rows processed:'+upsertAssetResult.size());
         for(Integer index = 0; index < upsertAssetResult.size(); index++) {
             if(upsertAssetResult[index].isSuccess()) {
                 rowsPassed++;
             }
             else {
                 rowsFailed++;
                 Database.Error error=upsertAssetResult.get(index).getErrors().get(0);
                 errors=error.getMessage();
            }
         }
         System.debug('Number of rows passed:'+rowsPassed);
         System.debug('Number of rows failed:'+rowsFailed);
    }
    public void finish(Database.BatchableContext bc){
         
         // Create Batch Import record
         
         CDI_Batch_Import__c batchImport = new CDI_Batch_Import__c();
         batchImport.Start_Time__c=starttime;
         batchImport.End_Time__c=DateTime.now();
         batchImport.Object_Type__c='GUS UDD Attributes';    
         batchImport.Rows_Succeeded__c=rowsPassed;
         batchImport.Rows_Failed__c=rowsFailed;
         batchImport.Errors__c=errors;
         System.debug('Errors:'+errors);
         System.debug('Finish GUS UDD Attributes Sync');
         insert batchImport;
     }
}
global with sharing class HW_RecordsFactory {
   public with sharing class EmailsRecords{
        public String email;
        public String ownername;
        public List<RecordsWithReducedAttributes> recsList;
        public Integer count;
        public Map<String, Integer> recsCount;

    }

    public with sharing class RecordsWithReducedAttributes{
        public String UserName;
        public String Email;
        public String Role;
        public String FirstName;
        public String LastName;
        public String OrgName;
        public String ManagerEmail;
        public String ManagerName;
        public String AddonName;
        public String Status;
        public String OrgID;
        public String AddonID;
        public String AppName;
        public String AppID;
        public String Query;
        public String AlertType;
        //public Integer RecCount;
    }
}
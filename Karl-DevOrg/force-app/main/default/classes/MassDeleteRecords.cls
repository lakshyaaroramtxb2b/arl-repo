public with sharing class MassDeleteRecords { private ApexPages.StandardSetController setCon; private string retURL = ''; 

    public MassDeleteRecords(ApexPages.StandardSetController controller) { 
        setCon = controller; 
        retURL = ApexPages.currentPage().getParameters().get('retURL'); 
    } 
    
    public PageReference DoDelete(){ 
        set<string> setToDelete = new set<string>(); 
        for(sObject c : setCon.getSelected()){ setToDelete.add(c.ID); 
        } 
        if(setToDelete.size() > 0){ 
            list<SC_CISO_CampaignMember__c> objItemList = new list<SC_CISO_CampaignMember__c>(); 
            objItemList = [Select ID From SC_CISO_CampaignMember__c Where ID IN :setToDelete]; 
            if(objItemList.size() > 0){ 
                delete objItemList; 
            } 
        } 
        if(string.isBlank(retURL) == false){ 
            return new PageReference(retURL); 
        }else{ //This should not get hit, but put the three digit prefix of you object here. This prefix can be programatically discovered too with something like this: /* string threeDigitPrefix = ''; 
            Schema.DescribeSObjectResult r; 
            r = SC_CISO_CampaignMember__c.sObjectType.getDescribe(); 
            String threeDigitPrefix = r.getKeyPrefix();
            return new PageReference('/' + threeDigitPrefix + '/o'); 
        } 
    } 
}
/**
 * @author Lakshya Arora
 * @email lakshya.arora@mtxb2b.com
 * @description Class for unit testing the KARL_ServiceDeskControllerHelper
 */
@isTest
public with sharing class KARL_ServiceDeskControllerHelperTest {
    /**
     * @author Lakshya Arora
     * @email lakshya.arora@mtxb2b.com
     * @description Method will create the Test Data
    */
    @TestSetup
    private static void makeData(){
        User opsAdminUser = ARL_TestDataFactory.createOpsAdmin();
        System.runAs(opsAdminUser){
            Account acc = ARL_TestDataFactory.createAccount('salesforce.com');
            insert acc;
            Contact con = ARL_TestDataFactory.createContact(acc.Id,'test','test@gmail.com.invalid');
            con.recordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Salesforce Employee').getRecordTypeId();
            insert con;

            Audit_Cycle__c auditCycleObj = ARL_TestDataFactory.createAuditCycle();
            insert auditCycleObj;

            Request_Item__c reqItemObj = ARL_TestDataFactory.createRequestItem();
            reqItemObj.KARL_GRC_Assignee__c = con.Id;
            insert reqItemObj;

            Cycle_Request_Item__c cycleRequestItemObj = ARL_TestDataFactory.createCycleRequestItem(reqItemObj.Id,auditCycleObj.Id);
            insert cycleRequestItemObj;

            KARL_Evidence_Request__c evidenceRequestObj = ARL_TestDataFactory.createEvidenceRequest(cycleRequestItemObj.Id);
            insert evidenceRequestObj;
            
            KARL_Audit_Scope_Master_Data__c auditscopeMasteDataObj = ARL_TestDataFactory.createAuditScope();
            auditscopeMasteDataObj.KARL_BU_Lead__c = con.Id;
            auditscopeMasteDataObj.KARL_Scope__c = 'AS';
            insert auditscopeMasteDataObj;
        }
    }
    /**
     * @author Lakshya Arora
     * @email lakshya.arora@mtxb2b.com
     * @description Method will unit test the getAuditScopeByKARLScope
    */
    @isTest
    private static void getAuditScopeByKARLScopeTest(){
        List<User> opsAdminUserList = ARL_TestDataFactory.fetchUser('abc@b.com.d');
        if(!opsAdminUserList.isEmpty()){
            System.runAs(opsAdminUserList[0]){
                System.assert(!Karl_ServiceDeskControllerHelper.getAuditScopeByKARLScope('AS').isEmpty(),'getAuditScopeByKARLScope failed');
            }
        }
    }
    /**
     * @author Lakshya Arora
     * @email lakshya.arora@mtxb2b.com
     * @description Method will unit test the getCycleARLItemById
    */
    @isTest
    private static void getCycleARLItemByIdTest(){
        List<User> opsAdminUserList = ARL_TestDataFactory.fetchUser('abc@b.com.d');
        if(!opsAdminUserList.isEmpty()){
            List<Cycle_Request_Item__c> cycleRequestItemList = [SELECT Id FROM Cycle_Request_Item__c];
            System.runAs(opsAdminUserList[0]){
                System.assert(!Karl_ServiceDeskControllerHelper.getCycleARLItemById(cycleRequestItemList[0].Id).isEmpty(),'getCycleARLItemById failed');
            }
        }
    }
}